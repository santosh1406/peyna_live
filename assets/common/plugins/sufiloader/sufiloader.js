/*
 * jQuery Loader Plugin (sufiloader.js)
 * @version: 0.1 (08/10/2015)
 * @requires jQuery v1.7.2 or later
 * @author : Suraj Rathod
 * Small loader
 * usage : $.loader();
 * $.loader(options) -> options =
 *  {
 *      
 * }
 *
 * To close loader : $.loader("close");
 *
 */
var SufiLoaderOptions = null;
(function($) {
    $.loader = function(option){

        switch(option)
        {
            case 'close':
                if(SufiLoaderOptions){
                    if($("#"+SufiLoaderOptions.id)){
                        $("#"+SufiLoaderOptions.id).remove();
                    }
                }
                return;
                break;

            default:

                var default_image = (option.content.defaultLoaderImage != "" && option.content.defaultLoaderImage != undefined) ? option.content.defaultLoaderImage : base_url+'assets/travelo/front/images/loader/default_loader.gif';
                var default_text = (option.content.defaultText != "" && option.content.defaultText != undefined) ? option.content.defaultText : (option.content.defaultText == null) ? "" : "Processing your request!";

                var options = $.extend(true, {
                    content:{   
                                id: "sufiContent",
                                class: "sufiBox",
                                text:"<table cellspacing='0' cellpadding='6' border='0' style='background-color: #ffffff;' id='tblUpdate'>"+
                                        "<tbody>"+
                                            "<tr style='width:40px;height:40px;vertical-align:middle;padding-top:10px;'>"+
                                                "<td style='padding-left:4px;vertical-align:middle;'>"+
                                                        "<img style='border-width:0px;' src='"+default_image+"'>"+
                                                "</td>"+
                                                "<td style='margin-left:10px;'></td>"+
                                                "<td style='vertical-align:middle;'>"+
                                                        "<span style='font-weight: bold; font-family: Calibri; font-size: medium;'>  "+default_text+"   </span>"+
                                                "</td>"+
                                            "</tr>"+
                                        "</tbody>"+
                                    "</table>"
                            },

                    background:{
                        opacity:0.4,
                        class:"progressBackground"
                    },

                    position_background:{
                        class:"sufiPosition",
                    },

                    id: "sufiLoader",

                }, option);
        }

        SufiLoaderOptions = options;

        var $sufidiv = $("<div/>", {id: "sufiLoader"});
        $sufidiv.append($("<div/>", {class: "sufiProgressBackground"}))
        $sufidiv = $sufidiv.append($("<div/>", {id:"sufiPosition", class: options.position_background.class, style: "padding: 4px; position: absolute; width: 100%;z-index: 1000000000;"}).append($("<div/>", {id:"sufiContent", class: options.content.class, style : "background-color: white;border-radius: 6px;padding: 15px;position:fixed;"})));

       $sufidiv.find("#sufiContent").html(options.content.text);

        if(jQuery.bgiframe){
            $sufidiv.bgiframe();
        }
        
        $sufidiv.appendTo('body');
    };  
})(jQuery);