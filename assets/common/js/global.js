var base_url = BASE_URL;
var csrf_token_patelinfo = '';
var countdown_expiry_msg = null;
var countdown_redirect_url = null;
msrtc_cb_url="http://localhost/cb_testing/index.php/admin/mlogin/cblogin/"  ;
/**************************** count down start here ***************************/
var CDown = function() {
    this.state=0;// if initialized
    this.counts=[];// array holding countdown date objects and id to print to {d:new Date(2013,11,18,18,54,36), id:"countbox1"}
    this.interval=null;// setInterval object
}

CDown.prototype = {
    init: function(){
        this.state=1;
        var self=this;
        this.interval=window.setInterval(function(){self.tick();}, 1000);
    },
    add: function(date,id){
        this.counts.push({d:date,id:id});
        this.tick();
        if(this.state==0) this.init();
    },
    expire: function(idxs){

        if(countdown_expiry_msg !== null)
        {
            var display_message = countdown_expiry_msg;
            countdown_expiry_msg = null;
            for(var x in idxs) {
                this.display(this.counts[idxs[x]], display_message);
                this.counts.splice(idxs[x], 1);
            } 
        }

        if(countdown_redirect_url != null)
        {
            var redirecturl = countdown_redirect_url;
            countdown_redirect_url = null;
            window.location.href = redirecturl;
        }
    },
    format: function(r){
        var out="";
        if(r.d != 0){out += r.d +" "+((r.d==1)?"day":"days")+", ";}
        if(r.h != 0){out += r.h +" "+((r.h==1)?"hour":"hours")+", ";}
        out += r.m +" "+((r.m==1)?"min":"mins")+", ";
        out += r.s +" "+((r.s==1)?"sec":"secs")+", ";

        return out.substr(0,out.length-2);
    },
    math: function(work){
        var y=w=d=h=m=s=ms=0;

        ms=(""+((work%1000)+1000)).substr(1,3);
        work=Math.floor(work/1000);//kill the "milliseconds" so just secs

        y=Math.floor(work/31536000);//years (no leapyear support)
        w=Math.floor(work/604800);//weeks
        d=Math.floor(work/86400);//days
        work=work%86400;

        h=Math.floor(work/3600);//hours
        work=work%3600;

        m=Math.floor(work/60);//minutes
        work=work%60;

        s=Math.floor(work);//seconds

        return {y:y,w:w,d:d,h:h,m:m,s:s,ms:ms};
    },
    tick: function(){
        var now=(new Date()).getTime(),
        expired=[],cnt=0,amount=0;

        if(this.counts)
            for(var idx=0,n=this.counts.length; idx<n; ++idx){
                cnt=this.counts[idx];
                amount=cnt.d.getTime()-now;//calc milliseconds between dates

                // if time is already past
                if(amount<0){
                    expired.push(idx);
                }
                // date is still good
                else{
                    this.display(cnt, this.format(this.math(amount)));
                }
            }

        // deal with any expired
        if(expired.length>0) this.expire(expired);

        // if no active counts, stop updating
        if(this.counts.length==0) window.clearTimeout(this.interval);
        
    },
    display: function(cnt,msg){
        document.getElementById(cnt.id).innerHTML=msg;
    }
};

var startCountDown = function(year, month, day, hour, minutes, seconds, display_id, expirymsg, redirecturl){
    var cdown = new CDown();
    countdown_expiry_msg = expirymsg;
    countdown_redirect_url = redirecturl
    cdown.add(new Date(year, month-1, day, hour, minutes, seconds), display_id);
}
/**************************** count down end here ***************************/

$(document).ready(function() {
    csrf_token_patelinfo = $('input[name="csrf_token"]').val();

    $.fn.reset_form = function (){
        $(this).find('input').val('');
        this[0].reset();        
        return $(this);
    }
    
    $.fn.chosenDestroy = function () 
    { 
        //$(this).chosen('destroy'); 
        $(this).removeClass('chzn-done'); 
        $(this).next().remove();
        
        //console.log(this);
        return $(this); 
    }
    
    $.icheck_reload = function() {
        // $("input[type='checkbox'], input[type='radio']").iCheck('destroy');
        // console.log('icheck');
        $("input[type='checkbox'], input[type='radio']").iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal'
        });       
    };

    $.icheck_update = function() {
        $("input[type='checkbox'], input[type='radio']").iCheck('update');
    };

    $.chosen_reload  = function(){
        
        var config = {
                '.chosen-select'           : {},
                '.chosen-select-deselect'  : {allow_single_deselect:true},
                '.chosen-select-no-single' : {disable_search_threshold:10},
                '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                '.chosen-select-width'     : {width:"95%"}
            }
        
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    };

    $.fn.timmer = function(){
        var elem = this;
        var myVar = setInterval(function(){
            var d = new Date();
            elem.html(d.toLocaleTimeString())
        },1000);
    }


    $.ui.autocomplete.filter = function (array, term) {
      
        var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");

        return $.grep(array, function (value) {
            return matcher.test(value.label || value.value || value);
        });
    };
    
    $(document).off('keypress', '.alpha').on('keypress', '.alpha', function(e) {
           e.preventDefault(); 
           var regex = new RegExp("^[a-zA-Z\b]+$");
             var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);

             if (regex.test(str)) {
                 return true;
             }


             return false;

         }); 


    $(document).off('keydown', '.alpha-only').on('keydown', '.alpha-only', function(e) {

        // if (e.shiftKey || e.ctrlKey || e.altKey)
        if (e.ctrlKey || e.altKey)
        {
            e.preventDefault();
        }
        else
        {
            // var key = e.keyCode;
            var key = (e.keyCode ? e.keyCode : e.which);
            if (!((key == 8) || (key == 9) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90)))
            {
                e.preventDefault();
            }
        }
    });

    $(document).off('keydown', '.age').on('keydown', '.age', function(e) {

        if (e.shiftKey || e.ctrlKey || e.altKey)
        {
            e.preventDefault();
        }
        else
        {
            var key = e.keyCode;

            if (!((key == 8) || (key == 9) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)))
            {
                e.preventDefault();
            }
        }
    });


    $(document).off('keydown blur', '.reg-alpha-only').on('keydown blur', '.reg-alpha-only', function(e) {
        var current_val = $(this).val();
        var filter = /^[a-zA-Z ]*$/;
        if(!filter.test(current_val))
        {
            $(this).val(current_val.substr(0, current_val.length - 1));
        }
    });

    $(document).off('keydown blur', '.reg-number-only').on('keydown blur', '.reg-number-only', function(e) {
        var current_val = $(this).val();
        var filter = /^[0-9]*$/;
        if(!filter.test(current_val))
        {
            $(this).val("");
        }
    });


    $(document).off('keydown', '.numbers-only').on('keydown', '.numbers-only', function(e) {

        if (e.shiftKey || e.ctrlKey || e.altKey)
        {
            e.preventDefault();
        }
        else
        {
            var key = e.keyCode;
            if (!((key == 8) || (key == 9) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)))
            {
                e.preventDefault();
            }
        }
    });

    $.validator.addMethod("email", function (value, element) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    
        if (filter.test(value)) {
            return true;
        }
        else {
            return false;
        }
    }, 'Please enter valid email address');

})// end of document


function icheck()
{
    $("input[type='checkbox'], input[type='radio']").iCheck({
        checkboxClass: 'icheckbox_minimal',
        radioClass: 'iradio_minimal'
    });
}


/*
 function    : get_data
 description : function will ajax response
 
 @param         ajax_url        URL where call ajax call is to be made 
 @param         form            id of 'form' from which data is to be serialized and submitted
 @param         form_div        id of 'form' where ajax response is loaded
 */
var time_limit = 1000;
function get_data(ajax_url, form, form_div, extra_parameters, callback_function, callback_function_on_complete, async_flag) {
    if (typeof (async_flag) == 'undefined') {
        async_flag = true;
    }

    var  _opt = {
        type: "POST",
        url: base_url + ajax_url,
        dataType: 'json',
        cache: false,
        async: async_flag,
        data: '',

    };

    var data = "";

    csrf_token_patelinfo = $('input[name="csrf_token_patelinfo"]').val();

    var cookie_value = csrf_token_patelinfo;
    if (cookie_value == undefined)
    {
        var data = "";
    }
    else if (form == "")
    {
        var data = 'csrf_token_patelinfo=' + cookie_value;
    }
    else {
        var data = 'csrf_token_patelinfo=' + cookie_value;
    }

    data = data + 'rokad_token=' + rokad_token;
    //var data  = "csrf_token_patelinfo="+csrf_token_patelinfo;

    if (form != "") {
        data = $("#" + form).serialize();

        var formType = $('#'+form).attr('enctype');

        if(typeof(formType) != 'undefined' && formType != false && formType == 'multipart/form-data')
        {
            data = new FormData( document.getElementById(form));
            _opt.processData = false;
            _opt.contentType = false;

        }

    }

    if (extra_parameters != undefined) {
        for (var key in extra_parameters)
        {
            data = data + "&" + key + "=" + extra_parameters[key];
        }
    }

    _opt.data       = data;
    _opt.success    = function(response) {
        var json_data_object = response;
        var success_flag = response['flag'];
        if ((success_flag == '@#success#@') || (success_flag == '@#sorry#@') || (success_flag == '@#error#@')) {
            var view = response['view'];
            if (jQuery.isArray(form_div)) {
                for (var key in form_div) {
                    if (response[key] != undefined) {
                        if (response[key] == "0") {
                            $("#" + form_div[key]).hide();
                        }
                        else {
                            $("#" + form_div[key]).show();
                            $("#" + form_div[key]).html(response[key]);
                        }
                    }
                }
            }
            else {
                $("#" + form_div).html(view);
            }
        }
        else if (response.redirect_url) {
            window.location.href = response['redirect_url'];
        } else if (response.redirect_html) {
            $(response.redirect_target_element).html(response.redirect_html);
        }

        if (typeof callback_function == 'function') { // make sure the callback is a function
            callback_function(response); // brings the scope to the callback
        }

    };

    _opt.complete = function() {
        if (typeof callback_function_on_complete == 'function') {
            callback_function_on_complete();
        }

    //reset_message_timer();
    };

    $.ajax( _opt );
}

/*
 function    : get_data
 description : function will ajax response
 
 @param         elem                            Element eg #from | .from
 @param         url                             url for Json data
 @param         callback_function_on_select     function on select
 */


function make_autocomplete(elem, dSource, callback_function_on_select )
{
    var option = {
        source: function( request, response ) {
            $.ajax({
                url: BASE_URL+dSource,
                dataType: "json",
                data: {
                    q: request.term
                },
                success: function( data ) {
                    response( data );
                }
            });
        },
        autoFocus: true,
        minLength: 1,
        select: function( event, ui ) {
            if (typeof callback_function_on_select == 'function') { // make sure the callback is a function
                callback_function_on_select(ui); // brings the scope to the callback
            }
        // log( ui.item ?
        //   "Selected: " + ui.item.label :
        //   "Nothing selected, input was " + this.value);
        },
        open: function() {
            $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function() {
            $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        },
        change: function(event, ui){
            // if (!ui.item) {
            //     $(this).val('');
            // }
        }
    };

    if( typeof(dSource) =='object')
    {
        option.source = dSource;
    }

    $(elem).autocomplete(option);
}

function show(data){
    console.log(data);
}


/******* DateJavascriptFormat javascript function start here *******/
function DateJavascriptFormat(sdate, add_day , minus_day)
{
    if(typeof(add_day) == "undefined")
    {
        add_day = ""  
    }

    if(typeof(minus_day) == "undefined")
    {
        minus_day = ""  
    }
        
    if(sdate != "")
    {
        var date_formats = {};
        var short_months_name = ["","Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ]; 
        var full_months_name = ['','January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']; 
        var sdate_array = sdate.split("-");
        var day = sdate_array[0];
        var month = sdate_array[1];
        var year = sdate_array[2];
        var javascript_date = day+"/"+month+"/"+year;
        date_formats["day"] = day;
        date_formats["month"] = month;
        date_formats["year"] = year;
        date_formats["short_month_name"] = short_months_name[parseInt(month)];
        date_formats["full_month_name"] = full_months_name[parseInt(month)];
        date_formats["javascript_date"] = javascript_date;
        date_formats["search_full_date"] = day+"-"+short_months_name[parseInt(month)]+"-"+year;
        date_formats["full_search_date"] = day+"-"+month+"-"+year;

        if(minus_day != "")
        {
            var min_date = new Date(year, month, day);
            min_date.setDate(min_date.getDate() - minus_day);
            var p_min_date = new Date(min_date);
            var pre_day= p_min_date.getDate();
            var pre_year = p_min_date.getFullYear();
            var pre_month = p_min_date.getMonth();
            var pre_short_month_name = short_months_name[parseInt(pre_month)];
            var pre_full_month_name = short_months_name[parseInt(pre_month)];

            if(pre_month <= 9)
                pre_month = '0'+pre_month;

            if(pre_day <= 9)
                pre_day = '0'+pre_day;

            date_formats["prev_date"] = p_min_date;
            date_formats["prev_day"] = pre_day;
            date_formats["prev_month"] = pre_month;
            date_formats["prev_short_month_name"] = pre_short_month_name;
            date_formats["prev_full_month_name"] = pre_full_month_name;
            date_formats["prev_year"] = pre_year;
            date_formats["prev_full_date"] = pre_day+"-"+pre_month+"-"+pre_year;

        }
       
        if(add_day != "")
        {
            var max_date = new Date(year, month, day);
            max_date.setDate(max_date.getDate() + add_day);
            var p_max_date = new Date(max_date);
            

            var next_day= p_max_date.getDate();
            var next_year = p_max_date.getFullYear();
            var next_month = p_max_date.getMonth();
            var next_short_month_name = short_months_name[parseInt(next_month)];
            var next_full_month_name = short_months_name[parseInt(next_month)];

            if(next_month <= 9)
                next_month = '0'+next_month;

            if(next_day <= 9)
                next_day = '0'+next_day;

            date_formats["next_date"] = p_max_date;
            date_formats["next_day"] = next_day;
            date_formats["next_month"] = next_month;
            date_formats["next_short_month_name"] = next_short_month_name;
            date_formats["next_full_month_name"] = next_full_month_name;
            date_formats["next_year"] = next_year;
            date_formats["next_full_date"] = next_day+"-"+next_month+"-"+next_year;
        }

        return date_formats;
    }
}



function date_operations(first_date, second_date)
{
    var js_date = {};
    js_date["is_null"] = false;

    first_date = (typeof(first_date) == "undefined") ? "" : first_date;
    second_date = (typeof(second_date) == "undefined") ? "" : second_date;

    if(first_date != "" && second_date != "")
    {
        var d1 = $.datepicker.parseDate('dd-mm-yy', first_date );
        var d2 = $.datepicker.parseDate('dd-mm-yy', second_date );
        js_date["difference"] = ((d2 - d1) / 86400000);
        js_date["date_compare"] = d1 <= d2;
    }
    else
    {
        js_date["is_null"] = true;
    }

    return js_date;
}


function current_date()
{
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!

    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 

    today = dd+'-'+mm+'-'+yyyy;

    return today
}

/******* DateJavascriptFormat javascript function end here *******/


function strtime_to_hours(str_time)
{
    var hours = Math.floor(str_time / 60);
    var minutes = str_time - (hours * 60);

    if (hours.length == 1)
        hours = '0' + hours;

    if (minutes.length == 1)
        minutes = '0' + minutes;
    if (minutes == 0)
        minutes = '00';

    if (hours >= 12) {
        if (hours == 12) {
            hours = hours;
            minutes = minutes + " PM";
        } else {
            hours = hours - 12;
            minutes = minutes + " PM";
        }
    } else {
        hours = hours;
        minutes = minutes + " AM";
    }
    if (hours == 0) {
        hours = 12;
        minutes = minutes;
    }

    return hours+':'+minutes;
}

function date_validation(value){
    
    var currVal = value;
    if(currVal == '')
        return false;
    //Declare Regex 
    var rxDatePattern = /^(\d{1,2})(\-)(\d{1,2})(\-)(\d{4})$/;
    var dtArray = currVal.match(rxDatePattern); // is format OK?
    if (dtArray == null)
        return false;
    //Checks for dd-mm-yyyy format.
    dtDay = dtArray[1];
    dtMonth= dtArray[3];
    dtYear = dtArray[5];
    if (dtMonth < 1 || dtMonth > 12)
    {
        return false;
    }
    else if (dtDay < 1 || dtDay> 31)
    {
        return false;
    }
    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)
    {
        return false;  
    }
    else if (dtMonth == 2)
    {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        
        if (dtDay> 29 || (dtDay ==29 && !isleap))
            return false;
    }

    return true;
 }
 
 function travelo_radio(){
     // checkbox
    tjq(".checkbox input[type='checkbox'], .radio input[type='radio']").each(function() {
        if (tjq(this).is(":checked")) {
            tjq(this).closest(".checkbox").addClass("checked");
            tjq(this).closest(".radio").addClass("checked");
        }
        else
        {
            tjq(this).closest(".checkbox").removeClass("checked");
            tjq(this).closest(".radio").removeClass("checked");
        }
    });
    tjq(".checkbox input[type='checkbox']").bind("change", function() {
        if (tjq(this).is(":checked")) {
            tjq(this).closest(".checkbox").addClass("checked");
        } else {
            tjq(this).closest(".checkbox").removeClass("checked");
        }
    });
    //radio
    tjq(".radio input[type='radio']").bind("change", function(event, ui) {
        if (tjq(this).is(":checked")) {
            var name = tjq(this).prop("name");
            if (typeof name != "undefined") {
                tjq(".radio input[name='" + name + "']").closest('.radio').removeClass("checked");
            }

            tjq(this).closest(".radio").addClass("checked");
        }
    });
 }

function bootstrapGrowl(msg, msg_type, msg_config)
{
    var offset_from = 'top';
    var offset_amount = 10;

    if (typeof (msg_type) == 'undefined')
    {
        msg_type = 'info';
    }

    if (typeof (msg_config) == 'undefined')
    {
        msg_config = {};
    }
    else if(typeof (msg_config.offset) != 'undefined' && msg_config.offset != null)
    {
        offset_from = (msg_config.offset.from != undefined && msg_config.offset.from != "") ? msg_config.offset.from : "top";
        offset_amount = (msg_config.offset.amount != undefined && msg_config.offset.amount != "") ? msg_config.offset.amount : 10;
    }

    var config = {
                  ele: 'body', // which element to append to
                  type: msg_type, // (null, 'info', 'danger', 'success')
                  offset: {from: offset_from, amount: offset_amount}, // 'top', or 'bottom'
                  align: 'right', // ('left', 'right', or 'center')
                  width: 350, // (integer, or 'auto')
                  delay: 3000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                  allow_dismiss: true, // If true then will display a cross to close the popup.
                  stackup_spacing: 10 // spacing between consecutively stacked growls.                
                };

    var fConfig = $.extend({}, config, msg_config);

    $.bootstrapGrowl(msg, fConfig);
}

jQuery.fn.center = function(parent)
{
    if (parent) {
        parent = this.parent();
    } else {
        parent = window;
    }
    this.css({
        "position": "absolute",
        "top": ((($(parent).height() - this.outerHeight()) / 2) + $(parent).scrollTop() + "px"),
        "left": ((($(parent).width() - this.outerWidth()) / 2) + $(parent).scrollLeft() + "px")
    });
    return this;
}



var secondsToHhmm = function(totalSeconds)
{
    var hours   = Math.floor(totalSeconds / 3600);
    var minutes = Math.floor((totalSeconds - (hours * 3600)) / 60);
    var seconds = totalSeconds - (hours * 3600) - (minutes * 60);

    // round seconds
    seconds = Math.round(seconds * 100) / 100

    var result = (hours < 10 ? "0" + hours : hours);
    result += ":" + (minutes < 10 ? "0" + minutes : minutes);
    // result += "-" + (seconds  < 10 ? "0" + seconds : seconds);
    return result;
}
/*
 * Validaiton Related function
 */



function printDOM(elem, pagetitle) {
    $('.noprint').hide();
    Popup($(elem).html(),pagetitle);
    $('.noprint').show();
}

function Popup(data, pagetitle)
{
    var mywindow = window.open('', pagetitle, 'height=400,width=600, menubar=0, scrollbars=1');
    mywindow.document.write('<html><head><title>' + pagetitle + '</title>');
    /*optional stylesheet add */
    mywindow.document.write('<link rel="stylesheet" href="../assets/css/bootstrap.css" type="text/css" />');
    mywindow.document.write('<div><h2>' + pagetitle + '</h2></div>');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');
    mywindow.print();
    return true;
}