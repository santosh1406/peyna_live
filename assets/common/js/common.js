//created by sachin 24-08-2018

function showLoaderImg(centerY){
    var url = COMMON_ASSETS+"/images/load.gif";
    $.blockUI({
        message: '<img src="'+ url + '" align="">',
        centerY: centerY != undefined ? centerY : true,
        css: {
//                    top: '10%',
            border: 'none',
            padding: '2px',
            backgroundColor: 'none'
        },
        overlayCSS: {
            backgroundColor: '#000',
            opacity: 0.15,
            cursor: 'wait'
        }
    });
}
function hideLoaderImg(){
    $.unblockUI({
        onUnblock: function () {
            $.removeAttr("style");
        }
    });
}


function redirectToLogout(){
    window.location.href = '/';
}

function bbpsLoaderImg(centerY){
    var url = COMMON_ASSETS+"/images/ajax-loader.gif";
    $.blockUI({
        message: '<img src="'+ url + '" align="">',
        centerY: centerY != undefined ? centerY : true,
        css: {
//                    top: '10%',
            border: 'none',
            padding: '2px',
            backgroundColor: 'none'
        },
        overlayCSS: {
            backgroundColor: '#000',
            opacity: 0.15,
            cursor: 'wait'
        }
    });
}