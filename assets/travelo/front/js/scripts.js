
$(document).ready(function() {
   	
	$(document).off('click', '.show-calender-datepicker').on('click', '.show-calender-datepicker', function(e){
		$(this).datepicker("show");
	});


    $("body").off('click','.filters-container .filters-option li').on('click', '.filters-container .filters-option li', function(e){
        
        e.preventDefault();

        if ($(this).hasClass("active"))
        {
            $(this).removeClass("active");
        } 
        else
        {
            $(this).addClass("active");
        }
    });

});//End of document ready



