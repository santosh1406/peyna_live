bus_services_app.factory('searchServices', function ($http, $q) {
    return {
        
        getSearch: function ($scope,data) 
        {
            var deferred = $q.defer();
            $http.post("http://localhost/internal_app/front/home/bus_services",data,{'Content-Type': 'application/x-www-form-urlencoded'})
            .then(function (response) 
            {
                deferred.resolve(response);
            },function(e){
                 deferred.reject(e);
            });
            
            return deferred.promise;
        }
    };
});