bus_services_app.filter('filterMultiple',['$filter',function ($filter) { 
	return function (items, keyObj) {
		var filterObj = {
                                    data:items,
                                    filteredData:[],
                                    applyFilter : function(obj,key)
                                    {
                                            var fData = [];
                                            if(this.filteredData != undefined && this.filteredData.length == 0)
                                            {
                                                    this.filteredData = this.data;
                                            }

                                            if(obj)
                                            {
                                                    var fObj = {};
                                                    if(angular.isString(obj))
                                                    {
                                                            fObj[key] = obj;
                                                            fData = fData.concat($filter('filter')(this.filteredData,fObj));
                                                    }
                                                    else if(angular.isArray(obj))
                                                    {
                                                            if(obj.length > 0)
                                                            {	
                                                                    for(var i=0;i<obj.length;i++)
                                                                    {
                                                                            if(angular.isString(obj[i]))
                                                                            {
                                                                                    fObj[key] = obj[i];
                                                                                    fData = fData.concat($filter('filter')(this.filteredData,fObj));	
                                                                            }
                                                                    }

                                                            }										
                                                    }

                                                    /*if(fData.length > 0)
                                                    {
                                                            this.filteredData = fData;
                                                    }*/

                                                    if(searchSize == 0)
                                                    {
                                                            this.filteredData = this.data;
                                                    }
                                                    else if(searchSize > 0 && fData.length == 0)
                                                    {
                                                            this.filteredData = [];
                                                    }
                                                    else if(fData.length > 0)
                                                    {
                                                            this.filteredData = fData;
                                                    }

                                            }
                                    }
            };

            var searchSize = 0;
            for(searchkeys in keyObj)
            {
                    searchSize += keyObj[searchkeys].length;
            }

            if(keyObj)
            {
                    if(searchSize > 0)
                    {
                            angular.forEach(keyObj,function(obj,key)
                            {
                                    if(obj.length > 0)
                                    {
                                            filterObj.applyFilter(obj,key);
                                    }
                            });			
                    }
                    else
                    {
                            filterObj.filteredData = items;
                    }
            }

            return filterObj.filteredData;
    }
}]);

bus_services_app.filter('unique', function() {
    return function(input, key) {
        console.log(input);
        console.log(key);
        var unique = {};
        var uniqueList = [];
        for(var i = 0; i < input.length; i++){
            if(typeof unique[input[i][key]] == "undefined"){
                unique[input[i][key]] = "";
                uniqueList.push(input[i]);
            }
        }
        return uniqueList;
    };
});

bus_services_app.filter('cut', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                  //Also remove . and , so its gives a cleaner result.
                  if (value.charAt(lastspace-1) == '.' || value.charAt(lastspace-1) == ',') {
                    lastspace = lastspace - 1;
                  }
                  value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
    });
    
    
    bus_services_app.filter('range', function() 
    {
        return function(input, total) 
        {
          total = parseInt(total);

          for (var i=1; i<=total; i++) 
          {
            input.push(i);
          }

          return input;
        };
    });
    
    bus_services_app.filter('jrange', function() 
    {
        return function(input, total,opname) 
        {
            total = parseInt(total);
			
            if(opname == "MSRTC" || opname == "RSRTC" || opname =="HRTC" || opname == "UPSRTC")
            {
                for(var i=1; i<=total; i++) 
                {
                  input.push(i);
                }
            }
            else
            {
				//alert(opname);
				 for (var i=total; i>=1; i--) 
                {
                  input.push(i);
                }
            }

            return input;
        };
    });
    
    bus_services_app.filter('unique', function() 
    {
        return function(collection, keyname) 
        {
            var output = [], 
            keys       = [];
            angular.forEach(collection, function(item) 
            {
                var key = item[keyname];
                if(keys.indexOf(key) === -1) 
                {
                  keys.push(key);
                  output.push(item);
                }
            });

            return output;
        };
   });
   
   
   bus_services_app.filter('orderByDayNumber', function() 
   {
        return function(items, field, reverse) 
        {
            var filtered = [];
            angular.forEach(items, function(item) 
            {
              filtered.push(item);
            });
            filtered.sort(function (a, b) {
              return (a[field] > b[field] ? 1 : -1);
            });
            if(reverse) filtered.reverse();
            return filtered;
        };
    });
    
    


