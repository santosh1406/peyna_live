    
var bus_services_app =   
    angular.module("bos_bus_services",["ngCookies"])
    .run(function($rootScope) {
        $rootScope.seatLayoutMaster = {};
        $rootScope.selectedSeat = [];
        $rootScope.click = true;
        $rootScope.msrtchit = true;
    })
    .controller("bos_bus_controller",function($scope,$rootScope,$http,searchServices,seatLayouts,seatTempbook,finalCalculation,busServicestopdetails,viewTicket,$window,$cookies,$timeout) {
        $scope.commission_percentage        = false;
        $scope.passenger                    = {};
        $scope.passenderdetails             = [];
        $scope.timeDuration                 = {};
        $scope.count                        = 1; 
        $scope.showcancel                   = false;
        $scope.showmtic                     = false;
        $scope.showalg                      = false;
        $scope.sortColumn                   = "str_dep_tm";
        $scope.reverseSort                  = false;
        $scope.searchTravelArray        = [];
        $scope.searchBusTypeArray       = [];
        $scope.searchDepartureArray     = [];
        $scope.searchdurationArray      = [];
        $scope.showheader                   = false;
        $scope.book                         = false;
        $scope.list                         = [];
        $scope.bookdetail                   = {};
        $scope.retbook                      = {};
        $scope.bookdetail.gender            = [];
        $scope.bookdetail.passenger_name    = [];
        $scope.bookdetail.passenger_age     = [];
        $scope.btotal = '';
        $scope.btotal_c = '';
        $scope.all_operation_names          = [];
        $scope.leavespace = 0;
        $scope.pick = false;
        $scope.base_total_fare        = 0;
        $scope.base_total_servicetax  = 0;
        $scope.base_total_opcharge    = 0;
        $scope.agentcommission        = 0;
        $scope.total_cal_fares = 0;
        $scope.tds                    = 0;
        $scope.btotal                 = 0;
        $scope.btotal_c               = 0;
        $scope.boarding_stops_details_new = {};
        $scope.agent_mobile_no;
        $scope.agent_email_id;
        $scope.bookdetail.payment_type = 'wallet';
        $scope.selectedRow = 0;
        var age = '';
        var name = '';
        $scope.from;
        $scope.to;
        $scope.provider   = 'all';
        $('.trvl_info').hide();
        $('.seatsbox').hide();
        $('#boseat').hide();
        //search filter
        $scope.filtersearch = function(post_data) {
            $scope.idSelected     = null;
            $scope.total_busServices = '';
            $scope.passenderdetails             = [];
            var response = searchServices.getSearch($scope,post_data).then(function (response) {
                //console.log(response);
                var lkeys                = Object.keys(response.data.bus_detail);
                var total_busServices    = lkeys.length;
                $scope.all_bus_services  = response;
                $scope.filter_travels        = response.data.filter_data.operator;
                $scope.filter_bus_types      = response.data.filter_data.bus_type;
                $scope.total_busServices     = total_busServices;
                $scope.showheader            = true;
                $scope.upsrtc_bsc           = response.data.user_data.from_stop_code;
                $scope.upsrtc_asc           = response.data.user_data.to_stop_code;
                if($scope.total_busServices == 0) {
                    $scope.noseat       = 'Seat Layout Not Avalable';
                    $scope.success      = 0;
                }
                $scope.stu_op_name = '';
                if(response.data.time_fare.stu_range)
                {
                    stu_response = response.data.time_fare.stu_range;

                    if( stu_response.op_name_array != null && stu_response.op_name_array.length == 1)
                    {
                        if(stu_response.op_name_array[0] == "msrtc")
                        {
                            $scope.stu_op_name = "MSRTC";
                        }
                        else if(stu_response.op_name_array[0] == "upsrtc")
                        {
                            $scope.stu_op_name = "UPSRTC";
                        }
                        else if(stu_response.op_name_array[0] == "hrtc")
                        {
                            $scope.stu_op_name = "HRTC";
                        }
                        else if(stu_response.op_name_array[0] == "rsrtc")
                        {
                            $scope.stu_op_name = "RSRTC";
                        }
                    }
                    else if(stu_response.op_name_array != null && stu_response.op_name_array.length > 1)
                    {
                       $scope.stu_op_name = stu_response.op_name_array.join("/").toUpperCase(); 
                    }

                    $scope.total_group_buses       = stu_response.stu_total_buses;
                    $scope.stu_bus_range           = (stu_response.min_fare != stu_response.max_fare) ? "Starting From "+stu_response.min_fare : stu_response.min_fare;
                    $scope.bus_range               = $scope.stu_bus_range;
                    $scope.start_at                = stu_response.start_at;
                    $scope.travel_duration_range   = stu_response.min_duration+"-"+stu_response.max_duration;
                    $scope.total_stu_seat_available = stu_response.stu_total_seats;
                    //debugger;
                }
            });
        }
        
        //getseatlayout
        $scope.newseatsget = function(postdata) {
            $scope.isdbledecker = '';
            $scope.seat_layout = '';
            $scope.tot_cols = '';
            $scope.tot_rows = '';
            var response = seatLayouts.getSeats($scope,postdata).then(function (response) {
                //console.log(response);
                if(response.data.flag == '@#success#@') {    
                    var jb                = Object.keys(response.data.seat_layout);
                    $scope.isdbledecker   = ((jb.length) > 1) ? true : false;
                    $scope.seat_layout    = response.data.seat_layout;
                    $rootScope.seatLayoutMaster = response.data.seat_layout;
                    $scope.tot_cols       = response.data.tot_cols;
                    $scope.tot_rows       = response.data.tot_rows;
                    $scope.success        = 1;
                    if(response.data.boarding_points != "" && response.data.boarding_points != undefined) {
                        $scope.boarding_stops_details_new = response.data.boarding_points;
                    }
                    $rootScope.click = true;
                    $rootScope.msrtchit = true;
                }
                else if((response.data.flag == '@#error#@' && response.data.fare == "0" && $scope.provider_type == 'api_provider' && typeof response.data.total_available_seats ===  "string") || (response.data.flag == '@#error#@' && response.data.fare == "0" && $scope.provider_type == 'operator' && ($scope.op_name  == 'UPSRTC' || $scope.op_name  == 'RSRTC' || $scope.op_name  == 'HRTC')  && typeof response.data.total_available_seats ===  "string")) {
                    $scope.noseat       = 'Sold Out';
                    $scope.success      = 0;
                    $rootScope.click = true;
                    $rootScope.msrtchit = true;
                }
                else if((response.data.flag ==  '@#error#@' && response.data.fare == "0" && $scope.provider_type == 'api_provider'  && typeof response.data.total_available_seats ===  "undefined") || (response.data.flag == '@#error#@' && response.data.fare == "0" && $scope.provider_type == 'operator' && ($scope.op_name  == 'UPSRTC' || $scope.op_name  == 'RSRTC' || $scope.op_name  == 'HRTC') && typeof response.data.total_available_seats ===  "undefined")) {
                    $scope.noseat       = 'Seat Layout Not Avalable';
                    $scope.success      = 0;
                    $rootScope.click = true;
                    $rootScope.msrtchit = true;
                }
                else if( $scope.provider_type == 'operator' && $scope.op_name == "MSRTC") {
                    $scope.noseat       = 'Sold Out';
                    $scope.success      = 0;
                    $rootScope.click = true;
                    $rootScope.msrtchit = true;
                }
                //console.log($scope.tot_cols);
                //console.log($scope.tot_rows);
                //console.log(response);
            });
            //$scope.first = true;
            $rootScope.click = false;
            $rootScope.msrtchit = false;
        }


        $scope.get_bus_service_stop_details = function(msrtcdata) {
            $('#seatspresent').hide();
            $scope.boarding_stops_details  = {};
            $scope.alighting_stops_details = {}; 
            $scope.passenderdetails    = [];

            var response = busServicestopdetails.getBustop($scope,msrtcdata).then(function (response) {
                if(response.data.status == 'success') {    
                    $scope.boarding_stops_details  = {};
                    $scope.alighting_stops_details = {};
              
                    /*angular.forEach(response.data.from_service_stops, function(key,tempstp) {
                       $scope.boarding_stops_details[tempstp]  = ({ 'destination': key.from_bus_stop_name+'-'+key.from_time,'boarding_stop_name':key.from_bus_stop_name,'boarding_point_id':key.from_bus_stop_code});
                    });
*/
                    angular.forEach(response.data.from_service_stops, function(key,tempstp) {
                       $scope.boarding_stops_details[tempstp]  = ({ 'name': key.from_bus_stop_name+'-'+key.from_time,'value':key.from_bus_stop_code+'-'+key.from_bus_stop_name+'-'+key.from_time});
                    });

                    angular.forEach(response.data.to_service_stops, function(key,temptostp) {
                       $scope.alighting_stops_details[temptostp]  = ({ 'name': key.to_bus_stop_name+'-'+key.to_time,'value':key.to_bus_stop_code+'-'+key.to_bus_stop_name+'-'+key.to_time});
                    });
                    //$scope.bookdetail.boarding_stps = $scope.boarding_stops_details[0].value;
                    $rootScope.click = true;
                }
                else if(response.data.flag == '@#error#@' ) {
                    console.log('error');
                    $rootScope.click = true;
                }
            });
            $rootScope.click = false;
        }

        $scope.tempbooking   = function(bus_post_data) {
            
            $('#mydiv2').show();
            var div= document.createElement("div");
            div.className += "overlay";
            document.body.appendChild(div);
            var response = seatTempbook.getTempseat($scope,bus_post_data).then(function (response) {
                if(response.data.status == 'success') 
                {
                    if(response.data.ticket_layout != "" && response.data.ticket_layout != undefined)
                    {
                        $('#mydiv2').hide();
                        $("#modal-body").html(response.data.ticket_layout);
                        $("#ticket_view").modal("show");
                    }
                    else if((response.data.type) == "cca")
                    {
                        $('#mydiv2').hide();
                        $("#encRequest").val(response.data.encrypted_data);
                        $( ".ccpost" ).submit();
                    }
                }
                else {
                    $('#mydiv2').hide();
                    if(response.data.msg != '' || response.data.msg != undefined)
                    {
                        showNotification("error",response.data.msg);
                    }
                    else {
                        showNotification("error","Some problem occured while booking ticket,Kindly Try Again");
                    }
                    $timeout(function () {
                        $window.location.href = base_url+'front/home/agent_search';
                    }, 2000);

                }
                //$( "div" ).removeClass( "overlay" );
                //$window.location.href = base_url+'front/home/agent_search';
            });
        }

        $scope.ShowHideBoarding = function(event,val,hideshow) {
            //console.log(val);
            //console.log(event.offsetX);
            //console.log(event.offsetY); 
            var tooltipSpan         = document.getElementById('tooltip-span');
            $scope.showdata         = hideshow;
            //console.log($scope.provider_id);
            $scope.typename = '';
            if(val.provider_id == '3')
            {
                $scope.typename = '(I)';
                
            }
            else if(val.provider_id == '2')
            {
                $scope.typename = '(E)';
            }
         
            $scope.busoperatiorname = val.op_name;
            $scope.timings          = val.str_dep_tm_his+'-'+val.arrival_time;
            $scope.busestype        = val.op_bus_type_name;
            $scope.buseavailab      = val.tot_seat_available;
            $scope.busfares         = 'Rs.' +val.seat_fare + '(SIT)';
            //tooltipSpan.style.top = (0 + event.offsetY) + 'px';
            //tooltipSpan.style.left = (event.offsetX + 20) + 'px';
        };

        $scope.initializeFilter = function(filterData) {
            $scope.triptype     =   filterData.triptype;
            $scope.from     =   filterData.from;
            $scope.to       =   filterData.to;
            $scope.date_of_jour =   filterData.date_of_jour;
            $scope.date_of_arr  =   filterData.date_of_arr;
            $scope.provider     =   filterData.provider;
            $scope.searchbustype    =   filterData.searchbustype;
            $scope.initServices();
        }

        $scope.initServices = function() {
            var post_data = {   
                "triptype":$scope.triptype, 
                "from":$scope.from, 
                "to":$scope.to, 
                "date_of_jour":$scope.date_of_jour, 
                "date_of_arr":$scope.date_of_arr, 
                "provider":$scope.provider,
                "searchbustype":$scope.searchbustype
            };
            $scope.filtersearch(post_data);                    
        }

        $scope.submit = function() { 
            $scope.searchTravelArray        = [];
            $scope.searchBusTypeArray       = [];
            $scope.searchDepartureArray     = [];
            $scope.searchdurationArray      = [];

            $('#seatspresent').hide();
            if($scope.from == '' || $scope.from ==  undefined) {
                showNotification("error","Please Enter From Stop");
                return false;
            }
            if($scope.to == '' || $scope.to ==  undefined) {
                showNotification("error","Please Enter to Stop");
                return false;
            }

            if($scope.date_of_jour == '' || $scope.date_of_jour ==  undefined) {
                showNotification("error","Please enter date of journey");
                return false;
            }

            var newpost_data =  {   
                "from":$scope.from, 
                "to":$scope.to, 
                "date_of_jour":$scope.date_of_jour, 
                "triptype":"single", 
                "provider": $scope.provider,
                "searchbustype":"all"
            };
            $scope.filtersearch(newpost_data);             
        }


        $scope.bookticket   = function() {
            $scope.tempbirth     = [];
            $scope.seatarr       = [];
            $scope.totalseat     = [];
            $scope.final         = {};
            var tmplower         = [];
            var tmpupper         = [];
            var tmpchairlower    = [];
            var tmpchairupper    = [];
            $scope.pt            = {};
            $scope.birthfinal    = {};
            $scope.d             = [];
            var re               = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var numbers          = /^[0-9]+$/; 
            var age = '';
            var name = '';
            var result = true;

            if($scope.provider_type == 'api_provider')
            {    
                if($scope.bookdetail.agent_markup == undefined )
                {
                    $scope.bookdetail.agent_markup = 0;
                }
                if(($scope.bookdetail.agent_markup < 0 || $scope.bookdetail.agent_markup > 100)) 
                {
                    showNotification("error","Agent mark up value must be less than or equal to 100");
                    result = false;
                    return false;
                }
            }
            
            if($scope.bookdetail.boarding_stps == '' || $scope.bookdetail.boarding_stps === undefined) {
                showNotification("error","Please Select Pickup Point");
                result = false;
                return false;
            }
            if($scope.bookdetail.alighting_stps == '' || $scope.bookdetail.alighting_stps === undefined) {
                showNotification("error","Please Select Dropoff Point");
                result = false;
                return false;
            }
            if($scope.bookdetail.passenger_email == '' || $scope.bookdetail.passenger_email === undefined) {
                $scope.passenger_email_touse  = email;
                //showNotification("error","Please Enter Passenger Email");
                //result = false;
                //return false;
            }
            else if(re.test($scope.bookdetail.passenger_email) === false) {
                showNotification("error","Please Enter Valid Email");
                result = false;
                return false;
            }
            if($scope.bookdetail.passenger_num == '' || $scope.bookdetail.passenger_num === undefined ) {
                showNotification("error","Please Enter Passenger Phone Number");
                result = false;
                return false;
            }
            else if (! /^\d{10}$/.test($scope.bookdetail.passenger_num)) {
                showNotification("error","Invalid Mobile number must be ten digits");
                result = false;
                return false;
            } 

            var age  = $scope.bookdetail.passenger_age.length;
            var name = $scope.bookdetail.passenger_name.length;

            if($scope.bookdetail.passenger_name.length == '0' || name > 0 || age > 0) {
                angular.forEach($scope.passenderdetails, function(key,val) {
                    var valset = key.birth;
                    var rowName = val + 1;
                    if($scope.bookdetail.gender[valset] == '' || $scope.bookdetail.gender[valset] === undefined) {
                        showNotification("error","Please Select Gender for Passenger "+ rowName);
                        result = false;
                        return false;
                    }   
                    if($scope.bookdetail.passenger_name[valset] == '' || $scope.bookdetail.passenger_name[valset] === undefined ) {
                        showNotification("error","Please Enter Name for Passenger "+ rowName);
                        result = false;
                        return false;
                    }
                    if($scope.bookdetail.passenger_age[valset] == '' || $scope.bookdetail.passenger_age[valset] === undefined) {
                        showNotification("error","Please Enter Age for Passenger "+ rowName);
                        result = false;
                        return false;
                    }
                    if(($scope.bookdetail.passenger_age[valset] < 5 || $scope.bookdetail.passenger_age[valset] > 999)) {
                        showNotification("error","Please Enter Valid Age for Passenger  "+ rowName);
                        result = false;
                        return false;
                    }
                    if((isNaN($scope.bookdetail.passenger_age[valset]))) {
                        showNotification("error","Please input numeric characters only for Passenger "+ rowName);
                        result = false;
                        return false;
                    }
                });

                if(result == true) {
                    if($scope.is_alighting == "Y") {
                        var values_a               = $scope.bookdetail.alighting_stps.split("-");
                        $scope.alighting_stp_sid   = values_a[0];
                        $scope.alighting_stps_name = values_a[1];
                        $scope.alighting_stps_time = values_a[2];
                    }
                    else if($scope.is_alighting == "N") { 
                        var values_a               = $scope.bookdetail.alighting_stps.split("-");
                        $scope.alighting_stp_sid   = values_a[0];
                        $scope.alighting_stps_name = values_a[1];
                    }
                    else if($scope.provider_type == 'operator' && $scope.op_name == "HRTC"){
                        $scope.alighting_stps_name   = $scope.bookdetail.alighting_stps;
                    }
                    else {
                        var values_a               = $scope.bookdetail.alighting_stps.split("-");
                        $scope.alighting_stp_sid   = values_a[0];
                        $scope.alighting_stps_name = values_a[1];
                    }
                    if($scope.is_boarding  == "Y") { 
                        var values_b = $scope.bookdetail.boarding_stps.split("-");
                        $scope.boarding_stp_sid   = values_b[0];
                        $scope.boarding_stps_name = values_b[1];
                        $scope.boarding_stps_time = values_b[2];
                    }
                    else if($scope.is_boarding == "N") {
                        var values_b              = $scope.bookdetail.boarding_stps.split("-");
                        $scope.boarding_stp_sid   = values_b[0];
                        $scope.boarding_stps_name = values_b[1];
                    }
                    else if($scope.provider_type == 'operator' && $scope.op_name == "HRTC" ){
                        $scope.boarding_stps_name = $scope.bookdetail.boarding_stps;
                    }
                    else {
                        var values_b = $scope.bookdetail.boarding_stps.split("-");
                        $scope.boarding_stp_sid   = values_b[0];
                        $scope.boarding_stps_name = values_b[1];
                    }
                    angular.forEach($scope.bookdetail.passenger_seat, function(key,valset) {  
                        var tempseat       = key.split('-');
                        var tIndex = tempseat[1]+'_'+ tempseat[0];

                        $scope.pt = {
                            name:$scope.bookdetail.passenger_name[tIndex],
                            seat_no:tempseat[0],
                            age:$scope.bookdetail.passenger_age[tIndex],
                            gender:$scope.bookdetail.gender[tIndex]
                        };

                        if(tempseat[1] == '1') {
                            tmplower.push($scope.pt);
                            tmpchairlower.push(tempseat[0]);
                        }
                        if(tempseat[1] == '2') {
                            tmpupper.push($scope.pt);
                            tmpchairupper.push(tempseat[0]);
                        }

                        $scope.seatarr.push(tempseat[0]);
                    });
                    
                    $scope.birthfinal[1] = tmplower;
                    $scope.birthfinal[2] = tmpupper;
                    $scope.d[1] = tmpchairlower;
                    $scope.d[2] = tmpchairupper;

                    $scope.final = { 
                        "seat":$scope.birthfinal,
                        "email_id":$scope.bookdetail.passenger_email,
                        'mobile_no':$scope.bookdetail.passenger_num,
                        'agent_email_id':email,
                        'agent_mobile_no':mobile,
                        'agent_markup':$scope.bookdetail.agent_markup,
                        'type':$scope.bookdetail.payment_type,
                    };

                    if($scope.provider_type == 'operator' && $scope.op_name == "MSRTC"  )  {
                        $scope.boarding_stop_cd    = $scope.msrtc_boarding;
                        $scope.destination_stop_cd = $scope.msrtc_alighting;
                        $scope.bus_type_id         = $scope.bus_type_id;
                        $scope.passenderdetails    = [];
                        $scope.temp_book_from_stop = $scope.boarding_stps_name;
                        $scope.temp_book_to_stop   = $scope.alighting_stps_name;
                    }
                    else if ($scope.provider_type == 'operator' && $scope.op_name == "UPSRTC" ) {
                        $scope.boarding_stop_cd    = $scope.boarding_stp_sid;
                        $scope.destination_stop_cd = $scope.alighting_stp_sid;
                        $scope.bus_type_id         = $scope.bus_type_id;
                        $scope.temp_book_from_stop = $scope.fromstop;
                        $scope.temp_book_to_stop   = $scope.tostop;
                        $scope.boarding_stps_name = $scope.from;
                        $scope.alighting_stps_name   = $scope.to;
                    }
                    else if ($scope.provider_type == 'operator' && $scope.op_name == "RSRTC" ) {
                        $scope.boarding_stop_cd    = $scope.boarding_stp_sid;
                        $scope.destination_stop_cd = $scope.alighting_stp_sid;
                        $scope.bus_type_id         = $scope.bus_type_id;
                        $scope.temp_book_from_stop = $scope.from;
                        $scope.temp_book_to_stop   = $scope.to;
                        $scope.boarding_stps_name  = $scope.boarding_stps_name;
                        $scope.alighting_stps_name = $scope.alighting_stps_name;
                        
                        if($scope.boarding_stps_name == undefined && $scope.alighting_stps_name == undefined)
                        {
                            $scope.boarding_stps_name  = $scope.fromstop;
                            $scope.alighting_stps_name = $scope.tostop;
                        }
                        
                    }
                    else if ($scope.provider_type == 'operator' && $scope.op_name == "HRTC") {
                        $scope.bus_type_id         = $scope.bus_type_id;
                        $scope.temp_book_from_stop = $scope.fromstop;
                        $scope.temp_book_to_stop   = $scope.tostop;
                    }
                    else {
                        $scope.boarding_stop_cd    = $scope.boarding_stp_sid;
                        $scope.destination_stop_cd = $scope.alighting_stp_sid;
                        $scope.bus_type_id         = "Others";
                        $scope.temp_book_from_stop = $scope.from;
                        $scope.temp_book_to_stop   = $scope.to;
                    }

                    var bus_post_data =  {   
                        "from":$scope.temp_book_from_stop, 
                        "to":$scope.temp_book_to_stop, 
                        "date":$scope.date_of_jour, 
                        "date_of_jour":$scope.date_of_jour, 
                        "boarding_stop_name":$scope.boarding_stps_name,
                        "destination_stop_name":$scope.alighting_stps_name,
                        "boarding_stop_cd":$scope.boarding_stop_cd,
                        "destination_stop_cd":$scope.destination_stop_cd,
                        "trip_no":$scope.trip_no,
                        "seats":$scope.d,
                        "op_name":$scope.op_name,
                        "op_id":$scope.op_id,
                        "bus_type_id":$scope.bus_type_id,
                        "bsd":$scope.boarding_stp_sid, 
                        //"dsd":"",
                        "dsd":$scope.alighting_stp_sid,
                        "provider_id":$scope.provider_id,
                        "provider_type":$scope.provider_type,
                        "sentby":"agent_ag",
                        "do_confirm_data":$scope.final,
                    };
                    if($scope.provider_id == '3') {
                        bus_post_data["its_reference_number"] = $scope.op_trip_no;
                    }
                    //console.log(bus_post_data);
                    $scope.tempbooking(bus_post_data);
                }
            }
            else  {

            }  
        }

        $scope.sortData = function (column) {
            $scope.reverseSort = ($scope.sortColumn == column) ? !$scope.reverseSort : false;
            $scope.sortColumn = column;
        }

        $scope.switch = function (from,to) {
            $scope.newfrom = to;
            $scope.newto   = from;
        }

        $scope.getSortClass = function (column) {
            if($scope.sortColumn == column) {
                return $scope.reverseSort ? 'arrow-down' : 'arrow-up';
            }
            return '';
        }

        $scope.shifUnshiftTravel = function (value) {
            var pos = $scope.searchTravelArray.indexOf(value);
            if (pos == -1) {
                $scope.searchTravelArray.push(value);
            }
            else {
                $scope.searchTravelArray.splice(pos, 1);
            }
        }

        $scope.shifUnshiftBusType = function (value) {
            var pos = $scope.searchBusTypeArray.indexOf(value);
            if (pos == -1) {
                $scope.searchBusTypeArray.push(value);
            }
            else {
                $scope.searchBusTypeArray.splice(pos, 1);
            }
        }

        $scope.shifUnshiftDeparture = function (value) {
            var pos = $scope.searchDepartureArray.indexOf(value);
            if (pos == -1) {
                $scope.searchDepartureArray.push(value);
            }
            else {
                $scope.searchDepartureArray.splice(pos, 1);
            }
        }

        $scope.shifUnshiftDuration = function (value) {
            var pos = $scope.searchdurationArray.indexOf(value);
            if (pos == -1) {
                $scope.searchdurationArray.push(value);
            }
            else {
                $scope.searchdurationArray.splice(pos, 1);
            }
        }

        $scope.swap = function() {
            if($scope.from == '' || $scope.from == "null") {
                showNotification("error","Please Enter From Stop");
                return false;
            }
            if($scope.to == '' || $scope.to == "null") {
                showNotification("error","Please Enter to Stop");
                return false;
            }

            $scope.newto   = $scope.from;
            $scope.newfrom = $scope.to;
            $scope.from    = $scope.newfrom;
            $scope.to      = $scope.newto;
        }

        $scope.Notifi = function(field, validation) {
            showNotification(field,validation);
        };


        //$scope.idSelected     = null;
        $scope.setSelected    = function(idSelectedVote,val,event,index) { 
            $scope.first = true;
            $scope.selectedRow = index;
            //console.log($scope.selectedRow);
            if ($rootScope.click == true && $scope.first == true)
            {
                $scope.first = false;
                $rootScope.click == false;
                $scope.idSelected = idSelectedVote;
                $scope.base_total_fare        = 0;
                $scope.base_total_servicetax  = 0;
                $scope.base_total_opcharge    = 0;
                $scope.agentcommission        = 0;
                $scope.tds                    = 0;
                $scope.btotal                 = 0;
                $scope.btotal_c               = 0;
                $rootScope.selectedSeat = [];
                $scope.passenger = {};
                $scope.passenderdetails = [];
                $scope.bookdetail.passenger_name = []; 
                $scope.bookdetail.passenger_seat = {};
                $scope.bookdetail.passenger_seatype = [];
                $scope.bookdetail.passenger_age = [];
                $scope.bookdetail.passenger_fare = [];
                $scope.bookdetail.passenger_uniq_seat = [];
                $scope.count = 1;
                var d = new Date(val.dept_tm);
                $scope.timedate = val.str_dep_tm_his+' '+(d.toDateString());
                $scope.fromstop = val.from_stop;
                $scope.tostop    = val.to_stop;
                $scope.provider_type = val.provider_type;
                $scope.op_name = val.op_name;
                $scope.op_trip_no = val.op_trip_no;
                $scope.op_comm_percentage = val.commission_percentage;
                
                if( event == undefined)
                {
                   $('#tablecontent td:first-child').find('span').removeClass('hide');
                }
                if( event != undefined)
                {
                    $(angular.element(event.currentTarget)).find('span').removeClass('hide');
                }

                //console.log(val.provider_type);
                //console.log(val.op_name);
                if((val.provider_type === 'api_provider')) {
                    $scope.seatlayout(val);
                }
                else if(val.provider_type === "operator" && val.op_name === "HRTC")
                {
                    $scope.seatlayout(val);
                }
                else if(val.provider_type === "operator" && val.op_name === "RSRTC")
                {
                    $scope.seatlayout(val);
                    $scope.cancellation_policy = [];
                    angular.forEach(val.cancellation_policy, function(cdetail, canval) 
                    {
                        var charges_percent       = cdetail.type == 'percent' ? " %" : "";
                        var charges_rupees        = cdetail.type == 'fixed' ? "Rs. " : "";
                        var from = cdetail.from != "" ? (cdetail.from/3600).toFixed(2) : "";
                        var till = cdetail.till != "" ? Math.round(cdetail.till/3600) : "";
                        if(from == '0.50')
                        {
                            $scope.cancellation_sentence = "Cancellation upto 30 mins ";
                        }
                        else
                        {
                            $scope.cancellation_sentence = "Cancellation upto "+from+" hrs  ";  
                        }

                        $scope.cancellation_policya = $scope.cancellation_sentence+"  "+ charges_rupees+cdetail.cancel_rate+charges_percent;
                        $scope.cancellation_policy.push($scope.cancellation_policya);
                        $scope.cancellation_charges_conditions = cdetail.note;


                    });
                }
                else if(val.provider_type === "operator" && val.op_name === "MSRTC") 
                {
                    console.log('111');
                    console.log($scope.selectedRow);
                    var newpost_data_mr =  {   
                        "from":$scope.from, 
                        "to":$scope.to, 
                        "date_of_jour":$scope.date_of_jour, 
                        "triptype":$scope.triptype, 
                        "provider":$scope.provider,
                        "searchbustype":$scope.searchbustype,
                        "date":val.dept_tm, 
                        "op_trip_no":val.op_trip_no,
                    };
                    $scope.date              =  val.dept_tm;  
                    //$scope.trip_no         =  val.op_trip_no;
                    $scope.trip_no           =  val.trip_no;
                    $scope.bus_type_id       =  val.bus_type_id;
                    $scope.op_id             =  val.op_id;
                    $scope.op_name           =  val.op_name;
                    $scope.provider_id       =  val.provider_id;
                    $scope.provider_type     =  val.provider_type;
                    $scope.op_bus_type_cd     =  val.op_bus_type_cd;
                    $scope.toolshow          =  '('+val.op_bus_type_name+')';
                    $scope.cancellation_policy = [];
                    if(val.mticket_allowed) {
                        $scope.mticket_allowed = "Mticket is allowed to board the bus.";
                    }
                    else {
                        $scope.mticket_allowed =  "Mticket is NOT allowed to board the bus.";
                    }

                    angular.forEach(val.cancellation_policy, function(cdetail, canval) {
                        var charges_percent       = cdetail.type == 'percent' ? " %" : "";
                        var charges_rupees        = cdetail.type == 'fixed' ? "Rs. " : "";
                        var from                  = cdetail.from != "" ? Math.round(cdetail.from/3600) : "";
                        var till = cdetail.till  != "" ? Math.round(cdetail.till/3600) : "";

                        $scope.cancellation_sentence = "Cancellation upto "+from+" hrs is ";  

                        $scope.cancellation_policya = $scope.cancellation_sentence+"  "+ charges_rupees+cdetail.cancel_rate+charges_percent;
                        $scope.cancellation_policy.push($scope.cancellation_policya);
                    });

                    $scope.get_bus_service_stop_details(newpost_data_mr);            
                }
                else if(val.provider_type == "operator" && val.op_name == "UPSRTC") {
                    $scope.seatlayout(val);
                    $scope.cancellation_policy = [];
                    angular.forEach(val.cancellation_policy, function(cdetail, canval) {
                        //console.log(cdetail);
                        //console.log(canval);
                        var charges_percent       = cdetail.type == 'percent' ? " %" : "";
                        var charges_rupees        = cdetail.type == 'fixed' ? "Rs. " : "";
                        var from                  = cdetail.from != "" ? Math.round(cdetail.from/3600) : "";
                        var till = cdetail.till != "" ? Math.round(cdetail.till/3600) : "";

                        $scope.cancellation_sentence = "Cancellation upto "+from+" hrs is ";  

                        $scope.cancellation_policya = $scope.cancellation_sentence+"  "+ charges_rupees+cdetail.cancel_rate+charges_percent;
                        $scope.cancellation_policy.push($scope.cancellation_policya);
                    });
                }
            }
        }
        $scope.$watch('selectedRow', function() {
            //console.log('Do Some processing');
        });
        
        $scope.seatlayout = function(val) {  
            if($(".seatCharts-seat").hasClass( "selectedseat" ) == true) {
                $(".seatCharts-seat").removeClass( "selectedseat" );
                $scope.passenderdetails.length = 0;
                $scope.passenger = {};
            }

            angular.forEach(val.barr, function(key, vals) {
                $scope.is_alighting =  key.is_alighting;
                $scope.is_boarding  =   key.is_boarding;
            });
            $scope.boarding_stops_details_new = val.barr;
            if(val.provider_type == "operator" && val.op_name == "HRTC")
            {
                var post_data = {   
                    "from":$scope.from, 
                    "to":$scope.to, 
                    "date":$scope.date_of_jour, 
                    "provider_id":val.provider_id,
                    "inventory_type":val.inventory_type,
                    "op_trip_no":val.op_trip_no,
                    "route_schedule_id":val.route_schedule_id,
                    "provider_type":val.provider_type,
                    "trip_no":val.trip_no,
                    "service_id":val.service_id,
                    "op_id":val.op_id,
                    "op_name":val.op_name,
                    "bus_type_id":val.bus_type_id,
                    "op_bus_type_cd":val.op_bus_type_cd,
                };
            }
            else if(val.provider_type == "operator" && val.op_name == "RSRTC")
            {
                var post_data = {   
                    "from":$scope.from, 
                    "to":$scope.to, 
                    "date":$scope.date_of_jour, 
                    "provider_id":val.provider_id,
                    "inventory_type":val.inventory_type,
                    "op_trip_no":val.op_trip_no,
                    "provider_type":val.provider_type,
                    "trip_no":val.trip_no,
                    "op_id":val.op_id,
                    "op_name":val.op_name,
                    "bus_type_id":val.bus_type_id,
                    "op_bus_type_cd":val.op_bus_type_cd,
                };
            }
            else
            {
                var post_data = {   
                    "from":val.from_stop, 
                    "to":val.to_stop, 
                    "date":val.dept_tm, 
                    "provider_id":val.provider_id,
                    "inventory_type":val.inventory_type,
                    "op_trip_no":val.op_trip_no,
                    "route_schedule_id":val.route_schedule_id,
                    "provider_type":val.provider_type,
                    "trip_no":val.trip_no,
                    "service_id":val.service_id,
                };
            }
            $scope.trip_no           =  val.trip_no;
            $scope.bus_type_id       =  val.bus_type_id;
            $scope.op_id             =  val.op_id;
            $scope.op_name           =  val.op_name;
            $scope.provider_id       =  val.provider_id;
            $scope.provider_type     =  val.provider_type;
            $scope.toolshow          =  '('+val.op_bus_type_name+')';

            if(val.mticket_allowed) {
                $scope.mticket_allowed = "Mticket is allowed to board the bus.";
            }
            else {
                $scope.mticket_allowed =  "Mticket is NOT allowed to board the bus.";
            }

            $scope.newseatsget(post_data);
            $timeout(function(){
                $scope.boarding_stops(val.cancellation_policy,val.from_stop,val.to_stop,$scope.boarding_stops_details_new); 
            },2000);
 
        }

        //getboardingstops
        $scope.boarding_stops  = function(cancellation_policy,from_stop,to_stop,stops) {
            $scope.boarding_stops_details  = {};
            $scope.alighting_stops_details = {};
            $scope.cancelpolicy_details = {};
            if( (stops.length > 0 &&  $scope.op_name != "RSRTC") || (Object.keys(stops).length > 0 &&  $scope.op_name != "RSRTC") ) { 
                angular.forEach(stops, function(value, key) {
                    if(value.is_boarding  == "Y") {
                        $scope.is_boarding = "Y";
                        if($scope.op_name == "UPSRTC") {
                            $scope.boarding_stops_details[key]  = ({ 'name': value.bus_stop_name+'-'+value.stop_code,'value':value.stop_code+'-'+value.bus_stop_name});
                        }
                        else {
                            $scope.boarding_stops_details[key]  = ({ 'name': value.arrival_duration+'-'+value.boarding_stop_name,'value':value.boarding_point_id+'-'+value.boarding_stop_name});
                        }
                        
                    }

                    if(value.is_alighting  == "Y") {
                        $scope.is_alighting = "Y";
                        if($scope.op_name == "UPSRTC") {
                            $scope.alighting_stops_details[key]  = ({ 'name': value.bus_stop_name+'-'+value.stop_code,'value':value.stop_code+'-'+value.bus_stop_name});
                        }
                        else {
                            $scope.alighting_stops_details[key]  = ({ 'name': value.arrival_duration+'-'+value.boarding_stop_name,'value':value.boarding_point_id+'-'+value.boarding_stop_name});
                        }
                        
                    }
                });  

                if( Object.keys($scope.boarding_stops_details).length == 0) {
                    if($scope.op_name == "UPSRTC") {
                        $scope.boarding_stops_details[0]  = ({ 'name':from_stop+'-'+$scope.upsrtc_bsc,'value':$scope.upsrtc_bsc});
                    }
                    else {
                        $scope.boarding_stops_details[0]  = ({ 'name':from_stop,'value':'0-'+from_stop});
                    }
                                             
                }

                if( Object.keys($scope.alighting_stops_details).length == 0) {
                    if($scope.op_name == "UPSRTC") {
                        $scope.alighting_stops_details[0]  = ({ 'name':to_stop+'-'+$scope.upsrtc_asc,'value':$scope.upsrtc_asc});
                    }
                    else {
                       $scope.alighting_stops_details[0]  = ({ 'name':to_stop,'value':'0-'+to_stop}); 
                    }                             
                } 
            }
            else if($scope.provider_type == "operator" && $scope.op_name == "HRTC") 
            {
                $scope.boarding_stops_details[0]  = ({ 'name':$scope.from,'value':$scope.from});
                $scope.alighting_stops_details[0]  = ({ 'name':$scope.to,'value':$scope.to});
            }
            else if($scope.provider_type == "operator" && $scope.op_name == "RSRTC") 
            {
                $scope.boarding_stops_details[0]  = ({ 'name':from_stop,'value':$scope.upsrtc_bsc});
                $scope.alighting_stops_details[0]  = ({ 'name':to_stop,'value':$scope.upsrtc_asc});
            }
            else
            {
                $scope.boarding_stops_details[0]  = ({ 'name':from_stop,'value':$scope.upsrtc_bsc});
                $scope.alighting_stops_details[0]  = ({ 'name':to_stop,'value':$scope.upsrtc_asc});
            }

            angular.forEach(cancellation_policy, function(cpoldetail,cpol) {
                var cancellation_sentence = "Cancellation within "+cpoldetail.cutoffTime+" hrs";
                $scope.cancelpolicy_details[cpol] = ({ 'cutoffTime':cancellation_sentence,'refundInPercentage':cpoldetail.refundInPercentage+ "%"  });
            });

            //console.log($scope.boarding_stops_details);
            //console.log($scope.alighting_stops_details);
        };

        //seatlayoutvariousseatclass
        $scope.whatClassIsIt= function(current_seat) {
            if( current_seat.available == 'Y') {
                if(current_seat.current_seat_type != "") {
                    return current_seat.current_seat_type+"_"+"available";
                }      
                else {
                    return "available";
                }
            }
            else if(current_seat.available == 'N') {
                if(current_seat.current_seat_type != "") 
                {
                    if(current_seat.booked_by != "" && current_seat.booked_by == "F")
                    {
                        return current_seat.current_seat_type+"_"+"seatbookedf";
                    }
                    else
                    {
                        return current_seat.current_seat_type+"_"+"seatbooked";
                    }
                }
                else if(current_seat.current_seat_type == "" && current_seat.booked_by != "" && current_seat.booked_by == "F")
                {
                    return "seatbookedf";
                }
                else {
                    return "seatbooked";
                }
            }
        }

        //seatlayoutselectseat
        $scope.seatselect = function(event,uniq,details,berth,seat_index) {
//            if($scope.op_name != "RSRTC")
//            {  
//                $scope.base_total_fare        = 0;
//                $scope.base_total_servicetax  = 0;
//                $scope.base_total_opcharge    = 0;
//                $scope.agentcommission        = 0;
//                $scope.tds                    = 0;
//                $scope.btotal                 = 0;
//                $scope.btotal_c               = 0;
//            }
            //$scope.passenderdetails             = [];
            var s = uniq.split("_");
            var berno = s[0];
            var ladies_seat = false;
            var sleeper = false;
            if ( angular.element(event.target).hasClass("seatbooked") == false && 
                (angular.element(event.target).hasClass("ladies_seatbooked") !=true &&  angular.element(event.target).hasClass("vladies_sleeper_seatbooked") !=true) 
                && angular.element(event.target).hasClass("sleeper_seatbooked") != true 
                && angular.element(event.target).hasClass("ladies_sleeper_seatbooked") != true 
                &&  angular.element(event.target).hasClass("vsleeper_seatbooked") != true 
                && angular.element(event.target).hasClass("seatbookedf") != true  
                && angular.element(event.target).hasClass("sleeper_seatbookedf") != true
                && ( angular.element(event.target).hasClass("ladies_seatbookedf") != true &&  angular.element(event.target).hasClass("vladies_sleeper_seatbookedf") !=true)
                && angular.element(event.target).hasClass("vsleeper_seatbookedf") != true
                ) {
                if (details.current_seat_type == "" || details.current_seat_type == "undefined") {
                    var seat_curr = 'S';
                }
                else {
                    if (details.current_seat_type == "ladies") {
                        ladies_seat = true;
                        var seat_curr = 'S';
                    }
                    else if (details.current_seat_type == "vladies_sleeper_available") {
                        ladies_seat = true;
                        var seat_curr = 'SL';
                    }
                    else if (details.current_seat_type == "vsleeper_available") {
                        var seat_curr = 'SL';
                        sleeper = true;
                    }
                    else if (details.current_seat_type == "ladies_sleeper") {
                        ladies_seat = true;
                        var seat_curr = 'SL';
                    }
                    else if (details.current_seat_type == "sleeper") {
                        var seat_curr = 'SL';
                        sleeper = true;
                    }
                }
 
                if (angular.element(event.target).hasClass("selectedseat") == true) {
                    angular.element(event.target).removeClass('selectedseat');
                    if(ladies_seat == true)
                    {
                        angular.element(event.target).addClass('ladies_available');
                    }
                    else if(sleeper == true)
                    {
                        angular.element(event.target).addClass('sleeper_available');
                    }
                    else
                    {
                       angular.element(event.target).addClass('available');
                    }
                    
                    
                    

                    for (var i = 0; i < $scope.passenderdetails.length; i += 1) {
                        if ($scope.passenderdetails[i].birth === uniq) {
                            $scope.passenderdetails.splice(i, 1);
                            delete $rootScope.selectedSeat[uniq];
                            delete $scope.bookdetail['passenger_name'][uniq];
                            delete $scope.bookdetail['passenger_age'][uniq];
                            delete $scope.bookdetail['gender'][uniq];
                            delete $scope.bookdetail['passenger_seat'][uniq];
                            delete $scope.bookdetail['passenger_fare'][uniq];
                            delete $scope.bookdetail['passenger_seatype'][uniq];
                            $scope.count--;
                            $scope.ticketratescheck(uniq);
                        }
                    }

                    if ($scope.count == 1) {
                        $scope.book = false;
                        if ($("#bookbtn").hasClass("btn-book") == true) {
                            $("#bookbtn").removeClass("btn-book");
                        }
                    }
                }
                else {
                    if (($scope.count <= 6) && ($scope.op_name != "RSRTC")) 
                    {
                        if(sleeper == true)
                        {
                            angular.element(event.target).addClass('selectedseat');
                            $(".selectedseat").width(25).height(40);
                        }
                        else
                        {
                            angular.element(event.target).addClass('selectedseat');
                        }
                        angular.element(event.target).removeClass('available');
                        angular.element(event.target).removeClass('ladies_available');
                        angular.element(event.target).removeClass('sleeper_available');
                        angular.element(event.target).removeClass('ladies_sleeper_available');
                         angular.element(event.target).removeClass('vsleeper_available');
                        
                        $scope.book = true;
                        if ($("#bookbtn").hasClass("btn-book") == false) {
                            $("#bookbtn").addClass("btn-book");
                        }

                        $scope.passenger = {seatno_curr: details.seat_no,
                            seat_basic_fare_curr: details.seat_basic_fare,
                            seat_curr: seat_curr,
                            birth: uniq,
                            ber: berno,
                            basefare: details.actual_seat_basic_fare,
                            servicetax: details.seat_service_tax_amt,
                            OpScharge: details.op_service_charge_amt,
                            agentcomm: details.agent_commission,
                            agentds: details.agent_comm_tds,
                            change_seat_fare_child: details.child_seat_fare,
                            is_ladies : ladies_seat
                        };
                        //$rootScope.selectedSeat.push($scope.passender);
                        var tempIndex = berth+'_'+details.seat_no;
                        $rootScope.selectedSeat[tempIndex] = $scope.passenger;
                        $scope.bookdetail['passenger_name'][tempIndex] = '';
                        $scope.bookdetail['passenger_age'][tempIndex] = '';
                        $scope.bookdetail['gender'][tempIndex] = '';
                        $scope.passenderdetails.push($scope.passenger);
                        //console.log($scope.passenderdetails);
                        $scope.ticketratescheck();
                        $scope.count++;
                    }
                    else if($scope.provider_type == 'operator' && $scope.op_name == "RSRTC")
                    {
                        if ($scope.count == 1)
                        {
                            if(sleeper == true)
                            {
                                angular.element(event.target).addClass('selectedseat');
                                $(".selectedseat").width(25).height(40);
                            }
                            angular.element(event.target).addClass('selectedseat');
                            angular.element(event.target).removeClass('available');
                            angular.element(event.target).removeClass('ladies_available');
                            angular.element(event.target).removeClass('sleeper_available');
                            angular.element(event.target).removeClass('ladies_sleeper_available');
                            angular.element(event.target).removeClass('vsleeper_available');
                            
                            $scope.book = true;
                            if ($("#bookbtn").hasClass("btn-book") == false) {
                                $("#bookbtn").addClass("btn-book");
                            }

                            $scope.passenger = {seatno_curr: details.seat_no,
                                seat_basic_fare_curr: details.seat_basic_fare,
                                seat_curr: seat_curr,
                                birth: uniq,
                                ber: berno,
                                basefare: details.actual_seat_basic_fare,
                                servicetax: details.seat_service_tax_amt,
                                OpScharge: details.op_service_charge_amt,
                                agentcomm: details.agent_commission,
                                agentds: details.agent_comm_tds,
                                change_seat_fare_child: details.child_seat_fare,
                                is_ladies : ladies_seat
                            };
                            //$rootScope.selectedSeat.push($scope.passender);
                            var tempIndex = berth+'_'+details.seat_no;
                            $rootScope.selectedSeat[tempIndex] = $scope.passenger;
                            $scope.bookdetail['passenger_name'][tempIndex] = '';
                            $scope.bookdetail['passenger_age'][tempIndex] = '';
                            $scope.bookdetail['gender'][tempIndex] = '';
                            $scope.passenderdetails.push($scope.passenger);
                            //console.log($scope.passenderdetails);
                            $scope.ticketratescheck();
                            $scope.count++;
                        }
                        else 
                        {
                            showNotification("error", "Only 1 Passengers allowed in one booking");
                        }
                    }
                    else 
                    {
                        showNotification("error", "Only 6 Passengers allowed in one booking");
                    }
                }
            }
            else 
            {
                showNotification("error", "Seat Already Booked");
            }
        }

        $scope.changeseatamt  = function(className) {
            $scope.ticketratescheck();
        }

        $scope.ticketratescheck  = function(uniq) 
        {
            $scope.base_total_fare        = 0;
            $scope.base_total_servicetax  = 0;
            $scope.base_total_opcharge    = 0;
            $scope.agentcommission        = 0;
            $scope.tds                    = 0;
            $scope.btotal                 = 0;
            $scope.btotal_c               = 0;
            $scope.total_cal_fares        = 0;
            $scope.seat_cal_fare          = {};
            var i = 0;
            for(var pk in $rootScope.selectedSeat) 
            {
                var className = '.passanger_age_' + pk;

                var age = $(className).val();
                if(parseInt(age) >= 5 && parseInt(age) < 12 ) {
                    if(typeof $rootScope.selectedSeat[pk].change_seat_fare_child != "undefined" && $rootScope.selectedSeat[pk].change_seat_fare_child != '' && typeof uniq == 'undefined') { 
                        if($scope.op_name == 'MSRTC' || $scope.op_name == 'UPSRTC' || $scope.op_name == 'RSRTC' || $scope.op_name == 'HRTC' ) {
                            $scope.base_total_fare        += parseFloat($rootScope.selectedSeat[pk].change_seat_fare_child);
                            $scope.seat_cal_fare[i]       = parseFloat($rootScope.selectedSeat[pk].change_seat_fare_child);
                        }
                        else {
                            $scope.base_total_fare        += parseFloat($rootScope.selectedSeat[pk].change_seat_fare_child);
                            $scope.seat_cal_fare[i]      = parseFloat($rootScope.selectedSeat[pk].change_seat_fare_child);
                        }
                    }
                    else {
                        $scope.base_total_fare        += parseFloat($rootScope.selectedSeat[pk].basefare);
                        $scope.seat_cal_fare[i]       = parseFloat($rootScope.selectedSeat[pk].basefare);
                    }
                }
                else 
                {
                    if($scope.op_name == 'MSRTC' || $scope.op_name == 'UPSRTC' || $scope.provider_id == "3") 
                    {
                        $scope.base_total_fare        += parseFloat($rootScope.selectedSeat[pk].basefare);
                        $scope.seat_cal_fare[i]       = parseFloat($rootScope.selectedSeat[pk].basefare);
                    }
                    else
                    {
                        $scope.base_total_fare        += parseFloat($rootScope.selectedSeat[pk].basefare); 
                        $scope.seat_cal_fare[i]       = parseFloat($rootScope.selectedSeat[pk].basefare);
                    }
                }
                
    
                $scope.base_total_servicetax += parseFloat($rootScope.selectedSeat[pk].servicetax);
                $scope.base_total_opcharge += parseFloat($rootScope.selectedSeat[pk].OpScharge);
                $scope.total_cal_fares  += parseFloat($rootScope.selectedSeat[pk].seat_basic_fare_curr);

                i++;
            }
            
            if($scope.provider_id !='1') {
                var calculation_data =  
                {   
                    "provider_id":$scope.provider_id,
                    "op_id":$scope.op_id,
                    "op_name":$scope.op_name,
                    "provider_type":$scope.provider_type,
                    "bus_type_id":$scope.bus_type_id,
                    "com_percentange":$scope.op_comm_percentage,
                    "seats":$scope.seat_cal_fare,
                    "total_service_tax":$scope.base_total_servicetax,
                    "total_cal_fare":  ($scope.total_cal_fares + $scope.base_total_opcharge + $scope.base_total_servicetax),
                };
                console.log(calculation_data);
                var response = finalCalculation.getCal($scope,calculation_data).then(function (response) {
                    $scope.agentcommission = parseFloat(response.data.agent_commission_amount);
                    $scope.tds = parseFloat(response.data.tds_value_on_commission_amount);
                    if(response.data.total_fare > 0) {
                        $scope.btotal = (response.data.deduction_amount);
                    }
                    else {
                        $scope.btotal = ($scope.base_total_fare + $scope.base_total_servicetax + $scope.base_total_opcharge + $scope.tds - $scope.agentcommission);
                    }
                    $scope.btotal_c = ($scope.base_total_fare + $scope.base_total_servicetax + $scope.base_total_opcharge);
                });
            }
            else {
                $scope.agentcommission = parseFloat('0.00');
                $scope.tds = parseFloat('0.00');
                $scope.btotal = ($scope.base_total_fare + $scope.base_total_servicetax + $scope.base_total_opcharge + $scope.tds - $scope.agentcommission);
                $scope.btotal_c = ($scope.base_total_fare + $scope.base_total_servicetax + $scope.base_total_opcharge);
            }
            
        };
        
        $scope.checkAll = function(name)
        {
            $scope.commission_percentage = name;
        };

        $scope.update    = function() {
            $scope.second = true;
            
            if($rootScope.msrtchit == true && $scope.second == true)
            {    
                $scope.second = false;
                $rootScope.msrtchit = false;
                $scope.base_total_fare        = 0;
                $scope.base_total_servicetax  = 0;
                $scope.base_total_opcharge    = 0;
                $scope.agentcommission        = 0;
                $scope.tds                    = 0;
                $scope.btotal                 = 0;
                $scope.btotal_c                 = 0;

                $rootScope.selectedSeat = [];
                $scope.passenger = {};
                $scope.passenderdetails = [];
                $scope.bookdetail.passenger_name = []; 
                $scope.bookdetail.passenger_seat = {};
                $scope.bookdetail.passenger_seatype = [];
                $scope.bookdetail.passenger_age = [];
                $scope.bookdetail.passenger_fare = [];
                $scope.bookdetail.passenger_uniq_seat = [];

                $scope.count = 1;
                if($scope.bookdetail.boarding_stps == '' || $scope.bookdetail.boarding_stps === undefined) {
                    showNotification("error","Please Select boarding stop");
                    return false;
                }
                if($scope.bookdetail.alighting_stps == '' || $scope.bookdetail.alighting_stps === undefined) {
                    showNotification("error","Please Select alighting stop");
                    return false;
                }

                //console.log($scope.bookdetail.boarding_stps);
                //console.log($scope.bookdetail.alighting_stps);
                if($scope.bookdetail.boarding_stps) {    
                    var values_b               = $scope.bookdetail.boarding_stps.split("-");
                    $scope.msrtc_boarding      = values_b[0];
                    $scope.boarding_stps_name  = values_b[1];
                    $scope.boarding_stps_time  = values_b[2];
                }
                if($scope.bookdetail.alighting_stps) { 
                    var values_a               = $scope.bookdetail.alighting_stps.split("-");
                    $scope.msrtc_alighting     = values_a[0];
                    $scope.alighting_stps_name = values_a[1];
                    $scope.alighting_stps_time = values_a[2];
                }

                var post_data = {   
                    "from":$scope.boarding_stps_name, 
                    "to":$scope.alighting_stps_name,
                    "msrtc_boarding":$scope.msrtc_boarding,
                    "msrtc_alighting":$scope.msrtc_alighting,
                    "trip_no":$scope.trip_no,
                    "provider_id":$scope.provider_id,
                    "provider_type":$scope.provider_type,
                    "date":$scope.date_of_jour,
                    "op_name":$scope.op_name,
                    "op_id":$scope.op_id,
                    "bus_type_id":$scope.bus_type_id,
                    "op_bus_type_cd":$scope.op_bus_type_cd,
                    "op_trip_no":$scope.op_trip_no,
                    "inventory_type":"",
                };
                //console.log(post_data);
                $scope.newseatsget(post_data);  
            }
        }

        $scope.retrivebooking = function() {
            var results = false;
            if(($scope.retbook.pnr_no == undefined) && ($scope.retbook.boss_ref_no === undefined)) {
                showNotification("error","Please Enter pnr_no or boss_ref_no");
                results = false;
                return false;
            }
            else {
                results = true;
                if(results ==  true) {
                    $scope.pnr = $scope.retbook.pnr_no;
                    $scope.boss_ref_no = $scope.retbook.boss_ref_no;

                    var post_data =  {   
                        "pnr":$scope.pnr, 
                        "boss_ref_no":$scope.boss_ref_no,
                    }
                    var response = viewTicket.getTicket($scope,post_data).then(function (response) {
                        if(response.data.flag == '@#success#@') {    
                           //console.log(response.data.ticket_ref_no);
                           $window.location.href = base_url+"front/booking_angular/view_ticket/"+response.data.ticket_ref_no;
                        }
                        else if((response.data.flag == '@#error#@')) {
                            showNotification("error","No result Found");
                            return false;
                        }
                    });
                }
            }
        }
        
    }).directive('input', [
    function () {
        return {
            restrict: 'E',
            require: '?ngModel',
            link: function ($scope, element, attrs, controller) {
                if (!attrs.type || attrs.type !== "text" || !controller) {
                    return;
                }
                element.on('blur', function () {
                    $scope.$apply(function () {
                        controller.$setViewValue(element.val());
                    });
                });
            }
        };
    }
    ]).directive('confirmationNeeded', function () 
    {
        return {
            priority: 1,
            terminal: true,
            link: function (scope, element, attr) {
              var msg = attr.confirmationNeeded || "Are you sure you want to book ticket?";
              var clickAction = attr.ngClick;
              element.bind('click',function () {
                if ( window.confirm(msg) ) {
                  scope.$eval(clickAction)
                }
              });
            }
        };
    }).directive('arrowSelector', ['$document', function($document) {
            return {
                restrict: 'A',
                link: function(scope, elem, attrs, ctrl) {
                    var elemFocus = false;
                    elem.on('mouseenter', function() {
                        elemFocus = true;
                        //console.log(elemFocus);
                    });
                    elem.on('mouseleave', function() {
                        elemFocus = false;
                        //console.log(elemFocus);
                    });
                    $document.bind('keydown keypress', function(e) {
                        if (elemFocus) {
                            if (e.keyCode == 38) {
                                //console.log(scope.selectedRow);
                                if (scope.selectedRow == 0) {
                                    return;
                                }
                                scope.selectedRow--;
                                scope.$apply();
                                e.preventDefault();
                            }
                            if (e.keyCode == 40) {
                                //console.log(scope.selectedRow);
                                if (scope.selectedRow == scope.all_bus_services.data.bus_detail.length - 1) {
                                    return;
                                }
                                scope.selectedRow++;
                                scope.$apply();
                                e.preventDefault();
                            }
			    if (e.keyCode == 13) 
                            {
                                console.log(scope.selectedRow);
                                if(scope.op_name == 'MSRTC' || scope.op_name == 'UPSRTC' || scope.op_name == 'RSRTC' || scope.op_name == 'HRTC')
                                {
                                    angular.element('#rowmhur_'+(scope.selectedRow)).click();
                                }
                                else
                                {
				   angular.element('#row_'+(scope.selectedRow)).click();
                                }
                            }
                        }
                    });
                }
            };
        }]);
