var app = angular.module("myapp",[])
    .controller("mycontroller",function($scope,$http,userServices)
    {
           $scope.hitAPI = true;
           $scope.userList = function () 
           {
                if($scope.hitAPI)
                {
                    var response = userServices.getUsers().then(function (response) {

                        $scope.names = response;
                        $scope.showMe = !$scope.showMe;
                        $scope.hitAPI = false;
                    });
                }
            };
            
            $scope.cancel = function () 
            {
                if ($scope.editing !== false) 
                {
                    $scope.hitAPI = true;
                    $scope.userList();
                    $scope.editing = false;
                }    
                //$scope.editMode = !$scope.editMode;
            };
        
        $scope.orderby = function (y)
        {
            $scope.orderme = y;
        }
        
        $scope.reset = function() 
        {
            $scope.firstName = '';
            $scope.lastName  = '';
            $scope.role_id   = '';
        };

         
        $scope.submitForm = function() 
        {
            var post_data = {   "firstname":$scope.firstName, 
                                "lastName":$scope.lastName, 
                                "role_id":$scope.role_id
                            };
            $http({
                url: base_url+"front/test/add",
                data: post_data,
                method: 'POST',
                headers : {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}

            }).success(function(data)
            {
                $scope.reset();
                console.log("OK", data);
                $scope.hitAPI = true;
                $scope.userList();
            }).error(function(err){"ERR", console.log(err)})
        };
        
        
        $scope.removeItem = function (x) 
        {
            $http({
                    url: base_url+"front/test/remove",
                    data: x,
                    method: 'POST',
                    headers : {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}

                }).success(function(data)
                {
                    $scope.reset();
                    console.log("OK", data);
                    $scope.hitAPI = true;
                    $scope.userList();
                }).error(function(err){"ERR", console.log(err)})
        }
        
        
        $scope.newField = {};
        $scope.editing = false;
        
        $scope.editAppKey = function(field) 
        {
            $scope.newField = angular.copy(field);
            $scope.editing  = true;
        }
        
        $scope.saveField = function(index) 
        {
            if ($scope.editing !== false) 
            {
                $scope.data = angular.copy(index);
                $scope.editing = false;
                $http({
                    url: base_url+"front/test/update",
                    data: $scope.data,
                    method: 'POST',
                    headers : {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}

                }).success(function(data)
                {
                    $scope.reset();
                    console.log("OK", data);
                    $scope.hitAPI = true;
                    $scope.userList();
                }).error(function(err){"ERR", console.log(err)})
            }       
        }; 
    });