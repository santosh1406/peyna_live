bus_services_app.factory('searchServices', function ($http, $q) { 
    return {
        
        getSearch: function ($scope,data) 
        {
            $('.no_routes_notification').hide();
            $('#passdetails').hide();
            $('#servtable').hide();
            $('#mydiv').show();
           
            var deferred = $q.defer();
            $http.post(base_url+"front/home/bus_services",data,{'Content-Type': 'application/x-www-form-urlencoded'})
            .then(function (response) 
            {
                $('#seatspresent').hide();
                $('.no_routes_notification').show();
                $('#mydiv').hide();
                $('#servtable').show();
                $('#passdetails').show();
                deferred.resolve(response);
            },function(e){
                 deferred.reject(e);
            });
            
            return deferred.promise;
        }
    };
});


bus_services_app.factory('seatLayouts', function ($http, $q) { 
    return {
        getSeats: function ($scope,data) 
        {
            $('#boseat').show();
            $('.trvl_info').hide();
            $('.seatsbox').hide();
            $('#mydiv_busstop').show();
            $('#seatspresent').hide();
            var deferred = $q.defer();
            $http.post(base_url+"front/booking_angular/get_seat_layout",data,{'Content-Type': 'application/x-www-form-urlencoded'})
            .then(function (response) 
            {
                $('#mydiv_busstop').hide();
                $('#seatspresent').show();
                $('.trvl_info').show();
                $('.seatsbox').show();
                $('#boseat').show();
                deferred.resolve(response);
            },function(e){
                 deferred.reject(e);
            });
            
            return deferred.promise;
        }
    };
});

bus_services_app.factory('seatTempbook', function ($http, $q) { 
    return {
        getTempseat: function ($scope,data) 
        {
            var deferred = $q.defer();
            $http.post(base_url+"front/booking_angular/temp_booking",data,{'Content-Type': 'application/x-www-form-urlencoded'})
            .then(function (response) 
            {
                deferred.resolve(response);
            },function(e){
                 deferred.reject(e);
            });
            
            return deferred.promise;
        }
    };
});


bus_services_app.factory('busServicestopdetails', function ($http, $q) 
{ 
    return {
        getBustop: function ($scope,data) 
        {
            var deferred = $q.defer();
            $http.post(base_url+"front/booking_angular/get_bus_service_stop_details",data,{'Content-Type': 'application/x-www-form-urlencoded'})
            .then(function (response) 
            {
                deferred.resolve(response);
            },function(e){
                 deferred.reject(e);
            });
            
            return deferred.promise;
        }
    };
});

bus_services_app.factory('viewTicket', function ($http, $q) 
{ 
    return {
        getTicket: function ($scope,data) 
        {
            var deferred = $q.defer();
            $http.post(base_url+"front/booking_angular/angular_get_view_ticket",data,{'Content-Type': 'application/x-www-form-urlencoded'})
            .then(function (response) 
            {
                deferred.resolve(response);
            },function(e){
                 deferred.reject(e);
            });
            
            return deferred.promise;
        }
    };
});

bus_services_app.factory('finalCalculation', function ($http, $q) { 
    return {
        getCal: function ($scope,data) 
        {
            var deferred = $q.defer();
            $http.post(base_url+"front/booking_angular/agent_commission_calculation",data,{'Content-Type': 'application/x-www-form-urlencoded'})
            .then(function (response) 
            {
                deferred.resolve(response);
            },function(e){
                 deferred.reject(e);
            });
            
            return deferred.promise;
        }
    };
});