//created by sachin 24-08-2018

function submitBBPSForm(formId) {
    var postStr = $(formId).serialize();
    var operator = $('#operator_id').val();
    if(operator == ''){
        $('#error_div').html('');
        $('#error_div').html('Please enter a valid operator.');
        return;
    }
//    showLoaderImg();
    $.ajax({
        type: "POST",
        url: base_url + "admin/Utilities/get_bill",
        data: postStr,
        dataType: 'json',
        success: function (resp) {
            hideLoaderImg();
            if (resp != '') {
                if (resp.status_field == 'Failed') {
                    $('#error_div').html('');
                    $('#error_div').html(resp.error_field);
                } else {
                    if (resp.status == 'Failed') {
                        $('#error_div').html('');
                        $('#error_div').html(resp.msg);
                    } else if (resp.status == 'Success') {
                        $('#bill_number').val(resp.data.check_result.BILL_NUMBER);
                        $('#bill_number_txt').html(resp.data.check_result.BILL_NUMBER);
                        $('#bill_date').html(resp.data.check_result.BILL_DATE);
                        $('#bill_due_date').html(resp.data.check_result.BILL_DUE_DATE);
                        $('#amount').html(resp.data.check_result.AMOUNT);
                        $('#bill_amt').val(resp.data.check_result.AMOUNT);
//                        $('#bill_amt').val(resp.data.check_result.PRICE);
                        $('#cust_name').html(resp.data.check_result.CUSTOMER_NAME);
                        $('#submit_bbps').addClass('hide');
                        $('#f_submit_bbps').removeClass('hide');
                        $('#bill_details').removeClass('hide');
                    }
                }
            } else {

            }
        }
    });
}


$(document).ready(function () {
    var rechargeType = $('#type').val();

    $("#operator").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: base_url + "admin/utilities/mobileOperators",
                dataType: "json",
                data: {
                    term: request.term,
                    type: rechargeType
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.value,
                            name: item.name,
                            id: item.id
                        };
                    }));
                }
            });
        },
        autoFocus: true,
        minLength: 1,
        select: function (event, ui) {
             $('#error_div').html('');
            removeBillDetails();
            $("#operator").val(ui.item.name);
            if (ui.item.name == "No result found") {
                $("#operator").val("");
            }else{
                $("#operator_id").val(ui.item.id);
            }
            //landline
            if(rechargeType == 'landline'){
                $('#authenticator_div').addClass('hide');
                $('#phone_label').html('Number Without STD Code *:');
                
                if (ui.item.id == 16 || ui.item.name == 17) { ////BSNL Landline-Online or BSNL Landline-Retail
                    $('#phone_label').html('Number With STD Code (Without 0) *:');
                    $('#authenticator_div').removeClass('hide');
                } else {
                    $('#authenticator_div').addClass('hide');
                    $('#phone_label').html('Number Without STD Code *:');
                }
            }
            //electricity
            if(rechargeType == 'electricity'){
                 $('#consumer_label').html('Consumer Number *:');
                 $('#account_label').html('Account Number *:');
                 $('#account_div').addClass('hide');
                 $('#authenticator_div').addClass('hide');
                
                if (ui.item.id == 39) { //Dakshin Haryana Bijli Vitran Nigam
                    $('#consumer_label').html('Account Number *:');
                    $('#account_label').html('Mobile Number *:');
                    $('#account_div').removeClass('hide');
                }else if(ui.item.id == 49){ //Jharkhand Bijli Vitran Nigam Limited (JBVNL)
                    $('#account_label').html('Subdivision Code *:');
                    $('#account_div').removeClass('hide');
                }else if(ui.item.id == 54){ // MSEDC
                    $('#account_label').html('Billing Unit *:');
                    $('#account_div').removeClass('hide')
                    $('#authenticator_div').removeClass('hide');
                }else if(ui.item.id == 63){ // Reliance Energy (Mumbai)
                    $('#consumer_label').html('Customer Number *:');
                    $('#account_label').html('Cycle Number *:');
                    $('#account_div').removeClass('hide');
                }else if(ui.item.id == 67){ // Southern Power Distr of Andhra Pradesh
                    $('#consumer_label').html('Service Number *:');
                }else if(ui.item.id == 68){ // Torrent
                    $('#consumer_label').html('Service Number *:');
                    $('#account_label').html('City Name *:');
                    $('#account_div').removeClass('hide');
                }else if(ui.item.id == 78){ // West Bengal State Electricity
                    $('#account_label').html('Mobile Number *:');
                    $('#account_div').removeClass('hide');
                }else if(ui.item.id == 35){ // Chhattisgarh State Electricity Board
                    $('#consumer_label').html('Business Partener Number *:');
                }else if(ui.item.id == 21 || ui.item.id == 33 || ui.item.id == 47 || ui.item.id == 48 || ui.item.id == 50 || ui.item.id == 64 || ui.item.id == 71){ // Bharatpur Electricity Services Ltd, Bikaner Electricity Supply Limited (BkESL)
                    $('#consumer_label').html('K Number *:');
                }else if(ui.item.id == 40 || ui.item.id == 75){ // DNH Power Distribution Company Limited, Uttarakhand Power Corporation Limited
                     $('#consumer_label').html('Service Connection Number *:');
                }else if(ui.item.id == 41){ // DNH Power Distribution Company Limited
                     $('#consumer_label').html('Service Number *:');
                }else if(ui.item.id == 46){ // Jamshedpur Utilities and Services Company Limited
                     $('#consumer_label').html('partner Number *:');
                }else if(ui.item.id == 51){ // Madhya Pradesh Paschim Kshetra Vidyut Vitaran Co. Ltd -Indore
                     $('#consumer_label').html('IVRS Number *:');
                }else if(ui.item.id == 57 ||ui.item.id == 65 ||ui.item.id == 72){ // North Bihar Power Distribution Company Ltd, South Bihar Power Distribution Company Ltd, Tata Power – Mumbai
                     $('#consumer_label').html('CA Number *:');
                }else if(ui.item.id == 37 || ui.item.id == 62 || ui.item.id == 74) { // Daman and Diu Electricity Department, Punjab State Power Corporation Ltd (PSPCL), Uttar Haryana Bijli Vitran Nigam
                     $('#consumer_label').html('Account Number *:');
                }else{
                    $('#consumer_label').html('Consumer Number *:');
                    $('#account_label').html('Account Number *:');
                    $('#account_div').addClass('hide');
                }
            }
            
            //gas
            if(rechargeType == 'gas'){
                $('#consumer_label').html('Consumer Number *:');
                $('#account_label').html('Account Number *:');
                $('#account_div').addClass('hide');
                    
                if (ui.item.id == 25) { //ADANI GAS
                    $('#consumer_label').html('Customer ID *:');
                    
                }else if(ui.item.id == 26){ // Gujarat Gas company Limited
                    $('#consumer_label').html('Service Number *:');
                    
                }else if(ui.item.id == 80){ // Mahanagar Gas Limited
                    $('#consumer_label').html('Customer Account Number *:');
                    $('#account_label').html('Bill Group Number *:');
                    $('#account_div').removeClass('hide')
                    
                }else if(ui.item.id == 81){ // Haryana City gas
                    $('#consumer_label').html('CRN Number *:');
                    
                }else if(ui.item.id == 82){ // siti
                    $('#consumer_label').html('ARN Number *:');
                    
                }else if(ui.item.id == 84){ // Sabarmati Gas Limited (SGL)
                    $('#consumer_label').html('Customer ID *:');
                 
                }else{
                    $('#consumer_label').html('Consumer Number *:');
                    $('#account_label').html('Account Number *:');
                    $('#account_div').addClass('hide');
                }
            }
            
            //water
            if(rechargeType == 'water'){
                if (ui.item.id == 27) { //UIT Bhiwadi
                    $('#consumer_label').html('Customer ID *:');
                    
                }else if(ui.item.id == 29){ // Delhi Jal Board-Retail
                    $('#consumer_label').html('K Number *:');
                    
                }else if(ui.item.id == 30){ // Municipal Corporation of Gurugram
                    $('#consumer_label').html('K Number *:');
                    
                }else{
                    $('#consumer_label').html('Consumer Number *:');
                }
            }
            
            //Broadband Provider
            if(rechargeType == 'broadband'){
                if (ui.item.id == 31) { //Connect Broadband-Retail
                    $('#consumer_label').html('Directory Number *:');
                    
                }else if(ui.item.id == 32){ // Hathway Broadband - Retail
                    $('#consumer_label').html('Customer ID *:');
                    
                }else{
                    $('#consumer_label').html('Consumer Number *:');
                }
            }
            
            
            return false;
        }
    });
});

$(document).ready(function () {
    $('#bbps_form').bootstrapValidator({
        group: '.form-control',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            recharge_type: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b>Please select type.</font>'
                    }
                }
            },
            authenticator: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b>Please select service type.</font>'
                    }
                }
            },
            operator: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b>Please enter operator.</font>'
                    }
                }
            },
            mobile_number: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b> Please enter mobile number.</font>'
                    },
                    regexp: {
                        regexp: /^[789]\d{9}$/,
                        message: '<b><font style="color:red; size="1">X</b>Enter valid mobile number.</font>'
                    }
                }
            },
            number: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b> Please enter number.</font>'
                    }
                }
            },
            phone_number: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b> Please enter phone number.</font>'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '<b><font style="color:red; size="1">X</b>Enter valid phone number.</font>'
                    }
                }
            },
            account_number: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b> Please enter number.</font>'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '<b><font style="color:red; size="1">X</b>Enter valid number.</font>'
                    }
                }
            },
            recharge_amount: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b> Please enter recharge amount.</font>'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '<b><font style="color:red; size="1">X</b>Amount can only consist of number.</font>'
                    }
                }
            }
        }
    });
});

function removeBillDetails() {
    $('#submit_bbps').removeClass('hide');
    $('#f_submit_bbps').addClass('hide');
    $('#bill_details').addClass('hide');
   
}