//created by sachin 24-08-2018
function submitBBPSForm(formId) {
    var postStr = $(formId).serialize();
    var operator = $('#operator_id').val();
    
    if(operator == ''){
        $('#error_div').html('');
        $('#error_div').html('Please enter a valid operator.');
        return;
    }
//    showLoaderImg();
    $.ajax({
        type: "POST",
        url: base_url + "admin/Utilities_bbps/get_bill",
        data: postStr,
        dataType: 'json',
        success: function (resp) {
            hideLoaderImg();
            if (resp != '') {
                if (resp.status_field == 'Failed') {
                    $('#error_div').html('');
                    $('#error_div').html(resp.error_field);
                } else {
                    if (resp.status == 'Failed') {
                        $('#error_div').html('');
                        $('#error_div').html(resp.msg);
                    } else if (resp.status == 'Success') {
                        $('#bill_number').val(resp.data.check_result.BILL_NUMBER);
                        $('#ref_id').val(resp.data.check_result.REF_ID);
                        $('#bill_number_txt').html(resp.data.check_result.BILL_NUMBER);
                        $('#bill_date').html(resp.data.check_result.BILL_DATE);
                        $('#bill_due_date').html(resp.data.check_result.BILL_DUE_DATE);
                        $('#amount').html(resp.data.check_result.AMOUNT);
                        $('#bill_amt').val(resp.data.check_result.AMOUNT);
//                        $('#bill_amt').val(resp.data.check_result.PRICE);
                        $('#cust_name').html(resp.data.check_result.CUSTOMER_NAME);
                        $('#submit_bbps').addClass('hide');
                        $('#f_submit_bbps').removeClass('hide');
                        $('#bill_details').removeClass('hide');
                    }
                }
            } else {

            }
        }
    });
}


$(document).ready(function () {
    var rechargeType = $('#type').val();

    $("#operator").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: base_url + "admin/Utilities_bbps/mobileOperators",
                dataType: "json",
                data: {
                    term: request.term,
                    type: rechargeType
                },
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.label,
                            value: item.value,
                            name: item.name,
                            id: item.id
                        };
                    }));
                }
            });
        },
        autoFocus: true,
        minLength: 1,
        select: function (event, ui) {
             $('#error_div').html('');
            removeBillDetails();
            $("#operator").val(ui.item.name);
            if (ui.item.name == "No result found") {
                $("#operator").val("");
            }else{
                $("#operator_id").val(ui.item.id);
            }
            //landline
            if(rechargeType == 'landline'){
                $('#authenticator_div').addClass('hide');
                $('#phone_label').html('Number Without STD Code *:');
                
                if (ui.item.id == 16 || ui.item.name == 17) { ////BSNL Landline-Online or BSNL Landline-Retail
                    $('#phone_label').html('Number With STD Code (Without 0) *:');
                    $('#authenticator_div').removeClass('hide');
                } else {
                    $('#authenticator_div').addClass('hide');
                    $('#phone_label').html('Number Without STD Code *:');
                }
            }
            //electricity
            if(rechargeType == 'electricity'){
                 $('#consumer_label').html('Consumer Number *:');
                 $('#account_label').html('Account Number *:');
                 $('#account_div').addClass('hide');
                 $('#authenticator_div').addClass('hide');
                
                if (ui.item.id == 39) { //Dakshin Haryana Bijli Vitran Nigam
                    $('#consumer_label').html('Account Number *:');
                    $('#account_label').html('Mobile Number *:');
                    $('#account_div').removeClass('hide');
                }else if(ui.item.id == 49){ //Jharkhand Bijli Vitran Nigam Limited (JBVNL)
                    $('#account_label').html('Subdivision Code *:');
                    $('#account_div').removeClass('hide');
                }else if(ui.item.id == 54){ // MSEDC
                    $('#account_label').html('Billing Unit *:');
                    $('#account_div').removeClass('hide')
                    $('#authenticator_div').removeClass('hide');
                }else if(ui.item.id == 63){ // Reliance Energy (Mumbai)
                    $('#consumer_label').html('Customer Number *:');
                    $('#account_label').html('Cycle Number *:');
                    $('#account_div').removeClass('hide');
                }else if(ui.item.id == 67){ // Southern Power Distr of Andhra Pradesh
                    $('#consumer_label').html('Service Number *:');
                }else if(ui.item.id == 68){ // Torrent
                    $('#consumer_label').html('Service Number *:');
                    $('#account_label').html('City Name *:');
                    $('#account_div').removeClass('hide');
                }else if(ui.item.id == 78){ // West Bengal State Electricity
                    $('#account_label').html('Mobile Number *:');
                    $('#account_div').removeClass('hide');
                }else if(ui.item.id == 35){ // Chhattisgarh State Electricity Board
                    $('#consumer_label').html('Business Partener Number *:');
                }else if(ui.item.id == 21 || ui.item.id == 33 || ui.item.id == 47 || ui.item.id == 48 || ui.item.id == 50 || ui.item.id == 64 || ui.item.id == 71){ // Bharatpur Electricity Services Ltd, Bikaner Electricity Supply Limited (BkESL)
                    $('#consumer_label').html('K Number *:');
                }else if(ui.item.id == 40 || ui.item.id == 75){ // DNH Power Distribution Company Limited, Uttarakhand Power Corporation Limited
                     $('#consumer_label').html('Service Connection Number *:');
                }else if(ui.item.id == 41){ // DNH Power Distribution Company Limited
                     $('#consumer_label').html('Service Number *:');
                }else if(ui.item.id == 46){ // Jamshedpur Utilities and Services Company Limited
                     $('#consumer_label').html('partner Number *:');
                }else if(ui.item.id == 51){ // Madhya Pradesh Paschim Kshetra Vidyut Vitaran Co. Ltd -Indore
                     $('#consumer_label').html('IVRS Number *:');
                }else if(ui.item.id == 57 ||ui.item.id == 65 ||ui.item.id == 72){ // North Bihar Power Distribution Company Ltd, South Bihar Power Distribution Company Ltd, Tata Power – Mumbai
                     $('#consumer_label').html('CA Number *:');
                }else if(ui.item.id == 37 || ui.item.id == 62 || ui.item.id == 74) { // Daman and Diu Electricity Department, Punjab State Power Corporation Ltd (PSPCL), Uttar Haryana Bijli Vitran Nigam
                     $('#consumer_label').html('Account Number *:');
                }else{
                    $('#consumer_label').html('Consumer Number *:');
                    $('#account_label').html('Account Number *:');
                    $('#account_div').addClass('hide');
                }
            }
            
            //gas
            if(rechargeType == 'gas'){
                $('#consumer_label').html('Consumer Number *:');
                $('#account_label').html('Account Number *:');
                $('#account_div').addClass('hide');
                    
                if (ui.item.id == 25) { //ADANI GAS
                    $('#consumer_label').html('Customer ID *:');
                    
                }else if(ui.item.id == 26){ // Gujarat Gas company Limited
                    $('#consumer_label').html('Service Number *:');
                    
                }else if(ui.item.id == 80){ // Mahanagar Gas Limited
                    $('#consumer_label').html('Customer Account Number *:');
                    $('#account_label').html('Bill Group Number *:');
                    $('#account_div').removeClass('hide')
                    
                }else if(ui.item.id == 81){ // Haryana City gas
                    $('#consumer_label').html('CRN Number *:');
                    
                }else if(ui.item.id == 82){ // siti
                    $('#consumer_label').html('ARN Number *:');
                    
                }else if(ui.item.id == 84){ // Sabarmati Gas Limited (SGL)
                    $('#consumer_label').html('Customer ID *:');
                 
                }else{
                    $('#consumer_label').html('Consumer Number *:');
                    $('#account_label').html('Account Number *:');
                    $('#account_div').addClass('hide');
                }
            }
            
            //water
            if(rechargeType == 'water'){
                if (ui.item.id == 27) { //UIT Bhiwadi
                    $('#consumer_label').html('Customer ID *:');
                    
                }else if(ui.item.id == 29){ // Delhi Jal Board-Retail
                    $('#consumer_label').html('K Number *:');
                    
                }else if(ui.item.id == 30){ // Municipal Corporation of Gurugram
                    $('#consumer_label').html('K Number *:');
                    
                }else{
                    $('#consumer_label').html('Consumer Number *:');
                }
            }
            
            //Broadband Provider
            if(rechargeType == 'broadband'){
                if (ui.item.id == 31) { //Connect Broadband-Retail
                    $('#consumer_label').html('Directory Number *:');
                    
                }else if(ui.item.id == 32){ // Hathway Broadband - Retail
                    $('#consumer_label').html('Customer ID *:');
                    
                }else{
                    $('#consumer_label').html('Consumer Number *:');
                }
            }
            return false;
        }
    });
});

$(document).ready(function () {
    var rechargeType = $('#type').val();
    //alert(rechargeType);
    $("#bbpsoperator").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: base_url + "admin/utilities_bbps/bbpsOperators",
                dataType: "json",
                data: {
                    term: request.term,
                    type: rechargeType
                },
                success: function (data) { 
                    response($.map(data, function (item) { 
                        return {
                            label: item.label,
                            value: item.value,
                            name: item.name,
                            id: item.id
                        };
                    }));
                }
            });
        },
        autoFocus: true,
        minLength: 1,
        select: function (event, ui) {
            $('#error_div').html('');
            removeBillDetails();
            $("#bbpsoperator").val(ui.item.name);
                       
            if (ui.item.name == "No result found") {
                $("#bbpsoperator").val("");
            }else{
                $("#operator_id").val(ui.item.id);
                 console.log(ui.item.id);
                 console.log(ui.item.name);
            }
            
            if(rechargeType == 'Electricity') { 
                $('#consumer_label').html('Consumer Number *:');
                $('#account_label').html('Account Number *:');
                $('#account_div').addClass('hide');
                $('#authenticator_div').addClass('hide');
                $('#city_div').addClass('hide');
                
                if (ui.item.id == "EPDCLOB00ANP01" || ui.item.id == "SPDCLOB00ANP01") { 
                    $('#consumer_label').html('Service Number *:');
                }else if(ui.item.id == "RELI00000MUM03" || ui.item.id == "RELI00000MUM01"){
                    $('#consumer_label').html('Consumer Number *:');
                }else if(ui.item.id == "AVVNL0000RAJ01" || ui.item.id =="JDVVNL000RAJ01" || ui.item.id =="BKESL0000RAJ02" || ui.item.id =="JVVNL0000RAJ01"
                    || ui.item.id =="KEDLOB000RAJ02" || ui.item.id =="KEDLOB000KTA01" || ui.item.id =="TPADL0000AJM02" || ui.item.id =="TPADL0000AJM01"
                    || ui.item.id == "HPSEB0000HIP01"){
                    $('#consumer_label').html('K Number *:');
                }else if(ui.item.id == "APDCL0000ASM02" || ui.item.id == "APDCL0000ASM01" || ui.item.id == "BEST00000MUM01" || ui.item.id =="WBSEDCL00WBL01"){
                    $('#consumer_label').html('Consumer ID *:');
                }else if(ui.item.id == "BSESRAJPLDEL01" || ui.item.id == "BSESYAMPLDEL01" || ui.item.id =="NBPDCL000BHI01" 
                    || ui.item.id =="SBPDCL000BHI01" || ui.item.id =="TATAPWR00DEL01"){
                    $('#consumer_label').html('CA Number *:');
                }else if(ui.item.id == "BESCOM000KAR01" || ui.item.id == "KESCO0000UTP01"){
                    $('#consumer_label').html('Consumer ID/Account Number *:');
                }else if(ui.item.id == "BESLOB000RAJ02" || ui.item.id == "BESLOB000BRT01" || ui.item.id == "BKESL0000BKR01"){
                    $('#consumer_label').html('K Number *:');
                }else if(ui.item.id == "CESC00000KOL01"){
                    $('#consumer_label').html('Customer ID (Not Consumer No) *:');
                }else if(ui.item.id == "CESCOM000KAR01" || ui.item.id =="GESCOM000KAR01" || ui.item.id =="NPCL00000NOI01" || ui.item.id == "HESCOM000KAR01"){
                    $('#consumer_label').html('Account ID (RAPDRP) OR Consumer Number / Connection ID (Non-RAPDRP) *:');
                }else if(ui.item.id == "CSPDCL000CHH01" || ui.item.id =="JUSC00000JAM01"){
                    $('#consumer_label').html('Business Partner Number *:');
                }else if(ui.item.id == "DNHPDCL0DNH001"){
                    $('#consumer_label').html('Service Connection Number *:');
                }else if(ui.item.id == "DGVCL0000GUJ01" || ui.item.id =="PGVCL0000GUJ01" || ui.item.id =="UGVCL0000GUJ01" || ui.item.id =="SNDL00000NAG01"){
                    $('#consumer_label').html('Consumer Number *:');
                }else if(ui.item.id == "NESCO0000ODI01" || ui.item.id =="SOUTHCO00ODI01" || ui.item.id =="UPPCL0000UTP02" ||
                    ui.item.id == "WESCO0000ODI01" || ui.item.id == "UPPCL0000UTP01"){
                    $('#consumer_label').html('Consumer Number *:');
                }else if(ui.item.id == "DHBVN0000HAR01" || ui.item.id =="PSPCL0000PUN01" || ui.item.id == "DDED00000DAD01"
                    || ui.item.id == "TNEB00000TND01" || ui.item.id == "UHBVN0000HAR01"){
                    $('#consumer_label').html('Account Number *:');
                }else if(ui.item.id == "JBVNL0000JHA01" || ui.item.id == "MGVCL0000GUJ01"){
                    $('#account_label').html('Sub division Code *:');
                    $('#account_div').removeClass('hide');
                }else if(ui.item.id == "MPCZ00000MAP02" || ui.item.id =="MPCZ00000MAP01" || ui.item.id == "MPEZ00000MAP02" || ui.item.id == "MPEZ00000MAP01" || ui.item.id =="NDMC00000DEL02"){
                    $('#consumer_label').html('Consumer Number/IVRS *:');
                }else if(ui.item.id == "MAHA00000MAH01" || ui.item.id == "TATAPWR00MUM01"){
                    $('#account_label').html('BU *:');
                    $('#account_div').removeClass('hide');
                }else if(ui.item.id == "MPDC00000MEG01" || ui.item.id == "TSEC00000TRI01"){
                     $('#consumer_label').html('Consumer ID *:');
                }else if(ui.item.id == "SKPR00000SIK01"){
                     $('#consumer_label').html('Contract Acc Number *:');
                }else if(ui.item.id == "TORR00000AGR01" || ui.item.id =="TORR00000AHM02" || ui.item.id == "TORR00000BHW03" || ui.item.id == "TORR00000SUR04" || ui.item.id =="NDMC00000DEL02"){
                    $('#consumer_label').html('Service Number *:');
                     $('#city_label').html('City *:');
                    $('#city_div').removeClass('hide');
                }else if(ui.item.id == "UPCL00000UTT01"){
                    ('#consumer_label').html('Service Connection Number *:');
                }else{
                    $('#consumer_label').html('Consumer Number *:');
                    $('#account_label').html('Account Number *:');
                    $('#account_div').addClass('hide');
                    $('#city_div').addClass('hide');
                }
            }
                         
            //gas
            if(rechargeType == 'Gas') { 
                $('#consumer_label').html('Customer ID *:');
                $('#account_label').html('Mobile Number *:');
                $('#account_div').addClass('hide');
                    
                if (ui.item.id == "AGL000000MAP01") { //AVANTIKA GAS
                    $('#consumer_label').html('Customer No *:');
                    
                }else if(ui.item.id == "ADAN00000NAT01"){ // ADANI GAS
                    $('#consumer_label').html('Customer ID *:');
                    
                }else if(ui.item.id == "CUGL00000UTP01"){ // Central U.P. Gas Limited
                    $('#consumer_label').html('Customer Code / CRN Number *:');
                   
                }else if(ui.item.id == "CGSM00000GUJ01"){ // Charotar Gas Sahakari Mandali Ltd
                    $('#consumer_label').html('Customer Number *:');
                    
                }else if(ui.item.id == "GUJGAS000GUJ01"){ // Gujrat Gas
                    $('#consumer_label').html('Customer ID *:');
                    
                }else if(ui.item.id == "HCG000000HAR02"){ // Haryana City Gas - Kapil Chopra Enterprise
                    $('#consumer_label').html('CRN Number *:');
                 
                }else if(ui.item.id == "HCG000000HAR01"){ // Haryana City Gas - Kapil Chopra Enterprise Old
                    $('#consumer_label').html('CRN Number *:');
                 
                }else if(ui.item.id == "IOAG00000DEL01"){ // Indian Oil
                    $('#consumer_label').html('Customer ID *:');
                 
                }else if(ui.item.id == "INDRAPGASDEL02"){ // Indraprastha Gas
                    $('#consumer_label').html('BP Number *:');
                 
                }else if(ui.item.id == "MAHA00000MUM01"){ //  Mahanagar Gas Limited
                    $('#consumer_label').html('CA Number *:');
                 
                }else if(ui.item.id == "MNGL00000MAH02"){ // Maharashtra Natural Gas Limited (MNGL)
                    $('#consumer_label').html('BP Number *:');
                 
                }else if(ui.item.id == "MNGL00000MAH01"){ // Maharashtra Natural Gas Limited (MNGL) - Old
                    $('#consumer_label').html('BP Number *:'); 
                    
                }else if(ui.item.id == "SGL000000GUJ01"){ // Sabarmati Gas Limited (SGL)
                    $('#consumer_label').html('Customer ID *:');
                 
                }else if(ui.item.id == "SITI00000UTP03"){ // Siti energy
                    $('#consumer_label').html('ARN Number *:');
                 
                }else if(ui.item.id == "TNGCLOB00TRI01"){ // Tripura natural gas
                    $('#consumer_label').html('Consumer Number *:');
                 
                }else if(ui.item.id == "UCPGPL000MAH01"){ // Unique Central Piped Gases Pvt Ltd (UCPGPL)
                    $('#consumer_label').html('Customer Number *:');
                 
                }else if(ui.item.id == "VGL000000GUJ01"){ // Vadodara Gas Ltd
                    $('#consumer_label').html('Consumer Number *:');
                 
                }else{
                    $('#consumer_label').html('Consumer Number *:');
                    $('#account_label').html('Mobile Number *:');
                    $('#account_div').addClass('hide');
                }
            }
            
            //water
            if(rechargeType == 'Water'){
                $('#consumer_mob_div').addClass('hide');
                $('#consumer_email_div').addClass('hide');
                    
                if (ui.item.id == "BWSSB0000KAR01") { 
                    $('#consumer_label').html('RR Number *:');
                    
                }else if(ui.item.id == "BMC000000MAP01" || ui.item.id == "GMC000000MAP01"){ 
                    $('#consumer_label').html('Connection Id *:');
                    
                }else if(ui.item.id == "DLJB00000DEL01" || ui.item.id == "MCG000000GUR01"){ 
                    $('#consumer_label').html('K Number *:');
                    
                }else if(ui.item.id == "NDMC00000DEL01" || ui.item.id == "PUNE00000MAHSE"){ 
                    $('#consumer_label').html('Consumer Number *:');
                    
                }else if(ui.item.id == "HMWSS0000HYD01"){ 
                    $('#consumer_label').html('CAN Number *:');
                    
                }else if(ui.item.id == "IMC000000MAP01" || ui.item.id == "JMC000000MAP01"){ 
                    $('#consumer_label').html('Service Number *:');
                    
                }else if(ui.item.id == "MCJ000000PUN01"){ 
                    $('#consumer_label').html('Account No *:');
                    $('#c_mob_number').html('Consumer Mobile Number *:');
                    $('#useremail').html('Consumer Email Id *:');
                    $('#consumer_mob_div').removeClass('hide');
                    $('#consumer_email_div').removeClass('hide');
                    $('#consumer_mob_div').show();
                    $('#consumer_email_div').show();
                    
                }else if(ui.item.id == "MCL000000PUN01"){ 
                    $('#consumer_label').html('Consumer Number *:');
                    $('#consumer_mob_div').removeClass('hide');
                    $('#consumer_email_div').removeClass('hide');
                    $('#consumer_mob_div').show();
                    $('#consumer_email_div').show();
                    
                }else if(ui.item.id == "MCG000000GUR01"){ 
                    $('#consumer_label').html('Consumer Number *:');
                    $('#consumer_mob_div').removeClass('hide');
                    $('#consumer_email_div').removeClass('hide');
                    $('#consumer_mob_div').show();
                    $('#consumer_email_div').show();
                    
                }else if(ui.item.id == "SMC000000GUJ01"){ 
                    $('#consumer_label').html('Connection Number *:');
                    
                }else if(ui.item.id == "UNN000000MAP01"){ 
                    $('#consumer_label').html('Business Partner Number *:');
                    
                }else if(ui.item.id == "UITWOB000BHW02" || ui.item.id =="UITWOB000BHW01"){ 
                    $('#consumer_label').html('Customer ID *:');
                    
                }else if(ui.item.id =="UJS000000UTT01"){
                     $('#consumer_label').html('Consumer Number (Last 7 Digits) *:');
                }else{
                    $('#consumer_label').html('Consumer Number *:');
                    
                }
            }
            
            //Broadband Provider
            if(rechargeType == 'Broadband Postpaid'){ 
                if (ui.item.id == "ACT000000NAT01") {
                    $('#consumer_label').html('Account Number *:');
                    
                }else if(ui.item.id == "ATBROAD00NAT01"){ 
                    $('#consumer_label').html('Landline Number with STD Code *:');
                    
                }else if(ui.item.id == "CONBB0000PUN01"){ 
                    $('#consumer_label').html('Directory Number *:');
                    
                }else if(ui.item.id == "HATHWAY00NAT01"){ 
                    $('#consumer_label').html('Customer ID *:');
                    
                }else if(ui.item.id == "NEXTRA000NAT01"){ 
                    $('#consumer_label').html('Consumer ID *:');
                    
                }else if(ui.item.id == "SPENET000NAT01"){ 
                    $('#consumer_label').html('CAN/Account ID *:');
                    
                }else if(ui.item.id == "TTN000000NAT01"){ 
                    $('#consumer_label').html('User Name *:');
                    
                }else if(ui.item.id == "TIKO00000NAT01"){ 
                    $('#consumer_label').html('Service Id *:');
                    
                }else{
                    $('#consumer_label').html('Account Number *:');
                }
            }
            
             //Landline Provider
            if(rechargeType == 'Landline Postpaid'){ 
                $('#mobile_label').html('Mobile Number *:');
                $('#consumer_div').removeClass('hide');
                $('#consumer_div').show();
                    
                if (ui.item.id == "ATLLI0000NAT01") {
                    $('#consumer_label').html('Landline Number with STD code *:');
                    
                }else if(ui.item.id == "BSNLLLCORNAT01"){ 
                    $('#consumer_label').html('Account Number *:');
                    
                }else if(ui.item.id == "BSNLLLINDNAT01"){ 
                     $('#consumer_label').html('Account Number *:');
                     $('#mobile_label').html('Number with STD Code (without 0) *:');   
                     
                }else if(ui.item.id == "MTNL00000DEL01" || ui.item.id == "MTNL00000MUM01"){ 
                     $('#consumer_label').html('Account Number *:');
                     $('#mobile_label').html('Telephone Number *:');
                     
                }else if(ui.item.id == "TATADLLI0NAT01"){ 
                    $('#consumer_div').addClass('hide');
                    $('#mobile_label').html('Landline Number with STD Code (without 0) *:');
                    
                }else{
                    $('#consumer_label').html('Account Number *:');
                    $('#mobile_label').html('Account Number *:');
                    $('#account_div').addClass('hide');
                }
            }
            
            if(rechargeType == 'DTH'){
                $('#consumer_label').html('Customer ID *:');
                $('#account_label').html('Amount *:');
                $('#account_div').addClass('hide');
                 
                if (ui.item.id == "DISH00000NAT01") { 
                    $('#consumer_label').html('Registered Mobile Number / Viewing Card Number *:');
                    
                }else if(ui.item.id == "SUND00000NAT01"){ 
                    $('#consumer_label').html('Subscriber Number *:');
                    
                }else if(ui.item.id == "SUND00000NAT02"){ 
                    $('#consumer_label').html('Subscriber Number *:');
                     $('#account_div').removeClass('hide');
                    
                }else if(ui.item.id == "TATASKY00NAT01"){ 
                    $('#consumer_label').html('Subscriber Number *:');
                    
                }else if(ui.item.id == "VIDEOCON0NAT01"){ 
                    $('#consumer_label').html('Customer ID / Registered Telephone No *:');
                    
                }else{
                    $('#consumer_label').html('Customer ID *:');
                    $('#account_label').html('Amount *:');
                    $('#account_div').addClass('hide');
                }
            }
    
            return false;
        }
    });
});

$(document).ready(function () {
    $('#bbps_form').bootstrapValidator({
        group: '.form-control',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            recharge_type: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b>Please select type.</font>'
                    }
                }
            },
            authenticator: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b>Please select service type.</font>'
                    }
                }
            },
            operator: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b>Please enter operator.</font>'
                    }
                }
            },
            mobile_number: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b> Please enter mobile number.</font>'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,//^[789]\d{9}$/,
                        message: '<b><font style="color:red; size="1">X</b>Enter valid mobile number.</font>'
                    }
                }
            },
            c_mob_number: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b> Please enter mobile number.</font>'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,//^[789]\d{9}$/,
                        message: '<b><font style="color:red; size="1">X</b>Enter valid mobile number.</font>'
                    }
                }
            },
            useremail: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b> Please enter email address.</font>'
                    },
                    regexp: {
                        regexp:  /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i,
                        message: '<b><font style="color:red; size="1">X</b>Enter valid email address.</font>'
                    }
                }
            },
            number: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b> Please enter number.</font>'
                    }
                }
            },
            phone_number: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b> Please enter phone number.</font>'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '<b><font style="color:red; size="1">X</b>Enter valid phone number.</font>'
                    }
                }
            },
            account_number: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b> Please enter number.</font>'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '<b><font style="color:red; size="1">X</b>Enter valid number.</font>'
                    }
                }
            },
            city: {
                 validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b> Please enter city.</font>'
                    },
                    regexp: {
                        regexp: /^[A-Za-z]+$/,
                        message: '<b><font style="color:red; size="1">X</b>Enter valid city name.</font>'
                    }
                }
            },
            recharge_amount: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b> Please enter recharge amount.</font>'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '<b><font style="color:red; size="1">X</b>Amount can only consist of number.</font>'
                    }
                }
            }
        }
    });
});

function submitBbpsForm(formId, val) {   
    var postStr = $(formId).serialize();
    var submitval = val;
    var operator = $('#operator_id').val();
    var getAmount = '';
    getAmount = $('#amount :selected').text();
    console.log(getAmount);
//    alert(operator);
//    alert(submitval);
//       
    if(operator == ''){
        $('#error_div').html('');
        $('#error_div').html('Please enter a valid operator.');
        return;
    }
    bbpsLoaderImg();
    $.ajax({
        type: "POST",
        url: base_url + "admin/Utilities_bbps/get_bill",
        data: postStr + "&submitval=" + submitval ,
        dataType: 'json',
        success: function (resp) {
            hideLoaderImg();
            if (resp != '') {
                console.log(resp.status);
                console.log(resp.data);
                console.log(resp.status_field);
                if (resp.status_field == 'failed') {
                    $('#error_div').html('');
                    $('#error_div').html(resp.error_field);
                } else {
                    if (resp.status.toLowerCase() == 'failed') {
                        $('#error_div').html('');
                        $('#error_div').html(resp.msg);
                    } else if(resp.status.toLowerCase() == 'successful'){
                        $('#error_div').html(resp.msg);
//                        $('#msg').html('Click button to proceed with your payment');
//                        
//                        $('#submit_bbps').addClass('hide');
//                        $('#f_submit_bbps').removeClass('hide');
//                        $('#bill_details').addClass('hide');
                    } else if (resp.status.toLowerCase() == 'success') { 
                        
                        //alert(resp.status);
                        $('#bill_number').val(resp.data.BILL_NUMBER);                        
                        $('#bill_number_txt').html(resp.data.BILL_NUMBER);
                        $('#bill_date').html(resp.data.BILL_DATE);
                        $('#bill_due_date').html(resp.data.BILL_DUE_DATE);
                        //$('#amount').html(resp.data.AMOUNT);
                        //$('#bill_amt').val(resp.data.AMOUNT);
                        $('#cust_name').html(resp.data.CUSTOMER_NAME);
                        $('#ref_id').html(resp.data.REF_ID);
                        $('#submit_bbps').addClass('hide');
                        $('#f_submit_bbps').removeClass('hide');
                        $('#bill_details').removeClass('hide');
                       
                        
                        $.each( resp.data.AMOUNT_TYPES, function( key, value ) {
                        $("#amount").append("<option value=" + value + ">" + value + "</option>");
                      });
                    }
                }
            } else {
                
            }
        }
    });
}


function removeBillDetails() {
    $('#submit_bbps').removeClass('hide');
    $('#f_submit_bbps').addClass('hide');
    $('#bill_details').addClass('hide');
   
}

    $(document).ready(function() {
        var getAmount = '';
        $("select").change(function(){
            getAmount = $('#amount :selected').text();
            
            console.log(getAmount);
            $('#bill_amt').val(getAmount);
        // $('#bill_amt').html(getAmount);

        });

    });
         
   
      
   