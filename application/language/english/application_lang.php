<?php defined('BASEPATH') || exit('No direct script access allowed');

//------------------------------------------------------------------------------
// ! GENERAL SETTINGS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// BF_Model
//------------------------------------------------------------------------------
$lang['bf_model_db_error']		= 'DB Error: %s';
$lang['bf_model_no_data']		= 'No data available.';
$lang['bf_model_invalid_id']	= 'Invalid ID passed to model.';
$lang['bf_model_no_table']		= 'Model has unspecified database table.';
$lang['bf_model_fetch_error']	= 'Not enough information to fetch field.';
$lang['bf_model_count_error']	= 'Not enough information to count results.';
$lang['bf_model_unique_error']	= 'Not enough information to check uniqueness.';
$lang['bf_model_find_error']	= 'Not enough information to find by.';


//------------------------------------------------------------------------------
// Profiler Template
//------------------------------------------------------------------------------
$lang['bf_profiler_box_benchmarks']  = 'Benchmarks';
$lang['bf_profiler_box_console']     = 'Console';
$lang['bf_profiler_box_files']       = 'Files';
$lang['bf_profiler_box_memory']      = 'Memory Usage';
$lang['bf_profiler_box_queries']     = 'Queries';
$lang['bf_profiler_box_session']     = 'Session User Data';

$lang['bf_profiler_menu_console']    = 'Console';
$lang['bf_profiler_menu_files']      = 'Files';
$lang['bf_profiler_menu_memory']     = 'Memory Used';
$lang['bf_profiler_menu_memory_mb']  = 'MB';
$lang['bf_profiler_menu_queries']    = 'Queries';
$lang['bf_profiler_menu_queries_db'] = 'Database';
$lang['bf_profiler_menu_time']       = 'Load Time';
$lang['bf_profiler_menu_time_ms']    = 'ms';
$lang['bf_profiler_menu_time_s']     = 's';
$lang['bf_profiler_menu_vars']       = '<span>vars</span> &amp; Config';

$lang['bf_profiler_true']            = 'true';
$lang['bf_profiler_false']           = 'false';