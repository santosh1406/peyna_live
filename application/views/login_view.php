<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Admin Login</title> 
        <script src="<?php echo base_url() . COMMON_ASSETS; ?>js/jquery.js" type="text/javascript"></script>
        <script src="<?php echo base_url() . COMMON_ASSETS; ?>js/jquery.validate.min.js" type="text/javascript"></script>
        <script>
            /* $(document).ready(function() {
             $("#login_form").validate();
             });*/
        </script>
        <script>
            // assumes you're using jQuery
            $(document).ready(function () {

                /************** for confirm message***********/
                $('.confirm-div').hide();
<?php if ($this->session->flashdata('invalid')) { ?>
                    $('.confirm-div').html('<?php echo $this->session->flashdata('invalid'); ?>').show();
<?php } else if ($this->session->flashdata('invalid')) { ?>
                    $('.confirm-div').html('<?php echo $this->session->flashdata('valid'); ?>').show();
<?php } ?>
                /*********************************************/

                $("#login_form").validate();
            });
        </script>
    </head>

    <body>
        <div class="confirm-div"></div>

        <div class="container login">
            <?php
            $attributes = array("method" => "POST", "id" => "login_form", "class" => "cmxform");
            echo form_open('login/login_authenticate', $attributes);
            ?>
            <fieldset>
                <p>
                    <label for="cemail">Username *</label>
                    <input type="text" id="cusername" name="username" data-rule-required="true" data-rule-username="true" data-msg-required="Please enter your username">
                </p>
                <p>
                    <label for="password">Password *</label>
                    <input type="password" id="cpassword" name="password" data-rule-required="true" data-rule-password="true" data-msg-required="Please enter your password" autocomplete="off">
                </p>
                <p>
                    <input class="submit" type="submit" value="Login">
                </p>
            </fieldset>
            <?php echo form_close(); ?>
        </div>    
    </body>
</html>