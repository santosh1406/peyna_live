<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Progress is going on</h2>
        </div>
    </div>
</div>
<section id="content">
    <div class="container">
        <div id="main">
            <div class="row">
                <center>
                    <h1>Something <b class='theme-green-color'>really good</b> is</h1>
                    <h1>coming <b class='theme-green-color'>very soon</b></h1>
                </center>
            </div>
            <br/><br/>
            <div class="row">
                <center>
                    <p>Redirecting in <span id='waiting_timeout'></span></p>
                    <p>
                        <img src="<?php echo base_url().COMMON_ASSETS;?>images/tick_tock_3.gif">
                    </p>
                    <p>
                        <b class='theme-green-color'>
                            <a href="<?php  echo base_url();?>" title="Bookonspot">
                                <?php  echo base_url();?>
                            </a>
                        </b>
                    </p>
                </center>
            </div>
        </div>
    </div>
</section>
<?php
    $current_time = time();
    $timeout_date = date('d-m-Y H:i:s',$current_time + '9 seconds');
    $timeout_str = strtotime($timeout_date);
?>
<script type="text/javascript">
$(document).ready(function(){
    startCountDown(<?php echo date("Y",$timeout_str);?>,<?php echo date("m",$timeout_str);?>,<?php echo date("d",$timeout_str);?>,<?php echo date("H",$timeout_str);?>,<?php echo date("i",$timeout_str);?>,<?php echo date("s",$timeout_str);?>, "waiting_timeout","0", base_url);  
});
</script>