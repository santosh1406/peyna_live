<section class="content-header">
    <h3>Wallet Transaction Retailer Report</h3>
</section>
 <section class="content">
   <div id="wallet_wrapper">
      <div class="row">
         <div class="col-xs-12">
                 <?php $attributes = array("method" => "POST", "id" => "userrpt",  "name" => "userrpt","onsubmit" => 'return false;');
                            echo form_open("#", $attributes); ?>
                <div class="box" style="padding:10px;">
                    <div class="row">
                        <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                <label for="input-text-1">From Date</label>
                                  <input class="form-control date" id="from_date" placeholder="<?php echo date('d-m-Y'); ?> " type="text" >
                             </div>
                        </div>
                        <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                <label for="input-text-2">Till date</label>
                                        <input type="hidden" name="def_to_date" id="def_to_date" value="<?php echo date('Y-m-d'); ?> ">
                                        <input type="hidden" name="def_from_date" id="def_from_date" value="<?php echo date('Y-m-d', strtotime(date('d-m-Y'))); ?>">
                                        <input class="form-control date" id="to_date" placeholder="<?php echo date('d-m-Y'); ?>" type="text">
                            </div>
                        </div>                     
                        
                          
                        </div>     
                       <div class="row"> 
                         <div style="display: block;" class="col-md-1 display-no">
                            <div style="position: static;" class="form-group">
                                <h1></h1>
                                 <button class="btn btn-primary" name="op_generate_btn" id="op_generate_btn" type="submit"><span></span>Search</button>
                            </div>
                         </div>
                         <div style="display: block;" class="col-md-1 display-no">
                            <div style="position: static;" class="form-group">
                                <h1></h1>
                                 <button class="btn btn-primary" name="reset_btn" id="reset_btn" type="reset">Reset</button>
                            </div>
                         </div>
                          <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                <h1></h1>
                                 <button  class="btn btn-primary" id="extoexcel" type="button">
                                   <span class="glyphicon glyphicon-export"></span>
                                   Export to Excel</button>  
                            </div>
                        </div>
                      </div> 
                  </div>
                   <br>  
                       <div class="row">
                            <div class="col-xs-12">
                                 <div class="box">
                                    <div class="box-body" id="table_show">
                                          <table id="op_rept" name="op_rept" class="table table-bordered table-hover">
                                                <thead><tr>
                                                    <th>Sr No</th>
                                                    <th>Txn date</th>
                                                    <th>User Name</th>            
                                                    <th>Agent Code</th>
                                                    <th>Waybill No</th>
                                                    <th>User Wallet Id</th>
                                                    <th>Transaction ID</th>
                                                    <th>Wallet Opening Balance(Rs.)</th>
                                                    <th>Txn Amt (Rs.)</th>
                                                    <th>Wallet Closing Balance(Rs.)</th>
                                                    <th>Txn Type</th>
                                                    <th>Txn Type Description</th>
                                                    </tr>
                                                 <tbody>                                           
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                         </div>
                    </form>
                  </div>   
               </div>
            </div>
      </section>

       <script>
       var data;
       $(document).ready(function() {
   
        var tconfig = {
        "processing": true,
        "serverSide": true,
        "iDisplayLength": 10,
        "ajax": {
            "url": BASE_URL+"report/ors_report/wallet_trans_retailer",
            "type": "POST",
            "data": "json",
            data   : function (d) {
                        var date = new Date();
                        var from_date;
                        var till_date;
                        if ($("#from_date").val() == '') {
                            from_date = $("#def_from_date").val();

                        } else {
                            from_date = $("#from_date").val();
                        }
                        if ($("#to_date").val() == '') {
                            till_date = $("#def_to_date").val();
                        } else {
                            till_date = $("#to_date").val();
                        }
                        d.from_date = from_date;
                        d.to_date = till_date;                       
                                        
                        d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;                       
                     }
        },
        "columnDefs": [
        {
            "searchable": false
        }
        ],
       "scrollX": true,
       "bFilter": false,
       "bPaginate": true,
       "bRetrieve": true,
        "aoColumnDefs": [
       
        {
            "bSortable": false,  
            "aTargets": [3]
        },
       ],
         "fnRowCallback": function(nRow, aData, iDisplayIndex) {
      
           
             $("td:first", nRow).html(iDisplayIndex + 1);
          
            return nRow;
        },
          "oLanguage": {
                    "sProcessing": '<img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>'
                },
    };
       data= $('#op_rept').dataTable(tconfig);          
        $(document).off('click', '#op_generate_btn').on('click', '#op_generate_btn', function (e) {
             
         
         data.fnDraw();       
      });
        $(document).off('click', '#reset_btn').on('click', '#reset_btn', function (e) {
        location.reload(true);
      });
       
       $( "#from_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        maxDate :0
        });
        $( "#to_date" ).datepicker({
            'dateFormat' : 'dd-mm-yy',
            maxDate:0
        });
       
         $('#extoexcel').on('click', function(e) 
            { 
                var from_date;
                var till_date;
                if ($("#from_date").val() == '') {
                    from_date = $("#def_from_date").val();

                } else {
                    from_date = $("#from_date").val();
                }
                if ($("#to_date").val() == '') {
                    till_date = $("#def_to_date").val();
                } else {
                    till_date = $("#to_date").val();
                }
                 from = from_date;
                 to = till_date;       
                
                 txn_type = $("#txn_type option:selected").val();                   

              window.location.href=BASE_URL+'report/ors_report/wallet_trans_retailer_excel?from_date='+from+"&to_date="+to;   
          });
          

    });
          
</script>
