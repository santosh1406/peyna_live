<section class="content-header">
    <h3>Wallet Transaction Report</h3>
</section>


 <section class="content">
   <div id="wallet_wrapper">
      <div class="row">
         <div class="col-xs-12">
                 <?php $attributes = array("method" => "POST", "id" => "userrpt",  "name" => "userrpt","onsubmit" => 'return false;');
                            echo form_open("#", $attributes); ?>
                <div class="box" style="padding:10px;">
                    <div class="row">
                        <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                <label for="input-text-1">From Date</label>
                                  <input class="form-control date" id="from_date" placeholder="<?php echo date('d-m-Y'); ?> " type="text" >
                             </div>
                        </div>
                        <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                <label for="input-text-2">To Date</label>
                                        <input type="hidden" name="def_to_date" id="def_to_date" value="<?php echo date('Y-m-d'); ?> ">
                                        <input type="hidden" name="def_from_date" id="def_from_date" value="<?php echo date('Y-m-d', strtotime(date('d-m-Y'))); ?>">
                                        <input class="form-control date" id="to_date" placeholder="<?php echo date('d-m-Y'); ?>" type="text">
                            </div>
                        </div>
                        <?php if($this->session->userdata('role_id') == SUPPORT_ROLE_ID) {?>
                         <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group"> 
                                <label for="select-1">User Type</label>
                                <select class="form-control chosen_select" id="user_type" name="user_type">
                                    <option value="all">All</option>
                                    <option value="<?php echo TRIMAX_ROLE_ID; ?>">Trimax</option> 
                                    <option value="<?php echo COMPANY_ROLE_ID; ?>">Company</option>
                                    <option value="<?php echo MASTER_DISTRIBUTOR_ROLE_ID; ?>">Regional Distributor</option>
                                    <option value="<?php echo AREA_DISTRIBUTOR_ROLE_ID; ?>">Divisional Distributor</option>
                                    <option value="<?php echo DISTRIBUTOR; ?>">Executive</option>
                                    <option value="<?php echo RETAILER_ROLE_ID; ?>">Sale Agent</option>
                                </select>
                            </div>
                        </div>
                        <?php } ?>
                        <!--<?php $session_level = $this->session->userdata('level');
                            if (empty($session_level)) {
                        ?>
                        <div class="col-md-2 display-no" style="display: none;">
                            <div class="form-group" style="position: static;">
                                <label for="select-1">User Type</label>
                                <select id="user_type" class="form-control" name="user_type">
                                    <option value="" onclick="hide('mydiv')">All</option>
                                    <option value="1" onclick="hide('mydiv')">Rokad</option>
                                    <option value="2" onclick="show('mydiv')">Master Distributor</option>
                                    <option value="3" onclick="show('mydiv')">Area Distributor</option>
                                    <option value="4" onclick="show('mydiv')">Distributor</option>
                                    <option value="5" onclick="show('mydiv')">Retailer</option>
                                </select>
                            </div>   
                        </div>
                        <div class="col-md-2 display-no" style="display: none;" id="mydiv">
                            <div class="form-group" style="position: static;">
                                <label for="select-1">Select Users</label>
                                <select id="select_user" class="form-control chosen_select" name="select_user" >
                                    <option value="">Select All </option>
                                </select>
                            </div>  
                        </div>
                            <?php } ?>-->
                         <div style="display: block;" class="col-md-2 display-no">    
                              <div style="position: static;" class="form-group">
                                   <label for="select-1">Txn Type</label>
                                   <select class="form-control chosen_select" id="txn_type" name="txn_type">
                                                            <option value="">Both</option>
                                        <option value="Credited">Credit</option>
                                        <option value="Debited">Debit</option>
                                   </select>
                              </div>
                         </div>
                          <!-- <div style="display: block;" class="col-md-2 display-no">    
                              <div style="position: static;" class="form-group">
                                   <label for="select-1">Txn Description</label>
                                   <select class="form-control chosen_select" id="txn_desc" name="txn_desc">
                                        <option value="">All</option>
                                         <?php 
                                    //if (isset($trans_desc) && count($trans_desc) > 0 ) { 
                                    //foreach ($trans_desc as $trans_desc){  ?>

                                    <option value="<?php //if(!empty($trans_desc['id'])) echo $trans_desc['id']?>" <?php //if(!empty($trans_desc['id'] && !empty($select_user)) && $select_user == $trans_desc['id']) echo "selected";?>>
                                        <?php //if(!empty($trans_desc['title'])) echo $trans_desc['title']?>
                                    </option>
                                        <?php //}
                                        //} ?>
                                   </select>
                              </div>
                         </div> -->
                         <div style="display: block;" class="col-md-2 display-no">    
                              <div style="position: static;" class="form-group">
                                   <label for="select-1">Topup Mode</label>
                                   <select class="form-control chosen_select" id="txn_mode" name="txn_mode">
                                        <option value="">All</option>
                                        <option value="Cash">Cash</option>
                                        <option value="Cheque">Cheque</option>
                                        <option value="Dd">DD</option>
                                        <?php if($this->session->userdata('role_id') != TRIMAX_ROLE_ID) {?>
                                        <option value="PG">Payment gateway</option>
                                        <?php }?>
                                   </select>
                                  <!-- <p class="help-block">Example block-level help text here.</p> -->
                              </div>
                         </div>  
                        </div>     
                       <div class="row"> 
                         <div style="display: block;" class="col-md-1 display-no">
                            <div style="position: static;" class="form-group">
                                 <button class="btn btn-primary" name="op_generate_btn" id="op_generate_btn" type="submit"><span></span>Search</button>
                            </div>
                         </div>
                         <div style="display: block;" class="col-md-1 display-no">
                            <div style="position: static;" class="form-group">
                                 <button class="btn btn-primary" name="reset_btn" id="reset_btn" type="reset">Reset</button>
                            </div>
                         </div>
                          <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                 <button  class="btn btn-primary" id="extoexcel" type="button">
                                   <span class="glyphicon glyphicon-export"></span>
                                   Export to Excel</button>  
                            </div>
                        </div>
                      </div> 
                  </div>
                   <br>  
                       <div class="row">
                            <div class="col-xs-12">
                                 <div class="box">
                                    <div class="box-body" id="table_show">
                                          <table id="op_rept" name="op_rept" class="table table-bordered table-hover">
                                                <thead><tr>
                                                    <th>Sr. No.</th>
                                                    <th>Txn Date</th>
                                                    <th>User Name</th>
                                                    <!-- <th>User Id</th> -->
                                                    <th>User Type</th>
                                                    <!-- <th>User Wallet Id</th> -->
                                                    <th>Transaction Id</th>
                                                    <th>Wallet Opening Balance(Rs.)</th>
                                                    <th>Txn Amt (Rs.)</th>
                                                    <th>Wallet Closing Balance(Rs.)</th>
                                                    <th>Txn Type</th>
                                                    <th>Txn Type Description</th>
                                                    <!-- <th>Rokad Ref No.</th>
                                                    <th>Ticket Status</th> -->
                                                    <th>Topup Mode</th>
                                                    <th>PG Tracking Id</th>
                                                    <th>Topup Transaction Date</th>
                                                    <th>Req Added Date</th>
                                                    </tr>
                                                 <tbody>                                           
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                         </div>
                    </form>
                  </div>   
               </div>
            </div>
      </section>

       <script>
       var oData;
       $(document).ready(function() {
   
        var tconfig = {
        "processing": true,
        "serverSide": true,
        "iDisplayLength": 10,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        "ajax": {
            "url": BASE_URL+"report/ors_report/wallet_trans",
            "type": "POST",
            "data": "json",
            data   : function (d) {
                        var date = new Date();
                        var from_date;
                        var till_date;
                        if ($("#from_date").val() == '') {
                            from_date = $("#def_from_date").val();

                        } else {
                            from_date = $("#from_date").val();
                        }
                        if ($("#to_date").val() == '') {
                            till_date = $("#def_to_date").val();
                        } else {
                            till_date = $("#to_date").val();
                        }
                        d.from_date = from_date;
                        d.to_date = till_date;
                        d.select_user = $("#select_user option:selected").val();
                        d.user_type = $("#user_type option:selected").val();
                        d.txn_type = $("#txn_type option:selected").val();
                        d.txn_desc = $("#txn_desc option:selected").val();
                        d.txn_mode = $("#txn_mode option:selected").val();
                        d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;                       
                     }
        },
        "columnDefs": [
        {
            "searchable": false
        }
        ],
        "order": [[3, "desc"]],
       "scrollX": true,
       "searching": false,
       "bFilter": false,
       "bPaginate": true,
       "bRetrieve": true,
        "aoColumnDefs": [
       
        {
            "bSortable": false,  
            "aTargets": [0]
        },
       ],
         "fnRowCallback": function(nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:first", nRow).html(info.start + iDisplayIndex + 1);
             // $("td:first", nRow).html(iDisplayIndex + 1);
            return nRow;
        },
        fnDrawCallback: function(data)
        {

            if (oData.fnGetData().length == 0)
            {
                $("#extoexcel").attr("disabled", "disabled"); 

            } else {
                 $("#extoexcel").removeAttr("disabled"); 

            }
        },
          "oLanguage": {
                    "sProcessing": '<img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>'
                },
    };
       oData= $('#op_rept').dataTable(tconfig);          
        $(document).off('click', '#op_generate_btn').on('click', '#op_generate_btn', function (e) {
             
         
         oData.fnDraw();       
      });
        $(document).off('click', '#reset_btn').on('click', '#reset_btn', function (e) {
        location.reload(true);
      });
       
        $("#from_date" ).datepicker({
            'dateFormat' : 'dd-mm-yy',
            maxDate :0
        }).on("change", function () {
            $('#to_date').datepicker('option', 'minDate', $("#from_date").val());
        });
        
        $( "#to_date" ).datepicker({
            'dateFormat' : 'dd-mm-yy',
            maxDate:0,
            minDate: $("#from_date").val()
        }).on("change", function () {
             $('#from_date').datepicker('option', 'maxDate', $("#to_date").val());
        });
        
       
         $('#extoexcel').on('click', function(e) 
            { 
                var from_date;
                var till_date;
                if ($("#from_date").val() == '') {
                    from_date = $("#def_from_date").val();

                } else {
                    from_date = $("#from_date").val();
                }
                if ($("#to_date").val() == '') {
                    till_date = $("#def_to_date").val();
                } else {
                    till_date = $("#to_date").val();
                }
                 from = from_date;
                 to = till_date;       
                 select_user = $("#select_user option:selected").val();
                 user_type = $("#user_type option:selected").val();
                 if(!(select_user)){
                     select_user = '';
                 }
                 if(!(user_type)){
                     user_type = '';
                 }
                 txn_type = $("#txn_type option:selected").val();
                 txn_desc = $("#txn_desc option:selected").val();
                 txn_mode = $("#txn_mode option:selected").val();     

              window.location.href=BASE_URL+'report/ors_report/wallet_trans_excel?from_date='+from+"&to_date="+to+"&select_user="+select_user+"&txn_type="+txn_type+"&txn_desc="+txn_desc+"&txn_mode="+txn_mode+"&user_type="+user_type;   
          });
            $(document).off('change', '#user_type').on('change', '#user_type', function(e) {
                e.preventDefault();
                $("#select_user").html("");
                var elem = $(this);
                var detail = {};
                var form = '';
                var div = '';
                detail['level'] = $("#user_type").find('option:selected').attr('value');
                var ajax_url = "report/ors_report/user_type_view";
                get_data(ajax_url, form, div, detail, function(response) {
                    var k = 1;
                    str = "";
                    $("#select_user").append("<option value=''>Select User</option>");
                    $.each(response, function(index, value) {
                        $('#example tbody').html('');
                        $("#select_user").append("<option value=" + value.id + ">" + value.display_name + "</option>");
                    });
                    $("#select_user").trigger("chosen:updated");
                });
            });

    });
            function show(target) {
            document.getElementById(target).style.display = 'block';
            }

            function hide(target) {
            document.getElementById(target).style.display = 'none';
            }
</script>
