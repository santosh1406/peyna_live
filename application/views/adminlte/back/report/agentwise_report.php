
<style>
    label.error, div.error, ul.error, span.error {
        color: #f00;
        font-size: 14px;
    }
</style>
<section class="content-header">
    <h3>Waybill Wise Commission Report</h3>
</section>

<section class="content">
    <div id="wallet_wrapper">
        <div class="row">
            <div class="col-xs-12">
                <?php $attributes = array("method" => "POST", "id" => "userrpt", "name" => "userrpt", "onsubmit" => 'return false;');
                echo form_open("#", $attributes);
                ?>
                <div class="box" style="padding:10px;">
                    <div class="row">
                        <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                <label>From Date</label>
                                <input type="text" class="form-control" id="from_date" name="from_date" placeholder="<?php echo date('d-m-Y'); ?> " >
                            </div>
                        </div> 
                        <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                <label>To Date</label>
                                <input type="hidden" name="def_to_date" id="def_to_date" value="<?php echo date('Y-m-d'); ?> ">
                                <input type="hidden" name="def_from_date" id="def_from_date" value="<?php echo date('Y-m-d', strtotime(date('d-m-Y'))); ?>">
                                <input type="text" class="form-control" id="to_date" name="to_date"  placeholder="<?php echo date('d-m-Y'); ?>" >
                            </div>
                        </div>  
                    </div>
                    <div class="row"> 
                        <div style="display: block;" class="col-md-1 display-no">
                            <div style="position: static;" class="form-group">
                                <button class="btn btn-primary" name="op_generate_btn" id="op_generate_btn" type="submit">Search</button>
                            </div>
                        </div>
                        <div style="display: block;" class="col-md-1 display-no">
                            <div style="position: static;" class="form-group">
                                <button class="btn btn-primary" name="reset_btn" id="reset_btn" type="reset">Reset</button>
                            </div>
                        </div>
                        <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                <button  class="btn btn-primary" id="extoexcel" type="button">
                                    <span class="glyphicon glyphicon-export"></span>Export To Excel</button>  


                            </div>
                        </div>
                    </div>
                </div>  
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-body" id="table_show">
                                <table id="op_rept" name="op_rept" class="table table-striped">
                                    <thead><tr>
                                            <th>Sr. No.</th>
                                            <th>Commission Calculate Date</th>
                                            <th>Agent Code</th>
                                            <th>Waybill No</th>
                                            <th>Application Amount (Rs.)</th>
                                            <th>Commission Percentage</th>
                                            <th>Commission Amount (Rs.)</th>
                                            <th>Tds Percentage</th>
                                            <th>Tds Value</th>
                                            <th>Net Commission Amount (Rs.)</th>
                                        </tr>
                                        <tbody>                                           
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>   
<?php echo form_close(); ?>
            </div>   
        </div>

</section>
<script>
    var tkt_data;
    $(document).ready(function() {

        var tconfig = {
            "processing": true,
            "serverSide": true,
            "searching": true,
            "iDisplayLength": 25,
            "aLengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            "order": [[1, "desc"]],
            "ajax": {
                "url": BASE_URL + "report/report/agent_wise_data",
                "type": "POST",
                "data": "json",
                data   : function(d) {
//        d.from_date   =  $("#from_date").val();
//        d.to_date     =  $("#to_date").val();
                    var date = new Date();
                    var from_date;
                    var to_date;
                    if ($("#from_date").val() == '') {
                        from_date = $("#def_from_date").val();
                    } else {
                        from_date = $("#from_date").val();
                    }
                    if ($("#to_date").val() == '') {
                        to_date = $("#def_to_date").val();
                    } else {
                        to_date = $("#to_date").val();
                    }
                    d.from_date = from_date;
                    d.to_date = to_date;
                    d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;
                }
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                var info = $(this).DataTable().page.info();
                $("td:first", nRow).html(info.start + iDisplayIndex + 1);
                return nRow;

            },
            fnDrawCallback: function(data)
            {

                if (tkt_data.fnGetData().length == 0)
                {
                    $("#extoexcel").attr("disabled", "disabled");

                } else {
                    $("#extoexcel").removeAttr("disabled");

                }
            },
            "columnDefs": [
                {
                    "searchable": false
                }
            ],
            "scrollX": true,
            "bFilter": false,
            "bPaginate": true,
            "bRetrieve": true,
            "aoColumnDefs": [
                {
                    "bSortable": false,
                    "aTargets": [0]
                },
                {
                    "bSortable": false,
                    "aTargets": [3]
                },
                {
                    "bSortable": false,
                    "aTargets": [4]
                },
                {
                    "bSortable": false,
                    "aTargets": [5]
                },
                {
                    "bSortable": false,
                    "aTargets": [6]
                },
            ],
            "oLanguage": {
                "sProcessing": '<center><img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/></center>'
            },
        };

        tkt_data = $('#op_rept').dataTable(tconfig);

        $(document).off('click', '#op_generate_btn').on('click', '#op_generate_btn', function(e) {
            tkt_data.fnDraw();
        });


        $('.datesel').datepicker({
            dateFormat: 'dd-mm-yy'
        });

        $.validator.addMethod("checkDepend",
                function(value, element) {
                    if ($("#from_date").val() != "" && $("#to_date").val() != "")
                    {
                        var date_diff = date_operations($("#from_date").val(), $("#to_date").val());
                        if (date_diff.date_compare)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return true;
                    }

                }, "To date must be greater than from date.");

        $('#userrpt').validate({
            ignore: "",
            rules: {
                to_date: {
                    checkDepend: true
                }
            },
            messages: {
                from_date: {
                }
            }
        });
        $('#extoexcel').on('click', function(e)
        {

            if ($("#from_date").val() == '') {
                from_date = $("#def_from_date").val();
            } else {
                from_date = $("#from_date").val();
            }
            if ($("#to_date").val() == '') {
                to_date = $("#def_to_date").val();
            } else {
                to_date = $("#to_date").val();
            }
            window.location.href = BASE_URL + 'report/report/agent_wise_excel?from_date=' + from_date + "&to_date=" + to_date;
        });

        //post the data to report

        $("#from_date").datepicker({
            'dateFormat': 'dd-mm-yy',
            maxDate: 0
        });
        $("#to_date").datepicker({
            'dateFormat': 'dd-mm-yy',
            maxDate: 0
        });

        $(document).off('click', '#reset_btn').on('click', '#reset_btn', function(e) {
            location.reload(true);
        });







    });

</script>