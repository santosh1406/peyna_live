<section class="content-header">
    <h3>Fund Transfer Report</h3>
</section>

 <section class="content">
   <div id="wallet_wrapper">
      <div class="row">
         <div class="col-xs-12">
             <form  onsubmit='return false;' name="userrpt" id="userrpt">
                <div class="box" style="padding:10px;">
                    <div class="row">
                          <div style="display: block;" class="col-md-2 display-no">
                              <div style="position: static;" class="form-group">
                                  <label for="input-text-1">From Date</label>
                                  <input type="text" class="form-control" id="from_date" name="from_date" placeholder="From Date" value="<?php echo set_value('from_date'); ?>" >
                               </div>
                          </div>
                          <div style="display: block;" class="col-md-2 display-no">
                              <div style="position: static;" class="form-group">
                                  <label for="input-text-2">To Date</label>
                                   <input type="text" class="form-control" id="to_date" name="to_date" placeholder="Till Date" value="<?php echo set_value('to_date'); ?>" >
                              </div>
                           </div>
                           <div style="display: block;" class="col-md-2 display-no">    
          		               <div style="position: static;" class="form-group">
          		                  <label for="select-1">User Type</label>
              		                 <select class="form-control" id="user_type" name="user_type">
                  			                <option value="">All</option>
                  			                <option value="4">Master Distributor</option>
                  			                <option value="4">Area Distributor</option>
                  		                  <option value="6">Distributor Distributor</option>
                                        <option value="7">Retailer</option>
              			               </select>
          		                 </div>
                           </div>
                       </div>
                       <div class="box-footer2">
                          <div >
                                  <button class="btn btn-primary" name="op_generate_btn" id="op_generate_btn" type="submit">Generate Report</button>&nbsp;&nbsp;
                                  <button  class="btn btn-primary" id="extoexcel" type="button">
                                  <span class="glyphicon glyphicon-export"></span>
                                  Export</button>     
                          </div>
                      </div>     
                </div>
                   <br>  
                       <div class="row">
                            <div class="col-xs-12">
                                 <div class="box">
                                    <div class="box-body table-responsive" id="table_show">
                                          <table id="op_rept" name="op_rept" class="table table-striped">
                                                <thead><tr>
                                                    <th>Sr No</th>
                                                    <th>transfer date</th>
                                                    <th>User Name</th>
                                                    <th>user Code</th>
                                                    <th>trasfer from</th>
                                                    <th>transfer amount</th>
                                                    <th>trasnfer to</th> 
                                                 <tbody>
                                                   
                                                 </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                         </div>
                    </form>
                  </div>   
               </div>
            </div>
      </section>

      <script>
       var agent_data;
       $(document).ready(function() {
   

  
             var tconfig = {
        // "sDom": '<"toolbar">hlfetlp',
        "processing": true,
        "serverSide": true,
        "iDisplayLength": 50,
		"aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
         "order": [[1, "desc"]],
        "ajax": {
            "url": BASE_URL+"report/report/fund_transfer_list",
            "type": "POST",
            "data": "json",
            data   : function (d) {
                        d.from_date    =  $("#from_date").val();
                        d.to_date      =  $("#to_date").val();
                        d.user_type    =  $("#user_type").val();
						d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;
                     }
        },
        "columnDefs": [
        {
            "searchable": false
        }
        ],
       "scrollX": true,
       "bFilter": false,
       "bPaginate": true,
       "bRetrieve": true,
         "aoColumnDefs": [
       
        {
            "bSortable": false,  
            "aTargets": [0]
        },
        {
            "bSortable": false,  
            "aTargets": [2]
        },
       {
            "bSortable": false,  
            "aTargets": [4]
        },
       {
            "bSortable": false,  
            "aTargets": [5]
        },
       {
            "bSortable": false,  
            "aTargets": [6]
        },
       
       
       
         ],
            "oLanguage": {
                    "sProcessing": '<img src="<?php echo base_url().BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>' 
                    },
         "fnRowCallback": function(nRow, aData, iDisplayIndex) {
      
           
             $("td:first", nRow).html(iDisplayIndex + 1);
          
            return nRow;
          
        },
    };
       agent_data= $('#op_rept').dataTable(tconfig);          
        $(document).off('click', '#op_generate_btn').on('click', '#op_generate_btn', function (e) {
             
         
         agent_data.fnDraw();       
      });
       
        $( "#from_date" ).datepicker({
                        dateFormat : 'dd-mm-yy',
                        maxDate:0
         });
          
         $( "#to_date" ).datepicker({
                        dateFormat : 'dd-mm-yy',
                        maxDate:0
         });

         $('.datesel').datepicker({
                dateFormat: 'dd-mm-yy'
        });
    
  //validate both  dates
          $.validator.addMethod("checkDepend",
                function(value, element) {
                if($("#from_date").val() != "" &&  $("#to_date").val() != "")
                {
                    var date_diff = date_operations($("#from_date").val(),$("#to_date").val());
                    if(date_diff.date_compare)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }

            },"To date must be greater than from date.");
  
             $('#userrpt').validate({
                ignore: "",
                rules: {
                    
                    to_date : {
                        checkDepend : true
                    }
                },
                messages: {
                    
                    from_date: {
          
                    
                    }
                }
            });
         
          $('#extoexcel').on('click', function(e) 
            { 
            
              from_date  = $("#from_date").val();
              to_date    = $("#to_date").val();
              role_id    = $("#user_type").find('option:selected').attr('value');
              user_id    = $("#select_user").find('option:selected').attr('value');
              select_state = $("#select_state").find('option:selected').attr('value');
              select_city = $("#select_city").find('option:selected').attr('value');
              
            /*  if(from_date==''){
                  var from_date = $("#from_date").attr('placeholder');  
              }
              if(to_date==''){
                  var to_date = $("#to_date").attr('placeholder');  
              }*/
              window.location.href=BASE_URL+'report/report/agent_registration_report_excel?from_date='+from_date+"&to_date="+to_date+"&role_id="+role_id+"&user_id="+user_id;   
            });


   
             $( "#from_date" ).datepicker({
                'dateFormat' : 'dd-mm-yy',
            });
            $( "#to_date" ).datepicker({
                'dateFormat' : 'dd-mm-yy',
            });

             $("#from_date").datepicker({
            numberOfMonths: 1,
            onSelect: function (selected)
            {
                var dt = new Date(selected);
                dt.setDate(dt.getDate() + 1);
                $("#to_date").datepicker("option", "minDate", dt);
                format: 'dd-mm-yyyy';
            }
        });

        $("#to_date").datepicker({
            numberOfMonths: 1,
            onSelect: function (selected)
            {
                var dt = new Date(selected);
                dt.setDate(dt.getDate() - 1);
                $("#from_date").datepicker("option", "maxDate", dt);
                format: 'dd-mm-yyyy';
            }
        });

    });


    $(document).off('change', '#user_type').on('change', '#user_type', function(e) {
                e.preventDefault();
                $("#select_user").html("");
                var elem = $(this);
                var detail = {};
                var form = '';
                var div = '';
                detail['role_id'] = $("#user_type").find('option:selected').attr('value');
                var ajax_url = "report/report/agent_type_view";

                get_data(ajax_url, form, div, detail, function(response)
                {

                    var k = 1;
                    str = "";
                     $("#select_user").append("<option >Select User</option>");
                    $.each(response, function(index, value) {
                        $('#example tbody').html('');
                        
                        $("#select_user").append("<option value=" + value.id + ">" + value.user_name + "</option>");
                    });
                    $("#select_user").trigger("chosen:updated");

                });

            });
</script>