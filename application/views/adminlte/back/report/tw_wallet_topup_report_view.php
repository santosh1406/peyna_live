<style>
    .overlay, .loading-img {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    .overlay {
        z-index: 1010;
        background: rgba(255, 255, 255, 0.7);
    }
    .overlay.dark {
        background: rgba(0, 0, 0, 0.5);
    }
</style>

<section class="content-header">
    <h1>Travel Wallet Topup Report</h1>
</section>

<!-- Main content -->
<section class="content">
    <div id="commission_wrapper">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body" id="table_show">

                        <?php
                        $attributes = array('class' => 'form', 'id' => 'trans_list_filter', 'name' => 'trans_list_filter', 'method' => 'post', 'onsubmit' => 'return false;');
                        echo form_open('#', $attributes);
                        
                        
                        ?>
                        <div class="row box-title">
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-1">From Date</label>
                                    <input class="form-control date" id="from_date" placeholder="<?php echo date('d-m-Y'); ?> " value="<?php if (!empty($from_date)) echo $from_date; ?>" type="text" >
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2">To Date</label>
                                    
                                    <input class="form-control date" id="to_date" placeholder="<?php echo date('d-m-Y'); ?>" value="<?php if (!empty($till_date)) echo $till_date; ?>" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        
                        <div class="row"> 
                            <div style="display: block;" class="col-md-1 display-no">
                                <div style="position: static;" class="form-group">
                                    <button class="btn btn-primary" name="card_generate_btn" id="card_generate_btn"  type="submit"><span></span>Search</button>
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-1 display-no">
                                <div style="position: static;" class="form-group">
                                    <button class="btn btn-primary" name="reset_btn" id="reset_btn" type="reset">Reset</button>
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <button  class="btn btn-primary" id="export_to_excel" type="button">
                                        <span class="glyphicon glyphicon-export"></span>
                                        Export to Excel</button>  
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                    <div style="display: none" id="loader" class="overlay"><i class="fa fa-refresh fa-spin"></i></div>
                    <table id="card_data" name="card_data" class="table table-bordered table-hover">
                        <thead>
                                            <tr>
                                                <th>Sr. No.</th>
                                                <th>Agent Code</th>     
                                                <th>Agent Name</th>
                                                <th>Depot Code</th>
                                                <th>Depot Name</th>
                                                <th>Card Id</th>
                                                <th>Card Holder name</th>
                                                <th>Top Up Date</th>
                                                <th>Top Up Amount</th>
                                                <th>Loading Fee</th>
                                                <th>GST on Banking Fee</th>
                                                <th>Load Amount after Fee and GST</th>
                                                <th>Bonus Point</th>
                                                <th>Actual Load Amount</th>
                                                <th>Wallet Balance Before Topup</th>
                                                <th>Wallet Balance After Topup</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $total_tp_amt = $total_lf_amt = $total_gst_amt = $total_load_amt = $total_bonus_amt = $total_actual_amt = $total_op_bal = $total_cl_bal = 0.00;?>
                                        <?php 
                                            foreach ($report_data as $key => $value) {
                                                $load_amt_gst_bfee = ($value['topup_amt'] - $value['loading_fee'] - $value['gst_fee']);
                                        ?>
                                            <tr>
                                                <td><?php echo ++$key; ?></td>
                                                <td><?php echo $value['agent_code']; ?></td>
                                                <td><?php echo $value['agent_name']; ?></td>
                                                <td><?php echo $value['DEPOT_CD']; ?></td>
                                                <td><?php echo $value['DEPOT_NM']; ?></td>
                                                <td><?php echo $value['cust_card_id']; ?></td>
                                                <td><?php echo $value['holder_name']; ?></td>
                                                <td><?php echo $value['topup_date']; ?></td>
                                                <td><?php echo $value['topup_amt']; ?></td>
                                                <td><?php echo $value['loading_fee']; ?></td>
                                                <td><?php echo $value['gst_fee']; ?></td>
                                                <td><?php echo $load_amt_gst_bfee; ?></td>
                                                <td><?php echo $value['bonus_fee']; ?></td>
                                                <td><?php echo $value['load_amt']; ?></td>
                                                <td><?php echo $value['bal_before_topup']; ?></td>
                                                <td><?php echo $value['bal_after_topup']; ?></td>
                                            </tr>
                                            <?php 
                                                $total_tp_amt += $value['topup_amt'];
                                                $total_lf_amt += $value['loading_fee'];
                                                $total_gst_amt += $value['gst_fee'];
                                                $total_load_amt += $load_amt_gst_bfee;
                                                $total_bonus_amt += $value['bonus_fee'];
                                                $total_actual_amt += $value['load_amt'];
                                                $total_op_bal += $value['bal_before_topup'];
                                                $total_cl_bal += $value['bal_after_topup'];
                                            ?>
                                        <?php } ?>
                                    </tbody>
                                        <tfoot>
                                        <tr>
                                            <th colspan="4" style="text-align:right"> Total : </th>
                                            <td><?php echo number_format($total_tp_amt, 2, '.', '');?></td>
                                            <td><?php echo number_format($total_lf_amt, 2, '.', '');?></td>
                                            <td><?php echo number_format($total_gst_amt, 2, '.', '');?></td>
                                            <td><?php echo number_format($total_load_amt, 2, '.', '');?></td>
                                            <td><?php echo number_format($total_bonus_amt, 2, '.', '');?></td>
                                            <td><?php echo number_format($total_actual_amt, 2, '.', '');?></td>
                                            <td><?php echo number_format($total_op_bal, 2, '.', '');?></td>
                                            <td><?php echo number_format($total_cl_bal, 2, '.', '');?></td>
                                        </tr>
                                    </tfoot>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</section>


<script>

$(document).ready(function() {
    
    $('input#from_date, input#to_date').bind('copy paste', function(e) {
        e.preventDefault();
    });

    $('#topup_data_filter input').unbind();

    $('#topup_data_filter input').bind('keypress', function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            oData.fnFilter(this.value);
        }
    });

    $(document).off('click', '#reset_btn').on('click', '#reset_btn', function(e) {
        location.reload(true);
    });

    $("#from_date").datepicker({
          'dateFormat': 'dd-mm-yy',
          autoclose: true,     
    });

    $("#to_date").datepicker({
          'dateFormat': 'dd-mm-yy',
          autoclose: true,
    });

    function createDatatable() {
        var datatable = $('#card_data').DataTable({
            "scrollX": true
        });
        datatable.draw();
    }

    createDatatable();
    
   $('#export_to_excel').on('click', function(e) {
        var from_date = $("#from_date").val();
        var to_date = $("#to_date").val();
        
        var flag = 'tw_topup_report'; 
        window.location.href = BASE_URL + "report/smart_card_report/tw_topup_excel?flag="+flag+"&from_date="+from_date+"&to_date="+to_date; 
    });

   $(document).off('click', '#card_generate_btn').on('click', '#card_generate_btn', function (e) {
        document.getElementById("loader").style.display = "block";
        $('#card_generate_btn').prop('disabled', true);
        var from_date = $("#from_date").val();
        var to_date = $("#to_date").val();
        

        $.ajax({
            "type": "POST",
            "url": BASE_URL + "report/smart_card_report/tw_topup_list",
            "data": {from_date:from_date,to_date:to_date},
            success: function (strJSON) {

                var objJSON = JSON.parse(strJSON);
                var row;
                var i = 0;
                var total_tp_amt = 0.00;
                var total_lf_amt = 0.00;
                var total_gst_amt = 0.00;
                var total_load_amt = 0.00;
                var total_bonus_amt = 0.00;
                var total_actual_amt = 0.00;
                var total_op_bal = 0.00;
                var total_cl_bal = 0.00;

                $('#card_data').DataTable().destroy();
                $('#card_data tbody').empty();
                $('#card_data tfoot tr').empty();
                $.each(objJSON['data'], function(index, value) {
                    i++;
                    var load_amt = (value['topup_amt'] - value['loading_fee'] - value['gst_fee']);

                    row += ("<tr><td>"+i+"</td><td>"+value['agent_code']+"</td><td>"+value['agent_name']+"</td><td>"+value['DEPOT_CD']+"</td><td>"+value['DEPOT_NM']+"</td><td>"+value['cust_card_id']+"</td><td>"+value['holder_name']+"</td><td>"+value['topup_date']+"</td><td>"+value['topup_amt']+"</td><td>"+value['loading_fee']+"</td><td>"+value['gst_fee']+"</td><td>"+load_amt+"</td><td>"+value['bonus_fee']+"</td><td>"+value['load_amt']+"</td><td>"+value['bal_before_topup']+"</td><td>"+value['bal_after_topup']+"</td></tr>");

                    total_tp_amt = parseFloat(value['topup_amt']) + parseFloat(total_tp_amt);
                    total_lf_amt = parseFloat(value['loading_fee']) + parseFloat(total_lf_amt);
                    total_gst_amt = parseFloat(value['gst_fee']) + parseFloat(total_gst_amt);
                    total_load_amt = parseFloat(load_amt) + parseFloat(total_load_amt);
                    total_bonus_amt = parseFloat(value['bonus_fee']) + parseFloat(total_bonus_amt);
                    total_actual_amt = parseFloat(value['load_amt']) + parseFloat(total_actual_amt);
                    total_op_bal = parseFloat(value['bal_before_topup']) + parseFloat(total_op_bal);
                    total_cl_bal = parseFloat(value['bal_after_topup']) + parseFloat(total_cl_bal);
                });
                var tfooter = "<th colspan='4' style='text-align:right'> Total : </th><td>"+total_tp_amt.toFixed(2)+"</td><td>"+total_lf_amt.toFixed(2)+"</td><td>"+total_gst_amt.toFixed(2)+"</td><td>"+total_load_amt.toFixed(2)+"</td><td>"+total_bonus_amt.toFixed(2)+"</td><td>"+total_actual_amt.toFixed(2)+"</td><td>"+total_op_bal.toFixed(2)+"</td><td>"+total_cl_bal.toFixed(2)+"</td>";
                $('#card_data tbody').append(row);
                $('#card_data tfoot tr').append(tfooter);
                createDatatable();
                // if(strJSON != ''){
                //     $('#depoDetails').show();
                //     $('#depoDetails').empty();
                //     var dt = "<p><b> Depot Code</b> : <?php echo $_SESSION['location_cd'] ?> </p>"+
                //     "<p><b>Running Date & Time</b> : <?php echo date('d-m-Y h:i:s a'); ?></p>";
                //     $('#depoDetails').append(dt);
                // }
                document.getElementById("loader").style.display = "none";
                $('#card_generate_btn').prop('disabled', false);
            }
        });
    });

    
});
</script>



