<section class="content-header">
    <h1>Transaction Details</h1>
</section>

<!-- Main content -->
<section class="content">
    <div id="commission_wrapper">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body" id="table_show">

                        <?php
                        $attributes = array('class' => 'form', 'id' => 'trans_list_filter', 'name' => 'trans_list_filter', 'method' => 'post', 'onsubmit' => 'return false;');
                        echo form_open('', $attributes);
                        ?>
                        <div class="row box-title">
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-1">From Date</label>
                                    <input class="form-control date" id="from_date" placeholder="<?php echo date('d-m-Y'); ?> " value="<?php if (!empty($from_date)) echo $from_date; ?>" type="text" >
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2">To Date</label>
                                    <input type="hidden" name="def_to_date" id="def_to_date" value="<?php echo date('d-m-Y'); ?> ">
                                    <input type="hidden" name="def_from_date" id="def_from_date" value="<?php echo date('d-m-Y'); ?> ">
                                    <input class="form-control date" id="to_date" placeholder="<?php echo date('d-m-Y'); ?>" value="<?php if (!empty($till_date)) echo $till_date; ?>" type="text">
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group"> 
                                    <label for="select-1">Transaction Type</label>
                                    <select class="form-control" id="transaction_type" name="transaction_type">                                   
                                        <option value="">All</option>
                                        <?php                                         
                                        foreach ($transaction_type as $key => $value) {
                                            echo "<option value='" . $key . "'>" . $value . "</option>";
                                        }                                        
                                        ?>
                                    </select>
                                </div>
                            </div> 
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        
                        <div class="row"> 
                            <div style="display: block;" class="col-md-1 display-no">
                                <div style="position: static;" class="form-group">
                                    <button class="btn btn-primary" name="search" id="search"  type="submit"><span></span>Search</button>
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-1 display-no">
                                <div style="position: static;" class="form-group">
                                    <button class="btn btn-primary" name="reset_btn" id="reset_btn" type="reset">Reset</button>
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <button  class="btn btn-primary" id="extoexcel" type="button">
                                        <span class="glyphicon glyphicon-export"></span>
                                        Export to Excel</button>  
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>

                    <table id="tran_list" name="tran_list" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th  width="10%">Sr.No</th>
                                <th>Client Id</th>   
                                <th>Agent Name</th> 
                                <th>Mobile No</th> 
                                <th>Transaction Mode</th>
                                <th>Account Number</th>                                
                                <th>Transaction Amount</th> 
                                <th>Available Balance</th>
                                <th>Transaction Date</th>                                
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</section>


<script>
    var oData;
    $(document).ready(function() {

        var tconfig = {
            "processing": true,
            "serverSide": true,
            "iDisplayLength": 10,
            "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
            "ajax": {
                "url": BASE_URL + "report/micro_atm_report/transaction",
                "type": "POST",
                "data": "json",
                data   : function(d) {
                    var date = new Date();
                    var from_date;
                    var till_date;
                    if ($("#from_date").val() == '') {
                        from_date = $("#def_from_date").val();

                    } else {
                        from_date = $("#from_date").val();
                    }
                    if ($("#to_date").val() == '') {
                        till_date = $("#def_to_date").val();
                    } else {
                        till_date = $("#to_date").val();
                    }
                    d.from_date = from_date;
                    d.to_date = till_date;                   
                    d.transaction_type = $("#transaction_type option:selected").val();

                    //  d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;
                }
            },
            "columnDefs": [
                {
                    "searchable": false
                }
            ],
            "order": [[3, "desc"]],
            "scrollX": true,
            "searching": false,
            "bFilter": false,
            "bPaginate": true,
            "bRetrieve": true,
            "aoColumnDefs": [
                {
                    "bSortable": false,
                    "aTargets": [0]
                },
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                var info = $(this).DataTable().page.info();
                $("td:first", nRow).html(info.start + iDisplayIndex + 1);
                // $("td:first", nRow).html(iDisplayIndex + 1);
                return nRow;
            },
            fnDrawCallback: function(data)
            {

                if (oData.fnGetData().length == 0)
                {
                    $("#extoexcel").attr("disabled", "disabled");

                } else {
                    $("#extoexcel").removeAttr("disabled");

                }
            },
            "oLanguage": {
                "sProcessing": '<img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>'
            },
        };
        oData = $('#tran_list').dataTable(tconfig);
        $(document).off('click', '#search').on('click', '#search', function(e) {
            oData.fnDraw();
        });
        $(document).off('click', '#reset_btn').on('click', '#reset_btn', function(e) {
            location.reload(true);
        });

        $("#from_date").datepicker({
            'dateFormat': 'dd-mm-yy',
            maxDate: 0
        }).on("change", function() {
            $('#to_date').datepicker('option', 'minDate', $("#from_date").val());
        });

        $("#to_date").datepicker({
            'dateFormat': 'dd-mm-yy',
            maxDate: 0,
            minDate: $("#from_date").val()
        }).on("change", function() {
            $('#from_date').datepicker('option', 'maxDate', $("#to_date").val());
        });


        $('#extoexcel').on('click', function(e)
        {
            var from_date;
            var till_date;
            if ($("#from_date").val() == '') {
                from_date = $("#def_from_date").val();

            } else {
                from_date = $("#from_date").val();
            }
            if ($("#to_date").val() == '') {
                till_date = $("#def_to_date").val();
            } else {
                till_date = $("#to_date").val();
            }
            from = from_date;
            to = till_date;
            transaction_type = $("#transaction_type option:selected").val();
            window.location.href = BASE_URL + 'report/micro_atm_report/matm_reports_excel?from_date=' + from + "&to_date=" + to + "&transaction_type=" + transaction_type;
        });

    });
    function show(target) {
        document.getElementById(target).style.display = 'block';
    }

    function hide(target) {
        document.getElementById(target).style.display = 'none';
    }
</script>



