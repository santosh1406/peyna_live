<script type="text/Javascript" src="<?php echo base_url() . COMMON_ASSETS ?>js/jquery.validate.min.js" ></script>

<script type="text/Javascript" src="<?php echo base_url() ?>js/back/report/bos_ticket_details_report.js" >
</script>
<style>
    label.error, div.error, ul.error, span.error {
        color: #f00;
        font-size: 14px;
    }
    th { white-space: nowrap; }
</style>
<section class="content-header">
    <h3>Ticket Details Report</h3>

</section>

<!-- Main content -->
<section class="content">
    <div id="wallet_wrapper">
        <div class="clearfix" style="height: 10px;clear: both;"></div>
        <div class="row"><div id="showDivAdvSearch" >
                <div class="col-xs-12">
                    <div class="box" style="padding:10px;">
                        <?php $attributes = array("method" => "POST", "id" => "theform",  "name" => "theform","onsubmit" => 'return false;');
                                echo form_open("#", $attributes); ?>
                            <div class="row box-title" >


                                <div style="display: block;" class="col-md-2 display-no">
                                    <div style="position: static;" class="form-group">
                                        <label for="input-text-1">From Date</label>
                                        <input class="form-control date" id="from_date" placeholder="<?php echo date('d-m-Y', strtotime('-1 days')); ?>" type="text" >
                                        <!-- <p class="help-block">Example block-level help text here.</p> -->
                                    </div>
                                </div>
                                <div style="display: block;" class="col-md-2 display-no">
                                    <div style="position: static;" class="form-group">
                                        <label for="input-text-2">To Date</label>
                                        <input type="hidden" name="def_to_date" id="def_to_date" value="<?php echo date('Y-m-d'); ?> ">
                                        <input type="hidden" name="def_from_date" id="def_from_date" value="<?php echo date('Y-m-d', strtotime('-1 days')); ?>">
                                        <input class="form-control date" id="till_date" placeholder="<?php echo date('d-m-Y'); ?>" type="text">
                                        <!-- <p class="help-block">Example block-level help text here.</p> -->
                                    </div>
                                </div>
                                <div style="display: block;" class="col-md-2 display-no">    
                                    <div style="position: static;" class="form-group">
                                        <label for="select-1">Transaction status</label>
                                        <select class="form-control" id="trans_type" name="trans_type">
                                           <!--  <option value="select_status">Select status</option>
                                            <option value="select_status">All</option> -->
                                            <option value="success">Success</option> 
                                            <?php  /*
                                                //foreach ($tran_type_data as $key => $value) {
                                                    //if ($key == 'success') {
                                                        //$select = 'selected="selected"';
                                                   // } else {
                                                        //$select = '';
                                                    //}
                                                    //echo "<option value='" . $key . "' $select> " . $value . " </option>";
                                                }*/
                                            ?> 
                                        </select>
                                        
                                    </div>
                                </div>
                                <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                        <label for="select-1">Operator</label>
                                        <select class="form-control" id="operator_id" name="operator_id">
                                           <!--  <option value="">Select Operator</option> -->
                                            <option value="msrtc">MSRTC</option>
                                           <!--  <option value="hrtc">HRTC</option>
                                            <option value="rsrtc">RSRTC</option>
                                            <option value="upsrtc">UPSRTC</option>
                                            <option value="etravelsmart">ETRAVELSMART</option> -->

                                        </select>
                                        <!-- <p class="help-block">Example block-level help text here.</p> -->
                                    </div>
                                    </div>
<!--                                <div style="display: none;" class="col-md-2 display-no">    
                                </div>-->

                                <div style="display: block;" class="col-md-4 display-no">    
                                    <div style="position: static;" class="form-group">
                                        <label for="select-1">Available columns</label>
                                        <!--<select class="form-control toggle-vis" multiple="multiple" id="trans_type" name="trans_type">-->
                                        <div style="height: 7em; width: 20em; overflow: auto;" class="form-control">

                                            <input type="checkbox" class="select_check" checked="checked" value="1" >Ticket id</br>
                                            <input type="checkbox" class="select_check" checked="checked" value="2" >Transaction Status</br>
                                            <input type="checkbox" class="select_check" checked="checked" value="3" >Rokad Ref No</br>

                                            <input type="checkbox" class="select_check" checked="checked" value="4" >PNR NO</br>
                                            <input type="checkbox" class="select_check" checked="checked" value="5" >Ticket Ref No</br>
                                            <input type="checkbox" class="select_check" checked="checked" value="6" >Provider Name</br>
                                            <input type="checkbox" class="select_check" checked="checked" value="7" >Operator Name</br>

                                            <input type="checkbox" class="select_check" checked="checked" value="8" >From Stop</br>
                                            <input type="checkbox" class="select_check" checked="checked" value="9" >To Stop</br>
                                            <input type="checkbox" class="select_check" checked="checked" value="10" >Boarding Stop</br>

                                            <input type="checkbox" class="select_check" checked="checked" value="11" >Issued date</br>
                                            <input type="checkbox" class="select_check" checked="checked" value="12" >DOJ</br>
                                            <input type="checkbox" class="select_check" checked="checked" value="13" >No of passenger</br>

                                            <input type="checkbox" class="select_check" checked="checked" value="14" >Service Tax</br>
                                            <input type="checkbox" class="select_check" checked="checked" value="15" >Total Fare Without Discount</br>
                                            <input type="checkbox" class="select_check" checked="checked" value="16" >Discount On</br>

                                            <input type="checkbox" class="select_check" checked="checked" value="17" >Discount Type</br>
                                            <input type="checkbox" class="select_check" checked="checked" value="18" >Discount Percentage</br>
                                            <input type="checkbox" class="select_check" checked="checked" value="19" >Discount Price</br>

                                            <input type="checkbox" class="select_check" checked="checked" value="20" >Ticket id</br>
                                            <input type="checkbox" class="select_check" checked="checked" value="21" >Transaction Status</br>
                                            <input type="checkbox" class="select_check" checked="checked" value="22" >Total Fare Paid</br>

                                          <!--   <input type="checkbox" class="select_check" checked="checked" value="23" >Refund Amount</br> -->
                                            <!-- <input type="checkbox" class="select_check" checked="checked" value="24" >Cancel Charge</br> -->
                                            <input type="checkbox" class="select_check" checked="checked" value="25" >PG Track Id</br>

                                        <!--</select>-->
                                        <!-- <p class="help-block">Example block-level help text here.</p> -->
                                    </div>
                                    </div>
                                </div>



                                <div style="display: block;" class="col-md-4 display-no">   
                                    <div style="display: block;"  class="box-footer2 exports_btn">
                                        <button type="submit" class="btn btn-primary ">Submit</button>
                                        <button  class=" btn btn-primary" id="extoexcel" type="button" style="display:none; margin-top: -34px;margin-left:96px; ">
                                            Export to Excel
                                        </button>
                                    </div>
                                </div>
                            </div> 




                        <?php echo form_close(); ?>










                        <br><br>
                        <div class="box-body table-responsive" id="table_show">

                            <table id="op_rept" name="op_rept" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Ticket id</th>
                                        <th>Transaction Status</th>
                                        <th>Rokad Ref No</th>
                                        <th>PNR NO</th>
                                        <th>Ticket Ref No</th>
                                        <th>Provider Name</th>
                                        <th>Operator Name</th>
                                        <th>From Stop</th>
                                        <!-- <th>From Code</th> -->
                                        <th>To Stop</th>
                                        <!-- <th>To Code</th> -->
                                        <th>Boarding Stop</th>
                                        <!-- <th>Destination Stop 10</th>
                                        <th>Destination Code</th> -->
                                        <th>Issued date</th>	
                                        <th>DOJ</th>
                                        <th>No of passenger</th>
                                        <th>Basic Fare</th>
                                        <th>Service Tax</th>
                                        <th>Total Fare Without Discount</th>
                                        <th>MSRTC Charges (3% or Rs.50)</th>
                                        <th>Discount On</th>
                                        <th>Discount Type</th>
                                        <th>Discount Percentage</th>
                                        <th>Discount Price</th>
                                        <th>Total Fare Paid</th>
                                       <!--  <th>Refund Amount</th> -->
                                        <!-- <th>Cancel Charge</th> -->
                                        <th>PG Track Id</th>
                                    </tr>
                                </thead>

                                <tbody>                                           

                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <!-- accepted payments column -->

                            <div class="col-xs-6">
                                <p class="lead">
                                    <b>Summary Under Report</b>
                                </p>
                                <div >
                                    <table class="table">
                                        <tbody><tr>
                                                <th style="width:50%">Total Tickets:</th>
                                                <td><span id="tot_booked_tick"><?php //echo $summary_details[0]['tot_ticket'];  ?></span></td>
                                            </tr>
                                            <tr>
                                                <th>Total Passengers:</th>
                                                <td><span id="tot_psgr"><?php //echo $summary_details[0]['No of Passenger'];  ?></span></td>
                                            </tr>
                                            <tr>
                                                <th>Total Basic Fare:</th>
                                                <td><span id="tot_basic_fare"><?php //echo $summary_details[0]['tot_fare_amt'];  ?></span></td>
                                            </tr>
                                            <tr>
                                                <th>Total MSRTC Addinal Charges per Passenger (3% or Rs.50):</th>
                                                <td><span id="tot_additional"><?php //echo $summary_details[0]['UPSRTC Additional Charges (3% or Rs.50)'];  ?></span></td>
                                            </tr>
<!--                                                     <tr>
                                                <th>Total Basic Fare + UPSRTC Add Fare (Expected):</th>
                                                <td><span id="tot_overdue"><?php // $summary_details[0]['No of Passenger'];  ?></span></td>
                                            </tr>-->
                                            <tr>
                                                <th> Total Discount Amount:</th>
                                                <td><span id="tot_disc_amt"><?php //echo $summary_details[0]['Discounted Amount'];  ?></span></td>
                                            </tr>
                                            <tr>
                                                <th> Final Fare:</th>
                                                <td><span id="tot_final_fare"><?php //echo $summary_details[0]['Reveue'];  ?></span></td>
                                            </tr>

                                            <!-- <tr>
                                                <th>Total Cancellation Charges:</th>
                                                <td><span id="tot_can_charges"><?php //echo $summary_details[0]['cancel_ticket_fare'];  ?></span></td>
                                            </tr> -->
                                            <tr>
                                                <!-- <th>Total Refunded Amount:</th>
                                                <td><span id="tot_ref_amt"><?php //echo $summary_details[0]['pg_refund_amount'];  ?></span></td> -->
                                            </tr>
                                        </tbody></table>
                                </div>
                            </div>
                        </div>
                    </div></div></div>


            <div class="clearfix" style="height: 10px;clear: both;"></div>   
        </div>
    </div>


    <script type="text/javascript">

        $(document).ready(function () {

            var tconfig = {
                // "sDom": 'C<"clear">lfrtip',
                // "colVis": {
                //     "buttonText": "Change columns"
                // },
                "processing": true,
                "serverSide": true,
                "scrollX": true,
                "ajax": {
                    "url": BASE_URL + "report/report/bos_tkt_details",
                    "type": "POST",
                    "data": "json",
                    data   : function (d) {
                        var date = new Date();
                        var from_date;
                        var till_date;
                        if ($("#from_date").val() == '') {
                            from_date = $("#def_from_date").val();

                        } else {
                            from_date = $("#from_date").val();
                        }
                        if ($("#till_date").val() == '') {
                            till_date = $("#def_to_date").val();
                        } else {
                            till_date = $("#till_date").val();
                        }

                        d.from_date = from_date;
                        d.till_date = till_date;


                        d.operator_id = $("#operator_id option:selected").val();
                        d.tran_type = $("#trans_type option:selected").val();
                        if (d.tran_type == '') {
                            d.tran_type = 'success';
                        }
                        d.pnr_no = $("#pnr_no").val();
                         d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;
                    }
                },
                "columnDefs": [
                    {
                        "searchable": false
                    }
                ],
                "iDisplayLength": 10,
                "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
                "bFilter": false,
                "bPaginate": true,
                "bRetrieve": true,
                "order": [[2, "desc"]],
                "aoColumnDefs": [
                    {
                        //  "targets": [13],
                        "visible": true,
                        "searchable": false

                    },
                    {
                        "bSortable": false,
                        "aTargets": [0]
                    },
                    {
                        "bSortable": false,
                        "aTargets": [1]
                    },
                ],
                "oLanguage": {
                    "sProcessing": '<img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>'
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {

                    var full_amt = 0;
                    var half_amt = 0;
                    var no_of_psgr = 0;

                    $("td:first", nRow).html(iDisplayIndex + 1);

                    return nRow;

                },
                fnDrawCallback: function (data)
                {

                    if (oTable.fnGetData().length == 0)
                    {
                        $('.print').css('display', 'none');

                    } else {
                        $('.print').css('display', 'inline');

                    }
                },
                footerCallback: function (tfoot, nRow, aData, start, end, display) {

                    var api = this.api(), aData;

                    if (nRow.length > 0)
                        $("#extoexcel").css('display', 'block');
                    else
                        $("#extoexcel").css('display', 'none');

                    var intVal = function (i) {
                        return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '') * 1 :
                                typeof i === 'number' ?
                                i : 0;
                    };


                    $(tfoot).find('th').eq(0).html("Total");

                    tot_psgr = api
                            .column(13, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);


                    basic_fare = api
                            .column(14, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                    add_charges = api
                            .column(17, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);


                    disc_price = api
                            .column(20, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                    tot_disc_amt = api
                            .column(21, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return (intVal(a) + intVal(b));
                            }, 0);
                    tot_fare = api
                            .column(22, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return (intVal(a) + intVal(b));
                            }, 0);

                    tot_ref_amt = api
                            .column(23, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                    tot_can_charges = api
                            .column(24, {page: 'current'})
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                    /*  tot_can_psgr = api
                     .column( 27, { page: 'current'} )
                     .data()
                     .reduce( function (a, b) {
                     return intVal(a) + intVal(b);
                     }, 0 );
                     tot_can_tick = api
                     .column( 26, { page: 'current'} )
                     .data()
                     .reduce( function (a, b) {
                     return intVal(a) + intVal(b);
                     }, 0 );*/


                    $('#tot_psgr').html(tot_psgr);
                    $('#tot_booked_tick').html(nRow.length);
                    $('#tot_basic_fare').html(basic_fare.toFixed(2));

                    $('#tot_additional').html(add_charges.toFixed(2));
                    $('#tot_disc_amt').html(tot_disc_amt.toFixed(2));

                    $('#tot_final_fare').html(tot_fare.toFixed(2));
                    // $('#tot_can_tic').html(tot_can_tick);
                    // $('#tot_can_psgr').html(tot_can_psgr);

                    $('#tot_can_charges').html(tot_can_charges.toFixed(2));
                    $('#tot_ref_amt').html(tot_ref_amt.toFixed(2));


                }

            };


            oTable = $('#op_rept').dataTable(tconfig);
            $('#theform').submit(function (e) {

                oTable.fnDraw();
                // summary_data();

            });

            $(document).off('change', '.toggle-vis').on('change', '.toggle-vis', function (e)
            {

                e.preventDefault();

                oTable.fnSetColumnVis($(this).val(), false);
            });


            $('#op_rept head tr th').each(function () {
                // $('#hide_show_col').
            });

 $(document).off('ifChanged', '.select_check').on('ifChanged', '.select_check', function (e) 
            { 
                   e.preventDefault();
                  
                  if ($(this).prop('checked')==true){ 
           
                     $(this).parent().addClass("checked");
                    oTable.fnSetColumnVis( $(this).val(), true );
                }else{
                    oTable.fnSetColumnVis( $(this).val(), false );
                }

            });

            $('.date').datepicker({
                dateFormat: 'dd-mm-yy'
            });

            //validate both  dates
            $.validator.addMethod("checkDepend",
                    function (value, element) {
                        if ($("#from_date").val() != "")
                        {
                            var date_diff = date_operations($("#from_date").val(), $("#till_date").val());
                            if (date_diff.date_compare)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return true;
                        }

                    }, "To date must be greater than from date.");

            $('#hand_held').validate({
                ignore: "",
                rules: {
                    from_date: {
                        required: true
                    },
                    till_date: {
                        checkDepend: true
                    }
                },
                messages: {
                    from_date: {
                        required: "Please Enter from date"

                    }
                }
            });

        });


        function showAdvSearch()
        {
            $("#showDivAdvSearch").toggle();
        }



    </script>

</section>