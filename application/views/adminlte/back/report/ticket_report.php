<section class="content-header">
    <h3>Ticket Report test</h3>
</section>

<section class="content">
  <div id="wallet_wrapper">
    <div class="row">
      <div class="col-xs-12">
        <?php $attributes = array("method" => "POST", "id" => "ticket_data_view",  "name" => "ticket_data_view","onsubmit" => 'return false;');
            echo form_open("#", $attributes); ?>
          <div class="box" style="padding:10px;">
            <div class="row">
                            <div style="display: block;" class="col-md-2 display-no">
                              <div style="position: static;" class="form-group">
                                  <label>From Date</label>
                                   <input class="form-control" id="from_date" name="from_date"  type="text" value="<?php if(!empty($from_date)) echo $from_date; else echo date('d-m-Y'); ?>">
                              </div>
                            </div> 
                            <div style="display: block;" class="col-md-2 display-no">
                              <div style="position: static;" class="form-group">
                                  <label>To Date</label>
                                   <input class="form-control" id="till_date" name="till_date" type="text" value="<?php if(isset($till_date))echo $till_date; else echo date('d-m-Y');?>"/>
                              </div>
                            </div> 
                      </div> 
              <div class="row"> 
                 <div style="display: block;" class="col-md-1 display-no">
                       <div style="position: static;" class="form-group">
                           <button class="btn btn-primary" name="extoexcel" id="extoexcel" type="submit">Export</button>
                       </div>
                   </div>
             </div> 

               <!-- <div class="row">
                  <div class="col-xs-12">
                     <div class="box">
                         <div class="box-body" id="table_show">
                             <table id="op_rept" name="op_rept" class="table table-striped">
                                  <thead>                    
                                    <tr>
                                        <th width="">Sr. No.</th>
                                        <th width="">Sale Agent Name</th>
                                        <th width="">Agent Code</th>
                                        <th width="">Etim No</th>
                                        <th width="">Waybill No</th>
                                        <th width="">Total Amount</th>
                                        <th width="">Base Fare</th>
                                        <th width="">ASN Amount</th>
                                        <th width="">GST </th>
                                        <th width="">Connection No</th>
                                        <th width="">Token No</th>
                                        <th width="">Route No</th>
                                        <th width="">Ticket No</th>
                                        <th width="">Ticket Code No</th>
                                        <th width="">Concession Code</th>
                                        <th width="">From Stop Name</th>
                                        <th width="">Till Stop Name</th>
                                        <th width="">Collection Time</th> 
                                    </tr>
                                </thead>
                                  <tbody>                                           
                              </tbody>
                          </table>
                      </div>
                 </div>
             </div>
         </div> -->
       <div class="clearfix" style="height: 10px;clear: both;"></div>   
    <?php echo form_close(); ?>
  </div>   
</div>
       
</section>
<script>
var oTables = {};
$(document).ready(function() {
    
    $( "#from_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        'maxDate': new Date()
    }).on("change", function () {
            $('#till_date').datepicker('option', 'minDate', $("#from_date").val());
    });

    $( "#till_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        'maxDate': new Date()
    }).on("change", function () {
            $('#from_date').datepicker('option', 'maxDate', $("#till_date").val());
    });


    // form validation code start here
   $('#ticket_data_view').validate({ 
            rules: {
                      "from_date" :{
                        required : true,
                      },
                      "till_date" :{
                        required : true,
                      },
       
                    },
            messages: {
                        "from_date" : {
                            required: "Please select from date",
                        },
                        "till_date" : {
                            required: "Please select to  date",
                        },
                    
                      },
            errorPlacement: function (error, element) { 
                  error.appendTo(element.parent("div") );
            }
      });

    $(document).off('click', '#extoexcel').on('click', '#extoexcel', function (e) {
    if ($("#ticket_data_view").valid()) {
            var from_date=$('#from_date').val();
            var till_date=$('#till_date').val();
  
            window.location.href = BASE_URL+'report/report/ticket_report_excel?from_date='+from_date+'&till_date='+till_date;
        } else {
          alert("Please select date first....");
          return false;
        }
    });

    
});

</script>