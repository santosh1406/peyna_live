<section class="content-header">
    <h3>Daily Commission Report</h3>
</section>
 <section class="content">
   <div id="wallet_wrapper">
      <div class="row">
         <div class="col-xs-12">
                 <?php $attributes = array("method" => "POST", "id" => "userrpt",  "name" => "userrpt","onsubmit" => 'return false;');
                            echo form_open("#", $attributes); ?>
                <div class="box" style="padding:10px;">
                    <div class="row">
                        <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                <label for="input-text-1">From Date</label>
                                  <input class="form-control date" id="from_date" placeholder="<?php echo date('d-m-Y'); ?> " value="<?php if(!empty($from_date))echo $from_date; ?>" type="text" >
                             </div>
                        </div>
                        <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                <label for="input-text-2">To Date</label>
                                    <input class="form-control date" id="to_date" placeholder="<?php echo date('d-m-Y'); ?>" value="<?php if(!empty($till_date))echo $till_date; ?>" type="text">
                            </div>
                        </div>
                    </div>     
                       <div class="row"> 
                         <div style="display: block;" class="col-md-1 display-no">
                            <div style="position: static;" class="form-group">
                                 <button class="btn btn-primary" name="op_generate_btn" id="op_generate_btn" type="submit"><span></span>Search</button>
                            </div>
                         </div>
                         <div style="display: block;" class="col-md-1 display-no">
                            <div style="position: static;" class="form-group">
                                 <button class="btn btn-primary" name="reset_btn" id="reset_btn" type="reset">Reset</button>
                            </div>
                         </div>
                          <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                 <button  class="btn btn-primary" id="extoexcel" type="button">
                                   <span class="glyphicon glyphicon-export"></span>
                                   Export to Excel</button>  
                            </div>
                        </div>
                      </div> 
                  </div>
                   <br>  
                       <div class="row">
                            <div class="col-xs-12">
                                 <div class="box">
                                    <div class="box-body" id="table_show">
                                          <table id="op_rept" name="op_rept" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Sr. No.</th>
                                                        <th>Commission Calculation Date</th>
                                                        <th>User Name</th>
                                                        <th>Applicable Amount (Rs.)</th>            
                                                        <th>Commission Percentage</th>
                                                        <th>Commission Amount (Rs.)</th>
                                                        <th>GST Percentage</th>
                                                        <th>GST Amount</th>
                                                        <th>Commission With GST (Rs.)</th>
                                                        <th>TDS Percentage</th>
                                                        <th>TDS Amount</th>
                                                        <th>Net Commission Amount (Rs.)</th>
                                                    </tr>
                                                 <tbody>                                           
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                         </div>
                    </form>
                  </div>   
               </div>
            </div>
      </section>

       <script>
       var oData;
       $(document).ready(function() {
   
        var tconfig = {
        "processing": true,
        "serverSide": true,
        "iDisplayLength": 10,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        "ajax": {
            "url": BASE_URL+"report/report/daily_commission",
            "type": "POST",
            "data": "json",
            data   : function (d) {
                        d.from_date = $("#from_date").val();
                        d.to_date = $("#to_date").val();
                     }
        },
        "columnDefs": [
        {
            "searchable": false,
            'targets'       : 2
        }
        ],
       "scrollX": true,
       // "searching": true,
       "bFilter": false,
       "bPaginate": true,
       "bRetrieve": true,
        "aoColumnDefs": [
       
        {
            "bSortable": false,  
            "aTargets": [0]
        },
       ],
         "fnRowCallback": function(nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();  
            $("td:first", nRow).html(info.start + iDisplayIndex + 1);         
            return nRow;
        },
        fnDrawCallback: function(data)
        {

            if (oData.fnGetData().length == 0)
            {
                $("#extoexcel").attr("disabled", "disabled"); 

            } else {
                 $("#extoexcel").removeAttr("disabled"); 

            }
        },
          "oLanguage": {
                    "sProcessing": '<img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>'
                },
    };
       oData= $('#op_rept').dataTable(tconfig);          
        $(document).off('click', '#op_generate_btn').on('click', '#op_generate_btn', function (e) {
             
         
         oData.fnDraw();       
      });
        $(document).off('click', '#reset_btn').on('click', '#reset_btn', function (e) {
        location.reload(true);
      });
       
       $( "#from_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        maxDate :0
        });
        $( "#to_date" ).datepicker({
            'dateFormat' : 'dd-mm-yy',
            maxDate:0
        });
       
         $('#extoexcel').on('click', function(e) 
            { 
                var from_date = $("#from_date").val();
                var till_date = $("#to_date").val();      
              window.location.href=BASE_URL+'report/report/daily_commission_excel?from_date='+from+"&to_date="+to;   
          });
          

    });
          
</script>
