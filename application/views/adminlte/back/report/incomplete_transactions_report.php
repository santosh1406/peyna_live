    <style>
    label.error, div.error, ul.error, span.error {
        color: #f00;
        font-size: 14px;
    }
    th { white-space: nowrap; }
</style>
<section class="content-header">
    <h3>Incomplete Transaction Report</h3>
</section>
<!-- Main content -->
<section class="content">
    <div id="wallet_wrapper"><div class="clearfix" style="height: 10px;clear: both;"></div>
        <div class="row"><div id="showDivAdvSearch" >
                <div class="col-xs-12">
                    <div class="box" style="padding:10px;">
                        <?php $attributes = array("method" => "POST", "id" => "theform",  "name" => "theform","onsubmit" => 'return false;');
                            echo form_open("#", $attributes); ?>
                            <div class="row box-title" >
                                <div style="display: block;" class="col-md-2 display-no">
                                    <div style="position: static;" class="form-group">
                                        <label for="input-text-1">From Date</label>
                                        <input class="form-control date" id="from_date" placeholder="<?php echo date('d-m-Y'); ?> " type="text" >
                                        <!-- <p class="help-block">Example block-level help text here.</p> -->
                                    </div>
                                </div>
                                <div style="display: block;" class="col-md-2 display-no">
                                    <div style="position: static;" class="form-group">
                                        <label for="input-text-2">To Date</label>
                                        <input type="hidden" name="def_to_date" id="def_to_date" value="<?php echo date('Y-m-d'); ?> ">
                                        <input type="hidden" name="def_from_date" id="def_from_date" value="<?php echo date('Y-m-d', strtotime(date('d-m-Y'))); ?>">
                                        <input class="form-control date" id="till_date" placeholder="<?php echo date('d-m-Y'); ?>" type="text">
                                        <!-- <p class="help-block">Example block-level help text here.</p> -->
                                    </div>
                                </div>

                                <div style="display: none;" class="col-md-2 display-no">    
                                    <div style="position: static;" class="form-group">
                                        <label for="select-1">Operator</label>
                                        <select class="form-control" id="operator_id" name="operator_id">
                                            <option value="">Select Operator</option>
											<option value="">All</option>
											
                                             <?php if(count($op_id) > 0) { foreach ($op_id as $op) {
                                                    echo "<option value='" . $op['op_id'] . "'> " . $op['op_name'] . " </option>";
                                             } }
                                            echo "<option value='Etravel
                                                    function incomplete_transactions(){
                                                        
                                                    }smart'>Etravelsmart</option>";
                                            ?>
                                        </select>
                                        <!-- <p class="help-block">Example block-level help text here.</p> -->
                                    </div>
                                </div>

                                <div style="display: block;" class="col-md-2 display-no">    
                                    <div style="position: static;" class="form-group">
                                        <label for="select-1">Transaction status</label>
                                        <select class="form-control" id="trans_type" name="trans_type">
                                            <option value="select_status">Select status</option>
											 <option value="select_status">All</option>
                                            <?php
                                            foreach ($tran_type_data as $key => $value) {
                                                if ($key == 'success') {
                                                    $select = 'selected="selected"';
                                                } else {
                                                    $select = '';
                                                }
                                                echo "<option value='" . $key . "' $select> " . $value . " </option>";
                                            }
                                            ?>
                                        </select>

                                    </div>
                                </div>
                                <?php 
                                eval(SYSTEM_USERS_ARRAY);
                                if(in_array(strtolower($this->session->userdata("role_name")), $system_users_array))
                                {
                                ?>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-2 display-no" style="display: none;">
                                            <div class="form-group" style="position: static;">
                                                <label for="select-1">User Type</label>
                                                <select id="user_type" class="form-control" name="user_type">
                                                <option value="">All</option>
                                                <option value="4">Master Distributor</option>
                                                <option value="4">Area Distributor</option>
                                                <option value="6">Distributor Distributor</option>
                                               <option value="7">Retailer</option>
                                                </select>
                                            </div>   
                                        </div>
                                        <div class="col-md-2 display-no" style="display: none;" id="mydiv">
                                            <div class="form-group" style="position: static;">
                                                <label for="select-1">Select Users</label>
                                                <select id="select_user" class="form-control chosen_select" name="select_user" >
                                                    <option value="">Select </option>
                                                </select>
                                            </div>  
                                        </div>                   
                                    </div>
                                </div>
                                <?php
                                }
                                ?>

                       <div style="display: block;" class="col-md-1 display-no">
                           <div style="position: static;" class="form-group">
                                <button type="submit" class="btn btn-primary ">Search</button>
                           </div>
                       </div>
                       <div style="display: block;" class="col-md-1 display-no">
                           <div style="position: static;" class="form-group">
                               <button class="btn btn-primary" name="reset_btn" id="reset_btn" type="reset">Reset</button>
                           </div>
                       </div>
                    <?php if($this->session->userdata('level') == '') { ?>
                       <div style="display: block;" class="col-md-2 display-no">
                           <div style="position: static;" class="form-group">
                               <button  class=" btn btn-primary" id="extoexcel" type="button">
                                <span class="glyphicon glyphicon-export"></span>
                                    Export to Excel</button>  
                           </div>
                       </div>
                    <?php } ?>
                    </div>
                    </div>
                        <?php echo form_close();?>


                        <br><br>
                       <div class="box-body table-responsive" id="table_show">

                            <table id="op_rept" name="op_rept" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Sr No</th>
                                        <th>Ticket Id</th>
                                        <th>User Type</th>
                                        <th>User Name</th>
                                        <th>Email Id</th>
                                        <th>Mobile No.</th>
                                        <th>Operator Name</th>
                                        <th>Provider Name</th>
                                        <th>From Stop</th>
                                        <th>To Stop</th>
                                        <th>Issued date</th>    
                                        <th>DOJ</th>
                                        <th>No of passenger</th>
                                        <th>Ticket Fare</th>
                                        <th>Transaction Status</th>
                                    </tr>
                                </thead>

                                <tbody>                                           

                                </tbody>
                            </table>
                        </div> 
                        <div class="row">
                            <!-- accepted payments column -->
                            
                            <div class="col-xs-6">
                                <p class="lead">
                                    <b>Summary Under Report</b>
                                </p>
                                <div >
                                    <table class="table">
                                        <tbody><tr>
                                                <th style="width:50%">Total Tickets:</th>
                                                <td><span id="tot_booked_tick"><?php //echo $summary_details[0]['tot_ticket']; ?></span></td>
                                            </tr>
                                            <tr>
                                                <th>Total Passenegrs:</th>
                                                <td><span id="tot_psgr"><?php //echo $summary_details[0]['No of Passenger']; ?></span></td>
                                            </tr>
                                            <tr>
                                                <th>Ticket Fare:</th>
                                                <td><span id="ticket_fare"><?php //echo $summary_details[0]['tot_fare_amt']; ?></span></td>
                                            </tr>
                                        </tbody></table>
                                </div>
                            </div>
                        </div>
                    </div></div></div>


            <div class="clearfix" style="height: 10px;clear: both;"></div>   
        </div>
    </div>

<!-- <style type="text/css" href="<?php echo base_url() . COMMON_ASSETS; ?>plugins/datatable/extension/dataTables.colVis.css"></style> -->
<!-- <script type="text/javascript" src="<?php echo base_url() . COMMON_ASSETS; ?>plugins/datatable/extension/dataTables.colVis.js" ></script> -->

    <script type="text/javascript">

        $(document).ready(function() {

            var tconfig = {
                "processing": true,
                "serverSide": true,
                "scrollX": true,
                "ajax": {
                    "url": BASE_URL + "report/report/incomplete_transactions",
                    "type": "POST",
                    "data": "json",
                    data   : function(d) {
                        var date = new Date();
                        var from_date;
                        var till_date;
                        if ($("#from_date").val() == '') {
                            from_date = $("#def_from_date").val();

                        } else {
                            from_date = $("#from_date").val();
                        }
                        if ($("#till_date").val() == '') {
                            till_date = $("#def_to_date").val();
                        } else {
                            till_date = $("#till_date").val();
                        }

                        d.from_date = from_date;
                        d.till_date = till_date;


                        d.operator_id = $("#operator_id option:selected").val();
                        d.user_type = $("#user_type option:selected").val();

                        d.tran_type = $("#trans_type option:selected").val();
                        if (d.tran_type == '') {
                            d.tran_type = 'success';
                        }
                        d.role_id = $("#user_type").find('option:selected').attr('value');
                        d.user_id = $("#select_user").find('option:selected').attr('value');
                        d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token ;
                    }
                },
                "columnDefs": [
                    {
                        "searchable": false
                    }
                ],
                "iDisplayLength": -1,
                "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
                "bFilter": false,
                "bPaginate": true,
                "bRetrieve": true,
                "order": [[1, "desc"]],
                "aoColumnDefs": [
                    {
                        //  "targets": [13],
                        "visible": true,
                        "searchable": false

                    },
                    {
                        "bSortable": false,
                        "aTargets": [0]
                    },
                    {
                        "bSortable": false,
                        "aTargets": [1]
                    },
                ],
                "oLanguage": {
                    "sProcessing": '<img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>'
                },
                "fnRowCallback": function(nRow, aData, iDisplayIndex) {

                    var full_amt = 0;
                    var half_amt = 0;
                    var no_of_psgr = 0;

                    $("td:first", nRow).html(iDisplayIndex + 1);

                    return nRow;

                },
                fnDrawCallback: function(data)
                {

                    if (oTable.fnGetData().length == 0)
                    {
                        $('.print').css('display', 'none');

                    } else {
                        $('.print').css('display', 'inline');

                    }
                },
                footerCallback: function(tfoot, nRow, aData, start, end, display) {

                    var api = this.api(), aData;
                    var intVal = function(i) {
                        return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '') * 1 :
                                typeof i === 'number' ?
                                i : 0;
                    };


                    $(tfoot).find('th').eq(0).html("Total");
                    console.log(this.api());
                    tot_psgr = api
                            .column(12, {page: 'current'})
                            .data()
                            .reduce(function(a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);


                    ticket_fare = api
                            .column(13, {page: 'current'})
                            .data()
                            .reduce(function(a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                    $('#tot_psgr').html(tot_psgr);
                    $('#tot_booked_tick').html(nRow.length);
                    $('#ticket_fare').html(ticket_fare.toFixed(2));

                }

            };


            oTable = $('#op_rept').dataTable(tconfig);
            $('#theform').submit(function(e) {

                oTable.fnDraw();
                // summary_data();

            });

            $(document).off('change', '.toggle-vis').on('change', '.toggle-vis', function(e)
            {
                
                e.preventDefault();

                oTable.fnSetColumnVis($(this).val(), false);
            });


            $('#op_rept head tr th').each(function() {
                // $('#hide_show_col').
            });



            $('.date').datepicker({
                dateFormat: 'dd-mm-yy',
                'maxDate':'0'
            });

            //validate both  dates
            $.validator.addMethod("checkDepend",
            function(value, element) {
                if ($("#from_date").val() != "")
                {
                    var date_diff = date_operations($("#from_date").val(), $("#till_date").val());
                    if (date_diff.date_compare)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }

            }, "To date must be greater than from date.");

            $(document).off('change', '#user_type').on('change', '#user_type', function(e) {
                e.preventDefault();
                $("#select_user").html("");
                var elem = $(this);
                var detail = {};
                var form = '';
                var div = '';//$(this).val()
                detail['role_id'] = $("#user_type").find('option:selected').attr('value');
                var ajax_url = "report/report/user_type_view";

                get_data(ajax_url, form, div, detail, function(response)
                {

                    var k = 1;
                    str = "";
                     $("#select_user").append("<option >Select User</option>");
                    $.each(response, function(index, value) {
                        $('#example tbody').html('');
                        
                        $("#select_user").append("<option value=" + value.id + ">" + value.user_name + "</option>");
                        //  $("#cyc_days").trigger("chosen:updated");
                        // $('#example tbody').append(str);


                    });
                    $("#select_user").trigger("chosen:updated");

                });

            });
             $(document).off('ifChanged', '.select_check').on('ifChanged', '.select_check', function (e) 
            { 
                   e.preventDefault();
                  
                  if ($(this).prop('checked')==true){ 
           
                   
                    oTable.fnSetColumnVis( $(this).val(), true );
                }else{
               
                   
                    oTable.fnSetColumnVis( $(this).val(), false );
                }

            });
    
             $('#extoexcel').on('click', function(e) 
            { //alert(4444);
            
              from_date  = $("#from_date").val();
              till_date    = $("#till_date").val();
              tran_type = $("#trans_type option:selected").val();
              role_id = $("#user_type").find('option:selected').attr('value');
              operator_id=$("#operator_id").find('option:selected').attr('value');
              user_id = $("#select_user").find('option:selected').attr('value');
              if(typeof(user_id) === 'undefined'){
                  user_id = '';
              }
              if(typeof(role_id) === 'undefined'){
                  role_id = '';
              }
              
              if(from_date==''){
                  var from_date = $("#from_date").attr('placeholder');  
              }
              if(till_date==''){
                  var till_date = $("#till_date").attr('placeholder');  
              }
              window.location.href=BASE_URL+'report/report/incomplete_transactions_export_excel?from_date='+from_date+"&operator_id="+operator_id+"&till_date="+till_date+"&tran_type="+tran_type+"&role_id="+role_id+"&user_id="+user_id;   
            });

            $(document).off('click', '#reset_btn').on('click', '#reset_btn', function (e) {
            location.reload(true);
            });
        });


        function showAdvSearch()
        {
            $("#showDivAdvSearch").toggle();
        }

        function show(target) 
        {
            document.getElementById(target).style.display = 'block';
        }

        function hide(target) {
            document.getElementById(target).style.display = 'none';
          
        }
    </script>

</section>

