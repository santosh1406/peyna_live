<style>
    .overlay, .loading-img {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    .overlay {
        z-index: 1010;
        background: rgba(255, 255, 255, 0.7);
    }
    .overlay.dark {
        background: rgba(0, 0, 0, 0.5);
    }
</style>

<section class="content-header">
    <h1>Registration and Issuance Report</h1>
</section>

<!-- Main content -->
<section class="content">
    <div id="commission_wrapper">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body" id="table_show">

                        <?php
                        $attributes = array('class' => 'form', 'id' => 'trans_list_filter', 'name' => 'trans_list_filter', 'method' => 'post', 'onsubmit' => 'return false;');
                        echo form_open('#', $attributes);
                        
                        
                        ?>
                        <div class="row box-title">
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-1">From Date</label>
                                    <input class="form-control date" id="from_date" placeholder="<?php echo date('d-m-Y'); ?> " value="<?php if (!empty($from_date)) echo $from_date; ?>" type="text" >
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2">To Date</label>
                                    
                                    <input class="form-control date" id="to_date" placeholder="<?php echo date('d-m-Y'); ?>" value="<?php if (!empty($till_date)) echo $till_date; ?>" type="text">
                                </div>
                            </div>
<!--                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2">Region Name:</label>
                                    <select class="form-control" id="region_name" name="region_name">
                                        <option value="all">Select</option>
                                        <?php foreach ($region_name as $key => $value) { ?>
                                            <option value="<?php echo $value['REGION_CD']; ?>" <?php echo set_select('region_name', $value['REGION_CD']); ?> ><?php echo $value['REGION_NM']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2">Division Name:</label>
                                    <select class="form-control" id="devision_name" name="devision_name">
                                        <option value="all">Select</option>
                                        <?php if (isset($division_name)) { ?>
                                            <?php foreach ($division_name as $key => $value) { ?>
                                                <option value="<?php echo $value['DIVISION_CD']; ?>" <?php echo set_select('devision_name', $value['DIVISION_CD']); ?> ><?php echo $value['DIVISION_NM']; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2">Depot Name:</label>
                                    <select class="form-control" id="depot_name" name="depot_name">
                                        <option value="all">Select</option>
                                        <?php if (isset($depot_name)) { ?>
                                            <?php foreach ($depot_name as $key => $value) { ?>
                                                <option value="<?php echo $value['DEPOT_CD']; ?>" <?php echo set_select('depot_name', $value['DEPOT_CD']); ?> ><?php echo $value['DEPOT_NM']; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>-->
                             
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        
                        <div class="row"> 
                            <div style="display: block;" class="col-md-1 display-no">
                                <div style="position: static;" class="form-group">
                                    <button class="btn btn-primary" name="card_generate_btn" id="card_generate_btn"  type="submit"><span></span>Search</button>
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-1 display-no">
                                <div style="position: static;" class="form-group">
                                    <button class="btn btn-primary" name="reset_btn" id="reset_btn" type="reset">Reset</button>
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <button  class="btn btn-primary" id="export_to_excel" type="button">
                                        <span class="glyphicon glyphicon-export"></span>
                                        Export to Excel</button>  
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                    <div style="display: none" id="loader" class="overlay"><i class="fa fa-refresh fa-spin"></i></div>
                    <table id="card_data" name="card_data" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="10%">Sr.No</th>
                                <th>Agent Code</th>     
                                <th>Agent Name</th>
                                <th>Depot Code</th>
                                <th>Depot Name</th>
                                <th>Registration Date</th>
                                <th>Student Name</th>                               
                                <th>Age</th>
                                <th>Card ID</th>
                                <th>Card Status</th>
                                <th>From Stop Code</th>
                                <th>Till Stop Code</th>
                                <th>Pass Amount</th>
                                <th>Smartcard Fee</th>
                                <th>Application Fee</th>
                                <th>GST</th>
                                <th>Total Amount</th>
                                <th>Pass Valid From</th>
                                <th>Pass Valid Till</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total_pass_amt = $total_amt = $total_scf_amt = $total_af_amt = $total_gst_amt = 0.00;
                            $key = 0; ?>
                            <?php foreach ($report_data as $key => $value) { ?>
                                <tr>
                                    <td><?php echo ++$key; ?></td>
                                    <td><?php echo $value['agent_code']; ?></td>
                                    <td><?php echo $value['agent_name']; ?></td>
                                    <td><?php echo $value['DEPOT_CD']; ?></td>
                                    <td><?php echo $value['DEPOT_NM']; ?></td>
                                    <td><?php echo $value['reg_date']; ?></td>
                                    <td><?php echo $value['name_on_card']; ?></td>
                                    <td><?php echo ($value['dob']); ?></td>
                                    <td><?php echo $value['cust_card_id']; ?></td>
                                    <td><?php echo $value['card_status']; ?></td>
                                    <td><?php echo $value['from_stop_code']; ?></td>
                                    <td><?php echo $value['till_stop_code']; ?></td>
                                    <td><?php echo $value['pass_amount']; ?></td>
                                    <td><?php echo $value['smart_card_fee']; ?></td>
                                    <td><?php echo $value['application_fee']; ?></td>
                                    <td><?php echo $value['gst_fee']; ?></td>
                                    <td><?php echo $value['total_amount']; ?></td>
                                    <td><?php echo $value['act_date']; ?></td>
                                    <td><?php echo $value['exp_date']; ?></td>
                                </tr>
                                <?php
                                $total_pass_amt += $value['pass_amount'];
                                $total_scf_amt += $value['smart_card_fee'];
                                $total_af_amt += $value['application_fee'];
                                $total_gst_amt += $value['gst_fee'];
                                $total_amt += $value['total_amount'];
                                ?>
                                <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="3" style="text-align:right"> Total Pass : </th>
                                <td><?php echo $key; ?></td>
                                <th colspan="4" style="text-align:right"> Total Amount :</th>
                                <td><?php echo number_format($total_pass_amt, 2, '.', ''); ?></td>
                                <td><?php echo number_format($total_scf_amt, 2, '.', ''); ?></td>
                                <td><?php echo number_format($total_af_amt, 2, '.', ''); ?></td>
                                <td><?php echo number_format($total_gst_amt, 2, '.', ''); ?></td>
                                <td colspan="2"><?php echo number_format($total_amt, 2, '.', ''); ?></td>
                            </tr>
                        </tfoot>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</section>


<script>

$(document).ready(function() {
    function createDatatable() {
        var datatable = $('#card_data').DataTable({
            "scrollX": true
        });
        datatable.draw();
    }

    createDatatable();
    
    $('#export_to_excel').on('click', function(e) {
        var from_date = $("#from_date").val();
        var to_date = $("#to_date").val();
        var region_code = $("#region_name").val();
        var division_code = $("#devision_name").val();
        var depot_code = $("#depot_name").val();
        var flag = 'report'; 
        // window.location.href = BASE_URL + "admin/report/servicePassExcel?flag="+flag+"&from_date="+from_date+"&to_date="+to_date; 
        window.location.href = BASE_URL + "report/smart_card_report/servicePassExcel?flag="+flag+"&from_date="+from_date+"&to_date="+to_date+"&region_code="+region_code+"&division_code="+division_code+"&depot_code="+depot_code; 
    });
    
    $(document).off('click', '#card_generate_btn').on('click', '#card_generate_btn', function (e) {
        
        document.getElementById("loader").style.display = "block";
        //$('#card_generate_btn').prop('disabled', true);
        var from_date = $("#from_date").val();
        var to_date = $("#to_date").val();
        var region_code = $("#region_name").val();
        var division_code = $("#devision_name").val();
        var depot_code = $("#depot_name").val();

        $.ajax({
            "type": "POST",
            "url": BASE_URL + "report/smart_card_report/service_pass_reg_list",
            "data": {from_date:from_date,to_date:to_date,region_code:region_code,division_code:division_code,depot_code:depot_code},
            success: function (strJSON) {

                var objJSON = JSON.parse(strJSON);
                var row;
                var i = 0;
                var total_pass_amt = 0.00;
                var total_scf_amt = 0.00;
                var total_af_amt = 0.00;
                var total_gst_amt = 0.00;
                var total_amt = 0.00;
                $('#card_data').DataTable().destroy();
                $('#card_data tbody').empty();
                $('#card_data tfoot tr').empty();
                $.each(objJSON['data'], function(index, value) {
                    i++;
                    row += ("<tr><td>" + i + "</td><td>"+value['agent_code']+"</td><td>"+value['agent_name']+"</td><td>"+value['DEPOT_CD']+"</td><td>"+value['DEPOT_NM']+"</td><td>" + value['reg_date'] + "</td><td>" + value['name_on_card'] + "</td><td>" + calculateAge(value['dob']) + "</td><td>" + value['cust_card_id'] + "</td><td>" + value['card_status'] + "</td><td>" + value['from_stop_code'] + "</td><td>" + value['till_stop_code'] + "</td><td>" + value['pass_amount'] + "</td><td>"+value['smart_card_fee']+"</td><td>"+value['application_fee']+"</td><td>"+value['gst_fee']+"</td><td>"+value['total_amount']+"</td><td>" + value['act_date'] + "</td><td>" + value['exp_date'] + "</td></tr>");
                    
                    total_pass_amt = parseFloat(value['pass_amount']) + parseFloat(total_pass_amt);
                    total_scf_amt = parseFloat(value['smart_card_fee']) + parseFloat(total_scf_amt);
                    total_af_amt = parseFloat(value['application_fee']) + parseFloat(total_af_amt);
                    total_gst_amt = parseFloat(value['gst_fee']) + parseFloat(total_gst_amt);
                    total_amt = parseFloat(value['total_amount']) + parseFloat(total_amt);
                });
                var tfooter = "<th colspan='3' style='text-align:right'> Total Pass : </th><td>"+i+"</td><th colspan='4' style='text-align:right'> Total Amount :</th><td>"+total_pass_amt.toFixed(2)+"</td><td>"+total_scf_amt.toFixed(2)+"</td><td>"+total_af_amt.toFixed(2)+"</td><td>"+total_gst_amt.toFixed(2)+"</td><td colspan='2'>"+total_amt.toFixed(2)+"</td>";
                $('#card_data tbody').append(row);
                $('#card_data tfoot tr').append(tfooter);
                createDatatable();
                if(strJSON != ''){
                    $('#depoDetails').show();
                    $('#depoDetails').empty();
                    var dt = "<p><b> Depot Code</b> : <?php echo $_SESSION['location_cd'] ?> </p>"+
                    "<p><b>Running Date & Time</b> : <?php echo date('d-m-Y h:i:s a'); ?></p>";
                    $('#depoDetails').append(dt);
                }
                document.getElementById("loader").style.display = "none";
                $('#card_generate_btn').prop('disabled', false);
            }
        });
    });
    
    function calculateAge(dob) {
        var birthdate = new Date(dob);
        var cur = new Date();
        var diff = cur-birthdate;
        var age = Math.floor(diff/31536000000);
        return age;
    }

    $("#from_date").datepicker({
          'dateFormat': 'dd-mm-yy',
          autoclose: true,     
    });

    $("#to_date").datepicker({
          'dateFormat': 'dd-mm-yy',
          autoclose: true,
    });
   
});
</script>



