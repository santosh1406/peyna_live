
<style>
label.error, div.error, ul.error, span.error {
    color: #f00;
    font-size: 14px;
}
</style>
<section class="content-header">
    <h3>Ticket Report</h3>
</section>

<section class="content">
  <div id="wallet_wrapper">
    <div class="row">
      <div class="col-xs-12">
        <?php $attributes = array("method" => "POST", "id" => "userrpt",  "name" => "userrpt","onsubmit" => 'return false;');
            echo form_open("#", $attributes); ?>
          <div class="box" style="padding:10px;">
            <div class="row">
                            <div style="display: block;" class="col-md-2 display-no">
                              <div style="position: static;" class="form-group">
                                  <label>From Date</label>
                                   <input class="form-control" id="from_date" name="from_date"  type="text" value="<?php if(!empty($from_date)) echo $from_date; else echo date('d-m-Y'); ?>">
                              </div>
                            </div> 
                            <div style="display: block;" class="col-md-2 display-no">
                              <div style="position: static;" class="form-group">
                                  <label>To Date</label>
                                   <input class="form-control" id="till_date" name="till_date" type="text" value="<?php if(isset($till_date))echo $till_date; else echo date('d-m-Y');?>"/>
                              </div>
                            </div> 
                      </div> 
              <div class="row"> 
                   <!-- <div style="display: block;" class="col-md-1 display-no">
                       <div style="position: static;" class="form-group">
                           <button class="btn btn-primary" name="op_generate_btn" id="op_generate_btn" type="submit">Search</button>
                       </div>
                   </div>
                   <div style="display: block;" class="col-md-1 display-no">
                       <div style="position: static;" class="form-group">
                           <button class="btn btn-primary" name="reset_btn" id="reset_btn" type="reset">Reset</button>
                       </div>
                   </div> -->
                   <div style="display: block;" class="col-md-2 display-no">
                       <div style="position: static;" class="form-group">
                           <button  class="btn btn-primary" id="extoexcel" type="button">
                       <span class="glyphicon glyphicon-export"></span>Export To Excel</button>  
                       </div>
                   </div>
             </div> <!-- <center> <h3>
             <?php /*if(!empty($result)) { 
                    echo $result; 
               } else {
                echo "";
                }*/ ?></center></h3> -->
           <!--  </div> -->
<!--              <div class="clearfix" style="height: 10px;clear: both;"></div>  
                 <div class="box-footer2">
                    <div class="pull-left">
                        <button class="btn btn-primary" name="op_generate_btn" id="op_generate_btn" type="submit">View</button>
                        <button class="btn btn-primary" name="reset_btn" id="reset_btn" type="reset">Reset</button>
                   </div>
                   <div class="pull-right">
                       <button  class="btn btn-primary margin prin btn-sm print" id="extoexcel" type="button">
                       <span class="glyphicon glyphicon-export"></span>Export To Excel</button>                                      
                  </div>
               </div>                    
               <br>  -->
               <!-- <div class="row">
                  <div class="col-xs-12">
                     <div class="box">
                         <div class="box-body" id="table_show">
                             <table id="op_rept" name="op_rept" class="table table-striped">
                                  <thead>                    
                                    <tr>
                                        <th width="">Sr. No.</th>
                                        <th width="">Sale Agent Name</th>
                                        <th width="">Agent Code</th>
                                        <th width="">Etim No</th>
                                        <th width="">Waybill No</th>
                                        <th width="">Total Amount</th>
                                        <th width="">Base Fare</th>
                                        <th width="">ASN Amount</th>
                                        <th width="">GST </th>
                                        <th width="">Connection No</th>
                                        <th width="">Token No</th>
                                        <th width="">Route No</th>
                                        <th width="">Ticket No</th>
                                        <th width="">Ticket Code No</th>
                                        <th width="">Concession Code</th>
                                        <th width="">From Stop Name</th>
                                        <th width="">Till Stop Name</th>
                                        <th width="">Collection Time</th> 
                                    </tr>
                                </thead>
                                  <tbody>                                           
                              </tbody>
                          </table>
                      </div>
                 </div>
             </div>
         </div> -->
       <div class="clearfix" style="height: 10px;clear: both;"></div>   
    <?php echo form_close(); ?>
  </div>   
</div>
       
</section>
<script>
var oTables = {};
$(document).ready(function() {
    
    $( "#from_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        'maxDate': new Date()
    }).on("change", function () {
            $('#till_date').datepicker('option', 'minDate', $("#from_date").val());
    });

    $( "#till_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        'maxDate': new Date()
    }).on("change", function () {
            $('#from_date').datepicker('option', 'maxDate', $("#till_date").val());
    });


    // form validation code start here
   /* $('#ticket_data_view').validate({ 
            rules: {
                      "waybill_no" :{
                        required : true,
                      },
       
                    },
            messages: {
                        "waybill_no" : {
                            required: "Please select waybill no",
                        },
                    
                      },
            errorPlacement: function (error, element) { 
                  error.appendTo(element.parent("div") );
            }
      });*/


  /* $(document).on("click","#ticket_data_filter_btn",function(e){ 
                      
                    
   $(".img-circle").show(); 

    if (!$("#ticket_data_view").valid()) {
            $(".img-circle").hide();
            $("#extoexcel").attr("disabled", "disabled");
            $("#extopdf").attr("disabled", "disabled");
            return false;
        } else { 
            //$(".img-circle").show();*/
    var tconfig = {
        processing: true,
        serverSide: true,
        ajax: {
            url: BASE_URL+"admin/cb_report_md/list_ticket_data",
            type: 'POST',
            data    : function (d) {
                        d.from_date = $("#from_date").val();
                        d.till_date = $("#till_date").val();
                       /* d.agent_code = $("#agent_code").val();
                        d.waybill_no = $("#waybill_no option:selected").val();*/
                      },
        },
        columnDefs: [
            {
                searchable: false
            }
        ],
        order: [[ 12, "asc" ]],
        scrollX: true,
        iDisplayLength: -1,
        aLengthMenu: [[-1], ["All"]],
        paginate: true,
        paging: true,
        searching: true,
        oLanguage: {
                   // "sProcessing": img
            },           
        aoColumnDefs: [
            {
                    "bSortable": false,
                    "bSearchable": false,
                    "aTargets": [0]
            },
                {
                    "bSortable": false,
                    "bSearchable": false,
                    "aTargets": [12]
                },
        ],
        "fnRowCallback": function(nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:first", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        },
        "fnDrawCallback": function(data) {
            if (oTables.fnGetData().length == 0) {
                $("#extoexcel").attr("disabled", "disabled"); 
                $("#extopdf").attr("disabled", "disabled"); 
            } else {
                $("#extoexcel").removeAttr("disabled"); 
                $("#extopdf").removeAttr("disabled"); 
            }
        },
        "footerCallback": function (tfoot,nRow, aData, start, end, display ) {

             var api = this.api(), aData;

             var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            var ticket = api.column( 12 ).data().unique();
            var ticket_data = api.column( 12 ).data();
        
            asn_amt = api.column( 7 ).data();
             total_asn_amt = asn_amt.length ?
                asn_amt.reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }): 0;
                total_asn_amt = parseFloat(total_asn_amt); 

            gst = api.column( 8 ).data();
             total_gst = gst.length ?
                gst.reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }): 0;
                total_gst = parseFloat(total_gst); 

            //BG Number 20121 start
            amount = api.column( 5 ).data();
            basic_fare = api.column( 6 ).data();
            var j = 0; var total_amount = 0; var total_basic_fare = 0;

            for (var i = 0; i < ticket_data.length; i++) {
                if(ticket_data[i] == ticket[j]) {
                    total_amount = intVal(total_amount) + intVal(amount[i]);
                    total_basic_fare = intVal(total_basic_fare) + intVal(basic_fare[i]);
                    j++;
                }
            }
            //BG Number 20121 end

            $('#tot_booked_tick').html(ticket.length);
            $('#tot_basic_fare').html(total_basic_fare.toFixed(2));
            $('#tot_asn_amt').html(total_asn_amt.toFixed(2)); 
            $('#tot_gst').html(total_gst.toFixed(2)); 
            $('#tot_amt').html(total_amount.toFixed(2)); 
        },
    };
    oTables = $('#ticket_data').dataTable(tconfig);
    $('#ticket_data_view').on('submit', function(e) { 
        e.preventDefault();
        oTables.fnDraw();
    });    
        /*}
    });
*/
    $(document).off('click', '#extoexcel').on('click', '#extoexcel', function (e) {
        var from_date=$('#from_date').val();
        var till_date=$('#till_date').val();
  
        window.location.href = BASE_URL+'report/report/ticket_report_excel?from_date='+from_date+'&till_date='+till_date;
        // $("#extoexcel").attr("href", show_url);
    });
    
});

</script>