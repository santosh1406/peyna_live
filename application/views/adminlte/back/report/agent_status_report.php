<style>
.overlay, .loading-img {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}
.overlay {
    z-index: 1010;
    background: rgba(255, 255, 255, 0.7);
}
.overlay.dark {
    background: rgba(0, 0, 0, 0.5);
}
</style>
<section class="content">
    <div id="commission_wrapper">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"></div><!-- /.box-header -->
              <?php
              //echo "<pre>";
              //print_r($report_data);
                    //die("RAJAN");
              //show($input_data);
              if(isset($input_data['from_date']) && $input_data['from_date'] != '')
              {
                $from_date  = $input_data[from_date];               
              }
              if(isset($input_data['to_date']) && $input_data['to_date'] != '')
              {
                
                $to_date  = $input_data[to_date];
               
              }
              ?>
              
              <?php $attributes = array("method" => "POST", "id" => "report_form",  "name" => "report_form");
                    echo form_open(base_url()."admin/agent_report_status/", $attributes); ?>
                    <br clear="all"/>
                    <div class="row">
                        <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                <label for="input-text-1">From Date</label>
                                  <input class="form-control date" name="from_date" id="from_date" placeholder="<?php echo date('d-m-Y'); ?> " value = "<?php echo empty($from_date) ? ''  : $from_date;?>" type="text" >
                             </div>
                        </div>
                        <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                <label for="input-text-2">To Date</label>
                                    <input class="form-control date" name="to_date" id="to_date" placeholder="<?php echo date('d-m-Y'); ?>" value = "<?php echo empty($to_date) ? '': $to_date;?>" type="text">
                            </div>
                        </div>
                    </div>
                      <div class="row">  
                       <div style="display: block;" class="col-md-1 display-no">
                            <div style="position: static;" class="form-group">
                                 <button class="btn btn-primary" name="report_submit" id="report_submit" type="submit"><span></span>Search</button>
                            </div>
                         </div>
                         <div style="display: block;" class="col-md-1 display-no">
                            <div style="position: static;" class="form-group">
                                 <button class="btn btn-primary" name="reset_btn" id="reset_btn" type="reset">Reset</button>
                            </div>
                         </div>    
                         <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                 <button  class="btn btn-primary" id="extoexcel" type="button">
                                   <span class="glyphicon glyphicon-export"></span>
                                   Export to Excel</button>  
                            </div>
                          </div>
                        </div>
              <?php echo form_close();  ?>
              
              <div style="display: none" id="loader" class="overlay"><i class="fa fa-refresh fa-spin"></i></div>
              <div  style="overflow: scroll;padding-left: 30px;" class="row">
                <?php
                 foreach ($report_data as $agent)
                            {
                                 ++$regionCount[$agent['region']]['region_count'];
                                 if($agent['kyc_verify_status'] == 'Yes'){
                                  ++$regionCount[$agent['region']]['kyc_count'];
                                 }
                            }
                ?>
                 <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Sr. No.</th>
                                <th>Region Code</th>
                                <th>Region Name</th>
                                <th>Agent Count</th>
                                <th>KYC Verified Count</th>
                             </tr>
                        </thead>
                        <tbody>
                            <?php
                            $srNo = 0;
                       
                            foreach ($regionData as $region)
                            {
                             ?>
                            <tr>
                                <td><?php echo ++$srNo; ?></td>
                               <td><?php echo $region['REGION_CD']; ?></td>
                               <td><?php echo $region['REGION_NM']; ?></td>
                               <td><?php 
                            if(!isset($regionCount[$region['REGION_CD']]['region_count'])){
                              $region_count = 0;
                            }else{
                              $region_count =$regionCount[$region['REGION_CD']]['region_count'];
                            }
                               echo $region_count; ?>
                                 
                               </td>
                               <td><?php 
                            if(!isset($regionCount[$region['REGION_CD']]['kyc_count'])){
                              $kyc_count = 0;
                            }else{
                              $kyc_count =$regionCount[$region['REGION_CD']]['kyc_count'];
                            }
                               echo $kyc_count; ?>
                                 
                               </td>
                            </tr>
                            <?php
                            $totalRegionCnt += $region_count; 
                            $totalkyc_count += $kyc_count; 
                            }
                            ?>
                         
                        </tbody>
                         <tfoot>
                          <tr>
                            <td colspan="3" style="text-align: right;"> 
                            <b>Total :</b>   
                            </td>
                            <td>
                              <b>
                              <?php 
echo $totalRegionCnt;
                              ?>
                            </b>
                            </td>
                              <td>
                              <b>
                              <?php 
echo $totalkyc_count;
                              ?>
                            </b>
                            </td>
                          </tr>
                        </tfoot>
                    </table>
              </div>
                <br clear="all"/>
              <div style="overflow: scroll;padding-left: 30px;" class="row">
                    <table class="table table-bordered table-hover" id="agent_report">
                        <thead>
                            <tr>
                                <th>Sr. No.</th>
                                <th>Agent ID</th>
                                 <th>Agent Code</th>
                                <th>Agent Name</th>
                                <th>Mobile No.</th>
                                <th>Address</th>
                                <th>Depo Name</th>
                                <th>Depot Code</th>
                                <th>Div Code</th>
                                  <th>Region</th>
                                <th>Terminal ID</th>
                                 <th>Terminal Generate Date</th>
                                <th>KYC Status</th>
                                <th>Email Status</th>
                                 <th>Verify Status</th>
                                  <th>Signup Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $srNo = 0;
                       
                            foreach ($report_data as $agentData)
                            {
                               

                            ?>
                            <tr>
                                <td><?php echo ++$srNo; ?></td>
                               <td><?php echo $agentData['agent_id']; ?></td>
                               <td><?php echo $agentData['agent_code']; ?></td>
                               <td><?php echo $agentData['f_name'].' '.$agentData['l_name']; ?></td>
                               <td><?php
                                 echo rokad_decrypt($agentData['agent_username'], $this->config->item('pos_encryption_key'));
                                 ?></td>
                               <td><?php echo $agentData['address']; ?></td> 
                               <td><?php echo $agentData['depot']; ?></td>
                               <td><?php echo $agentData['depot_cd']; ?></td>
                               <td><?php echo $agentData['division']; ?></td>
                               <td><?php echo $agentData['region_name']; ?></td>
                               <td><?php echo $agentData['terminal_code']; ?></td>
                               <td><?php echo $agentData['terminal_code_generate_date']; ?></td>
                               <td><?php echo $agentData['kyc_verify_status']; ?></td>
                               <td><?php echo $agentData['email_verify_status']; ?></td>
                               <td><?php echo $agentData['verify_status']; ?></td>
                               <td><?php echo $agentData['signup_date']; ?></td>
                            </tr>
                            <?php
                            }//for

                            /*echo "</br>cbAgentArr</br>";
                            echo "<pre>";
                            print_r($cbAgentArr);
                            echo "</pre>";/**/
                            ?>
                         
                        </tbody>
                       
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
$(document).ready(function() {
    
  var date = new Date();
  //$("#date").val($.datepicker.formatDate("dd-mm-yy", date)); 
    $("#from_date").datepicker({
        'dateFormat' : 'dd-mm-yy',
      maxDate :0
        });

    $(document).off('click', '#reset_btn').on('click', '#reset_btn', function(e) {
        location.reload(true);
    });

    $("#to_date").datepicker({
          'dateFormat': 'dd-mm-yy',
          autoclose: true,     
    });

    
   // $(document).off('click', '#report_submit').on('click', '#report_submit', function (e) {
      
   //      var from_date = $("#from_date").val();
   //  var to_date = $("#to_date").val();
   //  document.report_form.from_date.value = from_date;
   //  document.report_form.to_date.value = from_date;
   //   document.getElementById("report_form").submit();
   //  });
   $('#extoexcel').on('click', function(e) { 
    var from_date = $("#from_date").val();
    var till_date = $("#to_date").val();
    window.location.href=BASE_URL+'admin/agent_report_status/agent_status_report_list_excel?from_date='+from_date+"&to_date="+till_date;   
  });

  
   $('#agent_report').dataTable( {
  "pageLength": 50
} );
});
</script>
