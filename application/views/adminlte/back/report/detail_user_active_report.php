<section class="content-header">
  <h3>Active Agents Report</h3>
</section>
<section class="content">
 <div id="wallet_wrapper">
  <div class="row">
   <div class="col-xs-12">
     <?php $attributes = array("method" => "POST", "id" => "userrpt",  "name" => "userrpt","onsubmit" => 'return false;');
     echo form_open("#", $attributes); ?>
     <div class="box" style="padding:10px;">
      <div class="row">
        <div style="display: block;" class="col-md-2 display-no">
          <div style="position: static;" class="form-group">
            <label for="input-text-1">From Date</label>
            <input class="form-control date" id="from_date" placeholder="<?php echo date('d-m-Y'); ?> " value="<?php echo (!empty($from_date)) ? $from_date : date('d-m-Y'); ?>" type="text"  readonly="true">
          </div>
        </div>
        <div style="display: block;" class="col-md-2 display-no">
          <div style="position: static;" class="form-group">
            <label for="input-text-2">To Date</label>
            <input class="form-control date" id="to_date" placeholder="<?php echo date('d-m-Y'); ?>" value="<?php echo (!empty($till_date)) ? $till_date : date('d-m-Y'); ?>" type="text"  readonly="true">
          </div>
        </div>
        <!-- added by prabhat -->
        <div style="display: block;" class="col-md-2 display-no">
          <div style="position: static;" class="form-group">
            <label for="input-text-2">Service Type</label>
            <select class="form-control" name="report_type" id="report_type">
              <option value="all">All</option>
              <?php   if(!empty($service_type) ) { 
                foreach($service_type as $key=>$value) { ?>
                <option value="<?php echo $value['id']; ?>" <?php  if($value['id'] == '2'){echo " style='display:none'";}?>><?php echo $value['service_name']; ?></option>
                <?php } 
              } 
              ?>
            </select>
          </div>
        </div>
        <!-- end by prahbt -->

        <!-- added by Sonali -->
        <div style="display: block;" class="col-md-3 display-no">
          <div style="position: static;" class="form-group">
            <label for="input-text-2">Level</label>
            <?php  $role_id = $this->session->userdata(role_id);?>
            <select class="form-control" name="level_type" id="level_type">
              <option value="all">All</option>  
              <option value="<?php echo MASTER_DISTRIBUTOR_ROLE_ID;?>" <?php if($role_id == MASTER_DISTRIBUTOR_ROLE_ID || $role_id > MASTER_DISTRIBUTOR_ROLE_ID) {echo 'style=display:none';} ?>><?php echo 'Regional Distributor';?></option>
              <option value="<?php echo AREA_DISTRIBUTOR_ROLE_ID;?>" <?php if($role_id == AREA_DISTRIBUTOR_ROLE_ID || $role_id > AREA_DISTRIBUTOR_ROLE_ID) {echo 'style=display:none';} ?>><?php echo 'Divisional Distributor';?></option>
              <option value="<?php echo DISTRIBUTOR;?>" <?php if($role_id == DISTRIBUTOR || $role_id > DISTRIBUTOR) {echo 'style=display:none';} ?>><?php echo 'Executive';?></option>
              <option value="<?php echo RETAILER_ROLE_ID;?>" <?php if($role_id == RETAILER_ROLE_ID || $role_id > RETAILER_ROLE_ID) {echo 'style=display:none';} ?>><?php echo 'Sales Agent';?></option>
            </select>
          </div>     
        </div>

        <div style="display: none;" class="col-md-3 display-no level">
          <div style="position: static;" class="form-group">
            <label for="input-text-2">Level Names</label>

            <select class="form-control" name="level_names" id="level_names">
              <option value="all">All</option>  

            </select>
          </div>
        </div>
        <!-- end by sonali -->
      </div>     
      <div class="row"> 
       <div style="display: block;" class="col-md-1 display-no">
        <div style="position: static;" class="form-group">
         <button class="btn btn-primary" name="op_generate_btn" id="op_generate_btn" type="submit"><span></span>Search</button>
       </div>
     </div>
     <div style="display: block;" class="col-md-1 display-no">
      <div style="position: static;" class="form-group">
       <button class="btn btn-primary" name="reset_btn" id="reset_btn" type="reset">Reset</button>
     </div>
   </div>
   <div style="display: block;" class="col-md-2 display-no">
    <div style="position: static;" class="form-group">
     <button  class="btn btn-primary" id="extoexcel" type="button">
       <span class="glyphicon glyphicon-export"></span>
       Export to Excel</button>  
     </div>
   </div>
   <div style="display: block;" class="col-md-2 display-no">
    <div style="position: static;" class="form-group">
     <button  class="btn btn-primary" id="extopdf" type="button">
       <span class="glyphicon glyphicon-export"></span>
       Export to Pdf</button>  
     </div>
   </div>
 </div>
</div>
<br>  
<div class="row">
  <div class="col-xs-12">
   <div class="box">
    <div class="box-body" id="table_show">
      <table id="op_rept" name="op_rept" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th>Sr. No.</th>
            <th>Sales Agent</th>
            <th>Agent Code</th>
            <th>Service Name</th>
            <th>RD Name</th>
            <th>DD Name</th>
            <th>Executive name</th>
            <th>City</th>
            <th>State</th>
            <th>Commission Calculation Date</th>         
            <th>Commission Amount (Rs.)</th>
          </tr>
          <tbody>                                           
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</form>
</div>   
</div>
</div>
</section>

<script>
  var oData;
  $(document).ready(function() {
   var tconfig = {
     "processing": true,
     "serverSide": true,
     "iDisplayLength": 10,
     "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
     "ajax": {
       "url": BASE_URL+"report/report/daily_activeUserDetails",
       "type": "POST",
       "data": "json",
       data   : function (d) {
         d.from_date = $("#from_date").val();
         d.to_date = $("#to_date").val();
             d.report_type = $("#report_type").val(); // added by lilawati
             d.level_type = $("#level_type").val(); // added by sonali
             d.level_names = $("#level_names").val(); // added by sonali 
           }
         },
         "columnDefs": [
         {"bVisible": false, "aTargets": [1,2]},  
         ],
         "scrollX": true,
        // "searching": true,
        "bFilter": false,
        "bPaginate": true,
        "bRetrieve": true,
        "aoColumnDefs": [
        
        {
         "bSortable": false,  
         "aTargets": [0]
       },
       ],
       "fnRowCallback": function(nRow, aData, iDisplayIndex) {
         var info = $(this).DataTable().page.info();  
         $("td:first", nRow).html(info.start + iDisplayIndex + 1);         
         return nRow;
       },
       fnDrawCallback: function(data)
       {

         if (oData.fnGetData().length == 0)
         {
           $("#extoexcel").attr("disabled", "disabled"); 

         } else {
          $("#extoexcel").removeAttr("disabled"); 

        }if (oData.fnGetData().length == 0)
         {
           $("#extopdf").attr("disabled", "disabled"); 

         } else {
          $("#extopdf").removeAttr("disabled"); 

        }
      },
      "oLanguage": {
       "sProcessing": '<img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>'
     },
   };
    oData = $('#op_rept').dataTable(tconfig);          
    $(document).off('click', '#op_generate_btn').on('click', '#op_generate_btn', function (e) {
        oData.fnDraw();       
    });
    
    $(document).off('click', '#reset_btn').on('click', '#reset_btn', function (e) {
        location.reload(true);
    });

    $( "#from_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        'maxDate': new Date()
    }).on("change",function(){
        $("#to_date").datepicker('option','minDate',$("#from_date").val());
    });

    $( "#to_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        maxDate: new Date()
    }).on("change",function(){
        $("#from_date").datepicker('option','maxDate',$("#to_date").val());
            // $("#to_date").datepicker('option','minDate',new Date());
    });

    $('#extoexcel').on('click', function(e) { 
        var from_date = $("#from_date").val();
        var till_date = $("#to_date").val();
        var report_type = $("#report_type").val(); // added by prabhat
        var level_type = $("#level_type").val(); // added by sonali
        var level_names = $("#level_names").val(); // added by sonali 
        window.location.href=BASE_URL+'report/report/activeUsersDetails_download_excel_pdf?from_date='+from_date+"&to_date="+till_date+"&report_type="+report_type+"&level_names="+level_names+"&level_type="+level_type+"&download_type=EXCEL";  //modify by prabhat 
    });

    $('#extopdf').on('click', function(e) { 
        var from_date = $("#from_date").val();
        var till_date = $("#to_date").val();
        var report_type = $("#report_type").val(); // added by prabhat
        var level_type = $("#level_type").val(); // added by sonali
        var level_names = $("#level_names").val(); // added by sonali 
        window.location.href=BASE_URL+'report/report/activeUsersDetails_download_excel_pdf?from_date='+from_date+"&to_date="+till_date+"&report_type="+report_type+"&level_names="+level_names+"&level_type="+level_type+"&download_type=pdf";  //modify by prabhat 
    });

    $(document).off('change', '#level_type').on('change', '#level_type', function (e) {
        $("#level_names").html("<option value=''>Please wait..</option>");
        var detail = {};
        var div = '';
        var str = "";
        var form = '';
        var ajax_url = 'report/report/get_level_wise_data';
        detail['id'] = $("#level_type").find('option:selected').attr('value');
        if(detail['id']==='all') {
            $(".level").css("display", "none");
        }else {
            $(".level").css("display", "block");
            $("#level_names").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
            get_data(ajax_url, form, div, detail, function (response) {

                if (response.rd.length != 0) {
                    if(detail['id'] == '4'){
                        $("#level_names").html("<option value=''>Select Regional Distributor</option><option value='all'>All</option>");
                    }else if(detail['id'] == '5'){
                        $("#level_names").html("<option value=''>Select Divisional Distributor</option><option value='all'>All</option>");
                    }else if(detail['id'] == '6'){
                        $("#level_names").html("<option value=''>Select Executive</option><option value='all'>All</option>");
                    }else{
                        $("#level_names").html("<option value=''>Select Sales Agent</option><option value='all'>All</option>");
                    }
                    $.each(response.rd, function (i, value) {
                        $("#level_names").append("<option value=" + value.id + ">" + value.display_name + "</option>");
                        $(".chosen_select").trigger("chosen:updated");
                    });
                }else {
                    $("#level_names").html("<option value=''>No Data Found</option>").trigger('chosen:updated');
                }
            }, '', false);
        }
    });
});

</script>
