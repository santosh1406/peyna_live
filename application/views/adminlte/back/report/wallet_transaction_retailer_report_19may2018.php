<section class="content-header">
    <h3>MSRTC Waybill Wise Report</h3>
</section>
 <section class="content">
   <div id="wallet_wrapper">
      <div class="row">
         <div class="col-xs-12">
                 <?php $attributes = array("method" => "POST", "id" => "userrpt",  "name" => "userrpt","onsubmit" => 'return false;');
                            echo form_open("#", $attributes); ?>
                <div class="box" style="padding:10px;">
                    <div class="row">
                        <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                <label for="input-text-1">From Date</label>
                                  <input class="form-control date" id="from_date" placeholder="<?php echo date('d-m-Y'); ?> " type="text" >
                             </div>
                        </div>
                        <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                <label for="input-text-2">To Date</label>
                                    <input type="hidden" name="def_to_date" id="def_to_date" value="<?php echo date('Y-m-d'); ?>">
                                    <input type="hidden" name="def_from_date" id="def_from_date" value="<?php echo date('Y-m-d', strtotime(date('d-m-Y'))); ?>">
                                    <input class="form-control date" id="to_date" placeholder="<?php echo date('d-m-Y'); ?>" type="text">
                            </div>
                        </div>
                        <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group"> 
                                <label for="select-1">Comment Type</label>
                                <select class="form-control" id="comment" name="comment">                                   
                                    <option value="all">All</option>
                                    <?php foreach($comment as $val) {
                                    echo "<option value='".$val['comment']."'>".$val['comment']."</option>";
                                     }?>
                                    </select>
                            </div>
                        </div> 
                    </div>     
                       <div class="row"> 
                         <div style="display: block;" class="col-md-1 display-no">
                            <div style="position: static;" class="form-group">
                                 <button class="btn btn-primary" name="op_generate_btn" id="op_generate_btn" type="submit"><span></span>Search</button>
                            </div>
                         </div>
                         <div style="display: block;" class="col-md-1 display-no">
                            <div style="position: static;" class="form-group">
                                 <button class="btn btn-primary" name="reset_btn" id="reset_btn" type="reset">Reset</button>
                            </div>
                         </div>
                          <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                 <button  class="btn btn-primary" id="extoexcel" type="button">
                                   <span class="glyphicon glyphicon-export"></span>
                                   Export to Excel</button>  
                            </div>
                        </div>
                      </div> 
                  </div>
                   <br>  
                       <div class="row">
                            <div class="col-xs-12">
                                 <div class="box">
                                    <div class="box-body" id="table_show">
                                          <table id="op_rept" name="op_rept" class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Sr. No.</th>
                                                        <th>Txn Date</th>
                                                        <th>User Name</th>            
                                                        <th>Agent Code</th>
                                                        <th>Opening Balance (Rs.)</th>
                                                        <th>Txn Amt (Rs.)</th>
                                                        <th>Closing Balance (Rs.)</th>
                                                        <th>Txn Type</th>
                                                        <th>Waybill No</th>
                                                        <th>Transaction Id</th>
                                                        <th>Topup Amount (Rs.)</th>
                                                        <th>Ticket Sold Amount (Rs.)</th>
                                                        <th>Remaining Balance (Rs.)</th>
                                                        <th>Comment</th>
                                                    </tr>
                                                 <tbody>                                           
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                         </div>
                    </form>
                  </div>   
               </div>
            </div>
      </section>

       <script>
       var oData;
       $(document).ready(function() {
   
        var tconfig = {
        "processing": true,
        "serverSide": true,
        "iDisplayLength": 10,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        "ajax": {
            "url": BASE_URL+"report/ors_report/wallet_trans_retailer",
            "type": "POST",
            "data": "json",
            data   : function (d) {
                        var date = new Date();
                        var from_date;
                        var till_date;
                        var comment;
                        if ($("#from_date").val() == '') {
                            from_date = $("#def_from_date").val();

                        } else {
                            from_date = $("#from_date").val();
                        }
                        if ($("#to_date").val() == '') {
                            till_date = $("#def_to_date").val();
                        } else {
                            till_date = $("#to_date").val();
                        }

                        if($("#comment").val() != 'all') {
                            comment = $("#comment").val();
                        }
                        d.from_date = from_date;
                        d.to_date = till_date;
                        d.comment = comment;                       
                                        
                        d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;                       
                     }
        },
        "columnDefs": [
        {
            "searchable": false
        }
        ],
       "scrollX": true,
       "searching": true,
       "bFilter": false,
       "bPaginate": true,
       "bRetrieve": true,
        "aoColumnDefs": [
       
        {
            "bSortable": false,  
            "aTargets": [0]
        },
       ],
         "fnRowCallback": function(nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();  
            $("td:first", nRow).html(info.start + iDisplayIndex + 1);         
            return nRow;
        },
        fnDrawCallback: function(data)
        {

            if (oData.fnGetData().length == 0)
            {
                $("#extoexcel").attr("disabled", "disabled"); 

            } else {
                 $("#extoexcel").removeAttr("disabled"); 

            }
        },
          "oLanguage": {
                    "sProcessing": '<img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>'
                },
    };
       oData= $('#op_rept').dataTable(tconfig);          
        $(document).off('click', '#op_generate_btn').on('click', '#op_generate_btn', function (e) {
             
         
         oData.fnDraw();       
      });
        $(document).off('click', '#reset_btn').on('click', '#reset_btn', function (e) {
        location.reload(true);
      });
       
        
        $("#from_date" ).datepicker({
            'dateFormat' : 'dd-mm-yy',
            maxDate :0
        }).on("change", function () {
            $('#to_date').datepicker('option', 'minDate', $("#from_date").val());
        });
        
        $( "#to_date" ).datepicker({
            'dateFormat' : 'dd-mm-yy',
            maxDate:0,
            minDate: $("#from_date").val()
        }).on("change", function () {
             $('#from_date').datepicker('option', 'maxDate', $("#to_date").val());
        });
        
       
         $('#extoexcel').on('click', function(e) 
            { 
                var from_date;
                var till_date;
                if ($("#from_date").val() == '') {
                    from_date = $("#def_from_date").val();

                } else {
                    from_date = $("#from_date").val();
                }
                if ($("#to_date").val() == '') {
                    till_date = $("#def_to_date").val();
                } else {
                    till_date = $("#to_date").val();
                }
                 from = from_date;
                 to = till_date;       
                
                 txn_type = $("#txn_type option:selected").val();                   

              window.location.href=BASE_URL+'report/ors_report/wallet_trans_retailer_excel?from_date='+from+"&to_date="+to;   
          });
          

    });
          
</script>

