<style>
    .overlay, .loading-img {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    .overlay {
        z-index: 1010;
        background: rgba(255, 255, 255, 0.7);
    }
    .overlay.dark {
        background: rgba(0, 0, 0, 0.5);
    }
</style>

<section class="content-header">
    <h1>Concession Pass Registration Report</h1>
</section>

<!-- Main content -->
<section class="content">
    <div id="commission_wrapper">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body" id="table_show">

                        <?php
                        $attributes = array('class' => 'form', 'id' => 'trans_list_filter', 'name' => 'trans_list_filter', 'method' => 'post', 'onsubmit' => 'return false;');
                        echo form_open('#', $attributes);
                        
                        
                        ?>
                        <div class="row box-title">
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-1">From Date</label>
                                    <input class="form-control date" id="from_date" placeholder="<?php echo date('d-m-Y'); ?> " value="<?php if (!empty($from_date)) echo $from_date; ?>" type="text" >
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2">To Date</label>
                                    
                                    <input class="form-control date" id="to_date" placeholder="<?php echo date('d-m-Y'); ?>" value="<?php if (!empty($till_date)) echo $till_date; ?>" type="text">
                                </div>
                            </div>
<!--                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2">Region Name:</label>
                                    <select class="form-control" id="region_name" name="region_name">
                                        <option value="all">Select</option>
                                        <?php foreach ($region_name as $key => $value) { ?>
                                            <option value="<?php echo $value['REGION_CD']; ?>" <?php echo set_select('region_name', $value['REGION_CD']); ?> ><?php echo $value['REGION_NM']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2">Division Name:</label>
                                    <select class="form-control" id="devision_name" name="devision_name">
                                        <option value="all">Select</option>
                                        <?php if (isset($division_name)) { ?>
                                            <?php foreach ($division_name as $key => $value) { ?>
                                                <option value="<?php echo $value['DIVISION_CD']; ?>" <?php echo set_select('devision_name', $value['DIVISION_CD']); ?> ><?php echo $value['DIVISION_NM']; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2">Depot Name:</label>
                                    <select class="form-control" id="depot_name" name="depot_name">
                                        <option value="all">Select</option>
                                        <?php if (isset($depot_name)) { ?>
                                            <?php foreach ($depot_name as $key => $value) { ?>
                                                <option value="<?php echo $value['DEPOT_CD']; ?>" <?php echo set_select('depot_name', $value['DEPOT_CD']); ?> ><?php echo $value['DEPOT_NM']; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>-->
                             
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        
                        <div class="row"> 
                            <div style="display: block;" class="col-md-1 display-no">
                                <div style="position: static;" class="form-group">
                                    <button class="btn btn-primary" name="card_generate_btn" id="card_generate_btn"  type="submit"><span></span>Search</button>
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-1 display-no">
                                <div style="position: static;" class="form-group">
                                    <button class="btn btn-primary" name="reset_btn" id="reset_btn" type="reset">Reset</button>
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <button  class="btn btn-primary" id="export_to_excel" type="button">
                                        <span class="glyphicon glyphicon-export"></span>
                                        Export to Excel</button>  
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                    <div style="display: none" id="loader" class="overlay"><i class="fa fa-refresh fa-spin"></i></div>
                    <table id="card_data" name="card_data" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Sr. No.</th>
                                <th>Agent Code</th>     
                                <th>Agent Name</th>
                                <th>Depot Code</th>
                                <th>Depot Name</th>
                                <th>Registration Date</th>
                                <th>Concession Code</th>
                                <th>Concession Name</th>
                                <th>Customer Name</th>
                                <th>Age</th>
                                <th>Card Id</th>
                                <th>KM Limit</th>
                                <th>Issuace Date</th>
                                <th>Card Validity in Year</th>
                                <th>Expiry Date</th>
                                <th>Smartcard Fee</th>
                                <th>Application Fee</th>
                                <th>GST</th>
                                <th>Total Received</th>
                            </tr>
                        </thead>
                       <tbody>
                                        <?php $total_amt = $total_scf_amt = $total_af_amt= $total_gst_amt = 0.00;?>
                                        <?php 
                                            function calculateAge($dob) {
                                                        $datetime1 = new DateTime($dob);
                                                        $datetime2 = new DateTime(TODAY_DATE);
                                                        $interval = $datetime1->diff($datetime2);
                                                        $age = $interval->format('%y');
                                            return $age;
                                            }
                                            foreach ($report_data as $key => $value) {
                                        ?>
                                            <tr>
                                                <td><?php echo ++$key; ?></td>
                                                <td><?php echo $value['agent_code']; ?></td>
                                                <td><?php echo $value['agent_name']; ?></td>
                                                <td><?php echo $value['DEPOT_CD']; ?></td>
                                                <td><?php echo $value['DEPOT_NM']; ?></td>
                                                
                                                
                                                <td><?php echo $value['reg_date']; ?></td>
                                                <td><?php echo $value['concession_cd']; ?></td>
                                                <td><?php echo $value['concession_nm']; ?></td>
                                                <td><?php echo $value['name_on_card']; ?></td>
                                                <td><?php echo calculateAge($value['date_of_birth']); ?></td>
                                                <td><?php echo $value['cust_card_id']; ?></td>
                                                <td><?php echo $value['kms_limit']; ?></td>
                                                <td><?php echo $value['dispatch_date']; ?></td>
                                                <td><?php echo $value['card_validity']; ?></td>
                                                <td><?php echo $value['exp_date']; ?></td>
                                                <td><?php echo $value['smart_card_fee']; ?></td>
                                                <td><?php echo $value['application_fee']; ?></td>
                                                <td><?php echo $value['gst_fee']; ?></td>
                                                <td><?php echo $value['total_amount']; ?></td>
                                            </tr>
                                            <?php 
                                                $total_amt += $value['total_amount'];
                                                $total_scf_amt += $value['smart_card_fee'];
                                                $total_af_amt += $value['application_fee'];
                                                $total_gst_amt += $value['gst_fee'];
                                            ?>
                                        <?php } ?>
                                    </tbody>
                                   <tfoot>
                                        <tr>
                                            <th colspan="11" style="text-align:right"> Total : </th>
                                            <td><?php echo number_format($total_scf_amt, 2, '.', '');?></td>
                                            <td><?php echo number_format($total_af_amt, 2, '.', '');?></td>
                                            <td><?php echo number_format($total_gst_amt, 2, '.', '');?></td>
                                            <td><?php echo number_format($total_amt, 2, '.', '');?></td>
                                        </tr>
                                    </tfoot>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</section>


<script>

$(document).ready(function() {
    
    $('input#from_date, input#to_date').bind('copy paste', function(e) {
        e.preventDefault();
    });

    $('#topup_data_filter input').unbind();

    $('#topup_data_filter input').bind('keypress', function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            oData.fnFilter(this.value);
        }
    });

    $(document).off('click', '#reset_btn').on('click', '#reset_btn', function(e) {
        location.reload(true);
    });

    $("#from_date").datepicker({
          'dateFormat': 'dd-mm-yy',
          autoclose: true,     
    });

    $("#to_date").datepicker({
          'dateFormat': 'dd-mm-yy',
          autoclose: true,
    });

    function createDatatable() {
        var datatable = $('#card_data').DataTable({
            "scrollX": true
        });
        datatable.draw();
    }

    createDatatable();
    
    $('#export_to_excel').on('click', function(e) {
        var from_date = $("#from_date").val();
        var to_date = $("#to_date").val();
        
        var flag = 'conc_report'; 
        window.location.href = BASE_URL + "report/smart_card_report/concession_pass_reg_excel?flag="+flag+"&from_date="+from_date+"&to_date="+to_date; 
    });

    $(document).off('click', '#card_generate_btn').on('click', '#card_generate_btn', function (e) {
        document.getElementById("loader").style.display = "block";
        $('#card_generate_btn').prop('disabled', true);
        var from_date = $("#from_date").val();
        var to_date = $("#to_date").val();
        

        $.ajax({
            "type": "POST",
            "url": BASE_URL + "report/smart_card_report/concession_pass_reg_list",
            "data": {from_date:from_date,to_date:to_date},
            success: function (strJSON) {

                var objJSON = JSON.parse(strJSON);
                var row;
                var i = 0;
                var total_amt = 0.00;
                var total_scf_amt = 0.00;
                var total_af_amt = 0.00;
                var total_gst_amt = 0.00;
                $('#card_data').DataTable().destroy();
                $('#card_data tbody').empty();
                $('#card_data tfoot tr').empty();
                $.each(objJSON['data'], function(index, value) {
                    i++;

                    row += ("<tr><td>"+i+"</td><td>"+value['agent_code']+"</td><td>"+value['agent_name']+"</td><td>"+value['DEPOT_CD']+"</td><td>"+value['DEPOT_NM']+"</td><td>"+value['reg_date']+"</td><td>"+value['concession_cd']+"</td><td>"+value['concession_nm']+"</td><td>"+value['name_on_card']+"</td><td>"+calculateAge(value['date_of_birth'])+"</td><td>"+value['cust_card_id']+"</td><td>"+value['kms_limit']+"</td><td>"+value['dispatch_date']+"</td><td>"+value['card_validity']+"</td><td>"+value['exp_date']+"</td><td>"+value['smart_card_fee']+"</td><td>"+value['application_fee']+"</td><td>"+value['gst_fee']+"</td><td>"+value['total_amount']+"</td></tr>");
  //row += ("<tr><td>"+i+"</td><td>"+value['reg_date']+"</td><td>"+value['concession_cd']+"</td><td>"+value['concession_nm']+"</td><td>"+value['name_on_card']+"</td><td>"+calculateAge(value['date_of_birth'])+"</td><td>"+value['cust_card_id']+"</td><td>"+value['kms_limit']+"</td><td>"+value['dispatch_date']+"</td><td>"+value['card_validity']+"</td><td>"+value['exp_date']+"</td><td>"+value['smart_card_fee']+"</td><td>"+value['application_fee']+"</td><td>"+value['gst_fee']+"</td><td>"+value['total_amount']+"</td></tr>");
                    
                    total_amt = parseFloat(value['total_amount']) + parseFloat(total_amt);
                    total_scf_amt = parseFloat(value['smart_card_fee']) + parseFloat(total_scf_amt);
                    total_af_amt = parseFloat(value['application_fee']) + parseFloat(total_af_amt);
                    total_gst_amt = parseFloat(value['gst_fee']) + parseFloat(total_gst_amt);
                });
                var tfooter = "<th colspan='11' style='text-align:right'> Total : </th><td>"+total_scf_amt.toFixed(2)+"</td><td>"+total_af_amt.toFixed(2)+"</td><td>"+total_gst_amt.toFixed(2)+"</td><td>"+total_amt.toFixed(2)+"</td>";
                $('#card_data tbody').append(row);
                $('#card_data tfoot tr').append(tfooter);
                createDatatable();
                if(strJSON != ''){
                    $('#depoDetails').show();
                    $('#depoDetails').empty();
                    var dt = "<p><b> Depot Code</b> : <?php echo $_SESSION['location_cd'] ?> </p>"+
                    "<p><b>Running Date & Time</b> : <?php echo date('d-m-Y h:i:s a'); ?></p>";
                    $('#depoDetails').append(dt);
                }
                document.getElementById("loader").style.display = "none";
                $('#card_generate_btn').prop('disabled', false);
            }
        });
    });

    function calculateAge(dob) {
        var birthdate = new Date(dob);
        var cur = new Date();
        var diff = cur-birthdate;
        var age = Math.floor(diff/31536000000);
        return age;
    }
});
</script>



