<script type="text/javascript">
    var role_name = '0';
<?php if (in_array(strtolower($this->session->userdata('role_name')), array('admin', 'super admin'))) { ?>
        role_name = '1';
<?php } ?>
</script>
<style>
    .fields{display:none;}
</style>
<section class="content-header">
    <h1>Transaction Details</h1>

</section>

<!-- Main content -->
<section class="content">
    <div id="commission_wrapper">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body" id="table_show">

                        <?php
                        $attributes = array('class' => 'form', 'id' => 'trans_list_filter', 'name' => 'trans_list_filter', 'method' => 'post', 'onsubmit' => 'return false;');
                        echo form_open('', $attributes);
                        ?>
                        <div class="row box-title">

                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-1">From Date</label>
                                    <input class="form-control date" id="from_date" placeholder="<?php echo date('d-m-Y'); ?> " value="<?php if (!empty($from_date)) echo $from_date; ?>" type="text" >
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2">To Date</label>
                            <input type="hidden" name="def_to_date" id="def_to_date" value="<?php echo date('Y-m-d'); ?> ">
                           <input type="hidden" name="def_from_date" id="def_from_date" value="<?php echo date('Y-m-d', strtotime(date('d-m-Y'))); ?>">
                                
                         <input class="form-control date" id="to_date" placeholder="<?php echo date('d-m-Y'); ?>" value="<?php if (!empty($till_date)) echo $till_date; ?>" type="text">
                                </div>
                            </div>

                            <div style="display: block;" class="col-md-1 display-no">
                                <div style="position: static;" class="form-group">
                                    <button   class="btn btn-primary" name="search" id="search" type="submit"><span></span>Search</button>
                                </div>
                            </div>
                            
                           
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <button  class="btn btn-primary" id="extoexcel" type="button">
                                        <span class="glyphicon glyphicon-export"></span>
                                        Export to Excel</button>  
                                </div>
                            </div>
                            

                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                    </div>
                    <?php echo form_close(); ?>

                    <table id="tran_list" name="tran_list" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th  width="10%">Sr.No</th>
                                <th>Transaction Id</th>
                                <th>Transaction Mode</th>
                                <th>Mobile Number</th>
                                <th>IFSC Code</th>
                                <th>Account Number</th>
                                <th>Beneficiary Name</th>
                                <th>Transaction Amount</th>
                                <th>Customer Name</th>
                                <th>Transaction Date</th>
                                <th>Status</th>
                                <th>Convenience charge</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>



                </div>
            </div>

        </div>
    </div>
</div>
</section>


<script type="text/javascript">
    $(document).ready(function() {
        var tconfig = {
            "processing": true,
            "serverSide": true,
            "iDisplayLength": 10,
            "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
            "ajax": {
                "url": BASE_URL + "report/Dmt/transaction",
                "type": "POST",
                "data": "json",
                data   : function(d) {
                      var date = new Date();
                    var from_date;
                    var till_date;
                    if ($("#from_date").val() == '') {
                        from_date = $("#def_from_date").val();

                    } else {
                        from_date = $("#from_date").val();
                    }
                    if ($("#to_date").val() == '') {
                        till_date = $("#def_to_date").val();
                    } else {
                        till_date = $("#to_date").val();
                    }
                    d.from_date = from_date;
                    d.to_date = till_date;
                    
                }
            },
            "columnDefs": [
                {
                    "searchable": false
                }
            ],
            "order": [[3, "desc"]],
            "scrollX": true,
            "searching": true,
            "bFilter": false,
            "bPaginate": true,
            "bDestroy": true,
            "aoColumnDefs": [
                {
                    "bSortable": false,
                    "aTargets": [0]
                },
                {
                    "bSearchable": false,
                    "aTargets": [0, 3, 4, 5, 6, 7, 8, 9]
                },
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                // console.log(aData);
                var info = $(this).DataTable().page.info();
                $("td:first", nRow).html(info.start + iDisplayIndex + 1);
                return nRow;
            },
            fnDrawCallback: function(data) {
                //*************** THIS ONE ***********************
                if (oData.fnGetData().length == 0)
                {
                    $("#extoexcel").attr("disabled", "disabled");

                } else {
                    $("#extoexcel").removeAttr("disabled");

                }
               //*************** THIS ONE ***********************
            },
            "oLanguage": {
                "sProcessing": '<div id="loader" class="overlay"><i class="fa fa-refresh fa-spin"></i></div>'
            },
        };

        oData = $('#tran_list').dataTable(tconfig);


        $(document).off('click', '#search').on('click', '#search', function(e) {
            oData.fnDraw();
        });
        $("#from_date").datepicker({
            'dateFormat': 'dd-mm-yy',
            maxDate: 0
        }).on('change', function() {
            //$(this).valid();  // triggers the validation test
            // '$(this)' refers to '$("#datepicker")'
        });

        $("#to_date").datepicker({
            'dateFormat': 'dd-mm-yy',
            maxDate: 0
        }).on('change', function() {
            //$(this).valid();  // triggers the validation test
            // '$(this)' refers to '$("#datepicker")'
        });
        //*************** THIS ONE ***********************
        $('#extoexcel').on('click', function(e)
        {
            var from_date;
            var till_date;
            if ($("#from_date").val() == '') {
                from_date = $("#def_from_date").val();

            } else {
                from_date = $("#from_date").val();
            }
            if ($("#to_date").val() == '') {
                till_date = $("#def_to_date").val();
            } else {
                till_date = $("#to_date").val();
            }
            from = from_date;
            to = till_date;
            window.location.href = BASE_URL + 'report/Dmt/transaction_list_reports_excel?from_date=' + from + "&to_date=" + to+"";
        });
      //*************** THIS ONE ***********************

    });
</script>


