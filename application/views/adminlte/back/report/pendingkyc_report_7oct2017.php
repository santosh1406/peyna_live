
<style>
label.error, div.error, ul.error, span.error {
    color: #f00;
    font-size: 14px;
}
</style>
<section class="content-header">
    <h3>User Pending KYC Report</h3>
</section>

<section class="content">
  <div id="wallet_wrapper">
    <div class="row">
      <div class="col-xs-12">
        <?php $attributes = array("method" => "POST", "id" => "userrpt",  "name" => "userrpt","onsubmit" => 'return false;');
            echo form_open("#", $attributes); ?>
          <div class="box">
            <div class="row-fluid">
                      <table class="table table-bordered table-hover">
                            <tr>
                                <th>
                                   <label>From Date</label>
                                   <input type="text" class="form-control" id="from_date" name="from_date" placeholder="<?php echo date('d-m-Y'); ?> " >
                               </th>  
                               <th>
                                    <label>To Date</label>
                                     <input type="hidden" name="def_to_date" id="def_to_date" value="<?php echo date('Y-m-d'); ?> ">
                                        <input type="hidden" name="def_from_date" id="def_from_date" value="<?php echo date('Y-m-d', strtotime(date('d-m-Y'))); ?>">
                                    <input type="text" class="form-control" id="to_date" name="to_date"  placeholder="<?php echo date('d-m-Y'); ?>" >
                               </th>   
                               <th>
                                 <div style="display: block;" class=" display-no">  
                                    <label for="select-1">User Type</label>
                                    <select class="form-control" id="user_type" name="user_type">
	                                    <option value="all">All</option>                                    
	                                    <?php if($this->session->userdata('level') < COMPANY_LEVEL) {
	                                    echo "<option value='".COMPANY_ROLE_ID."' >Company</option>";
	                                    }?>
	                                    
	                                    <?php  if($this->session->userdata('level') < MASTER_DISTRIBUTOR_LEVEL){
	                                    echo "<option value='".MASTER_DISTRIBUTOR_ROLE_ID."' >Master Distributor</option>";
	                                    }?>
	                                    
	                                    <?php if($this->session->userdata('level') < AREA_DISTRIBUTOR_LEVEL) {
	                                    echo "<option value='".AREA_DISTRIBUTOR_ROLE_ID."' >Area Distributor</option>";
	                                    }?>
	                                    
	                                    <?php if($this->session->userdata('level') < POS_DISTRIBUTOR_LEVEL){
	                                    	echo "<option value='".DISTRIBUTOR."' >Agent</option>";
	                                    }?>                                  
	                                    <option value="<?php echo RETAILER_ROLE_ID; ?>">Sales Executive</option>
                                   </select>
                               </div>
                               </div>
                               </th>
                             </tr>
                      </table> 
                 </div>
            </div>
              <div class="clearfix" style="height: 10px;clear: both;"></div>  
                 <div class="box-footer2">
                    <div class="pull-left">
                        <button class="btn btn-primary" name="op_generate_btn" id="op_generate_btn" type="submit">View</button>
                        <button class="btn btn-primary" name="reset_btn" id="reset_btn" type="reset">Reset</button>
                   </div>
                   <div class="pull-right">
                       <button  class="btn btn-primary margin prin btn-sm print" id="extoexcel" type="button">
                       <span class="glyphicon glyphicon-export"></span>Export To Excel</button>                                      
                  </div>
               </div>                    
               <br>  
               <div class="row">
                  <div class="col-xs-12">
                     <div class="box">
                         <div class="box-body" id="table_show">
                             <table id="op_rept" name="op_rept" class="table table-striped">
                                 <thead><tr>
                                        <th>id</th>
                                        <th>Registration Date</th>
                                        <th>User Name</th>
                                        <th>State</th>
                                        <th>City</th>
                                        <th>Mobile No</th>
                                        <th>Email</th>
                                  <tbody>                                           
                              </tbody>
                          </table>
                      </div>
                 </div>
             </div>
         </div>
       <div class="clearfix" style="height: 10px;clear: both;"></div>   
    <?php echo form_close(); ?>
  </div>   
</div>
       
</section>
<script>
var tkt_data;
$(document).ready(function() {
             
  var tconfig = {
    "processing": true,
    "serverSide": true,
    "searching": true,
    "iDisplayLength": 25,
    "aLengthMenu": [[5, 10, 25,50, -1], [5, 10, 25,50, "All"]],
    "order": [[1, "desc"]],
    "ajax": {
      "url": BASE_URL+"report/report/list_of_pending_kyc",
      "type": "POST",
      "data": "json",
      data   : function (d) {
//        d.from_date   =  $("#from_date").val();
//        d.to_date     =  $("#to_date").val();
                        var date = new Date();
                        var from_date;
                        var to_date;
                        if ($("#from_date").val() == '') {
                        from_date = $("#def_from_date").val();
                        } else {
                        from_date = $("#from_date").val();
                        }
                        if ($("#to_date").val() == '') {
                        to_date = $("#def_to_date").val();
                        } else {
                        to_date = $("#to_date").val();
                        }
        d.from_date = from_date;
        d.to_date = to_date;
        d.user_type   =  $("#user_type").val();
        d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;                        
      }
    },
    "columnDefs": [
      {
        "searchable": false
      }
    ],
    "scrollX": true,
    "bFilter": false,
    "bPaginate": true,
    "bRetrieve": true,
    "aoColumnDefs": [
      {
        "bSortable": false,  
        "aTargets": [0]
      },
      {
        "bSortable": false,  
        "aTargets": [3]
      },
      {
        "bSortable": false,  
        "aTargets": [4]
      },
      {
        "bSortable": false,  
        "aTargets": [5]
      },
      {
        "bSortable": false,  
        "aTargets": [6]
      },
    ],
    "oLanguage": {
                  "sProcessing": '<img src="<?php echo base_url().BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>' 
                },
  };
  tkt_data= $('#op_rept').dataTable(tconfig);          
  $(document).off('click', '#op_generate_btn').on('click', '#op_generate_btn', function (e) {
    tkt_data.fnDraw();       
  });          
  $('.datesel').datepicker({
    dateFormat: 'dd-mm-yy'
  });
  
      $.validator.addMethod("checkDepend",
        function(value, element) {
        if($("#from_date").val() != "" &&  $("#to_date").val() != "")
        {
            var date_diff = date_operations($("#from_date").val(),$("#to_date").val());
            if(date_diff.date_compare)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }

        },"To date must be greater than from date.");
  
     $('#userrpt').validate({
        ignore: "",
        rules: {
            
            to_date : {
                checkDepend : true
            }
        },
        messages: {
            
            from_date: {
  
            
            }
        }
    });
           $('#extoexcel').on('click', function(e) 
            {
             
              if ($("#from_date").val() == '') {
                  from_date = $("#def_from_date").val();
              } else {
                  from_date = $("#from_date").val();
              }
              if ($("#to_date").val() == '') {
                  to_date = $("#def_to_date").val();
              } else {
                  to_date = $("#to_date").val();
              }
              user_type=    $("#user_type").val();
              window.location.href=BASE_URL+'report/report/pending_kyc_excel?from_date='+from_date+"&to_date="+to_date+"&user_type="+user_type;   
            });

   //post the data to report
   
     $( "#from_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        maxDate :0
    });
    $( "#to_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        maxDate:0
    });
    
      $('#reset_btn').on('click', function(e) 
            {
                    $("#from_date").val('');
                    $("#to_date").val('');
            });
            
    
   
   
  

   
});

</script>