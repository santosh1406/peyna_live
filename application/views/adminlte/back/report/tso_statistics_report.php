<!-- <section class="content" -->
<?php //echo $from_date; die('ddd');?>
    <div class="row">
        <h3 class="box-title">&nbsp;&nbsp;&nbsp;Tso Statistics Details</h3>
        <div class="clearfix" style="height: 10px;clear: both;"></div>
        <?php if ($this->session->flashdata('error')) { ?>
            <div class="alert alert-success alert-dismissable">
                <b>
                    <?php
                    echo $this->session->flashdata('error');
                    ?></b>
                <i class="fa fa-check"></i>
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            </div>
        <?php } ?>
        <div class="col-md-12">
            <div class="box" style="padding:10px;">
            <div class="row">
                 <?php echo form_open(base_url()."report/tso_report/statatics_reports") ;?>
                        <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                <label for="input-text-1">From Date</label>
                                <input class="form-control date" value="<?php echo isset($from_date) ? date('d-m-Y', strtotime($from_date)): date('d-m-Y'); ?>" name="from_date" id="from_date"  placeholder="<?php echo isset($from_date) ? date('d-m-Y', strtotime($from_date)): date('d-m-Y'); ?>" type="text" readonly >
                            </div>
                        </div>
                        <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                <label for="input-text-2">To Date</label>
                                <input class="form-control date" value="<?php echo isset($to_date) ? date('d-m-Y', strtotime($to_date)): date('d-m-Y'); ?>"  name="to_date" id="to_date"  placeholder="<?php echo isset($to_date) ? date('d-m-Y', strtotime($to_date)): date('d-m-Y'); ?>" type="text" readonly>
                            </div>
                        </div>
                        <div style="display: block;margin-top: 2%;" class="col-md-2 display-no">
                               <div class="form-group">
                                   <button class="btn btn-primary" id="op_generate_btn" type="submit"><span></span>Search</button>
                               </div>
                        </div>
                <?php echo form_close(); ?>
                    </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="bs-example">
                        <div class="alert alert-danger alert-error" style="display:none">
                            <strong>Error!</strong> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="commission name" TSO Statatics Details</label>
                    </div>
                    <div class="clearfix" style="height: 20px;clear: both;"><b> &nbsp;&nbsp;Status wise reports </b> </br></div>
                    <div class="clearfix" style="height: 5px;clear: both;"></div>
                    <div class="table-responsive">          
                        <table class="table table-bordered" id="provider-operator-commission">
                            <thead>
                                <tr>
                                    <th>Sr. No.</th>
                                    <th>Status</th>
                                    <th style="text-align:center">Count</th>
                                    <th style="text-align:center">Percentage %</th>
                                </tr>
                            </thead>
                            <tbody> 
                                
                                <?php if($statusWiseReport) {$srn = 1; foreach($statusWiseReport as $key => $val): ?>
                                <tr>
                                    <td><?php echo $srn; ?></td>
                                    <td><?php echo $val['status']; ?></td>
                                    <td style="text-align:center"><?php echo $val['count']; ?></td>
                                    <td style="text-align:center"><?php echo round($val['count']/$totalStatus*100, 2); ?></td>
                                </tr>
                                <?php $srn++; endforeach; }?>
                                <tr>
                                    <td></td>
                                    <td style="color: #000000;">Grand Total</td>
                                    <td style="text-align:center;color: #000000;"><?php echo $totalStatus; ?></td>
                                    <td style="text-align:center;color: #000000;"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                    <div class="clearfix" style="height: 20px;clear: both;"><b> &nbsp;&nbsp;Operators wise reports </b></br></div>
                    <div class="clearfix" style="height: 5px;clear: both;"></div>
                    <div class="table-responsive">          
                        <table class="table table-bordered" id="provider-operator-commission">
                            <thead>
                                <tr>
                                    <th>Sr. No.</th>
                                    <th>Operators</th>
                                    <th style="text-align:center">Recharge Successful</th>
                                    <th style="text-align:center">Recharge Unsuccessful</th>
                                    <th style="text-align:center">Reversed Transaction</th>
                                    <th style="text-align:center">Grand Total</th>
                                    <th style="text-align:center">Success Ratio</th>
                                </tr>
                            </thead>
                            <tbody> 
                                
                                  <?php if(count($operatorWise) >0) { $srn = 1; foreach($operatorWise as $key => $val): ?>   
                                <tr class="">
                                    <td><?php echo $srn; ?></td>
                                    <td><?php echo $key; ?></td>
                                    <td style="text-align:center"><?php echo (isset($val['Recharge Successful']['count']) ? $val['Recharge Successful']['count']: 0); ?></td>
                                    <td style="text-align:center"><?php echo (isset($val['Recharge Unsuccessful']['count']) ? $val['Recharge Unsuccessful']['count']: 0); ?></td>
                                    <td style="text-align:center"><?php echo (isset($val['Request Accepted']['count']) ? $val['Request Accepted']['count']: 0); ?></td>
                                    <td style="text-align:center"><?php echo (isset($val['total']) ? $val['total']: 0); ?></td>
                                    <td style="text-align:center"><?php echo round((isset($val['Recharge Successful']['count']) ? $val['Recharge Successful']['count']: 0)/(isset($val['total']) ? $val['total']: 1)*100, 2); ?></td>
                                </tr>
                                  <?php $srn++; endforeach; } ?>
                                <?php if(count($operatorWise) > 0) : ?>
                                <tr class="">
                                    <td></td>
                                    <td style="color: #000000;">Grand Total</td>
                                    <td style="text-align:center; color: #000000;"><?php echo (isset($goperatorWise['Grand Recharge Successful']) ? $goperatorWise['Grand Recharge Successful']: 0); ?></td>
                                    <td style="text-align:center; color: #000000;"><?php echo (isset($goperatorWise['Grand Recharge Unsuccessful']) ? $goperatorWise['Grand Recharge Unsuccessful']: 0); ?></td>
                                    <td style="text-align:center; color: #000000;"><?php echo (isset($goperatorWise['Grand Recharge in Process']) ? $goperatorWise['Grand Recharge in Process']: 0); ?></td>
                                    <td style="text-align:center; color: #000000;"><?php echo (isset($goperatorWise['grantTotal']) ? $goperatorWise['grantTotal']: 0); ?></td>
                                    <td style="text-align:center; color: #000000;"><?php echo round((isset($goperatorWise['Grand Recharge Successful']) ? $goperatorWise['Grand Recharge Successful']: 0)/(isset($goperatorWise['grantTotal']) ? $goperatorWise['grantTotal']: 1)*100, 2); ?></td>
                                </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    
                    <!--<div class="clearfix" style="height: 20px;clear: both;"><b> &nbsp;&nbsp; Reasons wise reports </b> </br></div>
                    <div class="clearfix" style="height: 5px;clear: both;"></div>
                    <div class="table-responsive">          
                        <table class="table table-bordered" id="provider-operator-commission">
                            <thead>
                                <tr>
                                    <th>Sr. No.</th>
                                    <th>Failure Reasons</th>
                                    <th style="text-align:center">Count of Reason</th>
                                    
                                </tr>
                            </thead>
                            <tbody> 
                                <?php if($reasonsWiseReport) { $srn =1; foreach($reasonsWiseReport as $ke =>$val) :?>
                                <tr class="">
                                    <td><?php echo $srn; ?></td>
                                    <td><?php echo $val['reason']; ?></td>
                                    <td style="text-align:center"><?php echo $val['count']; ?></td>
                                </tr>
                                <?php $srn++; endforeach; }?>
                                <tr class="">
                                    <td></td>
                                    <td style="color: #000000;">Grand Total</td>
                                    <td style="text-align:center; color: #000000;"><?php echo $reasons; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>-->
                    <div class="form-group">
                        <div class="col-lg-offset-4">
                            <a class="btn btn-primary" href="<?php echo base_url('admin/dashboard'); ?>" type="button">Back</a> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 




   <script type="text/javascript">
    $(document).ready(function (e) 
    {
        $("#from_date").datepicker({
            'dateFormat': 'dd-mm-yy',
            maxDate: 0
        }).on("change", function() {
            $('#to_date').datepicker('option', 'minDate', $("#from_date").val());
        });

        $("#to_date").datepicker({
            'dateFormat': 'dd-mm-yy',
            maxDate: 0,
            minDate: $("#from_date").val()
        }).on("change", function() {
            $('#from_date').datepicker('option', 'maxDate', $("#to_date").val());
        });
       
            
        $.validator.addMethod("custom_rule", function(value, element) 
        {
            var dropdown_val = $("#no_of_days").val();
            if((dropdown_val > 0 && dropdown_val <= 180))
            {
               return true;
            }
            else
            {
               return false;
            }
        }, "No of days should be greater than 0 and less than equal to 180");         
    
    
        $("#view_ticket_count_report").submit(function(e) 
        {
            e.preventDefault();
        }).validate({ 
        rules:  
        {   no_of_days : 
                    {
                        required: true,
                        custom_rule : true
                    }
        },
        messages: 
        { 
            no_of_days: { required: "Please enter no of days", }
        },
        submitHandler: function(form,e) 
        { 
            var tconfig = 
            {
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": BASE_URL + "report/Top_defaulter_report/defaulter_details",
                    "type": "POST",
                    "data": "json",
                    data:function(d) 
                    {
                        var date = new Date();
                        var from_date;
                        if($("#from_date").val()=='') 
                        {
                            from_date= $("#def_from_date").val();
                        }
                        else
                        {
                            from_date=$("#from_date").val();
                        }

                        d.from_date   = from_date;
                        d.no_of_days  = $("#no_of_days").val();
                        d.select_referrer=$("#select_referrer option:selected").val();
                        d.state       = $("#select_state").val();
                        d.city        = $("#select_city").val();

                    }
                },
                // "sAjaxSource": BASE_URL+'/data_table_eg/get_list1',
                "iDisplayLength": 10,
                "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
                //        "paginate": true,
                "order": [[2, "desc"]],
                "paging": true,
                "searching": true,
                "aoColumnDefs": [
                {"bVisible": false, "aTargets": [6]}
                  ] , 
                "fnRowCallback": function(nRow, aData, iDisplayIndex) {


                     $("td:first", nRow).html(iDisplayIndex + 1);

                    return nRow;

                },

            };
            
            $("#op_rept").dataTable().fnDestroy();
            var oTable = $('#op_rept').dataTable(tconfig);

            //e.preventDefault();
            //oTable.fnDraw();
            
        }
    });
        
      
    $('#extoexcel').on('click', function(e) 
    {
        var from_date  = $("#from_date").val();
        if($("#from_date").val()=='') 
        {
            from_date= $("#def_from_date").val();
        }
        if($("#no_of_days").val() == '')
        {
            $("#export-error").html('Please enter no of days').show();

        }
        else if ($("#no_of_days").val() < 0  || $("#no_of_days").val() > 180 )
        {
           $("#export-error_greater").html('No of days should be greater than 0 and less than or equal to 180').show();
        }
        else
        {
            var no_of_days = $("#no_of_days").val();
        }
        var state = $("#select_state").val();
        var city = $("#select_city").val();
        
        var select_referrer=$("#select_referrer option:selected").val();
    
        if($("#from_date").val() != '' && $("#no_of_days").val() != '' && ($("#no_of_days").val() > 0  && $("#no_of_days").val() <= 180 ))
        {
          window.location.href=BASE_URL+'report/Top_defaulter_report/defaulter_details_summary_excel?from_date='+from_date+"&no_of_days="+no_of_days+"&select_state="+state+"&select_city="+city+"&select_referrer="+select_referrer;   
        }
    }); 

    $(document).off('change', '.state').on('change', '.state', function(e)
    {
        $("#city").html("<option value=''>Please wait..</option>");
        
        var detail  =   {};
        var div     =  '';
        var str     =  "";
        var form    =  '';
        var ajax_url ='admin/ors_users/get_statewise_city';

        detail['state_id'] = $("#select_state").find('option:selected').attr('value');
        
            $("#select_city").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
            get_data(ajax_url, form, div, detail, function(response)
            {
                if(response.city.length != 0)
                {
                    $("#select_city").html("<option value=''>Select City</option>");
                    $.each(response.city, function(i,value)
                    {
                      $("#select_city").append("<option value="+value.intCityId+">"+value.stCity+"</option>");
                      $(".chosen_select").trigger("chosen:updated");
                    });
                }
                else
                {
                    $("#select_city").html("<option value=''>Select City</option>").trigger('chosen:updated');    
                }
            }, '', false);
    }); 
});
</script>
</section>

