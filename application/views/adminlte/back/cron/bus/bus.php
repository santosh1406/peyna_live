<section class="content">
    <div class="row">
        <div class="col-md-6">
        	<div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Get bus services</h3>
                </div>
				
				<?php
				$attributes = array("method" => "POST", "role" => "form");
				echo form_open(base_url().'cron/bus_api/get_available_services', $attributes);
				?>               
	                <div class="box-body">
	                    
	                    <div class="form-group">
	                        <label>Operator</label>
	                        <select id="operator" name="operator" class="form-control">
                                <option value="2">Upsrtc</option>
                                <option value="3">Msrtc</option>
                                <option value="1">Rsrtc</option>
                            </select>
	                    </div>

	                    <div class="form-group">
	                        <label>From</label>
	                        <input id="from" name="from" type="text" placeholder="From" class="form-control">
	                    </div>

	                    <div class="form-group">
	                        <label>Till</label>
	                        <input id="till" name="till" type="text" placeholder="Till" class="form-control">
	                    </div>


	                </div>
	                <div class="box-footer">
	                    <button class="btn btn-primary" type="submit">Submit</button>
	                </div>
            	</form>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function(){
	var min_date = $.datepicker.parseDate('yy/mm/dd','<?php // echo date("Y/m/d", strtotime($bus_detail["last_date"]."+1day")) ?>');

	

	$('#from').datepicker({
		dateFormat: 'dd-mm-yy',
		// minDate: min_date,
		 onClose: function( selectedDate ) {
				$( "#till" ).datepicker( "option", "minDate", selectedDate );
				$( "#till" ).datepicker( "setDate", selectedDate );
			}
	});
	$('#till').datepicker({
		dateFormat: 'dd-mm-yy',
		// minDate: min_date,
	});
});

</script>
