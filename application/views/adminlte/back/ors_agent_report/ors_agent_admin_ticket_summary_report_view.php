<script type="text/Javascript" src="<?php echo base_url() . COMMON_ASSETS ?>js/jquery.validate.min.js" ></script>
<style>
    label.error, div.error, ul.error, span.error {
        color: #f00;
        font-size: 14px;
    }
    th { white-space: nowrap; }
</style>
<section class="content-header">
    <h3>Ticket Summary Report</h3>
</section>
<!-- Main content -->
<section class="content">
    <div id="wallet_wrapper">
       
        <div class="clearfix" style="height: 10px;clear: both;"></div>
        <div class="row"><div id="showDivAdvSearch" >
                <div class="col-xs-12">
                    <div class="box" style="padding:10px;">
                        <form id="agent_ticket_summary_form" name="agent_ticket_summary_form" onsubmit="return false;">
                            <div class="row box-title" >
                                <div style="display: block;" class="col-md-2 display-no">
                                    <div style="position: static;" class="form-group">
                                        <label for="input-text-1">From Date</label>
                                       <input class="form-control" id="from_date" name="from_date" placeholder="From Date" type="text">
                                    
                                    </div>
                                </div>
                                <div style="display: block;" class="col-md-2 display-no">
                                    <div style="position: static;" class="form-group">
                                        <label for="input-text-2">To date</label>
                                        <input type="hidden" name="def_to_date" id="def_to_date" value="<?php echo date('Y-m-d'); ?> ">
                                        <input type="hidden" name="def_from_date" id="def_from_date" value="<?php echo date('Y-m-d', strtotime(date('d-m-Y'))); ?>">
                                        <input class="form-control " id="till_date" name="till_date" placeholder="Till Date" type="text">
                                       
                                    </div>
                                </div>

                                
                                
                                
										<div class="col-md-2 display-no" style="display: block;" id="mydiv">
                                            <div class="form-group" style="position: static;">
                                                <label for="ticket_type">Type</label>
													<select class="form-control" id="ticket_type" name="ticket_type">
														
														<option value="success">Booked Tickets</option>
														<option value="cancel">Cancelled Tickets</option>
														<option value="all" selected>Both</option>
													</select>
                                            </div>  
                                        </div>
										
										<?php																	
										if($this->session->userdata('role_name') == ORS_AGENT_ROLE_NAME && $multi_office_agent != "")
										{
										?>
										
										<div class="col-md-2 display-no" style="display: block;" id="mydiv">
                                            <div class="form-group" style="position: static;">
                                                <label for="office_select">Office</label>
													<select class="form-control" id="office_select" name="office_select">
														
														<option value="">Select Office</option>
												<?php
												if(sizeof(offices) > 0)
												{
													foreach($offices as $val)
													{
														echo "<option value='".$val['office_code']."'>".ucwords($val['office_name'])."</option>";
													}
												}
												?>														
													</select>
                                            </div>  
                                        </div>
										
										<div class="col-md-2 display-no" style="display: none;" id="ors_agent_user_div">
                                            <div class="form-group" style="position: static;">
                                                <label for="ors_agent_user">Select User</label>
													<select class="form-control" id="ors_agent_user" name="ors_agent_user">
														
														<option value="">Select User</option>													
													</select>
                                            </div>  
                                        </div>
										
										<?php
										}
										?>
										
										<?php 
											eval(SYSTEM_USERS_ARRAY);
											if(in_array(strtolower($this->session->userdata("role_name")), $system_users_array))
											{
											?>
										<div class="col-md-2 display-no" style="display: block;" id="mydiv">
                                            <div class="form-group" style="position: static;">
                                                <label for="">City</label>
												<select name="citynm" id="citynm" class="form-control">
												   <option value="">Please Select City</option> 
													 <?php foreach ($city as $key=>$val) :?>
												   <option value="<?php echo $val['intCityId']?>" <?php echo ($val['intCityId'] == $con_state_city['city'])?"selected":"" ?>><?php echo $val['stCity']; ?></option>
												   <?php endforeach; ?> 
													 
												 </select>	
                                            </div>  
                                        </div> 	
																			
                                   
                                
                                       
										<div class="col-md-2 display-no" style="display: block;" id="mydiv">
                                            <div class="form-group" style="position: static;">
                                                <label for="">Agent Name</label>
												<input class= "form-control" type="text" name="agent_name" id="agent_name">	
                                            </div>  
                                        </div> 	
										<div class="col-md-2 display-no" style="display: block;" id="mydiv">
                                            <div class="form-group" style="position: static;">
                                                <label for="">Mobile No</label>
												<input class= "form-control" type="text" name="agent_mobile" id="agent_mobile">	
                                            </div>  
                                        </div> 	
										<div class="col-md-2 display-no" style="display: block;" id="mydiv">
                                            <div class="form-group" style="position: static;">
                                                <label for="">Email ID</label>
												<input class= "form-control" type="text" name="agent_email" id="agent_email">	
                                            </div>  
                                        </div> 	
										
									</div>
										
																			
                                    
								<?php
									}
                                ?>
								<?php 
								eval(FME_AGENT_ROLE_NAME);
									if(in_array(strtolower($this->session->userdata("role_name")),$fme_agent_role_name))
									{	
								?>	
									<input type="hidden" id= "user_id" name="user_id" value="<?php echo $this->session->userdata['id']; ?>" >		
								<?php				
									}
								?>
								  </div>
								<div style="" class=" display-no">   
									<div style="padding:20px"  class="box-footer2 exports_btn">
										<button type="submit" class="btn btn-primary ">Submit</button>
										
												<button  class=" btn btn-primary" id="extoexcel" type="button">
													<span class="glyphicon glyphicon-export"></span>Export To Excel
												</button> 
								 	   
									 </div>
								 </div>
                                   
                          
						</form>


                        <br><br>
                       <div class="box-body" id="table_show">

                            <table id="op_rept" name="op_rept" class="table table-striped table-responsive">
                                <thead>
                                    <tr>
                                        <th>Sr No</th>
                                        <th>Booking Date</th>
                                        <th>Ticket Count</th>
                                        <th>Passenger Count</th>
                                        <th>Ticket Fare</th>
                                        <th>Tax Amt</th>
                                        <th>Operator Service Charge</th>
                                        <th>Agent Service Charge</th>
                                        <th>Commission</th>
                                        <th>TDS</th>
                                        <th>Total Ticket Fare</th>
                                        <th>Amount Paid</th>
                                        <th>Cancellation Charge</th>    
                                        <th>Refund Amt</th>
                                        <th>Profit Earned</th>
                                        
                                    </tr>
                                </thead>

                                <tbody>                                           

                                </tbody>
                            </table>
                        </div> 
                       
						 
                        
                    </div></div>
					</div>  </div>


            
      
   <div class="clearfix" style="height: 10px;clear: both;"></div>   
</div>


 <script type="text/javascript">

        $(document).ready(function() {
			var date = new Date();
	var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
	
        
        $("#from_date").val($.datepicker.formatDate("dd-mm-yy", firstDay));
	$("#till_date").val($.datepicker.formatDate("dd-mm-yy", date));

			$.validator.addMethod("checkDepend",
            function(value, element) {
				
                if ($("#from_date").val() != "")
                { 
                    var date_diff = date_operations($("#from_date").val(), $("#till_date").val());
                    if (date_diff.date_compare)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                { 
                    return true;
                }

            }, "To Date must be greater than From Date.");

			
			jQuery.validator.addMethod("lettersonly", function(value, element) {
			  return this.optional(element) || /^[a-z]+$/i.test(value);
			}, "Letters only please"); 


            $('#agent_ticket_summary_form').validate({
                rules: {
                    from_date: {
                        required: true
                    },
                    till_date: {
                        checkDepend: true
                    },
					agent_name:{
						lettersonly: true
					},
					agent_mobile:{
						number: true,
						minlength:9,
						maxlength:10
					},
					agent_email:{
						email: true
					},
					
                },
                messages: {
                    from_date: {
                        required: "Please Enter from date"

                    }
                }
            });

            var tconfig = {
                
                "processing": true,
                "serverSide": true,
                "scrollX": true,
                "ajax": {
                    "url": BASE_URL + "ors_agent_report/ors_agent_report/ors_agent_admin_ticket_summary_report_result",
                    "type": "POST",
                    "data": "json",
                    data   : function(d) {
                        var date = new Date();
                        var from_date;
                        var till_date;
						
                        if ($("#from_date").val() == '') {
                            from_date = $("#def_from_date").val();

                        } else {
                            from_date = $("#from_date").val();
                        }
                        if ($("#till_date").val() == '') {
                            till_date = $("#def_to_date").val();
                        } else {
                            till_date = $("#till_date").val();
                        }
						d.user_id=$("#user_id").val();
						d.agent_email=$("#agent_email").val();
						d.agent_mobile=$("#agent_mobile").val();
						d.agent_name=$("#agent_name").val();
						d.citynm = $("#citynm option:selected").val();
                        d.from_date = from_date;
                        d.till_date = till_date;
						d.ticket_type = $("#ticket_type option:selected").val();
						if (d.ticket_type == '') {
                            d.ticket_type = 'all';
                        }					
						
					d.office_select = ($("#office_select option:selected").val() == undefined) ? "" : 
					$("#office_select option:selected").val();
					
					d.ors_agent_user = ($("#ors_agent_user option:selected").val() == undefined) ? "" : 
					$("#ors_agent_user option:selected").val();
					
					d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;
							
					}
                },
                "columnDefs": [
                    {
                        "searchable": false
                    }
                ],
                "iDisplayLength": -1,
                "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
                "bFilter": false,
                "bPaginate": true,
                "bRetrieve": true,
                "order": [[1, "desc"]],
                
                "oLanguage": {
                    "sProcessing": '<img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>'
                },
                "fnRowCallback": function(nRow, aData, iDisplayIndex) {

                    var full_amt = 0;
                    var half_amt = 0;
                    var no_of_psgr = 0;

                    $("td:first", nRow).html(iDisplayIndex + 1);

                    return nRow;

                },
                fnDrawCallback: function(data)
                {

                    if (oTable.fnGetData().length == 0)
                    {
                        $('.print').css('display', 'none');

                    } else {
                        $('.print').css('display', 'inline');

                    }
                },
                

            };


            oTable = $('#op_rept').dataTable(tconfig);
            $('#agent_ticket_summary_form').submit(function(e) {

                oTable.fnDraw();
                // summary_data();

            });

            
            

 $( "#from_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        maxDate :0
        });
        $( "#till_date" ).datepicker({
            'dateFormat' : 'dd-mm-yy',
            maxDate:0
        });
           

         
             
             $('#extoexcel').on('click', function(e) 
            { //alert(4444);
            
				from_date  = $("#from_date").val();
				till_date  = $("#till_date").val();
				ticket_type=$("#ticket_type option:selected").val();
				agent_name=$("#agent_name").val();
				agent_email=$("#agent_email").val();
				agent_mobile=$("#agent_mobile").val();
				citynm = $("#citynm option:selected").val();
				user_id=$("#user_id").val();
				
				office_select = ($("#office_select option:selected").val() == undefined) ? "" : 
				$("#office_select option:selected").val();
				
				ors_agent_user = ($("#ors_agent_user option:selected").val() == undefined) ? "" : 
				$("#ors_agent_user option:selected").val();			
				
              if(from_date==''){
                  var from_date = $("#from_date").attr('placeholder');  
              }
              if(till_date==''){
                  var till_date = $("#till_date").attr('placeholder');  
              }
			 <?php 
											eval(SYSTEM_USERS_ARRAY);
											if(in_array(strtolower($this->session->userdata("role_name")), $system_users_array))
											{
											?>
             window.location.href=BASE_URL+'ors_agent_report/ors_agent_report/agent_admin_ticket_summary_excel?from_date='+from_date+"&till_date="+till_date+"&ticket_type="+ticket_type+"&agent_name="+agent_name+"&agent_email="+agent_email+"&agent_mobile="+agent_mobile+"&citynm="+citynm+"&office_select="+office_select+"&ors_agent_user="+ors_agent_user;
											<?php } else{ ?>
			window.location.href=BASE_URL+'ors_agent_report/ors_agent_report/agent_admin_ticket_summary_excel?from_date='+from_date+"&till_date="+till_date+"&ticket_type="+ticket_type+"&user_id="+user_id+"&office_select="+office_select+"&ors_agent_user="+ors_agent_user;								
										<?php	} ?>
			
			});


        });


        function showAdvSearch()
        {
            $("#showDivAdvSearch").toggle();
        }

        function show(target) 
        {
            document.getElementById(target).style.display = 'block';
        }

        function hide(target) {
            document.getElementById(target).style.display = 'none';
        }		
		
		$(document).on('change', '#office_select', function(){		
			var office_code = $(this).val();			
			$.ajax({
			  method: "POST",
			  url: "<?php echo base_url() ?>ors_agent_report/ors_agent_report/get_ors_agent_users",
			  dataType: "json",
			  data: { agent_office_code: $(this).val() }
			})
			  .done(function( data ) {			
				$('#ors_agent_user').html("");
				if(data.length > 0)
				{					
					$('#ors_agent_user').append($("<option></option>").attr("value","").text("Select User"));		
					$.each( data, function( key, value ) {				
					  $('#ors_agent_user')
					 .append($("<option></option>")
								.attr("value",value.id)
								.text(value.display_name));				
					
					});
					$('#ors_agent_user_div').show();
				}
				else
				{
					$('#ors_agent_user_div').hide();
				}				
			  });			
		});		
    </script>

</section>