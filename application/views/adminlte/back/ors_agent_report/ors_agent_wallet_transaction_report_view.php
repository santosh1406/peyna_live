<script type="text/Javascript" src="<?php echo base_url() . COMMON_ASSETS ?>js/jquery.validate.min.js" ></script>
<style>
    label.error, div.error, ul.error, span.error {
        color: #f00;
        font-size: 14px;
    }
    th { white-space: nowrap; }
</style>
<section class="content-header">
    <h3>Wallet Transaction Report</h3>
</section>
<!-- Main content -->
<section class="content">
    <div id="wallet_wrapper">

        <div class="clearfix" style="height: 10px;clear: both;"></div>
        <div class="row"><div id="showDivAdvSearch" >
                <div class="col-xs-12">
                    <div class="box" style="padding:10px;">
                        <form id="agent_wallet_transactions_form" name="agent_wallet_transactions_form" onsubmit="return false;">
                            <div class="row box-title" >
                                <div style="display: block;" class="col-md-2 display-no">
                                    <div style="position: static;" class="form-group">
                                        <label for="input-text-1">From Date</label>
                                        <input class="form-control" id="from_date" name="from_date" placeholder="From Date" type="text">

                                    </div>
                                </div>
                                <div style="display: block;" class="col-md-2 display-no">
                                    <div style="position: static;" class="form-group">
                                        <label for="input-text-2">To date</label>
                                        <input type="hidden" name="def_to_date" id="def_to_date" value="<?php echo date('Y-m-d'); ?> ">
                                        <input type="hidden" name="def_from_date" id="def_from_date" value="<?php echo date('Y-m-d', strtotime(date('d-m-Y'))); ?>">
                                        <input class="form-control " id="till_date" name="till_date" placeholder="Till Date" type="text">

                                    </div>
                                </div>

                                <div class="container">
                                    <div class="row">

                                        <div class="col-md-2 display-no" style="display: block;" id="mydiv">
                                            <div class="form-group" style="position: static;">
                                                <label for="ticket_type">Type</label>
                                                <select class="form-control" id="ticket_type" name="ticket_type">

                                                    <option value="Credited">Credit</option>
                                                    <option value="Debited">Debit</option>
                                                    <option value="all" selected>Both</option>
                                                </select>
                                            </div>  
                                        </div> 	
                                        <?php
                                            eval(FME_AGENT_ROLE_NAME);
                                            if (in_array(strtolower($this->session->userdata("role_name")), $fme_agent_role_name)) {
                                                ?>	
                                                <input type="hidden" id= "user_id" name="user_id" value="<?php echo $this->session->userdata['id']; ?>" >		
                                                <?php
                                            }
                                        ?>

                                    </div>
                                </div>


                                <div style="" class=" display-no">   
                                    <div style="padding:20px"  class="box-footer2 exports_btn">
                                        <button type="submit" class="btn btn-primary ">Submit</button>

                                        <button  class=" btn btn-primary" id="extoexcel" type="button">
                                            <span class="glyphicon glyphicon-export"></span>Export To Excel
                                        </button> 

                                    </div>
                                </div>

                            </div>
                        </form>


                        <br><br>
                        <div class="box-body table-responsive" id="table_show">

                            <table id="op_rept" name="op_rept" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Sr no.</th>
                                        <th>Wallet Opening Balance</th>
                                        <?php if (!empty($credit_detail)) { ?>
                                                <th>Opening Credit</th>
                                            <?php } ?> 
                                        <th>Type</th>
                                        <th>Txn Type</th>
                                        <th>Txn Amount</th>
                                        <th>Txn Date</th>
                                        <th>Rokad Ref No </th>
                                        <th>Wallet Closing Balance</th>
                                        <?php
                                            if (!empty($credit_detail)) {
                                                ?>
                                                <th>Closing Credit</th>
                                                <th>Outstanding</th>
                                            <?php } ?>
                                        <th>Txn Done By</th>
                                        <th>Booked By</th>
                                        <?php
                                            if ($multioffice_flag[0]['multioffice_flag'] == 'Y') {
                                                ?>
                                                <th>Office Name</th>
                                            <?php } ?>
                                        <th>Transaction Remarks</th>
                                        <th>Wallet Transaction ID</th>

                                    </tr>
                                </thead>

                                <tbody>                                           

                                </tbody>
                            </table>
                        </div> 



                    </div></div></div>


        </div>

    </div>
</section>

<script type="text/javascript">
    var flag = <?php echo (empty($credit_detail)) ? '1' : '0'; ?>;
    var flag2 = <?php echo($multioffice_flag[0]['multioffice_flag'] == 'Y') ? '1' : '0'; ?>;

    $(document).ready(function () {
        var date = new Date();
        var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);


        $("#from_date").val($.datepicker.formatDate("dd-mm-yy", firstDay));
        $("#till_date").val($.datepicker.formatDate("dd-mm-yy", date));

        $.validator.addMethod("checkDepend",
                function (value, element) {

                    if ($("#from_date").val() != "")
                    {
                        var date_diff = date_operations($("#from_date").val(), $("#till_date").val());
                        if (date_diff.date_compare)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return true;
                    }

                }, "To date must be greater than from date.");




        $('#agent_wallet_transactions_form').validate({
            rules: {
                from_date: {
                    required: true
                },
                till_date: {
                    checkDepend: true
                },
            },
            messages: {
                from_date: {
                    required: "Please Enter from date"

                }
            }
        });

        var tconfig = {
            "processing": true,
            "serverSide": true,
            "scrollX": true,
            "ajax": {
                "url": BASE_URL + "ors_agent_report/ors_agent_report/ors_agent_wallet_transaction_result",
                "type": "POST",
                "data": "json",
                data   : function (d) {
                    var date = new Date();
                    var from_date;
                    var till_date;

                    if ($("#from_date").val() == '') {
                        from_date = $("#def_from_date").val();

                    } else {
                        from_date = $("#from_date").val();
                    }
                    if ($("#till_date").val() == '') {
                        till_date = $("#def_to_date").val();
                    } else {
                        till_date = $("#till_date").val();
                    }

                    d.from_date = from_date;
                    d.till_date = till_date;

                    d.flag = flag;
                    d.flag2 = flag2;
                    d.ticket_type = $("#ticket_type option:selected").val();
                    if (d.ticket_type == '') {
                        d.ticket_type = 'all';
                    }
                    d.user_id = $("#user_id").val();
					d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;
                }
            },
            "columnDefs": [
                {
                    "searchable": false
                },
                //{"bVisible": false, "aTargets": [2,9,10]}
            ],
            "iDisplayLength": -1,
            "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
            "bFilter": false,
            "bPaginate": true,
            "bRetrieve": true,
            // "order": [[13, "asc"]],

            "oLanguage": {
                "sProcessing": '<img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>'
            },
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {

                var full_amt = 0;
                var half_amt = 0;
                var no_of_psgr = 0;

                $("td:first", nRow).html(iDisplayIndex + 1);
                return nRow;

            },
            fnDrawCallback: function (data)
            {

                if (oTable.fnGetData().length == 0)
                {
                    $('.print').css('display', 'none');

                } else {
                    $('.print').css('display', 'inline');

                }
            },
        };


        oTable = $('#op_rept').dataTable(tconfig);
        $('#agent_wallet_transactions_form').submit(function (e) {

            oTable.fnDraw();
            // summary_data();

        });




        $("#from_date").datepicker({
            'dateFormat': 'dd-mm-yy',
            maxDate: 0
        });
        $("#till_date").datepicker({
            'dateFormat': 'dd-mm-yy',
            maxDate: 0
        });




        $('#extoexcel').on('click', function (e)
        {

            from_date = $("#from_date").val();
            till_date = $("#till_date").val();
            ticket_type = $("#ticket_type option:selected").val();
            user_id = $("#user_id").val();
            flag = flag;
            flag2 = flag2;
            if (from_date == '') {
                var from_date = $("#from_date").attr('placeholder');
            }
            if (till_date == '') {
                var till_date = $("#till_date").attr('placeholder');
            }
            window.location.href = BASE_URL + 'ors_agent_report/ors_agent_report/agent_wallet_transaction_excel?from_date=' + from_date + "&till_date=" + till_date + "&ticket_type=" + ticket_type + "&user_id=" + user_id + "&flag=" + flag+"&flag2=" +flag2;

        });


    });


    function showAdvSearch()
    {
        $("#showDivAdvSearch").toggle();
    }

    function show(target)
    {
        document.getElementById(target).style.display = 'block';
    }

    function hide(target) {
        document.getElementById(target).style.display = 'none';
    }


</script>

