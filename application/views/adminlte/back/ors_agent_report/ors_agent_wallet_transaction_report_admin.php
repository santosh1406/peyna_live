<script type="text/javascript" src="<?php echo base_url() . COMMON_ASSETS ?>js/jquery.validate.min.js" ></script>
<style>
label.error, div.error, ul.error, span.error {
color: #f00;
font-size: 14px;
}
th { white-space: nowrap; }
</style>
<section class="content-header">
<h3>Wallet Transaction Passbook type report</h3>
</section>
<!-- Main content -->
<section class="content">
<div id="wallet_wrapper">

<div class="clearfix" style="height: 10px;clear: both;"></div>
<div class="row"><div id="showDivAdvSearch" >
		<div class="col-xs-12">
			<div class="box" style="padding:10px;">
				<form id="agent_wallet_transactions_admin_form" name="agent_wallet_transactions_admin_form" onsubmit="return false;">
					<div class="row box-title" >
						<div style="display: block;" class="col-md-2 display-no">
							<div style="position: static;" class="form-group">
								<label for="input-text-1">From Date</label>
							   <input class="form-control" id="from_date" name="from_date" placeholder="From Date" type="text">
							
							</div>
						</div>
						<div style="display: block;" class="col-md-2 display-no">
							<div style="position: static;" class="form-group">
								<label for="input-text-2">To date</label>
								<input type="hidden" name="def_to_date" id="def_to_date" value="<?php echo date('Y-m-d'); ?> ">
								<input type="hidden" name="def_from_date" id="def_from_date" value="<?php echo date('Y-m-d', strtotime(date('d-m-Y'))); ?>">
								<input class="form-control " id="till_date" name="till_date" placeholder="Till Date" type="text">
							   
							</div>
						</div>
						<div class="container">
							<div class="row">
								
								<div class="col-md-2 display-no" style="display: block;" id="mydiv">
									<div class="form-group" style="position: static;">
										<label for="ticket_type">Type</label>
											<select class="form-control" id="ticket_type" name="ticket_type">
												
												<option value="Credited">Credit</option>
												<option value="Debited">Debit</option>
												<option value="all" selected>Both</option>
											</select>
									</div>  
								</div> 	
								<div class="col-md-2 display-no" style="display: block;">
								  <div class="form-group" style="position: static;">
									<label for="select-1">Agent Name</label>
										<select id="select_agent" class="form-control" name="select_agent" >
											 <option value="">Select Agent</option>
											 <?php 
												if (isset($agent_name) && count($agent_name) > 0 ) { 
												foreach ($agent_name as $agent){  ?>

											<option value="<?php if(!empty($agent['id'])) echo $agent['id']?>" <?php if(!empty($agent['id'] && !empty($select_agent)) && $select_agent == $agent['id']) echo "selected";?>>
												<?php if(!empty($agent['display_name'])) echo $agent['display_name']?>
											</option>
										<?php }
										} ?>
									  </select>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group" style="position: static;" >
									<label for="select-1">Agent City</label>
									<select id="select_city" class="form-control" name="select_city">
									<option value="">Select City</option>
									 <?php 
										if (isset($city) && count($city) > 0 ) { 
										foreach ($city as $city){  ?>

									<option value="<?php if(!empty($city['intCityId'])) echo $city['intCityId']?>" <?php if(!empty($city['intCityId']) && !empty($select_city) && $select_city == $city['intCityId']) echo "selected";?>>
										<?php if(!empty($city['stCity'])) echo $city['stCity']?>
									</option>
										<?php }
										} ?>
									</select>
								</div>  
				</div>
				 </div>
				<div class="row">
				<div style="" class=" display-no">   
							<div style="padding:20px"  class="box-footer2 exports_btn">
								<button type="submit" class="btn btn-primary ">View</button>
								<button  class=" btn btn-primary" id="extoexcel" type="button">
									<span class="glyphicon glyphicon-export"></span>Export To Excel
								</button> 
							 </div>
						 </div>
					</div>	   
				
						</div>
						</div>
					
					</div>
				</form>

				<br><br>
			   <div class="box-body table-responsive" id="table_show">

					<table id="op_rept" name="op_rept" class="table table-striped">
						<thead>
							<tr>
								<th>Sr No</th>
								<th>Agent Name</th>
								<th>Email ID</th>
								<th>City</th>
								<th>Wallet Opening Balance</th>
								<th>Opening Credit</th>
								<th>Txn ID</th>
								<th>Txn Type</th>
								<th>Credit</th>
								<th>Debit</th>
								<th>Txn Date</th>
								<th>Rokad Ref No.</th>
								<th>Wallet Closing Balance</th>
								<th>Closing Credit</th>
								<th>Outstanding</th>
								<th>Txn Done By</th>
								<th>Txn Remarks</th>                                     
							</tr>
						</thead>

						<tbody>                                           

						</tbody>
					</table>
				</div> 
			   
				 
				
			</div></div></div>


	
</div>


</section>

<script type="text/javascript">

$(document).ready(function() {

var date = new Date();
var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);


$("#from_date").val($.datepicker.formatDate("dd-mm-yy", firstDay));
$("#till_date").val($.datepicker.formatDate("dd-mm-yy", date));

	$.validator.addMethod("checkDepend",
	function(value, element) {
		
		if ($("#from_date").val() != "")
		{ 
			var date_diff = date_operations($("#from_date").val(), $("#till_date").val());
			if (date_diff.date_compare)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{ 
			return true;
		}

	}, "To date must be greater than from date.");

	
	

	$('#agent_wallet_transactions_admin_form').validate({
		rules: {
			from_date: {
				required: true
			},
			till_date: {
				checkDepend: true
			},
			
			
		},
		messages: {
			from_date: {
				required: "Please Enter from date"

			}
		}
	});

	var tconfig = {
		
		"processing": true,
		"serverSide": true,
		"scrollX": true,
		"ajax": {
			"url": BASE_URL + "ors_agent_report/ors_agent_report/ors_agent_wallet_transaction_admin",
			"type": "POST",
			"data": "json",
			data   : function(d) {
				
				var date = new Date();
				var from_date;
				var till_date;
				
				if ($("#from_date").val() == '') {
					from_date = $("#def_from_date").val();

				} else {
					from_date = $("#from_date").val();
				}
				if ($("#till_date").val() == '') {
					till_date = $("#def_to_date").val();
				} else {
					till_date = $("#till_date").val();
				}
				
				d.from_date = from_date;
				d.till_date = till_date;
				d.ticket_type = $("#ticket_type option:selected").val();
				if (d.ticket_type == '') {
					d.ticket_type = 'all';
				}
				
				d.select_agent=$("#select_agent").val();
				d.select_city=$("#select_city").val();				
				d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;
			}
		},
		"columnDefs": [
			{
				"searchable": false
			}
			
		],
		"iDisplayLength": 5,
		"aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
		"bFilter": false,
		"bPaginate": true,
		"bRetrieve": true,
		"order": [[1, "desc"]],
		
		"oLanguage": {
			"sProcessing": '<img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>'
		},
		"fnRowCallback": function(nRow, aData, iDisplayIndex) {

			var full_amt = 0;
			var half_amt = 0;
			var no_of_psgr = 0;

			$("td:first", nRow).html(iDisplayIndex + 1);
			return nRow;

		},
		fnDrawCallback: function(data)
		{

			if (oTable.fnGetData().length == 0)
			{
				$('.print').css('display', 'none');

			} else {
				$('.print').css('display', 'inline');

			}
		},
		

	};


	oTable = $('#op_rept').dataTable(tconfig);
	$('#agent_wallet_transactions_admin_form').submit(function(e) {
		oTable.fnDraw();
	});

	
	

$( "#from_date" ).datepicker({
'dateFormat' : 'dd-mm-yy',
maxDate :0
});
$( "#till_date" ).datepicker({
	'dateFormat' : 'dd-mm-yy',
	maxDate:0
});
   

 
	 
	 $('#extoexcel').on('click', function(e) 
	{ 
		from_date  = $("#from_date").val();
		till_date  = $("#till_date").val();
		ticket_type=$("#ticket_type option:selected").val();
		if (ticket_type == '') 
		{
		    ticket_type = 'all';
		}
		select_agent=$("#select_agent").val();
		select_city=$("#select_city").val();		
				
		
	  if(from_date==''){
		  var from_date = $("#from_date").attr('placeholder');  
	  }
	  if(till_date==''){
		  var till_date = $("#till_date").attr('placeholder');  
	  }
	  
	 window.location.href=BASE_URL+'ors_agent_report/ors_agent_report/agent_wallet_transaction_admin_excel?from_date='+from_date+"&till_date="+till_date+"&ticket_type="+ticket_type+"&select_agent="+select_agent+"&select_city="+select_city;
	
	});


});


</script>

