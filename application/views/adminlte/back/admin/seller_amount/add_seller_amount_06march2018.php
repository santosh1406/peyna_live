<script>
    $(document).ready(function () {
            /************** for confirm message***********/
        $('.confirm-div').hide();
        <?php if ($this->session->flashdata('invalid')) { ?>
                    $('.confirm-div').html('<?php echo $this->session->flashdata('invalid'); ?>').show();
        <?php } else if ($this->session->flashdata('invalid')) { ?>
                    $('.confirm-div').html('<?php echo $this->session->flashdata('valid'); ?>').show();
        <?php } ?>
            /*********************************************/
   
    $("#add_seller_amount_validate").validate({
			rules: {
				seller_min_amt: {
					required: true,
					digits: true,
					check_numbers:true
				},
				seller_code: {
					required: true
//                                        check_select: ""
				}
			},
                        messages: {
                                seller_code: "Please select Seller Code." 
                        } ,
			errorPlacement: function (error, element) {				
				if (element.attr("name") == "request_to") {				
				error.appendTo( element.closest(".form-group") );
				} else {
				error.appendTo( element.parent("div") );
				}
			},
			submitHandler: function(form) {
				// var amt = $("#amount").val();
				// alert('The amount you are adding in your wallet is = '+amt);
				$("#add_amt").attr("disabled", true).html('Processing...');			
				$('.img-circle').show();
                				
				$.ajax({
					url: form.action,
					type: form.method,
					data: $(form).serialize(),
					async:false,
					dataType:'json',
					success: function(response) {
						console.log(response);
						if (response.flag == '@#success#@')
						{                   
							if(response.msg_type == "success")
							{
								// showLoader(response.msg_type,response.msg);
								$('.img-circle').hide(); 
								swal("Success", response.msg, response.msg_type);
								window.location = "<?php echo base_url() ?>admin/seller_amount";
							}
						}
						else
						{
							if(response.msg_type == "error")
							{
								//showLoader(response.msg_type,response.msg);
								$('.img-circle').hide(); 
								swal("Oops...", response.msg, "error");
								$("#add_amt").attr("disabled", false).html('Submit');
							}
							else
							{
								$('.img-circle').hide(); 
								swal("Oops...", response.msg, "error");
								$("#add_amt").attr("disabled", false).html('Submit');
							}     
						}
					}            
				});
			}			
		});
                $.validator.addMethod("check_numbers", function (value, element) {

                    var flag=false;
                    var TempAmount = parseInt(value);
                    if(TempAmount <=0){
                        flag = false;	
                    }else{
                        flag = true;	
                    }
                    return flag;
                }, 'Amount Should Be Greater Than 0.');
                                
    });
</script> 


<section class="content">
    <div class="row">
        <!-- right column -->
        <div class="col-md-12">
            <!-- general form elements disabled -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Add Seller Amount</h3>
                </div><!-- /.box-header -->
                  
                <?php
                    $attributes = array("method" => "POST", "id" => "add_seller_amount_validate", "class" => "cmxform form");
                    echo form_open('admin/seller_amount/add_seller_amount', $attributes);   
                ?>  
                
<!--                     <div class="form-group">
                                <label class="col-lg-3 control-label" for="card_type"> Card Type *: </label>
                                <div class="col-lg-6">
                                    <select class="form-control " id="card_type"  name="card_type">
                                        <option value="">Select Card Type</option>
                                        <option value="Personalised">Personalised</option>
                                        <option value="Non-Personalised">Non-Personalised</option>
                                    </select>
                                </div>
                            </div>-->
                
                    <div class="form-group">
                        <label class="col-lg-3 control-label" >Seller Code:*</label>
                        <div class="col-lg-6">
                        <!--<select name="seller_code" id="seller_code" class="chosen_select" title="Please select something!">-->
                        <select name="seller_code" id="seller_code" class="form-control" title="Please select something!">
                            <option value ="">Select Seller code</option>
                            <?php foreach($stop_cd as $value){?>
                                <option value="<?php echo $value->BUS_STOP_CD;?>"><?php echo  $value->BUS_STOP_CD;?></option>
                            <?php } ?>
                        </select>
                        </div>    
                    </div>
                    <div class="clearfix" style="height: 40px;clear: both;"></div>
                    <div class="form-group">
                        <label class="col-lg-3">Minimum Amount:*</label>
                        <div class="col-lg-6">
                        <input type="text" class="form-control" placeholder="Min amount" id="seller_min_amt" name="seller_min_amt" data-rule-required="true" data-msg-required="Please enter minimum amount" required/>
                        </div>
                    </div>
                    <div class="clearfix" style="height: 40px;clear: both;"></div>
                    <div class="form-group">
                        <center>
                            <button class="btn btn-primary" id="add_amt" name="add_amt" type="submit">Add Amount</button>
                            <button class="btn btn-primary" id="back_btn" name="back_btn" onclick="history.go(-1);return false;" >Back</button>
                        </center>
                    </div>
                    <div class="clearfix" style="height: 40px;clear: both;"></div>
                <?php echo form_close(); ?>
            </div> 
        </div>
    </div>    
</section>

