<script>
    $(document).ready(function () {
            /************** for confirm message***********/
        $('.confirm-div').hide();
        <?php if ($this->session->flashdata('invalid')) { ?>
                    $('.confirm-div').html('<?php echo $this->session->flashdata('invalid'); ?>').show();
        <?php } else if ($this->session->flashdata('invalid')) { ?>
                    $('.confirm-div').html('<?php echo $this->session->flashdata('valid'); ?>').show();
        <?php } ?>
            /*********************************************/
    

    $("#edit_seller_amount_validate").validate({
			rules: {
				seller_min_amt: {
					required: true,
					digits: true,
					check_numbers:true
				},
				seller_code: {
					required: true
				}
			},
			errorPlacement: function (error, element) {				
				if (element.attr("name") == "request_to") {				
				error.appendTo( element.closest(".form-group") );
				} else {
				error.appendTo( element.parent("div") );
				}
			},
			submitHandler: function(form) {
				// var amt = $("#amount").val();
				// alert('The amount you are adding in your wallet is = '+amt);
				$("#edit_amt").attr("disabled", true).html('Processing...');			
				$('.img-circle').show();		
				$.ajax({
					url: form.action,
					type: form.method,
					data: $(form).serialize(),
					async:false,
					dataType:'json',
					success: function(response) {
						console.log(response);
						if (response.flag == '@#success#@')
						{                   
							if(response.msg_type == "success")
							{
								// showLoader(response.msg_type,response.msg);
								$('.img-circle').hide(); 
                                                                swal({
                                                                        title: "Success",
                                                                        text: response.msg,
                                                                        type: response.msg_type,
                                                                        confirmButtonClass: "btn-success",
                                                                        confirmButtonText: "OK"
                                                                      },
                                                                      function(isConfirm) {
                                                                        if (isConfirm) {
                                                                          window.location = "<?php echo base_url() ?>admin/seller_amount";
                                                                        }
                                                                    });
							}
						}
						else
						{
							if(response.msg_type == "error")
							{
								//showLoader(response.msg_type,response.msg);
								$('.img-circle').hide(); 
								swal("Oops...", response.msg, "error");
								$("#edit_amt").attr("disabled", false).html('Submit');
							}
							else
							{
								$('.img-circle').hide(); 
								swal("Oops...", response.msg, "error");
								$("#edit_amt").attr("disabled", false).html('Submit');
							}     
						}
					}            
				});
			}			
		});
                $.validator.addMethod("check_numbers", function (value, element) {
                    var flag=false;
                    var TempAmount = parseInt(value);
                    if(TempAmount <=0){
                        flag = false;	
                    }else{
                        flag = true;	
                    }
                    return flag;
                }, 'Amount Should Be Greater Than 0.');
                
    });
</script> 


<section class="content">
    <div class="row">
        <!-- right column -->
        <div class="col-md-12">
            <!-- general form elements disabled -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Edit Seller Amount</h3>
                </div><!-- /.box-header -->
                  
                <?php
//                show($seller_amt_info,1);
                    $attributes = array("method" => "POST", "id" => "edit_seller_amount_validate", "class" => "cmxform form");
                    echo form_open('admin/seller_amount/edit_seller_amount/' .$seller_amt_info[0]['seller_amount_id'], $attributes);   
                ?>  
                    
                    <div class="form-group">
                        <label class="col-lg-3" >Seller Code:*</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" placeholder="Min amount" id="seller_code" name="seller_code" value="<?php echo $seller_amt_info[0]['BUS_STOP_CD']; ?>" disabled/>
                        </div>    
                    </div>
                    <div class="clearfix" style="height: 40px;clear: both;"></div>
                    <div class="form-group">
                        <label class="col-lg-3">Minimum Amount:*</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control" placeholder="Min amount" id="seller_min_amt" name="seller_min_amt" value="<?php echo $seller_amt_info[0]['min_amount']; ?>" data-rule-required="true" data-msg-required="Please enter minimum amount" required/>
                        </div>
                    </div>
                    <div class="clearfix" style="height: 40px;clear: both;"></div>
                    <div class="form-group">
                        <center> <button class="btn btn-primary" id="edit_amt" name="edit_amt" type="submit">Save</button>
                            <button class="btn btn-primary" id="back_btn" name="back_btn" onclick="history.go(-1);return false;" >Back</button></center>
                    </div>
                    <div class="clearfix" style="height: 40px;clear: both;"></div>
                <?php echo form_close(); ?>
            </div> 
        </div>
    </div>    
</section>

