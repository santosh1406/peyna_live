<section class="content-header">
    <h1> Seller Amount</h1>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <?php if($this->session->flashdata('msg')){ ?>
                <div id="msg_block" class="col-md-12 error_block <?php if(!empty($error)){ echo 'show';} else{ echo "show";} ?>">
                    <div class="alert alert-success alert-dismissable" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?php echo $this->session->flashdata('msg');  ?>
                    </div>
                </div>
            <?php } ?>
            <div class="box">
                <div class="box-header">
                    <div align="left" style="margin: 20px;">
                        <a class="btn btn-primary"  href="<?php echo site_url() . '/admin/seller_amount/add_seller_amount_view'?>" type="button" style="color: #FFFFFF;">Add Seller Amount </a> 
<!--                    <h4 align="right"><b><?php echo ucfirst($agent_name); ?></b></h4>-->
                    </div>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table id="example2" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>Sr. No.</th>
                                <th>Seller Point Code</th>
                                <th>Seller Point Name</th>
                                <th>Min Amount</th> 
                                <th>Action</th>  
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if (count($seller_amt_info) > 0) {
                                $cnt = 0;
                                    foreach ($seller_amt_info as $sellerinfo) {
                                        $cnt++;
                             ?>
                            <tr>
                            <td><?php echo $cnt; ?></td>
                            <td><?php echo $sellerinfo["BUS_STOP_CD"]; ?></td>
                            <td><?php echo $sellerinfo["BUS_STOP_NM"]; ?></td>
                            <td><?php echo $sellerinfo["min_amount"];  ?></td>
                            <td><a href='<?php echo base_url() . "admin/seller_amount/edit_seller_amount_view/" . $sellerinfo["seller_amount_id"] ?>'>Edit</a>&nbsp;
                                <a href='<?php echo base_url() . "admin/seller_amount/remove_seller_amount/" . $sellerinfo["seller_amount_id"] ?>' class="delete_btn">Delete</a>
                            </td>
                            </tr>
                            <?php 
                                    }
                                } else {
                                    ?>
                                    <tr><td colspan='5'> There is no seller with minimum amount.</td></tr>
                                    <?php 
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.row -->
</section><!-- /.content -->
    <!-- /.content-wrapper -->
<script>
     $(document).ready(function() {
         $(document).off('click', '.delete_btn').on('click', '.delete_btn', function(e) {
            var flag = confirm('Please Confirm To Delete Seller Amount?');

            if(flag)
            {
                return true;
            }

            return false;

        });
     });

</script>    