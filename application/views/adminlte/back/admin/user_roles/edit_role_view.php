

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3>
            &nbsp;&nbsp;Edit User Role
             <!--<small>advanced tables</small>-->
        </h3>
       
    </section>
<?php //show($pro_data,1);?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                 <?php
                            $attributes = array("method" => "POST", "id" => "group_create_form", "class" => "group_create_form");
                            echo  form_open(site_url().'/admin/user_role/update/'.$group->id, $attributes);
                       ?>
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                       
                        <div class="form-group">

                            <label class="col-lg-3 control-label" for="name">Name:</label>
                            <div class="col-lg-6">
                                <input name="role_id" type="hidden" id="role_id" class="form-control" value="<?php echo $group->id?>">
                                <input name="role_name" type="text" id="role_name" class="form-control" value="<?php echo $group->role_name ?>" >
                               
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                       

                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="op_addr_1">Homepage: </label>
                            <div class="col-lg-6">
                                <select name="homepage" class="form-control" id="front_back_access">
                                    <option value=''>Select Homepage</option>
                                    <option value="home" <?php echo ($group->home_page == 'home') ? 'selected': ''?> > Home</option>
                                    <option value='admin/dashboard' <?php echo ($group->home_page == 'admin/dashboard') ? 'selected': ''?>>Admin >> Dashboard</option>
                                </select>
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="op_addr_1">Access: </label>
                            <div class="col-lg-6">
                                <select name="access" class="form-control" id="front_back_access">
                                    <option value=''>Select Access</option>
                                    <option value='front' <?php echo ($group->front_back_access == 'front') ? 'selected': ''?>>Front</option>
                                    <option value="back" <?php echo ($group->front_back_access == 'back') ? 'selected': ''?>> Back</option>
                                    <option value="both" <?php echo ($group->front_back_access == 'both') ? 'selected': ''?>> Both</option>
                                </select>
                            </div>
                        </div>


                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="op_addr_1">Status: </label>
                            <div class="col-lg-6">
                                <select class="form-control" id="status" name="status">
                                    <?php if($group->record_status == 'Y') {?>
                                  <option value="Y" selected="">Active</option>
                                  <option value="N">Deactivate</option>
                                 <?php  }?>
                               <?php if($group->record_status == 'N') {?>
                                  <option value="Y">Active</option>
                                  <option value="N" selected="">Deactivate</option>percentage</option>
                                 <?php  }?>
                                </select>  
                            </div>
                        </div>
                       
                       
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">

                                <button class="btn btn-primary" id="save_group_data" name="save_group_data" type="submit">Save</button> 
                                <button class="btn btn-primary back" id="back_data" type="button">Back</button> 
                            </div>
                        </div>

                         
                    </div>
                <!-- /.box-body -->
                </div></form><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->



<script>

    $(document).ready(function() {

    $(document).off('click', '.back').on('click', '.back', function(e)
    {

        window.location.href = BASE_URL + 'admin/user_role/';
    });
    });
</script>
