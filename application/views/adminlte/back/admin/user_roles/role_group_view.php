<!-- <section class="content-header">
          <h3>
                Roles    
          </h3>
         
        </section> -->
<section class="content-header">
    <h1> Manage Permissions & Role
    </h1>
</section>
                       <!--  <?php
                    if($this->session->flashdata('msg')){ 
                ?>
                <div id="msg_block" class="col-md-12 error_block <?php if(!empty($error)){ echo 'show';} else{ echo "show";} ?>">
                    <div class="alert alert-success alert-dismissable" style ="margin-top:15px">
                        <i class="glyphicon glyphicon-ok"></i>
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?php echo '<b>Success!</b> ' . $this->session->flashdata('msg');  ?>                       
                    </div>
                </div>
                <?php } ?> -->
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <div align="right"><a href="<?php echo site_url() . '/admin/user_role/create_role' ?>" class="btn btn-warning" role="button" style="margin-top: 5px;">Create New Role</a>   
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
            <div class="table-responsive">
               <table id="example2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                     <th>Id</th>
                                     <th>Role Name</th>
                                     <th>Homepage</th>
                                     <th>Status</th>
                                     <th>Access</th>
                                     <th>Permissions</th>
                                     <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1;
                               /* if(isset($groups) && count($groups ) >0){
                                foreach ($groups as $group): 
                                    ?>
                                    <tr><td><?php echo $i; ?></td>
                                        <td><?php echo htmlspecialchars(ucwords($group->name), ENT_QUOTES, 'UTF-8'); ?></td>
                                        <td><?php echo htmlspecialchars(ucwords($group->description), ENT_QUOTES, 'UTF-8'); ?></td>
                                         <td><?php echo ($group->status=='0')?'Active':'Deactive' ; ?></td>
                                        <td><?php echo anchor("admin/permission_menu/index/" . $group->id, '<i class="fa fa-fw fa-list-alt"></i>','title="View Permission"'); ?>
                                            <?php echo anchor("admin/permission_menu/edit/" . $group->id, '<i class="fa fa-edit"></i> ','title="Edit Permission"'); ?>
                                                <?php //echo anchor("admin/permission_menu/delete/" .$group->id,'<i class="fa fa-trash per_delete_btn" data-ref="'.$group->id.'" ></i>','title="Delete Permission"'); ?>
                                            
                                        </td>
                                        <td> @if(check_permission('edit') )
                                            <?php echo anchor("admin/group/edit/" . $group->id, '<i class="fa fa-edit"></i>','title="Active/Inactive Role"'); ?>
                                            @endif
                                           
                                        </td>
                                    </tr>
                                    <?php $i++;
                                endforeach;
                                }else{
                                    echo '<tr><td colspan="5">No Data available</td></tr>';
                                }*/
                                ?>


                            </tbody>

               </table></div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
    <!-- /.content-wrapper -->


<script>
  
 $(document).ready(function() {
     var tconfig = {
        // "sDom": '<"toolbar">hlfetlp',
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": BASE_URL+"admin/user_role/list_of_groups",
            "type": "POST",
            "data": "json",
			 data : function(d) {
				 d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;
			}
        },
        "columnDefs": [
        {
            "searchable": false,
            "orderable": false
        }
        ],
        "iDisplayLength": 5,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        "paginate": true,
        "paging": true,
        "searching": true,
        "oLanguage": {
                "sProcessing": '<center><img src="<?php echo base_url().BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/></center>' 
            },
        "aoColumnDefs": [
            {"bSearchable": false, "aTargets": [0,4,5]},
            {"targets":[1,2,3], "orderable": false}
        ],
        "order": [[0, "desc"]],
        "fnRowCallback": function(nRow, aData, iDisplayIndex) {
         
            if (aData[3] == 'Y')
            {

                $("td:eq(3)", nRow).html("Active");
            }
            else
            {

                $("td:eq(3)", nRow).html("Deactive");
            }


            $("td:first", nRow).html(iDisplayIndex + 1);
            return nRow;
        }
        
    };
    var oTable = $('#example2').dataTable(tconfig);
    
    $(document).off('click', '.delete_btn').on('click', '.delete_btn', function(e) {
            var flag = confirm('Please confirm to delete User Role?');

            if(flag)
            {
                return true;
            }

            return false;

    });
});
   setTimeout(function(){
        $('#msg_block').fadeIn('slow');
    },1000);
</script>
