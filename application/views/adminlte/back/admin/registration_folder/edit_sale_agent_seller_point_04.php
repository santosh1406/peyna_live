<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h2 style="margin-left:20px">Change SA Seller Point</h2>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> </div>
                    <div class="box-body">
                        <?php echo validation_errors('<div class="error">', '</div>'); ?>
                        <?php $attributes = array("method" => "POST", "id" => "edit_registration_form", "name" => "edit_registration_form");
                              echo form_open(base_url() . 'admin/registration/update_seller_point', $attributes); ?>

                               <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <div class="form-group"> 
                                        <label class="col-lg-3 control-label" for="distributor">Select Executive *:</label>
                                        <div class="col-lg-6">
                                            <select class="form-control executive  show_ret" name="executive" id="executive">
                                                <option value="">Select Executive</option>
                                                <?php 
                                                if (isset($executive) && count($executive) > 0) {
                                                    foreach ($executive as $executive) {
                                                        ?>
                                                        <option value="<?php if (!empty($executive['id'])) echo $executive['id'] ?>">
                                                        <?php if (!empty($executive['display_name'])) echo $executive['display_name']; ?>
                                                        </option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>   
                                        </div>
                                    </div>

                                     <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <div class="form-group"> 
                                        <label class="col-lg-3 control-label" for="sale_agent">Select Sale Agent *:</label>
                                        <div class="col-lg-6">
                                            <select class="form-control sale_agent" name="sale_agent" id="sale_agent">
                                                <option value="">Select Sale Agent</option>
                                                <!-- <?php
                                               /* if (isset($sale_agent) && count($sale_agent) > 0) {
                                                    foreach ($sale_agent as $sale_agent) {*/
                                                        ?>
                                                        <option value="<?php //if (!empty($sale_agent['id'])) echo $sale_agent['id'] ?>">
                                                        <?php //if (!empty($sale_agent['display_name'])) echo $sale_agent['display_name']; ?>
                                                        </option>
                                                        <?php
                                                    //}
                                                //}
                                                ?> -->
                                            </select>   
                                        </div>
                                    </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                           <!--  <?php //if($show_seller_point==1) { $style='disabled="true"';} else { $style='';} ?> -->
                           <div class="form-group"> 

                                <label class="col-lg-3 control-label" for="agent_state">Seller Point *:</label>
                                <div class="col-lg-6">
                                    <select class="state form-control" name="seller_point" id="seller_point" value="">
                                        <option value="">Select Seller Point</option>
                                           <!--  <?php //if(is_array($sellerpoints)) { ?>
                                            <?php //foreach ($sellerpoints as $value) { ?>
                                                <option value="<?php //echo $value->BUS_STOP_CD; ?>" <?php //echo (($value->BUS_STOP_CD == $edit_data[0]['star_seller_code']) ? 'selected="selected" ' : '' ) ?>> <?php //echo $value->BUS_STOP_NM ?></option>
                                           <?php //} ?> 
                                        <?php //} ?> -->
                                    </select>
                                </div>
                            </div>

                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                           <!--  <?php //if($show_seller_point==1) { $style='disabled="true"';} else { $style='';} ?> -->
                           <div class="form-group"> 

                                <label class="col-lg-3 control-label" for="depot">Depot *:</label>
                                <div class="col-lg-6">
                                    <select class="state form-control" name="depot" id="depot" value="">
                                        <option value="">Select Depot</option>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" name="agent_code" id="agent_code" value="">
                            <div class="clearfix" style="height: 50px;clear: both;"></div>
                                                     
                           <div class="form-group"> 

                                <label class="col-lg-6 control-label" for="popup"></label>
                                <div class="col-lg-3">
                                    <div class="popup" onclick="myFunction()">
                                      <b><h4>Click Here</h4></b>
                                        <center><span class="popuptext" id="myPopup">Precondition :
                                                <ul>
                                                    <li>Waybill must be closed</li>  
                                                    <li>Collection should be done</li>
                                                </ul>
                                                PostCondition :
                                                <ul>
                                                    <li>Previous routes must be erased from machine.</li>
                                                    <li>Routes must be sync in devce.</li>
                                                </ul>
                                          </span></center>
                                    </div>
                                </div>
                            </div>

                         <!--  <div class="danger">
                              <p><strong>Important!</strong> Some text...</p>
                            </div> -->
                            <div class="clearfix" style="height: 10px;clear: both;"></div> 
                                <hr> 
                            <div class="form-group">
                                <div class="col-lg-offset-5">
                                    <button class="btn btn-primary" id="submit_btn" name="submit_btn" type="submit">Save</button> 
                                    <a class="btn btn-primary" href="<?php echo base_url('admin/registration') ?>" type="button">Back</a> 
                                </div>
                            </div>
                        <?php form_close(); ?>
                        <!--</div> /.box-body--> 
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
// When the user clicks on div, open the popup
function myFunction() {
    var popup = document.getElementById("myPopup");
    popup.classList.toggle("show");
}
</script>

<script>
$(document).ready(function () {

    $(document).off('change', '.executive').on('change', '.executive', function (e) {
            $("#sale_agent").html("<option value=''>Please wait..</option>");
            var detail = {};
            var div = '';
            var str = "";
            var form = '';
            var ajax_url = 'admin/registration/get_sa_data';

            detail['id'] = $("#executive").find('option:selected').attr('value');

            if (detail['id'] != '') {
                $("#sale_agent").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
                get_data(ajax_url, form, div, detail, function (response) {
                    if (response.sale_agent.length != 0) {
                        $("#sale_agent").html("<option value=''>Select Sale Agent</option>");
                        $.each(response.sale_agent, function (i, value) {
                            $("#sale_agent").append("<option value=" + value.id + ">" + value.display_name +"</option>");
                            $(".chosen_select").trigger("chosen:updated");
                        });
                        }
                    else {
                        $("#sale_agent").html("<option value=''>No Data Found</option>").trigger('chosen:updated');
                    }
                }, '', false);
            }
            else {
                $("#sale_agent").html("<option value=''>Select Sale Agent</option>").trigger('chosen:updated');
            }
        });

        $(document).off('change', '.sale_agent').on('change', '.sale_agent', function (e) {
            $("#seller_point").html("<option value=''>Please wait..</option>");
            var detail = {};
            var div = '';
            var str = "";
            var form = '';
            var ajax_url = 'admin/registration/get_sellerpoint_data';

            detail['id'] = $("#executive").find('option:selected').attr('value');
            detail['sale_agent'] = $("#sale_agent").find('option:selected').attr('value')
                
            if (detail['id'] != '') {
                $("#seller_point").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
                get_data(ajax_url, form, div, detail, function (response) { 
                    if (response.seller_point.length != 0) { 
                        $("#seller_point").html("<option value=''>Select Seller Point</option>");
                        $.each(response.seller_point, function (i, value) {
                                    if(value.BUS_STOP_CD==response.exists_seller_code) { 
                                        $("#seller_point").append("<option value=" + value.BUS_STOP_CD +" selected='selected'>" + value.BUS_STOP_NM +"</option>"); 
                                        $("#depot").append("<option value=" + response.exists_depot_cd +" selected='selected'>" +response.exists_depot_name +"</option>"); 
                                        
                                    } else {
                                        $("#seller_point").append("<option value=" + value.BUS_STOP_CD +">" + value.BUS_STOP_NM +"</option>");
                                    }

                                $(".chosen_select").trigger("chosen:updated");
                        });
                        $("#agent_code").attr("value",response.agent_code);
                        }
                    else {
                        $("#seller_point").html("<option value=''>No Data Found</option>").trigger('chosen:updated');
                    }
                }, '', false);
            }
            else {
                $("#seller_point").html("<option value=''>Select Seller Point</option>").trigger('chosen:updated');
            }
        });


        $(document).off('change', '#seller_point').on('change', '#seller_point', function (e) {
            $("#depot").html("<option value=''>Please wait..</option>");
            var detail = {};
            var div = '';
            var str = "";
            var form = '';
            var ajax_url = 'admin/registration/get_depot_data';

            detail['seller_cd'] = $("#seller_point").find('option:selected').attr('value')
            if (detail['seller_cd'] != '') {
                $("#depot").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
                get_data(ajax_url, form, div, detail, function (response) { 
                    if (response.depots.length != 0) { 
                        $("#depot").html("<option value=''>Select Depot</option>");
                        $.each(response.depots, function (i, value) {
                                    if(value.DEPOT_CD==response.exists_depot_cd) { 
                                        $("#depot").append("<option value=" +  value.DEPOT_CD +" selected='selected'>" +value.DEPOT_NM +"</option>"); 
                                    } else {
                                        $("#depot").append("<option value=" + value.DEPOT_CD +">" + value.DEPOT_NM +"</option>");
                                    }

                                $(".chosen_select").trigger("chosen:updated");
                        });
                        //$("#agent_code").attr("value",response.agent_code);
                        }
                    else {
                        $("#depot").html("<option value=''>No Data Found</option>").trigger('chosen:updated');
                    }
                }, '', false);
            }
            else {
                $("#depot").html("<option value=''>Select Depot</option>").trigger('chosen:updated');
            }
        });


    $("#edit_registration_form").validate({
        rules: {
           executive : {
               required :true
           },
            sale_agent : {
                required :true
            },
            seller_point : {
                required :true
            },
            depot : {
                required : true
            }
        },
        messages:
                {                  
                    seller_point: {
                        required: "Please enter the seller points",
                    },
                    sale_agent: {
                        required: "Please select sale agent",
                    },
                    executive: {
                        required: "Please select executive",
                    },
                    depot: {
                        required: "Please select depot",
                    }
                }
    });

    
    /******** DATE OF BIRTH DATEPICKER ********/
   
   /* var dt = new Date();
    dt.setFullYear(new Date().getFullYear() - 18);
    var endDate = dt;
    $("#dob").datepicker({
        changeMonth: true,
        changeYear: true,
        showMonthAfterYear: true,
        yearRange: "-100:-18",
        dateFormat: "dd-mm-yy",
        maxDate: endDate
    });
    $('#dob').keydown(function (event) {*/
    //event.preventDefault();    
//});
});
</script>

<style>
    /****DATEPICKER YEAR MONTH DROPDOWNN *******/
   /* .ui-datepicker .ui-datepicker-title select {
        color: #000;
    }*/
   /* .danger {
    background-color: #ffdddd;
    border-left: 6px solid #f44336;
}*/
</style>

<style>
/* Popup container - can be anything you want */
.popup {
    position: relative;
    display: inline-block;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* The actual popup */
.popup .popuptext {
    visibility: hidden;
    width: 200px;
    background-color: #555;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 8px 0;
    position: absolute;
    z-index: 1;
    bottom: -50%;
    left: 80%;
    margin-left: 225px;
    height: 200;
}

/* Popup arrow */
.popup .popuptext::before {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
}

/* Toggle this class - hide and show the popup */
.popup .show {
    visibility: visible;
    -webkit-animation: fadeIn 1s;
    animation: fadeIn 1s;
}

/* Add animation (fade in the popup) */
@-webkit-keyframes fadeIn {
    from {opacity: 0;} 
    to {opacity: 1;}
}

@keyframes fadeIn {
    from {opacity: 0;}
    to {opacity:1 ;}
}
</style>