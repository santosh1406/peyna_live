
<style>
label.error, div.error, ul.error, span.error {
    color: #f00;
    font-size: 14px;
}
</style>
<section class="content-header">
    <h3>Technical Users List</h3>
</section>

<section class="content">
  <div id="wallet_wrapper">
    <div class="row">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body" id="table_show">
              <table id="op_rept" name="op_rept" class="table table-striped">
                <thead>
                  <tr>
                    <th>Sr.No.</th>
                    <th>User Name</th>
                    <th>Role Name</th>
                    <th>Mobile No</th>
                    <th>Email Id</th>
                    <th>Email Verify Status</th>
                    <th>Created Date</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($users_list as $key => $value) { ?>
                    <tr>
                      <td><?php echo $key+1;?></td>
                      <td><?php echo $value['user_name'];?></td>
                      <td><?php echo $value['role_name'];?></td>
                      <td><?php echo rokad_decrypt($value['mobile_no'], $this->config->item('pos_encryption_key'));?></td>
                      <td><?php echo rokad_decrypt($value['email'], $this->config->item('pos_encryption_key'));?></td>
                      <td><?php echo $value['email_verify_status'];?></td>
                      <td><?php echo $value['created_date'];?></td>
                    </tr>
                  <?php } ?>                                           
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix" style="height: 10px;clear: both;"></div>   
    </div>   
  </div>     
</section>
<script>
$(document).ready(function() {

  $('#op_rept').dataTable();

});

</script>