<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php if ($edit_data[0]['level'] == MASTER_DISTRIBUTOR_LEVEL) { ?>
            <section class="content-header">
                <h2 style="margin-left:20px">Edit Regional Distributor</h2>
            </section>
        <?php } else if ($edit_data[0]['level'] == AREA_DISTRIBUTOR_LEVEL) { ?>
            <section class="content-header">
                <h2 style="margin-left:20px">Edit Divisional Distributor</h2>
            </section>
        <?php } else if ($edit_data[0]['level'] == POS_DISTRIBUTOR_LEVEL) { ?>
            <section class="content-header">
                <h2 style="margin-left:20px">Edit Executive</h2>
            </section>
        <?php } else if ($edit_data[0]['level'] == RETAILOR_LEVEL) { ?>
            <section class="content-header">
                <h2 style="margin-left:20px">Edit Sales Agent</h2>
            </section>
        <?php } ?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> </div>
                    <div class="box-body">
                        <?php echo validation_errors('<div class="error">', '</div>'); ?>
                        <?php $attributes = array("method" => "POST", "id" => "edit_registration_form", "name" => "edit_registration_form");
                              echo form_open(base_url() . 'admin/registration/update_details', $attributes); ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="firm_name">Name of Firm/Company *:</label>
                                <div class="col-lg-6">
                                    <input name="id" type="hidden" id="id" value="<?php echo  rokad_encrypt($edit_data[0]['id'], $this->config->item("pos_encryption_key")); ?>" class="form-control" autocomplete="off">
                                    <input name="firm_name" type="text" id="firm_name" value="<?php echo $edit_data[0]['firm_name']; ?>" class="form-control name" autocomplete="off">
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="first_name">First Name *:</label>
                                <div class="col-lg-6">
                                    <input name="first_name" type="text" id="first_name" value="<?php echo $edit_data[0]['first_name']; ?>" class="form-control name" autocomplete="off">
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="last_name">Last Name *:</label>
                                <div class="col-lg-6">
                                    <input name="last_name" type="text" id="last_name" value="<?php echo $edit_data[0]['last_name']; ?>" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="badge_no">Badge no : </label>
                                <div class="col-lg-6">
                                    <input id="badge_no" name="badge_no" maxlength="10" type="text" value="<?php echo $edit_data[0]['badge_no']; ?>" class="form-control" autocomplete="off">
                                </div>
                            </div>
<!--                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="mobile_no">Mobile no *: </label>
                                <div class="col-lg-6">
                                    <input name="mobile_no" type="text" minlength = "10" maxlength="10" id="mobile_no" value="<?php echo $edit_data[0]['']; ?>" class="form-control" autocomplete="off">
                                </div>
                            </div>-->
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="gender"> Gender : </label>
                                <div class="col-lg-6">
                                    <select class="form-control" id="gender"  name="gender">
                                        <option value="">Select</option>
                                        <option value="M" <?php echo (($edit_data[0]['gender'] == 'M') ? 'selected="selected" ' : '' ) ?>>Male</option>
                                        <option value="F" <?php echo (($edit_data[0]['gender'] == 'F') ? 'selected="selected" ' : '' ) ?>>Female</option>
                                        <option value="T" <?php echo (($edit_data[0]['gender'] == 'T') ? 'selected="selected" ' : '' ) ?>>Transgender</option>
                                    </select>
                                    <span id="prov_type_msg" class="err"></span>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="dob">Pin Code *:</label>
                                <div class="col-lg-6">
                                    <input type="text" name="pincode" id="pincode" value="<?php echo rokad_decrypt($edit_data[0]['pincode'],$this->config->item("pos_encryption_key")); ?>" class="form-control" maxlength="10" autocomplete="off" />
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="dob">DOB (DD-MM-YYYY) *:</label>
                                <div class="col-lg-6">
                                    <input name="dob" type="text" id="dob" value="<?php $dob = rokad_decrypt($edit_data[0]['dob'], $this->config->item("pos_encryption_key")); echo date("d-m-Y",strtotime($dob)); ?>" class="form-control"     >
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <hr>
                            <div class="form-group">                                    
                                <label class="col-lg-3 control-label" for="address1">Address1 *:</label>
                                <div class="col-lg-6">
                                    <input name="address1" type="text" id="address1" value="<?php echo $edit_data[0]['address1']; ?>" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="address2">Address2 :</label>
                                <div class="col-lg-6">
                                    <input name="address2" type="text" id="address2" value="<?php echo $edit_data[0]['address2']; ?>" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="agent_state">State *:</label>
                                <div class="col-lg-6">
                                    <select class="state form-control" name="agent_state" id="agent_state" value="<?php echo $edit_data[0]['state'] ?>" >
                                        <option value="">Select State</option>
                                        <?php
                                            foreach ($states as $row => $value) {
                                                ?>
                                                <option value="<?php echo $value['intStateId']; ?>" <?php echo (($value['intStateId'] == $edit_data[0]['state']) ? 'selected="selected" ' : '' ) ?>> <?php echo $value['stState'] ?></option>
                                            <?php }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">                                    
                                <label class="col-lg-3 control-label" for="agent_city">City *:</label>
                                <div class="col-lg-6">
                                    <select class="city form-control" name="agent_city" id="agent_city" >
                                        <option value="">Select City</option>

                                        <?php
                                            if ($city) {
                                                foreach ($city as $key => $value) {
                                                    ?>
                                                    <option value="<?php echo $value['intCityId']; ?>" <?php echo (($value['intCityId'] == $edit_data[0]['city']) ? 'selected="selected" ' : '' ) ?>> <?php echo $value['stCity'] ?></option>
                                                    <?php
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <hr>
                            
                             <?php  if($current_cb_agent_edit) {?>
<!--                            <input type="hidden" name="old_star_seller_cd" value="<?php echo $edit_data[0]['star_seller_code']; ?>">
                            <input type="hidden" name="old_depot_cd" value="<?php echo $edit_data[0]['depot_cd']; ?>">-->
                            <!-- <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="agent_state">Seller Point *:</label>
                                <div class="col-lg-6">
                                    <select class="state form-control" name="Seller_point" id="Seller_point" value="<?php echo $edit_data[0]['star_seller_code'] ?>" >
                                        <?php if(is_array($sellerpoints)) { ?>
                                            <?php foreach ($sellerpoints as $value) { ?>
                                                <option value="<?php echo $value->BUS_STOP_CD; ?>" <?php echo (($value->BUS_STOP_CD == $edit_data[0]['star_seller_code']) ? 'selected="selected" ' : '' ) ?>> <?php echo $value->BUS_STOP_NM ?></option>
                                           <?php } ?> 
                                        <?php }?>
                                    </select>
                                </div>
                            </div> -->

                           <!-- <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="address2">TOpup Limit :</label>
                                <div class="col-lg-6">
                                    <input name="topuplmit" type="text" id="topuplmit" value="<?php echo $edit_data[0]['topuplmit']; ?>" class="form-control" autocomplete="off">
                                </div>
                            </div>-->

                            <!-- <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="agent_state">Depot Name *:</label>
                                <div class="col-lg-6">
                                    <select class="state form-control" name="depot_code" id="depot_code">
                                    <?php if(is_array($depots)) {?>
                                    <?php foreach ($depots as $value) { ?>
                                         <option value="<?php echo $value->DEPOT_CD; ?>" <?php echo (($value->DEPOT_CD == $edit_data[0]['depot_cd']) ? 'selected="selected" ' : '' ) ?>> <?php echo $value->DEPOT_NM ?></option>
                                    <?php } } ?>
                                    </select>
                                </div>
                            </div>-->

                            <!-- <div class="clearfix" style="height: 10px;clear: both;"></div> -->
                            <!-- <hr> -->
                            <?php } ?> 
                            <div class="form-group">
                                <div class="col-lg-offset-5">
                                    <button class="btn btn-primary" id="submit_btn" name="submit_btn" type="submit">Save</button> 
                                    <a class="btn btn-primary" href="<?php echo base_url('admin/registration') ?>" type="button">Back</a> 
                                </div>
                            </div>

                        <?php form_close(); ?>
                        <!--</div> /.box-body--> 
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<script>
    //////////  edit form validation ////////
    $(document).ready(function () {
$("#edit_registration_form").validate({
        rules: {
            firm_name: {
                required: true,
                lettersonly: true
            },
            first_name: {
                required: true,
                lettersonly: true
            },
            last_name: {
                required: true,
                lettersonly: true
            },
            dob: {
                required: true,
            },
            address1: {
                required: true,
            },
            agent_state: {
                required: true,
            },
            agent_city: {
                required: true,
            },
            pincode: {
                required: true,
                number: true,
                minlength: 6,
                maxlength: 6,
            },
            // Seller_point : {
            //     required :true;
            // },
//            depot_code : {
//                required :true;
//            },
            topuplmit : {
                required :true;
            },

        },
        messages:
                {
                    firm_name: {
                        required: "Please enter firm/company name",
                        lettersonly: "Please enter valid firm/company name",
                    },
                    first_name: {
                        required: "Please enter first name",
                        lettersonly: "Please enter valid first name",
                    },
                    last_name: {
                        required: "Please enter last name",
                        lettersonly: "Please enter valid last name",
                    },
                    dob: {
                        required: "Please select the date of birth",
                    },
                    address1: {
                        required: "Please enter the  address line1",
                    },
                    agent_state: {
                        required: "Please select state name",
                    },
                    agent_city: {
                        required: "Please select city name",
                    },
                    pincode: {
                        required: "Please enter the pin code",
                        number: "Please enter valid pincode.",
                        minlength: "Please enter 6 digit pin code",
                        maxlength: "Please enter 6 digit pin code",
                    },
                    // Seller_point: {
                    //     required: "Please enter the  sellerpoints",
                    // },
//                    depot_code: {
//                        required: "Please select depots",
//                    },
                  /*  topuplmit: {
                        required: "Please enter topuplmit,
                    },*/
                }
                

    });
    jQuery.validator.addMethod("lettersonly", function (value, element)
    {
        return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
    }, "");
    /******** DATE OF BIRTH DATEPICKER ********/
    var dt = new Date();
    dt.setFullYear(new Date().getFullYear() - 18);
    var endDate = dt;
    $("#dob").datepicker({
        changeMonth: true,
        changeYear: true,
        showMonthAfterYear: true,
        yearRange: "-100:-18",
        dateFormat: "dd-mm-yy",
        maxDate: endDate
    });
    $('#dob').keydown(function (event) {
    event.preventDefault();    
});
});
</script>
<style>
    /****DATEPICKER YEAR MONTH DROPDOWNN *******/
    .ui-datepicker .ui-datepicker-title select {
        color: #000;
    }
</style>
