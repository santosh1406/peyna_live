<link href="<?php echo base_url() . COMMON_ASSETS ?>js/bootstrap/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() . COMMON_ASSETS ?>js/bootstrap/bootstrap-select.js" type="text/javascript"></script>

<section class="content-header">
    <h2 style="margin-left:20px">User Registration</h2>
</section>

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> </div>
                    <div class="box-body">
                        <?php echo validation_errors('<div class="error">', '</div>'); ?>
                        <?php
                            $attributes = array("method" => "POST", "id" => "admin_registration",  "name" => "admin_registration");
                            echo form_open(base_url() . 'admin/registration/save_admin_users', $attributes); 
                        ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <hr>
                            <div class="form-group">
                                <section class="content-header">
                                    <h4 style="margin-left:10px">Login Information</h4>
                                </section>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <label class="col-lg-3 control-label" for="email">Email *: </label>
                                <div class="col-lg-6">
                                    <input id="email" name="email" type="text" value="<?php echo set_value('email'); ?>" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            
                            <div class="clearfix" style="height: 10px;clear: both;"></div> 
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="mobile_no">Mobile no *: </label>
                                <div class="col-lg-6">
                                    <input name="mobile_no" type="text" minlength = "10" maxlength="10" id="mobile_no" value="<?php echo set_value('mobile_no'); ?>" class="form-control" autocomplete="off">
                                </div>
                            </div>

                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="role_id"> Select Role *: </label>
                                <div class="col-lg-6">
                                    <select class="form-control " id="role_id"  name="role_id">
                                        <option value="">Select Role</option>
                                        <?php
                                            foreach ($roles as $key => $value) {
                                        ?>
                                            <option value="<?php echo rokad_encrypt ( $value['id'], $this->config->item ( 'pos_encryption_key' ) ); ?>"><?php echo $value['role_name']; ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                    <span id="prov_type_msg" class="err"></span>
                                </div>
                            </div>

                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <hr>
                            <div class="form-group">
                                <section class="content-header">
                                    <h4 style="margin-left:10px">General</h4>
                                </section>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="first_name">First Name *:</label>
                                <div class="col-lg-6">
                                    <input name="first_name" type="text" id="first_name" value="<?php echo set_value('first_name'); ?>" class="form-control name" autocomplete="off">
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="last_name">Last Name *:</label>
                                <div class="col-lg-6">
                                    <input name="last_name" type="text" id="last_name" value="<?php echo set_value('last_name'); ?>" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="gender"> Gender : </label>
                                <div class="col-lg-6">
                                    <select class="form-control " id="gender"  name="gender">
                                        <option value="">Select Gender</option>
                                        <option value="M">Male</option>
                                        <option value="F">Female</option>
                                    </select>
                                    <span id="prov_type_msg" class="err"></span>
                                </div>
                            </div>
                            
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="dob">DOB (DD-MM-YYYY) *:</label>
                                <div class="col-lg-6">
                                    <input name="dob" type="text" id="dob" value="<?php echo set_value('dob'); ?>" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <hr>
                            <section class="content-header">
                                <h4 style="margin-left:10px">Contact Information</h4>
                            </section>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">                                    
                                <label class="col-lg-3 control-label" for="address1">Address1 *:</label>
                                <div class="col-lg-6">
                                    <input name="address1" type="text" id="address1" value="<?php echo set_value('address1'); ?>" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="address2">Address2 :</label>
                                <div class="col-lg-6">
                                    <input name="address2" type="text" id="address2" value="<?php echo set_value('address2'); ?>" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="agent_state">State *:</label>
                                <div class="col-lg-6">
                                    <select class="state form-control " name="agent_state" id="agent_state">
                                        <option value="">Select State</option>
                                        <?php
                                        foreach ($states as $row => $value) {
                                            echo "<option value='" . $value['intStateId'] . "'>" . $value['stState'] . "</option>";
                                        }
                                        ?>
                                    </select>   
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">                                    
                                <label class="col-lg-3 control-label " for="agent_city">City *:</label>
                                <div class="col-lg-6">
                                    <select class="agent_city form-control " name="agent_city" id="agent_city">
                                        <option value="">Select City</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="pincode">Pin Code *:</label>
                                <div class="col-lg-6">
                                    <input name="pincode" type="text" id="pincode" value="<?php echo set_value('pincode'); ?>" minlength = "6" maxlength="6" class="form-control" autocomplete="off">
                                </div>  
                            </div>
                    
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <div class="col-lg-offset-5">
                                    <button class="btn btn-primary" id="submit_btn" name="submit_btn" type="submit">Save</button> 
                                            <a class="btn btn-primary" href="<?php echo base_url('admin/registration/technical_users_list') ?>" type="button">Back</a> 
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script src="<?php echo base_url() ?>js/back/admin/user_registration.js"></script>
