<div class="row">
    <div class="col-xs-12">
        <div class="box">
<!--            <div class="box-header">
                <h3 class="box-title">Manage Registration</h3>
                <span style="float: right;margin-top: 10px;margin-right: 10px;"><button class="btn btn-primary add_btn">Add User</button></span>
            </div> /.box-header -->
            <div class="box-body table-responsive" style="min-height: 0.01%;overflow-x: auto;">
                <table id="user_display_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="20px;">User ID</th>
                            <th>Display Name</th>
                            <!--<th>Level</th>-->
                            <th>User Type</th>
                            <th>RMN</th>
                            <th>Status</th>
                            <th>RMN Verified</th>
                            <th>KYC Verify Status</th>
                            <th>Email Verify Status</th>
                            <th>KYC Status</th>
                            <th>Email Status</th>
                            <!--<th>Mobile Verified</th>-->
                            <th width="189px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>                                           

                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>
<script>
    $(document).ready(function () {
       var tconfig = {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": BASE_URL + "admin/registration/get_list",
            "type": "POST",
            "data": "json",
            data  : function (d) {
			d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;
      }
        },
        "iDisplayLength": 10,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        "paging": true,
        "searching": true,
        "aoColumnDefs": [
           {"bVisible": false, "aTargets": [6,7]},
            {"bSearchable": false, "aTargets": [5,6,7,8,9,10]}
        ],
          "order": [[ 0, 'desc' ]],
          "oLanguage": {
                  "sProcessing": '<img src="<?php echo base_url().BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>' 
                }
    };

    var oTable = $('#user_display_table').dataTable(tconfig);
    // to deactiavte users///
    $(document).off('click', '.soft_del_btn').on('click', '.soft_del_btn', function (e) {
        e.preventDefault();
        var id = $(this).attr('ref');
        var yn = $(this).attr('data-ref');
        if (yn == "N")
        {
            var postyn = "Y";
            var act = "enable";
        } else if (yn == "Y")
        {
            var postyn = "N";
            var act = "disable";
        }

        var detail = {};
        var div = "";
        var ajax_url = 'admin/registration/disable_user';
        var form = '';

        detail['id'] = id;
        detail['postyn'] = postyn;

        var yesno = confirm("Are you sure you want to " + act + " this user");
        if (yesno)
        {
            get_data(ajax_url, form, div, detail, function (response)
            {
                if (response.flag == '@#success#@')
                {
                    location.reload();
                } else if (response.flag == '@#error#@')
                {
                    showLoader(response.msg_type, response.msg);
                } else
                {
                    alert(response.msg);
                }
            }, '', false);
        }
    });
    $(document).off('click', '.del_btn').on('click', '.del_btn', function (e) {
        e.preventDefault();
        var id = $(this).attr('ref');

        var detail = {};
        var div = "";
        var ajax_url = 'admin/registration/delete_user';
        var form = '';

        detail['id'] = id;

        var yesno = confirm("Are you sure you want to delete this user. This user will not able to login again");
        if (yesno)
        {
            get_data(ajax_url, form, div, detail, function (response)
            {
                if (response.flag == '@#success#@')
                {
                    location.reload();
                } else if (response.flag == '@#error#@')
                {
                    showLoader(response.msg_type, response.msg);
                } else
                {
                    alert(response.msg);
                }
            }, '', false);
        }
    });
     $(document).off('click', '.resend_mail').on('click', '.resend_mail', function (e) {
        e.preventDefault();
        var id = $(this).attr('ref');
        var detail = {};
        var div = "";
        var ajax_url = 'admin/users/send_verification_email';
        var form = '';

        detail['id'] = id;
        
            get_data(ajax_url, form, div, detail, function (response)
            {
                if (response.flag == '@#success#@')
                {
                    location.reload();
                } else if (response.flag == '@#error#@')
                {
                    showLoader(response.msg_type, response.msg);
                } else
                {
                    alert(response.msg);
                }
            }, '', false);
    });
     $(document).off('click', '.reset_rmn').on('click', '.reset_rmn', function (e) {
        e.preventDefault();
        var id = $(this).attr('ref');
        var detail = {};
        var div = "";
        var ajax_url = 'admin/users/send_reset_rmn_link';
        var form = '';

        detail['id'] = id;
        
            get_data(ajax_url, form, div, detail, function (response)
            {
                if (response.flag == '@#success#@')
                {
                    location.reload();
                } else if (response.flag == '@#error#@')
                {
                    showLoader(response.msg_type, response.msg);
                } else
                {
                    alert(response.msg);
                }
            }, '', false);
    });
    });
</script>

