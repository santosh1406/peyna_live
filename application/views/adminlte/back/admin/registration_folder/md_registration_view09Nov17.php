<link href="<?php echo base_url() . COMMON_ASSETS ?>js/bootstrap/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() . COMMON_ASSETS ?>js/bootstrap/bootstrap-select.js" type="text/javascript"></script>

<?php if ($creat_role == COMPANY_ROLE_ID) { ?>
        <section class="content-header">
            <h2 style="margin-left:20px">Add Company</h2>
        </section>
<?php } else if ($creat_role == MASTER_DISTRIBUTOR_ROLE_ID) { ?>
        <section class="content-header">
            <h2 style="margin-left:20px">Add Master Distributor</h2>
        </section>
    <?php } else if ($creat_role == AREA_DISTRIBUTOR_ROLE_ID) { ?>
        <section class="content-header">
            <h2 style="margin-left:20px">Add Area Distributor</h2>
        </section>
    <?php } else if ($creat_role == DISTRIBUTOR) { ?>
        <section class="content-header">
            <h2 style="margin-left:20px">Add Agent</h2>
        </section>
    <?php } else if ($creat_role == RETAILER_ROLE_ID) { ?>
        <section class="content-header">
            <h2>Add Sales Executive</h2>
        </section>
    <?php } ?>
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> </div>
                    <div class="box-body">
                        <?php echo validation_errors('<div class="error">', '</div>'); ?>
                        <?php $attributes = array("method" => "POST", "id" => "registration_form",  "name" => "registration_form");
                              echo form_open(base_url() . 'admin/registration/saveuser', $attributes); ?>
                            <input name="role" type="hidden" id="role" value="<?php echo $role_id ?>" class="form-control" autocomplete="off">
                            <input name="level" type="hidden" id="level" value="<?php echo $level ?>" class="form-control" autocomplete="off">
                            <input name="creat_role" id="creat_role" type="hidden" value="<?php echo $creat_role ?>" class="form-control" autocomplete="off">
                            <input name="cmp_drop_down_flag" type="hidden" id="cmp_drop_down_flag" value="<?php echo $cmp_drop_down_flag?>" class="form-control" autocomplete="off">
                            <input name="md_drop_down_flag" type="hidden" id="md_drop_down_flag" value="<?php echo $md_drop_down_flag ?>" class="form-control" autocomplete="off">
                            <input name="ad_drop_down_flag" type="hidden" id="ad_drop_down_flag" value="<?php echo $ad_drop_down_flag ?>" class="form-control" autocomplete="off">
                            <input name="d_drop_down_flag"  type="hidden" id="d_drop_down_flag" value="<?php echo $d_drop_down_flag ?>" class="form-control" autocomplete="off">    
                            <?php if ($creat_role != COMPANY_ROLE_ID && (!empty($cmp_drop_down_flag))) { ?>
                            		<div class="clearfix" style="height: 10px;clear: both;"></div>
                            		<div class="form-group"> 
                                        <label class="col-lg-3 control-label" for="md">Select Company *:</label>
                                        <div class="col-lg-6">
                                            <select class="form-control cmp " name="cmp" id="cmp">
                                                <option value="">Select Company</option>
                                                <?php
                                                if (isset($all_cmp) && count($all_cmp) > 0) {
                                                    foreach ($all_cmp as $cmp) {
                                                        ?>
                                                        <option value="<?php if (!empty($cmp['id'])) echo $cmp['id'] ?>">
                                                            <?php if (!empty($cmp['display_name'])) echo $cmp['display_name'], ': ' ,rokad_decrypt($cmp['mobile_no'], $this->config->item('pos_encryption_key')); ?>
                                                        </option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>   
                                        </div>
                                    </div>                            
                            <?php } if ($creat_role != MASTER_DISTRIBUTOR_ROLE_ID && (!empty($md_drop_down_flag))) { ?>
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <div class="form-group"> 
                                        <label class="col-lg-3 control-label" for="md">Select Master Distributor *:</label>
                                        <div class="col-lg-6">
                                            <select class="form-control md " name="md" id="md">
                                                <option value="">Select Master Distributor</option>
                                                <?php
                                                if (!empty($all_mds)) {
                                                    foreach ($all_mds as $md) {
                                                        ?>
                                                        <option value="<?php if (!empty($md['id'])) echo $md['id'] ?>">
                                                            <?php if (!empty($md['display_name'])) echo $md['display_name'], ': ' ,rokad_decrypt($md['mobile_no'], $this->config->item('pos_encryption_key')); ?>
                                                        </option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>   
                                        </div>
                                    </div>
                                <?php } if ($creat_role != AREA_DISTRIBUTOR_ROLE_ID && (!empty($ad_drop_down_flag))) { ?>
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <div class="form-group"> 
                                        <label class="col-lg-3 control-label" for="ad">Select Area Distributor *:</label>
                                        <div class="col-lg-6">
                                            <select class="form-control  ad" name="ad" id="ad">
                                                <option value="">Select Area Distributor</option>
                                                <?php
                                                if (isset($area_distributor) && count($area_distributor) > 0) {
                                                    foreach ($area_distributor as $ad) {
                                                        ?>
                                                        <option value="<?php if (!empty($ad['id'])) echo $ad['id'] ?>">
                                                        <?php if (!empty($ad['display_name'])) echo $ad['display_name'], ': ' ,rokad_decrypt($ad['mobile_no'], $this->config->item('pos_encryption_key')); ?>
                                                        </option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>   
                                        </div>
                                    </div>
                                <?php } if($creat_role == RETAILER_ROLE_ID  && (!empty($d_drop_down_flag))) { ?>
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <div class="form-group"> 
                                        <label class="col-lg-3 control-label" for="distributor">Select Agent *:</label>
                                        <div class="col-lg-6">
                                            <select class="form-control distributor  show_ret" name="distributor" id="distributor">
                                                <option value="">Select Agent</option>
                                                <?php
                                                if (isset($distributors) && count($distributors) > 0) {
                                                    foreach ($distributors as $distributor) {
                                                        ?>
                                                        <option value="<?php if (!empty($distributor['id'])) echo $distributor['id'] ?>">
                                                        <?php if (!empty($distributor['display_name'])) echo $distributor['display_name'], ': ' ,rokad_decrypt($distributor['mobile_no'], $this->config->item('pos_encryption_key')); ?>
                                                        </option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>   
                                        </div>
                                    </div>
                                <?php }?>
                                 <?php if($creat_role == RETAILER_ROLE_ID) { ?>
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <div class="form-group"> 
                                        <label class="col-lg-3 control-label" for="service_name">Select Services *:</label>
                                        <div class="col-lg-6">
                                            <select name="service_name[]" id="service_name" class="selectpicker"  required="" data-live-search="true" onload="showServices()"> 

                                               <?php if(is_array($serviceDetails)) {?>
                                                <?php foreach ($serviceDetails as $sd) { ?>
                                                    <option value="<?php echo $sd->id; ?>"><?php echo $sd->service_name; ?></option>
                                                <?php } } ?>
                                            </select>
                                       </div> 
                                     </div>
                                <?php } ?>
                                
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <hr>
                            <div class="form-group">
                                <section class="content-header">
                                    <h4 style="margin-left:10px">Login Information</h4>
                                </section>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <label class="col-lg-3 control-label" for="email">Email *: </label>
                                <div class="col-lg-6">
                                    <input id="email" name="email" type="text" value="<?php echo set_value('email'); ?>" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="mobile_no">Mobile no *: </label>
                                <div class="col-lg-6">
                                    <input name="mobile_no" type="text" minlength = "10" maxlength="10" id="mobile_no" value="<?php echo set_value('mobile_no'); ?>" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <hr>
                            <div class="form-group">
                                <section class="content-header">
                                    <h4 style="margin-left:10px">General</h4>
                                </section>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <label class="col-lg-3 control-label" for="firm_name">Name of Firm/Company *:</label>
                                <div class="col-lg-6">
                                    <input name="firm_name" type="text" id="firm_name" value="<?php echo set_value('firm_name'); ?>" class="form-control name" autocomplete="off">
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="first_name">First Name *:</label>
                                <div class="col-lg-6">
                                    <input name="first_name" type="text" id="first_name" value="<?php echo set_value('first_name'); ?>" class="form-control name" autocomplete="off">
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="last_name">Last Name *:</label>
                                <div class="col-lg-6">
                                    <input name="last_name" type="text" id="last_name" value="<?php echo set_value('last_name'); ?>" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="gender"> Gender : </label>
                                <div class="col-lg-6">
                                    <select class="form-control " id="gender"  name="gender">
                                        <option value="">Select Gender</option>
                                        <option value="M">Male</option>
                                        <option value="F">Female</option>
                                    </select>
                                    <span id="prov_type_msg" class="err"></span>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="adharcard">Aadhaar Card *:</label>
                                <div class="col-lg-6">
                                    <input type="text" name="adharcard" id="adharcard" value="<?php echo set_value('adharcard'); ?>" class="form-control" maxlength="12" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="dob">PAN / TAN Card *:</label>
                                <div class="col-lg-6">
                                    <input type="text" name="pancard" id="pancard" value="<?php echo set_value('pancard'); ?>" class="form-control" maxlength="10" autocomplete="off" style="text-transform:uppercase"/>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="dob">DOB (DD-MM-YYYY) *:</label>
                                <div class="col-lg-6">
                                    <input name="dob" type="text" id="dob" value="<?php echo set_value('dob'); ?>" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <hr>
                            <section class="content-header">
                                <h4 style="margin-left:10px">Contact Information</h4>
                            </section>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">                                    
                                <label class="col-lg-3 control-label" for="address1">Address1 *:</label>
                                <div class="col-lg-6">
                                    <input name="address1" type="text" id="address1" value="<?php echo set_value('address1'); ?>" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="address2">Address2 :</label>
                                <div class="col-lg-6">
                                    <input name="address2" type="text" id="address2" value="<?php echo set_value('address2'); ?>" class="form-control" autocomplete="off">
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="agent_state">State *:</label>
                                <div class="col-lg-6">
                                    <select class="state form-control " name="agent_state" id="agent_state">
                                        <option value="">Select State</option>
                                        <?php
                                            foreach ($states as $row => $value) {
                                                echo "<option value='" . $value['intStateId'] . "'>" . $value['stState'] . "</option>";
                                            }
                                        ?>
                                    </select>   
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">                                    
                                <label class="col-lg-3 control-label " for="agent_city">City *:</label>
                                <div class="col-lg-6">
                                    <select class="agent_city form-control " name="agent_city" id="agent_city">
                                        <option value="">Select City</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="pincode">Pin Code *:</label>
                                 <div class="col-lg-6">
                                    <input name="pincode" type="text" id="pincode" value="<?php echo set_value('pincode'); ?>" minlength = "6" maxlength="6" class="form-control" autocomplete="off">
                                </div>  
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                             <hr>
                            <div class="retailer_show"> 
                            <section class="content-header">
                                <h4 style="margin-left:10px">Other Information</h4>
                            </section>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                           <!-- <div class="form-group">
                            <label class="col-lg-3 control-label" for="bType">Business Type:* </label>
                            <div class="col-lg-6">
                                <select name="bType" id="bType" class="form-control ">
                                    <option value="">Please select</option>  
                                   <?php //if(isset($btypes)) {?> 
                                   <?php //foreach ($btypes as $btype){ ?>
                                        <option value="<?php //echo $btype->btype_id; ?>"><?php //echo $btype->btype; ?></option>
                                    <?php //} ?> 
                                    <?php //} ?>
                                </select>
                            </div>                               
                        </div> -->
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <!-- <div class="form-group retailer">
                            <label class="col-lg-3 control-label" for="scheme">Select Scheme:* </label>
                            <div class="col-lg-6">
                                <select name="scheme" id="scheme" class="form-control ">
                                    <option value="">Please select</option>  
                                     <?php //if(is_array($schemes)) {?> 
                                    <?php //foreach ($schemes as $scheme) { ?>
                                        <option value="<?php //echo $scheme->scheme_id; ?>"><?php //echo $scheme->scheme; ?></option>
                                    <?php //} ?> <?php //}?>
                                </select>
                            </div>
                            <span id="schememsg" class="err"></span>                               
                        </div> -->
                        <input name="bType" id="bType" type="hidden" value="0" class="form-control" autocomplete="off">

                        <input name="scheme" id="scheme" type="hidden" value="0" class="form-control" autocomplete="off">

                        <div class="clearfix retailer" style="height: 10px;clear: both;"></div>
                        <div class="form-group retailer">
                            <label class="col-lg-3 control-label" for="sellerpoint">Seller Point:* </label>
                            <div class="col-lg-6">
                                <select name="sellerpoint" id="sellerpoint" class="form-control select2 ">
                                    <option value="">Please select</option>  
                                   <?php if(is_array($sellerpoints)) { ?>
                                   <?php foreach ($sellerpoints as $sellerpoint) { ?>
                                        <option value="<?php echo $sellerpoint->BUS_STOP_CD; ?>"><?php echo $sellerpoint->BUS_STOP_NM.'  ('.$sellerpoint->BUS_STOP_CD.')'; ?></option>
                                    <?php } ?> <?php }?>
                                    
                                </select>
                            </div>  
                            <span id="sellerpointmsg" class="err"></span>
                        </div>
                        <div class="clearfix retailer" style="height: 10px;clear: both;"></div>

                        <div class="form-group retailer">
                            <label class="col-lg-3 control-label" for="sellerdesc">Sellerpoint(description):* </label>
                            <div class="col-lg-6">
                                <input name="sellerdesc" id="sellerdesc" class="form-control " placeholder="Description" autocomplete="off" type="text">
                            </div>
                            <span id="sellerdescmsg" class="err"></span>
                        </div>
                        <div class="clearfix retailer" style="height: 10px;clear: both;"></div>
                        
                        <input name="topUpLimit" id="topUpLimit" type="hidden" value="0" class="form-control" autocomplete="off">

                        <input name="hhmLimit" id="hhmLimit" type="hidden" value="0" class="form-control" autocomplete="off">

                        <!-- <div class="form-group retailer">
                             <label class="col-lg-3 control-label" for="topUpLimit">Top Up Limit:*</label>
                                <div class="col-lg-6">
                                 <input name="topUpLimit" id="topUpLimit" class="form-control " placeholder="Top Up Limit" autocomplete="off" type="text">
                                </div>
                            <span id="topUpLimitmsg" class="err"></span>
                        </div>
                        <div class="clearfix retailer" style="height: 10px;clear: both;"></div>
                        
                        <div class="form-group retailer">
                            <label class="col-lg-3 control-label" for="hhmLimit">HHM Limit:* </label>
                            <div class="col-lg-6">
                                <input name="hhmLimit" id="hhmLimit" class="form-control " placeholder="HHM Limit" autocomplete="off" type="text">
                            </div>
                            <span id="hhmLimitmsg" class="err"></span>
                        </div> -->
                        <div class="clearfix retailer" style="height: 10px;clear: both;"></div>
                        <div class="form-group retailer">
                             <label class="col-lg-3 control-label" for="depot_code">Depot Name:* </label>
                            <div class="col-lg-6">
                                <select name="depot_code" id="depot_code" class="form-control select2 ">
                                    <option value="">Please select</option>  
                                   <?php if(is_array($depots)) {?>
                                    <?php foreach ($depots as $depot) { ?>
                                        <option value="<?php echo $depot->DEPOT_CD; ?>"><?php echo $depot->DEPOT_NM; ?></option>
                                    <?php }
                                    } ?>
                                </select>
                            </div>  
                            <span id="depot_codemsg" class="err"></span>
                        </div>
                        <div class="clearfix retailer" style="height: 10px;clear: both;"></div>
                        <div class="form-group retailer">
                            <label class="col-lg-3 control-label" for="securityDep">Security Deposit: </label>
                            <div class="col-lg-6">
                                <input name="securityDep" id="securityDep" class="form-control " placeholder="Security Deposit" autocomplete="off" type="text">
                            </div>
                        </div>
                        <div class="clearfix retailer" style="height: 10px;clear: both;"></div>
        
                            
                        <hr>
                        <section class="content-header">
                                <h4 style="margin-left:10px">Bank Account  Detail</h4>
                        </section>

                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="uname">Account No: </label>
                            <div class="col-lg-6">
                                <input type="text" name="accno" id="accno" class="form-control" placeholder="Account Number" value="<?php echo set_value('accno'); ?>"   data-msg-required="Please Enter Account Number" autocomplete="off" maxlength="21">
                            </div> 
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="uname">Bank Name: </label>
                            <div class="col-lg-6">
                                <input type="text" name="bname" id="bname"class="form-control upper" placeholder="Bank Name" data-msg-required="Please Enter Bank Name" autocomplete="off" value="<?php echo set_value('bname'); ?>" >
                            </div>    
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                         <div class="form-group">
                        <label class="col-lg-3 control-label" for="uname">Account Holder  Name:* </label>
                      <div class="col-lg-6">
                         <input type="text" name="accname" value="" id="accname"   class="form-control upper"  placeholder="Account name" value="<?php echo set_value('accname'); ?>">
                        </div>
                    </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="uname">Branch Name: </label>
                            <div class="col-lg-6">
                                <input type="text" name="brname" type="text" id="brname" class="form-control upper" placeholder="Branch Name" data-msg-required="Please Enter Branch Name" value="<?php echo set_value('brname'); ?>" autocomplete="off">
                            </div>  
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="uname">IFSC Code: </label>
                            <div class="col-lg-6">
                                <input type="text" name="ifsc" maxlength = 11 value="" id="ifsc"  data-msg-required="Please Enter IFSC code" data-msg-lfsccode="Please enter valid IFSC Code" class="form-control upper"  placeholder="IFSC Code"  value="<?php echo set_value('ifsc'); ?>">
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>



                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <div class="col-lg-offset-5">
                                    <button class="btn btn-primary" id="submit_btn" name="submit_btn" type="submit">Save</button> 
                                    <a class="btn btn-primary" href="<?php echo base_url('admin/registration') ?>" type="button">Back</a> 
                                </div>


                                <!--</div> /.box-body--> 
                            </div><!-- /.box -->
                        <?php echo form_close(); ?>
                    </div><!-- /.col -->
                </div><!-- /.row -->
                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->


            <script>
                var cmp_on_change_flag = '<?php echo $cmp_on_change_flag; ?>';
                var md_on_change_flag = '<?php echo $md_on_change_flag; ?>';
                var ad_on_change_flag = '<?php echo $ad_on_change_flag; ?>';
            </script>
           
            <script src="<?php echo base_url() ?>js/back/admin/registration.js"></script>
            <style>
                /****DATEPICKER YEAR MONTH DROPDOWNN *******/
                .ui-datepicker .ui-datepicker-title select {
                    color: #000;
                }
            </style>
            <script type="text/javascript">
             /*   $(document).off('change', '.show_ret').on('change', '.show_ret', function(e)
                {
                    $('.retailer_show').show();
                });
                 var current_role = '<?php echo $this->session->userdata('role_id'); ?>';
                 var distributor_role = '<?php echo DISTRIBUTOR ;?>'
                 if(current_role == distributor_role)
                 {
                    $('.retailer_show').show();
                 }*/
            // $(function() {
            //         $('.retailer_show').hide(); 
            //         $('#service_name').load(function(){
            //             if($('#service_name').val() == '2') {
            //                 $('.retailer_show').show(); 
            //             } else {
            //                 $('.retailer_show').hide(); 
            //             } 
                        
            //         });
            //    });

            function showServices() {
                    $('.retailer_show').hide(); 
                    if($('#service_name').val() == '2') {
                        $('.retailer_show').show(); 
                    } else {
                        $('.retailer_show').hide(); 
                    }                                            
               }
           </script>