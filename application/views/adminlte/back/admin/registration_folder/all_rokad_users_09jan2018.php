<section class="content-header">
    <h2>Users List
    </h2>
    <div style="display: block;float: right;margin-top: 2%;" class="form-group">
           <button  class="btn btn-primary" id="extoexcel" type="button">
       <span class="glyphicon glyphicon-export"></span>Export To Excel</button>  
       </div>
<!--    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="">Registration</li>
        <li class="active">Users List</li>
   	</ol>-->
</section>

<!-- Main content -->
<section class="content">
    <div id="menu_wrapper">
        <?php
        $this->load->view(ALL_ROKAD_USERS_AJAX);
        ?>
    </div>

    <!--?php $this->load->view(ORS_USERS_POPUP); ?-->
</section>
<!--<script src="<?php echo base_url() ?>js/back/admin/registration.js"></script>-->