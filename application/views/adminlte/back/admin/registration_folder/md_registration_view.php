<!--<link href="<?php echo base_url() . COMMON_ASSETS ?>js/bootstrap/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() . COMMON_ASSETS ?>js/bootstrap/bootstrap-select.js" type="text/javascript"></script>-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">-->

<style>
    //added by harshada kulkarni on 12-07-2018
    .overflowClass{
        //border: 1px solid #ccc;
        height: 50px;
        width: 48.5%;
        overflow-x:scroll;
        overflow-x:hidden;
    }
</style>

<?php if ($creat_role == COMPANY_ROLE_ID) { ?>
    <section class="content-header">
        <h2 style="margin-left:20px">Add Company</h2>
    </section>
<?php } else if ($creat_role == MASTER_DISTRIBUTOR_ROLE_ID) { ?>
    <section class="content-header">
        <h2 style="margin-left:20px">Add Regional Distributor</h2>
    </section>
<?php } else if ($creat_role == AREA_DISTRIBUTOR_ROLE_ID) { ?>
    <section class="content-header">
        <h2 style="margin-left:20px">Add Divisional Distributor</h2>
    </section>
<?php } else if ($creat_role == DISTRIBUTOR) { ?>
    <section class="content-header">
        <h2 style="margin-left:20px">Add Executive</h2>
    </section>
<?php } else if ($creat_role == RETAILER_ROLE_ID) { ?>
    <section class="content-header">
        <h2>Add Sale Agent</h2>
    </section>
<?php } ?>    
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> </div>
                    <div class="box-body">
                        <?php echo validation_errors('<div class="error">', '</div>'); ?>
                        <?php
                        $attributes = array("method" => "POST", "id" => "registration_form", "name" => "registration_form");
                        echo form_open(base_url() . 'admin/registration/saveuser', $attributes);
                        ?>
                        <input name="role" type="hidden" id="role" value="<?php echo $role_id ?>" class="form-control" autocomplete="off">
                        <input name="level" type="hidden" id="level" value="<?php echo $level ?>" class="form-control" autocomplete="off">
                        <input name="creat_role" id="creat_role" type="hidden" value="<?php echo $creat_role ?>" class="form-control" autocomplete="off">
                        <input name="cmp_drop_down_flag" type="hidden" id="cmp_drop_down_flag" value="<?php echo $cmp_drop_down_flag ?>" class="form-control" autocomplete="off">
                        <input name="md_drop_down_flag" type="hidden" id="md_drop_down_flag" value="<?php echo $md_drop_down_flag ?>" class="form-control" autocomplete="off">
                        <input name="ad_drop_down_flag" type="hidden" id="ad_drop_down_flag" value="<?php echo $ad_drop_down_flag ?>" class="form-control" autocomplete="off">
                        <input name="d_drop_down_flag"  type="hidden" id="d_drop_down_flag" value="<?php echo $d_drop_down_flag ?>" class="form-control" autocomplete="off">    
                        <?php if ($creat_role != COMPANY_ROLE_ID && (!empty($cmp_drop_down_flag))) { ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="md">Select Company *:</label>
                                <div class="col-lg-6">
                                    <select class="form-control cmp " name="cmp" id="cmp">
                                        <option value="">Select Company</option>
                                        <?php
                                        if (isset($all_cmp) && count($all_cmp) > 0) {
                                            foreach ($all_cmp as $cmp) {
                                                ?>
                                                <option value="<?php if (!empty($cmp['id'])) echo $cmp['id'] ?>">
                                                    <?php if (!empty($cmp['display_name'])) echo $cmp['display_name'], ': ', rokad_decrypt($cmp['mobile_no'], $this->config->item('pos_encryption_key')); ?>
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>   
                                </div>
                            </div>                            
                        <?php } if ($creat_role != MASTER_DISTRIBUTOR_ROLE_ID && (!empty($md_drop_down_flag))) { ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="md">Select Regional Distributor *:</label>
                                <div class="col-lg-6">
                                    <select class="form-control md " name="md" id="md">
                                        <option value="">Select Regional Distributor</option>
                                        <?php
                                        if (!empty($all_mds)) {
                                            foreach ($all_mds as $md) {
                                                ?>
                                                <option value="<?php if (!empty($md['id'])) echo $md['id'] ?>">
                                                    <?php if (!empty($md['display_name'])) echo $md['display_name'], ': ', rokad_decrypt($md['mobile_no'], $this->config->item('pos_encryption_key')); ?>
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>   
                                </div>
                            </div>
                        <?php } if ($creat_role != AREA_DISTRIBUTOR_ROLE_ID && (!empty($ad_drop_down_flag))) { ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="ad">Select Divisional Distributor *:</label>
                                <div class="col-lg-6">
                                    <select class="form-control  ad" name="ad" id="ad">
                                        <option value="">Select Divisional Distributor</option>
                                        <?php
                                        if (isset($area_distributor) && count($area_distributor) > 0) {
                                            foreach ($area_distributor as $ad) {
                                                ?>
                                                <option value="<?php if (!empty($ad['id'])) echo $ad['id'] ?>">
                                                    <?php if (!empty($ad['display_name'])) echo $ad['display_name'], ': ', rokad_decrypt($ad['mobile_no'], $this->config->item('pos_encryption_key')); ?>
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>   
                                </div>
                            </div>
                        <?php } if ($creat_role == RETAILER_ROLE_ID && (!empty($d_drop_down_flag))) { ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="distributor">Select Executive *:</label>
                                <div class="col-lg-6">
                                    <select class="form-control distributor  show_ret" name="distributor" id="distributor">
                                        <option value="">Select Executive</option>
                                        <?php
                                        if (isset($distributors) && count($distributors) > 0) {
                                            foreach ($distributors as $distributor) {
                                                ?>
                                                <option value="<?php if (!empty($distributor['id'])) echo $distributor['id'] ?>">
                                                    <?php if (!empty($distributor['display_name'])) echo $distributor['display_name'], ': ', rokad_decrypt($distributor['mobile_no'], $this->config->item('pos_encryption_key')); ?>
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>   
                                </div>
                            </div>
                        <?php } ?>
                        <!-- edited by harshada kulkarni on 12-07-2018 -start -->
                        <?php if ($creat_role == RETAILER_ROLE_ID) { ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group intent"> 
                                <label class="col-lg-3 control-label" for="service_name">Select Services *:</label>
                                <div class="col-lg-6 overflowClass" onload="showServices()">                                    
                                    <?php if (is_array($serviceDetails)) { ?>
                                        <?php foreach ($serviceDetails as $sd) { ?>
                                            <input type ="checkbox" name ="service_name[]" id ="service_name" value="<?php echo $sd->id; ?>" class="icheckbox_minimal"/>
                                            <?php echo $sd->service_name; ?><br>
                                            <?php
                                        }
                                    }
                                    ?> 

                                        <!--<select id="service_name" name="service_name[]" multiple class="form-control" >
                                    <?php if (is_array($serviceDetails)) { ?>
                                        <?php foreach ($serviceDetails as $sd) { ?>
                                                                                    <option value="<?php echo $sd->id ?>"><?php echo $sd->service_name; ?></option>                                          
                                            <?php
                                        }
                                    }
                                    ?>
                                        </select>-->
                                </div> 
                            </div>
                            <!-- edited by harshada kulkarni on 12-07-2018 -end -->
                            <br>
                            <br>
                             <?php if ($creat_role == RETAILER_ROLE_ID) { ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group" id="depot_div" style="display:none"> 
                                <label class="col-lg-3 control-label" for="distributor">Select Smartcard Depot *:</label>
                               <div class="col-lg-6">
                                <select class="form-control distributor  show_ret" name="depot_cd" id="depot_cd">
                                        <option value="">Select Smartcard Depot</option>
                                        <?php
                                        if (isset($depotsCode) && count($depotsCode) > 0) {
                                            foreach ($depotsCode as $dc) {
                                                ?>
                                                <option value="<?php 
                                                if (!empty($dc['DEPOT_CD']))
                                                 echo $dc['DEPOT_CD']; ?>">
                                                    <?php if (!empty($dc['DEPOT_CD'])) 
                                                    echo $dc['DEPOT_NM']?>
                                                </option>
                                                <?php
                                           }
                                        }
                                        ?>
                                    </select>    
                                </div>
                            </div>
                        <?php } ?>
                            <?php if ($creat_role == RETAILER_ROLE_ID) { ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group" id="division_div" style="display:none"> 
                                <label class="col-lg-3 control-label" for="distributor">Division*:</label>
                                <div class="col-lg-6">
                                    <input id="division_nm" name="division_nm" type="text" value="<?php echo $edit_data[0]['badge_no']; ?>" class="form-control" maxlength="10" autocomplete="off"></input> 
                                    <input type="hidden" id="division_cd" name="division_cd" value>
                                </div>
                            </div>
                        <?php } ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="badge_no">Badge no : </label>
                                <div class="col-lg-6">
                                    <input name="badge_no" type="text" id="badge_no"  maxlength="10" value="<?php echo set_value('badge_no'); ?>" class="form-control" autocomplete="off">
                                </div>
                            </div>
                        <?php } ?>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <hr>
                        <div class="form-group">
                            <section class="content-header">
                                <h4 style="margin-left:10px">Login Information</h4>
                            </section>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <label class="col-lg-3 control-label" for="email">Email *: </label>
                            <div class="col-lg-6">
                                <input id="email" name="email" type="text" value="<?php echo set_value('email'); ?>" class="form-control" autocomplete="off" maxlength="100">
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <label class="col-lg-3 control-label">Company GST No *: </label>
                        <div class="col-lg-6">
                            <input id="company_gst_no" name="company_gst_no" type="text" minlength = "15" maxlength="15" value="<?php echo set_value('company_gst_no'); ?>" class="form-control" autocomplete="off" > 
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div> 
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="mobile_no">Mobile no *: </label>
                            <div class="col-lg-6">
                                <input name="mobile_no" type="text" minlength = "10" maxlength="10" id="mobile_no" value="<?php echo set_value('mobile_no'); ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <hr>
                        <div class="form-group">
                            <section class="content-header">
                                <h4 style="margin-left:10px">General</h4>
                            </section>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <label class="col-lg-3 control-label" for="firm_name">Name of Firm/Company *:</label>
                            <div class="col-lg-6">
                                <input name="firm_name" type="text" id="firm_name" value="<?php echo set_value('firm_name'); ?>" class="form-control name" autocomplete="off">
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="first_name">First Name *:</label>
                            <div class="col-lg-6">
                                <input name="first_name" type="text" id="first_name" value="<?php echo set_value('first_name'); ?>" class="form-control name" autocomplete="off">
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="last_name">Last Name *:</label>
                            <div class="col-lg-6">
                                <input name="last_name" type="text" id="last_name" value="<?php echo set_value('last_name'); ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="gender"> Gender : </label>
                            <div class="col-lg-6">
                                <select class="form-control " id="gender"  name="gender">
                                    <option value="">Select Gender</option>
                                    <option value="M">Male</option>
                                    <option value="F">Female</option>
                                    <option value="T">Transgender</option>
                                </select>
                                <span id="prov_type_msg" class="err"></span>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="adharcard">Aadhaar Card *:</label>
                            <div class="col-lg-6">
                                <input type="text" name="adharcard" id="adharcard" value="<?php echo set_value('adharcard'); ?>" class="form-control" maxlength="12" autocomplete="off"/>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="dob">PAN / TAN Card *:</label>
                            <div class="col-lg-6">
                                <input type="text" name="pancard" id="pancard" value="<?php echo set_value('pancard'); ?>" class="form-control" maxlength="10" autocomplete="off" style="text-transform:uppercase"/>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="dob">DOB (DD-MM-YYYY) *:</label>
                            <div class="col-lg-6">
                                <input name="dob" type="text" id="dob" value="<?php echo set_value('dob'); ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <hr>
                        <section class="content-header">
                            <h4 style="margin-left:10px">Contact Information</h4>
                        </section>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">                                    
                            <label class="col-lg-3 control-label" for="address1">Address1 *:</label>
                            <div class="col-lg-6">
                                <input name="address1" type="text" id="address1" value="<?php echo set_value('address1'); ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label" for="address2">Address2 :</label>
                            <div class="col-lg-6">
                                <input name="address2" type="text" id="address2" value="<?php echo set_value('address2'); ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label" for="agent_state">State *:</label>
                            <div class="col-lg-6">
                                <select class="state form-control " name="agent_state" id="agent_state">
                                    <option value="">Select State</option>
                                    <?php
                                    foreach ($states as $row => $value) {
                                        echo "<option value='" . $value['intStateId'] . "'>" . $value['stState'] . "</option>";
                                    }
                                    ?>
                                </select>   
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">                                    
                            <label class="col-lg-3 control-label " for="agent_city">City *:</label>
                            <div class="col-lg-6">
                                <select class="agent_city form-control " name="agent_city" id="agent_city">
                                    <option value="">Select City</option>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label" for="pincode">Pin Code *:</label>
                            <div class="col-lg-6">
                                <input name="pincode" type="text" id="pincode" value="<?php echo set_value('pincode'); ?>" minlength = "6" maxlength="6" class="form-control" autocomplete="off">
                            </div>  
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <hr>
                        <?php if ($creat_role == DISTRIBUTOR) { ?>
                            <div class="distributor_show"> 
                                <section class="content-header">
                                    <h4 style="margin-left:10px">Other Information</h4>
                                </section>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>

                                <div class="form-group distributor intent">
                                    <label class="col-lg-3 control-label" for="sellerpoint">Seller Point:* </label>
                                    <!--  <div class="col-lg-6">  
                                    <?php if (is_array($sellerpoints)) { ?>
                                                                                                                                        <div class="col-md-4 form-group">
                                                                                                                                         <input type ='checkbox' id="checkAllOnce" class='col-lg-1 checkAll' style="display: block;">
                                                                                                                                         <span>All</span>
                                                                                                                                         </div>     
                                        <?php foreach ($sellerpoints as $sellerpoint) { ?>
                                                                                                                                                                                                                    <div class="col-md-4 form-group">
                                                                                                                                                                                                                    <input type ='checkbox' id='seller' value='<?php echo $sellerpoint->BUS_STOP_CD; ?>' class='col-lg-1' name ='user_seller_point[]' style="display: block;" />
                                                                                                                                                                                                                    <span><?php echo $sellerpoint->BUS_STOP_NM; ?></span> 
                                                                                                                                                                                                                    </div>
                                            <?php
                                        }
                                    }
                                    ?>                                  
                                     </div>   -->
                                    <div class="col-lg-6">
                                        <select name="user_seller_point[]" id="user_seller_point" class="form-control select2 " multiple="true">
                                            <option value="" id="select_all">All</option>
                                            <!--    <option value="" id="Deselect_all">De-Select All</option> -->
                                            <?php if (is_array($sellerpoints)) { ?>
                                                <?php foreach ($sellerpoints as $sellerpoint) { ?>
                                                    <option value="<?php echo $sellerpoint->BUS_STOP_CD; ?>"><?php echo $sellerpoint->BUS_STOP_NM . '  (' . $sellerpoint->BUS_STOP_CD . ')'; ?></option>
                                                <?php } ?> <?php } ?>

                                        </select>
                                        <h6>(Press "Ctrl" to select mutiple seller point)</h6>
                                    </div>  

                                    <span id="usersellerpointmsg" class="err"></span>
                                </div>
                                <div class="clearfix distributor" style="height: 10px;clear: both;"></div>

                            <?php } ?>

                            <?php if ($creat_role == RETAILER_ROLE_ID) { ?>
                                <div class="retailer_show"> 
                                    <section class="content-header">
                                        <h4 style="margin-left:10px">Other Information</h4>
                                    </section>
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <!-- <div class="form-group">
                                     <label class="col-lg-3 control-label" for="bType">Business Type:* </label>
                                     <div class="col-lg-6">
                                         <select name="bType" id="bType" class="form-control ">
                                             <option value="">Please select</option>  
                                    <?php //if(isset($btypes)) { ?> 
                                    <?php //foreach ($btypes as $btype){  ?>
                                                 <option value="<?php //echo $btype->btype_id;                      ?>"><?php //echo $btype->btype;                      ?></option>
                                    <?php //}   ?> 
                                    <?php //}    ?>
                                         </select>
                                     </div>                               
                                 </div> -->
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <!-- <div class="form-group retailer">
                                        <label class="col-lg-3 control-label" for="scheme">Select Scheme:* </label>
                                        <div class="col-lg-6">
                                            <select name="scheme" id="scheme" class="form-control ">
                                                <option value="">Please select</option>  
                                    <?php //if(is_array($schemes)) { ?> 
                                    <?php //foreach ($schemes as $scheme) {   ?>
                                                    <option value="<?php //echo $scheme->scheme_id;                       ?>"><?php //echo $scheme->scheme;                       ?></option>
                                    <?php //}    ?> <?php //}   ?>
                                            </select>
                                        </div>
                                        <span id="schememsg" class="err"></span>                               
                                    </div> -->
                                    <input name="bType" id="bType" type="hidden" value="0" class="form-control" autocomplete="off">

                                    <input name="scheme" id="scheme" type="hidden" value="0" class="form-control" autocomplete="off">

                                    <div class="clearfix retailer" style="height: 10px;clear: both;"></div>

                                    <div class="form-group retailer">
                                        <label class="col-lg-3 control-label" for="sellerpoint">Seller Point:* </label>
                                        <div class="col-lg-6">
                                            <select name="sellerpoint" id="sellerpoint" class="form-control select2 ">
                                                <option value="">Please select seller point</option>  
                                                <?php if(!empty($sellerpoints) && is_array($sellerpoints)) {  ?>
                                                <?php foreach ($sellerpoints as $sellerpoint) {   ?>
                                                     <option value="<?php echo $sellerpoint->BUS_STOP_CD;                       ?>">
                                                         <?php echo $sellerpoint->BUS_STOP_NM.'  ('.$sellerpoint->BUS_STOP_CD.')';   ?></option>
                                                <?php }    ?> <?php }   ?>

                                            </select>
                                        </div>  
                                        <span id="sellerpointmsg" class="err"></span>
                                    </div>
                                    <div class="clearfix retailer" style="height: 10px;clear: both;"></div>

                                    <div class="form-group retailer">
                                        <label class="col-lg-3 control-label" for="sellerdesc">Sellerpoint(description):* </label>
                                        <div class="col-lg-6">
                                            <input name="sellerdesc" id="sellerdesc" class="form-control " placeholder="Description" autocomplete="off" type="text">
                                        </div>
                                        <span id="sellerdescmsg" class="err"></span>
                                    </div>
                                    <div class="clearfix retailer" style="height: 10px;clear: both;"></div>

                                    <input name="topUpLimit" id="topUpLimit" type="hidden" value="0" class="form-control" autocomplete="off">

                                    <input name="hhmLimit" id="hhmLimit" type="hidden" value="0" class="form-control" autocomplete="off">

                                    <!-- <div class="form-group retailer">
                                         <label class="col-lg-3 control-label" for="topUpLimit">Top Up Limit:*</label>
                                            <div class="col-lg-6">
                                             <input name="topUpLimit" id="topUpLimit" class="form-control " placeholder="Top Up Limit" autocomplete="off" type="text">
                                            </div>
                                        <span id="topUpLimitmsg" class="err"></span>
                                    </div>
                                    <div class="clearfix retailer" style="height: 10px;clear: both;"></div>
                                    
                                    <div class="form-group retailer">
                                        <label class="col-lg-3 control-label" for="hhmLimit">HHM Limit:* </label>
                                        <div class="col-lg-6">
                                            <input name="hhmLimit" id="hhmLimit" class="form-control " placeholder="HHM Limit" autocomplete="off" type="text">
                                        </div>
                                        <span id="hhmLimitmsg" class="err"></span>
                                    </div> -->
                                    <div class="clearfix retailer" style="height: 10px;clear: both;"></div>
                                    <div class="form-group retailer">
                                        <label class="col-lg-3 control-label" for="depot_code">Depot Name:* </label>
                                        <div class="col-lg-6">
                                            <select name="depot_code" id="depot_code" class="form-control select2 ">
                                                <option value="">Please Select Depot Name</option>  
                                                <!-- <?php if (is_array($depots)) { ?>
                                                    <?php foreach ($depots as $depot) { ?>
                                                                                                                                                                                                                    <option value="<?php echo $depot->DEPOT_CD; ?>"><?php echo $depot->DEPOT_NM; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?> -->
                                            </select>
                                        </div>  
                                        <span id="depot_codemsg" class="err"></span>
                                    </div>
                                    <div class="clearfix retailer" style="height: 10px;clear: both;"></div>
                                    <div class="form-group retailer">
                                        <label class="col-lg-3 control-label" for="securityDep">Security Deposit:* </label>
                                        <div class="col-lg-6">
                                            <input name="securityDep" id="securityDep" class="form-control " placeholder="Security Deposit" autocomplete="off" type="text">
                                        </div>
                                    </div>
                                    <div class="clearfix retailer" style="height: 10px;clear: both;"></div>
                                <?php } ?>

                                <hr>
                                <section class="content-header">
                                    <h4 style="margin-left:10px">Bank Account Detail</h4>
                                </section>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="uname">Account No:* </label>
                                    <div class="col-lg-6">
                                        <input type="text" name="accno" id="accno" class="form-control" placeholder="Account Number" value="<?php echo set_value('accno'); ?>"   data-msg-required="Please Enter Account Number" autocomplete="off" maxlength="21">
                                    </div> 
                                </div>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="uname">Bank Name:* </label>
                                    <div class="col-lg-6">
                                        <input type="text" name="bname" id="bname"class="form-control upper" placeholder="Bank Name" data-msg-required="Please Enter Bank Name" autocomplete="off" value="<?php echo set_value('bname'); ?>" >
                                    </div>    
                                </div>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="uname">Account Holder  Name:* </label>
                                    <div class="col-lg-6">
                                        <input type="text" name="accname" value="" id="accname"   class="form-control upper"  placeholder="Account Holder name" value="<?php echo set_value('accname'); ?>">
                                    </div>
                                </div>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="uname">Branch Name:* </label>
                                    <div class="col-lg-6">
                                        <input type="text" name="brname" type="text" id="brname" class="form-control upper" placeholder="Branch Name" data-msg-required="Please Enter Branch Name" value="<?php echo set_value('brname'); ?>" autocomplete="off">
                                    </div>  
                                </div>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label" for="uname">IFSC Code:* </label>
                                    <div class="col-lg-6">
                                        <input type="text" name="ifsc" maxlength = 11 value="" id="ifsc"  data-msg-required="Please Enter IFSC code" data-msg-lfsccode="Please enter valid IFSC Code" class="form-control upper"  placeholder="IFSC Code"  value="<?php echo set_value('ifsc'); ?>" style="text-transform:uppercase">
                                    </div>
                                </div>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>



                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <div class="col-lg-offset-5">
                                    <button class="btn btn-primary" id="submit_btn" name="submit_btn" type="submit">Save</button> 
                                    <a class="btn btn-primary" href="<?php echo base_url('admin/registration') ?>" type="button">Back</a> 
                                </div>


                                <!--</div> /.box-body--> 
                            </div><!-- /.box -->
                            <?php echo form_close(); ?>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                    </section><!-- /.content -->
                </div><!-- /.content-wrapper -->


                <script>
                    var cmp_on_change_flag = '<?php echo $cmp_on_change_flag; ?>';
                    var md_on_change_flag = '<?php echo $md_on_change_flag; ?>';
                    var ad_on_change_flag = '<?php echo $ad_on_change_flag; ?>';
                    var show_retailor_flag = '<?php echo $show_retailor_flag; ?> ';
                    var creat_role = '<?php echo $creat_role; ?>';
                </script>

                <script src="<?php echo base_url() ?>js/back/admin/registration.js"></script>
                <style>
                    /****DATEPICKER YEAR MONTH DROPDOWNN *******/
                    .ui-datepicker .ui-datepicker-title select {
                        color: #000;
                    }
                </style>
                <script type="text/javascript">
                    /*   $(document).off('change', '.show_ret').on('change', '.show_ret', function(e)
                     {
                     $('.retailer_show').show();
                     });
                     var current_role = '<?php //echo $this->session->userdata('role_id');                        ?>';
                     var distributor_role = '<?php //echo DISTRIBUTOR ;                       ?>'
                     if(current_role == distributor_role)
                     {
                     $('.retailer_show').show();
                     }*/
                    // $(function() {
                    //         $('.retailer_show').hide(); 
                    //         $('#service_name').load(function(){
                    //             if($('#service_name').val() == '2') {
                    //                 $('.retailer_show').show(); 
                    //             } else {
                    //                 $('.retailer_show').hide(); 
                    //             } 

                    //         });
                    //    });

                    function showServices() {
                        $('.retailer_show').hide();
                        if ($('#service_name').val() == '2') {
                            $('.retailer_show').show();
                        } else {
                            $('.retailer_show').hide();
                        }
                    }

                    /*$(document).ready(function() {
                     $('#multiple-checkboxes').multiselect();
                     });*/

                    /*$(document).ready(function() {
                     $('#service_name').multiselect({
                     nonSelectedText: 'Select Services',
                     enableFiltering: true,
                     enableCaseInsensitiveFiltering: true,
                     buttonWidth: '510px'
                     });
                     });*/
    
    var url = BASE_URL+'admin/registration/check_smartcard_id';
        var smartcard_id;
        $.ajax({
            type:'GET',
            url: url,
            dataType: 'json',
            success: function(response){
                smartcard_id = response[0].id;
            }
        });


       $('input').on('ifChecked', function(event){


        if ($(this).val() === smartcard_id && smartcard_id != 'undefined') {

            $("#division_div").show();
            $("#depot_div").show();

        }
       
 
          });

       $('input').on('ifUnchecked', function(event){
            if ($(this).val() === smartcard_id && smartcard_id != 'undefined') {
                      $("#division_div").hide();
                      $("#depot_div").hide(); 
                }
          });
              
        $('#depot_cd').change(function(){
           
            var ajax_url = BASE_URL+'admin/registration/get_division';
            var depot_cd = $( "#depot_cd option:selected" ).val();
            $.ajax({
                type:'POST',
                url: ajax_url,
                data: {depot_cd: depot_cd},
                dataType: 'json',
                success: function(response){
                    $("#division_nm").val(response.DIVISION_NM);
                    $("#division_cd").val(response.DIVISION_CD);
                }
            });

        });

                </script>          
