<section class="content-header">
    <h2>Users List
    </h2>
    <div class="box-body table-responsive" id="table_show">             
      <div class="row box-title">
                            <?php $attributes = array('class' => 'form', 'id' => 'user_view', 'name'=>'user_view','method' => 'post','onsubmit'=>'return false;');
                              echo form_open('', $attributes);
                            ?>
    <div style="display: block;" class="col-md-2 display-no">
        <div style="position: static;" class="form-group">
            <label for="input-text-1">From Date</label>
              <input class="form-control date" id="from_date" placeholder="<?php echo date('d-m-Y'); ?> " value="<?php echo (!empty($from_date)) ? $from_date : date('d-m-Y'); ?>" type="text">
         </div>
    </div>
    <div style="display: block;" class="col-md-2 display-no">
        <div style="position: static;" class="form-group">
            <label for="input-text-2">To Date</label>
                <input class="form-control date" id="to_date" placeholder="<?php echo date('d-m-Y'); ?>" value="<?php echo (!empty($till_date)) ? $till_date : date('d-m-Y'); ?>" type="text">
        </div>
    </div>
    <div class="col-md-2">
      <div style="position: static;" class="form-group">
        <label for="input-text-1">User Type</label>
          <select class="form-control" name="user_type" id="user_type">
                       <option value="">-Select User-</option>
                       <?php if(!empty($role)) { 
                                foreach ($role as $key => $value) { ?>
                                  <option value=<?php echo $value['id']; ?>><?php echo $value['role_name'];?>       </option>
                          <?php }
                              }
                        ?>
          </select>
        
      </div>
  </div>
      <div class="col-md-2">
            <div style="position: static;" class="form-group">
                <label for="input-text-2">RMN Verified</label>
                 <select class="form-control" name="rmn_verified" id="rmn_verified">
                       <option value="">-Select RMN-</option>
                       <option value="Y">Yes</option>
                       <option value="N">No</option>
                </select>
            </div>
               
        </div>
                             
        <div class="col-md-2">
            <div style="position: static;" class="form-group">
                <label for="input-text-2">Status</label>
                <select class="form-control" name="status" id="status">
                      <option value="">-Select Status-</option>
                       <option value="Y">Y</option>
                       <option value="N">N</option>
                </select>
            </div>
        </div> 
        <div class="col-md-2">
            <div style="position: static;" class="form-group">
                <label for="input-text-2">KYC Status</label>
                <select class="form-control" name="kyc_status" id="kyc_status">
                      <option value="">-Select KYC Status-</option>
                      <option value="Y" selected="selected">Y</option>
                      <option value="N">N</option>
                </select>
            </div>
        </div>  
        <div class="col-md-2">
            <div style="position: static;" class="form-group">
                <label for="input-text-2">Email Status</label>
                <select class="form-control" name="email_status" id="email_status">
                    <option value="">-Select Email Status-</option>
                    <option value="Y" selected="selected">Y</option>
                    <option value="N">N</option>
                </select>
            </div>
        </div>   
    <div class="col-xs-1"><br>
         <button class="btn btn-primary" id="ticket_data_filter_btn" name="ticket_data_filter_btn" type="submit">Filter</button> 
    </div>  

   <!--  <div style="display: block;float: right;margin-top: 2%;" class="form-group"> -->
      <div class="col-xs-1"><br>
           <button  class="btn btn-primary" id="extoexcel" type="button">
       <span class="glyphicon glyphicon-export"></span>Export To Excel</button>  
    </div>
<!--    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="">Registration</li>
        <li class="active">Users List</li>
   	</ol>-->
     <?php echo form_close(); ?>
    </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div id="menu_wrapper">
        <?php
        $this->load->view(ALL_ROKAD_USERS_AJAX);
        ?>
    </div>

    <!--?php $this->load->view(ORS_USERS_POPUP); ?-->
</section>
<!--<script src="<?php echo base_url() ?>js/back/admin/registration.js"></script>-->