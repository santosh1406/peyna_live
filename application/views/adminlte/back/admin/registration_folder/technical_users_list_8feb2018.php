
<style>
label.error, div.error, ul.error, span.error {
    color: #f00;
    font-size: 14px;
}
</style>
<section class="content-header">
    <h3>Technical Users List</h3>
</section>

<section class="content">
  <div id="wallet_wrapper">
    <div class="row">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body" id="table_show">
              <table id="op_rept" name="op_rept" class="table table-striped">
                <thead>
                  <tr>
                    <th>Sr.No.</th>
                    <th>User Name</th>
                    <th>Role Name</th>
                    <th>Mobile No</th>
                    <th>Email Id</th>
                    <th>Email Verify Status</th>
                    <th>Created Date</th>
                  </tr>
                </thead>
                <tbody>                                           
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix" style="height: 10px;clear: both;"></div>   
    </div>   
  </div>     
</section>
<script>
$(document).ready(function() {
  var user_data;
             
  var tconfig = {
    "processing": true,
    "serverSide": true,
    "searching": true,
    "iDisplayLength": 10,
    "aLengthMenu": [[10, 25,50, -1], [10, 25,50, "All"]],
    "order": [[2, "desc"]],
    "ajax": {
      "url": BASE_URL+"admin/registration/technical_users",
      "type": "POST",
      "data": "json",
      data   : function (d) {
        d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;
      }
    },
    "fnRowCallback": function(nRow, aData, iDisplayIndex) {
      $("td:first", nRow).html(iDisplayIndex + 1);
      return nRow;
            
    },
    "columnDefs": [{
        "searchable": false
    }],
    "scrollX": true,
    "bFilter": false,
    "bPaginate": true,
    "bRetrieve": true,
    "aoColumnDefs": [
      {
        "bSortable": false,  
        "aTargets": [0]
      },      
    ],
    "oLanguage": {
      "sProcessing": '<img src="<?php echo base_url().BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>' 
    },
  };
  user_data= $('#op_rept').dataTable(tconfig);

  $(document).off('click', '#op_generate_btn').on('click', '#op_generate_btn', function (e) {
    user_data.fnDraw();       
  });
});

</script>