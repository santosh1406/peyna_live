    <!-- Content Header (Page header) -->
    <?php if ($view_data[0]['role_id'] == MASTER_DISTRIBUTOR_ROLE_ID) { ?>
            <section class="content-header">
                <h2 style="margin-left:20px">Regional Distributor Details</h2>
<!--                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="">Registration</li>
                    <li class="active">RD Registration</li>
                </ol>-->
            </section>
        <?php } else if ($view_data[0]['role_id'] == AREA_DISTRIBUTOR_ROLE_ID) { ?>
            <section class="content-header">
                <h2 style="margin-left:20px">Divisional Distributor Details</h2>
            </section>
        <?php } else if ($view_data[0]['role_id'] == DISTRIBUTOR) { ?>
            <section class="content-header">
                <h2 style="margin-left:20px">Executive Details</h2>
            </section>
        <?php } else if ($view_data[0]['role_id'] == RETAILER_ROLE_ID) { ?>
            <section class="content-header">
                <h2>Sale Agent Details</h2>
            </section>
        <?php } ?>
        <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> </div>
                    <div class="box-body">
                            <?php if ($view_data[0]['role_id'] != MASTER_DISTRIBUTOR_ROLE_ID && $view_data[0]['level'] > MASTER_DISTRIBUTOR_LEVEL) { ?>
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <div class="form-group"> 
                                        <label class="col-lg-3 control-label" for="md">Regional Distributor :</label>
                                        <div class="col-lg-6">
                                            <input type="text" value="<?php echo $level_2[0]->display_name ; ?>" class="form-control" disabled/>   
                                        </div>
                                    </div>
                                <?php } if ($view_data[0]['role_id'] == DISTRIBUTOR || $view_data[0]['role_id'] == RETAILER_ROLE_ID && $view_data[0]['level'] != MASTER_DISTRIBUTOR_LEVEL && $view_data[0]['level'] != AREA_DISTRIBUTOR_LEVEL) { ?>
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <div class="form-group"> 
                                        <label class="col-lg-3 control-label" for="ad">Divisional Distributor :</label>
                                        <div class="col-lg-6">
                                             <input type="text" value="<?php echo $level_3[0]->display_name ; ?>" class="form-control" disabled/>   
                                        </div>
                                    </div>
                                <?php } if($view_data[0]['role_id'] == RETAILER_ROLE_ID && $view_data[0]['level'] == RETAILOR_LEVEL && $view_data[0]['level'] != POS_DISTRIBUTOR_LEVEL) { ?>
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <div class="form-group"> 
                                        <label class="col-lg-3 control-label" for="distributor">Executive :</label>
                                        <div class="col-lg-6">
                                           <input type="text" value="<?php echo $level_4[0]->display_name ; ?>" class="form-control" disabled/>   
                                        </div>
                                    </div>
                                <?php }?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <hr>
                            <div class="form-group">
                                 <section class="content-header">
                                <h4 style="margin-left:10px">Login Information</h4>
                                 </section>
                                 <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <label class="col-lg-3 control-label" for="email">Email : </label>
                                <div class="col-lg-6">
                                    <input type="text" value="<?php echo rokad_decrypt($view_data[0]['email'], $this->config->item("pos_encryption_key")) ?>" class="form-control" disabled />
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="mobile_no">Mobile no : </label>
                                <div class="col-lg-6">
                                    <input value="<?php echo rokad_decrypt($view_data[0]['mobile_no'], $this->config->item("pos_encryption_key")) ?>" type="text" class="form-control" disabled/>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <label class="col-lg-3 control-label">Company GST No : </label>
                                <div class="col-lg-6">
                                    <input type="text" value="<?php echo $view_data[0]['company_gst_no']; ?>" class="form-control" disabled> 
                                </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <hr>
                            <div class="form-group">
                                <section class="content-header">
                                    <h4 style="margin-left:10px">General</h4>
                                    </section>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <label class="col-lg-3 control-label" for="firm_name">Name of Firm/Company :</label>
                                <div class="col-lg-6">
                                    <input type="text" value="<?php echo $view_data[0]['firm_name'] ?>" class="form-control" disabled/>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="first_name">First Name :</label>
                                <div class="col-lg-6">
                                    <input type="text" value="<?php echo $view_data[0]['first_name'] ?>" class="form-control" disabled/>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="last_name">Last Name :</label>
                                <div class="col-lg-6">
                                    <input type="text" value="<?php echo $view_data[0]['last_name'] ?>" class="form-control" disabled/>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="gender"> Gender : </label>
                                <div class="col-lg-6">
                                   <!--<input type="text" value="<?php if($view_data[0]['gender']=="M")echo "Male"; else echo "Female"; ?>" class="form-control" disabled/>-->
                                   <input type="text" value="<?php if($view_data[0]['gender']=="M"){echo "Male";} else if($view_data[0]['gender']=="T"){echo "Transgender"; }else {echo "Female";} ?>" class="form-control" disabled/>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="dob">Pan / Tan Card :</label>
                                <div class="col-lg-6">
                                    <input type="text" value="<?php echo rokad_decrypt($view_data[0]['pancard'], $this->config->item("pos_encryption_key")) ?>" class="form-control" disabled/>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="dob">DOB (DD-MM-YYYY) :</label>
                                <div class="col-lg-6">
                                    <input type="text" value="<?php $date=rokad_decrypt($view_data[0]['dob'], $this->config->item("pos_encryption_key")); echo date("d-m-Y",strtotime($date));?>" class="form-control" disabled/>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <hr>
                            <section class="content-header">
                                    <h4 style="margin-left:10px">Contact Information</h4>
                                </section>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">                                    
                                <label class="col-lg-3 control-label" for="address1">Address1 :</label>
                                <div class="col-lg-6">
                                    <input type="text" value="<?php echo $view_data[0]['address1'] ?>" class="form-control" disabled/>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="address2">Address2 :</label>
                                <div class="col-lg-6">
                                   <input type="text" value="<?php echo $view_data[0]['address2'] ?>" class="form-control" disabled/>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="agent_state">State :</label>
                                <div class="col-lg-6">
                                   <input type="text" value="<?php echo $state_name[0]->stState ?>" class="form-control" disabled/>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">                                    
                                <label class="col-lg-3 control-label " for="agent_city">City :</label>
                                <div class="col-lg-6">
                                    <input type="text" value="<?php echo $city_name[0]->stCity ?>" class="form-control" disabled/>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="pincode">Pin Code :</label>
                                <div class="col-lg-6">
                                    <input type="text" value="<?php echo rokad_decrypt($view_data[0]['pincode'], $this->config->item("pos_encryption_key")) ?>" class="form-control" disabled/>
                                </div>  
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <?php if(! empty($bank_detail) ) { ?>
                             <hr>
                                    <section class="content-header">
                                        <h4 style="margin-left:10px">Bank Account  Detail</h4>
                                    </section>
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <label class="col-lg-3 control-label" for="firm_name">Account No: :</label>
                                    <div class="col-lg-6">
                                        <input type="text"  class="form-control" value="<?php echo $bank_detail[0]->bank_acc_no; ?>" disabled>
                                    </div>
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="uname">Bank Name: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" value="<?php echo  $bank_detail[0]->bank_name;?>" disabled>
                                        </div>    
                                    </div>
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="uname">Account Holder Name: </label>
                                      <div class="col-lg-6">
                                         <input type="text" name="accname" class="form-control" value="<?php echo $bank_detail[0]->bank_acc_name; ?>" disabled>
                                    </div>
                                </div>
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="uname">Branch Name: </label>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" value="<?php echo $bank_detail[0]->bank_branch; ?>" disabled>
                                        </div>  
                                    </div>
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>

                                    <div class="form-group">
                                        <label class="col-lg-3 control-label" for="uname">IFSC Code: </label>
                                        <div class="col-lg-6">
                                            <input type="text" value="<?php echo $bank_detail[0]->bank_ifsc_code; ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                   
                                    <?php }?>
                            <hr>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <div class="col-lg-offset-5">
                                   
                                    <a class="btn btn-primary" href="<?php echo base_url('admin/registration') ?>" type="button">Back</a> 
                                </div>
                            </div>



                      

                        <!--</div> /.box-body--> 
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<script>
    
    var md_on_change_flag = '<?php echo $md_on_change_flag;?>';
    var ad_on_change_flag = '<?php echo $md_on_change_flag;?>';
</script>
<script src="<?php echo base_url() ?>js/back/admin/registration.js"></script>
<style>
    /****DATEPICKER YEAR MONTH DROPDOWNN *******/
    .ui-datepicker .ui-datepicker-title select {
        color: #000;
    }
</style>