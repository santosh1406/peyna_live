<style>
    //added by harshada kulkarni on 12-07-2018
    .overflowClass{
        //border: 1px solid #ccc;
        height: 50px;
        width: 48.5%;
        overflow-x:scroll;
        overflow-x:hidden;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php if ($edit_data[0]['level'] == MASTER_DISTRIBUTOR_LEVEL) { ?>
        <section class="content-header">
            <h2 style="margin-left:20px">Edit Regional Distributor</h2>
        </section>
    <?php } else if ($edit_data[0]['level'] == AREA_DISTRIBUTOR_LEVEL) { ?>
        <section class="content-header">
            <h2 style="margin-left:20px">Edit Divisional Distributor</h2>
        </section>
    <?php } else if ($edit_data[0]['level'] == POS_DISTRIBUTOR_LEVEL) { ?>
        <section class="content-header">
            <h2 style="margin-left:20px">Edit Executive</h2>
        </section>
    <?php } else if ($edit_data[0]['level'] == RETAILOR_LEVEL) { ?>
        <section class="content-header">
            <h2 style="margin-left:20px">Edit Sales Agent</h2>
        </section>
    <?php } ?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> </div>
                    <div class="box-body">
                        <?php echo validation_errors('<div class="error">', '</div>'); ?>
                        <?php
                        $attributes = array("method" => "POST", "id" => "edit_registration_form", "name" => "edit_registration_form");
                        echo form_open(base_url() . 'admin/registration/update_details', $attributes);
                        ?>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="firm_name">Name of Firm/Company *:</label>
                            <div class="col-lg-6">
                                <input name="id" type="hidden" id="id" value="<?php echo rokad_encrypt($edit_data[0]['id'], $this->config->item("pos_encryption_key")); ?>" class="form-control" autocomplete="off">
                                <input name="firm_name" type="text" id="firm_name" value="<?php echo $edit_data[0]['firm_name']; ?>" class="form-control name" autocomplete="off">
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <!-- added by harshada kulkarni on 12-07-2018 - start-->
                        <?php if ($edit_data[0]['role_id'] == RETAILER_ROLE_ID) { ?>
                            <div class="form-group intent"> 
                                <label class="col-lg-3 control-label" for="service_name">Select Services *:</label>
                                <div class="col-lg-2 overflowClass" onload="showServices()">                                    
                                    <?php 
                                    if (is_array($serviceDetails)) {
                                        $checked = "";
                                        foreach ($serviceDetails as $sd) {
                                            foreach ($agentServices as $as) {
                                                if ($sd->id == $as->service_id) {
                                                    $checked = "checked";
                                                    break;
                                                } else {
                                                    $checked = "";
                                                }
                                            }
                                            ?>
                                            <input type ="checkbox" name ="service_name[]" id ="service_name" value="<?php echo $sd->id; ?>" class="icheckbox_minimal" <?php echo $checked; ?>/>
                                            <?php echo $sd->service_name; ?><br>
                                            <?php
                                        }
                                    }
                                    ?>                                    
                                </div> 
                            </div>
                        <?php } ?> <br>
                        <?php if ($creat_role == RETAILER_ROLE_ID) {
                            ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group" id="depot_div" 
                               style="<?php
                                    $service = array();
                                foreach ($agentServices as $as) {
                                    $service[] = $as->service_id;
                                }
                                if(in_array(SMART_CARD_SERVICE,$service))
                                {
                                  echo 'display:block'; 
                                }  else {
                                  echo 'display:none';
                               }
                               ?>" > 
                                <label class="col-lg-3 control-label" for="distributor">Select Depot *:</label>
                               <div class="col-lg-6">
                                <select class="form-control distributor  show_ret" required="" name="depot_cd" id="depot_cd">
                                        <option value="">Select Depot</option>
                                        <?php
                                        if (isset($depotsCode) && count($depotsCode) > 0) {
                                            foreach ($depotsCode as $dc) {
                                                
                                                ?>
                                                <option value="<?php 
                                                if (!empty($dc['DEPOT_CD']))
                                                 echo $dc['DEPOT_CD']; ?>" <?php echo (($agentdepotList->depot_code==$dc['DEPOT_CD']) ? 'selected="selected" ' : '' ) ?>>
                                                    <?php if (!empty($dc['DEPOT_CD'])) 
                                                    echo $dc['DEPOT_NM']?>
                                                   
                                                    
                                                </option>
                                                <?php
                                           }
                                        }
                                        ?>
                                    </select>    
                                </div>
                            </div>
                        <?php } ?>
                            <?php if ($creat_role == RETAILER_ROLE_ID) { ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group" id="division_div" style="<?php
                                foreach ($agentServices as $as) {
                                    $service[] = $as->service_id;
                                }
                                if(in_array(SMART_CARD_SERVICE,$service))
                                {
                                  echo 'display:block'; 
                                }  else {
                                  echo 'display:none';
                               }
                               ?>"> 
                                <label class="col-lg-3 control-label" for="distributor">Division*:</label>
                                <div class="col-lg-6">
                                    <input id="division_nm" required="" name="division_nm" type="text" value="<?php
                                        if (isset($divisionData) && count($divisionData) > 0) {
                                            foreach ($divisionData as $dd) {                                                
                                                 echo (($agentdepotList->depot_code==$dd->DEPOT_CD) ? $dd->DIVISION_NM : '' );                                                
                                        }
                                        
                                            }
                                        ?>" class="form-control" maxlength="10" autocomplete="off"></input> 
                                    <input type="hidden" id="division_cd" name="division_cd" value="<?php
                                        if (isset($divisionData) && count($divisionData) > 0) {
                                            foreach ($divisionData as $dd) {                                                
                                                 echo (($agentdepotList->depot_code==$dd->DEPOT_CD) ? $dd->DIVISION_CD : '' );                                                
                                        }
                                        
                                            }
                                        ?>">
                                    
                                </div>
                            </div>
                        <?php } ?>
                        <!-- added by harshada kulkarni on 12-07-2018 - end-->
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="first_name">First Name *:</label>
                            <div class="col-lg-6">
                                <input name="first_name" type="text" id="first_name" value="<?php echo $edit_data[0]['first_name']; ?>" class="form-control name" autocomplete="off">
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="last_name">Last Name *:</label>
                            <div class="col-lg-6">
                                <input name="last_name" type="text" id="last_name" value="<?php echo $edit_data[0]['last_name']; ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="badge_no">Badge no : </label>
                            <div class="col-lg-6">
                                <input id="badge_no" name="badge_no" maxlength="10" type="text" value="<?php echo $edit_data[0]['badge_no']; ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <!--                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                                                    <div class="form-group">
                                                        <label class="col-lg-3 control-label" for="mobile_no">Mobile no *: </label>
                                                        <div class="col-lg-6">
                                                            <input name="mobile_no" type="text" minlength = "10" maxlength="10" id="mobile_no" value="<?php echo $edit_data[0]['']; ?>" class="form-control" autocomplete="off">
                                                        </div>
                                                    </div>-->
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="gender"> Gender : </label>
                            <div class="col-lg-6">
                                <select class="form-control" id="gender"  name="gender">
                                    <option value="">Select</option>
                                    <option value="M" <?php echo (($edit_data[0]['gender'] == 'M') ? 'selected="selected" ' : '' ) ?>>Male</option>
                                    <option value="F" <?php echo (($edit_data[0]['gender'] == 'F') ? 'selected="selected" ' : '' ) ?>>Female</option>
                                    <option value="T" <?php echo (($edit_data[0]['gender'] == 'T') ? 'selected="selected" ' : '' ) ?>>Transgender</option>
                                </select>
                                <span id="prov_type_msg" class="err"></span>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="dob">Pin Code *:</label>
                            <div class="col-lg-6">
                                <input type="text" name="pincode" id="pincode" value="<?php echo rokad_decrypt($edit_data[0]['pincode'], $this->config->item("pos_encryption_key")); ?>" class="form-control" maxlength="10" autocomplete="off" />
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="dob">DOB (DD-MM-YYYY) *:</label>
                            <div class="col-lg-6">
                                <input name="dob" type="text" id="dob" value="<?php
                                $dob = rokad_decrypt($edit_data[0]['dob'], $this->config->item("pos_encryption_key"));
                                echo date("d-m-Y", strtotime($dob));
                                ?>" class="form-control"     >
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <hr>
                        <div class="form-group">                                    
                            <label class="col-lg-3 control-label" for="address1">Address1 *:</label>
                            <div class="col-lg-6">
                                <input name="address1" type="text" id="address1" value="<?php echo $edit_data[0]['address1']; ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label" for="address2">Address2 :</label>
                            <div class="col-lg-6">
                                <input name="address2" type="text" id="address2" value="<?php echo $edit_data[0]['address2']; ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label" for="agent_state">State *:</label>
                            <div class="col-lg-6">
                                <select class="state form-control" name="agent_state" id="agent_state" value="<?php echo $edit_data[0]['state'] ?>" >
                                    <option value="">Select State</option>
                                    <?php
                                    foreach ($states as $row => $value) {
                                        ?>
                                        <option value="<?php echo $value['intStateId']; ?>" <?php echo (($value['intStateId'] == $edit_data[0]['state']) ? 'selected="selected" ' : '' ) ?>> <?php echo $value['stState'] ?></option>
                                    <?php }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">                                    
                            <label class="col-lg-3 control-label" for="agent_city">City *:</label>
                            <div class="col-lg-6">
                                <select class="city form-control" name="agent_city" id="agent_city" >
                                    <option value="">Select City</option>
                                    <?php
                                    if ($city) {
                                        foreach ($city as $key => $value) {
                                            ?>
                                            <option value="<?php echo $value['intCityId']; ?>" <?php echo (($value['intCityId'] == $edit_data[0]['city']) ? 'selected="selected" ' : '' ) ?>> <?php echo $value['stCity'] ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <hr>

                        <?php if ($current_cb_executive_edit) { ?>
                                                                                                    <!-- <input type="hidden" name="old_star_seller_cd" value="<?php //echo $edit_data[0]['star_seller_code'];                 ?>">
                                                                                                    <input type="hidden" name="old_depot_cd" value="<?php //echo $edit_data[0]['depot_cd'];                  ?>">-->
                            <input type="hidden" name="agent_code" value="<?php echo $agent_code; ?>">
                            <input type="hidden" name="user_type" id="user_type" value="<?php echo "executive"; ?>" >
                            <?php //if($show_seller_point==1) { $style='disabled="true"';} else { $style='';}          ?>

                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="agent_state">Seller Point *:</label>
                                <div class="col-lg-6">
                                    <select class="state form-control" name="user_seller_point[]" id="user_seller_point" multiple="multiple" <?php echo $style; ?> >
                                        <option value="" id="select_all">All</option>

                                        <?php if (!empty($sellerpoints)) { ?>  
                                            <?php foreach ($sellerpoints as $sellerpoint) { ?>
                                                <option value="<?php echo $sellerpoint->BUS_STOP_CD; ?>">
                                                    <?php echo $sellerpoint->BUS_STOP_NM . '  (' . $sellerpoint->BUS_STOP_CD . ')'; ?></option>
                                                <?php
                                            }
                                            ?> 
                                        <?php } ?>
                                    </select>  
                                    <h6>(Press "Ctrl" to select mutiple seller point)</h6>
                                </div>
                            </div>
                            <!-- <div class="clearfix" style="height: 10px;clear: both;"></div>
                             <div class="form-group"> 
                                 <label class="col-lg-3 control-label" for="address2">TOpup Limit :</label>
                                 <div class="col-lg-6">
                                     <input name="topuplmit" type="text" id="topuplmit" value="<?php // echo $edit_data[0]['topuplmit'];                   ?>" class="form-control" autocomplete="off">
                                 </div>
                             </div>-->

                            <!-- <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="agent_state">Depot Name *:</label>
                                <div class="col-lg-6">
                                    <select class="state form-control" name="depot_code" id="depot_code">
                            <?php //if(is_array($depots)) {      ?>
                            <?php //foreach ($depots as $value) {      ?>
                                         <option value="<?php //echo $value->DEPOT_CD;             ?>" <?php //echo (($value->DEPOT_CD == $edit_data[0]['depot_cd']) ? 'selected="selected" ' : '' )             ?>> <?php //echo $value->DEPOT_NM             ?></option>
                            <?php //} }        ?>
                                    </select>
                                </div>
                            </div>-->

                            <!-- <div class="clearfix" style="height: 10px;clear: both;"></div> -->
                            <!-- <hr> -->
                        <?php } ?> 

                        <?php //if($current_cb_agent_edit) {         ?>
<!-- <input type="hidden" name="old_star_seller_cd" value="<?php //echo $edit_data[0]['star_seller_code'];                ?>">
<input type="hidden" name="old_depot_cd" value="<?php //echo $edit_data[0]['depot_cd'];                  ?>">-->
<!--    <input type="hidden" name="agent_code" value="<?php //echo $agent_code;                  ?>">
<input type="hidden" name="user_type" id="user_type" value="<?php //echo "sale_agent";                ?> >

                        <?php //if($show_seller_point==1) { $style='disabled="true"';} else { $style='';}          ?>
<div class="form-group"> 

    <label class="col-lg-3 control-label" for="agent_state">Seller Point *:</label>
    <div class="col-lg-6">
        <select class="state form-control" name="Seller_point" id="Seller_point" value="<?php //echo $edit_data[0]['star_seller_code']              ?>" <?php //echo $style;              ?> >
                        <?php //if(is_array($sellerpoints)) {       ?>
                        <?php //foreach ($sellerpoints as $value) {      ?>
                    <option value="<?php //echo $value->BUS_STOP_CD;           ?>" <?php //echo (($value->BUS_STOP_CD == $edit_data[0]['star_seller_code']) ? 'selected="selected" ' : '' )           ?>> <?php //echo $value->BUS_STOP_NM           ?></option>
                        <?php //}     ?> 
                        <?php //}       ?>
        </select>
    </div>
</div> -->
                        <!-- <div class="clearfix" style="height: 10px;clear: both;"></div>
                         <div class="form-group"> 
                             <label class="col-lg-3 control-label" for="address2">TOpup Limit :</label>
                             <div class="col-lg-6">
                                 <input name="topuplmit" type="text" id="topuplmit" value="<?php //echo $edit_data[0]['topuplmit'];                   ?>" class="form-control" autocomplete="off">
                             </div>
                         </div>-->

                        <!-- <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label" for="agent_state">Depot Name *:</label>
                            <div class="col-lg-6">
                                <select class="state form-control" name="depot_code" id="depot_code">
                        <?php //if(is_array($depots)) {      ?>
                        <?php //foreach ($depots as $value) {      ?>
                                     <option value="<?php //echo $value->DEPOT_CD;             ?>" <?php //echo (($value->DEPOT_CD == $edit_data[0]['depot_cd']) ? 'selected="selected" ' : '' )             ?>> <?php //echo $value->DEPOT_NM             ?></option>
                        <?php //} }        ?>
                                </select>
                            </div>
                        </div>-->

                        <!-- <div class="clearfix" style="height: 10px;clear: both;"></div> -->
                        <!-- <hr> -->
                        <?php //}          ?> 
                        <div class="clearfix" style="height: 10px;clear: both;"></div> 
                        <hr> 
                        <div class="form-group">
                            <div class="col-lg-offset-5">
                                <button class="btn btn-primary" id="submit_btn" name="submit_btn" type="submit">Save</button> 
                                <a class="btn btn-primary" href="<?php echo base_url('admin/registration') ?>" type="button">Back</a> 
                            </div>
                        </div>

                        <?php form_close(); ?>
                        <!--</div> /.box-body--> 
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<script>
    //////////  edit form validation ////////
    $(document).ready(function() {

        var userSellerPoint = JSON.parse('<?php echo json_encode($user_seller_point); ?>');
        var arr = new Array();
        if (userSellerPoint != '') {
            $.each(userSellerPoint, function(key, obj) {
                if (obj.seller_code != "") {
                    arr.push(obj.seller_code);
                }
            });
            $('#user_seller_point').val(arr);

        }
        $("#edit_registration_form").validate({
            rules: {
                firm_name: {
                    required: true,
                    lettersonly: true
                },
                first_name: {
                    required: true,
                    lettersonly: true
                },
                last_name: {
                    required: true,
                    lettersonly: true
                },
                dob: {
                    required: true,
                },
                address1: {
                    required: true,
                },
                agent_state: {
                    required: true,
                },
                agent_city: {
                    required: true,
                },
                pincode: {
                    required: true,
                    number: true,
                    minlength: 6,
                    maxlength: 6,
                },
                // Seller_point : {
                //     required :true;
                // },
//            depot_code : {
//                required :true;
//            },
                topuplmit: {
                    required: true
                },
                "user_seller_point[]": {
                    required: true,
                },
                "service_name[]": {
                    required: true
                },
            },
            messages:
                    {
                        firm_name: {
                            required: "Please enter firm/company name",
                            lettersonly: "Please enter valid firm/company name",
                        },
                        first_name: {
                            required: "Please enter first name",
                            lettersonly: "Please enter valid first name",
                        },
                        last_name: {
                            required: "Please enter last name",
                            lettersonly: "Please enter valid last name",
                        },
                        dob: {
                            required: "Please select the date of birth",
                        },
                        address1: {
                            required: "Please enter the  address line1",
                        },
                        agent_state: {
                            required: "Please select state name",
                        },
                        agent_city: {
                            required: "Please select city name",
                        },
                        pincode: {
                            required: "Please enter the pin code",
                            number: "Please enter valid pincode.",
                            minlength: "Please enter 6 digit pin code",
                            maxlength: "Please enter 6 digit pin code",
                        },
                        // Seller_point: {
                        //     required: "Please enter the  sellerpoints",
                        // },
//                    depot_code: {
//                        required: "Please select depots",
//                    },
                        /*  topuplmit: {
                         required: "Please enter topuplmit,
                         },*/
                        "user_seller_point[]": {
                            required: "Please select at least one seller point",
                        },
                        "service_name[]": {
                            required: "Please check at least one service",
                        }                   
                        
                    },
                  errorPlacement: function (error, element) {
                     error.insertAfter('.intent');                    
                }


        });

        $('#select_all').click(function() {
            $('#user_seller_point option').prop('selected', true);
        });

        jQuery.validator.addMethod("lettersonly", function(value, element)
        {
            return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
        }, "");
        /******** DATE OF BIRTH DATEPICKER ********/
        var dt = new Date();
        dt.setFullYear(new Date().getFullYear() - 18);
        var endDate = dt;
        $("#dob").datepicker({
            changeMonth: true,
            changeYear: true,
            showMonthAfterYear: true,
            yearRange: "-100:-18",
            dateFormat: "dd-mm-yy",
            maxDate: endDate
        });
        $('#dob').keydown(function(event) {
            event.preventDefault();
        });
        
        //added by sonali
        
        
        
        var url = BASE_URL+'admin/registration/check_smartcard_id';
        var smartcard_id;
        $.ajax({
            type:'GET',
            url: url,
            dataType: 'json',
            success: function(response){
                smartcard_id = response[0].id;
               
            }
        });


       $('input').on('ifChecked', function(event){
         if ($(this).val() === smartcard_id && smartcard_id != 'undefined') {
            
            $("#division_div").show();
            $("#depot_div").show();
        }
          });

       $('input').on('ifUnchecked', function(event){
            if ($(this).val() === smartcard_id && smartcard_id != 'undefined') {
                      $("#division_div").hide();
                      $("#depot_div").hide(); 
                }
          });
              
        $('#depot_cd').change(function(){
           
            var ajax_url = BASE_URL+'admin/registration/get_division';
            var depot_cd = $( "#depot_cd option:selected" ).val();
            $.ajax({
                type:'POST',
                url: ajax_url,
                data: {depot_cd: depot_cd},
                dataType: 'json',
                success: function(response){
                    $("#division_nm").val(response.DIVISION_NM);
                    $("#division_cd").val(response.DIVISION_CD);
                }
            });

        });
        
    });
</script>
<style>
    /****DATEPICKER YEAR MONTH DROPDOWNN *******/
    .ui-datepicker .ui-datepicker-title select {
        color: #000;
    }
</style>