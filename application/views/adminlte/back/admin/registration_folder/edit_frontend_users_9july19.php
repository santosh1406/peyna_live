<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?php if ($creat_role == COMPANY_ROLE_ID) { ?>
        <section class="content-header">
            <h2 style="margin-left:20px">Add Company</h2>
        </section>
    <?php } else if ($creat_role == MASTER_DISTRIBUTOR_ROLE_ID) { ?>
        <section class="content-header">
            <h2 style="margin-left:20px">Add Regional Distributor</h2>
        </section>
    <?php } else if ($creat_role == AREA_DISTRIBUTOR_ROLE_ID) { ?>
        <section class="content-header">
            <h2 style="margin-left:20px">Add Divisional Distributor</h2>
        </section>
    <?php } else if ($creat_role == DISTRIBUTOR) { ?>
        <section class="content-header">
            <h2 style="margin-left:20px">Add Executive</h2>
        </section>
    <?php } else if ($creat_role == RETAILER_ROLE_ID) { ?>
        <section class="content-header">
            <h2>Sales Agent Mapping</h2>
        </section>
    <?php } ?>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> </div>
                    <div class="box-body">
                        <?php echo validation_errors('<div class="error">', '</div>'); ?>
                        
                        <?php
                        $attributes = array("method" => "POST", "id" => "edit_registration_form", "name" => "edit_registration_form");
                        echo form_open(base_url() . 'admin/frontend_registration/update_details', $attributes);
                        ?>
                        
                        <input name="role" type="hidden" id="role" value="<?php echo $role_id ?>" class="form-control" autocomplete="off">
                        <input name="level" type="hidden" id="level" value="<?php echo $level ?>" class="form-control" autocomplete="off">
                        <input name="creat_role" id="creat_role" type="hidden" value="<?php echo $creat_role ?>" class="form-control" autocomplete="off">
                        <input name="cmp_drop_down_flag" type="hidden" id="cmp_drop_down_flag" value="<?php echo $cmp_drop_down_flag ?>" class="form-control" autocomplete="off">
                        <input name="md_drop_down_flag" type="hidden" id="md_drop_down_flag" value="<?php echo $md_drop_down_flag ?>" class="form-control" autocomplete="off">
                        <input name="ad_drop_down_flag" type="hidden" id="ad_drop_down_flag" value="<?php echo $ad_drop_down_flag ?>" class="form-control" autocomplete="off">
                        <input name="d_drop_down_flag"  type="hidden" id="d_drop_down_flag" value="<?php echo $d_drop_down_flag ?>" class="form-control" autocomplete="off">    
                        
                        <?php if ($creat_role != COMPANY_ROLE_ID && (!empty($cmp_drop_down_flag))) { ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="md">Select Company *:</label>
                                <div class="col-lg-6">
                                    <select class="form-control cmp " name="cmp" id="cmp">
                                        <option value="">Select Company</option>
                                        <?php
                                        if (isset($all_cmp) && count($all_cmp) > 0) {
                                            foreach ($all_cmp as $cmp) {
                                                ?>
                                                <option value="<?php if (!empty($cmp['id'])) echo $cmp['id'] ?>">
                                                    <?php if (!empty($cmp['display_name'])) echo $cmp['display_name'], ': ', rokad_decrypt($cmp['mobile_no'], $this->config->item('pos_encryption_key')); ?>
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>   
                                </div>
                            </div>                            
                        <?php } if ($creat_role != MASTER_DISTRIBUTOR_ROLE_ID && (!empty($md_drop_down_flag))) { ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="md">Select Regional Distributor *:</label>
                                <div class="col-lg-6">
                                    <select class="form-control md " name="md" id="md">
                                        <option value="">Select Regional Distributor</option>
                                        <?php
                                        //if (!empty($all_mds)) {
                                            //foreach ($all_mds as $md) {
                                                ?>
                                                <option value="<?php //if (!empty($md['id'])) echo $md['id'] ?>">
                                                    <?php //if (!empty($md['display_name'])) echo $md['display_name'], ': ', rokad_decrypt($md['mobile_no'], $this->config->item('pos_encryption_key')); ?>
                                                </option>
                                                <?php
                                            //}
                                        //}
                                        ?>
                                    </select>   
                                </div>
                            </div>
                        <?php } if ($creat_role != AREA_DISTRIBUTOR_ROLE_ID && (!empty($ad_drop_down_flag))) { ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="ad">Select Divisional Distributor *:</label>
                                <div class="col-lg-6">
                                    <select class="form-control  ad" name="ad" id="ad">
                                        <option value="">Select Divisional Distributor</option>
                                        <?php
                                        //if (isset($area_distributor) && count($area_distributor) > 0) {
                                            //foreach ($area_distributor as $ad) {
                                                ?>
                                                <option value="<?php //if (!empty($ad['id'])) echo $ad['id'] ?>">
                                                    <?php //if (!empty($ad['display_name'])) echo $ad['display_name'], ': ', rokad_decrypt($ad['mobile_no'], $this->config->item('pos_encryption_key')); ?>
                                                </option>
                                                <?php
                                            //}
                                        //}
                                        ?>
                                    </select>   
                                </div>
                            </div>
                        <?php } if ($creat_role == RETAILER_ROLE_ID && (!empty($d_drop_down_flag))) { ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="distributor">Select Executive *:</label>
                                <div class="col-lg-6">
                                    <select class="form-control distributor  show_ret" name="distributor" id="distributor">
                                        <option value="">Select Executive</option>
                                        <?php
                                        //if (isset($distributors) && count($distributors) > 0) {
                                            //foreach ($distributors as $distributor) {
                                                ?>
                                                <option value="<?php //if (!empty($distributor['id'])) echo $distributor['id'] ?>">
                                                    <?php //if (!empty($distributor['display_name'])) echo $distributor['display_name'], ': ', rokad_decrypt($distributor['mobile_no'], $this->config->item('pos_encryption_key')); ?>
                                                </option>
                                                <?php
                                            //}
                                        //}
                                        ?>
                                    </select>   
                                </div>
                            </div>
                        <?php } ?>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <?php if ($creat_role == RETAILER_ROLE_ID) { ?>
                            <div class="form-group intent"> 
                                <label class="col-lg-3 control-label" for="service_name">Select Services *:</label>
                                <div class="col-lg-2 overflowClass" >                                    
                                    <?php
                                    if (is_array($serviceDetails)) {
                                        $checked = "";
                                        foreach ($serviceDetails as $sd) {
                                          
                                            foreach ($agentServices as $as) {
                                                if ($sd->id == $as->service_id) {
                                                    
                                                    $checked = "checked";
                                                    break;
                                                } else {
                                                    $checked = "";
                                                }
                                            }
                                            ?>
                                            <input type ="checkbox" name ="service_name[]" id ="service_name" value="<?php echo $sd->id; ?>"  <?php if($sd->id == SMART_CARD_SERVICE) { $checked = "checked";}?> class="service_class" <?php echo $checked; ?> />
                                            <?php echo $sd->service_name; ?><br>
                                            <?php
                                        }
                                    }
                                    ?>                                    
                                </div> 
                            </div>
                        <?php } ?> <br>
                            <?php if ($creat_role == RETAILER_ROLE_ID) { ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group" id="depot_div" style="display:none"> 
                                <label class="col-lg-3 control-label" for="distributor">Select Depot *:</label>
                                <div class="col-lg-6">
                                <select class="form-control distributor  show_ret" name="depot_cd" id="depot_cd">
                                     <?php echo ($agentServices['0']->depot_code) ?>
                                        <option value="">Select Depot</option>
                                        
                                        <?php
                                        if (isset($depotsCode) && count($depotsCode) > 0) {
                                            foreach ($depotsCode as $dc) {
                                                ?>
                                               
                                                <option value="<?php 
                                                if (!empty($dc['DEPOT_CD']))
                                                echo  $dc['DEPOT_CD']; ?>" <?php if($dc['DEPOT_CD']==$agentServices['0']->depot_code) echo 'selected="selected"';?>> 
                                                    <?php if (!empty($dc['DEPOT_CD'])) 
                                                    echo $dc['DEPOT_NM']?>
                                                </option>
                                                <?php                                                
                                           }
                                        }
                                        ?>
                                    </select>    
                                </div>
                            </div>
                        <?php } ?>
                            <?php if ($creat_role == RETAILER_ROLE_ID) { ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group" id="division_div" style="display:none"> 
                                <label class="col-lg-3 control-label" for="distributor">Division*:</label>
                                <div class="col-lg-6">
                                    <input id="division_nm" name="division_nm" type="text" value="<?php echo $edit_data[0]['badge_no']; ?>" class="form-control" maxlength="10" autocomplete="off"></input> 
                                    <input type="hidden" id="division_cd" name="division_cd" value>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group div_badge" style="display:none">
                            <label class="col-lg-3 control-label" for="badge_no">Badge No* : </label>
                            <div class="col-lg-6">
                                <input id="badge_no" name="badge_no" maxlength="10" type="text" value="<?php echo $edit_data[0]['badge_no']; ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <hr>
                        
                        <div class="form-group">
                            <section class="content-header">
                                <h4 style="margin-left:10px">Login Information</h4>
                            </section>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <label class="col-lg-3 control-label" for="email">Email *: </label>
                            <div class="col-lg-6">
                                <input id="email" name="email" type="text" value="<?= rokad_decrypt($edit_data[0]['email'], $this->config->item("pos_encryption_key")); ?>" class="form-control" autocomplete="off" maxlength="100" readonly>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <label class="col-lg-3 control-label">Company GST No: </label>
                        <div class="col-lg-6">
                            <input id="company_gst_no" name="company_gst_no" type="text" minlength = "15" maxlength="15" value="<?= $edit_data[0]['company_gst_no']; ?>" class="form-control" autocomplete="off" readonly> 
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div> 
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="mobile_no">Mobile No *: </label>
                            <div class="col-lg-6">
                                <input name="mobile_no" type="text" minlength = "10" maxlength="10" id="mobile_no" value="<?= rokad_decrypt($edit_data[0]['mobile_no'], $this->config->item("pos_encryption_key")); ?>" class="form-control" autocomplete="off" readonly>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <hr>

                        <div class="form-group">
                            <section class="content-header">
                                <h4 style="margin-left:10px">Personal Information</h4>
                            </section>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <label class="col-lg-3 control-label" for="firm_name">Name of Firm/Company *:</label>
                            <div class="col-lg-6">
                                <input name="id" type="hidden" id="id" value="<?php echo rokad_encrypt($edit_data[0]['id'], $this->config->item("pos_encryption_key")); ?>" class="form-control" autocomplete="off">
                                <input name="firm_name" type="text" id="firm_name" maxlength="50" value="<?php echo $edit_data[0]['firm_name']; ?>" class="form-control name" autocomplete="off" readonly>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="first_name">First Name *:</label>
                            <div class="col-lg-6">
                                <input name="first_name" type="text" id="first_name" maxlength="20" value="<?php echo $edit_data[0]['first_name']; ?>" class="form-control name" autocomplete="off" readonly>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="last_name">Last Name *:</label>
                            <div class="col-lg-6">
                                <input name="last_name" type="text" id="last_name" maxlength="20" value="<?php echo $edit_data[0]['last_name']; ?>" class="form-control" autocomplete="off" readonly>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="gender"> Gender : </label>
                            <div class="col-lg-6">
                                <select class="form-control" id="gender"  name="gender" disabled>
                                    <option value="">Select</option>
                                    <option value="M" <?php echo (($edit_data[0]['gender'] == 'M') ? 'selected="selected" ' : '' ) ?>>Male</option>
                                    <option value="F" <?php echo (($edit_data[0]['gender'] == 'F') ? 'selected="selected" ' : '' ) ?>>Female</option>
                                    <option value="T" <?php echo (($edit_data[0]['gender'] == 'T') ? 'selected="selected" ' : '' ) ?>>Transgender</option>
                                </select>
                                <span id="prov_type_msg" class="err"></span>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="adharcard">Aadhaar Card *:</label>
                            <div class="col-lg-6">
                                <input type="text" name="adharcard" id="adharcard" value="<?= rokad_decrypt($edit_data[0]['adharcard'], $this->config->item("pos_encryption_key")); ?>" class="form-control" maxlength="12" autocomplete="off" readonly/>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="dob">PAN / TAN Card *:</label>
                            <div class="col-lg-6">
                                <input type="text" name="pancard" id="pancard" value="<?= rokad_decrypt($edit_data[0]['pancard'], $this->config->item("pos_encryption_key")); ?>" class="form-control" maxlength="10" autocomplete="off" style="text-transform:uppercase" readonly/>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="dob">DOB (DD-MM-YYYY) *:</label>
                            <div class="col-lg-6">
                                <input name="dob" type="text" id="dob" value="<?php
                        $dob = rokad_decrypt($edit_data[0]['dob'], $this->config->item("pos_encryption_key"));
                        echo date("d-m-Y", strtotime($dob));
                        ?>" class="form-control"    disabled>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <hr>
                        <div class="form-group">
                            <section class="content-header">
                                <h4 style="margin-left:10px">Contact Information</h4>
                            </section>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <label class="col-lg-3 control-label" for="address1">Address1 *:</label>
                            <div class="col-lg-6">
                                <input name="address1" type="text" id="address1" maxlength="200" value="<?php echo $edit_data[0]['address1']; ?>" class="form-control" autocomplete="off" readonly>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label" for="address2">Address2 :</label>
                            <div class="col-lg-6">
                                <input name="address2" type="text" id="address2" maxlength="200" value="<?php echo $edit_data[0]['address2']; ?>" class="form-control" autocomplete="off" readonly>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label" for="agent_state">State *:</label>
                            <div class="col-lg-6">
                                <select name="agent_state" id="agent_state" class="state form-control" value="<?php echo $edit_data[0]['state'] ?>" disabled>
                                    <option value="">Select State</option>
                                    <?php
                                    foreach ($states as $row => $value) {
                                        ?>
                                        <option value="<?php echo $value['intStateId']; ?>" <?php echo (($value['intStateId'] == $edit_data[0]['state']) ? 'selected="selected" ' : '' ) ?>> <?php echo $value['stState'] ?></option>
                                    <?php }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">                                    
                            <label class="col-lg-3 control-label" for="agent_city">City *:</label>
                            <div class="col-lg-6">
                                <select class="city form-control" name="agent_city" id="agent_city" disabled>
                                    <option value="">Select City</option>
                                    <?php
                                    if ($city) {
                                        foreach ($city as $key => $value) {
                                            ?>
                                            <option value="<?php echo $value['intCityId']; ?>" <?php echo (($value['intCityId'] == $edit_data[0]['city']) ? 'selected="selected" ' : '' ) ?>> <?php echo $value['stCity'] ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="dob">Pin Code *:</label>
                            <div class="col-lg-6">
                                <input type="text" name="pincode" id="pincode" value="<?php echo rokad_decrypt($edit_data[0]['pincode'], $this->config->item("pos_encryption_key")); ?>" class="form-control" maxlength="10" autocomplete="off" readonly/>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        

                         
                        <?php if ($creat_role == RETAILER_ROLE_ID) { ?>
                            <div id ="main" class="retailer_show" style="display:none">
                                <hr>
                                <section class="content-header">
                                    <h4 style="margin-left:10px">Other Information</h4>
                                </section>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <input name="bType" id="bType" type="hidden" value="0" class="form-control" autocomplete="off">

                                <input name="scheme" id="scheme" type="hidden" value="0" class="form-control" autocomplete="off">

                                <div class="clearfix retailer" style="height: 10px;clear: both;"></div>

                                <div class="form-group retailer">
                                    <label class="col-lg-3 control-label" for="sellerpoint">Seller Point: </label>
                                    <div class="col-lg-6">
                                        <select name="sellerpoint" id="sellerpoint" class="form-control select2 ">
                                            <option value="">Please select seller point</option>

                                        </select>
                                    </div>  
                                    <span id="sellerpointmsg" class="err"></span>
                                </div>
                                <div class="clearfix retailer" style="height: 10px;clear: both;"></div>

                                <div class="form-group retailer">
                                    <label class="col-lg-3 control-label" for="sellerdesc">Sellerpoint(description): </label>
                                    <div class="col-lg-6">
                                        <input name="sellerdesc" id="sellerdesc" class="form-control " placeholder="Description" autocomplete="off" type="text">
                                    </div>
                                    <span id="sellerdescmsg" class="err"></span>
                                </div>

                                <input name="topUpLimit" id="topUpLimit" type="hidden" value="0" class="form-control" autocomplete="off">

                                <input name="hhmLimit" id="hhmLimit" type="hidden" value="0" class="form-control" autocomplete="off">

                                <div class="clearfix retailer" style="height: 10px;clear: both;"></div>
                                <div class="form-group retailer">
                                    <label class="col-lg-3 control-label" for="depot_code">Depot Name: </label>
                                    <div class="col-lg-6">
                                        <select name="depot_code" id="depot_code" class="form-control select2 ">
                                            <option value="">Please Select Depot Name</option>  
                                            <!-- <?php if (is_array($depots)) { ?>
                                                <?php foreach ($depots as $depot) { ?>
                                                                                                                                                                                                                    <option value="<?php echo $depot->DEPOT_CD; ?>"><?php echo $depot->DEPOT_NM; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?> -->
                                        </select>
                                    </div>  
                                    <span id="depot_codemsg" class="err"></span>
                                </div>
                                <div class="clearfix retailer" style="height: 10px;clear: both;"></div>
                                <div class="form-group retailer">
                                    <label class="col-lg-3 control-label" for="securityDep">Security Deposit: </label>
                                    <div class="col-lg-6">
                                        <input name="securityDep" id="securityDep" maxlength="11" class="form-control " placeholder="Security Deposit" autocomplete="off" type="text">
                                    </div>
                                </div>
                                <div class="clearfix retailer" style="height: 10px;clear: both;"></div>
                            </div>
                            <?php } ?>

                            <hr>
                            <section class="content-header">
                                <h4 style="margin-left:10px">Bank Account Detail</h4>
                            </section>
                            <div class="clearfix retailer" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="uname">Account No:* </label>
                                <div class="col-lg-6">
                                    <input type="text" name="accno" id="accno" maxlength="30" class="form-control" placeholder="Account Number" value="<?php echo $bank_data[0]['bank_acc_no'] ?>"   data-msg-required="Please Enter Account Number" autocomplete="off" readonly>
                                </div> 
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="uname">Bank Name:* </label>
                                <div class="col-lg-6">
                                    <input type="text" name="bname" id="bname" maxlength="50" class="form-control upper" placeholder="Bank Name" data-msg-required="Please Enter Bank Name" autocomplete="off" value="<?php echo $bank_data[0]['bank_name'] ?>" readonly>
                                </div>    
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="uname">Account Holder  Name:* </label>
                                <div class="col-lg-6">
                                    <input type="text" name="accname" id="accname" maxlength="50" class="form-control upper"  placeholder="Account Holder name" value="<?php echo $bank_data[0]['bank_acc_name'] ?>" readonly>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="uname">Branch Name:* </label>
                                <div class="col-lg-6">
                                    <input type="text" name="brname" type="text" id="brname" maxlength="30" class="form-control upper" placeholder="Branch Name" data-msg-required="Please Enter Branch Name" value="<?php echo $bank_data[0]['bank_branch'] ?>" autocomplete="off" readonly>
                                </div>  
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="uname">IFSC Code:* </label>
                                <div class="col-lg-6">
                                    <input type="text" name="ifsc" maxlength = 11 id="ifsc"  data-msg-required="Please Enter IFSC code" data-msg-lfsccode="Please enter valid IFSC Code" class="form-control upper"  placeholder="IFSC Code"  value="<?php echo $bank_data[0]['bank_ifsc_code'] ?>" style="text-transform:uppercase" readonly>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>



                        

                        <div class="clearfix" style="height: 10px;clear: both;"></div> 

                        <div class="form-group">
                            <div class="col-lg-offset-5">
                                <button class="btn btn-primary" id="submit_btn" name="submit_btn" type="submit">Save</button> 
                                <a class="btn btn-primary" href="<?php echo base_url('admin/frontend_registration') ?>" type="button">Back</a> 
                            </div>
                        </div>

                        <?php form_close(); ?>
                        </div><!-- /.box-body>-->
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
    var cmp_on_change_flag = '<?php echo $cmp_on_change_flag; ?>';
    var md_on_change_flag = '<?php echo $md_on_change_flag; ?>';
    var ad_on_change_flag = '<?php echo $ad_on_change_flag; ?>';
    var show_retailor_flag = '<?php echo $show_retailor_flag; ?> ';
    var creat_role = '<?php echo $creat_role; ?>';
</script>

<script>
    //////////  edit form validation ////////
    $(document).ready(function() {

        $('#company_gst_no').keyup(function() {
            $(this).val($(this).val().toUpperCase());
        });
     
        $("#edit_registration_form").validate({
             //ignore: "none:not(select)",
            rules: {
                badge_no : {
                    required: true,
                },
                firm_name: {
                    required: true,
                    letterswithspecialchar: true
                },
                first_name: {
                    required: true,
                    lettersonly: true
                },
                last_name: {
                    required: true,
                    lettersonly: true
                },
                email: {
                    required: true,
                    email: true,
                   //email_exists: true
                },
                company_gst_no: {
                    // required: true,
                    valid_gst_no: true
                },
                dob: {
                    required: true,
                },
                adharcard: {
                    required: true,
                    digits: true,
                    minlength: 12,
                    maxlength: 12,
                    valid_adharcard_no : true
                },
                pancard: {
                    required: true,
                    minlength: 10,
                    maxlength: 10,
                    valid_pancard_no : true
                },
                mobile_no: {
                    required: true,
                    minlength: 10,
                    maxlength: 10,
                    number: true,
                    valid_mobile_no: true,
                    //mobileno_exists: true,
                    mobileno_start_with:true
                },
                address1: {
                    required: true,
                },
                agent_state: {
                    required: true,
                },
                agent_city: {
                    required: true,
                },
                pincode: {
                    required: true,
                    number: true,
                    minlength: 6,
                    maxlength: 6,
                    pincode_start_with: true,
                },
                cmp:{
                    required: true,
                },
                md:{
                    required: true,
                },
                ad:{
                    required: true,
                },
                distributor:{
                    required: true,
                },
                bType:{
                    required:true,
                },
                //"user_seller_point[]":{
                    //required:true,
                //},
                scheme:{
                    required:true,
                },
                //sellerpoint:{
                    //required:true,
                //},
                //sellerdesc:{
                    //required:true,
                //},
                /*topUpLimit:{
                    required:true,
                },
                hhmLimit:{
                    required:true,
                },*/
                //depot_code:{
                    //required:true,
                //},
                securityDep:{
                    //required:true,
                    digits: true
                },
                accno:{
                    required:true,
                    digits: true,
                    valid_account_no: true,
                },
                accname:{
                    required:true,
                    lettersonly: true,
                },
                brname:{
                    required:true,
                    lettersonly: true,
                },
                ifsc:{
                    required:true,
                    lfsccode:true
                },
                bname:{
                    required:true,
                    lettersonly: true,
                },
                "service_name[]": {
                    required: true,
                }
            },
            errorPlacement: function (error, element) {

                if(element.attr("name") === "agent_state")
                {
                    error.appendTo(element.parent("div") );
                }
                else if(element.attr("name") === "agent_city")
                {
                    error.appendTo(element.parent("div") );
                }
                else if (element.attr("name") === "cmp") 
                {
                    error.appendTo(element.parent("div") );
                }
                else if (element.attr("name") === "md") 
                {
                    error.appendTo(element.parent("div") );
                }
                else if (element.attr("name") === "ad") 
                {
                    error.appendTo(element.parent("div") );
                }
                else if (element.attr("name") === "distributor") 
                {
                    error.appendTo(element.parent("div") );
                }
                else if (element.attr("name") === "bType") 
                {
                    error.appendTo(element.parent("div") );
                }
                /*else if (element.attr("name") === "user_seller_point[]") 
                {
                   error.insertAfter(element.closest(".intent"));
                }*/
                else if (element.attr("name") === "scheme") 
                {
                    error.appendTo(element.parent("div") );
                }
                /*else if (element.attr("name") === "sellerpoint") 
                {
                    error.appendTo(element.parent("div") );
                }
                else if (element.attr("name") === "depot_code") 
                {
                    error.appendTo(element.parent("div") );
                }*/
                else if (element.attr("name") === "service_name[]") {
                    error.insertAfter('.intent'); 
                }else {
                    error.insertAfter(element);
                }

            },
            messages:
                    {
                        badge_no: {
                            required: "Please Enter Badge No."
                        },
                        firm_name: {
                            required: "Please Enter Firm/Company Name",
                            letterswithspecialchar: "Please Enter Valid Firm/Company Name"
                        },
                        first_name: {
                            required: "Please Enter First Name",
                            lettersonly: "Please Enter Valid First Name"
                        },
                        last_name: {
                            required: "Please Enter Last Name",
                            lettersonly: "Please Enter Valid Last Name"
                        },
                        email: {
                            required: "Please Enter Email Address"
                            //email_exists:"Email Id Already Registered With Us"
                        },
                        company_gst_no: {
                            required: "Please Enter Company GST No",
                            digits:"Please Enter Digits only",
                            valid_gst_no : "Please Enter Valid Company GST No"
                        },
                        adharcard: {
                            required: "Please Enter Aadhaar Card Number",
                            digits:"Please Enter 12 Digit Aadhaar Card Number",
                            minlength: "Please Enter 12 Digit Aadhaar Card Number",
                            maxlength: "Please Enter 12 Digit Aadhaar Card Number",
                            valid_adharcard_no: "Please Enter Valid Aadhaar Card Number."
                        },                   
                        pancard: {
                            required: "Please Enter PAN/TAN Card Number",
                            minlength: "Please Enter 10 digit pan Card Number",
                            maxlength: "Please Enter 10 digit pan Card Number",
                            valid_pancard_no: "Please Enter Valid PAN/TAN Card Number."
                        },      
                        dob: {
                            required: "Please Select The DOB (DD-MM-YYYY)"
                        },
                        mobile_no: {
                            required: "Please Enter Mobile No",
                            number: "Please Enter Valid Mobile No",
                            minlength: "Please Enter 10 Digit Mobile No",
                            maxlength: "Please Enter 10 Digit Mobile No",
                            valid_mobile_no: "Please Enter Valid Mobile No",
                            //mobileno_exists: 'Mobile No Already Registered with us',
                            mobileno_start_with: 'Mobile No should start with 7 or 8 or 9'
                        },
                        address1: {
                            required: "Please Enter The  Address1"
                        },
                        agent_state: {
                            required: "Please Select State Name"
                        },
                        agent_city: {
                            required: "Please Select City Name"
                        },
                        pincode: {
                            required: "Please Enter The Pin Code",
                            number: "Please Enter The Valid Pin Code",
                            minlength: "Please Enter 6 Digit Pin Code",
                            maxlength: "Please Enter 6 Digit Pin Code",
                            pincode_start_with: "Pin Code Should Start Between 1 and 9"
                        },
                        cmp:{
                            required: "Please Select The Company"
                        },
                        md:{
                            required: "Please Select The Regional Distributor"
                        },
                        ad:{
                            required: "Please Select The Divisional Distributor"
                        },
                        distributor:{
                            required: "Please Select The Distributor"
                        },
                        "service_name[]":{
                            required: "Please check at least one service"
                        },
                        bType:{
                            required: "Please Select The Buisness type"
                        },
                        //"user_seller_point[]":{
                            //required: "Please select at least one seller point",
                        //},
                        scheme:{
                            required: "Please Select The Scheme Type"
                        },
                        //sellerpoint:{
                            //required: "Please Select The Sellerpoint",
                        //},
                        //sellerdesc:{
                            //required: "Please Enter The Sellerpoint Description",
                        //},
                        /*topUpLimit:{
                            required: "Please Enter The topUpLimit",
                        },
                        hhmLimit:{
                            required: "Please Enter The hhmLimit",
                        },*/
                        //depot_code:{
                            //required: "Please Select the Depot Code",
                        //},
                        securityDep:{
                            //required: "Please Enter The Security Deposit",
                            digits:"Please Enter Digits only"
                        },
                        accname:{
                            required: "Please Enter The Account Holder Name"
                        },
                        accno:{
                            required: "Please Enter The Account Number",
                            valid_account_no: "Please Enter Number Between 11 to 16 Digits"
                        },
                        brname:{
                            required: "Please Enter The Branch Name"
                        },
                        bname:{
                            required: "Please Enter Bank Name"
                        },
                        ifsc:{
                            lfsccode: "Please Enter Valid IFSC CODE "
                        }
                    }           
        });
    
        /// fetch the master distributor based on company ///////
        if(cmp_on_change_flag == true) {
            $(document).off('change', '.cmp').on('change', '.cmp', function (e) {
                /*if(show_retailor_flag == true) {
                    $('.retailer_show').show();
                } else { 
                    $('.retailer_show').hide();
                }*/
                $("#md").html("<option value=''>Please wait..</option>");
                var detail = {};
                var div = '';
                var str = "";
                var form = '';
                var ajax_url = 'admin/registration/get_md_data';

                detail['id'] = $("#cmp").find('option:selected').attr('value');

                if (detail['id'] != '') {
                    $("#md").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
                    get_data(ajax_url, form, div, detail, function (response) {
                        if (response.md.length != 0) {
                            $("#md").html("<option value=''>Select Regional Distributor</option>");
                            $.each(response.md, function (i, value) {
                                $("#md").append("<option value=" + value.id + ">" + value.display_name + ": " + value.mobile_no +"</option>");
                                $(".chosen_select").trigger("chosen:updated");
                            });
                            }
                        else {
                            $("#md").html("<option value=''>No Data Found</option>").trigger('chosen:updated');
                        }
                    }, '', false);
                }
                else {
                    $("#md").html("<option value=''>Select Divisional Distributor</option>").trigger('chosen:updated');
                }
            });
        }    
    
        /// fetch the area distributor based on md ///////
        if(md_on_change_flag == true) {
            $(document).off('change', '.md').on('change', '.md', function (e) {
                /*if(show_retailor_flag == true) {
                    $('.retailer_show').show();
                } else { 
                    $('.retailer_show').hide();
                }*/

                $("#ad").html("<option value=''>Please wait..</option>");
                var detail = {};
                var div = '';
                var str = "";
                var form = '';
                var ajax_url = 'admin/registration/get_ad_data';

                detail['id'] = $("#md").find('option:selected').attr('value');

                if (detail['id'] != '') {
                    $("#ad").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
                    get_data(ajax_url, form, div, detail, function (response) {
                        if (response.ad.length != 0) {
                            $("#ad").html("<option value=''>Select Divisional Distributor</option>");
                            $.each(response.ad, function (i, value) {
                                $("#ad").append("<option value=" + value.id + ">" + value.display_name + ": " + value.mobile_no +"</option>");
                                $(".chosen_select").trigger("chosen:updated");
                            });
                            }
                        else {
                            $("#ad").html("<option value=''>No Data Found</option>").trigger('chosen:updated');
                        }
                    }, '', false);
                }
                else {
                    $("#ad").html("<option value=''>Select Divisional Distributor</option>").trigger('chosen:updated');
                }
            });
        }
        /// fetch the distributor based on ad ///////
        if (ad_on_change_flag == true) {
            $(document).off('change', '.ad').on('change', '.ad', function (e) {
                /*if(show_retailor_flag == true) {
                    $('.retailer_show').show();
                } else { 
                    $('.retailer_show').hide();
                }*/

                $("#distributor").html("<option value=''>Please wait..</option>");
                var detail = {};
                var div = '';
                var str = "";
                var form = '';
                var ajax_url = 'admin/registration/get_distributor';

                detail['id'] = $("#ad").find('option:selected').attr('value');

                if (detail['id'] != '') {
                    $("#distributor").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
                    get_data(ajax_url, form, div, detail, function (response) {
                        if (response.distributor.length != 0) {
                            $("#distributor").html("<option value=''>Select Executive</option>");
                            $.each(response.distributor, function (i, value) {
                                $("#distributor").append("<option value=" + value.id + ">" + value.display_name +  ": " + value.mobile_no +"</option>");
                                $(".chosen_select").trigger("chosen:updated");
                            });
                        }
                        else {
                            $("#distributor").html("<option value=''>No Data Found</option>").trigger('chosen:updated');
                        }
                    }, '', false);
                }
                else {
                    $("#distributor").html("<option value=''>Select Executive</option>").trigger('chosen:updated');
                }
            });
        }

        //fetch depot name using seller point
        $(document).off('change', '#sellerpoint').on('change', '#sellerpoint', function (e) {

            $("#depot_code").html("<option value=''>Please wait..</option>");
            var detail = {};
            var div = '';
            var form = '';
            var ajax_url = 'admin/registration/get_depot_data';

            detail['seller_cd'] = $("#sellerpoint").find('option:selected').attr('value');

            if (detail['seller_cd'] != '') {
                $("#depot_code").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
                get_data(ajax_url, form, div, detail, function (response) {
                    if (response.depots.length != 0) {
                        $("#depot_code").html("<option value=''>Please Select Depot Name</option>");
                        $.each(response.depots, function (i, value) {
                            $("#depot_code").append("<option value=" + value.DEPOT_CD + ">" + value.DEPOT_NM + "</option>");
                            $(".chosen_select").trigger("chosen:updated");
                        });
                    } else {
                        $("#depot_code").html("<option value=''>No Data Found</option>").trigger('chosen:updated');
                    }
                }, '', false);
            }
            else {
                $("#depot_code").html("<option value=''>Please Select Depot Name</option>").trigger('chosen:updated');
            }
        });


        //fetch seller point using executive
        $(document).off('change', '#distributor').on('change', '#distributor', function (e) {

            $("#sellerpoint").html("<option value=''>Please wait..</option>");
            var detail = {};
            var div = '';
            var form = '';
            var ajax_url = 'admin/registration/get_seller_point_data';

            detail['distributor'] = $("#distributor").find('option:selected').attr('value');

            if (detail['distributor'] != '') {
                $("#sellerpoint").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
                get_data(ajax_url, form, div, detail, function (response) {
                    if (response.sellerpoint.length != 0) {
                        $("#sellerpoint").html("<option value=''>Please Select Seller Point</option>");
                        $.each(response.sellerpoint, function (i, value) {
                            $("#sellerpoint").append("<option value=" + value.BUS_STOP_CD + ">" + value.BUS_STOP_NM  +" ("+ value.BUS_STOP_CD + ") </option>");
                            $(".chosen_select").trigger("chosen:updated");
                        });
                    } else {
                        $("#sellerpoint").html("<option value=''>No Data Found</option>").trigger('chosen:updated');
                    }
                }, '', false);
            }
            else {
                $("#sellerpoint").html("<option value=''>Please Select Seller Point</option>").trigger('chosen:updated');
            }
        });
   
        jQuery.validator.addMethod("lettersonly", function (value, element)
        {
            return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
        }, "");

        jQuery.validator.addMethod("letterswithdigit", function (value, element)
        {
            return this.optional(element) || /^[a-zA-Z0-9\s]+$/i.test(value);
        }, "");

        jQuery.validator.addMethod("lfsccode", function(value, element) {
            return this.optional(element) || /^[a-z]{4}\d{7}$/i.test(value);
        }, "");

        $.validator.addMethod("letterswithspecialchar", function (value, element)
        {
            return this.optional(element) || /^[a-zA-Z.,@#&_\-\s]+$/i.test(value);
        }, "");

        $.validator.addMethod("valid_mobile_no", function (value, element) {
            return (value == "0000000000" || value == "9999999999") ? false : true;
        }, 'Please enter valid mobile number');
        
        $.validator.addMethod("mobileno_start_with", function (value, element) {
                var regex = new RegExp("^[7-9]{1}[0-9]{9}$");
                if (value.length > 0)
                {
                    if (regex.test(value))
                    {
                        return true;
                    } else {
                        return false;
                    }
                } else
                {
                    return true;
                }
            }, 'Mobile No. should start with 7 or 8 or 9 ');

        $.validator.addMethod("pincode_start_with", function (value, element) {
                var regex = new RegExp("^[1-9]{1}[0-9]{5}$");
                if (value.length > 0)
                {
                    if (regex.test(value))
                    {
                        return true;
                    } else {
                        return false;
                    }
                } else
                {
                    return true;
                }
        }, 'PIN Code. should start with 1-9 ');
        
        /******** DATE OF BIRTH DATEPICKER ********/
        var dt = new Date();
        dt.setFullYear(new Date().getFullYear() - 18);
        var endDate = dt;
        $("#dob").datepicker({
            changeMonth: true,
            changeYear: true,
            showMonthAfterYear: true,
            yearRange: "-100:-18",
            dateFormat: "dd-mm-yy",
            maxDate: endDate
        });
        $('#dob').keydown(function (event) {
            event.preventDefault();    
        });
       
        /******** aadhar card Validation start here ********/
        $.validator.addMethod("valid_adharcard_no", function (value, element) {
                var regex = new RegExp("^[0-9]{12}$");
                if (value.length > 0)
                {
                    if (regex.test(value))
                    {
                        return true;
                    } else {
                        return false;
                    }
                } else
                {
                    return true;
                }
        }, 'Please enter valid aadharcard number');
         /******** aadhar card Validation end here ********/ 

        /******** account number Validation start here ********/
        $.validator.addMethod("valid_account_no", function (value, element) {
               var regex = new RegExp("^[0-9]{11,16}$");
               if (value.length > 0)
               {
                   if (regex.test(value))
                   {
                       return true;
                   } else {
                       return false;
                   }
               } else
               {
                   return true;
               }
           }, 'Please enter valid aadharcard number');
        /******** account number Validation end here ********/ 

        /******** Pancard Validation start here ********/
        $.validator.addMethod("valid_pancard_no", function (value, element) {
                var regex = new RegExp("^[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}$");
                if (value.length > 0)
                {
                    if (regex.test(value))
                    {
                        return true;
                    } else {
                        return false;
                    }
                } else
                {
                    return true;
                }
            }, 'Please enter valid pancard number');
         /******** Pancard Validation end here ********/
         
        /******** mobile validation start here ********/
        $.validator.addMethod("mobileno_exists", function (value, element) {
               var flag;

               $.ajax({
                   url: BASE_URL + 'admin/registration/is_mobile_no_registered',
                   data: {mobile_no: value,rokad_token : rokad_token},
                   async: false,
                   type: 'POST',
                   success: function (r) {
                       flag = (r === 'true') ? false : true;
                   }
               });

               return flag;
           }, 'Mobile no. already exsist with us');
       /******** mobile validation start here ********/
        /******** email validation start here ********/
        $.validator.addMethod("email_exists", function (value, element) {
               var flag;

               $.ajax({
                   url: BASE_URL + 'admin/registration/is_email_registered',
                   data: {email: value,rokad_token : rokad_token},
                   async: false,
                   type: 'POST',
                   success: function (r) {
                       flag = (r === 'true') ? false : true;
                   }
               });

               return flag;
           }, 'Email id already exsist with us');
        /******** email validation start here ********/
       
        /******** GST number Validation start here ********/
        $.validator.addMethod("valid_gst_no", function (value, element) {
                var regex = new RegExp("^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[0-9]{1}Z[0-9 A-Z]{1}$");
                if (value.length > 0)
                {
                    if (regex.test(value))
                    {
                        return true;
                    } else {
                        return false;
                    }
                } else
                {
                    return true;
                }
            }, 'Please enter valid GST number');
         /******** GST number Validation end here ********/
         
        /*************STATE-CITY DROPDOWN **********/
        $(document).off('change', '.state').on('change', '.state', function (e)
        {
            $("#agent_city").html("<option value=''>Please wait..</option>");

            var detail = {};
            var div = '';
            var str = "";
            var form = '';
            var ajax_url = 'report/report/get_statewise_city';

            detail['state_id'] = $("#agent_state").find('option:selected').attr('value');

            if (detail['state_id'] != '')
            {
                $("#agent_city").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
                get_data(ajax_url, form, div, detail, function (response)
                {
                    if (response.city.length != 0)
                    {
                        $("#agent_city").html("<option value=''>Select City</option>");
                        $.each(response.city, function (i, value)
                        {
                            $("#agent_city").append("<option value=" + value.intCityId + ">" + value.stCity + "</option>");
                            $(".chosen_select").trigger("chosen:updated");
                        });
                    }
                    else
                    {
                        $("#agent_city").html("<option value=''>Select City</option>").trigger('chosen:updated');
                    }
                }, '', false);
            }
            else
            {
                $("#agent_city").html("<option value=''>Select City</option>").trigger('chosen:updated');
            }
        });

        /**************code for select all option **********************/
        $('#select_all').click( function() {
         //$('input[name="user_seller_point[]"] option').prop('selected', true);
         $('#user_seller_point option').prop('selected', true);
            /*if($("#select_all").val()=="all"){      
                $('#user_seller_point option').prop('selected', true);
                $("#select_all").val("unall");
            } else if($("#select_all").val()=="unall") { alert("sunel");
                $('#user_seller_point option').prop('selected', false);
            }*/
        });

        
        //var sr = $('#service_name').val(this.checked);
        //alert(sr);
        //console.log(sr);
        
    
    });
    
    
        var service = [];

        $(document).on('ready', function(){
            var smartcard_service_id = '<?php echo SMART_CARD_SERVICE;?>';
            //get checked values - sonali
            $('input[name="service_name[]"]:checked').each(function() {   
                if (this.value === smartcard_service_id) {
                         $("#division_div").show();
                         $("#depot_div").show();
                     }
             });


            $(".service_class:checked").each(function (){
                service.push(this.value);              
             
            });
            if($.inArray('2', service) !== -1){
                $('.retailer_show').css('display', 'block');
                $('.div_badge').css('display', 'block');
            }else if($.inArray(smartcard_service_id, service)){
                $("#division_div").show();
                $("#depot_div").show();
            }else{
                $('.retailer_show').css('display', 'none');
                $('.div_badge').css('display', 'none');
            }

        var url = BASE_URL+'admin/Frontend_registration/check_smartcard_id';

        var smartcard_id;
        $.ajax({
            type:'GET',
            url: url,
            dataType: 'json',
            success: function(response){
                
                if(response!==''){
                    smartcard_id = response[0].id;
                }
                
               
            }
        });

       $('input').on('ifChecked', function(event){
           

        if ($(this).val() === smartcard_id && smartcard_id != 'undefined') {

            $("#division_div").show();
            $("#depot_div").show();

        }else if($.inArray(smartcard_service_id, service)){
                $("#division_div").show();
                $("#depot_div").show();
            }        
        else{
           $("#division_div").hide();
           $("#depot_div").hide(); 
        }
       
 
          });

       $('input').on('ifUnchecked', function(event){
            
           
           if($(this).val()===smartcard_service_id){
                $("#division_div").hide();
                $("#depot_div").hide();
            }
                
          });
              
        $('#depot_cd').change(function(){
           
            var ajax_url = BASE_URL+'admin/Frontend_registration/get_division';
            var depot_cd = $( "#depot_cd option:selected" ).val();
            $.ajax({
                type:'POST',
                url: ajax_url,
                data: {depot_cd: depot_cd},
                dataType: 'json',
                success: function(response){
                    console.log(response);
                    $("#division_nm").val(response.DIVISION_NM);
                    $("#division_cd").val(response.DIVISION_CD);
                }
            });

        });
    });
    
        jQuery(document).on('ifToggled', '.service_class', function(e) {
            var sr = $(this).val();
            //console.log(sr);
            if ($.inArray(sr, service) !== -1) {
                var index = service.indexOf(sr);
                if (index > -1) {
                    service.splice(index, 1);
                }
            } else {
                service.push(sr);
            }
            
            if ($.inArray('2', service) !== -1) {
                 //console.log('innn3');
                 $('.retailer_show').css('display', 'block');
                 $('.div_badge').css('display', 'block');
            } else {
                 $('.retailer_show').css('display', 'none');
                 $('.div_badge').css('display', 'none');
            }
        });
    
        
    
        
</script>
<style>
    /****DATEPICKER YEAR MONTH DROPDOWNN *******/
    .ui-datepicker .ui-datepicker-title select {
        color: #000;
    }
</style>