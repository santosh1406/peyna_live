<div class="content-wrapper">
    <section class="content-header">
        <h3>
            &nbsp;Download Bus Service Data
        </h3>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                       <?php
                            $attributes = array("method" => "POST", "id" => "bus_service_form", "class" => "bus_service_form");
                            echo  form_open(site_url().'/admin/download_csv/save_routes', $attributes);
                       ?>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="name">Seller Point:* </label>
                            <div class="col-lg-6">
                                <select name="seller_point" id="seller_point" class="form-control">
                                     <option value="">Select Seller Point</option> 
                                    <?php if(!empty($bus_service)) { 
                                            foreach ($bus_service as $key => $value) { ?>
                                                <option value="<?php echo $value['star_seller_code'];?>"><?php echo $value['star_seller_code']."-".$value['star_seller_name']; ?></option>
                                   <?php    }
                                        } 
                                    ?>
                                </select>                           
                               
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <center><img src="<?php echo base_url().BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image" style="display:none;"/></center>
                        
                        <div class="form-group" id="route_tab" class="route_tab" style="display: none;">
                            <label class="col-lg-3 control-label" for="op_addr_1">Route No:</label>
                            <div id="route_list" class="col-lg-12">
                            </div>
                            
                        </div>
                       
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        
                         <?php echo validation_errors('<div class="error">', '</div>'); ?>
                     
                        <div class="form-group">
                            <div class="col-lg-offset-6">
                            
                                <button class="btn btn-primary" id="save_group_data"  name="save_group_data" type="submit">Download Bus Service Zip</button> 
                               <!--  <button class="btn btn-primary back" id="back_data" type="button">Back</button>  -->
                            
                            </div>
                        </div>
                    </form>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<script>
$(document).ready(function() {

 $('#save_group_data').hide();
 

/* --------------Form Validation---------------------*/

    $("#bus_service_form").validate({
        rules: {
                seller_point: {
                    required: true
                }/*,
                'route_no[]': {
                    required: true,
                    minlength: 1
                }*/
            },
            messages: {
                'seller_point': "Please Select Seller Point" ,
                'route_no[]': "Please Check At Least One Route",
            } ,
        errorPlacement: function (error, element) {             
            /*if (element.attr("name") == "route_no[]") {                
        error.appendTo( element.closest(".form-group") );
            } else {*/
        error.appendTo( element.parent("div") );
          // }
        }               
    });
   
   /*--------Get Route number data after select seller point----------*/
       
    $(document).off('change', '#seller_point').on('change', '#seller_point', function(e) 
    {  $('.img-circle').show();  
        var elem = $(this);
        var detail = {};
        var form = '';
        var div = '';

        detail['bus_service_id'] = $('#seller_point').val();
        var ajax_url = "admin/Download_csv/get_route_list";
        $('#route_list').html('');
        $('#route_tab').css('display', 'none');
        get_data(ajax_url, form, div, detail, function(response) {
            if(response.type=="success") {
                $('.img-circle').hide();  
                $.each(response, function(index, value) {
                if(index=="msg" && value.length>0) {
                    $('#route_tab').css('display', 'inline');
                    var check ="<input type ='checkbox' id='checkAll' style='display:block'class='col-lg-1'><span class='col-lg-2'>All</span>";
                       for (var i = 0; i < value.length; i++) { 
                        check +="<input type ='checkbox' id='"+value[i].route_no+"' value='"+value[i].route_no+"' name ='route_no[]' style='display:block' class='col-lg-1' ><span class='col-lg-2'>"+value[i].route_no+"</span>";
                        }
                    }  $('#route_list').append(check);
                });
            } else {
                $('.img-circle').hide(); 
                  $('#route_tab').css('display', 'none'); 
                swal("No Data Found ", response.msg, "error");
            }
        });
    });

    /*------------------check all checkbox-----------------------*/
    $(document).on("change","#checkAll",function() {
        if($(this).prop("checked")==true) {
            $('input[name="route_no[]"]').prop("checked",true);
            $('#save_group_data').show();

        } else {
            $('input[name="route_no[]"]').prop("checked",false);
            $('#save_group_data').hide();

        }
    })

/*-----------At least one checkbox checked display download button----------*/
    $(document).on('change','input[name="route_no[]"]',function() {
        var chk = $("#bus_service_form input:checked").length
        if(chk > 0 ) {
            $('#save_group_data').show();
        } else {
            $('#save_group_data').hide();
        }
    })
  
});
</script>
