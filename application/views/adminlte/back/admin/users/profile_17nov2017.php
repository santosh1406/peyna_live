<style>
    /****DATEPICKER YEAR MONTH DROPDOWNN *******/
    .ui-datepicker .ui-datepicker-title select {
        color: #000;
    }
</style>
<?php $siteurl = base_url();?>  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        &nbsp;Profile
      </h1>
      
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
			<img class="profile-user-img img-responsive img-circle" src="<?php echo $profile_image;?>" alt="profile picture">
			
			 
              <h3 class="profile-username text-center"><?php echo $user_array->first_name.' '.$user_array->last_name;?></h3>

              <p class="text-muted text-center"><?php echo $user_role;?></p>
              <?php //echo"<pre>";print_r($user_array); ?>
              <ul class="list-group list-group-unbordered">
			    <?php if($user_array->level==5){ ?>
                <li class="list-group-item">
                  <b>Sales Executive</b> <a class="pull-right"><?php echo $RtCount->TotalVal;?></a>
                </li>
				<?php } else if($user_array->level==4){?>
                <li class="list-group-item">
                  <b>Agent</b> <a class="pull-right"><?php echo $DistCount->TotalVal;?></a>
                </li>
                <li class="list-group-item">
                  <b>Sales Executive</b> <a class="pull-right"><?php echo $RtCount->TotalVal;?></a>
                </li>
				<?php } else if($user_array->level==3){?>
				<li class="list-group-item">
                  <b>Area&nbsp;Distributor</b> <a class="pull-right"><?php echo $ADCount->TotalVal;?></a>
                </li>
                <li class="list-group-item">
                  <b>Agent</b> <a class="pull-right"><?php echo $DistCount->TotalVal;?></a>
                </li>
				<li class="list-group-item">
                  <b>Sales Executive</b> <a class="pull-right"><?php echo $RtCount->TotalVal;?></a>
                </li>
				<?php } else if($user_array->level==2){ ?>
				<li class="list-group-item">
                  <b>Master&nbsp;Distributor</b> <a class="pull-right"><?php echo $MDCount->TotalVal;?></a>
                </li>
				<li class="list-group-item">
                  <b>Area&nbsp;Distributor</b> <a class="pull-right"><?php echo $ADCount->TotalVal;?></a>
                </li>
                <li class="list-group-item">
                  <b>Agent</b> <a class="pull-right"><?php echo $DistCount->TotalVal;?></a>
                </li>
				<li class="list-group-item">
                  <b>Sales Executive</b> <a class="pull-right"><?php echo $RtCount->TotalVal;?></a>
                </li>
				<?php } ?>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#personal_info" data-toggle="tab">Personal&nbsp;Detail</a></li>
              <li><a href="#contact_info" data-toggle="tab">Contact&nbsp;Information</a></li>
              <li><a href="#document_info" data-toggle="tab">Document&nbsp;Information</a></li>
              <li><a href="#settings" data-toggle="tab">Settings</a></li>
            </ul>
            <div class="tab-content">
			  <!----Personal Detail Tab Start--->
              <div class="active tab-pane" id="personal_info">
				<?php
				$attributes = array("method" => "POST", "name" => "personal_information", "id" => "personal_information", "class" => "form-horizontal");
				echo form_open(base_url().'admin/users/update_personal_info/'.$user_array->id, $attributes);
				?>                
				    <?php $errors = validation_errors();
                    if ($errors) { ?>
						<div style="display: block;" class="alert alert-danger alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $errors; ?>
						</div>
					<?php } ?>
					
				  <div class="form-group">
                    <label for="inputName" class="col-sm-1 control-label">First&nbsp;Name</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control" name="first_name" id="first_name" value="<?php echo $user_array->first_name;?>">
                    </div>
					
					<label for="inputName" class="col-sm-2 control-label">Last&nbsp;Name</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control" name="last_name" id="last_name" value="<?php echo $user_array->last_name;?>">
                    </div>
                  </div>
				  <div class="form-group">
                    <label for="inputName" class="col-sm-1 control-label">Gender</label>

                    <div class="col-sm-4">
					  <select class="form-control" name="gender">
						<option value="M" <?php if ($user_array->gender == 'M') { echo 'selected'; }?>>Male</option>
						<option value="F" <?php if ($user_array->gender == 'F') { echo 'selected'; }?>>Female</option>
					  </select>
                    </div>
					
					<label for="inputName" class="col-sm-2 control-label">DOB</label>
                    <?php $Dob = rokad_decrypt($user_array->dob, $this->config->item('pos_encryption_key')); ?>
                    <div class="col-sm-4">
                      <input type="text" class="form-control" name="dob" id="dob" value="<?php echo date('d-m-Y',strtotime($Dob));?>" >
                    </div>
                  </div>
				  <div class="form-group">
                    <label for="inputName" class="col-sm-1 control-label">Email</label>

                    <div class="col-sm-4">
                      <input type="email" class="form-control" value="<?php echo rokad_decrypt($user_array->email, $this->config->item('pos_encryption_key'));?>" disabled >
                    </div>
					
					<label for="inputName" class="col-sm-2 control-label">Mobile</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control" value="<?php echo rokad_decrypt($user_array->mobile_no, $this->config->item('pos_encryption_key'));?>" disabled >
                    </div>
                  </div>
				  <div class="form-group">
                    <label for="inputName" class="col-sm-1 control-label">PAN/ TAN Card</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control" id="pancard" placeholder="Pancard" value="<?php echo strtoupper(rokad_decrypt($user_array->pancard, $this->config->item('pos_encryption_key'))); ?>" disabled >
                    </div>
					
					<label for="inputName" class="col-sm-2 control-label">Firm&nbsp;Name</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control" name="firm_name" id="firm_name" placeholder="Firm Name" value="<?php echo $user_array->firm_name;?>" >
                    </div>
                  </div>
				  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Submit</button>
                    </div>
                  </div>
                <?php echo form_close(); ?>
              </div>
              <!----Personal Detail Tab End--->
			  
              <!----Contact Detail Tab Start--->
              <div class="tab-pane" id="contact_info">                
				<?php
				$attributes = array("method" => "POST", "name" => "contact_information", "id" => "contact_information", "class" => "form-horizontal");
				echo form_open(base_url().'admin/users/update_contact_info/'.$user_array->id, $attributes);
				?>
					<?php $errors = validation_errors();
                    if ($errors) { ?>
						<div style="display: block;" class="alert alert-danger alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $errors; ?>
						</div>
					<?php } ?>
					
				   <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Address&nbsp;Line&nbsp;1&nbsp;*</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="address1" id="address1" value="<?php echo $user_array->address1;?>">
                    </div>
                   </div>
				   <div class="form-group">
					<label for="inputName" class="col-sm-2 control-label">Address&nbsp;Line&nbsp;2</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="address2" id="address2" value="<?php echo $user_array->address2;?>">
                    </div>
                   </div>
				   <div class="form-group">
				    <label for="inputName" class="col-sm-2 control-label">Country</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control" value="India" disabled >
                    </div>
                    </div>
					<div class="form-group">
					<label for="inputName" class="col-sm-2 control-label">State&nbsp;*</label>

                    <div class="col-sm-8">
					    <select class="state form-control chosen_select" name="state_id" id="state_id">
							<option value="">Please State</option>
							<?php foreach ($states as $row => $value) { ?>
								<option value="<?php echo  $value['intStateId']; ?>"<?php echo ($value['intStateId'] == $user_array->state ? 'selected=selected' : '') ?>><?php echo $value['stState']; ?></option>
							<?php } ?>
						</select>
					</div>	
				    </div>
				    <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">City&nbsp;*</label>

                    <div class="col-sm-8">
                        <select class="city form-control chosen_select" name="city" id="city">
							<option value="">Please Select</option> 
							<?php foreach ($city as $row => $value) { ?>
								<option value="<?php echo $value['intCityId']; ?>"<?php echo ($value['intCityId'] == $user_array->city ? 'selected=selected' : '') ?>><?php echo $value['stCity']; ?></option>
							<?php } ?>
						</select>
                    </div>
                    </div>
					<div class="form-group">
					
					<label for="inputName" class="col-sm-2 control-label">Pincode&nbsp;*</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="pincode" id="pincode" value="<?php echo rokad_decrypt($user_array->pincode, $this->config->item('pos_encryption_key'));?>" disabled >
                    </div>
                    </div>
				    <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Submit</button>
                    </div>
                  </div>
                <?php echo form_close(); ?>
              </div>
             <!----Contact Detail Tab End--->
             <!----Document Detail Tab Start--->
			 
			  <div class="tab-pane" id="document_info">
			    
				
				  <div class="box">
					<div class="box-header with-border">
					  <h3 class="box-title">KYC Document List</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
					  <table class="table table-bordered">
						<tr>
						  <th style="width: 10px">#</th>
						  <th>Document Type</th>
						  <th>Document Name</th>
						  <th>Document</th>
						</tr>
						<?php 
						$i=0;
						if(count($KycDoc) > 0){
						foreach($KycDoc as $kyc){ 
						$i++;
						if($kyc->category_name =='ID PROOF'){
						$Path = 'id_proof';	
						}else if($kyc->category_name =='ADDRESS PROOF'){
						$Path =	'add_proof';
						}else{
						$Path =	'photo';
						}
						?>
						<tr>
						  <td><?php echo $i;?></td>
						  <td><?php echo $kyc->category_name;?></td>
						  <td><?php echo $kyc->docs_name;?></td>
						  <td><a class="with-caption image-link" title="<?php echo $kyc->docs_name;?>" class="with-caption image-link" href="<?php echo $siteurl;?>uploads/agent_docs/<?php echo $kyc->agent_id;?>/<?php echo $Path;?>/large/<?php echo $kyc->thum_img_link;?>"><img width="50px" height="50px" src="<?php echo $siteurl;?>uploads/agent_docs/<?php echo $kyc->agent_id;?>/<?php echo $Path;?>/thumb/<?php echo $kyc->thum_img_link;?>" /></a></td>
						</tr>
						<?php } }?>
					  </table>
					</div>
					<!-- /.box-body -->
				  </div>
				  <!-- /.box -->
				 
				
              </div>
			 
             <!----Document Detail Tab End--->
             <!----Settings Tab Start--->

              <div class="tab-pane" id="settings">
				<div class="tabbable">
					<ul class="nav nav-tabs">						
						<li class="active"><a href="#change_password_box">Change Password</a></li>
						<li><a href="#change_tpin_box">Change T-PIN</a></li>
						<li><a href="#change_email_box">Change Email</a></li>
					</ul>
					<div class="tab-content">									
						<div class="tab-pane fade active in" id="change_password_box">							
							<div class="row">
								<div class="col-md-12">
								<!--<h4>Change Password</h4>--->					
								<div class="row">					
								<div class="col-md-12">
									<?php
									$attributes = array("method" => "POST", "name" => "change_password_inp", "id" => "change_password_inp");
									echo form_open('', $attributes);
									?>									
										<div class="form-group">
											<label class="col-lg-3 control-label" for="old_password">Old Password</label>
											<div class="col-lg-4">
												<input name="old_password" type="password" id="old_password" autocomplete="off" class="form-control name">
											</div>
										</div>                                                       
										<div class="clearfix" style="height: 10px;clear: both;"></div>										
										<div class="form-group">
											<label class="col-lg-3 control-label" for="new_password">New Password</label>
											<div class="col-lg-4">
												<input name="new_password" type="password" id="new_password" maxlength="15" autocomplete="off" class="form-control name">
											</div>
										</div>                                                       
										<div class="clearfix" style="height: 10px;clear: both;"></div>										
										<div class="form-group">
											<label class="col-lg-3 control-label" for="cnf_password">Confirm Password</label>
											<div class="col-lg-4">
												<input name="cnf_password" type="password" id="cnf_password" maxlength="15" autocomplete="off" class="form-control name">
											</div>
										</div>                                                       
										<div class="clearfix" style="height: 10px;clear: both;"></div>							
										<hr>
										<div class="clearfix" style="height: 10px;clear: both;"></div>														
										<div class="form-group">
											<div class="col-lg-offset-5">
												<button class="btn btn-primary" id="change_password_submit" name="change_password_submit" type="button" >Submit</button>											 
											</div>
										</div>
									<?php echo form_close(); ?>

									<?php
									$attributes = array("method" => "POST", "name" => "change_password", "id" => "change_password");
									echo form_open(base_url().'admin/users/change_password', $attributes);
									?>								
									<input type="hidden" name="old_password_hidden" id="old_password_hidden" >
									<input type="hidden" name="new_password_hidden" id="new_password_hidden" >
									<input type="hidden" name="cnf_password_hidden" id="cnf_password_hidden" >									
									<?php echo form_close(); ?>														
									<!--</div> /.box-body--> 
								</div><!-- /.box -->					
								</div>
								</div>										
							</div>							
						</div>
						
						<div class="tab-pane fade" id="change_tpin_box">							
							<div class="row">
								<div class="col-md-12">
								<!--<h4>Change T-PIN</h4>--> 					
								<div class="row">					
								<div class="col-md-12">
									<?php
									$attributes = array("method" => "POST", "name" => "change_tpin_inp", "id" => "change_tpin_inp");
									echo form_open('', $attributes);
									?>									
										<div class="form-group">
											<label class="col-lg-3 control-label" for="old_tpin">Old T-PIN</label>
											<div class="col-lg-4">
												<input name="old_tpin" type="password" maxlength="4" id="old_tpin" class="form-control name" autocomplete="off">
											</div>
										</div>                                                       
										<div class="clearfix" style="height: 10px;clear: both;"></div>
										
										<div class="form-group">
											<label class="col-lg-3 control-label" for="new_tpin">New T-PIN</label>
											<div class="col-lg-4">
												<input name="new_tpin" type="password" maxlength="4" id="new_tpin" class="form-control name" autocomplete="off">
											</div>
										</div>                                                       
										<div class="clearfix" style="height: 10px;clear: both;"></div>
										
										<div class="form-group">
											<label class="col-lg-3 control-label" for="cnf_tpin">Confirm T-PIN</label>
											<div class="col-lg-4">
												<input name="cnf_tpin" type="password" maxlength="4" id="cnf_tpin" class="form-control name" autocomplete="off">
											</div>
										</div>                                                       
										<div class="clearfix" style="height: 10px;clear: both;"></div>							
										<hr>
										<div class="clearfix" style="height: 10px;clear: both;"></div>														
										<div class="form-group">
											<div class="col-lg-offset-5">												
												<button class="btn btn-primary" id="change_tpin_submit" name="change_tpin_submit" type="button" >Submit</button>
											</div>
										</div>
									<?php echo form_close(); ?>
									<?php
									$attributes = array("method" => "POST", "name" => "change_tpin", "id" => "change_tpin");
									echo form_open(base_url().'admin/users/change_tpin', $attributes);
									?>								
									<input type="hidden" name="old_tpin_hidden" id="old_tpin_hidden" >
									<input type="hidden" name="new_tpin_hidden" id="new_tpin_hidden" >
									<input type="hidden" name="cnf_tpin_hidden" id="cnf_tpin_hidden" >									
									<?php echo form_close(); ?>

									<?php
									$attributes = array("method" => "POST", "name" => "tpin_otp_inp", "id" => "tpin_otp_inp", "style" =>"display:none");
									echo form_open('', $attributes);
									?>
										<div class="form-group">
											<label class="col-lg-3 control-label" for="new_otp">OTP</label>                                
											<div class="col-lg-2">
												<input name="new_otp" type="text" maxlength="6" id="new_otp" class="form-control name">									
											</div>
											<div class="col-lg-2">                                    
												<span><a class="btn btn-primary resend_otp">Resend OTP</a></span>
											</div>
										</div>                                                       
										<div class="clearfix" style="height: 10px;clear: both;"></div>
										<hr>
										<div class="clearfix" style="height: 10px;clear: both;"></div>							
										<div class="form-group">
											<div class="col-lg-offset-5">																		
												<button class="btn btn-primary submit_btn" id="tpin_otp_submit" name="tpin_otp_submit" type="button" >Submit</button>
												<button class="btn btn-primary back_btn" name="back_btn" type="button" >Back</button>	
											</div>
										</div>
									<?php echo form_close(); ?>
									<?php
									$attributes = array("method" => "POST", "name" => "tpin_otp", "id" => "tpin_otp");
									echo form_open(base_url().'admin/users/validate_tpin_otp', $attributes);
									?>								
									<input type="hidden" name="new_otp_hidden" id="new_otp_hidden" >									
									<?php echo form_close(); ?>
														
									<!--</div> /.box-body--> 
								</div><!-- /.box -->					
								</div>
								</div>										
							</div>							
						</div>						
						
						<div class="tab-pane fade" id="change_email_box">							
							<div class="row">
								<div class="col-md-12">
								<!--<h4>Change Password</h4>--->					
								<div class="row">					
								<div class="col-md-12">
									<?php
									$attributes = array("method" => "POST", "name" => "change_email_inp", "id" => "change_email_inp");
									echo form_open('', $attributes);
									?>									
										<div class="form-group">
											<label class="col-lg-3 control-label" for="old_password">Primary email</label>
											<div class="col-lg-4">
												<input name="inp_email" type="email" id="inp_email" class="form-control name">
											</div>
										</div>                                                       
										<div class="clearfix" style="height: 10px;clear: both;"></div>										
										<hr>
										<div class="clearfix" style="height: 10px;clear: both;"></div>														
										<div class="form-group">
											<div class="col-lg-offset-5">												
												<button class="btn btn-primary" id="change_email_submit" name="change_email_submit" type="button" >Submit</button>											 
											</div>
										</div>
									<?php echo form_close(); ?>
									<?php
									$attributes = array("method" => "POST", "name" => "change_email", "id" => "change_email");
									echo form_open(base_url().'admin/users/change_email', $attributes);
									?>								
									<input type="hidden" name="inp_email_hidden" id="inp_email_hidden" >									
									<?php echo form_close(); ?>
														
									<!--</div> /.box-body--> 
								</div><!-- /.box -->					
								</div>
								</div>										
							</div>							
						</div>

					</div>
				</div>
				
				</div>			  
			  
              <!----Settings Tab End--->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script>
var back = '<?php echo $back; ?>';
</script> 
  
<link href="<?php echo base_url() . COMMON_ASSETS ?>plugins/magnific/magnific-popup.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() . COMMON_ASSETS ?>plugins/magnific/jquery.magnific-popup.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>js/back/profile/settings.js" type="text/javascript"></script>	
  <script>
  $("ul.nav-tabs a").click(function (e) {
  e.preventDefault();
  $(this).tab('show');
});
  
  $(document).ready(function () {
	  
	$('.with-caption').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		closeBtnInside: false,
		mainClass: 'mfp-with-zoom mfp-img-mobile',
		image: {
			verticalFit: true,
			titleSrc: function(item) {
				return item.el.attr('title');
			}
		},
		zoom: {
			enabled: true
		}
	});

    $("#personal_information").validate({
        rules: {
            firm_name: {
                required: true,
                lettersonly: true
            },
            first_name: {
                required: true,
                lettersonly: true
            },
            last_name: {
                required: true,
                lettersonly: true
            },
            dob: {
                required: true,
            },
            gender: {
                required: true,
            }
        
        },
        messages:
                {
                    firm_name: {
                        required: "Please enter firm/company name",
                        lettersonly: "Please enter valid firm/company name",
                    },
                    first_name: {
                        required: "Please enter first name",
                        lettersonly: "Please enter valid first name",
                    },
                    last_name: {
                        required: "Please enter last name",
                        lettersonly: "Please enter valid last name",
                    },
                    gender: {
                        required: "Please select your gender"
                    },
                    dob: {
                        required: "Please select the date of birth",
                    },
               }
                

    });
	
	$("#contact_information").validate({
        rules: {
            address1: {
                required: true,
            },
            state_id: {
                required: true,
            },
            city: {
                required: true,
            },
            pincode: {
                required: true,
                number: true,
                minlength: 6,
                maxlength: 6,
            }
        
        },
        messages:
                {
                    address1: {
                        required: "Please enter the  address line1",
                    },
                    state_id: {
                        required: "Please select state name",
                    },
                    city: {
                        required: "Please select city name",
                    },
                    pincode: {
                        required: "Please enter the pin code",
                        number: "Please enter valid pincode.",
                        minlength: "Please enter 6 digit pin code",
                        maxlength: "Please enter 6 digit pin code",
                    }
               }
                

    });

    jQuery.validator.addMethod("lettersonly", function (value, element)
    {
        return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
    }, "");

    /******** DATE OF BIRTH DATEPICKER START********/
    var dt = new Date();
    dt.setFullYear(new Date().getFullYear() - 18);
    var endDate = dt;
    $("#dob").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy",
        maxDate: endDate
    });
	/******** DATE OF BIRTH DATEPICKER END********/
	
	/*************STATE-CITY DROPDOWN START**********/
	$(document).off('change', '.state').on('change', '.state', function (e)
    {
        $("#city").html("<option value=''>Please wait..</option>");

        var detail = {};
        var div = '';
        var str = "";
        var form = '';
        var ajax_url = 'front/agent_registration/get_statewise_city';

        detail['state_id'] = $("#state_id").find('option:selected').attr('value');

        if (detail['state_id'] != '')
        {
            $("#city").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
            get_data(ajax_url, form, div, detail, function (response)
            {
                if (response.city.length != 0)
                {
                    $("#city").html("<option value=''>Select City</option>");
                    $.each(response.city, function (i, value)
                    {
                        $("#city").append("<option value=" + value.intCityId + ">" + value.stCity + "</option>");
                        $(".chosen_select").trigger("chosen:updated");
                    });
                }
                else
                {
                    $("#city").html("<option value=''>Select City</option>").trigger('chosen:updated');
                }
            }, '', false);
        }
        else
        {
            $("#city").html("<option value=''>Select City</option>").trigger('chosen:updated');
        }
    });
	/*************STATE-CITY DROPDOWN END**********/
 });
 
 
function disable_button(button_id)
{
	var temp = button_id+'_loading';
	$("#"+temp).remove();	
	var pdiv = $("#"+button_id).closest("div");
	pdiv.append( "<button class='btn btn-primary pull-left' id='"+temp+"' type='button' >Processing</button>" );				
	$("#"+button_id).hide();
}

function enable_button(button_id)
{
	var temp = button_id+'_loading';
	$("#"+temp).remove();				
	$("#"+button_id).show();
}
  </script>