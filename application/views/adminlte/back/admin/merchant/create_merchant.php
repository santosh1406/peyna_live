<style type="text/css">
    .mandatory{ color:red; }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3>
            &nbsp;Create New Merchant
             <!--<small>advanced tables</small>-->
        </h3s>       
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                       <?php
                            $attributes = array("method" => "POST", "id" => "create_merchant_form", "class" => "create_merchant_form");

                            echo  form_open(site_url().'/admin/merchant/save', $attributes);
                       ?>
                        <!--<div class="form-group">

                            <label class="col-lg-3 control-label" for="type">Merchant Type<small class="mandatory">*</small>:</label>
                            <div class="col-lg-6">
                                <select name="merchant_type" class="form-control" id="merchant_type">
                                    <option value=''>Select Merchant Type</option>
                                    <option value="agent">Agent</option>
                                    <option value='partner'>Partner</option>
                                    <option value='corporate'>Corporate</option>
                                </select>
                            </div>
                        </div>-->

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                       
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="company_name">Name of Firm/Company<small class="mandatory">*</small>: </label>
                            <div class="col-lg-6">
                                <input type="text" name="company_name" class="form-control" id="company_name">
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                       
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="first_name">First Name<small class="mandatory">*</small>: </label>
                            <div class="col-lg-6">
                                <input type="text" name="first_name" class="form-control" id="first_name">
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="last_name">Last Name<small class="mandatory">*</small>: </label>
                            <div class="col-lg-6">
                                <input type="text" name="last_name" class="form-control" id="last_name">
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="address_line_1">Address<small>(Line 1)</small><small class="mandatory">*</small>: </label>
                            <div class="col-lg-6">
                                <input type="text" name="address_line_1" class="form-control" id="address_line_1">
                            </div>
                        </div>    


                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="last_name">Address<small>(Line 2)</small>: </label>
                            <div class="col-lg-6">
                                <input type="text" name="address_line_2" class="form-control" id="address_line_2">
                            </div>
                        </div>    

                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="last_name">State<small class="mandatory">*</small>: </label>
                            <div class="col-lg-6">
                                <select id="state_id" name="state_id" class="form-control state">
                                    <option disabled selected value> Select State</option>
                                    <?php foreach( $states as $state): ?>
                                        <option value="<?php echo $state['intStateId']; ?>" >
                                            <?php echo $state['stState']; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        

                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="last_name">City<small class="mandatory">*</small>: </label>
                            <div class="col-lg-6">
                                <select id="city_id" name="city_id" class="form-control">
                                    <option selected disabled value> Select City</option>
                                </select>
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="pincode">Pincode<small class="mandatory">*</small>: </label>
                            <div class="col-lg-6">
                                <input type="text" name="pincode" class="form-control" id="pincode" minlength = "6" maxlength="6">
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="pan_card">PAN / TAN Card<small class="mandatory">*</small>:</label>
                            <div class="col-lg-6">
                                <input type="text" name="pan_card" class="form-control" id="pan_card" minlength = "10" maxlength="10" style="text-transform:uppercase">
                            </div>
                        </div>


                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="email">E-mail<small class="mandatory">*</small>: </label>
                            <div class="col-lg-6">
                                <input type="email" name="email" class="form-control" id="email">
                            </div>
                        </div>


                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="mobile">Mobile Number<small class="mandatory">*</small>: </label>
                            <div class="col-lg-6">
                                <input type="text" name="mobile" class="form-control" id="mobile" minlength = "10" maxlength="10">
                            </div>
                        </div>


                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="alternate_contact">Contact No.<small>(Alt)</small>: </label>
                            <div class="col-lg-6">
                                <input type="text" name="alternate_contact" class="form-control" id="alternate_contact">
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">

                                <button class="btn btn-primary" id="save_group_data" type="submit">Save</button> 
                                <a class="btn btn-primary" href="<?php echo base_url('admin/merchant') ?>" type="button">Back</a>
                            </div>
                        </div>


                     </form>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript" src=""></script>
<script>

    $(document).ready(function() {

        $(document).off('click', '.back').on('click', '.back', function(e)
        {

            window.location.href = BASE_URL + 'admin/merchant';
        });

        $.validator.setDefaults({
            errorElement: "span",
            errorClass: "help-block",
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $.validator.addMethod("email", function (value, element) {
            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

            if (filter.test(value)) {
                return true;
            } else {
                return false;
            }
        },'Please enter valid email address');



        $.validator.addMethod("email_exists", function (value, element) {
            var flag;

            $.ajax({
                url: BASE_URL + 'admin/merchant/is_registered',
                data: {email: value},
                async: false,
                type: 'POST',
                success: function (r) {
                    flag = (r === 'true') ? false : true;
                }
            });

            return flag;
        }, 'Email id already exsist');

        $.validator.addMethod("valid_mobile_no", function (value, element) {
            return (value == "0000000000" || value == "9999999999") ? false : true;
        }, 'Please enter valid mobile number');

        jQuery.validator.addMethod("lettersonly", function (value, element){
            return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
        }, "");

        //pancard should not accept 0 value
        // $(document).off('keypress', '#agent_pancard').on('keypress', '#agent_pancard', function(value, element){ 
        $.validator.addMethod("valid_pancard_no", function (value, element) {
            var regex = new RegExp("^[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}$");
            if (value.length > 0)
            {
                if (regex.test(value))
                {
                    return true;
                } else {
                    return false;
                }
            } else
            {
                return true;
            }
        }, 'Please enter valid pancard number');
        //mobile no should start with 7-8-9
            $.validator.addMethod("mobileno_start_with", function (value, element) {
            var regex = new RegExp("^[7-9]{1}[0-9]{9}$");
            if (value.length > 0)
            {
                if (regex.test(value))
                {
                    return true;
                } else {
                    return false;
                }
            } else
            {
                return true;
            }
        }, 'Mobile No. should start with 7 or 8 or 9 ');
        //mobile no should start with 1-9
        $.validator.addMethod("pincode_start_with", function (value, element) {
            var regex = new RegExp("^[1-9]{1}[0-9]{5}$");
            if (value.length > 0)
            {
                if (regex.test(value))
                {
                    return true;
                } else {
                    return false;
                }
            } else
            {
                return true;
            }
        }, 'PIN Code. should start with 1-9 ');
        $('#create_merchant_form').validate({
            ignore: "",
            rules: {
                // merchant_type: {
                //     required: true
                // },
                company_name: {
                    required: true,
                },
                address_line_1: {
                    required: true,
                },
                state_id: {
                    required: true,
                },
                city_id: {
                    required: true,
                },
                pincode: {
                    required: true,
                    number: true,
                    minlength: 6,
                    maxlength: 6,
                    pincode_start_with:true
                },
                pan_card: {
                    required: true,
                    alphanumeric: true,
                    minlength: 10,
                    maxlength: 10,
                    valid_pancard_no: true
                },
                email: {
                    required: true,
                    email: true,
                    email_exists: true,
                },
                first_name: {
                    required: true,
                    lettersonly: true
                },
                last_name: {
                    required: true,
                    lettersonly: true
                },
                mobile: {
                    required: true,
                    number: true,
                    valid_mobile_no: true,
                    minlength: 10,
                    maxlength: 10,
                    mobileno_start_with: true
                },
                alternate_contact: {
                    number: true,
                    valid_mobile_no: true,
                    minlength: 10,
                    maxlength: 10
                },

            },
            messages:
                    {

                        // merchant_type: {
                        //     required: "Please select type",
                        // },
                        company_name: {
                            required: "Please enter name of firm/company",
                        },
                        pincode: {
                            required: "Please enter pincode",
                            number: "Please enter valid pincode",
                            minlength: "Please enter 6 digit pincode number",
                            maxlength: "Please enter 6 digit pincode number",
                            pincode_start_with: "Pincode should start between 1 and 9"
                        },
                        pan_card: {
                            required: "Please enter pancard number",
                            alphnumeric: "Please enter valid pancard number",
                            minlength: "Please enter 10 digit pancard number",
                            maxlength: "Please enter 10 digit pancard number",
                        },
                        address_line_1: {
                            required: "Please enter the  address line1",
                        },
                        state_id: {
                            required: "Please select state name",
                        },
                        city_id: {
                            required: "Please select city name",
                        },
                        email: {
                            email_exists: 'Email address already exsists',
                            required: "Please enter email address"
                        },
                        first_name: {
                            required: "Please enter first name",
                            lettersonly: "Please enter valid first name",
                        },
                        last_name: {
                            required: "Please enter last name",
                            lettersonly: "Please enter valid last name",
                        },
                        mobile: {
                            required: "Please enter mobile number",
                            number: "Please enter valid mobile number",
                            minlength: "Please enter 10 digit mobile number",
                            maxlength: "Please enter 10 digit mobile number",
                            valid_mobile_no: "Please enter valid mobile number",
                            mobileno_start_with: 'Mobile No should start with 7 or 8 or 9'
                        },
                        alternate_contact: {
                            number: "Please enter valid mobile number",
                            minlength: "Please enter 10 digit mobile number",
                            maxlength: "Please enter 10 digit mobile number",
                            valid_mobile_no: "Please enter valid mobile number"
                        },
                    },
        });


        $(document).off('change', '.state').on('change', '.state', function (e)
        {
            console.log('here');
            $("#city_id").html("<option value=''>Please wait..</option>");

            var detail = {};
            var div = '';
            var str = "";
            var form = '';
            var ajax_url = 'admin/merchant/get_statewise_city';

            detail['state_id'] = $("#state_id").find('option:selected').attr('value');

            if (detail['state_id'] != '')
            {
                $("#city_id").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
                get_data(ajax_url, form, div, detail, function (response)
                {
                    if (response.city.length != 0)
                    {
                        $("#city_id").html("<option value=''>Select City</option>");
                        $.each(response.city, function (i, value)
                        {
                            $("#city_id").append("<option value=" + value.intCityId + ">" + value.stCity + "</option>");
                            $(".chosen_select").trigger("chosen:updated");
                        });
                    } else
                    {
                        $("#city_id").html("<option value=''>Select City</option>").trigger('chosen:updated');
                    }
                }, '', false);
            } else
            {
                $("#city_id").html("<option value=''>Select City</option>").trigger('chosen:updated');
            }
        });
    });
</script>