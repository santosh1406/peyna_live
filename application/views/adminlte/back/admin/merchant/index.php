<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">List of Merchants</h3>
          
          <div align="right"><a href="<?php echo site_url() . '/admin/merchant/create' ?>" class="btn btn-primary" role="button" style="margin-top: 5px;color:#fff">Create New Merchant</a>   
          </div>
        </div><!-- /.box-header -->
        <div class="box-body"><div class="table-responsive">
            <table id="example2" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Company Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table></div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
<!-- /.content-wrapper -->

<script type="text/javascript">

$(document).ready(function() {

    var tconfig = {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": BASE_URL+"admin/merchant/list_of_groups",
            "type": "POST",
            "data": "json",
             data  : function (d) {
             d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;
            }
        },
        "columnDefs": [{
            "searchable": false,
            "orderable": false
        }],
        "iDisplayLength": 5,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        "paginate": true,
        "paging": true,
        "searching": true,
        "oLanguage": {
                "sProcessing": '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url().BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>' 
            },
        "aoColumnDefs": [
            {"bSearchable": false, "aTargets": [0,1,2,3]},
            {"targets":[1,2,3], "orderable": false}
        ],
        "order": [[0, "desc"]]
    };
    
    var oTable = $('#example2').dataTable(tconfig);
    
    $(document).off('click', '.delete_btn').on('click', '.delete_btn', function(e) {
            var flag = confirm('Please confirm to delete User Role?');
            if(flag){
                return true;
            }
            return false;
    });
});

setTimeout(function(){
    $('#msg_block').fadeIn('slow');
},1000);

</script>
