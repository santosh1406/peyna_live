<section class="content-header">
    <h3>Wallet Balance</h3>

</section>
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> 
                        <h3 class="box-title">CyberPlat</h3>
                    </div>
                    <div class="box-body">
                          <p><?php if($status == 'success') { echo Rs.'.' . $balance; } else { echo "Error In API"; } ?></p>  
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var rechargeType;
        $('#dth_operator').on('click', function() {
            rechargeType = $('input[name="recharge_type"]:checked', '#dth_recharge').val();               
        });
        
        $("#dth_operator").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: base_url + "admin/utilities/mobileOperators",
                    dataType: "json",
                    data: {
                        term: request.term,
                        type: rechargeType
                    },
                    success: function(data) {
                        response($.map(data, function(item) {
                            return {
                                label: item.label,
                                value: item.value,
                                name: item.name,
                                id: item.id,
                            }
                        }));
                    }
                });
            },
            autoFocus: true,
            minLength: 1,
            select: function(event, ui) {
                $("#dth_operator").val(ui.item.name);
                if (ui.item.name == "No result found") {
                    $("#prepaid_operator").val("");
                }
                return false;
            }
        });
    });

    $('#dth_recharge').bootstrapValidator({
        group: '.form-control',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            recharge_type: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b>Please select type.</font>'
                    }
                }
            },
            mobile_operator: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b>Please enter operator.</font>'
                    }
                }
            },
            mobile_number: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b> Please enter Customer number.</font>'
                    },
                    regexp: {
                        regexp: /^[789]\d{9}$/,
                        message: '<b><font style="color:red; size="1">X</b>Enter valid Customer number</font>'
                    }
                }
            },
            recharge_amount: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b> Please enter recharge amount.</font>'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '<b><font style="color:red; size="1">X</b>Amount can only consist of number</font>'
                    }
                }
            }
        }
    });
</script>