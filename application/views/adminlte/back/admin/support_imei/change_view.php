<style type="text/css">
    .user_dets{
        font-weight: bold;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3>
            &nbsp;Reset Password
             <!--<small>advanced tables</small>-->
        </h3>
       
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                       <form class="pass_reset" id="pass_reset">
                           <div class="form-group">
                                <label class="col-lg-2 control-label" for="op_addr_1">Mobile No. :</label>
                                <div class="col-lg-4">
                                    <input name="mobile_no" type="text" id="mobile_no" placeholder="Enter your mobile number" class="form-control" minlength="10" maxlength="10">
                                    <p id="no_data" style="color: red; display: none">No Data found.</p>
                                </div>

                            </div>

                            <div class="clearfix"></div>
                            <div class="form-group" style="margin-top: 10px">
                                <label class="col-lg-2 control-label">Reason :</label>
                                <div class="col-lg-4">
                                    <textarea id="reason" name="reason" placeholder="Enter reason"   class="form-control" style="margin-bottom: 0px"></textarea>
                                </div>
                            </div>

                           <div class="clearfix"></div>              
                            <div class="form-group">
                                <div class="col-lg-offset-2" style="margin-top: 10px; margin-left: 17.99%">
                                    <button class="btn btn-primary disabled" id="get_data" type="submit">Get Details</button>
                                    <button class="btn btn-primary disabled" style="display: none" id="loading" type="submit">Loading...</button>
                                </div>
                            </div>



                         </form>

                         
                        
                    </div><!-- /.box-body -->

                    

                </div><!-- /.box -->

                <div class="user_data">
                </div>

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<script>

    $(document).ready(function() {

    $(document).off('click', '.back').on('click', '.back', function(e)
    {

        window.location.href = BASE_URL + '/home';
    });


        $('#pass_reset').validate({ 
             rules: {
                  mobile_no: {
                    required: true,
                    number:true,
                    minlength:10,
                    maxlength:10,
                    mobileno_start_with: true,
                    valid_mobile_no: true
                 },
                 reason: {
                    required: true,
                    minlength: 1
                 }
            },
            messages: {
                mobile_no:  {
                    number:"Mobile no should be number only.",
                    minlength: "Please enter 10 digit Mobile No",
                    maxlength: "Please enter 10 digit Mobile No", 
                    valid_mobile_no: "Please enter valid mobile number",
                    mobileno_start_with: 'Mobile No should start with 7 or 8 or 9'
                }
            },
            submitHandler: function () { 
                
            }
        }); 
    
    $('#pass_reset input').on('keyup blur click', function () { // fires on every keyup & blur
        $('#no_data').css('display', 'none');
        if ($('#pass_reset').validate().checkForm()) {                   // checks form for validity
            // $('#get_data').removeClass('button_disabled').prop('disabled', false); // enables button
            $('#get_data').removeClass('disabled').prop('disabled', false);
        } else {
            // $('#get_data').addClass('button_disabled').prop('disabled', true);   // disables button
            $('#get_data').addClass('disabled').prop('disabled', true);
        }
    });

    $('#pass_reset textarea').on('keyup blur click', function () { // fires on every keyup & blur
        $('#no_data').css('display', 'none');
        if ($('#pass_reset').validate().checkForm()) {                   // checks form for validity
            // $('#get_data').removeClass('button_disabled').prop('disabled', false); // enables button
            $('#get_data').removeClass('disabled').prop('disabled', false);
        } else {
            // $('#get_data').addClass('button_disabled').prop('disabled', true);   // disables button
            $('#get_data').addClass('disabled').prop('disabled', true);
        }
    });

        
    $.validator.addMethod("valid_mobile_no", function (value, element) {
        return (value == "0000000000" || value == "9999999999") ? false : true;
    }, 'Please enter valid mobile number');
    
    $.validator.addMethod("mobileno_start_with", function (value, element) {
        var regex = new RegExp("^[7-9]{1}[0-9]{9}$");
        if (value.length > 0)
        {
            if (regex.test(value))
            {
                return true;
            } else {
                return false;
            }
        } else
        {
            return true;
        }
    }, 'Mobile No. should start with 7 or 8 or 9 ');



    /* Get Details of mobile number */
    $('#get_data').click(function(e){
        e.preventDefault();
        $('#get_data').css('display', 'none');
        $('#loading').css('display', 'block');
        var mobile_number = $('#mobile_no').val();
        var url = "<?php echo base_url() . 'admin/password_reset/get_user_details' ?>";
        $.ajax({
            type: "POST",
            url: url,
            data: {'mobile': mobile_number},
            success: function (data) {
                data = JSON.parse(data);

                if(data == ''){
                    $('#no_data').css('display', 'block');
                    $html = '';
                }else{
                    $html = '';
                    $.each(data, function (key,val) {
                        $html += '<div class="container-fluid well span6">' +
                            '<div class="row-fluid">' +
                                '<div class="span8">' +
                                    '<h3 id="username">' + val.username +'</h3>' +
                                    '<h6>Display Name: <span class="user_dets" id="display_name">'+ val.display_name +'</span></h6>' +
                                    '<h6>Email: <span class="user_dets" id="email">'+ val.email +'</span></h6>' +
                                    '<h6>Depot Name: <span class="user_dets" id="depot">'+ val.depot_name + ' (' + val.depot_cd + ')' +'</span></h6>' +
                                    '<h6>Agent Code: <span class="user_dets" id="agent_code">'+ val.agent_code +'</span></h6>' +
                                    '</div>'+
                                    
                                    '<div class="span2">' +
                                    '<div class="btn-group">' +
                                    '<a class="btn btn-info change_password" data-user-id="' + val.id + '" href="#">Change Password</a> ' +
                                    '</div>' +

                                '</div>' +
                            '</div>' +
                        '</div>';

                    });
                }
                
                $('.user_data').html($html);
                $('#loading').css('display', 'none');
                $('#get_data').css('display', 'block');
                return false;
            }
        });

    });



    $(document).on('click', '.change_password', function(e){
        e.preventDefault();
        var user_id = $(this).attr('data-user-id');
        var reason = $('#reason').val();

        var url = "<?php echo base_url() . 'admin/password_reset/change_password' ?>";

        swal({
          title: 'Are you sure?',
          text: "You want to change the password!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes'
        }, function(){
                 $.ajax({
                    type: "POST",
                    url: url,
                    data: {'user_id': user_id, 'reason' : reason},
                    success: function (data) {
                        data = JSON.parse(data);
                        if(data.type == 'success'){
                            var msg = 'Password Updated Successfully';
                            type = 'success';
                        }
                        else{
                            var msg = 'Something went wrong!';
                            type = 'warning';
                        }
                        swal(type,msg,type);
                        
                    }
                });
            }
        );

        
    });

    });
</script>
