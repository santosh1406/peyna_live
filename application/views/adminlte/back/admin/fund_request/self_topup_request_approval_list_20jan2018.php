<script type="text/javascript">
var role_name = '0';
<?php if (in_array(strtolower($this->session->userdata('role_name')), array('admin', 'super admin'))) { ?>
        role_name = '1';
<?php } ?>
</script>
<style>
.fields{display:none;}
.reject{display:none;}
.row{margin-bottom:5px;} 
</style>
<section class="content-header">
    <h1>Self Topup List</h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="commission_wrapper">
        <div class="box">
            
            <div class="box-body">

                <div class="alert alert-success alert-dismissible no_margin mb10 hide" role="alert" id="alert_msg">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="body"></div>
                </div>
			    <?php
				$attributes = array("method" => "POST", "id" => "self_approval_list", "name" => "self_approval_list","class" => "self_approval_list");
				echo form_open('', $attributes);
                                ?><div class="table-responsive">
                <table id="self_topup_approval" name="self_topup_approval" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th  width="10%">ID</th>
							<th>Date Time</th>
							<th>Amount</th>
							<th>Trans Mode</th>
							<th>Remark</th>
							<th>Status </th>
							<th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>	</div>	    
                </form>		    
                <div class="clearfix"></div>
            </div><!-- /.box-body -->
            <div class="box-footer">

                <div class="clearfix"></div>
                    <!-- <code>.box-footer</code> -->
            </div><!-- /.box-footer-->
        </div>
    </div>
</section>
<script type="text/javascript">
    var frm = document.Filter_Frm;
    
    $(document).ready(function () {
       		
			
		DatatableRefresh();

       });
	   
	function DatatableRefresh() {
		
		$('#self_topup_approval').dataTable({
			"aoColumnDefs": [ {"bSortable": true, "aTargets": [0,6] } ], 
			//"scrollY": "600",
			"deferRender": true,
			"bProcessing": true,
			"bServerSide": true,
			"order": [[ 0, 'desc' ]],
			"lengthMenu": [
				[10, 50, 100, -1],
				[10, 50, 100, "All"] // change per page values here
			],
			"bScrollCollapse": true,
			"bAutoWidth": true,
			"sPaginationType": "full_numbers",
			"bDestroy": true,
			"sAjaxSource": "<?php echo base_url().'admin/fund_self_request/self_topup_approval_list'; ?>",
			"fnServerData": function (sSource, aoData, fnCallback) {
				    $.getJSON(sSource, aoData, function (json) {
					fnCallback(json);
				});
			},
			"fnRowCallback": function (nRow, aData, iDisplayIndex) {

				return nRow;
			},
			"fnFooterCallback": function (nRow, aData) {
			}
		});

	}
	

    function ResetFilter() {
		frm.reset();
		DatatableRefresh();
	}
	function setFilter() {
		DatatableRefresh();
	}	
</script>