<script type="text/javascript">
var role_name = '0';
<?php if (in_array(strtolower($this->session->userdata('role_name')), array('admin', 'super admin'))) { ?>
        role_name = '1';
<?php } ?>
</script>
<style>
.fields{display:none;}
</style>
<section class="content-header">
    <h1>Balance Request List</h1>
    <div align="right">
        <?php if ($kyc == 'Y' && $vip_role = '0') {
          
        } else { 
		if($this->session->userdata('role_id') != 1 ){
			if($this->session->userdata('role_id') > TRIMAX_ROLE_ID){
		?>
		<button class="btn btn-primary add_ccavenu_btn">Add Money</button>&nbsp;&nbsp;&nbsp;&nbsp;
		<a class="btn btn-primary add_btn" href="<?php echo base_url() ?>admin/fund_request/create_new">Create New Request</a>
        <?php }}} ?>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="commission_wrapper">
        <div class="box">
            
            <div class="box-body">

                <div class="alert alert-success alert-dismissible no_margin mb10 hide" role="alert" id="alert_msg">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="body"></div>
                </div>

                    <?php if ($this->session->flashdata('msg')) { ?>
                    <div class="alert alert alert-success alert-error" style="display:block" id="msg">
                    <?php echo $this->session->flashdata('msg') ?> 
                    </div>
                    <?php } ?>


                <table id="fund_request" name="fund_request" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th  width="10%">ID</th>
							<th>Request By</th>
							<th>Request To</th>
							<th>Date Time</th>
							<th>Amount</th>
							<th>Trans Mode</th>
							<th>Remark</th>
							<th>Status </th>
							<th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>		    
                <div class="clearfix"></div>
            </div><!-- /.box-body -->
            <div class="box-footer">

                <div class="clearfix"></div>
                    <!-- <code>.box-footer</code> -->
            </div><!-- /.box-footer-->
        </div>
    </div>
</section>
<?php $this->load->view(BACK_WALLET_ADD_MONEY); ?>
<script type="text/javascript">
    $(document).ready(function () {
       
		DatatableRefresh();

       });
	   
	function DatatableRefresh() {
		
		$('#fund_request').dataTable({
			"aoColumnDefs": [ {"bSortable": true, "aTargets": [0,8] } ],
			//"scrollY": "600",
			"deferRender": true,
			"bProcessing": true,
			"bServerSide": true,
			//"aaSorting": [[0, "desc"]],
			"order": [[ 0, 'desc' ]],
			"lengthMenu": [
				[10, 50, 100, -1],
				[10, 50, 100, "All"] // change per page values here
			],
			"bScrollCollapse": true,
			"bAutoWidth": true,
			"sPaginationType": "full_numbers",
			"bDestroy": true,
			"sAjaxSource": "<?php echo base_url().'admin/fund_request/DataTableFundRequest'; ?>",
			"fnServerData": function (sSource, aoData, fnCallback) {
				    $.getJSON(sSource, aoData, function (json) {
					fnCallback(json);
				});
			},
			"fnRowCallback": function (nRow, aData, iDisplayIndex) {

				return nRow;
			},
			"fnFooterCallback": function (nRow, aData) {
			}
		});

	}
	
	$(document).off('click', '.add_ccavenu_btn').on('click', '.add_ccavenu_btn', function () {
        $('#add_money_popup').modal('show');
    });
</script>