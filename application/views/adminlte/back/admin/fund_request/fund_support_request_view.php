<div class="modal fade popup" id="add_view_popup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Balance Support Request View</h4>
            </div>
            <?php
				$attributes = array("method" => "POST", "id" => "fund_support_request_form_view", "name" => "fund_support_request_form_view","class" => "form-horizontal");
				echo form_open('', $attributes);
            ?>
            <div class="modal-body">
			    <div class="form-group">
                    <label for="request_to" class="col-sm-2 control-label">Request&nbsp;To:</label>
					<div class="col-sm-4">
					<input type="text" placeholder="request_to" name="request_to" id="request_to" class="form-control"  />
					</div>
					
					<label for="requestby" class="col-sm-2 control-label">Request&nbsp;By:*</label>
                    <div class="col-sm-4">
                     <input type="text" placeholder="request_by" name="request_by" id="request_by" class="form-control"  />
                    </div>
				</div>
				
				<div class="form-group">
                    <label for="amount" class="col-sm-2 control-label">Amount:*</label>
					<div class="col-sm-3">
					 <input type="text" placeholder="Amount" name="amount" id="amount" class="form-control"  />
					</div>
					
					<label for="transanction_mode" class="col-sm-3 control-label">Transfer&nbsp;Mode:*</label>
                    <div class="col-sm-4">
                      <input type="text"  name="transanction_mode" id="transanction_mode" class="form-control"  />
                    </div>
				</div>
				
				<div class="form-group fields">
                    <label for="bank_name" class="col-sm-2 control-label">Bank&nbsp;Name:*</label>
					<div class="col-sm-3">
					<input type="text" placeholder="Bank Name" id="bank_name" name="bank_name"  class="form-control"/>
					</div>
					
					<label for="bank_account_no" class="col-sm-3 control-label">Bank&nbsp;Account&nbsp;No:*</label>
                    <div class="col-sm-4">
                     <input type="text"  id="bank_account_no" name="bank_account_no"  class="form-control"/>
                    </div>
				</div>
				
				<div class="form-group">
                    <label for="bank_account_no" class="col-sm-2 control-label">Remark</label>
                    <div class="col-sm-3">
                     <input type="text" placeholder="Remark" id="remark" name="remark"  class="form-control"/>
                    </div>
					
					<label for="transanction_ref" class="col-sm-3 control-label fields">Transanction&nbsp;Ref:*</label>
					<div class="col-sm-4 fields">
					<input type="text"  id="transanction_ref" name="transanction_ref" class="form-control"  />
					</div>
				</div>
			
			
			
                
            <div class="modal-footer clearfix">

                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>

            </div>
            </form>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->