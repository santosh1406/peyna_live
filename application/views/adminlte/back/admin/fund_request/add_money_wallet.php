<div class="modal fade popup" id="add_money_popup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog add_amt">
        <div class="modal-content">
		    
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-inr"></i>&nbsp;Add Amount To Wallet</h4>
			</div>
			<?php
				$attributes = array("method" => "POST", "id" => "waybill_amnt", "name" => "waybill_amnt");
				echo form_open(base_url().'admin/fund_request/add_wallet_money', $attributes);
            ?>
            
                <div class="modal-body">
					  <div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">Amount</span>
							<input name="agent_code" type="hidden" id="agent_code" class="form-control" />
							<!--<input name="amount" type="text"  id="amount" class="form-control" placeholder="Wabill Amount" min="1" maxlength="6" autocomplete="off"  required />&nbsp;-->
                                                        <input name="pg_amt" type="text"  id="pg_amt" class="form-control" placeholder="Waybill Amount" min="1" maxlength="6" autocomplete="off"  required />&nbsp;
                                                       
<span id="erramountmsg" class="error"></span>
						</div>
					 </div>
				</div>
				<div class="modal-footer clearfix">
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
					<button type="submit" id="pg_confirm" class="btn btn-primary pull-left" name="pg_confirm"> Add Amount To Wallet</button>
				</div>
            </form>
			
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    $(document).ready(function () {
		
		$("#amount").keypress(function (e) {
			 //if the letter is not digit then display error and don't type anything
			 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				//display error message
				$("#erramountmsg").html("Digits Only").show().fadeOut("slow");
					   return false;
			}
		});
		
       
		
	   
		$.validator.setDefaults({
          ignore: ":hidden:not(select)"
        });
    
		$("#waybill_amnt").validate({
			rules: {
				pg_amt: {
					required: true,
					digits: true,
					check_numbers:true
				}
			}	
		});
		
		$.validator.addMethod("check_numbers", function (value, element) {
		var flag=false;
		var TempAmount = parseInt(value);
		if(TempAmount <=0){
		flag = false;	
		}else{
		flag = true;	
		}
		return flag;
	}, 'Amount Should Be Greater Than 0.');
	});	
</script>	
