<style>
.fields{display:none;}
.reject{display:none;}
</style>
<section class="content-header">   
<h1 >Balance Request</h1>
<section class="content">
    <div class="row">
        <div class="col-md-12">           
		    <div class="box box-default">
			<?php if($user_data['kyc_verify_status'] == 'N'){?>
			<h3 style="text-align: center;"> Sorry, you can not proceed further as your KYC approval is pending. Kindly contact Customer Care for more details.</h3>
			<div class="modal-footer clearfix">
                <a href="<?php echo base_url();?>admin/fund_request/index" class="btn btn-primary pull-left"><i class="fa fa-fw fa-backward"></i> Back</a>
				
            </div>
			<?php } else {?>
			<?php
				$attributes = array("method" => "POST", "id" => "fund_request_form", "name" => "fund_request_form","class" => "fund_request_form");
				echo form_open(base_url().'admin/fund_request/save_fund_request', $attributes);
            ?>
                <div class="box-header"></div><!-- /.box-header -->
		    <?php if (isset($error) && count($error) >0) { ?>
             <div class="alert alert alert-danger alert-error" style="display:block"id="msg">
       
                <strong>Error!</strong> 
                <ul id="errormsg">
                <?php foreach($error as $key=>$values)
                    {
                        echo "<li>".$values."</li>";
                    }     
                ?>
                </ul>
			 </div>
            <?php } ?>
		   
            <div class="box-body">
			<div class="row">
			    <div class="col-md-12">
				<div class="form-group">
                    <label for="request_to">Request To:*</label>
					<?php if($CompanyOwner->id != $AboveLevel->id){?>
					<input type="radio" name="request_to" id="" class="form-control" value="<?php echo $CompanyOwner->id;?>" />&nbsp;&nbsp;<?php echo ucwords($CompanyOwner->first_name.' '.$CompanyOwner->last_name.' ('.$CompanyOwner->label.') ');?>
					<input type="radio" name="request_to" id="" class="form-control" value="<?php echo $AboveLevel->id;?>" />&nbsp;&nbsp;<?php echo ucwords($AboveLevel->first_name.' '.$AboveLevel->last_name.' ('.$AboveLevel->label.') ');?>
					<?php } else {?>
					<input type="radio" name="request_to" id="" class="form-control" value="<?php echo $CompanyOwner->id;?>" />&nbsp;&nbsp;<?php echo ucwords($CompanyOwner->first_name.' '.$CompanyOwner->last_name.' ('.$CompanyOwner->label.') ');?>
					<?php }?>
					
				</div>
				</div>
				
                <div class="col-md-12">
				<div class="form-group">
                    <label for="amount">Amount:*</label>
                    <input type="text" placeholder="Amount" name="amount" id="amount" class="form-control" min="1" maxlength="6" autocomplete="off" />&nbsp;<span id="erramountmsg" class="error"></span>
					
                </div>
				</div>
				
				<div class="col-md-12">
				<div class="form-group">
                    <label for="transanction_mode">Transfer Mode:*</label>
                    <select name="transanction_mode" id="transanction_mode" class="chosen_select form-control"  >
                        <option value = ''>Please Select</option>
						<?php foreach($Pay_Trans_Mode as $value){?>
                        <option value="<?php echo $value->id;?>"><?php echo  $value->mode;?></option>
						<?php } ?>
                    </select>
					
                </div>
				</div>
				<!-- <div class="col-md-12">
		  			<div class="form-group"> -->
                        <center><img src="<?php echo base_url().BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image" style="display:none;"/></center>
                   <!--  </div>
                </div>      -->          
                <div class="col-md-12">
				<div class="form-group fields">
                    <label for="transanction_ref">Transanction Reference:*</label>
                    <input type="text" placeholder="Transanction Reference" id="transanction_ref" name="transanction_ref" class="form-control" min="10" maxlength="24" autocomplete="off" />&nbsp;<span id="errtransrefmsg" class="error"></span>
					
                </div>
				</div>
				
                <div class="col-md-12">
				<div class="form-group fields">
                    <label for="bank_name">Bank Name:*</label>
                    <input type="text" placeholder="Bank Name" id="bank_name" name="bank_name" autocomplete="off"  class="form-control"/>
                    
                </div>
				</div>
				
				<div class="col-md-12">
				<div class="form-group fields">
                    <label for="bank_account_no">Bank Account No:*</label>
                    <input type="text" placeholder="Bank Account No" id="bank_account_no" name="bank_account_no"  class="form-control" maxlength="24" autocomplete="off" />&nbsp;<span id="errbankacntnomsg" class="error"></span>
                </div>
				</div>
				
				<div class="col-md-12">
				<div class="form-group">
                    <label for="remark">Remark:</label>
                    <input type="text" placeholder="Remark" id="remark" name="remark" autocomplete="off" class="form-control"/>
                    <input name="id" type="hidden" id="id" class="form-control"/>
                </div>
				</div>
			  		
			  	
            	</div>

            <div class="modal-footer clearfix">
                <a href="<?php echo base_url();?>admin/fund_request/index" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
				
                <button type="submit" id="submit_btn" class="btn btn-primary pull-left"><i class="fa fa-fw fa-save"></i> Submit </button>
            </div>
            </div>
            </form>
			<?php } ?>								
		  </div>
		</div>
	</div>
</section>


<script type="text/javascript">
    $(document).ready(function () {
		
		$("#amount").keypress(function (e) {
			 //if the letter is not digit then display error and don't type anything
			 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				//display error message
				$("#erramountmsg").html("Digits Only").show().fadeOut("slow");
					   return false;
			}
		});
		
        $("#transanction_ref").keypress(function (e) {
			 //if the letter is not digit then display error and don't type anything
			 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				//display error message
				$("#errtransrefmsg").html("Digits Only").show().fadeOut("slow");
					   return false;
			}
		});
		
		$("#bank_account_no").keypress(function (e) {
			 //if the letter is not digit then display error and don't type anything
			 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				//display error message
				$("#errbankacntnomsg").html("Digits Only").show().fadeOut("slow");
					   return false;
			}
		});
		
	    $('#transanction_mode').change(function(){
			var TransMode = $('#transanction_mode').val();
			if(TransMode ==1){
				$('.fields').hide();
			}else{
				$('.fields').show();
			}
		});
	    
		jQuery.validator.addMethod("lettersonly", function(value, element) {
		  return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
		}); 
		
		$.validator.setDefaults({ ignore: ":hidden:not(select)" });
    
		$("#fund_request_form").validate({
			rules: {
				request_to: {
					required: true     
				},
				amount: {
					required: true,
					digits: true,
					check_numbers:true
				},
				transanction_mode: {
					required: true
				},
				transanction_ref: {
					required: true,
					digits: true
				},
				bank_name: {
					required: true,
					lettersonly: true
				},
				bank_account_no: {
					required: true,
					digits: true
				}
			},
			errorPlacement: function (error, element) {				
				if (element.attr("name") == "request_to") {				
				error.appendTo( element.closest(".form-group") );
				} else {
				error.appendTo( element.parent("div") );
				}
			},
			submitHandler: function(form) {					
				disable_button('submit_btn');	// button id as parameter
                $('.img-circle').show();			
				$.ajax({
					url: form.action,
					type: form.method,
					data: $(form).serialize(),
					async:false,
					dataType:'json',
					success: function(response) {
						
						if (response.flag == '@#success#@')
						{                   
							if(response.msg_type == "success")
							{
								$('.img-circle').hide(); 
								showLoader(response.msg_type,response.msg);
								window.location = "<?php echo base_url() ?>admin/fund_request/index";
							}
						}
						else
						{
							if(response.msg_type == "error")
							{
								//showLoader(response.msg_type,response.msg);
								$('.img-circle').hide(); 
								swal("Oops...", response.msg, "error");
								enable_button('submit_btn');
							}
							else
							{
								$('.img-circle').hide(); 
								swal("Oops...", response.msg, "error");
								enable_button('submit_btn');
							}     
						}
						
							
						//enable_button('submit_btn');
					}            
				});

			}			
		});
		
		$.validator.addMethod("check_numbers", function (value, element) {
		var flag=false;
		var TempAmount = parseInt(value);
		if(TempAmount <=0){
		flag = false;	
		}else{
		flag = true;	
		}
		return flag;
	}, 'Amount Should Be Greater Than 0.');
	});	
	
	
	function disable_button(button_id)
	{
		var temp = button_id+'_loading';
		$("#"+temp).remove();
		var pdiv = $("#"+button_id).closest("div");
		pdiv.append( "<button class='btn disabled btn-primary pull-left' id='"+temp+"' type='button' >Processing</button>" );				
		$("#"+button_id).hide();
	}
	
	function enable_button(button_id)
	{
		var temp = button_id+'_loading';
		$("#"+temp).attr("disabled", false)
		$("#"+temp).remove();				
		$("#"+button_id).show();
	}
</script>