<style>
.fields{display:none;}
.reject{display:none;}
</style>
<section class="content-header">   
<h1 >Balance Request Approval</h1>
<section class="content">
    <div class="row">
        <div class="col-md-12">
          <div class="box box-default">
		    <?php if($user_data['kyc_verify_status'] == 'N'){?>
			<h3 style="text-align: center;">  Sorry, you can not proceed further as your KYC approval is pending. Kindly contact Customer Care for more details.</h3>
			<div class="modal-footer clearfix">
                <a href="<?php echo base_url();?>admin/fund_request/fund_request_approval" class="btn btn-primary pull-left"><i class="fa fa-fw fa-backward"></i> Back</a>
				
            </div>
			<?php } else {?>
			<?php
				$attributes = array("method" => "POST", "id" => "fund_request_approval_form", "name" => "fund_request_approval_form","class" => "fund_request_approval_form");
				echo form_open(base_url().'admin/fund_request/save_fund_request_approval', $attributes);
            ?>
			<div class="box-header"></div><!-- /.box-header -->
		    <?php if (isset($error) && count($error) >0) { ?>
             <div class="alert alert alert-danger alert-error" style="display:block"id="msg">
       
                <strong>Error!</strong> 
                <ul id="errormsg">
                <?php foreach($error as $key=>$values)
                    {
                        echo "<li>".$values."</li>";
                    }     
                ?>
                </ul>
			 </div>
            <?php } ?>
		   
            <div class="box-body">
			<div class="row">
			  <div class="col-md-6">
                <div class="form-group">
                    <label for="request_to">Request By:</label>
                    <input type="text" value="<?php echo $RequestBy;?>" name="request_by" id="request_by" class="form-control" disabled />
                    <input type="hidden" value="<?php echo $FundRequest->request_touserid;?>" name="request_to" id="request_to" />
                    <input type="hidden" value="<?php echo $FundRequest->user_id;?>" name="requested_by" id="requested_by" />
                </div>
                <div class="form-group">
                    <label for="transanction_mode">Transanction Mode:</label>
                    <input type="text" value="<?php echo $FundRequest->topup_by;?>" name="transanction_mode" id="transanction_mode" class="form-control" disabled />
                </div>
               </div>
			   <div class="col-md-6">
			    <div class="form-group">
                    <label for="amount">Amount:</label>
					<?php
					$Amnt = $FundRequest->amount;
					$Amt =strpos($Amnt,'.');
					$Amount =substr($Amnt,0,$Amnt);
					?>
                    <input type="text" value="<?php echo number_format($Amount,2);?>" class="form-control" disabled />
					<input type="hidden" value="<?php echo $FundRequest->amount;?>" name="amount" id="amount"  />
					<input type="hidden" value="<?php echo $FundRequest->amt;?>" name="balance_amount" id="balance_amount" class="form-control" />
                </div>
				<div class="form-group">
                    <label for="trans_date">Transanction Date:</label>
                    <input type="text" value="<?php echo $FundRequest->Date;?>" id="trans_date" name="trans_date"  class="form-control" disabled />
                </div>
                </div>
				<div class="col-md-6">
                <div class="form-group ">
                    <label for="bank_name">Bank Name:</label>
                    <input type="text" value="<?php echo $FundRequest->bank_name;?>" id="bank_name" name="bank_name"  class="form-control" disabled />
                </div>
				<div class="form-group ">
                    <label for="bank_account_no">Bank Account No:</label>
                    <input type="text" value="<?php echo $FundRequest->bank_acc_no;?>"  id="bank_account_no" name="bank_account_no"  class="form-control" disabled />
                </div>
				</div>
				<div class="col-md-6">
				<div class="form-group">
                    <label for="transanction_ref">Transanction Reference:</label>
                    <input type="text" value="<?php echo $FundRequest->transaction_no;?>"  id="transanction_ref" name="transanction_ref" class="form-control" disabled />
                </div>
				<div class="form-group">
                    <label for="remark">Remark:</label>
                    <input type="text" value="<?php echo $FundRequest->transaction_remark;?>" id="remark" name="remark"  class="form-control" disabled />
                 </div>
				
				</div>
				<div class="col-md-6">
				<div class="form-group">
                    <label for="request_status">Status:</label>
                    <select name="request_status" id="request_status" class="chosen_select form-control"  >
                        <option value = ''>Pelase Select</option>
                        <option value = 'A'>Accept</option>
                        <option value = 'R'>Reject</option>
                       
                    </select>
                </div>
				<div class="form-group reject">
                    <label for="reject_reason">Reason for Rejection:*</label>
                    <input type="text" placeholder="Reject Reason" id="reject_reason" name="reject_reason"  class="form-control"/>
                </div>
            </div>
            </div>
            <div class="modal-footer clearfix">
			   <input type="hidden" name="fundRequest_id" value="<?php echo $FundRequest->id;?>">
                <a href="<?php echo base_url();?>admin/fund_request/fund_request_approval" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
				
                <button type="submit" id="submit_btn" class="btn btn-primary pull-left"><i class="fa fa-fw fa-save"></i> Submit </button>
            </div>
            </div>
            </form>
			<?php } ?>
		</div>
		</div>
	</div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        
	    $('#request_status').change(function(){
			var ReqStatus = $('#request_status').val();
			if(ReqStatus =='R'){
				$('.reject').show();
			}else{
				$('.reject').hide();
			}
		});
		
		$.validator.setDefaults({
          ignore: ":hidden:not(select)"
        });
    
		$("#fund_request_approval_form").validate({
			rules: {
				request_status: {
					required: true,    
				},
				reject_reason: {
                         required: function(element) {
                                    return $("#request_status").val() !== 'A';
                                    }
                                }
			}
		});
		});
</script>	