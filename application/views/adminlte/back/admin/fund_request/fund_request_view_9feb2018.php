<section class="content-header">   
<h1 >Balance Request View</h1>
<section class="content">
    <div class="row">
        <div class="col-md-12">
		   <?php
				$attributes = array("method" => "POST", "id" => "fund_request_approval_form", "name" => "fund_request_approval_form","class" => "fund_request_approval_form");
				echo form_open('', $attributes);
            ?>
             <div class="box box-default">
                <div class="box-header"></div><!-- /.box-header -->
		    <?php if (isset($error) && count($error) >0) { ?>
             <div class="alert alert alert-danger alert-error" style="display:block"id="msg">
       
                <strong>Error!</strong> 
                <ul id="errormsg">
                <?php foreach($error as $key=>$values)
                    {
                        echo "<li>".$values."</li>";
                    }     
                ?>
                </ul>
			 </div>
            <?php } ?>
		   
            <div class="box-body">
			<div class="row">
			
			    <div class="col-md-6">
                <div class="form-group">
                    <label for="request_to">Request To:</label>
					<?php if($FundRequest->topup_by =='PG'){?>
                    <input type="text" value="<?php echo $RequestBy;?>" name="request_to" id="request_to" class="form-control" disabled />
					<?php } else{?>
					<input type="text" value="<?php echo $RequestTo;?>" name="request_to" id="request_to" class="form-control" disabled />
					<?php } ?>
                </div>
                <div class="form-group">
                    <label for="transanction_mode">Transanction Mode:</label>
                    <input type="text" value="<?php echo $FundRequest->topup_by;?>" name="transanction_mode" id="transanction_mode" class="form-control" disabled />
                </div>
                </div>
			   
			    <div class="col-md-6">
			    <div class="form-group">
                    <label for="amount">Amount:</label>
					<?php
					$Amnt = (string)$FundRequest->amount;
					$Amt =strpos($Amnt,'.');
					$Amount =substr($Amnt,0,$Amt+3);
					?>
                    <input type="text" value="<?php echo $Amount;?>" class="form-control" disabled />
				</div>
				<div class="form-group">
                    <label for="trans_date">Transanction Date:</label>
                    <input type="text" value="<?php echo $FundRequest->Date;?>" id="trans_date" name="trans_date"  class="form-control" disabled />
                </div>
                </div>
				<?php if($FundRequest->bank_name){?>
				<div class="col-md-6">
				<div class="form-group ">
                    <label for="bank_name">Bank Name:</label>
                    <input type="text" value="<?php echo $FundRequest->bank_name;?>" id="bank_name" name="bank_name"  class="form-control" disabled />
                </div>
				<div class="form-group ">
                    <label for="bank_account_no">Bank Account No:</label>
                    <input type="text" value="<?php echo $FundRequest->bank_acc_no;?>"  id="bank_account_no" name="bank_account_no"  class="form-control" disabled />
                </div>
				</div>
				<?php } ?>
				<div class="col-md-6">
				<?php if($FundRequest->bank_name){?>
				<div class="form-group">
                    <label for="transanction_ref">Transanction Reference:</label>
                    <input type="text" value="<?php echo $FundRequest->transaction_no;?>"  id="transanction_ref" name="transanction_ref" class="form-control" disabled />
                </div>
				<?php } ?>
				<div class="form-group">
                    <label for="remark">Remark:</label>
                    <input type="text" value="<?php echo $FundRequest->transaction_remark;?>" id="remark" name="remark"  class="form-control" disabled />
                 </div>
				
				</div>
				
				<div class="col-md-6">
				<?php if($FundRequest->topup_by =='PG'){?>
				<div class="form-group">
                    <label for="request_status">Status:</label>
                    <input type="text" value="<?php if($FundRequest->is_approvel == 'A'){ echo 'Accepted';} else if($FundRequest->is_approvel == 'NA' or $FundRequest->is_approvel == 'aborted' or $FundRequest->is_approvel == 'pfailed') { echo 'Failed';} else if($FundRequest->is_approvel == 'R'){echo 'Rejected';}?>"  class="form-control" disabled />
                </div>
				<?php } else {?>
				<div class="form-group">
                    <label for="request_status">Status:</label>
                    <input type="text" value="<?php if($FundRequest->is_approvel == 'A'){ echo 'Accepted';} else if($FundRequest->is_approvel == 'NA') { echo 'Pending';} else if($FundRequest->is_approvel == 'R'){echo 'Rejected';}?>"  class="form-control" disabled />
                </div>
				<?php } ?>
				<div class="form-group">
				    <?php if($FundRequest->is_approvel == 'R'){?>
                    <label for="reject_reason">Reason for Rejection:</label>
                    <input type="text" value="<?php echo $FundRequest->reject_reason;?>"  class="form-control" disabled />
					<?php } ?>
                </div>
			    </div>
				
            </div>
            <div class="modal-footer clearfix">
                <a href="<?php echo base_url();?>admin/fund_request" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
				
            </div>
            </div>
            </form>
		</div>
	</div>
</section>