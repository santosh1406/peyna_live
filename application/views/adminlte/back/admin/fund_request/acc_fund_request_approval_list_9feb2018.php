<script type="text/javascript">
var role_name = '0';
<?php if (in_array(strtolower($this->session->userdata('role_name')), array('admin', 'super admin'))) { ?>
        role_name = '1';
<?php } ?>
</script>
<style>
.fields{display:none;}
.reject{display:none;}
.row{margin-bottom:5px;} 
</style>
<section class="content-header">
    <h1>Balance Request Approval List</h1>
    <div class="row" style="margin-top:15px;">
		<div class="col-md-12">
            <div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
					   <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Advanced Search </a>
					</h4>
				</div>						
				<div class="panel-body" style="height:200px; overflow-y:auto;">
				   	<?php
						$attributes = array("method" => "POST", "id" => "Filter_Frm", "name" => "Filter_Frm","class" => "Filter_Frm");
						echo form_open('',$attributes);
					?>
					
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">Search By&nbsp;</label>
								   	<div class="col-md-9">
										<select name="SearchBy" id="SearchBy" class="form-control">
											<option value="0">All</option>
											<option value="4">Master Distributor</option>
											<option value="5">Area Distributor</option>
											<option value="6">Agent</option>
											<option value="7">Sales Executive</option>
										</select>
								   </div>                                                      
							    </div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">Request By</label>
									<div class="col-md-9">
									    <select name="RequestBy" id="RequestBy" class="form-control">
                                            <option value="">Select RequestBy</option>
										</select>   
									</div>
								</div>
						    </div>
						</div>
						<div class="row">
					        <div class="col-md-6">
								<div class="form-group">
									<label class="control-label col-md-3">Status&nbsp;</label>
									<div class="col-md-9">
										<select name="status" id="status" class="form-control" >
											<option value="">All</option>
											<option value="NA">Pending</option>
											<option value="A">Accept</option>
											<option value="R">Reject</option>
										</select>
									</div>
								</div>
							</div>	
					    </div>
						<div class="clearfix margin-top-20"></div>
						<div class="row">
							<div class="col-md-6">
							</div>
							<div class="col-md-6">
								<div class="col-md-offset-3 col-md-9">
									<button type="button" class="btn btn-primary btn-sm btn-small" onclick="setFilter()">Search</button>
									<button type="button" class="btn  btn-sm btn-small" onclick="ResetFilter()">Reset</button>
								</div>
							</div>
						</div>
						<div class="clearfix margin-top-20"></div>                                 
					</form> 
					<div class="clearfix margin-top-20"></div>
				</div>
        	</div>
		</div>
	</div>
</section>

<!-- Main content -->
<section class="content">
    <div class="commission_wrapper">
        <div class="box">
            <div class="box-body">
                <div class="alert alert-success alert-dismissible no_margin mb10 hide" role="alert" id="alert_msg">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="body"></div>
                </div>
			    <?php
					$attributes = array("method" => "POST", "id" => "approval_list", "name" => "approval_list");
					echo form_open('',$attributes);
				?>
	                <table id="fund_request_approval" name="fund_request_approval" class="table table-bordered table-hover">
	                    <thead>
	                        <tr>
	                            <th  width="10%">ID</th>
								<th>Request By</th>
								<th>Request To</th>
								<th>Date Time</th>
								<th>Amount</th>
								<th>Trans Mode</th>
								<th>Remark</th>
								<th>Company Status</th>
								<th>Account Status</th>
								<th>Actions</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    </tbody>
	                </table>		    
                </form>		    
                <div class="clearfix"></div>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <div class="clearfix"></div>
            </div><!-- /.box-footer-->
        </div>
    </div>
</section>
<script type="text/javascript">
    var frm = document.Filter_Frm;

    $(document).ready(function () {
		$("#SearchBy").change(function () {
			$('#RequestBy').empty();
			$("#RequestBy").html("<option value=''>Please wait..</option>");
			$.ajax({
				type: "POST",
				data: $('#Filter_Frm').serialize(),
				url: "<?php echo base_url().'admin/fund_request/getAccUserByLevel'; ?>",
				success: function (msg) {
					var Oresult = jQuery.parseJSON(msg);
					var ReqResult = Oresult['result'];
					if (ReqResult != '') {
                        $("#RequestBy").html("<option value=''>Please Select</option>");
					    var option = '<option value="">All Users</option>';
						for (var i = 0; i < ReqResult.length; i++) {
							option += '<option value="' + ReqResult[i]['id'] + '">' + ReqResult[i]['agent_name'] + '</option>';
						}
						$('#RequestBy').append(option);
					} else {
						$("#RequestBy").html("<option value=''>No Users</option>");
					}
				}
			});
        });
		DatatableRefresh();
   	});
	   
	function DatatableRefresh() {
		$('#fund_request_approval').dataTable({
			"aoColumnDefs": [ {"bSortable": true, "aTargets": [0,8] } ], 
			//"scrollY": "600",
			"deferRender": true,
			"bProcessing": true,
			"bServerSide": true,
			"order": [[ 0, 'desc' ]],
			"lengthMenu": [
				[10, 50, 100, -1],
				[10, 50, 100, "All"] // change per page values here
			],
			"bScrollCollapse": true,
			"bAutoWidth": true,
			"sPaginationType": "full_numbers",
			"bDestroy": true,
			"sAjaxSource": "<?php echo base_url().'admin/fund_request/DataTableAccFundRequestApproval'; ?>",
			"fnServerData": function (sSource, aoData, fnCallback) {
				    aoData.push({name: 'RequestBy', value:$('#RequestBy').val()});
                    aoData.push({name: 'status', value:$('#status').val()});
                    $.getJSON(sSource, aoData, function (json) {
					fnCallback(json);
				});
			},
			"fnRowCallback": function (nRow, aData, iDisplayIndex) {
				return nRow;
			},
			"fnFooterCallback": function (nRow, aData) {
			}
		});
	}

    function ResetFilter() {
		frm.reset();
		DatatableRefresh();
	}

	function setFilter() {
		DatatableRefresh();
	}
		
</script>