<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3>
            &nbsp;Ticket Window Topup            
        </h3s>
       
    </section>
       

   <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="box">   
                 <?php if($this->session->flashdata('msg')) { ?>
                    <div class="alert alert-success alert-dismissable">
                        <b>
                            <?php
                            $data = $this->session->flashdata('msg');
                            echo $data;
                            ?>
                        </b>
                      
                    </div>
                <?php } ?>    
                    <div class="box-body">
							<?php
								$attributes = array("method" => "POST", "id" => "ticket_window_form", "name" => "ticket_window_form");
								echo form_open(base_url().'admin/ticket_window_topup/save_amt', $attributes);
				            ?>
            
							<div class="form-group">
								<label class="col-lg-3 control-label" for="name">Amount</label>
		                        <div class="col-lg-6">				
								<input name="topup_amt" type="text"  id="topup_amt" class="form-control" placeholder="Topup Amount"  maxlength="6" autocomplete="off"  required />&nbsp;		                                                       
									<span id="erramountmsg" class="error"></span>
								</div>
							 </div>
						 	<div class="clearfix" style="height: 10px;clear: both;"></div>
						  	<div class="form-group">
		                        <div class="col-lg-offset-4">
					 				<button class="btn btn-primary" id="save_group_data" name="save_group_data" type="submit">Submit</button> 
		                          <!--   <button class="btn btn-primary back" id="back_data" type="button">Back</button>  -->
		                        </div>
		                    </div>
                         </form>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script type="text/javascript">
$(document).ready(function() { 
	$('#ticket_window_form').validate({ 
             rules: {
                topup_amt: {
                    required: true,
                    number:true,
                    digits:true
                    // minlength:20,
                    // maxlength:20
                },                 
            },
            messages: {
                imei_no: {
                    required :"Please enter amount",
                    number:"Amount should be number only.",
                    digits:"Amount should be number only.",
                    //  minlength: "Please enter 20 digit SIM No",
                    // maxlength: "Please enter 20 digit SIM No",          
                }
            },
            submitHandler: function () { 
                submit.form();
            }
        });      

});
</script>