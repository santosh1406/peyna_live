	
<section class="content-header">
    <h1> Request Refund Unsuccessful Ticket </h1>
</section>
<section class="content">
    <div class="content">
        <div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div id="main" class="box" style="padding:20px;">
        	<div class="row">
        		<div class="col-sm-3">
				
					<?php
					$attributes = array("method" => "POST", "name" => "ticket_detail_form", "id" => "ticket_detail_form");
					echo form_open(base_url.'admin/Unsuccessful_refundable_tickets/refund_index', $attributes);
					?>        			
					  <div class="form-group">
					    <label for="exampleInputEmail1">Email address</label>
					    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" value="<?php echo $email_id ?>" data-msg-required="Please enter Email Address">
					  </div>
					  <div class="form-group">
					    <label for="pnr_no">Rokad Ref No</label>
					    <input type="text" class="form-control bos_pnr" placeholder="Enter BOS Reference Number" name="bos_ref_no" id="bos_ref_no" value="<?php echo $bos_ref_no ?>">
					  </div>

					  <div class="seperator_or">
					  	<label class="text-align-center white dark_gray_bg">OR</label>
					  </div>
					  
					  <div class="form-group">
					    <label for="pnr_no">PNR no</label>
					    <input type="text" class="form-control bos_pnr" id="pnr_no" name="pnr_no" placeholder="Enter PNR No" value="<?php echo $pnr_no ?>" >
					  </div>
					  <div class="form-group">
					    <label for="">&nbsp;</label>
					    <button type="submit" class="btn btn-primary" name="show_ticket" id="show_ticket">Show Ticket</button>
					  </div>
					</form>
        		</div>
        		<div class="col-sm-9">
    				<div style="background: #fff;" class="pad20">
						<div style="background: #fff;" id="booking">
						   <h2>Trips You have Booked!</h2>
						   <div class="booking-history">
						   	<?php if(count($ticket_detail) > 0){ ?>
								<?php 
								foreach($ticket_detail as $key=> $info): 
								
								?>
								
									<form action="" class="cancel_booking_form" onsubmit=" return false;" method="post">
								      <div class="booking-info clearfix">
								        <div class="ticket_info" style="cursor: pointer;">
								            <div class="date">
								               	<label class="month"><?php echo date("M",strtotime($info["dept_time"])); ?></label>
								                <label class="date"><?php echo date("d",strtotime($info["dept_time"])); ?></label>
								                <label class="day"><?php echo date("D",strtotime($info["dept_time"])); ?></label>
								            </div>
								            <h4 class="box-title">
								               <i class="icon fa fa-bus yellow-color circle"></i>
								               <?php echo $info['boarding_stop_name']; ?> to <?php echo $info['destination_stop_name']; ?> <small><?php echo $info['op_name']; ?></small>
								            </h4>
								            <dl class="info cancel_info">
								               <dt>Total Fare</dt>
								               <dd class="mb5"><?php echo to_currency($info['tot_fare_amt_with_tax']); ?></dd>
								               <dt>Travel Date</dt>
								               <dd><?php echo date("jS M y h:i A", strtotime($info['dept_time'])); ?></dd>
								            </dl>
								            <dl class="info cancel_info">
								               <dt>PNR No</dt>
								               <dd class="mb5"><?php echo $info['pnr_no']; ?></dd>
								               <dt>Rokad Ref No</dt>
								               <dd class="mb5"><?php echo $info['boss_ref_no']; ?></dd>
								            </dl>
											 <?php
											 $travel_date=date("jS M y h:i A", strtotime($info['dept_time']));
											 $today_date= date("jS M y h:i A");
											
											 if(strtotime($travel_date) < strtotime($today_date)){
											
												 ?>
										    <button class="btn-mini status btn btn-warning btn-sm">Travelled</button> 
										    <?php
											 }
											 else{
											 
											 ?>
								            <button class="btn-mini status btn btn-warning btn-sm">UPCOMMING</button> 
										<?php	} ?>
								            <div class="clearfix"></div>
								         </div>
								         <div class="passenger_info clearfix" style="display:none;">
								            <table class="table table-hover table-striped">
								               <thead>
								                  <tr>
								                     <th>Name</th>
								                     <th>Age</th>
								                     <th>Gender</th>
								                     <th>Seat No</th>
								                     <th>Fare</th>
								                     <th>&nbsp;</th>
								                  </tr>
								               </thead>
								               <tbody>
								               		<?php foreach ($info['detail_arr'] as $td_key => $td_value ): ?>
								               		<tr>
									                    <td><?php echo $td_value->psgr_name ?></td>
									                    <td><?php echo $td_value->psgr_age ?></td>
									                    <td><?php echo $td_value->psgr_sex ?></td>
									                    <td><?php echo $td_value->seat_no ?></td>
									                    <td colspan='2'><?php echo to_currency($td_value->fare_amt) ?></td>
			
												<input type="hidden" id="provider_id" name="provider_id" value="<?php echo $info['provider_id'] ?>" > 
												<input type="hidden" id="proty" name="proty" value="<?php echo $info['provider_type'] ?>" > 
												<input type="hidden" id="op_id" name="op_id" value="<?php echo $info['op_id'] ?>" > 
												<input type="hidden" id="ticket_id" name="op_id" value="<?php echo $info['ticket_id'] ?>" > 
									                </tr>
									                <?php endforeach; ?>
								               </tbody>
								               <tfoot>
								               		<tr>
                                                                                            <td colspan="6" class="text-right">
                                                                                                <button id="cancellable_status" class="btn btn-warning btn-sm" name="cancellable_status" data-opname="<?php echo $info['op_name'] ?>"  data-pnr="<?php echo $info['pnr_no'] ?>" data-ticket-id="<?php echo $info['ticket_id'] ?>">Cancelable Status</button>
                                                                                                <button class="continueBtn cancel_btn btn btn-warning btn-sm" id="cancel_button" data-opname="<?php echo $info['op_name'] ?>"  data-pnr="<?php echo $info['pnr_no'] ?>" data-ticket-id="<?php echo $info['ticket_id'] ?>" disabled >Refund</button>
                                                                                            </td>
								               		</tr>
								               </tfoot>
								               
								            </table>
								         </div>
								      </div>
								    </form>
						      <?php endforeach; ?>
						   </div>
						</div>
					</div>
					<?php 
						}
						else
						{
					?>
							<div class="booking-info clearfix">
								<h3>No ticket found.<h3/>
							</div>
					
					<?php 
						}
					?>
					<div></div>
					<div class="row">
						<div class="col-md-12">
							<div class="pagination pull-right">
								<?php echo $links; ?>
							</div>
						</div>
					</div>

        		</div>
        	</div>
        </div>
        </div></div></div>
</section>
    
<script type="text/javascript">
$(document).ready(function(){
	$('.ticket_info').click(function(){
		$(this).parent().find('.passenger_info').slideToggle('medium');
	}).css({'cursor':'pointer'});

	
            

	$('#ticket_detail_form').validate({
            ignore: "",
            rules:
                {
                    email: 
                    {
                       required: true,
                        email: true,
                    },
                    pnr_no:
                        {
                            require_from_group: [1, ".bos_pnr"]
                        },
                    bos_ref_no:
                        {
                            require_from_group: [1, ".bos_pnr"]
                        }
                },
            messages: 
                {
                    pnr_no:
                        {
                        require_from_group: "Please enter either  Rokad Ref No or PNR No"
                        },
                    bos_ref_no:
                        {
                        require_from_group: "Please enter either  Rokad Ref No or PNR No"
                        }
                },
            groups:
                    {
                        mygroup: "bos_ref_no pnr_no" ,
                    },
                 errorPlacement: function (error, element) {
                if((element.attr('name')=='bos_ref_no') ||(element.attr('name')=='pnr_no') )
                {
                  error.insertBefore('.btn-large');
                }
                else 
                {
                    error.insertAfter(element);
                }
        },
       
    });


    $(document).off('click','#cancellable_status').on('click','#cancellable_status', function(){
		
    var elem 		= $(this);
    var wrapper 	= $(this).parents('.booking-info');
    var detail 		= {};
    var form 		= '';
    var div 		= '';
    var ajax_url 	= 'admin/unsuccessful_refundable_tickets/unsuccessfull_booked_tkt_status';
    var formElem   	= $(this).parents('form.cancel_booking_form');
    var seat 		= [];
    
        detail['ticket_id']             = $(this).attr('data-ticket-id');
        detail['pnr_no'] 		= $(this).attr('data-pnr');
        detail['op_name'] 		= $(this).attr('data-opname');
	detail['email']			= $("#email").val();
	detail['proty']			= $("#proty").val();


		get_data(ajax_url, form, div, detail, function(response)
	    {
			
			
				 if(response.flag=="@#success#@")
				 {
					 var yesno=confirm(response.message + "\n\n" + 'Refund amount Rs.'+ response.refund_amt );
					  if(yesno)
					  $('#cancel_button').prop('disabled', false);
				 }
				 else 
				 {
					 if(response.flag=="@#failed#@")
					 {
						 alert(response.error);
					 }
				 }
				
			
		});
	 });
  $(document).off('click','.cancel_btn').on('click','.cancel_btn', function(){
    
    var elem 		= $(this);
    var wrapper 	= $(this).parents('.booking-info');
    var detail 		= {};
    var form 		= '';
    var div 		= '';
    var ajax_url 	= 'admin/unsuccessful_refundable_tickets/refund_unsuccessfull_ticket';
    var formElem   	= $(this).parents('form.cancel_booking_form');
    var seat 		= [];

    detail['seat_no'] 		= seat;
    detail['ticket_id'] 	= $(this).attr('data-ticket-id');
    detail['pnr_no'] 		= $(this).attr('data-pnr');
    detail['op_name'] 		= $(this).attr('data-opname');
    detail['email']		= $("#email").val();
    detail['proty']		= $("#proty").val();

    // console.log(detail);
    var yesno = confirm("Are you sure you want to refund this ticket?");

    if(yesno)
    {
        get_data(ajax_url, form, div, detail, function(response)
        {
                if (response.flag == '@#success#@')
                {
                var msg_block 	= $.parseHTML($('#msg_box').html());

                var ul = $('<ul/>').addClass('arrow');
                ul.append($('<li/>').html('Refund amount Rs.'+ response.refund_amt));
//                ul.append($('<li/>').html('Cancellation charges Rs.'+ response.cancellation_cahrge));
                ul.append($('<li/>').html('Reason for cancellation: Unsuccessfull ticket booked after successfull payment'));
//                console.log(detail);
//                console.log(response);
                $(msg_block).find('.info_container').append(ul);
//				console.log($(msg_block));
                wrapper.addClass('cancelled').find('.ticket_info .status').html('CANCELLED');

                elem.parent().removeClass('text-right').addClass('text-left').html($(msg_block));


                }
                else
                {
                        alert(response.error);
                }
       });
    }

    // console.log( $(this).parent('form.cancel_booking_form').serialize() ); 


  });

}) // end of document

</script>
<style type="text/css">
	.pagination{ margin-right: 15px;}
	.pagination a{ padding: 5px; margin: 0 5px; }
        #booking .booking-info:last-child {
    border-bottom: 1px solid #f5f5f5;
}

</style>
<script type="text/html" id="msg_box">
	<div class="info-box auto_hide" data-time="300">
		<span class="close"></span>
		<div class="info_container"></div>
	</div>
</script>
