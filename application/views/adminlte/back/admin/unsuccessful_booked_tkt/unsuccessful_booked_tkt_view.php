<script type="text/Javascript" src="<?php echo base_url() . COMMON_ASSETS ?>js/jquery.validate.min.js" ></script>

<script type="text/Javascript" src="<?php echo base_url() ?>js/back/report/bos_ticket_details_report.js" >
</script>
<style>
    label.error, div.error, ul.error, span.error {
        color: #f00;
        font-size: 14px;
    }
    th { white-space: nowrap; }
</style>
<section class="content-header">
    <h3>Rokad Unsuccessful Booked Tickets Against Successful Payment</h3>

</section>

<!-- Main content -->
<section class="content">
    <div id="wallet_wrapper">
        <!--<button  type="button"  class="btn btn-add btn-sm adve" attr-data="modify_search_form" data-attr="next-pre-search" id="modify_search_form"  onclick = "javascript:showAdvSearch();" style="display:none;float:right;">Modify Search</button>
        <button type="button"  class="btn btn-add btn-sm adve" attr-data="next-pre-search" data-attr="modify_search_form"  id="next-pre-search"   onclick ="javascript:showAdvSearch();" style="float:right;">Hide Search</button>
        --><div class="clearfix" style="height: 10px;clear: both;"></div>
        <div class="row"><div id="showDivAdvSearch" >
            <div class="col-xs-12">
                <div class="box" style="padding:10px;">
                    <form id="theform" name="theform" onsubmit="return false;">
                        <div class="row box-title" >
                        

                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-1">From Date</label>
                                    <input class="form-control date" id="from_date" placeholder="<?php echo date('d-m-Y', strtotime('-1 days')); ?>" type="text" >
                                    <!-- <p class="help-block">Example block-level help text here.</p> -->
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2">Till date</label>
                                    <input type="hidden" name="def_to_date" id="def_to_date" value="<?php echo date('Y-m-d');?> ">
                                    <input type="hidden" name="def_from_date" id="def_from_date" value="<?php echo date('Y-m-d', strtotime('-1 days')); ?>">
                                    <input class="form-control date" id="till_date" placeholder="<?php echo date('d-m-Y');?>" type="text">
                                    <!-- <p class="help-block">Example block-level help text here.</p> -->
                                </div>
                            </div>

                            <div style="display: block;" class="col-md-2 display-no">    
                                <div style="position: static;" class="form-group">
                                   
                                  
                                </div>
                            </div>
                            
                             <div style="display: block;" class="col-md-2 display-no">    
                        </div>
                            
                            <div style="display: block;" class="col-md-4 display-no">    
                                <div style="position: static;" class="form-group">
                                   
                                    
                                    <!-- <p class="help-block">Example block-level help text here.</p> -->
                                </div>
                            </div>
                            

                            
                              <div style="display: block;" class="col-md-4 display-no">   
                                    <div style="display: block;"  class="box-footer2 exports_btn">
                                      
                                        <button type="submit" class="btn btn-primary ">Submit</button>
                                        <!--<a class="btn btn-primary " href="<?php echo base_url()?>admin/unsuccessful_refundable_tickets/refund_index/">Generate New Request</a>-->						
                                </div>
                              </div>
                        </div> 
                        
                        
                        
                 
                    </form>
                       
							 
					
                    <br><br>
                            <div class="box-body table-responsive" id="table_show">
                               
                                <table id="op_rept" name="op_rept" class="table table-striped">
								
                                    <thead>
                                        <tr>
                                             <th>Id</th>
                                           <th>Ticket Id</th>
                                            <th>Transaction Status</th>
                                            <th>Ticket Ref No</th>
                                            <th>PG Track Id</th>
                                            <th>User Email Id</th>
                                            <th>Issued Date</th>
                                            <th>DOJ</th>
                                           <th>Ticket Fair</th> 
                                           <th>Refund amount</th> 
                                           <th>Action</th>

                                            
                                        </tr>
                                    </thead>

                                    <tbody>                                           

                                    </tbody>
                                </table>
                            </div>
                        </div></div></div>


                <div class="clearfix" style="height: 10px;clear: both;"></div>   
            </div>
        </div>

<!-- <style type="text/css" href="<?php echo base_url().COMMON_ASSETS;?>plugins/datatable/extension/dataTables.colVis.css"></style> -->
<!-- <script type="text/javascript" src="<?php echo base_url().COMMON_ASSETS;?>plugins/datatable/extension/dataTables.colVis.js" ></script> -->
</section>
<script type="text/javascript">
    
    $(document).ready(function(){
   var tconfig = {
            "processing": true,
            "serverSide": true,
            "scrollX": true,
            "ajax": {
                "url": BASE_URL + "admin/unsuccessful_refundable_tickets/unsuccessful_tkt_details",
                "type": "POST",
                "data": "json",
                data   : function(d) {
					
                    var date = new Date();
                   // var from_date;
                    //var till_date;
                    if($("#from_date").val()=='') {
                        from_date= $("#def_from_date").val();
                        
                    }else{
                        from_date=$("#from_date").val();
                    }
                    if($("#till_date").val()=='') {
                        till_date=$("#def_to_date").val();
                    }else{
                        till_date=$("#till_date").val();
                    }
                
                    d.from_date =from_date;
                    d.till_date = till_date;
					d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;                    
                }
            },
            "columnDefs": [
                {
                    "searchable": false
                }
            ],
            "iDisplayLength": 10,
            "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
            "bFilter": false,
            "bPaginate": true,
            "bRetrieve": true,
            "order": [[1, "desc"]],
            "aoColumnDefs": [
                {
                  //  "targets": [13],
                    "visible": true,
                    "searchable": false

                },
                {
                    "bSortable": false,
                    "aTargets": [0]
                },
                {
                    "bSortable": false,
                    "aTargets": [1]
                },
              
            ],
               "oLanguage": {
                    "sProcessing": '<img src="<?php echo base_url().BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>' 
                    },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
               
                
             
                $("td:first", nRow).html(iDisplayIndex + 1);
              
                return nRow;

            },

        };

       
        oTable = $('#op_rept').dataTable(tconfig);
	
        $('#theform').submit(function(e) {
            
            oTable.fnDraw();
            // summary_data();

        });
        
        $(document).off('change', '.toggle-vis').on('change', '.toggle-vis', function (e) 
            { 

                e.preventDefault();
                   
                oTable.fnSetColumnVis( $(this).val(), false );
            });


        

        $('.date').datepicker({
            dateFormat: 'dd-mm-yy'
        });

        //validate both  dates
        $.validator.addMethod("checkDepend",
                function(value, element) {
                    if ($("#from_date").val() != "")
                    {
                        var date_diff = date_operations($("#from_date").val(), $("#till_date").val());
                        if (date_diff.date_compare)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return true;
                    }

                }, "To date must be greater than from date.");

        $('#hand_held').validate({
            ignore: "",
            rules: {
                from_date: {
                    required: true
                },
                till_date: {
                    checkDepend: true
                }
            },
            messages: {
                from_date: {
                    required: "Please Enter from date"

                }
            }
        });

    });
        
    
    function showAdvSearch()
    {
        $("#showDivAdvSearch").toggle();
    }


         
  



</script>
<script type="text/javascript">
   function doconfirm()
{  
   var yesno = confirm('Are you sure you want to refund this ticket');
        if(yesno != true)
       {
            return false;
  }
  }
</script>



