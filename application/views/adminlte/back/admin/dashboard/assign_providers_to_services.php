<!-- Created by harshada kulkarni on 24-07-2018 -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> 
                        <h3 class="box-title">Assign Providers</h3>
                    </div>
                    <div class="box-body">
                        <?php
                        echo validation_errors('<div class="error">', '</div>');
                        if ($this->session->flashdata('message')) {
                            ?>
                            <div class="alert alert-success alert-dismissable" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('message'); ?>
                            </div>
                            <?php
                        }

                        $attributes = array("method" => "POST", "id" => "assign_providers", "name" => "assign_providers");
                        // echo form_open(base_url() . 'admin/Dashboard/save_assign_providers', $attributes);
                        echo form_open('', $attributes);
                        ?>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                      <?php 
                      foreach($provider_array as $key => $provider){ 
                        // show($provider->id);show($selected_array[$provider->id], 1); 
                        ?>
                        <div class="form-group">       
                            <label class="col-lg-3 control-label"><?php echo $provider->provider_name ?></label>
                            <div class="col-lg-6">  
                              <select name="providers[<?php echo $provider->id ?>][]" class="form-control select2" title="Please select something!" multiple="multiple">
                              <option value ="">Select Services</option>
                              <?php foreach($utilities_array as $utilities){ ?>
                                  <option value="<?php echo $utilities->id;?>" <?php echo in_array($utilities->id, $selected_array[$provider->id]) ? 'selected' : '' ?> ><?php echo $utilities->utility_name;?></option>
                              <?php } ?>
                          </select>                            
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                      <?php } ?>
                        
                        <div class="form-group">
                            <div class="col-lg-offset-4">
                                <button class="btn btn-primary" id="submit_recharge" name="submit_recharge" type="submit">Save</button>         
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    $(document).ready(function(){
      $('.select2').select2();

      $(document).on('change', '.select2', function(e){
        var siblings = $(document).find('select').not(this).val();
        var current_selected = $(this).val();
        var old_val = current_selected;
        current_selected = current_selected[current_selected.length - 1];
        if($.inArray(current_selected, siblings)  !== -1){
          alert('Cannot use same service for multiple providers.')
          $(this).select2('val', old_val);
        }
        
      });

      $('#submit_recharge').click(function(e){
        e.preventDefault();
        var form_values = $("form").serialize();
        var url = "<?php echo base_url() . 'admin/dashboard/save_assign_providers' ?>";

        $.ajax({
         type: "POST",
         url: url,
         data: form_values, // serializes the form's elements.
         success: function(data)
         {
             swal({
              title: 'Success',
              text: "Data assigned successfully!",
              type: 'success',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Ok!'
            }, function(){
               location.reload();
            })
         }
       });

      });

    });
</script>