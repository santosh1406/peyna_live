<?php 
$style = '';
if($period != 'date_range') {
    $style="display:none;";
}
?>
<section class="content-header">
    <h1> Dashboard </h1>
    <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
   </ol> -->
  <?php 
   if($this->session->flashdata('msg')) {
   ?>
    <div class="alert alert-success alert-dismissable" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo $this->session->flashdata('msg'); ?>
    </div>
   <?php          
   }
  ?>
  
</section>  

<?php if($user_role_id ==  SUPERADMIN_ROLE_ID || $user_role_id == TRIMAX_ROLE_ID || $user_role_id ==  BUSINESS_ADMIN_ROLE_ID ||
        $user_role_id ==  OPERATION_ADMIN_ROLE_ID ||  $user_role_id ==  OPERATION_SUPPORT_ROLE_ID) {?> 
<section class="content"> 
    <div class="row">
      <!--Code added by Aparna Chalke -2018-06-29 - CR 314-->
    <form name="frmDashboard" id="frmDashboard" method="post" action= ""> 
        <input type="hidden" name="rokad_token" value="<?php echo $this->security->get_csrf_hash();?>"> 
        <div class="col-md-3 display-no">    
            <div style="position: static;" class="form-group">
                <label for="">For</label>
                <select class="form-control" id="period" name="period">
                    <option value="today" <?php if($period == 'today' || $period == '') { ?> selected="selected" <?php } ?> >Today</option>
                    <option value="current_month" <?php if($period == 'current_month') { ?> selected="selected" <?php } ?> >Current Month</option>
                    <option value="date_range" <?php if($period == 'date_range') { ?> selected="selected" <?php } ?> >Select Date Range</option>
                </select>
                <!-- <p class="help-block">Example block-level help text here.</p> -->
            </div>
            
        </div>
        <div id="dateRangeDiv" style="<?php echo $style; ?>">
            <div class="col-md-3 display-no">    
                <div style="position: static;" class="form-group">
                    <label for="input-text-1">From Date</label>
                    <input class="form-control" id="from_date" name="from_date" placeholder="From Date" type="text" value="<?php if(isset($from_date))echo $from_date; ?>" readonly="readonly">
                </div>
           </div>
            <div class="col-md-3 display-no">  
               <div style="position: static;" class="form-group">
                    <label for="input-text-2">Till date</label>
                    <input class="form-control " id="till_date" name="till_date" placeholder="Till Date" type="text" value="<?php if(isset($to_date)) echo $to_date; ?>" readonly="readonly">
                </div>
            </div>
            <div class="col-md-1 display-no"> 
                <label for="input-text-2" style=""></label>
                <input id type="submit" class="form-control btn btn-primary" value="Submit"/>
            </div>
        </div>
    </form>
    </div>
   <!--Code added by Aparna Chalke -2018-06-29 - CR 314-->
    <div class="row">
        <?php if($user_role_id ==  SUPERADMIN_ROLE_ID || $user_role_id == TRIMAX_ROLE_ID || $user_role_id ==  BUSINESS_ADMIN_ROLE_ID ||
        $user_role_id ==  OPERATION_ADMIN_ROLE_ID ||  $user_role_id ==  OPERATION_SUPPORT_ROLE_ID){ ?>
         <div class="col-lg-3 col-xs-6 total_transactn_amt" style="cursor: pointer;">
          <div class="small-box bg-purple">
            <!-- <span class="small-box-icon bg-purple"><i class="fa fa-archive"></i></p> -->
            <div class="inner">
              <p>Total GMV</p>
              <h3><?php echo $total_gmv; ?></h3>
            </div>
            <!-- /.inner -->
            <div class="icon"> <i class="fa fa-inr"></i> </div>
          </div>
          <!-- /.small-box -->
        </div>
         <div class="col-lg-3 col-xs-6 total_passenger">
          <div class="small-box bg-red">
           <!--  <span class="small-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span> -->
            <div class="inner">
              <p>Total Commission</p>  <h3><?=$total_commission_rokad;?></h3>
            </div>
             <div class="icon"> <i class="ion ion-bag"></i> </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6 active_agents" style="cursor: pointer;">
          <div class="small-box bg-aqua">
           <!--  <span class="small-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span> -->
            <div class="inner">
               <p>Active Agents</p>  <h3><?=$active_agents_rokad;?></h3>
            </div>
             <div class="icon"> <i class="ion ion-person-add"></i> </div>
          </div>
        </div>
       <?php } ?>
        <div class="col-lg-3 col-xs-6 total_tickets" >  
          <div class="small-box bg-green">
           <!--  <span class="small-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span> -->
            <div class="inner">
              <p>Ticket Booked</p>   <h3><?=$total_tickets_number;?></h3>
            </div>
             <div class="icon"> <i class="ion ion-bag"></i> </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6 total_passenger">
          <div class="small-box bg-red">
           <!--  <span class="small-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span> -->
            <div class="inner">
              <p>Total Passenger</p>   <h3><?=$total_passenger;?></h3>
            </div>
             <div class="icon"> <i class="ion ion-bag"></i> </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6 active_users" style="cursor: pointer;">
          <div class="small-box bg-aqua">
           <!--  <span class="small-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span> -->
            <div class="inner">
              <p>Active Users</p>
              <h3><?php echo $active_users; ?></h3>
            </div>
             <div class="icon"> <i class="ion ion-person-add"></i> </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6 pending_users" style="cursor: pointer;">
          <div class="small-box bg-red">
            <!-- <span class="small-box-icon bg-red"><i class="fa fa-users"></i></span> -->
            <div class="inner">
              <p>Pending Users (Email & Mobile)</p>
              <h3><?php echo $pending_users_data; ?></h3>
            </div>
            <div class="icon"> <i class="ion ion-email"></i> </div>
          </div>
        </div>
        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>
        <div class="col-lg-3 col-xs-6 pending_kyc" style="cursor: pointer;">
          <div class="small-box bg-green">
           <!--  <span class="small-box-icon bg-green"><i class="fa fa-file"></i></span> -->
            <div class="inner">
              <p>Pending KYC</p>
              <h3><?php echo $kycpending; ?></h3>
            </div>
            <div class="icon"> <i class="ion ion-document"></i> </div>
            <!-- /.inner -->
          </div>
          <!-- /.small-box -->
        </div>
        <!-- /.col -->
        <!-- <div class="col-lg-3 col-xs-6 ticket_details" style="cursor: pointer;">
          <div class="small-box">
            <span class="small-box-icon bg-yellow"><i class="fa fa-ticket"></i></p>

            <div class="inner">
              <p>Ticket Details</p>
              <h3><?php echo $ticket_report_success; ?></p>
            </div>
          </div>
        </div> -->
        <!-- <div class="clearfix visible-sm-block"></div>
        <div class="col-lg-3 col-xs-6 ticket_details" style="cursor: pointer;">
          <div class="small-box">
            <span class="small-box-icon bg-maroon"><i class="fa fa-check-square"></i></p>

            <div class="inner">
              <p>Today's Total No</p>
              <p>of Transaction</p>
              <h3><?php echo $ticket_report_success; ?></p>
            </div>
          </div>
        </div> -->
        <div class="clearfix visible-sm-block"></div>
        <div class="col-lg-3 col-xs-6 total_transactn_amt" style="cursor: pointer;">
          <div class="small-box bg-purple">
            <!-- <span class="small-box-icon bg-purple"><i class="fa fa-archive"></i></p> -->
            <div class="inner">
              <p>Total Transaction Amount</p>
              <h3><?php echo to_currency($total_tickets_amt); ?></h3>
            </div>
            <!-- /.inner -->
            <div class="icon"> <i class="fa fa-inr"></i> </div>
          </div>
          <!-- /.small-box -->
        </div>
        <div class="clearfix visible-sm-block"></div>
		<?php if($user_role_id == COMPANY_ROLE_ID){?>
        <div class="col-lg-3 col-xs-6 commission_per_day" style="cursor: pointer;">
          <div class="small-box bg-light-blue">
            <!-- <span class="small-box-icon bg-light-blue"><i class="fa fa-pie-chart"></i></p> -->
            <div class="inner">
              <p>Commission Earned</p>
              <h3><?php echo to_currency($final_commission); ?></h3>
            </div>
            <!-- /.inner -->
             <div class="icon"> <i class="fa fa-inr"></i> </div>
          </div>
          <!-- /.small-box -->
        </div>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-lg-3 col-xs-6 chain_commission">
          <div class="small-box bg-teal">
            <!-- <span class="small-box-icon bg-teal"><i class="fa fa-cog"></i></p> -->
            <div class="inner">
              <p>Chain Commission Earned</p>
              <h3><?php echo to_currency($final_commission_chained); ?></h3>
            </div>
            <!-- /.inner -->
             <div class="icon"> <i class="fa fa-inr"></i> </div>
          </div>
          <!-- /.small-box -->
        </div>
        <?php }?>
        <!-- /.col -->
      </div>
    <?php if($user_role_id ==  SUPERADMIN_ROLE_ID || $user_role_id == TRIMAX_ROLE_ID || $user_role_id ==  BUSINESS_ADMIN_ROLE_ID ||
        $user_role_id ==  OPERATION_ADMIN_ROLE_ID ||  $user_role_id ==  OPERATION_SUPPORT_ROLE_ID){ ?>
    <div class="box-body table-responsive" style="min-height: 0.01%;overflow-x: auto;">
        <table id="service_display" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th width="20px;">Sr No</th>
                    <th>Services</th>
                    <th>Total Transactions</th>
                    <th>Total GMV</th>
                    <th>Total Commission</th>
                </tr>
            </thead>
             <tfoot align="right">
		<tr><th></th><th></th><th></th><th></th><th></th></tr>
            </tfoot>
        </table>
    </div><!-- /.box-body -->
         <?php } ?>          
</section>
<?php }
elseif($user_role_id ==  COMPANY_ROLE_ID || $user_role_id ==  MASTER_DISTRIBUTOR_ROLE_ID || $user_role_id ==  AREA_DISTRIBUTOR_ROLE_ID 
        || $user_role_id ==  DISTRIBUTOR || $user_role_id ==  RETAILER_ROLE_ID ) {  ?>
  <section class="content">
    <div class="row">
       <!--Code added by Aparna Chalke -2018-06-29 - CR 314-->
      <form name="frmDashboard" id="frmDashboard" method="post" action="">
        <input type="hidden" name="rokad_token" value="<?php echo $this->security->get_csrf_hash();?>"> 
        <div class="col-md-3 display-no">    
            <div style="position: static;" class="form-group">
                <label for="">For</label>
                <select class="form-control" id="period" name="period">
                    <option value="today" <?php if($period == 'today' || $period == '') { ?> selected="selected" <?php } ?> >Today</option>
                    <option value="current_month" <?php if($period == 'current_month') { ?> selected="selected" <?php } ?> >Current Month</option>
                    <option value="date_range" <?php if($period == 'date_range') { ?> selected="selected" <?php } ?> >Select Date Range</option>
                </select>
                <!-- <p class="help-block">Example block-level help text here.</p> -->
            </div>
            
        </div>
        <div id="dateRangeDiv" style="<?php echo $style; ?>">
            <div class="col-md-3 display-no">    
                <div style="position: static;" class="form-group">
                    <label for="input-text-1">From Date</label>
                    <input class="form-control" id="from_date" name="from_date" placeholder="From Date" type="text" value="<?php if(isset($from_date))echo $from_date; ?>" readonly="readonly">
                </div>
           </div>
            <div class="col-md-3 display-no">  
               <div style="position: static;" class="form-group">
                    <label for="input-text-2">Till date</label>
                    <input class="form-control " id="till_date" name="till_date" placeholder="Till Date" type="text" value="<?php if(isset($to_date))echo $to_date; ?>"/ readonly="readonly">
                </div>
            </div>
            <div class="col-md-1 display-no"> 
                <label for="input-text-2" style=""></label>
                <input id type="submit" class="form-control btn btn-primary" value="Submit"/>
            </div>
        </div>
      </form>
       <!--Code added by Aparna Chalke -2018-06-29 - CR 314-->
    </div>
    <div class="row">
        <?php if($user_role_id ==  COMPANY_ROLE_ID || $user_role_id ==  MASTER_DISTRIBUTOR_ROLE_ID || $user_role_id ==  AREA_DISTRIBUTOR_ROLE_ID || $user_role_id ==  DISTRIBUTOR) { ?>
         <?php if($user_role_id ==  COMPANY_ROLE_ID || $user_role_id ==  MASTER_DISTRIBUTOR_ROLE_ID){    ?>
         <div class="col-lg-3 col-xs-6 total_transactn_amt" style="cursor: pointer;">
          <div class="small-box bg-purple">
            <!-- <span class="small-box-icon bg-purple"><i class="fa fa-archive"></i></p> -->
            <div class="inner">
              <p>Total GMV</p>
              <h3><?php echo $total_gmv; ?></h3>
            </div>
            <!-- /.inner -->
            <div class="icon"> <i class="fa fa-inr"></i> </div>
          </div>
          <!-- /.small-box -->
        </div>
         <div class="col-lg-3 col-xs-6 total_passenger">
          <div class="small-box bg-red">
           <!--  <span class="small-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span> -->
            <div class="inner">
              <p>Total Commission</p>  <h3><?=$total_commission_rokad;?></h3>
            </div>
             <div class="icon"> <i class="ion ion-bag"></i> </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6 active_agents" style="cursor: pointer;">
          <div class="small-box bg-aqua">
           <!--  <span class="small-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span> -->
            <div class="inner">
               <p>Active Agents</p>  <h3><?=$active_agents_rokad;?></h3>
            </div>
             <div class="icon"> <i class="ion ion-person-add"></i> </div>
          </div>
        </div>
       <?php } ?>
        <div class="col-lg-3 col-xs-6 total_tickets">
          <div class="small-box bg-green">
           <!--  <span class="small-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span> -->
            <div class="inner">
              <p>Ticket Booked</p>   
              <h3><?=$name['total_tickets'];?></h3>
            </div>
             <div class="icon"> <i class="ion ion-bag"></i> </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6 total_passenger" >
          <div class="small-box bg-red">
           <!--  <span class="small-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span> -->
            <div class="inner">
              <p>Total Passenger</p> 
              <h3><?=$name['total_passenger'];?></h3>
            </div>
             <div class="icon"> <i class="ion ion-bag"></i> </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6 my_network" style="cursor: pointer;">
          <div class="small-box bg-aqua">
            <!-- <span class="small-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></p> -->
            <div class="inner">
              <P><?php   echo "My Network"; //$user_role_name; ?></p>
              <h3><?php echo $name['result_c']; ?></h3>
            </div>
             <div class="icon"> <i class="ion ion-stats-bars"></i> </div>
          </div>
        </div>
        <?php } ?>
        <!-- <div class="clearfix visible-sm-block"></div>
        <div class="col-lg-3 col-xs-6 ticket_details" style="cursor: pointer;">
          <div class="small-box">
            <span class="small-box-icon bg-red"><i class="fa fa-ticket"></i></p>
            <div class="inner">
              <p>Ticket Details</p>
              <h3><?php echo $name['ticketdetails_total']; ?></p>
            </div>
          </div>
        </div> -->
        <?php if($user_role_id ==  COMPANY_ROLE_ID || $user_role_id ==  MASTER_DISTRIBUTOR_ROLE_ID || $user_role_id ==  AREA_DISTRIBUTOR_ROLE_ID || $user_role_id ==  DISTRIBUTOR) { ?>
        <div class="clearfix visible-sm-block"></div>

        <div class="col-lg-3 col-xs-6 pending_kyc" style="cursor: pointer;">
          <div class="small-box bg-green">
           <!--  <span class="small-box-icon bg-green"><i class="fa fa-file"></i></p> -->
            <div class="inner">
              <p>Pending Kyc</p>
              <h3><?php echo $name['pending_kyc_total']; ?></h3>
            </div>
            <!-- /.inner -->
             <div class="icon"> <i class="ion ion-document"></i> </div>
          </div>
          <!-- /.small-box -->
        </div>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-lg-3 col-xs-6 pending_users" style="cursor: pointer;">
          <div class="small-box bg-red">
            <!-- <span class="small-box-icon bg-red"><i class="fa fa-users"></i></p> -->
                <div class="inner">
                  <p>Pending Activation (Email & Mobile)</p>
                  <h3><?php echo $name['pending_activation_total']; ?></h3>
                </div>
            <!-- /.inner -->
            <div class="icon"> <i class="ion ion-email"></i> </div>
          </div>
          <!-- /.small-box -->
        </div>
        <?php } ?>
        <!-- <div class="clearfix visible-sm-block"></div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box">
            <span class="small-box-icon bg-maroon"><i class="fa fa-check-square"></i></p>
            <div class="inner">
              <p>Today's Total No</p>
              <p>of Transaction</p>
              <h3><?php //echo $name['']; ?></p>
            </div>
          </div>
        </div> -->
        <div class="clearfix visible-sm-block"></div>
        <div class="col-lg-3 col-xs-6 total_transactn_amt" style="cursor: pointer;">
          <div class="small-box bg-purple">
           <!--  <span class="small-box-icon bg-purple"><i class="fa fa-archive"></i></p> -->
            <div class="inner">
              <p>Total Transaction Amount</p>
              <h3><?php echo to_currency($name['total_transactn_amt']); ?></p>
            </div>
            <!-- /.inner -->
            <div class="icon"> <i class="fa fa-inr"></i> </div>
          </div>
          <!-- /.small-box -->
        </div>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-lg-3 col-xs-6 commission_per_day" style="cursor: pointer;">
          <div class="small-box bg-light-blue">
           <!--  <span class="small-box-icon bg-light-blue"><i class="fa fa-pie-chart"></i></p> -->
            <div class="inner" style="cursor: none;">
              <p>Commission Earned </p>
              <h3><?php echo to_currency($name['commission_earned_masters']) ; ?></p>
            </div>
            <!-- /.inner -->
             <div class="icon"> <i class="fa fa-inr"></i> </div>
          </div>
          <!-- /.small-box -->
        </div>
        <div class="clearfix visible-sm-block"></div>
        <?php if($user_role_id ==  COMPANY_ROLE_ID || $user_role_id ==  MASTER_DISTRIBUTOR_ROLE_ID || $user_role_id ==  AREA_DISTRIBUTOR_ROLE_ID || $user_role_id ==  DISTRIBUTOR) { ?>
        <div class="col-lg-3 col-xs-6 chain_commission">
          <div class="small-box bg-teal">
          <!--   <span class="small-box-icon bg-teal"><i class="fa fa-cog"></i></p> -->
            <div class="inner">
              <p>Chain Commission Earned</p>  
              <h3><?php echo to_currency($name['network_transactn_commission']); ?></p>
            </div>
            <!-- /.inner -->
             <div class="icon"> <i class="fa fa-inr"></i> </div>
          </div>
          <!-- /.small-box -->
        </div>
        <?php } ?>
        <!-- /.col -->
      </div>
      <?php if($user_role_id ==  COMPANY_ROLE_ID || $user_role_id ==  MASTER_DISTRIBUTOR_ROLE_ID){ ?>
    <div class="box-body table-responsive" style="min-height: 0.01%;overflow-x: auto;">
        <table id="service_display" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th width="20px;">Sr No</th>
                    <th>Services</th>
                    <th>Total Transactions</th>
                    <th>Total GMV</th>
                    <th>Total Commission</th>
                </tr>
            </thead>
           <tfoot align="right">
		<tr><th></th><th></th><th></th><th></th><th></th></tr>
            </tfoot>
        </table>
    </div><!-- /.box-body -->
         <?php } ?>   
  </section>
<?php } ?>
  <script>
    $(document).ready(function() {

        $(document).off('click', '.active_users').on('click', '.active_users', function(e)
        {
            window.location.href = BASE_URL + 'report/report/list_of_users_view';
        });
    
        $(document).off('click', '.pending_users').on('click', '.pending_users', function(e)
        {
            window.location.href = BASE_URL + 'report/report/pending_user';
        });
        $(document).off('click', '.pending_kyc').on('click', '.pending_kyc', function(e)
        {
            window.location.href = BASE_URL + 'report/report/pending_kyc';
        });
        $(document).off('click', '.ticket_details').on('click', '.ticket_details', function(e)
        {
            window.location.href = BASE_URL + 'report/report/bos_tkt_details_view';
        });

		/*code added by Aparna Chalke-2018-06-29 -CR 314*/
         $(document).off('click', '.my_network').on('click', '.my_network', function(e)
        {
            window.location.href = BASE_URL + 'report/report/list_of_users_view';
        });
         $(document).off('click', '.total_transactn_amt').on('click', '.total_transactn_amt', function(e)
        {
            window.location.href = BASE_URL + 'admin/user_wallet';
        })
        $(document).off('click', '.commission_per_day').on('click', '.commission_per_day', function(e)
        {
            window.location.href = BASE_URL + 'report/report/daily_commission_report';
        });
           /*code added by Aparna Chalke-2018-06-29 -CR 314*/
           
        $(document).off('click', '.active_agents').on('click', '.active_agents', function(e)
        {
            window.location.href = BASE_URL + 'report/report/activeUserDetails';
        });
      });

      $(document).off('change', '#period').on('change', '#period', function(e) {
          if($(this).val() != 'date_range'){
              $( "#frmDashboard" ).submit();
          }else{
              $('#dateRangeDiv').show();
          }
      });

    if($( "#from_date" ).length > 0){
      $( "#from_date" ).datepicker({
          'dateFormat' : 'yy-mm-dd',
          'maxDate': new Date(),
          'changeMonth': true,
           'changeYear': true
         /* 'minDate' : -30,
          'maxDate' : 0*/
      }).on("change", function () {
           $('#till_date').datepicker('option', 'minDate', $("#from_date").val());
      });
    }

    if($( "#till_date" ).length > 0){
        $( "#till_date" ).datepicker({
            'dateFormat' : 'yy-mm-dd',
            'maxDate': new Date(),
            'changeMonth': true,
           'changeYear': true
           /* 'minDate' : -30,
            'maxDate' : 0*/
        }).on("change", function () {
            $('#from_date').datepicker('option', 'maxDate', $("#till_date").val());
        });
    }
  
</script>

   <script>
  <?php if($user_role_id ==  SUPERADMIN_ROLE_ID || $user_role_id ==  COMPANY_ROLE_ID || $user_role_id ==  TRIMAX_ROLE_ID || $user_role_id ==  MASTER_DISTRIBUTOR_ROLE_ID){ ?>
    $(document).ready(function() {
        var daterange = $('#period').val();
        
        var d = new Date();
        var yy = d.getFullYear(); //year 2019
        var mm = (d.getMonth())+1; //0=January, 1=February etc.
        var dd = d.getDate(); //
        
        if(daterange == 'today'){
            var from_date = yy +'-'+mm+'-'+dd;
            var to_date = yy +'-'+mm+'-'+dd;
        }else if(daterange == 'current_month'){
            var from_date = yy +'-'+mm+'-'+'01';
            var to_date = yy +'-'+mm+'-'+'30';
        }else if(daterange == 'date_range'){
            from_date
            var from_date = $('#from_date').val();
            var to_date = $('#till_date').val();
        }
        
        var tconfig = {
            "destroy": true,
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": BASE_URL + "admin/dashboard/get_list",
                "type": "POST",
                "data": "json",
                data  : function(d) {
                    d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;
                    d.from = from_date;
                    d.to = to_date
                }
            },
            "iDisplayLength": 10,
            "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
            "paging": false,
            "searching": true,
            "bFilter": false,
            "aoColumnDefs": [
             {"bSearchable": false, "bSortable": false, "aTargets": [0]},
            {"bSearchable": false, "bSortable": false, "aTargets": [2]}
              
            ],
            "order": [[0, 'desc']],
            "oLanguage": {
                "sProcessing": '<center><img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/></center>'
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
              //  $("td:first", nRow).html(iDisplayIndex + 1);
               var info = $(this).DataTable().page.info();
                $("td:first", nRow).html(info.start + iDisplayIndex + 1);
                return nRow;
            },
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // computing column Total the complete result 
                var tot_trans = api
                    .column( 2 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                var tot_gmv = api
                    .column( 3 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                var tot_comm = api
                    .column(4)
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                  
                    
                // Update footer by showing the total with the reference of the column index 
                $( api.column( 1 ).footer() ).html('Total');
                $( api.column( 2 ).footer() ).html(tot_trans);
                $( api.column( 3 ).footer() ).html(tot_gmv.toFixed(2));
                $( api.column( 4 ).footer() ).html(tot_comm.toFixed(2));
            }
        };

        var oTable = $('#service_display').dataTable(tconfig);
        
    });

   <?php } ?>
</script>
