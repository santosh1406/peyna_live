<section class="content">
    <div class="row">
        <h3 class="box-title">&nbsp;&nbsp;&nbsp;View Commission</h3>
        <?php if ($this->session->flashdata('error')) { ?>
            <div class="alert alert-success alert-dismissable">
                <b>
                    <?php
                    echo $this->session->flashdata('error');
                    ?></b>
                <i class="fa fa-check"></i>
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            </div>
        <?php } ?>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="bs-example">
                        <div class="alert alert-danger alert-error" style="display:none">
                            <strong>Error!</strong> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="commission name">Commission Name</label>
                        <div class="col-lg-6">
                            <input name="name" type="text" id="name" value="<?php echo $commission_name; ?>" class="form-control" placeholder="Commission Name" readonly>
                        </div>
                    </div>
                    
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="commission_with_service_tax">Commission With Service Tax</label>
                        <div class="col-lg-6">
                            <input name="commission_with_service_tax" type="text" id="commission_with_service_tax" value="<?php echo (($commission_with_service_tax == '1') ? 'Yes' : 'No'); ?>" class="form-control" placeholder="Commission With Service Tax" readonly>

                            <span id="name_msg" class="err"></span>
                        </div>
                    </div>
                     <?php if ($commission_with_service_tax == '1') { ?>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="service_tax">Service Tax (%)</label>
                        <div class="col-lg-6">
                            <input name="service_tax" type="text" id="service_tax" value="<?php echo $service_tax; ?>" class="form-control" placeholder="Service Tax" readonly>
                            <span id="name_msg" class="err"></span>
                        </div>
                    </div>
                     <?php } ?>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group" id = "default_template">
                        <label class="col-lg-3 control-label" for="default_template">Default Template</label>
                        <div class="col-lg-6">
                            <input name="default_template" type="text" id="default_ttyemplate" value="<?php echo (($default_template == '1') ? 'Yes' : 'No'); ?>" class="form-control" placeholder="Default Template" readonly>

                            <span id="name_msg" class="err"></span>
                        </div>
                    </div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                  
                    <?php if ($default_template == '1') { ?>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="commission from">From - Till</label>
                            <div class="col-lg-6">
                                <input name="from_till" type="text" id="from_till" value="<?php echo $commission_from . ' - ' . $commission_till; ?>" class="form-control" placeholder="Commission From and Till" readonly>
                                <span id="from_msg" class="err"></span>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <input name="from" type="hidden" id="from" value="<?php echo $commission_from; ?>" class="form-control datepicker" placeholder="Commission From">
                            <span id="from_msg" class="err"></span>
                        </div>
                    </div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <input name="till" type="hidden" id="till" value="<?php echo $commission_till; ?>" class="form-control datepicker" placeholder="Commission Till">
                            <span id="till_msg" class="err"></span>
                        </div>
                    </div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <input name="commission_id" type="hidden" id="commission_id" value="<?php echo $commission_id; ?>" class="form-control" placeholder="">
                            <span id="commission_id" class="err"></span>
                        </div>
                    </div>

                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="table-responsive">          
                        <table class="table table-bordered" id="provider-operator-commission">
                            <thead>
                                <tr><th colspan="13" style="text-align:center" class="bg-gray disabled color-palette">
                                        <?php
                                        if ($operator_provider_name > 0) {
                                            echo strtoupper($operator_provider_name[$i]);
                                        }
                                        ?>
                                    </th></tr>

                                <tr>
                                    <th>#</th>
                                    <th style="text-align:center">Provider</th>
                                    <th style="text-align:center">Operator</th>
                                    <th style="text-align:center">Bus Type</th>
                                    <th style="text-align:center">Base Rate Flag</th>
                                    <th style="text-align:center">Base Rate (%)</th>
                                    <th style="text-align:center">Base Rate Fixed</th>
                                    <th style="text-align:center;">Rokad Commission(%)</th>
                                    <th style="text-align:center">MD Commission(%)</th>
                                    <th style="text-align:center">AD Commission(%)</th>
                                    <th style="text-align:center">Distributor Commission(%)</th>
                                    <th style="text-align:center">Retailer Commission(%)</th>
                                </tr>

                            </thead>
                            <tbody> 
                                <?php $counter = 0; ?>    
                                <?php
                                foreach ($details_provider_operator as $dpo_name => $provider_operator_details) {
                                    ?>
                                    <?php
                                    foreach ($provider_operator_details as $pod_key => $pod) {
                                        $current_op_name = strtolower(trim($dpo_name));
                                        $pr_op_id = $pod->provider_operator_id;

                                        if ($current_op_name == 'bookonspot') {
                                            ?>

                                            <tr class="<?php echo $current_op_name; ?>">
                                                <td><?php echo $counter + 1 ?></td>
                                                <td><?php echo $pod->provider_name; ?></td>
                                                <td><?php echo $pod->provider_operator_name; ?></td>
                                                <td><?php echo $pod->bus_type; ?></td>
                                                <td>
                                                    <?php
                                                    $base_flag = $pod->base_rate_flag;
                                                    $base_flag_value = "";
                                                    if ($base_flag == 1) {
                                                        $base_flag_value = "Yes";
                                                    } else {
                                                        $base_flag_value = "No";
                                                    }
                                                    echo $base_flag_value;
                                                    ?>

                                                </td>
                                                <td><?php echo $pod->base_perc; ?></td>
                                                <td><?php echo $pod->base_fixed; ?></td>
                                                <td><?php echo $pod->rookad_commission; ?></td>
                                                <td><?php echo $pod->md_commission; ?></td>
                                                <td><?php echo $pod->ad_commission; ?></td>
                                                <td><?php echo $pod->dist_commission; ?></td>
                                                <td><?php echo $pod->retailer_commission; ?></td>
                                            </tr>

                                            <?php
                                        } else {
                                            ?>
                                            <tr  class="<?php echo $current_op_name; ?>" >
                                                <td><?php echo $counter + 1 ?></td>
                                                <td><?php echo $pod->provider_name; ?></td>
                                                <td><?php echo $pod->operator_name; ?></td>
                                                <td><?php echo $pod->bus_type; ?></td>
                                                <td>
                                                    <?php
                                                    $base_flag = $pod->base_flag;
                                                    $base_flag_value = "";
                                                    if ($base_flag == 1) {
                                                        $base_flag_value = "Yes";
                                                    } else {
                                                        $base_flag_value = "No";
                                                    }
                                                    echo $base_flag_value;
                                                    ?>

                                                </td>    
                                                <td><?php echo $pod->base_perc; ?></td>
                                                <td><?php echo $pod->base_fixed; ?></td>
                                                <td><?php echo $pod->rookad_commission; ?></td>
                                                <td><?php echo $pod->md_commission; ?></td>
                                                <td><?php echo $pod->ad_commission; ?></td>
                                                <td><?php echo $pod->dist_commission; ?></td>
                                                <td><?php echo $pod->retailer_commission; ?></td>
                                            </tr>
                                            <?php
                                        }
                                        $counter++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-4">
                            <a class="btn btn-primary" href="<?php echo base_url('admin/commission'); ?>" type="button">Back</a> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


