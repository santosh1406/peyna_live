<section class="content">
    <div class="row">
        <!-- right column -->
        <div class="col-md-12">
            <!-- general form elements disabled -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Commission</h3>
                </div><!-- /.box-header -->
                <?php
            		if($this->session->flashdata('msg')){ 
            	?>
                <div id="msg_block" class="col-md-12 error_block <?php if(!empty($error)){ echo 'show';} else{ echo "show";} ?>">
                    <div class="alert alert-success alert-dismissable" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?php echo '<b>Alert! </b>' . $this->session->flashdata('msg');  ?>
                    </div>
                </div>
                <?php } ?>
                <div class="box-body">
                	<div class="row" style=" margin-bottom:10px;">
	                	<div class="col-md-2">
	                		<a href="<?php echo base_url().'admin/commission/redirect_new_commission'; ?>" class="btn btn-warning " role="button">Create New Commission</a>
	                	</div>
	                </div>
                	<table id="listdata" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="20px">Id</th>
                            <th width="">Name</th>
                            <!-- <th width="">From</th> -->
                            <!-- <th width="">Till</th> -->
                            <th width="">Created Date</th>
                            <th width="">Default Status</th>
                            <!-- <th width="">Commission Flag</th> -->
                            <th width="">Action</th>
                        </tr>
                    </thead>
                    <tbody id="listdata">                                           
                    </tbody>
                    </table>
				    <div class="clearfix"></div>                    
                </div><!-- /.box-body -->
                <div class="box-footer">&nbsp;</div>
            </div> <!-- /.box -->
        </div> <!--/.col (right) -->
    </div> <!-- /.row -->
</section>
<link href="<?php echo base_url().COMMON_ASSETS.'plugins/colorbox/example3/colorbox.css' ?>" rel="stylesheet" />
<script src="<?php echo base_url().COMMON_ASSETS.'plugins/colorbox/jquery.colorbox.js' ?>"></script>
<script type="text/javascript">
    var img = '<center><img src="<?php echo base_url().BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/></center>';
</script>
<script type="text/Javascript" src="<?php echo base_url(); ?>js/back/commission/commission.js" ></script>