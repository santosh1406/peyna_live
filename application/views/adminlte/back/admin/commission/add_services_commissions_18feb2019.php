<!-- Created by harshada kulkarni on 06-09-2018 -->
<section class="content">
    <div class="row">
        <!-- right column -->
        <div class="col-md-12">
            <!-- general form elements disabled -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Add Services Commission</h3>
                </div><!-- /.box-header -->
                <?php
                echo validation_errors('<div class="error">', '</div>');
                if ($this->session->flashdata('message')) {
                    ?>
                    <div class="alert alert-success alert-dismissable" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?php echo $this->session->flashdata('message'); ?>
                    </div>
                    <?php
                }
                $attributes = array("method" => "POST", "name" => "add_services_commissions", "id" => "add_services_commissions");
                echo form_open(base_url() . 'admin/commission/save_services_commissions', $attributes);
                ?>	
                <input type="hidden" name="service_commission_field_id" id="service_commission_field_id" value="<?php echo $serviceCommissionFields[0]->id; ?>" />
                <input type="hidden" name="service_id" id="service_id" value="<?php echo $serviceCommissionFields[0]->service_id; ?>" />
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="commission name">Commission Name</label>
                    <div class="col-lg-6">
                        <input name="name" type="text" id="name" class="form-control" placeholder="Commission Name" autocomplete="off">
                        <span id="name_msg" class="err"></span>
                    </div>
                </div>                
                <div class="clearfix" style="height: 10px;clear: both;"></div> 
                <div class="form-group" style="display: block;">
                    <label class="col-lg-3 control-label" for="commission name">Default commission</label>
                    <div class="col-lg-6">
                        <select class="form-control" name="default_commission" id="default_commission" aria-invalid="false" autocomplete="off">
                            <option value="N" selected="">No</option>
                            <option value="Y">Yes</option>
                        </select>
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>
                <div class="form-group from" style="display:none;">
                    <label class="col-lg-3 control-label" for="from date">From Date</label>
                    <div class="col-lg-6">
                        <input class="form-control " id="from" name="from" type="text" autocomplete="off" value ="<?php echo date('d-m-Y'); ?>">
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>
                <div class="form-group till" style="display:none;">
                    <label class="col-lg-3 control-label" for="till date">Till Date</label>
                    <div class="col-lg-6">
                        <input class="form-control " id="till" name="till" type="text" autocomplete="off" value ="<?php echo date('d-m-Y'); ?>">
                    </div>
                </div> 
                <div class="clearfix" style="height: 10px;clear: both;"></div> 
                <?php if (!empty($serviceCommissionFields)) { ?>
                    <div class="table-responsive" style="min-height: 0.01%;overflow-x: auto;">          
                        <table class="table table-bordered" id="provider-operator-commission" style="min-height: 0.01%;overflow-x: auto;">
                            <thead>
                                <tr class="bg-light-blue-active color-palette">
                                    <?php
                                    $value_array = json_decode($serviceCommissionFields[0]->values);
                                    foreach ($value_array as $value) {
                                        echo "<th>" . $value . "</th>";
                                    }
                                    ?>
                                </tr>
                                <tr>
                                    <?php
                                    $value_array = json_decode($serviceCommissionFields[0]->values);
                                    foreach ($value_array as $value) {
                                        $arr = explode(' ', trim($value));
                                        echo "<td><input type='number' min='0' step= '1' style='width:70px;' name='" . $arr[0] . "' class='" . $arr[0] . "' value='0' /></td>";
                                    }
                                    ?>
                                </tr>
                            </thead>
                            <tbody> 
                                <tr></tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-4">
                            <center><img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image" style="display:none;"/></center>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-4">
                            <button class="btn btn-primary" id="save_services_commissions" name="save_services_commissions" type="submit">Save</button> 
                            <a class="btn btn-primary" href="<?php echo base_url('admin/commission/load_services_commissions'); ?>" type="button">Back</a> 
                        </div>
                    </div>
                    <?php
                } else {
                    echo "Please add commission template first for this service.";
                }
                form_close();
                ?>
            </div>
        </div> <!-- /.box -->
    </div> <!--/.col (right) -->
</section>

<script>

    $('#add_services_commissions').validate({
        rules: {
            name: {
                required: true,
                alphanumericonly: true,
                package_exists: true
            },
            default_commission: {
                required: true
            },
            from: {
                required: true,
            },
            till: {
                required: true,
                enddate: true
            }
        },
        messages: {
            name: {
                required: "Please enter package name",
                alphanumericonly: "Please enter valid package name"
            },
            default_package: {
                required: "Please select default package",
            },
            from: {
                required: "Please select form date",
            },
            till: {
                required: "Please select till date",
                enddate: "Till date must be greater than from date"
            }
        },
        submitHandler: function(form) {
            $('.img-circle').show();
            $('#save_services_commissions').html('Processing....');
            $("#save_services_commissions").attr("disabled", "disabled");
            return true;
        }
    });

    $.validator.addMethod("enddate", function(value, element) {

        var startDate = $('#from').val();
        var newstartdate = startDate.split("-").reverse().join("-");

        var endDate = value;
        var newenddate = endDate.split("-").reverse().join("-");

        newstartdate = new Date(newstartdate);
        newenddate = new Date(newenddate);
        if (newenddate < newstartdate) {
            return false;

        } else {
            return true;
        }
    }, "Till Date must be greater than from Date.");

    // load datepicker
    $("#from").datepicker({
        dateFormat: 'dd-mm-yy', minDate: new Date(),
        onSelect: function() {
            var selected = $(this).val();
            $("#from").attr("value", "");
            $("#from").attr("value", selected);
        }
    });
    $("#till").datepicker({
        dateFormat: 'dd-mm-yy', minDate: new Date(),
        onSelect: function() {
            var selected = $(this).val();
            $("#till").attr("value", "");
            $("#till").attr("value", selected);
        }
    });
    $(".from").show();
    $(".till").show();
    // on change of default package value date show hide code start here 
    $(document).off('change', '#default_commission').on('change', '#default_commission', function() {
        if (this.value == 'Y') {
            $(".from").hide();
            $(".till").hide();
            $("#from").val("");
            $("#till").val("");
        } else {
            $(".from").show();
            $(".till").show();
        }

        $("#save_services_commissions").on("click", function(e) {
            var inputs = $(".dynamic_field_value");
            var val = 0;
            for (var i = 0; i < inputs.length; i++) {
                val += parseInt($(inputs[i]).val());
            }
            if ((val < 100) || (val > 100)) {
                alert("Field value addition should be equal to 100");
                e.preventDefault();
            }
        });
    });
</script>
