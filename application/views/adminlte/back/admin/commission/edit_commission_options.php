<!-- Created by harshada kulkarni on 04-09-2018 -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> 
                        <h3 class="box-title">Commission Options</h3>
                    </div>
                    <div class="box-body">
                        <?php
                        echo validation_errors('<div class="error">', '</div>');
                        if ($this->session->flashdata('message')) {
                            echo '<b>Success!</b> ' . $this->session->flashdata('message');
                        }
                        ?>
                        <?php
                        $attributes = array("method" => "POST", "id" => "commission_options", "name" => "commission_options");
                        echo form_open(base_url() . 'admin/commission/update_commission_options', $attributes);
                        ?>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group intent"> 
                            <label class="col-lg-3 control-label" for="service_name">Select Services *:</label>
                            <div class="col-lg-6">                                    
                                <input type="hidden" name="service_id" id="service_id" value="<?php echo $commissionOptions[0]->id; ?>"/> 
                                <input type="hidden" id="hidSelectedOptions" value="<?php echo $commissionValues; ?>" />
                                <input type="text" name="service_name" id="service_name" value="<?php echo $commissionOptions[0]->service_name; ?>" readonly="true" class="form-control"/>                           
                            </div> 
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group intent"> 
                            <label class="col-lg-3 control-label" for="fields">Fields *:</label>
                            <div class="col-lg-6">
                                <select id="dynamic_fields" name="dynamic_fields[]" multiple class="form-control">
                                    <?php
                                    $selectedFields = json_decode($commissionOptions[0]->values);
                                    if (is_array($dynamicFields)) {
                                        $checked = "";
                                        foreach ($dynamicFields as $df) {
                                            foreach ($selectedFields as $key => $value) {
                                                if ($df->field == $value) {
                                                    $checked = "selected=selected";
                                                    break;
                                                } else {
                                                    $checked = "";
                                                }
                                            }
                                            echo '<option value="' . $df->field . '" ' . $checked . '>' . $df->field . '</option>';
                                        }
                                    }
                                    ?> 
                                </select>
                            </div> 
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">
                                <button class="btn btn-primary" id="save_values" name="save_values" type="submit">Save</button>  
                                <button class="btn btn-primary back" id="back_data" type="button">Back</button> 
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<style>
    .multiselect-item.filter {
        width: 515px;
    }
</style>
<script type="text/javascript">

    $('#commission_options').bootstrapValidator({
        group: '.form-control',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            service_name: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b>Please select service.</font>'
                    }
                }
            },
            dynamic_fields: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b>Please select atleast one field.</font>'
                    }
                }
            }
        }
    });

    $(document).ready(function() {

        $(document).off('click', '.back').on('click', '.back', function(e) {
            window.location.href = BASE_URL + 'admin/commission/commission_options';
        });

        $('#dynamic_fields').multiselect({
            nonSelectedText: 'Select Fields',
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '515px',
            maxHeight: 200,
            selectedText: "# of # selected"
        });
    });
</script>