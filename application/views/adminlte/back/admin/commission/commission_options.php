<!-- Created by harshada kulkarni on 03-09-2018 -->
<section class="content">
    <div id="menu_wrapper">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Commission Options</h3>
                    </div>
                    <?php
                    echo validation_errors('<div class="error">', '</div>');
                    if ($this->session->flashdata('message')) {
                        echo '<b>Success!</b> ' . $this->session->flashdata('message');
                    }
                    ?>
                    <div align="left">
                        <a href="<?php echo base_url() . 'admin/commission/add_commission_options' ?>" class="btn btn-warning create_btn" role="button" style="margin-top: 5px;">Add Commission Options</a>   
                    </div>                    
                    <div class="box-body table-responsive" style="min-height: 0.01%;overflow-x: auto;">
                        <table id="commission_options_table" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="20px;">Sr No</th>
                                    <th>Service Name</th>
                                    <th>Values</th>
                                    <th width="189px;">Action</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </div>    
</section>
<script>
    $(document).ready(function() {
        var tconfig = {
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": BASE_URL + "admin/commission/commission_list",
                "type": "POST",
                "data": "json",
                data  : function(d) {
                    d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;
                }
            },
            "iDisplayLength": 10,
            "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
            "paging": true,
            "searching": true,
            "aoColumnDefs": [
                {"bSearchable": false, "aTargets": [2]}
            ],
            "order": [[0, 'desc']],
            "oLanguage": {
                "sProcessing": '<center><img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/></center>'
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                $("td:first", nRow).html(iDisplayIndex + 1);
            },
        };
        var oTable = $('#commission_options_table').dataTable(tconfig);
    });

    $(document).off('click', '.edit_btn').on('click', '.edit_btn', function(e) {
        e.preventDefault();
        var id = $(this).attr('ref');
        var base_url = BASE_URL + "admin/commission/edit_commission_options/" + id;
        window.location.href = base_url;
    });
</script>
