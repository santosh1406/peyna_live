    <div class="row">
        <h3 class="box-title">&nbsp;&nbsp;&nbsp;Commission Details</h3>
        <div class="clearfix" style="height: 10px;clear: both;"></div>
        <?php if ($this->session->flashdata('error')) { ?>
            <div class="alert alert-success alert-dismissable">
                <b>
                    <?php
                    echo $this->session->flashdata('error');
                    ?></b>
                <i class="fa fa-check"></i>
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            </div>
        <?php } ?>
        <div class="clearfix" style="height: 10px;clear: both;"></div>
        <div class="col-md-12">
            <?php 
            $attributes1 = array('class' => 'form', 'id' => 'save_from', 'name'=>'save_from','method' => 'post');
            echo form_open('admin/commission/my_commission', $attributes1);
            ?> 
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">Select Service</label>
                    <div class="col-md-9">
                        <select class="form-control" id="service_id" name="service_id" onchange="submitForm();">
                            <?php foreach ($service_master as $key => $value) { ?> 
                                <option value="<?php echo $value['id']; ?>" <?php if($value['id'] == $service_id){ echo 'selected'; } ?>><?php  echo $value['service_name']?></option>
                            <?php } ?> 
                        </select>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
        <div class="clearfix" style="height: 10px;clear: both;"></div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="bs-example">
                        <div class="alert alert-danger alert-error" style="display:none">
                            <strong>Error!</strong> 
                        </div>
                    </div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="table-responsive">          
                        <table class="table table-bordered" id="provider-operator-commission">
                            <thead>
                                <tr>
                                    <th colspan="13" style="text-align:center" class="bg-gray disabled color-palette"></th>
                                </tr>

                                <tr>
                                    <th>Sr. No.</th>
                                    <th style="text-align:center">Operator</th>
                                    <th style="text-align:center">Default Status</th>
                                    <th style="text-align:center">Commission Name</th>
                                    <th style="text-align:center">My Commission(%)</th>
                                </tr>

                            </thead>
                            <tbody>
                            <?php  ?> 

                            <?php if(!empty($commission_data) && $service_id == 2) {
                                    foreach ($commission_data as $key => $value) { ?> 
                                    <tr class="">
                                        <td style="text-align:center"><?php echo ++$key;?></td>
                                        <td style="text-align:center">MSRTC</td>
                                        <td style="text-align:center"><?php echo ($value['commission_default_flag'] == '0' ? 'No' : 'Yes');?></td>
                                        <td style="text-align:center"><?php echo $value['commission_name'];?></td>
                                        <td style="text-align:center"><?php echo round($value['commission_percentage'],0); ?>%</td>
                                    </tr>
                           <?php } } else { ?>
                                    <tr><td colspan="5" style="text-align:left">No data available</td></tr>
                           <?php } ?>                   
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-4">
                            <a class="btn btn-primary" href="<?php echo base_url('admin/dashboard'); ?>" type="button">Back</a> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 

<script type="text/javascript">
    $(document).ready(function (e) {
        $("#from_date" ).datepicker({
            'dateFormat' : 'dd-mm-yy',
            'maxDate':'0'
            }).datepicker("setDate", "0");
    });
    
    function submitForm(){
        $('#save_from').submit()
    }
</script>

