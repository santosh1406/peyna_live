<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script type="text/Javascript" src="<?php echo base_url(); ?>js/back/commission/commission.js" ></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<style>
    .shadow_red{border: 1px solid red !important;box-shadow: 0px 0px 19px -7px red;}
    .inner-table{margin: -9px !important; padding: 0;}
    .shadow_blue{border: 1px solid blue !important;box-shadow: 0px 0px 19px -7px blue;}
</style>
<section class="content">
    <div class="row">
        <!-- right column -->
        <div class="col-md-12">
            <!-- general form elements disabled -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Add Commission</h3>
                </div><!-- /.box-header -->
                <?php if ($this->session->flashdata('msg')) { ?>
                    <div class="alert alert-success alert-dismissable">
                        <b>
                            <?php
                            $data = $this->session->flashdata('msg');
                            foreach ($data as $key => $value) {
                                echo $value . "<br>";
                            }
                            ?>
                        </b>
                        <i class="fa fa-check"></i>
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                    </div>
                <?php } ?>                
                <?php
				$attributes = array("method" => "POST", "name" => "create_commission_form", "id" => "create_commission_form");
				echo form_open(site_url().'/admin/commission/store', $attributes);
				?>				 
					<div class="form-group">
                        <label class="col-lg-3 control-label" for="commission name">Commission Name</label>
                        <div class="col-lg-6">
                            <input name="name" type="text" id="name" class="form-control" placeholder="Commission Name" autocomplete="off">
                            <span id="name_msg" class="err"></span>
                        </div>
                    </div>

                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="commission name">Commission With Service Tax</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="commission_with_service_tax" id="commission_with_service_tax" aria-invalid="false" autocomplete="off">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                            <span id="" class="err"></span>
                        </div>
                    </div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group" id="service_tax_hide_show">
                        <label class="col-lg-3 control-label" for="commission name">Service Tax (%)</label>
                        <div class="col-lg-6">
                            <input name="service_tax" type="number" min="0" step= "0.01" id="service_tax" value="" class="form-control numbersOnly" placeholder="Service Tax" autocomplete="off">
                            <span id="name_msg" class="err"></span>
                        </div>
                    </div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="commission name">Default Template</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="default_template" id="default_template" aria-invalid="false" autocomplete="off">
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                            <span id="" class="err"></span>
                        </div>
                    </div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group" style="display: block;" id = "show_hide_date">
                        <label class="col-lg-3 control-label" for="commission from">From - Till</label>
                        <div class="col-lg-6">
                            <input name="from_till" type="text" id="from_till" class="form-control" placeholder="Commission From and Till" autocomplete="off">
                        </div>
                    </div>

                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <!-- end --->			
                    <div class="form-group">
                        <div class="col-lg-6">
                            <input name="from" type="hidden" id="from" value="" class="form-control datepicker" placeholder="Commission From">

                        </div>
                    </div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group">
                        <!-- <label class="col-lg-3 control-label" for="commission till">Till</label> -->
                        <div class="col-lg-6">
                            <input name="till" type="hidden" id="till" value="" class="form-control datepicker" placeholder="Commission Till">
                        </div>
                    </div>



                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="table-responsive" style="min-height: 0.01%;overflow-x: auto;">          
                        <table class="table table-bordered" id="provider-operator-commission" style="min-height: 0.01%;overflow-x: auto;">
                            <thead>
                                <tr class="bg-light-blue-active color-palette">
                                    <th>#</th>
                                    <th>Provider</th>
                                    <th>Operator</th>
                                    <th>Bus Type</th>
                                    <th>Base Rate Flag</th>
                                    <th>Base Rate(%)</th>
                                    <th>Base Rate(Fixed)</th>
                                    <th>Rokad Commission(%)</th>
                                    <th>MD Commission(%)</th>
                                    <th>AD Commission(%)</th>
                                    <th>Distributor Commission(%)</th>
                                    <th>Retailer Commission(%)</th>
                                </tr>
                            </thead>
                            <tbody> 
                                <?php $counter = 0; ?>    
                                <?php
                                foreach ($details_provider_operator as $dpo_name => $provider_operator_details) {
                                    ?>
                                <input type="hidden" name="api_provider_name[]" value="<?php echo strtolower(trim($dpo_name)); ?>">
                                <?php
                                foreach ($provider_operator_details as $pod_key => $pod) {
                                    $current_op_name = strtolower(trim($dpo_name));
                                    $pr_op_id = $pod->provider_operator_id;

                                    if ($current_op_name == 'bookonspot') {
                                        ?>

                                        <tr class="<?php echo $current_op_name; ?>">
                                            <td>
                                                <input type="hidden" name="<?php echo $current_op_name; ?>[operator_id][]" value="<?php echo $pod->operator_id; ?>">
                                                <?php echo $counter + 1 ?><input type="hidden" name="details[<?php echo $current_op_name; ?>][<?php echo $pod->operator_id; ?>][<?php echo $pod->bus_type; ?>][base_commission_id]" value="<?php echo $pod->base_commission_id; ?>"><input type="hidden" name="details[<?php echo $current_op_name; ?>][<?php echo $pod->operator_id; ?>][<?php echo $pod->bus_type; ?>][operator_id]" value="<?php echo $pod->operator_id; ?>">
                                            </td>
                                            <td>
                                                <?php echo $pod->provider_name; ?><input type="hidden" name="details[<?php echo $current_op_name; ?>][<?php echo $pod->operator_id; ?>][<?php echo $pod->bus_type; ?>][provider_id]" value="<?php echo $pod->provider_id; ?>">
                                            </td>
                                            <td>
                                                <?php echo $pod->operator_name; ?><input type="hidden" name="details[<?php echo $current_op_name; ?>][<?php echo $pod->operator_id; ?>][<?php echo $pod->bus_type; ?>][provider_operator_id]" value="<?php echo $pod->provider_operator_id; ?>">

                                            </td>
                                            <td><?php echo $pod->bus_type; ?></td>
                                            <td>
                                                <?php
                                                $base_flag = $pod->base_flag;
                                                $base_flag_value = "";
                                                if ($base_flag == 1) {
                                                    $base_flag_value = "Yes";
                                                } else {
                                                    $base_flag_value = "No";
                                                }
                                                echo $base_flag_value;
                                                ?>
                                                <input type="hidden" name="details[<?php echo $current_op_name; ?>][<?php echo $pod->operator_id; ?>][<?php echo $pod->bus_type; ?>][base_flag_value]" value="<?php echo $base_flag; ?>">

                                            </td>
                                            <td>
                                                <input type="hidden" class="percentage_base" id="percentage_base" name="details[<?php echo $current_op_name; ?>][<?php echo $pod->operator_id; ?>][<?php echo $pod->bus_type; ?>][percentage_base]" value="<?php echo $pod->percentage_value; ?>" >
                                                <?php
                                                $base_perc_value = "";
                                                $base_perc = $pod->percentage_value;

                                                if (empty($base_perc) || $base_perc == "" || $base_flag_value == "No") {
                                                    $base_perc_value = "0";
                                                } else {
                                                    $base_perc_value = $base_perc;
                                                }
                                                echo $base_perc_value;
                                                ?>	
                                            </td>
                                            <td>
                                                <input type="hidden" class="fixed_value" id="fixed_value" name="details[<?php echo $current_op_name; ?>][<?php echo $pod->operator_id; ?>][<?php echo $pod->bus_type; ?>][fixed_value]" value="<?php echo $pod->fixed_value; ?>" >
                                                <?php
                                                $fixed_value = "";
                                                $fixed_base_value = $pod->fixed_value;
                                                if (empty($fixed_base_value) || $fixed_base_value == "" || $base_flag_value == "No") {
                                                    $fixed_value = "0";
                                                } else {
                                                    $fixed_value = $fixed_base_value;
                                                }
                                                echo $fixed_value;
                                                ?>
                                            </td>
                                            <td><input type="number" min="0" step= "0.01" style="width:70px;" name="details[<?php echo $current_op_name; ?>][<?php echo $pod->operator_id; ?>][<?php echo $pod->bus_type; ?>][pos_comm]" id="" class="pos_commission" value='0'/></td>
                                            <td><input type="number" min="0" step= "0.01" style="width:70px;" name="details[<?php echo $current_op_name; ?>][<?php echo $pod->operator_id; ?>][<?php echo $pod->bus_type; ?>][md_comm]" id="" class="md_commission" value='0'/></td>
                                            <td><input type="number" min="0" step= "0.01" style="width:70px;" name="details[<?php echo $current_op_name; ?>][<?php echo $pod->operator_id; ?>][<?php echo $pod->bus_type; ?>][ad_comm]" id="" class="ad_commission" value='0'/></td>
                                            <td><input type="number" min="0" step= "0.01" style="width:70px;" name="details[<?php echo $current_op_name; ?>][<?php echo $pod->operator_id; ?>][<?php echo $pod->bus_type; ?>][dist_comm]" id="" class="dist_commission" value='0'/></td>
                                            <td><input type="number" min="0" step= "0.01" style="width:70px;" name="details[<?php echo $current_op_name; ?>][<?php echo $pod->operator_id; ?>][<?php echo $pod->bus_type; ?>][retailer_comm]" id="" class="retailer_commission" value='0'/></td>
                                        </tr>

                                        <?php
                                    } else {
                                        ?>
                                        <tr  class="<?php echo $current_op_name; ?>" >
                                            <td>
                                                <?php echo $counter + 1 ?><input type="hidden" name="details[<?php echo $current_op_name; ?>][0][<?php echo $pod->bus_type; ?>][base_commission_id]" value="<?php echo $pod->base_commission_id; ?>"><input type="hidden" name="details[<?php echo $current_op_name; ?>][0][<?php echo $pod->bus_type; ?>][operator_id]" value="0">
                                            </td>
                                            <td>
                                                <?php echo $pod->provider_name; ?><input type="hidden" name="details[<?php echo $current_op_name; ?>][0][<?php echo $pod->bus_type; ?>][provider_id]" value="<?php echo $pod->provider_id; ?>">
                                            </td>
                                            <td>
                                                <?php echo $pod->operator_name; ?><input type="hidden" name="details[<?php echo $current_op_name; ?>][0][<?php echo $pod->bus_type; ?>][provider_operator_id]" value="<?php echo $pod->provider_operator_id; ?>">

                                            </td>
                                            <td><?php echo $pod->bus_type; ?></td>
                                            <td>
                                                <?php
                                                $base_flag = $pod->base_flag;
                                                $base_flag_value = "";
                                                if ($base_flag == 1) {
                                                    $base_flag_value = "Yes";
                                                } else {
                                                    $base_flag_value = "No";
                                                }
                                                echo $base_flag_value;
                                                ?>
                                                <input type="hidden" name="details[<?php echo $current_op_name; ?>][0][<?php echo $pod->bus_type; ?>][base_flag_value]" value="<?php echo $base_flag; ?>">
                                                <input type="hidden" name="details[<?php echo $current_op_name; ?>][0][<?php echo $pod->bus_type; ?>][percentage_base]" value="0">
                                                <input type="hidden" name="details[<?php echo $current_op_name; ?>][0][<?php echo $pod->bus_type; ?>][fixed base]" value="0">
                                            </td>    
                                            <td>0.00</td>
                                            <td>0.00</td>
                                            <td><input type="number" min="0" step= "0.01" style="width:70px;" name="details[<?php echo $current_op_name; ?>][0][<?php echo $pod->bus_type; ?>][pos_comm]" id="" class="pos_commission" value='0'/></td>
                                            <td><input type="number" min="0" step= "0.01" style="width:70px;" name="details[<?php echo $current_op_name; ?>][0][<?php echo $pod->bus_type; ?>][md_comm]" id="" class="md_commission" value='0'/></td>
                                            <td><input type="number" min="0" step= "0.01" style="width:70px;" name="details[<?php echo $current_op_name; ?>][0][<?php echo $pod->bus_type; ?>][ad_comm]" id="" class="ad_commission" value='0'/></td>
                                            <td><input type="number" min="0" step= "0.01" style="width:70px;" name="details[<?php echo $current_op_name; ?>][0][<?php echo $pod->bus_type; ?>][dist_comm]" id="" class="dist_commission" value='0'/></td>
                                            <td><input type="number" min="0" step= "0.01" style="width:70px;" name="details[<?php echo $current_op_name; ?>][0][<?php echo $pod->bus_type; ?>][retailer_comm]" id="" class="retailer_commission" value='0'/></td>
                                        </tr>
                                        <?php
                                    }
                                    $counter++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-4">
                            <button class="btn btn-primary" id="save_commission_data" name="save_commission_data" type="submit">Save</button> 
                            <a class="btn btn-primary" href="<?php echo base_url('admin/commission'); ?>" type="button">Back</a> 
                        </div>
                    </div>
                </form>
            </div>
        </div> <!-- /.box -->
    </div> <!--/.col (right) -->
</section>
<style type="text/css" rel="stylesheet">
    .ui-datepicker{ z-index: 9990 !important; }
    .ui-datepicker select{ color:#000 !important; }
    .ui-datepicker-header{ background-color: #367fa9} 
</style>


   