<!-- <section class="content" -->
    <div class="row">
        <h3 class="box-title">&nbsp;&nbsp;&nbsp;Commission Details</h3>
        <div class="clearfix" style="height: 10px;clear: both;"></div>
        <?php if ($this->session->flashdata('error')) { ?>
            <div class="alert alert-success alert-dismissable">
                <b>
                    <?php
                    echo $this->session->flashdata('error');
                    ?></b>
                <i class="fa fa-check"></i>
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            </div>
        <?php } ?>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="bs-example">
                        <div class="alert alert-danger alert-error" style="display:none">
                            <strong>Error!</strong> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="commission name">Commission Name</label>
                        <div class="col-lg-6">
                            <input name="name" type="text" id="name" value="<?php echo $commission_name; ?>" class="form-control" placeholder="Commission Name" readonly>
                        </div>
                    </div>
                     <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="table-responsive">          
                        <table class="table table-bordered" id="provider-operator-commission">
                            <thead>
                                <tr><th colspan="13" style="text-align:center" class="bg-gray disabled color-palette">
                                        <?php
                                       /* if ($operator_provider_name > 0) {
                                            echo strtoupper($operator_provider_name[$i]);
                                        }*/
                                        ?>
                                    </th></tr>

                                <tr>
                                    <th>Sr. No.</th>
                                    <!-- <th style="text-align:center">Provider</th> -->
                                    <th style="text-align:center">Operator</th>
                                    <th style="text-align:center">Default Status</th>
                                    <th style="text-align:center">My Commission(%)</th>
                                   <!--  <?php //if($this->session->userdata('role_id')== MASTER_DISTRIBUTOR_ROLE_ID) {?>
                                    <th style="text-align:center">MD Commission(%)</th>
                                    <?php //} ?>
                                     <?php //if($this->session->userdata('role_id')== AREA_DISTRIBUTOR_ROLE_ID) {?>
                                    <th style="text-align:center">AD Commission(%)</th>
                                     <?php //} ?>
                                    <?php //if($this->session->userdata('role_id')== DISTRIBUTOR) {?>
                                    <th style="text-align:center">Agent Commission(%)</th>
                                     <?php //} ?>
                                    <?php //if($this->session->userdata('role_id')== RETAILER_ROLE_ID) {?>
                                    <th style="text-align:center">Sales Executive Commission(%)</th>
                                     <?php //} ?> -->
                                </tr>

                            </thead>
                            <tbody> 
                              <?php //show($commission_detail_result); ?>
                                <tr class="">
                                    <td style="text-align:center">1</td>
                                    <td style="text-align:center">MSRTC</td>
                                    <td style="text-align:center"><?php echo ($commission_default_flag == '0' ? 'No' : 'Yes');?></td>
                                     <?php if($this->session->userdata('role_id')== MASTER_DISTRIBUTOR_ROLE_ID) {?>
                                    <td style="text-align:center"><?php echo round($commission_detail_result['md_commission'],0); ?>%</td> <?php } ?>
                                     <?php if($this->session->userdata('role_id')== AREA_DISTRIBUTOR_ROLE_ID) {?>
                                    <td style="text-align:center"><?php echo round($commission_detail_result['ad_commission'],0); ?>%</td> <?php } ?>
                                     <?php if($this->session->userdata('role_id')== DISTRIBUTOR) {?>
                                    <td style="text-align:center"><?php echo  round($commission_detail_result['dist_commission'],0); ?>%</td> <?php } ?>
                                     <?php if($this->session->userdata('role_id')== RETAILER_ROLE_ID) {?>
                                    <td style="text-align:center"><?php echo  round($commission_detail_result['retailer_commission'],0); ?>%</td> <?php } ?>
                                </tr>
                        
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-4">
                            <a class="btn btn-primary" href="<?php echo base_url('admin/dashboard'); ?>" type="button">Back</a> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 




   <script type="text/javascript">
    $(document).ready(function (e) 
    {
        $("#from_date" ).datepicker({
            'dateFormat' : 'dd-mm-yy',
            'maxDate':'0'
            }).datepicker("setDate", "0");
        
       
            
        $.validator.addMethod("custom_rule", function(value, element) 
        {
            var dropdown_val = $("#no_of_days").val();
            if((dropdown_val > 0 && dropdown_val <= 180))
            {
               return true;
            }
            else
            {
               return false;
            }
        }, "No of days should be greater than 0 and less than equal to 180");         
    
    
        $("#view_ticket_count_report").submit(function(e) 
        {
            e.preventDefault();
        }).validate({ 
        rules:  
        {   no_of_days : 
                    {
                        required: true,
                        custom_rule : true
                    }
        },
        messages: 
        { 
            no_of_days: { required: "Please enter no of days", }
        },
        submitHandler: function(form,e) 
        { 
            var tconfig = 
            {
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": BASE_URL + "report/Top_defaulter_report/defaulter_details",
                    "type": "POST",
                    "data": "json",
                    data:function(d) 
                    {
                        var date = new Date();
                        var from_date;
                        if($("#from_date").val()=='') 
                        {
                            from_date= $("#def_from_date").val();
                        }
                        else
                        {
                            from_date=$("#from_date").val();
                        }

                        d.from_date   = from_date;
                        d.no_of_days  = $("#no_of_days").val();
                        d.select_referrer=$("#select_referrer option:selected").val();
                        d.state       = $("#select_state").val();
                        d.city        = $("#select_city").val();

                    }
                },
                // "sAjaxSource": BASE_URL+'/data_table_eg/get_list1',
                "iDisplayLength": 10,
                "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
                //        "paginate": true,
                "order": [[2, "desc"]],
                "paging": true,
                "searching": true,
                "aoColumnDefs": [
                {"bVisible": false, "aTargets": [6]}
                  ] , 
                "fnRowCallback": function(nRow, aData, iDisplayIndex) {


                     $("td:first", nRow).html(iDisplayIndex + 1);

                    return nRow;

                },

            };
            
            $("#op_rept").dataTable().fnDestroy();
            var oTable = $('#op_rept').dataTable(tconfig);

            //e.preventDefault();
            //oTable.fnDraw();
            
        }
    });
        
      
    $('#extoexcel').on('click', function(e) 
    {
        var from_date  = $("#from_date").val();
        if($("#from_date").val()=='') 
        {
            from_date= $("#def_from_date").val();
        }
        if($("#no_of_days").val() == '')
        {
            $("#export-error").html('Please enter no of days').show();

        }
        else if ($("#no_of_days").val() < 0  || $("#no_of_days").val() > 180 )
        {
           $("#export-error_greater").html('No of days should be greater than 0 and less than or equal to 180').show();
        }
        else
        {
            var no_of_days = $("#no_of_days").val();
        }
        var state = $("#select_state").val();
        var city = $("#select_city").val();
        
        var select_referrer=$("#select_referrer option:selected").val();
    
        if($("#from_date").val() != '' && $("#no_of_days").val() != '' && ($("#no_of_days").val() > 0  && $("#no_of_days").val() <= 180 ))
        {
          window.location.href=BASE_URL+'report/Top_defaulter_report/defaulter_details_summary_excel?from_date='+from_date+"&no_of_days="+no_of_days+"&select_state="+state+"&select_city="+city+"&select_referrer="+select_referrer;   
        }
    }); 

    $(document).off('change', '.state').on('change', '.state', function(e)
    {
        $("#city").html("<option value=''>Please wait..</option>");
        
        var detail  =   {};
        var div     =  '';
        var str     =  "";
        var form    =  '';
        var ajax_url ='admin/ors_users/get_statewise_city';

        detail['state_id'] = $("#select_state").find('option:selected').attr('value');
        
            $("#select_city").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
            get_data(ajax_url, form, div, detail, function(response)
            {
                if(response.city.length != 0)
                {
                    $("#select_city").html("<option value=''>Select City</option>");
                    $.each(response.city, function(i,value)
                    {
                      $("#select_city").append("<option value="+value.intCityId+">"+value.stCity+"</option>");
                      $(".chosen_select").trigger("chosen:updated");
                    });
                }
                else
                {
                    $("#select_city").html("<option value=''>Select City</option>").trigger('chosen:updated');    
                }
            }, '', false);
    }); 
});
</script>
</section>


