<section class="content">
    <div class="row">
        <!-- right column -->
        <div class="col-md-12">
            <!-- general form elements disabled -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Set Commission Level</h3>
                </div><!-- /.box-header -->
                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <?php  
                 echo validation_errors('<div class="error">', '</div>');
                if ($this->session->flashdata('message')) {
                    ?>
                    <div class="alert alert-success alert-dismissable" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?php echo $this->session->flashdata('message'); ?>
                    </div>
                    <?php
                }
                $attributes = array("method" => "POST", "id" => "select_service", "class" => "select_service");
                echo form_open(base_url() . 'admin/commission/save_commission_level', $attributes);
                ?>
                 <div class="form-group">
                    <label class="col-lg-3 control-label" for="commission user">Select User</label>
                    <div class="col-lg-6">
                        <select class="form-control" name="comm_user" id="comm_user" aria-invalid="false" autocomplete="off">
                            <option value="">Select User</option>
                            <?php 
                            foreach ($users as $key => $value) {
                                 $selected = $role = '';
                               if(isset($commission_level) && !empty($commission_level)){
                                   if($commission_level->user_id == $value['id']){
                                    $selected = 'selected';   
                                   }
                               }
                               if($value['role_id'] == '4'){
                                   $role = 'Regional Distributor';
                               }else if($value['role_id'] == '5'){
                                   $role = 'Divisional Distributor';
                               }
                               ?>
                                <option value="<?php echo $value['id'] ?>" <?php echo $selected; ?> data-roleid="<?php echo $value['role_id']; ?>"><?php echo $value['first_name'].' '.$value['last_name'].'  ('.$role.')' ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <span id="" class="err"></span>
                    </div>
                </div>
               

                <div class="clearfix" style="height: 10px;clear: both;"></div>
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="commission name">Select Commission Level</label>
                    <div class="col-lg-6">
                        <select class="form-control" name="comm_role" id="comm_role" aria-invalid="false" autocomplete="off">
                            <option value="">Select Commission Level</option>
                            <?php 
                            foreach ($role_array as $key => $value) {
                                 $selected = '';
                               if(isset($commission_level) && !empty($commission_level)){
                                   if($commission_level->role == $value['id']){
                                    $selected = 'selected';   
                                   }
                               }  ?>
                                <option value="<?php echo $value['id'] ?>" <?php echo $selected; ?>><?php echo $value['role_name'] ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <span id="" class="err"></span>
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>
                <div class="form-group">
                    <div class="col-lg-offset-4">
                        <button class="btn btn-primary" id="save_values" name="save_values" type="submit">Save</button>  
                        <button class="btn btn-primary back" id="back_data" type="button">Back</button> 
                    </div>
                </div>
                <?php
                form_close();
                ?>
            </div>
        </div>     
    </div> <!--/.col (right) -->
</section>

<script>
    $(document).ready(function() {
        
        $('#select_service').validate({
            rules: {
                comm_role: {
                    required: true
                },
                comm_user:{
                    required: true
                }
            },
            messages: {
                comm_role: {
                    required: "Please select level"
                },
                comm_user: {
                    required: "Please select user"
                }
            },
            errorPlacement: function(error, element) {
                error.appendTo(element.parent("div"));
            }
        });
        
         $(document).off('change', '#comm_user').on('change', '#comm_user', function(e){
            var user_id = $(this).val().toLowerCase();
            var role_id = $(this).find(':selected').data('roleid');
            if(role_id == '4'){
                  $("#comm_role option[value=4]").attr('selected', 'selected');
            }else if(role_id == '5'){
                  $("#comm_role option[value=5]").attr('selected', 'selected');
            }else{
                 $('select[name^="comm_role"] option:selected').attr("selected",null);
            }
       }); 
    });

    $("#back_data").on("click", function(e) {
            window.location.replace("<?php echo base_url('admin/dashboard'); ?>");

    });

</script>
