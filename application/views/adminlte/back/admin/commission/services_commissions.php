<!-- Created by harshada kulkarni on 06-09-2018 -->
<section class="content">
    <div class="row">
        <!-- right column -->
        <div class="col-md-12">
            <!-- general form elements disabled -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Select Service</h3>
                </div><!-- /.box-header -->
                <div class="clearfix" style="height: 10px;clear: both;"></div>

                <?php
                $attributes = array("method" => "POST", "id" => "select_service_form", "class" => "select_service_form");
                echo form_open(base_url() . 'admin/commission/load_services_commissions', $attributes);
                ?>
                <div class="form-group">
                    <label class="col-lg-3 control-label" for="commission name">Select Service</label>
                    <div class="col-lg-6">
                        <select class="form-control chosen-select" name="select_service" id="select_service_option" aria-invalid="false" autocomplete="off">
                            <option value="">Select Service</option>
                            <?php
                            foreach ($service_list as $key => $value) {
                                ?>
                                <option value="<?php echo $value->id ?>"><?php echo $value->service_name ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <span id="" class="err"></span>
                    </div>
                </div>

                <div class="clearfix" style="height: 10px;clear: both;"></div>
                <div class="form-group select_sub_service_div">
                    <label class="col-lg-3 control-label" for="select_sub_service_label">Select Sub Service</label>
                    <div class="col-lg-6">
                        <select class="form-control chosen-select" name="select_sub_service" id="select_sub_service" aria-invalid="false" autocomplete="off">
                            <option value="">Select Sub Service</option>
                        </select>
                        <span id="" class="err"></span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-6">
                        <center><img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image" style="display:none;"/></center>
                    </div>
                </div>
                <div class="clearfix" style="height: 10px;clear: both;"></div>
                <div class="form-group">
                    <div class="col-lg-offset-4">
                        <button class="btn btn-primary" id="save_group_data" name="save_group_data" type="submit">Next</button>
                        <a class="btn btn-primary" href="<?php echo base_url('admin/commission/services_commissions'); ?>" type="button">Reset</a> 
                    </div>
                </div>
                <?php
                form_close();
                ?>
            </div>
        </div>     
    </div> <!--/.col (right) -->
</section>

<script>
    $(document).ready(function() {
        //$(".chosen-select").chosen({disable_search_threshold: 10});

        $('#select_service_form').validate({
            rules: {
                "select_service": {
                    required: true,
                },
                "select_sub_service": {
                    required: true,
                }
            },
            messages: {
                "select_service": "Please select service first",
                "select_sub_service" : "Please select sub service first"
            },
            errorPlacement: function(error, element) {
                error.appendTo(element.parent("div"));
            },
            submitHandler: function() {
                $('.img-circle').show();
                $('#save_group_data').html('Processing....');
                $("#save_group_data").attr("disabled", "disabled");
                return true;
            }
        });
   // });

    $(document).on('change', '#select_sub_service', function(){
        var sub_service_id = $(this).val();
    });


    $(document).on('change', '#select_service_option', function(){
        var service_id = $(this).val();
       /* if(service_id == '4'){
            $('.select_sub_service_div').css('display', 'none');
        }else{*/
           //$('.select_sub_service_div').css('display', 'block');
            var url = "<?php echo base_url() . 'admin/Subservice/getSubServicesByServiceID' ?>";
            $.ajax({
              url: url,
              type: 'POST',
              data: {id: service_id},
              datatype: "json",
              success: function(result){
                var $select_elem = $("#select_sub_service");
                $select_elem.empty();
                $select_elem.append('<option value="">Select Sub Service</option>');
                $.each(result.data, function (idx, obj) {
                    $select_elem.append('<option value="' + obj.id + '">' + obj.text + '</option>');
                });
              //  $select_elem.chosen({ width: "95%" });
                $("#select_sub_service").trigger("chosen:updated");

              }
            });    
       // }
        
    });
});
    /*$(document).on('ready', function(){
        var service_id = "<?php echo $this->session->userdata('select_service') ?>";
        var sub_service_id = "<?php echo $this->session->userdata('select_sub_service') ?>";
        $('#select_service_option').val(service_id);
        $("#select_service_option").trigger("chosen:updated");
        if(service_id != ''){
            ///*if(service_id == '4'){
              ///  $('.select_sub_service_div').css('display', 'none');
            }else{*///
                /*$('.select_sub_service_div').css('display', 'block');
                var url = "<?php echo base_url() . 'admin/Subservice/getSubServicesByServiceID' ?>";
                $.ajax({
                  url: url,
                  type: 'POST',
                  data: {id: service_id},
                  datatype: "json",
                  success: function(result){
                    var $select_elem = $("#select_sub_service");
                    $select_elem.empty();
                    $select_elem.append('<option>Please select a sub service</option>');
                    $.each(result.data, function (idx, obj) {
                        $select_elem.append('<option value="' + obj.id + '">' + obj.text + '</option>');
                    });
                    $select_elem.chosen({ width: "95%" });

                    $('#select_sub_service').val(sub_service_id);
                    $("#select_sub_service").trigger("chosen:updated");

                  }
                });    
            //}    
        }
        
        
    });*/
</script>
