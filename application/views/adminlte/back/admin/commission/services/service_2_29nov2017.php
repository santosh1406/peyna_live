<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/back/commission/commission.js" ></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<style>
    .shadow_red{border: 1px solid red !important;box-shadow: 0px 0px 19px -7px red;}
    .inner-table{margin: -9px !important; padding: 0;}
    .shadow_blue{border: 1px solid blue !important;box-shadow: 0px 0px 19px -7px blue;}
</style>
<section class="content">
    <div class="row">
        <!-- right column -->
        <div class="col-md-12">
            <!-- general form elements disabled -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Add Commission For <?php echo $service_detail[0]->service_name?></h3>
                </div><!-- /.box-header -->
                <?php if ($this->session->flashdata('msg')) { ?>
                    <div class="alert alert-success alert-dismissable">
                        <b>
                            <?php
                            $data = $this->session->flashdata('msg');
                            foreach ($data as $key => $value) {
                                echo $value . "<br>";
                            }
                            ?>
                        </b>
                        <i class="fa fa-check"></i>
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                    </div>
                <?php } ?>                
                <?php
				$attributes = array("method" => "POST", "name" => "create_commission_form", "id" => "create_commission_form");
				echo form_open(site_url().'/admin/commission/save_commission_service_2', $attributes);
				?>				 
					<div class="form-group">
                        <label class="col-lg-3 control-label" for="commission name">Commission Name</label>
                        <div class="col-lg-6">
                            <input name="name" type="text" id="name" class="form-control" placeholder="Commission Name" autocomplete="off">
                            <span id="name_msg" class="err"></span>
                        </div>
                    </div>
                   <div class="clearfix" style="height: 10px;clear: both;"></div> 
                    <div class="form-group" style="display: block;">
                        <label class="col-lg-3 control-label" for="commission name">Default Template</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="default_template" id="default_template" aria-invalid="false" autocomplete="off">
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                            <span id="" class="err"></span>
                        </div>
                    </div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group" id ="show_hide_date" style="display: none;">
                        <label class="col-lg-3 control-label" for="commission from">From - Till</label>
                        <div class="col-lg-6">
                            <input name="from_till" type="text" id="from_till" class="form-control" placeholder="Commission From and Till" autocomplete="off">
                        </div>
                    </div>

                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <!-- end --->			
                    <div class="form-group">
                        <div class="col-lg-6">
                            <input name="from" type="hidden" id="from" value="" class="form-control datepicker" placeholder="Commission From">

                        </div>
                    </div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group">
                        <!-- <label class="col-lg-3 control-label" for="commission till">Till</label> -->
                        <div class="col-lg-6">
                            <input name="till" type="hidden" id="till" value="" class="form-control datepicker" placeholder="Commission Till">
                        </div>
                    </div>



                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="table-responsive" style="min-height: 0.01%;overflow-x: auto;">          
                        <table class="table table-bordered" id="provider-operator-commission" style="min-height: 0.01%;overflow-x: auto;">
                            <thead>
                                <tr class="bg-light-blue-active color-palette">
                                    <th>#</th>
                                    <th>Trimax Commission(%)</th>
                                    <th>Rokad Commission(%)</th>
                                    <th>MD Commission(%)</th>
                                    <th>AD Commission(%)</th>
                                    <th>Agent Commission(%)</th>
                                    <th>Sales Executive Commission(%)</th>
                                </tr>
                            </thead>
                            <tbody> 
                                        <tr>
                                            <td>1</td>
                                            <td><input type="number" min="0" step= "0.01" style="width:70px;" name="msrtc_trimax_comm" id="" class="trimax_comm" value='0' readonly="" /></td>
                                            <td><input type="number" min="0" step= "0.01" style="width:70px;" name="msrtc_pos_commission" id="" class="pos_commission" value='0' /></td>
                                            <td><input type="number" min="0" step= "0.01" style="width:70px;" name="msrtc_md_commission" id="" class="md_commission" value='0'/></td>
                                            <td><input type="number" min="0" step= "0.01" style="width:70px;" name="msrtc_ad_commission" id="" class="ad_commission" value='0'/></td>
                                            <td><input type="number" min="0" step= "0.01" style="width:70px;" name="msrtc_dist_commission" id="" class="dist_commission" value='0'/></td>
                                            <td><input type="number" min="0" step= "0.01" style="width:70px;" name="msrtc_retailer_commission" id="" class="retailer_commission" value='0'/></td>
                                        </tr>
                                        
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-4">
                            <button class="btn btn-primary" id="save_commission_data" name="save_commission_data" type="submit">Save</button> 
                            <a class="btn btn-primary" href="<?php echo base_url('admin/commission'); ?>" type="button">Back</a> 
                        </div>
                    </div>
                </form>
            </div>
        </div> <!-- /.box -->
    </div> <!--/.col (right) -->
</section>
<style type="text/css" rel="stylesheet">
    .ui-datepicker{ z-index: 9990 !important; }
    .ui-datepicker select{ color:#000 !important; }
    .ui-datepicker-header{ background-color: #367fa9} 
</style>


   