<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<section class="content">
    <div class="row">
        <!-- right column -->
        <div class="col-md-12">
            <!-- general form elements disabled -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Assign Commission Template</h3>
                </div><!-- /.box-header -->
                <?php
            		if($this->session->flashdata('msg')){ 
            	?>
                <div id="msg_block" class="col-md-12 error_block <?php if(!empty($error)){ echo 'show';} else{ echo "show";} ?>">
	                <div class="alert alert-info alert-dismissable" style ="margin-top:15px">
	                    <i class="fa fa-ban"></i>
	                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                    <?php echo '<b>Alert!</b>' . $this->session->flashdata('msg');  ?>	                    
	                </div>
	            </div>
                <?php } ?>
                <div class="box-body">
                    <div class="row" style=" margin-bottom:10px;">
                        <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group">
                                <label for="input-text-2">Select Mode to apply Commission</label>
                                <select class="form-control" id="check_way" name="check_way" >
                                    <option value="">Select Mode</option>
                                    <option value="all">Apply To All MD</option>
                                    <option value="each">Apply MD Wise</option>
                                </select> 
                                <a class="btn btn-primary" href="<?php echo base_url('admin/commission'); ?>" type="button">Back</a> 
                            </div>
                        </div>
                    </div>

                    <div class="row" style=" margin-bottom:10px;display: none;" id="for_all">
                        <?php 
                        $attributes1 = array('class' => 'form', 'id' => 'save_from', 'name'=>'save_from','method' => 'post');
                        echo form_open('admin/commission/save_agent_com_temp', $attributes1);
                        ?> 
                            <div style="display: block;" class="col-md-3">
                                <div  class="form-group">
                                    <label  for="input-text-2">Commission</label>
                                    <select class="form-control chosen_select" id="com_template" name="com_template" >
                                        <option value="0">Select Commission Template</option>
                                        <?php 
                                          if(!empty($commissionDetail))
                                            {
                                                foreach($commissionDetail as $cdkey => $detail)
                                                {
                                            ?>
                                                    <option value="<?php echo $detail['id']; ?>"><?php echo  $detail['name']; ?></option>
                                        <?php
                                                }
                                            }
                                        ?>
                                    </select> 
                                </div>
                            </div>
                         <div class="clearfix" style="height: 10px;clear: both;"></div>
                         <div class="form-group">
                        <label class="col-lg-2 control-label" for="commission from">From - Till</label>
                        <div class="col-lg-3">
                            <input name="from_till" type="text" id="from_till" class="form-control" placeholder="Commission From and Till">
                            <span id="from_msg" class="err"></span>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="col-lg-6">
                                <input name="from" type="hidden" id="from" value="" class="form-control datepicker" placeholder="Commission From">
                                <span id="from_msg" class="err"></span>
                            </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <!-- <label class="col-lg-3 control-label" for="commission till">Till</label> -->
                            <div class="col-lg-6">
                                <input name="till" type="hidden" id="till" value="" class="form-control datepicker" placeholder="Commission Till">
                                <span id="till_msg" class="err"></span>
                            </div>
                        </div>
                        </div>
                             <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="col-md-4">
                                <label for="input-text-2"></label>
                                <button class="btn btn-primary" id="save" name="save" type="submit">Save</button>
                             </div>

                        <?php echo form_close(); ?>

                    </div>

                     
                    <div class="row" style="margin-left:0px;display: none;" id="for_each"> 
                    	<div class="row" style=" margin-bottom:10px;">
                            
        	                	<div style="display: block;" class="col-md-2 display-no">
                                    <div style="position: static;" class="form-group">
                                        <label for="input-text-2">City</label>
                                        <select class="form-control chosen_select" id="city" name="city" >
                                            <option value="">Select City</option>
                                            <?php foreach($cities as $city_detail):?>
                                                <option value="<?php echo $city_detail['intCityId']?>" <?php if($city_detail['intCityId'] == $city_name) {?> selected <?php } ?>><?php echo $city_detail['stCity']?></option>
                                            <?php endforeach;?> 
                                        </select> 
                                    </div>
                                </div>
                                <div class="row" style=" margin-bottom:10px;">
                                <div style="display: block;" class="col-md-2 display-no">
                                    <div style="position: static;" class="form-group">
                                        <label for="input-text-2">Master Distributor</label>
                                        <select class="form-control chosen_select" id="agent" name="agent" >
                                            <option value="">Select Master Distributor</option>
                                            <?php foreach($agent_list as $agents):?>
                                                <option value="<?php echo $agents['id']?>" <?php if($agents['id'] == $agent_name) {?> selected <?php } ?>><?php echo $agents['display_name'] .'-'. $agents['id'] ?></option>
                                            <?php endforeach;?> 
                                        </select> 
                                    </div>
                                </div>

                                <div class="col-md-1">
                                    <label for="input-text-2"></label>
                                    <button class="btn btn-primary" id="agent_view_filter_btn" name="agent_view_filter_btn" type="submit">Filter</button> 

                                </div> 
                            
    	                </div>
                    
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">Agent Detail</h3>
                                    </div><!-- /.box-header -->
                                    <div class="box-body table-responsive">
                                        <table id="user_display_table" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th width="20px;">#</th>
                                                    <th>Agent Code</th>
                                                    <th>Agent Name</th>
                                                    <th>State</th>
                                                    <th>City</th>
                                                    <th>Template Name</th>
                                                    <th>From</th>
                                                    <th>Till</th>
                                                    <th>Type</th>
                                                    <th width="55px">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>                                           
                                              
                                            </tbody>

                                        </table>
                                   </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
				    <div class="clearfix"></div>                    
                </div><!-- /.box-body -->
                <div class="box-footer">&nbsp;</div>
            </div> <!-- /.box -->
        </div> <!--/.col (right) -->
    </div> <!-- /.row -->
</section>


<script type="text/javascript">
$(document).ready(function() {

var date = new Date();
   // date.setDate(date.getDate() - 1);
        $('input[name="from_till"]').on('click.daterangepicker', function (ev, picker) {
        $(".input-mini").hide();
        });
        
    $("#from_till").daterangepicker({
        startDate: date,
        minDate: date,
        autoUpdateInput: false,
        locale: {
            format: 'DD-MM-YYYY',
            cancelLabel: 'Clear'
        }},
        function (start, end, label) {
            // show("A new date range was chosen: " + start.format('DD-MM-YYYY') + ' to ' + end.format('DD-MM-YYYY'));
        }
    );

    $('input[name="from_till"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        $('input[name="from"]').val(picker.startDate.format('DD-MM-YYYY'));
        $('input[name="till"]').val(picker.endDate.format('DD-MM-YYYY')).attr("data-validate-date", picker.startDate.format('DD-MM-YYYY'));
    });

    $('input[name="from_till"]').on('cancel.daterangepicker', function (ev, picker){
        $(this).val("");
        $('input[name="from"]').val("").attr("data-validate-date", "");
        $('input[name="till"]').val("");
    });
    // $("#agent_view_filter_btn").on("click", function (e) {
    $(document).off('click', '#agent_view_filter_btn').on('click', '#agent_view_filter_btn', function(e) {         
        // e.preventDefault();
        var tconfig = {
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": BASE_URL + "admin/commission/get_agent_detail",
                "type": "POST",
                data: function(d){
                         d.agent = $("#agent option:selected").val();
                         d.city = $("#city option:selected").val();
						 d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;
                    }
            },
            // "sAjaxSource": BASE_URL+'/data_table_eg/get_list1',
            "iDisplayLength": 10,
            "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
            //        "paginate": true,
            "order": [[2, "asc"]],
            "paging": true,
            "searching": true,
            "aoColumnDefs": [
            // {"bVisible": false, "aTargets": [2]}
            // {"bSearchable": false, "aTargets": [5,6,7]},
        {"bSearchable": false, "aTargets": [5,6,7]},
            {"bSortable": false, "aTargets": [8,9]}
          ] ,  

        "fnRowCallback": function(nRow, aData, iDisplayIndex) {
      
           
               $("td:first", nRow).html(iDisplayIndex + 1);

                return nRow;
            },

        };

   
    var oTable = $('#user_display_table').dataTable(tconfig);



    $(document).off('click', '#agent_view_filter_btn').on('click', '#agent_view_filter_btn', function(e) {
        e.preventDefault();
        
        oTable.fnDraw();
    });
});



    $(document).off('change', '#check_way').on('change', '#check_way', function (e) 
    {
       
        var value = $(this).val();
        if(value == 'all')
        {
            $('#for_all').show();
            $('#for_each').hide();
        }
        else if(value == 'each' )
        {
            $('#for_each').show();
            $('#for_all').hide();
        }
        else
        {
            $('#for_all').hide();
            $('#for_each').hide();
        }

    });

    
    $(document).off('submit', '#save_from').on('submit', '#save_from', function (e) 
    {
        if($("#com_template").val()=="0")
        {
            alert("Please Select Commission Template ");
            return false;   
        }
        
        if($('#from_till').val() == '') {
        alert("Enter From and Till Date");
        return false;
        }
        
        var yesno = confirm("Are you sure you want to apply commission to all MD?");

        if(yesno)
        {
            return true;
        }
        else
        {
            return false;
        }
        
    });
    
    
});

</script>