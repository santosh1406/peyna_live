<section class="content">
    <div class="row">
        <h3 class="box-title">&nbsp;&nbsp;&nbsp;Commission Details</h3>
        <?php if ($this->session->flashdata('error')) { ?>
            <div class="alert alert-success alert-dismissable">
                <b>
                    <?php
                    echo $this->session->flashdata('error');
                    ?></b>
                <i class="fa fa-check"></i>
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            </div>
        <?php } ?>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="bs-example">
                        <div class="alert alert-danger alert-error" style="display:none">
                            <strong>Error!</strong> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="commission name">Commission Name</label>
                        <div class="col-lg-6">
                            <input name="name" type="text" id="name" value="<?php echo $commission_name; ?>" class="form-control" placeholder="Commission Name" readonly>
                        </div>
                    </div>
                     <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="table-responsive">          
                        <table class="table table-bordered" id="provider-operator-commission">
                            <thead>
                                <tr><th colspan="13" style="text-align:center" class="bg-gray disabled color-palette">
                                        <?php
                                        if ($operator_provider_name > 0) {
                                            echo strtoupper($operator_provider_name[$i]);
                                        }
                                        ?>
                                    </th></tr>

                                <tr>
                                    <th>#</th>
                                    <th style="text-align:center">Provider</th>
                                    <th style="text-align:center">Operator</th>
                                    <th style="text-align:center">Bus Type</th>
                                    <?php if($this->session->userdata('role_id')== MASTER_DISTRIBUTOR_ROLE_ID) {?>
                                    <th style="text-align:center">MD Commission(%)</th>
                                    <?php } ?>
                                     <?php if($this->session->userdata('role_id')== AREA_DISTRIBUTOR_ROLE_ID) {?>
                                    <th style="text-align:center">AD Commission(%)</th>
                                     <?php } ?>
                                    <?php if($this->session->userdata('role_id')== DISTRIBUTOR) {?>
                                    <th style="text-align:center">Distributor Commission(%)</th>
                                     <?php } ?>
                                    <?php if($this->session->userdata('role_id')== RETAILER_ROLE_ID) {?>
                                    <th style="text-align:center">Retailer Commission(%)</th>
                                     <?php } ?>
                                </tr>

                            </thead>
                            <tbody> 
                                <?php $counter = 0; ?>    
                                <?php
                                foreach ($details_provider_operator as $dpo_name => $provider_operator_details) {
                                    ?>
                                    <?php
                                    foreach ($provider_operator_details as $pod_key => $pod) {
                                        $current_op_name = strtolower(trim($dpo_name));
                                        $pr_op_id = $pod->provider_operator_id;

                                        if ($current_op_name == 'bookonspot') {
                                            ?>

                                            <tr class="<?php echo $current_op_name; ?>">
                                                <td style="text-align:center"><?php echo $counter + 1 ?></td>
                                                <td style="text-align:center"><?php echo $pod->provider_name; ?></td>
                                                <td style="text-align:center"><?php echo $pod->provider_operator_name; ?></td>
                                                <td style="text-align:center"><?php echo $pod->bus_type; ?></td>
                                                
                                                 <?php if($this->session->userdata('role_id')== MASTER_DISTRIBUTOR_ROLE_ID) {?>
                                                <td style="text-align:center"><?php echo $pod->md_commission; ?></td> <?php } ?>
                                                 <?php if($this->session->userdata('role_id')== AREA_DISTRIBUTOR_ROLE_ID) {?>
                                                <td style="text-align:center"><?php echo $pod->ad_commission; ?></td> <?php } ?>
                                                 <?php if($this->session->userdata('role_id')== DISTRIBUTOR) {?>
                                                <td style="text-align:center"><?php echo $pod->dist_commission; ?></td> <?php } ?>
                                                 <?php if($this->session->userdata('role_id')== RETAILER_ROLE_ID) {?>
                                                <td style="text-align:center"><?php echo $pod->retailer_commission; ?></td> <?php } ?>
                                               
                                            </tr>

                                            <?php
                                        } else {
                                            ?>
                                            <tr  class="<?php echo $current_op_name; ?>" >
                                                <td style="text-align:center"><?php echo $counter + 1 ?></td>
                                                <td style="text-align:center"><?php echo $pod->provider_name; ?></td>
                                                <td style="text-align:center"><?php echo $pod->operator_name; ?></td>
                                                <td style="text-align:center"><?php echo $pod->bus_type; ?></td>
                                                <?php if($this->session->userdata('role_id')== MASTER_DISTRIBUTOR_ROLE_ID) {?>
                                                <td style="text-align:center"><?php echo $pod->md_commission; ?></td> <?php } ?>
                                                 <?php if($this->session->userdata('role_id')== AREA_DISTRIBUTOR_ROLE_ID){ ?>
                                                <td style="text-align:center"><?php echo $pod->ad_commission; ?></td> <?php } ?>
                                                 <?php if($this->session->userdata('role_id')== DISTRIBUTOR) {?>
                                                <td style="text-align:center"><?php echo $pod->dist_commission; ?></td> <?php } ?>
                                                 <?php if($this->session->userdata('role_id')== RETAILER_ROLE_ID) {?>
                                                <td style="text-align:center"><?php echo $pod->retailer_commission; ?></td> <?php } ?>
                                            </tr>
                                            <?php
                                        }
                                        $counter++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-4">
                            <a class="btn btn-primary" href="<?php echo base_url('admin/dashboard'); ?>" type="button">Back</a> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


