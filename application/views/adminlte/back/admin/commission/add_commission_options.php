<!-- Created by harshada kulkarni on 06-09-2018 -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> 
                        <h3 class="box-title">Commission Options</h3>
                    </div>
                    <div class="box-body">
                        <?php
                        echo validation_errors('<div class="error">', '</div>');
                        if ($this->session->flashdata('message')) {
                            ?>
                            <div class="alert alert-success alert-dismissable" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('message'); ?>
                            </div>
                            <?php
                        }
                        $attributes = array("method" => "POST", "id" => "commission_options", "name" => "commission_options");
                        echo form_open(base_url() . 'admin/commission/add_commission_options', $attributes);
                        ?>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group intent"> 
                            <label class="col-lg-3 control-label" for="service_name">Services *:</label>
                            <div class="col-lg-6">                                    
                                <select id="service_name" name="service_name" class="form-control">
                                    <option value="">Select Services</option>
                                    <?php if (is_array($serviceDetails)) { ?>
                                        <?php foreach ($serviceDetails as $sd) { ?>
                                            <option value="<?php echo $sd->id ?>"><?php echo $sd->service_name; ?></option>                                          
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>                             
                            </div> 
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group intent"> 
                            <label class="col-lg-3 control-label" for="fields">Fields *:</label>
                            <div class="col-lg-6">                                
                                <select id="dynamic_fields" name="dynamic_fields[]" multiple class="form-control">
                                    <?php if (is_array($dynamicFields)) { ?>
                                        <?php foreach ($dynamicFields as $df) { ?>                                            
                                            <option value="<?php echo $df->field ?>"><?php echo $df->field; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>  
                                </select>
                            </div> 
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">
                                <button class="btn btn-primary" id="save_values" name="save_values" type="submit">Save</button>  
                                <button class="btn btn-primary back" id="back_data" type="button">Back</button> 
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<style>
    .multiselect-item.filter {
        width: 515px;
    }
</style>
<script type="text/javascript">
    $(document).ready(function() {

        $(document).off('click', '.back').on('click', '.back', function(e) {
            window.location.href = BASE_URL + 'admin/commission/commission_options';
        });

        $('#dynamic_fields').multiselect({
            nonSelectedText: 'Select Fields',
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '515px',
            maxHeight: 200
        });

        $("#commission_options").validate({
            rules: {
                service_name: {
                    required: true,
                }
            },
            messages: {
                service_name: {
                    required: "Please select service.",
                }
            }
        });

        $("#save_values").on("click", function(e) {
            var dropdownvalue = $('#dynamic_fields').val();
            if (dropdownvalue == 0 || dropdownvalue == "" || dropdownvalue == null) {
                alert("Please select atleast one field.");
                e.preventDefault();
            }
        });
    });
</script>