<!-- Created by harshada kulkarni on 06-09-2018 -->
<section class="content">
    <div class="row">
        <!-- right column -->
        <div class="col-md-12">
            <!-- general form elements disabled -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Commission</h3>
                </div><!-- /.box-header -->
                <?php
                echo validation_errors('<div class="error">', '</div>');
                if ($this->session->flashdata('message')) {
                    ?>
                    <div class="alert alert-success alert-dismissable" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?php echo $this->session->flashdata('message'); ?>
                    </div>
                <?php }
                ?>
                <div class="box-body">
                    <div class="row" style=" margin-bottom:10px;">
                        <div class="col-md-2">
                            <a href="<?php echo base_url() . 'admin/commission/add_services_commissions'; ?>" class="btn btn-warning " role="button">Create New Commission</a>
                        </div>
                    </div>
                    <table id="services_commissions_table" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="20px">Id</th>
                                <th width="">Name</th> 
                                <th width="">Default</th> 
                                <th width="">Valid From</th>
                                <th width="">Valid To</th>
                                <th width="">Status</th>
                                <th width="">Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                    <div class="clearfix"></div>                    
                </div><!-- /.box-body -->
                <div class="box-footer">&nbsp;</div>
            </div> <!-- /.box -->
        </div> <!--/.col (right) -->
    </div> <!-- /.row -->
</section>
<link href="<?php echo base_url() . COMMON_ASSETS . 'plugins/colorbox/example3/colorbox.css' ?>" rel="stylesheet" />
<script src="<?php echo base_url() . COMMON_ASSETS . 'plugins/colorbox/jquery.colorbox.js' ?>"></script>
<script type="text/javascript">
    var img = '<center><img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/></center>';
</script>
<script>
    $(document).ready(function() {
        var tconfig = {
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": BASE_URL + "admin/commission/services_commissions_list",
                "type": "POST",
                "data": "json",
                data  : function(d) {
                    d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;
                }
            },
            "iDisplayLength": 10,
            "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
            "paging": true,
            "searching": true,
            "aoColumnDefs": [
                {"bSearchable": false, "aTargets": [2]}
            ],
            "order": [[0, 'desc']],
            "oLanguage": {
                "sProcessing": '<center><img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/></center>'
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                if (aData[3] == "0000-00-00 00:00:00" || aData[3] == "1970-01-01 00:00:00") {
                    $("td:eq(3)", nRow).html("-");
                }
                if (aData[4] == "0000-00-00 00:00:00" || aData[4] == "1970-01-01 23:59:59") {
                    $("td:eq(4)", nRow).html("-");
                }
                var btn = '';
                var active_btn = 'fa fa-check-square-o';
                var status = 'Enable';
                btn += '<a title="Edit" href="" class="edit_btn margin" ref="' + aData[0] + '"> <i  class="glyphicon glyphicon-pencil"></i> </a>'
                if (aData[5] == 'N') {
                    active_btn = 'fa fa-square-o';
                    status = 'Disable';
                }
                if (aData[2] == 'N') {
                    btn += '<a title="' + status + '" href="" class="status_btn" ref="' + aData[0] + '" data-ref="' + aData[5] + '"><i class="' + active_btn + '"></i></a>';
                }
                $("td:first", nRow).html(iDisplayIndex + 1);
                $("td:last", nRow).html(btn);
            },
        };
        var oTable = $('#services_commissions_table').dataTable(tconfig);
    });

    $(document).off('click', '.edit_btn').on('click', '.edit_btn', function(e) {
        e.preventDefault();
        var id = $(this).attr('ref');
        var base_url = BASE_URL + "admin/commission/edit_services_commissions/" + id;
        window.location.href = base_url;
    });
    $(document).off('click', '.status_btn').on('click', '.status_btn', function(e) {
        e.preventDefault();
        var id = $(this).attr('ref');
        var base_url = BASE_URL + "admin/commission/change_status_services_commissions/" + id;
        window.location.href = base_url;
    });
</script>
