<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<section class="content">
    <div class="row">
        <!-- right column -->
        <div class="col-md-12">
            <!-- general form elements disabled -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Master Distributor Commission Detail</h3>
                    <h3 class="box-title pull-right">Agent Name : <?php echo $userData[0]->full_name; ?></h3>
                </div><!-- /.box-header -->
                <?php
            		if($this->session->flashdata('msg')){ 
            	?>
                <div id="msg_block" class="col-md-12 error_block <?php if(!empty($error)){ echo 'show';} else{ echo "show";} ?>">
	                <div class="alert alert-info alert-dismissable" style ="margin-top:15px">
	                    <i class="fa fa-ban"></i>
	                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                    <?php echo '<b>Alert!</b>' . $this->session->flashdata('msg');  ?>	                    
	                </div>
	            </div>
                <?php } ?>
                <div class="box-body">
                	<div class="row" style=" margin-bottom:10px;">
	                	
	                </div>
                	<table id="listdata" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="20px">Sr No</th>
                            <th width="">Commission Name</th>
                            <th width="">From</th>
                            <th width="">Till</th>
                            <th width="">Type</th>
                            <th width="">Assigned Date</th>
                            <th width="">Commission Master Status</th>
                            <th width="">Active</th>
                        </tr>
                    </thead>
                    <tbody id="listdata">  
                        <?php
                            $i = 1;
                            if(count($active_commission_detail) > 0 && !empty($active_commission_detail))
                            {
                                foreach($active_commission_detail as $detail)
                                { 
                                    if($activeComissionLogId[0]['id'] == $detail['log_id'])
                                    {
                        ?> 
                                        <tr style="background-color:#32CD32 ;">
                                <?php
                                    }
                                    else
                                    {
                                ?>
                                        <tr>
                                <?php
                                    }
                                ?>
                                <td><?php echo $i;?></td>
                                <td><?php echo $detail['name'];?></td>
                                <td><?php echo date('d-m-Y',strtotime($detail['commission_from']));?></td>
                                <td><?php echo date('d-m-Y',strtotime($detail['commission_to']));?></td>
                                <td><?php echo ($detail['commission_default_flag'] == '1' ? 'Default' : '');?></td>
                                <td><?php echo date('d-m-Y',strtotime($detail['added_on']));?></td>
                                <td><?php 
                                echo (($detail['status'] == 1) ? 'Active' : 'Inactive' );
                                    ?>
                                </td>
                                <?php
                                    $active_btn = 'fa fa-square-o';
                                    if($detail['log_status'] == '1')
                                    {
                                        $active_btn = 'fa fa-check-square-o';
                                    }
                                    if((date('Y-m-d',strtotime($detail['commission_to'])) >= date('Y-m-d')) && $detail['status'] == 1)
                                    {
                                ?>

                                <td><a title="Change status" href="#" class="change_status" ref="<?php echo $detail['log_status'];?>" ref-id="<?php echo $detail['log_id'];?>"><i class="<?php echo $active_btn;?>"></i></a></td>
                                <?php
                                    }
                                    else
                                    {
                                ?>
                                    <td></td>
                                <?php
                                    }
                                ?>
                            </tr>
                        <?php 
                                $i++;
                                }
                            }
                        ?>                                        
                    </tbody>
                    </table>
				    <div class="clearfix"></div>
                    <div class="row" style="margin-bottom:10px;">
                        <div class="row" style="display: block;margin-left: 0px;">
                            <div class="box-header">
                                    <h3 class="box-title">Assign Master Distributor Commission</h3>
                            </div><!-- /.box-header -->
                        </div>
                        <div style="display: block;" class="col-md-4">
                            <div style="position: static;" class="form-group">
                                <input type="hidden" id="agentId" name="agentId" value="<?php echo $agentId;?>">
                                <select class="form-control chosen_select" id="com_template" name="com_template" >
                                    <option value="0">Select Commission Template</option>
                                    <?php 
                                      if(!empty($commissionDetail))
                                        {
                                            foreach($commissionDetail as $cdkey => $detail)
                                            {
                                        ?>
                                                <option value="<?php echo $detail['id']; ?>"><?php echo  $detail['name']; ?></option>
                                    <?php
                                            }
                                        }
                                    ?>
                                </select> 
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label" for="commission from">From - Till</label>
                        <div class="col-lg-3">
                            <input name="from_till" type="text" id="from_till" value="<?php echo $this->session->flashdata('old_name'); ?>" class="form-control" placeholder="Commission From and Till">
                            <span id="from_msg" class="err"></span>
                        </div>
                    </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>    
                        <div class="col-md-4">
                            <label for="input-text-2"></label>
                            <button class="btn btn-primary com_temp" id="save" name="save" type="button">Save</button>
                            <a class="btn btn-primary" href="<?php echo base_url('admin/commission/assign_commission_template'); ?>" type="button">Back</a> 
                        </div>

                    </div>                    
                </div><!-- /.box-body -->
            </div> <!-- /.box -->
        </div> <!--/.col (right) -->
    </div> <!-- /.row -->
</section>


<script type="text/javascript">
$(document).ready(function () {
    var date = new Date();
    var fromd;
    var till;
    // date.setDate(date.getDate() - 1);
        $('input[name="from_till"]').on('click.daterangepicker', function (ev, picker) {
        $(".input-mini").hide();
        });
        
    $("#from_till").daterangepicker({
        startDate: date,
        minDate: date,
        autoUpdateInput: false,
        locale: {
            format: 'DD-MM-YYYY',
            cancelLabel: 'Clear'
        }},
        function (start, end, label) {
            // show("A new date range was chosen: " + start.format('DD-MM-YYYY') + ' to ' + end.format('DD-MM-YYYY'));
        }
    );

    $('input[name="from_till"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        $('input[name="from"]').val(picker.startDate.format('DD-MM-YYYY'));
        $('input[name="till"]').val(picker.endDate.format('DD-MM-YYYY')).attr("data-validate-date", picker.startDate.format('DD-MM-YYYY'));
        fromdate = picker.startDate.format('YYYY-MM-DD');
        enddate =  picker.endDate.format('YYYY-MM-DD');
    });
    $('input[name="from_till"]').on('cancel.daterangepicker', function (ev, picker){
        $(this).val("");
        $('input[name="from"]').val("").attr("data-validate-date", "");
        $('input[name="till"]').val("");
    });
    



    $(document).off('click', '.com_temp').on('click', '.com_temp', function (e) 
    {   
        if($('#from_till').val() == '') {
        alert("Enter From and Till Date");
        return false;
        }
        var id = $("#com_template").val();
        var agentId = $("#agentId").val();
        var detail      = {};
        var div         = "";
        var ajax_url    = "admin/commission/save_agent_wise_com_temp";
        var form        = '';
        detail['agentId'] = agentId;
        detail['commissionId'] = id;
        detail['from'] =  fromdate ;
        detail['till'] =   enddate ;
        get_data(ajax_url, form, div, detail, function (response)
        {
            if(response.flag=="@#success#@")
            {
                bootstrapGrowl('Template successfully Assign to Master Distributor','success');
                location.reload();
                
            }
            else
            {
                bootstrapGrowl('Template not assign to Master Distributor','success');
                
            }
        }, '', false);
        
    });
});
    $(document).off('click','.change_status').on('click','.change_status',function(e)
    {

        e.preventDefault();
        var detail    = {};
        var div       = "";
        var ajax_url  = 'admin/commission/chagne_agent_commission_status/'+ $(this).attr('ref');
        var form      = '';
        var err       = "";
        detail['id']  = $(this).attr('ref-id');
        detail['status'] = $(this).attr('ref');
        get_data(ajax_url, form, div, detail, function(response)
        {
            if (response.flag == '@#success#@')
            {
                bootstrapGrowl(response.msg, 'success');
                location.reload();
            }
        }, '', false)
    });
</script>