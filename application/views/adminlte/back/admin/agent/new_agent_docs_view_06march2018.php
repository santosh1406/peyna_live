<style>
    #film_strip li {
        float: left
    }
    #film_strip li img {
        float: left;
        background: #EFEFEF;
        padding: 2px;
        margin: 10px;
        border: 1px solid #AAA;

    }
    li.menu-item {
        margin:0 10px 12px 0;  
    }
    ul { list-style-type: none; }

    .text_sid
    {
        word-wrap: break-word;
        width: 5em; 
        margin-left:7px;
        margin-top:60px;
    }

</style>
<script type="text/Javascript">
    $(document).off('click', '.back_add_doc').on('click', '.back_add_doc', function(e) {

    id=$("#agid").val();
    window.location.href =BASE_URL+'admin/agent/add_new_doc?id='+id;
    });

</script>
<link href="<?php echo base_url() . COMMON_ASSETS ?>plugins/lightbox/src/css/lightbox.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() . COMMON_ASSETS ?>plugins/lightbox/src/js/lightbox.js" type="text/javascript"></script>

<script type="text/Javascript" src="<?php echo base_url() ?>js/back/master/new_agent_docs_upload.js" ></script>

<section class="content">


    <div class="col-sm-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Id proof Document</h3>
            </div>
            <div class="box-body">
                <div id="seat_layout_wrapper">

                    <div id="generated_seat_layout">

                        <ul id="film_strip">

                            <?php
                            if (!empty($idproof)) {
                                foreach ($idproof as $key => $val) {
                                    if (!empty($idproof[$key])) {
                                        ?>

                                        <li class="menu-item ">
                                            <div class="imageBox">
                                                <a href="<?php echo base_url($val['large_image']); ?>" data-lightbox="image-1" data-title="My caption" width="1000px" height="1000px">
                                                    <img src="<?php echo base_url($val['link']); ?>" alt="" width="120px" height="70px"/></a><br>

                                            </div>
                                            <p class="text_sid">
                                                <?php echo $val['docs']; ?>
                                            </p>   


                                        </li>

                                    <?php
                                    }
                                }
                            } else {
                                ?>
                                <li>No document uploaded</li>
<?php } ?>

                        </ul>


                    </div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                </div>   

            </div>


        </div>   




    </div>

    <!--    address proof image-->
    <input type="hidden" name="agid" id="agid" value="<?php echo $this->input->get('id'); ?>">
    <div class="col-sm-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Address  proof Document</h3>
            </div>
            <div class="box-body">
                <div id="seat_layout_wrapper">

                    <div id="generated_seat_layout">

                        <ul id="film_strip">

                            <?php
                            if (!empty($addressproof)) {
                                foreach ($addressproof as $key => $val) {
                                    if (!empty($addressproof[$key])) {
                                        ?>

                                        <li class="menu-item ">
                                            <div class="imageBox">
                                                <a href="<?php echo base_url($val['large_image']); ?>" data-lightbox="image-1" data-title="<?php echo $val['docs']; ?>">
                                                    <img src="<?php echo base_url($val['link']); ?>" alt=""   width="120px" height="70px"/></a><br>

                                            </div>
                                            <p class="text_sid">
            <?php echo $val['docs']; ?>
                                            </p>  
                                        </li>

                                    <?php
                                    }
                                }
                            } else {
                                ?>
                                <li>No document uploaded</li>
<?php } ?>

                        </ul>

                    </div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                </div>   

            </div>

        </div>   
    </div>
    <div class="box-footer">
        <div class="col-md-6 col-md-offset-4">
            <button class="back_add_doc submit btn btn-primary" type="button" >Add another Document</button>
            <button class="backto_list btn btn-primary" type="button" id="vwimg">List view</button>     
        </div>
    </div> 



</section>

