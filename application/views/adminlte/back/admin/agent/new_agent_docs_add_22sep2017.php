
<script type="text/Javascript" src="<?php echo base_url() ?>js/back/master/new_agent_docs_upload.js" ></script>

<body class="skin-blue sidebar-mini">
    <section class="content-header"> 
        <h1>Upload Document</h1>
    </section> 
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">

                        <b><span class="col-lg-12 col-lg-offset-5 text-center">User: <?php echo ucwords($full_name); ?> </span></b>
                    </div>


                    <div class="alert alert-danger"  style="display:none">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>Error!</strong>&nbsp;<p></p>
                        <ul>

                        </ul>
                    </div>
                    <?php
                    $var_1 = $this->input->get('id');
                    $id = isset($var_1) ? $var_1 : $agent_id;
                    ?>
                    <?php 
                        $attributes = array('id' => "docs_upload",'method' => "post",'name' => 'docs_upload','onsubmit' => "return false");
                        echo form_open_multipart("#", $attributes); 
                    ?>
                        <div class="box-body">
                            <input type="hidden" name="agid" id="agid" value="<?php echo $id; ?>" >
                                   <div class="clearfix" style="height: 10px;clear: both;">
                                <div class=""   id="idproof">

                                    <table class="table table-bordered" id="idtable">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Document Type</th>
                                                <th>Document Name</th>
                                                <th>Select File</th>
                                                <th>Add</th>
                                                <th>Delete</th>
                                            </tr>  
                                        </thead>

                                        <tr>

                                            <td>1.</td>
                                            <td >
                                                <select name="doccat[]" id="doccat" class="doccat form-control"     data-validation="required"  data-validation-error-msg-required="Please select Document category"><option value="">Select document category</option>
                                                    <?php foreach ($res as $key => $val) : ?>
                                                        <option value="<?php echo $val['doc_cat_id'] ?>"><?php echo $val['category_name'] ?></option>
                                                    <?php endforeach; ?>


                                                </select>
                                            </td>    
                                            <td >
                                                <select name="docnm[]" class="form-control docnm" data-validation="required"        data-validation-error-msg-required="Please select Document Name"><option value="">Select document name</option>

                                                </select>
                                                <input type="hidden" name="dropdowntext[]" class="drotext">   
                                            </td>     
                                            <td>
                                                <input type="file"  name="img_doc[]"
                                                       data-validation="length mime size"
                                                       data-validation-length="min1"
                                                       data-validation-allowing="jpg, png, gif"
                                                       data-validation-max-size="5000kb"
                                                       data-validation-error-msg-size="You can not upload images larger than 5000kb"
                                                       data-validation-error-msg-mime="You can only upload images"
                                                       data-validation-error-msg-length="You have to upload  image"
                                                       />


                                            </td> 
                                            <td><input type="button" class="form-control  ins_row btn btn-primary" value="Add New"></td> 
                                            <td><input type="button" class="form-control  btn btn-primary del" value="Delete"></td> 
                                            <td></td>

                                        </tr>


                                    </table>
                                    <div class="box-footer">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button class="upload submit btn btn-primary" type="submit" name="upload_doc_button" id="upload_doc_button">Upload</button>
                                            <button class="docs_display btn btn-primary" type="button" id="vwimg">View documents</button>

                                        </div>
                                    </div>
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>

                                </div>

                            </div>     
                    </form>
                </div>
            </div>   
        </div>



    </section> 


</body>