<script type="text/Javascript" src="<?php echo base_url() ?>/js/back/master/new_agent_docs_upload.js" ></script>
<link href="<?php echo base_url() . COMMON_ASSETS ?>plugins/lightbox/src/css/lightbox.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() . COMMON_ASSETS ?>plugins/lightbox/src/js/lightbox.js" type="text/javascript"></script>

<!-- Main content -->
<section class="content">
    <div id="datatable_wrapper">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Uploaded Documents</h3>

                    </div><!-- /.box-header -->
                    <input type="hidden" name="agid"  id="agid" value="<?php echo $_GET['agid']; ?>">

                    <?php if ($this->session->flashdata('msg') != "") { ?>
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>success!</strong><?php echo $this->session->flashdata('msg'); ?>
                            <span></span>
                        </div>
                    <?php } else {
                        ?>

                        <div class="alert alert-success" style="display:none">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>success!</strong>
                            <span></span>
                        </div>       
                    <?php } ?>
                    <div class="box-body table-responsive">
                        <div class="form-group">

                            <button  type="button" id="add_new" name="back_btn_prin" class="add_new btn btn-primary">Upload New Document</button>  

                            <a  type="button" id="back_main_page" name="back_main_page" class="btn btn-primary" href="<?php echo base_url() . "/admin/registration"; ?>">Back</a>
                            <?php 
                            eval(SYSTEM_USERS_ARRAY);
                            if(in_array(strtolower($this->session->userdata('role_name')),$system_users_array) || $this->session->userdata('role_name') == 'Trimax')
                            {
                            if ($kyc_status == "N") { ?>
                                <button  type="button" id="approve_kyc_button" name="approve_kyc_button" class="btn btn-primary">Verify KYC</button>
                            <?php } else { ?>
                                <button  type="button" id="approve_kyc_button" name="approve_kyc_button" class="btn btn-primary" disabled="true">KYC Verified</button>
                            <?php }} ?>

                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="col-md-6 col-md-offset-4">



                            <!--<button class="docs_display btn btn-primary" type="button" id="vwimg">View documents</button>-->  
                        </div>


                        <table id="allagent" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="20px">Sr No</th>
                                    <th width="20px">User Name</th>
                                    <th width="40px">Category</th>
                                    <th width="5px">Document Name</th>
                                    <th width="5px">Preview</th>
                                    <th width="30px">Action</th>
                                </tr>
                            </thead>
                            <tbody>                                           

                            </tbody>

                        </table> 
                    </div><!-- /.box-body -->
                    <div id="sel" align="center"></div>
                </div><!-- /.box -->
            </div>
        </div>
    </div>
</section>