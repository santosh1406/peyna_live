<script type="text/javascript">
    var role_name = '0';
<?php if (in_array(strtolower($this->session->userdata('role_name')), array('admin', 'super admin'))) {
    ?>
        role_name = '1';
        
<?php } 
?>
</script>
<script type="text/Javascript" src="<?php echo base_url() ?>js/back/master/agent.js" ></script>
<!--
<div id="updatestatus" style="display:none;">
    <div style='height: 100%; width: 100%; position: fixed; left: 0px; top: 0px;background-color: #000;opacity:0.5;filter:alpha(opacity=70);z-index: 100000000;' class='progressBackground'></div>
    <div style='padding: 4px; position: absolute; top: 50%; left: 40%; width: 100%;z-index: 1000000000;'>
        <div style='' class='browseBox' id='divUpdateProgress'>
            <table cellspacing='0' cellpadding='6' border='0' style='background-color: #ffffff;' id='tblUpdate'>
                <tbody>
                    <tr style='width:40px;height:40px;vertical-align:middle;padding-top:10px;'>
                        <td style='padding-left:4px;vertical-align:middle;'>
                            <img style='border-width:0px;' src='assets/profile_image/vtbusy.gif' id='ctl00_imgProgress'>
                        </td>
                        <td style="vertical-align:middle;">
                            <span style='font-weight: bold; font-family: Calibri; font-size: medium;' id="srchBlckMsg">  Processing request!  </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>-->

<section class="content-header">
    <h1>Agent List</h1>
    <div align="right">
        <?php if ($kyc == 'Y' && $vip_role = '0') {
            if($show_data != 1) {
                ?>

                <a href="<?php echo base_url() . 'admin/agent/create_add_agent' ?>" class="btn btn-warning " role="button">Create New Agent</a>   
            <?php } else if ($kyc == 'N') {
                ?>            
                <h5 class="pull-right">
                    <marquee behavior="alternate">
                        <font color="blue">Please upload the Documents for KYC Approve.....
                        </b></font>
                    </marquee>
                </h5>
            <?php }
        } else {
            ?>
            <a href="<?php echo base_url() . 'admin/agent/create_add_agent' ?>" class="btn btn-warning " role="button">Create New Agent</a>   
        <?php }
        ?>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="commission_wrapper">
        <div class="box">
            <div class="box-header">
                <?php
                $attributes = array('class' => 'form', 'id' => 'agent_filter_view', 'name' => 'agent_filter_view', 'method' => 'post', 'onsubmit' => 'return false;');
                echo form_open('', $attributes);
                if ((lcfirst($this->session->userdata('role_name')) == lcfirst(SUPER_ADMIN_ROLE_NAME) ||  
                        lcfirst($this->session->userdata('role_name')) == lcfirst(ADMIN_ROLE_NAME) || lcfirst($this->session->userdata('role_name')) == lcfirst('B2B Report')) || 
                        (array_key_exists('agent', $this->session->userdata()) && 
                                $this->session->userdata('agent')->level < AGENT_LEVEL_LIMIT)) {
                    ?>
                    <div class="row box-title">
                        <div class="col-xs-2">
                            <input class=" form-control" type="text" id="bos_code" name="bos_code" placeholder="Rokad Code" value="<?php echo ((isset($bos_code) && $bos_code!='')?$bos_code:'');?>" >
                        </div> 
                        <div class="col-xs-2">
                            <input class=" form-control" type="text" id="vender_code" name="vender_code" placeholder="Vender Code">
                        </div> 
                        <div class="col-xs-1">
                            <input class=" form-control" type="text" id="email" name="email" placeholder="Email" >
                        </div>
                        <div class="col-xs-2">
                            <input class=" form-control" type="text" id="phone" name="phone" placeholder="Phone">
                        </div> 
                        <div class="col-xs-1">
                            <select  class=" form-control" type="text" id="kyc" name="kyc" >
                                <option value=''>KYC status</option>
                                <option value='Y'>Y</option>
                                <option value='N'>N</option>
                            </select> 
                        </div> 
                        <div class="col-xs-2">
                            <input class=" form-control" type="text" id="created_by" name="created_by" placeholder="Created By" >
                        </div> 
                        <div class="col-xs-1">
                            <select class=" form-control" id="agent_level" name="agent_level" >
                                <option value="">Select Level</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option  value="3">3</option>
                                <option value="4" >4</option>
                                <option value="5" >5</option>
                            </select> 
                        </div> 
                        <div class="col-xs-1">
                            <button class="btn btn-primary" id="agent_view_filter_btn" name="agent_view_filter_btn" type="submit">Filter</button> 
                        </div>  
                    </div>
<?php } echo form_close(); ?>

            </div>
            <div class="box-body">

                <div class="alert alert-success alert-dismissible no_margin mb10 hide" role="alert" id="alert_msg">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="body"></div>
                </div>

                    <?php if ($this->session->flashdata('msg')) { ?>
                    <div class="alert alert alert-success alert-error" style="display:block" id="msg">
                    <?php echo $this->session->flashdata('msg') ?> 
                    </div>
<?php } ?>


                <table id="agent_view" name="agent_view" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="5%">Serial No.</th>
                            <th width="10%">Agent</th>
                            <th width="10%">Rokad code</th>
                            <th width="10%">Vendor Code</th>
                            <th width="10%">Email</th>
                            <th width="10%">Phone</th>
                            <th width="5%">level</th>
                            <th width="5%">status</th>
                             <!--<th width="10%">deleted</th>--> 
                            <th width="5%">Created by</th>
                            <th width="5%">kyc</th>
                            <th width="18%">Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>		    
                <div class="clearfix"></div>
            </div><!-- /.box-body -->
            <div class="box-footer">

                <div class="clearfix"></div>
                    <!-- <code>.box-footer</code> -->
            </div><!-- /.box-footer-->
        </div>
    </div>
</section>

<script type="text/javascript">
    $().ready(function() {
        $('#msg').fadeIn().delay(4000).fadeOut();
    });
</script>