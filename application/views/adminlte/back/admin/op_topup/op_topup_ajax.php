<div id="level1"></div>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Trimax Topup</h3>
                <?php 
                $attributes = array('class' => 'form', 'id' => 'operator_filter_view', 'name'=>'operator_filter_view','method' => 'post','onsubmit'=>'return false;');
                echo form_open('', $attributes);
                ?>
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box row-fluid">
                                <input type="hidden" id="hidwayno">
                                <div class="col-xs-2">  <label> From Date</label>
                                     <input class="form-control" id="from_date" name="from_date" placeholder="From Date" type="text" value="<?php if (!empty($from_date)) echo $from_date; ?>" readonly="true" />
                                    <div id="from_dateerror"></div></div>
                                <div class="col-xs-2">  <label>To Date</label>
                                    <input class="form-control" id="to_date" name="to_date" placeholder="To Date" type="text" value="<?php if (!empty($to_date)) echo $to_date; ?>" readonly="true" />
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="box-footer2"> 

                                <div class="pull-left">
                                    <button class="btn btn-add btn-sm" name="op_generate_btn" id="show_rpt" type="submit">Generate Report</button>
                                    <button class="btn btn-add btn-sm" name="reset_btn" id="reset_btn" type="reset">Reset</button>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                        </div>
                       <div id="add_topup" align="right">
                         <?php if($this->session->userdata('role_id')!= MSRTC_USER_ROLE_ID){ ?>
                             <button class="btn btn-info btn-sm margin" id="add_operator">Add Topup</button>
                             <?php } ?>
                             <?php if($this->session->userdata('role_id') == MSRTC_USER_ROLE_ID){ ?>
                             <button class="btn btn-info btn-sm margin add_ccavenu_btn">Add Money</button>
                             <?php }?>
                        </div>
                    </div>
               
                
            </div><!-- /.box-header -->
            <?php
            if($this->session->flashdata('op_status')){ ?>
            <div class="alert alert-success alert-dismissable">
                <b>
                <?php
                    echo $this->session->flashdata('op_status');
                     $this->session->unset_userdata('op_status');
                    ?></b>
                <i class="fa fa-check"></i>
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            </div>
            <?php } ?>
            <div class="box-body table-responsive" style="margin-left: 15px;">
                <?php 
                ?>
                <div class="row box-title">
                    <div class="clearfix" style="height: 10px;clear: both;">
                </div>
                    
                <?php 
                    echo form_close(); 
                ?>
                <table id="topup_data" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Sr No.</th>
                            <th>Date</th>
                            <th>Unique Txn No. </th>
                            <th>Deposited in Bank</th>
                            <th>Amount </th>
                            <th>Commission</th>
                            <th>TDS</th>
                            <th>Net Credit Amount</th>
                            <th>Opening Balance</th>
                            <th>Closing Balance </th>
                            <th>Status</th>
                            <th>Action</th>
                         </tr>
                    </thead>
                    <tfoot>
                    <tr>
                      <th colspan="4"  style="text-align:right"   >Total:</th>
                          <th> </th>
                          <th> </th>
	                      <th> </th>
                          <th> </th>
						  <th> </th>
						  <th> </th>
                          <th> </th>
                          <th> </th>
                     </tr>
                   </tfoot>
                    <tbody>                                           

                    </tbody>

                </table></div> 
            </div>
        </div><!-- /.box -->
    </div>
</div>
<div class="modal fade popup" id="add_money_popup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php if($user_data['kyc_verify_status'] == 'N'){?>
            <h3 style="text-align: center;"> Your kyc status is pending for approval.</h3>
            <div class="modal-footer clearfix">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
            </div>
            <?php } else {?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-inr"></i>&nbsp;Add Amount To Wallet</h4>
            </div>
            <?php
                $attributes = array("method" => "POST", "id" => "add_wallet_money", "name" => "add_wallet_money","class" => "add_wallet_money");
                echo form_open(base_url().'admin/op_topup/op_pg_wallet', $attributes);
            ?>
            
                <div class="modal-body">
                      <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">Amount</span>
                            <input name="w_id" type="hidden" id="w_id" class="form-control" placeholder="id" />
                            <input name="pg_amt" type="text"  id="pg_amt" class="form-control" placeholder="Add Amount" min="1" maxlength="6" autocomplete="off"  required />&nbsp;<span id="erramountmsg" class="error"></span>
                        </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <span id="erro_msg" class="error"><span>
                      </div>
                </div>
                <div class="modal-footer clearfix">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                    <button type="submit" id="pg_confirm" class="btn btn-primary pull-left" name="pg_confirm"> Add amount To wallet</button>
                </div>
            </form>
            <?php } ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
   
$(document).ready(function(){
    
    $(document).off('change','#provider_type').on('change', '#provider_type', function(e){
        var type = $(this).val();

        $('.provider_block').addClass('hide');

        $('.provider_block').find('select option').removeAttr('selected');

        $('#'+type+'_select_box').removeClass('hide');
    });
    
    $( "#from_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
    });
    $( "#to_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
    });
});

$(document).on('change','#from_date', function () {
    checkDate ();
});
$(document).on('change','#to_date', function () {
    checkDate ();
});
function checkDate () {
    var from_date = new Date($('#from_date').val().replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
    var to_date = new Date($('#to_date').val().replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
    if( from_date > to_date ) {
        alert('Enter valid Date ragne');
        return false;
    }
}
$(document).off('click', '.add_ccavenu_btn').on('click', '.add_ccavenu_btn', function () {
        $('#add_money_popup').modal('show');
    });
</script>

<script type="text/javascript">
    <script type="text/javascript">
    $(document).ready(function () {
        
        $("#pg_amt").keypress(function (e) {
             //if the letter is not digit then display error and don't type anything
             if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                $("#erramountmsg").html("Digits Only").show().fadeOut("slow");
                       return false;
            }
        });
        
       
        
       
        $.validator.setDefaults({
          ignore: ":hidden:not(select)"
        });
    
        $("#add_wallet_money").validate({
            rules: {
                pg_amt: {
                    required: true,
                    digits: true,
                    check_numbers:true
                }
            }   
        });
        
        $.validator.addMethod("check_numbers", function (value, element) {
        var flag=false;
        var TempAmount = parseInt(value);
        if(TempAmount <=0){
        flag = false;   
        }else{
        flag = true;    
        }
        return flag;
    }, 'Amount Should Be Greater Than 0.');
    }); 
</script>   
</script>
