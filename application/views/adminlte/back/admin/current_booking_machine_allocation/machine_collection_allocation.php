<script type="text/Javascript" src="<?php echo base_url() . COMMON_ASSETS ?>js/jquery.validate.min.js" ></script>

<script type="text/Javascript" src="<?php echo base_url() ?>js/back/report/bos_ticket_details_report.js" >
</script>
<style>
    label.error, div.error, ul.error, span.error {
        color: #f00;
        font-size: 14px;
    }
    th { white-space: nowrap; }

</style>
<section class="content-header">
    <h3>Agent Collection / Allocation Details</h3>
</section>

<!-- Main content -->
<section class="content">
    <div id="wallet_wrapper">
        <div class="clearfix" style="height: 10px;clear: both;"></div>
        <div class="row"><div id="showDivAdvSearch" >
                <div class="col-xs-12">
                    <div class="box" style="padding:10px;">
                         <div class="box-body table-responsive" id="table_show">
                             <table id="op_rept" name="op_rept" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Retailer Id</th>
                                        <th>Retailer Name</th>
                                        <th>Mobile No</th>
                                        <th>Email id</th>
                                        <th>Depot Name</th>
                                        <th>Seller Point</th>
                                        <th>Waybill No</th>
                                        <th>Waybill Amount </th>
                                        <th>Waybill Allocation date</th>
                                        <th>Waybill Collection Amount</th>
                                        <th>Waybill Collection Date</th>    
                                        <th>Balance Allocation Amount in HHM</th>
                                        <th>Wallet Balance </th>
                                        <th>Status</th>
                                    </tr>
                                </thead>

                                <tbody>                                           

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <div class="clearfix" style="height: 10px;clear: both;"></div>   
        </div>
    </div>


    <script type="text/javascript">

        $(document).ready(function () {

            var tconfig = {
                "processing": true,
                "serverSide": true,
                "scrollX": true,
                "ajax": {
                    "url": BASE_URL + "report/report/bos_tkt_details",
                    "type": "POST",
                    "data": "json",
                    data   : function (d) {
                        
                         d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;
                    }
                },
                "columnDefs": [
                    {
                        "searchable": false
                    }
                ],
                "iDisplayLength": 10,
                "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
                "bFilter": false,
                "bPaginate": true,
                "bRetrieve": true,
                "paging": true,
                "searching": true,
                "order": [[2, "desc"]],
                "aoColumnDefs": [
                    {
                        //  "targets": [13],
                        "visible": true,
                        "searchable": false

                    },
                    {
                        "bSortable": false,
                        "aTargets": [0]
                    },
                    {
                        "bSortable": false,
                        "aTargets": [1]
                    },
                ],
                "oLanguage": {
                    "sProcessing": 'Processing'
                },
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {

                    $("td:first", nRow).html(iDisplayIndex + 1);

                    return nRow;

                },

            };


            oTable = $('#op_rept').dataTable(tconfig);
        
        });

    </script>

</section>