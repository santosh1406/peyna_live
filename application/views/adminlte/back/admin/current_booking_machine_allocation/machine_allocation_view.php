<style>
.fields{display:none;}
.reject{display:none;}
.row{margin-bottom:5px;} 
.amnt_btn{  float: left;height: 34px; padding: 9px 12px;width: 20%;}
#amount {display: block;float: left;width: 74%;}
#erramountmsg {float:left;width: 100%;}
.amount_wrap{width:100%;}
</style>
<h3>Waybill Allocation</h3>
<?php if($UserList){ ?>
<section class="content-header">
    <div class="row" style="margin-top:15px;">
			<div class="col-md-12">
                <!--<div class="panel-group accordion" id="accordion3">-->
                    <div class="panel panel-default">

						<div class="panel-heading">
							<h4 class="panel-title">
							   <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Advanced Search </a>
							</h4>
						</div>
						<!--<div id="collapseOne" class="panel-collapse collapse">-->
						
							<div class="panel-body" style="height:auto; overflow-y:auto;">
							   <?php
									$attributes = array("method" => "POST", "id" => "Filter_Frm", "name" => "Filter_Frm","class" => "Filter_Frm");
									echo form_open('',$attributes);
								?>
								
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label col-md-3">Search By&nbsp;</label>
												   <div class="col-md-9">
													<select name="SearchBy" id="SearchBy" class="form-control">
														<option value="">All</option>
														<?php
														if(count($UserList) > 0){
															foreach($UserList as $u){
															echo '<option value='.$u->id.'>'.$u->Name.'</option>';
															}
														}
														?>
													</select>
												   </div>                                                      
											    </div>
											</div>
											<div class="col-md-6">
												<div class="col-md-offset-3 col-md-9">
													<button type="button" class="btn btn-primary btn-sm btn-small" onclick="setFilter()">Search</button>
													<button type="button" class="btn  btn-sm btn-small" onclick="ResetFilter()">Reset</button>
												</div>
										    </div>
										</div>
										<div class="clearfix margin-top-20"></div>
								</form> 
								<div class="clearfix margin-top-20"></div>
							</div>
						<!--</div>-->


                    </div>
                <!--</div>-->
            </div>
        </div>
</section>
<?php } ?>
<!-- Main content -->
<section class="content">
    <div class="commission_wrapper">
        <div class="box">
            
            <div class="box-body">

                <div class="alert alert-success alert-dismissible no_margin mb10 hide" role="alert" id="alert_msg">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="body"></div>
                </div>
			    <?php
					$attributes = array("method" => "POST", "id" => "waybill_assign_form", "name" => "waybill_assign_form");
					echo form_open('',$attributes);
				?><div class="table-responsive">
                <table id="machine_register" name="machine_register" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th  width="10%">Sr No.</th>
							<th>Name</th>
							<th>Agent Code</th>
							<th>Seller Location</th>
							<th>Depot Name</th>
							<!-- <th>Wallet Balance</th> -->
							<!-- <th>Amount</th>-->
							<th>Action</th> 
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
                </form>		    
                <div class="clearfix"></div>
            </div><!-- /.box-body -->
            <div class="box-footer">

                <div class="clearfix"></div>
                    <!-- <code>.box-footer</code> -->
            </div><!-- /.box-footer-->
        </div>
    </div>
</section>
<div class="modal fade popup" id="waybill_allow_popup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
		    
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-inr"></i>&nbsp;Add Amount To Waybill</h4>
			</div>
			<?php
				$attributes = array("method" => "POST", "id" => "waybill_amnt", "name" => "waybill_amnt");
				echo form_open(base_url().'admin/fund_request/add_wallet_money', $attributes);
            ?>
            
                <div class="modal-body">
					  <div class="form-group">
						<div class="input-group amount_wrap">
							<span class="input-group-addon amnt_btn">Amount</span>
							<input name="agent_code" type="hidden" id="agent_code" class="form-control" />
							<input name="amount" type="text"  id="amount" class="form-control" placeholder="Wabill Amount" min="1" maxlength="6" autocomplete="off"  required />&nbsp;<span id="erramountmsg" class="error"></span>
						</div>
					 </div>
				</div>
				<div class="modal-footer clearfix">
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
					<button type="submit" id="pg_confirm" class="btn btn-primary pull-left" name="pg_confirm"> Add Waybill Amount</button>
				</div>
            </form>
			
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    $(document).ready(function () {
		
		$("#amount").keypress(function (e) {
			 //if the letter is not digit then display error and don't type anything
			 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				//display error message
				$("#erramountmsg").html("Digits Only").show().fadeOut("slow");
					   return false;
			}
		});
		
       
		
	   
		$.validator.setDefaults({
          ignore: ":hidden:not(select)"
        });
    
		$("#waybill_amnt").validate({
			rules: {
				pg_amt: {
					required: true,
					digits: true,
					check_numbers:true
				}
			}	
		});
		
		$.validator.addMethod("check_numbers", function (value, element) {
		var flag=false;
		var TempAmount = parseInt(value);
		if(TempAmount <=0){
		flag = false;	
		}else{
		flag = true;	
		}
		return flag;
	}, 'Amount Should Be Greater Than 0.');
	});	
</script>	
<script type="text/javascript">
    var frm = document.Filter_Frm;
    
    $(document).ready(function () {
       	DatatableRefresh();
    });
	   
	function DatatableRefresh() {
		
		$('#machine_register').dataTable({
			"aoColumnDefs": [ {"bSortable": true, "aTargets": [0,5] } ], 
			"deferRender": true,
			"bProcessing": true,
			"bServerSide": true,
			"order": [[ 0, 'desc' ]],
			"lengthMenu": [
				[10, 50, 100, -1],
				[10, 50, 100, "All"] // change per page values here
			],
			"bScrollCollapse": true,
			"bAutoWidth": true,
			"sPaginationType": "full_numbers",
			"bDestroy": true,
			"oLanguage": {
                "sProcessing": '&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url().BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>' 
            },
			"sAjaxSource": "<?php echo site_url().'admin/cb_machine_allocation/DataTableWaybillAllocation'; ?>",
			"fnServerData": function (sSource, aoData, fnCallback) {
				    aoData.push({name: 'SearchBy', value:$('#SearchBy').val()});
                    $.getJSON(sSource, aoData, function (json) {
					fnCallback(json);
				});
			},
			"fnRowCallback": function (nRow, aData, iDisplayIndex) {

				return nRow;
			},
			"fnFooterCallback": function (nRow, aData) {
			}
		});

	}

    function ResetFilter() {
		frm.reset();
		DatatableRefresh();
	}
	function setFilter() {
		DatatableRefresh();
	}	
	
	function WaybilAllocation(Id,Amnt){
		//$('#waybill_allow_popup').modal('show');
	
	// var amount = $('#amount'+Id).val();
	// if(amount !='' && amount > 0 && amount < Amnt)
	
	// {
	 if(Id !='')
	    {	
	          
	        var detail = {};
	        var div = "";
	        var ajax_url = 'admin/cb_machine_allocation/assign_waybill';
	        var form = 'waybill_assign_form';
	        var err = "";
	        detail['user_id'] = Id;
	        detail['amount'] = amount;
			
	        get_data(ajax_url, form, div, detail, function (response)
	        {
	        	
	            if (response.flag == '@#success#@')
	            {
	            	
					var imp_val=response.encrypt_data;
					//window.open("http://10.50.30.113/msrtc_new/index.php/admin/mlogin/cblogin/"+imp_val);
                                     window.open(msrtc_cb_url+imp_val);
                                      //location.href=msrtc_cb_url+imp_val;
					
					
	               
	            }
	            else{
						alert('Allocation not possible cause of some N/W issue');
						return false;
			}
	        }, '', false)
	    }
 //    }else
 //    {
	// 	alert('Please Enter Waybill Amount Greater Than 0 And Less Than Current Balance');
	// }		
	}
</script>