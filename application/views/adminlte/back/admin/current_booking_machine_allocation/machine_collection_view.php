<style>
.fields{display:none;}
.reject{display:none;}
.row{margin-bottom:5px;} 
</style>
<h3 style="margin-left: 10px;">Waybill Collection</h3>
<?php if($UserList){ ?>
<section class="content-header">
    <div class="row" style="margin-top:15px;">
			<div class="col-md-12">
                <!--<div class="panel-group accordion" id="accordion3">-->
                    <div class="panel panel-default">

						<div class="panel-heading">
							<h4 class="panel-title">
							   <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Advanced Search </a>
							</h4>
						</div>
						<!--<div id="collapseOne" class="panel-collapse collapse">-->
						
							<div class="panel-body" style="height:auto; overflow-y:auto;">
							   <?php
									$attributes = array("method" => "POST", "id" => "Filter_Frm", "name" => "Filter_Frm","class" => "Filter_Frm");
									echo form_open('',$attributes);
								?>
								
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label col-md-3">Search By&nbsp;</label>
												   <div class="col-md-9">
													<select name="SearchBy" id="SearchBy" class="form-control">
														<option value="">All</option>
														<?php
														if(count($UserList) > 0){
															foreach($UserList as $u){
															echo '<option value='.$u->id.'>'.$u->Name.'</option>';
															}
														}
														?>
													</select>
												   </div>                                                      
											    </div>
											</div>
											<div class="col-md-6">
												<div class="col-md-offset-3 col-md-9">
													<button type="button" class="btn btn-primary btn-sm btn-small" onclick="setFilter()">Search</button>
													<button type="button" class="btn  btn-sm btn-small" onclick="ResetFilter()">Reset</button>
												</div>
										    </div>
										</div>
										<div class="clearfix margin-top-20"></div>
								</form> 
								<div class="clearfix margin-top-20"></div>
							</div>
						<!--</div>-->


                    </div>
                <!--</div>-->
            </div>
        </div>
</section>
<?php } ?>
<!-- Main content -->
<section class="content">
    <div class="commission_wrapper">
        <div class="box">
            
            <div class="box-body">

                <div class="alert alert-success alert-dismissible no_margin mb10 hide" role="alert" id="alert_msg">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="body"></div>
                </div>
			    <?php
					$attributes = array("method" => "POST", "id" => "machine_collection_form", "name" => "machine_collection_form");
					echo form_open('',$attributes);
				?><div class="table-responsive">
                <table id="machine_register" name="machine_register" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th  width="10%">Sr No.</th>
							<th>Name</th>
							<th>Agent Code</th>
							<th>Seller Point</th> 
							<th>Depot Name</th>
							<th>Current Balance</th>
							<th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table></div>		    
                </form>		    
                <div class="clearfix"></div>
            </div><!-- /.box-body -->
            <div class="box-footer">

                <div class="clearfix"></div>
                    <!-- <code>.box-footer</code> -->
            </div><!-- /.box-footer-->
        </div>
    </div>
</section>
<script type="text/javascript">
    var frm = document.Filter_Frm;
    
    $(document).ready(function () {
       	DatatableRefresh();
    });
	   
	function DatatableRefresh() {
		
		$('#machine_register').dataTable({
			"aoColumnDefs": [ {"bSortable": true, "aTargets": [0,6] } ], 
			"deferRender": true,
			"bProcessing": true,
			"bServerSide": true,
			"order": [[ 0, 'desc' ]],
			"lengthMenu": [
				[10, 50, 100, -1],
				[10, 50, 100, "All"] // change per page values here
			],
			"bScrollCollapse": true,
			"bAutoWidth": true,
			"sPaginationType": "full_numbers",
			"bDestroy": true,
			"oLanguage": {
                "sProcessing": '&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url().BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>' 
            },
			"sAjaxSource": "<?php echo base_url().'admin/cb_machine_allocation/DataTableMachineCollection'; ?>",
			"fnServerData": function (sSource, aoData, fnCallback) {
				    aoData.push({name: 'SearchBy', value:$('#SearchBy').val()});
                                    $.getJSON(sSource, aoData, function (json) {
					fnCallback(json);
				});
			},
			"fnRowCallback": function (nRow, aData, iDisplayIndex) {

				return nRow;
			},
			"fnFooterCallback": function (nRow, aData) {
			}
		});

	}

    function ResetFilter() {
		frm.reset();
		DatatableRefresh();
	}
	function setFilter() {
		DatatableRefresh();
	}	
	
	function WaybilCollection(Id){
		
	 if(Id !='')
	    {	
	          
	        var detail = {};
	        var div = "";
	        var ajax_url = 'admin/cb_machine_allocation/collection_machine';
	        var form = 'machine_collection_form';
	        var err = "";
	        detail['user_id'] = Id;
			
	        get_data(ajax_url, form, div, detail, function (response)
	        {
	        	
	            if (response.flag == '@#success#@')
	            {
	            	
					var imp_val=response.encrypt_data;
					window.open(msrtc_cb_url+imp_val,'_blank');
                                      //  window.open("http://10.50.30.113/msrtc_new/index.php/admin/mlogin/cblogin/"+imp_val,'_blank');
	               
	            }
	            else{
						alert('Allocation not possible cause of some N/W issue');
						return false;
			}
	        }, '', false)
	    }	
	}
</script>