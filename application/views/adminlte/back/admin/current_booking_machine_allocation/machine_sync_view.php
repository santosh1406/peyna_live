<!-- Main content -->
<section class="content">
    <div class="box-header">
        <h3 class="box-title">Machine Master Sync</h3>
    </div>
    <!-- /.box-header --> 
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <img style="display: none;" src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>
                    <table id="revenue_division" class="table table-bordered" ><!-- style="background-color:#ccd9ff;"> -->
                        <thead>
                            <tr style="background-color:#367fa9;color:#fff;">
                                <th>Sr. No.</th>
                                <th>IMEI_NO</th>
                                <th>IMEI_NO_2</th>
                                <th>Seller Point</th>
                                <th>Portal Status</th>
                                <th>Machine Status</th>
                                <th>Action</th>
                                <th style="display: none;">SWMN</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($UserList)) {
                                $i = 1;
                                foreach ($UserList as $key => $value) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $value['IMEI_NO']; ?></td>
                                        <td><?php echo $value['IMEI_NO_2']; ?></td>
                                        <td><?php echo $value['seller_point']; ?></td>
                                        <td><?php echo ($value['is_status'] == 'P' || $value['is_status'] == 'Y') ? "Done" : "Not done"; ?></td>
                                        <td><?php echo ($value['is_status'] == 'Y') ? "Done" : "Not done"; ?></td>
                                        <td></td>
                                        <td style="display: none;"><?php echo $value['SW_Machine_No']; ?></td>
                                    </tr>
                                    <?php $i++; ?>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

<script>
    $(document).ready(function() {
        
        var tconfig = {
            'searching' : false,
            'scrollX' : false,
            'ordering' : false,
            'aLengthMenu' : [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            'language' : {
                "info":''
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {			
                var info = $(this).DataTable().page.info();
                var str = aData[6]; 
            
                if(str.search("TOTAL") == -1) {
                    var swmnCode = aData[7];     
                    var btn = '';
                    btn += '<button type="button" style="padding: 0 4px;color:#fff;" class="pull-left view_btn btn-primary btn-sm btn-small"  ref="" rel="tooltip" data-placement="top" data-original-title="Master Sync" title="Master Sync" onclick="WaybilCollection(\'' + swmnCode + '\');">Master Sync</button>';
                    $("td:eq(6)", nRow).html(btn);
                    
                }
                return nRow;
            }
        };
        
        data = $('#revenue_division').DataTable(tconfig);
        
    });
    
    function WaybilCollection(Id){
        if(Id !='')
        {	
            $.ajax({
                type: 'POST',
                url: BASE_URL+"admin/Cb_machine_master_sync/master_synch",
                data: { "id" : Id},
                success: function(resultData)
                {
                    if(resultData == 'success'){
                        alert("Master Sync Successfully");
                    } else if(resultData == 'exist') {
                        if (confirm("Already Synched. Do you want to overwrite?")) {
                            //alert("You pressed OK!");
                            $.ajax({
                                type: 'POST',
                                url: BASE_URL+"admin/Cb_machine_master_sync/master_resynch",
                                data: { "id" : Id},
                                success: function(result)
                                {
                                    if(result == 'success'){
                                        alert("Master Sync Successfully");
                                    } else {
                                        alert("Failed Synchronized Process.");
                                    }
                                }
                            });
                        }
                    } else {
                        alert("Failed Synchronized Process.");
                    }
                }
            });
        }
    }
</script>
