<div class="col-md-12">
                            <!-- Custom Tabs -->
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#tab_1">Master Synch</a></li>
                                    <li class=""><a data-toggle="tab" href="#tab_2">Routes Synch</a></li>
                                    <li class=""><a data-toggle="tab" href="#tab_3">Waybill allocation</a></li>
                                     <li class=""><a data-toggle="tab" href="#tab_4">Duty Start</a></li>
                                     <li class=""><a data-toggle="tab" href="#tab_5">Erase Machine</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div id="tab_1" class="tab-pane active">
                                      <div class="form-group-sm">
                                <input class="btn btn-primary"  type="button" onclick="insertmaster('depot_master');" value="Insert Depot Master" tabindex="10" name="depot" id="depot">
                                <label id='depot_master_status'></label>
                            </div>
                            <br clear='all'>
                            <div class="form-group-sm">
                                <input class="btn btn-primary"  type="button" onclick="insertmaster('gprs_master');" value="Insert GPRS Master" tabindex="11" name="gprs" id="gprs">
                                <label id='gprs_master_status'></label>
                            </div>
                            <br clear='all'>
                            <div class="form-group-sm">
                                <input class="btn btn-primary"  type="button" onclick="insertmaster('activity_log_master');" value="Insert Activity Log Master" tabindex="12" name="activitylog" id="activitylog">
                                <label id='activity_log_master_status'></label>
                            </div>
<!--                            <br clear='all'>
                            <div class="form-group-sm">
                                <input class="btn btn-primary"  type="button" onclick="insertmaster('station_change_log');" value="Insert Station Change Master" tabindex="13" name="stationchange" id="stationchange">
                                <label id='station_change_log_status'></label>
                            </div>-->
                            <br clear='all'>
                            <div class="form-group-sm">
                                <input class="btn btn-primary"  type="button" onclick="insertmaster('bus_types');" value="Insert Bus Services" tabindex="14" name="busservice" name="busservice">
                                <label id='bus_types_status'></label>
                            </div>
                            <br clear='all'>
                            <div class="form-group-sm">
                                <input class="btn btn-primary"  type="button" onclick="insertmaster('service_operator');" value="Insert Service Operators" tabindex="15" name="serviceop" id="serviceop">
                                <label id='service_operator_status'></label>
                            </div>
 <br clear='all'>
                            <div class="form-group-sm">
                                <input class="btn btn-primary"  type="button" onclick="insertmaster('concession_table');" value="Insert Concession" tabindex="16" name="concession" id="concession">
                                <label id='concession_table_status'></label>
                            </div>
                            <br clear='all'>
                            <div class="form-group-sm">
                                <input class="btn btn-primary"  type="button" onclick="insertmaster('cause_master');" value="Insert Cause Master" tabindex="16" name="cause_master" id="cause_master">
                                <label id='cause_master_table_status'></label>
                            </div>
                            <br clear='all'>
                            <div class="form-group-sm">
                                <input class="btn btn-primary"  type="button" onclick="insertmaster('luggage_master');" value="Insert luggage Master" tabindex="16" name="luggage_master" id="luggage_master">
                                <label id='luggage_master_table_status'></label>
                            </div>
                                    </div><!-- /.tab-pane -->
                                    
                                    
                                     <!--- start Routes synch   --->
                                    <div id="tab_2" class="tab-pane">
                                        The European languages are members of the same family. Their separate existence is a myth.
                                        For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ
                                        in their grammar, their pronunciation and their most common words. Everyone realizes why a
                                        new common language would be desirable: one could refuse to pay expensive translators. To
                                        achieve this, it would be necessary to have uniform grammar, pronunciation and more common
                                        words. If several languages coalesce, the grammar of the resulting language is more simple
                                        and regular than that of the individual languages.
                                    </div>
                                     <!--- End Routes synch   --->
                                     
                                     
                                     
                                    <!--- start Waybill allocation   --->
                                    
                                    <div id="tab_3" class="tab-pane">
                                        
                                         <div class="col-sm-8">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Waybill Allocation</h3>
                </div>
                <?php 
                if($this->formData['waybillDetails']->WAYBILL_NO==""){
                ?>                 
                <div class="panel-body">
                    <form action="index.php?page=waybill_allocation" method="POST" name="waybillAllocationForm" id="waybillAllocationForm">
                        <input type="hidden" value="submit" name="method">
                        <table align="center" width="70%" cellspacing="0" cellpadding="2" border="0" class='table'>
                            <tbody>
                                <tr>
                                    <td style="">
                                        <table width="100%" cellspacing="0" cellpadding="2" border="0" class='table table-responsive table-bordered  font-small13'>
                                            <tbody>
                                                <tr>
                                                    <td align="left" style="" colspan="2">
                                                        <label>Booking Type</label> &nbsp; &nbsp;
                                                        &nbsp; &nbsp;Stand Booking:&nbsp; &nbsp;<input type="radio" onclick="createStandbooking()" value="S" name="booking_type" id="bcType"  checked="checked" > 
                                                    </td>
                                                </tr>                                                
                                                <tr>
                                                    <td><label>Agent Name</label></td>
                                                    <td>
                                                        <?php echo $_SESSION['FNAME']." ".$_SESSION['LNAME']; ?>
                                                        <input type="hidden" name="agentsid" id="agentsid" value="<?php echo $_SESSION['AGENTID']; ?>"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><label>Duty Date</label></td>
                                                    <td>
                                                        <?php echo date('d-m-Y'); ?>
                                                    </td>
                                                </tr>
                                                <tr id="sb_operator">
                                                    <td><label>Operator Code</label></td>
                                                    <td>
                                                        <?php echo $_SESSION['OP_CODE']; ?>
                                                        <input type="hidden" name="selOperator" id="selOperator" value="<?php echo $_SESSION['OP_CODE']; ?>"/>                                                        
<!--                                                        <select class="form-control" type="text" id="selOperator" value="" tabindex="5" maxlength="45" name="selOperator">
                                                            <option value="op1">Operator 1 </option>
                                                            <option value="op2">Operator 2 </option>
                                                        </select>    -->
                                                    </td>
                                                </tr> 
                                                <tr id="sb_location">
                                                    <td><label>Booking Location</label></td>
                                                    <td>
<!--                                                        <input type="hidden" id='bookingLocation' name='bookingLocation' value="<?php echo $_SESSION['BUS_STOP']; ?>"/>
                                                        <p id="carddetailsid"><?php //echo trim($_SESSION['BUS_STOP']); ?></p> -->
                                                      <?php  echo $this->formData['sellersPointDetails'][0]->bus_stop_name ?>
<!--                                                        <select class="form-control input-sm font-small13" name="bookingLocation" id="bookingLocation">
                                                            
                                                            <option value="">--- Select Booking Location ---</option>
                                                        </select>                                                         -->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><label>Agent Balance</label></td>
                                                    <td>
                                                        <?php 
                                                        echo $this->formData['sellersPointDetails']->bus_stop_name;
                                                        echo (isset($_SESSION['TOTAL_BALANCE'])?$_SESSION['TOTAL_BALANCE']:0);
							//	 echo (isset($_SESSION['BALANCE'])?$_SESSION['BALANCE']:0); ?>
                                                    </td>
                                                </tr>     
                                                <tr>
                                                    <td><label>ETIM No.</label></td>
                                                    <td>
                                                        <?php echo (isset($this->formData['machineregdata']->etim_no)?$this->formData['machineregdata']->etim_no:""); ?>
                                                        <input type="hidden" id="etimNo" value="<?php echo (isset($this->formData['machineregdata']->etim_no)?$this->formData['machineregdata']->etim_no:""); ?>" tabindex="7" maxlength="10" name="etimNo" class="ui-autocomplete-input" autocomplete="off">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="" colspan="2">
                                         <?php 
                                      
                                        if(isset($this->formData['less_bal']) && $this->formData['less_bal']=='N') { 
                                                  
                                                    echo 'In '.ucfirst($_SESSION['OP_CODE']).' operator Insufficient balance';
                                        }else{ ?>
                                        <?php 
                                        echo $this->formData['sellersPointDetails']->bus_stop_name;
                                        if(isset($_SESSION['BUS_STOP']) && trim($_SESSION['BUS_STOP']) !='') { ?>
                                                    <input type="button" id="4" class="btn btn-primary" onclick="javascript:formSubmit()" value="Submit" tabindex="7" name="submit">
                                        <?php  }else{
                                                    echo 'Please assign the Seller Point';
                                        }
                                        }?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
                <?php
                }else{
                ?>                
                <div class="panel-body">
                    Waybill already assigned To Agent. Please Start duty or do the Collection.
                </div>
                <?php
                }
                ?>                
            </div>
        </div>
                                    </div>
                                    
                                    
                                    <!--- End Waybill allocation   --->
                                    <div id="tab_4" class="tab-pane">
                                        The European languages are members of the same family. Their separate existence is a myth.
                                        For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ
                                        in their grammar, their pronunciation and their most common words. Everyone realizes why a
                                        new common language would be desirable: one could refuse to pay expensive translators. To
                                        achieve this, it would be necessary to have uniform grammar, pronunciation and more common
                                        words. If several languages coalesce, the grammar of the resulting language is more simple
                                        and regular than that of the individual languages.
                                    </div>
                                    <div id="tab_5" class="tab-pane">
                                        The European languages are members of the same family. Their separate existence is a myth.
                                        For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ
                                        in their grammar, their pronunciation and their most common words. Everyone realizes why a
                                        new common language would be desirable: one could refuse to pay expensive translators. To
                                        achieve this, it would be necessary to have uniform grammar, pronunciation and more common
                                        words. If several languages coalesce, the grammar of the resulting language is more simple
                                        and regular than that of the individual languages.
                                    </div>
                                </div><!-- /.tab-content -->
                            </div><!-- nav-tabs-custom -->
                        </div>