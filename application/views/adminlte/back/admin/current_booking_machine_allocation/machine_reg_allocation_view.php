
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3 style="font-family: Tahoma">
            &nbsp; Machine Registration Form
        </h3>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-8">
                <div class="box">
                    <div class="box">
					
						<?php if($user_data['kyc_verify_status'] == 'N'){?>
						<h3 style="text-align: center;"> Your kyc status is pending for approval.</h3>
						<div class="modal-footer clearfix">
						</div>
						<?php } else {?>
					    <?php
							$attributes = array("method" => "POST", "id" => "prov_create_form", "name" => "prov_create_form","class" => "prov_create_form");
							echo form_open(base_url().'admin/cb_machine_allocation/register_machine', $attributes);
						?>
                             <div class="form-group">

                                <label class="col-lg-3 control-label" for="amount" >Agent Name</label>
                                <div class="col-lg-6">
                                    <?php echo '<b>'.$userdata[0]->first_name .' '.$userdata[0]->last_name.'</b>'; ?>
                                     <input name="Agent_id" type="hidden" id="Agent_id" value="<?php echo $id; ?>" class="form-control" placeholder="" maxlength="14">
                                   
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>                
                            <div class="form-group">

                                <label class="col-lg-3 control-label" for="amount">Unique No</label>
                                <div class="col-lg-6">
                                    <?php if($machine_data['unique_no'] !='') {?>
                                    <input name="uniqid" type="text" id="uniqid" value="<?php echo $machine_data['unique_no']; ?>" class="form-control" placeholder="Unique No" maxlength="15" readonly>
                                    <?php } else {?>
                                     <input name="uniqid" type="text" id="uniqid" value="" class="form-control" placeholder="Unique No" maxlength="15">
                                    <?php }?>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">

                                <label class="col-lg-3 control-label" for="amount">Version Number</label>
                                <div class="col-lg-6">
                                    <?php //echo uniqid();die;?>
                                    <input name="Version_number" type="text" id="Version_number" value="" class="form-control" placeholder="<?php echo $machine_data['version_number']; ?>" maxlength="6" readonly="">
                                </div>
                            </div>
                             <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">

                                <label class="col-lg-3 control-label" for="amount">Version date</label>
                                <div class="col-lg-6">
                                    <?php //echo uniqid();die;?>
                                    <input name="Version_date" type="text" id="Version_date" value="" class="form-control" placeholder="<?php echo $machine_data['version_number']; ?>" maxlength="6" readonly="">
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-4">
                                    <?php if($machine_data['unique_no'] =='') { ?>
                                        <button class="btn btn-primary" id="save_prov_data" name="save_prov_data" type="submit">Submit</button>
                                        <?php } ?> 
                                        <a class="btn btn-primary" href="<?php echo base_url('admin/cb_machine_allocation') ?>" type="button">Back</a> 
                                    </div>
                                </div>

                        </form>
						
						<?php } ?>		
						
                    </div><!-- /.box-body -->
                <!--</div> /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<script>
    $(document).ready(function() {
        

        });

</script>
<script type="text/javascript">
 $(document).off('keypress', '#uniqid').on('keypress', '#uniqid', function(e) 
        {
           var regex = new RegExp("^[0-9\b]+$");//accept only _
           var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
             key=e.charCode? e.charCode : e.keyCode;
            if(key==9 )
            {
                return true; 
            }
            if (regex.test(str)) 
            {
                return true;
            }
            e.preventDefault();
            return false;
        });
</script>
<script>
    $(document).ready(function () {
        $('#prov_create_form').validate({ 
             rules: {
                uniqid: {
                    required: true,
                    number:true
                 }
            },
            messages: {
                uniqid: "Please enter valid Unique ID.",
            },
            submitHandler: function () { 
                submit.form();
            }
        });
    });
</script>
