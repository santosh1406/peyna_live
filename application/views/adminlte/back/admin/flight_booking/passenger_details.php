<!-- Inner Wrapper Start -->
<link href="<?php echo base_url() . BACK_ASSETS ?>css/search_style/style.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/jquery.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/qrcode.js" type="text/javascript"></script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/jquery.qrcode.js" type="text/javascript"></script> -->
<?php //echo "<pre>"; print_r($this->session->userdata()); die();?>
<div id="page-wrapper">
    <?php //echo "<pre>"; print_r($this->session->userdata()); die();?>
    <!-- Signin Content Start -->
    <section class="inner-wrapper">
        <div class="container col-content">
            <div class="row">
                <div class="col-md-8">
                    <div class="box-style2 minh450">
                        <h3>Payment Summary</h3>
                        <div class="payment-summary">
                            <form action="<?php echo base_url('admin/search_bus/temp_booking')?>" method="POST"  id="booking_process" method="post" accept-charset="utf-8" novalidate="novalidate">
                                <div class="steps mb10">
                                    <ul class="list-inline">
                                        <li><div class="square-number-bg">1</div> Select Seat & Boarding Point</li>
                                        <li class="active"><div class="square-number-bg-active">2</div> Travelers & Payment </li>
                                        <li><div class="square-number-bg">3</div> Booking Payment</li>
                                    </ul>
                                </div>

                                <h4 class="purple">Onward Journey Details</h4>
                                <?php
                                $seats = $seatlist = array();
                                $onwardJourny = $this->session->userdata['onwardJourny'];
                                $seats = $onwardJourny['onwardJourny']['seats'];
                                
                                //echo "<pre>"; print_r($seats);
                                if (count($seats) > 0) :
                                   $i = 0; foreach ($seats as $berthno => $seatlist) :
                                        if(count($seats) == 2){
                                            echo ($berthno > 0) ? "<h6>Upper Berth</h6>" : "<h6>Lower Berth</h6>";
                                        }
                                       // echo "<pre>"; print_r($seatlist); die();
                                        foreach ($seatlist as $k => $value) :  //echo "<pre>"; print_r(es($k)); die(); ?>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input name="seat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][name]"  id="psgr[<?php echo es($berthno); ?>][<?php echo es($k); ?>][name]" type="text" class="col-md-12 form-control psgr_name" placeholder="Name" >
                                                    <input type="hidden" name="seat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][seat_no]"  id="seat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][seat_no]"  class="col-md-12 form-control" value="<?php echo es($value['data_seat_no']); ?>" >
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <input type="text" name="seat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][age]"  id="psgr[<?php echo es($berthno); ?>][<?php echo es($k); ?>][age]" class="col-md-12 form-control psgr_age" placeholder="Age" min="1" max="3" required>
                                                </div>
                                            </div>
                                           <!-- <div class="col-md-2 col-xs-4">
                                                <div class="checkbox checkbox-info checkbox-circle pull-left">
                                                    <input id="seat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][sex]" class="checkbox-circle radioBtnClass"  type="radio" name="seat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][sex]"  value="M" <?php echo $value['is_ladies']; ?>>
                                                    <label>Male</label>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-4">
                                                <div class="checkbox checkbox-info checkbox-circle pull-left">
                                                    <input id="seat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][sex]" class="checkbox-circle radioBtnClass" type="radio" id="seat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][sex]" value="F" <?php echo ($value['is_ladies'] === 'true') ? 'checked' : ''; ?>>
                                                    <label>Female</label>
                                                </div>
                                            </div> -->
                                           <div class="col-sm-4 gender disp_radio">
                                              <label class="checkbox checkbox-info checkbox-circle pull-left">
                                                  <input type="radio" class="gender" id="seat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][sex]" name="seat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][sex]" value="M" <?php echo ($value['is_ladies'] == 'false' ? checked:'');?>> 
                                                  Male
                                              </label>
                                              <label class="checkbox checkbox-info checkbox-circle pull-left" style="margin-top: 10px;">
                                                  <input type="radio" class="gender" id="seat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][sex]" name="seat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][sex]" value="F" <?php echo ($value['is_ladies'] == 'false' ? '':checked);?>>
                                                  Female
                                              </label>
                                            </div>
                                            <div class="col-md-2 col-xs-4">
                                                <div class="form-group mt10">
                                                    <img src="<?php echo base_url() . BACK_ASSETS ?>img/seats.png" alt=""> <?php echo $value['data_seat_no']; ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <select class="form-control" id="" value="">
                                                        <option value="0" selected="">Select Quota</option>
                                                        <option value="">Senior Citizen</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <select class="form-control" id="" value="">
                                                        <option value="0" selected="">Select</option>
                                                        <option value="">Aadhard Card</option>
                                                        <option value="">Driving License</option>
                                                        <option value="">Passport</option>
                                                        <option value="">Tahsildar</option>
                                                        <option value="">Voting Card</option>
                                                        <option value="">Pan Card</option>
                                                        <option value="">Depot Manager</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input name="concession" type="text" class="col-md-12 form-control" placeholder="Concession P">
                                                </div>
                                            </div>
                                        </div>
                                <?php endforeach; endforeach;
                            endif; ?>
                                
                                
                                <?php
                                $seats = $seatlist = array();
                                $returnJourny = $this->session->userdata['returnJourny'];
                                $rseats = $returnJourny['returnJourny']['seats'];
                                
                                
                                if (count($rseats) > 0) : echo ' <h4 class="purple">Return Journey Details</h4>';
                                   $i = 0; foreach ($rseats as $berthno => $seatlist) :
                                        if(count($rseats) == 2){
                                            echo ($berthno > 0) ? "<h6>Upper Berth</h6>" : "<h6>Lower Berth</h6>";
                                        }
                                       
                                       // echo "<pre>"; print_r($seatlist); die();
                                        foreach ($seatlist as $k => $value) :  //echo "<pre>"; print_r(es($k)); die(); ?>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input name="rseat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][name]"  id="rpsgr[<?php echo es($berthno); ?>][<?php echo es($k); ?>][name]" type="text" class="col-md-12 form-control psgr_name" placeholder="Name" >
                                                    <input type="hidden" name="rseat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][seat_no]"  id="rseat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][seat_no]"  class="col-md-12 form-control" value="<?php echo es($value['data_seat_no']); ?>" >
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <input type="text" name="rseat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][age]"  id="rpsgr[<?php echo es($berthno); ?>][<?php echo es($k); ?>][age]" class="col-md-12 form-control psgr_age" placeholder="Age" min="1" max="3" required>
                                                </div>
                                            </div>
                                           <!-- <div class="col-md-2 col-xs-4">
                                                <div class="checkbox checkbox-info checkbox-circle pull-left">
                                                    <input id="seat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][sex]" class="checkbox-circle radioBtnClass"  type="radio" name="seat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][sex]"  value="M" <?php echo $value['is_ladies']; ?>>
                                                    <label>Male</label>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-xs-4">
                                                <div class="checkbox checkbox-info checkbox-circle pull-left">
                                                    <input id="seat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][sex]" class="checkbox-circle radioBtnClass" type="radio" id="seat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][sex]" value="F" <?php echo ($value['is_ladies'] === 'true') ? 'checked' : ''; ?>>
                                                    <label>Female</label>
                                                </div>
                                            </div> -->
                                           <div class="col-sm-4 gender disp_radio">
                                              <label class="checkbox checkbox-info checkbox-circle pull-left">
                                                  <input type="radio" class="gender" id="rseat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][sex]" name="rseat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][sex]" value="M" <?php echo ($value['is_ladies'] == 'false' ? checked:'');?>> 
                                                  Male
                                              </label>
                                              <label class="checkbox checkbox-info checkbox-circle pull-left" style="margin-top: 10px;">
                                                  <input type="radio" class="gender" id="rseat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][sex]" name="rseat[<?php echo es($berthno); ?>][<?php echo es($k); ?>][sex]" value="F" <?php echo ($value['is_ladies'] == 'false' ? '':checked);?>>
                                                  Female
                                              </label>
                                            </div>
                                            <div class="col-md-2 col-xs-4">
                                                <div class="form-group mt10">
                                                    <img src="<?php echo base_url() . BACK_ASSETS ?>img/seats.png" alt=""> <?php echo $value['data_seat_no']; ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <select class="form-control" id="" value="">
                                                        <option value="0" selected="">Select Quota</option>
                                                        <option value="">Senior Citizen</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <select class="form-control" id="" value="">
                                                        <option value="0" selected="">Select</option>
                                                        <option value="">Aadhard Card</option>
                                                        <option value="">Driving License</option>
                                                        <option value="">Passport</option>
                                                        <option value="">Tahsildar</option>
                                                        <option value="">Voting Card</option>
                                                        <option value="">Pan Card</option>
                                                        <option value="">Depot Manager</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input name="concession" type="text" class="col-md-12 form-control" placeholder="Concession P">
                                                </div>
                                            </div>
                                        </div>
                                <?php endforeach; endforeach;
                            endif; ?>
                                <h4 class="purple">Personal Details</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="email" id="email" name="email" class="col-md-12 form-control" placeholder="Email Address">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="mobile" id="mobile" name="mobile" class="col-md-12 form-control" pattern="\d{3}[\-]\d{3}[\-]\d{4}" placeholder="Mobile Number" minlength="10" maxlength="10" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6 col-xs-12 col-sm-offset-3">
                                        <div class="form-group">
                                            <span style="display: none" id="operator_name"  ><?php echo $this->session->userdata['operator_name']['operator_name']; ?></span>
                                            <input type="submit" class="btn btn-purple btn-lg btn-block login-button txt-w col-sm-4 mt20" value="Confirm Booking" name="confirm_booking" id="confirm_bookingd">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="box-style2 minh450">
                        <h3>Ticket Details</h3>
                        <div class="ticket-details mb20">
                            <ul class="tabs full-w">
                                <li class="active">
                                    <a href="#onward-journey">
                                        <i class="fa fa-bus fnt16"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <h5 class="text-center">Onwards Journey</h5>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading padding0" role="tab" id="headingOne">
                                    <h4 class="panel-title bg-purple color-w">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <i class="droparrow glyphicon glyphicon-chevron-down"></i>
                                            Booking Details
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <article class="bus-booking-details">
                                            <h5 class="travel-name"><?php echo $this->session->userdata['operator_name']['operator_name']; ?></h5>
                                            <div class="row">
                                                <div class="col-md-6 col-xs-6">
                                                    <span class="purple">From</span> <br/>
                                                    <small><?php echo trim($this->session->userdata['onwardJourny']['onwardJourny']['boarding_stop_name']); ?></small>
                                                </div>
                                                <div class="col-md-6 col-xs-6">
                                                    <span class="purple">To</span> <br/>
                                                    <small><?php echo trim($this->session->userdata['onwardJourny']['onwardJourny']['dropping_stop_name']); ?></small>
                                                </div>
                                            </div>

                                            <hr class="border">

                                            <div class="row">
                                                <div class="col-md-6 col-xs-6">
                                                    <span class="purple">Departure</span> <br/>
                                                    <small><?php echo trim($this->session->userdata['onwardJourny']['boarding_point']); ?></small>       
                                                </div>
                                                <div class="col-md-6 col-xs-6">
                                                    <span class="purple">Arrival</span> <br/>
                                                    <small><?php echo trim($this->session->userdata['onwardJourny']['dropping_point']); ?></small>          
                                                </div>
                                            </div>

                                            <hr class="border">

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <i class="short-full glyphicon glyphicon-time pull-left clock"></i> &nbsp;&nbsp;<?php echo trim($this->session->userdata['onwardJourny']['total_travel_time']); ?> Hrs
                                                </div>
                                            </div>
                                            <hr class="border">

                                            <div class="row">
                                                <div class="col-md-6 col-xs-6">
                                                    <span class="purple">Bus Type</span>
                                                </div>
                                                <div class="col-md-6 col-xs-6">
                                                    <span class="pull-right text-right"><?php echo $this->session->userdata['onwardJourny']['onwardJourny']['data_bus_type']; ?></span>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </div>

                       <!--     <div class="panel panel-default">
                                <div class="panel-heading padding0" role="tab" id="headingTwo">
                                    <h4 class="panel-title bg-purple">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <i class="droparrow glyphicon glyphicon-chevron-down"></i>
                                            Other Details
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        <article class="bus-booking-details">
                                            <div class="row">
                                                <div class="col-md-6 col-xs-6">
                                                    <span class="purple">Seat No(s):</span>
                                                </div>
                                                <div class="col-md-6 col-xs-6">
                                                    <span class="pull-right"><img src="<?php echo base_url() . BACK_ASSETS ?>img/seats.png" alt=""> <?php echo $this->session->userdata['temp_details']['seat_nos']; ?></span>
                                                </div>
                                            </div>

                                            <hr class="border">

                                            <div class="row">
                                                <div class="col-md-6 col-xs-6">
                                                    <span class="purple">Bus Type</span>
                                                </div>
                                                <div class="col-md-6 col-xs-6">
                                                    <span class="pull-right text-right"><?php echo $this->session->userdata['temp_details']['bus_name']; ?></span>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </div> -->

                          <!--  <div class="panel panel-default">
                                <div class="panel-heading padding0" role="tab" id="headingThree">
                                    <h4 class="panel-title bg-purple">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            <i class="droparrow glyphicon glyphicon-chevron-down"></i>
                                            Travellers List
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        <article class="bus-booking-details">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    Please login to see the travellers & favourite list.
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </div> -->

                        </div>
                        <?php if(!empty($this->session->userdata['returnJourny'])) :?>
                        <h5 class="text-center">Return Journey</h5>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            
                            <div class="panel panel-default">
                                <div class="panel-heading padding0" role="tab" id="headingTwo">
                                    <h4 class="panel-title bg-purple color-w">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">
                                            <i class="droparrow glyphicon glyphicon-chevron-down"></i>
                                            Booking Details
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        <article class="bus-booking-details">
                                            <h5 class="travel-name"><?php echo $this->session->userdata['operator_name']['operator_name']; ?></h5>
                                            <div class="row">
                                                <div class="col-md-6 col-xs-6">
                                                    <span class="purple">From</span> <br/>
                                                    <small><?php echo trim($this->session->userdata['returnJourny']['returnJourny']['boarding_stop_name']); ?></small>
                                                </div>
                                                <div class="col-md-6 col-xs-6">
                                                    <span class="purple">To</span> <br/>
                                                    <small><?php echo trim($this->session->userdata['returnJourny']['returnJourny']['dropping_stop_name']); ?></small>
                                                </div>
                                            </div>

                                            <hr class="border">

                                            <div class="row">
                                                <div class="col-md-6 col-xs-6">
                                                    <span class="purple">Departure</span> <br/>
                                                    <small><?php echo trim($this->session->userdata['returnJourny']['boarding_point']); ?></small>       
                                                </div>
                                                <div class="col-md-6 col-xs-6">
                                                    <span class="purple">Arrival</span> <br/>
                                                    <small><?php echo trim($this->session->userdata['returnJourny']['dropping_point']); ?></small>          
                                                </div>
                                            </div>

                                            <hr class="border">

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <i class="short-full glyphicon glyphicon-time pull-left clock"></i> &nbsp;&nbsp;<?php echo trim($this->session->userdata['returnJourny']['total_travel_time']); ?> Hrs
                                                </div>
                                            </div>
                                            <hr class="border">

                                            <div class="row">
                                                <div class="col-md-6 col-xs-6">
                                                    <span class="purple">Bus Type</span>
                                                </div>
                                                <div class="col-md-6 col-xs-6">
                                                    <span class="pull-right text-right"><?php echo $this->session->userdata['returnJourny']['returnJourny']['data_bus_type']; ?></span>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endif;?>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!-- Signin Content End -->             
</div>
<!-- Inner Wrapper End -->
<script type="text/javascript">


    $(document).ready(function() {
        $("#confirm_bookingd").click(function() { 
            var validate = false;
            var allValidate = false;
            var id_number = '0';
            var id_type = '0';
            var email = $("#email").val();
            var mobile = $("#mobile").val();
            //var psgr_age = $('.psgr_age').val();
            //alert(parseInt(psgr_age));return false;
            // id_number = $("#id_number").val();
            // id_type =$("#id_type").val();
            var operator_name = $("#operator_name").text();

            if ($("input[type='checkbox'].radioBtnClass").is(':checked')) {
                var psgr_sex = $("input[type='checkbox'].radioBtnClass:checked").map(function() {
                    return $(this).val()
                }).get();
            }

            var psgr_names = $('.psgr_name').map(function() {
                return $(this).val()
            }).get();
            var psgr_age = $('.psgr_age').map(function() {
                return $(this).val()
            }).get();
            /*name limit*/
            if(psgr_names !='') {
               allValidate = true;
            } else {
                alert('Please enter the name.');
                return false;
            }
            /*end name limit*/
            /*Age limit*/
            //var psgrAge = parseInt(psgr_age);
            if(parseInt(psgr_age) > 0 && parseInt(psgr_age) < 100) {
               allValidate = true;
            } else {
                alert('Please enter the valid age.');
                return false;
            }
           
            /*end limit*/
            /* email validation*/
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))  
            {  
              allValidate = true;
            } else{  
              alert("You have entered an invalid email address!")  
              return false; 
            }
            /* end email validation*/
            
            /* Mobile validation */
            var filter = /^[6789]{1}[0-9]{9}$/;
            if (filter.test(mobile)) {
              if(mobile.length==10){
                   var validate = true;
              } else {
                  alert('Please put 10  digit mobile number');
                  var validate = false;
              }
            }
            else {
              alert('Please, Enter the valid mobile number');
              var validate = false;
            }
            if(validate){
             return true;
            } else
            {
               return false;
            }
            /*End Mobile validation*/ 
            
            
        });
        
        $("#confirm_booking").click(function() {
           var validate = false;
           var allValidate = false;
            var id_number = '0';
            var id_type = '0';
            var email = $("#email").val();
            var mobile = $("#mobile").val();
            //var psgr_age = $('.psgr_age').val();
            //alert(parseInt(psgr_age));return false;
            // id_number = $("#id_number").val();
            // id_type =$("#id_type").val();
            var operator_name = $("#operator_name").text();

            if ($("input[type='checkbox'].radioBtnClass").is(':checked')) {
                var psgr_sex = $("input[type='checkbox'].radioBtnClass:checked").map(function() {
                    return $(this).val()
                }).get();
            }

            var psgr_names = $('.psgr_name').map(function() {
                return $(this).val()
            }).get();
            var psgr_age = $('.psgr_age').map(function() {
                return $(this).val()
            }).get();
            /*name limit*/
            if(psgr_names !='') {
               allValidate = true;
            } else {
                alert('Please enter the name.');
                return false;
            }
            /*end name limit*/
            /*Age limit*/
            //var psgrAge = parseInt(psgr_age);
            if(parseInt(psgr_age) > 0 || parseInt(psgr_age) < 130) {
               allValidate = true;
            } else {
                alert('Please enter the valid age.');
                return false;
            }
            /*end limit*/
            /* email validation*/
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))  
            {  
              allValidate = true;
            } else{  
              alert("You have entered an invalid email address!")  
              return false; 
            }
            /* end email validation*/
            
            /* Mobile validation */
            var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
            if (filter.test(mobile)) {
              if(mobile.length==10){
                   var validate = true;
              } else {
                  alert('Please put 10  digit mobile number');
                  var validate = false;
              }
            }
            else {
              alert('Not a valid number');
              var validate = false;
            }
            if(validate){
             return false;
            } else
            {
               return false;
            }
            /*End Mobile validation*/ 
            
            if (allValidate)
            {
                return true;
            }
            else
            {
                // AJAX Code To Submit Form.
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: base_url + "admin/Search_bus/temp_booking",
                    data: $( "form" ).serialize(),
                    success: function(data) {

                        /*alert(data.redirect);
                         alert(data.transaction_no);*/

                        // window.location.href = data.redirect+"?refrence_no="+data.transaction_no;
                        window.location.href = data.redirect;
                    }
                });
            }
            return false;
        });
        /*$(document).ajaxSuccess(function() {
         alert("AJAX request successfully completed");
         });*/
    });
</script>