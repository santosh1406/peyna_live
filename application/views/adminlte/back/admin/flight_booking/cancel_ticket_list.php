<!-- Inner Wrapper Start -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="<?php echo base_url() . BACK_ASSETS ?>css/search_style/style.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/jquery.js" type="text/javascript"></script>

<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/qrcode.js" type="text/javascript"></script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/jquery.qrcode.js" type="text/javascript"></script>

<!-- Inner Wrapper Start -->
<link href='<?php echo base_url() . BACK_ASSETS ?>css/search_style/style.css' rel="stylesheet" type="text/css" />
<style>
    .loader {
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #933b89;
        height: 120px;
        width: 120px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
        position: absolute;
        float: right;
        position: fixed;
        z-index: 9999999;
        left: 53%;
        top: 50%;
        /*background-color: #000;*/
        opacity: 0.7;
    }

    #loader{
        background: #000;
        opacity: 0.7;
        position: fixed;
        width: 100%;
        height: 120%;
        display: none;
        z-index: 999999;
        top: -10%;
    }


    /* Safari */
    @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
    .ladies_selected{
        background-position: -20px -20px;
    }
    input[type=range]::-moz-range-thumb {
    background-color: #933b89;
  }
</style>
<div id="page-wrapper">
    <div id="loader" >
        <div class="loader" ></div>
    </div>
    <!-- Signin Content Start -->
    <section class="inner-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="text-center purple">Cancel Ticket</h3>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-12 col-sm-12 container-c">
                                <div class="formBox">
                                    <?php echo validation_errors(); ?>
                                    <form action="<?php echo base_url() . 'admin/Search_bus/check_ticket_details'; ?>" method="POST" id="cacelTicket">                       
                                        <div class="row">
                                            <div class="col-sm-3 col-xs-12">
                                                <div class="inputBox  <?php echo !empty($email) ? 'focus' :''; ?>">
                                                    <div class="inputText">Email Address</div>
                                                    <input type="text" id="email" name="email" value="<?php echo $email; ?>" class="input">
                                                </div>
                                            </div>

                                            <div class="col-sm-3 col-xs-12">
                                                <div class="inputBox <?php echo (!empty($rokad_key) ? 'focus' :''); ?>">
                                                    <div class="inputText">Rokad Reference Number</div>
                                                    <input type="text" id="bosReference" value="<?php echo $rokad_key; ?>" name="bosReference" class="input">
                                                </div>
                                            </div>

                                            <div class="col-sm-1 col-xs-12">
                                                <div class="text-center center-block mt10">OR</div>
                                            </div>

                                            <div class="col-sm-3 col-xs-12">
                                                <div class="inputBox  <?php echo !empty($pnr_no) ? 'focus' :''; ?>">
                                                    <div class="inputText">PNR Number</div>
                                                    <input type="text" id="pnrNumber" name="pnrNumber" <?php echo $pnr_no; ?> class="input">
                                                </div>
                                            </div>

                                            <div class="col-sm-2 col-xs-12">
                                                <button type="submit" class="btn btn-purple btn-lg btn-block login-button txt-w col-sm-4">Show Ticket</button>
                                            </div>
                                        </div>
                                    </form>
                                </div> 
                            </div>
                        </div>
                    </div>

                    <h4 class="text-center">Trips you have booked!</h4>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="booking-history">
                                <?php if (!empty($ticketDetail)) : ?>
                                    <form class="cancel-booking-form">
                                        <div class="booking-info clearfix">
                                            <div class="ticket-info">
                                                <div class="col-md-1 col-xs-12">
                                                    <div class="date">
                                                        <label class="month">Nov</label>
                                                        <label class="date">13</label>
                                                        <label class="day">Tue</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-xs-12">
                                                    <h5 class="box-title">
                                                        <i class="icon fa fa-bus purple circle"></i> <span class="service-title-main"><?php echo isset($ticketDetail['boarding_stop_name']) ? $ticketDetail['boarding_stop_name'] : '' ?> <strong>to </strong><?php echo isset($ticketDetail['destination_stop_name']) ? $ticketDetail['destination_stop_name'] : '' ?> </span><br/>
                                                        <small>MSRTC</small>
                                                    </h5>
                                                </div>
                                                <div class="col-md-2 col-xs-12">
                                                    <button class="btn btn-purple login-button txt-w status">UPCOMMING</button>
                                                </div>
                                                <div class="col-md-3 col-xs-12">
                                                    <div class="info cancel-info">
                                                        <span class="fnt09 purple">PNR No</span><br/>
                                                        <span class="fnt09"><?php echo isset($ticketDetail['pnr_no']) ? $ticketDetail['pnr_no'] : '' ?></span><br/>

                                                        <span class="fnt09 purple">Rokad Ref No</span><br/>
                                                        <span class="fnt09"><?php echo isset($ticketDetail['rokad_ref_no']) ? $ticketDetail['rokad_ref_no'] : '' ?></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-xs-12">
                                                    <div class="info cancel-info">
                                                        <span class="fnt09 purple">Total Fare</span><br/>
                                                        <span class="fnt09">Rs. <?php echo isset($ticketDetail['total_fare_without_discount']) ? $ticketDetail['total_fare_without_discount'] : '' ?></span><br/>

                                                        <span class="fnt09 purple">Travel Date</span><br/>
                                                        <span class="fnt09"><?php echo isset($ticketDetail['boarding_time']) ? $ticketDetail['boarding_time'] : '' ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                <?php else: ?>
                                    <div class="booking-info clearfix">
                                        <p>No tickets details found.</p>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <?php if (!empty($ticketDetail)) : ?>
                        <div class="col-md-12">
                            <button id="ticket_cancelable" class="btn btn-purple login-button txt-w status pull-right" data-boss-ref="<?php echo isset($ticketDetail['boss_ref_no']) ? $ticketDetail['boss_ref_no'] : '' ?>" data-pnr-no="<?php echo isset($ticketDetail['pnr_no']) ? $ticketDetail['pnr_no'] : '' ?>">Cancel Ticket</button>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="modal fade popup col-md-12" id="isCancelable_popup" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog add_amt">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class=""></i>&nbsp; Cancel Ticket Details</h3>
                    </div>
                    <?php
                    $attributes = array("method" => "POST", "id" => "waybill_amnt", "name" => "waybill_amnt");
                    echo form_open(base_url() . 'admin/fund_request/add_wallet_money', $attributes);
                    ?>

                    <div class="modal-body">
                        <div class="form-group">
                            <!--<div class="input-group">
                                <span class="input-group-addon" id="msg">msg</span>
                                 <input name="agent_code" type="hidden" id="agent_code" class="form-control" />
                                <!--<input name="amount" type="text"  id="amount" class="form-control" placeholder="Wabill Amount" min="1" maxlength="6" autocomplete="off"  required />&nbsp;
                                <input name="pg_amt" type="text"  id="pg_amt" class="form-control" placeholder="Waybill Amount" min="1" maxlength="6" autocomplete="off"  required />&nbsp;

                                <span id="erramountmsg" class="error"></span>
                            </div> -->
                            <div class="col-md-12 col-xs-12" >
                                <div class="info cancel-info">
                                    <span class="fnt09 purple">Status </span> : &nbsp <strong id="msg"></strong>
                                </div>
                                <div class="info cancel-info" id="isCancelable">
                                    <span class="fnt09 purple">Refund Amount </span> : &nbsp <strong id="refundAmount"></strong><br/>

                                    <span class="fnt09 purple">Commission </span>: &nbsp <strong id="commissionId"></strong><br/>
                                    <span class="fnt09 purple">Cancellation Charge </span>: &nbsp <strong id="cancellationCharge"></strong><br/>

                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer clearfix">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        <button type="button" id="TicketCancelConfirm" data-dismiss="modal" class="btn btn-primary pull-left" name="TicketCancelConfirm" style="margin-left: 66%;" data-boss-ref="<?php echo isset($ticketDetail['boss_ref_no']) ? $ticketDetail['boss_ref_no'] : '' ?>" data-pnr-no="<?php echo isset($ticketDetail['pnr_no']) ? $ticketDetail['pnr_no'] : '' ?>"> Ticket Cancel</button>
                    </div>
                    </form>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </section>

</div>

<script>
    function openNav() {
        if ($(window).width() < 981) {
            document.getElementById("mySidenav").style.width = "100%";
        } else {
            document.getElementById("mySidenav").style.width = "15%";
        }


        var width = $("#mySidenav").width();
        if (width !== 0) {
            document.getElementById("mySidenav").style.width = "0";
        }

    }

    function closeNav() {

        document.getElementById("mySidenav").style.width = "0";
    }
</script>
<script>
    $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>
<script>
    //jQuery('#qrcode').qrcode("this plugin is great");
    jQuery('#qrcodeCanvas').qrcode({
        text: "http://jetienne.com"
    });
</script>
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script type="text/javascript">
    $(".input").focus(function() {
        $(this).parent().addClass("focus");
    })
</script>
<script type="text/javascript">
    $(document).ready(function(e) {

        $('body').off('click', '#ticket_cancelable').on('click', '#ticket_cancelable', function() {
            var boss_ref = $(this).attr('data-boss-ref');
            var pnr_no = $(this).attr('data-pnr-no');
            var url = base_url + "admin/Search_bus/is_ticket_cancel";
            $('#loader').css({"display": "block"});
            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: {bos_key: boss_ref, pnr_no: pnr_no},
                success: function(data) {
                    var datas = JSON.stringify(data);
                     var details = JSON.parse(datas);
                     
                    if (details.status == 'failed') {
                        $('#msg').text(details.msg);
                        $('#isCancelable').hide();
                        $('#TicketCancelConfirm').hide();
                    } else {
                       
                        $('#isCancelable').show();
                        $('#msg').text(details.msg);
                        $('#refundAmount').text(details.data.refundAmount);
                        $('#commissionId').text(details.data.commission);
                        $('#cancellationCharge').text(details.data.cancellationCharge);
                        $('#TicketCancelConfirm').show();
                    }
                    $('#loader').css({"display": "none"});
                     $('#isCancelable_popup').modal('show');
                },
                error: function(data) {
                    // do something
                }
            });
        });

        $('body').off('click', '#TicketCancelConfirm').on('click', '#TicketCancelConfirm', function() {
            var url = base_url + "admin/Search_bus/cancel_ticket_details";
            var boss_ref = $(this).attr('data-boss-ref');
            var pnr_no = $(this).attr('data-pnr-no');
           $('#loader').css({"display": "block"});
            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: {bos_key: boss_ref, pnr_no: pnr_no},
                success: function(data) {
                    var datas = JSON.stringify(data);
                    var details = JSON.parse(datas);
                    
                    if (data.status != 'status') {
                        $('#msg').text(details.msg);
                        $('#isCancelable').hide();
                    } else {
                        $('#isCancelable').show();
                        $('#msg').text(details.msg);
                    }
                    $('#TicketCancelConfirm').hide();
                    $('#loader').css({"display": "none"});
                    $('#isCancelable_popup').modal('show');
                },
                error: function(data) {
                    // do something
                }
            });
        });
    });
</script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/bootstrap.js" type="text/javascript"></script>
</body>

</html>
