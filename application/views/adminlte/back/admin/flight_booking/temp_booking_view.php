<!-- Created by prabhat on 20-Dec-2018 -->
<link href="<?php echo base_url() . ASSETS ?>common/css/bootstrap/bootstrapValidator.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() . ASSETS ?>common/css/bootstrap/bootstrapValidator.min.css" rel="stylesheet" type="text/css" />
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-8">
                <div class="box">
                    <div class="box-header"> 
                        <h3 class="box-title">Booking Details</h3>
                    </div>
                    <div>
                        <h5 class="box-title">Onward Journey Details</h5>
                    </div>
                    <?php
                                        $seats = explode(',',$this->session->userdata['temp_details']['seat_nos']);
                                        $gender = explode(',',$this->session->userdata['temp_details']['gender']);
                                        $i=0;
                                        foreach ($seats as $key => $value) {   
                                                                                    
                                      
                                        ?>
                    <div class="box-body ">                        
                        <div class="form-group col-xs-4">                            
                            <input type="text" class="form-control" id="psgr_name" name="psgr_name[]" placeholder="Name" value="">
                        </div>
                        <div class="form-group col-xs-2">                            
                            <input type="text" class="form-control" id="psgr_age" name="psgr_age[]" placeholder="Age" value="">
                        </div>                        
                        <div class="form-group col-xs-2">  Seat=><?php echo $seats[$i]; ?></div>
                        
                        <div class="form-group col-xs-4">  
                                                   
                            <input type="radio" class="form-control radioBtnClass" id="psgr_sex" name="psgr_sex[<?=$i;?>]" value="M" <?php if($gender[$i]=='M'){echo 'checked';}?> > Male
                            <input type="radio" class="form-control radioBtnClass" id="psgr_sex" name="psgr_sex[<?=$i;?>]" value="F" <?php  if($gender[$i]=='F'){echo 'checked';} ?> > Female<br>
                                    
                        </div>
                    </div><!--//box-body -->
                    <?php  $i++;} ?>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="box-body">
                        <div class="form-group col-xs-6">
                            <label for="exampleInputEmail1">Email Address</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" value="">
                        </div>
                        <div class="form-group col-xs-6">
                            <label for="bos_ref_no">Mobile Number</label>
                            <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" value="" >
                        </div>
                        <div class="form-group col-xs-6">
                            <label for="bos_ref_no">Id Type</label>
                            <input type="text" class="form-control" id="id_type" name="id_type" placeholder="ID type" value="" >
                        </div>
                        <div class="form-group col-xs-6">
                            <label for="bos_ref_no">Id Number</label>
                            <input type="text" class="form-control" id="id_number" name="id_number" placeholder="Id Number" value="" >
                        </div>
                        <div class="form-group">
                            <span style="display: none" id="operator_name"  ><?php echo $this->session->userdata['operator_name']['operator_name'];?></span>
                            <button type="button" class="button btn-medium <?php echo THEME_BTN;?>" name="confirm_booking" id="confirm_booking">Confirm Booking</button>
                        </div><!--//box-body -->
                    </div> <!--//box-body --> 
                </div><!--//box-->
            </div><!--//col-xs-8-->                
                    
            <div class="col-xs-4">
                <div class="box">
                    <div class="box-header"> 
                        <h3 class="box-title">Ticket Details</h3>
                    </div>

                    <div class="box-body">
                        <div class="form-group">
                        Booking Details
                            <table>
                                <tr>
                                    
                                    <td>From: <?php
                                    $arr = explode('-',trim($this->session->userdata['temp_details']['from_stop_name']));
                                    echo $arr[0];    

                                    ?></td>
                                    <td>To: <?php
                                    $arr = explode('-',trim($this->session->userdata['temp_details']['to_stop_name']));
                                    echo $arr[0];    

                                    ?></td>
                                </tr>
                                <tr>
                                    <td>Departure: <?php
                                    $arr = explode('-',trim($this->session->userdata['temp_details']['from_stop_name']));
                                    echo $date = $arr[1].'-'.$arr[2].'-'.$arr[3];

                                    
                                    ?></td>
                            <td>Arrival:  <?php
                                    $arr = explode('-',trim($this->session->userdata['temp_details']['to_stop_name']));
                                    echo $arr[1].'-'.$arr[2].'-'.$arr[3];
                                    ?></td>
                                </tr>
                            </table>
                        </div>
                        <div class="form-group">
                            Other Details
                            <table>
                                <tr>
                                    <td>Seat No.  <?php echo $this->session->userdata['temp_details']['seat_nos']; ?></td>
                                    <td>Bus Type</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="log"></div>
    </section>
</div>

<script type="text/javascript">
    
   
	$(document).ready(function(){
    $("#confirm_booking").click(function(){

    var email = $("#email").val();
    var mobile = $("#mobile").val();
    var id_number = $("#id_number").val();
    var id_type =$("#id_type").val();
    var operator_name =$("#operator_name").text();

    if($("input[type='radio'].radioBtnClass").is(':checked')) {
        var psgr_sex = $("input[type='radio'].radioBtnClass:checked").map(function(){ return $(this).val()}).get();    
    }

    var psgr_name=$('input[name="psgr_name[]"]').map(function(){ return $(this).val()}).get();
    var psgr_age=$('input[name="psgr_age[]"]').map(function(){ return $(this).val()}).get();

        if(mobile==''||email=='')
        {
        alert("Please Fill All Fields");
        }
        else
        {
        // AJAX Code To Submit Form.
                $.ajax({
                type:"POST",
                dataType: "json",
                url: base_url+"admin/Search_bus/temp_booking",
                data: {'mobile1': mobile , 'email1': email , 'id_type1': id_type , 'id_number1' : id_number , 'psgr_name1' : psgr_name , 'psgr_age1' : psgr_age , 'psgr_sex1' : psgr_sex ,'operator_name' : operator_name},
                
                success: function(data){
                   
                    /*alert(data.redirect);
                    alert(data.transaction_no);*/
                    
                    window.location.href = data.redirect+"?refrence_no="+data.transaction_no;
                }
                });
        }
    return false;
    });
    /*$(document).ajaxSuccess(function() {
        alert("AJAX request successfully completed");
    });*/
}); 
</script>