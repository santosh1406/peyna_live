<!-- Inner Wrapper Start -->
 <link href="<?php echo base_url() . BACK_ASSETS ?>css/search_style/style.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/qrcode.js" type="text/javascript"></script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/jquery.qrcode.js" type="text/javascript"></script>
        <div id="page-wrapper">
            
                <!-- Signin Content Start -->
                <section class="inner-wrapper">
                    <div class="container">
                        <div class="row">
                          <div class="col-md-6 col-md-offset-3">
                                <h3 class="text-center purple">Print Ticket</h3>
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <h4 class="text-center">Trips you have booked!</h4>
                                        <div class="col-md-12 col-sm-12 container-c">
                                            <div class="formBox">
                                                <form>                                
                                                    <div class="row">
                                                        <div class="col-sm-12 col-xs-12">
                                                            <div class="inputBox">
                                                                <div class="inputText">Email Address</div>
                                                                <input type="text" name="" class="input">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-sm-12 col-xs-12">
                                                            <div class="inputBox">
                                                                <div class="inputText">BOS Reference Number</div>
                                                                <input type="text" name="" class="input">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <label class="control-label col-md-12 label-text">Get Ticket By</label>
                                                        <div class="col-sm-4 col-xs-6">
                                                            <div class="checkbox checkbox-info checkbox-circle pull-right">
                                                                <input id="checkBox7" class="checkbox-circle" type="checkbox" name="" checked>
                                                                <label for="checkBox7"> Print/View</label>
                                                            </div>
                                                        </div>
                            
                                                        <div class="col-sm-4 col-xs-6">
                                                            <div class="inputBox">
                                                                <div class="checkbox checkbox-info checkbox-circle">
                                                                <input id="checkBox8" class="checkbox-circle" type="checkbox" name="">
                                                                <label for="checkBox8"> MTicket (SMS)</label>
                                                            </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-sm-4 col-xs-6">
                                                            <div class="inputBox">
                                                                <div class="checkbox checkbox-info checkbox-circle">
                                                                <input id="checkBox9" class="checkbox-circle" type="checkbox" name="">
                                                                <label for="checkBox9"> Email</label>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                            
                                                    <div class="row">
                                                        <div class="col-sm-6 col-xs-12 col-sm-offset-3">
                                                            <button type="button" class="btn btn-purple btn-lg btn-block login-button txt-w col-sm-4">Get Ticket</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                          </div>
                        </div>
                    </div>
                </section>
                
                <!-- Signin Content End -->             
        </div>
        <!-- Inner Wrapper End -->