<div class="col-md-12">
    <div class="box-style3 mb20 clearfix">
        <form>
            <div class="clearfix seat-block">
                <h3 class="more-seat-detail red" style="display:block;">Maximum 6 seats allowed per booking</h3>
                <div class="row">
                    <div class="col-md-12">
                        <div class="legend mb20">
                            <label>
                                <span class="seat available"></span>
                                Available Seat
                            </label>
                            <label>
                                <span class="seat selected"></span>
                                Selected Seat
                            </label>
                            <label>
                                <span class="seat booked"></span>
                                Booked Seat
                            </label>
                            <label>
                                <span class="seat ladies_available"></span>
                                Ladies Seat
                            </label>
                            <label>
                                <span class="seat mens_available"></span>
                                Mens Seat
                            </label>
                            <label>
                                <span class="seat handicapped_available"></span>
                                Blind/Handicapped
                            </label>
                            <label>
                                <span class="seat senior_citizen_available"></span>
                                Senior Citizen
                            </label>
                        </div>

                        <div class="col-md-8 col-xs-12 seat-layout">
                            <div class="fare-tab-filter show">
                                <ul></ul>
                            </div>
                            <br>
                            <span class="bus-seat-layout">
                                <ul class="deckrow">
                                    <li>
                                        <ul>
                                            <li><a class=" myBusDriveSeatImage driver_booked" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat booked" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat booked" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat senior_citizen_booked" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat booked" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat ladies_available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat ladies_available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat ladies_available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat booked" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <ul>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat booked" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat booked" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat senior_citizen_booked" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat booked" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat ladies_booked" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat ladies_available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat booked" href="#" style="width: 20px; height: 20px;"></a></li>

                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <ul>
                                            <li><a href="#" title="" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <ul>
                                            <li><a href="#" title="" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <ul>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat booked" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat booked" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <ul>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat booked" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat booked" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                            <li><a class="seat available" href="#" style="width: 20px; height: 20px;"></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </span>
                        </div>
                        <div class="col-md-4 col-xs-12 seat-detail-info pull-right mt40">
                            <div class="col-md-12 tprice">
                                <div class="SelectedSeats clearfix"><label>Seat(s)  :</label> <span class="sel_seat">25, 31, 32, 33, 42</span></div>
                                <div class="amount clearfix"><label>Base Fare : </label> <span class="fare_value">Rs. <b class="fare">0.00</b></span></div>
                                <div class="amount clearfix" style="display: none;"><label>GST : </label> <span class="fare_value">Rs. <b class="fare_service_tax">0.00</b></span></div>
                                <div class="amount clearfix"><label>Service Charge : </label> <span class="fare_value">Rs. <b class="fare_service_charge">0.00</b></span></div>
                                <div class="amount clearfix"><label>Sub Total : </label> <span class="fare_value">Rs. <b class="tot_fare">0.00</b></span></div>
                                <div class="amount clearfix"><label>Total Amount : </label> <span class="fare_value">Rs. <b class="total_amount">0.00</b></span></div>
                            </div>
                            <div class="col-md-12 boarding-dropping">          
                                <div>
                                    <button class="btn txt-w btn-medium continue btn-purple" data-trip-no="7418" data-bus-travel-id="445965523">Continue</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>