<!--Inner Wrapper Start -->
 <link href="<?php echo base_url() . BACK_ASSETS ?>css/search_style/style.css" rel="stylesheet" type="text/css" />
<!--<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/jquery.js" type="text/javascript"></script>
 <script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/qrcode.js" type="text/javascript"></script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/jquery.qrcode.js" type="text/javascript"></script> -->

<!--  <link href="<?php echo base_url() . BACK_ASSETS ?>css/search_style/bootstrap.css" rel="stylesheet" type="text/css" />
 <link href="<?php echo base_url() . BACK_ASSETS ?>css/search_style/bootstrap.min.css" rel="stylesheet" type="text/css" /> -->
		<div id="page-wrapper">
            
            	<!-- Signin Content Start -->
            	<section class="inner-wrapper">
                	<div class="container">
              			<div class="row">
                          <div class="col-md-12">
                              <?php
                                if(!empty($this->session->flashdata('item'))) {
                                  $message = $this->session->flashdata('item');
                                  //var_dump($message); die();
                                  ?>
                              <div id="alertMsgDangar" class="<?php echo $message['class']; ?>" style="text-align: center;"><h3><strong><?php echo $message['message']; ?></h3></strong></div>
                                <?php 
                                }
                                ?>
                          		<h3 class="text-center purple">Flight Booking</h3>
                                <div class="panel panel-default">
    								<div class="panel-body">
                                        <div class="col-md-12 col-sm-12 container-c">
                                         	<div class="formBox">
                                                <form action="<?php echo base_url()?>admin/flight_booking/flight_search" method="POST"  name="bus_booking" accept-charset="utf-8" id="search_services">
                                                	<div class="row gender disp_radio">
                                                        <div class="col-sm-6 col-xs-6">
                                     <div class="checkbox checkbox-info checkbox-circle pull-right">
                                                                <input id="checkBox7" class="checkbox-circle gender checkBox7check" type="radio" name="triptype" value="single" checked>
                                                                <label for="checkBox7" class="checkBox7check">Single Trip</label>
                                                            </div>
                                                        </div>
                            
                                                        <div class="col-sm-6 col-xs-6 checkBox8check">
                                                            <div class="inputBox">
                                                                <div class="checkbox checkbox-info checkbox-circle checkBox8check">
                                                                <input id="checkBox8" class="checkbox-circle gender checkBox8check" type="radio" value="round" name="triptype">
                                                                <label for="checkBox8" class="checkBox8check">Round Trip</label>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>                                
                                                    <div class="row col-lg-offset-1 col-md-offset-1">
                                                        <div class="col-sm-5 col-xs-12">
                                                        	<div class="inputBox">
                                                              <div class="inputText">From</div>
                                                              <input name="from_stop" id="from_stop" class="input" type="text" value="<?php echo isset($data['from_stop']) ? $data['from_stop'] : ''; ?>" >
                                                              
                                                              <label id="from-error" class="error" for="from"></label>
                                                              <div class="error" id="erFromstop"></div>
                                                              <?php echo form_error('from_stop', '<div class="error">', '</div>'); ?>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-sm-1 col-xs-12">
	                                                        <div class="form-group">
                                                            <img src="<?php echo base_url();?>assets/travelo/front/images/icon_reverse.png" class="img-responsive center-block cursor-pointer switch_cities" alt="Switch Cities" title="Switch Cities" width="32" height="33" style="cursor: pointer; " />
                                                          </div>
                                                        </div>
                            
                                                        <div class="col-sm-5 col-xs-12">
                                                        	<div class="inputBox">
                                                                <div class="inputText">To</div>
                                                                <input name="to_stop" id="to_stop" class="input" type="text">
                                                                <label id="to-error" class="error" for="to"></label>
                                                                <div class="error" id="erToStop"></div>
                                                                <?php echo form_error('to_stop', '<div class="error">', '</div>'); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                            
                                                    <div class="row col-lg-offset-1 col-md-offset-1">
                                                        <div class="col-sm-5 col-xs-12">
                                                        	<div class="inputBox cinputBox">
                                                                <div class="inputText">Departure Date</div>
                                                                <input id="datepicker"  name="departure_date" class="input" type="text">
                                                                <div class="error" id="erDatepicker"></div>
                                                                 <?php echo form_error('departure_date', '<div class="error">', '</div>'); ?>
                                                            </div>
                                                        	<!--<div class="form-group input-group inputBox focus">
                                                                <input class="input" name="email" id="email" placeholder="Departure Date" type="text">
                                                                <span class="input-group-addon input-cal calendar"><i class="fa fa-calendar fa" aria-hidden="true"></i></span>
                                                    		</div>-->
                                                        </div>
                                                        
                                                        <div class="col-sm-1 col-xs-12">
	                                                        <div class="form-group">
                                                            
                                                            </div>
                                                        </div>
                            
                                                        <div class="col-sm-5 col-xs-12">
                                                        	<div class="inputBox cinputBox rmfocus">
                                                                <div class="inputText">Return Date (Opt)</div>
                                                                <input id="datepicker1" name="return_date" class="input" type="text" >
                                                                <div class="error" id="erReturnDate"></div>
                                                                <?php echo form_error('return_date', '<div class="error">', '</div>'); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row col-lg-offset-1 col-md-offset-1">
                                                        <div class="col-sm-3 col-xs-12">
                                                        	<div class="selectBox">
                                                                <div class="inputText">Adults 12+ Yrs</div>
                                                                <select class="input selectText">
                                                                  <option value=""></option>
                                                                  <option>1</option>
                                                                  <option>2</option>
                                                                  <option>3</option>
                                                                  <option>4</option>
                                                                  <option>5</option>
                                                                  <option>6</option>
                                                                  <option>7</option>
                                                                  <option>8</option>
                                                                  <option>9</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-sm-1 col-xs-12">
	                                                        <div class="form-group">
                                                            
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-sm-3 col-xs-12">
                                                        	<div class="selectBox">
                                                                <div class="inputText">Children 2 - 12Yrs</div>
                                                                <select class="input selectText">
                                                                  <option value=""></option>
                                                                  <option>1</option>
                                                                  <option>2</option>
                                                                  <option>3</option>
                                                                  <option>4</option>
                                                                  <option>5</option>
                                                                  <option>6</option>
                                                                  <option>7</option>
                                                                  <option>8</option>
                                                                  <option>9</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-sm-1 col-xs-12">
	                                                        <div class="form-group">
                                                            
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-sm-3 col-xs-12">
                                                        	<div class="selectBox">
                                                                <div class="inputText">Infants 0 - 2Yrs</div>
                                                                <select class="input selectText">
                                                                  <option value=""></option>
                                                                  <option>1</option>
                                                                  <option>2</option>
                                                                  <option>3</option>
                                                                  <option>4</option>
                                                                  <option>5</option>
                                                                  <option>6</option>
                                                                  <option>7</option>
                                                                  <option>8</option>
                                                                  <option>9</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row col-lg-offset-1 col-md-offset-1">
                                                        <div class="col-sm-3 col-xs-12">
                                                        	<div class="selectBox">
                                                                <div class="inputText">Preferred Airline</div>
                                                                <select class="input selectText">
                                                                  <option value=""></option>
                                                                  <option>All</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-sm-1 col-xs-12">
	                                                        <div class="form-group">
                                                            
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-sm-3 col-xs-12">
                                                        	<div class="selectBox">
                                                                <div class="inputText">Children 2 - 12Yrs</div>
                                                                <select class="input selectText">
                                                                  <option value=""></option>
                                                                  <option>1</option>
                                                                  <option>2</option>
                                                                  <option>3</option>
                                                                  <option>4</option>
                                                                  <option>5</option>
                                                                  <option>6</option>
                                                                  <option>7</option>
                                                                  <option>8</option>
                                                                  <option>9</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-sm-1 col-xs-12">
	                                                        <div class="form-group">
                                                            
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-sm-3 col-xs-12">
                                                        	<div class="selectBox">
                                                                <div class="inputText">Infants 0 - 2Yrs</div>
                                                                <select class="input selectText">
                                                                  <option value=""></option>
                                                                  <option>1</option>
                                                                  <option>2</option>
                                                                  <option>3</option>
                                                                  <option>4</option>
                                                                  <option>5</option>
                                                                  <option>6</option>
                                                                  <option>7</option>
                                                                  <option>8</option>
                                                                  <option>9</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">                           
                                                        <div class="col-sm-4 col-xs-12 col-sm-offset-4">
                                                        	<input type="submit" class="btn btn-purple btn-lg btn-block login-button txt-w col-sm-4" value="Flight Search">
                                                        </div>
                                                    </div>
                                                    
                                                </form>
                                            </div> 
                                        </div>
                                    </div>
  								</div>
                          </div>
                      	</div>
                    </div>
              	</section>
                
                <section class="inner-wrapper">
                	<div class="col-content">
                		<div class="container-fluid">
                    	<div class="row">
                          <div class="col-md-12">
                                <div class="well">
                                	<h4>Why Rokad for Bus Booking?</h4>
                                  	<p>Rokad is a digital payment platform, powered by Trimax, to offer citizen centric multimodal and multipurpose services. Rokad promotes demonetization through cashless transactions and provide convenience while availing multi-ulility services offered by Rokad.</p>
                                    <h4>Services with Rokad while Bus Ticketing Booking</h4>
                                    <p>Services offered by Rokad:</p>
                                  	<p>
                                    	1. Rokad would enable the passengers to use the same payment instrument across spectrum of other transport operators<br/>
                                        2. Rokad would significantly reduce the cash requirement for travelling purposes<br/>
                                        3. Rokad can be used in paying bills with different merchants
                                    </p>
                                    <p>
                                        Rokad on-board user will get dashboard and peer-to- peer transfer facilities. The dashboard facility would enable the customers to view their tansactions, the wallet balance, option to raise a query / dispute, etc. The peer-to-peer transfer facilities would enable customers to remit money in a economic and secured manner.
									</p>
                            </div>
                          </div>
                        </div>    
                    </div>
                    </div>
                </section>
                <!-- Signin Content End -->             
        </div>
        <!-- Inner Wrapper End-->
        <script>


$(document).ready(function() { 
    
     /*if($("ins").hasClass("iCheck-helper")){
        $(".iCheck-helper").addClass("checkBox8check");
     } */
      $('#datepicker1').prop("disabled", true);

      $('.checkBox8check').hover(function () {
        //$('#datepicker1').prop("disabled", true);
        return false;
      }).click(function(){
          $('#datepicker1').prop("disabled", false);
      });
      $('.checkBox7check').hover(function () {
         return false;
         //$('#datepicker1').prop("disabled", false);
      }).click(function(){
          $('#datepicker1').val('');
          $('.rmfocus').removeClass('focus');
          $('#datepicker1').prop("disabled", true);
      });
$("body").on('click', '#search_buses', function(){
       
        //$("#search_buses").prop('disabled', true);
        //$("#search_buses").html("Loading......");
        var fromDate = $('#from_stop').val();
        var toDate = $('#to_stop').val();
        var departureDate = $("#datepicker").val();
        var returnDate = $("#datepicker1").val();
        var triptype = $("input[name='triptype']:checked"). val();
        
        if(fromDate == '' && toDate =='' && departureDate == '') {
            alert('Please fill out all required fields');
                return false;
        }
        if(fromDate == ''){
             $('#erFromStop').text('The From spot is required.');
                return false;
        }
        if(toDate == ''){
             $('#erToStop').text('The To stop is required.');
                return false;
        }
        if(departureDate == ''){
             $('#erDatepicker').text('The Departure date is required.');
                return false;
        }
        if(triptype == 'round') {
            if(returnDate == ''){
                $('#erReturnDate').text('The return date is required.');
                return false;
            }
        }
  });

  $(document).off("click",".switch_cities").on("click",".switch_cities",function(e){e.preventDefault();var r=$("#from_stop").val(),t=$("#to_stop").val();(""!=r||""!=t)&&($("#from_stop").val(t),$("#to_stop").val(r))});

/*$("body").on('click', '#search_buses', function(){
  var from_stop = $("#from_stop").val();
        var to_stop = $("#to_stop").val();
  if(from_stop == to_stop)
        {
            $('#from-error').html('From & to stop must be different');
            $("#to-error").html('From & to stop must be different');
        }
      });
*/


  $("#datepicker" ).datepicker({ 
      numberOfMonths: 2,
      minDate: 0,
      //showOn: "button",
      //buttonImage: "images/calendar.gif",
      //buttonImageOnly: true,
      //buttonText: "Select date"
    });
  $("#datepicker1" ).datepicker({ numberOfMonths: 2,minDate: 0});
    
    /**load all stops in from stop input**/
        $("#from_stop").autocomplete({
          source: function (request, response) {
              $.ajax({

                  url: base_url+"admin/search_bus/get_stop",
                  dataType: "json",
                  data: {
                      term: request.term,
                     // type: rechargeType
                  },
                  success: function (data) {
                      response($.map(data, function (item) {                        
                          return {
                              label: item.label,
                              
                              name: item.name,
                              
                          };
                      }));
                  }
              });
          },
          autoFocus: true,
          minLength: 1,
          select: function (event, ui) {
             
              /*$('#error_div').html('');
              removeBillDetails();*/
              $("#from_stop").val(ui.item.name);
              
              if (ui.item.name == "No result found") {
                  $("#from_stop").val("");
              }else{
                  $("#from_stop_id").val(ui.item.id);
              }
          }
      });

        /**load all stops in to stop input**/
      $("#to_stop").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: base_url+"admin/search_bus/get_stop",
                dataType: "json",
                data: {
                    term: request.term,
                   // type: rechargeType
                },
                success: function (data) {
                    response($.map(data, function (item) {                        
                        return {
                            label: item.label,
                            
                            name: item.name,
                            
                        };
                    }));
                }
            });
        },
        autoFocus: true,
        minLength: 1,
        select: function (event, ui) {
           
            /*$('#error_div').html('');
            removeBillDetails();*/
            $("#to_stop").val(ui.item.name);
            
            if (ui.item.name == "No result found") {
                $("#to_stop").val("");
            }else{
                $("#to_stop_id").val(ui.item.id);
            }
        }
    });
      ///////////////////*//////////////////////////////
     
     window.setInterval('alertMsgDangar()', 10000); // 20 seconds
      
});
function alertMsgDangar() {
    $('#alertMsgDangar').hide();
}
</script>
<script type="text/javascript">
    $(".input").focus(function() {
      $(this).parent().addClass("focus");
    })
   </script>
