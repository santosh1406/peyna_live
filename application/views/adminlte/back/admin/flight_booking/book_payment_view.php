<link href="<?php echo base_url() . BACK_ASSETS ?>css/search_style/style.css" rel="stylesheet" type="text/css" />
<!-- Inner Wrapper Start -->
<style>
    .loader {
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #933b89;
        height: 120px;
        width: 120px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
        position: absolute;
        float: right;
        position: fixed;
        z-index: 9999999;
        left: 53%;
        top: 50%;
        /*background-color: #000;*/
        opacity: 0.7;
    }

    #loader{
        background: #000;
        opacity: 0.7;
        position: fixed;
        width: 100%;
        height: 120%;
        display: none;
        z-index: 999999;
        top: -10%;
    }
    .loader-txt{color:#fff; font-size:2em; text-align:center; position:absolute; top: 30%; left: 26%;}
    </style>
<div id="page-wrapper">
     <div id="loader" >
          <div class="loader-txt">Please Wait,  while your payment is being processed...</div>
        <div class="loader" ></div>
    </div>
    <?php //echo "<pre>"; print_r($tempData); die();?>
    <!-- Signin Content Start -->
    <section class="inner-wrapper">
        <div class="container col-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box-style2 minh450">
                        <h3>Payment Summary</h3>
                        <div class="payment-summary">
                            <form action="#" method="post">
                                <div class="steps mb10">
                                    <ul class="list-inline">
                                        <li><div class="square-number-bg">1</div> Select Seat & Boarding Point</li>
                                        <li><div class="square-number-bg">2</div> Travelers & Payment </li>
                                        <li class="active"><div class="square-number-bg-active">3</div> Booking Payment</li>
                                    </ul>
                                </div>

                                <div class="payment-alert-notice bg-purple color-w col-md-12 mb20">
                                    <div class="col-md-4 text-left">Please do not refresh page.</div>
                                    <div class="col-md-8 text-right">Please complete your booking within next <span class="color-b">6 mins, 53 secs</span> minutes.</div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                            <!-- <div class="panel panel-default">
                                                 <div class="panel-heading padding0" role="tab" id="headingOne">
                                                     <h4 class="panel-title bg-purple color-w">
                                                         <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                             <i class="droparrow glyphicon glyphicon-chevron-down"></i>
                                                             Fare Details
                                                         </a>
                                                     </h4>
                                                 </div>
                                                 <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                     <div class="panel-body">
                                                         <article class="bus-booking-details">
                                                             <div class="row">
                                                                 <div class="col-md-6 col-xs-6">
                                                                     
                                                                 </div>
                                                             </div>
                                                         </article>
                                                     </div>
                                                 </div>
                                             </div> -->

                                            <div class="panel panel-default">
                                                <div class="panel-heading padding0" role="tab" id="headingTwo">
                                                    <h4 class="panel-title bg-purple">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                            <i class="droparrow glyphicon glyphicon-chevron-down"></i>
                                                            Onward Journey
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                    <div class="panel-body">
                                                        <article class="bus-booking-details">
                                                            <div class="row">
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-left">Total Ticket:</span>
                                                                </div>
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-right">

                                                                        <?php echo (!empty($totalSeat) ? $totalSeat : '0'); ?>

                                                                    </span>
                                                                </div>
                                                            </div>

                                                            <hr class="border">

                                                            <div class="row">
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-left">Ticket Fare:</span>
                                                                </div>
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-right text-right">Rs. <?php echo!empty($tempData->base_fare) ? $tempData->base_fare : '0' ?></span>
                                                                </div>
                                                            </div>

                                                            <hr class="border">

                                                            <div class="row">
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-left">GST:</span>
                                                                </div>
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-right text-right">Rs. 0.0</span>
                                                                </div>
                                                            </div>

                                                            <hr class="border">

                                                            <div class="row">
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-left">Operational Charges:</span>
                                                                </div>
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-right text-right">Rs. <?php echo!empty($tempData->ticket_detail->msrtc->reservation_charge) ? $tempData->ticket_detail->msrtc->reservation_charge : '0' ?></span>
                                                                </div>
                                                            </div>
                                                             <hr class="border">
                                                            <div class="row">
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-left">ASN Charges:</span>
                                                                </div>
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-right text-right">Rs. <?php echo!empty($tempData->ticket_detail->msrtc->asn_amount) ? $tempData->ticket_detail->msrtc->asn_amount : '0' ?></span>
                                                                </div>
                                                            </div>
                                                             <hr class="border">
                                                            <div class="row">
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-left">Service Charges:</span>
                                                                </div>
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-right text-right">Rs. <?php echo!empty($tempData->ticket_detail->msrtc->ac_service_charges) ? $tempData->ticket_detail->msrtc->ac_service_charges : '0' ?></span>
                                                                </div>
                                                            </div>

                                                            <hr class="border">

                                                            <div class="row">
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-left">Onward Journey Price:</span>
                                                                </div>
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-right text-right">Rs. <?php echo!empty($tempData->total_fare) ? $tempData->total_fare : '0' ?></span>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="bg-purple grandt color-w">
                                                                    <div class="col-md-6 col-xs-6">
                                                                        <span class="pull-left">Grand Total</span>
                                                                    </div>
                                                                    <div class="col-md-6 col-xs-6">
                                                                        <span class="pull-right text-right">Rs.<?php echo!empty($tempData->total_fare) ? $tempData->total_fare : '0' ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </article>
                                                    </div>
                                                </div>
                                                </div>
                                                <!-- return tickets view -->
                                                <?php if(!empty($rtempData)) :?>
                                                <div class="panel panel-default">
                                                <div class="panel-heading padding0" role="tab" id="headingThree">
                                                    <h4 class="panel-title bg-purple">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                            <i class="droparrow glyphicon glyphicon-chevron-down"></i>
                                                            Return Journey
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="panel-body">
                                                        <article class="bus-booking-details">
                                                            <div class="row">
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-left">Total Ticket:</span>
                                                                </div>
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-right">

                                                                        <?php echo (!empty($rtotalSeat) ? $rtotalSeat : '0'); ?>

                                                                    </span>
                                                                </div>
                                                            </div>

                                                            <hr class="border">

                                                            <div class="row">
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-left">Ticket Fare:</span>
                                                                </div>
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-right text-right">Rs. <?php echo!empty($rtempData->base_fare) ? $rtempData->base_fare : '0' ?></span>
                                                                </div>
                                                            </div>

                                                            <hr class="border">

                                                            <div class="row">
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-left">GST:</span>
                                                                </div>
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-right text-right">Rs. 0.0</span>
                                                                </div>
                                                            </div>

                                                            <hr class="border">

                                                            <div class="row">
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-left">Operational Charges:</span>
                                                                </div>
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-right text-right">Rs. <?php echo!empty($rtempData->ticket_detail->msrtc->reservation_charge) ? $rtempData->ticket_detail->msrtc->reservation_charge : '0' ?></span>
                                                                </div>
                                                            </div>
                                                             <hr class="border">
                                                            <div class="row">
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-left">ASN Charges:</span>
                                                                </div>
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-right text-right">Rs. <?php echo!empty($rtempData->ticket_detail->msrtc->asn_amount) ? $rtempData->ticket_detail->msrtc->asn_amount : '0' ?></span>
                                                                </div>
                                                            </div>
                                                             <hr class="border">
                                                            <div class="row">
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-left">Service Charges:</span>
                                                                </div>
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-right text-right">Rs. <?php echo!empty($rtempData->ticket_detail->msrtc->ac_service_charges) ? $rtempData->ticket_detail->msrtc->ac_service_charges : '0' ?></span>
                                                                </div>
                                                            </div>

                                                            <hr class="border">

                                                            <div class="row">
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-left">Return Journey Price:</span>
                                                                </div>
                                                                <div class="col-md-6 col-xs-6">
                                                                    <span class="pull-right text-right">Rs. <?php echo!empty($rtempData->total_fare) ? $rtempData->total_fare : '0' ?></span>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="bg-purple grandt color-w">
                                                                    <div class="col-md-6 col-xs-6">
                                                                        <span class="pull-left">Grand Total</span>
                                                                    </div>
                                                                    <div class="col-md-6 col-xs-6">
                                                                        <span class="pull-right text-right">Rs.<?php echo!empty($rtempData->total_fare) ? $rtempData->total_fare : '0' ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </article>
                                                    </div>
                                                </div>
                                            </div>
                                                <?php endif; ?>
                                                <!-- End return ticket -->
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="bg-purple amt-pay color-w">Amount Payable : Rs. <?php echo isset($transactionAmount) ? $transactionAmount : 0?>/-</div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="mt20 bg-purple1 click-tc">
                                                    <input name="terms_condition" id="term_condition" type="checkbox" value=""> By clicking on Make Payment, you agree to all the <a href="#">Terms and Conditions</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <label>Coupon</label>
                                        <div class="coupon">
                                            <div class="col-md-6 col-xs-8">
                                                <div class="form-group mt10">
                                                    <input name="coupon_code" id="coupon_code" value="" type="text" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-4 mt10"><button type="button" class="btn btn-purple txt-w">Apply</button></div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            <input type="hidden" id="block_key" name="block_key" value="<?php echo!empty($tempData->block_key) ? $tempData->block_key : '0' ?>" />
                                            <input type="hidden" id="count_seat" name="count_seat" value="<?php echo (!empty($totalSeat) ? $totalSeat : '0'); ?>" />
                                            <input type="hidden" id="rblock_key" name="rblock_key" value="<?php echo!empty($rtempData->block_key) ? $rtempData->block_key : '0' ?>" />
                                            <input type="hidden" id="rcount_seat" name="rcount_seat" value="<?php echo (!empty($rtotalSeat) ? $rtotalSeat : '0'); ?>" />
                                            <button type="button" id="confirm_book_ticket" class="btn btn-purple btn-lg btn-block login-button txt-w col-sm-4 mt20">Book Ticket</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="modal fade popup col-md-12" id="isCancelable_popup" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog add_amt">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class=""></i>&nbsp; Errors!</h3>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="col-md-12 col-xs-12" >
                                <div class="info cancel-info">
                                    <span class="fnt09 purple">Status </span> : &nbsp <strong id="msg"></strong>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer clearfix">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                    </div>
                    </form>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </section>

    <!-- Signin Content End -->             
</div>
<!-- Inner Wrapper End -->
<script type="text/javascript">
    $(document).ready(function() {

        $("#confirm_book_ticket").click(function(e) {
            e.preventDefault();
            var block_key = $("#block_key").val();
            var count_seat = $("#count_seat").val();
            var rblock_key = $("#rblock_key").val();
            var rcount_seat = $("#rcount_seat").val();
            var term_condition = $("#term_condition").val();
            var coupon_code = $("#coupon_code").val();
           if(block_key == '' || count_seat == '') {
               alert('Can not empty block key and Seat count.'); 
           }
            if(!$('#term_condition').is(':checked')){
                alert('Kindly click on terms and condition'); 
                return false;
            }
            $('#loader').css({"display": "block"});
            $.ajax({
                type: "POST",
                dataType: "json",
                url: base_url + "admin/Search_bus/confirm_booking",
                data: {'block_key': block_key, 'count_seat': count_seat, 'rblock_key': rblock_key, 'rcount_seat': rcount_seat,'coupon_code': coupon_code},
                success: function(data) {
                    
                    if(data.transaction_status == 'success') {
                        //alert('Your tickes has been booked '+ data.pnr_no);
                        window.location.href = data.redirect;
                    } else
                    {
                        //alert(JSON.stringify(data.message));
                        $('#loader').css({"display": "none"});
                        $('#msg').text(data.message);
                        $('#isCancelable_popup').modal('show').delay(10000);
                        window.location.href = data.redirect;
                    }
                    // window.location.href = data.redirect+"?refrence_no="+data.transaction_no;
                    //window.location.href = data.redirect;
                }
            });
        });
    });
</script>