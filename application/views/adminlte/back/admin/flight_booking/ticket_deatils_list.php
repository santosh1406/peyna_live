<style>
    .fields{display:none;}
</style>
<section class="content-header">
    <h1>Ticket Details Report</h1>
</section>

<!-- Main content -->
<section class="content">
    <div id="commission_wrapper">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body" id="table_show">

                        <?php
                        $attributes = array('class' => 'form', 'id' => 'trans_list_filter', 'name' => 'trans_list_filter', 'method' => 'post', 'onsubmit' => 'return false;');
                        echo form_open('', $attributes);
                        ?>
                        <div class="row box-title">
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-1">From Date</label>
                                    <input class="form-control date" id="from_date" placeholder="<?php echo date('d-m-Y'); ?> " value="<?php if (!empty($from_date)) echo $from_date; ?>" type="text" >
                                </div>
                            </div>

                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2">To Date</label
                                    <input type="hidden" name="def_to_date" id="def_to_date" value="<?php echo date('Y-m-d'); ?> ">
                                    <input type="hidden" name="def_from_date" id="def_from_date" value="<?php echo date('Y-m-d', strtotime(date('d-m-Y'))); ?>">

                                    <input class="form-control date" id="to_date" placeholder="<?php echo date('d-m-Y'); ?>" value="<?php if (!empty($till_date)) echo $till_date; ?>" type="text">
                                </div>
                            </div>

                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-1">Provider</label>
                                    <select name="provider_type" id="provider_type" class="form-control mb10" >                                        
                                        <option value="">Select status</option>
                                        <option value="">All</option>
                                        <option value="MSRTC">MSRTC</option>
                                        <option value="Travelyaari">Travelyaari</option>                                        
                                    </select>
                                </div>
                            </div>

                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-1">Transaction status</label>
                                    <select name="trans_type" id="trans_type" class="form-control mb10" >                                        
                                        <option value="">All</option>
                                        <option value="success" > Success </option>
                                        <option value="incomplete"> Incomplete </option>
                                        <option value="pfailed"> Payment Initiate </option>
                                        <option value="aborted"> Aborted </option>
                                        <option value="failure"> Payment Failed </option>
                                        <option value="failed"> Failed </option>
                                        <option value="cancel"> Cancel </option>                                 
                                    </select>
                                </div>
                            </div>

                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-1">Select Columns</label>
                                    <div style="height: 7em; width: 20em; overflow: auto;" class="form-control">

                                        <input type="checkbox" class="select_check" checked="checked" value="1" />&nbsp;BOS Reference No.</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="2"/>&nbsp;PNR No</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="3"/>&nbsp;ETS Ref No</br>
                                        
                                        <input type="checkbox" class="select_check" checked="checked" value="4"/>&nbsp;User Type</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="5"/>&nbsp;User</br>
                                       
                                        <input type="checkbox" class="select_check" checked="checked" value="6"/>&nbsp;Email Id</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="7"/>&nbsp;Agent Id</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="8"/>&nbsp;Mobile No</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="9"/>&nbsp;State</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="10"/>&nbsp;City</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="11"/>&nbsp;Operator Name</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="12"/>&nbsp;Provider Name</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="13"/>&nbsp;From Stop</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="14"/>&nbsp;To Stop</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="15" />&nbsp;Issued date</br>
                                        
                                        <input type="checkbox" class="select_check" checked="checked" value="16"/>&nbsp;DOJ</br>
                                       
                                        <input type="checkbox" class="select_check" checked="checked" value="17"/>&nbsp;No of passenger</br>                
                                        
                                        <input type="checkbox" class="select_check" checked="checked" value="18"/>&nbsp;Basic Fare</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="19"/>&nbsp;Service Tax</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="20"/>&nbsp;Total Fare</br>
                                        
                                        <input type="checkbox" class="select_check" checked="checked" value="21" />&nbsp;Discount On</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="22"/>&nbsp;Discount Type</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="23"/>&nbsp;Discount Percentage</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="24"/>&nbsp;Discount Price</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="25"/>&nbsp;Total Fare Paid</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="26"/>&nbsp;Refund Amount</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="27"/>&nbsp;Cancel Charge</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="28"/>&nbsp;PG Track Id</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="29"/>&nbsp;Payment By</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="30"/>&nbsp;Transaction Status</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="31"/>&nbsp;Ticket Ref No</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="32"/>&nbsp;Ticket Id</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="33"/>&nbsp;Commission percentage</br>
                                        <input type="checkbox" class="select_check" checked="checked" value="34"/>&nbsp;Commission value</br>  
                                        <input type="checkbox" class="select_check" checked="checked" value="34"/>&nbsp;Agent Commission Value</br>  
                                        <input type="checkbox" class="select_check" checked="checked" value="34"/>&nbsp;Agent GST</br>


                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="row box-title">
                            <!-- <div style="display: block;" class="col-md-2 display-no">    
                                <div style="position: static;" class="form-group">
                                    <label for="select-1">Select State</label>
                                    <select class="form-control chosen_select" id="state_type" name="state_type">
                                        <option value="">Select State</option>
                                        <option value="Credited">Credit</option>
                                        <option value="Debited">Debit</option>
                                    </select>
                                </div>
                            </div> -->

                            <!-- <div style="display: block;" class="col-md-2 display-no">    
                                <div style="position: static;" class="form-group">
                                    <label for="select-1">Select City</label>
                                    <select class="form-control chosen_select" id="city_type" name="city_type">
                                        <option value="">Select City</option>
                                        <option value="Credited">Credit</option>
                                        <option value="Debited">Debit</option>
                                    </select>
                                </div>
                            </div> -->

                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-1">User Type</label>
                                    <select name="user_type" id="user_type" class="form-control mb10" >                                        
                                        <option value="">Select User</option>
                                        <option value="">All</option>
                                        <option value="Agent">Agent</option>
                                        
                                    </select>
                                </div>
                            </div>

                            <!-- <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-1">Payment By</label>
                                    <select name="payment_by" id="payment_by" class="form-control mb10" >                                        
                                        <option value="">Select Payment By</option>
                                        <option value="">All</option>
                                        <option value="wallet">Wallet</option>
                                    </select>
                                </div>
                            </div> -->  

                        </div>

                        <div class="row box-title">
                            <div style="display: block;" class="col-md-2 display-no">    
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-1">Departure From Date</label>
                                    <input class="form-control date" id="departure_from_date" placeholder="<?php echo date('d-m-Y'); ?> " value="<?php if (!empty($from_date)) echo $from_date; ?>" type="text" >
                                </div>
                            </div>

                            <div style="display: block;" class="col-md-2 display-no">    
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-1">Depature Till Date</label>
                                    <input class="form-control date" id="departure_till_date" placeholder="<?php echo date('d-m-Y'); ?> " value="<?php if (!empty($from_date)) echo $from_date; ?>" type="text" >
                                </div>
                            </div>
                        </div>

                        <div class="row"> 
                            <div style="display: block;" class="col-md-1 display-no">
                                <div style="position: static;" class="form-group">
                                    <button class="btn btn-primary" name="search" id="search" type="submit"><span></span>Search</button>
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-1 display-no">
                                <div style="position: static;" class="form-group">
                                    <button class="btn btn-primary" name="reset_btn" id="reset_btn" type="reset">Reset</button>
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <button class="btn btn-primary" id="extoexcel" type="button">
                                        <span class="glyphicon glyphicon-export"></span>
                                        Export to Excel</button>  
                                </div>
                            </div>
                        </div>


                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                    </div>
                    <?php echo form_close(); ?>

                    <table id="tran_list" name="tran_list" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                    <th>Sr No</th>
                                    <th>Bos Ref No</th>
                                    <th>PNR No</th>
                                    <th>ETS Ref No</th>
                                    <th>User Type</th>
                                    <th>User Name</th>
                                    <th>Email Id</th>
                                    <th>User/Agent Id</th>
                                    <th>Mobile No.</th>
                                    <!-- <th>State</th>
                                    <th>City</th> -->
                                    <th>Operator Name</th>
                                    <!-- <th>Provider Name</th> -->
                                    <th>From Stop</th>
                                    <th>To Stop</th>
                                    <!-- <th>Issued date</th>                                        
                                    <th>DOJ</th> -->
                                    <th>No of passenger</th>
                                    <th>Basic Fare</th>
                                    <th>Service Tax</th>
                                    <th>Total Fare Without Discount</th>                                    
                                    <!-- <th>Discount On</th>
                                    <th>Discount Type</th> -->
                                    <th>Discount Percentage</th>
                                   <!--  <th>Discount Price</th>
                                    <th>Total Fare Paid</th> -->
                                    <th>Refund Amount</th>
                                    <th>Cancel Charge</th>
                                    <!-- <th>PG Track Id</th>
                                    <th>Payment By</th> -->
                                    <th>Transaction Status</th>
                                    <!-- <th>Ticket Ref No</th> -->
                                    <th>Ticket Id</th>
                                    <th>Commission Percentage</th>
                                    <!-- <th>Commission Value</th> -->
                                    <!-- <th>Agent Commission Value</th>
                                    <th> Agent GST</th> -->
                                    <!-- <th>Passenger Detail</th>  -->
                                </tr>
                        </thead>

                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</section>


<script type="text/javascript">
    $(document).ready(function() {
        var tconfig = {
            "processing": true,
            "serverSide": true,
            "iDisplayLength": 10, 
            "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],            
            "ajax": {
                "url": BASE_URL + "report/Searchbus_report/ticket_details_transaction",
                "type": "POST",
                "data": "json",
                data   : function(d) {
                    var date = new Date();
                    var from_date;
                    var till_date;
                    var departure_from_date;
                    var departure_till_date;
                    if ($("#from_date").val() == '') {
                        from_date = $("#def_from_date").val();

                    } else {
                        from_date = $("#from_date").val();
                    }
                    if ($("#to_date").val() == '') {
                        till_date = $("#def_to_date").val();
                    } else {
                        till_date = $("#to_date").val();
                    }
                    
                    if ($("#departure_from_date").val() == '') {
                          // departure_from_date = $("#def_from_date").val();
                           departure_from_date = '';
                    } else {
                        departure_from_date = $("#departure_from_date").val();
                    }
                    if ($("#departure_till_date").val() == '') {
                        departure_till_date = '';
                       // departure_till_date = $("#def_to_date").val();
                    } else {
                        departure_till_date = $("#departure_till_date").val();
                    }
                    
                    d.from_date = from_date;
                    d.to_date = till_date;
                    d.provider_type = $("#provider_type option:selected").val();
                    d.trans_type = $("#trans_type option:selected").val();   
                    if (d.tran_type == '') {
                            d.tran_type = 'success';
                        }
                    
                    d.state_type = $("#state_type option:selected").val(); 
                    d.city_type = $("#city_type option:selected").val(); 
                    d.user_type = $("#user_type option:selected").val(); 
                    d.payment_by = $("#payment_by option:selected").val(); 
                    d.departure_from_date = departure_from_date;
                    d.departure_till_date = departure_till_date;
                    
                    
                }
            },
            "columnDefs": [
                {
                    "searchable": false
                }
            ],
            "order": [[3, "desc"]],
            "scrollX": true,
            "searching": true,
            "bFilter": false,
            "bPaginate": true,
            "bDestroy": true,
            "aoColumnDefs": [
                {
                    "bSortable": false,
                    "aTargets": [0]
                },
                {
                    "bSearchable": false,
                    "aTargets": [0, 3, 4, 5, 6, 7, 8, 9]
                },
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                // console.log(aData);
                var info = $(this).DataTable().page.info();
                $("td:first", nRow).html(info.start + iDisplayIndex + 1);
                return nRow;
            },
            fnDrawCallback: function(data) {
                if (oData.fnGetData().length == 0)
                {
                    $("#extoexcel").attr("disabled", "disabled"); 

                } else {
                     $("#extoexcel").removeAttr("disabled"); 

                }
            },
            "oLanguage": {
                "sProcessing": '<img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>'
            },
        };

        oData = $('#tran_list').dataTable(tconfig);
            $(document).off('click', '#search').on('click', '#search', function(e) {
            oData.fnDraw();
        });
        $(document).off('click', '#reset_btn').on('click', '#reset_btn', function (e) {
        location.reload(true);
        });
        $("#from_date").datepicker({
            'dateFormat': 'dd-mm-yy',
            maxDate: 0
        }).on('change', function() {
            //$(this).valid();  // triggers the validation test
            // '$(this)' refers to '$("#datepicker")'
        });

        $("#to_date").datepicker({
            'dateFormat': 'dd-mm-yy',
            maxDate: 0
        }).on('change', function() {
            //$(this).valid();  // triggers the validation test
            // '$(this)' refers to '$("#datepicker")'
        });
        
        $("#departure_from_date").datepicker({
            'dateFormat': 'dd-mm-yy',
        }).on('change', function() {
            //$(this).valid();  // triggers the validation test
            // '$(this)' refers to '$("#datepicker")'
        });
        
        $("#departure_till_date").datepicker({
            'dateFormat': 'dd-mm-yy',            
        }).on('change', function() {
            //$(this).valid();  // triggers the validation test
            // '$(this)' refers to '$("#datepicker")'
        });
        
        $('#extoexcel').on('click', function(e) 
            { 
                var from_date;
                var till_date;
                if ($("#from_date").val() == '') {
                    from_date = $("#def_from_date").val();

                } else {
                    from_date = $("#from_date").val();
                }
                if ($("#to_date").val() == '') {
                    till_date = $("#def_to_date").val();
                } else {
                    till_date = $("#to_date").val();
                }
                from = from_date;
                to = till_date;
                provider_type = $("#provider_type option:selected").val();
                trans_type = $("#trans_type option:selected").val();                     
                state_type = $("#state_type option:selected").val(); 
                city_type = $("#city_type option:selected").val(); 
                user_type = $("#user_type option:selected").val(); 
                payment_by = $("#payment_by option:selected").val(); 
                departure_from_date = $("#departure_from_date").val();
                departure_till_date = $("#departure_till_date").val();
                 
                if(!(provider_type)){
                    provider_type = '';
                }
                if(!(trans_type)){
                    trans_type = '';
                }
                if(!(user_type)){
                    user_type = '';
                }
//                txn_type = $("#txn_type option:selected").val();
//                txn_desc = $("#txn_desc option:selected").val();
//                txn_mode = $("#txn_mode option:selected").val();     

              window.location.href=BASE_URL+'report/Searchbus_report/ticket_details_excel?from_date='+from+"&to_date="+to+"&provider_type="+provider_type+"&trans_type="+trans_type+"&user_type="+user_type;   
          });

          $(document).off('ifChanged', '.select_check').on('ifChanged', '.select_check', function (e) {
                e.preventDefault();
                if ($(this).prop('checked') == true) {
                    $(this).parent().addClass("checked");
                    oTable.fnSetColumnVis($(this).val(), true);
                } else {
                    oTable.fnSetColumnVis($(this).val(), false);
                }
            });
            
            $(document).off('change', '.toggle-vis').on('change', '.toggle-vis', function(e) {
                e.preventDefault();
                oTable.fnSetColumnVis($(this).val(), false);
            });
            $('#tran_list head tr th').each(function() {
                // $('#hide_show_col').
            });
            
            oTable = $('#tran_list').dataTable(tconfig);
            $('#trans_list_filter').submit(function(e) {
                e.preventDefault();
                oTable.fnDraw();
            });
    });
</script>


