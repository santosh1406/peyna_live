<link href='<?php echo base_url() . BACK_ASSETS ?>css/search_style/style.css' rel="stylesheet" type="text/css" />
<style>
    .loader {
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #933b89;
        height: 120px;
        width: 120px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
        position: absolute;
        float: right;
        position: fixed;
        z-index: 9999999;
        left: 53%;
        top: 50%;
        /*background-color: #000;*/
        opacity: 0.7;
    }

    #loader{
        background: #000;
        opacity: 0.7;
        position: fixed;
        width: 100%;
        height: 120%;
        display: none;
        z-index: 999999;
        top: 0;
    }


    /* Safari */
    @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
    .ladies_selected{
        background-position: -20px -20px;
    }
    input[type=range]::-moz-range-thumb {
        background-color: #933b89;
    }
    .loader-txt{color:#fff; font-size:2em; text-align:center; position:absolute; top: 30%; left: 26%;}
</style>
<!-- Inner Wrapper Start -->
<div id="page-wrapper">
    <div id="loader">
        <div class="loader-txt">Please wait, your bus services are loading...</div>
        <div class="loader" ></div>
    </div>
    <!-- Signin Content Start -->
    <section class="inner-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h3 class="text-center purple">Search Result</h3>

                    <div class="searchp-form well mb10">                       
                        <form action="#" method="POST" id="search_services" name="bus_booking" accept-charset="utf-8" class="form-inline mb20">
                            <div class="form-group">
                                <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                    <input class="form-control mb10" id="from_stop" name="from_stop"  type="text" placeholder="From" value="<?php echo $this->session->userdata('from'); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                    <input class="form-control mb10" id="to_stop" name="to_stop" type="text" placeholder="To" value="<?php echo $this->session->userdata('to'); ?>">
                                </div>
                            </div>
                            <div class="form-group has-feedback">
                                <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                    <input type="text" class="form-control mb10"  id="datepicker"  name="departure_date" placeholder="Departure Date" value="<?php echo $this->session->userdata('date'); ?>">
                                    <span class="glyphicon glyphicon-calendar form-input-icon icon-calendar"></span>
                                </div>
                            </div>
                            <div class="form-group has-feedback">
                                <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                    <input type="text" class="form-control mb10" id="datepicker1" name="return_date" placeholder="Return Date (opt)" value="<?php echo $this->session->userdata('return_date'); ?>">
                                    <span class="glyphicon glyphicon-calendar form-input-icon icon-calendar"></span>
                                </div>
                            </div>
                            <div class="form-group has-feedback">
                                <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                    <select name="provider" id="disabledSelect" class="form-control mb10" placeholder="Bus Operator Type (All)">

                                        <option value="all">Bus Operator Type (All)</option>
                                        <option value="operator">State Transport Operator</option>
                                        <option value="api">Private Operator</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group has-feedback">
                                <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                    <select  name="searchbustype" id="disabledSelect" class="form-control mb10"  placeholder="Bus Operator Type (All)">
                                        <option value="all">Bus Type (All)</option>
                                        <option value="ac">AC</option>
                                        <option value="non_ac">Non AC</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group has-feedback">
                                <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                    <select id="disabledSelect" class="form-control mb10" placeholder="Time Slot (All)" name="timeslot">
                                        <option value=""></option>
                                        <option value="morning">Morning (6:00 - 11:59)</option>
                                        <option value="afternoon">Afternoon (12:00 - 15:59)</option>
                                        <option value="evening">Evening (16:00 - 18:59)</option>
                                        <option value="night">Night (19:00 - 6:00)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                    <button type="button" id="search_buses" class="btn btn-purple txt-w mb10">Modify Search</button>
                                </div>
                            </div>
                            <input type="hidden" name="request_type" value="ajax_req"> 
                        </form>
                        <div class="next-re-search mb10">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-inline clearfix search-bar">
                                    <?php if(!empty($this->session->userdata['return_date'])) : ?>
                                    <!--onward  journey -->
                                    <div class="form-group next-pre col-sm-12 col-md-6 text-center" id="onwardJourney">
                                        <div class="from-to-search">
                                            <span class="mr10"><?php echo strtoupper($this->session->userdata['from']) ?></span>
                                            <span class="fa fa-arrow-right"></span>
                                            <span class="ml10"><?php echo strtoupper($this->session->userdata['to']) ?></span>
                                        </div>
                                        <div class="next_previous">
                                            <span>Previous</span>
                                            <span class="fa fa-chevron-left mr5 cursor-p"></span>
                                            <span id="date_shown"><?php echo date("d-M-Y", strtotime($this->session->userdata['date'])); ?></span>
                                            <span class="fa fa-chevron-right ml5 cursor-p"></span>
                                            <span>Next</span>
                                        </div>
                                    </div>
                                    <!--return journey -->
                                    <div class="form-group next-pre col-sm-12 col-md-6 text-center opacity-03" id="returnJourney">
                                        <div class="from-to-search">
                                            <span class="mr10"><?php echo strtoupper($this->session->userdata['to']) ?></span>
                                            <span class="fa fa-arrow-right"></span>
                                            <span class="ml10"><?php echo strtoupper($this->session->userdata['from']) ?></span>
                                        </div>
                                        <div class="next_previous">
                                            <span>Previous</span>
                                            <span class="fa fa-chevron-left mr5 cursor-p"></span>
                                            <span id="date_shown"><?php echo date("d-M-Y", strtotime($this->session->userdata['return_date'])); ?></span>
                                            <span class="fa fa-chevron-right ml5 cursor-p"></span>
                                            <span>Next</span>
                                        </div>
                                    </div>
                                    <?php else:?>
                                    <div class="form-group next-pre col-sm-12 col-md-12 text-center">
                                        <div class="from-to-search">
                                            <span class="mr10"><?php echo strtoupper($this->session->userdata['from']) ?></span>
                                            <span class="fa fa-arrow-right"></span>
                                            <span class="ml10"><?php echo strtoupper($this->session->userdata['to']) ?></span>
                                        </div>
                                        <div class="next_previous">
                                            <span>Previous</span>
                                            <span class="fa fa-chevron-left mr5 cursor-p"></span>
                                            <span id="date_shown"><?php echo date("d-M-Y", strtotime($this->session->userdata['date'])); ?></span>
                                            <span class="fa fa-chevron-right ml5 cursor-p"></span>
                                            <span>Next</span>
                                        </div>
                                    </div>
                                    <?php endif;?>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Modify Search Ends-->

            <div class="row" id="loading">
                <!--<div id="services_view_render">
                    <?php //$this->load->view('adminlte/back/admin/advanced_booking/services_view'); ?>
                </div>   -->
		<div class="col-sm-4 col-md-2">
    <h4 class="search-results-title">
        <i class="glyphicon glyphicon-search mr10 icon-search"></i>
        <b><span id="total-result-found"><?php echo count($serviceResult['result']); ?></span></b> results found.
    </h4>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <i class="short-full glyphicon glyphicon-plus"></i>
                        Fare
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse price-filter" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                <div class="panel-body">
                    <span><?php echo $min_fare?></span> <span style="float:right"><?php echo $max_fare?></span> 
                    <input id="range_slider" type="range" min="<?php echo $min_fare?>" max="<?php echo $max_fare?>" value="<?php echo $min_fare?>"  />  
                    
                </div>
                    <input id="ex1" data-slider-id="ex1Slider" type="text" data-slider-min="-5" data-slider-max="20" data-slider-step="1" data-slider-value="14" style="display: none;" data-value="11" value="11">
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <i class="short-full glyphicon glyphicon-plus"></i>
                        Travels
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                    <form action="" method="get">
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="MSRTC" type="checkbox" value=""> <a href="#">MSRTC</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">Neeta Tours and Travels</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        <i class="short-full glyphicon glyphicon-plus"></i>
                        Bus Type
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                    <form action="" method="get">
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="MSRTC" type="checkbox" value=""> <a href="#">Reclined [Seats]</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">Non AC Seater</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">AC Semi Sleeper</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">AC Sleeper</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">AC Seater</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">Others</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingFour">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        <i class="short-full glyphicon glyphicon-plus"></i>
                        Departure Time
                    </a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                <div class="panel-body">
                    <form action="" method="get">
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="MSRTC" type="checkbox" value=""> <a href="#">00:00 - 03:59(Mid Night)</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">04:00 - 11:59 (Morning)</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">12:00 - 15:59 (Afternoon)</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">16:00 - 18:59 (Evening)</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">19:00 - 23:59 (Night)</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingFive">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                        <i class="short-full glyphicon glyphicon-plus"></i>
                        Duration
                    </a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                <div class="panel-body">
                    <form action="" method="get">
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="MSRTC" type="checkbox" value=""> <a href="#">Upto 6 Hours</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">6 Hours - 12 Hours</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">12 Hours - 24 Hours</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">More than a day</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div><!-- panel-group -->
</div>

<div class="col-sm-8 col-md-10">
    <div class="sort-by-section clearfix mb10">
        <h4 class="sort-by-title block-sm">Sort results by:</h4>
        <ul class="sort-bar clearfix">
            <li class="sort-by-name sort_col" ref="data-travel" data-sort="asc"><a class="sort-by-container" href="#"><span>Travels</span></a></li>
            <li class="sort-by-depature sort_col" ref="data-dep_tm_str" data-sort="asc"><a class="sort-by-container" href="#"><span>Depature</span></a></li>
            <li class="sort-by-seats sort_col" ref="data-seat" data-sort="asc"><a class="sort-by-container" href="#"><span>Seats</span></a></li>
            <li class="sort-by-fare sort_col" ref="data-fare" data-sort="asc"><a class="sort-by-container" href="#"><span>Fare</span></a></li>
        </ul>
    </div>

    <div class="bus-service-list">
        <div class="state" id="services_view_render"> 
            
            <div class="panel panel-default">
                <div class="boxstyle1" role="tab">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            <i class="adddetails glyphicon glyphicon-plus"></i>
                            <h4 class="service-title">
                               
                                <?php if (strtolower($serviceResult['result'][0]['operator']) == 'msrtc') : ?>
                                <span class="service-title-main clearfix">Maharashtra State Road Transport Corporation</span>
                                <?php elseif(strtolower($serviceResult['result'][0]['operator_name']) == 'upsrtc') :?>
                                <span class="service-title-main clearfix">Uttar Pradesh State Road Transport Corporation</span>
                                <?php else:?>
                                <span class="service-title-main clearfix">Private operator</span>
                                <?php endif;?>
                                <small>Total Bus - <span id="totalBusCnt"><?php echo count($serviceResult['result']); ?> </span></small>
                            </h4>
                            <div class="sub-content">
                                <div class="col-md-2 col-xs-12">
                                    <div class="start-time padding20 border-r">
                                        <i class="soap-icon-adventure purple soap-icon"></i>
                                        <div class="service-details">
                                            <span class="text-uppercase txt-p">Starts at</span>
                                            <span><small><?php echo $startTime; ?></small></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-xs-12">
                                    <div class="duration padding20 border-r">
                                        <i class="soap-icon-clock purple soap-icon"></i>
                                        <div class="service-details">
                                            <span class="text-uppercase txt-p">Duration</span>
                                            <span><small><?php echo isset($min_time) ? $min_time : '' ?> - <?php echo isset($max_time) ? $max_time : '' ?></small></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-xs-12">
                                    <div class="available-seats padding20 border-r">
                                        <i class="soap-icon-passenger purple soap-icon"></i>
                                        <div class="service-details">
                                            <span class="text-uppercase txt-p">Available Seats</span>
                                            <span><small><?php echo $totalSeat; ?></small></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-xs-12">
                                    <div class="amenities padding20 border-r">
                                        <i class="fa fa-mobile circle"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </h4>
                </div>

            </div>
            
            <!--- State Road Transport Start -->
            <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                <div class="panel panel-default">
                    <div class="boxstyle1" role="tab">
                        
                        <article class="box fare_sort_list article_<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?> stu_sort_list" data-fare="<?php echo isset($value['seat_fare']) ? $value['seat_fare'] : '' ?>" data-child-fare="<?php echo isset($value['child_seat_fare']) ? $value['child_seat_fare'] : '' ?>" data-seat="<?php echo isset($value['available_seat']) ? $value['available_seat'] : '' ?>" data-bus-type="<?php echo isset($value['bus_type_name']) ? $value['bus_type_name'] : '' ?>" data-travel="<?php echo isset($value['operator_name']) ? $value['operator_name'] : '' ?>" data-duration-time="" data-dep_tm_str="<?php echo isset($value['departure_date']) ? $value['departure_date'] : '' ?>" data-op-bus-type-cd="SS" data-service-discount="false" op-trip-no="<?php echo isset($value['trip_id']) ? $value['trip_id'] : '' ?>" data-op-name="MSRTC" data-trip-no="<?php echo isset($value['trip_id']) ? $value['trip_id'] : '' ?>" data-bus-travel-id="<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>" data-from="<?php echo isset($value['from_stop']) ? $value['from_stop'] : '' ?>" data-to="<?php echo isset($value['to_stop']) ? $value['to_stop'] : '' ?>" data-date="<?php echo isset($value['departure_date']) ? $value['departure_date'] : '' ?>" data-provider-id="<?php echo isset($value['provider_id']) ? $value['provider_id'] : '' ?>" data-provider-type="stu"  data-travel-duration="" inventory-type="<?php echo isset($value['inventory_type']) ? $value['inventory_type'] : 0 ?>" >
                                <div class="boxstyle1 bg-grey-l" role="tab">
                                    <h4 class="panel-title">
                                        <a> 
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <h4 class="service-title">
                                                        <span class="service-title-main clearfix"><?php echo isset($value['operator_name']) ? $value['operator_name'] : '' ?> - <span class="from-to purple"><?php echo isset($value['route_no_name']) ? $value['route_no_name'] : '' ?></span></span>
                                                        <small class="bus-type"><?php if(!is_array($value['bus_type_name'])){echo isset($value['bus_type_name']) ? $value['bus_type_name'] . " / " . $value['op_bus_type_name'] : '';}else{array_pop($value['bus_type_name']);echo implode(' / ', $value['bus_type_name']);} ?> </small>
                                                    </h4>
                                                </div>
                                                <div class="col-md-3">
                                                    <span class="pull-right purple fnt16"><strong>Rs. <?php echo isset($value['seat_fare']) ? $value['seat_fare'] : '' ?>/-</strong></span>
                                                </div>
                                            </div>
                                            <div class="sub-content">
                                                <div class="col-md-2 col-xs-12">
                                                    <div class="start-time padding20 border-r">
                                                        <i class="soap-icon-adventure purple soap-icon"></i>
                                                        <div class="service-details">
                                                            <span class="text-uppercase txt-p">Departure</span>
                                                            <span><small id="sch_departure_<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>"><?php echo isset($value['departure_date']) ? $value['departure_date'] : '' ?></small></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-xs-12">
                                                    <div class="duration padding20 border-r">
                                                        <i class="soap-icon-departure purple soap-icon"></i>
                                                        <div class="service-details">
                                                            <span class="text-uppercase txt-p">Alighting</span>
                                                            <span><small id="sch_alighting_<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>"><?php echo isset($value['arrival_date']) ? $value['arrival_date'] : '' ?></small></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-xs-12">
                                                    <div class="duration padding20 border-r">
                                                        <i class="soap-icon-clock purple soap-icon"></i>
                                                        <div class="service-details">
                                                            <span class="text-uppercase txt-p">Duration</span>
                                                            <span><small><?php echo isset($value['duration']) ? $value['duration'] : '' ?> HRS</small></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-xs-12">
                                                    <div class="available-seats padding20 border-r">
                                                        <i class="soap-icon-passenger purple soap-icon"></i>
                                                        <div class="service-details">
                                                            <span class="text-uppercase txt-p">Available Seats</span>
                                                            <span><small><?php echo isset($value['available_seat']) ? $value['available_seat'] : '' ?></small></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-xs-12">
                                                    <div class="amenities padding20 border-r">
                                                        <i class="fa fa-mobile circle-d purple"></i>
                                                        <i class="soap-icon-notice circle purple"></i>
                                                        <i class="soap-icon-close circle purple"></i>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-xs-12">
                                                    <!--<button type="submit" id="submit_filter_btn" class="btn btn-purple txt-w mt20">Select Seat</button>-->
                                                    <button class="collapsed btn btn-purple txt-w mt20 view_bus_service_stops" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven" id="submit_filter_seat" data-trip-id="<?php echo isset($value['trip_id']) ? $value['trip_id'] : '' ?>" data-inventory-type="<?php echo isset($value['inventory_type']) ? $value['inventory_type'] : 0 ?>" data-service-id="<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>" data-provider-id="<?php echo isset($value['provider_id']) ? $value['provider_id'] : '' ?>" data-departure-date="<?php echo isset($value['departure_date']) ? $value['departure_date'] : '' ?>" data-op-name="<?php echo isset($value['operator_name']) ? $value['operator_name'] : '' ?>">Select Seat</button>
                                                </div>
                                            </div>
                                        </a>
                                    </h4>

                                    <div class="bording-alighting-div clearfix" id="view_stops_<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven" style="display:none;">
                                        <div class="col-md-12">
                                            <div class=" box-style3 mb20 clearfix">
                                                <div class="col-md-4 col-xs-12">
                                                    <span class="text-uppercase">Select Boarding Point </span>
                                                    <br>
                                                    <select id="from_stop_<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>" class="form-control mb10" placeholder="Select Bording Point" name="boarding_stop">
                                                        <option>Select Pickup Point</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4 col-xs-12">
                                                    <span class="text-uppercase">Select Dropping Point </span>
                                                    <br>
                                                    <select id="to_stop_<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>" class="form-control mb10" placeholder="Select Dropping Point" name="dropping_stop">
                                                        <option>Select Dropping Point</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-2 col-xs-12">
                                                    <button type="submit" id="select_seat_layout" class="btn btn-purple txt-w mt20" data-trip-id="<?php echo isset($value['trip_id']) ? $value['trip_id'] : '' ?>" data-service-id="<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>" data-provider-id="<?php echo isset($value['provider_id']) ? $value['provider_id'] : '' ?>" data-inventory-type="<?php echo isset($value['inventory_type']) ? $value['inventory_type'] : '' ?>" data-date="<?php echo isset($value['departure_date']) ? $value['departure_date'] : '' ?>"  data-op-name="<?php echo isset($value['operator_name']) ? $value['operator_name'] : '' ?>">Select Seat</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="bus-seat-div clearfix layout_div_close" style="display:none" id="layout_div_<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>" >
                                        <div class="col-md-12">
                                            <div class="box-style3 mb20 clearfix">
                                                <form>
                                                    <div class="clearfix seat-block">
                                                        <h3 class="more-seat-detail red" style="display:block;">Maximum 6 seats allowed per booking</h3>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="legend mb20">
                                                                    <label>
                                                                        <span class="seat available"></span>
                                                                        Available Seat
                                                                    </label>
                                                                    <label>
                                                                        <span class="seat selected"></span>
                                                                        Selected Seat
                                                                    </label>
                                                                    <label>
                                                                        <span class="seat booked"></span>
                                                                        Booked Seat
                                                                    </label>
                                                                    <label>
                                                                        <span class="seat ladies_available"></span>
                                                                        Ladies Seat
                                                                    </label>
                                                                    <label>
                                                                        <span class="seat mens_available"></span>
                                                                        Mens Seat
                                                                    </label>
                                                                    <label>
                                                                        <span class="seat handicapped_available"></span>
                                                                        Blind/Handicapped
                                                                    </label>
                                                                    <label>
                                                                        <span class="seat senior_citizen_available"></span>
                                                                        Senior Citizen
                                                                    </label>
                                                                </div>

                                                                <div class="col-md-8 col-xs-12 seat-layout">
                                                                    <div class="fare-tab-filter show">
                                                                        <ul></ul>
                                                                    </div>
                                                                    <br>
                                                                    <span class="bus-seat-layout" id="bus-seat-layout-<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>">
                                                                        
                                                                    </span>
                                                                </div>
                                                                <div class="col-md-4 col-xs-12 seat-detail-info pull-right mt40">
                                                                    
                                                                        <div class="SelectedSeats clearfix">
                                                                            <label>Seat(s)  :</label>
                                                                            <span class="sel_seat"></span>
                                                                        </div>
                                                                        <div class="amount clearfix">
                                                                            <label>Base Fare : </label>
                                                                            <span class="fare_value">Rs. <b class="fare">0.00</b></span>
                                                                        </div>
                                                                        <div class="amount clearfix" id="gst_amount_div">
                                                                            <label>GST : </label>
                                                                            <span class="fare_value">Rs. <b class="fare_service_tax">0.00</b></span>
                                                                        </div>
                                                                        <div class="amount clearfix">
                                                                            <label>Service Charge : </label>
                                                                            <span class="fare_value">Rs. <b class="fare_service_charge">0.00</b></span>
                                                                        </div>
                                                                        <div class="amount clearfix">
                                                                            <label>Sub Total : </label>
                                                                            <span class="fare_value">Rs. <b class="tot_fare">0.00</b></span>
                                                                        </div>
                                                                        <div class="amount clearfix total_fare_discount_div">
                                                                            <label>Discount : </label>
                                                                            <span class="fare_value">Rs. - <b class="total_fare_discount_value">0.00</b></span>
                                                                        </div>
                                                                         <div class="amount clearfix base_fare_discount_div hide">
                                                                            <label>Discount : </label>
                                                                            <span class="fare_value">Rs. - <b class="base_fare_discount_value">0</b></span>
                                                                        </div>
                                                                        <div class="amount clearfix">
                                                                            <label>Total Amount : </label>
                                                                            <span class="fare_value">Rs. <b class="total_amount">0.00</b></span>
                                                                        </div>
            
                                                                    <div class="col-md-12 boarding-dropping">          
                                                                        <div>
                                                                            <button class="btn txt-w btn-medium btn-purple continue " data-trip-no="<?php echo isset($value['trip_id']) ? $value['trip_id'] : '' ?>" data-bus-travel-id="<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>">Continue</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </article>
                            
                    </div>
                </div>
            </div>

        </div>  

    </div>
            </div>   
            <div class="modal fade popup col-md-12" id="isCancelable_popup" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog add_amt">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title"><i class=""></i>&nbsp; Alert!</h3>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="col-md-12 col-xs-12" >
                                <div class="info cancel-info">
                                    <span class="fnt11 purple">Status </span> : &nbsp <strong id="msg"></strong>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer clearfix">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                    </div>
                    </form>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </section>
    <!-- Inner Wrapper End -->
</div>



<!-- /#wrapper -->

<script>
    var seat_fare_detail = {};
    var temp_seats;
    var onwardJourny = {};
    var returnJourny = {};
    $(document).ready(function() {

        $("#datepicker").datepicker({numberOfMonths: 2, minDate: 0, });
        $("#datepicker1").datepicker({numberOfMonths: 2, minDate: 0});

        /**************************load  from stop ************************************/
        $("#from_stop").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: base_url + "admin/search_bus/get_stop",
                    dataType: "json",
                    data: {
                        term: request.term,
                        // type: rechargeType
                    },
                    success: function(data) {
                        response($.map(data, function(item) {
                            return {
                                label: item.label,
                                name: item.name,
                            };
                        }));
                    }
                });
            },
            autoFocus: true,
            minLength: 1,
            select: function(event, ui) {

                /*$('#error_div').html('');
                 removeBillDetails();*/
                $("#from_stop").val(ui.item.name);

                if (ui.item.name == "No result found") {
                    $("#from_stop").val("");
                } else {
                    $("#from_stop_id").val(ui.item.id);
                }
            }
        });
        /**************************load  from stop ************************************/

        $("#to_stop").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: base_url + "admin/search_bus/get_stop",
                    dataType: "json",
                    data: {
                        term: request.term,
                        // type: rechargeType
                    },
                    success: function(data) {
                        response($.map(data, function(item) {
                            return {
                                label: item.label,
                                name: item.name,
                            };
                        }));
                    }
                });
            },
            autoFocus: true,
            minLength: 1,
            select: function(event, ui) {

                $("#to_stop").val(ui.item.name);

                if (ui.item.name == "No result found") {
                    $("#to_stop").val("");
                } else {
                    $("#to_stop_id").val(ui.item.id);
                }
            }
        });
        /******************************load all stops in to stop input*************/
        $('body').on('change', 'select.boarding', function() {
            var selectedboardstop = $(this).children("option:selected").val();
            $("#boarding_stops").html(selectedboardstop);
        });

        $('body').on('change', 'select.dropping', function() {
            var selecteddropstop = $(this).children("option:selected").val();
            $("#dropping_stops").html(selecteddropstop);
        });

        $("body").on('click', '#search_buses', function(e) {
            var journey_date = $('#datepicker').val();
            var return_date = $('#datepicker1').val();

            $('#loader').css({"display": "block"});
            $("#date_shown").html(moment(journey_date).format('DD-MMM-YYYY'));
            var url = base_url + "admin/search_bus/search_services";
            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: $('#search_services').serialize(),
                async: false,
                success: function(data) {

                    $("body #services_view_render").html(data.results);
                    $('#loader').css({"display": "none"});
                }
            });
            e.preventDefault();
        });

        /********************load boarding and dropping stop*******************************/
        $("body").on('click', '#submit_filter_seat', function() {
            var op_name = $(this).data("op-name");
            if (op_name == 'MSRTC')
            {
                var $current_object = $(this);
                
                if($current_object.text().toLowerCase() == 'hide seat') {
                    $current_object.removeClass("bg_gray").addClass("btn-purple").text("Select Seat");
                }
                var service_id = $(this).data("service-id");
                var provider_id = $(this).data("provider-id");
                var date = $(this).data("departure-date");

                if ($("#view_stops_" + service_id).css('display') == 'block') {

                    $("#view_stops_" + service_id).css({"display": "none"});
                    $("#layout_div_" + service_id).css({"display": "none"});
                    return false;
                }
                $(".view_bus_service_stops").removeClass("bg_gray").addClass("btn-purple").text("Select Seat");

                $(".bording-alighting-div,.layout_div_close").css({"display": "none"});

                if ($("#view_stops_" + service_id).css('display') == 'none')
                {
                    $(this).removeClass("btn-purple").addClass("bg_gray").text("Please Wait");

                }
                var url = base_url + "admin/Search_bus/get_service_stop_details";
                var dataString = 'service_id=' + service_id + '&provider_id=' + provider_id + '&date=' + date;
            } else {
                $('.dropdown').css("display", "block");
                var service_id = $(this).data("service-id");
                var provider_id = $(this).data("provider-id");
                var date = $('.article_' + service_id).data("date");
                var from = $('.article_' + service_id).data("from");
                var to = $('.article_' + service_id).data("to");

                var trip_id = $(this).data("trip-id");
                var inventory_type = $(this).data("inventory-type");
                /* var boarding_stop =
                 var dropping_stop =*/
                var url = base_url + "admin/Search_bus/get_seat_layout";

                var dataString = 'service_id=' + service_id + '&provider_id=' + provider_id + '&date=' + date + '&from=' + from + '&to=' + to + '&trip_id=' + trip_id + '&inventory_type=' + inventory_type + '&operator_name=' + op_name;
            }
            
            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: dataString,
                async: false,
                success: function(data) {

                    if (data.from && data.to)
                    {
                        $("#view_stops_" + service_id).css({"display": "block"});
                        $current_object.removeClass("bg_gray").addClass("btn-purple").text("Hide seat");

                        var result = data.from;
                        var result1 = data.to;
                        var row = "";

                        row += "<option value='0'>Select Boarding Stop</option>";
                        for (i = 0; i < result.length; i++)
                        {
                            row += "<option value='" + result[i].from_bus_stop_name + "'>" + result[i].from_bus_stop_name + " - " + result[i].from_time + "</option>";
                        }


                        var row1 = "";
                        row1 += "<option value='0'>Select Dropping Stop</option>";
                        for (j = 0; j < result1.length; j++)
                        {
                            row1 += "<option value='" + result1[j].to_bus_stop_name + "'>" + result1[j].to_bus_stop_name + " - " + result1[j].to_time + "</option>";
                        }

                        $("#from_stop_" + service_id).html(row);
                        $("#to_stop_" + service_id).html(row1);
                    } else if (op_name == 'UPSRTC') {
                        var listrow = '';
                        seat_fare_detail = data;
                        var result = data.seats;
                        var row = data.total_rows;
                        var col = data.total_cols;
                        var result = data.seats;
                        var total_cols = data.total_cols;
                        var total_rows = data.total_rows;
                        var data = '<ul class="deckrow deckrow_' + service_id + '" >';
                        for (var i = 0; i < col; i++) {
                            data += "<li>";
                            data += "<ul>";
                            for (var j = 0; j < row; j++) {

                                if (i === 0 && j === 0) {
                                    data += '<li><a class=" myBusDriveSeatImage driver_booked" href="#" style="width: 20px; height: 20px;"></a></li>';
                                } else if (j === 0) {
                                    data += '<li><a href="#" style="width: 20px; height: 20px;">   </a></li>';
                                }

                                var isSeat = getSeatDetails(i, j, result, service_id);
                                if (isSeat) {
                                    data += isSeat;
                                } else {
                                    data += '<li><a href="#" style="width: 20px; height: 20px;">    </a></li>';
                                }

                            }
                            data += "</ul>";
                            data += "</li>";
                        }

                        data += "</ul>";
                        $(".layout_div_close").hide();
                        $("#layout_div_" + service_id).show();
                        $("#bus-seat-layout-" + service_id).html(data);
                        $('#loader').css({"display": "none"});
                    } else if (op_name.toLowerCase() == 'travelyaari') {
                        var listrow = '';
                        seat_fare_detail = data;
                        var result = data.seats;
                        //alert(JSON.stringify(result)); return false;
                        var row = data.total_rows;
                        var col = data.total_cols;
                        var result = data.seats;
                        var total_cols = data.total_cols;
                        var total_rows = data.total_rows;
                        var data = '<ul class="deckrow deckrow_' + service_id + '" >';
                        for (var i = 0; i < col; i++) {
                            data += "<li>";
                            data += "<ul>";
                            for (var j = 0; j < row; j++) {

                                if (i === 0 && j === 0) {
                                    data += '<li><a class=" myBusDriveSeatImage driver_booked" href="#" style="width: 20px; height: 20px;"></a></li>';
                                } else if (j === 0) {
                                    data += '<li><a href="#" style="width: 20px; height: 20px;">   </a></li>';
                                }

                                var isSeat = getSeatDetailsPrivate(i, j,result, service_id, 0);
                                if (isSeat) {
                                    data += isSeat;
                                } else {
                                    data += '<li><a href="#" style="width: 20px; height: 20px;">    </a></li>';
                                }

                            }
                            data += "</ul>";
                            data += "</li>";
                        }

                        data += "</ul>";
                        $(".layout_div_close").hide();
                        $("#layout_div_" + service_id).show();
                        $("#bus-seat-layout-" + service_id).html(data);
                        $('#loader').css({"display": "none"});
                    }
                    else {
                        /* setInterval(function() {
                         alert("No fond Boarding and Dropping Stop ");
                         }, 2000); */
                        $('#msg').text("Not found boarding and dropping stops.");
                        $('#isCancelable_popup').modal('show');
                        setTimeout(function() { $('#isCancelable_popup').modal('hide'); }, 2000);
                        //tempAlert("No fond Boarding and Dropping Stop ", 3000);
                        //setTimeout(function(){ alert("No fond Boarding and Dropping Stop "); }, 3000);
                        $current_object.removeClass("bg_gray").addClass("btn-purple").text("Select Seat");
                        return false;
                    }
                },
            });
        });
        /********************end load boarding and dropping stop *************************/
        /*********************seat layout start***********************************************/
        $('body').on('click', '#select_seat_layout', function() {
            var service_id = $(this).data("service-id");
            var provider_id = $(this).data("provider-id");
            var date = $(this).data("date");
            var from = $("#from_stop_" + service_id).val(); // get value from boarding dropdown 
            var to = $("#to_stop_" + service_id).val();        // get value from dropping dropdown
            var trip_id = $(this).data("trip-id");
            var inventory_type = $(this).data("inventory-type");
            var op_name = $(this).data("op-name");
            if((from =='0' || to == '0') && op_name == 'MSRTC' )
            {
                alert('Please, select Boarding and Dropping stop.');
                return false;
            }
            $('.loader-txt').text('Please wait, your bus seatlayout are loading...');
            $('#loader').css({"display": "block"});
            var url = base_url + "admin/Search_bus/get_seat_layout";

            var dataString = 'service_id=' + service_id + '&provider_id=' + provider_id + '&date=' + date + '&from=' + from + '&to=' + to + '&trip_id=' + trip_id + '&inventory_type=' + inventory_type + '&operator_name=' + op_name;

            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: dataString,
                async: false,
                success: function(data) {
                    var listrow = '';
                    seat_fare_detail = data;
                    var result = data.seats;
                    var row = data.total_rows;
                    var col = data.total_cols;
                    var result = data.seats;
                    var total_cols = data.total_cols;
                    var total_rows = data.total_rows;

                    /* listrow += "<ul class='deckrow'>"+
                     +"<li>"+"<ul>" + "<li>" +"<a class='myBusDriveSeatImage driver_booked' href='#' style='width: 20px; height: 20px;'></a></li>";
                     
                     for(var i = 0; i< total_cols; i++) {
                     listrow +=  "<li>" +
                     "<ul>";
                     for(var j = 0; j < total_rows; j++) {
                     var seat = getSeatDetails(i,j, result);
                     if(seat !=null){
                     listrow  += seat;
                     } else {
                     listrow  +='<li>  &nbsp; &nbsp; <li>';
                     }
                     }
                     listrow += "</ul>" + "</li>";
                     
                     }
                     
                     listrow += "</li></ul>";  deckrow_"+service_id+*/
                    var data = '<ul class="deckrow deckrow_' + service_id + '" >';
                    for (var i = 0; i < col; i++) {
                        data += "<li>";
                        data += "<ul>";
                        for (var j = 0; j < row; j++) {

                            if (i === 0 && j === 0) {
                                data += '<li><a class=" myBusDriveSeatImage driver_booked" href="#" style="width: 20px; height: 20px;"></a></li>';
                            } else if (j === 0) {
                                data += '<li><a href="#" style="width: 20px; height: 20px;">   </a></li>';
                            }

                            var isSeat = getSeatDetails(i, j, result, service_id);
                            if (isSeat) {
                                data += isSeat;
                            } else {
                                data += '<li><a href="#" style="width: 20px; height: 20px;">    </a></li>';
                            }

                        }
                        data += "</ul>";
                        data += "</li>";
                    }

                    data += "</ul>";
                    $(".layout_div_close").hide();
                    $("#layout_div_" + service_id).show();
                    $("#bus-seat-layout-" + service_id).html(data);
                    $('#loader').css({"display": "none"});
                },
                error: function(data) {
                    // do something
                }
            });


        });
        /************************ end seat layout start**************************************/


        /********************* on click seat get details*********************/
        /* block seat code*/


        $('body').on('click', ".deckrow .seat", function() {
            var seat_info = seat_fare_detail;
            var curtSeatFare = parseInt($('#get_fare_id').text()); // get total fare value
            var curtTotalFare = parseInt($('#total_fare').text()); // get total fare value
            var data_fare = $(this).attr('data-fare'); // get fare value;
            var total_fare = $(this).attr('data-total-fare'); // get fare value;
            var gender = $(this).attr('data-ladies-seat');
            var sleeper = $(this).attr('data-sleeper');
            var ac = $(this).attr('data-ac');

            var data_seat_no = $(this).attr('data-seat-no'); // get set no
            var birth = $(this).attr('data-berth');
            var current_seat_type = $(this).attr('data-gender');

            var bus_travel_id = $(this).attr('data-bus-travel-id');
            var $par_elem = $('#layout_div_' + bus_travel_id + '');
            var $deck_row = $par_elem.find('.deckrow_' + bus_travel_id + '');
            var class_prefix = (current_seat_type == "F" && current_seat_type != undefined) ? "ladies_" : "";

            for (var s = 0; s < seat_info.seats.length; s++) {
                if (typeof (data_seat_no) != "undefined" && seat_info.seats[s].available == "Y") {
                    if (seat_info.seats[s].seat_no == data_seat_no)
                    {

                        if ($(this).hasClass(class_prefix + 'available'))
                        {
                            $(this).removeClass(class_prefix + 'available');
                            $(this).addClass(class_prefix + 'selected userseatselected');
                        }
                        else if ($(this).hasClass(class_prefix + 'selected'))
                        {
                            $(this).removeClass(class_prefix + 'selected userseatselected');
                            $(this).addClass(class_prefix + 'available');
                        }
                        break
                    }
                }
            }
            var total_sel_seat = $deck_row.find('.seat.userseatselected').length;



            if (total_sel_seat > 6)
            {
                showNotification("danger", "You can not select more than 6 seat.");
                $(this).removeClass(class_prefix + 'selected userseatselected');
                $(this).addClass(class_prefix + 'available');
                return false;
            } else {

                cal_fare(seat_info, data_seat_no, bus_travel_id);
            }

        });

        /*temp booking on click continue*/
        $('body').off('click', ".continue").on('click', ".continue", function(e) {

            var form_url = base_url + "admin/search_bus/save_temp_booking";
            var trip_no = $(this).attr("data-trip-no");
            var bus_travel_id = $(this).attr("data-bus-travel-id");
            var provider_id = $('.article_' + bus_travel_id).attr("data-provider-id");
            var provider_type = $('.article_' + bus_travel_id).attr("data-provider-type");
            var op_name = $('.article_' + bus_travel_id).attr("data-travel");
                  
            if (provider_id != "" && provider_type != "") {
                if (Object.keys(onwardJourny).length == 0) {
                    if (temp_seats != "" && temp_seats != undefined && Object.keys(temp_seats).length) {
                        var from_stop_val = $('#from_stop_' + bus_travel_id).val();
                        var to_stop_val = $('#to_stop_' + bus_travel_id).val();

                        onwardJourny['from'] = $('.article_' + bus_travel_id).attr("data-from");
                        onwardJourny['to'] = $('.article_' + bus_travel_id).attr("data-to");
                        onwardJourny['date'] = $('#datepicker').val();
                        onwardJourny['date_of_jour'] = $('#datepicker').val();
                        
                        onwardJourny['dept_time'] = $('#sch_departure_' + bus_travel_id).text();
                        onwardJourny['alighting_time'] = $('#sch_alighting_' + bus_travel_id).text();
                        
                        if(op_name == 'MSRTC') {
                            onwardJourny['boarding_time'] = $('#from_stop_' + bus_travel_id).find('option:selected').text();
                            onwardJourny['droping_time'] = $('#to_stop_' + bus_travel_id).find('option:selected').text();
                            onwardJourny['boarding_stop_name'] = $('#from_stop_' + bus_travel_id).val();
                            onwardJourny['dropping_stop_name'] = $('#to_stop_' + bus_travel_id).val();
                        } else if(op_name == 'UPSRTC')
                        {
                            onwardJourny['boarding_time'] = $('.article_' + bus_travel_id).attr("data-from");
                            onwardJourny['droping_time'] =  $('.article_' + bus_travel_id).attr("data-to");
                            onwardJourny['boarding_stop_name'] = $('.article_' + bus_travel_id).attr("data-from");
                            onwardJourny['dropping_stop_name'] = $('.article_' + bus_travel_id).attr("data-to");
                        } else
                        {
                            onwardJourny['boarding_time'] = $('.article_' + bus_travel_id).attr("data-from");
                            onwardJourny['droping_time'] =  $('.article_' + bus_travel_id).attr("data-to");
                            onwardJourny['boarding_stop_name'] = $('.article_' + bus_travel_id).attr("data-from");
                            onwardJourny['dropping_stop_name'] = $('.article_' + bus_travel_id).attr("data-to");
                        }
                        
                        onwardJourny['trip_no'] = $('.article_' + bus_travel_id).attr("data-trip-no");
                        onwardJourny['service_id'] = bus_travel_id;
                        onwardJourny['provider_id'] = provider_id;
                        onwardJourny['provider_type'] = provider_type;
                        onwardJourny['inventory_type'] = $('.article_' + bus_travel_id).attr("inventory-type");
                        onwardJourny['data_bus_type'] = $('.article_' + bus_travel_id).attr("data-bus-type");
                        onwardJourny['op_name'] = $('.article_' + bus_travel_id).attr("data-travel");
                        onwardJourny['adult_fare'] = $('.article_' + bus_travel_id).attr("data-fare");
                        onwardJourny['child_fare'] = $('.article_' + bus_travel_id).attr("data-child-fare");
                        onwardJourny['seats'] = temp_seats;
                    }
                    /* start return journy*/
                    temp_seats = ''; //IMP DONT REMOVE. IF NECESSARY CHECK EFFECT AND THEN REMOVE
                    
                    if($.trim($("#datepicker1").val()) == "") {
                        form_url = base_url + "admin/search_bus/save_temp_booking";
                    }
                    else {
                        rejour_obj = {};
                        // rejour_obj.from = $('.article_'+trip_no).attr("data-to");
                        // rejour_obj.to = $('.article_'+trip_no).attr("data-from");

                        //To Skip Return Journey
                        skip_form_url = base_url+'front/booking/temp_booking?'+$.param(onwardJourny);

                        rejour_obj['from_stop'] = $('#to_stop').val();
                        rejour_obj['to_stop'] = $('#from_stop').val();
                        rejour_obj['departure_date'] = $('#datepicker1').val();
                        rejour_obj['is_return_journey'] = true;
                        rejour_obj['triptype'] ="";
                        rejour_obj['provider'] = "";
                        rejour_obj['rajax'] = "rajax";
                         
                        $("#onwardJourney").addClass( "opacity-03 inactive-link" );
                        $("#returnJourney").removeClass("opacity-03 inactive-link");
                            $('loader-txt').text('Please wait, your return bus services are loading...');
                            $('#loader').css({"display": "block"});
                            $("#date_shown").html(moment($('#datepicker1').val()).format('DD-MMM-YYYY'));
                            var url = base_url + "admin/search_bus/search_services";
                            form_url = '';
                            $.ajax({
                                type: "POST",
                                dataType: "json",
                                url: url,
                                data: rejour_obj,
                                async: true,
                                success: function(data) {
                                    
                                    $("body #services_view_render").html(data.results);
                                    $('#loader').css({"display": "none"});
                                    
                                }
                            });
                            e.preventDefault();
                    }
                } else if(Object.keys(returnJourny).length == 0) {
                    if (temp_seats != "" && temp_seats != undefined && Object.keys(temp_seats).length) {
                        var from_stop_val = $('#from_stop_' + bus_travel_id).val();
                        var to_stop_val = $('#to_stop_' + bus_travel_id).val();

                        returnJourny['to'] = $('.article_' + bus_travel_id).attr("data-from");
                        returnJourny['from'] = $('.article_' + bus_travel_id).attr("data-to");
                        returnJourny['date'] = $('#datepicker1').val();
                        returnJourny['date_of_jour'] = $('#datepicker1').val();
                        
                        returnJourny['dept_time'] = $('#sch_departure_' + bus_travel_id).text();
                        returnJourny['alighting_time'] = $('#sch_alighting_' + bus_travel_id).text();
                        
                        if(op_name == 'MSRTC') {
                            returnJourny['boarding_time'] = $('#from_stop_' + bus_travel_id).find('option:selected').text();
                            returnJourny['droping_time'] = $('#to_stop_' + bus_travel_id).find('option:selected').text();
                            returnJourny['boarding_stop_name'] = $('#from_stop_' + bus_travel_id).val();
                            returnJourny['dropping_stop_name'] = $('#to_stop_' + bus_travel_id).val();
                        } else if(op_name == 'UPSRTC')
                        {
                            returnJourny['boarding_time'] = $('.article_' + bus_travel_id).attr("data-from");
                            returnJourny['droping_time'] =  $('.article_' + bus_travel_id).attr("data-to");
                            returnJourny['boarding_stop_name'] = $('.article_' + bus_travel_id).attr("data-from");
                            returnJourny['dropping_stop_name'] = $('.article_' + bus_travel_id).attr("data-to");
                        } else
                        {
                            returnJourny['boarding_time'] = $('.article_' + bus_travel_id).attr("data-from");
                            returnJourny['droping_time'] =  $('.article_' + bus_travel_id).attr("data-to");
                            returnJourny['boarding_stop_name'] = $('.article_' + bus_travel_id).attr("data-from");
                            returnJourny['dropping_stop_name'] = $('.article_' + bus_travel_id).attr("data-to");
                        }
                        
                        returnJourny['trip_no'] = $('.article_' + bus_travel_id).attr("data-trip-no");
                        returnJourny['service_id'] = bus_travel_id;
                        returnJourny['provider_id'] = provider_id;
                        returnJourny['provider_type'] = provider_type;
                        returnJourny['inventory_type'] = $('.article_' + bus_travel_id).attr("inventory-type");
                        returnJourny['data_bus_type'] = $('.article_' + bus_travel_id).attr("data-bus-type");
                        returnJourny['op_name'] = $('.article_' + bus_travel_id).attr("data-travel");
                        returnJourny['adult_fare'] = $('.article_' + bus_travel_id).attr("data-fare");
                        returnJourny['child_fare'] = $('.article_' + bus_travel_id).attr("data-child-fare");
                        returnJourny['seats'] = temp_seats;
                    }
                }
                
            }
            if(form_url == '') {
                return false;
            }
            $.ajax({
                type: "POST",
                dataType: "json",
                url: form_url,
                data: {"onwardJourny": onwardJourny, "returnJourny": returnJourny},
                async: true,
                success: function(data) {

                    window.location.href = data.redirect;

                },
                error: function(data) {
                    // do something
                }
            });
            e.preventDefault();
        });

        /*temp booking*/

        /******** Sort Column Start**********/
        $(document).off('click', '.sort_col').on('click', '.sort_col', function(e) {
            e.preventDefault();

            $(".sort_col").removeClass("active");
            $(this).addClass("active");

            var col = $(this).attr('ref').trim();

            var stype = $(this).attr('data-sort').trim();

            $(this).attr('data-sort', stype == 'asc' ? 'desc' : 'asc');

            $('.sort_list').sort(function(a, b) {

                if (col != "data-travel")
                {
                    var a = parseFloat($(a).attr(col).trim());
                    var b = parseFloat($(b).attr(col).trim());
                }
                else
                {
                    var a = String($(a).attr(col).trim());
                    var b = String($(b).attr(col).trim());
                }


                if (stype == 'asc')
                {
                    return a > b ? 1 : -1;
                }
                else
                {
                    return a > b ? -1 : 1;
                }
            });


            $('.stu_sort_list').sort(function(a, b) {

                if (col != "data-travel")
                {
                    var a = parseFloat($(a).attr(col).trim());
                    var b = parseFloat($(b).attr(col).trim());
                }
                else
                {
                    var a = String($(a).attr(col).trim());
                    var b = String($(b).attr(col).trim());
                }

                if (stype == 'asc')
                {
                    return a > b ? 1 : -1;
                }
                else
                {
                    return a > b ? -1 : 1;
                }
            });
        });
        /******** Sort Column End************/

        /**** Range filter *********/
        $("#range_slider, input[type=range]").on("mouseup", function() {
            var buscount = 0;
            var rangeFare = $(this).val();

            $('.fare_sort_list').each(function(index) {

                if (parseFloat($(this).attr('data-fare')) >= rangeFare) {
                    buscount++;
                    $(this).show();
                    $('#total-result-found').text(buscount);
                    $('#totalBusCnt').text(buscount);
                } else {

                    $(this).hide();
                }

            });

        });

    });

    function getSeatDetails(cols, rows, result, service_id) {
        var seat = null;
        //alert(col+ ' col ----- '); alert(row+ ' row----- ');
        for (var i = 0; i < result.length; i++) {

            if (result[i].row === rows && result[i].col === cols) {
                //seat = result.seats[i];

                if (result[i].available == "Y")
                {
                    if (result[i].ladies_seat == true)
                    {
                        var current_seat_class = "ladies_available";
                    } else {
                        var current_seat_class = "available";
                    }
                }
                else if (result[i].available == "N") {
                    var current_seat_class = "booked";
                }


                var ac_seat = (result[i].ac) ? result[i].ac : "True";
                var ladies_seat = (result[i].ladies_seat) ? result[i].ladies_seat : "false";
                var sleeper = (result[i].sleeper) ? result[i].sleeper : "True";
                var berth = (result[i].berth) ? result[i].berth : "0";
                var total_fare = (result[i].total_fare) ? (result[i].total_fare) : (result[i].sub_total_fare);
                if (ladies_seat == 'false')
                {
                    var gender = 'M';
                }
                else
                {
                    var gender = 'F';
                }
                seat = "<li><a class='seat " + current_seat_class + "' href='javascript:void(0)' style='width: 20px; height: 20px;' data-bus-travel-id=" + service_id + " data-seat-no=" + result[i].seat_no + " data-fare=" + result[i].sub_total_fare + " data-current_seat_type =" + ladies_seat + " data-total-fare=" + total_fare + " title='Seat No " + result[i].seat_no + " | Fare : Rs. " + result[i].total_fare + "' data-ac=" + result[i].ac + " data-sleeper=" + result[i].sleeper + " data-ladies-seat=" + ladies_seat + " data-berth=" + result[i].berth + " data-gender=" + gender + " ></a></li>";
                break;
            }

        }

        return seat;
    }
    
    function getSeatDetailsPrivate(cols, rows, result, service_id, birthTravel) {
        var seat = null;
        
        //alert(col+ ' col ----- '); alert(row+ ' row----- ');
        for (var i = 0; i < result.length; i++) {

            if (result[i].row === rows && result[i].col === cols) {
                //seat = result.seats[i];

                if (result[i].available == "Y")
                {
                    if (result[i].ladies_seat == true)
                    {
                        var current_seat_class = "ladies_available";
                    } else {
                        var current_seat_class = "available";
                    }
                }
                else if (result[i].available == "N") {
                    var current_seat_class = "booked";
                }


                var ac_seat = (result[i].ac) ? result[i].ac : "True";
                var ladies_seat = (result[i].ladies_seat) ? result[i].ladies_seat : "false";
                var sleeper = (result[i].sleeper) ? result[i].sleeper : "True";
                var berth = (result[i].berth) ? result[i].berth : "0";
                var total_fare = (result[i].total_fare) ? (result[i].total_fare) : (result[i].sub_total_fare);
                if (ladies_seat == 'false')
                {
                    var gender = 'M';
                }
                else
                {
                    var gender = 'F';
                }
                seat = "<li><a class='seat " + current_seat_class + "' href='javascript:void(0)' style='width: 20px; height: 20px;' data-bus-travel-id=" + service_id + " data-seat-no=" + result[i].seat_no + " data-fare=" + result[i].sub_total_fare + " data-current_seat_type =" + ladies_seat + " data-total-fare=" + total_fare + " title='Seat No " + result[i].seat_no + " | Fare : Rs. " + result[i].sub_total_fare + "' data-ac=" + result[i].ac + " data-sleeper=" + result[i].sleeper + " data-ladies-seat=" + ladies_seat + " data-berth=" + birthTravel + " data-gender=" + gender + " ></a></li>";
                break;
            }

        }

        return seat;
    }

    function tempAlert(msg, duration)
    {

        var el = document.createElement("div");
        el.setAttribute("style", "position:fixed;top:40%;left:40%;width:500px;height:50px;background-color:#e44a50; text-align:center");
        el.innerHTML = msg;
        setTimeout(function() {
            el.parentNode.removeChild(el);
        }, duration);
        document.body.appendChild(el);
    }


    function showNotification(msg_type, msg)
    {
        msg_type = (msg_type == "error") ? "danger" : msg_type;
        // bootstrapGrowl(msg, msg_type, {"delay":"5000","align":"center","offset":{"from":"top","amount":"250"}}); 
        bootstrapGrowl(msg, msg_type, {"delay": "5000", "align": "right"});
    }

    function bootstrapGrowl(msg, msg_type, msg_config)
    {
        var offset_from = 'top';
        var offset_amount = 10;

        if (typeof (msg_type) == 'undefined')
        {
            msg_type = 'info';
        }

        if (typeof (msg_config) == 'undefined')
        {
            msg_config = {};
        }
        else if (typeof (msg_config.offset) != 'undefined' && msg_config.offset != null)
        {
            offset_from = (msg_config.offset.from != undefined && msg_config.offset.from != "") ? msg_config.offset.from : "top";
            offset_amount = (msg_config.offset.amount != undefined && msg_config.offset.amount != "") ? msg_config.offset.amount : 10;
        }

        var config = {
            ele: 'body', // which element to append to
            type: msg_type, // (null, 'info', 'danger', 'success')
            offset: {from: offset_from, amount: offset_amount}, // 'top', or 'bottom'
            align: 'right', // ('left', 'right', or 'center')
            width: 350, // (integer, or 'auto')
            delay: 3000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
            allow_dismiss: true, // If true then will display a cross to close the popup.
            stackup_spacing: 10 // spacing between consecutively stacked growls.                
        };

        var fConfig = $.extend({}, config, msg_config);

        $.bootstrapGrowl(msg, fConfig);
    }


    function cal_fare(seat_info, seat_no, bus_travel_id) {
        var new_seat_info = {};

        jQuery.each(seat_info.seats, function(i, val) {
            var k = val.seat_no;
            new_seat_info[k] = val;
        });

        var fares = 0;
        var actual_total_fares = 0;
        var total_discount = 0;
        var basic_fares = 0;
        var service_tax = 0;
        var op_service_charge = 0;
        var $par_elem = $('#layout_div_' + bus_travel_id);
        var $deck_row = $par_elem.find('.deckrow_' + bus_travel_id);
        var total_sel_seat = $deck_row.find('.seat.userseatselected').length;
        var tseat = {};
        var showSeat = {};
        var data_seat_info = {};
        var birth_count = $(".deckrow_" + bus_travel_id).length;
        /* var op_id = $('.article_'+bus_travel_id+'').attr('data-op-id');
         var bus_type_id = $('.article_'+bus_travel_id+'').attr('data-bus-type'); */

        $par_elem.find('.sel_seat').html("");

        $(".deckrow_" + bus_travel_id).each(function(bind)
        {
            var tbirth = $(this).find('.seat.userseatselected').attr('data-berth');

            if ($(this).find('.seat.userseatselected').length > 0)
            {
                tseat[tbirth] = new Array();
                showSeat[tbirth] = new Array();
                //tseat['birth'] = tbirth;
                var i = 0;
                $(this).find('.seat.userseatselected').each(function()
                {
                    if ($(this).hasClass('userseatselected'))
                    {
                        var data_seat_no = $(this).attr('data-seat-no');
                        data_seat_info['data_seat_no'] = $(this).attr('data-seat-no');
                        data_seat_info['seat_fare'] = parseFloat(new_seat_info[data_seat_no].sub_total_fare);
                        data_seat_info['total_fare'] = parseFloat(new_seat_info[data_seat_no].sub_total_basic_fare);
                        data_seat_info['is_ladies'] = $(this).attr('data-ladies-seat');
                        data_seat_info['is_sleeper'] = $(this).attr('data-sleeper');
                        data_seat_info['is_ac'] = $(this).attr('data-ac');
                        data_seat_info['seat_birth'] = tbirth;
                        // var data_berth_no = $(this).attr('data-berth');
                        //for(var s = 0; s < seat_info.seats.length; s++) {
                        // if(seat_info.seats[s].seat_no == data_seat_no)
                        // {
                        fares += parseFloat(new_seat_info[data_seat_no].sub_total_fare);
                        actual_total_fares += parseFloat(new_seat_info[data_seat_no].sub_total_basic_fare);
                        basic_fares += parseFloat(new_seat_info[data_seat_no].sub_total_basic_fare);
                        $par_elem.find('.fare_value .fare').html(basic_fares.toFixed(2));
                        service_tax += parseFloat(new_seat_info[data_seat_no].service_tax_amount);
                        //op_service_charge += (parseFloat(seat_info.seats[0].convey_charge_percent) != null && parseFloat(seat_info.seats[0].service_tax_amount) != undefined) ? parseFloat(seat_info.seats[0].convey_charge_percent)) : 0;
                        //total_discount += (seat_info[bus_travel_id][tbirth][data_seat_no]["seat_discount"] != null && seat_info[bus_travel_id][tbirth][data_seat_no]["seat_discount"] != undefined) ? parseFloat(seat_info[bus_travel_id][tbirth][data_seat_no]["seat_discount"]) : 0;

                        // }
                        // }
                        showSeat[tbirth].push(data_seat_no);
                        tseat[tbirth].push(data_seat_info);
                        i++;
                        data_seat_info = {};
                    }
                });

                showSeat[tbirth].sort(function(a, b) {
                    return a - b
                });

                if (birth_count > 1)
                {
                    var berth_name = (tbirth == 0) ? " Lower" : (tbirth == 2) ? " Upper" : "";
                    $par_elem.find('.sel_seat').append(berth_name + " : " + showSeat[tbirth].join(', '));
                }
                else
                {
                    $par_elem.find('.sel_seat').append(showSeat[tbirth].join(', '));
                }

                $par_elem.find('.fare').html(actual_total_fares.toFixed(2));
                $par_elem.find('.tot_fare').html(actual_total_fares.toFixed(2));
                $par_elem.find('.total_amount').html(fares.toFixed(2));
                $par_elem.find('.fare_service_tax').html(service_tax.toFixed(2));
                $par_elem.find('.fare_service_charge').html(op_service_charge.toFixed(2));
            } else {
                $par_elem.find('.fare').html('0.00');
                $par_elem.find('.tot_fare').html('0.00');
                $par_elem.find('.total_amount').html('0.00');
                $par_elem.find('.fare_service_tax').html('0.00');
                $par_elem.find('.fare_service_charge').html('0.00');
            }
            temp_seats = tseat;
        });
    }
    
    function get_bus_services(rejour_obj)
    {
            var journey_date = $('#datepicker').val();
            var return_date = $('#datepicker1').val();

            $('#loader').css({"display": "block"});
            $("#date_shown").html(moment(journey_date).format('DD-MMM-YYYY'));
            var url = base_url + "admin/search_bus/search_services";
            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: rejour_obj,
                async: false,
                success: function(data) {

                    $("body #services_view_render").html(data.results);
                    $('#loader').css({"display": "none"});
                }
            });
            
    }

</script>
