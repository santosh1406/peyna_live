<!DOCTYPE html>
<html>
    <head>
        <title>Tickets</title>
    </head>
    <body>
        <table style="font-size:13px;font-family: \'Arial\',sans-serif;     table-layout: fixed; line-height: 20px;color:#000;border-collapse: collapse;" align="center" width="800px" bordercolor="#cccccc" border="1px" cellpadding="2" cellspacing="2">
            <tr>
                <td align="left" valign="top">
                    <table width="100%" border="0">
                        <tr>
                            <td align="right" valign="top">
                                <img src="https://www.bookonspot.com/assets/travelo/front/images/ticket-logo/msrtc-logo.jpg" />
                            </td>
                            <td align="center" valign="top" style="vertical-align:middle; font-size:15px; font-weight:bold; line-height:24px;"> 
                                Maharashtra State Road Transport Corporation <br/> E-Reservation Ticket
                            </td>
                            <td align="left" valign="top" style="vertical-align:middle;">
                                <img src="https://www.bookonspot.com/assets/travelo/front/images/ticket-logo/bookonspot_logo.png" width="165" height="35" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr valign="middle" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 20px;">
                <td valign="middle"><span style="color:#ff940a;font-weight:bold; font-size:13px;">Journey:</span> </td>
                <td valign="middle"><?php echo isset($onwardJourny['onwardJourny']['boarding_stop_name']) ? $onwardJourny['onwardJourny']['boarding_stop_name'] . ' to ' . $onwardJourny['onwardJourny']['dropping_stop_name'] : ''; ?></td>
                <td valign="middle"><span style="color:#ff940a;font-weight:bold; font-size:13px;">Date of Journey: </span> </td>
                <td> <?php echo isset($onwardJourny['onwardJourny']['dept_time']) ? date('d-m-Y', strtotime($onwardJourny['onwardJourny']['dept_time'])) : ''; ?> </td>
            </tr>
            <tr valign="top">
                <td  style="padding:7px 0;" colspan="4" bgcolor="#efefef"><strong>Trip Information:</strong> </td>
            </tr>
            <tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
                <td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Travels:</span> </td>
                <td><?php echo isset($onwardJourny['onwardJourny']['op_name']) ? $onwardJourny['onwardJourny']['op_name'] : ''; ?></td>
                <td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Bus Type:</span> </td>
                <td><?php echo isset($onwardJourny['onwardJourny']['data_bus_type']) ? $onwardJourny['onwardJourny']['data_bus_type'] : ''; ?></td>
            </tr>
            <tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height:25px;">
                <td><span style="color:#ff940a;font-weight:bold; font-size:13px;">PNR No.:</span> </td>
                <td><?php echo isset($final_data['pnr_no']) ? $final_data['pnr_no'] : ''; ?></td>
                <td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Total Seat(s):</span> </td>
                <td><?php echo count($passengerDetails); ?></td>
            </tr>
            <tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
                <td><span style="color:#ff940a;font-weight:bold; font-size:13px;">BoS Ref No.:</span> </td>
                <td><?php echo isset($bosKey) ? $bosKey : ''; ?></td>
                <td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Total Fare:</span> </td>
                <td><?php echo isset($ticket_data['total_fare_without_discount']) ? $ticket_data['total_fare_without_discount'] : ''; ?></td>
            </tr>
            <tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
                <td style="padding:7px 0;" colspan="4" bgcolor="#efefef"><strong>Boarding Information:</strong> </td>
            </tr>
            <tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
                <td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Boarding Point:</span> </td>
                <td><?php echo isset($onwardJourny['onwardJourny']['boarding_stop_name']) ? $onwardJourny['onwardJourny']['boarding_stop_name'] : ''; ?></td>
                <td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Dropping Point:</span> </td>
                <td><?php echo isset($onwardJourny['onwardJourny']['dropping_stop_name']) ? $onwardJourny['onwardJourny']['dropping_stop_name'] : ''; ?></td>
            </tr>
            <tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
                <td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Departure Time:</span> </td>
                <td><?php echo isset($onwardJourny['boarding_point']) ? $onwardJourny['boarding_point'] : ''; ?></td>
                <td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Alighting Time:</span> </td>
                <td><?php echo isset($onwardJourny['dropping_point']) ? $onwardJourny['dropping_point'] : ''; ?></td>
            </tr>



            <tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">

                <td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Boarding Point Contact Number:</span> </td>
                <td></td>

                <td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Boarding Point Address:</span> </td>
                <td></td>

            </tr>

            <tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">

                <td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Boarding Point Landmark:</span> </td>
                <td></td>


            </tr>



            <tr valign="top">
                <td style="padding:7px 0;" colspan="4" bgcolor="#efefef"><strong>Passenger Information:</strong> </td>
            </tr>
            <tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 40px">
                <td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Email Id:</span> </td>
                <td><?php echo isset($passengerDetails[0]['email']) ? $passengerDetails[0]['email'] : ''; ?></td>
                <td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Contact No.:</span> </td>
                <td><?php echo isset($passengerDetails[0]['mobile']) ? $passengerDetails[0]['mobile'] : ''; ?></td>
            </tr>
            <tr valign="top"></tr>
            <tr valign="top">
                <td colspan="4"><table class="respond" style="font-size:14px;font-family: \'Arial\',sans-serif; color:#000;" align="center" width="100%" bordercolor="#cccccc" border="1px" cellpadding="0" cellspacing="0">
                        <tr >
                            <th style="padding:7px;text-align:center;" > <span style="color:#000;font-weight:bold; font-size:13px;">Passenger Name</span> </th>
                            <th style="padding:7px 0;text-align:center;"> <span style="color:#000;font-weight:bold; font-size:13px;">Age</span> </th>
                            <th style="padding:7px 0;text-align:center;"> <span style="color:#000;font-weight:bold; font-size:13px;">Gender</span> </th>
                            <th style="padding:7px 0;text-align:center;"> <span style="color:#000;font-weight:bold; font-size:13px;">Seat No.</span> </th>
                        </tr>
                        <?php if (!empty($passengerDetails)) : foreach ($passengerDetails as $value) : ?>
                                <tr >
                                    <th style="padding:7px;text-align:center;" > <span style="color:#000;font-weight:bold; font-size:13px;"><?php echo isset($value['name']) ? $value['name'] : ''; ?></span> </th>
                                    <th style="padding:7px 0;text-align:center;"> <span style="color:#000;font-weight:bold; font-size:13px;"><?php echo isset($value['age']) ? $value['age'] : ''; ?></span> </th>
                                    <th style="padding:7px 0;text-align:center;"> <span style="color:#000;font-weight:bold; font-size:13px;"><?php echo isset($value['sex']) ? $value['sex'] : ''; ?></span> </th>
                                    <th style="padding:7px 0;text-align:center;"> <span style="color:#000;font-weight:bold; font-size:13px;"><?php echo isset($value['seat_no']) ? $value['seat_no'] : ''; ?></span> </th>
                                </tr>
                            <?php endforeach;
                        endif; ?>
                    </table>
                </td>
            </tr>
        </table>
        <table class="respond" style="font-size:13px;font-family: \'Arial\',sans-serif; text-align:justify; line-height: 20px;color:#000;border-collapse: collapse;" align="center" width="800px" bordercolor="#cccccc" border="0px" cellpadding="0" cellspacing="0">
            <tr>
                <td><strong></br>Terms & Conditions: </strong> </td>
            </tr>
            <tr>
                <td> Bookonspot (BoS) acts as an “Intermediary” solely to assist customers in gathering travel information, determining the availability of travel related products and services, making legitimate reservations or otherwise transacting business with travel operators, and for facilitating travel requirements. You acknowledge that BoS merely provides intermediary services in order to facilitate these services. </td>
            </tr>
            <tr>
                <td><strong>Communication Policy :</strong> </td>
            </tr>
            <tr>
                <td>Upon transacting on the BoS website, you will receive an e-mail from BoS informing the status of your transaction on your registered email ID or the email ID provided at the time of booking. You will also receive a SMS on your registered mobile number provided at the time of booking. The responsibility of entering your accurate contact details like your name, email ID and mobile number is solely yours. BoS is not responsible for providing any information to you regarding any change in bus schedules, cancellation of service, status of bus service, etc.</td>
            </tr>
            <tr>
                <td><strong>Refunds:</strong> </td>
            </tr>
            <tr>
                <td> In case of cancellation, due refund amount will be paid according to the bus operator\'s cancellation policy. After scheduled departure of bus service cancellation/ Refund request cannot be accepted. Refund process may take duration of 7-8 working days for online payments/net banking etc. In case of service cancellation from operator, you will receive full refund except the payment gateway charges and any other non refundable charges. Basic fare is refundable as per cancellation policy and it varies from operator to operator. Service tax/Reservation charges or any other operational charges are non refundable. </td>
            </tr>
            <tr>
                <td><strong>Grievance Policy:</strong> </td>
            </tr>
            <tr>
                <td> You can file a grievance/share feedback if you are disappointed with the services rendered by BoS or any other issues. You can give your grievance/feedback through email by emailing us at <a href="mailto:support@bookonspot.com">support@bookonspot.com</a> or by calling us on <strong>022-66813930, +917710805805</strong> </td>
            </tr>
            <tr>
                <td><strong>Other Points:</strong> </td>
            </tr>
            <tr>
                <td> BoS shall not be responsible or held liable for the behavior of operational staff in bus. BoS shall not be responsible or held liable if the bus does not stop at a pick up or drop point. BoS shall not be responsible or held liable if passenger reaches boarding point after the Scheduled departure time. If there are any on board charges from bus operator, then BoS is not responsible for it. BoS shall not be responsible or held liable for any bus delay due to breakdown or operational issues/road blockage/traffic jam/agitation etc. BoS shall not be held liable for accident claims. BoS shall not be responsible or held liable for criminal/civil offense by the operator.</td>
            </tr>
            <tr>
                <td><strong>Cancellation Process:</strong> </td>
            </tr>
            <tr>
                <td> You can cancel your ticket online by logging in to <a href ="https://bookonspot.com/home" target="blank"> www.bookonspot.com</a> Cancellation charges of the tickets may vary as per the policies of the operators. Cancellations are allowed till scheduled departure of service and as per policy of operator.</td>
            </tr>
            <tr>
                <td><strong>Cancellation policy:</strong> </td>
            </tr>                                                   
            <tr>                                    
            <table class="respond" style="font-size:13px;font-family: \'Arial\,sans-serif;    line-height: 20px;color:#000;border-collapse: collapse;" align="center" width="800px" bordercolor="#cccccc" border="0px" cellpadding="10" cellspacing="10">                   
                <tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;">
                    <td style="padding:3px;">Cancellation Time (Befor Departure)</td>
                    <td style="padding:3px;">Charges</td>
                </tr>

                <tr>
                    <td> For all queries call us on <strong>022-66813930, +917710805805 </strong> or email us at <a href="mailto:support@bookonspot.com">support@bookonspot.com</a> </td>
                </tr>
            </table>
        </tr>
    </table>
</body>
</html>

