<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> 
                        <h3 class="box-title">Mobile Recharge</h3>
                    </div>
                    <div class="box-body">
                        <?php
                        // show($this->session->userdata('message'));
                        echo validation_errors('<div class="error">', '</div>');
                        if ($this->session->flashdata('message')) {
                            ?>
                            <div class="alert alert-success alert-dismissable" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('message');

                                 ?>
                            </div>

                            <?php
                            // $this->session->unset_userdata('message');
                        }

                        $attributes = array("method" => "POST", "id" => "mobile_recharge", "name" => "mobile_recharge");
                        echo form_open(base_url() . 'admin/Tso/recharge', $attributes);
                        ?>
                        <input type="hidden" name="recharge_from" id="recharge_from" value="web"/>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">       
                            <label class="col-lg-3 control-label">Select Type *:</label>
                            <div class="col-lg-6">
                                <input type="radio" name="plan_type" value="Prepaid Mobile" class="form-control plan_type" checked/> Prepaid
                                <input type="radio" name="plan_type" value="Postpaid Mobile" class="form-control plan_type"/> Postpaid
                            </div>
                        </div>
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label">Select Operator *:</label>
                                <div class="col-lg-6 tso_pre">
                                    <input type="hidden" name="service" value="<?php echo $type; ?>">
                                    <select id="pre_operator" name="pre_operator" class="form-control select2">
                                        <option value="">Please select mobile operator</option>
                                        <?php foreach ($pre_operators as $key => $value) { ?>
                                            <option value="<?php echo $key ?>"><?php echo $value ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-lg-6 tso_post" style="display:none">
                                    <input type="hidden" name="service" value="<?php echo $type; ?>">
                                    <select id="post_operator" name="post_operator" class="form-control select2">
                                        <option value="">Please select mobile operator</option>
                                        <?php foreach ($post_operators as $key => $value) { ?>
                                            <option value="<?php echo $key ?>"><?php echo $value ?></option>
                                        <?php } ?>
                                    </select>

                                </div>
                        </div> 
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label">Mobile Number *:</label>
                            <div class="col-lg-6">
                                <input type="text" id="mobile_number" name="mobile_number" class="form-control" placeholder="Mobile Number" maxlength="10"/>
                            </div>
                        </div>
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">       
                            <label class="col-lg-3 control-label">Recharge Type :</label>
                                <div class="col-lg-6">
                                    <select id="recharge_type" name="recharge_type" class="form-control select2">
                                        <option value="0">Topup</option>
                                        <option value="1">Scheme</option>
                                    </select>
                                </div>
                        </div>
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label">Recharge Amount *:</label>
                            <div class="col-lg-6">
                                <input type="text" id="recharge_amount" name="recharge_amount" class="form-control" placeholder="Recharge Amount"/>
                            </div>
                            <!--<div class="col-lg-2">
                                <a id="choose_plans" class="btn btn-default">Choose from plans</a>
                            </div>-->
                        </div>
                        
                        
                        
                        <!-- <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label">Comments:</label>
                            <div class="col-lg-6">
                                <input type="text" id="comments" name="comments" class="form-control" placeholder="Comments"/>
                            </div>
                        </div> -->
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">
                                <button class="btn btn-primary" id="submit_recharge" name="submit_recharge" type="submit">Recharge</button>                                
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<script type="text/javascript">
    var table;
    $(document).ready(function() {
        
        $('.plan_type').on('ifChanged', function(event){
            if($("input[type='radio'].plan_type").is(':checked')) {
                var card_type = $("input[type='radio'].plan_type:checked").val();
                if(card_type == 'Prepaid Mobile'){
                    $('.tso_pre').css('display', 'block');
                    $('.tso_post').css('display', 'none');
                    $('#choose_plans').attr('disabled', 'false');

                }else{
                    $('.tso_pre').css('display', 'none');
                    $('.tso_post').css('display', 'block');
                    $('#choose_plans').attr('disabled', 'true');
                }
            }
        });

        
    });
    

    $('#mobile_recharge').bootstrapValidator({
        group: '.form-control',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            plan_type: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b>Please select type.</font>'
                    }
                }
            },
            mobile_operator: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b>Please enter operator.</font>'
                    }
                }
            },
            mobile_number: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b> Please enter mobile number.</font>'
                    },
                    regexp: {
                        regexp: /^[789]\d{9}$/,
                        message: '<b><font style="color:red; size="1"></b>Enter valid mobile number</font>'
                    }
                }
            },
            recharge_amount: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b> Please enter recharge amount.</font>'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '<b><font style="color:red; size="1"></b>Amount can only consist of number</font>'
                    }
                }
            }
        }
    });

    
</script>