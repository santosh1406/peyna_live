<style type="text/css">
    .help-block{
        color: red;
    }
</style>
<!-- Created by harshada kulkarni on 24-07-2018 -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> 
                        <h3 class="box-title">Mobile Recharge</h3>
                    </div>
                    <div class="box-body">
                        <?php
                        // show($this->session);
                        echo validation_errors('<div class="error">', '</div>');
                        if ($this->session->tempdata('message')) {
                            ?>
                            <div class="alert alert-success alert-dismissable" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->tempdata('message'); ?>
                            </div>
                            <?php
                        }

                        $attributes = array("method" => "POST", "id" => "mobile_recharge", "name" => "mobile_recharge");
                        echo form_open(base_url() . 'admin/Utilities/recharge', $attributes);
                        ?>
                        <input type="hidden" name="recharge_from" id="recharge_from" value="web"/>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">       
                            <label class="col-lg-3 control-label">Select Type *:</label>
                            <div class="col-lg-6">
                                <input type="radio" name="recharge_type" value="Prepaid Mobile" class="form-control recharge_type" checked/> Prepaid
                                <input type="radio" name="recharge_type" value="Postpaid Mobile" class="form-control recharge_type"/> Postpaid
                                <!-- <input type="radio" name="recharge_type" value="Postpaid Mobile" class="form-control recharge_type"/> Postpaid -->
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label">Mobile Number *:</label>
                            <div class="col-lg-6">
                                <input type="number" id="mobile_number" name="mobile_number" class="form-control" placeholder="Mobile Number" min="1" maxlength="10"/>
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label">Select Operator *:</label>
                            
                            
                                <!-- <div class="col-lg-6 non_jri_input">  
                                    <input type="text" id="mobile_operator1" name="mobile_operator" class="form-control" placeholder="Mobile Operator"/>
                                </div> -->
                                <div class="col-lg-6 jri_input">
                                    <input type="hidden" name="aggregator" id="aggregator" value="jri"/>
                                    <input type="hidden" name="type" value="<?php echo $type; ?>">
                                    <select id="mobile_operator" name="mobile_operator" class="form-control select2">
                                        <option value="">Please select mobile operator</option>
                                        <?php foreach ($operators as $key => $value) { ?>
                                            <option value="<?php echo $key ?>"><?php echo $value ?></option>
                                        <?php } ?>
                                    </select>
                                    <span id="mobile_operator_error" style="color: red; display: none">Please select Operator</span>

                                </div>
                        </div> 


                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label">Location :</label>
                            <div class="col-lg-6">
                                <select id="location" name="location" class="form-control select2">
                                    <option value="">Please select location</option>
                                    <?php foreach ($locations as $key => $value) { ?>
                                        <option value="<?php echo $key ?>"><?php echo $value ?></option>
                                    <?php } ?>
                                </select>
                                <span id="location_error" style="color: red; display: none">Please select Location</span>
                            </div>
                        </div>

                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label">Recharge Amount *:</label>
                            <div class="col-lg-4">
                                <input type="text" id="recharge_amount" name="recharge_amount" class="form-control" placeholder="Recharge Amount" />
                            </div>
                            <div class="col-lg-2">
                                <a id="choose_plans" class="btn btn-default">Choose from plans</a>
                            </div>
                        </div>

                        <!-- <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label">Comments:</label>
                            <div class="col-lg-6">
                                <input type="text" id="comments" name="comments" class="form-control" placeholder="Comments" maxlength="150" />
                            </div>
                        </div> -->
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">
                                <button class="btn btn-primary" id="submit_recharge" name="submit_recharge" type="submit">Recharge</button>                                
                                <button class="btn btn-primary disabled" style="display: none" id="loading">Please wait..</button>                                
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" style="width: 100%">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Choose Plans</h4>
      </div>
      <div class="modal-body">
            <table id="choose_plans_table" class="table table-striped table-bordered table-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>Description</th>
                        <th>Talktime</th>
                        <th>Validity</th>
                        <th>Amount</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<script type="text/javascript">
    var table;
    $(document).ready(function() {
        $("#recharge_amount, #mobile_number").ForceNumericOnly();

        table = $('#choose_plans_table').DataTable({
            "destroy": true,
            "paging":   false,
            responsive: true,
            "columns": [
             {"data" : "PlanName"},
             {"data" : "Description"},
             {"data" : "Talktime"},              
             {"data" : "Validity"},              
             {"data" : "Amount"}              
            ]
        });

        $('#choose_plans_table tbody').on( 'click', '.select_plan', function () {
            var data = $(this).parents('tr').find('td:nth-child(5)')[0].innerHTML;
            $('#recharge_amount').val(data);
            $('#myModal').modal('hide');
        } );

        var rechargeType;
        $('#mobile_operator').on('click', function() {
            rechargeType = $('input[name="recharge_type"]:checked', '#mobile_recharge').val();
        });

        $("#mobile_operator").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: base_url + "admin/Utilities/mobileOperators",
                    dataType: "json",
                    data: {
                        term: request.term,
                        type: rechargeType
                    },
                    success: function(data) {
                        response($.map(data, function(item) {
                            return {
                                label: item.label,
                                value: item.value,
                                name: item.name,
                                id: item.id,
                            }
                        }));
                    }
                });
            },
            autoFocus: true,
            minLength: 1,
            select: function(event, ui) {
                $("#mobile_operator").val(ui.item.name);
                if (ui.item.name == "No result found") {
                    $("#mobile_operator").val("");
                }
                return false;
            }
        });


        $('#mobile_operator').select2();
        $('#location').select2();


        $('.recharge_type').on('ifChanged', function(event){
            if($("input[type='radio'].recharge_type").is(':checked')) {
                var card_type = $("input[type='radio'].recharge_type:checked").val();
                if(card_type == 'Prepaid Mobile'){
                    $('#choose_plans').attr('disabled', false);
                }else{
                    $('#choose_plans').attr('disabled', 'true');
                }
            }
        });

        

    });

    // if($("input[type='radio'].recharge_type").is(':checked')) {
    //     var card_type = $("input[type='radio'].recharge_type:checked").val();
    //     if(card_type == 'Prepaid Mobile'){
    //         $('.jri_input').css('display', 'block');
    //         $('.non_jri_input').css('display', 'none');
    //                 $('#choose_plans').attr('disabled', 'false');

    //     }else{
    //         $('.jri_input').css('display', 'none');
    //         $('.non_jri_input').css('display', 'block');
    //                 $('#choose_plans').attr('disabled', 'true');

    //     }
    // }

    $('#mobile_recharge').bootstrapValidator({
        group: '.form-control',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            recharge_type: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b>Please select type.</font>'
                    }
                }
            },
            mobile_operator: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b>Please select Operator.</font>'
                    }
                }
            },
            mobile_number: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b> Please enter mobile number.</font>'
                    },
                    regexp: {
                        regexp: /^[789]\d{9}$/,
                        message: '<b><font style="color:red; size="1"></b>Enter valid mobile number</font>'
                    }
                }
            },
            recharge_amount: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b> Please enter recharge amount.</font>'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '<b><font style="color:red; size="1"></b>Amount can only consist of number</font>'
                    }
                }
            }
        }
    });

    $('#choose_plans').click(function(e){
        e.preventDefault();
        var location = $('#location').val();
        var mobile_operator = $('#mobile_operator').val();
        var error = 0;
        if(mobile_operator == ''){
            // $('#location').select2('open');
            $('#mobile_operator_error').css('display', 'block');
            error = 1;
        }
        if(location == ''){
            $('#location_error').css('display', 'block');
            error = 1;
        }
        if(error == 1){
            return false;
        }else{

            $('#choose_plans_table').DataTable({
                "ajax": {
                    "url": base_url + "admin/utilities/getDataPlans",
                    "type": "POST",
                    "data": {
                        location: $('#location').val(),
                        service_type: 'M',
                        provider_name: $('#mobile_operator').val(),
                    },
                    "dataType": "json"
                },
                "paging":   false,
                "searching": false,
                "destroy": true,
                "columnDefs": [ {
                    "targets": 5,
                    "data": null,
                    "defaultContent": "<button class='select_plan'>Select</button>"
                } ],
                "columns": [
                 {"data" : "PlanName"},
                 {"data" : "Description"},
                 {"data" : "Talktime"},              
                 {"data" : "Validity"},              
                 {"data" : "Amount"}              
                ]
            });

            $('#myModal').modal('show');
            
        }
        
    });

    $(document).on('change', '#mobile_operator', function(){
        if($(this).val() != ''){
            $('#mobile_operator_error').css('display', 'none');   
        }
    });

    $(document).on('change', '#location', function(){
        if($(this).val() != ''){
            $('#location_error').css('display', 'none');   
        }
    });

    // Numeric only control handler
    $.fn.ForceNumericOnly =
    function()
    {
        return this.each(function()
        {
            $(this).keydown(function(e)
            {
                var key = e.charCode || e.keyCode || 0;
                // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
                // home, end, period, and numpad decimal
                return (
                    key == 8 || 
                    key == 9 ||
                    key == 13 ||
                    key == 46 ||
                    key == 110 ||
                    key == 190 ||
                    (key >= 35 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105));
            });
        });
    };

    $('#mobile_number').keyup(function(e){
        e.preventDefault();
        var mob_no = $(this).val();
        if(mob_no.length == 10){
            $('#submit_recharge').css('display', 'none');
            $('#loading').css('display', 'block');
            checkMNP(mob_no);
            
        }

    });

    function checkMNP(mob_no){
        var mobile_number = mob_no;

        $.ajax({
            url: base_url + "admin/utilities/checkMNPJRI",
            type: "post",
            dataType: "json",
            data: {
                MobileNo: mobile_number
            },
            success: function(data) {
                // console.log(data.CurrentOperator);
                $('#mobile_operator').val(data.CurrentOperator).trigger('change');
                // console.log($("#location option:contains('Mumbai')").val());
                $('#location').val($("#location option:contains('" + data.CurrentLocation + "')").val()).trigger('change')
                // $('#mobile_operator').select2('val', data.CurrentOperator).trigger('change');
                $('#submit_recharge').css('display', 'block');
                $('#loading').css('display', 'none');
            }
        });
    }
</script>