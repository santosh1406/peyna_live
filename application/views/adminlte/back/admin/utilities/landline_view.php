<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> 
                        <h3 class="box-title">Pay Your Landline Bill</h3>
                    </div>
                    <div class="box-body">
                        <?php
                        echo validation_errors('<div class="error" id="error_div">', '</div>');
                        if($this->session->flashdata('message')){ 
                            echo '<div class="error" id="error_div"><b>' . $this->session->flashdata('message') . '</b></div>';
                        }
                        ?>
                        <div class="error" id="error_div"></div>
                        <?php
                            $attributes = array("method" => "POST", "id" => "bbps_form", "name" => "bbps_form");
                            echo form_open(base_url() . 'admin/Utilities/pay_bill', $attributes);
                            ?>
                            <input type="hidden" name="recharge_from" id="recharge_from" value="web"/>
                            <input type="hidden" name="type" id="type" value="landline"/>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label">Select operator *:</label>
                                <div class="col-lg-6">
                                    <input type="text" id="operator" name="operator" class="form-control" placeholder="Landline Operator" onchange="removeBillDetails();" value="<?php echo isset($data['operator']) ? $data['operator'] : ''; ?>" />
                                    <input type="hidden" id="operator_id" name="operator_id" class="" value="<?php echo isset($data['operator_id']) ? $data['operator_id'] : ''; ?>" />
                                </div>
                            </div> 
                            <div id="authenticator_div" class="hide">
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label">Service Type *:</label>
                                <div class="col-lg-6">
                                    <select class="form-control" id="authenticator" name="authenticator" onchange="removeBillDetails();">
                                        <option value="LLI" <?php if(isset($data['authenticator']) && $data['authenticator'] == 'LLI') { 'selected'; }?> > Landline Individual</option>
                                        <option value="LLC" <?php if(isset($data['authenticator']) && $data['authenticator'] == 'LLC') { 'selected'; }?>> Landline Corporate</option>
                                    </select>
                                </div>
                            </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label">Account Number *:</label>
                                <div class="col-lg-6">
                                    <input type="text" id="account_number" name="account_number" class="form-control" placeholder="Account Number" maxlength="10"  onchange="removeBillDetails();" value="<?php echo isset($data['account_number']) ? $data['account_number'] : ''; ?>"/>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" id="phone_label">Number Without STD Code *:</label>
                                <div class="col-lg-6">
                                    <input type="text" id="number" name="number" class="form-control" placeholder="Phone Number" maxlength="10"  onchange="removeBillDetails();" value="<?php echo isset($data['number']) ? $data['number'] : ''; ?>"/>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label">Comments:</label>
                                <div class="col-lg-6">
                                    <input type="text" id="comments" name="comments" class="form-control" placeholder="Comments" value="<?php echo isset($data['comments']) ? $data['comments'] : ''; ?>"/>
                                </div>
                            </div>
                            <div id="bill_details" class="hide">
                            <div class="clearfix" style="height: 10px; clear: both;"></div>
                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-6">
                                    <label class="control-label">Bill Number: <span id="bill_number_txt"><?php echo isset($data['bill_number']) ? $data['bill_number'] : ''; ?></span> </label>
                                </div>
                            </div>
                            <div class="clearfix" style="clear: both;"></div>
                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-6">
                                    <label class="control-label">Bill Date:<span id="bill_date"> <?php echo isset($data['bill_date']) ? $data['bill_date'] : ''; ?></span></label>
                                </div>
                            </div>
                            <div class="clearfix" style="clear: both;"></div>
                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-6">
                                    <label class="control-label">Bill Due Date: <span  id="bill_due_date"><?php echo isset($data['bill_due_date']) ? $data['bill_due_date'] : ''; ?></span></label>
                                </div>
                            </div>
                            <div class="clearfix" style="clear: both;"></div>
                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-6">
                                    <label class="control-label">Amount: <span id="amount"> <?php echo isset($data['amount']) ? $data['amount'] : ''; ?></span></label>
                                </div>
                            </div>
                            <div class="clearfix" style="clear: both;"></div>
                            <div class="form-group">
                                <div class="col-lg-offset-3 col-lg-6">
                                    <label class="control-label">Customer Name: <span  id="cust_name"> <?php echo isset($data['customer_name']) ? $data['customer_name'] : ''; ?></span></label>
                                </div>
                            </div>
                            <input type="hidden" id="bill_amt" name="bill_amt" value="">
                            <input type="hidden" id="bill_number" name="bill_number" value="">
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <div class="col-lg-offset-4">
                                    <button class="btn btn-primary" id="submit_bbps" name="submit_bbps" type="button" onclick="submitBBPSForm('#bbps_form');">Proceed</button>                                
                                    <button class="btn btn-primary hide" id="f_submit_bbps" name="f_submit_bbps" type="submit" >Proceed</button>                                
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/AdminLTE/bbps.js" type="text/javascript"></script>