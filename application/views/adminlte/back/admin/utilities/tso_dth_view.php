
<!-- Inner Wrapper Start -->
<div id="page-wrapper">
    <!-- Signin Content Start -->
    <section class="inner-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h3 class="text-center purple">DTH Recharge</h3>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-12 col-sm-12 container-c">
                                <div class="formBox">
                                    <?php
                                    echo validation_errors('<div  class="error">', '</div>');
                                    if ($this->session->flashdata('message')) {
                                        ?>
                                        <div class="alert alert-success alert-dismissable" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                            <?php echo $this->session->flashdata('message'); ?>
                                        </div>
                                        <?php
                                    }

                                    ?>
                                    <?php
                                    $attributes = array("method" => "POST", "id" => "dth_recharge", "name" => "dth_recharge");
                                    echo form_open(base_url() . 'admin/utilities/dth_recharge', $attributes);
                                    ?>

                                <?php if($type == 'TSO'){ ?>
                                    <div>
                                        <input type="hidden" name="recharge_from" id="recharge_from" value="web"/>
                                        <input type="hidden" id="type" name="type" value="<?php echo $type; ?>">
                                        <select id="dth_operator" name="dth_operator" class="form-control select2">
                                            <option value="">Select DTH Operator</option>
                                            <?php foreach ($operators as $key => $value) { ?>
                                                <option value="<?php echo $key ?>"><?php echo $value ?></option>
                                            <?php } ?>
                                        </select>
                                        <span id="dth_operator_error" style="color: red; display: none">Please select operator</span>
                                    </div>
                                <?php }else{ ?>
                                    <input type="hidden" name="dth_from" id="dth_from" value="web"/>
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="inputBox">
                                                <input type="text" id="dth_operator" name="dth_operator" class="form-control" placeholder="DTH Operator"/>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>

                                    <div>
                                        <select id="location" name="location" class="form-control select2">
                                            <option value="">Select Location</option>
                                            <?php foreach ($locations as $key => $value) { ?>
                                                <option value="<?php echo $key ?>"><?php echo $value ?></option>
                                            <?php } ?>
                                        </select>
                                        <span id="location_error" style="color: red; display: none">Please select location</span>

                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="inputBox">
                                               <input type="text" id="customer_number" name="customer_number"  class="form-control" placeholder="Enter Subscriber ID Or Registered Mobile Number "/>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    

                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="input-box col-sm-4" style="padding-left: 0px;">
                                                <input type="text" id="recharge_amount" name="recharge_amount" class="form-control" placeholder="Recharge Amount"/>
                                            </div>
                                            <div class="col-sm-4" style="padding-left: 8px;">
                                                <a id="choose_plans" class="btn btn-default">Choose from plans</a>                                  
                                            </div>
                                            <?php if($type == 'TSO'): ?>
                                            <div class="col-sm-4" style="">
                                            
                                                <select id="recharge_type" name="recharge_type" class="form-control select2">
                                                    <option value="0">Topup</option>
                                                    <option value="1">Scheme</option>
                                                </select>
                                            </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                    
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12 col-sm-offset-3">
                                            <button type="submit" class="btn btn-purple btn-lg btn-block login-button txt-w col-sm-4">Proceed to recharge</button>
                                        </div>
                                    </div>


                                    <?php echo form_close(); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>

 <section class="inner-wrapper">
                    <div class="col-content">
                        <div class="container-fluid">
                        <div class="row">
                          <div class="col-md-12">
                                <div class="well">
                                    <h4>Online DTH Recharge - Enjoy Uninterrupted TV Experience!</h4>
                                  <p>Now, your DTH recharge is just a few clicks away with Rokad. We are one of the most reliable websites for online DTH recharge for millions of users across the country. Amidst hectic schedules and busy routines, it happens many times that you miss out your due date resulting in late fee charges and disrupted DTH services. Now, worry no more! With Rokad, you enjoy the ease & handiness of recharging anytime and from anywhere without spending much of your time & energy. With Rokad, you enjoy the comfort of choosing plans before paying the final amount. Be it a 6 Month Pack, Annual Pack, Monthly Pack or a 3 Month Pack, you can easily get it recharged from Rokad.</p>
                                    <h4>Easy & Quick DTH Recharge!</h4>
                                    <p>Rokad is the simplest way to recharge your DTH. With simple recharge method, you just need to select your operator and fill in the customer id & the amount. Choose your payment method from Debit/Credit Card, Net banking or Paytm Wallet and you can remain tension-free that your money is in safe hands. Enjoy watching your favorite shows without interruption. Our user-friendly App makes it easier for you to recharge through smartphones. Download our Rokad Mobile App for Android, Windows and iOS platforms from your play store & make a recharge in less than a minute.</p>
                            </div>
                          </div>
                        </div>    
                    </div>
                    </div>
                </section>
    <!-- Signin Content End -->             
</div>
<!-- Inner Wrapper End -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Choose Plans</h4>
      </div>
      <div class="modal-body">
            <table id="choose_plans_table" class="table table-striped table-bordered table-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>Description</th>
                        <!-- <th>Talktime</th> -->
                        <th>Validity</th>
                        <th>Amount</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
    window.msg = '';
    // $('#location').select2();
    $("#dth_operator").on('change', function() {
        $('#customer_number').val('');
            if ($("#dth_operator").val() == 'Airtel DTH' || $("#dth_operator").val() == 'Airtel TV DTH') {
                $("#customer_number").attr('maxlength', '10');
                $("#customer_number").attr("placeholder", "Enter the Customer Number").blur();
                var msg = "Should start with 3 or 7 or 8 or 9";
                $('#customer_number').data('msg', msg);
                $('#customer_number').blur();
            }
            else if ($("#dth_operator").val() == 'BIG TV / Reliance Digital TV' || $("#dth_operator").val() == 'RELIANCE BIG TV') {
                $("#customer_number").attr('maxlength', '12');
                $("#customer_number").attr("placeholder", "Enter Smart Card Number ").blur();
                var msg = "Should start with 2 or 7 or 8 or 9";
                $('#customer_number').data('msg', msg);
                $('#customer_number').blur();
            }
            else if ($("#dth_operator").val() == 'SUN TV' || $("#dth_operator").val() == 'SUN DIRECT') {
                $("#customer_number").attr('maxlength', '11');
                $("#customer_number").attr("placeholder", "Enter Smart Card Number").blur();
                var msg = "Should start with 1 or 4 or 7 or 8 or 9";
                $('#customer_number').data('msg', msg);
                $('#customer_number').blur();
            }
            else if ($("#dth_operator").val() == 'DISH TV' || $("#dth_operator").val() == 'DISH TV DTH') {
                $("#customer_number").attr('maxlength', '11');
                $("#customer_number").attr("placeholder", "Enter Card Number or Registered Mobile Number").blur();
                var msg = "Should start with 0 or 7 or 8 or 9";
                $('#customer_number').data('msg', msg);
                $('#customer_number').blur();
            }
            else if ($("#dth_operator").val() == 'TATA SKY (B2B)' || $("#dth_operator").val() == 'TATA SKY') {
                $("#customer_number").attr('maxlength', '10')
                $("#customer_number").attr("placeholder", "Enter Subscriber ID Or Registered Mobile Number").blur();
                var msg = "Should start with 1 or 7 or 8 or 9";
                $('#customer_number').data('msg', msg);
                $('#customer_number').blur();
            }
            else if ($("#dth_operator").val() == 'Videocon D2H') {
                $("#customer_number").attr("placeholder", "Enter Subscriber ID ").blur();
                var msg = "Should start with or 7 or 8 or 9";
                $('#customer_number').data('msg', msg);
                $('#customer_number').blur();
            }else{
                var msg = "";
                $('#customer_number').data('msg', msg);
                $('#customer_number').blur();
            }

            if($(this).val() !== ''){
                $('#dth_operator_error').css('display', 'none');
                // $('#customer_number').data('msg', '');
            }
    });


    $("#location").on('change', function() {
        if($(this).val() !== ''){
            $('#location_error').css('display', 'none');
            // error = 0;
        }
    });

</script>

<script type="text/javascript">
    var table;
    $(document).ready(function() {

        table = $('#choose_plans_table').DataTable({
            "destroy": true,
            "paging":   false,
            responsive: true,
            "columns": [
             {"data" : "PlanName"},
             {"data" : "Description"},
             // {"data" : "Talktime"},              
             {"data" : "Validity"},              
             {"data" : "Amount"}              
            ]
        });

        $('#choose_plans_table tbody').on( 'click', '.select_plan', function () {
            var data = $(this).parents('tr').find('td:nth-child(4)')[0].innerHTML;
            $('#recharge_amount').val(data);
            $('#myModal').modal('hide');
        } );

        var rechargeType;
        $('#dth_operator').on('click', function() {
            rechargeType = $('input[name="recharge_type"]:checked', '#dth_recharge').val();
        });

        $("#dth_operator").autocomplete({
            source: function(request, response) {
                $.ajax({
                    //  url: base_url + "admin/utilities/mobileOperators",
                    url: base_url + "admin/utilities/dthOperators",
                    dataType: "json",
                    data: {
                        term: request.term,
                        type: rechargeType
                    },
                    success: function(data) {
                        response($.map(data, function(item) {
                            return {
                                label: item.label,
                                value: item.value,
                                name: item.name,
                                id: item.id,
                            }
                        }));
                    }
                });
            },
            autoFocus: true,
            minLength: 1,
            select: function(event, ui) {
                $("#dth_operator").val(ui.item.name);
                if (ui.item.name == "No result found") {
                    $("#dth_operator").val("");
                }
                return false;
            }
        });
    });




    // $('#dth_recharge').bootstrapValidator({
    //     group: '.form-control',
    //     feedbackIcons: {
    //         valid: 'glyphicon glyphicon-ok',
    //         invalid: 'glyphicon',
    //         validating: 'glyphicon glyphicon-refresh'
    //     },
    //     fields: {
    //         recharge_type: {
    //             validators: {
    //                 notEmpty: {
    //                     message: '<b><font style="color:red; size="1">X</b>Please select type.</font>'
    //                 }
    //             }
    //         },
    //         dth_operator: {
    //             validators: {
    //                 notEmpty: {
    //                     message: '<b><font style="color:red; size="1">X</b>Please enter operator.</font>'
    //                 }
    //             }
    //         },
    //         customer_number: {
    //             validators: {
    //                 notEmpty: {
    //                     message: '<b><font style="color:red; size="1">X</b> Please enter Customer number.</font>'
    //                 },
    //                 regexp: {
    //                     regexp: /^[0-9]+$/,
    //                     message: '<b><font style="color:red; size="1">X</b>Enter valid Customer number</font>'
    //                 }
    //             }
    //         },
    //         recharge_amount: {
    //             validators: {
    //                 notEmpty: {
    //                     message: '<b><font style="color:red; size="1">X</b> Please enter recharge amount.</font>'
    //                 },
    //                 regexp: {
    //                     regexp: /^[0-9]+$/,
    //                     message: '<b><font style="color:red; size="1">X</b>Amount can only consist of number</font>'
    //                 }
    //             }
    //         }
    //     }
    // });

    $('#dth_recharge').validate({ 
             rules: {
                  dth_operator: {
                    required: true
                 },
                 location: {
                    required: true
                 },
                 customer_number: {
                    required: true,
                    number:true,
                    mobileno_start_with: true,                    
                    minlength: 1
                 },
                 recharge_amount: {
                    required: true,
                    number:true,
                    minlength: 1
                 }
            },
            messages: {
                mobile_no:  {
                    number:"Mobile no should be number only.",
                    minlength: "Please enter 10 digit Mobile No",
                    maxlength: "Please enter 10 digit Mobile No", 
                    valid_mobile_no: "Please enter valid mobile number",
                    mobileno_start_with: 'Mobile No should start with 7 or 8 or 9'
                }
            },
            submitHandler: function () { 
                $('#dth_recharge')[0].submit();
            }
        }); 

        
        $.validator.addMethod("mobileno_start_with", function (value, element) {
            
            var dth_operator = $('#dth_operator').val();

            if(dth_operator == 'Airtel TV DTH'){
                var max_len = '10';
                var regex = new RegExp("^[3,7-9]{1}[0-9]{" + (max_len-1) + "}$");
            }else if(dth_operator == 'DISH TV DTH'){
                var max_len = '11';
                var regex = new RegExp("^[0,7-9]{1}[0-9]{" + (max_len-1) + "}$");
            }else if(dth_operator == 'RELIANCE BIG TV'){
                var max_len = '12';
                var regex = new RegExp("^[2,7-9]{1}[0-9]{" + (max_len-1) + "}$");
            }else if(dth_operator == 'SUN DIRECT'){
                var max_len = '11';
                var regex = new RegExp("^[1,4,7-9]{1}[0-9]{" + (max_len-1) + "}$");
            }else if(dth_operator == 'TATA SKY'){
                var max_len = '10';
                var regex = new RegExp("^[1,7-9]{1}[0-9]{" + (max_len-1) + "}$");
            }else if(dth_operator == 'VIDEOCON D2h'){
                var max_len = '10';
                var regex = new RegExp("^[7-9]{1}[0-9]{" + (max_len-1) + "}$");
            }
            if (value.length > 0)
            {
                if(regex.test(value)) {
                    return true;
                } else {
                    return false;
                }
            }
             else
            {
                return true;
            }

        }, function(params, element) {
            console.log(element);
            return 'test';
        });

        $('#choose_plans').click(function(e){
            e.preventDefault();
            var location = $('#location').val();
            var dth_operator = $('#dth_operator').val();

            var error = 0;
            if(dth_operator == ''){
                // $('#location').select2('open');
                $('#dth_operator_error').css('display', 'block');
                error = 1;
            }
            if(location == ''){
                $('#location_error').css('display', 'block');
                error = 1;
            }
            if(error == 1){
                return false;
            }else{

                $('#choose_plans_table').DataTable({
                    "ajax": {
                        "url": base_url + "admin/utilities/getDataPlans",
                        "type": "POST",
                        "data": {
                            location: $('#location').val(),
                            service_type: 'D',
                            provider_name: $('#dth_operator').val(),
                        }
                    },
                    "paging":   false,
                    "searching": false,
                    "destroy": true,
                    "columnDefs": [ {
                        "targets": 4,
                        "data": null,
                        "defaultContent": "<button class='select_plan'>Select</button>"
                    } ],
                    "columns": [
                     {"data" : "PlanName"},
                     {"data" : "Description"},
                     // {"data" : "Talktime"},              
                     {"data" : "Validity"},              
                     {"data" : "Amount"}              
                    ]
                });

                $('#myModal').modal('show');
                
            }
            
        });

    $(document).on('change', '#dth_operator', function(){
        if($(this).val() != ''){
            $('#dth_operator_error').css('display', 'none');   
        }
    });

    $(document).on('change', '#location', function(){
        if($(this).val() != ''){
            $('#location_error').css('display', 'none');   
        }
    });

</script>
