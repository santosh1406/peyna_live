<script>
    $(document).ready(function () {
            /************** for confirm message***********/
        $('.confirm-div').hide();
        <?php if ($this->session->flashdata('invalid')) { ?>
                    $('.confirm-div').html('<?php echo $this->session->flashdata('invalid'); ?>').show();
        <?php } else if ($this->session->flashdata('invalid')) { ?>
                    $('.confirm-div').html('<?php echo $this->session->flashdata('valid'); ?>').show();
        <?php } ?>
            /*********************************************/
    

    $("#edit_bus_stop_validate").validate({
			rules: {
				bus_stops: {
					required: true					
				},
				seller_code: {
					required: true
				}
			},
			errorPlacement: function (error, element) {				
				if (element.attr("name") == "request_to") {				
				error.appendTo( element.closest(".form-group") );
				} else {
				error.appendTo( element.parent("div") );
				}
			},
			submitHandler: function(form) {
				// var amt = $("#amount").val();
				// alert('The amount you are adding in your wallet is = '+amt);
				$("#edit_amt").attr("disabled", true).html('Processing...');			
				$('.img-circle').show();		
				$.ajax({
					url: form.action,
					type: form.method,
					data: $(form).serialize(),
					async:false,
					dataType:'json',
					success: function(response) {
						console.log(response);
						if (response.flag == '@#success#@')
						{                   
							if(response.msg_type == "success")
							{
								// showLoader(response.msg_type,response.msg);
								$('.img-circle').hide(); 
                                swal({
                                        title: "Success",
                                        text: response.msg,
                                        type: response.msg_type,
                                        confirmButtonClass: "btn-success",
                                        confirmButtonText: "OK"
                                      },
                                      function(isConfirm) {
                                        if (isConfirm) {
                                          window.location = "<?php echo base_url() ?>admin/merge_bus_depot";
                                        }
                                    });
							}
						}
						else
						{
                            if(response.msg_type == "error")
							{
								//showLoader(response.msg_type,response.msg);
								$('.img-circle').hide(); 
								swal("Oops...", response.msg, "error");
								$("#edit_amt").attr("disabled", false).html('Submit');
							}
							else
							{
								$('.img-circle').hide(); 
								swal("Oops...", response.msg, "error");
								$("#edit_amt").attr("disabled", false).html('Submit');
							}     
						}
					}            
				});
			}			
		});
                $.validator.addMethod("check_numbers", function (value, element) {
                    var flag=false;
                    var TempAmount = parseInt(value);
                    if(TempAmount <=0){
                        flag = false;	
                    }else{
                        flag = true;	
                    }
                    return flag;
                }, 'Amount Should Be Greater Than 0.');
                
    });
</script> 


<section class="content">
    <div class="row">
        <!-- right column -->
        <div class="col-md-12">
            <!-- general form elements disabled -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Edit Bus Stop</h3>
                </div><!-- /.box-header -->
                  
                <?php
//                show($bus_stop_info,1);
                    $attributes = array("method" => "POST", "id" => "edit_bus_stop_validate", "class" => "cmxform form");
                    echo form_open('admin/merge_bus_depot/save_bus_stop/' .$bus_stop_info[0]['BUS_STOP_CD'], $attributes);   
                ?>  
                    
                    <div class="form-group">
                        <label class="col-lg-3" >Bus Stop:*</label>
                        <div class="col-lg-6">
                            
                            <input type="text" class="form-control" id="bus_stop_cd" name="bus_stop_cd" value="<?php echo $bus_stop_info[0]['BUS_STOP_NM'] . " (" . $bus_stop_info[0]['BUS_STOP_CD'] . ")"; ?>" disabled/>
                            <input type="hidden" name="bus_stop_cd" value="<?php echo $bus_stop_info[0]['BUS_STOP_CD']; ?>"/>
                            
                        </div>  


                    <div class="clearfix" style="height: 40px;clear: both;"></div>
                        <div class="form-group select2_div" style="display: none">
                            <label class="col-lg-3" >Depot:*</label>
                            <div class="col-lg-4">
                                <select class="select2" id="bus_depots" name="depot_cd" style="width: 300px;" tabindex="-1">
                                </select>
                            </div>   
                            <div class="col-lg-2">
                            <button class="btn btn-success btn-sm close_btn">X</button>
                            </div>   
                        </div>
                          
                    <div class="clearfix" style="clear: both;"></div>
                    <div class="form-group name_div">
                        <label class="col-lg-3" >Depot:*</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" value="<?php echo $bus_stop_info[0]['DEPOT_CD']; ?>" disabled/>
                            <input type="hidden" name="old_bus_depot" value="<?php echo $bus_stop_info[0]['DEPOT_CD']; ?>"/>
                            
                        </div>    
                        <div class="col-lg-2">
                            <a href="#" class="btn btn-success change_btn">Change</a>
                            
                        </div>  
                    </div>

                    <div class="clearfix" style="height: 40px;clear: both;"></div>
                    <div class="form-group">
                        <center> <button class="btn btn-primary" id="edit_amt" name="edit_amt" type="submit">Save</button>
                            <button class="btn btn-primary" id="back_btn" name="back_btn" onclick="history.go(-1);return false;" >Back</button></center>
                    </div>
                    <div class="clearfix" style="height: 40px;clear: both;"></div>
                <?php echo form_close(); ?>
            </div> 
        </div>
    </div>    
</section>

<script>
     $(document).ready(function() {

            $( "#bus_depots" ).select2({        
                  ajax: {
                    url: "<?php echo base_url() . '/admin/merge_bus_depot/get_bus_depots'; ?>",
                    dataType: 'json',
                    delay: 0,
                    data: function (params) {
                      return {
                        search_text: params.term // search term
                      };
                    },
                    processResults: function (data) {
                      // parse the results into the format expected by Select2.
                      // since we are using custom formatting functions we do not need to
                      // alter the remote JSON data
                      return {
                        results: $.map(data.bus_depot_info, function (item) {

                                return {
                                    text: item.DEPOT_NM,
                                    id: item.DEPOT_CD
                                }
                            })
                        };
                    },
                    cache: false
                  },
                  minimumInputLength: 2
            });



            $('.change_btn').click(function(e){
                e.preventDefault();
                $('.name_div').css('display', 'none');
                $('.select2_div').css('display', 'block');
            });

            $('.close_btn').click(function(e){
                e.preventDefault();
                $('.name_div').css('display', 'block');
                $('.select2_div').css('display', 'none');
            });

            

     });


</script>