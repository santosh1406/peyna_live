<section class="content-header">
    <h1> Bus Stops</h1>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <?php if($this->session->flashdata('msg')){ ?>
                <div id="msg_block" class="col-md-12 error_block <?php if(!empty($error)){ echo 'show';} else{ echo "show";} ?>">
                    <div class="alert alert-success alert-dismissable" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?php echo $this->session->flashdata('msg');  ?>
                    </div>
                </div>
            <?php } ?>
            <div class="box">
                <div class="box-header">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label id="bus_stops_label">Select Bus Stop</label>
                        <div>
                          <select class="select2" id="bus_stops" style="width: 300px;" tabindex="-1">
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="col-sm-12">
                      <div class="form-group">
                        <label id="bus_depots_label">Select Bus Depot</label>
                        <div>
                          <select class="select2" id="bus_depots" style="width: 300px;" tabindex="-1">
                          </select>
                        </div>
                      </div>
                    </div>

                    <div align="left" style="margin: 20px;">

                        <a class="btn btn-primary" id="add_stop" type="button" style="color: #FFFFFF;">Add Bus Stop </a> 
<!--                    <h4 align="right"><b><?php echo ucfirst($agent_name); ?></b></h4>-->
                    </div>

                    
                </div>
            </div><!-- /.box-header -->


            <div class="box-body">
                <div class="table-responsive">
                    <table id="merge_bus_stops_table" class="table table-bordered table-striped">
                        <thead>
                            <tr> 
                                <th>Bus Stop Code</th>
                                <th>Bus Stop Name</th>
                                <th>Depot Code</th> 
                                <th>Action</th>  
                            </tr>
                        </thead>
                         <tbody>
                        </tbody> 
                    </table>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.row -->
</section><!-- /.content -->
    <!-- /.content-wrapper -->
<script>
    var tkt_data;
     $(document).ready(function() {

        var tconfig = {
            "processing": true,
            "serverSide": false,
            "searching": true,
            "iDisplayLength": 25,
            "aLengthMenu": [[5, 10, 25,50, -1], [5, 10, 25,50, "All"]],
            "order": [[1, "desc"]],
            "ajax": {
              "url": BASE_URL+"admin/merge_bus_depot/getDataTableData",
              "type": "GET",
              "data": "json",
            },
            "columnDefs": [
              {
                "searchable": false
              }
            ],
            "scrollX": true,
            "bFilter": false,
            "bPaginate": true,
            "bRetrieve": true,            
            "oLanguage": {
              "sProcessing": '<img src="<?php echo base_url().BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>' 
            },
          };

          tkt_data = $('#merge_bus_stops_table').dataTable(tconfig);    
         
         error = false;
         
         $( "#bus_stops" ).select2({        
          ajax: {
            url: "<?php echo base_url() . '/admin/merge_bus_depot/get_bus_stop'; ?>",
            dataType: 'json',
            delay: 0,
            data: function (params) {
              return {
                search_text: params.term // search term
              };
            },
            processResults: function (data) {
              // parse the results into the format expected by Select2.
              // since we are using custom formatting functions we do not need to
              // alter the remote JSON data
              return {
                results: $.map(data.bus_stop_info, function (item) {

                        return {
                            text: item.BUS_STOP_NM,
                            id: item.BUS_STOP_CD
                        }
                    })
                };
            },
            cache: false
          },
          minimumInputLength: 2
        });

         $( "#bus_depots" ).select2({        
          ajax: {
            url: "<?php echo base_url() . '/admin/merge_bus_depot/get_bus_depots'; ?>",
            dataType: 'json',
            delay: 0,
            data: function (params) {
              return {
                search_text: params.term // search term
              };
            },
            processResults: function (data) {
              // parse the results into the format expected by Select2.
              // since we are using custom formatting functions we do not need to
              // alter the remote JSON data
              return {
                results: $.map(data.bus_depot_info, function (item) {
                        return {
                            text: item.DEPOT_NM,
                            id: item.DEPOT_CD
                        }
                    })
                };
            },
            cache: false
          },
          minimumInputLength: 2
        });


         // $(document).on('change', '#bus_depots', function(){
         //    if($(this).val() !== ''){
         //        $('#bus_depots_chosen a').css('color', 'black');
         //        error = false;
         //    }else{
         //        $('#bus_depots_chosen a').css('color', 'red');
         //        error = true;
         //    }
            
         // });


         $('#add_stop').click(function(e){
            e.preventDefault();
            var bus_stops = $('#bus_stops').val();
            var bus_depots = $('#bus_depots').val();

            if(bus_stops == '' || bus_stops == null){
                $('#bus_stops_label').css('color', 'red');
                error = true;
            }else{
                $('#bus_stops_label').css('color', 'black');
                error = false;
            }
            if(bus_depots == '' || bus_depots == null){
               $('#bus_depots_label').css('color', 'red');
               error = true;
            }else{
                $('#bus_depots_label').css('color', 'black');
                error = false;
            }

            if(error == false){
                
                save_url = "<?php echo base_url() . '/admin/merge_bus_depot/add_bus_stop'; ?>";
                $.ajax(
                {
                    type: 'POST',
                    url: save_url, 
                    data: {'bus_depot': bus_depots, 'bus_stops': bus_stops},
                    success: function(result){

                      swal({
                    title: 'Success',
                    html: true,
                    text: "Bus Stop Added Successfully!",
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ok'
                  }, function(){
                    location.reload();
                  }
                );

                        // tkt_data.api().ajax.url(BASE_URL+"admin/merge_bus_depot/getDataTableData").load();

                    }
                });
            }

         });

         $(document).on('click', '.delete', function(e){
            e.preventDefault();
             var delete_id = $(this).attr('data-delete-id');
            swal({
                    title: 'Are you sure?',
                    html: true,
                    text: "You want to delete this data.",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                  }, function(){
                     var url = "<?php echo base_url() . 'admin/merge_bus_depot/remove_bus_info' ?>"

                    $.ajax(
                    {
                        type: 'POST',
                        url: url, 
                        data: {'delete_id': delete_id},
                        success: function(result){
                          if(result == 'success'){
                            swal(
                              'Deleted!',
                              'Your data has been deleted.',
                              'success'
                            )
                          }
                          tkt_data.api().ajax.url(BASE_URL+"admin/merge_bus_depot/getDataTableData").load();
                        }
                    });
                  }
                );

         })

     });

</script>    