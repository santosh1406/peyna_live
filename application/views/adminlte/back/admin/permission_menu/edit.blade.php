@extends(strip_template(BACK_LAYOUT.'index'))

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <h1>
            Edit Permissions

        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    @include(strip_template(BACK_LAYOUT.'error'))
					
					<?php
					$attributes = array("method" => "POST", "name" => "prov_create_form", "id" => "prov_create_form");
					echo form_open(site_url().'/admin/permission_menu/store', $attributes);
					?>                    
                        <div class="box-header">
                            <div class="row-fluid">
                                <div class="col-md-3">
                                    <h3 class="box-title"><b>Group Name</b>: {{ ucwords($rolename->name) }}</h3>
                                    <input type="hidden" id="group_id" name="group_id" value="{{ $rolename->id }}">
<!--                                    <select align="left" class="form-control" id="group_id"  name="group_id" >
                                        <option value="">Select Groups</option>
                                        @foreach ($rolename as $key=>$sub_values)
                                        <option value="{{ $sub_values->id }}" {{ $sub_values->id == $group_id ? 'selected' : '' }} >{{ ucwords($sub_values->name) }}</option>
                                        @endforeach

                                    </select>   -->
                                </div>
                                <div style="text-align:right;margin-bottom: 5px;" class="col-md-9">
                                    <input type="checkbox" style="margin-left: 40%" class="alignMiddle select_all_chk" id="select_all_chk" name="select_all_chk"  >&nbsp;&nbsp;<b>Select All</b>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <button type="submit" class="btn btn-primary">Save Permissions</button>
                                    <a href="<?php echo base_url() . 'admin/permission_menu/delete/' . $group_id ?>" type="button" data-toggle="modal" class="btn btn-primary per_delete_btn">Delete All Permission</a>

                                    <!--<button class="btn btn-primary per_delete_btn" id="back_data" type="button"></button>--> 
                                    <button class="btn btn-primary back" id="back_data" type="button">Back</button> 
                                </div> 
                            </div>
                        </div>

                        <div class="box-body">
                            <table id="example" class="table table-bordered table-striped">
                                <thead>

                                    <tr >

                                        <th width="25%" style="border-left: 1px solid #DDD !important;"><center>Menu</center></th>
                                <th width="10%" style="border-left: 1px solid #DDD !important;"><center>View</center>
                                </th>
                                <th width="11%" style="border-left: 1px solid #DDD !important;">

                                    &nbsp;<center>Create</center>

                                </th>
                                <th width="10%" style="border-left: 1px solid #DDD !important;">

                                <center>  Edit</center>

                                </th>
                                <th width="12%" style="border-left: 1px solid #DDD !important;">
                                <center>   Delete</center>

                                </th>
                                <th width="35%" nowrap="nowrap" style="border-left: 1px solid #DDD !important;"><center>Tool Privileges</center></th>
                                </tr>

                                </thead>
                                <tbody >
                                    <?php
                                    //show($menu_parent,1);
                                    if (isset($menu_parent) && count($menu_parent) > 0) {
                                        $i = 1;
                                        ?>

                                        <?php
                                        foreach ($menu_parent as $key => $values) {
                                            $view_select = '';
                                            $add_select = '';
                                            $edit_select = '';
                                            $del_select = '';
                                            if ((isset($values['vview']) && $values['vview'] != '') && $values['vview'] != 'N')
                                                $view_select = 'checked="checked"';
                                            if ((isset($values['aadd']) && $values['aadd'] != '') && $values['aadd'] != 'N')
                                                $add_select = 'checked="checked"';
                                            if ((isset($values['eedit']) && $values['eedit'] != '') && $values['eedit'] != 'N')
                                                $edit_select = 'checked="checked"';
                                            if ((isset($values['ddelete']) && $values['ddelete'] != '') && $values['ddelete'] != 'N')
                                                $del_select = 'checked="checked"';
                                            ?>
                                        <input type='hidden' id='main_menu_"{{ $values['id'] }}' name='main_menu[]' value='{{ $values['id'] }}' >
                                        <tr class="ui-state-default tr_clone_{{ $values['id'] }}" style="background:#62c5ff; color:#fff; " id='{{ $values['id'] }}' >
                                            <td> {{ ucfirst($values['display_name']) }} </td>
                                            <td > 

                                        <center> 
                                            <input type="checkbox" id="main_view_{{ $values['id'] }}"  name="main_view_{{ $values['id'] }}" show_child="{{ $values['id'] }}" style=" "   class="alignMiddle view_chk_all"  <?php echo $view_select; ?> >&nbsp;&nbsp;
                                        </center>

                                        </td>
                                        <td > 
                                            <span class="horizontalAlignCenter"><center> 
                                                    <input type="checkbox" style=" " id="main_create_{{ $values['id'] }}" show_child="{{ $values['id'] }}" name="main_create_{{ $values['id'] }}" class="alignMiddle create_chk_all"  <?php echo $add_select; ?>>&nbsp;&nbsp;
                                                </center>
                                            </span>
                                        </td>
                                        <td > 
                                            <span class="horizontalAlignCenter"><center> 
                                                    <input type="checkbox" style=" " id="main_edit_{{ $values['id'] }}"  show_child="{{ $values['id'] }}" name="main_edit_{{ $values['id'] }}" class="alignMiddle edit_chk_all"  <?php echo $edit_select; ?>>&nbsp;&nbsp;
                                                </center>
                                            </span>
                                        </td>
                                        <td > 
                                            <span class="horizontalAlignCenter"><center> 
                                                    <input type="checkbox" style=" " id="main_delete_{{ $values['id'] }}"  show_child="{{ $values['id'] }}" name="main_delete_{{ $values['id'] }}" class="alignMiddle del_chk_all"  <?php echo $del_select; ?>>&nbsp;&nbsp;
                                                </center>
                                            </span>
                                        </td><td></td>

                                        </tr>



                                        <?php
                                        foreach ($sub_menu_view as $sub_key => $sub_values) {
                                            if ($values['id'] == $sub_values['parent']) {
                                                ?>
                                                <tr class="ui-state-default tr_clone_{{ $values['id'] }}" style="background:#97d9ff;" id='{{ $values['id'] }}' >
                                                    <td >  <input type='hidden' id='sub_main_menu_"{{ $sub_values['id'] }}' name='sub_main_menu[]' value='{{ $sub_values['id'] }}' >
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ ucfirst($sub_values['display_name']) }}
                                                    </td>
                                                    <td >  <center> <?php
                                                    echo (((isset($sub_values['vview']) && $sub_values['vview'] != '') && $sub_values['vview'] != 'N' ) ?
                                                            '<input type="checkbox" style=" "  class="alignMiddle sub_menu_view_chk chk_parent_view_' . $values['id'] . '"  self_id="' . $sub_values['id'] . '"  parent_id="' . $values['id'] . '" id="sub_main_view_' . $sub_values['menu_id'] . '" name="sub_main_view_' . $sub_values['menu_id'] . '" checked="checked" >' :
                                                            ' <input type="checkbox" style=" "  class="alignMiddle sub_menu_view_chk chk_parent_view_' . $values['id'] . '"  self_id="' . $sub_values['id'] . '"   parent_id="' . $values['id'] . '" id="sub_main_view_' . $sub_values['id'] . '" name="sub_main_view_' . $sub_values['id'] . '"  >')
                                                    ?> </center></td>
                                                <td ><center>  <?php
                                                    echo (((isset($sub_values['aadd']) && $sub_values['aadd'] != '') && $sub_values['aadd'] != 'N' ) ?
                                                            '<input type="checkbox" style=" " class="alignMiddle sub_menu_create_chk chk_parent_add_' . $values['id'] . ' "  self_id="' . $sub_values['id'] . '"    parent_id="' . $values['id'] . '" id="sub_main_create_' . $sub_values['menu_id'] . '" name="sub_main_create_' . $sub_values['menu_id'] . '" checked="checked" >' :
                                                            '<input type="checkbox" style=" "  class="alignMiddle sub_menu_create_chk chk_parent_add_' . $values['id'] . '" self_id="' . $sub_values['id'] . '"  parent_id="' . $values['id'] . '" id="sub_main_create_' . $sub_values['id'] . '" name="sub_main_create_' . $sub_values['id'] . '" >')
                                                    ?></center>   </td>
                                                <td > <center>  <?php
                                                    echo (((isset($sub_values['eedit']) && $sub_values['eedit'] != '') && $sub_values['eedit'] != 'N' ) ?
                                                            '<input type="checkbox" style=" "  class="alignMiddle sub_menu_edit_chk chk_parent_edit_' . $values['id'] . '" self_id="' . $sub_values['id'] . '"   parent_id="' . $values['id'] . '" id="sub_main_edit_' . $sub_values['menu_id'] . '" name="sub_main_edit_' . $sub_values['menu_id'] . '" checked="checked">' :
                                                            '<input type="checkbox" style=" "  class="alignMiddle sub_menu_edit_chk chk_parent_edit_' . $values['id'] . '" self_id="' . $sub_values['id'] . '"  parent_id="' . $values['id'] . '" id="sub_main_edit_' . $sub_values['id'] . '" name="sub_main_edit_' . $sub_values['id'] . '">')
                                                    ?> </center></td>
                                                <td >  <center> <?php
                                                    echo (((isset($sub_values['ddelete']) && $sub_values['ddelete'] != '') && $sub_values['ddelete'] != 'N' ) ?
                                                            '<input type="checkbox" style=" "   class="alignMiddle sub_menu_delete_chk chk_parent_delete_' . $values['id'] . '" self_id="' . $sub_values['id'] . '"  parent_id="' . $values['id'] . '" id="sub_main_delete_' . $sub_values['menu_id'] . '" name="sub_main_delete_' . $sub_values['menu_id'] . '" checked="checked">' :
                                                            ' <input type="checkbox" style=" "  class="alignMiddle sub_menu_delete_chk chk_parent_delete_' . $values['id'] . '" self_id="' . $sub_values['id'] . '"  parent_id="' . $values['id'] . '" id="sub_main_delete_' . $sub_values['id'] . '" name="sub_main_delete_' . $sub_values['id'] . '">')
                                                    ?> </center></td>
                                                <td> <div class="row-fluid">
                                                        <!--<div class="col-md-2">-->
                                                        @foreach ($menu_action_mapping as $act_key=>$act_values)
                                                        <input type='hidden' id='sub_tool_main_{{ $act_values['id'] }}' name='sub_tool_main[]' value='{{ $act_values['id'] }}' >
                                                        <div class="col-md-6">
                                                            <div class="btn-group btn-othergrp36">

                                                                <?php
                                                                $val_sub = strchr($sub_values['action_id'], $act_values['id']);
                                                                //$val_sub=strpos($sub_values['action_id'], $act_values['id'] ) ;
                                                                if ($act_values['relentity'] != 'All') {
                                                                    if ((isset($val_sub) && !empty($val_sub))) {
                                                                        $sub_tool = true;
                                                                    } else {
                                                                        $sub_tool = false;
                                                                    }
                                                                } else {
                                                                    $sub_tool = true;
                                                                }
                                                                //if (($sub_values['id'] == $act_values['menu_id']) || $act_values['menu_id']=='All')  { 
                                                                if ($sub_tool == true) {
                                                                    ?>
                                                                    <input type="checkbox" checked="checked" class="sub_tool_chk"  name="sub_tool_<?php echo $act_values['id'] . '_' . $sub_values['id']; ?>" id="sub_tool_<?php echo $act_values['id'] . '_' . $sub_values['id']; ?>" >
                                                                <?php } else { ?>

                                                                    <input type="checkbox"  class="sub_tool_chk"  name="sub_tool_<?php echo $act_values['id'] . '_' . $sub_values['id']; ?>" id="sub_tool_<?php echo $act_values['id'] . '_' . $sub_values['id']; ?>" >
                                                            <?php } ?>  
                                                            </div> 
                                                            {{ ucfirst($act_values['name']) }}
                <?php ?>
                                                        </div>
                                                        @endforeach

                                                    </div>
                                                </td>
                                                </tr>           
                                            <?php
                                            } else {
                                                
                                            }
                                            ?>



                                            <!--                                    /**** End sub menu & start the sub sub menu*****/-->

                                            <?php
                                            foreach ($sub_sub_menu as $sub_sub_key => $sub_sub_values) {
                                                //show($sub_sub_values['parent']);
                                                if ($values['id'] == $sub_values['parent'] && $sub_sub_values['parent'] == $sub_values['id']) {
                                                    // show($sub_values['id']); show('success');
                                                    ?>
                                                    <tr class="ui-state-default tr_clone_{{ $sub_values['id'] }}"  style="background:#cdedff;color:#fff;"  id='{{ $sub_values['id'] }}' >
                                                        <td >  <input type='hidden' id='sub_sub_main_menu_"{{ $sub_sub_values['id'] }}' name='sub_sub_main_menu[]' value='{{ $sub_sub_values['id'] }}' >
                                                    <center>{{ ucfirst($sub_sub_values['display_name']) }}</center> </td>
                                                    <td >  <center> <?php
                                                        echo (((isset($sub_sub_values['vview']) && $sub_sub_values['vview'] != '') && $sub_sub_values['vview'] != 'N' ) ?
                                                                '<input type="checkbox" style=" "  class="alignMiddle sub_sub_menu_view_chk child_chk_parent_view_' . $sub_values['id'] . ' sub_chk_parent_view_' . $values['id'] . '"  parent_id="' . $sub_values['id'] . '" id="sub_sub_main_view_' . $sub_sub_values['menu_id'] . '" name="sub_sub_main_view_' . $sub_sub_values['menu_id'] . '" checked="checked" >' :
                                                                ' <input type="checkbox" style=" "  class="alignMiddle sub_sub_menu_view_chk  child_chk_parent_view_' . $sub_values['id'] . '   sub_chk_parent_view_' . $values['id'] . '"  parent_id="' . $sub_values['id'] . '" id="sub_sub_main_view_' . $sub_sub_values['id'] . '" name="sub_sub_main_view_' . $sub_sub_values['id'] . '"  >')
                                                        ?> </center></td>
                                                    <td ><center>  <?php
                                                        echo (((isset($sub_sub_values['aadd']) && $sub_sub_values['aadd'] != '') && $sub_sub_values['aadd'] != 'N' ) ?
                                                                '<input type="checkbox" style=" " class="alignMiddle sub_sub_menu_create_chk  child_chk_parent_add_' . $sub_values['id'] . ' sub_chk_parent_add_' . $values['id'] . ' " parent_id="' . $sub_values['id'] . '" id="sub_sub_main_create_' . $sub_sub_values['menu_id'] . '" name="sub_sub_main_create_' . $sub_sub_values['menu_id'] . '" checked="checked" >' :
                                                                '<input type="checkbox" style=" "  class="alignMiddle sub_sub_menu_create_chk  child_chk_parent_add_' . $sub_values['id'] . ' sub_chk_parent_add_' . $values['id'] . '" parent_id="' . $sub_values['id'] . '" id="sub_sub_main_create_' . $sub_sub_values['id'] . '" name="sub_sub_main_create_' . $sub_sub_values['id'] . '" >')
                                                        ?></center>   </td>
                                                    <td > <center>  <?php
                                                        echo (((isset($sub_sub_values['eedit']) && $sub_sub_values['eedit'] != '') && $sub_sub_values['eedit'] != 'N' ) ?
                                                                '<input type="checkbox" style=" "  class="alignMiddle sub_sub_menu_edit_chk child_chk_parent_edit_' . $sub_values['id'] . ' sub_chk_parent_edit_' . $values['id'] . '" parent_id="' . $sub_values['id'] . '" id="sub_sub_main_edit_' . $sub_sub_values['menu_id'] . '" name="sub_sub_main_edit_' . $sub_sub_values['menu_id'] . '" checked="checked">' :
                                                                '<input type="checkbox" style=" "  class="alignMiddle sub_sub_menu_edit_chk child_chk_parent_edit_' . $sub_values['id'] . ' sub_chk_parent_edit_' . $values['id'] . '" parent_id="' . $sub_values['id'] . '" id="sub_sub_main_edit_' . $sub_sub_values['id'] . '" name="sub_sub_main_edit_' . $sub_sub_values['id'] . '">')
                                                        ?> </center></td>
                                                    <td >  <center> <?php
                                                        echo (((isset($sub_sub_values['ddelete']) && $sub_sub_values['ddelete'] != '') && $sub_sub_values['ddelete'] != 'N' ) ?
                                                                '<input type="checkbox" style=" "   class="alignMiddle sub_sub_menu_delete_chk child_chk_parent_delete_' . $sub_values['id'] . ' sub_chk_parent_delete_' . $values['id'] . '" parent_id="' . $sub_values['id'] . '" id="sub_sub_main_delete_' . $sub_sub_values['menu_id'] . '" name="sub_sub_main_delete_' . $sub_sub_values['menu_id'] . '" checked="checked">' :
                                                                ' <input type="checkbox" style=" "  class="alignMiddle sub_sub_menu_delete_chk child_chk_parent_delete_' . $sub_values['id'] . ' sub_chk_parent_delete_' . $values['id'] . '" parent_id="' . $sub_values['id'] . '" id="sub_sub_main_delete_' . $sub_sub_values['id'] . '" name="sub_sub_main_delete_' . $sub_sub_values['id'] . '">')
                                                        ?> </center></td>
                                                    <td> <div class="row-fluid">
                                                            <!--<div class="col-md-2">-->
                                                            @foreach ($menu_action_mapping as $act_key=>$act_values)
                                                            <input type='hidden' id='sub_sub_tool_main_{{ $act_values['id'] }}' name='sub_sub_tool_main[]' value='{{ $act_values['id'] }}' >
                                                            <div class="col-md-6">
                                                                <div class="btn-group btn-othergrp36">

                                                                    <?php
                                                                    $val_sub = strchr($sub_sub_values['action_id'], $act_values['id']);
                                                                    //$val_sub=strpos($sub_sub_values['action_id'], $act_values['id'] ) ;
                                                                    if ($act_values['relentity'] != 'All') {
                                                                        if ((isset($val_sub) && !empty($val_sub))) {
                                                                            $sub_tool = true;
                                                                        } else {
                                                                            $sub_tool = false;
                                                                        }
                                                                    } else {
                                                                        $sub_tool = true;
                                                                    }
                                                                    //if (($sub_values['id'] == $act_values['menu_id']) || $act_values['menu_id']=='All')  { 
                                                                    if ($sub_tool == true) {
                                                                        ?>
                                                                        <input type="checkbox" checked="checked" class="sub_tool_chk"  name="sub_sub_tool_<?php echo $act_values['id'] . '_' . $sub_sub_values['id']; ?>" id="sub_sub_tool_<?php echo $act_values['id'] . '_' . $sub_sub_values['id']; ?>" >
                                                                    <?php } else { ?>

                                                                        <input type="checkbox"  class="sub_tool_chk"  name="sub_sub_tool_<?php echo $act_values['id'] . '_' . $sub_sub_values['id']; ?>" id="sub_sub_tool_<?php echo $act_values['id'] . '_' . $sub_sub_values['id']; ?>" >
                                                                <?php } ?>  
                                                                </div> 
                                                                {{ ucfirst($act_values['name']) }}
                    <?php ?>
                                                            </div>
                                                            @endforeach

                                                        </div>
                                                    </td>
                                                    </tr>           
                                                <?php
                                                } else {
                                                    
                                                }
                                            }
                                        }
                                        ?>



                                        <?php $i++; ?>

                                    <?php } ?>

                                    <?php
                                } else {
                                    echo 'No data available';
                                }
                                ?>
                                </tbody>

                            </table>
                        </div><!-- /.box-body -->
                    </form>
                </div>
                <!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

@push('scripts')


<script>

    $(document).ready(function() {

        $(document).off('click', '.fa-plus').on('click', '.fa-plus', function(e) {
            /*e.preventDefault();
             //var elem = $('.fa-plus').index(this);alert(elem);
             var elem = $(this).attr('attr_sh');
             $(".sub_menu_" + elem).html('');
             
             var detail = {};
             var form = '';
             var div = '';
             detail['menu_id'] = elem;
             var ajax_url = 'admin/permission_menu/sub_menu_view/' + detail['menu_id'];
             $(this).addClass('fa-minus');
             $(this).removeClass('fa-plus');
             $('.sub_menu_' + elem).toggle('slow');
             
             get_data(ajax_url, form, div, detail, function(response)
             {
             var k = 1;
             str = "";
             
             $.each(response, function(index, value) {
             str += "<tr class='ui-state-default sub_menu_" + elem + "' id='" + value.id + "'><td><input type='hidden' id='sub_main_menu_" + value.id + "' name='sub_main_menu[]' value='" + value.id + "' ></td>";
             str += "<td><center>" + value['display_name'] + "</center></td>";
             str += '<td><input type="checkbox" style="margin-left: 40%" class="alignMiddle sub_menu_view_chk" id="sub_main_view_' + value.id + '" name="sub_main_view_' + value.id + '" ></td>';
             str += '<td><input type="checkbox" style="margin-left: 40%" class="alignMiddle sub_menu_create_chk" id="sub_main_create_' + value.id + '" name="sub_main_create_' + value.id + '" ></td>';
             str += '<td><input type="checkbox" style="margin-left: 40%" class="alignMiddle sub_menu_edit_chk" id="sub_main_edit_' + value.id + '" name="sub_main_edit_' + value.id + '"></td>';
             str += '<td><input type="checkbox" style="margin-left: 40%" class="alignMiddle sub_menu_delete_chk" id="sub_main_delete_' + value.id + '" name="sub_main_delete_' + value.id + '"  ></td>';
             
             
             str += ' <td> <div class="row-fluid">';
             
<?php //foreach ($menu_action_mapping as $key => $sub_values) {   ?>
             str += '<div class="col-md-6">';
             str += '<div class="btn-group btn-othergrp36">';
             
             str += '<input type="checkbox"  class="sub_tool_chk"  name="sub_tool_<?php echo $sub_values['id']; ?>_' + value.id + '" id="sub_tool_<?php echo $sub_values['id']; ?>_' + value.id + '" >';
             str += '</div><?php echo $sub_values['name']; ?>';
             str += '</div>';
<?php //}   ?>
             
             str += '</div></td>';
             str += "</tr>";
             
             
             });
             $(str).insertAfter(".tr_clone_" + elem);
             });
             */

        });

        /* $(document).off('click', '.fa-minus').on('click', '.fa-minus', function(e) {
         
         e.preventDefault();
         var elem = $(this).attr('attr_sh');
         
         $(this).addClass('fa-plus');
         $(this).removeClass('fa-minus');
         
         $('.sub_menu_' + elem).toggle('slow');
         });
         */



        $(document).off('change', '.select_all_chk').on('change', '.select_all_chk', function(e) {
            var isChecked = $(this).is(':checked');
            if (isChecked)
            {

                $('.view_chk_all,.view_chk,.sub_menu_view_chk,.sub_sub_menu_view_chk,.sub_sub_menu_create_chk,.sub_sub_menu_edit_chk,.sub_sub_menu_delete_chk,.create_chk_all,.create_chk,\n\
                    .sub_menu_create_chk,.edit_chk_all,.edit_chk,.sub_menu_edit_chk,.del_chk_all,\n\
                    .delete_chk,.sub_menu_delete_chk,.sub_tool_chk,.tool_chk').prop('checked', true);
            }
            else
            {

                $('.view_chk_all,.view_chk,.sub_menu_view_chk,\n\
                    .sub_sub_menu_view_chk,.sub_sub_menu_create_chk,.sub_sub_menu_edit_chk,.sub_sub_menu_delete_chk,.create_chk_all,\n\
                    .create_chk,.sub_menu_create_chk,.edit_chk_all,.edit_chk,.sub_menu_edit_chk,.del_chk_all,\n\
                    .delete_chk,.sub_menu_delete_chk,.sub_tool_chk,.tool_chk').prop('checked', false);
            }
        });


        $(document).off('change', '.view_chk_all').on('change', '.view_chk_all', function(e) {

            var elem = $(this).attr('show_child');

            var isChecked = $(this).is(':checked');

            if (isChecked)
            {
                $('.view_chk').prop('checked', true);
                $('.chk_parent_view_' + elem).prop('checked', true);
                $('.sub_chk_parent_view_' + elem).prop('checked', true);
            }
            else
            {
                $('.chk_parent_view_' + elem).prop('checked', false);
                $('.sub_chk_parent_view_' + elem).prop('checked', false);
                $('.view_chk').prop('checked', false);
                $('.select_all_chk').prop('checked', false);

            }
        });

        $(document).off('change', '.create_chk_all').on('change', '.create_chk_all', function(e) {
            var isChecked = $(this).is(':checked');
            var elem = $(this).attr('show_child');
            if (isChecked)
            {
                $('.create_chk').prop('checked', true);
                $('.chk_parent_add_' + elem).prop('checked', true);
                $('.sub_chk_parent_add_' + elem).prop('checked', true);
            }
            else
            {
                $('.chk_parent_add_' + elem).prop('checked', false);
                $('.sub_chk_parent_add_' + elem).prop('checked', false);
                $('.create_chk').prop('checked', false);
                $('.select_all_chk').prop('checked', false);
            }
        });

        $(document).off('change', '.edit_chk_all').on('change', '.edit_chk_all', function(e) {
            var isChecked = $(this).is(':checked');
            var elem = $(this).attr('show_child');
            if (isChecked)
            {
                $('.edit_chk').prop('checked', true);
                $('.chk_parent_edit_' + elem).prop('checked', true);
                $('.sub_chk_parent_edit_' + elem).prop('checked', true);
            }
            else
            {
                $('.edit_chk').prop('checked', false);
                $('.chk_parent_edit_' + elem).prop('checked', false);
                $('.sub_chk_parent_edit_' + elem).prop('checked', false);
                $('.select_all_chk').prop('checked', false);
            }
        });

        $(document).off('change', '.del_chk_all').on('change', '.del_chk_all', function(e) {
            var isChecked = $(this).is(':checked');
            var elem = $(this).attr('show_child');
            if (isChecked)
            {
                $('.delete_chk').prop('checked', true);
                $('.chk_parent_delete_' + elem).prop('checked', true);
                $('.sub_chk_parent_delete_' + elem).prop('checked', true);
            }
            else
            {
                $('.delete_chk').prop('checked', false);
                $('.chk_parent_delete_' + elem).prop('checked', false);
                $('.sub_chk_parent_delete_' + elem).prop('checked', false);
                $('.select_all_chk').prop('checked', false);
            }
        });



        $(document).off('change', '.sub_menu_view_chk').on('change', '.sub_menu_view_chk', function(e) {
            var isChecked = $(this).is(':checked');

            var elem = $(this).attr('parent_id');
            var self_elem = $(this).attr('self_id');

            if ($('.chk_parent_view_' + elem + ':checked').length == 1) {
                $('#main_view_' + elem).prop('checked', true);
            }
            if ($('.chk_parent_view_' + elem + ':checked').length == 0) {
                $('#main_view_' + elem).prop('checked', false);
            }

            if (!isChecked)
            {

                //   $('#main_view_' + elem).prop('checked', false);
                $('.child_chk_parent_view_' + self_elem).prop('checked', false);
                $('.select_all_chk').prop('checked', false);
            } else {
                $('.child_chk_parent_view_' + self_elem).prop('checked', true);
            }
        });

        $(document).off('change', '.sub_menu_create_chk').on('change', '.sub_menu_create_chk', function(e) {
            var isChecked = $(this).is(':checked');
            var elem = $(this).attr('parent_id');
            var self_elem = $(this).attr('self_id');

            if ($('.chk_parent_add_' + elem + ':checked').length == 1) {
                $('#main_create_' + elem).prop('checked', true);
            }
            if ($('.chk_parent_add_' + elem + ':checked').length == 0) {
                $('#main_create_' + elem).prop('checked', false);
            }
            if (!isChecked)
            {

                //  $('#main_create_' + elem).prop('checked', false);
                $('.child_chk_parent_add_' + self_elem).prop('checked', false);
                $('.select_all_chk').prop('checked', false);
            } else {
                $('.child_chk_parent_add_' + self_elem).prop('checked', true);
            }
        });

        $(document).off('change', '.sub_menu_edit_chk').on('change', '.sub_menu_edit_chk', function(e) {
            var isChecked = $(this).is(':checked');
            var elem = $(this).attr('parent_id');
            var self_elem = $(this).attr('self_id');

            if ($('.chk_parent_edit_' + elem + ':checked').length == 1) {
                $('#main_edit_' + elem).prop('checked', true);
            }
            if ($('.chk_parent_edit_' + elem + ':checked').length == 0) {
                $('#main_edit_' + elem).prop('checked', false);
            }

            if (!isChecked)
            {

                //$('#main_edit_' + elem).prop('checked', false);
                $('.child_chk_parent_edit_' + self_elem).prop('checked', false);
                $('.select_all_chk').prop('checked', false);

            } else {
                $('.child_chk_parent_edit_' + self_elem).prop('checked', true);
            }
        });
        $(document).off('change', '.sub_menu_delete_chk').on('change', '.sub_menu_delete_chk', function(e) {
            var isChecked = $(this).is(':checked');
            var elem = $(this).attr('parent_id');
            var self_elem = $(this).attr('self_id');


            if ($('.chk_parent_delete_' + elem + ':checked').length == 1) {
                $('#main_delete_' + elem).prop('checked', true);
            }
            if ($('.chk_parent_delete_' + elem + ':checked').length == 0) {
                $('#main_delete_' + elem).prop('checked', false);
            }
            if (!isChecked)
            {

                //  $('#main_delete_' + elem).prop('checked', false);
                $('.child_chk_parent_delete_' + self_elem).prop('checked', false);
                $('.select_all_chk').prop('checked', false);
            } else {
                $('.child_chk_parent_delete_' + self_elem).prop('checked', true);
            }
        });




        $(document).off('click', '.back').on('click', '.back', function(e)
        {

            window.location.href = BASE_URL + 'admin/group/';
        });


        $(document).off('click', '.per_delete_btn').on('click', '.per_delete_btn', function(e) {
            var flag = confirm('Please confirm to delete permission');

            if (flag)
            {
                return true;
            }

            return false;

        });


    });

    function selectMultiple()
    {
        var isChecked = document.getElementById('chkSelectAll').checked;
        if (isChecked)
        {
            $('input[name^=chkSelect]').prop('checked', true);
        }
        else
        {
            $('input[name^=chkSelect]').prop('checked', false);
            ;
        }
    }
</script>
@endpush
@stop