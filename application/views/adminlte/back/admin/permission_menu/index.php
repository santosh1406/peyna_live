<div class="content-wrapper">

    <section class="content-header">
        <h3>
            &nbsp;&nbsp;View Permissions

        </h3>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
					<?php
					$attributes = array("method" => "POST", "name" => "prov_create_form", "id" => "prov_create_form");
					echo form_open(site_url().'/admin/permission_menu/store', $attributes);
					?>                    
                        <div class="box-header">
                            <div class="row-fluid">
                                <div class="col-md-6">
                                    <h3 class="box-title"><b>Role Name</b> &nbsp;:<?php echo  ucwords($rolename->role_name) ?></h3>
<!--                                    <select align="left" class="form-control" id="group_id"  name="group_id" >
                                        <option value="">Select Groups</option>
                                        @foreach ($rolename as $key=>$sub_values)
                                        <option value="{{ $sub_values->id }}" {{ $sub_values->id == $group_id ? 'selected' : '' }}>{{ ucwords($sub_values->name) }}</option>
                                        @endforeach

                                    </select>   -->
                                </div>
                                <div style="text-align:right;margin-bottom: 5px;" class="col-md-6">
                                    <!--<input type="checkbox" style="margin-left: 40%" class="alignMiddle select_all_chk" id="select_all_chk" name="select_all_chk"  >&nbsp;&nbsp;<b>Select All</b>&nbsp;&nbsp;&nbsp;&nbsp;-->
                                    <a class="btn btn-primary back" href="<?php echo site_url() . '/admin/permission_menu/edit/'.$group_id ?>" class="btn btn-primary " role="button">Edit Permission</a>  
                                    <button class="btn btn-primary back" id="back_data" type="button">Back</button> 
                                </div> 
                            </div> </div>

                        <div class="box-body">
                            <table id="example" class="table table-bordered table-striped">
                                <thead>

                                <th width="25%" style="border-left: 1px solid #DDD !important;"><center>Menu</center></th>
                                <th width="10%" style="border-left: 1px solid #DDD !important;"><span class="horizontalAlignCenter"><center>View</center></span></th>
                                <th width="11%" style="border-left: 1px solid #DDD !important;"><span class="horizontalAlignCenter"><center>Create</center></span></th>
                                <th width="10%" style="border-left: 1px solid #DDD !important;"><span class="horizontalAlignCenter"><center>Edit</center></span></th>
                                <th width="12%" style="border-left: 1px solid #DDD !important;"><span class="horizontalAlignCenter"><center>Delete</center></span></th>
                                <th width="35%" nowrap="nowrap" style="border-left: 1px solid #DDD !important;"><center>Tool Privileges</center></th>
                                </tr>

                                </thead>
                                <tbody >
                                    <?php
                                    if (isset($menu_parent) && count($menu_parent) > 0) {
                                        $i = 1;
                                        
                                        foreach ($menu_parent as $key=>$values) { ?>
                                        <tr class="ui-state-default tr_clon_<?php echo $values['id'] ?>" style="background-color:#f5f5f5;" id='<?php echo $values['id'] ?>' >
                                        <td style="background: wheat;cursor: pointer;" id='<?php echo $values['id'] ?>' class="show_sub"> <?php echo ucfirst($values['display_name']) ?> <i class="glyphicon glyphicon-chevron-right pull-right" style="margin"></i></td>
                                            
                                        <td style="background: wheat">  <center> <?php echo (((isset($values['vview']) && $values['vview'] != '') && $values['vview']!='N'  ) ? '<i class="fa fa-check"></i>' : ' <img src="'.base_url() .'assets/images/Disable.png">') ?> </center></td>
                                        <td style="background: wheat"><center>  <?php echo (((isset($values['aadd']) && $values['aadd'] != '') && $values['aadd']!='N' ) ? '<i class="fa fa-check"></i>' :' <img src="'.base_url() .'assets/images/Disable.png">') ?></center>   </td>
                                        <td style="background: wheat"> <center>  <?php echo (((isset($values['eedit']) && $values['eedit'] != '') && $values['eedit']!='N' )  ? '<i class="fa fa-check"></i>' : ' <img src="'.base_url() .'assets/images/Disable.png">') ?> </center></td>
                                        <td style="background: wheat">  <center> <?php echo (((isset($values['ddelete']) && $values['ddelete'] != '') && $values['ddelete']!='N' )? '<i class="fa fa-check"></i>' :' <img src="'.base_url() .'assets/images/Disable.png">') ?> </center></td>
                            <td style="background: wheat"></td>

                                        </tr>


                                       
                                        <?php   foreach($sub_menu_view as $sub_key=>$sub_values){
                                        if ($values['id'] == $sub_values['parent']) {  ?>
                                        <tr class="ui-state-default tr_clone_<?php echo  $values['id'] ?> sub_menu_tr" id='<?php echo $values['id'] ?>'>
                                                <td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo ucfirst($sub_values['display_name']) ?> </td>
                                        <td >  <center> <?php echo (((isset($sub_values['vview']) && $sub_values['vview'] != '') && $sub_values['vview']!='N'  ) ? '<i class="fa fa-check"></i>' : ' <img src="'.base_url() .'assets/images/Disable.png">') ?> </center></td>
                                        <td ><center>  <?php echo (((isset($sub_values['aadd']) && $sub_values['aadd'] != '') && $sub_values['aadd']!='N' ) ? '<i class="fa fa-check"></i>' :' <img src="'.base_url() .'assets/images/Disable.png">') ?></center>   </td>
                                        <td > <center>  <?php echo (((isset($sub_values['eedit']) && $sub_values['eedit'] != '') && $sub_values['eedit']!='N' )  ? '<i class="fa fa-check"></i>' : ' <img src="'.base_url() .'assets/images/Disable.png">') ?> </center></td>
                                        <td >  <center> <?php echo (((isset($sub_values['ddelete']) && $sub_values['ddelete'] != '') && $sub_values['ddelete']!='N' )? '<i class="fa fa-check"></i>' :' <img src="'.base_url() .'assets/images/Disable.png">') ?> </center></td>
                                          <td> <div class="row-fluid">
                                                <!--<div class="col-md-2">-->
                                                
                                                <?php foreach ($menu_action_mapping as $act_key=>$act_values) {?>
                                                <div class="col-md-6">
                                                    <div class="btn-group btn-othergrp36">
                                                        
                                                      <?php
                                                   
                                                     $val_sub = strchr($sub_values['action_id'], $act_values['id']);
                                                     
                                                    if ($act_values['relentity'] != 'All') {
                                                            if ((isset($val_sub) && !empty($val_sub))) {
                                                                $sub_tool = true;
                                                            } else {
                                                                $sub_tool = false;
                                                            }
                                                        } else {
                                                            $sub_tool = true;
                                                        }
                                                     if($sub_tool==true ){
                                                      echo '<i class="fa fa-check"></i>' ;}
                                                      else {echo ' <img src="'.base_url() .'assets/images/Disable.png">';
                                                      
                                                      }?>  
                                                    </div> 
                                                    <?php echo ucfirst($act_values['name']) ?>
                                                      <?php 
                                                       ?>
                                                </div>
                                                <?php } ?>

                                            </div>
                                        </td>
                            </tr>
                            
                            
                            
                                    <?php } 
                                    /**** show sub sub menu****/
                                    
                                       foreach($sub_sub_menu as $sub_sub_key=>$sub_sub_values){
                                      if ($values['id'] == $sub_values['parent']  && $sub_sub_values['parent'] == $sub_values['id'] ) {
                                           // show($sub_values['id']); show('success');
                                            ?>
                                            <tr class="ui-state-default tr_clone_<?php echo $sub_values['id'] ?>" id='<?php echo $sub_values['id'] ?>' >
                                                <td > <center>{{ ucfirst($sub_sub_values['display_name']) }}</center> </td>
                                        <td >  <center> <?php echo (((isset($sub_sub_values['vview']) && $sub_sub_values['vview'] != '') && $sub_sub_values['vview']!='N'  ) ? '<i class="fa fa-check"></i>' : ' <img src="'.base_url() .'assets/images/Disable.png">') ?> </center></td>
                                        <td ><center>  <?php echo (((isset($sub_sub_values['aadd']) && $sub_sub_values['aadd'] != '') && $sub_sub_values['aadd']!='N' ) ? '<i class="fa fa-check"></i>' :' <img src="'.base_url() .'assets/images/Disable.png">') ?></center>   </td>
                                        <td > <center>  <?php echo (((isset($sub_sub_values['eedit']) && $sub_sub_values['eedit'] != '') && $sub_sub_values['eedit']!='N' )  ? '<i class="fa fa-check"></i>' : ' <img src="'.base_url() .'assets/images/Disable.png">') ?> </center></td>
                                        <td >  <center> <?php echo (((isset($sub_sub_values['ddelete']) && $sub_sub_values['ddelete'] != '') && $sub_sub_values['ddelete']!='N' )? '<i class="fa fa-check"></i>' :' <img src="'.base_url() .'assets/images/Disable.png">') ?> </center></td>
                                          <td> <div class="row-fluid">
                                                <!--<div class="col-md-2">-->
                                                
                                                <?php foreach ($menu_action_mapping as $act_key=>$act_values) {?>
                                                <div class="col-md-6">
                                                    <div class="btn-group btn-othergrp36">
                                                        
                                                      <?php
                                                   
                                                     $val_sub = strchr($sub_sub_values['action_id'], $act_values['id']);
                                                     
                                                    if ($act_values['relentity'] != 'All') {
                                                            if ((isset($val_sub) && !empty($val_sub))) {
                                                                $sub_tool = true;
                                                            } else {
                                                                $sub_tool = false;
                                                            }
                                                        } else {
                                                            $sub_tool = true;
                                                        }
                                                     if($sub_tool==true ){
                                                      echo '<i class="fa fa-check"></i>' ;}
                                                      else {echo ' <img src="'.base_url() .'assets/images/Disable.png">';
                                                      
                                                      }?>  
                                                    </div> 
                                                    {{ ucfirst($act_values['name']) }}
                                                      <?php 
                                                       ?>
                                                </div>
                                                <?php } ?>

                                            </div>
                                        </td>
                            </tr>
                            
                            
                            
                                    <?php } 
                                    
                                    
                                       }
                                    
                                    
                                    
                                    ?>
                                   <?php } ?>

                                    <?php $i++; 
                                    }?>
                                    
                                    <?php
                                } else {
                                    echo 'No data available';
                                }
                                ?>
                                </tbody>

                            </table>
                        </div><!-- /.box-body -->
                    </form>
                </div>
                <!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<script>

    $(document).ready(function() {
         $(".sub_menu_tr").hide();
         $(".show_sub").click(function() {
             var id  = $(this).attr("id");
              $(".tr_clone_"+id).slideToggle("slow");
         });

         $(document).off('click', '.back').on('click', '.back', function(e)
        {

            window.location.href = BASE_URL + 'admin/user_role';
        });
    });


</script>
<style>
    .fa-check{
        color:green;
        font-size: 17px;
    }
    </style>