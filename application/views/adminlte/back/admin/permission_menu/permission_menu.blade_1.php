<section class="content-header">
    <h1> Dashboard <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Menu Permission</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div id="role_wrapper">
        
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Menu Permission</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <!-- body content start -->
                        <div class="row">
                            <div class="col-xs-12 bhoechie-tab-container">
                                <div class="col-xs-2 bhoechie-tab-menu">
                                    <div class="list-group">
                                        <a href="#" class="list-group-item roles_direction text-center">
                                            <h4 class="glyphicon">Roles</h4><br/>
                                        </a>
                                    </div>
                                </div>
                                <div class="permission_type col-xs-10">
                                    <ul class="list-inline">
                                        <li>Menu Name</li>
                                        <li>View</li>
                                        <li>Add</li>
                                        <li>Edit</li>
                                        <li>Delete</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 bhoechie-tab-container">
                                <div class="col-lg-2 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                                    <div><!--<div class="list-group">-->
                                        <?php
                                        foreach ($rolename as $role_name) {
                                            if (strtolower($role_name->role_name) != "super admin") {
                                                echo "<a href='" . base_url() . "admin/role_menu_rel/get_role_permissions/" . $role_name->id . "' class='list-group-item rolename'>";
                                                echo $role_name->role_name;
                                                echo "</a>";
                                            }
                                       }
                                        ?> 
                                    </div>
                                </div>
                                <div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                                    <div class="bhoechie-tab-content" id="role_menu_rel_permission">
                                        <?php
                                       if (isset($role_id)) {
                                            $pmenu = array('items' => array(), 'parents' => array());
                                            // Builds the array lists with data from the menu table
                                            foreach ($menu_data as $mkey => $mvalue) {
                                                // Creates entry into items array with current menu item id ie. $menu['items'][1]
                                                $pmenu['items'][$mvalue['id']] = $mvalue;
                                                $pmenu['parents'][$mvalue['parent']][] = $mvalue['id'];
                                            }
                                            
                                            $permission = array();
                                            if ($permission_array != "") {
                                                foreach ($permission_array as $pkey => $pvalue) {
                                                    $permission[$pvalue["id"]] = $pvalue;
                                                }
                                            } else {
                                                $permission = "";
                                            }

                                            echo "<ul>";
                                            echo "<li>";
                                            echo get_child_menus(0, $pmenu, $permission);
                                            echo "</li>";
                                            echo "</ul>";
                                        }
                                        else
                                        {
                                            $role_id = "";
                                        }
                                        ?>
                                        <!-- Menu permission main content end -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- body content end -->
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        
    </div>
</section>

<script>                        
    $(document).ready(function(){                        
        
        $(document).off('click', 'div.bhoechie-tab-menu>div.list-group>a').on('click', 'div.bhoechie-tab-menu>div.list-group>a', function (e) {
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
            $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
        });
        
        $('.parent-check').on('ifChecked ifUnchecked', function(e){
            
            var type            = $(this).attr("name");
            var menu_data_attr  = $(this).closest( "li" ).attr( "data-category" );
            if(type == "view")
            {
                if(e.type == 'ifChecked')
                {
                    $("li[parent-category=" + menu_data_attr + "]").show(1500);
                }
                else if(e.type == 'ifUnchecked')
                {
                    $("li[data-category=" + menu_data_attr + "]").find('ul li').hide(1500);      
                    $(this).closest( "li" ).find('input').attr('checked', false)
                }
                else
                {
                    $("li[data-category=" + menu_data_attr + "]").find('ul li').hide(1500);
                }
            }//end of view event
            
            var checkboxvalue = "N";
            if(e.type == 'ifChecked')
            {
                checkboxvalue = "Y";
            }
            else if(e.type == 'ifUnchecked')
            {
                checkboxvalue = "N";      
            }
            
            // update menu permission
            $.post("<?php echo base_url(); ?>admin/role_menu_rel/set_role_permissions",{type: type, role:<?php echo $role_id; ?>, menu: menu_data_attr, checkboxvalue:checkboxvalue},function(response, status){
                if(status == "success")
                {
                    showLoader(response.msg_type,response.msg);
                }
            });

            $('input[type="checkbox"]').iCheck('update');
        });
        
});// end of document ready
</script>
