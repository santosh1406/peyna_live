@extends(strip_template(BACK_LAYOUT.'index'))

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <h1>
          Create New Permissions

        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    @include(strip_template(BACK_LAYOUT.'error'))                    
					<?php
					$attributes = array("method" => "POST", "name" => "prov_create_form", "id" => "prov_create_form");
					echo form_open(site_url().'/admin/permission_menu/store', $attributes);
					?>					
					<div class="box-header">
                        <div class="row-fluid">
                            <div class="col-md-6">
                                <select align="left" class="form-control" id="group_id"  name="group_id" >
                                    <option value="">Select Groups</option>
                                    @foreach ($rolename as $key=>$values)
                                    <option value="{{ $values->id }}" >{{ $values->name }}</option>
                                    @endforeach

                                </select>   
                            </div>
                            <div style="text-align:right;margin-bottom: 5px;" class="col-md-6">
                                <input type="checkbox" style="margin-left: 40%" class="alignMiddle select_all_chk" id="select_all_chk" name="select_all_chk"  >&nbsp;&nbsp;<b>Select All</b>&nbsp;&nbsp;&nbsp;&nbsp;
                                <button type="submit" class="btn btn-primary">Save Permissions</button>
                                <button class="btn btn-primary back" id="back_data" type="button">Back</button> 
                            </div> 
                        </div> </div>

                    <div class="box-body">
                        <table id="example" class="table table-bordered table-striped">
                            <thead>

                                <tr > <td>#</td>
                                    <th width="25%" style="border-left: 1px solid #DDD !important;"><center>Menu</center></th>
                            <th width="10%" style="border-left: 1px solid #DDD !important;"><span class="horizontalAlignCenter"><center> <input type="checkbox" style="margin-left: 40%" class="alignMiddle view_chk_all">&nbsp;&nbsp;View</center></span></th>
                            <th width="11%" style="border-left: 1px solid #DDD !important;"><span class="horizontalAlignCenter"><center><input type="checkbox" style="margin-left: 40%" class="alignMiddle create_chk_all">&nbsp;Create</center></span></th>
                            <th width="10%" style="border-left: 1px solid #DDD !important;"><span class="horizontalAlignCenter"><center><input type="checkbox" style="margin-left: 40%" class="alignMiddle edit_chk_all">&nbsp;&nbsp;Edit</center></span></th>
                            <th width="12%" style="border-left: 1px solid #DDD !important;"><span class="horizontalAlignCenter"><center><input type="checkbox" style="margin-left: 40%" class="alignMiddle del_chk_all">&nbsp;&nbsp;Delete</center></span></th>
                            <th width="35%" nowrap="nowrap" style="border-left: 1px solid #DDD !important;"><center>Tool Privileges</center></th>
                            </tr>

                            </thead>
                            <tbody >
                                <?php
                                //show($menu_parent,1);
                                if (isset($menu_parent) && count($menu_parent) > 0) {
                                    $i = 1;
                                    ?>
                                    @foreach ($menu_parent as $key=>$values)
                                    <tr class="ui-state-default tr_clone_{{ $values['id'] }}" id='{{ $values['id'] }}' >
                                        <td id='{{ $values['id'] }}'><i class="fa fa-plus" attr_sh="{{ $values['id'] }}"></i><input type="hidden" id="main_menu_{{ $values['id'] }}" name="main_menu[]" value="{{ $values['id'] }}"></td>
                                        <td> {{ $values['display_name'] }} </td>
                                        <td> <input type="checkbox" style="margin-left: 40%" class="alignMiddle view_chk" id="view_{{ $values['id'] }}" name="main_view_{{ $values['id'] }}"  ></td>
                                        <td> <input type="checkbox" style="margin-left: 40%" class="alignMiddle create_chk" id="create_{{ $values['id'] }}" name="main_create_{{ $values['id'] }}"  ></td>
                                        <td> <input type="checkbox" style="margin-left: 40%" class="alignMiddle edit_chk" id="edit_{{ $values['id'] }}" name="main_edit_{{ $values['id'] }}"  ></td>
                                        <td> <input type="checkbox" style="margin-left: 40%" class="alignMiddle delete_chk" id="delete_{{ $values['id'] }}" name="main_delete_{{ $values['id'] }}"  ></td>
                                        <td> <div class="row-fluid">
                                                <!--<div class="col-md-2">-->
                                                @foreach ($menu_action_mapping as $key=>$values)
                                                <div class="col-md-6">
                                                    <div class="btn-group btn-othergrp36">
                                                        
                                                        <input type="checkbox" class="tool_chk"  >
                                                    </div> 
                                                    {{ $values['actionname'] }}
                                                </div>
                                                @endforeach

                                            </div>
                                        </td>
                                    </tr>


                                    <?php $i++; ?>
                                    @endforeach
                                    <?php
                                } else {
                                    echo 'No data available';
                                }
                                ?>
                            </tbody>

                        </table>
                    </div><!-- /.box-body -->
                </form>
                </div>
                <!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

@push('scripts')


<script>

    $(document).ready(function() {

        $(document).off('click', '.fa-plus').on('click', '.fa-plus', function(e) {
            e.preventDefault();
            //var elem = $('.fa-plus').index(this);alert(elem);
            var elem = $(this).attr('attr_sh');
            $(".sub_menu_" + elem).html('');

            var detail = {};
            var form = '';
            var div = '';
            detail['menu_id'] = elem;
            var ajax_url = 'admin/permission_menu/sub_menu_view/' + detail['menu_id'];
            $(this).addClass('fa-minus');
            $(this).removeClass('fa-plus');
            $('.sub_menu_' + elem).toggle('slow');

            get_data(ajax_url, form, div, detail, function(response)
            {
                var k = 1;
                str = "";

                $.each(response, function(index, value) {
                    str += "<tr class='ui-state-default sub_menu_" + elem + "' id='" + value.id + "'><td><input type='hidden' id='sub_main_menu_"+value.id+"' name='sub_main_menu[]' value='"+value.id+"' ></td>";
                    str += "<td><center>" + value['display_name'] + "</center></td>";
                    str += '<td><input type="checkbox" style="margin-left: 40%" class="alignMiddle sub_menu_view_chk" id="sub_main_view_'+value.id+'" name="sub_main_view_'+value.id+'" ></td>';
                    str += '<td><input type="checkbox" style="margin-left: 40%" class="alignMiddle sub_menu_create_chk" id="sub_main_create_'+value.id+'" name="sub_main_create_'+value.id+'" ></td>';
                    str += '<td><input type="checkbox" style="margin-left: 40%" class="alignMiddle sub_menu_edit_chk" id="sub_main_edit_'+value.id+'" name="sub_main_edit_'+value.id+'"></td>';
                    str += '<td><input type="checkbox" style="margin-left: 40%" class="alignMiddle sub_menu_delete_chk" id="sub_main_delete_'+value.id+'" name="sub_main_delete_'+value.id+'"  ></td>';


                    str += ' <td> <div class="row-fluid">';

<?php foreach ($menu_action_mapping as $key => $values) { ?>
                        str += '<div class="col-md-6">';
                        str += '<div class="btn-group btn-othergrp36">';

                        str += '<input type="checkbox"  class="sub_tool_chk"  name="sub_tool_<?php echo $values['actionid']; ?>_'+value.id+'" id="sub_tool_<?php echo $values['actionid']; ?>_'+value.id+'" >';
                        str += '</div><?php echo $values['actionname']; ?>';
                        str += '</div>';
<?php } ?>

                    str += '</div></td>';
                    str += "</tr>";


                });
                $(str).insertAfter(".tr_clone_" + elem);
            });


        });
        $(document).off('click', '.back').on('click', '.back', function(e)
        {

            window.location.href = BASE_URL + 'admin/group';
        });
        $(document).off('click', '.fa-minus').on('click', '.fa-minus', function(e) {

            e.preventDefault();
            var elem = $(this).attr('attr_sh');

            $(this).addClass('fa-plus');
            $(this).removeClass('fa-minus');

            $('.sub_menu_' + elem).toggle('slow');
        });




        $(document).off('change', '.select_all_chk').on('change', '.select_all_chk', function(e) {
         var isChecked=$(this).is(':checked');
         if(isChecked)
         {
         
         $('.view_chk_all,.view_chk,.sub_menu_view_chk,.create_chk_all,.create_chk,.sub_menu_create_chk,.edit_chk_all,.edit_chk,.sub_menu_edit_chk,.del_chk_all,.delete_chk,.sub_menu_delete_chk,.sub_tool_chk,.tool_chk').prop('checked', true);               
         }
         else
         {
       
         $('.view_chk_all,.view_chk,.sub_menu_view_chk,.create_chk_all,.create_chk,.sub_menu_create_chk,.edit_chk_all,.edit_chk,.sub_menu_edit_chk,.del_chk_all,.delete_chk,.sub_menu_delete_chk,.sub_tool_chk,.tool_chk').prop('checked', false);
         }   
         });
         
         
        $(document).off('change', '.view_chk_all').on('change', '.view_chk_all', function(e) {


            var isChecked = $(this).is(':checked');
            
            if (isChecked)
            {
                $('.view_chk').prop('checked', true);
                $('.sub_menu_view_chk').prop('checked', true);
            }
            else
            {
                $('.sub_menu_view_chk').prop('checked', false);
                $('.view_chk').prop('checked', false);
                $('.select_all_chk').prop('checked', false);

            }
        });
        
        $(document).off('change', '.create_chk_all').on('change', '.create_chk_all', function(e) {
            var isChecked = $(this).is(':checked');
            if (isChecked)
            {
                $('.create_chk').prop('checked', true);
                $('.sub_menu_create_chk').prop('checked', true);
            }
            else
            {
                $('.sub_menu_create_chk').prop('checked', false);
                $('.create_chk').prop('checked', false);
                 $('.select_all_chk').prop('checked', false);
            }
        });
        
        $(document).off('change', '.edit_chk_all').on('change', '.edit_chk_all', function(e) {
            var isChecked = $(this).is(':checked');
            if (isChecked)
            {
                $('.edit_chk').prop('checked', true);
                $('.sub_menu_edit_chk').prop('checked', true);
            }
            else
            {
                $('.edit_chk').prop('checked', false);
                $('.sub_menu_edit_chk').prop('checked', false);
                 $('.select_all_chk').prop('checked', false);
            }
        });
        
         $(document).off('change', '.del_chk_all').on('change', '.del_chk_all', function(e) {
            var isChecked = $(this).is(':checked');
            if (isChecked)
            {
                $('.delete_chk').prop('checked', true);
                $('.sub_menu_delete_chk').prop('checked', true);
            }
            else
            {
                $('.delete_chk').prop('checked', false);
                $('.sub_menu_delete_chk').prop('checked', false);
                $('.select_all_chk').prop('checked', false);
            }
        });
        
        $(document).off('change', '.view_chk').on('change', '.view_chk', function(e) {
            var isChecked = $(this).is(':checked');
          /*  if (isChecked)
            {
                $('.delete_chk').prop('checked', true);
                $('.sub_menu_delete_chk').prop('checked', true);
            }
            else
            {
                $('.delete_chk').prop('checked', false);
                $('.sub_menu_delete_chk').prop('checked', false);
                $('.select_all_chk').prop('checked', false);
            }*/
        });
        
        
        

        /*$('input[name^=tabid_]').on('change',function() {
         var isChecked=$(this).is(':checked');
         var idArr=this.name.split('_');
         if(isChecked)
         {
         $('.btn-viewgrp input[id=view_'+idArr[1]+']').prop('checked', true);
         }
         else
         {
         $('.btn-viewgrp input[id=view_'+idArr[1]+']').prop('checked', false); 
         $('.btn-addgrp input[id=create_'+idArr[1]+']').prop('checked', false);                 
         $('.btn-editgrp input[id=edit_'+idArr[1]+']').prop('checked', false); 
         $('.btn-deletegrp input[id=delete_'+idArr[1]+']').prop('checked', false); 
         $('.btn-othergrp'+idArr[1]+' input[type=checkbox]').prop('checked', false); 
         }   
         });
         $('input[name^=view_],input[name^=create_],input[name^=edit_],input[name^=delete_]').on('change',function() {
         var isChecked=$(this).is(':checked');
         var idArr=this.name.split('_');
         if(isChecked)
         {
         $('.btn-group input[id=tabid_'+idArr[1]+']').prop('checked', true);
         }
         else
         {
         if (!$('.btn-group input[id=view_'+idArr[1]+']').is(':checked') &&
         !$('.btn-group input[id=create_'+idArr[1]+']').is(':checked') &&
         !$('.btn-group input[id=edit_'+idArr[1]+']').is(':checked') &&
         !$('.btn-group input[id=delete_'+idArr[1]+']').is(':checked'))
         {
         $('.btn-group input[id=tabid_'+idArr[1]+']').prop('checked', false);
         }
         }   
         });
         $('input[name^=view_]').on('change',function() {
         var isChecked=$(this).is(':checked');
         var idArr=this.name.split('_');
         if(isChecked)
         {
         $('.btn-group input[id=tabid_'+idArr[1]+']').prop('checked', true);
         }
         else
         {                
         $(this).closest('tr').find("input[type=checkbox]").prop('checked', false);
         }   
         });
         $('input[name^=create_],input[name^=edit_],input[name^=delete_]').on('change',function() {
         var isChecked=$(this).is(':checked');
         var idArr=this.name.split('_');
         if(isChecked)
         {
         $('.btn-group input[id=view_'+idArr[1]+']').prop('checked', true);
         }   
         });
         $('.other-grp').on('change',function() {
         var isChecked=$(this).is(':checked');
         var idArr=this.name.split('_');
         if(isChecked)
         {
         $('.btn-group input[id=tabid_'+idArr[1]+']').prop('checked', true);
         $('.btn-group input[id=view_'+idArr[1]+']').prop('checked', true);
         }   
         }); 
         $('.btn-group').find('input[type=checkbox]').on('change',function() {
         var isChecked=$(this).is(':checked');
         var isAllChecked=$("#toggle-all").is(':checked');
         if(!isChecked)
         {
         $("#toggle-all").prop('checked', false); 
         }
         
         if($('.btn-group').find('input[type=checkbox]').length==$('.btn-group').find('input[type=checkbox]:checked').length){
         $("#toggle-all").prop('checked', true);
         }
         
         })
         */


    });

    function selectMultiple()
    {
        var isChecked = document.getElementById('chkSelectAll').checked;
        if (isChecked)
        {
            $('input[name^=chkSelect]').prop('checked', true);
        }
        else
        {
            $('input[name^=chkSelect]').prop('checked', false);
            ;
        }
    }
</script>
@endpush
=======
@extends(strip_template(BACK_LAYOUT.'index'))

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <h1>
          Create New Permissions

        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    @include(strip_template(BACK_LAYOUT.'error'))                   
					<?php
					$attributes = array("method" => "POST", "name" => "prov_create_form", "id" => "prov_create_form");
					echo form_open(site_url().'/admin/permission_menu/store', $attributes);
					?>
					<div class="box-header">
                        <div class="row-fluid">
                            <div class="col-md-6">
                                <select align="left" class="form-control" id="group_id"  name="group_id" >
                                    <option value="">Select Groups</option>
                                    @foreach ($rolename as $key=>$values)
                                    <option value="{{ $values->id }}" >{{ $values->name }}</option>
                                    @endforeach

                                </select>   
                            </div>
                            <div style="text-align:right;margin-bottom: 5px;" class="col-md-6">
                                <input type="checkbox" style="margin-left: 40%" class="alignMiddle select_all_chk" id="select_all_chk" name="select_all_chk"  >&nbsp;&nbsp;<b>Select All</b>&nbsp;&nbsp;&nbsp;&nbsp;
                                <button type="submit" class="btn btn-primary">Save Permissions</button>
                                <button class="btn btn-primary back" id="back_data" type="button">Back</button> 
                            </div> 
                        </div> </div>

                    <div class="box-body">
                        <table id="example" class="table table-bordered table-striped">
                            <thead>

                                <tr > <td>#</td>
                                    <th width="25%" style="border-left: 1px solid #DDD !important;"><center>Menu</center></th>
                            <th width="10%" style="border-left: 1px solid #DDD !important;"><span class="horizontalAlignCenter"><center> <input type="checkbox" style="margin-left: 40%" class="alignMiddle view_chk_all">&nbsp;&nbsp;View</center></span></th>
                            <th width="11%" style="border-left: 1px solid #DDD !important;"><span class="horizontalAlignCenter"><center><input type="checkbox" style="margin-left: 40%" class="alignMiddle create_chk_all">&nbsp;Create</center></span></th>
                            <th width="10%" style="border-left: 1px solid #DDD !important;"><span class="horizontalAlignCenter"><center><input type="checkbox" style="margin-left: 40%" class="alignMiddle edit_chk_all">&nbsp;&nbsp;Edit</center></span></th>
                            <th width="12%" style="border-left: 1px solid #DDD !important;"><span class="horizontalAlignCenter"><center><input type="checkbox" style="margin-left: 40%" class="alignMiddle del_chk_all">&nbsp;&nbsp;Delete</center></span></th>
                            <th width="35%" nowrap="nowrap" style="border-left: 1px solid #DDD !important;"><center>Tool Privileges</center></th>
                            </tr>

                            </thead>
                            <tbody >
                                <?php
                                //show($menu_parent,1);
                                if (isset($menu_parent) && count($menu_parent) > 0) {
                                    $i = 1;
                                    ?>
                                    @foreach ($menu_parent as $key=>$values)
                                    <tr class="ui-state-default tr_clone_{{ $values['id'] }}" id='{{ $values['id'] }}' >
                                        <td id='{{ $values['id'] }}'><i class="fa fa-plus" attr_sh="{{ $values['id'] }}"></i><input type="hidden" id="main_menu_{{ $values['id'] }}" name="main_menu[]" value="{{ $values['id'] }}"></td>
                                        <td> {{ $values['display_name'] }} </td>
                                        <td> <input type="checkbox" style="margin-left: 40%" class="alignMiddle view_chk" id="view_{{ $values['id'] }}" name="main_view_{{ $values['id'] }}"  ></td>
                                        <td> <input type="checkbox" style="margin-left: 40%" class="alignMiddle create_chk" id="create_{{ $values['id'] }}" name="main_create_{{ $values['id'] }}"  ></td>
                                        <td> <input type="checkbox" style="margin-left: 40%" class="alignMiddle edit_chk" id="edit_{{ $values['id'] }}" name="main_edit_{{ $values['id'] }}"  ></td>
                                        <td> <input type="checkbox" style="margin-left: 40%" class="alignMiddle delete_chk" id="delete_{{ $values['id'] }}" name="main_delete_{{ $values['id'] }}"  ></td>
                                        <td> <div class="row-fluid">
                                                <!--<div class="col-md-2">-->
                                                @foreach ($menu_action_mapping as $key=>$values)
                                                <div class="col-md-6">
                                                    <div class="btn-group btn-othergrp36">
                                                        
                                                        <input type="checkbox" class="tool_chk"  >
                                                    </div> 
                                                    {{ $values['actionname'] }}
                                                </div>
                                                @endforeach

                                            </div>
                                        </td>
                                    </tr>


                                    <?php $i++; ?>
                                    @endforeach
                                    <?php
                                } else {
                                    echo 'No data available';
                                }
                                ?>
                            </tbody>

                        </table>
                    </div><!-- /.box-body -->
                </form>
                </div>
                <!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

@push('scripts')


<script>

    $(document).ready(function() {

        $(document).off('click', '.fa-plus').on('click', '.fa-plus', function(e) {
            e.preventDefault();
            //var elem = $('.fa-plus').index(this);alert(elem);
            var elem = $(this).attr('attr_sh');
            $(".sub_menu_" + elem).html('');

            var detail = {};
            var form = '';
            var div = '';
            detail['menu_id'] = elem;
            var ajax_url = 'admin/permission_menu/sub_menu_view/' + detail['menu_id'];
            $(this).addClass('fa-minus');
            $(this).removeClass('fa-plus');
            $('.sub_menu_' + elem).toggle('slow');

            get_data(ajax_url, form, div, detail, function(response)
            {
                var k = 1;
                str = "";

                $.each(response, function(index, value) {
                    str += "<tr class='ui-state-default sub_menu_" + elem + "' id='" + value.id + "'><td><input type='hidden' id='sub_main_menu_"+value.id+"' name='sub_main_menu[]' value='"+value.id+"' ></td>";
                    str += "<td><center>" + value['display_name'] + "</center></td>";
                    str += '<td><input type="checkbox" style="margin-left: 40%" class="alignMiddle sub_menu_view_chk" id="sub_main_view_'+value.id+'" name="sub_main_view_'+value.id+'" ></td>';
                    str += '<td><input type="checkbox" style="margin-left: 40%" class="alignMiddle sub_menu_create_chk" id="sub_main_create_'+value.id+'" name="sub_main_create_'+value.id+'" ></td>';
                    str += '<td><input type="checkbox" style="margin-left: 40%" class="alignMiddle sub_menu_edit_chk" id="sub_main_edit_'+value.id+'" name="sub_main_edit_'+value.id+'"></td>';
                    str += '<td><input type="checkbox" style="margin-left: 40%" class="alignMiddle sub_menu_delete_chk" id="sub_main_delete_'+value.id+'" name="sub_main_delete_'+value.id+'"  ></td>';


                    str += ' <td> <div class="row-fluid">';

<?php foreach ($menu_action_mapping as $key => $values) { ?>
                        str += '<div class="col-md-6">';
                        str += '<div class="btn-group btn-othergrp36">';

                        str += '<input type="checkbox"  class="sub_tool_chk"  name="sub_tool_<?php echo $values['actionid']; ?>_'+value.id+'" id="sub_tool_<?php echo $values['actionid']; ?>_'+value.id+'" >';
                        str += '</div><?php echo $values['actionname']; ?>';
                        str += '</div>';
<?php } ?>

                    str += '</div></td>';
                    str += "</tr>";


                });
                $(str).insertAfter(".tr_clone_" + elem);
            });


        });
        
        $(document).off('click', '.back').on('click', '.back', function(e)
        {

            window.location.href = BASE_URL + 'admin/group';
        });
        $(document).off('click', '.fa-minus').on('click', '.fa-minus', function(e) {

            e.preventDefault();
            var elem = $(this).attr('attr_sh');

            $(this).addClass('fa-plus');
            $(this).removeClass('fa-minus');

            $('.sub_menu_' + elem).toggle('slow');
        });




        $(document).off('change', '.select_all_chk').on('change', '.select_all_chk', function(e) {
         var isChecked=$(this).is(':checked');
         if(isChecked)
         {
         
         $('.view_chk_all,.view_chk,.sub_menu_view_chk,.create_chk_all,.create_chk,.sub_menu_create_chk,.edit_chk_all,.edit_chk,.sub_menu_edit_chk,.del_chk_all,.delete_chk,.sub_menu_delete_chk,.sub_tool_chk,.tool_chk').prop('checked', true);               
         }
         else
         {
       
         $('.view_chk_all,.view_chk,.sub_menu_view_chk,.create_chk_all,.create_chk,.sub_menu_create_chk,.edit_chk_all,.edit_chk,.sub_menu_edit_chk,.del_chk_all,.delete_chk,.sub_menu_delete_chk,.sub_tool_chk,.tool_chk').prop('checked', false);
         }   
         });
         
         
        $(document).off('change', '.view_chk_all').on('change', '.view_chk_all', function(e) {


            var isChecked = $(this).is(':checked');
            
            if (isChecked)
            {
                $('.view_chk').prop('checked', true);
                $('.sub_menu_view_chk').prop('checked', true);
            }
            else
            {
                $('.sub_menu_view_chk').prop('checked', false);
                $('.view_chk').prop('checked', false);
                $('.select_all_chk').prop('checked', false);

            }
        });
        
        $(document).off('change', '.create_chk_all').on('change', '.create_chk_all', function(e) {
            var isChecked = $(this).is(':checked');
            if (isChecked)
            {
                $('.create_chk').prop('checked', true);
                $('.sub_menu_create_chk').prop('checked', true);
            }
            else
            {
                $('.sub_menu_create_chk').prop('checked', false);
                $('.create_chk').prop('checked', false);
                 $('.select_all_chk').prop('checked', false);
            }
        });
        
        $(document).off('change', '.edit_chk_all').on('change', '.edit_chk_all', function(e) {
            var isChecked = $(this).is(':checked');
            if (isChecked)
            {
                $('.edit_chk').prop('checked', true);
                $('.sub_menu_edit_chk').prop('checked', true);
            }
            else
            {
                $('.edit_chk').prop('checked', false);
                $('.sub_menu_edit_chk').prop('checked', false);
                 $('.select_all_chk').prop('checked', false);
            }
        });
        
         $(document).off('change', '.del_chk_all').on('change', '.del_chk_all', function(e) {
            var isChecked = $(this).is(':checked');
            if (isChecked)
            {
                $('.delete_chk').prop('checked', true);
                $('.sub_menu_delete_chk').prop('checked', true);
            }
            else
            {
                $('.delete_chk').prop('checked', false);
                $('.sub_menu_delete_chk').prop('checked', false);
                $('.select_all_chk').prop('checked', false);
            }
        });
        
        $(document).off('change', '.view_chk').on('change', '.view_chk', function(e) {
            var isChecked = $(this).is(':checked');
          /*  if (isChecked)
            {
                $('.delete_chk').prop('checked', true);
                $('.sub_menu_delete_chk').prop('checked', true);
            }
            else
            {
                $('.delete_chk').prop('checked', false);
                $('.sub_menu_delete_chk').prop('checked', false);
                $('.select_all_chk').prop('checked', false);
            }*/
        });
        
        
        

        /*$('input[name^=tabid_]').on('change',function() {
         var isChecked=$(this).is(':checked');
         var idArr=this.name.split('_');
         if(isChecked)
         {
         $('.btn-viewgrp input[id=view_'+idArr[1]+']').prop('checked', true);
         }
         else
         {
         $('.btn-viewgrp input[id=view_'+idArr[1]+']').prop('checked', false); 
         $('.btn-addgrp input[id=create_'+idArr[1]+']').prop('checked', false);                 
         $('.btn-editgrp input[id=edit_'+idArr[1]+']').prop('checked', false); 
         $('.btn-deletegrp input[id=delete_'+idArr[1]+']').prop('checked', false); 
         $('.btn-othergrp'+idArr[1]+' input[type=checkbox]').prop('checked', false); 
         }   
         });
         $('input[name^=view_],input[name^=create_],input[name^=edit_],input[name^=delete_]').on('change',function() {
         var isChecked=$(this).is(':checked');
         var idArr=this.name.split('_');
         if(isChecked)
         {
         $('.btn-group input[id=tabid_'+idArr[1]+']').prop('checked', true);
         }
         else
         {
         if (!$('.btn-group input[id=view_'+idArr[1]+']').is(':checked') &&
         !$('.btn-group input[id=create_'+idArr[1]+']').is(':checked') &&
         !$('.btn-group input[id=edit_'+idArr[1]+']').is(':checked') &&
         !$('.btn-group input[id=delete_'+idArr[1]+']').is(':checked'))
         {
         $('.btn-group input[id=tabid_'+idArr[1]+']').prop('checked', false);
         }
         }   
         });
         $('input[name^=view_]').on('change',function() {
         var isChecked=$(this).is(':checked');
         var idArr=this.name.split('_');
         if(isChecked)
         {
         $('.btn-group input[id=tabid_'+idArr[1]+']').prop('checked', true);
         }
         else
         {                
         $(this).closest('tr').find("input[type=checkbox]").prop('checked', false);
         }   
         });
         $('input[name^=create_],input[name^=edit_],input[name^=delete_]').on('change',function() {
         var isChecked=$(this).is(':checked');
         var idArr=this.name.split('_');
         if(isChecked)
         {
         $('.btn-group input[id=view_'+idArr[1]+']').prop('checked', true);
         }   
         });
         $('.other-grp').on('change',function() {
         var isChecked=$(this).is(':checked');
         var idArr=this.name.split('_');
         if(isChecked)
         {
         $('.btn-group input[id=tabid_'+idArr[1]+']').prop('checked', true);
         $('.btn-group input[id=view_'+idArr[1]+']').prop('checked', true);
         }   
         }); 
         $('.btn-group').find('input[type=checkbox]').on('change',function() {
         var isChecked=$(this).is(':checked');
         var isAllChecked=$("#toggle-all").is(':checked');
         if(!isChecked)
         {
         $("#toggle-all").prop('checked', false); 
         }
         
         if($('.btn-group').find('input[type=checkbox]').length==$('.btn-group').find('input[type=checkbox]:checked').length){
         $("#toggle-all").prop('checked', true);
         }
         
         })
         */


    });

    function selectMultiple()
    {
        var isChecked = document.getElementById('chkSelectAll').checked;
        if (isChecked)
        {
            $('input[name^=chkSelect]').prop('checked', true);
        }
        else
        {
            $('input[name^=chkSelect]').prop('checked', false);
            ;
        }
    }
</script>
@endpush
@stop