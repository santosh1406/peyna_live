<style type="text/css">
    .help-block{
        color: red;
    }
</style>

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">MATM Amount Settlement View</h3>
                    </div>
                    <div class="box-body">
                        <?php
                        echo validation_errors('<div class="error">', '</div>');
                        if (!empty($msg)) {
                            ?>
                            <div class="alert alert-success alert-dismissable hide-it" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $msg; ?>
                            </div>
                            <?php
                        }
                        $attributes = array("method" => "POST", "id" => "mobile_recharge", "name" => "mobile_recharge");
                        echo form_open(base_url() . 'admin/matm/show_amount_approval_history', $attributes);
                        ?>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Transaction date :</label>
                            <div class="col-lg-2">
                                <input type="text" id="trans_date" name="trans_date" value="<?php echo $trans_date; ?>" class="form-control" />
                            </div>
                            <div class="col-lg-2">
                                <button class="btn btn-primary" id="search" name="search" type="submit">Search</button>
                            </div>                            
                            <div class="col-lg-3">
                                <a href="<?php echo base_url() . 'admin/matm/check_and_approve_amt_settlement/' ?>"><h4>Approve new settlement</h4></a>
                            </div>
                            <div class="col-lg-offset-2 ">&nbsp;</div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>


                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Last Credited Amount</h3>
                    </div>
                    <div class="box-body">
                        <table id="menu_display_table" class="table table-bordered table-hover"">
                            <thead>
                                <tr>
                                    <td><b>Transaction Date</b></td>
                                    <td><b>Amount</b></td>
                                    <td><b>Updated Date</b></td>
                                    <td><b>Status</b></td>
                                    <td><b>Comments</b></td>
                                    <td><b>View Details</b></td>
                                </tr>
                            </thead>

                            <?php
                            if (!empty($result)) {
                                foreach ($result as $eachRow) {
                                    $link = '<a href="' . base_url() . 'admin/matm/check_and_approve_amt_settlement?date=' . $eachRow['transaction_date'] . '">View</a>';
                                    ?>
                                    <tr>                                        
                                        <td><?php echo $eachRow['transaction_date']; ?></td>
                                        <td><?php echo $eachRow['credit_amount']; ?></td>
                                        <td><?php echo $eachRow['current_date']; ?></td>
                                        <td><?php echo $eachRow['status']; ?></td>
                                        <td><?php echo $eachRow['comments']; ?></td>
                                        <td><?php echo $link; ?></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </table>
                        <?php echo $pagi_links; ?>
                    </div>
                </div>

            </div>
        </div>
</div>
</section>
</div>


<script type="text/javascript">
        $(document).ready(function () {
            $(".hide-it").hide(10000);

            $("#trans_date").datepicker({
                'dateFormat': 'dd-mm-yy',
                maxDate: 0
            });
        });
</script>