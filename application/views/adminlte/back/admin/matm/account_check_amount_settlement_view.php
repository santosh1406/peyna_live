<style type="text/css">
    .help-block{
        color: red;
    }
</style>
<div id="divLoading" style="margin: 0px; padding: 0px; position: fixed; right: 0px; top: 0px; width: 100%; height: 100%; background-color: rgb(102, 102, 102); z-index: 30001; opacity: 0.8;display: none">
    <p style="position: absolute; color: White; top: 38%; left: 45%;">
    Please wait...
    <img src="http://loadinggif.com/images/image-selection/3.gif">
    </p>
</div>

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Approve agent amount settlement</h3>
                    </div>
                    <div class="box-body">
                        <?php
                        echo validation_errors('<div class="error">', '</div>');
                        if (!empty($msg)) {
                            ?>
                            <div class="alert alert-success alert-dismissable hide-it" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $msg; ?>
                            </div>
                            <?php
                        }
                        $attributes = array("method" => "POST", "id" => "mobile_recharge", "name" => "mobile_recharge");
                        echo form_open(base_url() . 'admin/matm/check_and_approve_amt_settlement', $attributes);
                        ?>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Transaction date :</label>
                            <div class="col-lg-2">
                                <input type="text" id="trans_date" name="trans_date" value="<?php echo $trans_date; ?>" class="form-control" />
                            </div>
                            <div class="col-lg-offset-2 ">
                                <button class="btn btn-primary" id="search" name="search" type="submit">Search</button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div>
                    <div class="box-body table-responsive">
                        <table id="menu_display_table" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Agent Name</th>
                                    <th>Transaction Date</th>
                                    <th>Amount</th>
                                    <th>Created Date</th>
                                    <th>View Transactions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($result)) {
                                    foreach ($result as $eachRow) {
                                        $table_id = $eachRow['id'];
                                        ?>
                                        <tr>
                                            <td><?php echo $eachRow['display_name']; ?></td>
                                            <td><?php echo $eachRow['transaction_date']; ?></td>
                                            <td><?php echo $eachRow['amount']; ?></td>
                                            <td><?php echo $eachRow['created_date']; ?></td>
                                            <td>
                                                <a id="<?php echo $table_id; ?>" class="btn btn-default view_agent_transations">View</a>
                                                <input type="hidden" id="user_id_<?php echo $table_id; ?>" name="user_id_<?php echo $table_id; ?>" value="<?php echo $eachRow['user_id']; ?>" />
                                                <input type="hidden" id="transaction_date_<?php echo $table_id; ?>" name="transaction_date_<?php echo $table_id; ?>" value="<?php echo $eachRow['transaction_date']; ?>" />                                                
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    echo "<tr><td colspan='5'>No data found</td></tr>";
                                }
                                ?>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div>

                <?php
                if (!empty($total_amount)) {
                    ?>
                    <br><br>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="box" style="padding:15px;">                        
                        <?php
                        //  Display form to approve the Settlement Amount
                        //  after submitting, data added in `matm_transfer_amount`
                        //
                        if ($total_amount > 0 && empty($amt_transfer_info)) {
                            $attributes = array("method" => "POST", "id" => "matm_amount_approval", "name" => "matm_amount_approval");
                            echo form_open(base_url() . 'admin/matm/check_and_approve_amt_settlement_submit', $attributes);
                            ?>
                            <div class="row">
                                <div style="display: block;" class="col-md-3 display-no">
                                    <div style="position: static;" class="form-group"><label>Total Settlement Amount : </label></div>
                                </div>
                                <div style="display: block;" class="col-md-3 display-no">
                                    <div style="position: static;" class="form-group"><b><?php echo $total_amount; ?></b></div>
                                </div>
                            </div>
                            <div class="row">
                                <div style="display: block;" class="col-md-3 display-no">
                                    <div style="position: static;" class="form-group"><label>Transaction Date : </label></div>
                                </div>
                                <div style="display: block;" class="col-md-3 display-no">
                                    <div style="position: static;" class="form-group"><b><?php echo $trans_date; ?></b></div>
                                </div>
                            </div>
                            <div class="row">
                                <div style="display: block;" class="col-md-3 display-no">
                                    <div style="position: static;" class="form-group"><label>Comments : </label></div>
                                </div>
                                <div style="display: block;" class="col-md-3 display-no">
                                    <div style="position: static;" class="form-group"><textarea name="comments"></textarea></div>
                                </div>
                            </div>
                            <div class="row">
                                <input type="hidden" name="total_matm_settlement_amount_in_day" id="total_matm_settlement_amount_in_day" value="<?php echo base64_encode($total_amount); ?>">
                                <input type="hidden" name="transaction_date_for_approval" value="<?php echo base64_encode($trans_date); ?>">

                                <div style="display: block;" class="col-md-1 display-no">
                                    <button class="btn btn-primary" id="save" name="save" type="submit">Approve</button>
                                </div>
                            </div>
                            <?php
                            echo form_close();
                        } else {
                            //
                            //  Display settlement ammount info, iff Account team already approved
                            //  date shows from `matm_transfer_amount` table
                            //
                            ?>
                            <div class="row">
                                <div style="display: block;" class="col-md-3 display-no">
                                    <div style="position: static;" class="form-group"><label>Total Settlement Amount : </label></div>
                                </div>
                                <div style="display: block;" class="col-md-3 display-no">
                                    <div style="position: static;" class="form-group"><b><?php echo $amt_transfer_info['credit_amount']; ?></b></div>
                                </div>
                            </div>
                            <div class="row">
                                <div style="display: block;" class="col-md-3 display-no">
                                    <div style="position: static;" class="form-group"><label>Transaction Date : </label></div>
                                </div>
                                <div style="display: block;" class="col-md-3 display-no">
                                    <div style="position: static;" class="form-group"><b><?php echo $amt_transfer_info['transaction_date']; ?></b></div>
                                </div>
                            </div>
                            <div class="row">
                                <div style="display: block;" class="col-md-3 display-no">
                                    <div style="position: static;" class="form-group"><label>Comments : </label></div>
                                </div>
                                <div style="display: block;" class="col-md-3 display-no">
                                    <div style="position: static;" class="form-group"><?php echo $amt_transfer_info['comments'];?></div>
                                </div>
                            </div>
                            <div class="row">
                                <div style="display: block;" class="col-md-3 display-no">
                                    <div style="position: static;" class="form-group"><label>Approved Date : </label></div>
                                </div>
                                <div style="display: block;" class="col-md-3 display-no">
                                    <div style="position: static;" class="form-group"><?php echo $amt_transfer_info['current_date'];?></div>
                                </div>
                            </div>
                            <div class="row">
                                <div style="display: block;" class="col-md-3 display-no">
                                    <div style="position: static;" class="form-group"><label>Is Trimax Wallet Updated ?  </label></div>
                                </div>
                                <div style="display: block;" class="col-md-3 display-no">
                                    <div style="position: static;" class="form-group">
                                       <image src="<?php echo base_url() . '/assets/adminlte/back/img/' . get_image_name($amt_transfer_info['is_trimax_wallet_transfer']); ?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div style="display: block;" class="col-md-3 display-no">
                                    <div style="position: static;" class="form-group"><label>Is Agent Wallet Updated ?  </label></div>
                                </div>
                                <div style="display: block;" class="col-md-3 display-no">
                                    <div style="position: static;" class="form-group">
                                        <image src="<?php echo base_url() . '/assets/adminlte/back/img/' . get_image_name($amt_transfer_info['is_agent_wallet_transfer']); ?>" />
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
</div>
</section>
</div>



<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 100%">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Transaction List</h4>
            </div>
            <div class="modal-body">
                <table id="choose_plans_table" class="table table-striped table-bordered table-responsive nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>AgentName</th>
                            <th>Type</th>
                            <!--<th>Operator</th>-->
                            <th>TxnDate</th>
                            <th>Amount</th>
                            <th>TrimaxId</th>
                            <th>WalletTranId</th>
                            <!--<th>CreatedOn</th>-->
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $(".hide-it").hide(10000);

        $("#trans_date").datepicker({
            'dateFormat': 'dd-mm-yy',
            maxDate: 0
        });

        table = $('#choose_plans_table').DataTable({
            "destroy": true,
            "paging":   false,
            responsive: true,
            "columns": [                
                {"data" : "No"},
                {"data" : "AgentName"},
                {"data" : "Type"},
//                {"data" : "Operator"},
                {"data" : "TxnDate"},
                {"data" : "Amount"},
                {"data" : "TrimaxId"},
                {"data" : "WalletTranId"},
//                {"data" : "CreatedOn"}
            ]
        });

        $(".view_agent_transations").click(function(e){
            var view_clicked = $(this);
            var view_id = view_clicked.attr("id");
            var transaction_date = $('#transaction_date_' + view_id).val();
            var user_id = $('#user_id_' + view_id).val();            
            //alert(view_id +" === "+ transaction_date +" === "+ user_id);            
            e.preventDefault();
            $('#choose_plans_table').DataTable({
                "ajax": {
                    "url": base_url + "admin/matm/ajax_view_agent_transactions_by_date",
                    "type": "POST",
                    "data": {
                        id : view_id,
                        transaction_date: transaction_date,
                        user_id : user_id
                    },
                    "dataType": "json"
                },
                "paging":   false,
                "searching": false,
                "destroy": true,
                "columnDefs": [{
                    "targets": 0,
                    "data": null
                }],
                "columns": [
                    {"data" : "No"},
                    {"data" : "AgentName"},
                    {"data" : "Type"},
//                    {"data" : "Operator"},
                    {"data" : "TxnDate"},
                    {"data" : "Amount"},
                    {"data" : "TrimaxId"},
                    {"data" : "WalletTranId"},
//                    {"data" : "CreatedOn"}
                ]
            });
            $('#myModal').modal('show');
        });
        
    $(document).on('click', '#save', function(){
      
        $('#save').attr('disabled', 'disabled');
        $('#divLoading').css('display', 'block');
     });
     
    });
</script>