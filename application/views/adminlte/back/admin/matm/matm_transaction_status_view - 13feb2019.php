<section class="content">   
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header "> 
                    <h3 class="box-title">Transaction Status</h3>
                </div>                
                <div class="box-body">                    
                    <div>
                        <?php
                        
                        if ($this->session->tempdata('msg')) {
                            ?>
                            <div class="alert alert-success alert-dismissable" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->tempdata('msg'); ?>
                            </div>
                            <?php
                        }
                        $attributes = array("method" => "POST", "id" => "matm_form", "name" => "matm_form");
                        echo form_open(site_url() . '/admin/matm/transaction_process', $attributes);
                        ?>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        
                        <?php echo validation_errors('<div class="error">', '</div>'); ?>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-0">
                                <input type="hidden" name="service_id" id="service_id" value="<?php echo $SERVICEID ?>">
                                <input type="hidden" name="Amount" id="Amount" value="<?php echo '0' ?>">
                                <button class="btn btn-primary" id="submit"  type="submit">Submit</button>                                
<!--                                <a class="btn btn-primary" href="<?php echo base_url('admin/dashboard'); ?>" type="button">Cancel</a>-->
                            </div><!--/div-->
                        </div><!--/div-->
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>

<!-- Signin Content End -->             
<!--/div-->
<!-- Inner Wrapper End -->


