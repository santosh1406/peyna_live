<style type="text/css">
    .help-block{
        color: red;
    }
</style>
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h2 class="box-title">Agent Settlement</h2>
                    </div>

                    <div class="box-body table-responsive">
                        <?php
                        $attributes = array("method" => "POST", "id" => "agent_settlement_report", "name" => "agent_settlement_report");
                        echo form_open(base_url() . 'admin/epaylater/agent_settlement_report', $attributes);
                        ?>
                        <div class="box" style="padding:5px;">
                            <br>
                            <div class="row">
                                <div style="display: block;" class="col-md-2 display-no">
                                    <div style="position: static;" class="form-group">
                                        <label>Agent Id : </label>
                                    </div>
                                </div>
                                <div style="display: block;" class="col-md-3 display-no">
                                    <div style="position: static;" class="form-group">
                                        <input type="text" id="agent_id" name="agent_id" class="form-control" value="<?php echo $agent_id; ?>"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div style="display: block;" class="col-md-2 display-no">
                                    <div style="position: static;" class="form-group">
                                        <label>Transaction Date : </label>
                                    </div>
                                </div>
                                <div style="display: block;" class="col-md-3 display-no">
                                    <div style="position: static;" class="form-group">
                                        <input type="text" id="trans_date" name="trans_date" class="form-control" value="<?php echo $trans_date; ?>"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div style="display: block;" class="col-md-1 display-no">
                                    <button class="btn btn-primary" name="submit" id="submit" value="search" type="submit">Search</button>
                                </div>
                                <div class="col-lg-1 control-label">
                                    <button class="btn btn-primary" id="reset" name="reset" type="button">Reset</button>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>

                    <div class="box-body table-responsive">
                        <table id="menu_display_table" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Agent Name</th>
                                    <th>Transaction Date</th>
                                    <th>Amount</th>
                                    <th>Created Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($result)) {
                                    foreach ($result as $eachRow) {
                                        ?>
                                        <tr>
                                            <td><?php echo $eachRow['display_name']; ?></td>
                                            <td><?php echo $eachRow['transaction_date']; ?></td>
                                            <td><?php echo $eachRow['amount']; ?></td>
                                            <td><?php echo $eachRow['created_date']; ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>

                            </tbody>
                        </table>
                        <?php echo $pagi_links; ?>
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    $(document).ready(function () {
        $("#trans_date").datepicker({
            'dateFormat': 'dd-mm-yy',
            maxDate: 0
        });

        $("#reset").click(function () {
            $('#agent_id').val("");
            $('#trans_date').val("");
            // submit form
            $("#submit").trigger('click');
        });
    });
</script>


