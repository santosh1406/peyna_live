<section class="content">   
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header "> 
                    <h3 class="box-title">Cash Withdraw</h3>
                </div>                
                <div class="box-body">                    
                    <div>
                        <?php
                        
                        if ($this->session->tempdata('msg')) {
                            ?>
                            <div class="alert alert-success alert-dismissable" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->tempdata('msg'); ?>
                            </div>
                            <?php
                        }
                        $attributes = array("method" => "POST", "id" => "matm_form", "name" => "matm_form");
                        echo form_open(site_url() . '/admin/matm/transaction_process', $attributes);
                        ?>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="from-group">
                            <label class="col-lg-4 control-label">Amount*:</label>  
                            <div class="col-lg-4">
                                <input type="text" id="Amount" name="Amount" class="form-control" value="" placeholder="Please Enter Amount "/>                                
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <?php echo validation_errors('<div class="error">', '</div>'); ?>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-5">
                                <input type="hidden" name="ClientUniqueID" id="ClientUniqueID" value="<?php echo $this->sessionData["id"]  . 'M' . time();?>"> 
                                <input type="hidden" name="service_id" id="service_id" value="<?php echo $SERVICEID ?>">

                                <button class="btn btn-primary" id="submit"  type="submit">Submit</button>                                
                                <a class="btn btn-primary" href="<?php echo base_url('admin/dashboard'); ?>" type="button">Cancel</a>
                            </div><!--/div-->
                        </div><!--/div-->
                        <?php echo form_close();?>
                    </div>
                    <?php
                    if ($this->session->tempdata('msg')=='Card Transaction Successfully') {
                        echo '<script>window.open("'.export_pdf.'/'.$this->sessionData['id'].'","_blank")</script>';
                            
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
</div>

<!-- Signin Content End -->             
<!--/div-->
<!-- Inner Wrapper End -->

<script type="text/javascript">
    $(document).ready(function() {
    var service_id = $('#service_id').val();
    var min_matm_amt = '100';
    
//    if(service_id==MATM_CASH_WITHDRAW)
//    {
//        var min_matm_amt = '100';
//    }
//    
    
     $('#matm_form').validate({ 
         rules: {            
             Amount: {
                required:true,   
                number:true,
                valid_amt:true,
                    // min_limt:true           
                    min: min_matm_amt,
                    max: 50000        
                },                
            },
            messages: {                                
                Amount: {
                    required:"Please Enter Amount",                   
                    valid_amt:"Please Enter Valid Amount",
                    min_limt:"Please Enter amount Greater than equal to "+ min_matm_amt+"",                   
                },                               
            },
            submitHandler: function (form) { 
                // submit.form();
                form.submit();
            }
        }); 

     $.validator.addMethod("valid_amt", function (value, element) {
       
     
     
       
        var regex = new RegExp("^[0-9]+$");             
        if(parseInt(value) > 0 && regex.test(value))
        {
            return true;  
        }
        else
        {
            return false;
        }                        
    },'Please Enter Valid Amount');

//     $('input').on('blur', function() {
//        if ($("#matm_form").valid()) {
//            $('#submit').prop('disabled', false);  
//        } else {
//            $('#submit').prop('disabled', 'disabled');
//        }
//    });

     $.validator.addMethod("min_limt", function (value, element) {
        var flag=false;
        var TempAmount = parseInt(value);
        if(TempAmount < min_matm_amt){
            flag = false;   
        }else{
            flag = true;    
        }
        return flag;
    }, 'Please enter amount greater than or equal to '+min_matm_amt);    

 }); 
    



</script>
