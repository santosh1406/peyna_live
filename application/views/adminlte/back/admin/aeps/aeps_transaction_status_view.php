<section class="content">   
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header "> 
                    <h3 class="box-title">Transaction Status</h3>
                </div>                
                <div class="box-body" id="table_show">
                    <?php
                        if ($this->session->tempdata('msg')) {
                            ?>
                            <div class="alert alert-success alert-dismissable" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->tempdata('msg'); ?>
                            </div>
                           
                        
                    <?php
                        }
                    $attributes = array('class' => 'form', 'id' => 'trans_status', 'name' => 'trans_status', 'method' => 'post',);
                    echo form_open(site_url() . '/admin/aeps/transaction_process', $attributes);
                    ?>
                    <div class="row box-title">

                        <div class="col-sm-3 col-xs-3">
                            <div class="inputBox">
                                <input type="text" id="ClientUniqueID" name="ClientUniqueID" class="form-control" placeholder="Please Enter Transaction Number"/>
                                
                            </div>
                        </div>
                        
                        <div class="col-sm-3 col-xs-3">
                            <div class="inputBox">                                
                                <label  id="status"></label>
                            </div>
                        </div>

                    </div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <?php echo validation_errors('<div class="error">', '</div>'); ?>
                    <div class="row">
                        <div style="display: block;" class="col-md-1 display-no">
                            <div style="position: static;" class="form-group">
                                <input type="hidden" name="service_id" id="service_id" value="<?php echo $SERVICEID ?>">
                                <input type="hidden" name="Amount" id="Amount" value="<?php echo '0' ?>">                                
                                <button class="btn btn-primary" id="submit"  type="submit">Submit</button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>

                </div>
            </div>
        </div>
    </div>
</section>
</div>

<!-- Signin Content End -->             
<!--/div-->
<!-- Inner Wrapper End -->

<script type="text/javascript">
    $(document).ready(function() {
        $('#submit').prop('disabled', true);
        $('#trans_status').validate({
            rules: {
                ClientUniqueID: {
                    required: true,
                    //number:true,

                },
            },
            messages: {
                ClientUniqueID: {
                    required: "Please Enter Transaction Number",
                    // valid_amt:"Please Enter Valid Amount",

                },
            },
            submitHandler: function(form) {
                // submit.form();
                form.submit();
            }
        });
   
    $("#ClientUniqueID").change(function() {
    var client_id = $("#ClientUniqueID").val();
    
        $("#status").html('<img style="width:17px;" src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" align="absmiddle">&nbsp;Checking availability...');
        $.ajax({ 
        type: "POST", 
        url: BASE_URL + "admin/aeps/chk_client_ref_id",
        data: "ClientUniqueID="+ client_id,
        dataType: 'text',
            success: function(msg){
                if(msg == 'This is valid transaction no.'){
                alert (msg);
                $('#submit').prop('disabled', false);
                $("#status").html('&nbsp;<img src="<?php echo base_url() . BACK_ASSETS ?>img/verify.png" align="absmiddle">');
                
                }
                else {
                   alert (msg);
                   
                   $("#status").html(msg);
                   $('#ClientUniqueID').val("");
               }
            }

        });
     
    });
});
</script>
