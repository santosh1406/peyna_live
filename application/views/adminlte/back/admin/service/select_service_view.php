<!-- <style type="text/css">
    .create_btn{
        pointer-events: none;
    }
</style> -->
<section class="content">
    <div id="menu_wrapper">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Service List</h3>
                    </div>
                    <!-- edited by harshada kulkarni on 12-07-2018 -->
                    <div align="left"><a href="<?php echo site_url() . '/admin/service/create_service' ?>" class="btn btn-warning create_btn" role="button" style="margin-top: 5px;">Create New Service</a>   
                    </div>
                    <!--<div align="left">
                        <a href="" class="btn btn-warning" role="button" style="margin-top: 5px;" disabled>Create New Service
                        </a>   
                    </div>-->
                    <div class="box-body table-responsive" style="min-height: 0.01%;overflow-x: auto;">
                        <table id="service_display" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="20px;">Sr No</th>
                                    <th>Service Name</th>
                                    <th width="189px;">Action</th>
                                </tr>
                            </thead>
                            <tbody>                                           

                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </div>
    <div class="modal fade popup" id="add_edit_popup" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Service</h4>
                </div>

                <?php
                $attributes = array("method" => "POST", "name" => "service_edit_form", "id" => "service_edit_form");
                echo form_open(base_url() . 'admin/service/update_service', $attributes);
                ?>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Service Name:</label>
                        <input name="id" type="hidden" id="id" class="form-control"/>
                        <input name="service_name" type="text" id="service_name" class="form-control required" placeholder="Role Name" data-rule-required="true" data-rule-fullname="true" data-msg-required="Please enter Service name" required/>
                    </div>
                    <div class="form-group">
                        <label for="link">Service Detail:</label>
                        <textarea class="form-control" name="service_detail" id="service_detail" cols="50" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="link">Service Detail:</label>
                        <select name="providers[]" class="form-control select2" title="Please select something!" multiple="multiple">
                              <option value ="">Select Providers</option>
                              <?php foreach($provider_array as $provider){ ?>
                                  <option value="<?php echo $provider->id;?>"><?php echo $provider->provider_name;?></option>
                              <?php } ?>
                          </select> 
                    </div>
                </div>

                <div class="modal-footer clearfix">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                    <button type="submit" id="submit_btn" class="btn btn-primary pull-left"></i> Update Service</button>
                </div>
                <?php
                form_close();
                ?>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</section>
<script>
    $(document).ready(function() {
        var select2 = $('.select2').select2({
            width: '100%'
        });

        var tconfig = {
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": BASE_URL + "admin/service/get_list",
                "type": "POST",
                "data": "json",
                data  : function(d) {
                    d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;
                }
            },
            "iDisplayLength": 10,
            "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
            "paging": true,
            "searching": true,
            "aoColumnDefs": [
                {"bSearchable": false, "aTargets": [2]}
            ],
            "order": [[0, 'desc']],
            "oLanguage": {
                "sProcessing": '<center><img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/></center>'
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                $("td:first", nRow).html(iDisplayIndex + 1);

            },
        };

        var oTable = $('#service_display').dataTable(tconfig);

        $(document).off('click', '.edit_btn').on('click', '.edit_btn', function(e) {
            e.preventDefault();
            var id = $(this).attr('ref');

            var detail = {};
            var div = "";
            var ajax_url = 'admin/service/edit_service/';
            var form = '';

            detail['id'] = id;

            get_data(ajax_url, form, div, detail, function(response)
            {
                if (response.flag == '@#success#@')
                {
                    service_data = response.service_data;
                    $("#service_edit_form #id").val(service_data.id);
                    $("#service_edit_form #service_name").val(service_data.service_name);
                    $("#service_edit_form #service_detail").val(service_data.service_detail);
                    select2.val(response.provider_selected_array).trigger('change');
                    $('#add_edit_popup').modal('show');
                }
                else
                {
                    alert(response.msg);
                }
            }, '', false);
        });

    });
</script>
