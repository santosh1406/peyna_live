
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3>
            &nbsp;Create New Service
             <!--<small>advanced tables</small>-->
        </h3s>
       
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                       <?php
                            $attributes = array("method" => "POST", "id" => "service_create_form", "class" => "service_create_form");
                            echo  form_open(site_url().'/admin/service/save_service', $attributes);
                       ?>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="name">Service Name</label>
                            <div class="col-lg-6">
                                <input name="service_name" type="text" id="service_name" class="form-control" value="" required="">
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="name">Service Description</label>
                            <div class="col-lg-6">
                                <textarea name="service_detail" id="service_detail" class="form-control" ></textarea>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="name">Assign Providers</label>
                            <div class="col-lg-6">
                            <select name="providers[]" class="form-control select2" title="Please select something!" multiple="multiple">
                              <option value ="">Select Providers</option>
                              <?php foreach($provider_array as $provider){ ?>
                                  <option value="<?php echo $provider->id;?>"><?php echo $provider->provider_name;?></option>
                              <?php } ?>
                          </select> 
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                    </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">
                                <button class="btn btn-primary" id="save_group_data" name="save_group_data" type="submit">Save</button> 
                                <button class="btn btn-primary back" id="back_data" type="button">Back</button> 
                            </div>
                        </div>

                         </form>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<script>

    $(document).ready(function() {
        $('.select2').select2();

        $(document).off('click', '.back').on('click', '.back', function(e)
        {
            window.location.href = BASE_URL + 'admin/service/';
        });

        $('#service_create_form').validate({
            rules : {
                    service_name : {
                        'required' : true ,
                        'is_service_exists' : true
                    }
                } ,
            messages : {
                        service_name : {
                            'required' : 'Please enter service name',
                            'is_service_exists' : 'Service name is already exists'
                        }
                    },
            errorPlacement : function(error, element) {
                error.appendTo(element.parent('div'));
            }

        });

        $.validator.addMethod("is_service_exists", function(value,element) {
            var flag;

            $.ajax({
                url : BASE_URL + "admin/service/is_service_exists",
                data : {name :value},
                type : 'POST',
                async : false,
                success : function(r) {
                    flag = (r === 'true') ? false : true;
                } 
                
            });
            return flag;
        }, "Service name already exists"); 
 
    });
</script>