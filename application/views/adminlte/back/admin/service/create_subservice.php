
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3>
            &nbsp;Create New Subservice
             <!--<small>advanced tables</small>-->
        </h3>
       
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                       <?php
                            $attributes = array("method" => "POST", "id" => "service_create_form", "class" => "service_create_form");
                            echo  form_open(site_url().'/admin/subservice/save_subservice', $attributes);
                       ?>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="name">Name *:</label>
                            <div class="col-lg-6">
                                <input name="service_name" type="text" id="service_name" class="form-control" value="" required="" maxlength="30" minlength="2">
                            </div>
                        </div>
                           <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="name">Service Type *:</label>
                            <div class="col-lg-6">
                                <select name="types" id="types" class="form-control">
                                    <option value ="">Please select a service type</option>
                                    <?php foreach ($types as $type) {
                                    ?>
                                    <option value="<?php echo $type['type'] ?>"><?php echo $type['type'] ?></option>
                                    <?php
                                    } 
                                    ?>
                                </select>
                            </div>
                        </div>
                         <div class="form-group" id="service" style="display: none">
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <label class="col-lg-3 control-label" for="name">Service *:</label>
                            <div class="col-lg-6">
                                <select name="service" id="services" class="form-control">
                                    <option value ="">Please select a service</option>
                                    <?php foreach ($services as $service) {
                                    ?>
                                    <option value="<?php echo $service['id'] ?>"><?php echo $service['service_name'] ?></option>
                                    <?php
                                    } 
                                    ?>
                                </select>
                            </div>
                        </div>  
                         <div class="form-group" id="aggregator" style="display:none">
                             <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <label class="col-lg-3 control-label" for="name">Select aggregator *:</label>
                            <div class="col-lg-6">
                                <select name="provider_id" class="form-control">
                                    <option value ="">Please select aggregator</option>
                                    <?php foreach ($providers as $provider) {
                                    ?>
                                    <option value="<?php echo $provider->id ?>"><?php echo $provider->provider_name ?></option>
                                    <?php
                                    } 
                                    ?>
                                </select>
                            </div>
                        </div> 
                         
                        <div class="form-group" id="lower_slab" style="display: none">
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <label class="col-lg-3 control-label" for="name">Lower Slab *:</label>
                            <div class="col-lg-6">
                                <input type="text" name="lower_slab" id="lower_slab" class="form-control" value="" required="" maxlength="4">
                            </div>
                        </div>
                         
                        <div class="form-group" id="upper_slab" style="display: none">
                             <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <label class="col-lg-3 control-label" for="name">Upper Slab *:</label>
                            <div class="col-lg-6">
                                <input type="text" name="upper_slab" id="upper_slab" class="form-control" value="" required="" maxlength="10">
                            </div>
                        </div>
                          
                        <div class="form-group" id="cust_charge" style="display: none">
                             <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <label class="col-lg-3 control-label" for="name">Customer Charge *:</label>
                            <div class="col-lg-6">
                                <input type="text" name="cust_charge" id="cust_charge" class="form-control" value="" required="" maxlength="4">
                            </div>
                        </div>
                        <!-- added by sonali 05-march-2019 @micro atm service -->
                        <div id="fino_caped_per" style="display: none">   
                            <div class="form-group" >
                                 <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <label class="col-lg-3 control-label" for="name">BOS Caped % *:</label>
                                <div class="col-lg-6">
                                    <input type="text" name="bos_caped_per" id="bos_caped_per" class="form-control" value="" required="" >
                                </div>
                            </div>
                            
                            <div class="form-group" >
                                 <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <label class="col-lg-3 control-label" for="name">BOS Caped Rupees *:</label>
                                <div class="col-lg-6">
                                    <input type="text" name="bos_caped_rupees" id="bos_caped_rupees" class="form-control" value="" required="" >
                                </div>
                            </div>
                            
                            <div class="form-group" >
                                 <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <label class="col-lg-3 control-label" for="name">Agent Caped % *:</label>
                                <div class="col-lg-6">
                                    <input type="text" name="agent_caped_per" id="agent_caped_per" class="form-control" value="" required="" >
                                </div>
                            </div>
                            
                            <div class="form-group" >
                                 <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <label class="col-lg-3 control-label" for="name">Agent Caped Rupees *:</label>
                                <div class="col-lg-6">
                                    <input type="text" name="agent_caped_rupees" id="agent_caped_rupees" class="form-control" value="" required="" >
                                </div>
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                    </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">
                                <button class="btn btn-primary" id="save_group_data" name="save_group_data" type="submit">Save</button> 
                                <button class="btn btn-primary back" id="back_data" type="button">Back</button> 
                            </div>
                        </div>

                         </form>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<script>

    $(document).ready(function() {
        $(document).off('click', '.back').on('click', '.back', function(e)
        {
            window.location.href = BASE_URL + 'admin/subservice/';
        });

        $('#service_create_form').validate({
            rules : {
                    service_name : {
                        'required' : true ,
                        'is_service_exists' : true
                    },
                     provider_id : {
                        'required' : true 
                    },
                     types : {
                        'required' : true
                    },
                     service : {
                        'required' : true 
                    },
                    lower_slab : {
                        'required' : true,
                         'number' : true,
                        'min':1
                    },
                     upper_slab : {
                        'required' : true,
                         'number' : true,
                        'min':1

                    },
                     cust_charge : {
                        'required' : true ,
                         'number' : true,
                         'min':1
                    }
                } ,
            messages : {
                        service_name : {
                            'required' : 'Please enter service name',
                            'is_service_exists' : 'Sub service name is already exists'
                        },
                         lower_slab : {
                              'min':'Please enter value greater than 0'
                         },
                         upper_slab : {
                              'min':'Please enter value greater than 0'
                         },
                          cust_charge : {
                              'min':'Please enter value greater than 0'
                         }

                       
                    },
            errorPlacement : function(error, element) {
                error.appendTo(element.parent('div'));
            }

        });

$.validator.addMethod('minStrict', function (value, el, param) {
    return value > param;
});
         $.validator.addMethod("letterswithspecialchar", function (value, element)
        {
            return this.optional(element) || /^[a-zA-Z0-9-\s]+$/i.test(value);
        }, "");

        $.validator.addMethod("is_service_exists", function(value,element) {
            var flag;

            $.ajax({
                url : BASE_URL + "admin/subservice/is_subservice_exists",
                data : {name :value},
                type : 'POST',
                async : false,
                success : function(r) {
                    flag = (r === 'true') ? false : true;
                } 
                
            });
            return flag;
        }, "Service name already exists"); 
 
    });
     $(document).off('change', '#types').on('change', '#types', function(e){
        var service_type = $(this).val().toLowerCase();
        $('select#services option').removeAttr("selected");
      if(service_type == 'dmt' || service_type == 'bbps' ){
            $('#lower_slab').css('display', 'block');
            $('#upper_slab').css('display', 'block');
            $('#cust_charge').css('display', 'block');
             $('#aggregator').css('display', 'none');
            $('#service').css('display', 'none');
            $('#fino_caped_per').css('display','none');
        }else if(service_type == 'prepaid' || service_type == 'postpaid' || service_type == 'dth' || service_type == 'datacard' ){
            
            $('#lower_slab').css('display', 'none');
            $('#upper_slab').css('display', 'none');
            $('#cust_charge').css('display', 'none');
            $('#aggregator').css('display', 'block');
            $('#service').css('display', 'block');
            $('#fino_caped_per').css('display','none');
            if(service_type == 'prepaid'){
             //$("#service option[value!='5']").remove();
             //$("#services option[value=5]").attr('selected', 'selected');
             $('select[name^="services"] option[value=5]').attr("selected","selected");
             }else if(service_type == 'postpaid'){
                //$("#services option[value=6]").attr('selected', 'selected');
                 $('select[name^="services"] option[value=6]').attr("selected","selected");
            }else if(service_type == 'dth'){
                //$("#services option[value=3]").attr('selected', 'selected');
                 $('select[name^="services"] option[value=3]').attr("selected","selected");
            }else if(service_type == 'datacard'){
                //$("#services option[value=12]").attr('selected', 'selected');
                 $('select[name^="services"] option[value=12]').attr("selected","selected");
                $("#aggregator option[value=2]").attr('selected', 'selected');
                $('#aggregator').css('display', 'none');
            }
        }else if(service_type == 'micro_atm'){
           
            $('#fino_caped_per').css('display','block');
            $('#service').css('display', 'block');
            $('#lower_slab').css('display', 'none');
            $('#upper_slab').css('display', 'none');
            $('#cust_charge').css('display', 'none');
            $('#aggregator').css('display', 'none');
            $("#service option[value=17]").attr('selected', 'selected');
            
            
        }else if(service_type == 'aeps'){
           
            $('#fino_caped_per').css('display','block');
            $('#service').css('display', 'block');
            $('#lower_slab').css('display', 'none');
            $('#upper_slab').css('display', 'none');
            $('#cust_charge').css('display', 'none');
            $('#aggregator').css('display', 'none');
            $("#service option[value=20]").attr('selected', 'selected');
            
        }else if(service_type=='smartcard'){
            $('#service').css('display', 'block');
            $('#lower_slab').css('display', 'none');
            $('#upper_slab').css('display', 'none');
            $('#cust_charge').css('display', 'none');
            $('#aggregator').css('display', 'none');
            $("#service option[value=11]").attr('selected', 'selected');
        }

    }); 
   
</script>
