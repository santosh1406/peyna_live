<!--Inner Wrapper Start -->
<link href="<?php echo base_url() . BACK_ASSETS ?>css/hotel_style/style.css" rel="stylesheet" type="text/css" />
<div id="page-wrapper">

    <!-- Signin Content Start -->
   <section class="inner-wrapper">
        <div class="container">
            <div class="row">                
                <?php
                if ($this->session->tempdata('msg')) {
                            ?>
                            <div class="alert alert-success alert-dismissable" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->tempdata('msg'); ?>
                            </div>
                            <?php
                        }
                      ?>
                          	
              <div class="col-md-6 col-md-offset-3">
                    <h3 class="text-center purple">Hotel Booking</h3>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-12 col-sm-12 container-c">
                                    <div class="formBox">
                                    <form action="<?php echo base_url()?>admin/search_hotel/hotel_services" method="POST" id="hotel_search" name="hotel_search" accept-charset="utf-8">                              
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                    <div class="selectBox">
                                                    <div class="inputText">Destination</div>
                                                    <select class="input selectText" id="city" name="city">
                                                        <option value=""></option>
                                                        <?php foreach ($cityList as $key => $value) { 

                                                          echo '<option value='.$value['city_id'].'>'.$value['name'].'</option>';
                                                          ?>

                                                      <?php } ?>
                                                    </select>
                                                    <div class="error" id="erDatepicker"></div>
                                                    <?php echo form_error('city', '<div class="error">', '</div>'); ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="inputBox cinputBox">
                                                    <div class="inputText">Check In</div>
                                                    <input id="datepicker"  name="check_in_date" class="input" type="text" value="<?php echo date('m/d/Y')?>">
                                                    <div class="error" id="erDatepicker"></div>
                                                    <?php echo form_error('check_in_date', '<div class="error">', '</div>'); ?>
                                                </div>                                                    
                                            </div>

                                            <div class="col-sm-6 col-xs-12">
                                                    <div class="inputBox cinputBox">
                                                    <div class="inputText">Check Out</div>
                                                    <input id="datepicker1" name="check_out_date" class="input" type="text" value="<?php echo  date('m/d/Y', strtotime(' +1 day'))?>" >
                                                    <div class="error" id="erDatepicker1"></div>
                                                    <?php echo form_error('check_out_date', '<div class="error">', '</div>'); ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <!-- <div class="col-sm-4 col-xs-12">
                                                <div class="selectBox">
                                                    <div class="inputText">Rooms</div>
                                                    <select class="input selectText">
                                                      <option value=""></option>
                                                      <option>1</option>
                                                      <option>2</option>
                                                      <option>3</option>
                                                      <option>4</option>
                                                      <option>5</option>
                                                      <option>6</option>
                                                      <option>7</option>
                                                      <option>8</option>
                                                      <option>9</option>
                                                      <option>10</option>
                                                    </select>
                                                </div>
                                                

                                            </div>-->

<!--                                            <div class="col-sm-4 col-xs-12">
                                                    <div class="selectBox">
                                                    <div class="inputText">Adults</div>
                                                    <select class="input selectText">
                                                      <option value=""></option>
                                                      <option>1</option>
                                                      <option>2</option>
                                                      <option>3</option>
                                                      <option>4</option>
                                                      <option>5</option>
                                                      <option>6</option>
                                                      <option>7</option>
                                                      <option>8</option>
                                                      <option>9</option>
                                                      <option>10</option>
                                                    </select>
                                                </div>
                                            </div>-->

<!--                                            <div class="col-sm-4 col-xs-12">
                                                    <div class="selectBox">
                                                    <div class="inputText">Childrens</div>
                                                    <select class="input selectText">
                                                      <option value=""></option>
                                                      <option>1</option>
                                                      <option>2</option>
                                                      <option>3</option>
                                                      <option>4</option>
                                                      <option>5</option>
                                                      <option>6</option>
                                                      <option>7</option>
                                                      <option>8</option>
                                                      <option>9</option>
                                                      <option>10</option>
                                                    </select>
                                                </div>
                                            </div>-->
                                           
                                           
                                            <table class="table table-bordered" id="item_table">
                                            <tr>
                                                <td>
                                                    <button type="button" name="add" id="add_doc_btn" class="btn btn-success btn-sm add"><span>Add Rooms</span></button>
                                                </td>
                                            </tr>
                                            </table>


                                            
                                           
                                        </div>

<!--                                        <div class="row">
                                            <div class="col-sm-6 col-xs-12">
                                                    <div class="selectBox">
                                                    <div class="inputText">Nationality</div>
                                                    <select class="input selectText">
                                                      <option value=""></option>
                                                      <option>India</option>
                                                      <option>United States of America</option>
                                                      <option>United Kingdom</option>
                                                      <option>France</option>
                                                      <option>China</option>
                                                      <option>Cananda</option>
                                                      <option>Italy</option>
                                                      <option>Germany</option>
                                                      <option>Other</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-xs-12">
                                                    <div class="selectBox">
                                                    <div class="inputText">Residence</div>
                                                    <select class="input selectText">
                                                      <option value=""></option>
                                                      <option>India</option>
                                                      <option>United States of America</option>
                                                      <option>United Kingdom</option>
                                                      <option>France</option>
                                                      <option>China</option>
                                                      <option>Cananda</option>
                                                      <option>Italy</option>
                                                      <option>Germany</option>
                                                      <option>Other</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>-->

                                        <div class="row">
                                            <div class="col-sm-4 col-xs-12 col-sm-offset-4">
                                                <input type="hidden" name="city_value" id="city_value" value=""/>
                                                <button type="submit" id="search_hotel" class="btn btn-purple btn-lg btn-block login-button txt-w col-sm-4">Search Hotels</button>
                                            </div>
                                        </div>
                                    </form>
                                </div> 
                            </div>
                        </div>
                    </div>
              </div>
            </div>
        </div>
    </section>


    <!-- Signin Content End -->             
</div>
<!-- Inner Wrapper End-->

<script>
    $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
        input.attr("type", "text");
            } else {
        input.attr("type", "password");
  	}
    });
</script>
<script>
	//jQuery('#qrcode').qrcode("this plugin is great");
	/*jQuery('#qrcodeCanvas').qrcode({
		text	: "http://jetienne.com"
	});	*/
</script>
<!--<script src="https://code.jquery.com/jquery-3.2.1.js"></script>-->
<script type="text/javascript">
       $(".input").focus(function() {
               $(this).parent().addClass("focus");
       });
</script>
         
<script>    
    $("#datepicker").datepicker({
        numberOfMonths: 2,
        minDate: 0,
    });
    $("#datepicker1").datepicker({numberOfMonths: 2, minDate: 0});    
</script>
<script>
    var x = 0;
    $(document).on('click', '.add', function(){
        x++;       
        //var rowCount = $('#item_table tr').length;     
        var rowCount = x;

        console.log(rowCount);
        var html = '';
        html += '<tr>';

        html += '<td><div class="bui-stepper" data-bui-component="InputStepper"><b>Room'+x+'</b><div class="bui-stepper__title-wrapper"><label class="bui-stepper__title" for="group_adults">Adults</label></div><div class="bui-stepper__wrapper"><input  id="show_adult_count'+rowCount+'" type="hidden" name="room['+'Room'+x+'][group_adults][]" min="1" max="30" value="1" ><button class="bui-button bui-button--secondary bui-stepper__subtract-button" data-bui-ref="input-stepper-subtract-button" type="button" aria-hidden="true" data-person="adult" data-room="" onclick="decrement(this,'+rowCount+')"><span class="bui-button__text">-</span></button><span class="bui-stepper__display" data-bui-ref="input-stepper-value" aria-hidden="true" id="adult_span'+rowCount+'">1</span><button class="bui-button bui-button--secondary bui-stepper__add-button" data-bui-ref="input-stepper-add-button" type="button" aria-hidden="true" data-person="adult" data-room="" onclick="increment(this,'+rowCount+')"><span class="bui-button__text"  >+</span></button></div></div>';

        html += '<div class="bui-stepper" data-bui-component="InputStepper"><div class="bui-stepper__title-wrapper"><label class="bui-stepper__title" for="group_children">Children</label></div><div class="bui-stepper__wrapper"><input type="hidden"  id="show_childrens_count'+rowCount+'" name="room['+'Room'+x+'][group_children][]" min="0" max="10" value="0" > <button class="bui-button bui-button--secondary bui-stepper__subtract-button  remove_child" data-bui-ref="input-stepper-subtract-button" type="button" data-person="childrens" data-room="Room'+rowCount+'" onclick="decrement(this,'+rowCount+')"><span class="bui-button__text">-</span></button><span class="bui-stepper__display" data-bui-ref="input-stepper-value" aria-hidden="true" id="childrens_span'+rowCount+'">0</span><button class="bui-button bui-button--secondary bui-stepper__add-button add1" data-bui-ref="input-stepper-add-button" type="button" aria-hidden="true" data-person="childrens" data-room="Room'+rowCount+'" onclick="increment(this,'+rowCount+')"> <span class="bui-button__text ">+</span></button></div></div>';


        html += '<table class="table table-bordered" id="item_table'+rowCount+'"><tr></tr> </table></td>';


        html += '<td><button type="button" name="remove" class="btn btn-danger btn-sm remove"><span class="glyphicon glyphicon-minus"></span></button></td></tr>';
        $('#item_table').append(html);
    });

    $(document).on('click', '.remove', function(){
        var rowCount = $('#item_table tr').length;  
        $(this).closest('tr').remove();
    });
        
    function increment(obj,count){
        var person= $(obj).attr('data-person');
        var room= $(obj).attr('data-room');
        //alert(room);
        //alert(rowcount);
        console.log(room);

        var show_count='';
        var span='';
        if(person == 'adult'){
            
          show_count='show_adult_count'+count;
          span='adult_span'+count;
          
          console.log(show_count);

        }else if(person == 'childrens'){
            show_count='show_childrens_count'+count;
            span='childrens_span'+count;
            console.log(show_count);

            addChidrenAge(count,room);

        }else if(person == 'rooms'){
            show_count='show_rooms_count';
            span='rooms_span';
            
            console.log(show_count);
        }


        var value = parseInt(document.getElementById(show_count).value, 10);
        value = isNaN(value) ? 0 : value;
        value++;
        document.getElementById(show_count).value = value;
        $('#'+span).text(value);
    
    }

    function decrement(obj,count){
        var person= $(obj).attr('data-person');
        var room= $(obj).attr('data-room');
        //alert(room);
        //alert(person);
          var show_count='';
          var span='';
        if(person == 'adult'){
          show_count='show_adult_count'+count;
          span='adult_span'+count;


        }else if(person == 'childrens'){
            show_count='show_childrens_count'+count;
                  span='childrens_span'+count;
                   removeChidrenAge(count,room);


        }else if(person == 'rooms'){
            show_count='show_rooms_count';
            span='rooms_span';


        }
        var value = parseInt(document.getElementById(show_count).value, 10);
        value = isNaN(value) ? 0 : value;
        if(value > 0){
        value--;
        document.getElementById(show_count).value = value;
        $('#'+span).text(value);


        }
    }

    function addChidrenAge(count,room){

        //name="room['+x+'][group_adults][]"
             var rowCount = $('#item_table'+count+' tr').length;
          //alert(rowCount);


        if(rowCount<=10){
          var html = '';
        html += '<tr id="'+rowCount+''+room+'">';

        html += '<td><label class="sb-searchbox__label -small sb-group__children__label">How old are the children your traveling with?</label>';

        html += '<td><select name="room['+room+'][childrens_age][]" data-group-child-age="0" aria-label="Child 1 age"> <option selected="" class="sb_child_ages_empty_zero" value="12 ">Age at check-out</option><option value="0">0 years old</option> <option value="1"> 1 year old</option><option value="2">2 years old</option><option value="3">3 years old</option><option value="4"> 4 years old</option><option value="5">5 years old</option><option value="6"> 6 years old </option>    </select>';

        html += '</tr>';
        $('#item_table'+count).append(html);
      }else{

        //alert('Not Allowed more than 3');
        //$("#add_doc_btn").prop("disabled", true);
      }



    }

    function removeChidrenAge(count,room){

        var row=$("#show_childrens_count"+count).val();
        if(row>=0){
        var t=parseInt(row);

        $('#'+t+''+room).remove();

        }else{

        $('#show_childrens_count'+count).val(0);

        $('#childrens_span'+count).text(0);
        }

    }
</script>
<script>         
    $(document).ready(function() {
       
        $('#city').change(function() {
            var city = "";
            city = $('#city').val();
            $("#city_value").val(city);
            console.log($("#show_adult_count").val());
            console.log(city);		
	});

       
       
        $("body").on('click', '#search_hotel', function(){
            var getCity = "";
            getCity = $('#city_value').val();
            console.log(getCity);
  
            var check_in_date = $("#datepicker").val();
            var check_out_date = $("#datepicker1").val();
  
    
            if(check_in_date === '' && check_out_date ==='' && getCity === '') {
                alert('Please fill out all required fields');
                    return false;
            }
        
        
        });
    });
</script>
    


