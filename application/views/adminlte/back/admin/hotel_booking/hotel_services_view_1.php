<div class="col-sm-4 col-md-2">
    <h4 class="search-results-title">
        <i class="glyphicon glyphicon-search mr10 icon-search"></i>
        <b><span id="total-result-found"><?php 
         //var_dump($searchId);die;
        echo count($result->hotelResults); ?></span></b> results found.
    </h4>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <i class="short-full glyphicon glyphicon-plus"></i>
                        Fare
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse price-filter" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                <div class="panel-body">
                    <span><?php echo $min_fare?></span> <span style="float:right"><?php echo $max_fare?></span> 
                    <input id="range_slider" type="range" min="<?php echo $min_fare?>" max="<?php echo $max_fare?>" value="<?php echo $min_fare?>"  />  
                    
                </div>
                    <input id="ex1" data-slider-id="ex1Slider" type="text" data-slider-min="-5" data-slider-max="20" data-slider-step="1" data-slider-value="14" style="display: none;" data-value="11" value="11">
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <i class="short-full glyphicon glyphicon-plus"></i>
                        Travels
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                    <form action="" method="get">
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="MSRTC" type="checkbox" value=""> <a href="#">MSRTC</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">Neeta Tours and Travels</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        <i class="short-full glyphicon glyphicon-plus"></i>
                        Bus Type
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                    <form action="" method="get">
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="MSRTC" type="checkbox" value=""> <a href="#">Reclined [Seats]</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">Non AC Seater</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">AC Semi Sleeper</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">AC Sleeper</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">AC Seater</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">Others</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingFour">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        <i class="short-full glyphicon glyphicon-plus"></i>
                        Departure Time
                    </a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                <div class="panel-body">
                    <form action="" method="get">
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="MSRTC" type="checkbox" value=""> <a href="#">00:00 - 03:59(Mid Night)</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">04:00 - 11:59 (Morning)</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">12:00 - 15:59 (Afternoon)</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">16:00 - 18:59 (Evening)</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">19:00 - 23:59 (Night)</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingFive">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                        <i class="short-full glyphicon glyphicon-plus"></i>
                        Duration
                    </a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                <div class="panel-body">
                    <form action="" method="get">
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="MSRTC" type="checkbox" value=""> <a href="#">Upto 6 Hours</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">6 Hours - 12 Hours</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">12 Hours - 24 Hours</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">More than a day</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div><!-- panel-group -->
</div>

<div class="col-sm-8 col-md-10">
    <div class="sort-by-section clearfix mb10">
        <h4 class="sort-by-title block-sm">Sort results by:</h4>
        <ul class="sort-bar clearfix">
            <li class="sort-by-name sort_col" ref="data-travel" data-sort="asc"><a class="sort-by-container" href="#"><span>Travels</span></a></li>
            <li class="sort-by-depature sort_col" ref="data-dep_tm_str" data-sort="asc"><a class="sort-by-container" href="#"><span>Depature</span></a></li>
            <li class="sort-by-seats sort_col" ref="data-seat" data-sort="asc"><a class="sort-by-container" href="#"><span>Seats</span></a></li>
            <li class="sort-by-fare sort_col" ref="data-fare" data-sort="asc"><a class="sort-by-container" href="#"><span>Fare</span></a></li>
        </ul>
    </div>

    <div class="bus-service-list">
        <div class="state" id="services_view_render"> 
            <?php 
            if (count($hotelList) > 0) : 
                foreach ($hotelList as $key => $hotelDetails) {?>
            <div class="panel panel-default">
                <div class="boxstyle1" role="tab">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <!-- <i class="adddetails glyphicon glyphicon-plus"></i> -->
                                <h4 class="service-title">
                                    <span class="service-title-main clearfix"><?php echo $hotelDetails['name']?></span>
                                    <small>Total Bus - <span id="totalBusCnt"><?php echo $hotelDetails['address']?> </span></small>
                                </h4>
                            </a>
                        </h4>
                        <input type="hidden" id="hotel_id" value="<?php echo $hotelDetails['id']?>">
                        <input type="button" class="btn btn-primary hotel_rooms" id="<?php echo $hotelDetails['id']?>" value="Select Rooms" style="position: relative;left: 4px;bottom: 7px;"> 
                </div>

            </div>
            <div class="panel panel-default" id="room-list" style="display:none">
               <h1>Hiiiii</h1>
            </div>
             <?php }
                ?>
            <?php else: ?>
            <div class="panel panel-default">
                <h3>No Services Found.</h3>
            </div>
            <?php endif; ?>

        </div>  
    </div>
<script type="text/javascript">
$(document).ready(function(){ 

    var url = base_url +"admin/Search_hotel/get_rooms";

    $(".hotel_rooms").on('click', function(){
    var hotel_id = $(this).attr('id');
    var searchId = '<?php echo $searchId;?>';
    $.ajax({
                type:"POST",
                dataType: "json",
                url : url,
                data:{"hotelIds":hotel_id,"searchId":searchId},
                success : function(data) { 
                    console.log(data);

                }

            });
    $( "#room-list" ).toggle();
     });




});

</script>
    <!-- End -->