<!--Inner Wrapper Start -->
<link href="<?php echo base_url() . BACK_ASSETS ?>css/hotel_style/style.css" rel="stylesheet" type="text/css" />
<div id="page-wrapper">

    <!-- Signin Content Start -->
   <section class="inner-wrapper">
        <div class="container">
            <div class="row">
              <div class="col-md-6 col-md-offset-3">
                    <h3 class="text-center purple">Hotel Booking</h3>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-12 col-sm-12 container-c">
                                    <div class="formBox">
                                    <form>                                
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                    <div class="selectBox">
                                                    <div class="inputText">Destination</div>
                                                    <select class="input selectText" id="city" name="city">
                                                        <option value=""></option>
                                                        <?php foreach ($cityList as $key => $value) { 

                                                          echo '<option value='.$value['city_id'].'>'.$value['name'].'</option>';
                                                          ?>

                                                      <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="inputBox cinputBox">
                                                    <div class="inputText">Check In</div>
                                                    <input id="datepicker"  name="check_in_date" class="input" type="text">
                                                    <div class="error" id="erDatepicker"></div>
                                                    <?php echo form_error('check_in_date', '<div class="error">', '</div>'); ?>
                                                </div>                                                    
                                            </div>

                                            <div class="col-sm-6 col-xs-12">
                                                    <div class="inputBox cinputBox">
                                                    <div class="inputText">Check Out</div>
                                                    <input id="datepicker1" name="check_out_date" class="input" type="text" >
                                                    <div class="error" id="erDatepicker1"></div>
                                                    <?php echo form_error('check_out_date', '<div class="error">', '</div>'); ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                           <!-- <div class="col-sm-4 col-xs-12">
                                                <div class="selectBox">
                                                    <div class="inputText">Rooms</div>
                                                    <select class="input selectText">
                                                      <option value=""></option>
                                                      <option>1</option>
                                                      <option>2</option>
                                                      <option>3</option>
                                                      <option>4</option>
                                                      <option>5</option>
                                                      <option>6</option>
                                                      <option>7</option>
                                                      <option>8</option>
                                                      <option>9</option>
                                                      <option>10</option>
                                                    </select>
                                                </div>
                                                

                                            </div>-->

                                            <div class="col-sm-4 col-xs-12">
                                                    <div class="selectBox">
                                                    <div class="inputText">Adults</div>
                                                    <select class="input selectText">
                                                      <option value=""></option>
                                                      <option>1</option>
                                                      <option>2</option>
                                                      <option>3</option>
                                                      <option>4</option>
                                                      <option>5</option>
                                                      <option>6</option>
                                                      <option>7</option>
                                                      <option>8</option>
                                                      <option>9</option>
                                                      <option>10</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-sm-4 col-xs-12">
                                                    <div class="selectBox">
                                                    <div class="inputText">Childrens</div>
                                                    <select class="input selectText">
                                                      <option value=""></option>
                                                      <option>1</option>
                                                      <option>2</option>
                                                      <option>3</option>
                                                      <option>4</option>
                                                      <option>5</option>
                                                      <option>6</option>
                                                      <option>7</option>
                                                      <option>8</option>
                                                      <option>9</option>
                                                      <option>10</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

<!--                                        <div class="row">
                                            <div class="col-sm-6 col-xs-12">
                                                    <div class="selectBox">
                                                    <div class="inputText">Nationality</div>
                                                    <select class="input selectText">
                                                      <option value=""></option>
                                                      <option>India</option>
                                                      <option>United States of America</option>
                                                      <option>United Kingdom</option>
                                                      <option>France</option>
                                                      <option>China</option>
                                                      <option>Cananda</option>
                                                      <option>Italy</option>
                                                      <option>Germany</option>
                                                      <option>Other</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-xs-12">
                                                    <div class="selectBox">
                                                    <div class="inputText">Residence</div>
                                                    <select class="input selectText">
                                                      <option value=""></option>
                                                      <option>India</option>
                                                      <option>United States of America</option>
                                                      <option>United Kingdom</option>
                                                      <option>France</option>
                                                      <option>China</option>
                                                      <option>Cananda</option>
                                                      <option>Italy</option>
                                                      <option>Germany</option>
                                                      <option>Other</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>-->

                                        <div class="row">
                                            <div class="col-sm-4 col-xs-12 col-sm-offset-4">
                                                <button type="button" class="btn btn-purple btn-lg btn-block login-button txt-w col-sm-4">Search Hotels</button>
                                            </div>
                                        </div>
                                    </form>
                                </div> 
                            </div>
                        </div>
                    </div>
              </div>
            </div>
        </div>
    </section>


    <!-- Signin Content End -->             
</div>
<!-- Inner Wrapper End-->

<script>
	$(".toggle-password").click(function() {
  	$(this).toggleClass("fa-eye fa-eye-slash");
  	var input = $($(this).attr("toggle"));
  	if (input.attr("type") == "password") {
    input.attr("type", "text");
  	} else {
    input.attr("type", "password");
  	}
});
</script>
<script>
	//jQuery('#qrcode').qrcode("this plugin is great");
	/*jQuery('#qrcodeCanvas').qrcode({
		text	: "http://jetienne.com"
	});	*/
</script>
<!--<script src="https://code.jquery.com/jquery-3.2.1.js"></script>-->
<script type="text/javascript">
       $(".input").focus(function() {
               $(this).parent().addClass("focus");
       })
</script>
         
<script>    
    $("#datepicker").datepicker({
        numberOfMonths: 2,
        minDate: 0,
    });
    $("#datepicker1").datepicker({numberOfMonths: 2, minDate: 0});    
</script>


