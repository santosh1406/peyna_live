<!--Inner Wrapper Start -->
<link href="<?php echo base_url() . BACK_ASSETS ?>css/hotel_style/style.css" rel="stylesheet" type="text/css" />
<div id="page-wrapper">

    <!-- Signin Content Start -->
    <section class="inner-wrapper">
                	<div class="container-fluid">
                    	<div class="row">
                        	<div class="col-md-12 col-sm-12">
                            	<h3 class="text-center purple">Search Result</h3>
                                <div class="searchp-form well mb10">
                                    <form class="form-inline mb20">
                                        <div class="form-group">
                                        	<div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                          		<input class="form-control mb10" id="" type="text" placeholder="Destination">
                                          	</div>
                                        </div>
                                        <div class="form-group has-feedback">
                                        	<div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                          		<input type="text" class="form-control mb10" id="" placeholder="Check In">
                                          		<span class="glyphicon glyphicon-calendar form-input-icon icon-calendar"></span>
                                            </div>
                                        </div>
                                        <div class="form-group has-feedback">
                                        	<div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                          		<input type="text" class="form-control mb10" id="" placeholder="Check Out">
                                          		<span class="glyphicon glyphicon-calendar form-input-icon icon-calendar"></span>
                                            </div>
                                        </div>
                                        <div class="form-group has-feedback">
                                        	<div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                                <select id="disabledSelect" class="form-control mb10" placeholder="">
                                                    <option selected="selected" value="Rooms">Rooms</option>
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                    <option>6</option>
                                                    <option>7</option>
                                                    <option>8</option>
                                                    <option>9</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group has-feedback">
                                        	<div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                                  <select id="disabledSelect" class="form-control mb10" placeholder="">
                                                    <option selected="selected" value="Adults 12+ Yrs">Adults</option>
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                    <option>6</option>
                                                    <option>7</option>
                                                    <option>8</option>
                                                    <option>9</option>
                                                  </select>
                                            </div>
                                        </div>
                                        <div class="form-group has-feedback">
                                        	<div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                                  <select id="disabledSelect" class="form-control mb10" placeholder="">
                                                    <option selected="selected" value="Children 2-12 Yrs">Children</option>
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                    <option>6</option>
                                                    <option>7</option>
                                                    <option>8</option>
                                                    <option>9</option>
                                                  </select>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group has-feedback">
                                        	<div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                                  <select id="disabledSelect" class="form-control mb10" placeholder="">
                                                    <option selected="selected" value="Preferred Airlines">Nationality</option>
                                                     <option>India</option>
                                                     <option>United States of America</option>
                                                     <option>United Kingdom</option>
                                                     <option>France</option>
                                                     <option>China</option>
                                                     <option>Canada</option>
                                                     <option>Italy</option>
                                                     <option>Germany</option>
                                                     <option>Other</option>
                                                  </select>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group has-feedback">
                                        	<div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                                  <select id="disabledSelect" class="form-control mb10" placeholder="">
                                                    <option selected="selected" value="Residence">Residence</option>
                                                    <option>India</option>
                                                     <option>United States of America</option>
                                                     <option>United Kingdom</option>
                                                     <option>France</option>
                                                     <option>China</option>
                                                     <option>Canada</option>
                                                     <option>Italy</option>
                                                     <option>Germany</option>
                                                     <option>Other</option>
                                                  </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                        	<div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                        		<button type="submit" id="submit_filter_btn" class="btn btn-purple txt-w mb10">Modify Search</button>
                                        	</div>
                                        </div>
                                  </form>
                                  <div class="next-re-search mb10">
                                  	<div class="col-md-12 col-xs-12">
                                    	<div class="form-inline clearfix search-bar">
                                        	<div class="form-group next-pre col-sm-12 col-md-12 text-center">
                                            	<div class="from-to-search">
                                                	<span class="mr10">Mumbai</span>
                                                    <span class="fa fa-arrow-right"></span>
                                                    <span class="ml10">Goa</span>
                                                </div>
                                                <div class="next_previous">
                                                	<span>Previous</span>
                                                    <span class="fa fa-chevron-left mr5 cursor-p"></span>
                                                    <span>20-Nov-2018</span>
                                                    <span class="fa fa-chevron-right ml5 cursor-p"></span>
                                                    <span>Next</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <!--Modify Search Ends-->
                        
                        <div class="row">
							<div class="col-sm-4 col-md-2">
                            	<h4 class="search-results-title">
                                	<i class="glyphicon glyphicon-search mr10 icon-search"></i>
                                    <b><span>255</span></b> results found.
                                </h4>
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingOne">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                    <i class="short-full glyphicon glyphicon-plus"></i>
                                                    Type
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                            <div class="panel-body">
                                            	<form action="" method="get">
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="MSRTC" type="checkbox" value=""> <a href="#">All Hotels (2515)</a>
                                                            </div>
                                                    	</div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">BUSINESS HOTEL (13)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">RESORT (209)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">BEACH HOTEL (31)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">HOME STAY (17)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">SERVICE APARTMENT (22)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">CITY HOTEL (762)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">APARTMENT HOTEL (22)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">BEACH RESORT (38)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">TENT OR CABANAS (5)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">HERITAGE (2)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">LODGE (6)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">CAMPING GROUND (14)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">HOSTEL (4)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">VILLA (12)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">BUDGET HOTEL (20)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">WOODEN HUTS (5)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">COTTAGES (12)</a>
                                                            </div>
                                                        </div><div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">GUEST HOUSE (39)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">LUXURY HOTEL (2)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">BOUTIQUE HOTEL (14)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">INN (1)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">TOWNHOUSE (2)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">PALACE (2)</a>
                                                            </div>
                                                        </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingTwo">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                                    <i class="short-full glyphicon glyphicon-plus"></i>
                                                    Fare
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                            <div class="panel-body">
                                            	<div class="slider slider-horizontal" id="ex1Slider"><div class="slider-track"><div class="slider-track-low" style="left: 0px; width: 0%;"></div><div class="slider-selection" style="left: 0%; width: 56%;"></div><div class="slider-track-high" style="right: 0px; width: 44%;"></div></div><div class="tooltip tooltip-main top" role="presentation" style="left: 56%;"><div class="tooltip-arrow"></div><div class="tooltip-inner">Current value: 9</div></div><div class="tooltip tooltip-min top" role="presentation" style="display: none;"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div><div class="tooltip tooltip-max top" role="presentation" style="display: none;"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div><div class="slider-handle min-slider-handle round" role="slider" aria-valuemin="-5" aria-valuemax="20" style="left: 56%;" aria-valuenow="9" aria-valuetext="Current value: 9" tabindex="0"></div><div class="slider-handle max-slider-handle round hide" role="slider" aria-valuemin="-5" aria-valuemax="20" style="left: 0%;" aria-valuenow="-5" aria-valuetext="Current value: -5" tabindex="0"></div></div>
                                                  <input id="ex1" data-slider-id="ex1Slider" type="text" data-slider-min="-5" data-slider-max="20" data-slider-step="1" data-slider-value="14" style="display: none;" data-value="11" value="11">
                                            </div>
                                        </div>
                                    </div>
                            
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingThree">
                                            <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                    <i class="short-full glyphicon glyphicon-plus"></i>
                                                    Stars
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                            <div class="panel-body">
                                                <form action="" method="get">
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="1 Stop" type="checkbox" value=""> <a href="#">All Stars (2515)</a>
                                                            </div>
                                                    	</div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="2 Stops" type="checkbox" value=""> <a href="#"><i class="fa fa-star" aria-hidden="true"></i> (184)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="3 Stops" type="checkbox" value=""> <a href="#"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>  (254)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> (305)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> (108)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> (56)</a>
                                                            </div>
                                                        </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                            
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingFour">
                                            <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                                    <i class="short-full glyphicon glyphicon-plus"></i>
                                                    All Amenities
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                            <div class="panel-body">
                                                <form action="" method="get">
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="1 Stop" type="checkbox" value=""> <a href="#">All Amenities</a>
                                                            </div>
                                                    	</div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="2 Stops" type="checkbox" value=""> <a href="#">Airport/Station Transfer (170)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="3 Stops" type="checkbox" value=""> <a href="#">ATM (3)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Audiovisual Equipment (4)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Ayurveda Center (40)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Babysitting (3)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Ballroom (104)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Bar (371)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Business Center (121)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Car/Bike Rental Facilities (139)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Casino (2)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Coffee Shop (196)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Complimentary newspapers in lobby (25)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Concierge (4)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Conference Facility (143)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Credit Cards Accepted (151)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Currency Exchange (14)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Doctor on call (360)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Elevator (17)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Golf (2)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Gym (126)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Physically Challenged Friendly (52)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Internet Facility (435)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Laundry (578)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Library (4)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Luggage Storage (46)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Multilingual (1)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Night Club (1)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Outdoor Games (12)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Parking Facility (506)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Pets Allowed (15)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Pool table (4)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Swimming Pool (382)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Power Backup (203)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Private beach (2)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Restaurant (183)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Room Service (603)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Safety Deposit Lockers (183)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Salon (3)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Sauna (1)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Shopping (11)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Smoking Area (8)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Spa (87)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Steam Room (17)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Television in lobby (3)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Travel Desk (392)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Trekking Nearby (1)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">24 Hour Front Desk (269)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Air conditioning (92)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Front Desk (Limited Hours) (4)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Indoor Games (2)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Non Smoking (46)</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Multistops" type="checkbox" value=""> <a href="#">Phone (6)</a>
                                                            </div>
                                                        </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="headingFive">
                                            <h4 class="panel-title">
                                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                                    <i class="short-full glyphicon glyphicon-plus"></i>
                                                    Locations
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                            <div class="panel-body">
                                                <form action="" method="get">
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="MSRTC" type="checkbox" value=""> <a href="#">All Locations</a>
                                                            </div>
                                                    	</div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">Agonda</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">Agonda Beach</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">Aguada Lighthouse</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb5">
                                                        	<div class="collapse-cnt">
                                                            <input name="Neeta" type="checkbox" value=""> <a href="#">Alto de Porvorim</a>
                                                            </div>
                                                        </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>                            
                                </div><!-- panel-group -->
                            </div>
                            
                            <div class="col-sm-8 col-md-10">
                            	<div class="sort-by-section clearfix mb10">
                                	<h4 class="sort-by-title block-sm">Sort results by:</h4>
                                    <ul class="sort-bar clearfix">
                                    	<li class="sort-by-name"><a class="sort-by-container" href="#"><span>Hotel Name</span></a></li>
                                        <li class="sort-by-price"><a class="sort-by-container" href="#"><span>Price</span></a></li>
                                        <li class="sort-by-amenities"><a class="sort-by-container" href="#"><span>Amenities</span></a></li>
                                        <li class="sort-by-stars"><a class="sort-by-container" href="#"><span>Stars</span></a></li>
                                    </ul>
                                </div>
                                
                                <!-- flight-service-list start -->
                                <div class="flight-service-list">
                                    <div class="panel-body" role="tab">
                                        <div class="one-way dataTables_wrapper">
                                            <div class="row sorters">
                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><div class="sorter-title-h">Stars</div></div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"><div class="sorter-title-h">Hotel Name</div></div>
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"><div class="sorter-title-h">TripAdvisor Rating</div></div>
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"><div class="sorter-title-h">Price</div></div>
                                            </div>
                                            
                                            <div class="row results">
                                               <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                   <div class="hotelthumbnail">
                                                    	<div class="img-thumbnail"><img src="images/img-1.jpg"/></div>
                                                        <div class="stars">
                                                          <span class="white"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i></span>
                                                        </div>
                                                        <div class="viewhotel">
                                                          <button type="submit" id="submit_filter_btn" class="btn btn-purple txt-w mt10"><i class="soap-icon-hotel"></i> View Hotel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 border-middle">
                                                        <div class="hoteldetailsdiv">
                                                            <p class="name"><a href="#">Taj Holiday Village Resort & Spa, Goa</a></p>
                                                            <small>Dando, Candolim, Sinquerim,403519,Goa,Goa,India</small>
                                                        </div>
                                                        <div class="row">
                                                        	<div class="col-md-12">
                                                                <div class="hotelfacilities">
                                                                    <a href="#"><span data-toggle="tooltip" data-placement="top" title="sdfdsfs"><i class="fa fa-wifi font20 pd-r"></i></span></a>
                                                                    <a href="#"><span><i class="fa fa-concierge-bell pd-r"></i></span></a>
                                                                    <a href="#"><span><i class="soap-icon-swimming font25 pd-r"></i></span></a>
                                                                    <a href="#"><span><i class="soap-icon-coffee font30 pd-r"></i></span></a>
                                                                    <a href="#"><span><i class="soap-icon-businessbag font25 pd-r"></i></span></a>
                                                                    <a href="#"><span>& more...</span></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                        <div class="tripadvisor-rating">
                                                        	dfdfffdfds
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                   		<div class="pricesec">
                                                            <div><small>Starting From</small></div>
                                                            <div>
                                                                <span class="fltprice txt-p"><i class="fa fa-rupee"></i> 10,986 </span>
                                                                <span><small>Per room/night</small></span>
                                                            </div>
                                                        </div>
                                                        <div class="bookbtn">
                                                            <button type="submit" id="submit_filter_btn" class="btn btn-purple txt-w mt10">Select Rooms</button>
                                                        </div>
                                                   </div>
                                            </div>
                                          </div>
                                        </div>
                                    </div>    
                                
                                <!-- flight-service-list end -->
                            </div>  
                                                  
                        </div>
                        
                    </div>
              	</section>


    <!-- Signin Content End -->             
</div>
<!-- Inner Wrapper End-->
<script>
function openNav() {
	if ($(window).width() < 981){
		document.getElementById("mySidenav").style.width = "100%";		
	}else{
		document.getElementById("mySidenav").style.width = "15%";
	}
	
	
	var width = $("#mySidenav").width();
	if (width !== 0){
		document.getElementById("mySidenav").style.width = "0";	
	}
    
}

function closeNav() {
	
    document.getElementById("mySidenav").style.width = "0";
}
</script>
<script>
	$(".toggle-password").click(function() {
  	$(this).toggleClass("fa-eye fa-eye-slash");
  	var input = $($(this).attr("toggle"));
  	if (input.attr("type") == "password") {
    input.attr("type", "text");
  	} else {
    input.attr("type", "password");
  	}
});
</script>
<script>
	//jQuery('#qrcode').qrcode("this plugin is great");
//	jQuery('#qrcodeCanvas').qrcode({
//		text	: "http://jetienne.com"
//	});	
</script>
<!--<script src="https://code.jquery.com/jquery-3.2.1.js"></script>-->
	 <script type="text/javascript">
	 	$(".input").focus(function() {
	 		$(this).parent().addClass("focus");
	 	})
	 </script>

