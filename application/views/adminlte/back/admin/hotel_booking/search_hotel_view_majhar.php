<!--Inner Wrapper Start -->
<link href="<?php echo base_url() . BACK_ASSETS ?>css/hotel_style/style.css" rel="stylesheet" type="text/css" />
<div id="page-wrapper">

    <!-- Signin Content Start -->
    <section class="inner-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    if (!empty($this->session->flashdata('item'))) {
                        $message = $this->session->flashdata('item');
                        //var_dump($message); die();
                        ?>
                        <div id="alertMsgDangar" class="<?php echo $message['class']; ?>" style="text-align: center;"><h3><strong><?php echo $message['message']; ?></h3></strong></div>
                        <?php
                    }
                    ?>
                    <h3 class="text-center purple">Hotel Booking</h3>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-12 col-sm-12 container-c">
                                <div class="formBox">
                                   <form action="<?php echo base_url() ?>admin/search_hotel/search_services" method="POST"  name="hotel_search" accept-charset="utf-8" id="hotel_search">                                
                                            <div class="row">
                                                <div class="col-sm-12 col-xs-12">
                                                    <div class="inputBox dinputBox">
                                                        <div class="inputText">Destination</div>  
                                                            <select class="input selectText" id="city" name="city">
                                                                  <option value=""></option>
                                                                  <?php foreach ($cityList as $key => $value) { 

                                                                    echo '<option value='.$value['city_id'].'>'.$value['name'].'</option>';
                                                                    ?>

                                                                <?php } ?>
                                                            </select>
                                                        
                                                            <div class="error" id="erDestination"></div>
                                                            <?php echo form_error('destination', '<div class="error">', '</div>'); ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="inputBox cinputBox">
                                                        <div class="inputText">Check In</div>
                                                         <input id="datepicker"  name="check_in_date" class="input" type="text">
                                            <div class="error" id="erDatepicker"></div>
                                            <?php echo form_error('check_in_date', '<div class="error">', '</div>'); ?>
                                                    </div>
                                                    
                                                </div>

                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="inputBox cinputBox">
                                                        <div class="inputText">Check Out</div>
                                                       <input id="datepicker1" name="check_out_date" class="input" type="text" >
                                            <div class="error" id="erDatepicker1"></div>
                                            <?php echo form_error('check_out_date', '<div class="error">', '</div>'); ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                            <table class="table table-bordered" id="item_table">
                                            <tr>

                                            <th><button type="button" name="add" id="add_doc_btn" class="btn btn-success btn-sm add"><span>Add Rooms</span></button></th>
                                            </tr>
                                            </table>


                                            </div>

                                            <!--<div class="row">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="selectBox">
                                                        <div class="inputText">Nationality</div>
                                                        <select class="input selectText">
                                                          <option value=""></option>
                                                          <option>India</option>
                                                          <option>United States of America</option>
                                                          <option>United Kingdom</option>
                                                          <option>France</option>
                                                          <option>China</option>
                                                          <option>Cananda</option>
                                                          <option>Italy</option>
                                                          <option>Germany</option>
                                                          <option>Other</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="selectBox">
                                                        <div class="inputText">Residence</div>
                                                        <select class="input selectText">
                                                          <option value=""></option>
                                                          <option>India</option>
                                                          <option>United States of America</option>
                                                          <option>United Kingdom</option>
                                                          <option>France</option>
                                                          <option>China</option>
                                                          <option>Cananda</option>
                                                          <option>Italy</option>
                                                          <option>Germany</option>
                                                          <option>Other</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>-->

                                            <div class="row">
                                                <div class="col-sm-4 col-xs-12 col-sm-offset-4">
                                                    <!-- <button type="button" class="btn btn-purple btn-lg btn-block login-button txt-w col-sm-4">Search Hotels</button> -->
                                                     <button type="submit" id="search_hotel" class="btn btn-purple btn-lg btn-block login-button txt-w col-sm-4">Search Hotel</button>
                                                </div>
                                            </div>
                                    </form>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<!--    <section class="inner-wrapper">
        <div class="col-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="well">
                            <h4>Why Rokad for Bus Booking?</h4>
                            <p>Rokad is a digital payment platform, powered by Trimax, to offer citizen centric multimodal and multipurpose services. Rokad promotes demonetization through cashless transactions and provide convenience while availing multi-ulility services offered by Rokad.</p>
                            <h4>Services with Rokad while Bus Ticketing Booking</h4>
                            <p>Services offered by Rokad:</p>
                            <p>
                                1. Rokad would enable the passengers to use the same payment instrument across spectrum of other transport operators<br/>
                                2. Rokad would significantly reduce the cash requirement for travelling purposes<br/>
                                3. Rokad can be used in paying bills with different merchants
                            </p>
                            <p>
                                Rokad on-board user will get dashboard and peer-to- peer transfer facilities. The dashboard facility would enable the customers to view their tansactions, the wallet balance, option to raise a query / dispute, etc. The peer-to-peer transfer facilities would enable customers to remit money in a economic and secured manner.
                            </p>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </section>-->
    <!-- Signin Content End -->             
</div>
<!-- Inner Wrapper End-->
<script>
    $(document).ready(function() {
        var x = 0;
        $(document).on('click', '.add', function(){
            var rowCount = $('#item_table tr').length;
            //alert(rowCount);
            x++;

           var html = '';
            html += '<tr>';

            html += '<td><div class="bui-stepper" data-bui-component="InputStepper"><b>Room'+x+'</b><div class="bui-stepper__title-wrapper"><label class="bui-stepper__title" for="group_adults">Adults</label></div><div class="bui-stepper__wrapper"><input  id="show_adult_count'+rowCount+'" type="hidden" name="room['+x+'][group_adults][]" min="1" max="30" value="1" ><button class="bui-button bui-button--secondary bui-stepper__subtract-button" data-bui-ref="input-stepper-subtract-button" type="button" aria-hidden="true" data-person="adult" data-room="" onclick="decrement(this,'+rowCount+')"><span class="bui-button__text">-</span></button><span class="bui-stepper__display" data-bui-ref="input-stepper-value" aria-hidden="true" id="adult_span'+rowCount+'">1</span><button class="bui-button bui-button--secondary bui-stepper__add-button" data-bui-ref="input-stepper-add-button" type="button" aria-hidden="true" data-person="adult" data-room="" onclick="increment(this,'+rowCount+')"><span class="bui-button__text"  >+</span></button></div></div>';

            html += '<div class="bui-stepper" data-bui-component="InputStepper"><div class="bui-stepper__title-wrapper"><label class="bui-stepper__title" for="group_children">Children</label></div><div class="bui-stepper__wrapper"><input type="hidden"  id="show_childrens_count'+rowCount+'" name="room['+x+'][group_children][]" min="0" max="10" value="0" > <button class="bui-button bui-button--secondary bui-stepper__subtract-button  remove_child" data-bui-ref="input-stepper-subtract-button" type="button" data-person="childrens" data-room="Room'+rowCount+'" onclick="decrement(this,'+rowCount+')"><span class="bui-button__text">-</span></button><span class="bui-stepper__display" data-bui-ref="input-stepper-value" aria-hidden="true" id="childrens_span'+rowCount+'">0</span><button class="bui-button bui-button--secondary bui-stepper__add-button add1" data-bui-ref="input-stepper-add-button" type="button" aria-hidden="true" data-person="childrens" data-room="Room'+rowCount+'" onclick="increment(this,'+rowCount+')"> <span class="bui-button__text ">+</span></button></div></div>';


            html += '<table class="table table-bordered" id="item_table'+rowCount+'"><tr></tr> </table></td>';


            html += '<td><button type="button" name="remove" class="btn btn-danger btn-sm remove"><span class="glyphicon glyphicon-minus"></span></button></td></tr>';
            $('#item_table').append(html);
        });

        $(document).on('click', '.remove', function(){
            var rowCount = $('#item_table tr').length;  
            $(this).closest('tr').remove();
        });

        $("body").on('click', '#search_hotel', function() {

            //aler('ss');
            
            /*var destination = $('#destination').val();
            var checkIn = $("#datepicker").val();
            var checkOut = $("#datepicker1").val();
            var roomList = $("#roomGuest").val();
            var nationality = $("#nationality").val();
            var residence = $("#residence").val();*/
           
           /* if (destination == '' && checkIn == '' && checkOut == '') {
                alert('Please fill out all required fields');
                return false;
            }
            if (destination == '') {
                $('#destination').text('The Destination is required.');
                return false;
            }
            if (checkIn == '') {
                $('#erDatepicker').text('The Check In is required.');
                return false;
            }
            if (checkOut == '') {
                $('#erDatepicker1').text('The Check Out is required.');
                return false;
            }
            if (checkOut == '') {
                $('#erDatepicker1').text('The Check Out is required.');
                return false;
            }
            if (roomList == '') {
                $('#erRoomGuest').text('The Roon and List is required.');
                return false;
            }
            if (nationality == '') {
                $('#erNationality').text('The Nationality is required.');
                return false;
            }
            if (residence == '') {
                $('#erResidence').text('The Residence is required.');
                return false;
            }*/
        });

        $("#datepicker").datepicker({
            numberOfMonths: 2,
            minDate: 0,
        });
        $("#datepicker1").datepicker({numberOfMonths: 2, minDate: 0});

        /**load all stops in from stop input**/
        $("#destination").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: base_url + "admin/search_bus/getCity",
                    dataType: "json",
                    data: {
                        term: request.term,
                        // type: rechargeType
                    },
                    success: function(data) {
                        response($.map(data, function(item) {
                            return {
                                label: item.label,
                                name: item.name,
                            };
                        }));
                    }
                });
            },
            autoFocus: true,
            minLength: 1,
            select: function(event, ui) {

                $("#from_stop").val(ui.item.name);
                if (ui.item.name == "No result found") {
                    $("#from_stop").val("");
                } else {
                    $("#from_stop_id").val(ui.item.id);
                }
            }
        });

        ///////////////////*//////////////////////////////

        window.setInterval('alertMsgDangar()', 10000); // 20 seconds

    });
    function alertMsgDangar() {
        $('#alertMsgDangar').hide();
    }


    function increment(obj,count){


  var person= $(obj).attr('data-person');
  var room= $(obj).attr('data-room');
  //alert(room);


  //alert(rowcount);

    var show_count='';
    var span='';
    if(person == 'adult'){
      show_count='show_adult_count'+count;
      span='adult_span'+count;
        

    }else if(person == 'childrens'){
        show_count='show_childrens_count'+count;
        span='childrens_span'+count;

            addChidrenAge(count,room);


    }else if(person == 'rooms'){
        show_count='show_rooms_count';
        span='rooms_span';


    }


    var value = parseInt(document.getElementById(show_count).value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    document.getElementById(show_count).value = value;
    $('#'+span).text(value);





    
}

    function decrement(obj,count){
  var person= $(obj).attr('data-person');
  var room= $(obj).attr('data-room');
  //alert(room);
  //alert(person);
    var show_count='';
    var span='';
    if(person == 'adult'){
      show_count='show_adult_count'+count;
      span='adult_span'+count;
        

    }else if(person == 'childrens'){
        show_count='show_childrens_count'+count;
              span='childrens_span'+count;
               removeChidrenAge(count,room);


    }else if(person == 'rooms'){
        show_count='show_rooms_count';
        span='rooms_span';


    }
    var value = parseInt(document.getElementById(show_count).value, 10);
    value = isNaN(value) ? 0 : value;
    if(value > 0){
    value--;
    document.getElementById(show_count).value = value;
    $('#'+span).text(value);


    }


    
}

    function addChidrenAge(count,room){

//name="room['+x+'][group_adults][]"
     var rowCount = $('#item_table'+count+' tr').length;
  //alert(rowCount);


  if(rowCount<=10){
    var html = '';
  html += '<tr id="'+rowCount+''+room+'">';

  html += '<td><label class="sb-searchbox__label -small sb-group__children__label">How old are the children your traveling with?</label>';

  html += '<td><select name="room['+room+'][childrens_age][]" data-group-child-age="0" aria-label="Child 1 age"> <option selected="" class="sb_child_ages_empty_zero" value="12 ">Age at check-out</option><option value="0">0 years old</option> <option value="1"> 1 year old</option><option value="2">2 years old</option><option value="3">3 years old</option><option value="4"> 4 years old</option><option value="5">5 years old</option><option value="6"> 6 years old </option>    </select>';
 
  html += '</tr>';
  $('#item_table'+count).append(html);
}else{

  //alert('Not Allowed more than 3');
  //$("#add_doc_btn").prop("disabled", true);
}



     }

    function removeChidrenAge(count,room){

    var row=$("#show_childrens_count"+count).val();
    if(row>=0){
    var t=parseInt(row);

    $('#'+t+''+room).remove();

    }else{

    $('#show_childrens_count'+count).val(0);

    $('#childrens_span'+count).text(0);
    }



    }

</script>
<script type="text/javascript">
    $(".input").focus(function() {
        $(this).parent().addClass("focus");
    })
</script>


