<!--Inner Wrapper Start -->
<link href="<?php echo base_url() . BACK_ASSETS ?>css/hotel_style/style.css" rel="stylesheet" type="text/css" />
<div id="page-wrapper">    
    <!-- Signin Content Start -->
    <section class="inner-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                <h3 class="text-center purple">Hotel Search Result</h3>
                    <div class="searchp-form well mb10">
                    <form action="#" method="POST" id="search_services" name="hotel_booking" accept-charset="utf-8" class="form-inline mb20">
                        <div class="form-group">
                            <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                <select class="form-control mb10" id="city" name="city">                                                        
                                    <?php foreach ($cityList as $key => $value) { ?>

                                      <option value='<?php echo $value['city_id']?>' <?php  if($value['city_id'] == $this->session->userdata('city')) echo 'selected'; ?> ><?php echo $value['name']?></option>;


                                  <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                <input type="text" class="form-control mb10" id="datepicker"  name="check_in_date" placeholder="Check In" value="<?php echo $this->session->userdata('checkInDate'); ?>" readonly>
                                <span class="glyphicon glyphicon-calendar form-input-icon icon-calendar"></span>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                <input type="text" class="form-control mb10" id="datepicker1" name="check_out_date" placeholder="Check Out" value="<?php echo $this->session->userdata('checkOutDate'); ?>" readonly>
                                <span class="glyphicon glyphicon-calendar form-input-icon icon-calendar"></span>
                            </div>
                        </div>
                        
                        <div class="form-group has-feedback">
                                <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                    <table class="table table-bordered" id="item_table">
                                        <tr>
                                            <th><button type="button" name="add" id="add_doc_btn" class="btn btn-success btn-sm add"><span>Add Rooms</span></button></th>
                                        </tr>
                                    </table>
                            </div>
                        </div>

<!--                        <div class="form-group has-feedback">
                                <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                  <select id="disabledSelect" class="form-control mb10" placeholder="">
                                    <option selected="selected" value="Preferred Airlines">Nationality</option>
                                     <option>India</option>
                                     <option>United States of America</option>
                                     <option>United Kingdom</option>
                                     <option>France</option>
                                     <option>China</option>
                                     <option>Canada</option>
                                     <option>Italy</option>
                                     <option>Germany</option>
                                     <option>Other</option>
                                  </select>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                                <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                  <select id="disabledSelect" class="form-control mb10" placeholder="">
                                    <option selected="selected" value="Residence">Residence</option>
                                    <option>India</option>
                                     <option>United States of America</option>
                                     <option>United Kingdom</option>
                                     <option>France</option>
                                     <option>China</option>
                                     <option>Canada</option>
                                     <option>Italy</option>
                                     <option>Germany</option>
                                     <option>Other</option>
                                  </select>
                            </div>
                        </div>-->
                        <div class="form-group">
                            <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                <button type="button" id="search_hotel" class="btn btn-purple txt-w mb10">Modify Search</button>
                            </div>
                        </div>
                        <input type="hidden" name="request_type" value="ajax_req"> 
                  </form>
<!--                  <div class="next-re-search mb10">
                        <div class="col-md-12 col-xs-12">
                        <div class="form-inline clearfix search-bar">
                                <div class="form-group next-pre col-sm-12 col-md-12 text-center">
                                <div class="from-to-search">
                                    <span class="mr10"><?php echo $this->session->userdata('checkInDate'); ?></span>
                                    <span class="fa fa-arrow-right"></span>
                                    <span class="ml10"><?php echo $this->session->userdata('checkOutDate'); ?></span>
                                </div>
                                <div class="next_previous">
                                        <span>Previous</span>
                                    <span class="fa fa-chevron-left mr5 cursor-p"></span>
                                    <span>20-Nov-2018</span>
                                    <span class="fa fa-chevron-right ml5 cursor-p"></span>
                                    <span>Next</span>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>-->
                </div>
                </div>
            </div>
            <!--Modify Search Ends-->
            <div class="row" id="loading">
                <div id="services_view_render">
                    <?php $this->load->view('adminlte/back/admin/hotel_booking/hotel_services_view'); ?>
                </div>   
            </div>      
        </div>
    </section>


    <!-- Signin Content End -->             
</div>
<!-- Inner Wrapper End-->
<script>
function openNav() {
	if ($(window).width() < 981){
		document.getElementById("mySidenav").style.width = "100%";		
	}else{
		document.getElementById("mySidenav").style.width = "15%";
	}
	
	
	var width = $("#mySidenav").width();
	if (width !== 0){
		document.getElementById("mySidenav").style.width = "0";	
	}
    
}

function closeNav() {
	
    document.getElementById("mySidenav").style.width = "0";
}
</script>
<script>
	$(".toggle-password").click(function() {
  	$(this).toggleClass("fa-eye fa-eye-slash");
  	var input = $($(this).attr("toggle"));
  	if (input.attr("type") == "password") {
    input.attr("type", "text");
  	} else {
    input.attr("type", "password");
  	}
});
</script>
<script>
	//jQuery('#qrcode').qrcode("this plugin is great");
//	jQuery('#qrcodeCanvas').qrcode({
//		text	: "http://jetienne.com"
//	});	
</script>
<!--<script src="https://code.jquery.com/jquery-3.2.1.js"></script>-->
<script type="text/javascript">
	 	$(".input").focus(function() {
	 		$(this).parent().addClass("focus");
	 	})
</script>
<script>
    $("#datepicker" ).datepicker({ 
        numberOfMonths: 2,
        minDate: 0,
        dateFormat: 'dd-mm-yy',

        }).on("change", function() {
                $('#datepicker1').datepicker('option', 'minDate', $("#datepicker").val());
    });
            
    $("#datepicker1" ).datepicker({
      dateFormat: 'dd-mm-yy',
      numberOfMonths: 2,
     minDate:0
    }).on("change", function() {
            $('#datepicker').datepicker('option', 'minDate', 0);
    });
</script>
<script>
    $(document).ready(function() {
     /************************start search hotel ***************************/ 
     $("body").on('click','#search_hotel',function(){
        
            var check_in_date = $('#datepicker').val();
            var check_out_date = $('#datepicker1').val();
            var city = $('#city').val();
            var toStop = $('#to_stop').val();
            var rfromStop = $('#rto_stop').val();
            var rtoStop = $('#rto_stop').val();        
            if(check_in_date == check_out_date) {
             alert('Check In date and Check Out date cant not be same!');
                return false;
            }
            var url = base_url + "admin/search_hotel/hotel_services";
            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: $('#search_services').serialize(),
                async: false,
                success: function(data) {
                    console.log(data);
                    $("body #services_view_render").html(data.results);
                    $('#loader').css({"display": "none"});
                }
            });
     });
     
     /************************end search hotel ***************************/
        
        
    /**************************start get rooms *****************************/    
    $("body").on('click', '#submit_filter_btn', function(){
        var hotel_id = $(this).data("hotel-id");  
        var search_id = $(this).data("search-id");    
        var hotel_name = $(this).data("hotel-name");

        
        console.log(hotel_id); 
        console.log(search_id);
        
        if ($("#hotel_block_" + hotel_id).css('display') === 'none'){
            $("#hotel_block_" + hotel_id).css("display","block");
        }else{
            $("#hotel_block_" + hotel_id).css("display","none");
            return false;
        }
        
        bbpsLoaderImg();
        
        var url = base_url + "admin/search_hotel/get_rooms";
        var dataString = 'hotel_id=' + hotel_id + '&search_id=' + search_id + '&hotel_name=' + hotel_name;
        //if ($("#hotel_block_" + hotel_id).css('display') === 'none'){
        $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: dataString,
                async: false,
                success: function(data) {
                console.log(data);
                  
                hideLoaderImg();
             
                var row = '';
                var result = data.room_details;
                for (i = 0; i < result.length; i++)
                        {    
                            console.log(result[i].itineraryKey);
                            console.log(result[i].room_name);
                            row += '<div class="col-md-4 col-xs-12">'+result[i].room_name+'</div><div class="col-md-4 col-xs-12">'+result[i].price+' Per room/night + Tax '+result[i].tax+'</div><div class="col-md-2 col-xs-12"><button type="submit" id="book_room" class="btn btn-purple txt-w mt20" data-amount= '+result[i].price+' data-itinerary-key='+result[i].itineraryKey+' data-room-name="'+result[i].room_name+'" data-room-info="'+result[i].info+'">Book Room</button></div>';
                            
                        }
                
                $("#hotel_name_" + hotel_id).html(row);
                },
                
            });
       // }

    });
    
    /**************************end get rooms *****************************/
    
    /**************************start book room *****************************/
    $("body").on('click', '#book_room', function(){
        
        var itinerary_key = $(this).data("itinerary-key");      
        var room_name = $(this).data("room-name"); 
        var amount = $(this).data("amount");  
        var room_info = $(this).data("room-info");  

  
        console.log(room_info);
        console.log(itinerary_key); 
        console.log(room_name);
        bbpsLoaderImg();
        
        var url = base_url + "admin/search_hotel/reprice";
        var dataString = 'itinerary_key=' + itinerary_key + '&room_name=' + room_name + '&amount=' + amount + '&room_info=' + room_info;
//        alert(dataString);
//        return false;
        $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: dataString,
                async: false,
                success: function(data) {
                     hideLoaderImg();
                console.log(data);
                console.log(data.status);  
                console.log(data.error);
                    if(data.status == 'failed'){
                        alert(data.error);
                        return false;
                    }else if(data.bookingKey !='null')
                    {
                        console.log(data);
                        window.location.href = data.redirect;
                        
                    }
                },
                error: function(data) {
                    // do something
                }
                
            });

    });
    
    /**************************end  book room *****************************/
    });
    
    
</script>

<script>
    var x = 0;
    $(document).on('click', '.add', function(){
       // var rowCount = $('#item_table tr').length;
        //alert(rowCount);
        x++;
        //var rowCount = $('#item_table tr').length;     
        var rowCount = x;
        console.log(rowCount);
        var html = '';
        html += '<tr>';

        html += '<td><div class="bui-stepper" data-bui-component="InputStepper"><b>Room'+x+'</b><div class="bui-stepper__title-wrapper"><label class="bui-stepper__title" for="group_adults">Adults</label></div><div class="bui-stepper__wrapper"><input  id="show_adult_count'+rowCount+'" type="hidden" name="room['+'Room'+x+'][group_adults][]" min="1" max="30" value="1" ><button class="bui-button bui-button--secondary bui-stepper__subtract-button" data-bui-ref="input-stepper-subtract-button" type="button" aria-hidden="true" data-person="adult" data-room="" onclick="decrement(this,'+rowCount+')"><span class="bui-button__text">-</span></button><span class="bui-stepper__display" data-bui-ref="input-stepper-value" aria-hidden="true" id="adult_span'+rowCount+'">1</span><button class="bui-button bui-button--secondary bui-stepper__add-button" data-bui-ref="input-stepper-add-button" type="button" aria-hidden="true" data-person="adult" data-room="" onclick="increment(this,'+rowCount+')"><span class="bui-button__text"  >+</span></button></div></div>';

        html += '<div class="bui-stepper" data-bui-component="InputStepper"><div class="bui-stepper__title-wrapper"><label class="bui-stepper__title" for="group_children">Children</label></div><div class="bui-stepper__wrapper"><input type="hidden"  id="show_childrens_count'+rowCount+'" name="room['+'Room'+x+'][group_children][]" min="0" max="10" value="0" > <button class="bui-button bui-button--secondary bui-stepper__subtract-button  remove_child" data-bui-ref="input-stepper-subtract-button" type="button" data-person="childrens" data-room="Room'+rowCount+'" onclick="decrement(this,'+rowCount+')"><span class="bui-button__text">-</span></button><span class="bui-stepper__display" data-bui-ref="input-stepper-value" aria-hidden="true" id="childrens_span'+rowCount+'">0</span><button class="bui-button bui-button--secondary bui-stepper__add-button add1" data-bui-ref="input-stepper-add-button" type="button" aria-hidden="true" data-person="childrens" data-room="Room'+rowCount+'" onclick="increment(this,'+rowCount+')"> <span class="bui-button__text ">+</span></button></div></div>';


        html += '<table class="table table-bordered" id="item_table'+rowCount+'"><tr></tr> </table></td>';


        html += '<td><button type="button" name="remove" class="btn btn-danger btn-sm remove"><span class="glyphicon glyphicon-minus"></span></button></td></tr>';
        $('#item_table').append(html);
    });

    $(document).on('click', '.remove', function(){
        var rowCount = $('#item_table tr').length;  
        $(this).closest('tr').remove();
    });
        
    function increment(obj,count){
        var person= $(obj).attr('data-person');
        var room= $(obj).attr('data-room');
        //alert(room);
        //alert(rowcount);

        var show_count='';
        var span='';
        if(person == 'adult'){
            
          show_count='show_adult_count'+count;
          span='adult_span'+count;

        }else if(person == 'childrens'){
            show_count='show_childrens_count'+count;
            span='childrens_span'+count;

            addChidrenAge(count,room);

        }else if(person == 'rooms'){
            show_count='show_rooms_count';
            span='rooms_span';
        }


        var value = parseInt(document.getElementById(show_count).value, 10);
        value = isNaN(value) ? 0 : value;
        value++;
        document.getElementById(show_count).value = value;
        $('#'+span).text(value);
    
    }

    function decrement(obj,count){
        var person= $(obj).attr('data-person');
        var room= $(obj).attr('data-room');
        //alert(room);
        //alert(person);
          var show_count='';
          var span='';
        if(person == 'adult'){
          show_count='show_adult_count'+count;
          span='adult_span'+count;


        }else if(person == 'childrens'){
            show_count='show_childrens_count'+count;
                  span='childrens_span'+count;
                   removeChidrenAge(count,room);


        }else if(person == 'rooms'){
            show_count='show_rooms_count';
            span='rooms_span';


        }
        var value = parseInt(document.getElementById(show_count).value, 10);
        value = isNaN(value) ? 0 : value;
        if(value > 0){
        value--;
        document.getElementById(show_count).value = value;
        $('#'+span).text(value);


        }
    }

    function addChidrenAge(count,room){

        //name="room['+x+'][group_adults][]"
             var rowCount = $('#item_table'+count+' tr').length;
          //alert(rowCount);


        if(rowCount<=10){
          var html = '';
        html += '<tr id="'+rowCount+''+room+'">';

        html += '<td><label class="sb-searchbox__label -small sb-group__children__label">How old are the children your traveling with?</label>';

        html += '<td><select name="room['+room+'][childrens_age][]" data-group-child-age="0" aria-label="Child 1 age"> <option selected="" class="sb_child_ages_empty_zero" value="12 ">Age at check-out</option><option value="0">0 years old</option> <option value="1"> 1 year old</option><option value="2">2 years old</option><option value="3">3 years old</option><option value="4"> 4 years old</option><option value="5">5 years old</option><option value="6"> 6 years old </option>    </select>';

        html += '</tr>';
        $('#item_table'+count).append(html);
      }else{

        //alert('Not Allowed more than 3');
        //$("#add_doc_btn").prop("disabled", true);
      }



    }

    function removeChidrenAge(count,room){

        var row=$("#show_childrens_count"+count).val();
        if(row>=0){
        var t=parseInt(row);

        $('#'+t+''+room).remove();

        }else{

        $('#show_childrens_count'+count).val(0);

        $('#childrens_span'+count).text(0);
        }

    }
</script>

