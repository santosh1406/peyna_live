<div class="col-sm-4 col-md-2">
    <h4 class="search-results-title">
            <i class="glyphicon glyphicon-search mr10 icon-search"></i>
        <b><span><?php echo $hotelResultCount;?></span></b> results found.
    </h4>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <i class="short-full glyphicon glyphicon-plus"></i>
                        Type
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <form action="" method="get">
                            <div class="col-md-12 mb5">
                                    <div class="collapse-cnt">
                                <input name="MSRTC" type="checkbox" value=""> <a href="#">All Hotels (<?php echo $hotelResultCount;?>)</a>
                                </div>
                            </div>
                            <div class="col-md-12 mb5">
                                    <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">BUSINESS HOTEL (13)</a>
                                </div>
                            </div>
                            
                    </form>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                        <i class="short-full glyphicon glyphicon-plus"></i>
                        Fare
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                    <div class="slider slider-horizontal" id="ex1Slider"><div class="slider-track"><div class="slider-track-low" style="left: 0px; width: 0%;"></div><div class="slider-selection" style="left: 0%; width: 56%;"></div><div class="slider-track-high" style="right: 0px; width: 44%;"></div></div><div class="tooltip tooltip-main top" role="presentation" style="left: 56%;"><div class="tooltip-arrow"></div><div class="tooltip-inner">Current value: 9</div></div><div class="tooltip tooltip-min top" role="presentation" style="display: none;"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div><div class="tooltip tooltip-max top" role="presentation" style="display: none;"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div><div class="slider-handle min-slider-handle round" role="slider" aria-valuemin="-5" aria-valuemax="20" style="left: 56%;" aria-valuenow="9" aria-valuetext="Current value: 9" tabindex="0"></div><div class="slider-handle max-slider-handle round hide" role="slider" aria-valuemin="-5" aria-valuemax="20" style="left: 0%;" aria-valuenow="-5" aria-valuetext="Current value: -5" tabindex="0"></div></div>
                      <input id="ex1" data-slider-id="ex1Slider" type="text" data-slider-min="-5" data-slider-max="20" data-slider-step="1" data-slider-value="14" style="display: none;" data-value="11" value="11">
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        <i class="short-full glyphicon glyphicon-plus"></i>
                        Stars
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                    <form action="" method="get">
                            <div class="col-md-12 mb5">
                                    <div class="collapse-cnt">
                                <input name="1 Stop" type="checkbox" value=""> <a href="#">All Stars (2515)</a>
                                </div>
                            </div>
                            <div class="col-md-12 mb5">
                                    <div class="collapse-cnt">
                                <input name="2 Stops" type="checkbox" value=""> <a href="#"><i class="fa fa-star" aria-hidden="true"></i> (184)</a>
                                </div>
                            </div>
                            <div class="col-md-12 mb5">
                                    <div class="collapse-cnt">
                                <input name="3 Stops" type="checkbox" value=""> <a href="#"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>  (254)</a>
                                </div>
                            </div>
                            <div class="col-md-12 mb5">
                                    <div class="collapse-cnt">
                                <input name="Multistops" type="checkbox" value=""> <a href="#"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> (305)</a>
                                </div>
                            </div>
                            <div class="col-md-12 mb5">
                                    <div class="collapse-cnt">
                                <input name="Multistops" type="checkbox" value=""> <a href="#"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> (108)</a>
                                </div>
                            </div>
                            <div class="col-md-12 mb5">
                                    <div class="collapse-cnt">
                                <input name="Multistops" type="checkbox" value=""> <a href="#"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> (56)</a>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingFour">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        <i class="short-full glyphicon glyphicon-plus"></i>
                        All Amenities
                    </a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                <div class="panel-body">
                    <form action="" method="get">
                            <div class="col-md-12 mb5">
                                    <div class="collapse-cnt">
                                <input name="1 Stop" type="checkbox" value=""> <a href="#">All Amenities</a>
                                </div>
                            </div>
                            <div class="col-md-12 mb5">
                                    <div class="collapse-cnt">
                                <input name="2 Stops" type="checkbox" value=""> <a href="#">Airport/Station Transfer (170)</a>
                                </div>
                            </div>
                            
                    </form>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingFive">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                        <i class="short-full glyphicon glyphicon-plus"></i>
                        Locations
                    </a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                <div class="panel-body">
                    <form action="" method="get">
                            <div class="col-md-12 mb5">
                                    <div class="collapse-cnt">
                                <input name="MSRTC" type="checkbox" value=""> <a href="#">All Locations</a>
                                </div>
                            </div>
                            <div class="col-md-12 mb5">
                                    <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">Agonda</a>
                                </div>
                            </div>
                            <div class="col-md-12 mb5">
                                    <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">Agonda Beach</a>
                                </div>
                            </div>
                            <div class="col-md-12 mb5">
                                    <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">Aguada Lighthouse</a>
                                </div>
                            </div>
                            <div class="col-md-12 mb5">
                                    <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">Alto de Porvorim</a>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>                            
    </div><!-- panel-group -->
</div>
                            
<div class="col-sm-8 col-md-10">
    <div class="sort-by-section clearfix mb10">
        <h4 class="sort-by-title block-sm">Sort results by:</h4>
        <ul class="sort-bar clearfix">
            <li class="sort-by-name"><a class="sort-by-container" href="#"><span>Hotel Name</span></a></li>
            <li class="sort-by-price"><a class="sort-by-container" href="#"><span>Price</span></a></li>
            <li class="sort-by-amenities"><a class="sort-by-container" href="#"><span>Amenities</span></a></li>
            <li class="sort-by-stars"><a class="sort-by-container" href="#"><span>Stars</span></a></li>
        </ul>
    </div>

    <!-- flight-service-list start -->
    <div class="flight-service-list">
         <?php if(isset($hotelResultCount) && ($hotelResultCount) > 0 ):
            ?>
         
            <div class="panel-body" role="tab">
                <div class="one-way dataTables_wrapper">
                    <div class="row sorters">
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><div class="sorter-title-h">Stars</div></div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"><div class="sorter-title-h">Hotel Name</div></div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"><div class="sorter-title-h">TripAdvisor Rating</div></div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"><div class="sorter-title-h">Price</div></div>
                    </div>

                    <?php //if(isset($hotelResultCount) && ($hotelResultCount) > 0 ):
                        foreach ($hotelDetails as $key => $value) :          
                    ?>
                    <div class="row results ">
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    <!--                       <div class="hotelthumbnail">
                                <div class="img-thumbnail"><img src="images/img-1.jpg"/></div>
                                <div class="stars">
                                  <span class="white"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i></span>
                                </div>
                                <div class="viewhotel">
                                  <button type="submit" id="submit_filter_btn" class="btn btn-purple txt-w mt10"><i class="soap-icon-hotel"></i> View Hotel</button>
                                </div>
                            </div>-->
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 border-middle">
                            <div class="hoteldetailsdiv">
                                <p class="name"><a href="#"><?php echo $value['hotelName'];?></a></p>
<!--                                <small>Dando, Candolim, Sinquerim,403519,Goa,Goa,India</small>-->
                            </div>
                            <div class="row">
                                    <div class="col-md-12">
<!--                                    <div class="hotelfacilities">
                                        <a href="#"><span data-toggle="tooltip" data-placement="top" title="sdfdsfs"><i class="fa fa-wifi font20 pd-r"></i></span></a>
                                        <a href="#"><span><i class="fa fa-concierge-bell pd-r"></i></span></a>
                                        <a href="#"><span><i class="soap-icon-swimming font25 pd-r"></i></span></a>
                                        <a href="#"><span><i class="soap-icon-coffee font30 pd-r"></i></span></a>
                                        <a href="#"><span><i class="soap-icon-businessbag font25 pd-r"></i></span></a>
                                        <a href="#"><span>& more...</span></a>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div class="tripadvisor-rating">
<!--                                    dfdfffdfds-->
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                    <div class="pricesec">
                                <div><small>Starting From</small></div>
                                <div>
                                    <span class="fltprice txt-p"><i class="fa fa-rupee"></i> <?php echo $value['price'];?> </span>
                                    <span><small>Per room/night</small></span>
                                </div>
                            </div>
                            <div class="bookbtn">                            
                                <button type="submit" id="submit_filter_btn" class="btn btn-purple txt-w mt10" data-hotel-name="<?php echo isset($value['hotelName']) ? $value['hotelName'] : '' ?>" data-hotel-id="<?php echo isset($value['hotelId']) ? $value['hotelId'] : '' ?>" data-search-id="<?php echo isset($searchId) ? $searchId : '' ?>">Select Rooms</button>
                            </div>
                       </div>

                        <div class="bording-alighting-div clearfix" id="view_stops_<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven" style="display:block;">
                            <div class="col-md-12" style="display:none" id="hotel_block_<?php echo $value['hotelId']?>">
                                <div class=" box-style3 mb20 clearfix" id="hotel_name_<?php echo $value['hotelId']?>">

                                </div>
                            </div>
                        </div>

                    </div>
                    <?php 
                        endforeach;

                    ?>
                  </div>
            </div>
        <?php else: ?>
        <div class="panel-body" role="tab">
            <div class="one-way dataTables_wrapper" >
                        <h3>Ooops!</h3>
                        <div class="no_routes_found">

                            <div class="no_routes_notification">
                                <span>No hotels found for this date.</span>
                                <p>
                                    For more details mail us on <b>support@rokad.in</b> .
                                </p>
                            </div>
                        </div>
            </div>
        </div>
        
        <?php endif;?>
   </div>    

    <!-- flight-service-list end -->
</div>  