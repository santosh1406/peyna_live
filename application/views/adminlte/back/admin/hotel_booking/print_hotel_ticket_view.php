<!-- Inner Wrapper Start -->
 <link href="<?php echo base_url() . BACK_ASSETS ?>css/search_style/style.css" rel="stylesheet" type="text/css" />
        <div id="page-wrapper">
            
                <!-- Signin Content Start -->
                <section class="inner-wrapper">
                    <div class="container">
                        <div class="row">
                          <div class="col-md-6 col-md-offset-3">
                                <h3 class="text-center purple">Print Ticket</h3>
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <h4 class="text-center">Hotel you have booked!</h4>
                                        <div class="col-md-12 col-sm-12 container-c">
                                            <div class="formBox">
                                                <form id="ticket_detail_form" name="ticket_detail_form" onsubmit="return false;" novalidate="novalidate">                                
                                                    <div class="row">
                                                        <div class="col-sm-12 col-xs-12">
                                                            <div class="inputBox">
                                                                <div class="inputText">Email Address</div>
                                                                <input type="text" id="email" name="email" class="input">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-sm-12 col-xs-12">                                                            
                                                                <h4 class="text-center">OR</h4>  
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-sm-12 col-xs-12">
                                                            <div class="inputBox">
                                                                <div class="inputText">Mobile</div>
                                                                <input type="text" id="mobile" name="mobile" class="input">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-sm-12 col-xs-12">
                                                            <div class="inputBox">
                                                                <div class="inputText">Reference Number</div>
                                                                <input type="text"  id="ref_no" name="ref_no" class="input">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <label class="control-label col-md-12 label-text">Get Ticket By</label>
                                                        <div class="col-sm-4 col-xs-6">
                                                            <div class="checkbox checkbox-info checkbox-circle pull-right">
                                                                <input id="checkBox7" class="checkbox-circle" type="radio" name="ticket_mode" value="print"  checked>
                                                                <label for="checkBox7"> Print/View</label>
                                                            </div>
                                                        </div>
                            
                                                        <div class="col-sm-4 col-xs-6">
                                                            <div class="inputBox">
                                                                <div class="checkbox checkbox-info checkbox-circle">
                                                                <input id="checkBox8" class="checkbox-circle" type="radio" name="ticket_mode" value="sms">
                                                                <label for="checkBox8"> MTicket (SMS)</label>
                                                            </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-sm-4 col-xs-6">
                                                            <div class="inputBox">
                                                                <div class="checkbox checkbox-info checkbox-circle">
                                                                <input id="checkBox9" class="checkbox-circle" type="radio" name="ticket_mode" value="mail">
                                                                <label for="checkBox9"> Email</label>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                            
                                                    <div class="row">
                                                        <div class="col-sm-6 col-xs-12 col-sm-offset-3">
                                                            <button id="ticket_mode" name="show_ticket" type="button" class="btn btn-purple btn-lg btn-block login-button txt-w col-sm-4">Get Ticket</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                          </div>
                        </div>
                    </div>
                </section>
                
                <!-- Signin Content End -->             
        </div>
        <!-- Inner Wrapper End -->
<script type="text/javascript">
	$(document).ready(function(){
            
            $(document).off("click","#ticket_mode").on("click","#ticket_mode", function(e){
               
                if($('#ticket_detail_form').valid())
                {
                    var detail = {};
	            var div = "";
	            var ajax_url = 'admin/search_hotel/ticket_by_sms_email';
	            var form = '';

	            detail['ticket_mode'] =  $('#ticket_detail_form input[name=ticket_mode]:checked').val();;
	            detail['ref_no'] = $('#ticket_detail_form #ref_no').val();
	            detail['email'] = $('#ticket_detail_form #email').val();
                    detail['mobile'] = $('#ticket_detail_form #mobile').val();

	            detail['is_history_task'] = 0;

	            $(this).addClass("bg_gray").prop('disabled', true);

	            get_data(ajax_url, form, div, detail, function(response)
	            {
	                if (response.flag == '@#success#@')
	                {
	                	if(response.type == "print")
	                	{
	                		//window.location.href = base_url+"admin/search_hotel/view_tickets/"+response.ticket_details.boss_ref_no;
	                	}
	                	else
	                	{
	                		showNotification(response.msg_type,response.msg);
	                	}
	                }
	                else if(response.flag == '@#error#@')
	                {
	                	showNotification(response.msg_type,response.msg);
	                }
	                else
	                {
	                    alert(response.msg);
	                }

	                $("#ticket_mode").removeClass("bg_gray").prop('disabled', false);

	            }, '', false);
        	}
            });
            
            $('#ticket_detail_form').validate({
	        ignore: "",
	        rules: {
	            email: {
	                required: false,
	            },
	            ref_no : {
	                required: true,
	            },
	            ticket_mode : {
	                required: true,
	            }
	        },
	        messages: {
	            email: {
	              required: "Please Enter Email Address",
	            },
	            ref_no: {
	              required: "Please Enter BOS Reference Number",
	            }
	            ,
	            ticket_mode: {
	              required: "Please Enter ticket mode"
	            }
	        }
	    });
        });  

        function showNotification(msg_type, msg)
        {
               msg_type = (msg_type == "error") ?  "danger" : msg_type;
               // bootstrapGrowl(msg, msg_type, {"delay":"5000","align":"center","offset":{"from":"top","amount":"250"}}); 
               bootstrapGrowl(msg, msg_type, {"delay":"5000","align":"right"}); 
        }
           
        function bootstrapGrowl(msg, msg_type, msg_config)
        {
        var offset_from = 'top';
        var offset_amount = 10;

        if (typeof (msg_type) == 'undefined')
        {
            msg_type = 'info';
        }

        if (typeof (msg_config) == 'undefined')
        {
            msg_config = {};
        }
        else if (typeof (msg_config.offset) != 'undefined' && msg_config.offset != null)
        {
            offset_from = (msg_config.offset.from != undefined && msg_config.offset.from != "") ? msg_config.offset.from : "top";
            offset_amount = (msg_config.offset.amount != undefined && msg_config.offset.amount != "") ? msg_config.offset.amount : 10;
        }

        var config = {
            ele: 'body', // which element to append to
            type: msg_type, // (null, 'info', 'danger', 'success')
            offset: {from: offset_from, amount: offset_amount}, // 'top', or 'bottom'
            align: 'right', // ('left', 'right', or 'center')
            width: 350, // (integer, or 'auto')
            delay: 3000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
            allow_dismiss: true, // If true then will display a cross to close the popup.
            stackup_spacing: 10 // spacing between consecutively stacked growls.                
        };

        var fConfig = $.extend({}, config, msg_config);

        $.bootstrapGrowl(msg, fConfig);
    }

    $(".input").focus(function() {
        $(this).parent().addClass("focus");
    });
</script>