<!--<link href="<?php echo base_url() . COMMON_ASSETS ?>js/bootstrap/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() . COMMON_ASSETS ?>js/bootstrap/bootstrap-select.js" type="text/javascript"></script>-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">-->

<style>
    //added by harshada kulkarni on 12-07-2018
    .overflowClass{
        //border: 1px solid #ccc;
        height: 50px;
        width: 48.5%;
        overflow-x:scroll;
        overflow-x:hidden;
    }
</style>

<section class="content-header">
        <h2 style="margin-left:20px">New Registration</h2>
</section>
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> </div>
                    <div class="box-body">
                        <?php echo validation_errors('<div class="error">', '</div>'); ?>
                        <?php
                        $attributes = array("method" => "POST", "id" => "registration_form", "name" => "registration_form");
                        echo form_open(base_url() . 'admin/registration/saveuser', $attributes);
                        ?>
                        <input name="role" type="hidden" id="role" value="<?php echo $role_id ?>" class="form-control" autocomplete="off">
                        <input name="level" type="hidden" id="level" value="<?php echo $level ?>" class="form-control" autocomplete="off">
                        <input name="creat_role" id="creat_role" type="hidden" value="<?php echo $creat_role ?>" class="form-control" autocomplete="off">
                        <input name="cmp_drop_down_flag" type="hidden" id="cmp_drop_down_flag" value="<?php echo $cmp_drop_down_flag ?>" class="form-control" autocomplete="off">
                        <input name="md_drop_down_flag" type="hidden" id="md_drop_down_flag" value="<?php echo $md_drop_down_flag ?>" class="form-control" autocomplete="off">
                        <input name="ad_drop_down_flag" type="hidden" id="ad_drop_down_flag" value="<?php echo $ad_drop_down_flag ?>" class="form-control" autocomplete="off">
                        <input name="d_drop_down_flag"  type="hidden" id="d_drop_down_flag" value="<?php echo $d_drop_down_flag ?>" class="form-control" autocomplete="off">    
                        <?php if ($creat_role != COMPANY_ROLE_ID && (!empty($cmp_drop_down_flag))) { ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="md">Select Company *:</label>
                                <div class="col-lg-6">
                                    <select class="form-control cmp " name="cmp" id="cmp">
                                        <option value="">Select Company</option>
                                        <?php
                                        if (isset($all_cmp) && count($all_cmp) > 0) {
                                            foreach ($all_cmp as $cmp) {
                                                ?>
                                                <option value="<?php if (!empty($cmp['id'])) echo $cmp['id'] ?>">
                                                    <?php if (!empty($cmp['display_name'])) echo $cmp['display_name'], ': ', rokad_decrypt($cmp['mobile_no'], $this->config->item('pos_encryption_key')); ?>
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>   
                                </div>
                            </div>                            
                        <?php } if ($creat_role != MASTER_DISTRIBUTOR_ROLE_ID && (!empty($md_drop_down_flag))) { ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="md">Select Regional Distributor *:</label>
                                <div class="col-lg-6">
                                    <select class="form-control md " name="md" id="md">
                                        <option value="">Select Regional Distributor</option>
                                        <?php
                                        if (!empty($all_mds)) {
                                            foreach ($all_mds as $md) {
                                                ?>
                                                <option value="<?php if (!empty($md['id'])) echo $md['id'] ?>">
                                                    <?php if (!empty($md['display_name'])) echo $md['display_name'], ': ', rokad_decrypt($md['mobile_no'], $this->config->item('pos_encryption_key')); ?>
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>   
                                </div>
                            </div>
                        <?php } if ($creat_role != AREA_DISTRIBUTOR_ROLE_ID && (!empty($ad_drop_down_flag))) { ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="ad">Select Divisional Distributor *:</label>
                                <div class="col-lg-6">
                                    <select class="form-control  ad" name="ad" id="ad">
                                        <option value="">Select Divisional Distributor</option>
                                        <?php
                                        if (isset($area_distributor) && count($area_distributor) > 0) {
                                            foreach ($area_distributor as $ad) {
                                                ?>
                                                <option value="<?php if (!empty($ad['id'])) echo $ad['id'] ?>">
                                                    <?php if (!empty($ad['display_name'])) echo $ad['display_name'], ': ', rokad_decrypt($ad['mobile_no'], $this->config->item('pos_encryption_key')); ?>
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>   
                                </div>
                            </div>
                        <?php } if ($creat_role == RETAILER_ROLE_ID && (!empty($d_drop_down_flag))) { ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" for="distributor">Select Executive *:</label>
                                <div class="col-lg-6">
                                    <select class="form-control distributor  show_ret" name="distributor" id="distributor">
                                        <option value="">Select Executive</option>
                                        <?php
                                        if (isset($distributors) && count($distributors) > 0) {
                                            foreach ($distributors as $distributor) {
                                                ?>
                                                <option value="<?php if (!empty($distributor['id'])) echo $distributor['id'] ?>">
                                                    <?php if (!empty($distributor['display_name'])) echo $distributor['display_name'], ': ', rokad_decrypt($distributor['mobile_no'], $this->config->item('pos_encryption_key')); ?>
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>   
                                </div>
                            </div>
                        <?php } ?>
                        <!-- edited by harshada kulkarni on 12-07-2018 -start -->
                        <?php if ($creat_role == RETAILER_ROLE_ID) { ?>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group intent"> 
                                <label class="col-lg-3 control-label" for="service_name">Select Services *:</label>
                                <div class="col-lg-6 overflowClass" onload="showServices()">                                    
                                    <?php if (is_array($serviceDetails)) { ?>
                                        <?php foreach ($serviceDetails as $sd) { ?>
                                            <input type ="checkbox" name ="service_name[]" id ="service_name" value="<?php echo $sd->id; ?>" class="icheckbox_minimal"/>
                                            <?php echo $sd->service_name; ?><br>
                                            <?php
                                        }
                                    }
                                    ?> 

                                        <!--<select id="service_name" name="service_name[]" multiple class="form-control" >
                                    <?php if (is_array($serviceDetails)) { ?>
                                        <?php foreach ($serviceDetails as $sd) { ?>
                                                                                    <option value="<?php echo $sd->id ?>"><?php echo $sd->service_name; ?></option>                                          
                                            <?php
                                        }
                                    }
                                    ?>
                                        </select>-->
                                </div> 
                            </div>
                            <!-- edited by harshada kulkarni on 12-07-2018 -end -->
                            <br>
                            <br>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="badge_no">Badge no : </label>
                                <div class="col-lg-6">
                                    <input name="badge_no" type="text" id="badge_no"  maxlength="10" value="<?php echo set_value('badge_no'); ?>" class="form-control" autocomplete="off">
                                </div>
                            </div>
                        <?php } ?>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="aadhar_card">Aadhar Card No *:</label>
                            <div class="col-lg-6">
                                <input name="aadhar_card" type="text" id="aadhar_card" value="<?php echo set_value('aadhar_card'); ?>" class="form-control name" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-6">
                                <button>Get Details</button>
                            </div>
                        </div>
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="first_name">First Name *:</label>
                            <div class="col-lg-6">
                                <input name="first_name" type="text" id="first_name" value="<?php echo set_value('first_name'); ?>" class="form-control name" autocomplete="off">
                            </div>
                        </div>
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="last_name">Middle Name *:</label>
                            <div class="col-lg-6">
                                <input name="last_name" type="text" id="last_name" value="<?php echo set_value('last_name'); ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="last_name">Last Name *:</label>
                            <div class="col-lg-6">
                                <input name="last_name" type="text" id="last_name" value="<?php echo set_value('last_name'); ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="gender"> Gender : </label>
                            <div class="col-lg-6">
                                <select class="form-control " id="gender"  name="gender">
                                    <option value="">Select Gender</option>
                                    <option value="M">Male</option>
                                    <option value="F">Female</option>
                                    <option value="T">Transgender</option>
                                </select>
                                <span id="prov_type_msg" class="err"></span>
                            </div>
                        </div>
                     
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="dob">DOB (DD-MM-YYYY) *:</label>
                            <div class="col-lg-6">
                                <input name="dob" type="text" id="dob" value="<?php echo set_value('dob'); ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="age">Age *:</label>
                            <div class="col-lg-6">
                                <input name="age" type="text" id="age" value="<?php echo set_value('age'); ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <hr>
                        <section class="content-header">
                            <h4 style="margin-left:10px">Contact Information</h4>
                        </section>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">                                    
                            <label class="col-lg-3 control-label" for="address1">Address *:</label>
                            <div class="col-lg-6">
                                <input name="address1" type="text" id="address1" value="<?php echo set_value('address1'); ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label" for="agent_state">State *:</label>
                            <div class="col-lg-6">
                                <select class="state form-control " name="agent_state" id="agent_state">
                                    <option value="">Select State</option>
                                    <?php
                                    foreach ($states as $row => $value) {
                                        echo "<option value='" . $value['intStateId'] . "'>" . $value['stState'] . "</option>";
                                    }
                                    ?>
                                </select>   
                            </div>
                        </div>
                      <div class="clearfix" style="height: 10px;clear: both;"></div>

                          <div class="form-group">                                    
                            <label class="col-lg-3 control-label" for="address1">District *:</label>
                            <div class="col-lg-6">
                                <input name="address1" type="text" id="District" value="<?php echo set_value('District'); ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>
                      
                      <div class="clearfix" style="height: 10px;clear: both;"></div>

                          <div class="form-group">                                    
                            <label class="col-lg-3 control-label" for="address1">Taluka *:</label>
                            <div class="col-lg-6">
                                <input name="address1" type="text" id="Taluka" value="<?php echo set_value('Taluka'); ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">                                    
                            <label class="col-lg-3 control-label " for="agent_city">City *:</label>
                            <div class="col-lg-6">
                                <select class="agent_city form-control " name="agent_city" id="agent_city">
                                    <option value="">Select City</option>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label" for="pincode">Pin Code *:</label>
                            <div class="col-lg-6">
                                <input name="pincode" type="text" id="pincode" value="<?php echo set_value('pincode'); ?>" minlength = "6" maxlength="6" class="form-control" autocomplete="off">
                            </div>  
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        
                         <div class="form-group">
                            <label class="col-lg-3 control-label" for="mobile_no">Mobile no *: </label>
                            <div class="col-lg-6">
                                <input name="mobile_no" type="text" minlength = "10" maxlength="10" id="mobile_no" value="<?php echo set_value('mobile_no'); ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        
                         <div class="form-group">
                            <label class="col-lg-3 control-label" for="email_id">Email Id *: </label>
                            <div class="col-lg-6">
                                <input name="email_id" type="text" minlength = "10" maxlength="10" id="email_id" value="<?php echo set_value('email_id'); ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="occupation"> Occupation : </label>
                            <div class="col-lg-6">
                               
                                <select class="form-control " id="occupation"  name="occupation[]">
                                    <option value="">Please select occupation</option>
                                    <?php
                                    foreach ($occupationList as $key => $occupation) {  ?>
                                    
                                    <option value="<?php echo $occupation; ?>"><?php echo $occupation;?></option>
                                     <?php }?>
                                </select>
                                <span id="prov_type_msg" class="err"></span>
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="occupation"> Upload Photo : </label>
                            <div class="col-lg-6">
                                <input type="radio" class="form-control" name="profile_photo" value="web_cam" id="web_cam" checked>
                                <label for="web_cam"> Web Cam</label><br>
                                <input type="radio" class="form-control" name="profile_photo" value="upload" id="upload">
                                <label for="upload">Upload</label><br>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group" id="upload_profile_picture">
                            <label class="col-lg-3"></label>
                            <div class="col-lg-6"> 
                                <input type="file" class="form-control" id="photo" name="photo">
                                <span id="prov_type_msg" class="err"></span>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <hr>
                        <?php if ($creat_role == DISTRIBUTOR) { ?>  
                            <div class="distributor_show"> 
                                <section class="content-header">
                                    <h4 style="margin-left:10px">Other Information</h4>
                                </section>
                                <div class="clearfix" style="height: 10px;clear: both;"></div>

                                <div class="form-group distributor intent">
                                    <label class="col-lg-3 control-label" for="sellerpoint">Seller Point:* </label>
                                    <!--  <div class="col-lg-6">  
                                    <?php if (is_array($sellerpoints)) { ?>
                                        <div class="col-md-4 form-group">
                                         <input type ='checkbox' id="checkAllOnce" class='col-lg-1 checkAll' style="display: block;">
                                         <span>All</span>
                                         </div>     
                                        <?php foreach ($sellerpoints as $sellerpoint) { ?>
                                        <div class="col-md-4 form-group">
                                        <input type ='checkbox' id='seller' value='<?php echo $sellerpoint->BUS_STOP_CD; ?>' class='col-lg-1' name ='user_seller_point[]' style="display: block;" />
                                        <span><?php echo $sellerpoint->BUS_STOP_NM; ?></span> 
                                        </div>
                                                    <?php
                                                }
                                            }
                                            ?>                                  
                                             </div>   -->
                                    <div class="col-lg-6">
                                        <select name="user_seller_point[]" id="user_seller_point" class="form-control select2 " multiple="true">
                                            <option value="" id="select_all">All</option>
                                            <!--    <option value="" id="Deselect_all">De-Select All</option> -->
                                            <?php if (is_array($sellerpoints)) { ?>
                                                <?php foreach ($sellerpoints as $sellerpoint) { ?>
                                                    <option value="<?php echo $sellerpoint->BUS_STOP_CD; ?>"><?php echo $sellerpoint->BUS_STOP_NM . '  (' . $sellerpoint->BUS_STOP_CD . ')'; ?></option>
                                                <?php } ?> <?php } ?>

                                        </select>
                                        <h6>(Press "Ctrl" to select mutiple seller point)</h6>
                                    </div>  

                                    <span id="usersellerpointmsg" class="err"></span>
                                </div>
                                <div class="clearfix distributor" style="height: 10px;clear: both;"></div>

                            <?php } ?>

                            <?php if ($creat_role == RETAILER_ROLE_ID) { ?>
                                <div class="retailer_show"> 
                                    <section class="content-header">
                                        <h4 style="margin-left:10px">Other Information</h4>
                                    </section>
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <!-- <div class="form-group">
                                     <label class="col-lg-3 control-label" for="bType">Business Type:* </label>
                                     <div class="col-lg-6">
                                         <select name="bType" id="bType" class="form-control ">
                                             <option value="">Please select</option>  
                                    <?php //if(isset($btypes)) { ?> 
                                    <?php //foreach ($btypes as $btype){  ?>
                                                 <option value="<?php //echo $btype->btype_id;                      ?>"><?php //echo $btype->btype;                      ?></option>
                                    <?php //}   ?> 
                                    <?php //}    ?>
                                         </select>
                                     </div>                               
                                 </div> -->
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <!-- <div class="form-group retailer">
                                        <label class="col-lg-3 control-label" for="scheme">Select Scheme:* </label>
                                        <div class="col-lg-6">
                                            <select name="scheme" id="scheme" class="form-control ">
                                                <option value="">Please select</option>  
                                    <?php //if(is_array($schemes)) { ?> 
                                    <?php //foreach ($schemes as $scheme) {   ?>
                                                    <option value="<?php //echo $scheme->scheme_id;                       ?>"><?php //echo $scheme->scheme;                       ?></option>
                                    <?php //}    ?> <?php //}   ?>
                                            </select>
                                        </div>
                                        <span id="schememsg" class="err"></span>                               
                                    </div> -->
                                    <input name="bType" id="bType" type="hidden" value="0" class="form-control" autocomplete="off">

                                    <input name="scheme" id="scheme" type="hidden" value="0" class="form-control" autocomplete="off">

                                    <div class="clearfix retailer" style="height: 10px;clear: both;"></div>

                                    <div class="form-group retailer">
                                        <label class="col-lg-3 control-label" for="sellerpoint">Seller Point:* </label>
                                        <div class="col-lg-6">
                                            <select name="sellerpoint" id="sellerpoint" class="form-control select2 ">
                                                <option value="">Please select seller point</option>  
                                                <?php //if(!empty($sellerpoints) && is_array($sellerpoints)) {  ?>
                                                <?php //foreach ($sellerpoints as $sellerpoint) {   ?>
                                                    <!-- <option value="<?php //echo $sellerpoint->BUS_STOP_CD;                       ?>"><?php //echo $sellerpoint->BUS_STOP_NM.'  ('.$sellerpoint->BUS_STOP_CD.')';                       ?></option> -->
                                                <?php //}    ?> <?php //}   ?>

                                            </select>
                                        </div>  
                                        <span id="sellerpointmsg" class="err"></span>
                                    </div>
                                    <div class="clearfix retailer" style="height: 10px;clear: both;"></div>

                                    <div class="form-group retailer">
                                        <label class="col-lg-3 control-label" for="sellerdesc">Sellerpoint(description):* </label>
                                        <div class="col-lg-6">
                                            <input name="sellerdesc" id="sellerdesc" class="form-control " placeholder="Description" autocomplete="off" type="text">
                                        </div>
                                        <span id="sellerdescmsg" class="err"></span>
                                    </div>
                                    <div class="clearfix retailer" style="height: 10px;clear: both;"></div>

                                    <input name="topUpLimit" id="topUpLimit" type="hidden" value="0" class="form-control" autocomplete="off">

                                    <input name="hhmLimit" id="hhmLimit" type="hidden" value="0" class="form-control" autocomplete="off">

                                    <!-- <div class="form-group retailer">
                                         <label class="col-lg-3 control-label" for="topUpLimit">Top Up Limit:*</label>
                                            <div class="col-lg-6">
                                             <input name="topUpLimit" id="topUpLimit" class="form-control " placeholder="Top Up Limit" autocomplete="off" type="text">
                                            </div>
                                        <span id="topUpLimitmsg" class="err"></span>
                                    </div>
                                    <div class="clearfix retailer" style="height: 10px;clear: both;"></div>
                                    
                                    <div class="form-group retailer">
                                        <label class="col-lg-3 control-label" for="hhmLimit">HHM Limit:* </label>
                                        <div class="col-lg-6">
                                            <input name="hhmLimit" id="hhmLimit" class="form-control " placeholder="HHM Limit" autocomplete="off" type="text">
                                        </div>
                                        <span id="hhmLimitmsg" class="err"></span>
                                    </div> -->
                                    <div class="clearfix retailer" style="height: 10px;clear: both;"></div>
                                    <div class="form-group retailer">
                                        <label class="col-lg-3 control-label" for="depot_code">Depot Name:* </label>
                                        <div class="col-lg-6">
                                            <select name="depot_code" id="depot_code" class="form-control select2 ">
                                                <option value="">Please Select Depot Name</option>  
                                                <!-- <?php if (is_array($depots)) { ?>
                                                    <?php foreach ($depots as $depot) { ?>
                                                <option value="<?php echo $depot->DEPOT_CD; ?>"><?php echo $depot->DEPOT_NM; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?> -->
                                            </select>
                                        </div>  
                                        <span id="depot_codemsg" class="err"></span>
                                    </div>
                                    <div class="clearfix retailer" style="height: 10px;clear: both;"></div>
                                    <div class="form-group retailer">
                                        <label class="col-lg-3 control-label" for="securityDep">Security Deposit:* </label>
                                        <div class="col-lg-6">
                                            <input name="securityDep" id="securityDep" class="form-control " placeholder="Security Deposit" autocomplete="off" type="text">
                                        </div>
                                    </div>
                                    <div class="clearfix retailer" style="height: 10px;clear: both;"></div>
                                <?php } ?>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <div class="col-lg-offset-5">
                                    <button class="btn btn-primary" id="submit_btn" name="submit_btn" type="submit">Save</button> 
                                    <a class="btn btn-primary" href="<?php echo base_url('admin/registration') ?>" type="button">Back</a> 
                                </div>


                                <!--</div> /.box-body--> 
                            </div><!-- /.box -->
                            <?php echo form_close(); ?>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                    </section><!-- /.content -->
                </div><!-- /.content-wrapper -->


                <script>
                    var cmp_on_change_flag = '<?php echo $cmp_on_change_flag; ?>';
                    var md_on_change_flag = '<?php echo $md_on_change_flag; ?>';
                    var ad_on_change_flag = '<?php echo $ad_on_change_flag; ?>';
                    var show_retailor_flag = '<?php echo $show_retailor_flag; ?> ';
                    var creat_role = '<?php echo $creat_role; ?>';
                </script>

                <script src="<?php echo base_url() ?>js/back/admin/registration.js"></script>
                <style>
                    /****DATEPICKER YEAR MONTH DROPDOWNN *******/
                    .ui-datepicker .ui-datepicker-title select {
                        color: #000;
                    }
                </style>
                <script type="text/javascript">
                    /*   $(document).off('change', '.show_ret').on('change', '.show_ret', function(e)
                     {
                     $('.retailer_show').show();
                     });
                     var current_role = '<?php //echo $this->session->userdata('role_id');                        ?>';
                     var distributor_role = '<?php //echo DISTRIBUTOR ;                       ?>'
                     if(current_role == distributor_role)
                     {
                     $('.retailer_show').show();
                     }*/
                    // $(function() {
                    //         $('.retailer_show').hide(); 
                    //         $('#service_name').load(function(){
                    //             if($('#service_name').val() == '2') {
                    //                 $('.retailer_show').show(); 
                    //             } else {
                    //                 $('.retailer_show').hide(); 
                    //             } 

                    //         });
                    //    });

                    function showServices() {
                        $('.retailer_show').hide();
                        if ($('#service_name').val() == '2') {
                            $('.retailer_show').show();
                        } else {
                            $('.retailer_show').hide();
                        }
                    }

                    /*$(document).ready(function() {
                     $('#multiple-checkboxes').multiselect();
                     });*/

                    /*$(document).ready(function() {
                     $('#service_name').multiselect({
                     nonSelectedText: 'Select Services',
                     enableFiltering: true,
                     enableCaseInsensitiveFiltering: true,
                     buttonWidth: '510px'
                     });
                     });*/

                    $(document).ready(function() {
                        $("div#upload_profile_picture").hide();
                        $("input[value='upload']").click(function() {
                            alert();
                            
                            $("div#upload_profile_picture").show();
                            
                        });
                    });

                    // $(document).ready(function() {
                    //     $("#upload").click(function (){
                    //         $("#upload_profile_picture").css({'display':'block'});
                    //     });

                    // });

                    
                </script>          
