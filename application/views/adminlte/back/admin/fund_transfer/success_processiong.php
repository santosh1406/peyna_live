<section id="content">
   	<?php
		$attributes = array("method" => "POST", "id" => "success_form", "name" => "redirect","class" => "success_form");
		echo form_open(base_url().'admin/fund_transfer', $attributes);
	?>
	
    </form>
    
    <!-- Wait Start Here -->
    <div class="modal fade show_waiting" id="show_waiting" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <!-- tab container start-->
            <center>
                <div class="search_progress">
                    <button type="button" class="close" data-dismiss="modal" id="back_to">&times;</button>
                   <span><h4><i class="fa fa-inr"></i> <?php echo $amount; ?> Successfully Transfer To <?php echo $name; ?></h4></span>
                   <span class=""><img src="<?php echo base_url() ?>assets/travelo/front/images/loader/moving_money.gif" alt="BOS Loading"></span>
                </div>
            </center>
            <!-- tab container end-->
          </div>
        </div>
      </div>
    </div>
    <!-- Wait End Here -->
</section>
<style type="text/css">
.modal {text-align: center;}
.modal:before {display: inline-block;vertical-align: middle;content: " ";height: 50%;}
.modal-dialog {display: inline-block;text-align: left;vertical-align: middle;}
</style>

<script type="text/javascript">

    $(document).ready(function(){

        $('#show_waiting').modal({
                                backdrop: 'static',
                                keyboard: false  
                            })

        $("#show_waiting").modal("show");
        $(document).off('click', '#back_to').on('click', '#back_to', function(e)
        {
            window.location.href = BASE_URL + 'admin/fund_transfer';
        });
    }); 
    if (document.getElementById("success_form")) {
         setTimeout("submitForm()", 5000); // set timout 
     }
     function submitForm() { 
           document.getElementById("success_form").submit();
     }
</script>
