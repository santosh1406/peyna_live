
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3 style="font-family: Tahoma">
            &nbsp; Fund Transfer
        </h3>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-10">
                <div class="box">
                    <div class="box">
					
						<?php if($user_data['kyc_verify_status'] == 'N'){?>
						<h3 style="text-align: center;"> Your kyc status is pending for approval.</h3>
						<div class="modal-footer clearfix">
						</div>
						<?php } else {?>
					    <?php
							$attributes = array("method" => "POST", "id" => "prov_create_form", "name" => "prov_create_form","class" => "prov_create_form");
							echo form_open(base_url().'admin/fund_transfer/proceed', $attributes);
						?>
                          <div class="form-group">

                                <label class="col-lg-3 control-label" for="provider name" style="margin-top: -10px"><h4><b style="font-family: icon">Name</b></h4></label>
                                <div class="col-lg-3" style="margin-top: -10px">
                                
<!--                                     <input name="name" type="text" id="name" value="" class="form-control" placeholder="<?php echo $users_details[0]->display_name; ?>" readonly="" > -->
                               <label><h4><b style="font-family: icon"><?php echo $users_details[0]->first_name.' '.$users_details[0]->last_name; ?></b></h4></label>
                                </div>
                                <div class="col-lg-3 pull-right" style="margin-top: -23px">
                                    <h3><b style="color: peru">
                                            <i class="fa fa-inr"></i>
                                            <?php echo number_format($wallet_balance,2); ?></b>
                                    </h3>
                                </div>
                            </div><div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                               <label class="col-lg-3"><h5 style="font-family: Trebuchet MS"><b></b></h5></label>
                               <div class="col-lg-6">
                                <label ><i style="margin-left: 35px;margin-top:10px" class="    glyphicon glyphicon-arrow-down"></i></label>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            
                     <?php  if($role_id < COMPANY_ROLE_ID)
                      { ?>
                            <div class="form-group" id="cmp">
                                <label class="col-lg-3 control-label" for="company" style="font-family: times new roman"> Company</label>
                                <div class="col-lg-6">
                                    <select id="company" class="form-control chosen_select company" name="company">
                                    <option value="">Select Company</option>
                                    <?php
                                    if (isset($companies) && count($companies) > 0) {
                                    	foreach ($companies as $cmp) {
                                            ?>
                                            <option value="<?php if (!empty($cmp->id)) echo $cmp->id ?>"?>
                                            <?php if (!empty($cmp->first_name)) echo $cmp->first_name.' '.$cmp->last_name ?>
                                            </option>
                                        <?php }
                                    }
                                    ?>
                                </select>
                                </div>
                                 <div class="col-lg-3">
                                    <label class="cmp_wallet_amt" id="cmp_wallet_amt"></label>
                                </div>
                            </div>
                          <div class="clearfix" style="height: 10px;clear: both;"></div> 
                        <?php } ?>

                      <?php  if($role_id < MASTER_DISTRIBUTOR_ROLE_ID)
                      { ?>
                            <div class="form-group" id="md">
                                <label class="col-lg-3 control-label" for="op_addr_1" style="font-family: times new roman"> Master Distributor</label>
                                <div class="col-lg-6">
                                    <select id="md_type" class="form-control chosen_select master_di" name="md_type">
                                    <option value="">Select Master Distributor</option>
                                    <?php
                                    if (isset($master_distributor) && count($master_distributor) > 0) {
                                        foreach ($master_distributor as $md) {
                                            ?>

                                            <option value="<?php if (!empty($md->id)) echo $md->id ?>"?>
                                            <?php if (!empty($md->first_name)) echo $md->first_name.' '.$md->last_name ?>
                                            </option>
                                        <?php }
                                    }
                                    ?>
                                </select>
                                </div>
                                 <div class="col-lg-3">
                                    <label class="md_wallet_amt" id="md_wallet_amt"></label>
                                </div>
                            </div>
                          <div class="clearfix" style="height: 10px;clear: both;"></div> 
                        <?php } ?>
                         
                      <?php  if($role_id < AREA_DISTRIBUTOR_ROLE_ID)
                      { ?>
                             <div class="form-group" id="ad">
                                <label class="col-lg-3 control-label" for="op_addr_1" style="font-family: times new roman"> 

                                Area Distributor</label>
                                <div class="col-lg-6">
                                    <select id="area_di" class="form-control chosen_select area_di" name="area_di">
                                    <option value="">Select Area Distributor</option>
                                     <?php
                                    if (isset($area_distributor) && count($area_distributor) > 0) {
                                        foreach ($area_distributor as $ad) {
                                            ?>

                                            <option value="<?php if (!empty($ad->id)) echo $ad->id ?>"?>
                                            <?php if (!empty($ad->first_name)) echo $ad->first_name.' '.$ad->last_name ?>
                                            </option>
                                        <?php }
                                    }
                                    ?>
                                    
                                   </select>
                                </div>
                                <div class="col-lg-3">
                                    <label class="area_di_wallet_amt" id="area_di_wallet_amt"></label>
                                </div>
                            </div>
                            
                            <div class="clearfix" style="height: 10px;clear: both;"></div> 
                            <?php } ?>
                              
                       <?php  if($role_id < DISTRIBUTOR)
                      { ?>       
                              
                            <div class="form-group" style="display: block;" id="di">
                                <label class="col-lg-3 control-label" for="op_addr_1" style="font-family: times new roman"> 
                                Agent</label>
                               
                                <div class="col-lg-6">
                                    <select id="only_di" class="form-control chosen_select only_di" name="only_di">
                                   <option value="">Select Agent</option> 
                                    <?php
                                    if (isset($distributor) && count($distributor) > 0) {
                                        foreach ($distributor as $ad) {
                                            ?>

                                            <option value="<?php if (!empty($ad->id)) echo $ad->id ?>"?>
                                            <?php if (!empty($ad->first_name)) echo $ad->first_name.' '.$ad->last_name ?>
                                            </option>
                                        <?php }
                                    }
                                    ?>
                                   </select>
                                </div><div class="col-lg-3">
                                    <label class="only_di_wallet_amt" id="only_di_wallet_amt"></label>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div> 
                            <?php }?>
                            
                            <div class="form-group" style="display: block;" id="ret">
                                <label class="col-lg-3 control-label" for="op_addr_1" style="font-family: times new roman"> Sales Executive</label>
                                <div class="col-lg-6">
                                    <select id="retailer" class="form-control chosen_select retailer" name="retailer">
                                   <option value="">Select Sales Executive</option>
                                    <?php
                                    if (isset($retailer) && count($retailer) > 0) {
                                        foreach ($retailer as $ad) {
                                            ?>

                                            <option value="<?php if (!empty($ad->id)) echo $ad->id ?>"?>
                                            <?php if (!empty($ad->first_name)) echo $ad->first_name.' '.$ad->last_name ?>
                                            </option>
                                        <?php }
                                    }
                                    ?>
                                   </select>
                                </div>
                                <div class="col-lg-3">
                                    <label class="retailer_wallet_amt" id="retailer_wallet_amt"></label>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">

                                <label class="col-lg-3 control-label" for="amount" style="font-family: times new roman">Amount</label>
                                <div class="col-lg-6">
                                    <?php //echo uniqid();die;?>
                                    <input name="amount" type="text" id="amount" value="" class="form-control" placeholder="Amount" maxlength="6">
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" for="remark" style="font-family: times new roman">Remark</label>
                                <div class="col-lg-6">
                                <input type="text" placeholder="Remark" id="remark" name="remark"  class="form-control" autocomplete="off" />
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-4">
                                        <button class="btn btn-primary" id="save_prov_data" name="save_prov_data" type="submit">Proceed</button> 
                                        <!-- <a class="btn btn-primary" href="<?php echo base_url('master/discount') ?>" type="button">Back</a>  -->
                                    </div>
                                </div>

                        </form>
						
						<?php } ?>		
						
                    </div><!-- /.box-body -->
                <!--</div> /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<script>
    $(document).ready(function() {

    	 $(document).off('change', '.company').on('change', '.company', function(e) {

             $("#md_type").html("<option value=''>Please wait..</option>");
             var detail  =   {};
             var div     =  '';
             var str     =  "";
             var form    =  '';
             var ajax_url ='admin/fund_transfer/get_md_data';

             detail['id'] = $("#company").find('option:selected').attr('value');
     
             if(detail['id'] != '') {
                 $("#md_type").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
                 get_data(ajax_url, form, div, detail, function(response) {
                     if(response.response_data != 0.00) {
                         $("#md_type").html("<option value=''>Select Master Distributor</option>");
                         $.each(response.response_data, function(i,value) {
                           $("#md_type").append("<option value="+value.id+">"+value.display_name+"</option>");
                           $(".chosen_select").trigger("chosen:updated");
                         });
                     }
                     else {
                         $("#md_type").html("<option value=''>No result found</option>").trigger('chosen:updated');    
                     }
                     $("#cmp_wallet_amt").html("<i class='fa fa-inr'></i>&nbsp;"+"<font color='black'>"+response.wallet_balance+"</font>");
                 }, '', false);
             }
             else {
            	 $("#md_type").html("<option value=''>Select Master Distributor</option>").trigger('chosen:updated'); 
                 $("#area_di").html("<option value=''>Select Area Distributor</option>").trigger('chosen:updated'); 
                 $(".only_di").html("<option value=''>Select Agent</option>").trigger('chosen:updated'); 
                 $(".retailer").html("<option value=''>Select Sales Executive</option>").trigger('chosen:updated'); 
                 $("#cmp_wallet_amt").html("");
                 $("#md_wallet_amt").html("");
                 $("#area_di_wallet_amt").html("");
                 $("#only_di_wallet_amt").html("");
                 $("#retailer_wallet_amt").html("");
             }
         });

        $(document).off('change', '.master_di').on('change', '.master_di', function(e) {

                $("#area_di").html("<option value=''>Please wait..</option>");
                var detail  =   {};
                var div     =  '';
                var str     =  "";
                var form    =  '';
                var ajax_url ='admin/fund_transfer/get_ad_data';

                detail['id'] = $("#md_type").find('option:selected').attr('value');
        
                if(detail['id'] != '') {
                    $("#area_di").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
                    get_data(ajax_url, form, div, detail, function(response) {
                        if(response.response_data != 0.00) {
                            $("#area_di").html("<option value=''>Select Area Distributor</option>");
                            $.each(response.response_data, function(i,value) {
                              $("#area_di").append("<option value="+value.id+">"+value.display_name+"</option>");
                              $(".chosen_select").trigger("chosen:updated");
                            });
                        }
                        else {
                            $("#area_di").html("<option value=''>No result found</option>").trigger('chosen:updated');    
                        }
                        $("#md_wallet_amt").html("<i class='fa fa-inr'></i>&nbsp;"+"<font color='black'>"+response.wallet_balance+"</font>");
                    }, '', false);
                }
                else {
                    $("#area_di").html("<option value=''>Select Area Distributor</option>").trigger('chosen:updated'); 
                    $(".only_di").html("<option value=''>Select Agent</option>").trigger('chosen:updated'); 
                    $(".retailer").html("<option value=''>Select Sales Executive</option>").trigger('chosen:updated'); 
                    $("#md_wallet_amt").html("");
                    $("#area_di_wallet_amt").html("");
                    $("#only_di_wallet_amt").html("");
                    $("#retailer_wallet_amt").html("");
                }
            });


            $(document).off('change', '.area_di').on('change', '.area_di', function(e) {
                //alert("asdas");
                $("#only_di").html("<option value=''>Please wait..</option>");
                var detail  =   {};
                var div     =  '';
                var str     =  "";
                var form    =  '';
                var ajax_url ='admin/fund_transfer/get_distributor_data';

                detail['id'] = $("#area_di").find('option:selected').attr('value');
        
                if(detail['id'] != '') {
                    $("#only_di").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
                    get_data(ajax_url, form, div, detail, function(response) {
                        if(response.response_data!= 0.00) {
                            $("#only_di").html("<option value=''>Select Agent</option>");
                            $.each(response.response_data, function(i,value) {
                              $("#only_di").append("<option value="+value.id+">"+value.display_name+"</option>");
                              $(".chosen_select").trigger("chosen:updated");
                            });
                        }
                        else {

                            $("#only_di").html("<option value=''>No result found</option>").trigger('chosen:updated');       
                        }
                         $("#area_di_wallet_amt").html("<i class='fa fa-inr'></i>&nbsp;"+"<font color='black'>"+response.wallet_balance+"</font>");
                    }, '', false);
                }
                else {
                    $(".only_di").html("<option value=''>Select Agent</option>").trigger('chosen:updated'); 
                    $(".retailer").html("<option value=''>Select Sales Executive</option>").trigger('chosen:updated'); 
                    $("#area_di_wallet_amt").html(""); 
                    $("#only_di_wallet_amt").html("");
                    $("#retailer_wallet_amt").html("");
                }
            });

            $(document).off('change', '.only_di').on('change', '.only_di', function(e) {
                $("#retailer").html("<option value=''>Please wait..</option>");
                var detail  =   {};
                var div     =  '';
                var str     =  "";
                var form    =  '';
                var ajax_url ='admin/fund_transfer/get_retailer_data';

                detail['id'] = $("#only_di").find('option:selected').attr('value');
        
                if(detail['id'] != '') {
                    $("#retailer").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
                    get_data(ajax_url, form, div, detail, function(response) {
                        if(response.response_data != 0.00) {
                            $("#retailer").html("<option value=''>Select Sales Executive</option>");
                            $.each(response.response_data, function(i,value) {
                              $("#retailer").append("<option value="+value.id+">"+value.display_name+"</option>");
                              $(".chosen_select").trigger("chosen:updated");
                            });
                        }
                        else {

                            $("#retailer").html("<option value=''>No result found</option>").trigger('chosen:updated'); 

                           /* $("#retailer").html("<option value=''>Select Sales Executive</option>").trigger('chosen:updated');    */
                        }
                         $("#only_di_wallet_amt").html("<i class='fa fa-inr'></i>&nbsp;"+"<font color='black'>"+response.wallet_balance+"</font");
                    }, '', false);
                }
                else {
                    $("#retailer").html("<option value=''>Select Sales Executive</option>").trigger('chosen:updated'); 
                    $("#only_di_wallet_amt").html("");
                    $("#retailer_wallet_amt").html("");
                }
            });
            
             $(document).off('change', '.retailer').on('change', '.retailer', function(e) {
                var detail  =   {};
                var div     =  '';
                var str     =  "";
                var form    =  '';
                var ajax_url ='admin/fund_transfer/retailer_wallet_amount';

                detail['id'] = $("#retailer").find('option:selected').attr('value');
        
                if(detail['id'] != '') {
                    get_data(ajax_url, form, div, detail, function(response) {
                         $("#retailer_wallet_amt").html("<i class='fa fa-inr'></i>&nbsp;"+"<font color='black'>"+response.wallet_balance+"</font");
                    }, '', false);
                }
                else {
                }
            });
        

        });

</script>
<script type="text/javascript">
 $(document).off('keypress', '#amount').on('keypress', '#amount', function(e) 
        {
           var regex = new RegExp("^[0-9\b]+$");//accept only _
           var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
             key=e.charCode? e.charCode : e.keyCode;
            if(key==9 )
            {
                return true; 
            }
            if (regex.test(str)) 
            {
                return true;
            }
            e.preventDefault();
            return false;
        });
</script>
<script>
    $(document).ready(function () {
        $('#prov_create_form').validate({ 
             rules: {
                amount: {
                    required: true,
                    number:true
                 },
                 remark: {
                    required: true,
                 }
            },
            messages: {
                amount: "Please enter valid amount.",
                remark: "Please enter remark",
            },
            submitHandler: function () { 
                submit.form();
            }
        });
    });
</script>
