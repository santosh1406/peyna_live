<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3>
            &nbsp; Fund Transfer Details
        </h3>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-10">
                <div class="box">
                    <div class="box">
					    <?php
							$attributes = array("method" => "POST", "id" => "prov_create_form", "name" => "prov_create_form","class" => "prov_create_form");
							echo form_open(base_url().'admin/fund_transfer/confirm_transaction', $attributes);
						?>
                        
                        <input name="from_userid" type="hidden" id="name" class="form-control" value="<?php echo $transfer_from_userid; ?>" >
                       <input name="to_userid" type="hidden" id="to_userid" value="<?php echo $transfer_to_userid; ?>" class="form-control" >
                         
                        <input name="from_bal" type="hidden" id="from_bal" class="form-control" value="<?php echo $transfer_from_wallet_bal; ?>" >
                       <input name="to_bal" type="hidden" id="to_bal" value="<?php echo $transfer_to_wallet_bal; ?>" class="form-control" >
                       <input name="transfer_amount" type="hidden" id="transfer_amount" value="<?php echo $transfer_amount; ?>" class="form-control" >
                       <input name="fund_transfer_remark" type="hidden" id="fund_transfer_remark" value="<?php echo $fund_transfer_remark; ?>" class="form-control" >

                       <!--  <div class="form-group">
                             
                                <label class="col-lg-3 control-label" for="provider name" style="font-family: times new roman">Fund Transfer From<b class="pull-right">:</b></label>
                                <div class="col-lg-6">
                                    <label style="font-family: times new roman"><b><?php echo $transfer_from_firstname." ".$transfer_from_lastname; ?></b></label>
                                </div>
                                <div class="col-lg-3 pull-right" style="margin-top: -20px">
                                    <h4 style="margin-top:20px;">
                                            <i class="fa fa-inr"></i>
                                            <?php  echo number_format($transfer_from_wallet_bal,2) ?>
                                    </h4>
                                </div>
                            </div> -->
                             <div class="form-group">

                                <label class="col-lg-3 control-label" for="provider name" style="margin-top: -10px"><h5><b style="font-family: icon">From</b><b class="pull-right">:</b></h5></label>
                                <div class="col-lg-5" style="margin-top: -10px">
                                
<!--                                     <input name="name" type="text" id="name" value="" class="form-control" placeholder="<?php echo $users_details[0]->display_name; ?>" readonly="" > -->
                               <label><h5><b style="font-family: icon"><?php echo $transfer_from_firstname." ".$transfer_from_lastname; ?></b></h5></label>
                                </div>
                                <div class="col-lg-3 pull-right" style="margin-top: -8px">
                                    <h4><b style="color: peru">
                                            <i class="fa fa-inr"></i>
                                            <?php echo number_format($transfer_from_wallet_bal,2); ?></b>
                                    </h4>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">

                                <label class="col-lg-3 control-label" for="provider name"></label>
                                <div class="col-lg-6">
                                   <i class="glyphicon glyphicon-circle-arrow-down" style="margin-left: 20px"></i>
                                </div>
                            </div><div class="clearfix" style="height: 10px;clear: both;"></div>
                            <!-- <div class="form-group">

                                <label class="col-lg-3 control-label" for="provider name" style="font-family: times new roman">Fund Transfer To<b class="pull-right">:</b></label>
                                <div class="col-lg-6"> -->
                                    <!-- <input name="transfer_to" type="text" id="transfer_to" value="<?php  echo $benefaciary_details[0]->first_name." ".$benefaciary_details[0]->last_name ?>" class="form-control" readonly > -->
                                    <!-- <label style="font-family: times new roman"><b><?php  echo $benefaciary_details[0]->first_name." ".$benefaciary_details[0]->last_name ?></b></label>
                                </div>
                                <div class="col-lg-3 pull-right" style="margin-top: -20px">
                                     <h4 style="margin-top:20px;">
                                            <i class="fa fa-inr"></i>
                                            <?php  echo number_format($transfer_to_wallet_bal,2) ?>
                                    </h4>
                                </div>
                            </div> -->
                             <div class="form-group">

                                <label class="col-lg-3 control-label" for="provider name" style="margin-top: -10px"><h5><b style="font-family: icon">To</b><b class="pull-right">:</b></h5></label>
                                <div class="col-lg-5" style="margin-top: -10px">
                               <label><h5><b style="font-family: icon"><?php  echo $benefaciary_details[0]->first_name." ".$benefaciary_details[0]->last_name ?></b></h5></label>
                                </div>
                                <div class="col-lg-3 pull-right" style="margin-top: -10px">
                                    <h4><b style="color: peru">
                                            <i class="fa fa-inr"></i>
                                            <?php echo number_format($transfer_to_wallet_bal,2); ?></b>
                                    </h4>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">

                                <label class="col-lg-3 control-label" for="provider name" style="margin-top: -10px"><h5><b style="font-family: icon">Amount</b><b class="pull-right">:</b></h5></label>
                                <div class="col-lg-5" style="margin-top: -10px">
                                <label><h5><b style="font-family: icon"><?php echo $transfer_amount ?></b></h5></label>
                                </div>
                              
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">

                                <label class="col-lg-3 control-label" for="provider name" style="margin-top: -10px"><h5><b style="font-family: icon">T-PIN</b><b class="pull-right">:</b></h5></label>
                                <div class="col-lg-5" style="margin-top: -10px">
                                  <input name="txpn" type="password" id="txpn" value="" class="form-control" placeholder="Please enter valid Transaction pin" maxlength="4" autocomplete="off">
                                </div>
                              
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-4">
                                        <button class="btn btn-primary confirm_transaction" id="save_prov_data" name="save_prov_data" type="" onclick="">Confirm</button> 
                                         <a class="btn btn-primary back" type="button">Back</a> 
                                    </div>
                                </div>

                        </form>
                    </div><!-- /.box-body -->
                <!--</div> /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<script>
    $(document).ready(function() {
        
        $('.select-only').bind("cut copy paste drop keyup keypress",function(e) {
            e.preventDefault();
        });

        $('#txpn').contextmenu(function() {
            return false;
        });      

        $(document).off('click', '.back').on('click', '.back', function(e)
        {

            window.location.href = BASE_URL + 'admin/fund_transfer';
        });
        $(document).off('keypress', '#txpn').on('keypress', '#txpn', function(e) 
        {
           var regex = new RegExp("^[0-9\b]+$");//accept only _
           var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
             key=e.charCode? e.charCode : e.keyCode;
            if(key==9 )
            {
                return true; 
            }
            if (regex.test(str)) 
            {
                return true;
            }
            e.preventDefault();
            return false;
        });

        });

</script>
<script>

$("#prov_create_form").submit(function(e){
     e.preventDefault();
     check_tpn();
  });
function check_tpn(){
               
                var detail   =   {};
                var div      =  '';
                var str      =  "";
                var form     =  '';
                var ajax_url = 'admin/fund_transfer/check_tpn';

                detail['id'] = "<?php echo $transfer_from_userid; ?>";
                detail['txpn'] = $('#txpn').val();
        
                if(detail['id'] != '') {
                    get_data(ajax_url, form, div, detail, function(response) {
                         if(response.valid_tpn == 'true')
                           {
                                $('#prov_create_form').unbind().submit();
                                $('#save_prov_data').prop('disabled',true);

                         }else{
                                
                                 alert('Please Enter Valid T-PIN');
                                                                 
                         } 
                    }, '', false);
                }
                else {
                }
     }       
</script>
