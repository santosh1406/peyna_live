<section class="content-header">
    <h1>User Wallet List</h1>
</section>

<!-- Main content -->
<section class="content">
    <div id="menu_wrapper">
        <?php $this->load->view(USER_WALLET_VIEW); ?>
    </div>
</section>
<script type="text/javascript">
$(document).ready(function () {
    
    var tconfig = {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": BASE_URL + "admin/user_wallet/get_user_wallet",
            "type": "POST",
            "data": "json",
             data  : function (d) {
             d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;
            }
        },
        // "sAjaxSource": BASE_URL+'/data_table_eg/get_list1',
        "iDisplayLength": 10,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        "order": [[4, "desc"]],
        "paging": true,
        "searching": true,
        "aoColumnDefs": [
            {
                "bSortable": false,  
                "aTargets": [0]
            },
          ] ,    
        "fnRowCallback": function(nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:first", nRow).html(info.start + iDisplayIndex + 1);
            // $("td:first", nRow).html(iDisplayIndex + 1);  
            return nRow;
        },
    };
   
    var oTable = $('#user_display_table').dataTable(tconfig);

    /* Added code for exporting result in excel format*/
     $('#walletviewexcel').on('click', function(e) {
        window.location.href=BASE_URL+"admin/user_wallet/get_user_wallet_excel";
     });    
});
</script>
