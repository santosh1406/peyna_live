<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <button  class="btn btn-primary pull-right" id="walletviewexcel" type="button">
                <span class="glyphicon glyphicon-export pull"></span>
                                                        Export to Excel</button>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="user_display_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="20px;">Sr. No.</th>
                            <th>User Name</th>
                            <th>Agent Code</th>
                            <th>RD Name</th>
                            <th>DD Name</th>
                            <th>User Code</th>
                            <th>User Type</th>
                            <th>Balance(Rs.)</th>
                            <th>CB Agent</th>
                            <th width="55px">View Transaction</th>
                        </tr>
                        
                    </thead>
                    <tbody>                                           
                      
                    </tbody>

                </table>
           </div>
        </div>
    </div>
</div>
