<section class="content">
    <div id="wallet_wrapper">
        <div class="box-header">
            <h3 class="box-title pull-left">Users Wallet Transactions</h3>
        </div>
        <br><!-- /.box-header -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body table-responsive" id="table_show">
                        <?php 
                        $attributes = array('class' => 'form', 'id' => 'users_wallet_filter', 'name'=>'users_wallet_filter','method' => 'post','onsubmit'=>'return false;');
                        echo form_open('', $attributes);
                        ?>
                        <div class="row box-title">
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-1">From Date</label>
                                    <input type="text" class="form-control" id="from_date" name="from_date" placeholder="From Date" >
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2">Till date</label>
                                    <input type="text" class="form-control" id="till_date" name="till_date" placeholder="Till Date">
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2">Credit/Debit</label>
                                    <select class="form-control" id="tran_type" name="tran_type" >
                                        <option value="">Credit/Debit</option>
                                        <option value="Credited">Credit</option>
                                        <option value="Debited">Debit</option>
                                    </select> 
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2">Select Reason</label>
                                    <select class="form-control chosen_select" name="tran_reason" id="tran_reason">
                                        <option value="">Select Reason</option>     
                                        <?php foreach($tran_reason as $tran_reason):?>
                                            <option value="<?php echo $tran_reason['id']?>"><?php echo $tran_reason['title']?></option>
                                        <?php endforeach;?>    
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div style="display: block;" class="col-md-1">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2"></label>
                                    <button class="btn btn-primary" id="users_wallet_filter_btn" name="users_wallet_filter_btn" type="submit">Filter</button> 
                                </div>  
                            </div>
                            <div style="display: block;" class="col-md-1">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2"></label>
                                    <button  class="btn btn-primary" id="extoexcel" type="button">
                                        <span class="glyphicon glyphicon-export"></span>
                                        Export</button>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                        </div>
                        <?php echo form_close(); ?>

                        <table id="users_wallet_trans" name="data-result" class="table table-bordered table-hover" >
                            <thead><tr>
                                <th>Sr.No.</th>
                                <th>User Name</th>
                                <th>User Code</th>
                                <th>User Type</th>
                                <th>Credited/ Debited By</th>
                                <th>Opening Balance (Rs.)</th>
                                <th>Transaction Type</th>
                                <th>Amount (Rs.)</th>
                                <th>Closing Balance (Rs.)</th>
                                <th>Transaction date</th>
                                <th>Comment</th>
                            </tr></thead>
                            <tbody>
                            </tbody>
                        </table>

                        <div class="clearfix" style="margin-bottom:15px">
                            <a href="<?php echo base_url() ?>report/report/users_wallet_transactions/" class="btn btn-primary pull-right">  Back </a>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    
$(document).ready(function() {
    var oTables;
    var date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    
    $("#from_date").val($.datepicker.formatDate("dd-mm-yy", firstDay));
    $("#till_date").val($.datepicker.formatDate("dd-mm-yy", date));

    var wallet_trans_config = {
        processing: true,
        serverSide: true,
        ajax: {
            url: BASE_URL+"report/report/get_users_wallet_trans_detail/",
            type: 'POST',
             data: function (d) {
                        d.from_date = $("#from_date").val();
                        d.till_date = $("#till_date").val();
                        d.tran_type = $("#tran_type option:selected").val();
                        d.tran_reason = $("#tran_reason option:selected").val();
                        d.rokad_token = rokad_token;
                    },     
        },
        columnDefs: [{
            searchable: false
        }],
        order: [ [ 2, 'asc' ], [ 9, "asc" ]],
        iDisplayLength: 50,
        aLengthMenu: [[5, 10, 50, -1], [5, 10, 50, "All"]],
        paginate: true,
        paging: true,
        searching: true,
        aoColumnDefs: [{
                bSortable: false,  
                aTargets: [0]
            },
        ],
        "oLanguage": {
                "sProcessing": '<center><img src="'+ base_url +'assets/adminlte/back/img/ajax-loader.gif" class="img-circle centered" alt="user image"/></center>' 
            },  
        "fnRowCallback": function(nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            $("td:first", nRow).html(info.start + iDisplayIndex + 1);
            return nRow;
        },
        fnDrawCallback: function(data) {
            if (oTables.fnGetData().length == 0) {
                $("#extoexcel").attr("disabled", "disabled"); 
            } else {
                $("#extoexcel").removeAttr("disabled"); 
            }
        },
    };

    oTables = $('#users_wallet_trans').dataTable(wallet_trans_config);

    $('#users_wallet_filter').on('submit', function(e) {
        e.preventDefault();
        oTables.fnDraw();
    });
    
    $.validator.addMethod("checkDepend",function(value, element) {
        if($("#from_date").val() != "" &&  $("#till_date").val() != "") {   
            var date_diff = date_operations($("#from_date").val(),$("#till_date").val());
            if(date_diff.date_compare) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    },"Till date must be greater than from date.");

    $('#users_wallet_filter').validate({
        ignore: "",
        rules: {
            till_date : {
                checkDepend : true
            }
        },
        messages: {
            till_date: {

            }
        }
    }); 
        
    $( "#from_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        maxDate :0
        });
    $( "#till_date" ).datepicker({
        'dateFormat' : 'dd-mm-yy',
        maxDate:0
     });
     

    $('#extoexcel').on('click', function(e) {
        var formData = $("#users_wallet_filter").serialize();
        window.location.href=BASE_URL+"report/report/users_wallet_transaction_report_excel?"+formData;
    });
     
});

</script>
