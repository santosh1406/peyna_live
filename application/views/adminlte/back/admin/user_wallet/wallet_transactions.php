<script type="text/javascript">
    var wallet_id = '<?php echo $id ?>';
</script>
<script type="text/Javascript" src="<?php echo base_url() ?>js/back/user_wallet/wallet_trans.js" ></script>
<section class="content">
    <div id="wallet_wrapper">
        <div class="box-header">
            <h3 class="box-title pull-left">Wallet Transactions</h3>
            <h5 class="box-title pull-right">User Name: <?php echo $user_data[0]['first_name']. " ".$user_data[0]['last_name'];?> <br>User Code  : <?php echo $user_data[0]['id'];?></h5>
        </div>
        <br><!-- /.box-header -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                        <div class="box-body table-responsive" id="table_show">
                            
                            <?php 
                                $attributes = array('class' => 'form', 'id' => 'user_wallet_filter', 'name'=>'user_wallet_filter','method' => 'post','onsubmit'=>'return false;');
                                echo form_open('', $attributes);
                            ?>
                                <div class="row box-title">
                                   
                                        <div style="display: block;" class="col-md-2 display-no">
                                                <div style="position: static;" class="form-group">
                                                    <label for="input-text-1">From Date</label>
                                                    <input type="text" class="form-control" id="from_date" name="from_date" placeholder="From Date" >
                                                 </div>
                                        </div>
                                         <div style="display: block;" class="col-md-2 display-no">
                                                    <div style="position: static;" class="form-group">
                                                        <label for="input-text-2">Till date</label>
                                                        <input type="text" class="form-control" id="till_date" name="till_date" placeholder="Till Date">
                                                    </div>
                                         </div>
                                           <div style="display: block;" class="col-md-2 display-no">
                                                <div style="position: static;" class="form-group">
                                                           <label for="input-text-2">Credit/Debit</label>
                                                            <select class="form-control" id="tran_type" name="tran_type" >
                                                                <option value="">Credit/Debit</option>
                                                                <option value="Credited">Credit</option>
                                                                <option value="Debited">Debit</option>
                                                            </select> 
                                                </div>
                                           </div>
                                            <div style="display: block;" class="col-md-2 display-no">
                                                <div style="position: static;" class="form-group">
                                                    <label for="input-text-2">Select Reason</label>
                                                         <select class="form-control chosen_select" name="tran_reason" id="tran_reason">
                                                             <option value="">Select Reason</option>     
                                                                     <?php foreach($tran_reason as $tran_reason):?>
                                                                             <option value="<?php echo $tran_reason['id']?>"><?php echo $tran_reason['title']?></option>
                                                                     <?php endforeach;?>    
                                                           </select>
  
                                                </div>
                                           </div>
                                           <div style="display: block;" class="col-md-2">
                                                <div style="position: static;" class="form-group">
                                                    <label for="input-text-2"></label>
                                                    <button class="btn btn-primary" id="agent_view_filter_btn" name="agent_view_filter_btn" type="submit">Filter</button> 
                                                </div>  
                                           </div>
                                            <div style="display: block;" class="col-md-2">
                                                <div style="position: static;" class="form-group">
                                                      <label for="input-text-2"></label>
                                                      <button  class="btn btn-primary" id="extoexcel" type="button">
                                                      <span class="glyphicon glyphicon-export"></span>
                                                        Export</button>
                                                </div>
                                            </div>

                                            <div class="clearfix" style="height: 10px;clear: both;"></div>

                                </div>
                            <?php echo form_close(); ?>

                            <table id="user_wallet_trans" name="data-result" class="table table-bordered table-hover" >
                                <thead><tr>
                                        <th>Sr. No.</th>
                                        <th>Opening Balance</th>
                                        <th>Transaction Type</th>
                                        <th>Closing Balance</th>
                                        <th>Amount</th>
                                        <th>Transaction date</th>
                                        <th>Comment</th>
                                    </tr></thead>
                                <tbody>
                                </tbody>
                            </table>

                           
                         <div class="clearfix" style="margin-bottom:15px">
                        <a href="<?php echo base_url() ?>admin/user_wallet/" class="btn btn-primary pull-right">  Back </a>
                         </div> 
                        </div>
                    </div>
                  
            </div>
        </div>
    </div>
</section>
