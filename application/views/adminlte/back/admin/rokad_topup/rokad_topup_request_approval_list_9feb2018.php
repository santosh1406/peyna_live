<script type="text/javascript">
var role_name = '0';
<?php if (in_array(strtolower($this->session->userdata('role_name')), array('admin', 'super admin'))) { ?>
        role_name = '1';
<?php } ?>
</script>
<style>
.fields{display:none;}
.reject{display:none;}
.row{margin-bottom:5px;} 
</style>
<section class="content-header">
    <h1>Rokad Topup Approval List</h1>
    <div align="right">
        <?php if ($kyc == 'Y' && $vip_role = '0') {
          
        } else { ?>
        <!--<button class="btn btn-primary add_btn">Fund Request Approval List</button>--> 
        <?php } ?>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="commission_wrapper">
        <div class="box">
            
            <div class="box-body">

                <div class="alert alert-success alert-dismissible no_margin mb10 hide" role="alert" id="alert_msg">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="body"></div>
                </div>
			    <?php
				$attributes = array("method" => "POST", "id" => "approval_list", "name" => "approval_list","class" => "approval_list");
				echo form_open('', $attributes);
                                ?><div class="table-responsive">
                <table id="rokad_topup_approval" name="rokad_topup_approval" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th  width="10%">ID</th>
							<th>Date Time</th>
							<th>Amount</th>
							<th>Trans Mode</th>
							<th>Remark</th>
							<th>Status </th>
							<th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>	</div>	    
                </form>		    
                <div class="clearfix"></div>
            </div><!-- /.box-body -->
            <div class="box-footer">

                <div class="clearfix"></div>
                    <!-- <code>.box-footer</code> -->
            </div><!-- /.box-footer-->
        </div>
    </div>
</section>
<script type="text/javascript">
    var frm = document.Filter_Frm;
    
    $(document).ready(function () {
       		
			
		DatatableRefresh();

       });
	   
	function DatatableRefresh() {
		
		$('#rokad_topup_approval').dataTable({
			"aoColumnDefs": [ {"bSortable": true, "aTargets": [0,6] } ], 
			//"scrollY": "600",
			"deferRender": true,
			"bProcessing": true,
			"bServerSide": true,
			"order": [[ 0, 'desc' ]],
			"lengthMenu": [
				[10, 50, 100, -1],
				[10, 50, 100, "All"] // change per page values here
			],
			"bScrollCollapse": true,
			"bAutoWidth": true,
			"sPaginationType": "full_numbers",
			"bDestroy": true,
			 "oLanguage": {
                "sProcessing": '&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url().BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>' 
            },
			"sAjaxSource": "<?php echo base_url().'admin/rokad_topup/DataTableTopupApproval'; ?>",
			"fnServerData": function (sSource, aoData, fnCallback) {
				    $.getJSON(sSource, aoData, function (json) {
					fnCallback(json);
				});
			},
			"fnRowCallback": function (nRow, aData, iDisplayIndex) {

				return nRow;
			},
			"fnFooterCallback": function (nRow, aData) {
			}
		});

	}
	


    function ResetFilter() {
		frm.reset();
		DatatableRefresh();
	}
	function setFilter() {
		DatatableRefresh();
	}	
</script>