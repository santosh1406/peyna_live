<script type="text/javascript">
var role_name = '0';
<?php if (in_array(strtolower($this->session->userdata('role_name')), array('admin', 'super admin'))) { ?>
        role_name = '1';
<?php } ?>
</script>
<style>
.fields{display:none;}
</style>
<section class="content-header">
    <h1>Rokad Topup Request List</h1>
    <div align="right">
        <?php if ($kyc == 'Y' && $vip_role = '0') {
          
        } else { 
		if($this->session->userdata('role_id') != 1 ){
		if($this->session->userdata('role_id') == COMPANY_ROLE_ID ){
		?>
		<a class="btn btn-primary add_btn" href="<?php echo base_url() ?>admin/rokad_topup/create_new">Topup Request</a>
        <?php }}} ?>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="commission_wrapper">
        <div class="box">
            
            <div class="box-body">

                <div class="alert alert-success alert-dismissible no_margin mb10 hide" role="alert" id="alert_msg">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="body"></div>
                </div>

                    <?php if ($this->session->flashdata('msg')) { ?>
                    <div class="alert alert alert-success alert-error" style="display:block" id="msg">
                    <?php echo $this->session->flashdata('msg') ?> 
                    </div>
                    <?php } ?>


                <table id="rokad_topup" name="rokad_topup" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th  width="10%">ID</th>
							<th>Date Time</th>
							<th>Amount</th>
							<th>Trans Mode</th>
							<th>Remark</th>
							<th>Status </th>
							<th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>		    
                <div class="clearfix"></div>
            </div><!-- /.box-body -->
            <div class="box-footer">

                <div class="clearfix"></div>
                    <!-- <code>.box-footer</code> -->
            </div><!-- /.box-footer-->
        </div>
    </div>
</section>
<?php $this->load->view(BACK_ROKAD_TOPUP_REQUEST_VIEW); ?>
<script type="text/javascript">
    $(document).ready(function () {
        
		DatatableRefresh();

       });
	   
	function DatatableRefresh() {
		
		$('#rokad_topup').dataTable({
			"aoColumnDefs": [ {"bSortable": true, "aTargets": [0,6] } ],
			//"scrollY": "600",
			"deferRender": true,
			"bProcessing": true,
			"bServerSide": true,
			//"aaSorting": [[0, "desc"]],
			"order": [[ 0, 'desc' ]],
			"lengthMenu": [
				[10, 50, 100, -1],
				[10, 50, 100, "All"] // change per page values here
			],
			"bScrollCollapse": true,
			"bAutoWidth": true,
			"sPaginationType": "full_numbers",
			"bDestroy": true,
			"sAjaxSource": "<?php echo base_url().'admin/rokad_topup/DataTableTopupRequest'; ?>",
			"fnServerData": function (sSource, aoData, fnCallback) {
				    $.getJSON(sSource, aoData, function (json) {
					fnCallback(json);
				});
			},
			"fnRowCallback": function (nRow, aData, iDisplayIndex) {

				return nRow;
			},
			"fnFooterCallback": function (nRow, aData) {
			}
		});

	}
	

	$(document).off('click', '.view_btn').on('click', '.view_btn', function (e) {
        e.preventDefault();
        var id = $(this).attr('ref');
            
        var detail = {};
        var div = "";
        var ajax_url = 'admin/rokad_topup/view_rokad_topup';
        var form = '';

        detail['id'] = id;

        get_data(ajax_url, form, div, detail, function (response)
        {
            if (response.flag == '@#success#@')
            {
                RokadTopup = response.FundRequest;
                RequestTo = response.RequestTo;
                
				if(RokadTopup.topup_by != 'CASH'){
					$('.fields').show();
				}
				
                $("#rokad_topup_form_view #amount").val(RokadTopup.amount);
				$("#rokad_topup_form_view #amount").attr("readonly", true);
                $("#rokad_topup_form_view #transanction_mode").val(RokadTopup.topup_by);
				$("#rokad_topup_form_view #transanction_mode").attr("readonly", true);
                $("#rokad_topup_form_view #transanction_ref").val(RokadTopup.transaction_no);
				$("#rokad_topup_form_view #transanction_ref").attr("readonly", true);
				$("#rokad_topup_form_view #bank_name").val(RokadTopup.bank_name);
				$("#rokad_topup_form_view #bank_name").attr("readonly", true);
				$("#rokad_topup_form_view #bank_account_no").val(RokadTopup.bank_acc_no);
				$("#rokad_topup_form_view #bank_account_no").attr("readonly", true);
                $("#rokad_topup_form_view #remark").val(RokadTopup.transaction_remark);
				$("#rokad_topup_form_view #remark").attr("readonly", true);
                $('#add_view_popup').modal('show');
            }
            else
            {
                alert(response.msg);  
            }
        }, '', false);
    });
</script>