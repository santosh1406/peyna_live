<section class="content-header">   
<h1 >Topup Request Approval</h1>
<section class="content">
    <div class="row">
        <div class="col-md-12">
		    <?php
			$attributes = array("method" => "POST", "id" => "rokad_topup_approval_form", "name" => "rokad_topup_approval_form");
			echo form_open('', $attributes);
		   ?>
           <div class="box box-default">
                <div class="box-header"></div><!-- /.box-header -->
		    <?php if (isset($error) && count($error) >0) { ?>
             <div class="alert alert alert-danger alert-error" style="display:block"id="msg">
       
                <strong>Error!</strong> 
                <ul id="errormsg">
                <?php foreach($error as $key=>$values)
                    {
                        echo "<li>".$values."</li>";
                    }     
                ?>
                </ul>
			 </div>
            <?php } ?>
		   
            <div class="box-body">
			<div class="row">
			
			    <div class="col-md-6">
                    <div class="form-group">
                        <label for="transanction_mode">Transanction Mode:</label>
                        <input type="text" value="<?php echo $FundRequest->topup_by;?>" name="transanction_mode" id="transanction_mode" class="form-control" disabled />
                    </div>
                </div>
			   
			    <div class="col-md-6">
    			    <div class="form-group">
                        <label for="amount">Amount:</label>
                        <input type="text" value="<?php echo number_format($FundRequest->amount,2);?>" class="form-control" disabled />
    					<input type="hidden" value="<?php echo $FundRequest->amount;?>" name="amount" id="amount"  />
    					<input type="hidden" value="<?php echo $FundRequest->amt;?>" name="balance_amount" id="balance_amount" class="form-control" />
                    </div>
                </div>

                <div class="col-md-6">
    				<div class="form-group">
                        <label for="trans_date">Transanction Date:</label>
                        <input type="text" value="<?php echo $FundRequest->Date;?>" id="trans_date" name="trans_date"  class="form-control" disabled />
                    </div>
                </div>
				
				<div class="col-md-6">
                    <div class="form-group ">
                        <label for="bank_name">Bank Name:</label>
                        <input type="text" value="<?php echo $FundRequest->bank_name;?>" id="bank_name" name="bank_name"  class="form-control" disabled />
                    </div>
                </div>
                <div class="col-md-6">
    				<div class="form-group ">
                        <label for="bank_account_no">Bank Account No:</label>
                        <input type="text" value="<?php echo $FundRequest->bank_acc_no;?>"  id="bank_account_no" name="bank_account_no"  class="form-control" disabled />
                    </div>
				</div>
				
				<div class="col-md-6">
    				<div class="form-group">
                        <label for="transanction_ref">Transanction Reference:</label>
                        <input type="text" value="<?php echo $FundRequest->transaction_no;?>"  id="transanction_ref" name="transanction_ref" class="form-control" disabled />
                    </div>
                </div>

                <div class="col-md-6">
    				<div class="form-group">
                        <label for="remark">Remark:</label>
                        <input type="text" value="<?php echo $FundRequest->transaction_remark;?>" id="remark" name="remark"  class="form-control" disabled />
                     </div>
				</div>
				
				
				<div class="col-md-6">
    				<div class="form-group">
                        <label for="request_status">Status:</label>
                        <input type="text" value="<?php if($FundRequest->is_approvel == 'A'){ echo 'Accept';} else if($FundRequest->is_approvel == 'NA') { echo 'NA';} else if($FundRequest->is_approvel == 'R'){echo 'Reject';}?>"  class="form-control" disabled />
                    </div>
                </div>
				<?php if($FundRequest->is_approvel == 'R'){?>
                <div class="col-md-6">
    				<div class="form-group">
                        <label for="reject_reason">Reject Reason:*</label>
                        <input type="text" value="<?php echo $FundRequest->reject_reason;?>" id="reject_reason" name="reject_reason"  class="form-control" disabled />
                    </div>
                </div>
				<?php } ?>
                
				
            </div>
            <div class="modal-footer clearfix">
            <?php
                $href_view = base_url('admin/rokad_topup/rokad_topup_approval');
                if(!empty($self_view)) {
                    $href_view = base_url('admin/fund_self_request/self_topup_list');
                }
            ?>
                <a href="<?php echo $href_view;?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
				
            </div>
            </div>
            </form>
		</div>
	</div>
</section>