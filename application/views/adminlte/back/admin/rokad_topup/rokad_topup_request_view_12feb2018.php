<div class="modal fade popup" id="add_view_popup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Topup Request View</h4>
            </div>
			<?php
			$attributes = array("method" => "POST", "id" => "rokad_topup_form_view", "name" => "rokad_topup_form_view");
			echo form_open('', $attributes);
		   ?>
            <div class="modal-body">
                
                <div class="form-group">
                    <label for="amount">Amount:</label>
                    <input type="text" placeholder="Amount" name="amount" id="amount" class="form-control"  />
                </div>
				<div class="form-group">
                    <label for="transanction_mode">Transfer Mode:</label>
					<input type="text" placeholder="transanction_mode" name="transanction_mode" id="transanction_mode" class="form-control"  />
                </div>
                <div class="form-group fields">
                    <label for="transanction_ref">Transanction Reference:</label>
                    <input type="text" placeholder="Transanction Reference" id="transanction_ref" name="transanction_ref" class="form-control"  />
                </div>
                <div class="form-group fields">
                    <label for="bank_name">Bank Name:</label>
                    <input type="text" placeholder="Bank Name" id="bank_name" name="bank_name"  class="form-control"/>
                </div>
				<div class="form-group fields">
                    <label for="bank_account_no">Bank Account No:</label>
                    <input type="text"  id="bank_account_no" name="bank_account_no"  class="form-control"/>
                </div>
				<div class="form-group">
                    <label for="remark">Remark:</label>
                    <input type="text" placeholder="Remark" id="remark" name="remark"  class="form-control"/>
                    <input name="id" type="hidden" id="id" class="form-control"/>
                </div>
            </div>
            <div class="modal-footer clearfix">

                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>

            </div>
            </form>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->