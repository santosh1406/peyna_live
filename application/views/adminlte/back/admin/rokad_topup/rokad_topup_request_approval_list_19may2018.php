<script type="text/javascript">
var role_name = '0';
<?php if (in_array(strtolower($this->session->userdata('role_name')), array('admin', 'super admin'))) { ?>
        role_name = '1';
<?php } ?>
</script>
<style>
.fields{display:none;}
.reject{display:none;}
.row{margin-bottom:5px;} 
</style>
<section class="content-header">
    <h1>Topup Approval List</h1>
    <div align="right">
        <?php if ($kyc == 'Y' && $vip_role = '0') {
          
        } else { ?>
        <!--<button class="btn btn-primary add_btn">Fund Request Approval List</button>--> 
        <?php } ?>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div class="commission_wrapper">
        
                <?php $attributes = array("method" => "POST", "id" => "self_topup",  "name" => "self_topup","onsubmit" => 'return false;');
                echo form_open("#", $attributes); ?>
                <div class="box" style="padding:10px;">
                    <div class="row">
                         <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group"> 
                                <label for="select-1">Trans Mode</label>
                                <select class="form-control" id="trans_mode" name="trans_mode">
                                    <option value="all">All</option>                                    
                                    <?php foreach($Pay_Trans_Mode as $mode){?>
                                    <option value="<?php echo $mode->mode ;?>"><?php echo $mode->mode;?></option>    
                                    }<?php }?>
                                </select>
                            </div>
                        </div>
                        <div style="display: block;" class="col-md-2 display-no">
                            <div style="position: static;" class="form-group"> 
                                <label for="select-2">Status</label>
                                <select class="form-control" id="status" name="status">
                                    <option value="all">All</option>                                    
                                    <option value="A">Accepted</option>                                    
                                    <option value="R">Rejected</option>                                        
                                    <option value="P">Pending</option>                                        
                                </select>
                            </div>
                        </div>
                    </div>
                <div class="row"> 
                   <div style="display: block;" class="col-md-1 display-no">
                       <div style="position: static;" class="form-group">
                           <button class="btn btn-primary" name="op_generate_btn" id="op_generate_btn" type="submit">Search</button>
                       </div>
                   </div>
                   <div style="display: block;" class="col-md-1 display-no">
                       <div style="position: static;" class="form-group">
                           <button class="btn btn-primary" name="reset_btn" id="reset_btn" type="reset">Reset</button>
                       </div>
                   </div>
                   
               </div>
            </div>
        
        <div class="box">
            
            <div class="box-body">

                <div class="alert alert-success alert-dismissible no_margin mb10 hide" role="alert" id="alert_msg">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="body"></div>
                </div>
			    <?php
				$attributes = array("method" => "POST", "id" => "approval_list", "name" => "approval_list","class" => "approval_list");
				echo form_open('', $attributes);
                                ?><div class="table-responsive">
                <table id="rokad_topup_approval" name="rokad_topup_approval" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th  width="10%">ID</th>
							<th>Date Time</th>
							<th>Amount</th>
							<th>Trans Mode</th>
							<th>Remark</th>
							<th>Status </th>
							<th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>	</div>	    
                </form>		    
                <div class="clearfix"></div>
            </div><!-- /.box-body -->
            <div class="box-footer">

                <div class="clearfix"></div>
                    <!-- <code>.box-footer</code> -->
            </div><!-- /.box-footer-->
        </div>
    </div>
</section>
<script type="text/javascript">
    var frm = document.Filter_Frm;
    
    $(document).ready(function () {
       		
			
		DatatableRefresh();

       });
	   
	function DatatableRefresh() {
		
		var tconfig ={
			"aoColumnDefs": [ {"bSortable": true, "aTargets": [0,6] } ], 
			//"scrollY": "600",
			"deferRender": true,
			"bProcessing": true,
			"bServerSide": true,
                        "searching": false,
			"order": [[ 0, 'desc' ]],
			"lengthMenu": [
				[10, 50, 100, -1],
				[10, 50, 100, "All"] // change per page values here
			],
			"bScrollCollapse": true,
			"bAutoWidth": true,
			"sPaginationType": "full_numbers",
			"bDestroy": true,
			 "oLanguage": {
                "sProcessing": '&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url().BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>' 
            },
			"sAjaxSource": "<?php echo base_url().'admin/rokad_topup/DataTableTopupApproval'; ?>",
			"fnServerData": function (sSource, aoData, fnCallback) {
                            aoData.push( { "name": "trans_mode", "value": $("#trans_mode").val() } );
                            aoData.push( { "name": "status", "value": $("#status").val() } );
				    $.getJSON(sSource, aoData, function (json) {
					fnCallback(json);
				});
			},
			"fnRowCallback": function (nRow, aData, iDisplayIndex) {

				return nRow;
			},
			"fnFooterCallback": function (nRow, aData) {
			}
		};
                
                oData= $('#rokad_topup_approval').dataTable(tconfig);
                
                $(document).off('click', '#op_generate_btn').on('click', '#op_generate_btn', function (e) {
                    oData.fnDraw();       
                });
                $(document).off('click', '#reset_btn').on('click', '#reset_btn', function (e) {
                location.reload(true);
                });

	}
	


    function ResetFilter() {
		frm.reset();
		DatatableRefresh();
	}
	function setFilter() {
		DatatableRefresh();
	}	
</script>
