<style>
.fields{display:none;}
.reject{display:none;}
</style>
<section class="content-header">   
<h1 >Rokad Topup Approval</h1>
<section class="content">
    <div class="row">
        <div class="col-md-12">
		   <?php
				$attributes = array("method" => "POST", "id" => "rokad_topup_approval_form", "name" => "rokad_topup_approval_form","class" => "rokad_topup_approval_form");
				echo form_open(base_url().'admin/rokad_topup/save_rokad_topup_approval', $attributes);
            ?>
		    <div class="box box-default">
                <div class="box-header"></div><!-- /.box-header -->
		    <?php if (isset($error) && count($error) >0) { ?>
             <div class="alert alert alert-danger alert-error" style="display:block"id="msg">
       
                <strong>Error!</strong> 
                <ul id="errormsg">
                <?php foreach($error as $key=>$values)
                    {
                        echo "<li>".$values."</li>";
                    }     
                ?>
                </ul>
			 </div>
            <?php } ?>
		   
            <div class="box-body">
			<div class="row">
			    <div class="col-md-6">
				 <div class="form-group">
                    <label for="amount">Amount:</label>
                    <input type="text" value="<?php echo number_format($FundRequest->amount,2);?>" class="form-control" disabled />
					<input type="hidden" value="<?php echo $FundRequest->amount;?>" name="amount" id="amount"  />
					<input type="hidden" value="<?php echo $FundRequest->user_id;?>" name="requested_by" id="requested_by"  />
                </div>
                <div class="form-group">
                    <label for="transanction_mode">Transanction Mode:</label>
                    <input type="text" value="<?php echo $FundRequest->topup_by;?>" name="transanction_mode" id="transanction_mode" class="form-control" disabled />
                </div>
               </div>
			   <div class="col-md-6">
			    <div class="form-group">
                    <label for="trans_date">Transanction Date:</label>
                    <input type="text" value="<?php echo $FundRequest->Date;?>" id="trans_date" name="trans_date"  class="form-control" disabled />
                </div>
				<div class="form-group ">
                    <label for="bank_name">Bank Name:</label>
                    <input type="text" value="<?php echo $FundRequest->bank_name;?>" id="bank_name" name="bank_name"  class="form-control" disabled />
                </div>
                </div>
				<div class="col-md-6">
                <div class="form-group ">
                    <label for="bank_account_no">Bank Account No:</label>
                    <input type="text" value="<?php echo $FundRequest->bank_acc_no;?>"  id="bank_account_no" name="bank_account_no"  class="form-control" disabled />
                </div>
				<div class="form-group">
                    <label for="transanction_ref">Transanction Reference:</label>
                    <input type="text" value="<?php echo $FundRequest->transaction_no;?>"  id="transanction_ref" name="transanction_ref" class="form-control" disabled />
                </div>
				</div>
				<div class="col-md-6">
				<div class="form-group">
                    <label for="remark">Remark:</label>
                    <input type="text" value="<?php echo $FundRequest->transaction_remark;?>" id="remark" name="remark"  class="form-control" disabled />
                 </div>
				 <div class="form-group">
                    <label for="request_status">Status:</label>
                    <select name="request_status" id="request_status" class="chosen_select form-control"  >
                        <option value = ''>Pelase Select</option>
                        <option value = 'A'>Accept</option>
                        <option value = 'R'>Reject</option>
                       
                    </select>
                </div>
			    </div>
				<div class="col-md-6">
				<div class="form-group reject">
                    <label for="reject_reason">Reject Reason:*</label>
                    <input type="text" placeholder="Reject Reason" id="reject_reason" name="reject_reason"  class="form-control"/>
                </div>
            </div>
            </div>
            <div class="modal-footer clearfix">
				
				<input type="hidden" id="fundRequest_id" name="fundRequest_id" value="<?php echo $FundRequest->id;?>" />
                <a href="<?php echo base_url();?>admin/rokad_topup/rokad_topup_approval" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
				
                <button type="button" id="submit_btn" class="btn btn-primary pull-left"><i class="fa fa-fw fa-save"></i> Submit </button>
            </div>
            </div>
			</div>
            </form>
			<?php
				$attributes = array("method" => "POST", "id" => "rokad_topup_otp", "name" => "rokad_topup_otp","class" => "rokad_topup_otp","style"=>"display:none;");
				echo form_open(base_url().'admin/rokad_topup/otp_rokad_topup', $attributes);
            ?>
			<div class="modal-body">                
                <div class="form-group">
                    <label for="otp">OTP:*</label>
                    <input type="text" placeholder="OTP" name="otp" id="otp" class="form-control"  />
                </div>				
            </div>
            <div class="modal-footer clearfix">                
				<button type="button" id="otp_back_btn" class="btn btn-primary">Back </button>
                <button type="submit" id="otp_submit_btn" class="btn btn-primary pull-left"><i class="fa fa-fw fa-save"></i> Submit </button>
            </div>
            </form>	
			
		</div>
	</div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        
	    $('#request_status').change(function(){
			var ReqStatus = $('#request_status').val();
			if(ReqStatus =='R'){
				$('.reject').show();
			}else{
				$('.reject').hide();
			}
		});
		
		$.validator.setDefaults({
          ignore: ":hidden:not(select)"
        });
    
		$("#rokad_topup_approval_form").validate({
			rules: {
				request_status: {
					required: true,    
				},
				reject_reason: {
                         required: function(element) {
                                    return $("#request_status").val() !== 'A';
                                    }
                                }
			}
		});
		});
		
		
		$(document).off('click', '#submit_btn').on('click', '#submit_btn', function (e) {
        e.preventDefault();
        var id = $(this).attr('ref');		

        var detail = {};
        var div = "";
        var ajax_url = 'admin/rokad_topup/save_rokad_topup_approval';
        var form = 'rokad_topup_approval_form';
        
        if($('#rokad_topup_approval_form').valid()) 
        {
			$('#submit_btn').attr('disabled', true).html('Processing...');
            get_data(ajax_url, form, div, detail, function (response)
            {
				if (response.flag == '@#success#@')
                {                   
                    if(response.msg_type == "success")
                    {
						console.log('fb');
						$('#submit_btn').attr('disabled', false).html('Submit');
						$('#rokad_topup_approval_form').hide();
                        $('#rokad_topup_otp').show();
                    }
                }
                else
                {
                    if(response.msg_type == "error")
                    {
                        //showLoader(response.msg_type,response.msg);
						swal("Oops...", response.msg, "error");
						$('#submit_btn').attr('disabled', false).html('Submit');
                    }
                    else
                    {
						alert(response.msg);
                    }     
                }
            }, '', false);           
        }
    });
		
		
		
	$(document).off('click', '#otp_submit_btn').on('click', '#otp_submit_btn', function (e) {
        e.preventDefault();
        var id = $(this).attr('ref');		

        var detail = {};
        var div = "";
        var ajax_url = 'admin/rokad_topup/rokad_topup_approval_otp';
        var form = 'rokad_topup_otp';
        
        if($('#rokad_topup_otp').valid()) 
        {
			$('#otp_submit_btn').attr('disabled', true).html('Processing...');
            get_data(ajax_url, form, div, detail, function (response)
            {
				if (response.flag == '@#success#@')
                {                   
                    if(response.msg_type == "success")
                    {
                        showLoader(response.msg_type,response.msg);
						//$('#otp_submit_btn').attr('disabled', false).html('Submit');
						window.location = "<?php echo base_url() ?>admin/rokad_topup/rokad_topup_approval";
                    }
                }
                else
                {
					//console.log(response);					
                    if(response.msg_type == "error")
                    {
                        //showLoader(response.msg_type,response.msg);
						swal("Oops...", response.msg, "error");
						$('#otp_submit_btn').attr('disabled', false).html('Submit');
                    }
                    else
                    {
						alert(response.msg);
                    }     
                }
            }, '', false);           
        }
    });
	
	$(document).on("click", "#otp_back_btn", function() {
		var form = $(this).closest("form");	
		console.log(form);
		if(form.attr('id') == 'rokad_topup_otp')
		{		
			document.getElementById("rokad_topup_otp").reset();		
			$('#rokad_topup_otp').hide();						
			$('#rokad_topup_approval_form').show();
		}	
	});		
		
</script>	