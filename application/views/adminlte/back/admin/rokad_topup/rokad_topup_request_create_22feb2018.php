<style>
.fields{display:none;}
.reject{display:none;}
</style>
<section class="content-header">   
<h1 >Topup Request</h1>
<section class="content">
    <div class="row">
        <div class="col-md-12">           
		    <div class="box box-default">
			<?php
				$attributes = array("method" => "POST", "id" => "rokad_topup_form", "name" => "rokad_topup_form","class" => "rokad_topup_form");
				echo form_open(base_url().'admin/rokad_topup/save_rokad_topup', $attributes);
            ?>
           
            <div class="modal-body">
                
                <div class="form-group">
                    <label for="amount">Amount:*</label>
                    <input type="text" placeholder="Amount" name="amount" id="amount" class="form-control"  autocomplete="off" />&nbsp;<span id="erramountmsg" class="error"></span>
                </div>
				<div class="form-group">
                    <label for="transanction_mode">Transfer Mode:*</label>
                    <select name="transanction_mode" id="transanction_mode" class="chosen_select form-control"  >
                        <option value = ''>Please Select</option>
						<?php foreach($Pay_Trans_Mode as $value){?>
                        <option value="<?php echo $value->id;?>"><?php echo  $value->mode;?></option>
						<?php } ?>
                    </select>
                </div>
                <div class="form-group fields">
                    <label for="transanction_ref">Transanction Reference:*</label>
                    <input type="text" placeholder="Transanction Reference" id="transanction_ref" name="transanction_ref" class="form-control" min="10" maxlength="24" autocomplete="off" />&nbsp;<span id="errtransrefmsg" class="error"></span>
                </div>
                <div class="form-group fields">
                    <label for="bank_name">Bank Name:*</label>
                    <input type="text" placeholder="Bank Name" id="bank_name" name="bank_name"  class="form-control" autocomplete="off" />
                    
                </div>
				<div class="form-group fields">
                    <label for="bank_account_no">Bank Account No:*</label>
                    <input type="text" placeholder="Bank Account No" id="bank_account_no" name="bank_account_no"  class="form-control" maxlength="24" autocomplete="off" />&nbsp;<span id="errbankacntnomsg" class="error"></span>
                </div>
				<div class="form-group">
                    <label for="remark">Remark:</label>
                    <input type="text" placeholder="Remark" id="remark" name="remark"  class="form-control" autocomplete="off" />
                    <input name="id" type="hidden" id="id" class="form-control"/>
                </div>
            </div>
            <div class="modal-footer clearfix">

                <a href="<?php echo base_url();?>admin/rokad_topup" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                <button type="submit" id="submit_btn" class="btn btn-primary pull-left"><i class="fa fa-fw fa-save"></i> Submit </button>
				
            </div>
            </form>		
			<?php
				$attributes = array("method" => "POST", "id" => "rokad_topup_otp", "name" => "rokad_topup_otp","class" => "rokad_topup_otp","style"=>"display:none;");
				echo form_open(base_url().'admin/rokad_topup/otp_rokad_topup', $attributes);
            ?>
			<div class="modal-body">                
                <div class="form-group">
                    <label for="otp">OTP:*</label>
                    <input type="text" placeholder="OTP" name="otp" id="otp" class="form-control"  autocomplete="off" />
                </div>				
            </div>
            <div class="modal-footer clearfix">                
				<button type="button" id="otp_back_btn" class="btn btn-primary">Back </button>
                <button type="submit" id="otp_submit_btn" class="btn btn-primary pull-left"><i class="fa fa-fw fa-save"></i> Submit </button>
            </div>
            </form>		
			
          </div>
		</div>
	</div>
</section>


<script type="text/javascript">
    $(document).ready(function () {
		
		$("#amount").keypress(function (e) {
			 //if the letter is not digit then display error and don't type anything
			 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				//display error message
				$("#erramountmsg").html("Digits Only").show().fadeOut("slow");
					   return false;
			}
		});
		
		$("#transanction_ref").keypress(function (e) {
			 //if the letter is not digit then display error and don't type anything
			 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				//display error message
				$("#errtransrefmsg").html("Digits Only").show().fadeOut("slow");
					   return false;
			}
		});
		
		$("#bank_account_no").keypress(function (e) {
			 //if the letter is not digit then display error and don't type anything
			 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				//display error message
				$("#errbankacntnomsg").html("Digits Only").show().fadeOut("slow");
					   return false;
			}
		});
        
	    $('#transanction_mode').change(function(){
			var TransMode = $('#transanction_mode').val();
			if(TransMode ==1){
				$('.fields').hide();
			}else{
				$('.fields').show();
			}
		});
	
		$.validator.setDefaults({
          ignore: ":hidden:not(select)"
        });
		
		jQuery.validator.addMethod("lettersonly", function(value, element) {
		  return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
		}, "Letters only please");

		// jQuery.validator.addMethod("nozeros", function(value, element) {
		// 	var val = parseFloat(value);
		// 	return (isNaN(val) || val === 0) ? false : true;
		// }, "Please Enter Valid Account Number");
    
		$("#rokad_topup_form").validate({
			rules: {
				amount: {
					required: true,
					digits: true,
					check_numbers:true
				},
				transanction_mode: {
					required: true
				},
				transanction_ref: {
					required: true,
					digits: true
				},
				bank_name: {
					required: true,
					lettersonly: true
				},
				bank_account_no: {
					required: true,
					//nozeros: true,
					digits: true
				}
			},
			submitHandler: function(form) {					
				disable_button('submit_btn');	// button id as parameter					
				$.ajax({
					url: form.action,
					type: form.method,
					data: $(form).serialize(),
					async:false,
					dataType:'json',
					success: function(response) {
						
						if (response.flag == '@#success#@')
						{                   
							if(response.msg_type == "success")
							{								
								$('#rokad_topup_form').hide();
								$('#rokad_topup_otp').show();
							}
						}
						else
						{
							if(response.msg_type == "error")
							{
								//showLoader(response.msg_type,response.msg);
								swal("Oops...", response.msg, "error");	
								enable_button('submit_btn');
							}
							else
							{
								//alert(response.msg);
								swal("Oops...", response.msg, "error");
								enable_button('submit_btn');
							}     
						}
					}            
				});
			}
		});
		
		$.validator.addMethod("check_numbers", function (value, element) {
		var flag=false;
		var TempAmount = parseInt(value);
		if(TempAmount <=0){
		flag = false;	
		}else{
		flag = true;	
		}
		return flag;
	}, 'Amount Should Be Greater Than 0.');
		
		$("#rokad_topup_otp").validate({
			rules: {
				otp: {
					required: true,
					digits: true
				}				
			},
			submitHandler: function(form) {
				disable_button('otp_submit_btn');
				$.ajax({
					url: form.action,
					type: form.method,
					data: $(form).serialize(),
					async:false,
					dataType:'json',
					success: function(response) {
						
						if (response.flag == '@#success#@')
						{                   
							if(response.msg_type == "success")
							{
								showLoader(response.msg_type,response.msg);
								$('#otp_submit_btn').attr('disabled', false).html('Submit');
								window.location = "<?php echo base_url() ?>admin/rokad_topup";
							}
						}
						else
						{
							//console.log(response);					
							if(response.msg_type == "error")
							{
								//showLoader(response.msg_type,response.msg);
								swal("Oops...", response.msg, "error");
								enable_button('otp_submit_btn');
							}
							else
							{
								alert(response.msg);
								swal("Oops...", response.msg, "error");
								enable_button('otp_submit_btn');
							}     
						}								
					}            
				});
			}
		});
		
	
    });
	
	function disable_button(button_id)
	{
		var temp = button_id+'_loading';
		$("#"+temp).remove();
		var pdiv = $("#"+button_id).closest("div");
		pdiv.append( "<span class='btn btn-primary pull-left' disabled id='"+temp+"' type='button' >Processing</span>" );				
		$("#"+button_id).hide();
	}
	
	function enable_button(button_id)
	{
		var temp = button_id+'_loading';
		$("#"+temp).remove();				
		$("#"+button_id).show();
	}
	
	
	$(document).on("click", "#otp_back_btn", function() {
		var form = $(this).closest("form");	
		console.log(form);
		if(form.attr('id') == 'rokad_topup_otp')
		{		
			document.getElementById("rokad_topup_otp").reset();		
			$('#rokad_topup_otp').hide();						
			$('#rokad_topup_form').show();
		}	
	});
</script>