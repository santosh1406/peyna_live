<div class="content-wrapper">
    <!-- Main content -->
    
    
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> 
                        <img  style="float:right" src="<?php echo base_url().BACK_ASSETS ?>img/bbps_logo.png" height="45px" width="166px"/>
<!--                        <h3 class="box-title" style="color:red;padding: 10px 10px 10px 10px;">Electricity service is under mentainance please do transaction after 2 hours.</h3>
                        <br/><br/> <br/>-->

                        <h3 class="box-title">BBPS Pay For Electricity</h3>
                    </div>
                    <div class="box-body">
                        <?php
                        
                        if ($this->session->tempdata('msg')) {
                            ?>
                            <div class="alert alert-success alert-dismissable" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->tempdata('msg'); ?>
                            </div>
                            <?php
                        }
                        ?>
                        <?php
                        
                        
                        echo validation_errors('<div class="error" id="error_div">', '</div>');
                        if($this->session->flashdata('message')){ 
                           echo '<div class="error" id="error_div"><b>' . $this->session->flashdata('message') . '</b></div>';
                        }
                        ?>
                        <div class="error" id="error_div"></div>
                        <?php
                            $attributes = array("method" => "POST", "id" => "bbps_form", "name" => "bbps_form");
                            echo form_open(base_url() . 'admin/Utilities_bbps/pay_bbps_bill', $attributes);
                            ?>
                            <input type="hidden" name="recharge_from" id="recharge_from" value=""/>
                            <input type="hidden" name="type" id="type" value="Electricity"/>
                            <input type="hidden" name="redirecttype" id="redirecttype" value="bbpselectricity"/>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label">Select operator *:</label>
                                <div class="col-lg-6">
                                    <input type="text" id="bbpsoperator" name="operator" class="form-control" placeholder="Electricity Operator" onchange="removeBillDetails();" value="<?php echo isset($data['operator']) ? $data['operator'] : ''; ?>" />
                                    <input type="hidden" id="operator_id" name="operator_id" class="" value="<?php echo isset($data['operator_id']) ? $data['operator_id'] : ''; ?>" />
                                </div>
                            </div> 
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" id="consumer_label">Consumer Number *:</label>
                                <div class="col-lg-6">
                                    <input type="text" id="number" name="number" class="form-control" placeholder="" maxlength="12"  onchange="removeBillDetails();" value="<?php echo isset($data['number']) ? $data['number'] : ''; ?>"/>
                                </div>
                            </div>
                            <div id="account_div" class="hide">
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group"> 
                                    <label class="col-lg-3 control-label" id="account_label">Account Number *:</label>
                                    <div class="col-lg-6">
                                        <input type="text" id="account_number" name="account_number" class="form-control" placeholder="" maxlength="15"  onchange="removeBillDetails();" value="<?php echo isset($data['account_number']) ? $data['account_number'] : ''; ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div id="city_div" class="hide">
                                <div class="clearfix" style="height: 10px;clear: both;"></div>
                                <div class="form-group"> 
                                    <label class="col-lg-3 control-label" id="city_label">City *:</label>
                                    <div class="col-lg-6">
                                        <input type="text" id="city" name="city" class="form-control" placeholder="" maxlength="15"  onchange="removeBillDetails();" value="<?php echo isset($data['city']) ? $data['city'] : ''; ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label">Mobile Number *:</label>
                                <div class="col-lg-6">
                                    <input type="text" id="mobile_number" name="mobile_number" class="form-control" placeholder="" 
                                           value="<?php echo isset($data['mobile_number']) ? $data['mobile_number'] : ''; ?>"/>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label">Email Id *:</label>
                                <div class="col-lg-6">
                                    <input type="text" id="useremail" name="useremail" class="form-control" placeholder="" 
                                           value="<?php echo isset($data['useremail']) ? $data['useremail'] : ''; ?>"/>
                                </div>
                            </div>
                             
                            <div id="bill_details" class="hide">
                                <div class="clearfix" style="height: 10px; clear: both;"></div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <label class="control-label">Customer Name: <span  id="cust_name"> <?php echo isset($data['customer_name']) ? $data['customer_name'] : ''; ?></span></label>
                                    </div>
                                </div>
                                <div class="clearfix" style="clear: both;"></div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <label class="control-label">Bill Number: <span id="bill_number_txt"><?php echo isset($data['bill_number']) ? $data['bill_number'] : ''; ?></span> </label>
                                    </div>
                                </div>
                                <div class="clearfix" style="clear: both;"></div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <label class="control-label">Ref Id: 
                                            <span id="ref_id"><?php echo isset($data['ref_id']) ? $data['ref_id'] : ''; ?></span> </label>
                                    </div>
                                </div>
                                <div class="clearfix" style="clear: both;"></div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <label class="control-label">Bill Date:<span id="bill_date"> <?php echo isset($data['bill_date']) ? $data['bill_date'] : ''; ?></span></label>
                                    </div>
                                </div>
                                <div class="clearfix" style="clear: both;"></div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <label class="control-label">Bill Due Date: <span  id="bill_due_date"><?php echo isset($data['bill_due_date']) ? $data['bill_due_date'] : ''; ?></span></label>
                                    </div>
                                </div>
                                <div class="clearfix" style="clear: both;"></div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <label class="control-label">Amount Options: <span  id="amount" name="amount"></span></label>
                                    </div>
                                </div>
                                <div class="clearfix" style="clear: both;"></div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <label class="control-label">Customer Charge: <span  id="cust_charge"> </span></label>
                                    </div>
                                </div>
                                <div class="clearfix" style="clear: both;"></div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-6">
                                        <label class="control-label">Total Amount: <span  id="total_amount"> </span></label>
                                    </div>
                                </div>
                                
                                <input type="hidden" id="agent_id" value="<?php echo $this->sessionData['id'];?>">
                                
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <div class="col-lg-offset-4">
                                   <button class="btn btn-primary" id="submit_bbps" name="submit_bbps" type="button" onclick="submitBbpsForm('#bbps_form','1');">Proceed</button>
                                    <!--<button class="btn btn-primary hide" id="f_submit_bbps" name="f_submit_bbps" type="submit" >Proceed</button>-->
                                    <button class="btn btn-primary hide" id="f_submit_bbps" name="f_submit_bbps" type="button" onclick="submitBbpsForm('#bbps_form','0');">Do Payment</button>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                    <?php
//                   if ($this->session->tempdata('msg')=='Response - Payment successful') {
//                      
//                       echo '<script>window.open("'.export_pdf.'","_blank")</script>';
//                            
//                    }
                    ?>
                </div>
            </div>
        </div>
    </section>
</div>

<script src="<?php echo base_url() . BACK_ASSETS ?>js/AdminLTE/bbps.js" type="text/javascript"></script>
