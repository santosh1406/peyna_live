<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> 
                        <h3 class="box-title">Receipt</h3>
                    </div>
                    <div class="box-body">
                        <!--<a href="#" onclick="PrintDiv();" style="float: left;">Print Receipt</a>-->
                        <button class="btn btn-primary" id="submit_bbps" name="submit_bbps" style="float: right;" 
                                type="button" onclick="PrintDiv();">Print Receipt</button>
                        <div id="divToPrint">
                            <?php

                              if(isset($this->session->userdata['file_data']) && !empty($this->session->userdata['file_data'])){
                                print_r($this->session->userdata['file_data']['file_content']);
                              }else{
                                  header("Location: ".base_url() . "admin/dashboard");
                              }
                            ?>
                        </div>
                         
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
 <script type="text/javascript">     
    function PrintDiv() {    
       var divToPrint = document.getElementById('divToPrint');
       var popupWin = window.open('');
       popupWin.document.open();
       popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
       popupWin.document.close();
    }
 </script>