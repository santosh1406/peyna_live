<script type="text/javascript">
    var role_name = '0';
<?php if (in_array(strtolower($this->session->userdata('role_name')), array('admin', 'super admin'))) { ?>
        role_name = '1';
<?php } ?>
</script>
<style>
    .fields{display:none;}
</style>
<section class="content-header">
    <h1>Transaction Status</h1>

</section>

<!-- Main content -->
<section class="content">
    <div id="commission_wrapper">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body" id="table_show">

                        <?php
                        $attributes = array('class' => 'form', 'id' => 'trans_status', 'name' => 'trans_status', 'method' => 'post', 'onsubmit' => 'return false;');
                        echo form_open('', $attributes);
                        ?>
                        <div class="row box-title">
                                 
                                        <div class="col-sm-4 col-xs-4">
                                            <div class="inputBox">
                                                <input type="text" id="ClientUniqueID" name="ClientUniqueID" class="form-control" placeholder="Please Enter Transaction Number"/>
                                            </div>
                                        </div>
                                   
                           
                            <div style="display: block;" class="col-md-1 display-no">
                                <div style="position: static;" class="form-group">
                                    <button   class="btn btn-primary" name="ser" id="ser" type="button" onclick='search()'>Search</button>
                                </div>
                            </div>

                        </div>


                    </div>
                    <?php echo form_close(); ?>

         <!--           <table id="transaction" name="transaction" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                               
                                <th>Transaction Id</th>
                                <th>Transaction Mode</th>
                                <th>Mobile Number</th>
                                <th>IFSC Code</th>
                                <th>Account Number</th>
                                <th>Beneficiary Name</th>
                                <th>Transaction Amount</th>
                                <th>Customer Name</th>
                                <th>Transaction Date</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>-->



                </div>
            </div>

        </div>
    </div>

</section>


<script type="text/javascript">
     function search(){
   
       var dataString = $('#trans_status').serialize();
       $("#ser").prop('disabled', true);
        $("#ser").html("Searching......");
        
       $.ajax({
          url:'<?php echo base_url("admin/Dmt/transaction_status");?>',
           data: dataString,
           async: false,
            type:'POST',
            dataType:'json',
           success: function(data){
             alert(data.DisplayMessage)
            $("#ser").prop('disabled', false);
            $("#ser").html("Search");
         
          document.getElementById("trans_status").reset();   
           }
 
         });
    }
    
     $('#trans_status').bootstrapValidator({    
        group: '.form-control',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            ClientUniqueID: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b>Please Enter Transaction Number.</font>'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '<b><font style="color:red; size="1">X</b>Enter Valid Transaction Number</font>'
                    }
                }
            }
        }
    });
    
    
    
</script>


