<!-- Created by pooja kambali on 27-09-2018 -->

<!-- Inner Wrapper Start -->
<!--div id="page-wrapper"-->
<!-- Signin Content Start -->
<!--section class="inner-wrapper"-->
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header "> 
                        <h3 class="box-title">Digital Money Transfer</h3>
                    </div>
                    <!--div class="panel panel-default"-->
                    <div class="box-body">
                        <!--div class="col-md-12 col-md-12 container-c"-->
                        <!--div class="formBox"-->
                        <form id='dmt_form'>
                            <?php
                            //echo validation_errors('<div class="error">', '</div>');
                            if ($this->session->flashdata('message')) {
                                ?>
                                <div class="alert alert-success alert-dismissable" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                    <?php echo $this->session->flashdata('message'); ?>
                                </div>
                                <?php
                            }
                            $attributes = array("method" => "POST", "id" => "dmt", "name" => "dmt");
                            echo form_open(base_url() . 'admin/dmt/neft', $attributes);
                            ?>
                            <input type="hidden" name="recharge_from" id="recharge_from" value="web"/>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">       
                                <label class="col-lg-4 control-label">Select Type *:</label>
                                <div class="col-lg-6">
                                    <input type="radio" name="type" id="type" value="Neft" class="form-control" checked/> NEFT
                                    <input type="radio" name="type" id="type" value="Imps" class="form-control"/> IMPS
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Remitter Mobile Number *:</label>  
                                <div class="col-lg-6">
                                    <input type="text" id="CustomerMobileNo" name="CustomerMobileNo" class="form-control" placeholder="Please Enter mobile Number" maxlength="10"/>
                                    <?php echo form_error('CustomerMobileNo'); ?>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>

                            <div class="from-group">
                                <label class="col-lg-4 control-label">Remitter Name *:</label>  
                                <div class="col-lg-6">
                                    <input type="text" id="CustomerName" name="CustomerName" class="form-control" placeholder="Please Enter Remitter Name" maxlength="25"/>
                                    <?php echo form_error('CustomerName'); ?>
                                </div>

                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Beneficiary IFSC Code *:</label>  
                                <div class="col-lg-6">
                                    <input type="text" id="BeneIFSCCode" name="BeneIFSCCode" class="form-control" placeholder="Please Enter IFSC Code " maxlength="11"/>
                                    <?php echo form_error('BeneIFSCCode'); ?>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">Beneficiary Account Number *:</label>  
                                <div class="col-lg-6">
                                    <input type="text" id="BeneAccountNo" name="BeneAccountNo" class="form-control" placeholder="Please Enter Beneficiary Account Number"  maxlength="19"/>
                                    <?php echo form_error('BeneAccountNo'); ?>

                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>

                            <div class="from-group">
                                <label class="col-lg-4 control-label">Beneficiary Name *:</label>  
                                <div class="col-lg-6">
                                    <input type="text" id="BeneName" name="BeneName" class="form-control" placeholder="Please Enter Beneficiary Name " maxlength="20"/>
                                    <?php echo form_error('BeneName'); ?>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>

                            <div class="from-group">
                                <label class="col-lg-4 control-label">Amount *:</label>  
                                <div class="col-lg-6">
                                    <input type="text" id="Amount" name="Amount" class="form-control" placeholder="Please Enter Amount "/>
                                    <?php echo form_error('Amount'); ?>
                                </div>
                            </div>

                            <div class="clearfix" style="height: 10px;clear: both;"></div>

                            <div class="from-group">
                                <label class="col-lg-4 control-label">Remark:</label>  

                                <div class="col-lg-6">
                                    <input type="text" id="rfu1" name="rfu1" class="form-control" placeholder="Please Enter Remarks" maxlength="20"/>

                                </div>
                            </div>
                            <!--   <div class="row">
                                   <div class="col-sm-12 col-xs-12">
                                       <div class="inputBox">
                                           <input type="text" id="rfu2" name="rfu2" class="form-control" placeholder="Please enter RFU1" maxlength="200"/>
                                       </div>
                                   </div>
                               </div>
                               <div class="row">
                                   <div class="col-sm-12 col-xs-12">
                                       <div class="inputBox">
                                           <input type="text" id="rfu3" name="rfu3" class="form-control" placeholder="Please enter RFU1" maxlength="200"/>
                                       </div>
                                   </div>
                               </div>-->
                            <!--input type="hidden" name="status" id="status" value="0"/-->
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <div class="col-lg-offset-6">
                                    <button class="btn btn-primary" id="transfer"  type="submit" onclick='save()'>Transfer</button>                                
                                       <a class="btn btn-primary" href="<?php echo base_url('admin/dashboard'); ?>" type="button">Cancel</a> 

                                </div>
                                <!--/div-->

                        </form>

                        <!--/div-->
                    </div>
                </div>
            </div>
        </div>
</div>
</section>
</div>


<!-- Signin Content End -->             
<!--/div-->
<!-- Inner Wrapper End -->

<script type="text/javascript">
    function save() {

        var dataString = $('#dmt_form').serialize();
        // console.log(dataString);
        //    $("#transfer").prop('disabled', true);
        // $("#transfer").html("Loading......");
        $.ajax({
            url: '<?php echo base_url("admin/Dmt/neft"); ?>',
            data: dataString,
            async: false,
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                //  $("#transfer").prop('disabled', false);
                // $("#transfer").html("Transfer");
                if (data.status == 'failed') {
                    var msg = data.error;
                    for (var i in msg) {
                        alert(msg[i]);
                    }

                } else {
                    alert(data.DisplayMessage)
                }

                //document.getElementById("dmt_form").reset();
            }

        });
    }

    $('#dmt_form').bootstrapValidator({
        group: '.form-control',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            type: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b>Please Select Type.</font>'
                    }
                }
            },
            CustomerMobileNo: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b>Please Enter Mobile Number.</font>'
                    },
                    regexp: {
                        regexp: /^[789]\d{9}$/,
                        message: '<b><font style="color:red; size="1"></b>Valid Mobile Number</font>'
                    }
                }
            },
            BeneIFSCCode: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b>Please Enter IFSC CODE.</font>'
                    },
                    regexp: {
                        regexp: /^[a-z]{4}\d{7}$/i,
                        message: '<b><font style="color:red; size="1"></b>Please Enter Valid IFSC CODE.</font>'
                    }
                }
            },
            BeneAccountNo: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b>Please Enter Account Number.</font>'
                    },
                    callback: {
                        message: '<b><font style="color:red; size="1"></b>Enter Valid Account number</font>',
                        callback: function(value, validator, $field) {
                           var regex = new RegExp("^[0-9]{11,19}$");
                            if(value.length > 0)                          
                            {
                                if(parseInt(value) > 0 && regex.test(value))
                                {
                                    // success
                                }
                                else
                                {
                                    return false;
                                }
                            }
                            return true;
                        }
                    }
                }
            },
            BeneName: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b> Please Enter Beneficiary Name.</font>'
                    },
                    regexp: {
                        regexp: /^[a-z]+$/i,
                        message: '<b><font style="color:red; size="1"></b>Enter Valid Beneficiary Name</font>'

                    }
                }
            },
            Amount: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b> Please Enter Amount.</font>'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '<b><font style="color:red; size="1"></b>Enter Valid Amount</font>'
                    }
                }
            },
            CustomerName: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b> Please Enter Remitter Name.</font>'
                    },
                    regexp: {
                        regexp: /^[a-z]+$/i,
                        message: '<b><font style="color:red; size="1"></b>Enter Valid Remitter Name</font>'

                    }
                }
            }

        }

    });

   

</script>
