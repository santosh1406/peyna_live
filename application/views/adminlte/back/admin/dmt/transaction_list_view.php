<script type="text/javascript">
    var role_name = '0';
<?php if (in_array(strtolower($this->session->userdata('role_name')), array('admin', 'super admin'))) { ?>
        role_name = '1';
<?php } ?>
</script>
<style>
    .fields{display:none;}
</style>
<section class="content-header">
    <h1>Transaction Details</h1>

</section>

<!-- Main content -->
<section class="content">
    <div id="commission_wrapper">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body" id="table_show">

                        <?php
                        $attributes = array('class' => 'form', 'id' => 'trans_list_filter', 'name' => 'trans_list_filter', 'method' => 'post', 'onsubmit' => 'return false;');
                        echo form_open('', $attributes);
                        ?>
                        <div class="row box-title">

                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-1">From Date</label>
                                    <input class="form-control date" id="from_date" placeholder="<?php echo date('d-m-Y'); ?> " value="<?php echo (!empty($from_date)) ? $from_date : date('d-m-Y'); ?>" type="text"  readonly="true">
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <label for="input-text-2">To Date</label>
                                    <input class="form-control date" id="to_date" placeholder="<?php echo date('d-m-Y'); ?>" value="<?php echo (!empty($till_date)) ? $till_date : date('d-m-Y'); ?>" type="text"  readonly="true">
                                </div>
                            </div>

                        </div>
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        
                        <div class="row">
                            <div style="display: block;" class="col-md-1 display-no">
                                <div style="position: static;" class="form-group">
                                    <button   class="btn btn-primary" name="search" id="search" type="submit"><span></span>Search</button>
                                </div>
                            </div>
                            
                            <div style="display: block;" class="col-md-1 display-no">
                                <div style="position: static;" class="form-group">
                                    <button class="btn btn-primary" name="reset_btn" id="reset_btn" type="reset">Reset</button>
                                </div>
                            </div>
                            <div style="display: block;" class="col-md-2 display-no">
                                <div style="position: static;" class="form-group">
                                    <button  class="btn btn-primary" id="extoexcel" type="button">
                                        <span class="glyphicon glyphicon-export"></span>
                                        Export to Excel</button>  
                                </div>
                            </div>
                            
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                    </div>
                    <?php echo form_close(); ?>

                    <table id="tran_list" name="tran_list" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Sr.No</th>
                                <th>Sale Agent Name</th>
                                <th>Mobile No</th>
                                <th>Transaction Id</th>
                                <th>Transaction Mode</th>
                                <th>Customer Mobile Number</th>
                                <th>IFSC Code</th>
                                <th>Account Number</th>
                                <th>Beneficiary Name</th>
                                <th>Transaction Amount</th>
                                <th>Customer Name</th>
                                <th>Transaction Date</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>



                </div>
            </div>

        </div>
    </div>
</div>
</section>


<script type="text/javascript">
    $(document).ready(function() {
        var tconfig = {
            "processing": true,
            "serverSide": true,
            "iDisplayLength": 10,
            "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
            "ajax": {
                "url": BASE_URL + "admin/Dmt/transaction",
                "type": "POST",
                "data": "json",
                data   : function(d) {
                    var date = new Date();
                    var from_date;
                    var till_date;
                    
                    from_date = $("#from_date").val();
                    till_date = $("#to_date").val();    
                    d.from_date = from_date;
                    d.to_date = till_date;
                    
                }
            },
            "columnDefs": [
                {
                    "searchable": false
                }
            ],
            "order": [[3, "desc"]],
            "scrollX": true,
            "searching": true,
            "bFilter": false,
            "bPaginate": true,
            "bDestroy": true,
            "aoColumnDefs": [
                {
                    "bSortable": false,
                    "aTargets": [0]
                },
                {
                    "bSearchable": false,
                    "aTargets": [0, 3, 4, 5, 6, 7, 8, 9]
                },
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                // console.log(aData);
                var info = $(this).DataTable().page.info();
                $("td:first", nRow).html(info.start + iDisplayIndex + 1);
                return nRow;
            },
            fnDrawCallback: function(data) {
                if (oData.fnGetData().length == 0)
                {
                    $("#extoexcel").attr("disabled", "disabled");

                } else {
                    $("#extoexcel").removeAttr("disabled");

                }
            },
            "oLanguage": {
                 "sProcessing": '<img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>'
            },
        };

        oData = $('#tran_list').dataTable(tconfig);


        $(document).off('click', '#search').on('click', '#search', function(e) {
            oData.fnDraw();
        });
        
        $(document).off('click', '#reset_btn').on('click', '#reset_btn', function(e) {
            location.reload(true);
        });
        
        $("#from_date").datepicker({
            'dateFormat': 'dd-mm-yy',
            maxDate: 0
        }).on('change', function() {
            //$(this).valid();  // triggers the validation test
            // '$(this)' refers to '$("#datepicker")'
        });

        $("#to_date").datepicker({
            'dateFormat': 'dd-mm-yy',
            maxDate: 0
        }).on('change', function() {
            //$(this).valid();  // triggers the validation test
            // '$(this)' refers to '$("#datepicker")'
        });
        
        $('#extoexcel').on('click', function(e)
        {
            var from_date;
            var till_date;
            from_date = $("#from_date").val();
            till_date = $("#to_date").val();
            from = from_date;
            to = till_date;
            
            window.location.href = BASE_URL + 'admin/dmt/dmt_reports_excel?from_date=' + from + "&to_date=" + to;
        });


    });
</script>


