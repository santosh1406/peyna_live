
<!-- Inner Wrapper Start -->
<div id="page-wrapper">
    <!-- Signin Content Start -->
    <section class="inner-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h3 class="text-center purple">Digital Money Transfer</h3>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-12 col-sm-12 container-c">
                                <div class="formBox">
                                    <form id='fino_form'>
                                    <?php
                                    echo validation_errors('<div  class="error">', '</div>');
                                    if ($this->session->flashdata('message')) {
                                        echo '<b>Success!</b> ' . $this->session->flashdata('message');
                                    }
                                    ?>
                                    <?php
                                    $attributes = array("method" => "POST", "id" => "fino", "name" => "fino");
                                    echo form_open(base_url() . 'admin/Fino/neft', $attributes);
                                    ?>
                                    <!--input type="hidden" name="transfer_from" id="transfer_from" value="web"/-->
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <div class="col-lg-6 form-group">       
                                        <label class="col-lg-3 control-label">Select type *:</label>
                                           <input type="radio" name="type" id="type" value="Neft" class="form-control" checked/> NEFT
                                            <input type="radio" name="type" id="type" value="Imps" class="form-control"/> IMPS
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="inputBox">
                                                <input type="text" id="mobile_number" name="mobile_number" class="form-control" placeholder="Please Enter mobile Number" maxlength="10"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="inputBox">
                                                <input type="text" id="ifsc_code" name="ifsc_code" class="form-control" placeholder="Please Enter IFSC Code " maxlength="11"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="inputBox">
                                                <input type="text" id="bene_acc_no" name="bene_acc_no" class="form-control" placeholder="Please Enter Beneficiary Account Number" maxlength="20"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="inputBox">
                                                <input type="text" id="bene_name" name="bene_name" class="form-control" placeholder="Please Enter Beneficiary Name " maxlength="25"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="inputBox">
                                                <input type="text" id="amount" name="amount" class="form-control" placeholder="Please Enter Amount "/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="inputBox">
                                                <input type="text" id="customer_name" name="customer_name" class="form-control" placeholder="Please Enter Customer Name" maxlength="25"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="inputBox">
                                                <input type="text" id="rfu1" name="rfu1" class="form-control" placeholder="Please Enter Remarks "maxlength="200"/>
                                            </div>
                                        </div>
                                    </div>
                                 <!--   <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="inputBox">
                                                <input type="text" id="rfu2" name="rfu2" class="form-control" placeholder="Please enter RFU1" maxlength="200"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="inputBox">
                                                <input type="text" id="rfu3" name="rfu3" class="form-control" placeholder="Please enter RFU1" maxlength="200"/>
                                            </div>
                                        </div>
                                    </div>-->
                                   <!--input type="hidden" name="status" id="status" value="0"/-->
                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                    <div class="form-group">
                                        <div class="col-lg-offset-4">
                                            <button class="btn btn-primary" id="transfer"  type="button" onclick='save()'>Transfer</button>                                
                                        </div>
                                    </div>

</form>
                                    <?php echo form_close(); ?>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>

    <!-- Signin Content End -->             
</div>
<!-- Inner Wrapper End -->



<script type="text/javascript">
    function save(){
      //alert('hello');
       var dataString = $('#fino_form').serialize();
       $.ajax({
          url:'<?php echo base_url("admin/Fino/neft");?>',
           data: dataString,
           async: false,
            type:'POST',
            dataType:'json',
           success: function(data){
            // alert(JSON.stringify(data));
            alert(data.DisplayMessage)
               
           }
 
         });
    }

    $('#fino').bootstrapValidator({
        group: '.form-control',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            type: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b>Please Select Type.</font>'
                    }
                }
            },
              mobile_number: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b>Please Enter Mobile Number.</font>'
                    },
                    regexp: {
                        regexp: /^[789]\d{9}$/,
                        message: '<b><font style="color:red; size="1">X</b>Enter Valid Mobile Number</font>'
                    }
                }
            },
            ifsc_code: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b>Please Enter IFSC CODE.</font>'
                    }
                }
            },
            bene_acc_no: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b>Please Enter Account Number.</font>'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '<b><font style="color:red; size="1">X</b>Enter Valid Account Number</font>'
                    }
                }
            },
            
            bene_name: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b> Please Enter Beneficiary Name.</font>'
                    },
                    regexp:{
                      regexp: /^[a-z]+$/i,
                    message: '<b><font style="color:red; size="1">X</b>Enter Valid Beneficiary Name</font>'

                    }
                }
            },
            amount: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b> Please Enter Amount.</font>'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '<b><font style="color:red; size="1">X</b>Enter valid Amount</font>'
                    }
                }
            }
        }
    });
</script>
