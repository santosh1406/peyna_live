<section class="content">
    <div id="divLoading" style="margin: 0px; padding: 0px; position: fixed; right: 0px; top: 0px; width: 100%; height: 100%; background-color: rgb(102, 102, 102); z-index: 30001; opacity: 0.8;display: none">
        <p style="position: absolute; color: White; top: 38%; left: 45%;">
            Transferring, please wait...
            <img src="http://loadinggif.com/images/image-selection/3.gif">
        </p>
    </div>
    <div id="divLoadingautofill" style="margin: 0px; padding: 0px; position: fixed; right: 0px; top: 0px; width: 100%; height: 100%; background-color: rgb(102, 102, 102); z-index: 30001; opacity: 0.8;display: none">
        <p style="position: absolute; color: White; top: 38%; left: 45%;">
            Loading...
            <img src="http://loadinggif.com/images/image-selection/3.gif">
        </p>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header "> 
                    <h3 class="box-title">Domestic Money Transfer</h3>
                </div>
                <!--div class="panel panel-default"-->
                <div class="box-body">
                    <div class="alert alert-success alert-dismissable" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;display: none;">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> Please Do the Account verification first then perform DMT                               
                    </div>
                    <!--div class="col-md-12 col-md-12 container-c"-->
                    <!--div class="formBox"-->
                    <div id='dmt'>
                        <?php
                        if ($this->session->tempdata('msg')) {
                            ?>
                            <div class="alert alert-success alert-dismissable" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->tempdata('msg'); ?>
                            </div>
                            <?php
                        }
                        $attributes = array("method" => "POST", "id" => "dmt_form", "name" => "dmt");
                        echo form_open(site_url() . '/admin/dmt/neft', $attributes);
                        ?>
                        <input type="hidden" name="recharge_from" id="recharge_from" value="web"/>
                        <input type="hidden" name="customer_charge" id="customer_charge" value=""/>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">       
                            <label class="col-lg-4 control-label">Select type *:</label>
                            <div class="col-lg-6">
                                <input type="radio" name="type" id="type" value="Neft" class="form-control" checked/> NEFT
                                <input type="radio" name="type" id="type" value="Imps" class="form-control"/> IMPS
                            </div>
                        </div>

                        <div class="clearfix" style="height: 5px;clear: both;"></div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">Beneficiary Account Number*:</label>  
                            <div class="col-lg-6">
                                <input type="text" id="BeneAccountNo" name="BeneAccountNo"  class="form-control" placeholder="Please Enter Beneficiary Account Number"  minlength="8" maxlength="19" value="<?php echo set_value('BeneAccountNo') ?>" />
                                <?php //echo form_error('BeneAccountNo'); ?>
                            </div>
                             <!-- <span class="popuptext" id="myPopup" style="display: none;">
                                Please Do the Account verification first then perform DMT </span> -->
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">Sender Mobile Number *:</label>  
                            <div class="col-lg-6">
                                <input type="text" id="CustomerMobileNo" name="CustomerMobileNo" class="form-control" placeholder="Please Enter mobile Number" maxlength="10" value="<?php echo set_value('CustomerMobileNo'); ?>" />
                                <?php //echo form_error('CustomerMobileNo'); ?>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 5px;clear: both;"></div>

                        <div class="from-group">
                            <label class="col-lg-4 control-label">Sender Name*:</label>  
                            <div class="col-lg-6">
                                <input type="text" id="CustomerName" name="CustomerName" class="form-control" placeholder="Please Enter Customer Name" maxlength="25" value="<?php echo set_value('CustomerName'); ?>" />
                                <?php //echo form_error('CustomerName'); ?>
                            </div>

                        </div>
                        <div class="clearfix" style="height: 5px;clear: both;"></div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">Beneficiary IFSC Code *:</label>  
                            <div class="col-lg-6">
                                <input type="text" id="BeneIFSCCode" name="BeneIFSCCode" class="form-control" placeholder="Please Enter IFSC Code " maxlength="11" value="<?php echo set_value('BeneIFSCCode'); ?>" />
                                <?php //echo form_error('BeneIFSCCode'); ?>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 5px;clear: both;"></div>


                        <div class="from-group">
                            <label class="col-lg-4 control-label">Beneficiary Name*:</label>  
                            <div class="col-lg-6">
                                <input type="text" id="BeneName" name="BeneName" class="form-control" placeholder="Please Enter Beneficiary Name " maxlength="20" value="<?php echo set_value('BeneName'); ?>" />
                                <?php //echo form_error('BeneName'); ?>
                            </div>
                        </div>
                        <div class="clearfix" style="height: 5px;clear: both;"></div>

                        <div class="from-group">
                            <label class="col-lg-4 control-label">Amount*:</label>  
                            <div class="col-lg-6">
                                <input type="text" id="Amount" name="Amount" class="form-control" value="<?php echo set_value('Amount'); ?>" placeholder="Please Enter Amount "/>
                                <?php //echo form_error('Amount'); ?>
                            </div>
                        </div>

                        <div class="clearfix" style="height: 5px;clear: both;"></div>

                        <div class="from-group">
                            <label class="col-lg-4 control-label">Remark:</label>  

                            <div class="col-lg-6">
                                <input type="text" id="rfu1" name="rfu1" class="form-control" value="<?php echo set_value('rfu1'); ?>"placeholder="Please Enter Remarks" maxlength="200"/>

                            </div>
                        </div>
                        <!--   <div class="row">
                               <div class="col-sm-12 col-xs-12">
                                   <div class="inputBox">
                                       <input type="text" id="rfu2" name="rfu2" class="form-control" placeholder="Please enter RFU1" maxlength="200"/>
                                   </div>
                               </div>
                           </div>
                           <div class="row">
                               <div class="col-sm-12 col-xs-12">
                                   <div class="inputBox">
                                       <input type="text" id="rfu3" name="rfu3" class="form-control" placeholder="Please enter RFU1" maxlength="200"/>
                                   </div>
                               </div>
                           </div>-->
                        <!--input type="hidden" name="status" id="status" value="0"/-->
                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                        <?php echo validation_errors('<div class="error">', '</div>'); ?>
                        <center><img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image" style="display:none;"/></center>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-6">
                                <button class="btn btn-primary" id="transfer"  type="submit">Transfer</button>                                
                                <a class="btn btn-primary" href="<?php echo base_url('admin/dashboard'); ?>" type="button">Cancel</a> 

                            </div>
                            <!--/div-->


                        </div>

                        <!--/div-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Transfer Details</h4>
            </div>
            <div class="modal-body" style="font-size: 18px">
                <p><span>Amount:</span>
                    <span id="amount_span" style="float: right; margin-right: 20px"></span>
                </p>
                <p><span>Service fee:</span>
                    <span id="charges_span" style="float: right; margin-right: 20px"></span>
                </p>
                <hr style="margin: 0px">
                <p><span style="color: #80067f"><strong>Total Transfer Amount:</strong></span>
                    <span  id="total_charge" style="float: right; margin-right: 20px"></span>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" id="proceed" class="btn btn-primary">Proceed</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>

    </div>
</div>

<!-- Signin Content End -->             
<!--/div-->
<!-- Inner Wrapper End -->

<script type="text/javascript">
    $(document).ready(function() {
        var min_dmt_amt = '<?php echo MIN_DMT_AMT; ?>';
        var max_dmt_amt = '<?php echo MAX_DMT_AMT; ?>';

        $('#dmt_form').validate({ 
            rules: {
                CustomerMobileNo: {
                    required:true,
                    number:true,
                    minlength:10,
                    maxlength:10,
                    valid_mobile_no: true,
                    mobileno_start_with: true
                },
                CustomerName: {
                    required: true, 
                    valid_benename :true                 
                },
                BeneIFSCCode: { 
                    required:true,                   
                    //number:true,
                    valid_ifsc:true
                },
                BeneAccountNo: {
                    required:true,
                    number:true
                    /*minlength:20,
                    maxlength:20 */
                },
                BeneName: {
                    required:true,
                    valid_benename :true
                },
                BeneAccountNo: {
                    required:true,   
                    number:true,
                    valid_beneacct:true
                    /*   minlength:20,
                 maxlength:20*/
                },
                Amount: {
                    required:true,   
                    number:true,
                    valid_amt:true,
                    // min_limt:true           
                    /* min: min_dmt_amt,
                    max: max_dmt_amt    */    
                    min:100,
                    max:5000 
                },
                rfu1: {
                    required:true,                   
                },
            },
            messages: {

                CustomerMobileNo: {
                    required:"Please Enter Customer Mobile No",
                    number:"Mobile no should be number only.",
                    minlength: "Please enter 10 digit Mobile No",
                    maxlength: "Please enter 10 digit Mobile No", 
                    valid_mobile_no: "Please enter valid Mobile Number",
                    mobileno_start_with: 'Mobile No should start with 7 or 8 or 9'
                },
                CustomerName: {
                    required: "Please Enter Customer Name", 
                    valid_benename :"Please Enter Valid Customer Name"                 
                },
                BeneIFSCCode: {    
                    required:"Please Enter Beneficiary IFSC Code", 
                    valid_ifsc:"Please Enter Valid IFSC Code"
                },
                BeneAccountNo: {
                    required:"Please Enter Beneficiary Account No",                    
                    valid_beneacct:"Please Enter Valid Beneficiary Account No"                 
                },
                BeneName: {
                    required:"Please Enter Beneficiary Name",
                    valid_benename :"Please Enter Valid Beneficiary Name",  
                },                  
                Amount: {
                    required:"Please Enter Amount",                   
                    valid_amt:"Please Enter Valid Amount",
                    min_limt:"Please Enter amount Greater than equal to "+ min_dmt_amt+"",                   
                },
                rfu1: {
                    required:"Please Enter Remark",           
                },                
            },
            submitHandler: function (form) { 
                // submit.form();
                form.submit();
            }
        }); 
        $.validator.addMethod("check_numbers", function (value, element) {
            var flag=false;
            var TempAmount = parseInt(value);
            if(TempAmount <=0){
                flag = false;   
            }else{
                flag = true;    
            }
            return flag;
        }, 'Number Should Be Greater Than 0.');

        $.validator.addMethod("valid_mobile_no", function (value, element) {
            return (value == "0000000000" || value == "9999999999") ? false : true;
        }, 'Please enter valid mobile number');

        $.validator.addMethod("mobileno_start_with", function (value, element) {
            var regex = new RegExp("^[7-9]{1}[0-9]{9}$");
            if (value.length > 0)
            {
                if (regex.test(value))
                {
                    return true;
                } else {
                    return false;
                }
            } else
            {
                return true;
            }
        }, 'Mobile No. should start with 7 or 8 or 9 '); 


        $.validator.addMethod("valid_mobile_no", function (value, element) {
            return (value == "0000000000" || value == "9999999999") ? false : true;
        }, 'Please enter valid mobile number');

        $.validator.addMethod("mobileno_start_with", function (value, element) {
            var regex = new RegExp("^[7-9]{1}[0-9]{9}$");
            if (value.length > 0)
            {
                if (regex.test(value))
                {
                    return true;
                } else {
                    return false;
                }
            } else
            {
                return true;
            }
        }, 'Mobile No. should start with 7 or 8 or 9 ');

        jQuery.validator.addMethod("valid_ifsc", function(value, element) {
            //return this.optional(element) || /^[a-z]{4}\d{7}$/i.test(value);
            return this.optional(element) || /^[a-z]{4}[A-Za-z0-9]{7}$/i.test(value);
        }, "");

        jQuery.validator.addMethod("valid_benename", function(value, element) {
            return this.optional(element) || /^[a-z\sA-Z]+$/i.test(value);
        }, "Please Enter Valid Beneficiary Name");

        $.validator.addMethod("valid_amt", function (value, element) {
            var regex = new RegExp("^[0-9]+$");             
            if(parseInt(value) > 0 && regex.test(value))
            {
                return true;  
            }
            else
            {
                return false;
            }                        
        },'Please Enter Valid Amount');

        $.validator.addMethod("valid_beneacct", function (value, element) {
            var regex = new RegExp("^[0-9]{8,19}$");
            if(value.length > 0)                          
            {
                if(parseInt(value) > 0 && regex.test(value))
                {
                    return true;  
                }
                else
                {
                    return false;
                }
            }
            return true;         
        },'Please Enter Valid Beneficiary Account No.');

        $(document).on("click","#transfer",function(e){
            e.preventDefault();
            // $('#dmt_form').attr('action', "<?php echo base_url(); ?>admin/dmt/neft").submit();
            // $(".img-circle").hide();
            if($("#dmt_form").valid()){

                /* Ajax request to get the customer charge based on slab - Gopal */
                var url = "<?php echo site_url() . '/admin/dmt/get_cc_from_slab' ?>";
                var amount = $('#Amount').val();
                $('#amount_span').text(amount);

                $.ajax({
                    type:"POST",
                    url: url,
                    data: {amount: amount},
                    success:function(response){   
                        $('#customer_charge').val(response);
                        $('#charges_span').text(response);
                        $('#total_charge').text(parseInt(amount) + parseInt(response));
                        $('#myModal').modal('show');
                    }
                });
                /* Ajax request to get the customer charge based on slab - Gopal */

            }
        }); 

        $('input').on('blur', function() {
            if ($("#dmt_form").valid()) {
                $('#transfer').prop('disabled', false);  
            } else {
                $('#transfer').prop('disabled', 'disabled');
            }
        });

        $(document).on('click', '#proceed', function(){
            // $(".img-circle").show();
            $('#myModal').modal('hide');
            $('#proceed').attr('disabled', 'disabled');
            // $('#proceed').text('Transfering...');
            $('#dmt_form').submit();
            $('#divLoading').css('display', 'block');
        });

        $.validator.addMethod("min_limt", function (value, element) {
            var flag=false;
            var TempAmount = parseInt(value);
            if(TempAmount < min_dmt_amt){
                flag = false;   
            }else{
                flag = true;    
            }
            return flag;
        }, 'Please enter amount greater than or equal to '+min_dmt_amt);  

        $(document).on("change","#BeneAccountNo",function(e){
            e.preventDefault();
            /* Ajax request to get the customer charge based on slab - Gopal */
            var url = "<?php echo site_url() . '/admin/dmt/check_bene_acct_exists' ?>";
            var BeneAccountNo = $('#BeneAccountNo').val();               
            $.ajax({
                type:"POST",
                url: url,
                data: {BeneAccountNo: BeneAccountNo},

                success:function(response){  
                    // if(response.exists==1) {
                    if(response==1) {
                        $('#divLoadingautofill').css('display', 'block');
                        $("#transfer").attr("disabled",false);
                        /* $.each(response.acct_info,function(key,value){alert(value.CustomerMobileNo);
                        $("CustomerMobileNo").text(value.CustomerMobileNo);
                        $("BeneName").text(value.BeneName);
                        $("BeneIFSCCode").text(value.BeneIFSCCode);
                        $("CustomerName").text(value.CustomerName);
                    })*/
                        //If account verification is done then autofill form dields based on DB values
                        var BenAccountNo = $('#BeneAccountNo').val();    
                        var url =  "<?php echo site_url() . '/admin/dmt/autofill_fields' ?>";
                        // AJAX 
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: {BenAccountNo: BenAccountNo},
                            success:function(response){
                                $('#divLoadingautofill').css('display', 'none');
                                var obj = JSON.parse(response);
                                // Set value to textboxes
                                document.getElementById('CustomerMobileNo').value = obj.CustomerMobileNo;
                                document.getElementById('CustomerName').value = obj.CustomerName;
                                document.getElementById('BeneIFSCCode').value = obj.BeneIFSCCode;
                                document.getElementById('BeneName').value = obj.BeneName;
                                document.getElementById('rfu1').value = obj.comments;
                                $("#CustomerMobileNo").removeClass("error");
                                $("#CustomerName").removeClass("error");
                                $("#BeneIFSCCode").removeClass("error");
                                $("#BeneName").removeClass("error");
                                $("#rfu1").removeClass("error");
                            }
                        });

                    } else if(response==0) {
                        $("#transfer").attr("disabled","disabled");
                        // $("#dmt_form").validate().settings.ignore = "*";
                        //$(".alert").attr("display","block");                    
                        $(".alert").show();
                        $('#dmt_form')[0].reset();
                        $('#dmt_form').validate({
                            ignore: "#dmt_form"
                        });
                        $("#dmt_form input").attr('disabled',true);
                    } else{

                    }
                }
            });  
        });    
    
        $(document).on('keydown', '.username', function() {
            var id = this.id;
            var splitid = id.split('_');
            var index = splitid[1];

            // Initialize jQuery UI autocomplete
            $( '#'+id ).autocomplete({
                source: function( request, response ) {
                    $.ajax({
                        url: "getDetails.php",
                        type: 'post',
                        dataType: "json",
                        data: {
                            search: request.term,request:1
                        },
                        success: function( data ) {
                            response( data );
                        }
                    });
                },
                select: function (event, ui) {
                    $(this).val(ui.item.label); // display the selected text
                    var userid = ui.item.value; // selected value

                    // AJAX
                    $.ajax({
                        url: 'getDetails.php',
                        type: 'post',
                        data: {userid:userid,request:2},
                        dataType: 'json',
                        success:function(response){

                            var len = response.length;

                            if(len > 0){
                                var id = response[0]['id'];
                                var name = response[0]['name'];
                                var email = response[0]['email'];
                                var age = response[0]['age'];
                                var salary = response[0]['salary'];

                                // Set value to textboxes
                                document.getElementById('name_'+index).value = name;
                                document.getElementById('age_'+index).value = age;
                                document.getElementById('email_'+index).value = email;
                                document.getElementById('salary_'+index).value = salary;

                            }

                        }
                    });

                    return false;
                }
            });
        });

    }); 
    /* function save() {

        var dataString = $('#dmt_form').serialize();
        // console.log(dataString);
        //$("#transfer").attr('disabled', 'disabled');
        // $("#transfer").html("Loading......");
        $.ajax({
            url: '<?php //echo base_url("admin/Dmt/neft");  ?>',
            data: dataString,
            async: false,
            type: 'POST',
            dataType: 'json',
            success: function(data) {alert(data.status);
              //  $("#transfer").attr('disabled', false);
                // $("#transfer").html("Transfer");
                if (data.status == 'failed') {
                    var msg = data.error;
                    for (var i in msg) {
                        alert(msg[i]);
                    }
                   // $("#transfer").attr('disabled', false);
                   // $("#transfer").html("Transfer");

                } else {
                    alert(data.DisplayMessage)
                }
             //  $("#transfer").prop('disabled', false);
               // $("#transfer").html("Transfer");
                //document.getElementById("dmt_form").reset();
            }
           // alert('dss');

        });
    }

    $('#dmt_form').bootstrapValidator({
        group: '.form-control',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            type: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b>Please Select Type.</font>'
                    }
                }
            },
            CustomerMobileNo: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b>Please Enter Mobile Number.</font>'
                    },
                    regexp: {
                        regexp: /^[789]\d{9}$/,
                        message: '<b><font style="color:red; size="1"></b>Enter Valid Mobile Number</font>'
                    }
                }
            },
            BeneIFSCCode: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b>Please Enter IFSC CODE.</font>'
                    },
                    regexp: {
                        regexp: /^[a-z]{4}\d{7}$/i,
                        message: '<b><font style="color:red; size="1"></b>Please Enter Valid IFSC CODE.</font>'
                    }
                }
            },
            BeneAccountNo: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b>Please Enter Account Number.</font>'
                    },
                    callback: {
                        message: '<b><font style="color:red; size="1"></b>Please Enter valid Account Number</font>',
                        callback: function(value, validator, $field) {
                            var regex = new RegExp("^[0-9]{19}$");
                            if(value.length > 0)                          
                            {
                                if(parseInt(value) > 0 && regex.test(value))
                                {
                                    // success
                                }
                                else
                                {
                                    return false;
                                }
                            }
                            return true;                          
                        }
                    }
                }
            },
            BeneName: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b> Please Enter Beneficiary Name.</font>'
                    },
                    regexp: {
                        regexp: /^[a-z]+$/i,
                        message: '<b><font style="color:red; size="1"></b>Enter Valid Beneficiary Name</font>'

                    }
                }
            },
            Amount: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b> Please Enter Amount.</font>'
                    },
                    callback: {
                        message: '<b><font style="color:red; size="1"></b>Please Enter valid Amount</font>',
                        callback: function(value, validator, $field) {
                            var regex = new RegExp("^[0-9]+$");
                            //if(value.length > 0)                          
                            //{
                                if(parseInt(value) > 0 && regex.test(value))
                                {
                                    // success
                                }
                                else
                                {
                                    return false;
                                }
                            //}
                          //  return true;                          
                        }
                    }
                    
                    
                    
                 /*   regexp: {
                        regexp: /^[0-9]+$/,
                        message: '<b><font style="color:red; size="1"></b>Enter valid Amount</font>'
                    }*/
    /*   }
           },
            CustomerName: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b> Please Enter Customer Name.</font>'
                    },
                    regexp: {
                        regexp: /^[a-z]+$/i,
                        message: '<b><font style="color:red; size="1"></b>Enter Valid Customer Name</font>'

                    }
                }
            }

        }

    }); */



</script>
