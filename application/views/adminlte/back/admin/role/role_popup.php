<div class="modal fade popup" id="add_edit_popup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Compose New Message</h4>
            </div>
			
				<?php
				$attributes = array("method" => "POST", "name" => "role_form", "id" => "role_form", "onsubmit" => "return false;");
				echo form_open(base_url().'admin/role/save', $attributes);
				?>
                <div class="modal-body">
                     <div class="form-group">
                        <label for="name">Name:</label>
                        <input name="id" type="hidden" id="id" class="form-control"/>
                        <input name="role_name" type="text" id="role_name" class="form-control required" placeholder="Role Name" data-rule-required="true" data-rule-fullname="true" data-msg-required="Please enter role name" required/>
                    </div>
                    <div class="form-group">
                        <label for="link">Home Page:</label>
                        <input type="text" name="home_page" id="home_page" class="form-control required" placeholder="Home Page"   data-rule-required="true" data-msg-required="Please enter home page" required/>
                    </div>
                </div>

                <div class="modal-footer clearfix">

                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>

                    <button type="submit" id="submit_btn" class="btn btn-primary pull-left"><i class="fa fa-envelope"></i> Save Role</button>
                </div>
            </form>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
