
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Manage Roles</h3>
                <span style="float: right;margin-top: 10px;margin-right: 10px;"><button class="btn btn-primary add_btn">Add Role</button></span>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="example" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="20px">id</th>
                            <th width="">Role Name</th>
                            <th width="">Home Page</th>
                            <th width="">status</th>
                            <th width="100px">Action</th>
                        </tr>
                    </thead>
                    <tbody>                                           

                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>
