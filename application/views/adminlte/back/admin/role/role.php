<section class="content-header">
    <h1> Dashboard <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div id="role_wrapper">
        <?php $this->load->view(ROLE_AJAX); ?>
    </div>

    <?php $this->load->view(ROLE_POPUP); ?>

</section>
<script src="<?php echo base_url()?>js/back/admin/admin_role.js"></script>

