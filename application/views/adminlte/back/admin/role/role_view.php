<div class="confirm-div"></div>
<h1>Roles   <a href="<?php echo base_url(); ?>index.php/admin/role/add_role_view">Add roles</a></h1>
<table width="100%">
    <tr>
        <th>#</th>
        <th>Role Name</th>    
        <th>Home Page</th>    
        <th>Action</th>   
    </tr>

    <?php
    if (count($role_data) > 0) {
        $count = 0;
        foreach ($role_data as $roledata) {
            $count++;
            echo "<tr>";
            echo "<td>" . $count . "</td>";
            echo "<td>" . $roledata["role_name"] . "</td>";
            echo "<td>" . $roledata["home_page"] . "</td>";
            echo "<td>";
            echo "<a href='" . base_url() . "index.php/admin/role/edit_role/" . $roledata["id"] . "'>Edit</a>&nbsp;";
            echo "<a href='" . base_url() . "index.php/admin/role/remove_role/" . $roledata["id"] . "'>Delete</a>";
            echo "</td>";
            echo "</tr>";
        }
    } else {
        echo "<tr><td colspan='5'> There are no role.</td></tr>";
    }
    ?>
</tr>
</table>