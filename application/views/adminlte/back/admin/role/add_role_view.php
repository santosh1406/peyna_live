<script>
    $(document).ready(function () {
        /************** for confirm message***********/
        $('.confirm-div').hide();
<?php if ($this->session->flashdata('invalid')) { ?>
            $('.confirm-div').html('<?php echo $this->session->flashdata('invalid'); ?>').show();
<?php } else if ($this->session->flashdata('invalid')) { ?>
            $('.confirm-div').html('<?php echo $this->session->flashdata('valid'); ?>').show();
<?php } ?>
        /*********************************************/
    });
    
$("#add_role_form_validate").validate({
            rules: {
                 role_name: {
                    required: true     
                },
                role_home_page: {
                    required: true
                }
            }
        });
</script> 
<div class="confirm-div"></div>
<h1>Add Role</h1>
<?php
$attributes = array("method" => "POST", "id" => "add_role_form_validate", "class" => "cmxform form");
echo form_open('admin/role/add_role_action', $attributes);
?>

<p class="field">
    <label>Name</label>
    <input type="text" placeholder="Name" id="role_name" name="role_name" data-rule-required="true" data-rule-fullname="true" data-msg-required="Please enter role name" required/>
    <i class="fa fa-user"></i>
</p>
<p class="field">
    <label>Home Page</label>
    <input type="text" placeholder="Home Page" id="role_home_page" name="role_home_page" data-rule-required="true" data-msg-required="Please enter home page" required/>
    <i class="fa fa-envelope"></i>
</p> 

<p class="submit"><input type="submit" value="Add Role"></p>
<?php echo form_close(); ?>