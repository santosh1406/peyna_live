<style>
    //added by Sonali Kamble on 03-11-2018
    .overflowClass{
        //border: 1px solid #ccc;
        height: 50px;
        width: 48.5%;
        overflow-x:scroll;
        overflow-x:hidden;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
        <section class="content-header">
            <h2 style="margin-left:20px">Edit Sub Services</h2>
        </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> </div>
                    <div class="box-body">
                        <?php echo validation_errors('<div class="error">', '</div>'); ?>
                        <?php
                        $attributes = array("method" => "POST", "id" => "edit_registration_form", "name" => "edit_registration_form");
                        echo form_open(base_url() . 'admin/services/update_service', $attributes);
                        ?>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label" for="agent_state">Select Service *:</label>
                            <div class="col-lg-6">
                                <select class="form-control"  id="service" name="service_name">
                                        <option value="">Select Service</option>
                                        <?php
                                   foreach($services as $service){ 
                                        
                                    ?>

                                        <option value="<?php echo $service['id'] ?>" <?php if($service['id'] == 1)  { ?> selected="selected"<?php } ?> ><?php echo $service['service_name'] ?></option>
          
                                  <?php  }
                                ?>
                                    </select> 
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group intent"> 
                            <label class="col-lg-3 control-label" for="sub_service">Select Sub Services *:</label>
                            <div class="col-lg-6 overflowClass" id="sub_service" > 
                                   <?php    
                                   if($agentServices!='')
                                   {
                                    foreach ($agentServices as $key => $value) { ?>
                                      
                                      <input type ="checkbox" name ="sub_service_name[]" id ="sub_service_name" value="<?php echo $value['id']?>" <?php if($value['id']==$value['sub_service_id']){echo 'checked';} ?> > <?php echo  $value['service_name']?><br>


                                  <?php }
                                   }
                                   
                                    ?>                                          
                            </div> 
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div> 
                        <hr> 
                        <div class="form-group">
                            <div class="col-lg-offset-5">
                                <button class="btn btn-primary" id="submit_btn" name="submit_btn" type="submit">Save</button> 
                                <a class="btn btn-primary" href="<?php echo base_url('admin/registration') ?>" type="button">Back</a> 
                            </div>
                        </div>

                        <?php form_close(); ?>
                        <!--</div> /.box-body--> 
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<script type='text/javascript'>
  // baseURL variable
  var baseURL= "<?php echo base_url();?>";
 
  $(document).ready(function(){
 
    // City change
    $('#service').change(function(){

        //event.preventDefault();
      var service = $(this).val();

      // AJAX request
      $.ajax({
        url:'<?=base_url()?>admin/services/getSubServicesbyService',
        method: 'post',
        data: {service: service},
        dataType: 'json',
        success: function(response){
            
          // Remove options 

         //$('#sub_service').find('option').not(':first').remove();
         $('#sub_service').empty();

          // Add options
          $.each(response,function(index,data){

            if(data['id']==data['sub_service_id'])
            {
                 var checked = 'checked';
            }
            else
            {
                var checked = '';
            }

             $('#sub_service').append('<input type ="checkbox" name ="sub_service_name[]" id ="sub_service_name" value="'+data['id']+'" '+checked+'   class="icheckbox_minimal" />'+data['service_name']+'<br>');
/*<option value="'+data['id']+'">'+data['service_name']+'</option>*/

          });
        }
     });
   });

   
 
 });
 </script>
