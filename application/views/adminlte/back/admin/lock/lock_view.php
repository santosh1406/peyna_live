<section class="content-header">
    <h2>SA Lock List</h2>
    <div style="display: block;float: right;margin-top: 2%;" class="form-group">
        <button  class="btn btn-primary" id="extoexcel" type="button">
      <!--   <span class="glyphicon glyphicon-export"></span>Export To Excel</button>   -->
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div id="menu_wrapper">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                                <table id="display_table" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th width="20px">Sr No.</th>
                                        <th width="20px">Name</th>
                                        <th width="20px">Agent Code</th>
                                        <th width="20px">Lock Status</th>
                                        <th width="20px">Lock Reason</th>
                                        <th width="20px">Lock By Whom</th>
                                        <th width="20px">Unlock By Whom</th>
                                        <th width="20px">Action</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>                                           

                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
    </div>

</section>

<script type="text/javascript">
$(document).ready(function() {

 var tconfig = {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": BASE_URL + "admin/lock/get_lock_list",
            "type": "POST",
            "data": "json",
            data: function(d){               
            }
        },    
        "iDisplayLength": 10,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        //        "paginate": true,
        "paging": true,
        "searching": true,
        "oLanguage": {
                "sProcessing": '&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url().BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>' 
            },
        "aoColumnDefs": [
         //  {"bVisible": false, "aTargets": [1]},
           // {"bSearchable": false, "aTargets": [6,7,8,9,10]}
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    $("td:first", nRow).html(info.start + iDisplayIndex + 1);
                   
                    var active_btn = 'fa fa-square-o';
                    var title = 'Unlock';
                    if (aData[3] == 'Y') 
                    {
                        active_btn = 'fa fa-check-square-o';
                        title = 'Lock';
                    }

                     var btn = '';                   
                     btn += '<a title="'+ title +'" class="del_btn" ref="' + aData[0] + '" data-ref="'+ aData[3] +'" data-status="'+ aData[7]+'"><i class="' + active_btn + '"></i></a>';
                     $("td:last", nRow).html(btn);
        }    

    };

    var oTable = $('#display_table').dataTable(tconfig);

    $(document).off('click','.del_btn').on('click','.del_btn', function(e) {
        e.preventDefault();
        var id = $(this).attr('ref');
        var act = $(this).attr('data-ref');
        var status = $(this).attr('data-status');
     
        var detail={};
        var flag = 0;
        var call =0;
     
        if(act== "Y"){
            var postyn = "N";
            var act1 = "unlock";           
            detail['unlock_user_id'] = '<?php echo $this->session->userdata('id');?>';
            call = 1;

        }
        else if(act=="N") {
            var postyn = "Y";
            var act1 = "lock";            
            if(status=="Y") {                            
                var yesno  = confirm("This user is registered to device. Still you want to lock it ");               

            } 
            else { 
                flag =1;
            }
            if(yesno==1|| flag ==1) { 
                var reason = prompt("Please enter reason for locking this user ", "");
                if (reason!= null) { 
                    detail['reason'] = reason;
                    detail['lock_user_id'] = '<?php echo $this->session->userdata('id');?>';
                    call =1;
                }              
                
            }
        }
        if(call==1) {
            var div="";
            var ajax_url = 'admin/lock/change_status';
            var form ="";
            detail['id'] = id;
            detail['postyn'] = postyn;
               // var yesno = confirm("Are you sure you want to "+ act1 +" this user");
           // if(yesno) {
                get_data(ajax_url,form,div,detail,function(response) {
                   
                    if(response.flag == '@#success#@') {
                        location.reload();
                    }
                    else{
                        alert(response.msg);
                    }
                },'',false);
        }
    }); 
   

});
</script>