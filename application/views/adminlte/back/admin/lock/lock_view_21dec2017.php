<section class="content-header">
    <h2>SE Lock List</h2>
    <div style="display: block;float: right;margin-top: 2%;" class="form-group">
        <button  class="btn btn-primary" id="extoexcel" type="button">
      <!--   <span class="glyphicon glyphicon-export"></span>Export To Excel</button>   -->
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div id="menu_wrapper">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                                <table id="display_table" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th width="20px">Sr No.</th>
                                        <th>Name</th>
                                        <th>Agent Code</th>                                 
                                        <th>Lock Status</th>
                                        <th>Action</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>                                           

                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
    </div>

</section>





<script type="text/javascript">
$(document).ready(function() {

 var tconfig = {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": BASE_URL + "admin/lock/get_lock_list",
            "type": "POST",
            "data": "json",
            data: function(d){
                //d.rokad_token = rokad_token;
            }
        },    
        "iDisplayLength": 10,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        //        "paginate": true,
        "paging": true,
        "searching": true,
        "aoColumnDefs": [
         //  {"bVisible": false, "aTargets": [1]},
           // {"bSearchable": false, "aTargets": [6,7,8,9,10]}
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    $("td:first", nRow).html(info.start + iDisplayIndex + 1);
                   
                    var active_btn = 'fa fa-square-o';
                    if (aData[3] == 'Y') 
                    {
                        active_btn = 'fa fa-check-square-o';
                    }

                     var btn = '';                   
                     btn += '<a title="Lock" class="del_btn" ref="' + aData[0] + '" data-ref="'+ aData[3] +'"><i class="' + active_btn + '"></i></a>';
                     $("td:last", nRow).html(btn);
        }
    // "aoColumns": [
    //     { "mData": "id" },
    //     { "mData": "amount" },
    //     { "mData": "status" },
    //     { "mData": "action" }
    //  ]        

    };

    var oTable = $('#display_table').dataTable(tconfig);

    $(document).off('click','.del_btn').on('click','.del_btn', function(e) {
        e.preventDefault();
        var id = $(this).attr('ref');
        var act = $(this).attr('data-ref');
        if(act=="Y"){
            var postyn = "N";
            var act1 = "unlock";

        }
        else if(act=="N") {
            var postyn = "Y";
            var act1 = "lock";
        }
        var detail={};
        var div="";
        var ajax_url = 'admin/lock/change_status';
        var form ="";
        detail['id'] = id;
        detail['postyn'] = postyn;

        var yesno = confirm("Are you sure you want to "+ act1 +" this user");
        if(yesno) {
            get_data(ajax_url,form,div,detail,function(response) {
               
                if(response.flag == '@#success#@') {
                    location.reload();
                }
                else{
                    alert(response.msg);
                }
            },'',false);
        }
    }); 
   

});
</script>