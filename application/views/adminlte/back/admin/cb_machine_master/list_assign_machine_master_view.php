
<section class="content-header">
    <h1> Assign Machines
    </h1>
</section>
                     
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <!--  <?php //if ($this->session->flashdata('msg')) { ?>
                    <div class="alert alert alert-success alert-error" style="display:block" id="msg">
                    <?php //echo $this->session->flashdata('msg') ?> 
                    </div>
<?php  //} ?> -->
                  <div align="left" style="margin: 20px;">
                   <a class="btn btn-primary"  href="<?php echo site_url() . '/admin/cb_machine_master/list_agent_Device'?>" type="button" style="color: #FFFFFF;">Back</a> 
                    <h4 align="right"><b><?php echo ucfirst($agent_name); ?></b></h4>
                </div>

                    <input type="hidden"   name='agent_id' id='agent_id' value="<?php echo  $agent_id; ?>"
                </div>
                </div><!-- /.box-header -->
                <div class="box-body">
            <div class="table-responsive">
               <table id="example2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th> 
                                    <th>IMEI NO</th>
                                    <th>SIM NO</th> 
                                    <!-- <th>Action</th>   -->
                                </tr>
                            </thead>
                            <tbody>
                               


                            </tbody>

               </table></div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
    <!-- /.content-wrapper -->


<script>
  
 $(document).ready(function() { 
     var tconfig = {        
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": BASE_URL+"admin/cb_machine_master/list_of_agent_device_data",
            "type": "POST",
            "data": "json",
			 data : function(d) {
				d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;
                d.agent_id=$('#agent_id').val();
			}
        },       
        "iDisplayLength": 10,
        "info" : false,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        "paginate": true,
        "paging": false,
        "searching": true,
         "oLanguage": {
                "sProcessing": '&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url().BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>' 
            },
        // "aoColumnDefs": [
        //    {"bVisible": false, "aTargets": [3,4,5]},
        //     //{"bSearchable": false, "aTargets": [0,3,4,5]},
        //     //{"targets":[1,2], "orderable": false}
        // ],
        "order": [[0, "desc"]],
        "fnRowCallback": function(nRow, aData, iDisplayIndex) {
         
           /* if (aData[3] == 'Y')
            {

                $("td:eq(3)", nRow).html("Active");
            }
            else
            {

                $("td:eq(3)", nRow).html("Deactive");
            }
*/
 
            /*var active_btn = 'Unassign Machine';
            if (aData[4] == 'N') 
            {
                active_btn = 'Assign Machine';
            }
            
            var btn = '';  
            if(!aData[5] || aData[6]=='Y') {                              
                // btn += '<a title="Unassign" class="unassign_btn" ref="' + aData[0] + '" data-ref="'+ aData[3] +'" data-status="'+ aData[4]+'"><i class="' + active_btn + '"></i></a>';
                btn +='<a class="btn btn-primary btn-sm btn-small"  ref="' + aData[0] + '" data-ref="'+ aData[3] +'" data-status="'+ aData[4]+'" type="button" style="color: #FFFFFF;width20%" id="unassign">'+active_btn+'</a>'; 

                /* btn += '<button title="Unassign" class="unassign_btn" ref="' + aData[0] + '" data-ref="'+ aData[3] +'" data-status="'+ aData[4]+'" class="">';*/
             /*} else {
                btn += '';                
            }

            $("td:last", nRow).html(btn);*/
            $("td:first", nRow).html(iDisplayIndex + 1);


            return nRow;
        }
        
    };
    var oTable = $('#example2').dataTable(tconfig);


    $(document).off('click','#unassign').on('click','#unassign', function(e) {
        e.preventDefault();
        var id = $(this).attr('ref');
        var machineMasterId = $(this).attr('data-ref');
        var status = $(this).attr('data-status');
     
        var detail = {};
        var msg;
        var is_status; 
     
        var form = '';
     
        if(status== "Y"){
             is_status = "N";  
             msg="unassign";          
           

        }
        else if(status=="N") {
            is_status = "Y";  
            msg="assign";   
            
        }
       
            var div="";
            var ajax_url = 'admin/cb_machine_master/machine_unassign';
            var form ="";
            detail['user_id'] = id;
            detail['machine_master_id'] = machineMasterId;
            detail['status'] = is_status;
                var yesno = confirm("Are you sure you want to "+ msg +" this machine");
            if(yesno) {
                get_data(ajax_url,form,div,detail,function(response) {
                   
                    if(response.flag == '@#success#@') {
                        location.reload();                        
                    }
                    else{
                        alert(response.msg);
                    }
                },'',false);
        }
    }); 
   

    
    $(document).off('click', '.delete_btn').on('click', '.delete_btn', function(e) {
            var flag = confirm('Please confirm to delete Machine?');

            if(flag)
            {
                return true;
            }

            return false;

    });
});
   setTimeout(function(){
        $('#msg_block').fadeIn('slow');
    },1000);
</script>
