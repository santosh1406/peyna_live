

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3>
            &nbsp;&nbsp;Edit Machine Master
             <!--<small>advanced tables</small>-->
        </h3>
       
    </section>
<?php //show($pro_data,1);?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                 <?php
                            $attributes = array("method" => "POST", "id" => "group_create_form", "class" => "group_create_form");
                            echo  form_open(site_url().'/admin/cb_machine_master/update/'.$group->id, $attributes);
                       ?>
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                       
                        <div class="form-group">

                            <label class="col-lg-3 control-label" for="name">IMEI No.</label>
                            <div class="col-lg-6">
                                
                                <input name="imei_no" type="hidden" id="imei_no" class="form-control" value="<?php echo $group->IMEI_NO?>">
                                <input name="imei_no" type="text" minlength="15" maxlength="15" id="imei_no" class="form-control" value="<?php echo $group->IMEI_NO ?>" >
                               
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                       

                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="op_addr_1">SIM NO(Optional) </label>
                            <div class="col-lg-6">
                                <input name="sim_no" type="hidden" id="sim_no" class="form-control" value="<?php echo $group->SIM_NO?>">

                                 <input name="sim_no" type="text" id="sim_no" class="form-control" value="<?php echo $group->SIM_NO ?>" minlength="20" maxlength="20" >
                                
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="op_addr_1">Mobile NO(Optional) </label>
                            <div class="col-lg-6">
                                <input name="mobile_no" type="hidden" id="mobile_no" class="form-control" value="<?php echo $group->mobile_no?>">
                            
                                 <input name="mobile_no" type="text" id="mobile_no" class="form-control" value="<?php echo $group->mobile_no ?>" minlength="10" maxlength="10" >
                                
                            </div>
                        </div>
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="op_addr_1">Status: </label>
                            <div class="col-lg-6">
                                <select class="form-control" id="status" name="status">
                                    <?php if($group->is_status == 'Y') {?>
                                  <option value="Y" selected="">Active</option>
                                  <option value="N">Deactivate</option>
                                 <?php  }?>
                               <?php if($group->is_status == 'N') {?>
                                  <option value="Y">Active</option>
                                  <option value="N" selected="">Deactivate</option>percentage</option>
                                 <?php  }?>
                                </select>  
                            </div>
                        </div>
                       
                       
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">

                                <button class="btn btn-primary" id="save_group_data" name="save_group_data" type="submit">Save</button> 
                                <button class="btn btn-primary back" id="back_data" type="button">Back</button> 
                            </div>
                        </div>

                         
                    </div>
                <!-- /.box-body -->
                </div></form><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->



<script>

    $(document).ready(function() {

    $(document).off('click', '.back').on('click', '.back', function(e)
    {

        window.location.href = BASE_URL + 'admin/cb_machine_master/';
    });


     $('#group_create_form').validate({ 
             rules: {
                imei_no: {
                    required: true,
                    number:true
                 },
                 sim_no: {
                    number:true,
                    minlength:20,
                    maxlength:20
                 },
                  mobile_no: {
                    number:true,
                    minlength:10,
                    maxlength:10
                 },
            },
            messages: {
                imei_no: {
                    required :"Please enter valid 15 digit unique IMEI no.",
                    number:"Please enter valid 15 digit unique IMEI no.",
                },
                sim_no: {
                    number: "SIM no should be number only.",
                    minlength: "Please enter 20 digit SIM No",
                    maxlength: "Please enter 20 digit SIM No",                  
                },
                mobile_no:  {
                    number:"Mobile no should be number only.",
                    minlength: "Please enter 10 digit Mobile No",
                    maxlength: "Please enter 10 digit Mobile No",    
                }
            },
            submitHandler: function () { 
                submit.form();
            }
    });
});
</script>
