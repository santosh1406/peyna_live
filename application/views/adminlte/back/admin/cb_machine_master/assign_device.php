<section class="content">
    <div class="row">
        <!-- right column -->
        <div class="col-md-12">
            <!-- general form elements disabled -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Assign Device</h3>
                </div><!-- /.box-header -->
                <?php if ($this->session->flashdata('msg')) { ?>
                    <div class="alert alert-success alert-dismissable">
                        <b>
                            <?php
                            $data = $this->session->flashdata('msg');
                            echo $data;
                            ?>
                        </b>
                        <i class="fa fa-check"></i>
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                    </div>
                <?php } ?>      
                <?php
                
                $attributes = array("method" => "POST", "id" => "assign_device_form", "class" => "assign_device_form");
                echo  form_open(site_url().'/admin/cb_machine_master/save_assign_machine_master', $attributes);
                ?>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="package name">Executive Name</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="agent_id" id="agent_id">
                             <option value="">Select Executive</option> 
                            <?php if(count($agent_details) > 0) { foreach ($agent_details as $key => $agent) {
                                echo "<option value='" . $agent->id . "'> " . $agent->first_name." ". $agent->last_name. " </option>";
                               } }
                               ?>
                            </select>
                        </div>
                    </div>
<div class="clearfix" style="height: 40px;clear: both;"></div>
                 <div class="form-group">
                        <label class="col-lg-3 control-label" for="package name">Select IMEI NO.</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="imei_no" id="imei_no" >
                             <option value="">Select IMEI NO.</option> 
                            <?php if(count($imei_details) > 0) { foreach ($imei_details as $key => $imei) {
                                //show($imei['id'],1);
                                echo "<option value='" . $imei['id'] . "'> " . $imei['IMEI_NO']." </option>";
                               } }
                               ?>
                            </select>
                        </div>
                    </div>
                

                    <div class="form-group pkg_exist col-lg-12" >
                       
                        
                        
                        
                    </div>

                    <div class="clearfix" style="height: 40px;clear: both;"></div>
                   <div class="show_serices" >
                        <div class="form-group">
                            

                          <div class="clearfix" style="height: 10px;clear: both;"></div>
                         

                          <div class="form-group">
                              <div class="col-lg-offset">
                                  <center> <button class="btn btn-primary" id="save_package_data" name="save_package_data" type="submit">Save</button> </center>
                                   <div class="clearfix" style="height: 10px;clear: both;"></div>
                             
                                
                              </div>
                          </div>
                           <div class="clearfix" style="height: 10px;clear: both;"></div>
                      </div>
                  </div>
            </div>
        </div> <!-- /.box -->
    </div> <!--/.col (right) -->
    <?php echo form_close(); ?>
</section>

<script type="text/javascript">
    
$(document).ready(function ()
{   
      //list commission ajax starts here.
    $('.pkg_exist').hide();
    $('.alert-dismissable').delay(10000).fadeOut("slow");
    $(function() {
       
  });

    $('#assign_device_form').validate({ 
             rules: {
                agent_id: {
                    required: true,
                 },
                 imei_no: {
                    required:true,
                }
            },
            messages: {
                agent_id: "Please select agent.",
                
                
                imei_no: "Please select IMEI no."
                
               
            },
            submitHandler: function () { 
                submit.form();
            }
        });      


    
});

</script>   
