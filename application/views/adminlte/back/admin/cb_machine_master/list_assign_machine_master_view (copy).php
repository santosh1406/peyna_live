
<section class="content-header">
    <h1> Assign Machines
    </h1>
</section>
                     
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <div align="left" style="margin: 20px;">
                   <a class="btn btn-primary"  href="<?php echo site_url() . '/admin/cb_machine_master/list_agent_Device'?>" type="button" style="color: #FFFFFF;">Back</a> 
                    <h4 align="right"><b><?php echo ucfirst($agent_name); ?></b></h4>
                </div>

                    <input type="hidden"   name='agent_id' id='agent_id' value="<?php echo  $agent_id; ?>"
                </div>
                </div><!-- /.box-header -->
                <div class="box-body">
            <div class="table-responsive">
               <table id="example2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th> 
                                    <th>IMEI NO.</th>
                                     <th>SIM NO.</th> 
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1;
                               
                                ?>


                            </tbody>

               </table></div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
    <!-- /.content-wrapper -->


<script>
  
 $(document).ready(function() { 
     var tconfig = {
        // "sDom": '<"toolbar">hlfetlp',
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": BASE_URL+"admin/cb_machine_master/list_of_agent_device_data",
            "type": "POST",
            "data": "json",
			 data : function(d) {
				 d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;
                 d.agent_id=$('#agent_id').val();
			}
        },
        "columnDefs": [
        {
            "searchable": false,
            "orderable": false
        }
        ],
        "iDisplayLength": 10,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        "paginate": true,
        "paging": true,
        "searching": true,
         "oLanguage": {
                "sProcessing": '&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url().BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>' 
            },
        "aoColumnDefs": [
            {"bSearchable": false, "aTargets": [0]},
            {"targets":[1,2], "orderable": false}
        ],
        "order": [[0, "desc"]],
        "fnRowCallback": function(nRow, aData, iDisplayIndex) {
         
           /* if (aData[3] == 'Y')
            {

                $("td:eq(3)", nRow).html("Active");
            }
            else
            {

                $("td:eq(3)", nRow).html("Deactive");
            }
*/

            $("td:first", nRow).html(iDisplayIndex + 1);
            return nRow;
        }
        
    };
    var oTable = $('#example2').dataTable(tconfig);
    
    $(document).off('click', '.delete_btn').on('click', '.delete_btn', function(e) {
            var flag = confirm('Please confirm to delete Machine?');

            if(flag)
            {
                return true;
            }

            return false;

    });
});
   setTimeout(function(){
        $('#msg_block').fadeIn('slow');
    },1000);
</script>
