
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h3>
            &nbsp;Create New Machine master
             <!--<small>advanced tables</small>-->
        </h3s>
       
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body">
                       <?php
                            $attributes = array("method" => "POST", "id" => "group_create_form", "class" => "group_create_form");
                            echo  form_open(site_url().'/admin/cb_machine_master/save_machine_master', $attributes);
                       ?>
                        <div class="form-group">

                            <label class="col-lg-3 control-label" for="name">IMEI No.</label>
                            <div class="col-lg-6">
                                <input name="imei_no" type="hidden" id="imei_no" class="form-control" value="">
                                <input name="imei_no" type="text" minlength="15" maxlength="15" id="imei_no" class="form-control" value="<?php  echo set_value('imei_no'); ?>">
                               
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>

                        <!-- //BG 20258 - 3.1   -->
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="name">IMEI No2.</label>
                            <div class="col-lg-6">
                                <input name="imei_no_2" type="hidden" id="imei_no_2" class="form-control" value="">
                                <input name="imei_no_2" type="text" minlength="15" maxlength="15" id="imei_no_2" class="form-control" value="<?php  echo set_value('imei_no_2'); ?>">                               
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                       
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="op_addr_1">SIM NO(Optional)</label>
                            <div class="col-lg-6"> 
                                <input name="sim_no" type="hidden" id="sim_no" class="form-control" value="">                             
                                <input name="sim_no" type="text" id="sim_no" class="form-control" value="<?php  echo set_value('sim_no'); ?>" minlength="20" maxlength="20">
                            </div>
                        </div>

                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                       <div class="form-group">
                            <label class="col-lg-3 control-label" for="op_addr_1">Mobile NO(Optional)</label>
                            <div class="col-lg-6">
                               <input name="mobile_no" type="hidden" id="mobile_no" class="form-control"  value="">
                                <input name="mobile_no" type="text" id="mobile_no" class="form-control" minlength="10" maxlength="10" value="<?php  echo set_value('mobile_no'); ?>" >
                            </div>
                        </div>

                                             
                    </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        
                         <?php echo validation_errors('<div class="error">', '</div>'); ?>
                     
                        <div class="form-group">
                            <div class="col-lg-offset-4">

                                <button class="btn btn-primary" id="save_group_data" name="save_group_data" type="submit">Save</button> 
                                <button class="btn btn-primary back" id="back_data" type="button">Back</button> 
                            </div>
                        </div>

                         </form>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<script>

    $(document).ready(function() {

    $(document).off('click', '.back').on('click', '.back', function(e)
    {

        window.location.href = BASE_URL + 'admin/cb_machine_master/';
    });

    $('#group_create_form').validate({ 
             rules: {
                imei_no: {
                    required: true,
                    number:true,
                    check_numbers:true
                 },
                 imei_no_2: {                    
                    number:true,
                    check_numbers:true
                 },
                 sim_no: {
                    number:true,
                    minlength:20,
                    maxlength:20
                 },
                  mobile_no: {
                    number:true,
                    minlength:10,
                    maxlength:10,
                    valid_mobile_no: true,
                    mobileno_start_with: true
                 },
            },
            messages: {
                imei_no: {
                    required :"Please enter valid 15 digit unique IMEI no.",
                    number:"Please enter valid 15 digit unique IMEI no.",
                },
                imei_no_2: {                    
                    number:"Please enter valid 15 digit unique IMEI no.",
                },
                sim_no: {
                    number: "SIM no should be number only.",
                    minlength: "Please enter 20 digit SIM No",
                    maxlength: "Please enter 20 digit SIM No",                  
                },
                mobile_no:  {
                    number:"Mobile no should be number only.",
                    minlength: "Please enter 10 digit Mobile No",
                    maxlength: "Please enter 10 digit Mobile No", 
                    valid_mobile_no: "Please enter valid mobile number",
                    mobileno_start_with: 'Mobile No should start with 7 or 8 or 9'
                }
            },
            submitHandler: function () { 
                submit.form();
            }
        }); 
        $.validator.addMethod("check_numbers", function (value, element) {

                    var flag=false;
                    var TempAmount = parseInt(value);
                    if(TempAmount <=0){
                        flag = false;	
                    }else{
                        flag = true;	
                    }
                    return flag;
                }, 'Number Should Be Greater Than 0.');
        
        $.validator.addMethod("valid_mobile_no", function (value, element) {
            return (value == "0000000000" || value == "9999999999") ? false : true;
        }, 'Please enter valid mobile number');
        
        $.validator.addMethod("mobileno_start_with", function (value, element) {
            var regex = new RegExp("^[7-9]{1}[0-9]{9}$");
            if (value.length > 0)
            {
                if (regex.test(value))
                {
                    return true;
                } else {
                    return false;
                }
            } else
            {
                return true;
            }
        }, 'Mobile No. should start with 7 or 8 or 9 ');


    });
</script>
