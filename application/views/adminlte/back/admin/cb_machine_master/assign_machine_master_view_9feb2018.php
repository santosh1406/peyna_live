
<section class="content-header">
    <h1> Assign Machines
    </h1>
</section>
                       <!--  <?php
                    if($this->session->flashdata('msg')){ 
                ?>
                <div id="msg_block" class="col-md-12 error_block <?php if(!empty($error)){ echo 'show';} else{ echo "show";} ?>">
                    <div class="alert alert-success alert-dismissable" style ="margin-top:15px">
                        <i class="glyphicon glyphicon-ok"></i>
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?php echo '<b>Success!</b> ' . $this->session->flashdata('msg');  ?>                       
                    </div>
                </div>
                <?php } ?> -->
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <div align="right"><a href="<?php echo site_url() . '/admin/cb_machine_master/assignDevice' ?>" class="btn btn-warning" role="button" style="margin-top: 5px;">Assign New Machines</a>   
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
            <div class="table-responsive">
               <table id="example2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th> 
                                    <th>Agent Name</th>
                                     <th>Total Devices</th> 
                                     <th>Action</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1;
                               
                                ?>


                            </tbody>

               </table></div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
    <!-- /.content-wrapper -->


<script>
  
 $(document).ready(function() {
     var tconfig = {
        // "sDom": '<"toolbar">hlfetlp',
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": BASE_URL+"admin/cb_machine_master/list_of_agent_device",
            "type": "POST",
            "data": "json",
			 data : function(d) {
				 d.<?php echo $this->security->get_csrf_token_name(); ?> = rokad_token;
                 d.agent_id = $('#agent_id').val();
			}
        },
        "columnDefs": [
        {
            "searchable": false,
            "orderable": false
        }
        ],
        "iDisplayLength": 10,
        "aLengthMenu": [[5, 10, 50, -1], [5, 10, 50, "All"]],
        "paginate": true,
        "paging": true,
        "searching": true,
         "oLanguage": {
                "sProcessing": '&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url().BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>' 
            },
        "aoColumnDefs": [
            {"bSearchable": false, "aTargets": [0]},
            {"targets":[1,2,3], "orderable": false}
        ],
        "order": [[0, "desc"]],
        "fnRowCallback": function(nRow, aData, iDisplayIndex) {
         
           /* if (aData[3] == 'Y')
            {

                $("td:eq(3)", nRow).html("Active");
            }
            else
            {

                $("td:eq(3)", nRow).html("Deactive");
            }
*/

            $("td:first", nRow).html(iDisplayIndex + 1);
            return nRow;
        }
        
    };
    var oTable = $('#example2').dataTable(tconfig);
    
    $(document).off('click', '.delete_btn').on('click', '.delete_btn', function(e) {
            var flag = confirm('Please confirm to delete Machine?');

            if(flag)
            {
                return true;
            }

            return false;

    });
});
   setTimeout(function(){
        $('#msg_block').fadeIn('slow');
    },1000);
</script>
