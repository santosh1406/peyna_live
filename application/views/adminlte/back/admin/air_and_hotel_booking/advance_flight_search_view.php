<link href='<?php echo base_url() . BACK_ASSETS ?>css/search_style/style.css' rel="stylesheet" type="text/css" />
<style>
    .loader {
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #933b89;
        height: 120px;
        width: 120px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
        position: absolute;
        float: right;
        position: fixed;
        z-index: 9999999;
        left: 53%;
        top: 50%;
        /*background-color: #000;*/
        opacity: 0.7;
    }

    #loader{
        background: #000;
        opacity: 0.7;
        position: fixed;
        width: 100%;
        height: 120%;
        display: none;
        z-index: 999999;
        top: -10%;
    }


    /* Safari */
    @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
    100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
    }
    .ladies_selected{
        background-position: -20px -20px;
    }
    input[type=range]::-moz-range-thumb {
        background-color: #933b89;
    }


    .sorter-title-h{padding:4px; float:left;}
    .sorters{margin-bottom: 20px; border: 1px solid transparent; border-color: #ddd; border-radius: 5px; padding:10px; box-shadow:0 1px 1px rgba(0,0,0,.05); background:#f9f9f9;}
    .results{margin-bottom: 20px; background-color: #fff; border: 1px solid transparent; border-color: #ddd; padding:10px;}
    .time, .dur{font-size:22px; color:#333333;}
    .name{font-size:18px; color:#333333;}
    .city, .stops, .route, .fltNum, .terminal, .chkin{font-size:12px; font-weight:300; color:#666666;}
    .goairlogo{float:left; margin-right:5px; margin-top:4px;}
    .fltdet{font-size:12px; font-weight:600; padding:9px 0;}
    .fltdet .det{padding:5px; float:left;}
    .fltprice{font-size:22px; font-weight:600; padding:7px 0;}
    .flightinfo{height:auto;}
    .flightdetail-tab{display:none; clear:both; position:relative; top:10px;margin-bottom:20px;}
    /* Flight Details Tab Start */
    .tabs {max-width: 640px; margin: 0 auto; padding: 0 20px;}
    #fltdettab-button {display: table; table-layout: fixed; width: 100%; margin: 0; padding: 0; list-style: none;}
    #fltdettab-button li {display: table-cell; width: 20%;}
    #fltdettab-button li a {display: block; padding: .5em; background: #eee; border: 1px solid #ddd; text-align: center; color: #933b89; text-decoration: none;}
    #fltdettab-button li:not(:first-child) a {border-left: none;}
    #fltdettab-button li a:hover, #fltdettab-button .is-active a {border-bottom-color: transparent; background: #933b89; border-bottom: 1px solid #933b89; color:#fff;}
    .fltdettab-contents {padding: .5em 2em 1em; border: 1px solid #ddd;}
    .fltdettab-button-outer {display: none;0}
    .fltdettab-contents {margin-top: 20px; display:inline-grid;}
    @media screen and (min-width: 768px) {
        .fltdettab-button-outer {position: relative; z-index: 2; display: block;}
        .fltdettab-select-outer {display: none;}
        .fltdettab-contents {position: relative; top: -1px; margin-top: 0; display:grid;}
    }
    /* Flight Details Tab End */
    .font12{font-size:12px;}
    .font30{font-size:30px;}
    .icon-plane{position:relative; top:4px;}
    .baginfo{padding:10px 0; border-bottom:1px solid #CCC; margin-bottom:10px;}
    .feebox{border:1px solid #e5e5e5; padding:5px; margin:10px 0; font-weight:600; font-size:12px; font-weight:bold;}
    .totalamtb{border-bottom:1px solid #ccc;}
    .box-grey{background:#f2f2f2; padding:10px; display:}
    .border-middle{border-bottom: 1px solid #999; padding:10px 0; float:left;}
    .traveller-details{line-height:50px;}
    .traveller-contact{line-height:50px;}
</style>
<!-- Inner Wrapper Start -->
<div id="page-wrapper">

    <!-- Signin Content Start -->
    <section class="inner-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h3 class="text-center purple">Search Result</h3>
                    <div class="searchp-form well mb10" style="display:none;">
                        <form class="form-inline mb20">
                            <div class="form-group">
                                <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                    <input class="form-control mb10" id="" type="text" placeholder="From">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                    <input class="form-control mb10" id="" type="text" placeholder="To">
                                </div>
                            </div>
                            <div class="form-group has-feedback">
                                <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                    <input type="text" class="form-control mb10" id="" placeholder="Departure Date">
                                    <span class="glyphicon glyphicon-calendar form-input-icon icon-calendar"></span>
                                </div>
                            </div>
                            <div class="form-group has-feedback">
                                <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                    <input type="text" class="form-control mb10" id="" placeholder="Return Date (opt)">
                                    <span class="glyphicon glyphicon-calendar form-input-icon icon-calendar"></span>
                                </div>
                            </div>
                            <div class="form-group has-feedback">
                                <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                    <select id="disabledSelect" class="form-control mb10" placeholder="">
                                        <option selected="selected" value="Adults 12+ Yrs">Adults 12+ Yrs</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>6</option>
                                        <option>7</option>
                                        <option>8</option>
                                        <option>9</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group has-feedback">
                                <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                    <select id="disabledSelect" class="form-control mb10" placeholder="">
                                        <option selected="selected" value="Adults 12+ Yrs">Children 2-12 Yrs</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>6</option>
                                        <option>7</option>
                                        <option>8</option>
                                        <option>9</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group has-feedback">
                                <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                    <select id="disabledSelect" class="form-control mb10" placeholder="">
                                        <option selected="selected" value="Infants 0-2 Yrs">Infants 0-2 Yrs</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>6</option>
                                        <option>7</option>
                                        <option>8</option>
                                        <option>9</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                    <select id="disabledSelect" class="form-control mb10" placeholder="">
                                        <option selected="selected" value="Preferred Airlines">Preferred Airlines</option>
                                        <option>All</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                    <select id="disabledSelect" class="form-control mb10" placeholder="">
                                        <option selected="selected" value="Class">Class</option>
                                        <option>All</option>
                                        <option>Economy</option>
                                        <option>Business</option>
                                        <option>Premium Economy</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                    <select id="disabledSelect" class="form-control mb10" placeholder="">
                                        <option selected="selected" value="Routing">Routing</option>
                                        <option>All</option>
                                        <option>Direct Flights</option>
                                        <option>Nonstop Direct Flights</option>
                                        <option>Single Connecting</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-1 col-md-3 col-sm-4 col-xs-12">
                                    <button type="submit" id="submit_filter_btn" class="btn btn-purple txt-w mb10">Modify Search</button>
                                </div>
                            </div>
                        </form>
                        <div class="next-re-search mb10">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-inline clearfix search-bar">
                                    <div class="form-group next-pre col-sm-12 col-md-12 text-center">
                                        <div class="from-to-search">
                                            <span class="mr10">Mumbai</span>
                                            <span class="fa fa-arrow-right"></span>
                                            <span class="ml10">Goa</span>
                                        </div>
                                        <div class="next_previous">
                                            <span>Previous</span>
                                            <span class="fa fa-chevron-left mr5 cursor-p"></span>
                                            <span>20-Nov-2018</span>
                                            <span class="fa fa-chevron-right ml5 cursor-p"></span>
                                            <span>Next</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Modify Search Ends-->

            <div class="row">
                <div class="col-sm-4 col-md-2" style="display:none;">
                    <h4 class="search-results-title">
                        <i class="glyphicon glyphicon-search mr10 icon-search"></i>
                        <b><span>255</span></b> results found.
                    </h4>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <i class="short-full glyphicon glyphicon-plus"></i>
                                        Fare
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="slider slider-horizontal" id="ex1Slider"><div class="slider-track"><div class="slider-track-low" style="left: 0px; width: 0%;"></div><div class="slider-selection" style="left: 0%; width: 56%;"></div><div class="slider-track-high" style="right: 0px; width: 44%;"></div></div><div class="tooltip tooltip-main top" role="presentation" style="left: 56%;"><div class="tooltip-arrow"></div><div class="tooltip-inner">Current value: 9</div></div><div class="tooltip tooltip-min top" role="presentation" style="display: none;"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div><div class="tooltip tooltip-max top" role="presentation" style="display: none;"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div><div class="slider-handle min-slider-handle round" role="slider" aria-valuemin="-5" aria-valuemax="20" style="left: 56%;" aria-valuenow="9" aria-valuetext="Current value: 9" tabindex="0"></div><div class="slider-handle max-slider-handle round hide" role="slider" aria-valuemin="-5" aria-valuemax="20" style="left: 0%;" aria-valuenow="-5" aria-valuetext="Current value: -5" tabindex="0"></div></div>
                                    <input id="ex1" data-slider-id="ex1Slider" type="text" data-slider-min="-5" data-slider-max="20" data-slider-step="1" data-slider-value="14" style="display: none;" data-value="11" value="11">
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                        <i class="short-full glyphicon glyphicon-plus"></i>
                                        Flight Times
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <form action="" method="get">
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="MSRTC" type="checkbox" value=""> <a href="#">00:00 - 03:59(Mid Night)</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="Neeta" type="checkbox" value=""> <a href="#">04:00 - 11:59 (Morning)</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="Neeta" type="checkbox" value=""> <a href="#">12:00 - 15:59 (Afternoon)</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="Neeta" type="checkbox" value=""> <a href="#">16:00 - 18:59 (Evening)</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="Neeta" type="checkbox" value=""> <a href="#">19:00 - 23:59 (Night)</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <i class="short-full glyphicon glyphicon-plus"></i>
                                        Fight Stops
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <form action="" method="get">
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="1 Stop" type="checkbox" value=""> <a href="#">1 Stop</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="2 Stops" type="checkbox" value=""> <a href="#">2 Stops</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="3 Stops" type="checkbox" value=""> <a href="#">3 Stops</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="Multistops" type="checkbox" value=""> <a href="#">Multistops</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        <i class="short-full glyphicon glyphicon-plus"></i>
                                        Airlines
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                <div class="panel-body">
                                    <form action="" method="get">
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="Select All" type="checkbox" value=""> <a href="#">Select All</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="Spicejet" type="checkbox" value=""> <a href="#">Spicejet</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="AirIndia" type="checkbox" value=""> <a href="#">AirIndia</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="Vistara" type="checkbox" value=""> <a href="#">Vistara</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="JetAirways" type="checkbox" value=""> <a href="#">JetAirways</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="GoAir" type="checkbox" value=""> <a href="#">GoAir</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFive">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                        <i class="short-full glyphicon glyphicon-plus"></i>
                                        Flight Type
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                <div class="panel-body">
                                    <form action="" method="get">
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="MSRTC" type="checkbox" value=""> <a href="#">00:00 - 03:59(Mid Night)</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="Neeta" type="checkbox" value=""> <a href="#">04:00 - 11:59 (Morning)</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="Neeta" type="checkbox" value=""> <a href="#">12:00 - 15:59 (Afternoon)</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="Neeta" type="checkbox" value=""> <a href="#">16:00 - 18:59 (Evening)</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="Neeta" type="checkbox" value=""> <a href="#">19:00 - 23:59 (Night)</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingSix">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                        <i class="short-full glyphicon glyphicon-plus"></i>
                                        Duration
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                <div class="panel-body">
                                    <form action="" method="get">
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="MSRTC" type="checkbox" value=""> <a href="#">Upto 6 Hours</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="Neeta" type="checkbox" value=""> <a href="#">6 Hours - 12 Hours</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="Neeta" type="checkbox" value=""> <a href="#">12 Hours - 24 Hours</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="Neeta" type="checkbox" value=""> <a href="#">More than a day</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingSeven">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                        <i class="short-full glyphicon glyphicon-plus"></i>
                                        Itineraries
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                <div class="panel-body">
                                    <form action="" method="get">
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="itineraries" type="checkbox" value=""> <a href="#">All Itineraries</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="baggage" type="checkbox" value=""> <a href="#">Baggage Included</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb5">
                                            <div class="collapse-cnt">
                                                <input name="meals" type="checkbox" value=""> <a href="#">Meals Available</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div><!-- panel-group -->
                </div>

                <div class="col-sm-8 col-md-10">
                    <div class="sort-by-section clearfix mb10" style="display:none;">
                        <h4 class="sort-by-title block-sm">Sort results by:</h4>
                        <ul class="sort-bar clearfix">
                            <li class="sort-by-name"><a class="sort-by-container" href="#"><span>Name</span></a></li>
                            <li class="sort-by-price"><a class="sort-by-container" href="#"><span>Price</span></a></li>
                            <li class="sort-by-duration"><a class="sort-by-container" href="#"><span>Duration</span></a></li>
                            <li class="sort-by-fare"><a class="sort-by-container" href="#"><span>Fare</span></a></li>
                        </ul>
                    </div>

                    <!-- flight-service-list start -->
                    <div class="flight-service-list">
                        <div class="panel-body" role="tab">
                            <div class="one-way dataTables_wrapper">
                                <div class="row sorters">
                                    <div class="col-md-1 col-sm-4 col-xs-12"><i class="soap-icon-adventure purple soap-icon"></i> <div class="sorter-title-h">Departure</div></div>
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12"><i class="soap-icon-clock purple soap-icon"></i> <div class="sorter-title-h">Duration</div></div>
                                    <div class="col-md-1 col-sm-4 col-xs-12"><i class="soap-icon-departure purple soap-icon"></i> <div class="sorter-title-h">Arrival</div></div>
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12"><i class="soap-icon-plane purple soap-icon"></i> <div class="sorter-title-h">Airlines</div></div>
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12"><i class="soap-icon-status purple soap-icon"></i> <div class="sorter-title-h">Details</div></div>
                                    <div class="col-lg-2 col-md-1 col-sm-4 col-xs-12"><div class="sorter-title-h">Price</div></div>
                                    <div class="col-lg-1 col-md-1 col-sm-4 col-xs-12"><div class="sorter-title-h"></div></div>
                                </div>
                                <?php
                                //print_r($onwardJourneysDetails);die;
                                $incr = 0;
                                if (!empty($onwardJourneysDetails)) {
                                    foreach ($onwardJourneysDetails as $key => $value) {
                                        // print_r($value);die;
                                        foreach ($value['flights'] as $index => $flightInfo) {


                                            $flyTime = $value['journeyInfo'][0]['journeyTime'];
                                            $hours = floor($flyTime / 60);
                                            $min = $flyTime - ($hours * 60);
                                            $duration = $hours . ":" . $min;
                                            $index = count($value['flights']);
                                            $index = $index - 1;
                                            $arrival_city = $value['flights'][$index]['arrDetail']['city'];
                                            $via_flight = array();
                                            //if()
                                            for ($i = 0; $i <= $index; $i++) {
                                                $via_flight[] = $value['flights'][$i]['depDetail']['code'];
                                                $via_flight[] = $value['flights'][$i]['arrDetail']['code'];
                                            }
                                            $via_flight = array_unique($via_flight);
                                            $via_flight = implode('#', $via_flight);
                                            $via_flight = explode('#', $via_flight);
                                            ?>   
                                            <div class="row results">
                                                <div class="flightinfo">
                                                    <div class="col-lg-1 col-md-1 col-sm-4 col-xs-12">
                                                        <div class="depTime">
                                                            <div class="time"><?php echo date("H:i", strtotime($value['flights'][0]['depDetail']['time'])) ?></span></div> 
                                                            <div class="city"><?php echo $value['flights'][0]['depDetail']['city'] ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                                        <div class="duration">
                                                            <div class="dur"><?php echo $duration ?> <span class="stops"><?php echo (count($via_flight) - 2) ?> Stop(s)</span></div> 
                                                            <div class="route">

            <!--                                                                BOM <i class="soap-icon-longarrow-right"></i> DEL <i class="soap-icon-longarrow-right"></i> GOI-->
            <?php
            //print_r($value['flights']);exit;
            foreach ($via_flight as $index => $val) {
                ?>
                                                                    <?php
                                                                    echo $val;
                                                                    if (count($via_flight) != ($index + 1)) {
                                                                        ?> 
                                                                        <i class="soap-icon-longarrow-right"></i>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-4 col-xs-12">
                                                        <div class="arrTime">
                                                            <div class="time"><?php echo date("H:i", strtotime($flightInfo['arrDetail']['time'])) ?></span></div> 
                                                            <div class="city"><?php echo $arrival_city; ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                                        <div class="airline">
                                                            <!--  <div class="goairlogo" style="padding-bottom:10px;">
                                                               <img src="images/go-air-logo.png"/>
                                                           </div>  -->
                                                            <div class="airdet">
                                                                <div class="name"><?php echo $flightInfo['carrier']['name'] ?></div>
                                                                <div class="fltNum">
            <?php echo str_replace('#', '', $value['fareKey']); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                                                        <div class="flttdettab">
                                                            <div class="fltdet">
                                                                <a href="javascript:void();" onclick="toggle_visibility('flightdetail-tab_<?php echo $incr; ?>');getfare('<?php echo $value['key'] ?>','<?php echo $incr; ?>');"><i class="soap-icon-plane purple soap-icon"></i> <span class="det purple"> Flight Details</span></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2 col-md-1 col-sm-4 col-xs-12">
                                                        <div class="fltpricediv">
                                                            <div class="fltprice txt-p"><i class="fa fa-rupee"></i> <?php echo $value['fares']['totalFare']['total']['amount'] ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-1 col-md-1 col-sm-4 col-xs-12">
                                                        <div class="bookbtn">
                                                            <button onclick="book('<?php echo $value['key'] ?>');" type="submit" id="submit_filter_btn" class="btn btn-purple txt-w mt10">Book</button>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div  class="flightdetail-tab" id="flightdetail-tab_<?php echo $incr; ?>">
                                                    <div class="fltdettabs">
                                                        <div class="fltdettab-button-outer">
                                                            <ul id="fltdettab-button">
                                                                <li><a href="#tab01" ><i class="soap-icon-plane"></i> Flight Details</a></li>
                                                                <li><a href="#tab02"><i class="soap-icon-suitcase"></i> Baggage Information</a></li>
                                                                <li><a href="#tab03"><i class="soap-icon-notice"></i> Fare Rules</a></li>
                                                                <li><a href="#tab04"><i class="fa fa-rupee"></i> Fare Details</a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="fltdettab-select-outer">
                                                            <select id="fltdettab-select">
                                                                <option value="#tab01">Flight Details</option>
                                                                <option value="#tab02">Baggage Information</option>
                                                                <option value="#tab03">Fare Rules</option>
                                                                <option value="#tab04">Fare Details</option>
                                                            </select>
                                                        </div>

                                                        <div id="tab01" class="fltdettab-contents">
                                                            <div>
                                                                <i class="soap-icon-plane-right font30 purple icon-plane"></i> <strong><?php echo $value['flights'][0]['depDetail']['city'] ?> to <?php echo $arrival_city; ?></strong> <?php echo date("d M Y", strtotime($flightInfo['depDetail']['time'])) ?> | <?php echo $duration ?> <span class="stops">0 Stops</span>
                                                            </div>
            <?php
            foreach ($value['flights'] as $flight_details) {
                $flyTime = $flight_details['flyTime'];
                $hours = floor($flyTime / 60);
                $min = $flyTime - ($hours * 60);
                $individual_duration = $hours . ":" . $min;
                ?>
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
                                                                        <span class="name"><?php echo $flight_details['depDetail']['code']; ?></span> <span class="city"><?php echo $flight_details['city']; ?></span> <br/>
                                                                        <span class="route"><?php echo $flight_details['depDetail']['country']; ?></span> <br/>
                                                                        <span class="terminal"><strong>Terminal</strong> : <?php echo $flight_details['depDetail']['terminal']; ?></span> <br/>
                                                                        <span class="time"><?php echo date("H:i", strtotime($flight_details['depDetail']['time'])) ?></span>
                                                                    </div>
                                                                    <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
                                                                        <!--  <div class="goairlogo" style="padding-bottom:10px;">
                                                                        <img src="images/go-air-logo.png"/>
                                                                    </div>  -->
                                                                        <div class="airdet">
                                                                            <div class="name"><?php echo $flight_details['carrier']['name']; ?></div>
                                                                            <div class="fltNum"><?php echo $flight_details['carrier']['code']; ?>-<?php echo $flight_details['flightNo']; ?></div>
                                                                            <div class="txt-p"><?php echo $individual_duration; ?></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
                                                                        <span class="name"><?php echo $flight_details['arrDetail']['code']; ?></span> <span class="city"><?php echo $flight_details['arrDetail']['city']; ?></span> <br/>
                                                                        <span class="route"><?php echo $flight_details['arrDetail']['country']; ?></span> <br/>
                                                                        <span class="time"><?php echo date("H:i", strtotime($flight_details['arrDetail']['time'])) ?></span>
                                                                    </div>
                                                                </div>
                                                                <hr>
            <?php } ?>  
                                                        </div>




                                                        <!--  test   -->
                                                        <div id="tab02" class="fltdettab-contents">
                                                            <div>
                                                                <i class="soap-icon-plane-right font30 purple icon-plane"></i> <strong><?php echo $value['flights'][0]['depDetail']['city'] ?> to <?php echo $arrival_city; ?></strong> | Baggage Information
                                                            </div>
                                                            <div class="baginfo">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">Airline</div>
                                                                    <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">Check In</div>
                                                                    <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">Cabin Baggage</div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
                                                                    <!--  <div class="goairlogo" style="padding-bottom:10px;">
                                                                    <img src="images/go-air-logo.png"/>
                                                                </div>  -->
                                                                    <div class="airdet">
                                                                        <div class="name"><?php echo $flight_details['carrier']['code']; ?></div>
                                                                        <div class="fltNum"><?php echo $flight_details['carrier']['code']; ?>-<?php echo $flight_details['flightNo']; ?></div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
                                                                    <div>Adult: <strong><?php echo $flightInfo['amenities']['baggage']['checkin']['adt']['qty']; ?> <?php echo $flightInfo['amenities']['baggage']['cabin']['adt']['unit']; ?> baggage</strong></div>
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
                                                                    <div>Adult: <strong><?php echo $flightInfo['amenities']['baggage']['cabin']['adt']['qty']; ?> <?php echo $flightInfo['amenities']['baggage']['cabin']['adt']['unit']; ?></strong></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="tab03" class="fltdettab-contents">
                                                            <div class="boxstyle1">
                                                                            <?php echo $flight_details['depDetail']['code']; ?> <i class="soap-icon-plane-right font30 purple icon-plane"></i> <?php echo $flight_details['arrDetail']['code']; ?>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="col-lg-9">
                                                                        <p><i class="fa fa-exclamation" aria-hidden="true"></i> Fare Rules: <?php echo $flight_details['depDetail']['code']; ?> <i class="soap-icon-plane-right font30 icon-plane"></i> <?php echo $flight_details['arrDetail']['code']; ?> </p>
                                                                        <p><small>* The below Fare Rules are just a guideline for your convenience and is subject to changes by the Airline from time to time. </small></p>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <button type="submit" id="submit_filter_btn" class="btn btn-purple txt-w mt10">Refundable</button>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        Cancellation Fee <small>(Per Passenger)</small>
                                                                        <div class="feebox">Cancellation Fee Amount <i class="fa fa-rupee"></i> <span id="cancellation_<?php echo $incr; ?>"></span></div>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        Date Change Fee <small>(Per Passenger)</small>
                                                                        <div class="feebox">Date Change Amount <i class="fa fa-rupee"></i> <span id="date_change_<?php echo $incr; ?>"></span></div>
                                                                    </div>
                                                                    <div class="col-lg-12 feebox">Other Terms</div>
                                                                    <div class="col-lg-12"><span id="termcondition_<?php echo $incr; ?>"></span></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="tab04" class="fltdettab-contents">
                                                            <div>
                                                                <h4>Fare Details</h4>
                                                            </div>
                                                            <div class="baginfo">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                    <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">Type</div>
                                                                    <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">Fare (<i class="fa fa-rupee txt-p"></i>)</div>
                                                                    <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">Total (<i class="fa fa-rupee txt-p"></i>)</div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                                                <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
                                                                    Adult (s)
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
            <?php echo ($value['fares']['paxFares']['adt']['tax']['amount'] + $value['fares']['paxFares']['adt']['base']['amount'] ); ?>x   1 </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
            <?php echo $value['fares']['paxFares']['adt']['total']['amount']; ?>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                                                <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
                                                                    Child
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
            <?php echo ($value['fares']['paxFares']['chd']['tax']['amount'] + $value['fares']['paxFares']['chd']['base']['amount'] ); ?>x   1 
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
            <?php echo $value['fares']['paxFares']['chd']['total']['amount']; ?>
                                                                </div>
                                                            </div>


                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                                                <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
                                                                    Infant (s)
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
            <?php echo ($value['fares']['paxFares']['inf']['tax']['amount'] + $value['fares']['paxFares']['inf']['base']['amount'] ); ?>x   1 
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
            <?php echo $value['fares']['paxFares']['inf']['total']['amount']; ?>
                                                                </div>
                                                            </div>
            <?php
            $total_amount = 0;
            if (!empty($value['fares']['paxFares']['inf']['total']['amount'])) {
                $total_amount = $total_amount + $value['fares']['paxFares']['inf']['total']['amount'];
            }

            if (!empty($value['fares']['paxFares']['adt']['total']['amount'])) {
                $total_amount = $total_amount + $value['fares']['paxFares']['adt']['total']['amount'];
            }

            if (!empty($value['fares']['paxFares']['chd']['total']['amount'])) {
                $total_amount = $total_amount + $value['fares']['paxFares']['chd']['total']['amount'];
            }
            $other_amount = $value['fares']['totalFare']['total']['amount'] - $total_amount;
            ?>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                                                <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
                                                                    Other Amount
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
                                                            <?php
                                                            if ($other_amount > 0)
                                                                echo $other_amount;
                                                            else
                                                                echo '0';
                                                            ?>
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
                                                                    <?php
                                                                    if ($other_amount > 0)
                                                                        echo $other_amount;
                                                                    else
                                                                        echo '0';
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 totalamtb"></div>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
                                                                    <strong>Total Amount</strong>
                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">

                                                                </div>
                                                                <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
                                                                    <span class="fltprice txt-p"><i class="fa fa-rupee"></i><?php echo $value['fares']['totalFare']['total']['amount'] ?></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>                                          	
                                                </div>
                                            </div>

                                            <?php
                                            $incr++;
                                        }
                                    }
                                }else {
                                    ?>
                                    <div class="row results">
                                        <div class="flightinfo">
                                            No flights found
                                            <span style="float: right;"> <a id="back_main_page" name="back_main_page" href="<?php echo base_url() . 'admin/Search_flight' ?>">Back</a></span>
                                        </div>
                                    </div>					

                                <?php }
                                ?>
                                <!-- code end -->
                            </div>
                        </div>
                    </div>    

                    <!-- flight-service-list end -->
                </div>  

            </div>

        </div>
    </section>


</div>



<!-- /#wrapper -->

<script>
    var seat_fare_detail = {};
    var temp_seats;
    var onwardJourny = {};
    var returnJourny = {};
    $(document).ready(function() {

        $("#datepicker").datepicker({numberOfMonths: 2, minDate: 0, });
        $("#datepicker1").datepicker({numberOfMonths: 2, minDate: 0});

        /**************************load  from stop ************************************/
        $("#from_stop").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: base_url + "admin/search_bus/get_stop",
                    dataType: "json",
                    data: {
                        term: request.term,
                        // type: rechargeType
                    },
                    success: function(data) {
                        response($.map(data, function(item) {
                            return {
                                label: item.label,
                                name: item.name,
                            };
                        }));
                    }
                });
            },
            autoFocus: true,
            minLength: 1,
            select: function(event, ui) {

                /*$('#error_div').html('');
                 removeBillDetails();*/
                $("#from_stop").val(ui.item.name);

                if (ui.item.name == "No result found") {
                    $("#from_stop").val("");
                } else {
                    $("#from_stop_id").val(ui.item.id);
                }
            }
        });
        /**************************load  from stop ************************************/

        $("#to_stop").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: base_url + "admin/search_bus/get_stop",
                    dataType: "json",
                    data: {
                        term: request.term,
                        // type: rechargeType
                    },
                    success: function(data) {
                        response($.map(data, function(item) {
                            return {
                                label: item.label,
                                name: item.name,
                            };
                        }));
                    }
                });
            },
            autoFocus: true,
            minLength: 1,
            select: function(event, ui) {

                $("#to_stop").val(ui.item.name);

                if (ui.item.name == "No result found") {
                    $("#to_stop").val("");
                } else {
                    $("#to_stop_id").val(ui.item.id);
                }
            }
        });
        /******************************load all stops in to stop input*************/
        $('body').on('change', 'select.boarding', function() {
            var selectedboardstop = $(this).children("option:selected").val();
            $("#boarding_stops").html(selectedboardstop);
        });

        $('body').on('change', 'select.dropping', function() {
            var selecteddropstop = $(this).children("option:selected").val();
            $("#dropping_stops").html(selecteddropstop);
        });

        $("body").on('click', '#search_buses', function(e) {
            var journey_date = $('#datepicker').val();
            var return_date = $('#datepicker1').val();
            
            $('#loader').css({"display": "block"});
            $("#date_shown").html(moment(journey_date).format('DD-MMM-YYYY'));
            var url = base_url + "admin/search_bus/search_services";
            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: $('#search_services').serialize(),
                success: function(data) {

                    $("body #services_view_render").html(data.results);
                    $('#loader').css({"display": "none"});
                }
            });
            e.preventDefault();
        });

        /********************load boarding and dropping stop*******************************/
        $("body").on('click', '#submit_filter_seat', function() {
            var op_name = $(this).data("op-name");

            if (op_name == 'MSRTC')
            {
                var $current_object = $(this);
                var service_id = $(this).data("service-id");
                var provider_id = $(this).data("provider-id");
                var date = $(this).data("departure-date");

                if ($("#view_stops_" + service_id).css('display') == 'block') {

                    $("#view_stops_" + service_id).css({"display": "none"});
                    $("#layout_div_" + service_id).css({"display": "none"});
                    return false;
                }
                $(".view_bus_service_stops").removeClass("bg_gray").addClass("btn-purple").text("Select Seat");

                $(".bording-alighting-div,.layout_div_close").css({"display": "none"});

                if ($("#view_stops_" + service_id).css('display') == 'none')
                {
                    $(this).removeClass("btn-purple").addClass("bg_gray").text("Please Wait");

                }
                var url = base_url + "admin/Search_bus/get_service_stop_details";
                var dataString = 'service_id=' + service_id + '&provider_id=' + provider_id + '&date=' + date;
            } else {
                $('.dropdown').css("display", "block");
                var service_id = $(this).data("service-id");
                var provider_id = $(this).data("provider-id");
                var date = $(this).data("date");
                var from = $(this).data("from");
                var to = $(this).data("to");

                var trip_id = $(this).data("trip-id");
                var inventory_type = $(this).data("inventory-type");
                /* var boarding_stop =
                 var dropping_stop =*/
                var url = base_url + "admin/Search_bus/get_seat_layout";

                var dataString = 'service_id=' + service_id + '&provider_id=' + provider_id + '&date=' + date + '&from=' + from + '&to=' + to + '&trip_id=' + trip_id + '&inventory_type=' + inventory_type + '&operator_name=' + op_name;


            }
            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: dataString,
                success: function(data) {

                    if (data.from && data.to)
                    {
                        $("#view_stops_" + service_id).css({"display": "block"});
                        $current_object.removeClass("bg_gray").addClass("btn-purple").text("Hide seat");

                        var result = data.from;
                        var result1 = data.to;
                        var row = "";

                        row += "<option value='0'>Select Boarding Stop</option>";
                        for (i = 0; i < result.length; i++)
                        {
                            row += "<option value='" + result[i].from_bus_stop_name + "'>" + result[i].from_bus_stop_name + " - " + result[i].from_time + "</option>";
                        }


                        var row1 = "";
                        row1 += "<option value='0'>Select Dropping Stop</option>";
                        for (j = 0; j < result1.length; j++)
                        {
                            row1 += "<option value='" + result1[j].to_bus_stop_name + "'>" + result1[j].to_bus_stop_name + " - " + result1[j].to_time + "</option>";
                        }

                        $("#from_stop_" + service_id).html(row);
                        $("#to_stop_" + service_id).html(row1);
                    } else {
                        /* setInterval(function() {
                         alert("No fond Boarding and Dropping Stop ");
                         }, 2000); */
                        tempAlert("No fond Boarding and Dropping Stop ", 3000);
                        //setTimeout(function(){ alert("No fond Boarding and Dropping Stop "); }, 3000);
                        $current_object.removeClass("bg_gray").addClass("btn-purple").text("Select Seat");
                        return false;
                    }
                },
            });
        });
        /********************end load boarding and dropping stop *************************/
        /*********************seat layout start***********************************************/
        $('body').on('click', '#select_seat_layout', function() {

            $('#loader').css({"display": "block"});
            var service_id = $(this).data("service-id");
            var provider_id = $(this).data("provider-id");
            var date = $(this).data("date");
            var from = $("#from_stop_" + service_id).val(); // get value from boarding dropdown 
            var to = $("#to_stop_" + service_id).val();        // get value from dropping dropdown
            var trip_id = $(this).data("trip-id");
            var inventory_type = $(this).data("inventory-type");
            var op_name = $(this).data("op-name");

            var url = base_url + "admin/Search_bus/get_seat_layout";

            var dataString = 'service_id=' + service_id + '&provider_id=' + provider_id + '&date=' + date + '&from=' + from + '&to=' + to + '&trip_id=' + trip_id + '&inventory_type=' + inventory_type + '&operator_name=' + op_name;

            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: dataString,
                success: function(data) {
                    var listrow = '';
                    seat_fare_detail = data;
                    var result = data.seats;
                    var row = data.total_rows;
                    var col = data.total_cols;
                    var result = data.seats;
                    var total_cols = data.total_cols;
                    var total_rows = data.total_rows;
                    
                    /* listrow += "<ul class='deckrow'>"+
                     +"<li>"+"<ul>" + "<li>" +"<a class='myBusDriveSeatImage driver_booked' href='#' style='width: 20px; height: 20px;'></a></li>";
                     
                     for(var i = 0; i< total_cols; i++) {
                     listrow +=  "<li>" +
                     "<ul>";
                     for(var j = 0; j < total_rows; j++) {
                     var seat = getSeatDetails(i,j, result);
                     if(seat !=null){
                     listrow  += seat;
                     } else {
                     listrow  +='<li>  &nbsp; &nbsp; <li>';
                     }
                     }
                     listrow += "</ul>" + "</li>";
                     
                     }
                     
                     listrow += "</li></ul>";  deckrow_"+service_id+*/
                    var data = '<ul class="deckrow deckrow_'+service_id+'" >';
                    for (var i = 0; i < col; i++) {
                        data += "<li>";
                        data += "<ul>";
                        for (var j = 0; j < row; j++) {

                            if (i === 0 && j === 0) {
                                data += '<li><a class=" myBusDriveSeatImage driver_booked" href="#" style="width: 20px; height: 20px;"></a></li>';
                            } else if (j === 0) {
                                data += '<li><a href="#" style="width: 20px; height: 20px;">   </a></li>';
                            }

                            var isSeat = getSeatDetails(i, j, result, service_id);
                            if (isSeat) {
                                data += isSeat;
                            } else {
                                data += '<li><a href="#" style="width: 20px; height: 20px;">    </a></li>';
                            }

                        }
                        data += "</ul>";
                        data += "</li>";
                    }

                    data += "</ul>";
                    $(".layout_div_close").hide();
                    $("#layout_div_" + service_id).show();
                    $("#bus-seat-layout-" + service_id).html(data);
                    $('#loader').css({"display": "none"});
                },
                error: function(data) {
                    // do something
                }
            });


        });
        /************************ end seat layout start**************************************/


        /********************* on click seat get details*********************/
        /* block seat code*/
       

        $('body').on('click', ".deckrow .seat", function() {
            var seat_info = seat_fare_detail;
            var curtSeatFare = parseInt($('#get_fare_id').text()); // get total fare value
            var curtTotalFare = parseInt($('#total_fare').text()); // get total fare value
            var data_fare = $(this).attr('data-fare'); // get fare value;
            var total_fare = $(this).attr('data-total-fare'); // get fare value;
            var gender = $(this).attr('data-ladies-seat');
            var sleeper = $(this).attr('data-sleeper');
            var ac = $(this).attr('data-ac');

            var data_seat_no = $(this).attr('data-seat-no'); // get set no
            var birth = $(this).attr('data-berth');
            var current_seat_type = $(this).attr('data-gender');
            
            var bus_travel_id  = $(this).attr('data-bus-travel-id');
            var $par_elem = $('#layout_div_'+bus_travel_id+'');
            var $deck_row = $par_elem.find('.deckrow_'+bus_travel_id+'');
            var class_prefix = (current_seat_type == "F" && current_seat_type != undefined) ? "ladies_" : "";
            
            for(var s = 0; s < seat_info.seats.length; s++) {
                if(typeof(data_seat_no) != "undefined" && seat_info.seats[s].available == "Y") {
                    if(seat_info.seats[s].seat_no == data_seat_no)
                    {
                        
                        if($(this).hasClass(class_prefix+'available'))
                        {
                            $(this).removeClass(class_prefix+'available');
                            $(this).addClass(class_prefix+'selected userseatselected');
                        }
                        else if($(this).hasClass(class_prefix+'selected'))
                        {
                            $(this).removeClass(class_prefix+'selected userseatselected');
                            $(this).addClass(class_prefix+'available');           
                        } 
                        break
                    }
                }
            }
            var total_sel_seat = $deck_row.find('.seat.userseatselected').length;
            
            
            
            if (total_sel_seat > 6)
            {
                showNotification("danger", "You can not select more than 6 seat.");
                $(this).removeClass(class_prefix+'selected userseatselected');
                $(this).addClass(class_prefix+'available');  
                return false;
            } else {
               
                cal_fare(seat_info, data_seat_no, bus_travel_id);
            }

        });

        /*temp booking on click continue*/
        $('body').on('click', ".continue", function(e) {
            
            var form_url = base_url + "admin/search_bus/save_temp_booking";
            var trip_no = $(this).attr("data-trip-no");
            var bus_travel_id = $(this).attr("data-bus-travel-id");
            var provider_id = $('.article_'+bus_travel_id).attr("data-provider-id");
            var provider_type = $('.article_'+bus_travel_id).attr("data-provider-type");
            
            if(provider_id != "" && provider_type != "") {
                if(Object.keys(onwardJourny).length == 0) {
                    if(temp_seats != "" && temp_seats != undefined && Object.keys(temp_seats).length) {
                        var from_stop_val = $('#from_stop_'+bus_travel_id).val();
                        var to_stop_val = $('#to_stop_'+bus_travel_id).val();
                        
                        onwardJourny['from'] = $('.article_'+bus_travel_id).attr("data-from");
                        onwardJourny['to'] = $('.article_'+bus_travel_id).attr("data-to");
                        onwardJourny['date'] = $('#datepicker').val();
                        onwardJourny['date_of_jour'] = $('#datepicker').val();
                        
                        onwardJourny['dept_time'] = $('#sch_departure_'+bus_travel_id).text();
                        onwardJourny['alighting_time'] = $('#sch_alighting_'+bus_travel_id).text();
                        onwardJourny['boarding_time'] = $('#from_stop_'+bus_travel_id).find('option:selected').text();
                        onwardJourny['droping_time'] = $('#to_stop_'+bus_travel_id).find('option:selected').text();
                        
                        onwardJourny['boarding_stop_name'] = $('#from_stop_'+bus_travel_id).val();
                        onwardJourny['dropping_stop_name'] = $('#to_stop_'+bus_travel_id).val();
                        onwardJourny['trip_no'] = $('.article_'+bus_travel_id).attr("data-trip-no");
                        onwardJourny['service_id'] = bus_travel_id;
                        onwardJourny['provider_id'] = provider_id;
                        onwardJourny['provider_type'] = provider_type;
                        onwardJourny['inventory_type'] = $('.article_'+bus_travel_id).attr("inventory-type");
                        onwardJourny['data_bus_type'] = $('.article_'+bus_travel_id).attr("data-bus-type");
                        onwardJourny['op_name'] = $('.article_'+bus_travel_id).attr("data-travel");
                    }
                }
            }
            onwardJourny['seats'] = temp_seats;
            
            $.ajax({
                type: "POST",
                dataType: "json",
                url: form_url,
                data: {"onwardJourny": onwardJourny},
                success: function(data) {

                    window.location.href = data.redirect;

                },
                error: function(data) {
                    // do something
                }
            });
            e.preventDefault();
        });
        
        /*temp booking*/
        
        /******** Sort Column Start**********/
        $(document).off('click', '.sort_col').on('click', '.sort_col', function(e) {
            e.preventDefault();

            $(".sort_col").removeClass("active");
            $(this).addClass("active");

            var col = $(this).attr('ref').trim();
            
            var stype = $(this).attr('data-sort').trim();

            $(this).attr('data-sort', stype == 'asc' ? 'desc' : 'asc');
            
            $('.sort_list').sort(function(a, b) {

                if(col != "data-travel")
                {
                    var a = parseFloat($(a).attr(col).trim());
                    var b = parseFloat($(b).attr(col).trim());
                }
                else
                {
                    var a = String($(a).attr(col).trim());
                    var b = String($(b).attr(col).trim());
                }
                
                
                if (stype == 'asc')
                {
                    return a > b ? 1 : -1;
                }
                else
                {
                    return a > b ? -1 : 1;
                }
            });


            $('.stu_sort_list').sort(function(a, b) {

                if(col != "data-travel")
                {
                    var a = parseFloat($(a).attr(col).trim());
                    var b = parseFloat($(b).attr(col).trim());
                }
                else
                {
                    var a = String($(a).attr(col).trim());
                    var b = String($(b).attr(col).trim());
                }
                
                if (stype == 'asc')
                {
                    return a > b ? 1 : -1;
                }
                else
                {
                    return a > b ? -1 : 1;
                }
            });
        });
        /******** Sort Column End************/
        
        /**** Range filter *********/
        $("#range_slider, input[type=range]").on("mouseup", function() {
            var buscount = 0;
            var rangeFare = $(this).val();
           
            $('.fare_sort_list').each(function(index) {
                
                if(parseFloat($(this).attr('data-fare')) >= rangeFare) {
                    buscount++;
                    $(this).show();
                    $('#total-result-found').text(buscount);
                    $('#totalBusCnt').text(buscount);
                } else {
                    
                    $(this).hide();
                }
               
            });
           
        });
        
    });

    function getSeatDetails(cols, rows, result, service_id) {
        var seat = null;
        //alert(col+ ' col ----- '); alert(row+ ' row----- ');
        for (var i = 0; i < result.length; i++) {

            if (result[i].row === rows && result[i].col === cols) {
                //seat = result.seats[i];

                if (result[i].available == "Y")
                {
                    if (result[i].ladies_seat == true)
                    {
                        var current_seat_class = "ladies_available";
                    } else {
                        var current_seat_class = "available";
                    }
                }
                else if (result[i].available == "N") {
                    var current_seat_class = "booked";
                }


                var ac_seat = (result[i].ac) ? result[i].ac : "True";
                var ladies_seat = (result[i].ladies_seat) ? result[i].ladies_seat : "false";
                var sleeper = (result[i].sleeper) ? result[i].sleeper : "True";
                var berth = (result[i].berth) ? result[i].berth : "0";
                var total_fare = (result[i].total_fare) ? (result[i].total_fare) : (result[i].sub_total_fare);
                if (ladies_seat == 'false')
                {
                    var gender = 'M';
                }
                else
                {
                    var gender = 'F';
                }
                seat = "<li><a class='seat " + current_seat_class + "' href='javascript:void(0)' style='width: 20px; height: 20px;' data-bus-travel-id=" + service_id + " data-seat-no=" + result[i].seat_no + " data-fare=" + result[i].sub_total_fare +" data-current_seat_type =" + ladies_seat +" data-total-fare=" + total_fare + " title='Seat No " + result[i].seat_no + " | Fare : Rs. " + result[i].total_fare + "' data-ac=" + result[i].ac + " data-sleeper=" + result[i].sleeper + " data-ladies-seat=" + ladies_seat + " data-berth=" + result[i].berth + " data-gender=" + gender + " ></a></li>";
                break;
            }

        }

        return seat;
    }

    function tempAlert(msg, duration)
    {

        var el = document.createElement("div");
        el.setAttribute("style", "position:fixed;top:40%;left:40%;width:500px;height:50px;background-color:#e44a50; text-align:center");
        el.innerHTML = msg;
        setTimeout(function() {
            el.parentNode.removeChild(el);
        }, duration);
        document.body.appendChild(el);
    }


    function showNotification(msg_type, msg)
    {
        msg_type = (msg_type == "error") ? "danger" : msg_type;
        // bootstrapGrowl(msg, msg_type, {"delay":"5000","align":"center","offset":{"from":"top","amount":"250"}}); 
        bootstrapGrowl(msg, msg_type, {"delay": "5000", "align": "right"});
    }

    function bootstrapGrowl(msg, msg_type, msg_config)
    {
        var offset_from = 'top';
        var offset_amount = 10;

        if (typeof (msg_type) == 'undefined')
        {
            msg_type = 'info';
        }

        if (typeof (msg_config) == 'undefined')
        {
            msg_config = {};
        }
        else if (typeof (msg_config.offset) != 'undefined' && msg_config.offset != null)
        {
            offset_from = (msg_config.offset.from != undefined && msg_config.offset.from != "") ? msg_config.offset.from : "top";
            offset_amount = (msg_config.offset.amount != undefined && msg_config.offset.amount != "") ? msg_config.offset.amount : 10;
        }

        var config = {
            ele: 'body', // which element to append to
            type: msg_type, // (null, 'info', 'danger', 'success')
            offset: {from: offset_from, amount: offset_amount}, // 'top', or 'bottom'
            align: 'right', // ('left', 'right', or 'center')
            width: 350, // (integer, or 'auto')
            delay: 3000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
            allow_dismiss: true, // If true then will display a cross to close the popup.
            stackup_spacing: 10 // spacing between consecutively stacked growls.                
        };

        var fConfig = $.extend({}, config, msg_config);

        $.bootstrapGrowl(msg, fConfig);
    }
    
    
    function cal_fare(seat_info, seat_no, bus_travel_id) {
        var new_seat_info = {};
       
        jQuery.each( seat_info.seats, function( i, val ) {
            var k = val.seat_no;
            new_seat_info[k]= val;
        });
       
        var fares = 0;
        var actual_total_fares = 0;
        var total_discount = 0;
        var basic_fares = 0;
        var service_tax = 0;
        var op_service_charge = 0;
        var $par_elem = $('#layout_div_'+bus_travel_id);
        var $deck_row = $par_elem.find('.deckrow_'+bus_travel_id);
        var total_sel_seat = $deck_row.find('.seat.userseatselected').length;
        var tseat = {};
        var showSeat = {};
        var data_seat_info = {};
        var birth_count = $(".deckrow_"+bus_travel_id).length;
        /* var op_id = $('.article_'+bus_travel_id+'').attr('data-op-id');
            var bus_type_id = $('.article_'+bus_travel_id+'').attr('data-bus-type'); */

        $par_elem.find('.sel_seat').html("");
            
        $(".deckrow_"+bus_travel_id).each(function(bind)
        {
            var tbirth = $(this).find('.seat.userseatselected').attr('data-berth');
                
            if($(this).find('.seat.userseatselected').length > 0)
            {
                tseat[tbirth] = new Array();
                showSeat[tbirth] = new Array();
                //tseat['birth'] = tbirth;
                var i =0;
                $(this).find('.seat.userseatselected').each(function()
                {
                    if($(this).hasClass('userseatselected'))
                    {
                        var data_seat_no = $(this).attr('data-seat-no');
                        data_seat_info['data_seat_no'] = $(this).attr('data-seat-no');
                        data_seat_info['seat_fare'] = parseFloat(new_seat_info[data_seat_no].sub_total_fare);
                        data_seat_info['total_fare'] = parseFloat(new_seat_info[data_seat_no].total_fare);
                        data_seat_info['is_ladies'] = $(this).attr('data-ladies-seat');
                        data_seat_info['is_sleeper'] = $(this).attr('data-sleeper');
                        data_seat_info['is_ac'] = $(this).attr('data-ac');
                        data_seat_info['seat_birth'] = tbirth;
                        // var data_berth_no = $(this).attr('data-berth');
                        //for(var s = 0; s < seat_info.seats.length; s++) {
                        // if(seat_info.seats[s].seat_no == data_seat_no)
                        // {
                        fares += parseFloat(new_seat_info[data_seat_no].sub_total_fare);
                        actual_total_fares += parseFloat(new_seat_info[data_seat_no].total_fare);
                        basic_fares += parseFloat(new_seat_info[data_seat_no].sub_total_basic_fare);
                        $par_elem.find('.fare_value .fare').html(basic_fares.toFixed(2));
                        service_tax += parseFloat(new_seat_info[data_seat_no].service_tax_amount);
                        //op_service_charge += (parseFloat(seat_info.seats[0].convey_charge_percent) != null && parseFloat(seat_info.seats[0].service_tax_amount) != undefined) ? parseFloat(seat_info.seats[0].convey_charge_percent)) : 0;
                        //total_discount += (seat_info[bus_travel_id][tbirth][data_seat_no]["seat_discount"] != null && seat_info[bus_travel_id][tbirth][data_seat_no]["seat_discount"] != undefined) ? parseFloat(seat_info[bus_travel_id][tbirth][data_seat_no]["seat_discount"]) : 0;
                                    
                        // }
                        // }
                        showSeat[tbirth].push(data_seat_no);
                        tseat[tbirth].push(data_seat_info);
                        i++;
                        data_seat_info = {};
                    }
                });

                showSeat[tbirth].sort(function(a, b){return a-b});

                if(birth_count > 1)
                {
                    var berth_name = (tbirth == 0) ? " Lower" :  (tbirth == 2) ? " Upper" : "";
                    $par_elem.find('.sel_seat').append(berth_name+" : "+showSeat[tbirth].join(', '));
                }
                else
                {
                    $par_elem.find('.sel_seat').append(showSeat[tbirth].join(', '));
                }
                    
                $par_elem.find('.fare').html(actual_total_fares.toFixed(2));
                $par_elem.find('.tot_fare').html(actual_total_fares.toFixed(2));
                $par_elem.find('.total_amount').html(fares.toFixed(2));
                $par_elem.find('.fare_service_tax').html(service_tax.toFixed(2));
                $par_elem.find('.fare_service_charge').html(op_service_charge.toFixed(2));
            } else {
                $par_elem.find('.fare').html('0.00');
                $par_elem.find('.tot_fare').html('0.00');
                $par_elem.find('.total_amount').html('0.00');
                $par_elem.find('.fare_service_tax').html('0.00');
                $par_elem.find('.fare_service_charge').html('0.00');
            }
            temp_seats = tseat;
        });
    }
    
    function toggle_visibility(id) 
    {
        var e = document.getElementById(id);
        if(e.style.display == 'block')
            e.style.display = 'none';
        else
            e.style.display = 'block';
    }

</script>
<script type="text/javascript">


    function getfare(key,index)
    {   
        var url = "<?php echo base_url() ?>" + "admin/search_flight/fare_rule";
        // alert(key);
        var dataString = 'keys=' + key ;
        $.ajax({
            type: 'POST',
            url: url,
            dataType: "json",
            data: dataString,
            success: function (response) {
                $("#termcondition_"+index).html(response.additionalTextData.additionalTextData);
                if(response.additionalTextData.dateChangeFee.isChangePermitted){
                    $("#date_change_"+index).html(response.additionalTextData.dateChangeFee.dateChangeRange[0].Value);  
                }
                if(response.additionalTextData.cancellationFee.isRefundable){
                    $("#cancellation_"+index).html(response.additionalTextData.cancellationFee.cancellationRange[0].Value);   
                }
            
            }
        });
        ///alert("herer");
    }
</script>

<script type="text/javascript">
    function book(key){

        console.log(key);

        var dataString = 'key=' + key;
        $.post("<?php echo base_url() ?>admin/search_flight/set_flight_key", {"key": key});

        /* url = "<?php echo base_url() ?>admin/search_flight/set_flight_key";

    $.ajax({
        type: "POST",
        dataType: "text",
        url: url,
        data: dataString,
        success: function(data) {
                console.log(data);

      }, error: function (jqXHR, exception) {
                console.log(jqXHR.status);
                console.log(exception);
		
        }

  });

    var booking_form = "<?php echo base_url() ?>admin/search_flight/flight_booking_form";
    console.log(booking_form);
    window.location = booking_form;*/
        var booking_form = "<?php echo base_url() ?>admin/search_flight/flight_booking_form";
        // window.location = booking_form;
        setTimeout(function() {
            window.location.href = booking_form;
        }, 1000);

    }
</script>
