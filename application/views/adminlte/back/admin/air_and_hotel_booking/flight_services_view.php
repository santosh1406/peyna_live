<div class="col-sm-4 col-md-2">
    <h4 class="search-results-title">
        <i class="glyphicon glyphicon-search mr10 icon-search"></i>
        <b><span id="total-result-found"><?php echo count($serviceResult['result']); ?></span></b> results found.
    </h4>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <i class="short-full glyphicon glyphicon-plus"></i>
                        Fare
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse price-filter" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                <div class="panel-body">
                    <span><?php echo $min_fare?></span> <span style="float:right"><?php echo $max_fare?></span> 
                    <input id="range_slider" type="range" min="<?php echo $min_fare?>" max="<?php echo $max_fare?>" value="<?php echo $min_fare?>"  />  
                    
                </div>
                    <input id="ex1" data-slider-id="ex1Slider" type="text" data-slider-min="-5" data-slider-max="20" data-slider-step="1" data-slider-value="14" style="display: none;" data-value="11" value="11">
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <i class="short-full glyphicon glyphicon-plus"></i>
                        Travels
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                    <form action="" method="get">
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="MSRTC" type="checkbox" value=""> <a href="#">MSRTC</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">Neeta Tours and Travels</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        <i class="short-full glyphicon glyphicon-plus"></i>
                        Bus Type
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                    <form action="" method="get">
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="MSRTC" type="checkbox" value=""> <a href="#">Reclined [Seats]</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">Non AC Seater</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">AC Semi Sleeper</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">AC Sleeper</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">AC Seater</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">Others</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingFour">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        <i class="short-full glyphicon glyphicon-plus"></i>
                        Departure Time
                    </a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                <div class="panel-body">
                    <form action="" method="get">
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="MSRTC" type="checkbox" value=""> <a href="#">00:00 - 03:59(Mid Night)</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">04:00 - 11:59 (Morning)</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">12:00 - 15:59 (Afternoon)</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">16:00 - 18:59 (Evening)</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">19:00 - 23:59 (Night)</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingFive">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                        <i class="short-full glyphicon glyphicon-plus"></i>
                        Duration
                    </a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                <div class="panel-body">
                    <form action="" method="get">
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="MSRTC" type="checkbox" value=""> <a href="#">Upto 6 Hours</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">6 Hours - 12 Hours</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">12 Hours - 24 Hours</a>
                            </div>
                        </div>
                        <div class="col-md-12 mb5">
                            <div class="collapse-cnt">
                                <input name="Neeta" type="checkbox" value=""> <a href="#">More than a day</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div><!-- panel-group -->
</div>

<div class="col-sm-8 col-md-10">
    <div class="sort-by-section clearfix mb10">
        <h4 class="sort-by-title block-sm">Sort results by:</h4>
        <ul class="sort-bar clearfix">
            <li class="sort-by-name sort_col" ref="data-travel" data-sort="asc"><a class="sort-by-container" href="#"><span>Travels</span></a></li>
            <li class="sort-by-depature sort_col" ref="data-dep_tm_str" data-sort="asc"><a class="sort-by-container" href="#"><span>Depature</span></a></li>
            <li class="sort-by-seats sort_col" ref="data-seat" data-sort="asc"><a class="sort-by-container" href="#"><span>Seats</span></a></li>
            <li class="sort-by-fare sort_col" ref="data-fare" data-sort="asc"><a class="sort-by-container" href="#"><span>Fare</span></a></li>
        </ul>
    </div>

    <div class="bus-service-list">
        <div class="state" id="services_view_render"> 
            <?php 

           //echo '<pre>';print_r($flightDetails);die;
            if (count($onwardJourneysDetails) > 0) : ?>
            <div class="panel panel-default">
                <div class="boxstyle1" role="tab">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            <i class="adddetails glyphicon glyphicon-plus"></i>
                            <h4 class="service-title">
                                <!-- <span class="service-title-main clearfix">Maharashtra State Road Transport Corporation</span>
                                <small>Total Bus - <span id="totalBusCnt"><?php echo count($serviceResult['result']); ?> </span></small> -->
                            </h4>

                            <div>
                                <form action="<?php echo base_url()?>admin/search_flight/flight_booking" method="POST">
                                <table>
                                    <tr>
                                        <th>Depart</th>
                                        <th>Duration</th>
                                        <th>Arrival</th>
                                        <th>Airlines</th>
                                        <th>Price</th>
                                    </tr>
                                    <?php 

                                    
                                    
                                    foreach ($onwardJourneysDetails as $key => $value) {

                                        foreach ($value['flights'] as $index => $flightInfo) {

                                            $flyTime = $flightInfo['flyTime'];
                                            $hours = floor($flyTime / 60);
                                            $min = $flyTime - ($hours * 60);
                                            $duration = $hours.":".$min;

                                    
                                           //echo '<pre>'; print_r($value['fares']['totalFare']['total']['amount']);die;
                                       ?>
                                       
                                   

                                    <tr>
                                        <td><?php echo date("H:i",strtotime($flightInfo['depDetail']['time']))?></td>
                                        <td><?php echo $duration?></td>
                                        <td><?php echo date("H:i",strtotime($flightInfo['arrDetail']['time']))?></td>
                                        <td><?php echo $flightInfo['carrier']['name']?></td>
                                        <td><?php echo $value['fares']['totalFare']['total']['amount']?></td>
                                        <td>
                                          <!--   <input type="submit" value="Book"> -->
                                            <a href="" onclick="book('<?php echo $value['key']?>');">Book</a>
                                        </td>
                                        <td>
                                            <input type="hidden" name="key" id="key" value="<?php echo $value['key']?>">
                                        </td>
                                    </tr>
                                  

                                    <?php }}
                                        ?>


                                </table>
                                
                                </form>

                            </div>

                            <div class="sub-content">
                                <div class="col-md-2 col-xs-12">
                                    <div class="start-time padding20 border-r">
                                        <i class="soap-icon-adventure purple soap-icon"></i>
                                        <div class="service-details">
                                            <span class="text-uppercase txt-p">Starts at</span>
                                            <span><small><?php echo $startTime; ?></small></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-xs-12">
                                    <div class="duration padding20 border-r">
                                        <i class="soap-icon-clock purple soap-icon"></i>
                                        <div class="service-details">
                                            <span class="text-uppercase txt-p">Duration</span>
                                            <span><small><?php echo isset($min_time) ? $min_time : '' ?> - <?php echo isset($max_time) ? $max_time : '' ?></small></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-xs-12">
                                    <div class="available-seats padding20 border-r">
                                        <i class="soap-icon-passenger purple soap-icon"></i>
                                        <div class="service-details">
                                            <span class="text-uppercase txt-p">Available Seats</span>
                                            <span><small><?php echo $totalSeat; ?></small></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-xs-12">
                                    <div class="amenities padding20 border-r">
                                        <i class="fa fa-mobile circle"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </h4>
                </div>

            </div>
            <?php else: ?>
            <div class="panel panel-default">
                <h3>No Services Found!!!!.</h3>
            </div>
            <?php endif; ?>
            <!--- State Road Transport Start -->
            <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                <div class="panel panel-default">
                    <div class="boxstyle1" role="tab">
                        <?php if (count($serviceResult['result']) > 0) : foreach ($serviceResult['result'] as $key => $value) : ?>
                        <article class="box fare_sort_list article_<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?> stu_sort_list" data-fare="<?php echo isset($value['seat_fare']) ? $value['seat_fare'] : '' ?>" data-seat="<?php echo isset($value['available_seat']) ? $value['available_seat'] : '' ?>" data-bus-type="<?php echo isset($value['bus_type_name']) ? $value['bus_type_name'] : '' ?>" data-travel="<?php echo isset($value['operator_name']) ? $value['operator_name'] : '' ?>" data-duration-time="" data-dep_tm_str="<?php echo isset($value['departure_date']) ? $value['departure_date'] : '' ?>" data-op-bus-type-cd="SS" data-service-discount="false" op-trip-no="<?php echo isset($value['trip_id']) ? $value['trip_id'] : '' ?>" data-op-name="MSRTC" data-trip-no="<?php echo isset($value['trip_id']) ? $value['trip_id'] : '' ?>" data-bus-travel-id="<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>" data-from="<?php echo isset($value['from_stop']) ? $value['from_stop'] : '' ?>" data-to="<?php echo isset($value['to_stop']) ? $value['to_stop'] : '' ?>" data-date="<?php echo isset($value['departure_date']) ? $value['departure_date'] : '' ?>" data-provider-id="<?php echo isset($value['provider_id']) ? $value['provider_id'] : '' ?>" data-provider-type="stu"  data-travel-duration="" inventory-type="<?php echo isset($value['inventory_type']) ? $value['inventory_type'] : '' ?>" >
                                <div class="boxstyle1 bg-grey-l" role="tab">
                                    <h4 class="panel-title">
                                        <a> 
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <h4 class="service-title">
                                                        <span class="service-title-main clearfix"><?php echo isset($value['operator_name']) ? $value['operator_name'] : '' ?> - <span class="from-to purple"><?php echo isset($value['route_no_name']) ? $value['route_no_name'] : '' ?></span></span>
                                                        <small class="bus-type"><?php echo isset($value['bus_type_name']) ? $value['bus_type_name'] . " / " . $value['op_bus_type_name'] : '' ?> </small>
                                                    </h4>
                                                </div>
                                                <div class="col-md-3">
                                                    <span class="pull-right purple fnt16"><strong>Rs. <?php echo isset($value['seat_fare']) ? $value['seat_fare'] : '' ?>/-</strong></span>
                                                </div>
                                            </div>
                                            <div class="sub-content">
                                                <div class="col-md-2 col-xs-12">
                                                    <div class="start-time padding20 border-r">
                                                        <i class="soap-icon-adventure purple soap-icon"></i>
                                                        <div class="service-details">
                                                            <span class="text-uppercase txt-p">Departure</span>
                                                            <span><small id="sch_departure_<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>"><?php echo isset($value['departure_date']) ? $value['departure_date'] : '' ?></small></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-xs-12">
                                                    <div class="duration padding20 border-r">
                                                        <i class="soap-icon-departure purple soap-icon"></i>
                                                        <div class="service-details">
                                                            <span class="text-uppercase txt-p">Alighting</span>
                                                            <span><small id="sch_alighting_<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>"><?php echo isset($value['arrival_date']) ? $value['arrival_date'] : '' ?></small></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-xs-12">
                                                    <div class="duration padding20 border-r">
                                                        <i class="soap-icon-clock purple soap-icon"></i>
                                                        <div class="service-details">
                                                            <span class="text-uppercase txt-p">Duration</span>
                                                            <span><small><?php echo isset($value['duration']) ? $value['duration'] : '' ?> HRS</small></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-xs-12">
                                                    <div class="available-seats padding20 border-r">
                                                        <i class="soap-icon-passenger purple soap-icon"></i>
                                                        <div class="service-details">
                                                            <span class="text-uppercase txt-p">Available Seats</span>
                                                            <span><small><?php echo isset($value['available_seat']) ? $value['available_seat'] : '' ?></small></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-xs-12">
                                                    <div class="amenities padding20 border-r">
                                                        <i class="fa fa-mobile circle-d purple"></i>
                                                        <i class="soap-icon-notice circle purple"></i>
                                                        <i class="soap-icon-close circle purple"></i>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-xs-12">
                                                    <!--<button type="submit" id="submit_filter_btn" class="btn btn-purple txt-w mt20">Select Seat</button>-->
                                                    <button class="collapsed btn btn-purple txt-w mt20 view_bus_service_stops" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven" id="submit_filter_seat" data-service-id="<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>" data-provider-id="<?php echo isset($value['provider_id']) ? $value['provider_id'] : '' ?>" data-departure-date="<?php echo isset($value['departure_date']) ? $value['departure_date'] : '' ?>" data-op-name="<?php echo isset($value['operator_name']) ? $value['operator_name'] : '' ?>">Select Seat</button>
                                                </div>
                                            </div>
                                        </a>
                                    </h4>

                                    <div class="bording-alighting-div clearfix" id="view_stops_<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven" style="display:none;">
                                        <div class="col-md-12">
                                            <div class=" box-style3 mb20 clearfix">
                                                <div class="col-md-4 col-xs-12">
                                                    <span class="text-uppercase">Select Boarding Point </span>
                                                    <br>
                                                    <select id="from_stop_<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>" class="form-control mb10" placeholder="Select Bording Point" name="boarding_stop">
                                                        <option>Select Pickup Point</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4 col-xs-12">
                                                    <span class="text-uppercase">Select Dropping Point </span>
                                                    <br>
                                                    <select id="to_stop_<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>" class="form-control mb10" placeholder="Select Dropping Point" name="dropping_stop">
                                                        <option>Select Dropping Point</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-2 col-xs-12">
                                                    <button type="submit" id="select_seat_layout" class="btn btn-purple txt-w mt20" data-trip-id="<?php echo isset($value['trip_id']) ? $value['trip_id'] : '' ?>" data-service-id="<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>" data-provider-id="<?php echo isset($value['provider_id']) ? $value['provider_id'] : '' ?>" data-inventory-type="<?php echo isset($value['inventory_type']) ? $value['inventory_type'] : '' ?>" data-date="<?php echo isset($value['departure_date']) ? $value['departure_date'] : '' ?>"  data-op-name="<?php echo isset($value['operator_name']) ? $value['operator_name'] : '' ?>">Select Seat</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="bus-seat-div clearfix layout_div_close" style="display:none" id="layout_div_<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>" >
                                        <div class="col-md-12">
                                            <div class="box-style3 mb20 clearfix">
                                                <form>
                                                    <div class="clearfix seat-block">
                                                        <h3 class="more-seat-detail red" style="display:block;">Maximum 6 seats allowed per booking</h3>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="legend mb20">
                                                                    <label>
                                                                        <span class="seat available"></span>
                                                                        Available Seat
                                                                    </label>
                                                                    <label>
                                                                        <span class="seat selected"></span>
                                                                        Selected Seat
                                                                    </label>
                                                                    <label>
                                                                        <span class="seat booked"></span>
                                                                        Booked Seat
                                                                    </label>
                                                                    <label>
                                                                        <span class="seat ladies_available"></span>
                                                                        Ladies Seat
                                                                    </label>
                                                                    <label>
                                                                        <span class="seat mens_available"></span>
                                                                        Mens Seat
                                                                    </label>
                                                                    <label>
                                                                        <span class="seat handicapped_available"></span>
                                                                        Blind/Handicapped
                                                                    </label>
                                                                    <label>
                                                                        <span class="seat senior_citizen_available"></span>
                                                                        Senior Citizen
                                                                    </label>
                                                                </div>

                                                                <div class="col-md-8 col-xs-12 seat-layout">
                                                                    <div class="fare-tab-filter show">
                                                                        <ul></ul>
                                                                    </div>
                                                                    <br>
                                                                    <span class="bus-seat-layout" id="bus-seat-layout-<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>">
                                                                        
                                                                    </span>
                                                                </div>
                                                                <div class="col-md-4 col-xs-12 seat-detail-info pull-right mt40">
                                                                    
                                                                        <div class="SelectedSeats clearfix">
                                                                            <label>Seat(s)  :</label>
                                                                            <span class="sel_seat"></span>
                                                                        </div>
                                                                        <div class="amount clearfix">
                                                                            <label>Base Fare : </label>
                                                                            <span class="fare_value">Rs. <b class="fare">0.00</b></span>
                                                                        </div>
                                                                        <div class="amount clearfix" id="gst_amount_div">
                                                                            <label>GST : </label>
                                                                            <span class="fare_value">Rs. <b class="fare_service_tax">0.00</b></span>
                                                                        </div>
                                                                        <div class="amount clearfix">
                                                                            <label>Service Charge : </label>
                                                                            <span class="fare_value">Rs. <b class="fare_service_charge">0.00</b></span>
                                                                        </div>
                                                                        <div class="amount clearfix">
                                                                            <label>Sub Total : </label>
                                                                            <span class="fare_value">Rs. <b class="tot_fare">0.00</b></span>
                                                                        </div>
                                                                        <div class="amount clearfix total_fare_discount_div">
                                                                            <label>Discount : </label>
                                                                            <span class="fare_value">Rs. - <b class="total_fare_discount_value">0.00</b></span>
                                                                        </div>
                                                                         <div class="amount clearfix base_fare_discount_div hide">
                                                                            <label>Discount : </label>
                                                                            <span class="fare_value">Rs. - <b class="base_fare_discount_value">0</b></span>
                                                                        </div>
                                                                        <div class="amount clearfix">
                                                                            <label>Total Amount : </label>
                                                                            <span class="fare_value">Rs. <b class="total_amount">0.00</b></span>
                                                                        </div>
            
                                                                    <div class="col-md-12 boarding-dropping">          
                                                                        <div>
                                                                            <button class="btn txt-w btn-medium btn-purple continue " data-trip-no="<?php echo isset($value['trip_id']) ? $value['trip_id'] : '' ?>" data-bus-travel-id="<?php echo isset($value['service_id']) ? $value['service_id'] : '' ?>">Continue</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </article>
                            <?php endforeach;
                        else: ?>
                            <p>No services found!!!!! </p>
<?php endif; ?>
                    </div>
                </div>
            </div>

        </div>  

    </div>
<script type="text/javascript">
function book(key){

    console.log(key);

    var dataString = 'key=' + key;
    url = "<?php echo base_url()?>admin/search_flight/set_flight_key";

    $.ajax({
        type: "POST",
        dataType: "json",
        url: url,
        data: dataString,
        success: function(data) {

      }

  });

    var booking_form = "<?php echo base_url()?>admin/search_flight/flight_booking_form";
    console.log(booking_form);
    window.location = booking_form;
  }
</script>

    <!-- End -->