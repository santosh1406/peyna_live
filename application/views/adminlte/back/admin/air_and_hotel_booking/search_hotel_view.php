<!-- Created by Sonali Kamble on 20-august-2018 -->
<style>
ul.seat li
{
    cursor: pointer; 
    display: inline; 
    padding: 2px;  
}
</style>
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content" id="module">
        <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header"> 
                            <h3 class="box-title">Hotel Booking</h3>
                        </div>
                        <div class="box-body">
                            <?php
                            echo validation_errors('<div class="error">', '</div>');
                            if($this->session->flashdata('message')){ 
                                echo '<b>Success!</b> ' . $this->session->flashdata('message');
                            }
                            ?>
                            <form action="<?php echo base_url()?>admin/search_hotel/hotel_services" method="POST" id="hotel_booking" name="hotel_booking" accept-charset="utf-8">
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label">CheckIn Date*:</label>
                                <div class="col-lg-6">
                                    <input type="text" id="datepicker" name="checkin_date" class="form-control checkin_date" placeholder="CheckIn Date"/>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label">CheckOut Date:</label>
                                <div class="col-lg-6">
                                    <input type="text" id="datepicker1" name="checkout_date" class="form-control checkout_date" placeholder="CheckOut Date"/>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label">Select City*:</label>
                                <div class="col-lg-6">
                                    <select id="city" name="city" class="form-control">
                                        <option value="">Select City</option>
                                        <?php foreach ($cityList as $key => $value) { 

                                            echo '<option value='.$value['city_id'].'>'.$value['name'].'</option>';
                                            ?>
                                          
                                        <?php } ?>
                                       
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label">Rooms and Guests*:</label>
                                <div class="col-lg-2">
                                    <label class="col-lg-1 control-label">Adults:</label>
                                    <input type="number" min="1" max="10" name="adults" class="form-control" >
                                </div>
                                <div class="col-lg-2">
                                    <label class="col-lg-1 control-label">Children:</label>
                                    <input type="number" min="0" max="3" name="children" class="form-control" >
                                </div>
                                <div class="col-lg-2">
                                    <label class="col-lg-1 control-label">Children Age:</label>
                                    <input type="number" min="1" max="12" name="children_age" class="form-control" >
                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <div class="col-lg-offset-4">
                                    <button class="btn btn-primary" id="search_hotels"  type="submit">Search Hotels</button>                                
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            <div class="col-sm-12">
                                <div id="next-pre" style="text-align: center;" class="form-group col-sm-6 col-md-6"></div>
                                <div id="return_next-pre" style="text-align: center;display: none" class="form-group col-sm-6 col-md-6"></div>
                            </div>
                            <div id="bus_services">
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </section>
</div>
    
<script>

$(document).ready(function(){ 

    var convertJSDate = function(changeDate){
        var dateParts = changeDate.split(/(\d{1,2})\/(\d{1,2})\/(\d{4})/);
        return dateParts[3] + "-" + dateParts[1] + "-" + dateParts[2];
    }

    

    $("#datepicker" ).datepicker({ numberOfMonths: 2,minDate: 0});
    $("#datepicker1" ).datepicker({ numberOfMonths: 2,minDate: 0});

    /**on click select seat hide show dropdown*/
    $(document).on('click','a[name=tab]',function(){    
        
        service_id=$(this).data("service-id");
        
        var $this= $(this);

        $this.toggleClass('selectseat2');
        if($this.hasClass('selectseat2')){
            $this.text('Select Seat');
        }else
        {
            $this.text('Hide Seat');
        }
        $("#dropdown_"+service_id).toggle();
        
    });

    /**on click select seat hide show dropdown*/

    /*bus service stop details start*/
    
    $(document).on('click','a[name=tab]',function(){
    var op_name=$(this).data("op-name");
    if(op_name =='MSRTC')
    {
        var service_id=$(this).data("service-id");
        var provider_id=$(this).data("provider-id");
        var date=$(this).data("date");
        var url = base_url +"admin/Search_bus/get_service_stop_details";
        var dataString = 'service_id='+ service_id + '&provider_id='+ provider_id +'&date='+ date;
    }else{
        $('.dropdown').css("display","block");
       var service_id=$(this).data("service-id");
       var provider_id=$(this).data("provider-id");
       var date=$(this).data("date");   
       var from = $(this).data("from"); 
       var to = $(this).data("to"); 
       var trip_id = $(this).data("trip-id");
       var inventory_type = $(this).data("inventory-type");
      /* var boarding_stop =
       var dropping_stop =*/
        var url = base_url +"admin/Search_bus/get_seat_layout";

        var dataString = 'service_id='+ service_id + '&provider_id='+ provider_id +'&date='+ date +'&from='+ from + '&to='+ to + '&trip_id='+trip_id + '&inventory_type=' + inventory_type + '&operator_name=' + op_name;
     

    }
    
    $.ajax({
        type:"POST",
        dataType: "json",
        url : url,
        data : dataString,
        success : function(data) { 
            
          
            if(data.from && data.to)  
            {
                var result = data.from; 
                var result1 = data.to;
                var row ="";
                row +="<option value='0'>Select Boarding Stop</option>";
                for(i=0; i < result.length; i++) { 
                row +="<option value='"+result[i].from_bus_stop_name+"'>"+result[i].from_bus_stop_name+" - "+result[i].from_time+"</option>";
                 }
               
                
                var row1 ="";
                row1 +="<option value='0'>Select Dropping Stop</option>";
                for(j=0; j < result1.length; j++) { 
                row1 +="<option value='"+result1[j].to_bus_stop_name+"'>"+result1[j].to_bus_stop_name+" - "+result1[j].to_time+"</option>";
                 }
                
               $("#from_stop_"+service_id).html(row);
               $("#to_stop_"+service_id).html(row1);
            } 
            else
            {
                if(op_name =='Travelyaari')
                {
                    var result = data.seats[0];
                }else
                {
                    var result = data.seats;
                }                                         
                var row = "";
                var row1= "";

                for(i=0; i < result.length; i++) {

                var ac_seat = (result[i].ac)?result[i].ac:"True";
                var ladies_seat = (result[i].ladies_seat)?result[i].ladies_seat:"false";
                var sleeper = (result[i].sleeper)?result[i].sleeper:"True";
                var berth = (result[i].berth)?result[i].berth:"0";
                var total_fare =  (result[i].total_fare)?(result[i].total_fare):(result[i].sub_total_fare);


                    if(result[i].available=='Y')
                        {var class_name ='available'; var Sstatus = 'S'}
                    else
                        {var class_name = 'booked'; var Sstatus = 'N'}
                        row += "<li><a href='javascript:void(0)' class="+class_name+" data-seat-no="+result[i].seat_no +" data-fare="+result[i].sub_total_fare+" data-total-fare="+total_fare+" title='Seat No"+ result[i].seat_no +" | Fare:Rs."+ result[i].total_fare+"' data-ac="+ac_seat+" data-sleeper="+sleeper+" data-ladies-seat="+ladies_seat+" data-berth="+berth+" >"+Sstatus+"</a></li>";
                    
                } 
                 $("#fare_tab_filter_id_"+service_id).html(row);
                 $('.seat_detail_info_'+service_id).css("display","block");
                
                if(op_name =='UPSRTC')
                {
                    var boarding_stops = data.boarding_stops;                 
                    var dropping_stops = data.Dropoffs;
                    var board_stops= '';
                    var drop_stops='';

                   // board_stops +="<option value='0'>Select Boarding Stop</option>";
                   // for(b=0; b < boarding_stops.length; b++) { 
                    board_stops +="<option value='"+from+"'>"+from+"</option>";
                    //}
                    $("#from_stop_"+service_id).html(board_stops);

                    //drop_stops +="<option value='0'>Select Dropping Stop</option>";
                    //for(d=0; d < dropping_stops.length; d++) { 
                    drop_stops +="<option value='"+to+"'>"+to+"</option>";
                    // }
                     $("#to_stop_"+service_id).html(drop_stops);
                  }
                  else
                  {
                     var boarding_stops = data.boarding_stops;                 
                     var dropping_stops = data.Dropoffs;
                     var board_stops= '';
                     var drop_stops='';

                    board_stops +="<option value='0'>Select Boarding Stop</option>";
                    for(b=0; b < boarding_stops.length; b++) { 
                    board_stops +="<option value='"+boarding_stops[b].PickupName+" - "+boarding_stops[b].PickupTime+"' data-bsd='"+boarding_stops[b].PickupCode+"' data-stop='"+boarding_stops[b].PickupName+" - "+boarding_stops[b].PickupTime+"'>"+boarding_stops[b].PickupName+" - "+boarding_stops[b].PickupTime+"</option>";
                     }
                     $("#from_stop_"+service_id).html(board_stops);

                    drop_stops +="<option value='0'>Select Dropping Stop</option>";
                    for(d=0; d < dropping_stops.length; d++) { 
                    drop_stops +="<option value='"+dropping_stops[d].DropoffName+" - "+dropping_stops[d].DropoffTime+"' data-dsd='"+dropping_stops[d].DropoffCode+"' data-stop='"+dropping_stops[d].DropoffName+" - "+dropping_stops[d].DropoffTime+"'>"+dropping_stops[d].DropoffName+" - "+dropping_stops[d].DropoffTime+"</option>";
                     }
                     $("#to_stop_"+service_id).html(drop_stops);
                  }

                
                


            }    
           
            
           
             
        },
        error : function(data) {
            // do something
        }
    });

});
    /*bus service stop details end*/

    /*validation for select dropdown*/
    $(document).on('click','.get_seat',function(){     
        service_id=$(this).data("service-id");

        var error = 0;
        var from_stop = $('#from_stop_'+service_id).val();
        var to_stop = $('#to_stop_'+service_id).val();
        
        if (from_stop == '0') {
            error = 1;
            alert('You should select a boarding stop.');
        }
        else if(to_stop == '0'){
            error =1 ;
            alert('You should select a dropping stop.');
        }

        if (error) {
            return false;
        } else {
            return true;
        }

    });

     /*validation for select dropdown*/

      /*seat layout start*/
    $(document).on('click','.get_seat',function(){ 
        var service_id=$(this).data("service-id");
        var provider_id=$(this).data("provider-id");
        var date=$(this).data("date");   
        var from = $("#from_stop_"+service_id).val();
        var to = $("#to_stop_"+service_id).val();
        var trip_id = $(this).data("trip-id");
        var inventory_type = $(this).data("inventory-type");
        var op_name=$(this).data("op-name");

        var url = base_url +"admin/Search_bus/get_seat_layout";

        var dataString = 'service_id='+ service_id + '&provider_id='+ provider_id +'&date='+ date +'&from='+ from + '&to='+ to + '&trip_id='+trip_id + '&inventory_type=' + inventory_type + '&operator_name=' + op_name;
     
   // {"service_id" : service_id, "provider_id" : provider_id, "date" : date, "from" : from, "to" : to, "trip_id" : trip_id, "inventory_type" : inventory_type},
        $.ajax({
        type:"POST",
        dataType: "json",
        url : url,
        data : dataString,
        success : function(data) { 
            var result = data.seats;
            var row = "";
            var row1= "";
            for(i=0; i < result.length; i++) {
                if(result[i].available=='Y')
                    {var class_name ='available'; var Sstatus = 'S'}
                else
                    { var class_name = 'booked'; var Sstatus = 'N'}

                var total_fare =  (result[i].total_fare)?(result[i].total_fare):(result[i].sub_total_fare);
                

                row += "<li><a href='javascript:void(0)' class="+class_name+" data-seat-no="+result[i].seat_no +" data-fare="+result[i].sub_total_fare+" data-total-fare="+total_fare+" title='Seat No"+ result[i].seat_no +" | Fare:Rs."+ result[i].total_fare+"' data-ac="+result[i].ac+" data-sleeper="+result[i].sleeper+" data-ladies-seat="+result[i].ladies_seat+" data-berth="+result[i].berth+" >"+Sstatus+"</a></li>";


                  /*  row += "<li><a href='javascript:void(0)' class="+class_name+" data-trip-id="+trip_id+" data-seat-no="+result[i].seat_no +" data-fare="+result[i].total_fare+" title=Seat No"+result[i].seat_no+'| Fare: Rs.'+result[i].total_fare+'c*r:'+ result[i].pos +" data-berth="+result[i].berth+" data-ac="+result[i].ac+" data-sleeper="+result[i].sleeper+" data-ladies-seat="+result[i].ladies_seat+" >"+Sstatus+"</a></li>";*/
                
            } 
             $("#fare_tab_filter_id_"+service_id).html(row);
             $('.seat_detail_info_'+service_id).css("display","block");
                
             
                
           
        },
        error : function(data) {
            // do something
        }
        });


    });
     /*seat layout start*/

     /* block seat code*/
     var count_seats = [];
     var count_gender_chk = []; 
     var count_gender = [];
     var count_sleeper = [];
     var count_ac=[];
     var count_birth = [];
     var count_data_fare = [];

     $('body').on('click', ".available", function(){

        var curtSeatFare = parseInt($('#get_fare_id').text()); // get total fare value
        var curtTotalFare = parseInt($('#total_fare').text()); // get total fare value

        var data_seat_no = $(this).attr('data-seat-no'); // get set no

        var data_fare = $(this).attr('data-fare'); // get fare value;
        var total_fare = $(this).attr('data-total-fare'); // get fare value;

        var gender = $(this).attr('data-ladies-seat');
        var sleeper = $(this).attr('data-sleeper');
        var ac = $(this).attr('data-ac');
        var birth = $(this).attr('data-berth');  

        count_seats.push( $(this).data('seat-no') );

         if(gender=='false')
         {
            var gender_chk = 'M';
         }
         else
         {
            var gender_chk = 'F';
         }

        SumOfSeatFare = parseInt(data_fare) + curtSeatFare; // addition of seat fare value
        SumOfTotalFare = parseInt(total_fare) + curtTotalFare; // addition of titol fare value

       

        
        //$(".sel_seat").append(data_seat_no+',');
        $(".sel_seat").html(count_seats.join(","));
        $(".fare").html(SumOfSeatFare); // set fare value 
        $(".tot_fare").html(SumOfTotalFare); // set fare value 
        $(".gender").append(gender_chk+',');
        $(".is_ladies").append(gender+',');
        $(".sleeper").append(sleeper+',');
        $(".ac").append(ac+','); 
        $(".birth").append(birth+',');
        $(".seat_fare").append(data_fare+',');
        $(".totalFare").append(total_fare+',');



        $('[data-seat-no='+data_seat_no+']').addClass('selected');
        $('[data-seat-no='+data_seat_no+']').removeClass('available');
      
    
    });

     $('body').on('click', ".booked", function(){
        alert('Seat already booked Try another seat.');
    
    });

     $('body').on('click', ".selected", function(){
     //var add_class = $(this).attr('class') ;
     
     var removeItem = 31;



     var data_seat_no = $(this).attr('data-seat-no'); // get set no
     /*//y.splice( $.inArray(removeItem,y) ,1 );
     data_seat_no.splice( $.inArray(removeItem,data_seat_no) ,1 );

     alert(data_seat_no);*/

     var data_fare = $(this).attr('data-fare'); // get fare value;
     
     $('[data-seat-no='+data_seat_no+']').removeClass('selected');
     $('[data-seat-no='+data_seat_no+']').addClass('available');

     
    
     var seat_nos = $("#get_seat_no").text();
     
     var get_total_fare = parseInt($('#total_fare').text()); 

     var bal_fare = get_total_fare - parseInt(data_fare);

    $(".fare").html(bal_fare); // set fare value 
    $(".tot_fare").html(bal_fare); // set fare value  

     $("#get_seat_no").empty();
     $("#gender").empty(); 
         $("#is_ladies").empty();
         $("#sleeper").empty();
         $("#ac").empty();
    
    });


      /* block seat code*/

      /***/

      $('body').on('change', 'select.boarding', function() {
        var selectedStopCode = $(this).find(':selected').data('bsd');
        var selectedStopName = $(this).find(':selected').data('stop');

        $(".from_b_stop").html(selectedStopCode);
        $(".from_stop_name").html(selectedStopName);
    });


        

        $('body').on('change', 'select.dropping', function() {        
        var selectedStopCode = $(this).find(':selected').data('dsd');  
        var selectedStopName = $(this).find(':selected').data('stop');    

        $(".to_d_stop").html(selectedStopCode);
        $(".to_stop_name").html(selectedStopName);
        });
        
      /***/

      /*temp booking on click continue*/
      $('body').on('click', ".continue", function(){
        var button_val = $(this).val();
        
        event.preventDefault();
    var form_url = base_url +"admin/Search_bus/save_temp_booking";

    /*var provider_id = $(this).attr('data-provider-id'); */
    var seat_no = $("#get_seat_no").text();
    var sub_total = $('#get_fare_id').text(); 
    var total_fare = $('#total_fare').text(); 
    var birth = $('#birth').text();
    var gender_list = $('#gender').text();
    var is_ladies_list = $('#is_ladies').text();
    var sleeper = $('#sleeper').text();
    var is_ac= $('#ac').text();
   // var seat_info= $('#seat_details1').text();
    var sum_of_seat_fare= $('#seat_fare').text();
    var sum_of_total_fare= $('#totalFare').text();
    var data_bsd = $('#from_b_stop').text(); // get boarding stop
    var data_dsd = $('#to_d_stop').text(); // get dropping stop
    var from_stop_name = $('#from_stop_name').text(); // get dropping stop
    var to_stop_name = $('#to_stop_name').text(); // get dropping stop

    var checkout_date = $("#checkout_date_shown").text();
    $.ajax({
        type:"POST",
        dataType: "json",
        url : form_url,
        data : {"seat_no":seat_no,'birth':birth,'gender':gender_list,'is_ladies':is_ladies_list,"sleeper":sleeper,"ac":is_ac, "boarding_stop":data_bsd,"dropping_stop":data_dsd,"sum_of_seat_fare":sum_of_seat_fare,"sum_of_total_fare":sum_of_total_fare,"from_stop_name":from_stop_name,"to_stop_name":to_stop_name},
        success : function(data) { 

          
           window.location.href = data.redirect;

        },
        error : function(data) {
            // do something
        }
    });

    });
    
});




</script>
