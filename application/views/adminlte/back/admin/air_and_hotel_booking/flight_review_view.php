<div id="divLoading" style="margin: 0px; padding: 0px; position: fixed; right: 0px; top: 0px; width: 100%; height: 100%; background-color: rgb(102, 102, 102); z-index: 30001; opacity: 0.8;display: none">
    <p style="position: absolute; color: White; top: 38%; left: 45%;">
        Processing, please wait...
        <img src="http://loadinggif.com/images/image-selection/3.gif">
    </p>
</div>
<!-- Inner Wrapper Start -->
<?php if (!isset($reviewDetails['err'])): 
?>    <div id="page-wrapper">

    <!-- Signin Content Start -->
    <section class="inner-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3><i class="soap-icon-plane font30 purple icon-plane"></i> Review Itinerary</h3>
                    <?php
                    foreach ($reviewDetails['flights'] as $key => $flightDetails) {
                        $flyTime = $flightDetails['flyTime'];
                        $hours = floor($flyTime / 60);
                        $min = $flyTime - ($hours * 60);
                        $duration = $hours . "h" . $min . "m";
                        ?>   
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="col-md-12 col-sm-12 container-c">
                                    <i class="soap-icon-plane-right font30 purple icon-plane"></i> <strong><?php echo $flightDetails['depDetail']['name'] ?> to <?php echo $flightDetails['arrDetail']['name'] ?></strong> <?php echo date('H:i', strtotime($flightDetails['depDetail']['time'])) ?>| <?php echo $duration; ?> <span class="stops">0 Stops</span>
                                    <div class="row box-grey">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
                                                <span class="name"><?php echo $flightDetails['depDetail']['code'] ?></span> <span class="city"><?php echo $flightDetails['depDetail']['name'] ?></span> <br/>
                                                <span class="route"><?php echo $flightDetails['depDetail']['country'] ?></span> <br/>
                                                <span class="terminal"><strong>Terminal</strong> : <?php echo $flightDetails['depDetail']['terminal'] ?></span> <br/>
                                                <span class="time"><?php echo date('H:i', strtotime($flightDetails['depDetail']['time'])) ?></span>
                                            </div>
                                            <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
                                                <!--  <div class="goairlogo" style="padding-bottom:10px;">
                                                      <img src="images/go-air-logo.png"/>
                                                  </div>  -->
                                                <div class="airdet">
                                                    <div class="name"><?php echo $flightDetails['carrier']['name'] ?></div>
                                                    <div class="fltNum"><?php echo $flightDetails['carrier']['name'] . '-' . $flightDetails['flightNo']; ?></div>
                                                    <div class="txt-p"><?php echo $duration; ?></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-12 col-sm-4 col-xs-4">
                                                <span class="name"><?php echo $flightDetails['arrDetail']['code'] ?></span> <span class="city"><?php echo $flightDetails['arrDetail']['name'] ?></span> <br/>
                                                <span class="route"><?php echo $flightDetails['arrDetail']['country'] ?></span> <br/>
                                                <span class="time"><?php echo date('H:i', strtotime($flightDetails['arrDetail']['time'])) ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                    }
                    ?>   
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h3><i class="soap-icon-user font30 purple"></i> Travellers' Details</h3>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-12 col-sm-12 container-c">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">Name</div>
                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">Type</div>
                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">DOB</div>
                                            <!--<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">Passport</div>
                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">Frequent Flier No.</div> -->
                                        </div>
                                <?php foreach ($deliveryData['adt_title'] as $key => $travellerDetails) { ?>
                                    <div class="row box-grey traveller-details">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"><span class="name"><strong><?php echo $travellerDetails . ' ' . $deliveryData['adt_firstname'][$key] . ' ' . $deliveryData['adt_lastname'][$key]; ?></strong></span></div>
                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">Adult</div>
                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"><?php echo $deliveryData['adt_dob'][$key] ?></div>
                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
                                            <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
                                        </div>

                                    </div>
                                <?php } ?>

                                <?php
                                if (!empty($deliveryData['chd_title'])) {
                                    foreach ($deliveryData['chd_title'] as $key => $travellerDetails) {
                                        ?>
                                        <div class="row box-grey traveller-details">

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"><span class="name"><strong><?php echo $travellerDetails . ' ' . $deliveryData['chd_firstname'][$key] . ' ' . $deliveryData['chd_lastname'][$key]; ?></strong></span></div>
                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">Child</div>
                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"><?php echo $deliveryData['chd_dob'][$key] ?></div>
                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
                                            </div>

                                        </div>
                                    <?php }
                                }
                                ?>

                                <?php
                                if (!empty($deliveryData['inf_title'])) {
                                    foreach ($deliveryData['inf_title'] as $key => $travellerDetails) {
                                        ?>
                                        <div class="row box-grey traveller-details">

                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"><span class="name"><strong><?php echo $travellerDetails . ' ' . $deliveryData['inf_firstname'][$key] . ' ' . $deliveryData['inf_lastname'][$key]; ?></strong></span></div>
                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">Infant</div>
                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"><?php echo $deliveryData['inf_dob'][$key] ?></div>
                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
                                                <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
                                            </div>

                                        </div>
                                    <?php }
                                }
                                ?>


<?php if (isset($deliveryData)) { ?>
                                    <div class="traveller-contact">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><span class="name"><i class="soap-icon-user purple"></i> <strong>Contact Details</strong></span></div>
                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">Email: <span class="name"><strong><?php echo $deliveryData['email'] ?></strong></span></div>
                                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">Mobile:  <span class="name"><strong><?php echo $deliveryData['mobile_number'] ?></strong></span></div>
                                            </div>
                                        </div>
                                    </div>
<?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-12 col-sm-12 container-c">
                                <div class="row box-grey">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                            Amount to be paid<br/>
                                            <div class="fltpricediv">
                                                <div class="fltprice txt-p"><i class="fa fa-rupee"></i>
                                                    <?php
                                                    echo $amountToCharge;
                                                    ?></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                            <a href="<?php echo base_url() ?>admin/search_flight/flight_booking_form"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Details</a>
                                        </div>

                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                            <span style="display:none;" id="loader"><img src="<?php echo base_url() . BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/><br><br><span>Loading... Please Wait</span></span>
                                            <button type="button" class="btn btn-purple btn-lg btn-block login-button txt-w" id="make_payment" name="make_payment"><i class="soap-icon-settings"></i> Make Payment<br/> <small>100% Secure and Easy to Use</small></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Signin Content End -->             
</div>

<?php  endif;  ?>

<script type="text/javascript">
    $(document).on('click', '#make_payment', function() {
        var url = base_url + "admin/search_flight/flight_booking";
        //$('#loader').show();
        $('#divLoading').css('display', 'block');
        console.log(url);
        $.ajax({
            url: url,
            dataType: "text",
            data: {},
            success: function (data) {
                data = data.trim();

                //if(data=='NotBooked'){
                // $('#loader').hide();
                $('#divLoading').css('display', 'none');
                //alert("Error occurred during booking, please try again");
                // return ;
                //}
                window.location=base_url +"admin/dashboard";
                //console.log(data);
            }, complete: function(){
                //           $('#loader').hide();
                $('#divLoading').css('display', 'none');
            }
        });
    });
</script>
