<?php
$attributes = array("method" => "POST", "id" => "customer_info", "name" => "customer_info");
echo form_open(site_url() . '/admin/search_flight/flight_review', $attributes);
?>
<div id="page-wrapper">
    <!-- Signin Content Start -->
    <section class="inner-wrapper" id="captcha">
        <div class="container">

            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <h3 class="text-center purple">Book Flight</h3>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-12 col-sm-12 container-c">
                                <fieldset class="group-border">
                                    <legend class="group-border"><strong>Adult Traveller's Details</strong></legend>
                                    <?php
                                    $passengerDetails = json_decode($_SESSION['paxCount']);
                                    $passengerDetails->adt;
                                    for ($i = 0; $i < $passengerDetails->adt; $i++) {
                                        ?>
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <div class="form-group">
                                                    <label class="control-label" for="title">Title<span class="asteriskField">*</span></label>
                                                    <select class="form-control adt_title title_input"  name="adt_title[]<?php echo $i; ?>" id="adt_title<?php echo $i; ?>" value=""/>
                                                    <option value="none" selected>Title</option>
                                                    <option value="Mr">Mr</option>
                                                    <option value="Mrs">Mrs</option>
                                                    <option value="Miss">Miss</option>
                                                    <option value="Ms">Ms</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-5">
                                                <div class="form-group">
                                                    <label class="control-label" for="FirstName">First Name / Given Name<span class="asteriskField">*</span></label>
                                                    <input class="form-control input-sm adt_firstname fname_input" id="adt_firstname<?php echo $i; ?>" name="adt_firstname[]<?php echo $i; ?>" maxlength="20" placeholder="First Name" type="text" value="">
                                                </div>
                                            </div>
                                            <div class="col-lg-5">
                                                <div class="form-group">
                                                    <label class="control-label" for="LastName">Last Name / Surname<span class="asteriskField">*</span></label>
                                                    <input class="form-control input-sm adt_lastname lname_input" id="adt_lastname<?php echo $i; ?>" name="adt_lastname[]<?php echo $i; ?>"  maxlength="20" placeholder="Last Name" type="text" value="">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-1">
                                                <div class="form-group">
                                                    <label class="control-label" for="Age">Age<span class="asteriskField">*</span></label>
                                                    <input class="form-control input-sm adt_age age_input" id="adt_age<?php echo $i; ?>" name="adt_age[]<?php echo $i; ?>" placeholder="Age" type="text" value=""  maxlength="3">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="control-label" for="DateOfBirth">Date of Birth<span class="asteriskField">*</span></label>
                                                    <input class="form-control input-sm adt_dob dob_input" id="adt_dob<?php echo $i; ?>" name="adt_dob[]<?php echo $i; ?>" placeholder="Date of Birth" type="text" value="">
                                                </div>
                                            </div>
                                        </div> 

                                        <hr style="border-bottom: 1px solid #e5e5e5">

                                    <?php } ?>    

                                    <!-- childer code start here -->

                                    <?php
                                    if ($passengerDetails->chd > 0):
                                        ?>
                                        <legend class="group-border"><strong>Child Details </strong></legend>                           
                                        <?php
                                        for ($i = 0; $i < $passengerDetails->chd; $i++) {
                                            ?>

                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <label class="control-label" for="title">Title<span class="asteriskField">*</span></label>
                                                        <select class="form-control title_input"  name="chd_title[]<?php echo $i; ?>" id="chd_title<?php echo $i; ?>" value=""/>
                                                        <option value="none" selected>Title</option>
                                                        <option value="Mstr">Mstr</option>
                                                        <option value="Miss">Miss</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-5">
                                                    <div class="form-group">
                                                        <label class="control-label" for="FirstName">First Name / Given Name<span class="asteriskField">*</span></label>
                                                        <input autocomplete="off" class="form-control input-sm fname_input" id="chd_firstname<?php echo $i; ?>"  maxlength="20" name="chd_firstname[]<?php echo $i; ?>" placeholder="First Name" type="text" value="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-5">
                                                    <div class="form-group">
                                                        <label class="control-label" for="LastName">Last Name / Surname<span class="asteriskField">*</span></label>
                                                        <input autocomplete="off" class="form-control input-sm lname_input" id="chd_lastname<?php echo $i; ?>"  maxlength="20" name="chd_lastname[]<?php echo $i; ?>" placeholder="Last Name" type="text" value="">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-1">
                                                    <div class="form-group">
                                                        <label class="control-label" for="Age">Age<span class="asteriskField">*</span></label>
                                                        <input autocomplete="off"  class="form-control input-sm age_input" id="chd_age<?php echo $i; ?>" name="chd_age[]<?php echo $i; ?>" placeholder="Age" type="text" value=""  maxlength="3">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label class="control-label" for="DateOfBirth">Date of Birth<span class="asteriskField">*</span></label>
                                                        <input autocomplete="off" class="form-control input-sm chd_dob dob_input" id="chd_dob<?php echo $i; ?>" name="chd_dob[]<?php echo $i; ?>" placeholder="Date of Birth" type="text" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <hr style="border-bottom: 1px solid #e5e5e5">

                                        <?php }endif; ?> 


                                    <?php
                                    ///  $passengerDetails->inf=1 ;
                                    if ($passengerDetails->inf > 0):
                                        ?>
                                        <legend class="group-border"><strong>Infant Details Below 2 Years</strong></legend>                          
                                        <?php for ($i = 0; $i < $passengerDetails->inf; $i++) {
                                            ?>
                                            <div class="row"><div class="col-lg-2">
                                                    <div class="form-group">
                                                        <label class="control-label" for="title">Title<span class="asteriskField">*</span></label>
                                                        <select class="form-control title_input"  name="inf_title[]<?php echo $i; ?>" id="inf_title<?php echo $i; ?>" value=""/>
                                                        <option value="none" selected>Title</option>
                                                        <option value="Miss">Miss</option>
                                                        <option value="Mstr">Mstr</option>
                                                        </select>
                                                    </div>

                                                </div>

                                                <div class="col-lg-5">
                                                    <div class="form-group">
                                                        <label class="control-label" for="FirstName">First Name / Given Name<span class="asteriskField">*</span></label>
                                                        <input class="form-control input-sm fname_input" id="inf_firstname<?php echo $i; ?>" name="inf_firstname[]<?php echo $i; ?>" placeholder="First Name" type="text" value="" maxlength="20">
                                                    </div>
                                                </div>
                                                <div class="col-lg-5">
                                                    <div class="form-group">
                                                        <label class="control-label" for="LastName">Last Name / Surname<span class="asteriskField">*</span></label>
                                                        <input autocomplete="off" class="form-control input-sm lname_input" id="inf_lastname<?php echo $i; ?>" name="inf_lastname[]<?php echo $i; ?>" placeholder="Last Name" type="text" value="" maxlength="20">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-1">
                                                    <div class="form-group">
                                                        <label class="control-label" for="Age">Age<span class="asteriskField">*</span></label>
                                                        <input autocomplete="off" class="form-control input-sm age_input" id="inf_age<?php echo $i; ?>" name="inf_age[]<?php echo $i; ?>" placeholder="Age" type="text" value="" maxlength="3">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label class="control-label" for="DateOfBirth">Date of Birth<span class="asteriskField">*</span></label>
                                                        <input autocomplete="off" class="form-control input-sm inf_dob dob_input" id="inf_dob<?php echo $i; ?>" name="inf_dob[]<?php echo $i; ?>" placeholder="Date of Birth" type="text" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <hr style="border-bottom: 1px solid #e5e5e5">

                                        <?php }endif; ?>    


                                    <div class="row">                                    
                                        <fieldset>
                                            <legend><strong>Delivery Information</strong></legend>
                                            <div class="form-group">
                                                <div class="col-sm-6">
                                                    <label for="mobile">Mobile<span class="asteriskField">*</span></label>
                                                    <input autocomplete="off" class="form-control input-sm mobile_input" id="mobile_number" name="mobile_number" placeholder="Mobile" type="text" value="" maxlength="10">
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="email">Email<span class="asteriskField">*</span></label>
                                                    <input autocomplete="off" class="form-control input-sm email_input" id="email" name="email" placeholder="Email" type="text" value="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <label for="country">Country<span class="asteriskField">*</span></label>
                                                    <!--<input autocomplete="off" class="form-control country_input" id="country" name="country" size="30" type="text" />-->
                                                    <select class="form-control country_input" name="country" id="country"/>
                                                    <option value="">Select Country</option>
                                                    <?php
                                                    foreach ($country as $row => $value) {
                                                        if (strtolower($value['stCountryName']) == 'india') {
                                                            $select = 'selected';
                                                        }
                                                        echo "<option value='" . $value['intCountryId'] . "' $select>" . $value['stCountryName'] . "</option>";
                                                    }
                                                    ?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label for="state">State<span class="asteriskField">*</span></label>
                                            <!--          <input autocomplete="off" class="form-control state_input" id="state" name="state" size="30" type="text" />-->
                                                    <select class="state form-control state_input" name="state" id="state"/>
                                                    <option value="">Select State</option>
                                                    <?php
                                                    foreach ($states as $row => $value) {
                                                        echo "<option value='" . $value['intStateId'] . "'>" . $value['stState'] . "</option>";
                                                    }
                                                    ?>
                                                    </select>
                                                    <input type="hidden"  name="country_element" id="country_element">
                                                    <input type="hidden"  name="state_element" id="state_element">
                                                    <input type="hidden"  name="city_element" id="city_element">

                                                </div>
                                                <div class="col-sm-4">
                                                    <label for="city">City<span class="asteriskField">*</span></label>
                                     <!--                 <input autocomplete="off" class="form-control city_input" id="city" name="city" size="30" type="text" />-->
                                                    <select class="agent_city form-control city_input" name="city" id="city"/>
                                                    <option value="">Select City</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-6">
                                                    <label for="street">Street<span class="asteriskField">*</span></label>
                                                    <input autocomplete="off" class="form-control street_input" id="street" name="street" size="30" maxlength="100" type="text" />
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="pincode">Pincode<span class="asteriskField">*</span></label>
                                                    <input autocomplete="off" class="form-control pincode_input" id="pincode" name="pincode" size="30" maxlength="6" type="text" />
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>                                  



                                    <!--  -->
                                    <div class="row" style="margin-top:12px;">
                                        <div class="col-sm-4 col-lg-offset-4 col-sm-offset-4 col-xs-offset-0">
                                            <div class="form-group">
                                                <button  id="customer_info_sub" class="btn btn-purple btn-lg btn-block login-button txt-w" type="submit">Proceed to Booking</button>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>

                            </div>
                        </div>
                    </div>
                    <?php echo validation_errors('<div class="error">', '</div>'); ?>
                    </form> 
                    <!--dsad-->
                </div>
            </div>
        </div>
    </section>
    <!-- Signin Content End -->             
</div>
</form>
<script type="text/javascript">
    $('form#customer_info').on('submit', function(event) {
        //Add validation rule for dynamically generated name fields
    
        $('.fname_input').each(function() {
            $(this).rules("add",
            {
                required: true,
                lettersonly: true,
                messages: {
                    required: "Please enter first name",
                    lettersonly : "Please enter valid first name"
                }
            });
        });
    
        $('.lname_input').each(function() {
            $(this).rules("add",
            {
                required: true,
                lettersonly: true,
                messages: {
                    required: "Please enter last name",
                    lettersonly : "Please enter valid last name"
                }
            });
        });
        $('.age_input').each(function() {
            $(this).rules("add",
            {
                required: true,
                digits: true,
                min:1,
                messages: {
                    required: "Please enter age",
                    digits : "Please enter valid age",
                    min:"Minimum age should be 1"
                }
            });
        });
        $('.dob_input').each(function() {
            $(this).rules("add",
            {
                required: true,
                messages: {
                    required: "Please select date of birth"
                }
            });
        });
        $('.mobile_input').each(function() {
            $(this).rules("add",
            {
                required: true,
                minlength: 10,
                maxlength: 10,
                number: true,
                valid_mobile_no: true,
                mobileno_start_with:true,
                messages: {
                    required: "Please Enter Mobile No",
                    number: "Please Enter Valid Number",
                    valid_mobile_no: "Please enter valid mobile number",
                    mobileno_start_with:"Mobile Number should start with 6 - 9",
                    minlength: "Please Enter 10 Digit Mobile No",
                    maxlength: "Please Enter 10 Digit Mobile No"
                }
            });
        });
    
        $('.pincode_input').each(function() {
            $(this).rules("add",
            {
                required: true,
                digits: true,
                minlength: 6,
                maxlength: 6,
                pincode_start_with: true,
                messages: {
                    required: "Please Enter Pincode",
                    digits: "Please Enter Valid Pincode",
                    minlength: "Please Enter 6 Digit Pincode",
                    maxlength: "Please Enter 6 Digit Pincode",
                    pincode_start_with: "Pin Code Should Start Between 1 and 9"
                }
            });
        });
        $('.street_input').each(function() {
            $(this).rules("add",
            {
                required: true,
                letterswithspecialchardigit:true,
                messages: {
                    required: "Please Enter Street",
                    letterswithspecialchardigit:"Please Enter Valid Street"
                }
            });
        });
        $('.email_input').each(function() {
            $(this).rules("add",
            {
                required: true,
                email: true,
                messages: {
                    required: "Please Enter Email Address",
                    email: "Please Enter Valid Email Address"
                }
            });
        });
        $('.country_input').each(function() {
            $(this).rules("add",
            {
                required: true,
                messages: {
                    required: "Please select country"
                }
            });
        });
        $('.state_input').each(function() {
            $(this).rules("add",
            {
                required: true,
                messages: {
                    required: "Please select state"
                }
            });
        });
        $('.city_input').each(function() {
            $(this).rules("add",
            {
                required: true,                
                messages: {
                    required: "Please select city"
                }
            });
        });
        $('.title_input').each(function() {
            $(this).rules("add",
            {
                notNone: true               
            });
        });
 
    });
    $("#customer_info").validate();
    $(document).ready(function () {
        jQuery.validator.addMethod("lettersonly", function (value, element)
        {
            return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
        }, "");

        jQuery.validator.addMethod("letterswithdigit", function (value, element)
        {
            return this.optional(element) || /^[a-zA-Z0-9\s]+$/i.test(value);
        }, "");

        $.validator.addMethod("letterswithspecialchardigit", function (value, element)
        {
            return this.optional(element) || /^[a-zA-Z0-9.,@#&_\-\s]+$/i.test(value);
        }, "");
    
        $.validator.addMethod("valid_mobile_no", function (value, element) {
            return (value == "0000000000" || value == "9999999999") ? false : true;
        }, 'Please enter valid mobile number');
        
        $.validator.addMethod("mobileno_start_with", function (value, element) {
            var regex = new RegExp("^[6-9]{1}[0-9]{9}$");
            if (value.length > 0)
            {
                if (regex.test(value))
                {
                    return true;
                } else {
                    return false;
                }
            } else
            {
                return true;
            }
        }, 'Mobile No. should start with 6 - 9 ');

        $.validator.addMethod("pincode_start_with", function (value, element) {
            var regex = new RegExp("^[1-9]{1}[0-9]{5}$");
            if (value.length > 0)
            {
                if (regex.test(value))
                {
                    return true;
                } else {
                    return false;
                }
            } else
            {
                return true;
            }
        }, 'PIN Code. should start with 1-9 ');
        
        $.validator.addMethod('notNone', function(value, element) {
            return (value != 'none');
        }, 'Please select title');
        
        /******** DATE OF BIRTH DATEPICKER ********/
        var dt = new Date();
        dt.setFullYear(new Date().getFullYear() - 18);
        var endDate = dt;
        $(".adt_dob").datepicker({
            changeMonth: true,
            changeYear: true,
            showMonthAfterYear: true,
            //            yearRange: "-100:-18",
            yearRange: "-100:+0",
            dateFormat: "dd-mm-yy",
        });
        $('.adt_dob').keydown(function (event) {
            event.preventDefault();    
        });
        var dt = new Date();
        dt.setFullYear(new Date().getFullYear() - 18);
        var endDate = dt;
        $(".chd_dob").datepicker({
            changeMonth: true,
            changeYear: true,
            showMonthAfterYear: true,
            //            yearRange: "-100:-18",
           yearRange: "2007:2017",
            dateFormat: "dd-mm-yy"
        });
        $('.chd_dob').keydown(function (event) {
            event.preventDefault();    
        });
        var dt = new Date();
        dt.setFullYear(new Date().getFullYear() - 18);
        var endDate = dt;
        $(".inf_dob").datepicker({
            changeMonth: true,
            changeYear: true,
            showMonthAfterYear: true,
            yearRange: "2017:2019",
//            yearRange: "-100:+0",

            dateFormat: "dd-mm-yy"
        });
        $('.inf_dob').keydown(function (event) {
            event.preventDefault();    
        });
        
        $(document).on('change', '#city', function (e)
        {
            $("#country_element").val($("#country :selected").text());
            $("#city_element").val($("#city :selected").text());
            
        });
        /*************STATE-CITY DROPDOWN **********/
        $(document).off('change', '.state').on('change', '.state', function (e)
        {
            $("#city").html("<option value=''>Please wait..</option>");

            var detail = {};
            var div = '';
            var str = "";
            var form = '';
            var ajax_url = 'admin/login/get_statewise_city';
			
            $("#state_element").val($("#state :selected").text());


            detail['state_id'] = $("#state").find('option:selected').attr('value');

            if (detail['state_id'] != '')
            {
                $("#city").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
                get_data(ajax_url, form, div, detail, function (response)
                {
                    if (response.city.length != 0)
                    {
                        $("#city").html("<option value=''>Select City</option>");
                        $.each(response.city, function (i, value)
                        {
                            $("#city").append("<option value=" + value.intCityId + ">" + value.stCity + "</option>");
                            $(".chosen_select").trigger("chosen:updated");
                        });
                    }
                    else
                    {
                        $("#city").html("<option value=''>Select City</option>").trigger('chosen:updated');
                    }
                }, '', false);
            }
            else
            {
                $("#city").html("<option value=''>Select City</option>").trigger('chosen:updated');
            }
        });

    });
</script>
