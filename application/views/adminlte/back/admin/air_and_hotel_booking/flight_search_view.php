<div id="divLoading" style="margin: 0px; padding: 0px; position: fixed; right: 0px; top: 0px; width: 100%; height: 100%; background-color: rgb(102, 102, 102); z-index: 30001; opacity: 0.8;display: none">
    <p style="position: absolute; color: White; top: 38%; left: 45%;">
        Loading, please wait...
        <img src="http://loadinggif.com/images/image-selection/3.gif">
    </p>
</div>
<!--Inner Wrapper Start -->
 <!-- <link href="<?php echo base_url() . BACK_ASSETS ?>css/search_style/style.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/qrcode.js" type="text/javascript"></script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/jquery.qrcode.js" type="text/javascript"></script> 
<link href="<?php echo base_url() . BACK_ASSETS ?>css/search_style/bootstrap.css" rel="stylesheet" type="text/css" />
 <link href="<?php echo base_url() . BACK_ASSETS ?>css/search_style/bootstrap.min.css" rel="stylesheet" type="text/css" />  -->
 <style type="text/css">
   .airCode {
    right: 10px;
}
 .airCode {
    top: -4px;
    right: 70px;
    position: absolute;
    background-color: #19171708;
    text-align: center;
    padding: 0 !important;
    color: #666;
    font-weight: 400;
    width: 100px;
    height: 50px;    
}
 </style>
    <div id="page-wrapper">
            
              <!-- Signin Content Start -->
              <section class="inner-wrapper">
                  <div class="container">
                    <div class="row">
                          <div class="col-md-12">
                              <?php
                    if (!empty($this->session->flashdata('item'))) {
                                  $message = $this->session->flashdata('item');
                                  //var_dump($message); die();
                                  ?>
                              <div id="alertMsgDangar" class="<?php echo $message['class']; ?>" style="text-align: center;"><h3><strong><?php echo $message['message']; ?></h3></strong></div>
                                <?php 
                                }
                                ?>
                              <h3 class="text-center purple">Flight Booking</h3>
                                <div class="panel panel-default">
                    <div class="panel-body">
                                        <div class="col-md-12 col-sm-12 container-c">
                                          <div class="formBox">
                                    <form action="<?php echo base_url() ?>admin/search_flight/flight_services" method="POST"  name="bus_booking" accept-charset="utf-8" id="search_services" autocomplete="off">
                                                  <div class="row gender disp_radio">
                                                        <div class="col-sm-6 col-xs-6">
                                     <div class="checkbox checkbox-info checkbox-circle pull-right">
                                                                <input id="checkBox7" class="checkbox-circle gender checkBox7check" type="radio" name="triptype" value="single" checked>
                                                                <label for="checkBox7" class="checkBox7check">Single Trip</label>
                                                            </div>
                                                        </div>
                            
                                                        <div class="col-sm-6 col-xs-6 checkBox8check">
                                                            <div class="inputBox">
                                                                <div class="checkbox checkbox-info checkbox-circle checkBox8check">
                                                                <input id="checkBox8" class="checkbox-circle gender checkBox8check" type="radio" value="round" name="triptype">
                                                                <label for="checkBox8" class="checkBox8check">Round Trip</label>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>                                
                                                    <div class="row col-lg-offset-1 col-md-offset-1">
                                                        <div class="col-sm-5 col-xs-12">
                                                          <div class="inputBox">
                                                              <div class="inputText">From</div>
                                                              <input name="source_input" id="source_input" class="input" type="text" value="" autocomplete="off">  
                                                              <input name="source" id="source" class="input" type="hidden">  
                                                              <input name="source_value" id="source_value" class="airCode" value="" readonly="true">                                                                  
                                                              <label id="from-error" class="error" for="from"></label>
                                                    <div class="error" id="erFromStop"></div>
                                                              <?php echo form_error('source', '<div class="error">', '</div>'); ?>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-sm-1 col-xs-12">
                                                          <div class="form-group">
<!--                                                            <img src="<?php echo base_url(); ?>assets/travelo/front/images/icon_reverse.png" class="img-responsive center-block cursor-pointer switch_cities" alt="Switch Cities" title="Switch Cities1" width="32" height="33" style="cursor: pointer; " />-->
                                                          </div>
                                                        </div>
                            
                                                        <div class="col-sm-5 col-xs-12">
                                                          <div class="inputBox">
                                                                <div class="inputText">To</div>
                                                                
                                                                <input name="destination_input" id="destination_input" class="input" type="text" autocomplete="off" value="" >

                                                                <input name="destination_value" id="destination_value" class="airCode" value="" readonly="true"> 
                                                                 <input name="destination" id="destination" class="input" type="hidden">

                                                                <label id="to-error" class="error" for="to"></label>
                                                                <div class="error" id="erToStop"></div>
                                                                <?php echo form_error('destination', '<div class="error">', '</div>'); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                            
                                                    <div class="row col-lg-offset-1 col-md-offset-1">
                                                        <div class="col-sm-5 col-xs-12">
                                                          <div class="inputBox cinputBox">
                                                                <div class="inputText">Departure Date</div>
                                                                <input id="departure_date"  name="departure_date" class="input" type="text" readonly>
                                                                <div class="error" id="erDatepicker"></div>
                                                                 <?php echo form_error('departure_date', '<div class="error">', '</div>'); ?>
                                                            </div>
                                                          <!--<div class="form-group input-group inputBox focus">
                                                                <input class="input" name="email" id="email" placeholder="Departure Date" type="text">
                                                                <span class="input-group-addon input-cal calendar"><i class="fa fa-calendar fa" aria-hidden="true"></i></span>
                                                        </div>-->
                                                        </div>
                                                        
                                                        <div class="col-sm-1 col-xs-12">
                                                          <div class="form-group">
                                                            
                                                            </div>
                                                        </div>
                            
                                                        <div class="col-sm-5 col-xs-12">
                                                <div class="inputBox cinputBox-return rmfocus" id="cinputBox-return">
                                                                <div class="inputText">Return Date (Opt)</div>
                                                                <input id="datepicker1" name="return_date" class="input return_date" type="text" >
                                                                <div class="error" id="erReturnDate"></div>
                                                                <?php echo form_error('return_date', '<div class="error">', '</div>'); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row col-lg-offset-1 col-md-offset-1">
                                                        <div class="col-sm-3 col-xs-12">
                                                          <div class="selectBox">
                                                                <div class="inputText">Adults 12+ Yrs</div>
                                                                <select class="input selectText" name="adult" id="adult">
                                                                  <option value="1">1</option>
                                                                  <option value="2">2</option>
                                                                  <option value="3">3</option>
                                                                  <option value="4">4</option>
                                                                  <option value="5">5</option>
                                                                  <option value="6">6</option>
                                                                  <option value="7">7</option>
                                                                  <option value="8">8</option>
                                                                  <option value="9">9</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-sm-1 col-xs-12">
                                                          <div class="form-group">
                                                            
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-sm-3 col-xs-12">
                                                          <div class="selectBox">
                                                                <div class="inputText">Children 2 - 12Yrs</div>
                                                                <select class="input selectText" name="child" id="child">
                                                                  <option value="0">0</option>
                                                                  <option value="1">1</option>
                                                                  <option value="2">2</option>
                                                                  <option value="3">3</option>
                                                                  <option value="4">4</option>
                                                                  <option value="5">5</option>
                                                                  <option value="6">6</option>
                                                                  <option value="7">7</option>
                                                                  <option value="8">8</option>
                                                                  <option value="9">9</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-sm-1 col-xs-12">
                                                          <div class="form-group">
                                                            
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-sm-3 col-xs-12">
                                                          <div class="selectBox">
                                                                <div class="inputText">Infants 0 - 2Yrs</div>
                                                                <select class="input selectText" name="infant" id="infant">
                                                                  <option value="0">0</option>
                                                                  <option value="1">1</option>
                                                                  <option value="2">2</option>
                                                                  <option value="3">3</option>
                                                                  <option value="4">4</option>
                                                                  <option value="5">5</option>
                                                                  <option value="6">6</option>
                                                                  <option value="7">7</option>
                                                                  <option value="8">8</option>
                                                                  <option value="9">9</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
 <!--                                                    <div class="row col-lg-offset-1 col-md-offset-1">
                                                        <div class="col-sm-3 col-xs-12">
                                                          <div class="selectBox">
                                                                <div class="inputText">Preferred Airline</div>
                                                                <select class="input selectText" name="">
                                                                  <option value=""></option>
                                                                  <option>All</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-sm-1 col-xs-12">
                                                          <div class="form-group">
                                                            
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-sm-1 col-xs-12">
                                                          <div class="form-group">
                                                            
                                                            </div>
                                                        </div>
                                                        
                                                    </div> -->
                                                    <div class="row col-lg-offset-1 col-md-offset-1">
                                                      <div class="col-sm-3 col-xs-12">
                                                        <div class="selectBox">
                                                              <div class="inputText">Preferred Airline</div>
                                                              <select class="input selectText" name="airline" id="airline">
                                                                <option>All</option>
                                                              </select>
	                                                    <a onclick="getairline();" style="cursor: pointer; ;top-margin:-10px;">Click to load preferred airline</a>
                                                          </div>
                                                      </div>
                                                       <div class="col-sm-3 col-xs-12">
                                                          <div class="selectBox">
                                                                <div class="inputText">Routing</div>
                                                                <select class="input selectText" name="routingType" id="routingType">
                                                                  <option value="ALL">All</option>
                                                                  <option value="DIRECT">Direct Flights</option>
                                                                  <option value="DIRECT_NONSTOP">Nonstop Direct Flights</option>
                                                                  <option value="SINGLE_CONNECTING">Single Connecting</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 col-xs-12">
                                                          <div class="inputText">Class</div>
                                                          <select class="preferredClass" name="preferredClass" id="preferredClass">
                                                              <option value="ALL">All</option>
                                                              <option value="ECONOMY">Economy</option>
                                                              <option value="BUSINESS">Business</option>
                                                              <option value="ECONOMY_FULL">Premium Economy</option>                      
                                                          </select>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">                           
                                                        <div class="col-sm-4 col-xs-12 col-sm-offset-4">
                                                            <input type="submit" class="btn btn-purple btn-lg btn-block login-button txt-w col-sm-4" id="search_flight" value="Flight Search">
                                                        </div>
                                                    </div>
                                                    
                                                </form>
                                            </div> 
                                        </div>
                                    </div>
                  </div>
                          </div>
                        </div>
                    </div>
                </section>
               
                <!-- Signin Content End -->             
        </div>
        <!-- Inner Wrapper End-->
        <script>


$(document).ready(function() { 
    if($("#destination_input").val()=="") {
      $("#destination").val("");
      $("#destination_code").val("");
    }
     if($("#source_input").val()=="") {
      $("#source").val("");
      $("#source_code").val("");
    }
     /*if($("ins").hasClass("iCheck-helper")){
        $(".iCheck-helper").addClass("checkBox8check");
     } */

    // added by Lilawati karale on 21-02-2019
     /**load all city in from stop input**/
        $("#source_input").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: base_url + "admin/Search_flight/get_city",
                    dataType: "json",
                    data: {
                        term: request.term,
                        // type: rechargeType
                    },
                    success: function(data) {
                        response($.map(data, function(item) {
                            return {
                                label: item.label,
                                name: item.name,
                                code: item.code,
                            };
                        }));
                    }
                });
            },
            autoFocus: true,
            minLength: 1,
            select: function(event, ui) {

                /*$('#error_div').html('');
                 removeBillDetails();*/
                $("#source_input").val(ui.item.name);
                $("#source").val(ui.item.code);
                $("#source_value").val(ui.item.code);
                if (ui.item.name == "No result found") {
                    $("#source").val("");
                    $("#source_input").val("");
                    $("#source_value").val("");
                } else {
                    $("#source").val(ui.item.id);
                }
            }
        });

        /**load all city in to destination input**/
        $("#destination_input").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: base_url + "admin/search_flight/get_city",
                    dataType: "json",
                    data: {
                        term: request.term,
                        // type: rechargeType
                    },
                    success: function(data) {
                        response($.map(data, function(item) {
                            return {
                                label: item.label,
                                name: item.name,
                                code: item.code
                            };
                        }));
                    }
                });
            },
            autoFocus: true,
            minLength: 1,
            select: function(event, ui) {

                /*$('#error_div').html('');
                 removeBillDetails();*/
                $("#destination_input").val(ui.item.name); 
                $("#destination").val(ui.item.code);  
                $("#destination_value").val(ui.item.code);          
                if (ui.item.name == "No result found") {
                    $("#destination").val("");
                    $("#destination_value").val("");  
                    $("#destination_input").val(""); 
                } else {
                    $("#destination").val(ui.item.id);

                }
            }
        });

       $('#datepicker1').prop("disabled", true);
        $('#checkBox8').on('ifChanged', function(event){
            console.log('return');
             $('#datepicker1').prop("disabled", false);
             $('#cinputBox-return').removeClass('cinputBox-return');
            $('#cinputBox-return').addClass('cinputBox');
        });
        
         $('#checkBox7').on('ifChanged', function(event){
             console.log('single');
            $('#datepicker1').val('');
            $('.rmfocus').removeClass('focus');
            $('#datepicker1').prop("disabled", true);
            $('#cinputBox-return').removeClass('cinputBox');
            $('#cinputBox-return').addClass('cinputBox-return');
        });
//      $('.checkBox8check').hover(function () {
//        //$('#datepicker1').prop("disabled", true);
//        return false;
//      }).click(function(){
//          $('#datepicker1').prop("disabled", false);
//          $('#cinputBox-return').removeClass('cinputBox-return');
//          $('#cinputBox-return').addClass('cinputBox');
//      });
//      $('.checkBox7check').hover(function () {
//         return false;
//         //$('#datepicker1').prop("disabled", false);
//      }).click(function(){
//          $('#datepicker1').val('');
//          $('.rmfocus').removeClass('focus');
//          $('#datepicker1').prop("disabled", true);
//           $('#cinputBox-return').removeClass('cinputBox');
//          $('#cinputBox-return').addClass('cinputBox-return');
//      });
       
    $("body").on('click', '#search_flight', function(){   
        //$("#search_buses").prop('disabled', true);
        //$("#search_buses").html("Loading......");
        var fromDate = $('#source_input').val();
        var toDate = $('#destination_input').val();
        var departureDate = $("#departure_date").val();
        var returnDate = $("#datepicker1").val();
        var triptype = $("input[name='triptype']:checked"). val();
        
        if(fromDate == '' && toDate =='' && departureDate == '' && triptype == 'round') {
                $('#erFromStop').text('Please enter a Source');
                $('#erToStop').text('Please enter a Destination');
                $('#erDatepicker').text('The Departure date is required.');
                $('#erReturnDate').text('The Return date is required.');
                return false;
    }else if(fromDate == '' && toDate =='' && departureDate == '' && triptype == 'single') {
                $('#erFromStop').text('Please enter a Source');
                $('#erToStop').text('Please enter a Destination');
                $('#erDatepicker').text('The Departure date is required.');
                return false;
        }
        if(fromDate == ''){
                $('#erFromStop').text('Please enter a Source');
                return false;
        }
        if(toDate == ''){
                $('#erToStop').text('Please enter a Destination');
                return false;
        }
        if(departureDate == ''){
             $('#erDatepicker').text('The Departure date is required.');
                return false;
        }
        if(triptype == 'round') {
            if(returnDate == ''){
                    $('#erReturnDate').text('The Return date is required.');
                return false;
            }
        }
  });

  //$(document).off("click",".switch_cities").on("click",".switch_cities",function(e){e.preventDefault();var r=$("#from_stop").val(),t=$("#to_stop").val();(""!=r||""!=t)&&($("#from_stop").val(t),$("#to_stop").val(r))});

       $("#departure_date" ).datepicker({ 
            numberOfMonths: 2,
            minDate: 0,
            dateFormat: 'dd-mm-yy'
        }).on("change", function() {
                $('#datepicker1').datepicker('option', 'minDate', $("#departure_date").val());
      });

        $("#datepicker1" ).datepicker({
      numberOfMonths: 2,
            minDate: 0,
            dateFormat: 'dd-mm-yy'
        }).on("change", function() {
                $('#departure_date').datepicker('option', 'minDate', 0);
    });
    
    /**load all stops in from stop input**/
        $("#from_stop").autocomplete({
          source: function (request, response) {
              $.ajax({

                  url: base_url+"admin/search_bus/get_stop",
                  dataType: "json",
                  data: {
                      term: request.term,
                     // type: rechargeType
                  },
                  success: function (data) {
                      response($.map(data, function (item) {                        
                          return {
                              label: item.label,
                              
                              name: item.name,
                              
                          };
                      }));
                  }
              });
          },
          autoFocus: true,
          minLength: 1,
          select: function (event, ui) {
             
              /*$('#error_div').html('');
              removeBillDetails();*/
              $("#from_stop").val(ui.item.name);
              
              if (ui.item.name == "No result found") {
                  $("#from_stop").val("");
              }else{
                  $("#from_stop_id").val(ui.item.id);
              }
          }
      });

        /**load all stops in to stop input**/
      $("#to_stop").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: base_url+"admin/search_bus/get_stop",
                dataType: "json",
                data: {
                    term: request.term,
                   // type: rechargeType
                },
                success: function (data) {
                    response($.map(data, function (item) {                        
                        return {
                            label: item.label,
                            
                            name: item.name,
                            
                        };
                    }));
                }
            });
        },
        autoFocus: true,
        minLength: 1,
        select: function (event, ui) {
           
            /*$('#error_div').html('');
            removeBillDetails();*/
            $("#to_stop").val(ui.item.name);
            
            if (ui.item.name == "No result found") {
                $("#to_stop").val("");
            }else{
                $("#to_stop_id").val(ui.item.id);
            }
        }
    });
      ///////////////////*//////////////////////////////
     
     window.setInterval('alertMsgDangar()', 10000); // 20 seconds
      
});
function alertMsgDangar() {
    $('#alertMsgDangar').hide();
}
</script>
<script type="text/javascript">
    $(".input").focus(function() {
      $(this).parent().addClass("focus");
    })

    $(document).ready(function(){
       var options = {
         url: "http://localhost/rokad/js/flight.cities.min.json",
          getValue: "name",

          list: {	
            match: {
              enabled: true
            }
          },
           theme: "square"
        };

            $("#source").easyAutocomplete(options);

    });


    function getairline(){
      var source = $('#source_value').val();
      var destination = $('#destination_value').val();
      var departure_date = $('#departure_date').val();
      var adult = $("#adult").val();
      var child = $("#child").val();
      var infant = $("#infant").val();
      var routingType = $("#routingType").val();
      var preferredClass = $("#preferredClass").val();

      var url = base_url + "admin/search_flight/airline_list";
        if(source == '' || destination =='' || departure_date == '') {
            alert('Source, destination and departure date is required');
            return false;
        }else{
      var dataString = 'source=' + source + '&destination=' + destination + '&departure_date=' + departure_date + '&adult=' + adult + '&child=' + child + '&infant=' + infant + '&routingType=' + routingType + '&preferredClass=' + preferredClass;
             $('#divLoading').css('display', 'block');
      $.ajax({
          type: 'POST',
          url: url,
          dataType: "json",
          data: dataString,
          success: function (response) {
                    $('#divLoading').css('display', 'none');
            if (response.carriers != "") {
              var toAppend = '';
             $.each(response.carriers,function(index,value){
              toAppend += '<option value="' + value.code + '">' + value.name + '</option>';
            });
           $('#airline').append(toAppend);
            };
                }, complete: function(){
                    $('#divLoading').css('display', 'none');
          }
      });
            
	}
    }

   </script>
