<script>
    $(document).ready(function () {
            /************** for confirm message***********/
        $('.confirm-div').hide();
        <?php if ($this->session->flashdata('invalid')) { ?>
                    $('.confirm-div').html('<?php echo $this->session->flashdata('invalid'); ?>').show();
        <?php } else if ($this->session->flashdata('invalid')) { ?>
                    $('.confirm-div').html('<?php echo $this->session->flashdata('valid'); ?>').show();
        <?php } ?>
            /*********************************************/
   
    /***************To Fetch Routes of Seller Point***************/
    $('#seller_code').on("change",function() {                                      
        var check_b = '';
        $('#route_stops').empty();
        var seller_cd = $('#seller_code').val();
        $.ajax ({
            url : BASE_URL+"admin/zip_route_stop/list_route?seller_cd="+seller_cd,
            type : "GET",
            dataType : "json",
            success : function (data) {
                $('#route_stops').empty();
                if(data.length >0){
                    check_b +="<input type ='checkbox' id='checkAll' style='display:block'class='col-lg-1'><span class='col-lg-2'>All</span>";
                    for(i=0; i< data.length; i++){
                        check_b +="<input type ='checkbox' id='"+data[i]['ROUTE_NO']+"' value='"+data[i]['ROUTE_NO']+"' name ='route[]' style='display:block'class='col-lg-1' ><span class='col-lg-2'>"+data[i]['ROUTE_NO']+"</span>";
                    }    
                }
                $('#route_stops').append(check_b)
            }
        });

    });   
    /***************To Check All Options***************/
    $('#route_stops').on('change','#checkAll',function(){
        if($(this).prop("checked") == true){ 
            $('input[name="route[]"]').prop("checked", true);
        } else {
            $('input[name="route[]"]').prop("checked", false);
        }
    });
    /****************Validation***********************/

    $("#zip_route_stop").validate({
	rules: {
            seller_code: {
		required: true
            },
            'route[]': {
                required: true,
                minlength: 1
            }
        },
        messages: {
            seller_code: "Please Select Seller Code." ,
            'route[]': "Please Check At Least One Route Stop",
        } ,
	errorPlacement: function (error, element) {				
            if (element.attr("name") == "request_to") {				
		error.appendTo( element.closest(".form-group") );
            } else {
		error.appendTo( element.parent("div") );
            }
	}				
    });

});
</script> 


<section class="content">
    <div class="row">
        <!-- right column -->
        <div class="col-md-12">
            <!-- general form elements disabled -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">zip Route Stops</h3>
                </div><!-- /.box-header -->
                  
                <?php
                    $attributes = array("method" => "POST", "id" => "zip_route_stop", "class" => "cmxform form");
                    echo form_open('admin/zip_route_stop/generate_route_stop_zip', $attributes);   
                ?>  

                
                    <div class="form-group">
                        <label class="col-lg-3 control-label" >Seller Code:*</label>
                        <div class="col-lg-6">
                        <!--<select name="seller_code" id="seller_code" class="chosen_select" title="Please select something!">-->
                        <select name="seller_code" id="seller_code" class="form-control" title="Please select something!">
                            <option value ="">Select Seller code</option>
                            <?php foreach($seller_point as $value){?>
                                <option value="<?php echo $value->BUS_STOP_CD;?>"><?php echo  $value->BUS_STOP_CD;?></option>
                            <?php } ?>
                        </select>
                        </div>    
                    </div>
                    <div class="clearfix" style="height: 40px;clear: both;"></div>
                    <div class="form-group">
                        <label class="col-lg-12">Route Stop:</label>
                        <div id="route_stops" class="col-lg-12">
                        
                        </div>
                    </div>
                    <div class="clearfix" style="height: 40px;clear: both;"></div>
                    <div class="form-group">
                        <center>
                            <button class="btn btn-primary" id="download_zip" name="download_zip" type="submit">Download Zip</button>
                        </center>
                    </div>
                    <div class="clearfix" style="height: 40px;clear: both;"></div>
                <?php echo form_close(); ?>
            </div> 
        </div>
    </div>    
</section>


            