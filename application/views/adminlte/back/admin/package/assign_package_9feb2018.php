<section class="content">
    <div class="row">
        <!-- right column -->
        <div class="col-md-12">
            <!-- general form elements disabled -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Assign Package</h3>
                </div><!-- /.box-header -->
                <?php if ($this->session->flashdata('msg')) { ?>
                    <div class="alert alert-success alert-dismissable">
                        <b>
                            <?php
                            $data = $this->session->flashdata('msg');
                            echo $data;
                            ?>
                        </b>
                        <i class="fa fa-check"></i>
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                    </div>
                <?php } ?>      
                <?php
                $attributes = array ("method" => "POST" ,"name" => "create_package_form" , "id" => "create_package_form");
                echo form_open(site_url()."/admin/package/assignPackageToMd", $attributes);
                ?>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="package name">Regional Distributor Name</label>
                        <div class="col-lg-6">
                            <select class="form-control chosen_select" name="md_id" id="md_id" aria-invalid="false" autocomplete="off">
                             <option value="">Select Regional Distributor</option> 
                            <?php if(count($md_details) > 0) { foreach ($md_details as $key => $md) {
                                echo "<option value='" . $md->id . "'> " . $md->first_name." ". $md->last_name. " </option>";
                               } }
                               ?>
                            </select>
                        </div>
                    </div>


                    <div class="form-group pkg_exist col-lg-12" style="display: none;">
                        <label class="col-lg-3 control-label" for="package name"></label>
                        <div class="col-lg-6">
                           <h3 style="color: red;"></h3>
                        </div>
                    </div>

                    <div class="clearfix" style="height: 40px;clear: both;"></div>
                   <div class="show_serices" style="display: none;">
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="package name">Services</label>
                            <div class="col-lg-6">
                            <?php  
                                foreach($service as $key => $value){ ?>
                                <div class="form-group" >
                                    <div class="col-lg-12 intent">
                                      <div class="col-lg-5">
                                        <input type="checkbox" name="service_name[]" id="service_name_<?php echo $value['id'];?>" value="<?php echo $value['id'];?>" class="service_type"  required=""> <?php echo $value['service_name']; ?>
                                      </div>
                                      <div class="col-lg-7">
                                          <select id="select_package_<?php echo $value['id'];?>" class="form-control" name="select_package[]" style="display: none;" required="">
                                          <!-- <option value="">Select Package</option> -->
                                            </select>
                                      </div> 
                                </div>
                            </div>

                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                          <?php } ?>
                          </div>

                          <div class="clearfix" style="height: 10px;clear: both;"></div>
                          <div class="clearfix" style="height: 10px;clear: both;"></div>

                          <div class="form-group">
                              <div class="col-lg-offset-4">
                                  <button class="btn btn-primary" id="save_package_data" name="save_package_data" type="submit">Save</button> 
                                 <!--  <a class="btn btn-primary" href="<?php echo base_url('admin/package'); ?>" type="button">Back</a>  -->
                              </div>
                          </div>
                           <div class="clearfix" style="height: 10px;clear: both;"></div>
                      </div>
                  </div>
            </div>
        </div> <!-- /.box -->
    </div> <!--/.col (right) -->
    <?php echo form_close(); ?>
</section>

<script type="text/javascript">
    
$(document).ready(function ()
{   
      //list commission ajax starts here.
    $('.pkg_exist').hide();
    $('.alert-dismissable').delay(10000).fadeOut("slow");
    $(function() {
        $('#md_id').change(function(){
        if($('#md_id').val() != '') {
          
          // $('.show_serices').hide();
          var detail = {};
          var div = '';
          var form = '';
          var ajax_url = 'admin/package/current_package_detail';
          detail['md_id'] = $('#md_id').val();
        
          if (detail['md_id'] != '')
          {
              get_data(ajax_url, form, div, detail, function (response)
              {
                  $('.show_serices').show();
                  if (response)
                  {
                      $('.pkg_exist').show();
                      $('.pkg_exist h3').html('Package '+response['package_name']+' Already Exist to this MD.');
                  }
                  else
                  {
                       $('.pkg_exist').hide();

                  }
              }, '', false);
            }
          } 
          else 
          {
              $('.show_serices').hide(); 
              $('.pkg_exist').hide();
          } 
      });
  });


    $('#create_package_form').validate({ 
            rules: {
                      
                      "service_name[]" :{
                        required : true,
                      },
                      "select_package[]" :{
                        required : true
                      } 
                    },
            messages: {
                        "service_name[]" : {
                            required: "Please select at least one service",
                        },
                        "select_package[]" : {
                            required: "Please select package",
                        }
                      },
            errorPlacement: function (error, element) { 
              if ($(element).hasClass("service_type")) { 
                  var lastCheckBox = $('[name="'+element.attr("name")+'"]:last');
                  error.insertAfter(lastCheckBox.closest('.intent'));
              } else {
                  error.appendTo(element.parent("div") );
              }
            }
      });

    //On change of service value load commission value in dropdown from db using ajax here start //
    $(document).off('ifChanged', '.service_type').on('ifChanged', '.service_type', function (e) {
        service_id = $(this).attr('value');
        if ($(this).is(':checked')){
                e.preventDefault();
                this.setAttribute('checked',this.checked);
              
                var elem = $(this);
                var detail = {};
                var form = '';
                var div = '';
                detail['service_id'] = $(this).attr('value');
                var ajax_url = "admin/package/list_package_detail";
                get_data(ajax_url, form, div, detail, function(response) { 
                    $("#select_package_"+detail['service_id']).append('<option value="">Select Package</option>');
                    $("#select_package_"+detail['service_id']).show();
                    $.each(response, function(index, value) {  
                        $("#select_package_"+detail['service_id']).append("<option value=" + value.id + ">" + value.package_name + "</option>");
                        $("#service_name_"+detail['service_id']).removeClass('checked');
                    });
                });
            }
            else 
            {
              $("#select_package_"+service_id).hide();
              $('select[name="select_package[]"]').empty();
            }
    });
    
});

</script>   
