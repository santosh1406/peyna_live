<section class="content">
    <div class="row">
        <h3 class="box-title">&nbsp;&nbsp;&nbsp;View Package</h3>
        <?php if ($this->session->flashdata('error')) { ?>
            <div class="alert alert-success alert-dismissable">
                <b>
                    <?php
                    echo $this->session->flashdata('error');
                    ?></b>
                <i class="fa fa-check"></i>
                <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            </div>
        <?php } ?>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="bs-example">
                        <div class="alert alert-danger alert-error" style="display:none">
                            <strong>Error!</strong> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="package name">Package Name</label>
                        <div class="col-lg-6">
                            <input name="name" type="text" id="name" value="<?php echo $package_name; ?>" class="form-control" placeholder="package Name" readonly>
                        </div>
                    </div>
                    
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group" id = "default_package">
                        <label class="col-lg-3 control-label" for="default_package">Default Package</label>
                        <div class="col-lg-6">
                            <input name="default_package" type="text" id="default_package" value="<?php echo (($default_package == '1') ? 'Yes' : 'No'); ?>" class="form-control" placeholder="Default PackageTemplate" readonly>
                        </div>
                    </div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                  
                    <?php if ($default_package == '0') { ?>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="package from">From Date</label>
                            <div class="col-lg-6">
                               <input class="form-control " id="from" name="from" value="<?php echo $package_from; ?>"  type="text" readonly>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group till">
                                <label class="col-lg-3 control-label" for="till date">Till Date</label>
                                <div class="col-lg-6">
                                    <input class="form-control " id="till" name="till" value="<?php echo $package_till; ?>" type="text" readonly>
                                </div>
                            </div>    
                        </div>
                    <?php } ?>

                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <input name="package_id" type="hidden" id="package_id" value="<?php echo $package_id; ?>" class="form-control" placeholder="">
                            <span id="package_id" class="err"></span>
                        </div>
                    </div>

                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                     <div class="form-group">
                        <div class="col-lg-10">
                            <div class="table-responsive">          
                                <table class="table table-bordered" id="provider-operator-package" style="width:90%;">
                                <?php $counter = 1; ?>   
                                    <thead>
                                        <tr>
                                            <th colspan="3" style="text-align:center" class="bg-gray disabled color-palette"></th>
                                        </tr>
                                        <tr>
                                            <th style="text-align:center">#</th>
                                            <th style="text-align:center">Services</th>
                                            <th style="text-align:center">Commission</th>        
                                        </tr>

                                    </thead>
                                    <tbody> 
                                        <?php foreach ($package_detail as $key => $value) { ?>
                                        <tr>
                                            <td><?php echo $counter ++; ?></td>
                                            <td><?php echo $value["service_name"]; ?></td>
                                            <td><?php echo $value["commission_name"]; ?></td>
                                        </tr>
                                    </tbody>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                     <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group">
                        <div class="col-lg-offset-4">
                            <a class="btn btn-primary" href="<?php echo base_url('admin/package'); ?>" type="button">Back</a> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


