<script type="text/Javascript" src="<?php echo base_url(); ?>js/back/commission/package.js" ></script>
<section class="content">
    <div class="row">
        <!-- right column -->
        <div class="col-md-12">
            <!-- general form elements disabled -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Add Package</h3>
                </div><!-- /.box-header -->
                <?php if ($this->session->flashdata('msg')) { ?>
                    <div class="alert alert-success alert-dismissable">
                        <b>
                            <?php
                            $data = $this->session->flashdata('msg');
                            foreach ($data as $key => $value) {
                                echo $value . "<br>";
                            }
                            ?>
                        </b>
                        <i class="fa fa-check"></i>
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                    </div>
                <?php } ?>      
                <?php
                $attributes = array ("method" => "POST" ,"name" => "create_package_form" , "id" => "create_package_form");
                echo form_open(site_url()."/admin/package/create", $attributes);
                ?>
                	<div class="form-group">
                        <label class="col-lg-3 control-label" for="package name">Package Name</label>
                        <div class="col-lg-6">
                            <input name="name" type="text" id="name" class="form-control" placeholder="Package Name" autocomplete="off">
                        </div>
                   	</div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div> 
                    <div class="form-group" style="display: block;">
                        <label class="col-lg-3 control-label" for="package name">Default Package</label>
                        <div class="col-lg-6">
                            <select class="form-control" name="default_package" id="default_package" aria-invalid="false" autocomplete="off">
                                <option value="0" selected="">No</option>
                                <option value="1">Yes</option>
                            </select>
                        </div>
                    </div>

                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group from" style="display:none;">
                        <label class="col-lg-3 control-label" for="from date">From Date</label>
                        <div class="col-lg-6">
                                  <input class="form-control " id="from" name="from" placeholder="<?php echo date('d-m-Y'); ?> " type="text" autocomplete="off">
                        </div>
                    </div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group till" style="display:none;">
                            <label class="col-lg-3 control-label" for="till date">Till Date</label>
                            <div class="col-lg-6">
                                <input class="form-control " id="till" name="till" placeholder="<?php echo date('d-m-Y'); ?> " type="text" autocomplete="off">
                            </div>
                        </div> 

                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="package name">Services</label>
                        <div class="col-lg-6">
                        <?php  
                            foreach($service as $key => $value){ ?>
                            <div class="form-group" >
                                <div class="col-lg-10 intent">
                                  <div class="col-lg-6">
                                    <input type="checkbox" name="service_name[]" id="service_name_<?php echo $value['id'];?>" value="<?php echo $value['id'];?>" class="service_type"  required=""> <?php echo $value['service_name']; ?>
                                  </div>
                                  <div class="col-lg-6">
                                      <select id="select_commission_<?php echo $value['id'];?>" class="form-control" name="select_commission[]" style="display: none;" required="">
                                        </select>
                                  </div> 
                            </div>
                        </div>

                          <div class="clearfix" style="height: 10px;clear: both;"></div>
                      <?php } ?>
                      </div>

                      <div class="clearfix" style="height: 10px;clear: both;"></div>
                      <div class="clearfix" style="height: 10px;clear: both;"></div>

                      <center><img src="<?php echo base_url().BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image" style="display:none;"/></center>
                      
                      <div class="form-group">
                          <div class="col-lg-offset-4">
                              <button class="btn btn-primary" id="save_package_data" name="save_package_data" type="submit">Save</button> 
                              <a class="btn btn-primary" href="<?php echo base_url('admin/package'); ?>" type="button">Back</a> 
                          </div>
                      </div>
                  </div>
            </div>
        </div> <!-- /.box -->
    </div> <!--/.col (right) -->
    <?php echo form_close(); ?>
</section>

<script type = "text/javascript">
var img = '&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url().BACK_ASSETS ?>img/ajax-loader.gif" class="img-circle" alt="user image"/>';
  // $(document).ready(function() {

  //   //form validation code start here
  //   $('#create_package_form').validate({ 
  //           rules: {
  //                     name: {
  //                       required: true,
  //                       package_exists : true
  //                     },
  //                     default_package : {
  //                       required : true
  //                     },
  //                     from :{
  //                       required : true
  //                     },
  //                     till : {
  //                       required : true
  //                     },
  //                     "service_name[]" :{
  //                       required : true,
  //                       //check_commission : true;
  //                     },
  //                     "select_commission[]" :{
  //                       required : true
  //                     } 
  //                   },
  //           messages: {
  //                       name: {
  //                         required: "Please enter package name",
  //                         package_exists : "Package name already exists"
  //                       },
  //                       default_package: {
  //                         required: "Please select default package",
  //                       },
  //                       from :{
  //                           required : "Please select form date",
  //                       },
  //                       till : {
  //                           required : "Please select till date",
  //                       },
  //                       "service_name[]" : {
  //                           required: "Please select at least one service",
  //                       },
  //                       "select_commission[]" : {
  //                           required: "Please select commission",
  //                       }
  //                     },
  //           errorPlacement: function (error, element) { 
  //             if ($(element).hasClass("service_type")) { 
  //                 var lastCheckBox = $('[name="'+element.attr("name")+'"]:last');
  //                 error.insertAfter(lastCheckBox.closest('.intent'));
  //             } else {
  //                 error.appendTo(element.parent("div") );
  //             }
  //           }
  //     });

  //   // check package already exists code start here
  //   $.validator.addMethod("package_exists",function(value, element) { 
  //     var flag;

  //     $.ajax({ 
  //       url : BASE_URL + "admin/package/package_unique",
  //       data : {name : value},
  //       async : false,
  //       type : 'POST',
  //       success : function(r) {
  //         flag = (r === 'true') ? false : true;
  //       }
  //     });
  //     return flag;
  //   } ,"Package name already exists");

  //   // load datepicker
  //   $( "#from" ).datepicker({
  //   'dateFormat' : 'dd-mm-yy',
  //   maxDate :0
  //   });
  //   $( "#till" ).datepicker({
  //       'dateFormat' : 'dd-mm-yy',
  //       maxDate:0
  //   });
  
  //   // on change of default package value date show hide code start here 
  //   $(document).off('change', '#default_package').on('change', '#default_package', function () {
  //       if (this.value == '1')
  //       {
  //         $(".from").show();
  //         $(".till").show();
  //       }
  //       else
  //       {
  //         $(".from").hide();
  //         $(".till").hide();
  //       }
  //   });     

  //   //On change of service value load commission value in dropdown from db using ajax here start //
  //   $(document).off('ifChanged', '.service_type').on('ifChanged', '.service_type', function (e) {
  //     service_id = $(this).attr('value');
  //     if ($(this).is(':checked')){
  //         e.preventDefault();
  //         this.setAttribute('checked',this.checked);
  //         $("#select_commission_"+service_id).empty();
  //         $("#select_commission").html("");
  //         var elem = $(this);
  //         var detail = {};
  //         var form = '';
  //         var div = '';
  //         detail['service_id'] = $(this).attr('value');
  //         var ajax_url = "admin/package/list_commission_detail";
  //         get_data(ajax_url, form, div, detail, function(response) { 
  //             $("#select_commission_"+detail['service_id']).show();
  //             // $("#select_commission_"+detail['service_id']).append("<option value=''>Select Commission</option>");
  //             $.each(response, function(index, value) {  
  //               $("#select_commission_"+detail['service_id']).append("<option value=" + value.id + ">" + value.name + "</option>");
  //               $("#service_name_"+detail['service_id']).removeClass('checked');
  //             });
  //         });
  //       }
  //       else 
  //       {
  //         $("#select_commission_"+service_id).hide();
  //       }
  //   });

  // });  
</script>   
   