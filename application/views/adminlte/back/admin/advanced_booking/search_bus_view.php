<!-- Created by Sonali Kamble on 20-august-2018 -->
<style>
    ul.seat li
    {
        cursor: pointer; 
        display: inline; 
        padding: 2px;  
    }
</style>
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content" id="module">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> 
                        <h3 class="box-title">Advance Bus Booking</h3>
                    </div>
                    <div class="box-body">
                        <?php
                        echo validation_errors('<div class="error" id="error_div">', '</div>');
                        ?>
                          <?php 
                          $msg = $this->session->flashdata('item');
                        if($msg) {
                        ?>
                         <div class="alert alert-success alert-dismissable" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                             <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                             <?php echo $msg['message']; ?>
                         </div>
                        <?php          
                        }
                       ?>
                        <form action="#" method="POST" id="bus_booking" name="bus_booking" accept-charset="utf-8">  

                            <div class="row form-group">
                                <div class="col-sm-6 col-xs-6">
                                    <div class="checkbox checkbox-info checkbox-circle pull-right">
                                        <input id="checkBox7" class="checkbox-circle" type="checkbox" name="triptype" value="single" checked>
                                        <label for="checkBox7">Single Trip</label>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-xs-6">
                                    <div class="inputBox">
                                        <div class="checkbox checkbox-info checkbox-circle">
                                            <input id="checkBox8" class="checkbox-circle" type="checkbox" name="triptype" value="return">
                                            <label for="checkBox8">Round Trip</label>
                                        </div>
                                    </div>
                                </div>
                            </div>                      

                            <div class="form-group"> 
                                <label class="col-lg-3 control-label">From *:</label>
                                <div class="col-lg-6">

                                    <input class="form-control" type="text" id="from_stop" name="from_stop" value="<?php echo isset($data['from_stop']) ? $data['from_stop'] : ''; ?>" value="<?php echo set_value('from_stop'); ?>"/>
                                    <input type="hidden" id="from_stop_id" name="from_stop_id" class="" value="<?php echo isset($data['from_stop_id']) ? $data['from_stop_id'] : ''; ?>" />
                                    <label id="from-error" class="error" for="from"></label>
                                </div>
                            </div> 
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label">To *:</label>
                                <div class="col-lg-6">
                                    <input type="text" id="to_stop" name="to_stop" class="form-control" placeholder="To Stop" />
                                    <label id="to-error" class="error" for="to"></label>

                                </div>
                            </div>
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label">Departure Date*:</label>
                                <div class="col-lg-6">
                                    <input type="text"   id="datepicker"  name="departure_date" class="form-control departure_date" placeholder="Departure Date"/>
                                   <!--  <p>Date: <input type="text" id="datepicker" ></p> -->
                                </div>
                            </div><!--//-->
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label">Return Date(opt):</label>
                                <div class="col-lg-6">
                                    <input type="text" id="datepicker1" name="return_date" class="form-control" placeholder="Return Date"/>
                                </div>
                            </div><!--//-->
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label">Bus Operator Type (All):</label>
                                <div class="col-lg-6">
                                    <select class="input selectText form-control" name="provider">
                                        <option value="all">Bus Operator Type (All)</option>
                                        <option value="operator">State Transport Operator</option>
                                        <option value="api">Private Operator</option>
                                    </select>               
                                </div>
                            </div><!--//-->
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label">Bus Type(All):</label>
                                <div class="col-lg-6">
                                    <select class="input selectText form-control" name="searchbustype">
                                        <option value="all">Bus Type (All)</option>
                                        <option value="ac">AC</option>
                                        <option value="non_ac">Non AC</option>
                                    </select>           
                                </div>
                            </div><!--//-->
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label">Bus Type(All):</label>
                                <div class="col-lg-6">
                                    <select class="input selectText form-control" name="timeslot">
                                        <option value=""></option>
                                        <option value="morning">Morning (6:00 - 11:59)</option>
                                        <option value="afternoon">Afternoon (12:00 - 15:59)</option>
                                        <option value="evening">Evening (16:00 - 18:59)</option>
                                        <option value="night">Night (19:00 - 6:00)</option>
                                    </select>                       
                                </div>
                            </div><!--//-->
                            <div class="clearfix" style="height: 10px;clear: both;"></div>
                            <div class="form-group">
                                <div class="col-lg-offset-4">
                                    <button class="btn btn-primary" id="search_buses"  type="submit">Search Buses</button>                                
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div class="col-sm-12">
                            <div id="next-pre" style="text-align: center;" class="form-group col-sm-6 col-md-6"></div>
                            <div id="return_next-pre" style="text-align: center;display: none" class="form-group col-sm-6 col-md-6"></div>
                        </div>
                        <div class="no_routes_found" style="display: none;">
                            <h6>Ooops!</h6>
                            <div class="no_routes_notification">
                                <span>No bus found for this route today.</span>
                            </div>
                        </div>
                        <div id="bus_services">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>


    $(document).ready(function() {

        /**load all stops in from stop input**/
        $("#from_stop").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: base_url + "admin/search_bus/get_stop",
                    dataType: "json",
                    data: {
                        term: request.term,
                        // type: rechargeType
                    },
                    success: function(data) {
                        response($.map(data, function(item) {
                            return {
                                label: item.label,
                                name: item.name,
                            };
                        }));
                    }
                });
            },
            autoFocus: true,
            minLength: 1,
            select: function(event, ui) {

                /*$('#error_div').html('');
                 removeBillDetails();*/
                $("#from_stop").val(ui.item.name);

                if (ui.item.name == "No result found") {
                    $("#from_stop").val("");
                } else {
                    $("#from_stop_id").val(ui.item.id);
                }
            }
        });

        /**load all stops in to stop input**/
        $("#to_stop").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: base_url + "admin/search_bus/get_stop",
                    dataType: "json",
                    data: {
                        term: request.term,
                        // type: rechargeType
                    },
                    success: function(data) {
                        response($.map(data, function(item) {
                            return {
                                label: item.label,
                                name: item.name,
                            };
                        }));
                    }
                });
            },
            autoFocus: true,
            minLength: 1,
            select: function(event, ui) {

                /*$('#error_div').html('');
                 removeBillDetails();*/
                $("#to_stop").val(ui.item.name);

                if (ui.item.name == "No result found") {
                    $("#to_stop").val("");
                } else {
                    $("#to_stop_id").val(ui.item.id);
                }
            }
        });

        $("#datepicker").datepicker({numberOfMonths: 2, minDate: 0, });
        $("#datepicker1").datepicker({numberOfMonths: 2, minDate: 0});

        /**************bus_services ******************/
        $("body").on('click', '#search_buses', function() {

            event.preventDefault();
            $("#search_buses").prop('disabled', true);
            $("#search_buses").html("Loading......");


            var from_stop = $("#from_stop").val();
            var to_stop = $("#to_stop").val();
            var departure_date = $(".departure_date").val();
            var return_date = $("#return_date").val();
            var timeslot = $("input[name=timeslot]").val();
            var searchbustype = $("input[name=searchbustype]").val();
            var provider = $("input[name=provider]").val();
            var triptype = $("input[name=triptype]").val();

            var url = base_url + "admin/search_bus/bus_services";

            if (from_stop == to_stop)
            {
                $('#from-error').html('From & to stop must be different');
                $("#to-error").html('From & to stop must be different');
            }

            if (from_stop == '' || to_stop == '' || departure_date == '')
            {
                alert("Please Fill All Fields");
            }
            else
            {
                // AJAX Code To Submit Form.
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: url,
                    data: $('#bus_booking').serialize(),
                    success: function(data) {
                        $("#search_buses").prop('disabled', false);
                        $("#search_buses").html("Search Buses");
                        var error = data.msg;

                        if (error == 'null')
                        {
                            $('.no_routes_found').css("display", "block");
                        }
                        else if (error == 'No service found')
                        {
                            $('.no_routes_found').css("display", "block");
                        }
                        else
                        {
                            $('.no_routes_found').css("display", "none");


                        }


                        var next_pre = "";
                        var return_next_pre = "";
                        var input_data = data.input_data;
                        //for single journey
                        var next_pre = '<div class="from_to_search">' +
                                '<span class="mr10" id="show_from">' + input_data.from + '</span>' +
                                '<span class="fa fa-arrow-right"></span>' +
                                '<span class="ml10" id="show_to">' + input_data.to + '</span>' +
                                '</div>' +
                                '<div class="next_previous">' +
                                '<span id="date_shown">' + moment(input_data.date, "MM/DD/YYYY").format("DD-MMM-YYYY") + '</span>';
                        '</div>';

                        $("#next-pre").html(next_pre);
                        //for return journey
                        if (input_data.return_date != '')
                        {
                            $('#return_next-pre').css("display", "block");
                            var return_next_pre = '<div class="from_to_search">' +
                                    '<span class="mr10" id="return_show_from">' + input_data.to + '</span>' +
                                    '<span class="fa fa-arrow-right"></span>' +
                                    '<span class="ml10" id="return_show_to">' + input_data.from + '</span>' +
                                    '</div>' +
                                    '<div class="next_previous">' +
                                    '<span id="return_date_shown">' + moment(input_data.return_date, "MM/DD/YYYY").format("DD-MMM-YYYY") + '</span>';
                            '</div>';
                            $("#return_next-pre").html(return_next_pre);
                        }
                        else
                        {
                            $('#return_next-pre').css("display", "none");
                        }






                        //load services in table
                        var result = data.result;

                        var row = "";

                        for (i = 0; i < result.length; i++) {

                            var inventory_type = (result[i].inventory_type) ? result[i].inventory_type : "0";

                            var company_name = (result[i].company_name) ? (result[i].company_name) : " ";
                            var bus_name = (result[i].op_bus_type_name + '/' + result[i].bus_type_name) ? (result[i].op_bus_type_name + '/' + result[i].bus_type_name) : ' ttt';

                            row += "<div class='form-group'><table><tr><td>" + company_name + "</td></tr><tr><td>Operator Name:</td><td> " + result[i].operator_name + "</td></tr><tr><td>From:</td><td>" + result[i].from_stop + "</td></tr><tr><td>To:</td><td>" + result[i].to_stop + "</td></tr><tr><td>Bus Name:</td><td id='bus_name'>" + bus_name + "</td></tr><tr><td >Departure Name:</td><td id='dept_date'>" + result[i].departure_date + "</td></tr><tr><td >Arrival Name:</td><td id='arr_date'>" + result[i].arrival_date + "</td></tr><tr><td>Available seat:</td><td>" + result[i].available_seat + "</td></tr><tr><td>Price:</td><td>" + result[i].seat_fare_with_tax + "</td></tr></table></div><div class='form-group'><div class='col-lg-offset-4'><a href='javascript:void(0)' name='tab' class='btn btn-primary selectseat2' data-trip-id=" + result[i].trip_id + " data-service-id=" + result[i].service_id + " data-provider-id=" + result[i].provider_id + " data-inventory-type=" + inventory_type + " data-date='" + result[i].departure_date + "' data-from='" + result[i].from_stop + "' data-to='" + result[i].to_stop + "' data-op-name='" + result[i].operator_name + "'>Select Seat</a> </div></div><div class='clearfix' style='height: 10px;clear: both;'></div>";

                            if (result[i].operator_name == 'MSRTC') {

                                row += "<div class='form-group' style='display:none' id='dropdown_" + result[i].service_id + "'>" +
                                        "<div class='col-lg-4'>" +
                                        "<select class='form-control' id='from_stop_" + result[i].service_id + "'>" +
                                        "<option value=''> Selet Boarding stop</option>" +
                                        "</select>" +
                                        "</div>" +
                                        "<div class='col-lg-4'>" +
                                        "<select class='form-control' id='to_stop_" + result[i].service_id + "'>" +
                                        "+<option value=''> Selet Dropping stop</option>" +
                                        "</select>" +
                                        "</div>" +
                                        "<div class='col-lg-4'>" +
                                        "<a href='javascript:void(0)' class='btn btn-primary get_seat' data-trip-id=" + result[i].trip_id + " data-service-id=" + result[i].service_id + " data-provider-id=" + result[i].provider_id + " data-inventory-type=" + result[i].inventory_type + " data-date='" + result[i].departure_date + "' data-from='" + result[i].from_stop + "' data-to='" + result[i].to_stop + "' data-op-name='" + result[i].operator_name + "' >Select Seat</a>" +
                                        "</div>" +
                                        "</div>";



                            }

                            row += "<div class='form-group'><span class='bus_seat_layout'><div class='col-md-7'><div class='fare_tab_filter show'><ul id='fare_tab_filter_id_" + result[i].service_id + "' class='seat'></ul></div></div></span>";

                            row += " <form method='post' action='#'>" +
                                    "<div class='col-md-4 pull-right seat_detail_info_" + result[i].service_id + "' style='display:none';>" +
                                    "<div class='col-md-12 tprice'>" +
                                    "<div class='SelectedSeats clearfix'>" +
                                    "<label>Seat(s)  :</label>" +
                                    "<span class='sel_seat' id='get_seat_no'></span>" +
                                    "</div>" +
                                    "<div class='amount clearfix'>" +
                                    "<label>Base Fare : </label>" +
                                    "<span class='fare_value'>Rs. <b class='fare' id='get_fare_id'>0</b></span>" +
                                    "</div>" +
                                    "<div class='amount clearfix'>" +
                                    "<label>Service Tax : </label>" +
                                    "<span class='fare_value'>Rs. <b class='fare_service_tax'>NA</b></span>" +
                                    "</div>" +
                                    "<div class='amount clearfix'>" +
                                    "<label>Service Charge : </label><span class='fare_value'>Rs. <b class='fare_service_charge'>NA</b></span>" +
                                    "</div>" +
                                    "<div class='amount clearfix'>" +
                                    "<label>Sub Total : </label><span class='fare_value'>Rs. <b class='tot_fare' id='total_fare'>0</b></span>" +
                                    "</div>" +
                                    "<div class='amount clearfix total_fare_discount_div hide'>" +
                                    "<label>Discount : </label><span class='fare_value'>Rs. - <b class='total_fare_discount_value'>0</b></span>" +
                                    "</div>" +
                                    "<div>" +
                                    "<p id='birth' class='birth' style='display:none'></p>" +
                                    "<p id='gender' class='gender' style='display:none'></p>" +
                                    "<p id='is_ladies' class='is_ladies' style='display:none'></p>" +
                                    "<p id='sleeper' class='sleeper' style='display:none'></p>" +
                                    "<p id='ac' class='ac' style='display:none'></p>";

                            row += "<p id='seat_fare' class='seat_fare' style='display:none'></p>" +
                                    "<p id='totalFare' class='totalFare' style='display:none'></p>" +
                                    "<p class='seat_details1' id='seat_details1' style='display:none'></p>";

                            row += "<div class='dropdown' style='display:none'><select class='form-control boarding' id='from_stop_" + result[i].service_id + "'><option value=''> Selet Boarding stop</option></select>";

                            row += "<select class='form-control dropping' id='to_stop_" + result[i].service_id + "'><option value=''> Selet Dropping stop</option></select></div>";


                            //upsrtc condition
                            /*row +='<div><select class="form-control boarding" id="from_stop_'+result[i].service_id+'"><option value='+result[i].from_stop+'>'+result[i].from_stop+'</option></select>';
                             
                             row += '<select class="form-control dropping" id="to_stop_'+result[i].service_id+'"><option value='+result[i].to_stop+'>'+result[i].to_stop+'</option></select></div>';*/

                            row += "<div><p id='from_b_stop' class='from_b_stop' style='display:none;'></p><p id='to_d_stop' class='to_d_stop' style='display:none;'></p></div><p class='service_id' style='display:none;'>" + result[i].service_id + "</p><p id='from_stop_name' class='from_stop_name' style='display:none;'></p><p id='to_stop_name' class='to_stop_name' style='display:none;'></p><button class='button btn-medium continue' value='continue'>Continue</button>";

                            row += "</div></div></div></form>";

                            row += "</div>";
                        }

                        $("#bus_services").html(row);

                        /*var pickups = data.result[0]['Pickups'];
                         var dropoffs = data.result[0]['Dropoffs'];
                         
                         alert(pickups);*/


                    },
                });
            }
            return false;
        });

        /**************bus_services ******************/

        /**on click select seat hide show dropdown*/
        $(document).on('click', 'a[name=tab]', function() {

            service_id = $(this).data("service-id");

            var $this = $(this);

            $this.toggleClass('selectseat2');
            if ($this.hasClass('selectseat2')) {
                $this.text('Select Seat');
            } else
            {
                $this.text('Hide Seat');
            }
            $("#dropdown_" + service_id).toggle();

        });

        /**on click select seat hide show dropdown*/

        /*bus service stop details start*/

        $(document).on('click', 'a[name=tab]', function() {
            var op_name = $(this).data("op-name");
            if (op_name == 'MSRTC')
            {
                var service_id = $(this).data("service-id");
                var provider_id = $(this).data("provider-id");
                var date = $(this).data("date");
                var url = base_url + "admin/Search_bus/get_service_stop_details";
                var dataString = 'service_id=' + service_id + '&provider_id=' + provider_id + '&date=' + date;
            } else {
                $('.dropdown').css("display", "block");
                var service_id = $(this).data("service-id");
                var provider_id = $(this).data("provider-id");
                var date = $(this).data("date");
                var from = $(this).data("from");
                var to = $(this).data("to");
                var trip_id = $(this).data("trip-id");
                var inventory_type = $(this).data("inventory-type");
                /* var boarding_stop =
                 var dropping_stop =*/
                var url = base_url + "admin/Search_bus/get_seat_layout";

                var dataString = 'service_id=' + service_id + '&provider_id=' + provider_id + '&date=' + date + '&from=' + from + '&to=' + to + '&trip_id=' + trip_id + '&inventory_type=' + inventory_type + '&operator_name=' + op_name;


            }

            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: dataString,
                success: function(data) {
                    //alert(data.msg);

                    if (data.from && data.to)
                    {
                        var result = data.from;
                        var result1 = data.to;
                        var row = "";
                        row += "<option value='0'>Select Boarding Stop</option>";
                        for (i = 0; i < result.length; i--) {
                            row += "<option value='" + result[i].from_bus_stop_name + "'>" + result[i].from_bus_stop_name + " - " + result[i].from_time + "</option>";
                        }


                        var row1 = "";
                        row1 += "<option value='0'>Select Dropping Stop</option>";
                        for (j = 0; j < result1.length; j++) {
                            row1 += "<option value='" + result1[j].to_bus_stop_name + "'>" + result1[j].to_bus_stop_name + " - " + result1[j].to_time + "</option>";
                        }

                        $("#from_stop_" + service_id).html(row);
                        $("#to_stop_" + service_id).html(row1);
                    }
                    else
                    {
                        if (op_name == 'Travelyaari')
                        {
                            var result = data.seats[0];
                        } else
                        {
                            var result = data.seats;
                        }
                        var row = "";
                        var row1 = "";

                        for (i = 0; i < result.length; i++) {

                            var ac_seat = (result[i].ac) ? result[i].ac : "True";
                            var ladies_seat = (result[i].ladies_seat) ? result[i].ladies_seat : "false";
                            var sleeper = (result[i].sleeper) ? result[i].sleeper : "True";
                            var berth = (result[i].berth) ? result[i].berth : "0";
                            var total_fare = (result[i].total_fare) ? (result[i].total_fare) : (result[i].sub_total_fare);
                            if (ladies_seat == 'false')
                            {
                                var gender = 'M';
                            }
                            else
                            {
                                var gender = 'F';
                            }



                            if (result[i].available == 'Y')
                            {
                                var class_name = 'available';
                                var Sstatus = 'S'
                            }
                            else
                            {
                                var class_name = 'booked';
                                var Sstatus = 'N'
                            }
                            row += "<li><a href='javascript:void(0)' class=" + class_name + " data-seat-no=" + result[i].seat_no + " data-fare=" + result[i].sub_total_fare + " data-total-fare=" + total_fare + " title='Seat No" + result[i].seat_no + " | Fare:Rs." + result[i].total_fare + "' data-ac=" + ac_seat + " data-sleeper=" + sleeper + " data-ladies-seat=" + ladies_seat + " data-berth=" + berth + " data-gender=" + gender + ">" + Sstatus + "</a></li>";

                        } //end for loop
                        $("#fare_tab_filter_id_" + service_id).html(row);
                        $('.seat_detail_info_' + service_id).css("display", "block");

                        if (op_name == 'UPSRTC')
                        {
                            var boarding_stops = data.boarding_stops;
                            var dropping_stops = data.Dropoffs;
                            var board_stops = '';
                            var drop_stops = '';

                            // board_stops +="<option value='0'>Select Boarding Stop</option>";
                            // for(b=0; b < boarding_stops.length; b++) { 
                            board_stops += "<option value='" + from + "'>" + from + "</option>";
                            //}
                            $("#from_stop_" + service_id).html(board_stops);

                            //drop_stops +="<option value='0'>Select Dropping Stop</option>";
                            //for(d=0; d < dropping_stops.length; d++) { 
                            drop_stops += "<option value='" + to + "'>" + to + "</option>";
                            // }
                            $("#to_stop_" + service_id).html(drop_stops);
                        }
                        else
                        {

                            //RSRTC Route
                            var boarding_stops = data.boarding_stops;
                            var dropping_stops = data.Dropoffs;
                            var board_stops = '';
                            var drop_stops = '';

                            board_stops += "<option value='0'>Select Boarding Stop</option>";
                            for (b = 0; b < boarding_stops.length; b++) {
                                board_stops += "<option value='" + boarding_stops[b].PickupName + "' data-bsd='" + boarding_stops[b].PickupCode + "' data-stop='" + boarding_stops[b].PickupName + "'>" + boarding_stops[b].PickupName + " - " + boarding_stops[b].PickupTime + "</option>";
                            }
                            $("#from_stop_" + service_id).html(board_stops);

                            drop_stops += "<option value='0'>Select Dropping Stop</option>";
                            for (d = 0; d < dropping_stops.length; d++) {
                                drop_stops += "<option value='" + dropping_stops[d].DropoffName + "' data-dsd='" + dropping_stops[d].DropoffCode + "' data-stop='" + dropping_stops[d].DropoffName + "'>" + dropping_stops[d].DropoffName + " - " + dropping_stops[d].DropoffTime + "</option>";
                            }
                            $("#to_stop_" + service_id).html(drop_stops);
                        }





                    }




                },
                error: function(data) {
                    // do something
                }
            });

        });
        /*bus service stop details end*/

        /*validation for select dropdown*/
        $(document).on('click', '.get_seat', function() {
            service_id = $(this).data("service-id");

            var error = 0;
            var from_stop = $('#from_stop_' + service_id).val();
            var to_stop = $('#to_stop_' + service_id).val();

            if (from_stop == '0') {
                error = 1;
                alert('You should select a boarding stop.');
            }
            else if (to_stop == '0') {
                error = 1;
                alert('You should select a dropping stop.');
            }

            if (error) {
                return false;
            } else {
                return true;
            }

        });

        /*validation for select dropdown*/

        /*seat layout start*/
        $(document).on('click', '.get_seat', function() {
            var service_id = $(this).data("service-id");
            var provider_id = $(this).data("provider-id");
            var date = $(this).data("date");
            var from = $("#from_stop_" + service_id).val();
            var to = $("#to_stop_" + service_id).val();
            var trip_id = $(this).data("trip-id");
            var inventory_type = $(this).data("inventory-type");
            var op_name = $(this).data("op-name");

            var url = base_url + "admin/Search_bus/get_seat_layout";

            var dataString = 'service_id=' + service_id + '&provider_id=' + provider_id + '&date=' + date + '&from=' + from + '&to=' + to + '&trip_id=' + trip_id + '&inventory_type=' + inventory_type + '&operator_name=' + op_name;

            // {"service_id" : service_id, "provider_id" : provider_id, "date" : date, "from" : from, "to" : to, "trip_id" : trip_id, "inventory_type" : inventory_type},
            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                data: dataString,
                success: function(data) {
                    var result = data.seats;
                    var row = "";
                    var row1 = "";
                    for (i = 0; i < result.length; i++) {
                        if (result[i].available == 'Y')
                        {
                            var class_name = 'available';
                            var Sstatus = 'S'
                        }
                        else
                        {
                            var class_name = 'booked';
                            var Sstatus = 'N'
                        }

                        var total_fare = (result[i].total_fare) ? (result[i].total_fare) : (result[i].sub_total_fare);
                        var ladies_seat = (result[i].ladies_seat) ? result[i].ladies_seat : "false";

                        if (ladies_seat == 'false')
                        {
                            var gender = 'M';
                        }
                        else
                        {
                            var gender = 'F';
                        }

                        row += "<li><a href='javascript:void(0)' class=" + class_name + " data-seat-no=" + result[i].seat_no + " data-fare=" + result[i].sub_total_fare + " data-total-fare=" + total_fare + " title='Seat No" + result[i].seat_no + " | Fare:Rs." + result[i].total_fare + "' data-ac=" + result[i].ac + " data-sleeper=" + result[i].sleeper + " data-ladies-seat=" + ladies_seat + " data-berth=" + result[i].berth + " data-gender=" + gender + " >" + Sstatus + "</a></li>";


                        /*  row += "<li><a href='javascript:void(0)' class="+class_name+" data-trip-id="+trip_id+" data-seat-no="+result[i].seat_no +" data-fare="+result[i].total_fare+" title=Seat No"+result[i].seat_no+'| Fare: Rs.'+result[i].total_fare+'c*r:'+ result[i].pos +" data-berth="+result[i].berth+" data-ac="+result[i].ac+" data-sleeper="+result[i].sleeper+" data-ladies-seat="+result[i].ladies_seat+" >"+Sstatus+"</a></li>";*/

                    }
                    $("#fare_tab_filter_id_" + service_id).html(row);
                    $('.seat_detail_info_' + service_id).css("display", "block");




                },
                error: function(data) {
                    // do something
                }
            });


        });
        /*seat layout start*/

        /* block seat code*/
        var count_seats = [];
        var count_gender_chk = [];
        var count_gender = [];
        var count_sleeper = [];
        var count_ac = [];
        var count_birth = [];
        var count_data_fare = [];

        $('body').on('click', ".available", function() {

            var curtSeatFare = parseInt($('#get_fare_id').text()); // get total fare value
            var curtTotalFare = parseInt($('#total_fare').text()); // get total fare value

            var data_seat_no = $(this).attr('data-seat-no'); // get set no

            var data_fare = $(this).attr('data-fare'); // get fare value;
            var total_fare = $(this).attr('data-total-fare'); // get fare value;

            var gender = $(this).attr('data-ladies-seat');
            var sleeper = $(this).attr('data-sleeper');
            var ac = $(this).attr('data-ac');
            var birth = $(this).attr('data-berth');

            count_seats.push($(this).data('seat-no'));
            count_gender.push($(this).data('gender'));


            if (gender == 'false')
            {
                var gender_chk = 'M';
            }
            else
            {
                var gender_chk = 'F';
            }

            SumOfSeatFare = parseInt(data_fare) + curtSeatFare; // addition of seat fare value
            SumOfTotalFare = parseInt(total_fare) + curtTotalFare; // addition of titol fare value




            //$(".sel_seat").append(data_seat_no+',');
            $(".sel_seat").html(count_seats.join(","));
            $(".fare").html(SumOfSeatFare); // set fare value 
            $(".tot_fare").html(SumOfTotalFare); // set fare value 
            $(".gender").html(count_gender.join(","));
            $(".is_ladies").append(gender + ',');
            $(".sleeper").append(sleeper + ',');
            $(".ac").append(ac + ',');
            $(".birth").append(birth + ',');
            $(".seat_fare").append(data_fare + ',');
            $(".totalFare").append(total_fare + ',');



            $('[data-seat-no=' + data_seat_no + ']').addClass('selected');
            $('[data-seat-no=' + data_seat_no + ']').removeClass('available');


        });

        $('body').on('click', ".booked", function() {
            alert('Seat already booked Try another seat.');

        });

        $('body').on('click', ".selected", function() {
            //var add_class = $(this).attr('class') ;

            var removeItem = 31;



            var data_seat_no = $(this).attr('data-seat-no'); // get set no
            /*//y.splice( $.inArray(removeItem,y) ,1 );
             data_seat_no.splice( $.inArray(removeItem,data_seat_no) ,1 );
             
             alert(data_seat_no);*/

            var data_fare = $(this).attr('data-fare'); // get fare value;

            $('[data-seat-no=' + data_seat_no + ']').removeClass('selected');
            $('[data-seat-no=' + data_seat_no + ']').addClass('available');



            var seat_nos = $("#get_seat_no").text();

            var get_total_fare = parseInt($('#total_fare').text());

            var bal_fare = get_total_fare - parseInt(data_fare);

            $(".fare").html(bal_fare); // set fare value 
            $(".tot_fare").html(bal_fare); // set fare value  

            $("#get_seat_no").empty();
            $("#gender").empty();
            $("#is_ladies").empty();
            $("#sleeper").empty();
            $("#ac").empty();

        });


        /* block seat code*/

        /***/

        $('body').on('change', 'select.boarding', function() {
            var selectedStopCode = $(this).find(':selected').data('bsd');
            var selectedStopName = $(this).find(':selected').data('stop');

            $(".from_b_stop").html(selectedStopCode);
            $(".from_stop_name").html(selectedStopName);
        });




        $('body').on('change', 'select.dropping', function() {
            var selectedStopCode = $(this).find(':selected').data('dsd');
            var selectedStopName = $(this).find(':selected').data('stop');

            $(".to_d_stop").html(selectedStopCode);
            $(".to_stop_name").html(selectedStopName);
        });

        /***/

        /*temp booking on click continue*/
        $('body').on('click', ".continue", function() {
            var button_val = $(this).val();

            event.preventDefault();
            var form_url = base_url + "admin/Search_bus/save_temp_booking";

            /*var provider_id = $(this).attr('data-provider-id'); */
            var seat_no = $("#get_seat_no").text();
            var sub_total = $('#get_fare_id').text();
            var total_fare = $('#total_fare').text();
            var birth = $('#birth').text();
            var gender_list = $('#gender').text();
            var is_ladies_list = $('#is_ladies').text();
            var sleeper = $('#sleeper').text();
            var is_ac = $('#ac').text();
            // var seat_info= $('#seat_details1').text();
            var sum_of_seat_fare = $('#seat_fare').text();
            var sum_of_total_fare = $('#totalFare').text();
            var data_bsd = $('#from_b_stop').text(); // get boarding stop
            var data_dsd = $('#to_d_stop').text(); // get dropping stop
            var from_stop_name = $('#from_stop_name').text(); // get dropping stop
            var to_stop_name = $('#to_stop_name').text(); // get dropping stop
            var dept_date = $('#dept_date').text(); // get dropping date
            var arr_date = $('#arr_date').text(); // get arrival date
            var bus_name = $('#bus_name').text(); // get bus name




            var return_date = $("#return_date_shown").text();
            //alert('return_date'+return_date);

            $.ajax({
                type: "POST",
                dataType: "json",
                url: form_url,
                data: {"seat_no": seat_no, 'birth': birth, 'gender': gender_list, 'is_ladies': is_ladies_list, "sleeper": sleeper, "ac": is_ac, "boarding_stop": data_bsd, "dropping_stop": data_dsd, "sum_of_seat_fare": sum_of_seat_fare, "sum_of_total_fare": sum_of_total_fare, "from_stop_name": from_stop_name, "to_stop_name": to_stop_name, "dept_date": dept_date, "arr_date": arr_date, "bus_name": bus_name},
                success: function(data) {

                    //loadUrl = base_url+"admin/search_bus/do_temp_booking";
                    /**** return jurney code start ****/
                    /*if(return_date!='')
                     {  
                     //console.log(return_date) ;
                     var from_stop = $("#return_show_from").text();
                     var to_stop = $("#return_show_to").text();     
                     var departure_date = $("#date_shown").text();           
                     var return_date = $("#return_date_shown").text();
                     var url = base_url+"admin/search_bus/bus_services";
                     
                     // window.location.href = url;
                     
                     
                     // AJAX Code To Submit Form.
                     $.ajax({
                     type: "POST",
                     dataType: "json",
                     url: url ,
                     data: {'from_stop':from_stop,'to_stop':to_stop,'departure_date':departure_date,'return_date':return_date,'button_val':button_val},                
                     success: function(data){
                     //location.reload();
                     //console.log(data);
                     
                     /*****
                     var result = data.result;
                     
                     var row = "";
                     
                     for(i=0; i < result.length; i++) {   
                     
                     var inventory_type = (result[i].inventory_type) ? result[i].inventory_type : "0";
                     
                     var company_name = (result[i].company_name) ?  (result[i].company_name) : " ";                     
                     var bus_name =  (result[i].bus_type_name+'/'+result[i].op_bus_type_name) ? (result[i].bus_type_name+'/'+result[i].op_bus_type_name) : ' ttt';
                     
                     row += "<div class='form-group'><table><tr><td>"+company_name+"</td></tr><tr><td>Operator Name:</td><td> "+result[i].operator_name+"</td></tr><tr><td>From:</td><td>"+result[i].from_stop+"</td></tr><tr><td>To:</td><td>"+result[i].to_stop+"</td></tr><tr><td>Bus Name:</td><td>"+bus_name+"</td></tr><tr><td>Departure Name:</td><td>"+result[i].departure_date+"</td></tr><tr><td>Arrival Name:</td><td>"+result[i].arrival_date+"</td></tr><tr><td>Available seat:</td><td>"+result[i].available_seat+"</td></tr><tr><td>Price:</td><td>"+result[i].seat_fare_with_tax+"</td></tr></table></div><div class='form-group'><div class='col-lg-offset-4'><a href='javascript:void(0)' name='tab' class='btn btn-primary selectseat2' data-trip-id="+result[i].trip_id+" data-service-id="+result[i].service_id+" data-provider-id="+result[i].provider_id+" data-inventory-type="+inventory_type+" data-date='"+result[i].departure_date+"' data-from='"+result[i].from_stop+"' data-to='"+result[i].to_stop+"' data-op-name='"+result[i].operator_name+"'>Select Seat</a> </div></div>";
                     
                     if(result[i].operator_name=='MSRTC'){
                     
                     row += "<div class='form-group' style='display:none' id='dropdown_"+result[i].service_id+"'><div class='col-lg-4'><select class='form-control' id='from_stop_"+result[i].service_id+"'><option value=''> Selet Boarding stop</option></select></div><div class='col-lg-4'><select class='form-control' id='to_stop_"+result[i].service_id+"'><option value=''> Selet Dropping stop</option></select></div><div class='col-lg-4'><a href='javascript:void(0)' class='btn btn-primary get_seat ' data-trip-id="+result[i].trip_id+" data-service-id="+result[i].service_id+" data-provider-id="+result[i].provider_id+" data-inventory-type="+result[i].inventory_type+" data-date='"+result[i].departure_date+"' data-from='"+result[i].from_stop+"' data-to='"+result[i].to_stop+"' >Select Seat</a></div></div>";
                     
                     }
                     
                     row += "<div class='form-group'><span class='bus_seat_layout'><div class='col-md-7'><div class='fare_tab_filter show'><ul id='fare_tab_filter_id_"+result[i].service_id+"' class='seat'></ul></div></div></span>";
                     
                     row +=" <form method='post' action='#'>"+
                     "<div class='col-md-4 pull-right seat_detail_info_"+result[i].service_id+"' style='display:none';>"+
                     "<div class='col-md-12 tprice'>"+
                     "<div class='SelectedSeats clearfix'>"+
                     "<label>Seat(s)  :</label>"+
                     "<span class='sel_seat' id='get_seat_no'></span>"+
                     "</div>"+
                     "<div class='amount clearfix'>"+
                     "<label>Base Fare : </label>"+
                     "<span class='fare_value'>Rs. <b class='fare' id='get_fare_id'>0</b></span>"+
                     "</div>"+
                     "<div class='amount clearfix'>"+
                     "<label>Service Tax : </label>"+
                     "<span class='fare_value'>Rs. <b class='fare_service_tax'>NA</b></span>"+
                     "</div>"+
                     "<div class='amount clearfix'>"+
                     "<label>Service Charge : </label><span class='fare_value'>Rs. <b class='fare_service_charge'>NA</b></span>"+
                     "</div>"+
                     "<div class='amount clearfix'>"+
                     "<label>Sub Total : </label><span class='fare_value'>Rs. <b class='tot_fare' id='total_fare'>0</b></span>"+
                     "</div>"+
                     "<div class='amount clearfix total_fare_discount_div hide'>"+
                     "<label>Discount : </label><span class='fare_value'>Rs. - <b class='total_fare_discount_value'>0</b></span>"+
                     "</div>"+
                     "<div>"+
                     "<p id='birth' class='birth' style='display:none'></p>"+
                     "<p id='gender' class='gender'></p>"+
                     "<p id='is_ladies' class='is_ladies'></p>"+
                     "<p id='sleeper' class='sleeper'></p>"+
                     "<p id='ac' class='ac'></p>";
                     
                     row +=  "<p id='seat_fare' class='seat_fare'></p>"+
                     "<p id='totalFare' class='totalFare'></p>"+
                     "<p class='seat_details1' id='seat_details1'></p>";
                     
                     row +="<div class='dropdown' style='display:none'><select class='form-control boarding' id='from_stop_"+result[i].service_id+"'><option value=''> Selet Boarding stop</option></select>";
                     
                     row += "<select class='form-control dropping' id='to_stop_"+result[i].service_id+"'><option value=''> Selet Dropping stop</option></select></div>";
                     
                     
                     //upsrtc condition
                     /*row +='<div><select class="form-control boarding" id="from_stop_'+result[i].service_id+'"><option value='+result[i].from_stop+'>'+result[i].from_stop+'</option></select>';
                     
                     row += '<select class="form-control dropping" id="to_stop_'+result[i].service_id+'"><option value='+result[i].to_stop+'>'+result[i].to_stop+'</option></select></div>';*/

                    /*
                     row +="<div><p id='from_b_stop' class='from_b_stop'></p><p id='to_d_stop' class='to_d_stop'></p></div><p class='service_id'>"+result[i].service_id+"</p><p id='from_stop_name' class='from_stop_name'></p><p id='to_stop_name' class='to_stop_name'></p><button class='button btn-medium continue' value='continue'>Continue</button>";
                     
                     row +="</div></div></div></form>";
                     
                     row +="</div>";
                     }
                     
                     $("#bus_services").html(row);
                     /*****
                     
                     },
                     
                     });
                     
                     /*$(document).ajaxSuccess(function() {
                     alert("Triggered ajaxSuccess handler." );
                     });*/
                    /* }
                     else
                     {
                     // console.log(data.redirect);
                     
                     
                     
                     }*/
                    /**** return jurney code end ****/
                    window.location.href = data.redirect;

                },
                error: function(data) {
                    // do something
                }
            });

        });

        /*temp booking*/





















    });




</script>
