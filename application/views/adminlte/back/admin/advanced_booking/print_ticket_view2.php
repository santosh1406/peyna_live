<!-- Created by Sonali Kamble on 20-august-2018 -->
<link href="<?php echo base_url() . ASSETS ?>common/css/bootstrap/bootstrapValidator.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() . ASSETS ?>common/css/bootstrap/bootstrapValidator.min.css" rel="stylesheet" type="text/css" />
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> 
                        <h3 class="box-title">Print Ticket</h3>
                    </div>
                    <div class="box-body " id="form">
                        
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label">Refrence no *:</label>
                            <div class="col-lg-6">
                                <input type="text" id="refrence_no" name="refrence_no" class="form-control" placeholder=""/>
                            </div>
                        </div> 
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label">email id *:</label>
                            <div class="col-lg-6">
                                <input type="text" id="email" name="email" class="form-control" placeholder="" />
                            </div>
                        </div>
                       
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">
                                <button class="btn btn-primary" id="submit" value="submit" type="submit">Print</button>                              
                            </div>
                        </div>
                        
                    </div>
                    <div class="test box-body">
                    <div id="test">
                    <?php 
                    if($result)
                        {
                            foreach ($result as $key => $value) {
                                echo "<table><tr><td colspan='4'>Trip information</td></tr><tr><td>Trip Number:</td><td>406614</td></tr><tr><td>Ticket Number:</td><td>".$value->pnr_no."</td></tr><tr><td>Service Start Place:</td><td>Service End Place:</td><td></td></tr><tr><td>Passenger Boarding Point :</td><td>".$value->boarding_stop_name."</td><td>Passenger Alighting Point:</td><td>".$value->destination_stop_name."</td></tr><tr><td>Date of Journey</td><td>".$value->inserted_date."</td><td>Departure time from Starting Place:</td><td></td></tr><tr><td>Bus Service Type :</td><td></td><td>No. of Seats :</td><td>1</td></tr><tr><td colspan='4'>Passengers Information</td></tr><tr><td>Name</td><td>".$value->psgr_name."</td><td>Age</td><td>".$value->psgr_age."</td><td>Sex</td><td>".$value->psgr_sex."</td><td>Seat NO</td><td>".$value->seat_no."</td></tr><tr><td colspan='4'>Total Fare Details</td></tr><tr><td>Basic Fare</td><td>Rs.".$value->tot_fare_amt."</td></tr><tr><td>Reservation Charge</td><td>Rs.".$value->fare_reservationCharge."</td></tr><tr><td>ASN Amount :</td><td>Rs.".$value->asn_fare."</td></tr><tr><td>Total Chargeable Amount :</td><td>Rs.".$value->total_fare."</td></tr><tr><td colspan='4'>Bookonspot eTicket Information</td></tr><tr><td>BOS Ref. No.:</td><td colspan='2'>".$value->boss_ref_no."</td><td> Email:</td><td> ".$value->user_email_id."</td></tr><tr><td> Mobile No:</td><td> ".$value->mobile_no."</td></tr></table>";
                            }//end foreach
                        }
                    ?>
                        	<!-- <table>
                        	<tr>
                        	<td>
                        	reference no : 6b49dc59</td></tr>
        <tr><td>email :rohitesh@justclickkaro.com</td></tr>
				<tr><td>	mobile no : 8468862808</td></tr>
					
				<tr><td>	From stop :Kurla nehru nagar</td></tr>
				<tr><td>	from stop : Swargate, pune</td></tr>
				<tr><td>	Amount =>270</td></tr>
				<tr><td>	transaction_status=> success</td></tr>	
				<tr><td>no of passenger=> 1</td></tr>
				</table> -->				
					
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    
   /* $('#bus_booking').bootstrapValidator({

        group: '.form-control',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            from: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b>Please enter from stop.</font>'
                    }
                }
            },
            to: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b>Please enter to stop.</font>'
                    }
                }
            },
            date: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1">X</b> Please enter departure date.</font>'
                    }                    
                }
            },
            
        }
    });*/

    /*bus service stop details start*/

	/*bus service stop details end*/


	$(document).ready(function(){
$("#submit").click(function(){

var email = $("#email").val();
var refrence_no = $("#refrence_no").val();

// Returns successful data submission message when the entered information is stored in database.
var dataString = 'refrence_no1='+ refrence_no + '&email1='+ email;
if(refrence_no=='')
{
alert("Please Fill All Fields");
}
else
{
// AJAX Code To Submit Form.
$.ajax({
type: "POST",
dataType: "json",
url: base_url+"admin/Search_bus/do_print_ticket",
data: dataString,
cache: false,
success: function(data){
        console.log(data);

		/**********/
		var result = data.result;
			var row = "";
			var row1= "";
			for(i=0; i < result.length; i++) {
				 
					
				row += "<table><tr><td colspan='4'>Trip information</td></tr><tr><td>Trip Number:</td><td>406614</td></tr><tr><td>Ticket Number</td><td>"+result[i].pnr_no+"</td></tr><tr><td>Service Start Place:</td><td>Service End Place:</td><td></td></tr><tr><td>Passenger Boarding Point :</td><td>"+result[i].boarding_stop_name+"</td><td>Passenger Alighting Point:</td><td>"+result[i].destination_stop_name+"</td></tr><tr><td>Date of Journey</td><td>"+result[i].inserted_date+"</td><td>Departure time from Starting Place:</td><td></td></tr><tr><td>Bus Service Type :</td><td></td><td>No. of Seats :</td><td>1</td></tr><tr><td colspan='4'>Passengers Information</td></tr><tr><td>Name</td><td>"+result[i].psgr_name+"</td><td>Age</td><td>"+result[i].psgr_age+"</td><td>Sex</td><td>"+result[i].psgr_sex+"</td><td>Seat NO</td><td>"+result[i].seat_no+"</td></tr><tr><td colspan='4'>Total Fare Details</td></tr><tr><td>Basic Fare</td><td>Rs."+result[i].tot_fare_amt+"</td></tr><tr><td>Reservation Charge</td><td>Rs."+result[i].fare_reservationCharge+"</td></tr><tr><td>ASN Amount :</td><td>Rs."+result[i].asn_fare+"</td></tr><tr><td>Total Chargeable Amount :</td><td>Rs."+result[i].total_fare+"</td></tr><tr><td colspan='4'>Bookonspot eTicket Information</td></tr><tr><td>BOS Ref. No.:</td><td>"+result[i].boss_ref_no+"</td><td> Email:</td><td> "+result[i].user_email_id+"</td></tr><tr><td> Mobile No:</td><td> "+result[i].mobile_no+"</td></tr>";			
				
				
			} 

		   
		   $("#test").html(row);
		/*************/
}
});
}
return false;
});
}); 
</script>