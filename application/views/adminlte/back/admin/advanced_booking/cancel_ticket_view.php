<!-- Inner Wrapper Start -->
<link href="<?php echo base_url() . BACK_ASSETS ?>css/search_style/style.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/qrcode.js" type="text/javascript"></script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/jquery.qrcode.js" type="text/javascript"></script>
<div id="page-wrapper">

    <!-- Signin Content Start -->
    <section class="inner-wrapper">
        <div class="container">
            <div class="row">
                <!-- col-md-offset-3 -->
                <div class="col-md-4 col-md-offset-3">
                    <h3 class="text-center purple">Cancel Ticket</h3>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h4 class="text-center">Trips you have booked!</h4>
                            <div class="col-md-12 col-sm-12 container-c">
                                <div class="formBox">
                                    <?php echo validation_errors(); ?>
                                    <form action="<?php echo base_url() . 'admin/Search_bus/check_ticket_details'; ?>" method="POST" id="cacelTicket">                                
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                <div class="inputBox">
                                                    <!--<div class="inputText">Email Address</div>-->
                                                    <input type="text" id="email" name="email" class="input" placeholder="Email Address">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                <div class="inputBox">
                                                    <!--<div class="inputText"></div> -->
                                                    <input type="text" id="rokadReference" name="rokadReference" class="input" placeholder="Rokad Reference Number">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                <div class="text-center center-block">OR</div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                <div class="inputBox">
                                                    <!--<div class="inputText">PNR Number</div> -->
                                                    <input type="text" id="pnrNumber" name="pnrNumber" class="input" placeholder="PNR Number">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6 col-xs-12 col-sm-offset-3">
                                                <button type="submit" class="btn btn-purple btn-lg btn-block login-button txt-w col-sm-4">Show Ticket</button>
                                            </div>
                                        </div>
                                    </form>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>

    <!-- Signin Content End -->             
</div>
<!-- Inner Wrapper End -->