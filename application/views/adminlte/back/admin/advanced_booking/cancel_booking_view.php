<!-- Created by Sonali Kamble on 17-sept-2018 -->

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> 
                        <h3 class="box-title">Request Refund</h3>
                    </div>
                    

                    <div class="box-body col-xs-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email Address</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" value="">
                        </div>
                        <div class="form-group">
                            <label for="bos_ref_no">BOS REFERENCE NUMBER</label>
                            <input type="text" class="form-control" id="bos_ref_no" name="bos_ref_no" placeholder="BOS REFERENCE NUMBER" value="" >
                        </div>
                        OR
                        <div class="form-group">
                            <label for="bos_ref_no">PNR NO</label>
                            <input type="text" class="form-control" id="pnr_no" name="pnr_no" placeholder="PNR NO" value="" >
                        </div>
                        <div class="form-group">
                            <label for="">&nbsp;</label>
                            <button type="button" class="button btn-medium <?php echo THEME_BTN;?>" name="show_ticket" id="show_ticket">Show Ticket</button>
                        </div>
                    </div>

                    <div class="box-body col-xs-4" id="ticket_details">
                       
                    </div>

                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $("#show_ticket").click(function(){
        var email = $("#email").val();
        var boss_ref_no = $("#bos_ref_no").val();
        var pnr_no = $("#pnr_no").val(); 
      
        var dataString = 'email1='+ email + '&boss_ref_no1='+ boss_ref_no + '&pnr_no1=' + pnr_no ;
            if(email=='')
            {
            alert("Please Fill All Fields");
            }
            else
            {
            // AJAX Code To Submit Form.
                    $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: base_url+"admin/Search_bus/check_ticket_details",
                    data: dataString,
                    cache: false,
                    success: function(data){
                         /**********/
                        var result = data.ticket_details;                       
                        var row = "";
                        
                        for(i=0; i < result.length; i++) {
                            row += "<div><h3>Trips You have Booked</h3></div><div><span>"+result[i].dept_time+"</span><spa>"+result[i].boarding_stop_name + " to "+result[i].destination_stop_name+"</span><div><span>PNR NO"+result[i].pnr_no+"TOTAL FARE"+result[i].tot_fare_amt+"BOS REF NO"+result[i].boss_ref_no+"</div>"; 

                            row +="<div><span><button type='button' class='is_cancel' data-reference-no="+result[i].boss_ref_no+">Cancellable Status</button></span>&nbsp;&nbsp;<span><button type='button' class='cancel_ticket' data-reference-no="+result[i].boss_ref_no+">Cancel Ticket</button></span></div>";                             
                        }                        
                       $("#ticket_details").html(row);
                    /*************/
                    }
                    });

            }
        return false;
        });
        // AJAX Code To Submit Form.

        /***is cancellable ticket***/
        $('body').on('click', ".is_cancel", function(){
        var bos_key=$(this).data("reference-no");          
        var dataString = 'bos_key='+ bos_key;
            // AJAX Code To Submit Form.
         $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: base_url+"admin/Search_bus/is_ticket_cancel",
                    data: dataString,
                    cache: false,
                    success: function(data){
                        //alert(data.status);
                      if(data.status=="success")
                      {
                        confirm(data.msg+' Please confirm cancellation\n\nRefund Amount Rs.' + data.data.refundAmount+'\nCancellation charges Rs.'+data.data.cancellationCharge);
                      }
                      else
                      {
                        confirm(data.msg)
                      }
                       
                      
                    }
            });
        
        });
        /***is cancellable ticket***/

        /***ticket cancel***/
         $('body').on('click', ".cancel_ticket", function(){
        var bos_key=$(this).data("reference-no");          
        var dataString = 'bos_key='+ bos_key;
            // AJAX Code To Submit Form.
         $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: base_url+"admin/Search_bus/cancel_ticket_details",
                    data: dataString,
                    cache: false,
                    success: function(data){
                        alert(data);
                      //confirm(data.msg+' Please confirm cancellation\n\nRefund Amount Rs.' + data.data.refundAmount+'\nCancellation charges Rs.'+data.data.cancellationCharge);
                   
                    }
            });
        
        });
        /***ticket cancel***/

                    

        
}); 
</script>
            
    