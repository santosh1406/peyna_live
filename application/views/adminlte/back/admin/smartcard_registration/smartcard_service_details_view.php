<!--<link href="<?php echo base_url() . COMMON_ASSETS ?>js/bootstrap/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() . COMMON_ASSETS ?>js/bootstrap/bootstrap-select.js" type="text/javascript"></script>-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">-->

<style>
    //added by harshada kulkarni on 12-07-2018
    .overflowClass{
        //border: 1px solid #ccc;
        height: 50px;
        width: 48.5%;
        overflow-x:scroll;
        overflow-x:hidden;
    }
</style>

<section class="content-header">
        <h2 style="margin-left:20px">Pass Details- Service Pass(Normal)</h2>
</section>
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> </div>
                    <div class="box-body">
                        <?php echo validation_errors('<div class="error">', '</div>'); ?>
                        <?php
                        $attributes = array("method" => "POST", "id" => "sc_registration_form", "name" => "sc_registration_form");
                        echo form_open(base_url() . 'admin/registration/sc_registration_cardtypeplan', $attributes);
                        ?>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="Concession_Type">Concession Type *:</label>
                            <div class="col-lg-6">
                               <select class="form-control " id="concession_Type"  name="concession_Type">
                                    <option value="">Select Concession Type</option>
                                    <option value="M">Male</option>
                                    <option value="F">Female</option>
                                    <option value="T">Transgender</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="proof_type">Proof Type *:</label>
                            <div class="col-lg-6">
                               <select class="form-control " id="proof_type"  name="proof_type">
                                    <option value="">Select Proof Type</option>
                                    <option value="M">Male</option>
                                    <option value="F">Female</option>
                                    <option value="T">Transgender</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="proof_type">Proof Reference No *:</label>
                            <div class="col-lg-6">
                                     <input name="proof_reference" type="text" id="proof_reference" value="<?php echo $middle_name; ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>
                           
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="proof_type">Pass validity *:</label>
                            <div class="col-lg-6">
                               <select class="form-control " id="proof_type"  name="proof_type">
                                    <option value="">Select</option>
                                    <option value="M">Male</option>
                                    <option value="F">Female</option>
                                    <option value="T">Transgender</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="dob">pass valid from*:</label>
                            <div class="col-lg-6">
                                <input name="proof_reference" type="text" id="proof_reference" value="<?php echo $middle_name; ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>
                        
                         <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="dob">pass valid till*:</label>
                            <div class="col-lg-6">
                                <input name="proof_reference" type="text" id="proof_reference" value="<?php echo $middle_name; ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>
                         
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="dob">Two Routes:</label>
                            <div class="col-lg-6">
                                <input name="two_routes" type="checkbox" id="two_routes" value="<?php echo $middle_name; ?>" class="form-control" autocomplete="off">
                            </div>
                        </div>
                      <div class="clearfix" style="height: 10px;clear: both;"></div>

                        
                       <div class="form-group">
                            <label class="col-lg-3 control-label" for="proof_type">From Stop *:</label>
                            <div class="col-lg-6">
                               <select class="form-control " id="proof_type"  name="proof_type">
                                    <option value="">Select</option>
                                    <option value="M">Male</option>
                                    <option value="F">Female</option>
                                    <option value="T">Transgender</option>
                                </select>
                            </div>
                        </div>
                          <div class="clearfix" style="height: 10px;clear: both;"></div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label" for="proof_type">Till Stop *:</label>
                            <div class="col-lg-6">
                               <select class="form-control " id="proof_type"  name="proof_type">
                                    <option value="">Select</option>
                                    <option value="M">Male</option>
                                    <option value="F">Female</option>
                                    <option value="T">Transgender</option>
                                </select>
                            </div>
                        </div>
                                               <div class="clearfix" style="height: 10px;clear: both;"></div>
 
                       <div class="form-group">
                            <label class="col-lg-3 control-label" for="proof_type">Bus Type *:</label>
                            <div class="col-lg-6">
                               <select class="form-control " id="proof_type"  name="proof_type">
                                    <option value="">Select</option>
                                    <option value="M">Male</option>
                                    <option value="F">Female</option>
                                    <option value="T">Transgender</option>
                                </select>
                            </div>
                        </div>
                         
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                         <div class="form-group">
                                <div class="col-lg-offset-5">
                                      <a class="btn btn-primary" href="<?php echo base_url('admin/registration/SC_registration_normal') ?>" type="button">Back</a> 
                                    <button class="btn btn-primary" id="submit_btn" name="submit_btn" type="submit">Next</button> 
                                  
                                </div>
                        </div><!-- /.box -->
                            <?php echo form_close(); ?>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                    </section><!-- /.content -->
                </div><!-- /.content-wrapper -->


                <script>
                    var cmp_on_change_flag = '<?php echo $cmp_on_change_flag; ?>';
                    var md_on_change_flag = '<?php echo $md_on_change_flag; ?>';
                    var ad_on_change_flag = '<?php echo $ad_on_change_flag; ?>';
                    var show_retailor_flag = '<?php echo $show_retailor_flag; ?> ';
                    var creat_role = '<?php echo $creat_role; ?>';
                </script>

                <script src="<?php echo base_url() ?>js/back/admin/registration.js"></script>
                <style>
                    /****DATEPICKER YEAR MONTH DROPDOWNN *******/
                    .ui-datepicker .ui-datepicker-title select {
                        color: #000;
                    }
                </style>
                <script type="text/javascript">
                    /*   $(document).off('change', '.show_ret').on('change', '.show_ret', function(e)
                     {
                     $('.retailer_show').show();
                     });
                     var current_role = '<?php //echo $this->session->userdata('role_id');                        ?>';
                     var distributor_role = '<?php //echo DISTRIBUTOR ;                       ?>'
                     if(current_role == distributor_role)
                     {
                     $('.retailer_show').show();
                     }*/
                    // $(function() {
                    //         $('.retailer_show').hide(); 
                    //         $('#service_name').load(function(){
                    //             if($('#service_name').val() == '2') {
                    //                 $('.retailer_show').show(); 
                    //             } else {
                    //                 $('.retailer_show').hide(); 
                    //             } 

                    //         });
                    //    });

                    function showServices() {
                        $('.retailer_show').hide();
                        if ($('#service_name').val() == '2') {
                            $('.retailer_show').show();
                        } else {
                            $('.retailer_show').hide();
                        }
                    }

                    /*$(document).ready(function() {
                     $('#multiple-checkboxes').multiselect();
                     });*/

                    /*$(document).ready(function() {
                     $('#service_name').multiselect({
                     nonSelectedText: 'Select Services',
                     enableFiltering: true,
                     enableCaseInsensitiveFiltering: true,
                     buttonWidth: '510px'
                     });
                     });*/
                </script>          
