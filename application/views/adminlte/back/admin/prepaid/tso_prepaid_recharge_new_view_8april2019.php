<style type="text/css">
.help-block{
    color: red;
}
</style>
<div id="divLoading" style="margin: 0px; padding: 0px; position: fixed; right: 0px; top: 0px; width: 100%; height: 100%; background-color: rgb(102, 102, 102); z-index: 30001; opacity: 0.8;display: none">
<p style="position: absolute; color: White; top: 38%; left: 45%;">
Loading, please wait...
<img src="http://loadinggif.com/images/image-selection/3.gif">
</p>
</div>
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"> 
                        <h3 class="box-title">Mobile Prepaid</h3>
                    </div>
                    <div class="box-body">
                        <?php

                        echo validation_errors('<div class="error">', '</div>');
                        if ($this->session->tempdata('message')) {
                            ?>
                            <div class="alert alert-success alert-dismissable hide-it" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->tempdata('message');

                                 ?>
                            </div>

                            <?php
                            // $this->session->unset_userdata('message');
                        }

                        $attributes = array("method" => "POST", "id" => "mobile_recharge", "name" => "mobile_recharge");
                        echo form_open(base_url() . 'admin/Prepaid/tso_recharge', $attributes);
                        ?>
                        <input type="hidden" name="recharge_from" id="recharge_from" value="web"/>
                        <!-- <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">       
                            <label class="col-lg-3 control-label">Select Type *:</label>
                            <div class="col-lg-6">
                                <input type="radio" name="plan_type" value="Prepaid Mobile" class="form-control plan_type" checked/> Prepaid
                                <input type="radio" name="plan_type" value="Postpaid Mobile" class="form-control plan_type"/> Postpaid
                            <div class="error"><?php //echo form_error('plan_type'); ?></div>
                            </div>
                        </div> -->
                        <input type="hidden" name="plan_type" id="plan_type" value="Prepaid Mobile">
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label">Select Operator *:</label>
                                <div class="col-lg-6 tso_pre">
                                    <input type="hidden" name="type" value="<?php echo $type; ?>">
                                    <input type="hidden" name="service" value="<?php echo $type; ?>">
                                    <select id="pre_operator" name="pre_operator" class="form-control select2">
                                        <option value="">Please select mobile operator</option>
                                        <?php foreach ($pre_operators as $key => $value) { ?>
                                            <option value="<?php echo $key ?>"><?php echo $value ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="error"><?php echo form_error('pre_operator'); ?></div>
                                </div>
                                
                                <!-- <div class="col-lg-6 tso_post" style="display:none">
                                    <input type="hidden" name="service" value="<?php //echo $type; ?>">
                                    <select id="post_operator" name="post_operator" class="form-control select2">
                                        <option value="">Please select mobile operator</option>
                                        <?php //foreach ($post_operators as $key => $value) { ?>
                                            <option value="<?php //echo $key ?>"><?php //echo $value ?></option>
                                        <?php //} ?>
                                    </select>
                                    <div class="error"><?php //echo form_error('post_operator'); ?></div>
                                </div> -->
                        </div> 
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label">Mobile Number *:</label>
                            <div class="col-lg-6">
                                <input type="text" id="mobile_number" name="mobile_number" class="form-control" placeholder="Mobile Number" maxlength="10"/>
                            <div class="error"><?php echo form_error('mobile_number'); ?></div>
                            </div>
                        </div>
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group r_type">       
                            <label class="col-lg-3 control-label">Recharge Type :</label>
                                <div class="col-lg-6">
                                    <select id="recharge_type" name="recharge_type" class="form-control select2">
                                        <option value="0">Topup</option>
                                        <option value="1">Scheme</option>
                                    </select>
                                <div class="error"><?php echo form_error('recharge_type'); ?></div>
                                </div>
                        </div>
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label">Recharge Amount *:</label>
                            <div class="col-lg-6">
                                <input type="text" id="recharge_amount" name="recharge_amount" class="form-control" min="1" placeholder="Recharge Amount"/>
                            <div class="error"><?php echo form_error('recharge_amount'); ?></div>
                            </div>
                            <!--<div class="col-lg-2">
                                <a id="choose_plans" class="btn btn-default">Choose from plans</a>
                            </div>-->
                        </div>
                        
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group" id="payment_type_block" >
                            <label class="col-lg-3 control-label">Payment Type *:</label>
                            <div class="col-lg-6">
                                <input type="radio" name="payment_type" id="payment_type_cash" value="cash" class="form-control payment_type_mode" checked/> Cash
                                <input type="radio" name="payment_type" id="payment_type_epaylater" value="epaylater" class="form-control payment_type_mode"/> ePayLater
                            </div>
                        </div>
                        
                        
                        <!-- <div class="clearfix" style="height: 10px;clear: both;"></div>n
                        <div class="form-group"> 
                            <label class="col-lg-3 control-label">Comments:</label>
                            <div class="col-lg-6">
                                <input type="text" id="comments" name="comments" class="form-control" placeholder="Comments"/>
                            </div>
                        </div> -->
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-4">
                                <button class="btn btn-primary" id="submit_recharge" name="submit_recharge" type="submit">Recharge</button>                                
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<script type="text/javascript">
    var table;
    $(document).ready(function() {
        $(".hide-it").hide(10000);

        $("#recharge_amount, #mobile_number").ForceNumericOnly();
        $('#recharge_amount').keypress(function(e){
           if (this.value.length == 0 && e.which == 48 ){
              return false;
           }
        });

        $("#recharge_amount, #mobile_number").ForceNumericOnly();
        $('#recharge_amount').keypress(function(e){
           if (this.value.length == 0 && e.which == 48 ){
              return false;
           }
        });
        
        /*$('.plan_type').on('ifChanged', function(event){
            if($("input[type='radio'].plan_type").is(':checked')) {
                var card_type = $("input[type='radio'].plan_type:checked").val();
                if(card_type == 'Prepaid Mobile'){
                    $('.tso_pre').css('display', 'block');
                    $('.tso_post').css('display', 'none');
                    $('.r_type').css('display', 'block');
                    $("#submit_recharge").html('Recharge');
                }else{
                    $('.tso_pre').css('display', 'none');
                    $('.tso_post').css('display', 'block');
                    $('.r_type').css('display', 'none');
                    $("#submit_recharge").html('PayBill');
                }
            }
        });*/

        
    });
    
    $('.payment_type_mode').on('ifChanged', function(event) {
        if($("input[type='radio'].payment_type_mode").is(':checked')) {
            var payment_type = $("input[type='radio'].payment_type_mode:checked").val();
            if (payment_type == 'cash') {
               $("#mobile_recharge").attr('action', base_url + 'admin/Prepaid/index');
            } else if (payment_type == 'epaylater') {
                $("#mobile_recharge").attr('action', base_url + 'admin/epaylater/epaylater_recharge');                
            }
        }
    });
    
    $("#submit_recharge").on('click', function(){
        $("#submit_recharge").html("Please Wait....");
        setTimeout(function(){
            $("#submit_recharge").html("Recharge");
        },5000);
    });
    
    $('#mobile_recharge').bootstrapValidator({
        group: '.form-control',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            /*plan_type: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b>Please select type.</font>'
                    }
                }
            },*/
            pre_operator: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b>Please enter operator.</font>'
                    }
                }
            },
            /*post_operator: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b>Please enter operator.</font>'
                    }
                }
            },*/
            mobile_number: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b> Please enter mobile number.</font>'
                    },
                    regexp: {
                        regexp: /^[789]\d{9}$/,
                        message: '<b><font style="color:red; size="1"></b>Enter valid mobile number</font>'
                    }
                }
            },
            recharge_amount: {
                validators: {
                    notEmpty: {
                        message: '<b><font style="color:red; size="1"></b> Please enter recharge amount.</font>'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: '<b><font style="color:red; size="1"></b>Amount can only consist of number</font>'
                    }
                }
            }
            
        }
    }).on('success.form.bv',function(e)
     {
        $('#divLoading').css('display', 'block');
     });

    // Numeric only control handler
    $.fn.ForceNumericOnly =
    function()
    {
        return this.each(function()
        {
            $(this).keydown(function(e)
            {
                var key = e.charCode || e.keyCode || 0;
                // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
                // home, end, period, and numpad decimal
                return (
                    key == 8 || 
                    key == 9 ||
                    key == 13 ||
                    key == 17 ||
                    key == 46 ||
                    key == 86 ||
                    key == 110 ||
                    key == 116 ||
                    key == 190 ||
                    (key >= 35 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105));
            });
        });
    };

    
</script>
