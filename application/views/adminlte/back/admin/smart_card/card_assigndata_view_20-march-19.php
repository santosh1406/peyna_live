<section class="content">   
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header "> 
                    <h3 class="box-title">Card Assign Data</h3>
                </div>                
                <div class="box-body">                    
                    <div>
                        <?php
                        
                        if ($this->session->tempdata('msg')) {
                            ?>
                            <div class="alert alert-success alert-dismissable" style="position: fixed; margin: 0px; z-index: 9999; top: 10px; width: 350px; right: 20px;">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->tempdata('msg'); ?>
                            </div>
                            <?php
                        }
                        $attributes = array("method" => "POST", "id" => "matm_form", "name" => "matm_form");
                        echo form_open(site_url() . '/admin/otc_card_inventory/get_card_assignData', $attributes);
                        ?>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="from-group">
                            <label class="col-lg-4 control-label">Courier No *:</label>  
                            <div class="col-lg-4">
                                <input type="text" id="courier_no" name="courier_no" class="form-control" value="" placeholder="Please Enter Total Cards "/>                                
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <?php echo validation_errors('<div class="error">', '</div>'); ?>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-5">
                                <button class="btn btn-primary" id="submit"  type="submit">Show</button>                                
                                <a class="btn btn-primary" href="<?php echo base_url('admin/dashboard'); ?>" type="button">Cancel</a>
                            </div><!--/div-->
                        </div><!--/div-->
                        <?php echo form_close();?>
                    </div>
                    
                </div>
                <table id="card_list" name="tran_list" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th  width="10%">Sr.No</th>
                                <th>Card No</th> 
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</section>
</div>

<!-- Signin Content End -->             
<!--/div-->
<!-- Inner Wrapper End -->

<script>
    $("body").on('click', '#submit', function(){

        event.preventDefault();

        var courier_no = $("#courier_no").val();
        
        var url = base_url+"admin/otc_card_inventory/get_card_assignData";
        $('#card_list').DataTable({
        "ajax": {
                type: "POST",
                dataType: "json",
                url: url ,
                data: {'courier_no':courier_no},         
                success: function(data){
                var result = data.data.trimax_card_id;
                
                
                var row = "";
                 
                for(i=0; i < result.length; i++) {   

                row += (result[i]) ? result[i] : "0";
                }
                
                //$("#card_list").html(row);
            }
        },
    });  
    });
    </script>

