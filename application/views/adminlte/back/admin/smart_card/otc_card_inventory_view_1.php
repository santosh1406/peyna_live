<section class="content">   
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header "> 
                    <h3 class="box-title">OTC Card Inventory</h3>
                </div>                
                <div class="box-body">                    
                    <div>
                        <div class="error" id="error_div"></div>
                        <?php echo validation_errors('<div class="error">', '</div>'); ?>
                        <?php
                        
                        $attributes = array("method" => "POST", "id" => "generate_otc_card_form", "name" => "generate_otc_card_form");
                        echo form_open(site_url() . '/admin/otc_card_inventory/generate_otc_card', $attributes);
                        ?>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="from-group">
                            <label class="col-lg-4 control-label">Total Cards*:</label>  
                            <div class="col-lg-4">
                                <input type="text" id="cards" name="cards" class="form-control" value="<?php echo MIN_CARD_AMOUNT ?>" placeholder="Please Enter Total Cards " maxlength="4"/>                                
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <?php echo validation_errors('<div class="error">', '</div>'); ?>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-5">
                                <button class="btn btn-primary" id="get_card" name="get_card" type="button">Show</button>       
                                <input type="hidden" value="<?php echo PER_CARD_AMOUNT; ?>" id="per_card_amt" />
                                <a class="btn btn-primary" href="<?php echo base_url('admin/dashboard'); ?>" type="button">Cancel</a>
                            </div><!--/div-->
                        </div><!--/div-->
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Total Cards Amount</h4>
            </div>
            <div class="modal-body" style="font-size: 18px">
                <p><span>Total Cards:</span>
                    <span id="total_cards" style="float: right; margin-right: 20px"></span>
                </p>
                <p><span>Per Card Amount:</span>
                    <span id="charges_span" style="float: right; margin-right: 20px"><?php echo PER_CARD_AMOUNT; ?></span>
                </p>
                <hr style="margin: 0px">
                <p><span style="color: #80067f"><strong>Total Amount:</strong></span>
                    <span  id="total_charge" style="float: right; margin-right: 20px"></span>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" id="proceed" class="btn btn-primary">Proceed</button>
                <input type="hidden" id="msg" value="" />
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>

    </div>
</div>
<!--//model end -->

<!-- Signin Content End -->             
<!--/div-->
<!-- Inner Wrapper End -->

<script>
    $(document).ready(function(){
        $("#generate_otc_card_form").validate({
            
            rules: {
                cards: {
                    required: true,
                    number: true
                    //min: 20,
                    
                }
            },
            
            messages: {                        
                cards: {
                        required: "Please Enter Total Cards",
                        number: "Please Enter Valid Cards"                          
                          //  min_limt:"Please Enter value Greater than equal to 20"
                    }     
            }
            
                     
        });
        
      $("body").on('click', '#get_card', function(e){

       
        //showLoaderImg();
        
        
        var cards = parseInt($("#cards").val());
        $('#total_cards').text(cards);
        var per_card_amt = $('#per_card_amt').val();
        console.log(per_card_amt);
        
        var total_amt = cards * per_card_amt;
        console.log(total_amt);
        $('#total_charge').text(total_amt);
        
        var url = base_url+"admin/otc_card_inventory/generate_otc_card";
        if(cards!=='' && cards >=  '<?php echo MIN_CARD_AMOUNT ?>'){
			if(cards % 10 == '0'){
            //alert(cards);
            e.preventDefault();
            $.ajax({
                type: "POST",
                dataType: "json",
                url: url ,
                data: {'cards':cards},         
                success: function(data){
                    //hideLoaderImg();
                    if(data.status.toLowerCase() === 'failure'){
                        $('#error_div').html(data.msg);
                        //alert(data.msg);
                    }else{
                        $('#myModal').modal('show');
			$('#msg').val(data.data.msg);			
//			swal({
//                            title: data.data.msg,
//                            html: true,
//                        });
				
				
			                    
                    }
                  
                    //window.location.href = BASE_URL + 'admin/otc_card_inventory';
                        
                }
            }); 
        }
		else{
			alert('Please Enter the OTC Card Multiples of 10');
		}
		
		}
        
		else{
            alert('Add atleast 1 card and Please Enter value Greater than equal to 20');
        }
         
         
        });
        
        $(document).on('click', '#proceed', function(){
            // $(".img-circle").show();
            var show_msg = $('#msg').val();
            $('#myModal').modal('hide');
            $('#proceed').attr('disabled', 'disabled');
            
            swal({
                    //title: data.data.msg,                 
                    title: show_msg,
                    html: true,
                });
                       
            window.location.href = BASE_URL + 'admin/otc_card_inventory';
            
        });
        
    });
    
    function showLoaderImg(centerY){
    var url = COMMON_ASSETS+"/images/ajax-loader.gif";
    $.blockUI({
        message: '<img src="'+ url + '" align="">',
        centerY: centerY != undefined ? centerY : true,
        css: {
//                    top: '10%',
            border: 'none',
            padding: '2px',
            backgroundColor: 'none'
        },
        overlayCSS: {
            backgroundColor: '#000',
            opacity: 0.15,
            cursor: 'wait'
        }
    });
}

    function hideLoaderImg(){
        $.unblockUI({
            onUnblock: function () {
                $.removeAttr("style");
            }
        });
    }
    
    </script>