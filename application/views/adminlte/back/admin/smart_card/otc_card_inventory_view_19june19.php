<section class="content">   
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header "> 
                    <h3 class="box-title">OTC Card Inventory</h3>
                </div>                
                <div class="box-body">                    
                    <div>
                        <div class="error" id="error_div"></div>
                        <?php echo validation_errors('<div class="error">', '</div>'); ?>
                        <?php
                        
                        $attributes = array("method" => "POST", "id" => "generate_otc_card_form", "name" => "generate_otc_card_form");
                        echo form_open(site_url() . '/admin/otc_card_inventory/generate_otc_card', $attributes);
                        ?>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="from-group">
                            <label class="col-lg-4 control-label">Total Cards*:</label>  
                            <div class="col-lg-4">
                                <input type="text" id="cards" name="cards" class="form-control" value="" placeholder="Please Enter Total Cards " maxlength="4"/>                                
                            </div>
                        </div>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <?php echo validation_errors('<div class="error">', '</div>'); ?>
                        <div class="clearfix" style="height: 10px;clear: both;"></div>
                        <div class="form-group">
                            <div class="col-lg-offset-5">
                                <button class="btn btn-primary" id="get_card" name="get_card" type="button">Show</button>                                
                                <a class="btn btn-primary" href="<?php echo base_url('admin/dashboard'); ?>" type="button">Cancel</a>
                            </div><!--/div-->
                        </div><!--/div-->
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>

<!-- Signin Content End -->             
<!--/div-->
<!-- Inner Wrapper End -->

<script>
    $(document).ready(function(){
        $("#generate_otc_card_form").validate({
             
            rules: {
                cards: {
                    required: true,
                    number: true,
                    min: 20,
                    
                }
            },
            
            messages: {                        
                cards: {
                            required: "Please Enter Total Cards",
                            number: "Please Enter Valid Cards",                            
                            min_limt:"Please Enter value Greater than equal to 20"
                    }     
            }
            
                     
        });
        
      $("body").on('click', '#get_card', function(){

       
        //showLoaderImg();
        var cards = $("#cards").val();
        var url = base_url+"admin/otc_card_inventory/generate_otc_card";
        if(cards!==''){
			
			if(cards % 10 == '0'){
            //alert(cards);
            event.preventDefault();
            $.ajax({
                type: "POST",
                dataType: "json",
                url: url ,
                data: {'cards':cards},         
                success: function(data){
                    //hideLoaderImg();
                    if(data.status.toLowerCase() === 'failure'){
                        $('#error_div').html(data.msg);
                        //alert(data.msg);
                    }else{
                        $('#error_div').html(data.data.msg);
                        //alert(data.data.msg);                        
                    }
                   // window.location = "<?php // echo base_url('admin/otc_card_inventory'); ?>";   
                    window.location.href = BASE_URL + 'admin/otc_card_inventory';
                        
                
                }
            }); 
        }
		else{
			alert('Please Enter the OTC Card Multiples of 10');
		}
		}
        
		else{
            alert('Add atleast 1 card');
        }
         
         
        });
    });
    
    function showLoaderImg(centerY){
    var url = COMMON_ASSETS+"/images/ajax-loader.gif";
    $.blockUI({
        message: '<img src="'+ url + '" align="">',
        centerY: centerY != undefined ? centerY : true,
        css: {
//                    top: '10%',
            border: 'none',
            padding: '2px',
            backgroundColor: 'none'
        },
        overlayCSS: {
            backgroundColor: '#000',
            opacity: 0.15,
            cursor: 'wait'
        }
    });
}

    function hideLoaderImg(){
        $.unblockUI({
            onUnblock: function () {
                $.removeAttr("style");
            }
        });
    }
    
    </script>