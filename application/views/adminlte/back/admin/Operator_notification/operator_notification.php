<section class="content">
    <div id="role_wrapper">
        <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Operator Minimum Balance Notification</h3>
                    </div> 
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12 bhoechie-tab-container">
                                <div class="col-xs-4 bhoechie-tab-menu">
                                    <div class="list-group">
                                        <a href="#" class="list-group-item roles_direction text-center">
                                            <h4 class="glyphicon">Operator</h4><br/>
                                        </a>
                                    </div>
                                </div>
                                <div class="permission_type col-xs-8">
                                    <ul class="list-inline">
                                        <li>Minimum Balance</li>
                                     <!--   <li>Operator_Email</li> -->
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 bhoechie-tab-container">
                                <div class="col-lg-4 col-xs-4 bhoechie-tab-content">
                                    <div><!--<div class="list-group">-->
                                        <?php
                                           
                                             echo "<a class='list-group-item rolename'>";
                                             echo "<b>"."Rsrtc"."</b>";
                                             echo "</a>";
                                              echo "<a class='list-group-item rolename'>";
                                             echo "<b>"."Upsrtc"."</b>";
                                             echo "</a>";
                                              echo "<a class='list-group-item rolename'>";
                                             echo "<b>"."Msrtc"."</b>";
                                             echo "</a>";
                                              echo "<a class='list-group-item rolename'>";
                                             echo "<b>"."Hrtc"."</b>";
                                             echo "</a>";
                                             echo "<a class='list-group-item rolename'>";
                                             echo "<b>"."Etravelsmart"."</b>";
                                             echo "</a>";
                                        ?>

                                  </div>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 bhoechie-tab">
                                    <div class="bhoechie-tab-content" id="role_menu_rel_permission">
                                          <?php
                                                $attributes = array("method" => "POST", "id" => "op_form");
                                                echo form_open(base_url().'admin/operator_notification/send_mail_operator', $attributes);
                                             ?>
                                         <div class="input-group col-sm-8">
                                                <span class="input-group-addon">Minimum Balance</span>
                                                <input name="rsrtc" type="text" id="min_bal" class="form-control" placeholder="" value ="<?php if ($min_rsrtc) echo $min_rsrtc[0]['min_balance'] ?>" required/>
                                         </div>
                                         <div class="clearfix" style="height: 10px;clear: both;"></div>
                                              <div class="input-group col-sm-8">
                                                    <span class="input-group-addon">Minimum Balance</span>
                                                    <input name="upsrtc" type="text" id="min_bal" class="form-control" placeholder="" value ="<?php if ($min_upsrtc) echo $min_upsrtc[0]['min_balance'] ?>" required/>
                                             </div> 
                                         <div class="clearfix" style="height: 10px;clear: both;"></div>
                                              <div class="input-group col-sm-8">
                                                    <span class="input-group-addon">Minimum Balance</span>
                                                    <input name="msrtc" type="text" id="min_bal" class="form-control" placeholder="" value ="<?php if ($min_msrtc) echo $min_msrtc[0]['min_balance'] ?>" required/>
                                             </div>
                                         <div class="clearfix" style="height: 7px;clear: both;"></div>
                                              <div class="input-group col-sm-8">
                                                    <span class="input-group-addon">Minimum Balance</span>
                                                    <input name="hrtc" type="text" id="min_bal" class="form-control" placeholder="" value ="<?php if ($min_hrtc) echo $min_hrtc[0]['min_balance'] ?>" required/>
                                             </div>
                                         <div class="clearfix" style="height: 7px;clear: both;"></div>
                                              <div class="input-group col-sm-8">
                                                    <span class="input-group-addon">Minimum Balance</span>
                                                    <input name="ets" type="text" id="min_bal" class="form-control" placeholder="" value ="<?php if ($min_ets) echo $min_ets[0]['min_balance'] ?>"required/>
                                             </div>   
                                         <div class="clearfix" style="height: 7px;clear: both;"></div>
                                              <div class="input-group col-sm-8">
                                                     <input name="user_email" type="email" id="user_email" class="form-control" placeholder="email id" value="<?php if($user_email) echo $user_email ?>"required/>
                                                      <span class="input-group-addon">User Email Id</span>
                                             </div>
                                         <div class="clearfix" style="height: 7px;clear: both;"></div>
                                           <button type="submit" id="pg_confirm" class="btn btn-primary pull-left" name="pg_confirm">Submit</button>
                                         <div class="clearfix" style="height: 7px;clear: both;"></div>
                                      </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- body content end -->
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        
    </div>
</section>
<script type="text/javascript">
 $(document).off('keypress', '#min_bal').on('keypress', '#min_bal', function(e) 
        {
           var regex = new RegExp("^[0-9\b]+$");//accept only _
           var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
             key=e.charCode? e.charCode : e.keyCode;
            if(key==9 )
            {
                return true; 
            }
            if (regex.test(str)) 
            {
                return true;
            }
            e.preventDefault();
            return false;
        });
</script>