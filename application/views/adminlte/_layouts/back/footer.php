    </aside>
    <!-- for session message notification start -->
        <div id="notification-header-outer">
            <div id="notification-header-inner" class="loader-glass-effect">
                <img src="<?php echo base_url() . BACK_ASSETS_IMG ?>win_loader.png"/>
            </div>
        </div>
        
    <div id="notification-footer-outer">
        <div id="notification-footer-inner" class="error-glass-effect">
           
        </div>
    </div>
 
    <script>
        $(document).ready(function(){ 
            var msg_type = "<?php echo $this->session->flashdata('msg_type'); ?>";
            var msg = "<?php echo $this->session->flashdata('msg'); ?>";
           
            if(msg_type != "" && msg != "")
            {
                // showLoader(msg_type, msg);
                bootstrapGrowl(msg, msg_type);
            }

            setInterval(function () {
                checkSession();
            }, 10000);

            function checkSession()
            {
                var url = "<?php echo base_url() . 'admin/session_check/check_session' ?>";
                $.ajax({
                    type: "POST",
                    url: url,
                    success: function (data) {
                        data = JSON.parse(data);
                        if(data.response != "true")
                        {   
                            window.location.href = base_url  + 'admin/dashboard';  
                        }
                    }
                });
            }
        });
                
        function showLoader(msg_type,msg)
        {
            bootstrapGrowl(msg, msg_type);           
        }
        
    </script>
    <!-- for session message notification end  -->
    <script src="<?php echo base_url() . BACK_ASSETS ?>js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?php echo base_url() . BACK_ASSETS ?>js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    
    </body>
</html>
