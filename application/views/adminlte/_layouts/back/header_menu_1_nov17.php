<header class="header">
            <a href="<?php echo base_url();?>" class="logo" style="font-size: 40px;">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                <img src="<?php echo base_url();?>assets/travelo/front/images/logo.png" alt="">
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown tasks-menu">
                            <a class="dropdown-toggle" title="" caption="">
                                <i class="fa fa-phone"></i>
                                <span>Customer Care Number : 022-6681 3970, +91 7738343137</span>
                            </a>
                        </li>
                        <li class="dropdown tasks-menu">
                            <a class="dropdown-toggle" title="" caption="">
                                <?php if($this->session->userdata('role_id')== 
                                        AREA_DISTRIBUTOR_ROLE_ID || $this->session->userdata('role_id') == DISTRIBUTOR ||
                                        $this->session->userdata('role_id')== RETAILER_ROLE_ID) { 
                                                      $url = MSRTC_APPLICATION.'rest_server/msrtc/msrtc_current_balance';
                                                      $response = getDataFromMsrtc($url);
                                                      $response = json_decode($response);
                                                      if(!empty($response)) {
                                                    ?>
                                                    <span>MSRTC Ticket Balance : <?php  echo "Rs. ".$response->data->amount; ?> 
                                                    </span>
                                                    <?php } else { } 
                                        }?>

                            </a>
                        </li>
                        <li class="dropdown tasks-menu">
                            <a class="dropdown-toggle" title="" caption="">
                                <!--<i class="fa fa-inr" aria-hidden="true"></i>-->
                                <!--<img src="<?php echo base_url().BACK_ASSETS_IMG.'images.jpg'; ?>" class="img-circle" alt="Wallet Image" width="22"/>-->
                                <?php if($this->session->userdata('role_id') ==  MASTER_DISTRIBUTOR_ROLE_ID || $this->session->userdata('role_id') ==  AREA_DISTRIBUTOR_ROLE_ID || $this->session->userdata('role_id') ==  DISTRIBUTOR || $this->session->userdata('role_id') ==  RETAILER_ROLE_ID || $this->session->userdata('role_id') == COMPANY_ROLE_ID) 
                                { ?>
                                <span>My Wallet Balance :
                                <?php
                                $wallet_balance = $this->wallet_model->where('user_id',$this->session->userdata('user_id'))->find_all();
                                if($wallet_balance)
                                {
                                    $wallet_amt = to_currency($wallet_balance[0]->amt);
                                }
                                else
                                {
                                  $wallet_amt ='0.00';
                                }
                                 
                                   echo $wallet_amt;
                                }
                                ?></span>
                            </a>
                        </li>
<!--                        <li class="dropdown tasks-menu">
                            <a href="<?php echo base_url();?>" class="dropdown-toggle" title="" caption="">
                                <i class="fa fa-home"></i>
                            </a>
                        </li>-->
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu sign_wraper">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php echo ucwords($this->session->userdata("display_name")); ?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="<?php echo $this->session->userdata('profile_image'); ?>" class="img-circle" alt="User Image" />
                                <!--<p>
                                        <?php echo ucwords($this->session->userdata("display_name")); echo " -".ucwords($this->session->userdata("role_name")); ?> 
                                        <small>Member since Nov. 2012</small>
                                    </p>-->
                                </li>
                                <!-- Menu Body -->
                            <!--<li class="user-body">
                                    <div class="col-xs-4 text-center">
                                        <?php $role_id=$this->session->userdata('role_id');
                                        if ($role_id == '19' || $role_id == '20' || $role_id== '21') {?>
                                            <a href="">Home</a>
                                      <?php   }else   {          ?>
                                        <a href="<?php echo base_url(); ?>">Home</a>
                                      <?php } ?>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Sales</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Friends</a>
                                    </div>
                                </li>-->
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?php echo base_url().'admin/users/profile/'.base64_encode($this->session->userdata('id'));?>" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?php echo base_url().'login/logout';?>" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>