<?php 

//print_r($_SESSION);die;?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Peyna - Digital E-wallet</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> -->
        <!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->

        <!--flight search Inner Wrapper Start -->
         <link href="<?php echo base_url() . BACK_ASSETS ?>css/search_style/style.css" rel="stylesheet" type="text/css" />
        <script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/jquery.js" type="text/javascript"></script>
        <script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/bootstrap.js" type="text/javascript"></script>
        <script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/qrcode.js" type="text/javascript"></script>
        <script src="<?php echo base_url() . BACK_ASSETS ?>js/search_js/jquery.qrcode.js" type="text/javascript"></script> 
        <link href="<?php echo base_url() . BACK_ASSETS ?>css/search_style/bootstrap.css" rel="stylesheet" type="text/css" />
         <link href="<?php echo base_url() . BACK_ASSETS ?>css/search_style/bootstrap.min.css" rel="stylesheet" type="text/css" /> 
        <!--flight search  Inner Wrapper end -->

        <link href="<?php echo base_url() . BACK_ASSETS ?>css/maxcdn_bootstrapcdn_min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() . COMMON_ASSETS ?>css/jquery-ui.css" rel="stylesheet">

        <!-- <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> -->
        <link href="<?php echo base_url() . COMMON_ASSETS ?>css/bootstrap/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
        <!-- added by harshada kulkarni on 25-07-2018 -->
        <script src="<?php echo base_url() . COMMON_ASSETS ?>css/bootstrap/bootstrapValidator.css" rel="stylesheet" type="text/css"></script>
        <script src="<?php echo base_url() . COMMON_ASSETS ?>css/bootstrap/bootstrapValidator.min.css" rel="stylesheet" type="text/css"></script>
        <!-- added by harshada kulkarni on 31-08-2018 -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />

        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        

        <!-- Ionicons  -->
        <!-- <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" /> -->
        <!-- link href="//cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- link rel="icon" href="<?php echo base_url().FRONT_ASSETS;?>images/icon/favicon.ico?token=03052017" type="image/x-icon" -->
        <!-- Morris chart -->
        <link href="<?php echo base_url() . BACK_ASSETS ?>css/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() . BACK_ASSETS ?>css/chosen/chosen.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() . BACK_ASSETS ?>css/soap-icon.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="<?php echo base_url() . BACK_ASSETS ?>css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- Date Picker -->
<!--        <link href="<?php echo base_url() . BACK_ASSETS ?>css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />-->
        <link href="<?php echo base_url() . BACK_ASSETS ?>css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="<?php echo base_url() . BACK_ASSETS ?>css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!--<link href="<?php echo base_url() . BACK_ASSETS ?>css/timepicker/jquery.timepicker.css" rel="stylesheet" type="text/css" />-->
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="<?php echo base_url() . BACK_ASSETS ?>css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo base_url() . BACK_ASSETS ?>css/soap-icon.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() . BACK_ASSETS ?>css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- Custome style -->
        <link href="<?php echo base_url() . BACK_ASSETS ?>css/custom.css" rel="stylesheet" type="text/css" />
        <!-- Admin Search -->
        <link href="<?php echo base_url() . BACK_ASSETS ?>css/style_admin.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() . BACK_ASSETS ?>css/dashboard.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" >
            var BASE_URL = "<?php echo base_url(); ?>";
            var COMMON_ASSETS = "<?php echo base_url() . COMMON_ASSETS ?>";
            var rokad_token = '<?php echo $this->security->get_csrf_hash(); ?>';
        </script>
        <script src="<?php echo base_url() . BACK_ASSETS ?>js/jquery.min.js"></script>
        <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
        <!-- <script src="<?php echo base_url() . COMMON_ASSETS ?>js/bootstrap/bootstrap.min.js" type="text/javascript"></script> -->
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>
        
        <!-- added by harshada kulkarni on 25-07-2018 -->
        <script src="<?php echo base_url() . COMMON_ASSETS ?>js/bootstrap/bootstrapValidator.js" type="text/javascript"></script>
        <!-- added by harshada kulkarni on 31-08-2018 -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
        
        <!--<script src="<?php echo base_url() . COMMON_ASSETS ?>js/bootstrap/bootstrap.min.js" type="text/javascript"></script>-->
        <script src="<?php echo base_url() . COMMON_ASSETS ?>js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() . COMMON_ASSETS ?>js/additional-methods.min.js" ></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.1/jquery.form-validator.min.js"></script> 
        <script src="<?php echo base_url() . COMMON_ASSETS ?>js/jquery-ui.min.js" type="text/javascript"></script>
        <!-- Morris.js charts -->
        <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> --.
        <script src="<?php echo base_url() . BACK_ASSETS ?>js/plugins/morris/morris.min.js" type="text/javascript"></script>
        <!--<script src="<?php echo base_url() . BACK_ASSETS ?>js/plugins/timepicker/jquery.timepicker.js" type="text/javascript"></script>-->
        <script src="<?php echo base_url() . BACK_ASSETS ?>js/plugins/chosen/chosen.jquery.js" type="text/javascript"></script>
        <!-- Sparkline -->
        <script src="<?php echo base_url() . BACK_ASSETS ?>js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- jvectormap -->
        <!-- daterangepicker -->
        <!--<script src="<?php echo base_url() . BACK_ASSETS ?>js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>-->
        <!-- datepicker -->
        <!--<script src="<?php echo base_url() . BACK_ASSETS ?>js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>-->
        <!-- Bootstrap WYSIHTML5 -->
        <script src="<?php echo base_url() . BACK_ASSETS ?>js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="<?php echo base_url() . BACK_ASSETS ?>js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() . BACK_ASSETS ?>js/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url() . BACK_ASSETS ?>js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <!--<script src="<?php echo base_url() . BACK_ASSETS ?>js/AdminLTE/dashboard.js" type="text/javascript"></script>-->

        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url() . BACK_ASSETS ?>js/AdminLTE/demo.js" type="text/javascript"></script>


        <script src="<?php echo base_url() . COMMON_ASSETS ?>plugins/bootstrap-growl/jquery.bootstrap-growl.js"></script>
        <script src="<?php echo base_url() . COMMON_ASSETS ?>plugins/bootbox/bootbox.min.js"></script>
        <script src="<?php echo base_url() . COMMON_ASSETS ?>plugins/bootbox/bootbox.min.js"></script>
        <script src="<?php echo base_url() . COMMON_ASSETS ?>js/moment.js"></script>
        <script src="<?php echo base_url() . COMMON_ASSETS ?>js/moment.js"></script>
        <script src="<?php echo base_url() . COMMON_ASSETS ?>js/global.js"></script>
        <script src="<?php echo base_url() . COMMON_ASSETS ?>js/common.js"></script>
        <script src="<?php echo base_url() . COMMON_ASSETS ?>js/jquery.blockui.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        
		
		
		
        <script>
            $(document).ready(function() {
//                $('.datepicker').datepicker({
//                    format: 'dd-mm-yyyy'
//                })

                $(".chosen_select").chosen({width: '100%',search_contains: true});
            });
        </script>



        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
		<link href="<?php echo base_url() . COMMON_ASSETS ?>plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
		<script src="<?php echo base_url() . COMMON_ASSETS ?>plugins/sweetalert/sweetalert-dev.js" type="text/javascript"></script>
        <script src="<?php echo base_url() . COMMON_ASSETS ?>js/sort.js"></script>
		<?php 
		    $SessionID = $this->session->userdata('session_id');
		    $UserID = $this->session->userdata('user_id');
			if($UserID){
		    $Flag = checkUserAlreadyLoggedIn($SessionID,$UserID);
			if($Flag == 1){
				$this->session->sess_destroy();
				unset($_SESSION);
				$this->output->clear_all_cache();
				if ($this->session->userdata('user_id') == "") {
                    redirect(base_url() . "home");
				}
			}
			}
		?>
    </head>
    <body class="skin-blue">

        <?php $this->load->view(BACK_LAYOUTS . 'header_menu'); ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">

            <?php $this->load->view(BACK_LAYOUTS . 'sider_bar'); ?>

            <aside class="right-side">

