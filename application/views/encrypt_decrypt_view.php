<html>
<head>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/jquery.min.js"></script>
</head>

<body>
<h3>Encrypt Val</h3>

<div class="box">
<form id="encrypt_form" name="encrypt_form">
<textarea name="inp" class="inp" cols="60"></textarea>
<input type="button" value="Encypt" class="submit_btn" id="encrypt">
</form>
<textarea class="retval" readonly cols="60"></textarea>
</div>

<p></p>
<hr>

<h3>Decrypt Val</h3>

<div class="box">
<form id="decrypt_form" name="decrypt_form">
<textarea name="inp" class="inp" cols="60"></textarea>
<input type="button" value="Decrypt" class="submit_btn" id="decrypt">
</form>
<textarea class="retval" readonly cols="60"></textarea>
</div>


<script>
function get_token()
{
	var rokad_token = '<?php echo $this->security->get_csrf_hash(); ?>';
	return rokad_token;
}

$(document).on("click", ".submit_btn", function() {
rokad_token = get_token();
var curr_obj = $(this);
var inp_key = curr_obj.attr("id");
var inp_val = curr_obj.closest("form").find(".inp").val();

	$.ajax({
	method: "POST",
	url: "<?php echo base_url() ?>admin/tools/get_value",
	data: { rokad_token: rokad_token, inp_key: inp_key, inp_val:inp_val }
	}).done(function(data) {		

		if(data.msg_type == "success")
		{
			if(inp_key == "encrypt" || inp_key == "decrypt")
			{
				var inp_val = curr_obj.closest(".box").find(".retval").val(data.msg);
			}				
			else
			{
				alert("error");
			} 
		}
		else
		{
			alert("error");
		}  
	});
});
</script>
</body>
</html>