<!--<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Login</h2>
        </div>
    </div>
</div>-->
<style>
.login-page .login_form input.input-text
{
	background: #fff;
}
</style>

<section id="content" class="abouttop">
<div class="termsimg"><img src="<?php echo base_url();?>assets/travelo/front/images/login1.jpg" >
<h2>Reset RMN</h2>
</div>
    <div class="container">
        <div id="main" class="login-page forgotwrap">
            <div id="login-block">
                <!-- <p style="font-size: 1.5em;" class="block">Please login to your account.</p> -->
            
                <div class="col-sm-8 col-md-6 col-lg-5 no-float no-padding center-block">
				<div class="clearfix" style="height: 10px;clear: both;"></div>  

				<?php
				$show_error = 'display:none';
				if ($msg_type == "error") 
				{	$show_error = 'display:block';	}			

				?>				
				<div class="travelo-box" id="error_box" style="<?php echo $show_error; ?>">
					<h3 class="page-title"><i class="glyphicon glyphicon-remove red mr10"></i><span class="msg"><?php echo $msg;?></span></h3>
				</div>

				<?php
				if ($show_RMN_form) {
				?>
				
				<?php
				$attributes = array("method" => "POST", "name" => "reset_rmn1_inp", "id" => "reset_rmn1_inp", "class" => "login_form");
				echo form_open('', $attributes);
				?>				
					<div class="col-md-12 form-group regwrap">
					<input type="text" minlength = "10" maxlength="10" placeholder="10 digit Registered Mobile Number"  name="username" id="username" data-rule-required="true" data-rule-username="true" data-msg-required="Please enter your 10 digit RMN" class="input-text input-large full-width" required>
					</div>					
					<div class="col-md-12">
					<button class="btn-large full-width loginbtn" id="reset_rmn1_submit" name="reset_rmn1_submit" type="button" >SUBMIT</button>
					</div>
				<?php echo form_close(); ?>
				<?php
				$attributes = array("method" => "POST", "name" => "reset_rmn1", "id" => "reset_rmn1");
				echo form_open(base_url().'admin/login/reset_username_authenticate', $attributes);
				?>								
				<input type="hidden" name="username_hidden" id="username_hidden" >									
				<?php echo form_close(); ?>
				
				<?php
				$attributes = array("method" => "POST", "name" => "reset_rmn2_inp", "id" => "reset_rmn2_inp", "class" => "login_form", "style" => "display:none");
				echo form_open('', $attributes);
				?>			
					<div class="col-md-6 form-group regwrap">
					<input type="text" minlength = "6" maxlength="6" placeholder="OTP received on RMN"  name="otp" id="otp" data-rule-required="true" data-rule-username="true" data-msg-required="Please enter your 6 digit OTP" class="input-text input-large full-width" required>
					</div>   
						
					<div class="col-md-6 form-group regwrap">
					<button class="btn-large full-width loginbtn resend_otp" id="resend_otp" name="resend_otp" type="button" >RESEND OTP</button>
					</div> 
					
					<div class="col-md-12 form-group">
					<input type="text" minlength = "10" maxlength="10" placeholder="New mobile number"  name="new_username" id="new_username" data-rule-required="true" data-rule-username="true" data-msg-required="Please enter your 10 digit New mobile number" class="input-text input-large full-width" required>
					</div> 
										
					<div class="col-md-6">
					<button class="btn-large full-width loginbtn" id="reset_rmn2_submit" name="reset_rmn2_submit" type="button" >SUBMIT</button>
					</div>
					
					<div class="col-md-6">
					<button class="btn-large full-width loginbtn back_button" id="back_button" name="back_button" type="button" >BACK</button>
					</div>				
				<?php echo form_close(); ?>
				<?php
				$attributes = array("method" => "POST", "name" => "reset_rmn2", "id" => "reset_rmn2");
				echo form_open(base_url().'admin/login/validate_username_otp', $attributes);
				?>								
				<input type="hidden" name="otp_hidden" id="otp_hidden" >
				<input type="hidden" name="new_username_hidden" id="new_username_hidden" >				
				<?php echo form_close(); ?>
				
				<?php
				$attributes = array("method" => "POST", "name" => "reset_rmn3_inp", "id" => "reset_rmn3_inp", "class" => "login_form", "style" => "display:none");
				echo form_open('', $attributes);
				?>									
					<div class="col-md-6 form-group regwrap">
					<input type="text" minlength = "6" maxlength="6" placeholder="OTP received on New mobile"  name="new_otp" id="new_otp" data-rule-required="true" data-rule-username="true" data-msg-required="Please enter your 6 digit OTP received on New mobile" class="input-text input-large full-width" required>
					</div>   
						
					<div class="col-md-6 form-group regwrap">
					<button class="btn-large full-width loginbtn resend_otp" id="resend_otp" name="resend_otp" type="button" >RESEND OTP</button>
					</div> 
					
					<div class="col-md-6">
					<button class="btn-large full-width loginbtn" id="reset_rmn3_submit" name="reset_rmn3_submit" type="button" >SUBMIT</button>
					</div>	
					
					<div class="col-md-6">
					<button class="btn-large full-width loginbtn back_button" id="back_button" name="back_button" type="button" >BACK</button>
					</div>
				<?php echo form_close(); ?>
				<?php
				$attributes = array("method" => "POST", "name" => "reset_rmn3", "id" => "reset_rmn3");
				echo form_open(base_url().'admin/login/validate_newusername_otp', $attributes);
				?>								
				<input type="hidden" name="new_otp_hidden" id="new_otp_hidden" >									
				<?php echo form_close(); ?>				
				
				<?php } ?>	

				<div class="clearfix" style="height: 10px;clear: both;"></div>
					
                </div>
            </div>
            <!--  login block end  -->
        </div>
    </div>
</section>
<script type="text/javascript">
var back = 'N';
$(document).ready(function () {	
	document.getElementById("reset_rmn1_inp").reset();
	document.getElementById("reset_rmn1").reset();
	
	document.getElementById("reset_rmn2_inp").reset();
	document.getElementById("reset_rmn2").reset();
	
	document.getElementById("reset_rmn3_inp").reset();
	document.getElementById("reset_rmn3").reset();
	
    $("#reset_rmn1_inp").validate({
        rules: {            
            username: {
                required: true,
				digits: true,                
                minlength: 10,
                maxlength: 10
            },            
        },
		messages:{
			username: {
				required: "Please enter mobile number",
				digits: "Please enter valid mobile number",
				minlength: "Please enter 10 digit mobile number",
				maxlength: "Please enter 10 digit mobile number"
			}                    
		}		
     });
	 
	 $(document).on("click","#reset_rmn1_submit",function() {		
		if($('#reset_rmn1_inp').valid())
		{
			disable_button('reset_rmn1_submit');
			
			var inp_form = $("#reset_rmn1_inp");
			var main_form = $("#reset_rmn1");				
			
			main_form.find("#username_hidden").val( btoa(inp_form.find("#username").val()) );			
			
			$.ajax({
				url: main_form.attr("action"),
				type: main_form.attr("method"),
				data: main_form.serialize(),				
				success: function(response) {					
					enable_button('reset_rmn1_submit');				
					if(response.msg_type == 'success')
					{
						document.getElementById("reset_rmn1_inp").reset();
						document.getElementById("reset_rmn1").reset();
						$('#reset_rmn1_inp').hide();
						$('#reset_rmn2_inp').show();												
					}
					else if(response.msg_type == 'error')
					{
						showNotification(response.msg_type, response.msg);						
					}
				}            
			});			
		}
	});
	 
	 $("#reset_rmn2_inp").validate({
        rules: {             
			otp: {
                required: true,
				digits: true,                
                minlength: 6,
                maxlength: 6
            },
			new_username: {
                required: true,
				digits: true,                
                minlength: 10,
                maxlength: 10,			
				valid_mobile_no: true,				
				mobileno_start_with:true
			}			
        },
		messages:{			
			otp: {
                required: "Please enter OTP",
				digits: "Please enter valid OTP",
				minlength: "Please enter 6 digit OTP",
				maxlength: "Please enter 6 digit OTP"
            },
			new_username: {
                required: "Please enter new mobile number",
				digits: "Please enter valid new mobile number",
				minlength: "Please enter 10 digit new mobile number",
				maxlength: "Please enter 10 digit new mobile number",
				valid_mobile_no: "Please Enter Valid Mobile No",				
				mobileno_start_with: 'Mobile No should start with 7 or 8 or 9'
            }			
		}		
     });
	 
	 $(document).on("click","#reset_rmn2_submit",function() {		
		if($('#reset_rmn2_inp').valid())
		{
			disable_button('reset_rmn2_submit');
			
			var inp_form = $("#reset_rmn2_inp");
			var main_form = $("#reset_rmn2");				
			
			main_form.find("#otp_hidden").val( btoa(inp_form.find("#otp").val()) );
			main_form.find("#new_username_hidden").val( btoa(inp_form.find("#new_username").val()) );			
			
			$.ajax({
				url: main_form.attr("action"),
				type: main_form.attr("method"),
				data: main_form.serialize(),				
				success: function(response) {					
					enable_button('reset_rmn2_submit');					
					if(response.msg_type == 'success')
					{
						document.getElementById("reset_rmn2_inp").reset();
						document.getElementById("reset_rmn2").reset();
						$('#reset_rmn2_inp').hide();
						$('#reset_rmn3_inp').show();												
					}
					else if(response.msg_type == 'error')
					{
						showNotification(response.msg_type, response.msg);						
					}
				}            
			});			
		}
	});	 
	 
	$.validator.addMethod("valid_mobile_no", function (value, element) {
		return (value == "0000000000" || value == "9999999999") ? false : true;
	}, 'Please enter valid mobile number');
	
	$.validator.addMethod("mobileno_start_with", function (value, element) {
		var regex = new RegExp("^[7-9]{1}[0-9]{9}$");
		if (value.length > 0)
		{
			if (regex.test(value))
			{
				return true;
			} else {
				return false;
			}
		} else
		{
			return true;
		}
	}, 'Mobile No. should start with 7 or 8 or 9 ');
	 
	 $("#reset_rmn3_inp").validate({
        rules: {            
            new_otp: {
                required: true,
				digits: true,                
                minlength: 6,
                maxlength: 6
            },            
        },
		messages:{
			new_otp: {
				required: "Please enter OTP",
				digits: "Please enter valid OTP",
				minlength: "Please enter 6 digit OTP",
				maxlength: "Please enter 6 digit OTP"
			}                    
		}		
     });
	 
	 $(document).on("click","#reset_rmn3_submit",function() {		
		if($('#reset_rmn3_inp').valid())
		{
			disable_button('reset_rmn3_submit');
			
			var inp_form = $("#reset_rmn3_inp");
			var main_form = $("#reset_rmn3");				
			
			main_form.find("#new_otp_hidden").val( btoa(inp_form.find("#new_otp").val()) );			
			
			$.ajax({
				url: main_form.attr("action"),
				type: main_form.attr("method"),
				data: main_form.serialize(),				
				success: function(response) {
					enable_button('reset_rmn3_submit');
					if(response.msg_type == 'success')
					{	
						document.getElementById("reset_rmn3_inp").reset();
						document.getElementById("reset_rmn3").reset();
						showNotification(response.msg_type, response.msg);							
						window.setTimeout(function(){
						window.location = BASE_URL;
						}, 5000);							
					}
					else if(response.msg_type == 'error')
					{					
						showNotification(response.msg_type, response.msg);						
					}
				}            
			});			
		}
	});
	 
});


$(document).on("click", ".resend_otp", function() {
	var form = $(this).closest("form");
	var form_id = form.attr('id');	
	if(form_id == 'reset_rmn2_inp')
	{
		resend_key = 'reset_rmn_old';
	}
	else if(form_id == 'reset_rmn3_inp')
	{
		resend_key = 'reset_rmn_new';
	}
	else
	{
		return false;
	}
	
	$.ajax({
		url: "<?php echo base_url(); ?>admin/login/resend_otp",
		method: "POST",		
		data: { resend_key : resend_key, rokad_token : rokad_token },		
		success: function(response) {			
			if(response.msg_type == 'success')
			{				
				showNotification(response.msg_type, "OTP sent successfully");			
			}
			else if(response.msg_type == 'error')
			{						
				form.find('.error_box').find(".msg").html(response.msg);
				form.find('.error_box').show();		
			}
		}            
	});	
});

$(document).on("click", ".back_button", function() {
	var form = $(this).closest("form");	
	console.log(form);

	if(form.attr('id') == 'reset_rmn2_inp')
	{		
		document.getElementById("reset_rmn2_inp").reset();
		document.getElementById("reset_rmn2").reset();		
		document.getElementById("reset_rmn1_inp").reset();
		document.getElementById("reset_rmn1").reset();
		$('#reset_rmn2_inp').hide();						
		$('#reset_rmn1_inp').show();
	}
	else if(form.attr('id') == 'reset_rmn3')
	{		
		document.getElementById("reset_rmn3_inp").reset();
		document.getElementById("reset_rmn3").reset();		
		document.getElementById("reset_rmn2_inp").reset();
		document.getElementById("reset_rmn2").reset();
		$('#reset_rmn3_inp').hide();						
		$('#reset_rmn2_inp').show();
	}
});

function disable_button(button_id)
{
	var temp = button_id+'_loading';
	$("#"+temp).remove();	
	var pdiv = $("#"+button_id).closest("div");
	pdiv.append( "<button class='btn-large full-width loginbtn' id='"+temp+"' type='button' >Processing</button>" );				
	$("#"+button_id).hide();
}

function enable_button(button_id)
{
	var temp = button_id+'_loading';
	$("#"+temp).remove();				
	$("#"+button_id).show();
}

$(document).off('keypress', '#username, #otp, #new_username, #new_otp').on('keypress', '#username, #otp, #new_username, #new_otp', function(e) 
{
   var regex = new RegExp("^[0-9\b]+$");//accept only _
   var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
	 key=e.charCode? e.charCode : e.keyCode;
	if(key==9 )
	{
		return true; 
	}
	if (regex.test(str)) 
	{
		return true;
	}
	e.preventDefault();
	return false;
});
</script>