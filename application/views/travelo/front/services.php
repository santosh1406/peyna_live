<section id="content">
    <div class="container">
        <div id="main">


        	<div class="col-md-12">
                <h2>What we do</h2>

                <div id="accordion1" class="toggle-container box">
                    <div class="panel style1">
                        <h2 class="panel-title">
                            <a data-parent="#accordion1" data-toggle="collapse" href="#acc1">Interstate travelers</a>
                        </h2>
                        <div id="acc1" class="panel-collapse collapse in">
                            <div class="panel-content">
                                <p>When traveling across cities in multiple states (eg. Bengaluru to Mumbai), travelers face the challenge of booking tickets along the way with different private or public transport operators. BoS offers a one-stop solution that enables for travelers the convenience of advance booking of bus tickets for the entire route in a single transaction.</p>
                            </div><!-- end content -->
                        </div>
                    </div>
                    
                    <div class="panel style1">
                        <h2 class="panel-title">
                            <a data-parent="#accordion1" data-toggle="collapse" href="#acc2" class="collapsed">Intrastate Multicity travelers</a>
                        </h2>
                        <div id="acc2" class="panel-collapse collapse">
                            <div class="panel-content">
                                <p>Often, within the single state too, travelers have to change buses in absence of direct routes. BoS ensures assured booking for seamless travel throughout the state by means of offering multiple bus SERVICES as per the commuter requirement.</p>
                            </div><!-- end content -->
                        </div>
                    </div>
                    
                    <div class="panel style1">
                        <h2 class="panel-title">
                            <a data-parent="#accordion1" data-toggle="collapse" href="#acc3" class="collapsed">Regular travelers</a>
                        </h2>
                        <div id="acc3" class="panel-collapse collapse">
                            <div class="panel-content">
                                <p>Many regular commuters like students, employees, businessman, sales representative etc. who require Bus pass for daily travel have to currently go through a time-consuming process of getting a bus pass. This process is now made simple and more convenient on the BoS portal for services offered by STU’s or Municipal Corporations</p>
                            </div><!-- end content -->
                        </div>
                    </div>

                    <div class="panel style1">
                        <h2 class="panel-title">
                            <a data-parent="#accordion1" data-toggle="collapse" href="#acc4" class="collapsed">Pilgrimage travelers</a>
                        </h2>
                        <div id="acc4" class="panel-collapse collapse">
                            <div class="panel-content">
                                <p>BoS enables people traveling to multiple places on pilgrimage to get their forward and return travel booking at one place. Besides this, BoS also allows travelers to make advance ‘Pooja’ bookings at their convenience and donations adding further to the convenience and also reducing the risk associated with carrying cash for donations and avoid waiting time for pooja</p>
                            </div><!-- end content -->
                        </div>
                    </div>

                    <div class="panel style1">
                        <h2 class="panel-title">
                            <a data-parent="#accordion1" data-toggle="collapse" href="#acc5" class="collapsed">State Transport Undertakings (STUs)</a>
                        </h2>
                        <div id="acc5" class="panel-collapse collapse">
                            <div class="panel-content">
                                <p>BoS offers “Distributorship (Agency) Model ticket booking franchise” for State Transport Undertakings</p>

                                <h4 class="s-title">Service offered to STUs:</h4>

                                <ul class="chevron box">
				                	<li>Innovative schemes from BOS will enable to know REALTIME seat AVAILABILITY in bus</li>
									<li>Counter booking  through BoS operated outlets1</li>
				                    <li>Booking through Authorized Booking agents</li>
				                  	<li>Web booking</li>
				                    <li>Booking through third party portals or outlets as an API</li>
				                </ul>

				                <h4 class="s-title">Solution offered to STUs:</h4>
				                <ul class="chevron box">
				                	<li>BoS will be the master distributor for selling STU tickets through BoS outlets &amp; Authorized Booking agents</li>
				                    <li>BoS will set up outlets all over the state</li>
				                    <li>BoS will invest in Infrastructure, Connectivity and Manpower required to setup these outlets</li>
				                    <li>Training and Technology related issues will be the responsibility of BoS</li>
				                    <li>Authorised booking agents will maintain pre-deposit wallet with BoS for booking tickets</li>
				                    <li>BoS will send MIS to STU on daily basis</li>
				                    <li>BoS will settle funds to STU on T+2 basis</li>
				                    <li>BoS will help in reconciliation of the transactions</li>
				                    <li>BOS can provide technology solutions and end-to-end software solutions as per requirements</li>
				                </ul>

				                <h4 class="s-title">Advantages &amp; Promotional offers to Citizens:</h4>
				                <ul class="chevron box">
				                	<li>Easy to book ticket from home and through mobile</li>
				                    <li>Cash less Transactions i.e payments through Credit/Debit cards and Net Banking</li>
				                    <li>Travelers can get status of the ticket availability from home or from any location through mobile or nearby outlet without travelling to Bus stations</li>
				                </ul>

				                <h4 class="s-title"></i>Advantages to STU:</h4>
				                <ul class="chevron box">
				                	<li>BoS will handle all the accounting of vendors and Authorised booking agents</li>
				                    <li>BoS will be the single point of contact (SPOC) for STU for any queries/issues</li>
				                    <li>There will be no reconciliation issues to STU and BoS account will be the single point of contact for reconciliation</li>
				                    <li>BoS will maintain pre-deposit with STU for transactions so there will be no scope for exposure</li>
				                </ul>
                            </div><!-- end content -->
                        </div>
                    </div>

                    <div class="panel style1">
                        <h2 class="panel-title">
                            <a data-parent="#accordion1" data-toggle="collapse" href="#acc6" class="collapsed">Private Bus Operators</a>
                        </h2>
                        <div id="acc6" class="panel-collapse collapse">
                            <div class="panel-content">
                                <p>BoS portal will act as an added avenue to sell inventory, leading to increased occupancy rate. BoS provides support to digitize the inventory of fleet owners and others with manual inventory. Integration of inventory on the portal leads to swiftness in ticket booking, reduced revenue leakages, easy reconciliation and revenue augmentation.</p>
                                <p>BoS will provide support on demand and supply gap. This analytics sharing, on regular basis, can be used by operators to optimize trip planning and in designing fare strategy. Additional trips can be operated on routes and seasons highlighted in the report.</p>

                                <h4 class="s-title"></i>Online ticket inventory has multifold benefits like:</h4>
                                <ul class="chevron box">
							    	<li>Realtime analytics of bus occupancy</li>
							        <li>Any change in the fare structure can be transmitted and implemented realtime, without any revenue lag</li>
							        <li>Marketing and visibility to private bus operators</li>
							        <li>REALTIME seat AVAILABILITY in bus</li>
							        <li>Launching of attractive and swift mobile apps for commuters for bus ticket booking</li>
							        <li>Transmission of ticket amount through RTGS  therefore no blocking of revenue.</li>
							        <li>Sell point of inventory increases exponentially, since it is available to B2C travelers via portal and huge number of agents via agent booking portal</li>
							    </ul>
                            </div><!-- end content -->
                        </div>
                    </div>

                    <div class="panel style1">
                        <h2 class="panel-title">
                            <a data-parent="#accordion1" data-toggle="collapse" href="#acc7" class="collapsed">Booking Agents</a>
                        </h2>
                        <div id="acc7" class="panel-collapse collapse">
                            <div class="panel-content">
                                <ul class="chevron box">
				                	<li>Passenger will have host of route/Bus type /timing/fare options to choose from the portal. Hence probability of commuter to book the ticket increases</li>
				                    <li>Attractive commission and various incentive schemes for high performing agents </li>
				                    <li>Prompt support in case of any technical/operational glitches in software of hardware</li>
				                    <li>Option to sell inventory of STUs, without the hassle of complying to authorized booking agent norms</li>
				                </ul>
                            </div><!-- end content -->
                        </div>
                    </div>



                </div>
            </div>


        </div>
    </div>
</div>