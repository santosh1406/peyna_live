<div class="page-title-container">
	<div class="container">
		<div class="page-title pull-left">
			<h2 class="entry-title">Failed Transaction</h2>
		</div>
	</div>
</div>

<section id="content">
	<div class="container">
		<div id="main">
			<?php
				if(count($falied_tansaction) > 0)
				{
					foreach ($falied_tansaction as $key => $value) 
					{	
			?>
				<div id="booking" class="bg_white">
					<div class="booking-history">
						<div class="booking-info clearfix">
							<div class="ticket_info">
								<div class="date">
									<label class="month"><?php echo date("M",strtotime($value["dept_time"]));?></label>
									<label class="date"><?php echo date("d",strtotime($value["dept_time"]));?></label>
									<label class="day"><?php echo date("D",strtotime($value["dept_time"]));?></label>
								</div>
								<h4 class="box-title">
									<i class="icon fa fa-bus yellow-color circle"></i>
									<span class="from"><?php echo $value["from_stop_name"];?></span> to <span class="to"><?php echo $value["till_stop_name"];?></span> 
									<small><?php echo $value["op_name"];?></small>
								</h4>
								<dl class="info">
									<dt>Mobile No</dt>
									<dd class="pnr_no"><?php echo isset($value["mobile_no"]) ? $value["mobile_no"] : "NA";?></dd>
									
								</dl>
								<button class="btn-mini status">FAILED</button>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			<?php
					}
				}
				else
				{
			?>
				<div id="booking" class="bg_white">
					<div class="booking-history">
						<div class="booking-info clearfix">
							<div style="cursor: pointer;" class="ticket_info">
								No failed transaction found.
							</div>
						</div>
					</div>
				</div>
			<?php
				}
			?>
        </div>
	</div>
</section>