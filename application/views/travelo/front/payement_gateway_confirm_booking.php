<?php
    $arrayname = "onwardJourney";
	if(isset($returnJourney) && count($returnJourney) > 0)
	{
		if(isset($returnJourney["block_time_minutes"]))
		{
			$arrayname = (strtotime($returnJourney["block_time_minutes"]) > strtotime($onwardJourney["block_time_minutes"])) ? "onwardJourney" : "returnJourney";
		}
	}
 
	$new_class_name = "mt135";
	if($is_agent_markup_onward == true || $is_agent_markup_return== true)
	{
		$new_class_name = "mt25";
	}

	$main_array = $$arrayname;
	$ttb_year = $main_array["ttb_year"];
	$ttb_month = $main_array["ttb_month"];
	$ttb_day = $main_array["ttb_day"];
	$ttb_hours = $main_array["ttb_hours"];
	$ttb_minutes = $main_array["ttb_minutes"];
	$ttb_seconds = $main_array["ttb_seconds"];
 
?>
<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Booking Detail</h2>
        </div>
       
    </div>
</div>
<section id="content" class="gray-area">
	<div class="container shortcode">
		<div id="main">
			<div class="row">
				<div class="col-md-8 payment-box-bg">
					<h2>Booking Details</h2>                   
					<div class="steps">
						<ul>
							<li><div class="square-number-bg">1</div>Select Seat & Boarding Point</li>
							<li><div class="square-number-bg">2</div>Travelers & Payment</li>
							<li class="active"><div class="square-number-bg-active">3</div>Booking Payment</li>
						</ul>
					</div>
					<?php
                        if(isset($onwardJourney["time_to_block"]) && $onwardJourney["time_to_block"] != "") {
                            echo '<div class="alert alert-notice payment-refresh-notice bg_blue marg-bottom">';
                            echo 'Please do not refresh page.';
                            echo '<span class="pull-right">Please complete your booking within next  <b><span id="remaining_time" class="red">  </span></b> minutes.</span>';
                            echo '</div>';
                        }
                        if ($onwardJourney["status"] == "FAILURE") {
                            echo '<div class="alert alert-error bg_red marg-bottom""> Transaction failure <b>' . $onwardJourney["error"] . ' </b>  <span class="pull-right"><a href="' . base_url() . 'home/" /> Home </a></span> </div>';
                        } 
                        else {
                    ?>
							<div class="block">
								<div id="main">
									<div class="tab-container full-width-style arrow-left dashboard">
										<ul class="tabs">
											<li class="active"><a data-toggle="tab" href="#wallet_payment" ><i class="soap-icon-businessbag circle"></i>Wallet Payment</a></li>
										</ul>
								
										<div class="tab-content"><!-- tab content start-->
											<div id="wallet_payment" class="tab-pane fade in active">
												<div class="tab-container full-width-style">
													<div class="row">
														<div class="col-md-12">
															<div class="tab-content">
																<div class="tab-pane fade in active" id="wallet-tab">
																	<h2 class="tab-content-title">Amount Payable : Rs. <b class="total_value"><?php echo $total_fare; ?></b></h2>
																	<span class="error-befor-button error-message-format"></span>
																	<?php
																		if($this->session->userdata('kyc_verify_status') == 'Y') {
																			if($wallet_balance > $total_fare) {
																			$attributes = array('class' => 'temp_booking_wallet', 'id' => 'temp_booking_wallet', 'method' => 'post');
																			echo form_open(base_url().'front/booking/do_confirm_booking/' . $onwardJourney["block_key"], $attributes);
																				if($is_agent_markup_onward || $is_agent_markup_return) {
																	?>
																					<div class="form-group row" style="margin-top:25px;"></div>
																					<div class="form-group row">
																						<div class="col-sm-12 col-md-12">
																							<span class="agent_markup_label font14">Agent Service Charge: Rs. </span>
																							<input type="text" name="agent_markup" id="agent_markup" value="" class="input-text" placeholder="Amount" required />
																						</div>
																					</div>
																					<div class="form-group row">
																						<div class="col-sm-12 col-md-12">
																							<span class="red"><b>Note: </b>Agent markup value is not applicable to any state transport.</span>
																						</div>
																					</div>
																	<?php
																				} 
																	?>
																					<div class="form-group row <?php echo $new_class_name;?>"></div>
																					<div class="form-group row">
																						<div class="col-sm-12 col-md-12">
																							<span class="agent_markup_label font14">T-PIN:</span>
																							<input type="password" name="t_pin" id="t_pin" value="" class="input-text" placeholder="T-PIN" maxlength="4" autocomplete="off" required />
																						</div>
																					</div>
																					<div class="form-group row">
																						<div class="col-sm-12 col-md-12">
																							<input type="checkbox" name="terms_and_conditions" id="terms_and_conditions" value="1" class="cursor-pointer" required>
																							<span class="ml5 font13">By Clicking on Make Payment, you agree to all the <a onclick="window.open(this.href, 'Terms & Conditions','left=20,top=20,width=1000,height=500,toolbar=1,scrollbars=yes,resizable=0'); return false;" href="<?php echo base_url() ?>terms_and_condition" class="active">Terms and Conditions </a></span>
																							<!-- <span class="ml5 font13">By Clicking on Make Payment, you agree to all the Terms and Conditions</span> -->
																						</div>
																					</div>
																					<?php
																						echo '<h2><label for="payment_gateway">Wallet Balance: <b> ';
																						
							                                                        	echo to_currency($actual_wallet_balance);
							                                                    		echo '</b></label></h2>';
																						echo '<input type="hidden" id="type_of_payment" name="type_of_payment" value="wallet"/>';
																						echo '<input type="hidden" id="wallet" name="payment_type" value="wallet"/>';
																						echo '<input type="hidden" id="user_coupon_code_wallet" name="user_coupon_code" value=""/>';
																						if(isset($returnJourney["block_key"]) && $returnJourney["block_key"] != "") {
																							echo '<input type="hidden" name="block_key_return" value="'.$returnJourney["block_key"].'" />';
																						}
																						echo '<button class="btn-large '.THEME_BTN.'" type="submit">MAKE PAYMENT</button>';
																						echo form_close();
																			}
																			else {
																				echo "<p>You do not have enough amount in your wallet to complete the transaction.</p>";
																				echo "<p>Please click <a class='underline' href='".base_url()."admin/users/get_profile#wallet' ><b><font color='red'>here</font></b></a> to recharge your wallet.</p>";
																			}
																		}
																		else {
																			echo "<p><b>Sorry, you can not proceed further as your KYC approval is pending. </p><p>Kindly contact Customer Care for more details. <b></p>";
																		}
																	?>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
						            </div>
						        </div>
					        </div>
					
					<?php
						}
					?>
				</div>
				<?php
				if($onwardJourney["status"] != "FAILURE")
				{
				?>				
				<div class="col-md-4">
					<div class="booking-details travelo-box payment_fare_details border-none">
						<h3>Payment Summary</h3>
						<article class="image-box cruise listing-style1 border-sh p10">
							<h3 class="fare_detail_header_new">Fare Detail</h3>
							<h5 class="fare_detail_subheader">Onward Journey</h5>
							<dl class="other-details">
								<dt class="feature">Total Ticket:</dt>
								<dd class="value"><?php echo $onwardJourney["total_seats"];?></dd>
								<dt class="feature">Ticket Fare:</dt>
								<dd class="value">Rs. <?php echo $onwardJourney["total_fare_without_discount"];?></dd>
								
								<?php
                                    
									if(!empty($onwardJourney["tax_charges"])) {
										foreach ($onwardJourney["tax_charges"] as $tkey => $tvalue)  {   
											echo "<dt class='feature'>".ucwords($tvalue->convey_charge_type_name).":</dt><dd class='value'>Rs. ".$tvalue->trans_val."</dd>";
										}
									}
								?>
								<dt class="total-price">Onward Journey Price</dt>
								<dd class="total-price-value onward-total-price-value">Rs. <?php echo $onwardJourney["total_fare"];?></dd>
								<?php 
									if(isset($onwardJourney["final_retailer_commission"]) && $onwardJourney["final_retailer_commission"] > 0 && $this->config->item('calculate_commission') ) {
								?>
                                    <dt class="feature">Commission:</dt>
									<dd class="value">Rs. <?php echo round($onwardJourney["retailer_commission"],2);?></dd>
									<dt class="feature">TDS:</dt>
									<dd class="value">- Rs. <?php echo round($onwardJourney["tds_on_retailer_commission"],2);?></dd>
									<dt class="feature">Net Commission:</dt>
									<dd class="value">Rs. <?php echo round($onwardJourney["final_retailer_commission"],2);?></dd>
								<?php
									}
								?>
                                    
							</dl>
							<!-- Return Journey Detail Start Here -->
							<?php
							if(isset($returnJourney) && count($returnJourney) > 0 && isset($returnJourney["total_fare"]) && $returnJourney["total_fare"] > 0 && isset($returnJourney["ticket_fare"]) && $returnJourney["ticket_fare"] > 0) {
							?>
								<br/>
								<h5 class="fare_detail_subheader">Return Journey</h5>
								<dl class="other-details">
									<dt class="feature">Total Ticket:</dt>
									<dd class="value"><?php echo $returnJourney["total_seats"];?></dd>
									<dt class="feature">Ticket Fare:</dt>
									<dd class="value">Rs. <?php echo $returnJourney["total_fare_without_discount"];?></dd>
									        
									<?php
                                        
										if(isset($returnJourney["tax_charges"]) && count($returnJourney["tax_charges"]) > 0) {
											foreach ($returnJourney["tax_charges"] as $tkey => $tvalue)  {   
												echo "<dt class='feature'>".ucwords($tvalue->convey_charge_type_name).":</dt><dd class='value'>Rs. ".$tvalue->trans_val."</dd>";
											}
										}
									?>
									
									<dt class="total-price">Return Journey Price</dt>
									<dd class="total-price-value return-total-price-value">Rs. <?php echo $returnJourney["total_fare"];?></dd>
									<?php 
									if($returnJourney["retailer_commission"] > 0 && $this->config->item('calculate_commission')) {
									?>
                                        <dt class="feature">Commission:</dt>
										<dd class="value">Rs. <?php echo round($returnJourney["retailer_commission"],2);?></dd>
										<dt class="feature">TDS:</dt>
										<dd class="value">- Rs. <?php echo round($returnJourney["tds_on_retailer_commission"],2);?></dd>
										<dt class="feature">Net Commission:</dt>
										<dd class="value">Rs. <?php echo round($returnJourney["final_retailer_commission"],2);?></dd>
									<?php
										}
									?>
								</dl>
							<?php
							}
							?>
							<dl class="other-details">
								<dt class="feature theme-orange-color coupon_discount_div hide">Coupon Discount</dt>
								<dd class="value theme-orange-color coupon_discount_value hide"></dd>
							</dl>
							<dl class="other-details grand-total">
								<dt>Grand Total</dt>
								<dd>Rs. <b class="total_value"><?php echo $total_fare;?></b></dd>
							</dl> 
							<!-- Return Journey Detail End Here -->
						</article>
					 </div><!--end of booking-details travelo-box -->

					 <?php
                                            eval(FME_AGENT_ROLE_ID);
                                            if(!in_array( $this->session->userdata("role_id"), $fme_agent_role_id))
                                            {
					?>
					
					<?php } ?>
				</div><!--end of col-sm-4 col-md-4 mt10 -->
				<?php
				}
				?>
			</div> <!-- class  row end -->
		</div> <!-- ID MAin End -->
	</div> <!-- class  container shortcode end -->
</section>
<?php
	$is_return_journey_condition_true = false;
	if(isset($returnJourney['block_key']) && $returnJourney["block_key"] != '' )
	{
		$is_return_journey_condition_true = true;
	}
?>
<script type="text/javascript">

    $(document).ready(function()
    {

        var cbooking_rules = new Object();
        var cbooking_messages = new Object();
        var cbooking_rules_w = new Object();
        var cbooking_messages_w = new Object();

        $.validator.addMethod('minStrict', function (value, el, param) {
            return value <= param;
        });

        cbooking_rules["payment_type"] = {required: true };
        cbooking_messages["payment_type"] = { required : "Something unwanted happend. Please try again." };
        cbooking_rules["terms_and_conditions"] = {required: true};
        cbooking_messages["terms_and_conditions"] = { required : "Please accept terms and condition."};
        if("<?php echo $is_return_journey_condition_true;?>" == true)
        {
        	cbooking_rules["return_journey_condition"] = {required: true};
        	cbooking_messages["return_journey_condition"] = { required : "Please accept cancellation policy."};
        }

        if("<?php echo $is_agent_markup_onward;?>" == true || "<?php echo  $is_agent_markup_return;?>" == true)
        {
            cbooking_rules["agent_markup"] = {required: agent_markup_Required,number: true, minStrict: <?php echo ORS_AGENT_MARKUP_AMOUNT;?>};
            cbooking_messages["agent_markup"] = { required : "Please enter agent markup value.",number:"Please provide numeric value",minStrict:"Agent mark up value must be less than or equal to <?php echo ORS_AGENT_MARKUP_AMOUNT;?>" };
        }

        cbooking_rules_w["payment_type"] = {required: true };
        cbooking_messages_w["payment_type"] = { required : "Something unwanted happend. Please try again." };
        cbooking_rules_w["terms_and_conditions"] = {required: true};
        cbooking_messages_w["terms_and_conditions"] = { required : "Please accept terms and condition."};
        
        if("<?php echo $is_agent_markup_onward;?>" == true || "<?php echo  $is_agent_markup_return;?>" == true)
        {
            cbooking_rules_w["agent_markup"] = {required: agent_markup_Required,number: true, minStrict: <?php echo ORS_AGENT_MARKUP_AMOUNT;?>};
            cbooking_messages_w["agent_markup"] = { required : "Please enter agent markup value.",number:"Please provide numeric value",minStrict:"Agent mark up value must be less than or equal to <?php echo ORS_AGENT_MARKUP_AMOUNT;?>" };
        }

        cbooking_rules_w["t_pin"] = {digits: true,maxlength:4,minlength:4,required: true};
        cbooking_messages_w["t_pin"] = { required : "Please input your T-PIN." , minlength : "Please enter valid T-PIN"};

        function agent_markup_Required() {
    			return $('#agent_markup').val().length > 0;
		}
		
        $("#temp_booking_wallet").validate({
            ignore: "",
            rules: cbooking_rules_w,
            messages: cbooking_messages_w,
            errorPlacement: function(error, element) {
            	bootstrapGrowl(error, 'danger', {"delay":"2000","align":"right"});
                
            }
        });
        
        
		$( "#temp_booking_wallet" ).submit(function( event ) {   
			
			var coupon_code = $('#couponcode').val();
			$('#user_coupon_code_wallet').val(coupon_code);
        });
    });

    $('#t_pin').bind("cut copy paste",function(e) {
          e.preventDefault();
      });

    $(document).off('keypress', '#t_pin').on('keypress', '#t_pin', function(e) 
    {
       var regex = new RegExp("^[0-9\b]+$");//accept only _
       var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
         key=e.charCode? e.charCode : e.keyCode;
        if(key==9 )
        {
            return true; 
        }
        if (regex.test(str)) 
        {
            return true;
        }
        e.preventDefault();
        return false;
    });
	
    window.onload=function(){
        if("<?php echo $ttb_year;?>" != "")
        {
            startCountDown(<?php echo $ttb_year;?>,<?php echo $ttb_month;?>,<?php echo $ttb_day;?>,<?php echo $ttb_hours;?>,<?php echo $ttb_minutes;?>,<?php echo $ttb_seconds;?>, "remaining_time","timeup", base_url);  
        }
    };
</script>