<style>
.error{
	margin-top:10px;
	margin-bottom:10px;
}
</style>

<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Verification</h2>
        </div>
    </div>
</div>

<section id="content">
    <div class="container tour-detail-page">
        <div class="row login-page">
            <div class="col-sm-8 col-md-6 col-lg-5 no-float no-padding center-block">			
				
				<?php
				$show_error = 'display:none';
				if ($msg_type == "error") 
				{	$show_error = 'display:block';	}
			
				$show_msg = 'display:none';
				if ($msg_type == "success") 
				{	$show_msg = 'display:block';	}
			
				?>				
				<div class="travelo-box" id="error_box" style="<?php echo $show_error; ?>">
					<h3 class="page-title"><i class="glyphicon glyphicon-remove red mr10"></i><span class="msg"><?php echo $msg;?></span></h3>
				</div>

				<div class="travelo-box" id="success_box" style="<?php echo $show_msg; ?>">
					<h3 class="page-title"><i class="soap-icon-check green mr10"></i><span class="msg"><?php echo $msg;?></span></h3>
				</div>			   
            </div>
        </div>
    </div>
</section>