<!-- Inner Wrapper Start -->
<div id="page-wrapper">

    <!-- Signin Content Start -->
    <section class="inner-wrapper">
        <div class="container" class="active" id="login-form">
            <div class="row">
                <?php if ($this->session->flashdata('msg_type') == 'success') { ?>
                    <div class="alert alert-success alert-dismissable">
                        <b>
                            <?php
                            echo $this->session->flashdata('msg');
                            ?></b>
                        <i class="fa fa-check"></i>
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                    </div>
                <?php } ?>
                <div class="col-md-6 col-md-offset-3">
                    <h2 class="text-center purple">Log-In</h2>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-12 col-sm-12 container-c">
                                <form class="form-horizontal" id="login_form" method="post" action="<?php echo base_url() . 'admin/Login/login_authenticate' ?>">
                                    <div class="form-group">
                                        <div class="cols-sm-12">
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                                <input type="text" class="form-control" minlength = "10" maxlength="10" placeholder="10 digit Registered Mobile Number"  name="username" id="cusername"  placeholder="Email Address / Mobile Number" data-rule-required="true" data-msg-required="Please enter your 10 digit RMN" autocomplete="off" required/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="cols-sm-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                                <input type="password" class="form-control" placeholder="Password" id="cpassword" name="password"  placeholder="Password" value="" data-rule-required="true" data-msg-required="Please enter password" autocomplete="off" required/>
                                                <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-purple btn-lg btn-block login-button txt-w" id="login-form-link">LOGIN TO YOUR ACCOUNT</button>
                                    </div>
                                    <div class="form-group">
                                        <div class="pull-left">Not a member? <a href="<?php echo base_url() . 'admin/Login/signup_view' ?>" id="register-form-link">Register</a></div>
                                        <div class="pull-right"><a href="#" id="reset-form-link">Forgot Password?</a></div>
                                    </div>
                                </form> 
                            </div>
                        </div>
                    </div>

                    <!--h4 class="text-center purple">Scan with the Rokad App to Login</h4>
                    <div id="qrcodeCanvas" align="center"></div>
                    <div class="col-sx-6 col-sm-6 col-md-6 col-lg-8 col-md-offset-2">
                        <div class="row ">
                            <div class="col-sx-3 col-sm-3 col-md-3 col-lg-2">
                                    <img src="images/qr-code-icon.png" class="img-responsive"/>
                            </div>
                            <div class="col-sx-9 col-sm-9 col-md-9 col-lg-9">
                                <p>Open Rokad app and tap on scan icon<br>
                                <a href="#">Learn more</a>
                                                                            </p>
                                <p>If you are not able to scan this QR code please update your Rokad app</p>
                            </div>
                        </div>
                    </div-->
                </div>
            </div>
        </div>

        <div class="container" id="reset-form" style="display: none;">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h2 class="text-center purple">Forgot Password</h2>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-12 col-sm-12 container-c">
                                <form class="form-horizontal" method="post" id="forgot-password-form" action="<?php echo base_url() . 'admin/login/forgot_password' ?>">
                                    <div class="form-group">
                                        <div class="cols-sm-12">
                                            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                                <input type="text" class="form-control" name="email" id="email"  placeholder="Please enter your Email" data-rule-required="true" data-msg-required="Please enter registered email address" autocomplete="off" required/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-purple btn-lg btn-block login-button txt-w">Reset Password</button>
                                    </div>
                                </form> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Signin Content End -->             
</div>
<!-- Inner Wrapper End -->

<!-- jQuery -->
<script src="<?php echo base_url() . FRONT_ASSETS; ?>js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url() . FRONT_ASSETS; ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() . FRONT_ASSETS; ?>js/jquery.qrcode.js"></script>
<script type="text/javascript" src="<?php echo base_url() . FRONT_ASSETS; ?>js/qrcode.js"></script>
<!--script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"async defer></script-->
<script type="text/javascript" src="<?php echo base_url() . COMMON_ASSETS; ?>js/aes.js"></script>
<script>
    function openNav() {
        if ($(window).width() < 981){
            document.getElementById("mySidenav").style.width = "100%";		
        }else{
            document.getElementById("mySidenav").style.width = "15%";
        }
	
	
        var width = $("#mySidenav").width();
        if (width !== 0){
            document.getElementById("mySidenav").style.width = "0";	
        }
    
    }

    function closeNav() {
	
        document.getElementById("mySidenav").style.width = "0";
    }
</script>
<script>
    $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>
<script>
    //jQuery('#qrcode').qrcode("this plugin is great");
    jQuery('#qrcodeCanvas').qrcode({
        text	: "http://jetienne.com"
    });	
</script>
<script>
    $(function() {
        /*$('#register-form-link').click(function(e) {
                $("#register-form").delay(100).fadeIn(100);
                $("#login-form").fadeOut(100);
                $('#login-form-link').removeClass('active');
                $(this).addClass('active');
                e.preventDefault();
        });*/
        $('#reset-form-link').click(function(e) {
            $("#reset-form").delay(100).fadeIn(100);
            $("#login-form").fadeOut(100);
            $('#login-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });

    });
</script>

<script type="text/javascript">
    (function($) {
        CryptoJSAesJson = {
            stringify: function (cipherParams) {
                var j = {ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64)};
                if (cipherParams.iv) j.iv = cipherParams.iv.toString();
                if (cipherParams.salt) j.s = cipherParams.salt.toString();
                return JSON.stringify(j);
            },
            parse: function (jsonStr) {
                var j = JSON.parse(jsonStr);
                var cipherParams = CryptoJS.lib.CipherParams.create({ciphertext: CryptoJS.enc.Base64.parse(j.ct)});
                if (j.iv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv);
                if (j.s) cipherParams.salt = CryptoJS.enc.Hex.parse(j.s);
                return cipherParams;
            }
        };
    })(jQuery);

    $(document).ready(function(){
        $('#cpassword').bind("cut copy paste drop",function(e) {
            e.preventDefault();
        });

        $('#cpassword').contextmenu(function() {
            return false;
        });      
        $("#login-form-link").click(function () {		
            if($('#login_form').valid()){		   
                var cpassword = $('#cpassword').val();
                var cusername = $('#cusername').val();
                $('#cpassword').val(CryptoJS.AES.encrypt(JSON.stringify(cpassword), cusername, {format: CryptoJSAesJson}).toString());			
            }
        });

        $('#login_form').validate({
            ignore: "",
            focusInvalid: false,
            rules: {
                username: {
                    required: true,
                    minlength: 10,
                    maxlength: 10,
                    number: true,
                },
                password: {
                    required: true,
                },
                //hiddenRecaptcha_login: {
                //required: function() {
                //if(grecaptcha.getResponse(greverify_login) == '') {
                //	return true;
                //} else {
                //    return false;
                //}
                //}
                //}
            },
            messages:{
                username: {
                    required: "Please enter your 10 digit RMN",				
                    minlength: "Please enter your 10 digit RMN",
                    maxlength: "Please enter your 10 digit RMN",
                    number: "Please enter your 10 digit RMN"
                },
                password: {
                    required: "Please enter password"			
                },
                //hiddenRecaptcha_login: {
                //	required: "Please verify captcha"			
                //}
            }		
        });

        $('#forgot-password-form').validate({
            ignore: "",
            focusInvalid: false,
            rules: {
                email: {
                    required: true,
                },
                //hiddenRecaptcha: {
                //	required: function() {
                //		if(grecaptcha.getResponse(greverify) == '') {
                //                    return true;
                //		} else {
                //                    return false;
                //		}
                //	}
                //}
            },
            messages:{
                email: {
                    required: "Please enter registered email address"	
                },
                //hiddenRecaptcha: {
                //required: "Please verify captcha"
                //}			
            }       
        });
    }); // end of document ready

</script>