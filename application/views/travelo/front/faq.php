<!--<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">FAQ's</h2>
        </div>
    </div> 
</div>-->
 <section id="content" class="gray-area abouttop">
<div class="termsimg"><img src="<?php echo base_url();?>assets/travelo/front/images/faq1.jpg" >
<h2>FAQ's</h2>
</div>
    <div class="container shortcode">
        <div class="block">
            <div class="row">
                <div class="col-sm-12">
                    <div class="toggle-container box faq_toggle faqwrapper" id="accordion1">
                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a href="#acc1" data-toggle="collapse" data-parent="#accordion1">
                                   What are the benefits of online bus ticket booking through Rokad platform? 
                                </a>
                            </h4>
                            <div class="panel-collapse collapse in" id="acc1">
                                <div class="panel-content">
                                    <p>
                                       Rokad platform provides online bus ticket booking service with an ample choice to select bus operators, departure and arrival timings, bus type, etc. Payment through Rokad Wallet is quick and secure.
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>
                        
                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc2" data-toggle="collapse" data-parent="#accordion1">
                                  Whether registration to Rokad platform is mandatory to avail bus ticket booking service?
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc2">
                                <div class="panel-content">
                                    <p>
                                        Yes. Registration to Rokad platform is a mandatory requirement to book tickets. As a part of registration process, basic details are captured like Name, Gender, Address, Age, Mobile Number and Email ID. 
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>
                        
                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc3" data-toggle="collapse" data-parent="#accordion1">
                                   What do I need to prepare before booking online? 
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc3">
                                <div class="panel-content">
                                    <p>
                                       All you need is to have a computer / laptop / Smart Phone with an internet connection. You just need to log-in and explore options to book a bus ticket. You need to ensure your Rokad Wallet is with sufficient balance to book a bus ticket.
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>


                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc4" data-toggle="collapse" data-parent="#accordion1">
                                  How do I book bus ticket through Rokad platform? 
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc4">
                                <div class="panel-content">
                                    <p>Booking a bus ticket through Rokad is very simple and quick:</p>
                                        <ul class="chevron box">
                                          <li>Select a source and destination location</li>
                                          <li>Select a date of departure / return and select 'Search' option</li>
                                          <li>Select a desirable trip from the available options </li>
                                          <li>Select a 'Select Seat' option to populate number of passenger(s) travelling</li>
                                          <li>Select a 'Pick Up' and 'Drop Off' option and select 'Continue' option</li>
                                          <li>Populate 'Booking Details' and select 'Confirm Booking' option</li>
                                          <li>Payment will be completed through Rokad Wallet </li>
                                        </ul>
                                    
                                </div><!-- end content -->
                            </div>
                        </div>



                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc5" data-toggle="collapse" data-parent="#accordion1">
                                  Will I see a confirmation message after I have made a booking online? 
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc5">
                                <div class="panel-content">
                                    <p>
                                       Yes, you can see a confirmation page after making a successful booking. Please communicate to Rokad support in case you do not see confirmation page.
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>

                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc6" data-toggle="collapse" data-parent="#accordion1">
                                    Whether partial cancellation is acceptable (Eg: 3 to 2 seats)? 
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc6">
                                <div class="panel-content">
                                    <p>
                                        No, partial cancellation is not allowed. 
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>


                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc7" data-toggle="collapse" data-parent="#accordion1">
                                   Will I receive intimation for successful booking of the ticket? 
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc7">
                                <div class="panel-content">
                                    <p>
                                        Yes, Rokad will send SMS which includes Passenger Name Record (PNR) number.
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>


                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc8" data-toggle="collapse" data-parent="#accordion1">
                                    Whether ticket printout is mandatory? 
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc8">
                                <div class="panel-content">
                                    <p>
                                        SMS and mobile email tickets are accepted by majority of operators as valid ticket, as a go-green initiative. However, a few bus operators may insist on hard copy of ticket, this shall be informed to the passenger at the time of ticket booking. 
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>


                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc9" data-toggle="collapse" data-parent="#accordion1">
                                   How can I cancel my ticket? 
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc9">
                                <div class="panel-content">
                                    <p>
                                       Yes, ticket can be cancelled by log-in to Rokad. Cancellation policy are as per individual bus operator. We recommend to skim through cancellation policy prior to booking.
                                        <?php
                                        /*
                                            Yes, we do allow partial cancellations. However, in certain  State Transport Undertaking like UPSRTC partial cancellation is not allowed. 
                                            However, please place your cancellation request on our website or through our call center 24 hrs prior to the departure time. 
                                        */
                                        ?>
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>


                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc10" data-toggle="collapse" data-parent="#accordion1">
                                  What is Rokad's refund policy, if I cancel my ticket? 
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc10">
                                <div class="panel-content">
                                    <p>
                                       Cancel ticket refund are as per the bus oprator's cancellation policy. Refund will be credited to your Rokad Wallet.
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>



                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc11" data-toggle="collapse" data-parent="#accordion1">
                                  How can I reach you in case of any additional information?
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc11">
                                <div class="panel-content">
                                    <p>
                                      If you have other questions please <a href="https://www.rokad.in/contact_us" class="clickhere">click here</a>
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>


                   


                 


                        <!-- ------------ -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>