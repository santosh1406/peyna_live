<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <!-- <h2 class="entry-title">Maintenance</h2> -->
        </div>
    </div>
</div>

<section id="content">
    <div class="container tour-detail-page">
        <div class="row">
            <div class="col-md-12" id="main">
                
                <div class="travelo-box" id="tour-details">
                    <center>
                        <img src="<?php echo base_url().FRONT_ASSETS;?>images/logo.png?token=bookonspot" alt="Online Ticket Booking System" />
                        <h1 class="page-title">System Under Maintenance</h1>
                        <h4 class="">System is in maintenance. Please try again after sometime.</h4>
                    </center>
                </div>
            </div>
        </div>
    </div>
</section>
