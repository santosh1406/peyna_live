<script type="text/javascript">
(function($) {
	onloadCallback = function() {	
		greverify_login = grecaptcha.render('g_recaptcha_login', {
		'sitekey' : "<?php echo $this->config->item('google_captcha_site_key'); ?>"	
		});

		greverify = grecaptcha.render('g_recaptcha', {
		'sitekey' : "<?php echo $this->config->item('google_captcha_site_key'); ?>"	
		});
	};	
})(jQuery);
</script>

<section id="content" class="abouttop">
<div class="termsimg"><img src="<?php echo base_url();?>assets/travelo/front/images/login1.jpg" >
<h2 id="header_lable">Login</h2>
</div>
    <div class="container">
        <div id="main" class="login-page forgotwrap">
            <div id="login-block">
                <!-- <p style="font-size: 1.5em;" class="block">Please login to your account.</p> -->
            
                <div class="col-sm-8 col-md-6 col-lg-5 no-float no-padding center-block">
					<div class="clearfix"></div>
                    <?php
                        $attributes = array("method" => "POST", "id" => "login_form", "class" => "login-form");
                        echo form_open(base_url().'admin/login/login_authenticate', $attributes);
                    ?>
                        <div class="form-group regwrap">
                            <input type="text" minlength = "10" maxlength="10" placeholder="10 digit Registered Mobile Number"  name="username" id="cusername" class="input-text input-large full-width cusername" data-rule-required="true" data-msg-required="Please enter your 10 digit RMN" autocomplete="off" required>
                        </div>
                        <div class="form-group">
                            <input type="password" placeholder="Password" id="cpassword" name="password" class="input-text input-large full-width cpassword" data-rule-required="true" data-msg-required="Please enter password" autocomplete="off" required>
                        </div>
						<span id="LoginError"></span>
                        <div class="form-group">
                        <!--    <label class="checkbox pull-left">
                                <input type="checkbox"  id="remember" name="remember" value="on">Remember Me                                    </label>-->
                            <a class="forgot-password pull-right" href="javascript:void(0)">Reset Password</a>
                            <div class="clearfix"></div>
                        </div>
						
						<div class="form-group col-md-12">
							<div class="g_recaptcha_login" id="g_recaptcha_login"></div>
							<input type="hidden" class="hiddenRecaptcha_login" name="hiddenRecaptcha_login" id="hiddenRecaptcha_login" data-rule-required="true" data-msg-required="Please verify captcha" autocomplete="off" required />
						</div>
						
                        <span class="error-befor-button error-message-format"></span>
                        <button class="btn-large full-width loginbtn" id="signinButton" type="submit">LOGIN TO YOUR ACCOUNT</button>
                    <?php echo form_close(); ?>
                </div>

            </div>
            <!--  login block end  -->


            <!--  Forgor Passwod block start  -->
            <div id="forgot-password-block" style="display:none" class="col-sm-8 col-md-6 col-lg-5 no-float no-padding center-block">
                <p style="font-size: 1.5em;" class="block">Email</p>                        
                <?php
                    $attributes = array("method" => "POST", "id" => "forgot-password-form", "class" => "forgot-password-form");
                    echo form_open( base_url().'admin/login/forgot_password', $attributes);
                ?>
                    <div class="form-group col-md-8">
                        <input type="text" placeholder="Please enter your Email"  name="email" id="email" class="input-text input-large full-width" data-rule-required="true" data-msg-required="Please enter registered email address" autocomplete="off" required >
                    </div>

                    <div class="form-group col-md-12">
                        <div class="g_recaptcha" id="g_recaptcha"></div>
                        <input type="hidden" class="hiddenRecaptcha" name="hiddenRecaptcha" id="hiddenRecaptcha" data-rule-required="true" data-msg-required="Please verify captcha" autocomplete="off" required />
                    </div>
					
					<div class="form-group col-md-12">
					 <span class="error-forgot-password error-message-format"></span>
                    <button class="btn-large btnorange" id="send_password" type="submit">Submit</button>
                    <button class="btn-large btnorange" id="login_view" type="button">Go back to Login</button>                        
                    </div>                   

                <?php echo form_close(); ?>

                <div>
                
                </div>
            </div>
            <!--  Forgor Passwod block end  -->
        </div>
    </div>
</section>

<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"async defer></script>
<script type="text/javascript" src="<?php echo base_url().COMMON_ASSETS; ?>js/aes.js"></script>
<script type="text/javascript">
(function($) {
	CryptoJSAesJson = {
		stringify: function (cipherParams) {
			var j = {ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64)};
			if (cipherParams.iv) j.iv = cipherParams.iv.toString();
			if (cipherParams.salt) j.s = cipherParams.salt.toString();
			return JSON.stringify(j);
		},
		parse: function (jsonStr) {
			var j = JSON.parse(jsonStr);
			var cipherParams = CryptoJS.lib.CipherParams.create({ciphertext: CryptoJS.enc.Base64.parse(j.ct)});
			if (j.iv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv);
			if (j.s) cipherParams.salt = CryptoJS.enc.Hex.parse(j.s);
			return cipherParams;
		}
	};
})(jQuery);

$(document).ready(function(){
    
    $("#signinButton").click(function () {		
       if($('#login_form').valid()){		   
			var cpassword = $('#cpassword').val();
			var cusername = $('#cusername').val();
			$('#cpassword').val(CryptoJS.AES.encrypt(JSON.stringify(cpassword), cusername, {format: CryptoJSAesJson}).toString());			
	   }
    });

    $('#login_form').validate({
        ignore: "",
        focusInvalid: false,
        rules: {
            username: {
                required: true,
                minlength: 10,
                maxlength: 10,
                number: true,
            },
            password: {
                required: true,
            },
			hiddenRecaptcha_login: {
				required: function() {
					if(grecaptcha.getResponse(greverify_login) == '') {
						return true;
					} else {
						return false;
					}
				}
			}
        },
		messages:{
			username: {
				required: "Please enter your 10 digit RMN",				
				minlength: "Please enter your 10 digit RMN",
				maxlength: "Please enter your 10 digit RMN",
				number: "Please enter your 10 digit RMN"
			},
			password: {
				required: "Please enter password"			
			},
			hiddenRecaptcha_login: {
				required: "Please verify captcha"			
			}
		}		
    });

    $('#forgot-password-form').validate({
        ignore: "",
        focusInvalid: false,
         rules: {
            email: {
                required: true,
            },
			hiddenRecaptcha: {
				required: function() {
					if(grecaptcha.getResponse(greverify) == '') {
						return true;
					} else {
						return false;
					}
				}
			}
        },
		messages:{
			email: {
				required: "Please enter registered email address"	
			},
			hiddenRecaptcha: {
				required: "Please verify captcha"
			}			
		}       
    });
}); // end of document ready

// $('.forgot-password').click(function(){
$(document).off('click', '.forgot-password').on('click', '.forgot-password', function(e) {
    $('#login-block').hide();
    $('#forgot-password-block').slideDown('medium');
	$("#header_lable").html("RESET PASSWORD");
});

// $('#login_view').click(function(){
$(document).off('click', '#login_view').on('click', '#login_view', function(e) {
    $('#forgot-password-block').hide();
    $('#login-block').slideDown('medium');
	$("#header_lable").html("LOGIN");
});

</script>