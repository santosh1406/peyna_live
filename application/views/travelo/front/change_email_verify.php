
<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Email Verification</h2>
        </div>
    </div>
</div>

<section id="content">
    <div class="container tour-detail-page">
        <div class="row">
            <div class="col-md-9" id="main">
                <div class="travelo-box" id="tour-details">
                    <?php
                        if ($msg_type == "error") {
                    ?>
                     <h1 class="page-title"><i class="glyphicon glyphicon-remove red mr10"></i><?php echo $msg;?></h1>
                   
                   <?php } else { ?>
                             <h1 class="page-title"><i class="soap-icon-check green mr10"></i><?php echo $msg;?></h1>
                    <h4 class="no-padding">
                        
                    </h4>
                    
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
