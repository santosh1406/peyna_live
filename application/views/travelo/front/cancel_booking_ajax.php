<?php

    if(count($ticket_detail) == 0)
    { 
    echo "<font size=4>Sorry,There is no ticket to display</font>";

    }
    else{
    ?>

    

    <div class="traveldiv">
        <div class="searchfilter">My Ticket</div>
        <div class="traveldetails">
            <ul class="BusList">
                <li>
                    <div class="busdetaildiv">
                        <div class="scol4 busdetailscol clearfix">
                            <h3>PNR No.</h3>
                        </div>
                        <div class="scol2 busdetailscol clearfix">
                            <h3>Operator</h3>
                        </div>
                        <div class="scol4 busdetailscol clearfix">
                            <h3>Departure - Time</h3>
                        </div>
<!--                        <div class="scol2 busdetailscol clearfix">
                            <h3>Total Psgrs</h3>
                        </div>-->
                        <div class="scol2 busdetailscol clearfix">
                            <h3>Boarding</h3>
                        </div>
                        <div class="scol2 busdetailscol clearfix">
                            <h3>Destination</h3>
                        </div>
                        <div class="scol1 busdetailscol clearfix">
                            <h3>Fare</h3>
                        </div>
                    </div>
                </li>

                <?php foreach($ticket_detail as $key=> $info): ?>
                    <li>
                        <div class="busdetaildiv">
                            <div class="scol4 busdetailscol clearfix">
                                <h4><?php echo $info['pnr_no']; ?></h4>
                            </div>
                            <div class="scol2 busdetailscol clearfix">
                                <h4><?php echo $info['op_name']; ?></h4>
                            </div>
                            <div class="scol4 busdetailscol clearfix">
                                <h4><?php echo $info['dept_time']; ?></h4>
                            </div>
<!--                            <div class="scol2 busdetailscol clearfix">
                                <h4><?php echo $info['num_passgr']; ?></h4>
                            </div>-->
                            <div class="scol2 busdetailscol clearfix">
                                <h4><?php echo $info['from_stop_cd']; ?></h4>
                            </div>
                            <div class="scol2 busdetailscol clearfix">
                                <h4><?php echo $info['till_stop_cd']; ?></h4>
                            </div>
                            <div class="scol1 busdetailscol clearfix">
                                <h4><?php echo $info['tot_fare_amt']; ?>
                                    <input type="hidden" id="op_name" name="op_name" value="<?php echo $info['op_name']?>"></h4>
                            </div>
                            <div class="scol2 busdetailscol clearfix">
                                <a class="bus" href="javascript:void(0);"></a>
                                <button class="selectbusbtn cancelticketbtn" id="selectbtn1" value="<?php echo $info['ticket_ref_no']; ?>">Cancel Ticket</button>
                            </div>
                            
                            
                        </div>

                        <div style="display:" class="busdiv1 busdivblock busdivblock_<?php echo $info['ticket_no'] ?> clearfix">
                            <div class="busseatdiv clearfix">
                                <div class="canceltcktleft">
                                    <div class="busseats clearfix">
                                        <ul class="BusList">
                                            <li>
                                                <div class="busdetaildiv">
                                                    <div class="scol1 busdetailscol clearfix">
                                                        <h4>Sr.No</h4></div>
                                                    <div class="scol4 busdetailscol clearfix">
                                                        <h4>Passenger Name</h4></div>
                                                    <div class="scol1 busdetailscol clearfix">
                                                        <h4>Age</h4></div>
                                                    <div class="scol1 busdetailscol clearfix">
                                                        <h4>Gender</h4></div>
                                                    <div class="scol2 busdetailscol clearfix">
                                                        <h4>Seat No.</h4></div>
                                                    <div class="scol2 busdetailscol clearfix">
                                                        <h4>Fare</h4></div>
                                                    <div class="scol2 busdetailscol clearfix">
                                                        <h4>Select</h4></div>
                                                </div>
                                            </li>
                                            <?php 
                                                $total_amt = 0.00;
                                                foreach ($info['detail_arr'] as $td_key => $td_value) { 

                                                    $total_amt += $td_value->fare_amt;
                                            ?>

                                                <li class="list">
                                                    <div class="busdetaildiv">
                                                        <div class="scol1 busdetailscol clearfix">
                                                            <h5>1</h5></div>
                                                        <div class="scol4 busdetailscol clearfix">
                                                            <h5><?php echo $td_value->psgr_name ?></h5></div>
                                                        <div class="scol1 busdetailscol clearfix">
                                                            <h5><?php echo $td_value->psgr_age ?></h5></div>
                                                        <div class="scol1 busdetailscol clearfix">
                                                            <h5><?php echo $td_value->psgr_sex ?></h5></div>
                                                        <div class="scol2 busdetailscol clearfix">
                                                            <h5><?php echo $td_value->seat_no ?></h5></div>
                                                        <div class="scol2 busdetailscol clearfix">
                                                            <h5><?php echo $td_value->psgr_name ?></h5>
                                                        </div>
                                                        <?php if($td_value) {  ?> 
                                                        <div class="scol2 busdetailscol clearfix">

                                                            <input type="checkbox" class="tktid"
                                                                   
                                                                   id="<?php echo $td_value->ticket_det_id ?>" 
                                                                   data-seat="<?php echo $td_value->seat_no ?>" 
                                                                   value="<?php echo $info['pnr_no'] ?>" name="" />
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                </li>

                                            <?php  } ?>
                                            
                                        </ul>
                                    </div>
                                    <div style="display:" class="cancelbtn">
                                        <div class="clearfix">

                                            <button class="continueBtn cancel_btn" data-opname="<?php echo $info['op_name'] ?>"  data-pnr="<?php echo $info['pnr_no'] ?>"  data-ref="<?php echo $info['ticket_no'] ?>" data-ticket-id="<?php echo $info['ticket_id'] ?>" >Cancel Ticket</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="canceltcktinfo">
                                    <div class="pricerate">
                                        <p>In case of e-Ticket cancellation; refundable amount, cancellation charges and other charges would applicable at the time of cancellation.</p>
                                        <p>Human resource charges, toll tax, octrai(if applied) and accidental surcharge would be completely refundable.</p>
                                        <p>RSRTC calculate cancellation charges on base fare of e-Ticket.</p>
                                        
                                    </div>
                                </div>
                                <div class="canceltcktrght">
                                    <div class="pricerate">
                                        <div class="seatselectedcount clearfix"><label>Seat(s) :</label><b><span class="seats"> 8</span></b></div>
                                        <div class="seatamount clearfix"><label>Fare :<label id="fare"></label> </label><b><span class="fare"></span></b></div>
                                        <div class="seatamount clearfix"><b><label>Service Tax : </label></b><span></span></div>
                                        <div class="seatamount clearfix"><b><label>Total Fare : </label></b><b><span class="tfare"><?php echo $total_amt ?></span></b></div>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </li>

                 <?php endforeach;?>
            </ul>


            <div class="paginate wrapper" align="right">
                
               <?php
               
               if(isset($links)) echo $links;
               
               ?>
            </div>

        </div>
    </div>   

<?php } ?>
