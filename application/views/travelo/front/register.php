<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Register</title> 
        <script src="<?php echo base_url() . COMMON_ASSETS; ?>js/jquery.js" type="text/javascript"></script>
        <script src="<?php echo base_url() . COMMON_ASSETS; ?>js/jquery.validate.min.js" type="text/javascript"></script>
        <script>
            // assumes you're using jQuery
            $(document).ready(function () {

                /************** for confirm message***********/
                $('.confirm-div').hide();
<?php if ($this->session->flashdata('invalid')) { ?>
                    $('.confirm-div').html('<?php echo $this->session->flashdata('invalid'); ?>').show();
<?php } ?>
                /*********************************************/

                //$("#register_form").validate();
                $("#register_form").validate({
                    rules: {
                        username: {
                            required: true,
                            minlength: 5
                        },
                        password: {
                            required: true,
                            minlength: 8
                        },
                        confirm_password: {
                            equalTo: "#password"
                        }
                    }
                });
            });
        </script>
    </head>

    <body>
        <div class="confirm-div"></div>

        <div class="container login">
            <?php
            $attributes = array("method" => "POST", "id" => "register_form", "class" => "cmxform");
            echo form_open('login/register_authenticate', $attributes);
            ?>
            <fieldset>
                <p>
                    <label for="fullname">Full Name *</label>
                    <input type="text" id="fullname" name="fullname" data-rule-required="true" data-rule-fullname="true" data-msg-required="Please enter your fullname">
                </p>
                <p>
                    <label for="email">Email *</label>
                    <input type="text" id="email" name="email" data-rule-required="true" data-rule-email="true" data-msg-required="Please enter your email">
                </p>
                <p>
                    <label for="username">Username *</label>
                    <input type="text" id="username" name="username" data-rule-required="true" data-rule-username="true" data-msg-required="Please enter your username">
                </p>
                <p>
                    <label for="password">Password *</label>
                    <input type="password" id="password" name="password" data-rule-required="true" data-rule-password="true" data-msg-required="Please enter your password" >
                </p>
                <p>
                    <label for="password">Confirm Password *</label>
                    <input type="password" id="confirm_password" name="confirm_password" data-rule-required="true" data-rule-password="true" data-msg-required="Please enter your password" >
                </p>
                <p>
                    <input class="submit" type="submit" value="Register">
                </p>
            </fieldset>
            <?php echo form_close(); ?>
        </div>
    </body>
</html>