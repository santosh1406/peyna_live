<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title"><?php echo isset($custom_title) ? $custom_title : "Error";?></h2>
        </div>
    </div>
</div>
<section id="content" class="gray-area">
    <div class="container">
        <div class="row">
            <div id="main" class="col-sm-12 col-md-12">
                <div class="booking-information travelo-box">
                    <h2><?php echo isset($custom_heading) ? $custom_heading : "Error";?></h2>
                    <hr />
                    <div class="booking-confirmation clearfix">
                        <i class="fa fa-thumbs-down icon circle"></i>
                        <div class="message">
                            <h4 class="main-message">
                                <?php echo isset($custom_message) ? $custom_message : "Error Message";?>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>

