<script type="text/html" id="bus_service_tpl">
  <article class="box sort_list fare_sort_list" data-fare="" data-seat="" data-bus-type="" data-arrival="" data-dept="" data-travel="" data-duration-time="">
      <div class="details col-sm-12">
          <div class="box-tag-img"></div>
          <div class="clearfix">
              <h4 class="box-title pull-left">
                <span class='op_name'></span>
                <span class="theme-green-color"><b class='from-to-to'></b></span>
                <small class="bus_type"></small> 
              </h4>

              <span class="pull-right seat_fare flft">
                <span class="fare_span" >                                      
                  <a href="javascript:void(0)" data-ref="" class="getfare">Get Fare</a>
                </span>    
              </span>
              
          </div>
          <div class="character clearfix">
              <div class="col-xs-1 date tabledis">
                  <i class="soap-icon-adventure yellow-color"></i>
                  <div class="boarding_droping_point">
                      <span class="skin-color">Departure</span><br>
                      <span class="sch_departure_date"></span>
                  </div>
                  <div class="boarding_point arrow_box" style="display: none;">
                      <div class='title'>
                        Departure (Boarding Point) 
                        <span class='pull-right quick-view'>
                          <i class="fa fa-info-circle"></i>
                        </span>
                      </div>
                      <ul class="barr">
                      </ul>
                  </div>
              </div>
              <div class="col-xs-1 departure tabledis">
                  <i class="soap-icon-departure yellow-color"></i>
                  <div class="boarding_droping_point">
                      <span class="skin-color">Alighting</span><br>
                      <span class="arrival_tm"></span>
                  </div>
                  <div class="droping_point arrow_box" style="display: none;">
                      <div class='title'>
                        Alighting (Dropping Point)
                        <span class='pull-right quick-view'>
                          <i class="fa fa-info-circle"></i>
                        </span>
                      </div>
                      <ul class="darr">
                      </ul>
                  </div>
              </div>
              <div class="col-xs-1 departure tabledis">
                  <i class="soap-icon-clock yellow-color"></i>
                  <div>
                      <span class="skin-color">Duration</span><br>
                      <span class="short_travel_duration"></span>

                  </div>
              </div>
              <div class="col-xs-1 departure tabledis">
                  <i class="soap-icon-passenger yellow-color"></i>
                  <div>
                      <span class="skin-color">Available</span><br>
                      <span class="tot_seat_available"></span>

                  </div>
              </div>
              <div class="col-xs-1 departure tabledis">
                  <div class="amenities-n">
                      <i class="fa fa-mobile circle mtooltip" data-toggle="tooltip" data-placement="top" title=""></i>
                      <i class="soap-icon-notice popup_all_details circle active cursor-pointer" data-toggle="modal" data-target=".all_details" title="Bus Details & Cancellation Policy"></i>
                      <i class="soap-icon-close bus_cancellation_policy circle active cursor-pointer" data-toggle="modal" data-target=".bus_cancellation_policy_popup" title="Cancellation Policy"></i>
                  </div>
                  <div class="ttp_ticket_cancellation_policy arrow_box" style="display: none;">
                    <div class='title'>
                      Cancellation Policy
                      <span class='pull-right quick-view'>
                        <i class="fa fa-info-circle"></i>
                      </span>
                    </div>
                      <div class="table-responsive">
                        <table class="table font13">
                            <thead>
                              <tr>
                                <th>Cancellation Time</th>
                                <th>Charges</th>
                              </tr>
                            </thead>
                            <tbody>
                              
                            </tbody>
                        </table>
                      </div>
                  </div>
              </div>
              <div class="col-xs-1 departure tabledis">
                  <a href="javascript:void(0)" class="button btn-small seat_select_button selectbusbtn get_bus_seat <?php echo THEME_BTN;?>" data-trip-no="" data-op-name="" op-trip-no="" data-from="" data-to="" data-date="" api-type="" api-name="" inventory-type="">Select Seat</a>
              </div>
          </div>
          
          <div style="display:none;" class="bus_seat_div busdiv1 clearfix" id="">
            <center><span class="loader_25"> <img src="<?php echo base_url().COMMON_ASSETS ;?>images/loading.gif" alt="BOS Loading"></span></center>
          </div>

      </div>
  </article>
</script>

<script type="text/template" id="seat_layout_tpl">
<?php
  $attributes = array("class" => "temp_booking", "name" => "temp_booking", "onsubmit" => "return false");
  echo form_open('#', $attributes);
?>
<div class="clearfix seat_block">
    <div class="col-md-12">
        <h3 class="more_seat_detail red" style="display:none;">Maximum 6 seats allowed per booking</h3>
        <h3 class="rsrtc_more_seat_detail red" style="display:none;">Maximum 4 seat allowed per booking</h3>
        <div class="legend">
            <label><span class="seat available"></span>Available Seat</label>
            <label><span class="seat selected"></span>Selected Seat</label>
            <label><span class="seat booked"></span>Booked Seat</label>
            <label><span class="seat ladies_available"></span>Ladies Seat</label>
        </div>
        <div class="col-md-7 seat_layout">
            <div class="fare_tab_filter show">
                <ul id="fare_tab_filter_id">
                  
                </ul>
            </div>
            <br/>
            <span class="bus_seat_layout">
            </span>
        </div>
        <div class="col-md-4 pull-right seat_detail_info">
            <div class="col-md-12 tprice">
                <div class="SelectedSeats clearfix">
                    <label>Seat(s)  :</label>
                    <span class="sel_seat"></span>
                </div>
                <div class="amount clearfix">
                    <label>Base Fare : </label>
                    <span class="fare_value">Rs. <b class="fare">0</b></span>
                </div>
                <div class="amount clearfix">
                    <label>Service Tax : </label>
                    <span class="fare_value">Rs. <b class="fare_service_tax">NA</b></span>
                </div>
                <div class="amount clearfix">
                    <label>Service Charge : </label>
                    <span class="fare_value">Rs. <b class="fare_service_charge">NA</b></span>
                </div>
                <div class="amount clearfix">
                    <label>Sub Total : </label>
                    <span class="fare_value">Rs. <b class="tot_fare">0</b></span>
                </div>
                <div class="amount clearfix total_fare_discount_div hide">
                    <label>Discount : </label>
                    <span class="fare_value">Rs. - <b class="total_fare_discount_value">0</b></span>
                </div>
				<div class="amount clearfix base_fare_discount_div hide">
                    <label>Discount : </label>
                    <span class="fare_value">Rs. - <b class="base_fare_discount_value">0</b></span>
                </div>
                <div class="amount clearfix">
                    <label>Total Amount : </label>
                    <span class="fare_value">Rs. <b class="total_amount">0</b></span>
                </div>
            </div>
            <div class="col-md-12 boarding_dropping">
                <div class="detail_boarding_alighting_section">
                  <div><b>Pick Up</b></div>
                  <div class="bd_point">
                      <select class="full-width bp_dropdown chosen_select" data-placeholder="Choose a boarding point">
                          
                      </select>
                  </div>
                  <div><b>Drop Off</b></div>
                  <div class="bd_point">
                      <select class="full-width dp_dropdown chosen_select" data-placeholder="Choose a dropping point">
                          
                      </select>
                  </div>
                </div>
                <div>
                    <button class="button btn-medium continue <?php echo THEME_BTN;?>">Continue</button>
                </div>
            </div>
        </div>
    </div>
</div>
  <?php echo form_close(); ?>
</script>


<script type="text/template" id="no_routes_found_tpl">
  <div class="no_routes_found">
      <h6>Ooops!</h6>
      <div class="no_routes_notification">
          <span>No bus found for this route today.</span>
          <p>
              Search Result based on bus Service availability.
              <br>
              For more details mail us on <b>support@rokad.in</b> .
          </p>
      </div>
  </div>
</script>

<script type="text/template" id="return_no_routes_found_tpl">
  <div class="no_routes_found">
      <h6>Ooops!</h6>
      <div class="no_routes_notification">
          <span>Return journey not found. Please <span style="color:#01B7F2;cursor:pointer" class='skip'>Click Here</span> to skip return journey or <span style="color:#01B7F2;cursor:pointer" class='refresh'>Click Here</span> to search again.</span>
          <p>
              Search Result based on bus Service availability.
              <br>
              For more details mail us on <b>support@rokad.in</b> .
          </p>
      </div>
  </div>
</script>


<script type="text/template" id="bus_service_search_waiting">
  <center>
    <div class="search_progress">
      <span>Please wait, searching for buses</span>
      <br>
      <span>from</span>
      <span>
        <b class="pro_from_city theme-blue-color">From</b>
      </span>
      <span> to </span>
      <span>
        <b class="pro_to_city theme-blue-color"> To </b>
      </span> 
      <span> on </span>
      <span>
        <b class="pro_date theme-blue-color"> Date </b>
      </span>
      <br>
      <span>from real-time bus systems</span>
      <br><br>
      <span class="service_search_waiting_loader">
        <img src="">
      </span>
    </div>
  </center>
</script>


<script type="text/template" id="stu_grouping_tpl">
  <div class="box cursor-pointer">
    <div class="details col-sm-12">
        <div class="clearfix">
            <span class="pull-left stu_grouping_plus_minus font30 mt_10 footer-color">
              <i class="fa fa-plus-circle" aria-hidden="true"></i>
            </span>
            <h4 class="box-title pull-left ml10">
                <span class="stu_op_name"></span>
                <small class="total_stu_buses"></small> 
            </h4>
            <span class="pull-right seat_fare flft">
                <span class="search_fare_text price stu_price"></span>
            </span>
        </div>
        <div class="character clearfix">
            <div class="col-xs-3 date tabledis">
                <i class="soap-icon-adventure yellow-color"></i>
                <div>
                    <span class="skin-color">Starts At</span><br>
                    <span class="start_at"></span>
                </div>
            </div>
            <div class="col-xs-3 departure tabledis">
                <i class="soap-icon-clock yellow-color"></i>
                <div>
                    <span class="skin-color">Duration</span><br>
                    <span class="travel_duration_range"></span>
                </div>
            </div>
            <div class="col-xs-3 departure tabledis">
                <i class="soap-icon-passenger yellow-color"></i>
                <div>
                    <span class="skin-color">Available Seats</span><br>
                    <span class="total_stu_seat_available"></span>
                </div>
            </div>
            <div class="col-xs-2 departure tabledis">
                <div class="amenities-n pull-right">
                    <i title="" data-placement="top" data-toggle="tooltip" class="fa fa-mobile circle mtooltip" data-original-title=""></i>
                </div>
            </div>
        </div>
    </div>
  </div>
</script>


<script type="text/template" id="stu_grouping_structure_tpl">
  <div id="stu_bus_summary"></div>
  <div id="stu_bus_services" style="display:none;"></div>
  <div id="private_bus_services"></div>
</script>

<script type="text/template" id="msrtc_boarding_alighting_section">
  <div id="" class="msrtc_boarding_alighting_div clearfix" style="display:none;">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-5">
          <span class="boarding_alighting_span">
            Select Boarding Point
          </span>
          <br>
          <select class="msrtc_boarding_select chosen_select" data-placeholder="Choose a boarding point">
            <option value="">Select Boarding Point</option>
          </select>
          <input type="hidden" name="msrtc_boarding_input" class="msrtc_boarding_input" value=""/>
        </div>
        <div class="col-lg-5">
          <span class="boarding_alighting_span">
            Select Dropping Point
          </span>
          <br>
          <select class="msrtc_alighting_select chosen_select" data-placeholder="Choose a dropping point">
            <option value="">Select Dropping Point</option>
          </select>
          <input type="hidden" name="msrtc_alighting_input" class="msrtc_alighting_input" value=""/>
        </div>
        <div class="col-lg-2">
          <br>
          <a class="button btn-small pull-right selectbusbtn btnorange msrtc_view_seats" href="javascript:void(0)">Select Seat</a>
        </div>
      </div>
    </div>
  </div>
</script>
