<style> 
.tab-pane {min-height: 723px !important;}
.sweet-alert button{padding: 0px 32px;}
</style>
<?php 
$user_activity_log = array(
                        'default' =>  array('icon' => 'fa spinner', 'icon-color' => 'info'),
                        'logged in' =>  array('icon' => 'fa fa-sign-in', 'icon-color' => 'success'), 
                        'logged out' =>  array('icon' => 'fa fa-sign-out', 'icon-color' => 'warning'),
                        'bus ticket booked' =>  array('icon' => 'fa fa-ticket', 'icon-color' => 'info'),
                        'searched bus service' =>  array('icon' => 'fa fa-search', 'icon-color' => 'info'),
                        'forgot password' =>  array('icon' => 'fa fa-user-secret', 'icon-color' => 'info'),
                      );
?>
<link href="<?php echo base_url() . BACK_ASSETS ?>css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script type="text/Javascript" src="<?php echo base_url() . COMMON_ASSETS ?>js/jquery.validate.min.js" ></script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url() . BACK_ASSETS ?>js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/Javascript" src="<?php echo base_url() . COMMON_ASSETS ?>js/jquery.validate.min.js" ></script>
<script type="text/Javascript" src="<?php echo base_url(); ?>js/front/user/profile.js" ></script>
<!-- <script src="//jqueryvalidation.org/files/dist/additional-methods.min.js"></script> -->
<script src="<?php echo base_url() . BACK_ASSETS ?>js/plugins/datepicker/bootstrap-datepicker.js"></script>
<link href="<?php echo base_url() . BACK_ASSETS ?>css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />

<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">My Account</h2>
        </div>
    </div>
</div>
<section id="content" class="gray-area profile-page">
    <div class="container">
        <div id="main">
            <div class="tab-container full-width-style arrow-left dashboard tabwrap">
                <ul class="tabs">
                    <!-- <li class=""><a data-toggle="tab" href="#dashboard"><i class="soap-icon-anchor circle"></i>Dashboard</a></li> -->
                    <li class="active"><a data-toggle="tab" href="#profile"><i class="soap-icon-user circle"></i>Profile</a></li>
                    <li class=""><a data-toggle="tab" href="#booking"><i class="soap-icon-businessbag circle"></i>Booking</a></li>
                    <li class=""><a data-toggle="tab" href="#wallet"><i class="fa fa-krw circle"></i>Wallet</a></li>
                    <li class=""><a data-toggle="tab" href="#wallet1"><i class="fa fa-rupee circle"></i>Transactions</a></li>
					<?php
                        if(strtolower($this->session->userdata("role_name")) != SUPPORT_ROLE_NAME)
                        {
                    ?>
                        <li class=""><a data-toggle="tab" href="#settings"><i class="soap-icon-settings circle"></i>Settings</a></li>
                        
                    <?php
                        }
                    ?>
                </ul>
                <div class="tab-content"><!-- tab content start-->
                    <div id="profile" class="tab-pane fade in active">
                        <div class="view-profile">
                            <div class="col-sm-12">
                                <div class="row form-group">
                                    <div class="col-sms-12 clearfix">
                                        <a href="#" class="button btn-mini pull-right edit-profile-btn <?php echo THEME_BTN;?>">EDIT PROFILE</a>
                                    </div>
                                </div>
                                <hr>
                                <h2>Personal Details</h2>
                                <div class="row form-group">
                                    <div class="col-sms-6 col-sm-6">
                                        <article class="prof_image   style2 box innerstyle personal-details">
                                            <figure class="">
                                               
                                                <img  src="<?php echo $this->session->userdata('profile_image');?>" class="prof_image" title="<?php echo (!$title="") ? $title : '' ?>" style="max-width:300px;max-height:250px;">
                                            </figure>
                                        </article>
                                    </div>
                                    <div class="col-sms-6 col-sm-6">
                                        <div class="row form-group">
                                            <div class="col-sms-12 col-sm-12 clearfix">
                                                <label>FIRST NAME</label>
                                                <div class="display-input-txt"><?php echo ($udata['first_name']!="") ? $udata['first_name'] : '-' ?></div>                                            
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-sms-12 col-sm-12">
                                                <label>LAST NAME</label>
                                                <div class="display-input-txt"><?php echo ($udata['last_name']!="") ? $udata['last_name'] : '-' ?></div>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                             <div class="col-sms-12 col-sm-12">
                                                <label>USERNAME</label>
                                                <div class="display-input-txt">
                                                    <?php echo ($udata['username']!="") ? $udata['username'] : '-' ?>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="row form-group">                                   
                                            <div class="col-sms-12 col-sm-12">
                                                <label>EMAIL ID</label>
                                                <div class="display-input-txt"><?php echo ($udata['email']!="") ? $udata['email'] : '-' ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                

                                <div class="row form-group">                                    
                                    <div class="col-sms-6 col-sm-6">
                                        <label>DATE OF BIRTH</label>
                                        <div class="display-input-txt"><?php echo ($udata['dob'] != "" && $udata['dob'] != "0000-00-00") ? date("d M Y", strtotime($udata['dob'])) : "-"; ?></div>
                                    </div>
                                    <div class="col-sms-6 col-sm-6">
                                        <label>GENDER</label>
                                        <div class="display-input-txt"><?php if($udata['gender'] == "") { echo '-';}else if($udata['gender']=="M"){ echo 'Male';} else if($udata['gender']=='F') { echo 'Female';} ?></div>
                                    </div>
                                </div>
                                <hr>
                                <h2>Contact Details</h2>

                                <div class="row form-group">                                    
                                    <div class="col-sms-6 col-sm-6">
                                        <label>ADDRESS1</label>
                                        <div class="display-input-txt"><?php echo ($udata['address1'] != "") ? $udata['address1'] : '-' ?></div>
                                        
                                    </div>
                                    <div class="col-sms-6 col-sm-6">
                                        <label>ADDRESS2</label>
                                        <div class="display-input-txt"><?php echo ($udata['address2'] != "") ? $udata['address2'] : '-' ?></div>
                                    </div>
                                </div>
                                <div class="row form-group">                                    
                                    <div class="col-sms-6 col-sm-6">
                                        <label>COUNTRY</label>
                                        <div class="display-input-txt"><?php echo ($udata['countrynm'] != "") ? $udata['countrynm'] : '-' ?></div>
                                        
                                    </div>
                                    <div class="col-sms-6 col-sm-6">
                                        <label>STATE</label>
                                        <div class="display-input-txt"><?php echo ($udata['statenm'] != "") ? $udata['statenm'] : '-' ?></div>
                                    </div>
                                </div>
                                <div class="row form-group">                                    
                                    <div class="col-sms-6 col-sm-6">
                                        <label>CITY</label>
                                        <div class="display-input-txt"><?php echo ($udata['citynm'] != "") ? $udata['citynm'] : '-' ?></div>
                                        
                                    </div>
                                    <div class="col-sms-6 col-sm-6">
                                        <label>PINCODE</label>
                                        <div class="display-input-txt"><?php echo ($udata['pincode'] != "") ? $udata['pincode'] : '-' ?></div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sms-6 col-sm-6">
                                        <label>PHONE NUMBER</label>
                                        <div class="display-input-txt"><?php echo ($udata['mobile_no']!="") ? $udata['mobile_no'] : '-' ?></div>
                                    </div>
                                </div>

                                <div class="clearfix"> </div>
                                
                            </div>
                            <div class="clearfix"></div>
                            <hr>
                        </div>
                        <div class="edit-profile">                            
							<?php
							$attributes = array("method" => "POST", "name" => "edit-profile-form", "id" => "edit-profile-form", "class" => "edit-profile-form");
							echo form_open_multipart(base_url().'admin/login/login_authenticate', $attributes);
							?>							
                                <h2>Personal Details</h2>
                                <div class="col-sm-9 no-padding no-float">
                                    <div class="row form-group">
                                        <div class="col-sms-6 col-sm-6">
                                            <label>Username</label>
                                            <span class="input-text full-width alpha"><?php echo $udata['username']; ?></span>
                                        </div>
                                      <div class="col-sms-6 col-sm-6">
                                        <label>Email Address</label>
                                        <span class="input-text full-width alpha"><?php echo $udata['email']; ?></span>
                                      </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sms-6 col-sm-6">
                                            <label>First Name</label>
                                            <input type="text" class="input-text full-width alpha-only" value="<?php echo ucfirst($udata['first_name']); ?>" name="fnm"  id="psgr_name" placeholder="">
                                        <span class="errfir error"></span>
                                        </div>
                                      <div class="col-sms-6 col-sm-6">
                                        <label>Last Name</label>
                                        <input type="text" class="input-text full-width alpha-only" value="<?php echo ucfirst($udata['last_name']); ?>"   name="lnm"  id="lnm" placeholder="">
                                        <span class="errlast error"></span>
                                      </div>
                                    </div>
                                    <div class="row form-group">
                                        <!--                                        
                                        <div class="col-sms-6 col-sm-6">
                                        <label>Email Address</label>
                                        <input type="text" class="input-text full-width" placeholder="">
                                        </div>
                                        <div class="col-sms-6 col-sm-6">
                                        <label>Verify Email Address</label>
                                        <input type="text" class="input-text full-width" placeholder="">
                                        </div>-->
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sms-6 col-sm-6">
                                            <label>Country Code</label>
                                            <div class="selector">
                                                <select class="full-width">
                                                    <option>India(+91)</option>
                                                    
                                                </select>
                                            
                                            </div>
                                        </div>
                                        <div class="col-sms-6 col-sm-6">
                                            <label>Phone Number</label>
                                            <input type="text" class="input-text full-width" value="<?php echo $udata['mobile_no']; ?>" name="mob" maxlength="10" id="mob" placeholder="" disabled="true">
                                             <span class="errphno error"></span>
                                        </div>
                                    </div>
                                    <div class="row form-group ">
                                        <div class="col-sm-6">
                                       <label>Date Of Birth</label>
                                            <?php
                                                $user_dob = (($udata['dob'] != '' && $udata['dob']!='0000-00-00')) ? date("d-m-Y", strtotime($udata['dob'])) : "";
                                            ?>
                                            <div class="">
                                                <input type="text" class="input-text full-width datepicker"  value="<?php echo $user_dob; ?>" placeholder="Date Of Birth" name="dob" id="dob"/>
                                                <span class="errdob error"></span>
                                            </div>
                                       
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Gender</label>
                                            <label class="radio radio-inline">
                                                <input type="radio" name="gen" value="M" <?php echo ($udata['gender'] == 'M') ? 'checked' : '' ?>/>
                                                Male
                                            </label>
                                            <label class="radio radio-inline">
                                                <input type="radio" name="gen" value="F" <?php echo ($udata['gender'] == 'F') ? 'checked' : '' ?> />
                                                Female
                                            </label>
                                            <span class="errgen error ml10"></span>
                                        </div>
                                      
                                    </div>
                                    <hr>
                                    <h2>Contact Details</h2>
                                    <div class="row form-group">
                                        <div class="col-sms-6 col-sm-6">
                                            <label>Address1</label>
                                            <input type="text" value="<?php echo $udata['address1']; ?>" class="input-text full-width add" name="add1">
                                              <!--<span class="erradd1 error"></span>-->
                                        </div>
                                        <div class="col-sms-6 col-sm-6">
                                            <label>Address2</label>
                                            <input type="text"  value="<?php echo $udata['address2']; ?>" class="input-text full-width" name="add2">
                                            <!-- <span class="erradd2 error"></span> -->
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sms-6 col-sm-6">
                                            <label>Country</label>
                                            <div class="">
                                                 <select class="full-width chosen_select" data-placeholder="Choose a country" name="country" id="country">
                                                    <option value=""></option>
                                                    <?php foreach($country as $key=>$val):?>
                                                   <option value="<?php echo $val['intCountryId']; ?>" <?php echo ($val['intCountryId'] == $udata['country'])?"selected":"" ?>><?php echo $val['stCountryName']; ?></option>
                                               <?php endforeach; ?>
                                                </select>
                                            </div>
                                              <span class="coun_err error"></span>
                                        </div>
                                        <div class="col-sms-6 col-sm-6">
                                            <label>State</label>
                                            <div class="">
                                                <select class="full-width chosen_select" data-placeholder="Choose a state" name="state" id="state">
                                                     <?php foreach($states as $key=>$val):  ?>
                                                    <option value="<?php echo $val['intStateId']; ?>" <?php echo ($val['intStateId'] == $udata['state'])?"selected":"" ?>><?php  echo $val['stState']; ?></option>
                                                <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <span class="state_err error"></span>
                                        </div>
                                        </div>
                                    <div class="row form-group">
                                        <div class="col-sms-6 col-sm-6">
                                            <label>City</label>
                                            <div class="">
                                                <select class="full-width chosen_select" data-placeholder="Choose a city" name="city" id="city">
                                                     <?php foreach($city as $key=>$val):  ?>
                                                    <option value="<?php echo $val['intCityId']; ?>" <?php echo ($val['intCityId'] == $udata['city'])?"selected":"" ?> ><?php  echo $val['stCity']; ?></option>
                                                <?php endforeach; ?>
                                                </select>
                                               
                                            </div>
                                            <span  class="city_err error"></span>
                                        </div>
                                       
                                            <div class="col-sms-6 col-sm-6">
                                            <label>Pincode</label>
                                           
                                             <input type="text" value="<?php echo $udata['pincode']; ?>" class="input-text full-width" name="pcode" id="pcode" maxlength="6">    
                                             <span class="pcode_err error"></span>
                                        </div>
                                    </div>
                                    <hr>
                                    <h2>Upload Profile Photo</h2>
                                    <div class="row form-group">
                                        <div class="col-sms-12 col-sm-6 no-float">
                                             <div class="fileinput full-width">
                                           <input type="file" class="input-text" id="userfile" name="userfile" accept="image/*">
                                           </div>
                                        </div>
                                          <span class="file_err error"></span>
                                    </div>
                                    <hr>
                                    <div class="from-group">
                                        <button type="button" class="btn btn-medium col-sms-6 col-sm-4 prof_upd mr5 <?php echo THEME_BTN;?>">UPDATE PROFILE</button> 
                                        <button type="button" id="cancle-profile-btn" class="btn btn-medium col-sms-6 col-sm-4 cancle-profile-btn <?php echo THEME_BTN;?>">GO BACK</button>
                                    </div>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                    <div id="booking" class="tab-pane fade">
                        <h2>Trips You have Booked!</h2>
                        <div class="filter-section gray-area clearfix">
                            
                        </div>
                        <div class="booking-history">
                           
                        </div>
                    </div>
                    
                    <?php
                    if(strtolower($this->session->userdata("role_name")) != SUPPORT_ROLE_NAME)
                    {
                    ?>
                        
                        <div id="wallet" class="tab-pane fade">
                            <div class="row">
                                <div class= "col-sm-5">
                                    <h2><i class="fa fa-krw circle"></i> Wallet</h2>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h2>Total amount in your wallet 
                                                <?php 
                                                    if($wallet_flag)
                                                    { 
                                                        echo to_currency(0); 
                                                    }
                                                    else 
                                                    { 
                                                       echo to_currency($wallet_agent_balance_show);
                                                    }
                                                ?> 
                                            </h2>
                                        </div>
                                        <div class="col-sm-12">
                                            <button type="button" class="btn btn-info <?php echo THEME_BTN;?>" data-toggle="modal" href="#myModal">Request Recharge</button>
                                            &nbsp;
                                            <?php if ($this->session->userdata('role_id') != SUBAGENTS_ROLE_ID) {?>
                                            <button type="button" class="btn btn-info <?php echo THEME_BTN;?>" data-toggle="modal" data-target="#myModal1"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;Add Money</button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                <!--<div class= "col-sm-12">
                                            <?php 
                                                $image_path =  base_url().FRONT_IMAGES."logo/ICICI_Bank_Logo.png";
                                            ?>
                                            <p><img src="<?php echo $image_path;?>" title="Bank Logo" style="width:150px;"><b></b></p>
                                            <p><b>A/c Name :</b>  BOOKONSPOT TRAVEL SOLUTION PRIVATE LIMITED</p>
                                            <p><b>A/c No :</b>  104505002070</p>
                                            <p><b>Bank Name :</b> ICICI BANK</p>
                                            <p><b>IFSC Code :</b>   ICIC0001045</p>
                                            <p><b>Branch & City :</b> Vikroli (West)</p>         
                                            <hr>
                                    </div>
                                    <hr>
                                    <div class= "col-sm-12">
                                            <?php 
                                                $image_path =  base_url().FRONT_IMAGES."logo/sbi-bank.png";
                                            ?>
                                            <p><img src="<?php echo $image_path;?>" title="Bank Logo" style="width:150px;"><b></b></p>
                                            <p><b>A/c Name :</b>  BOOKONSPOT TRAVEL SOLUTION PRIVATE LIMITED</p>
                                            <p><b>A/c No :</b>  36724934476</p>
                                            <p><b>Bank Name :</b> STATE BANK OF  INDIA</p>
                                            <p><b>IFSC Code :</b>   SBIN0011710</p>
                                            <p><b>Branch & City :</b> Vidyavihar (West)</p>         
                                        
                                    </div>-->
                                </div>
                            </div>
                            <hr/>
                            <br>
                            <?php if($wallet && $balance_request ) :?>
                    
                                <div class="row">
                                    <div class="col-sm-12 table-responsive">
                                        <h3>Payment  Details </h3>
                                        <table class="table table-bordered table-hover table-responsive" id="wallet_table">
                                            <thead>
                                                <tr>
                                                    <th>Sr.no</th>
                                                    <th>Topup By</th>
                                                    <th>Amount</th>
                                                    <th>Bank_name</th>
                                                    <th>Bank_account_no</th>
                                                    <th>Pg_tracking_id </th>
                                                    <th>Transaction_remark</th>
                                                    <th>Transaction_date</th>
                                                    <th>Request_date</th>
                                                    <th>Response_date</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                    
                                                      
                                                    $c = $this->uri->segment(4);
                                                      foreach ($balance_request as $key => $value) {            
        												     $c++;
                                                ?>
                                                    
                                                <tr>
                                                    <td><?php echo $c; ?>.</td>
                                                    <td><?php echo $value['topup_by']; ?></td>
                                                    <td><?php echo to_currency ($value['amount']); ?></td>
                                                    <td><?php echo $value['bank_name']; ?></td>
                                                    <td><?php echo $value['bank_acc_no']; ?></td>
                                                    <td><?php echo $value['pg_tracking_id'] ?></td>
                                                    <td><?php echo $value['transaction_remark']; ?></td>
                                                    <td><?php echo date("d-m-Y", strtotime($value['transaction_date'])); ?></td>
                                                    <td><?php echo date("d-m-Y H:i:s", strtotime($value['created_date'])); ?></td>
                                                    <!-- <td><?php echo date("d-m-Y", strtotime($value['updated_date'])); ?></td> -->
                                                    <td><?php echo $value['updated_date']; ?></td>
                                                    <td><?php echo $value['is_approvel']; ?></td>
                                                </tr>
                                                <?php  
                                         
                                                 } ?>
                                            </tbody>  
                                        
										</table>
                                        <div class="col-md-12">
                                            <div class="pull-right">
                                                <ul class="pagination">
                                                    <p> <?php echo $links ?> </p>
                                                </ul>                                              
                                            </div>
                                        </div>                                          
                                    </div> 
								</div>
                            <?php else: ?>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h2>
                                            No Payment Request done
                                        </h2>
                                    </div>
                                </div>
                            <!--</form>-->
                            <?php endif;?>
                            
                            <div class="modal fade" id="myModal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close dclose" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Balance Request</h4>
                                        </div>    									
										<?php
										$attributes = array("method" => "POST", "name" => "modal_form", "id" => "modal_form");
										echo form_open('', $attributes);
										?>
        									<div class="modal-body">
        											<div class="form-group row">
        												<div class="col-md-6">
        													<label>Request To * </label>
        													<input type="radio" name="request_to" id="" class="" value="<?php echo $CompanyOwner->id;?>" />&nbsp;&nbsp;<?php echo ucwords($CompanyOwner->first_name.' '.$CompanyOwner->last_name);?>&nbsp;&nbsp;
        													<input type="radio" name="request_to" id="" class="" value="<?php echo $AboveLevel->id;?>" />&nbsp;&nbsp;<?php echo ucwords($AboveLevel->first_name.' '.$AboveLevel->last_name);?>
        													<span id="reqstmsg" class="err"></span>
        												</div>
        												
        											</div>
        											
        											<div class="form-group row">
        											<div class="col-md-6">
        												<label>Transaction Type *</label>
        												<select class ="form-control" id="trans_type" name="trans_type" >
        													<option value = ''>Please Select</option>
        													<?php foreach($Pay_Trans_Mode as $value){?>
        													<option value="<?php echo $value->mode;?>"><?php echo  $value->mode;?></option>
        													<?php } ?>
        												</select>
        											</div>     
        											<div class="col-md-6">
        												<label>Enter Amount *</label>
        												<input class="form-control" id="enter_amount" name="enter_amount" placeholder="Enter Amount" maxlength="10" type="text">
        											</div>
        											</div>
        									
        											<div class="form-group row">
        											<div class="col-md-6 fields">
        												<label>Enter Bank Name *</label>
        												<input class="form-control" id="bank_name" name="bank_name" placeholder="Enter Bank Name" type="text">
        											</div>     
        											<div class="col-md-6 fields">
        												 <label>Enter Account Number *</label>
        												 <input class="form-control" id="account_number" name="account_number" placeholder="Enter Account Number" maxlength="20" type="text">
        											</div>
        											</div>
        											
        											<div class="form-group row">
        											<div class="col-md-6 fields">
        												<label>Transaction Reference No. *</label>
        												<input class="form-control" id="tran_ref_no" name="tran_ref_no" placeholder="Transaction Reference No." type="text">
        											 </div>
        											<div class="col-md-6">
        												<label>Remarks</label>
        												<input class="form-control full-width-style" id="remarks" name="remarks" placeholder="Enter Remarks here..." type="text area">
        											 </div>
        											</div>
        											<!--
        											<div class="form-group row">
        											<label>For Direct&nbsp;Topup, Consider Below Bank Details.</label>
        											</div>
        											
        											<div class="row table-bordered">
        												<div class="col-md-6" style="padding:10px;">
        													<?php 
        														$image_path =  base_url().FRONT_IMAGES."logo/ICICI_Bank_Logo.png";
        													?>
        													<p><img src="<?php echo $image_path;?>" title="Bank Logo" style="width:150px;"><b></b></p>                 
        													<span><b>A/c Name :</b>  BOOKONSPOT TRAVEL SOLUTION PRIVATE LIMITED</span><br>
        													<span><b>A/c No :</b>  104505002070</span><br>
        													<span><b>Bank Name :</b> ICICI BANK</span><br>
        													<span><b>IFSC Code :</b>   ICIC0001045</span><br>
        													<span><b>Branch & City :</b>Vikroli (West)</span>         
        												</div>
        												<div class="col-md-6" style="padding:10px;">
        													<?php 
        														$image_path =  base_url().FRONT_IMAGES."logo/sbi-bank.png";
        													?>
        													<p><img src="<?php echo $image_path;?>" title="Bank Logo" style="width:150px;"><b></b></p>                 
        													<span><b>A/c Name :</b>  BOOKONSPOT TRAVEL SOLUTION PRIVATE LIMITED</span><br>
        													<span><b>A/c No :</b>  36724934476</span><br>
        													<span><b>Bank Name :</b> STATE BANK OF INDIA</span><br>
        													<span><b>IFSC Code :</b>   SBIN0011710</span><br>
        													<span><b>Branch & City :</b>Vidyavihar(West)</span>         
        												</div>
        											</div>-->
        										
        									</div>
        									<div class="modal-footer">
                                                                                        <button type="submit" class="btn btn-default btn-primary" name="success" id="success">Submit</button>
        										<button type="button" class="btn btn-default btn-danger dclose" name="cancel" id="cancel" data-dismiss="modal">Close &times;</button>
        									</div>
    									<?php echo form_close(); ?>
									</div>
								</div>
							</div> 
							  

                            <div class="modal fade popup" id="myModal1" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title"><i class="fa fa-inr"></i>&nbsp;Add Amount To Wallet</h4>
                                        </div>
                                        <?php
                                            $attributes = array("method" => "POST", "id" => "pg_form", "class" => "login-form");
                                            echo form_open(base_url().'admin/users/add_money', $attributes);
                                        ?>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">Amount</span>
                                                        <input name="w_id" type="hidden" id="w_id" class="form-control" placeholder="id" />
                                                        <input name="pg_amt" type="text" maxlength="10" id="pg_amt" class="form-control" placeholder="Enter Amount to be added in Wallet" required/>
                                                    </div>
                                                    <div class="clearfix" style="height: 10px;clear: both;"></div>
                                                    <span id="erro_msg" class="error"><span>
                                                </div>
                                                <div class="modal-footer clearfix">
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                                                    <button type="submit" id="pg_confirm" class="btn btn-primary pull-left" name="pg_confirm"> Add amount To wallet</button>
                                                </div>
                                            </div>
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                        <div id="wallet1" class="tab-pane fade">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h2>Wallet Transaction Details!</h2>
                                    <div><hr></div>
                                </div>
                            </div>
                            <?php if($wallet && $wallet_detail ) :?>
                    
                                <div class="row table-responsive">
                                    <div class="">
                                        <table class="table table-bordered table-hover" id="wallet_table">
                                            <thead>
                                                <tr>
                                                    <th>Sr no.</th>
                                                    <th>Wallet Opening Balance</th>
                                                    <th>Type</th>
                                                    <th>Txn type</th>
                                                    <th>Txn Amount</th>
                                                    <th>Txn Date</th>
                                                    <th>Rokad Ref No </th>
                                                    <th>Wallet Closing Balance</th>
                                                     <th>Txn Done By</th>
                                                     <th>Transaction Remarks</th>
                                                     <th>Wallet Transaction ID</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                    $c = 1;
                                                    foreach ($wallet_detail as $key => $value) 
                                                    {
                                                ?>
                                                <tr>
                                                    <td><?php echo $c; ?>.</td>
                                                    <td><?php echo to_currency ($value['amt_before_trans']); ?></td>
                                                    <td><?php echo $value ['transaction_type']; ?></td>
                                                    <td><?php echo $value['title']; ?></td>
                                                    <td><?php echo to_currency ($value['amt']); ?></td>
                                                    <td><?php echo $value['added_date']; ?></td>
                                                    <td><?php echo $value['boss_ref_no']; ?></td>
                                                    <td><?php echo to_currency ($value['amt_after_trans']); ?></td>
                                                    <td><?php echo $value['done_by']; ?></td>
                                                    <td><?php echo $value['comment']; ?></td>
                                                    <td><?php echo $value['id']; ?></td>
                                                </tr>
                                                <?php $c++;  } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div> 
                            <?php else: ?>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h3>
                                            No transaction done
                                        </h3>
                                    </div>
                                </div>
                            <!--</form>-->
                            <?php endif;?>
                        </div>

                        <div id="settings" class="tab-pane fade">
                            <h2>Account Settings</h2>
                            <h5 class="skin-color">Change Your Password</h5>                             
                                <?php
								$attributes = array("method" => "POST", "name" => "change_password_inp", "id" => "change_password_inp");
								echo form_open('', $attributes);
								?>								
									<div class="row form-group">
										<div class="col-xs-12 col-sm-6 col-md-4">
											<label>Old Password</label>                                       
											<input name="old_password" type="password" id="old_password" autocomplete="off" class="input-text full-width">
										</div>									   
									</div>
									<div class="row form-group">
										<div class="col-xs-12 col-sm-6 col-md-4">
											<label>Enter New Password</label>                                        
											<input name="new_password" type="password" id="new_password" maxlength="15" autocomplete="off" class="input-text full-width">
										</div>										 
									</div>                               
									<div class="row form-group">
										<div class="col-xs-12 col-sm-6 col-md-4">
											<label>Confirm New password</label>                                        
											<input name="cnf_password" type="password" id="cnf_password" maxlength="15" autocomplete="off" class="input-text full-width">
										</div>
									</div>
									<div class="row form-group">								
										<button class="btn-medium <?php echo THEME_BTN;?>" id="change_password_submit" name="change_password_submit" type="button" >Submit</button>
									</div>
								<?php echo form_close(); ?>
								
								<?php
								$attributes = array("method" => "POST", "name" => "change_password", "id" => "change_password");
								echo form_open(base_url().'admin/users/change_password', $attributes);
								?>								
									<input type="hidden" name="old_password_hidden" id="old_password_hidden" >
									<input type="hidden" name="new_password_hidden" id="new_password_hidden" >
									<input type="hidden" name="cnf_password_hidden" id="cnf_password_hidden" >									
								<?php echo form_close(); ?>
                            <hr>
							
							<h5 class="skin-color">Change Your T-PIN</h5>
								<?php
								$attributes = array("method" => "POST", "name" => "change_tpin_inp", "id" => "change_tpin_inp");
								echo form_open('', $attributes);
								?>								
									<div class="row form-group">
										<div class="col-xs-12 col-sm-6 col-md-4">
											<label>Old T-PIN</label>                                        
											<input name="old_tpin" type="password" maxlength="4" id="old_tpin" class="input-text full-width" autocomplete="off">
										</div>                                   
									</div>
									<div class="row form-group">
										<div class="col-xs-12 col-sm-6 col-md-4">
											<label>New T-PIN</label>                                        
											<input name="new_tpin" type="password" maxlength="4" id="new_tpin" class="input-text full-width" autocomplete="off">
										</div>                                     
									</div>								   
									<div class="row form-group">
										<div class="col-xs-12 col-sm-6 col-md-4">
											<label>Confirm T-PIN</label>                                        
											<input name="cnf_tpin" type="password" maxlength="4" id="cnf_tpin" class="input-text full-width" autocomplete="off">
										</div>
									</div>
									<div class="form-group">                                    
										<button class="btn-medium <?php echo THEME_BTN;?>" id="change_tpin_submit" name="change_tpin_submit" type="button" >Submit</button>
									</div>
								<?php echo form_close(); ?>
								<?php
								$attributes = array("method" => "POST", "name" => "change_tpin", "id" => "change_tpin");
								echo form_open(base_url().'admin/users/change_tpin', $attributes);
								?>								
									<input type="hidden" name="old_tpin_hidden" id="old_tpin_hidden" >
									<input type="hidden" name="new_tpin_hidden" id="new_tpin_hidden" >
									<input type="hidden" name="cnf_tpin_hidden" id="cnf_tpin_hidden" >									
								<?php echo form_close(); ?>
							
								<?php
								$attributes = array("method" => "POST", "name" => "tpin_otp_inp", "id" => "tpin_otp_inp", "style" =>"display:none");
								echo form_open('', $attributes);
								?>								
									<div class="row form-group">
										<div class="col-xs-12 col-sm-6 col-md-4">
											<label>OTP</label>                                        
											<input name="new_otp" type="text" maxlength="6" id="new_otp" class="input-text full-width"><br>
											<button class="btn-small <?php echo THEME_BTN;?>  resend_otp" type="button" >Resend OTP</button>                               
										</div>                                   
									</div>                                
								   <div class="form-group">									
										<button class="btn-medium <?php echo THEME_BTN;?> submit_btn" id="tpin_otp_submit" name="tpin_otp_submit" type="button" >Submit</button>
										<button class="btn-medium <?php echo THEME_BTN;?> back_btn" name="back_btn" type="button" >Back</button>
									</div>
								<?php echo form_close(); ?>
								<?php
								$attributes = array("method" => "POST", "name" => "tpin_otp", "id" => "tpin_otp");
								echo form_open(base_url().'admin/users/validate_tpin_otp', $attributes);
								?>								
									<input type="hidden" name="new_otp_hidden" id="new_otp_hidden" >									
								<?php echo form_close(); ?>
                            <hr>							
							
							<h5 class="skin-color">Change Your Email</h5>								
								<?php
								$attributes = array("method" => "POST", "name" => "change_email_inp", "id" => "change_email_inp");
								echo form_open('', $attributes);
								?>
									<div class="row form-group">
										<div class="col-xs-12 col-sm-6 col-md-4">
											<label>Primary email</label>                                        
											<input name="inp_email" type="email" id="inp_email" class="input-text full-width">
										</div>									   
									</div>
									<div class="form-group">                                    
										<button class="btn-medium <?php echo THEME_BTN;?>" id="change_email_submit" name="change_email_submit" type="button" >Submit</button>
									</div>
								<?php echo form_close(); ?>
								<?php
								$attributes = array("method" => "POST", "name" => "change_email", "id" => "change_email");
								echo form_open(base_url().'admin/users/change_email', $attributes);
								?>								
									<input type="hidden" name="inp_email_hidden" id="inp_email_hidden" >									
								<?php echo form_close(); ?>
                            <hr>

                        </div>
                    <?php } ?>
                </div><!-- tab content end-->
            </div>
        </div>
    </div> <!-- -->   
    <div class="modal fade" id="fav_model"  name="fav_model" style="display:none">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <span>Are you sure , you want to delete this record ?</span>
                    <input type="hidden" id="fav_div_id" name="fav_div_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" value="false">Close</button>
                    <button type="button" class="btn btn-primary"  id="confirm_del" name="confirm_del">Confirm</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</section>

<script>
var back = '<?php echo $back; ?>';
</script> 

<script src="<?php echo base_url() ?>js/back/profile/settings.js" type="text/javascript"></script>
<script type="text/javascript">

    $(document).ready(function() {
		
	$('#myModal').on('hidden.bs.modal', function (e) {
	  $('#myModal form :input').val("");
	})
	
    var date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    $("#from_date").val($.datepicker.formatDate("dd-mm-yy", firstDay));
    $("#till_date").val($.datepicker.formatDate("dd-mm-yy", date));
    $("#created_date").val($.datepicker.formatDate("dd-mm-yy", date));
    
    $(".view-profile").show();
      
    $("#profile .edit-profile-btn").click(function(e) {
        e.preventDefault();
        $(".view-profile").fadeOut();
        $(".edit-profile").fadeIn();
    });

 
    $("#profile .cancle-profile-btn").click(function(e) {
        e.preventDefault();
        $(".view-profile").fadeIn();
        $(".edit-profile").fadeOut(); 
    });

    setTimeout(function() {
        $(".notification-area").append('<div class="info-box block"><span class="close"></span><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus ab quis a dolorem, placeat eos doloribus esse repellendus quasi libero illum dolore. Esse minima voluptas magni impedit, iusto, obcaecati dignissimos.</p></div>');
    }, 10000);

    /* function for booking history  and cancel ticket */
    
    $(document).off('click', '.ticket_info').on('click', '.ticket_info', function(e) {
         $(this).parent().find('.passenger_info').slideToggle('medium');
     }); 
     //////@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@??/////////
     //declaring variables
//var base_url = '<?php echo base_url(); ?>';
//var offset = 1;//customize this as your need
//var request_ajax = true;
//var ajax_is_on = false;
//var session_user = '<?php echo $_SESSION['user_name']; ?>';//customize this as your need
//var objHeight=$(window).height()-50;//customize this as your need
//var user_name = '<?php echo $this->uri->segment('4'); ?>';//customize this as your need
//var last_scroll_top = 0;
//
//$(window).scroll(function(event) {
//
//    var st = $(this).scrollTop();
//
//    if(st > last_scroll_top){
//
//        if ($(window).scrollTop() + 100 > $(document).height() - $(window).height()) {
//
//            var user_posts = '';
//
//            if (request_ajax === true && ajax_is_on === false) {
//
//                ajax_is_on = true;
//                $("#wallet_table").removeClass('hideGif').addClass('displayGif');
//
//                $.ajax({
//
//                    url: BASE_URL+"admin/users/wallet_trans_detail",
//                    data: {page_number: offset, user_name: user_name},
//                    type: 'post',
//                    async: false,
//                    dataType: 'json',
//                    success: function(data) {
//console.log(data);
//                        $("#wallet_table").removeClass('displayGif').addClass('hideGif');
//
//                        if (data != '0') {
//
//                            for (var x = 0; x < data.length; x++) {
//console.log(x);
//                                user_posts +=
//                                  '<div class="row-fluid">' +
//                                  '<div class="span7" style="">' ;
//
//                                if (data[x].status != '1') {
//
//                                    user_posts += 
//                                      '<div class="label" style="">Unpublished</div>';
//                                } else {
//
//                                    user_posts += '';
//                                }
//
//                                user_posts += HTMLdesignWITHjqueryVARIABLES;
//                            }
//
//                            $('#wallet_table').append(user_posts);
//
//                            offset += 1;
//
//                        } else {
//
//                            request_ajax = false;
//                            $("#message").addClass('alert alert-success');
//                            $("#pagination_message").html('No More Posts');
//                        }
//
//                        ajax_is_on = false;
//                    }  
//                });
//            }   
//        }  
//    }  
//    last_scroll_top = st;
//});
     //////////@@@@@@@@@@@@@@@@@???????//////////////

    /* function to cancel ticket */    
    $(document).off('click','.cancel_ticket_btn').on('click','.cancel_ticket_btn', function(){
    
        var elem        = $(this);
        var wrapper     = $(this).parents('.booking-info');

        var flag = confirm("Are you sure you want to cancel this ticket?");
        
        if(flag)
        {
            $(this).addClass('bg_disabled').removeClass('cancel_ticket_btn').html('Please Wait');

            var detail      = {};
            var form        = '';
            var div         = '';
            var ajax_url    = 'cancel_tickets';
            var formElem    = $(this).parents('form.cancel_booking_form');
            var seat        = [];

            formElem.find('.seat_no:checked').each(function(i, chekbox){
                seat.push($(this).val());
            });

            /*detail['seat_no']       = seat;
            detail['ticket_id']     = $(this).attr('data-ticket-id'); //to comment
            detail['pnr_no']        = $(this).attr('data-pnr');//to comment
            detail['op_name']       = $(this).attr('data-opname');*/
            detail['seat_no']       = seat;
            detail['email']         = $(this).attr('data-ei');
            detail['pnr_no']        = $(this).attr('data-pn');
            detail['pro_id']        = $(this).attr('data-pro');
            detail['ticket_id']     = $(this).attr('data-t');
            detail['pc']            = $(this).attr('data-pc');

            detail['proty']         = $(this).attr('data-pro-ty');
            detail['opi']           = $(this).attr('data-opi');

            // console.log(detail);
            if(detail['pro_id'] != "" && detail['pro_id'] != undefined)
            {
                get_data(ajax_url, form, div, detail, function(response)
                {
                    if (response.flag == '@#success#@')
                    {
                        var info_block  = $.parseHTML($('#info-tpl').html());

                        var ul = $('<ul/>').addClass('arrow');
                        ul.append($('<li/>').html('Refund amount Rs.'+ response.refund_amt));
                        ul.append($('<li/>').html('Cancellation cahrges Rs.'+ response.cancellation_cahrge));

                        $(info_block).find('.info-container').append(ul);

                        wrapper.addClass('cancelled').find('.ticket_info .status').html('CANCELLED');

                        elem.parent().removeClass('text-right').addClass('text-left').html($(info_block));
                    }
                    else
                    {
                        alert(response.error);
                    }
               });
            }
            else
            {
                showNotification("danger","Some problem occured. Please refresh page.");
            }
        } // end of if
    });
  
        
        //to disable cut copy paste in field....
         $('#enter_amount').bind('paste',function(e) { 
            e.preventDefault(); //disable cut,copy,paste
            alert('Paste option is not allowed here !!');
            });
            
            $(document).off('keypress', '#enter_amount').on('keypress', '#enter_amount', function(e) 
        {
           var regex = new RegExp("^[0-9.\b]+$");//accept only _
           var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
             key=e.charCode? e.charCode : e.keyCode;
            if(key==9)
            {
                return true; 
            }
            if (regex.test(str)) 
            {
                return true;
            }
            e.preventDefault();
            return false;
        });
        $(document).off('keypress', '#account_number').on('keypress', '#account_number', function(e) 
        {
           var regex = new RegExp("^[0-9\b]+$");//accept only _
           var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
             key=e.charCode? e.charCode : e.keyCode;
            if(key==9)
            {
                return true; 
            }
            if (regex.test(str)) 
            {
                return true;
            }
            e.preventDefault();
            return false;
        });
        
        $( "#created_date" ).datepicker({
            'dateFormat' : 'dd-mm-yy',
            maxDate:0,
         });
        
        var enforceModalFocusFn = $.fn.modal.Constructor.prototype.enforceFocus;

        $.fn.modal.Constructor.prototype.enforceFocus = function() {};
        
		$('#trans_type').change(function(){
			var TransMode = $('#trans_type').val();
			if(TransMode == 'CASH'){
				$('.fields').hide();
			}else{
				$('.fields').show();
			}
		});
  
        $('#modal_form').validate({

                ignore: "",
                rules: {
					request_to: {
                        required: true
                    },
                    enter_amount: {
                        required: true,
					check_numbers:true
                    },
                    trans_type:{
                        required:true
                    },
                    bank_name: {
                         required: function(element) {
                                    return $("#trans_type").val() !== 'CASH';
                                    }
                                },
                    account_number: {
                        required: function(element) {
                                    return $("#trans_type").val() !== 'CASH';
                                    }
                    },
                    tran_ref_no: {
                        required: function(element) {
                                    return $("#trans_type").val() !== 'CASH';
                                    }
                                }

                },
				errorPlacement: function(error, element) {
					if (element.attr("name") === "request_to") {
						error.appendTo("#reqstmsg");
					}

					else {
						error.insertAfter(element);
					}
				},
                messages: {
					request_to:{
                        required:"Please select the Request To"
                    },
                    enter_amount: {
                        required: "Please enter the amount"
                    },
                    trans_type:{
                        required:"Please select the transaction type"
                    },
                    bank_name: {
                         required: "Please enter the bank name"
                    },
                    account_number: {
                         required: "Please enter the account number"
                    },
                    tran_ref_no: {
                         required: "Please enter the transaction refrence number"
                    },
                   
                },
                //success: function(element) {
                submitHandler: function (form) {

                    postreq();
                }
        
        });
		
		$.validator.addMethod("check_numbers", function (value, element) {
		var flag=false;
		var TempAmount = parseInt(value);
		if(TempAmount <=0){
		flag = false;	
		}else{
		flag = true;	
		}
		return flag;
	}, 'Amount Should Be Greater Than 0.');
        
        function postreq() {
            $('#success').attr('disabled', true).html('Processing...');
            var detail      = {};
            var div         = "";
            var ajax_url    = "admin/users/wallet_trans_topup";
            detail['request_to'] = $("input[name='request_to']:checked"). val();
            detail['trans_type'] = $('#trans_type').val();
            detail['enter_amount'] = $('#enter_amount').val();
            detail['bank_name'] = $('#bank_name').val();
            detail['account_number'] = $('#account_number').val();
            detail['tran_ref_no'] = $('#tran_ref_no').val();
            detail['remarks'] = $('#remarks').val();
            detail['created_date'] = $('#created_date').val();
            var form        = '';
            get_data(ajax_url, form, div, detail, function (response)
            {
                if(response.flag=="@#success#@")
                    {   
                        $('#modal_form')[0].reset();				
                        detail['enter_amount'] = $('#enter_amount').val('');
                        detail['bank_name'] = $('#bank_name').val('');
                        detail['account_number'] = $('#account_number').val('');
                        detail['tran_ref_no'] = $('#tran_ref_no').val('');
                        detail['remarks'] = $('#remarks').val('');
						$('#myModal form :input').val("");
                        $('#success').attr('disabled', false).html('Submit');
                        $('.close').click();
                        showNotification('success','Request has been submited successfully');
                    }
					else if (response.flag=="@#failed#@")
					{
						swal("Oops...", response.msg, "error");
                        $('#modal_form')[0].reset();
                        $('#myModal').modal('hide');
                        //showNotification('error','Please fill the appropriate data');
						$('#success').attr('disabled', false).html('Submit');
					}

            }, '', false);
        }
        
        var div              = window.location.href;
        var chk              = div.substr(div.lastIndexOf('/') + 1);
        if(chk.match(/^\d+$/))
        {
            var div              = window.location.href;
            window.location.href = (div+"#wallet");
        }
           
        /* function for booking history  and cancel ticket */    
 
 $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var elem        = $(this).attr('href');
        
        if(elem == '#booking')
        {

            var detail      = {};
            var div         = "";
            var ajax_url    = 'front/booking/booking_history_json';
            var form        = '';

            $(elem+" .booking-history").html("");
            get_data(ajax_url, form, div, detail, function(response)
            {
                if(Object.keys(response.tickets).length >0)
                {

                    $.each(response.tickets, function(i, ticket){
                        var ticket_tpl = $.parseHTML($('#ticket_tpl').html());
                        var ticket_info_block = $(ticket_tpl).find('.ticket_info');
                        var passenger_tpl = $.parseHTML($('#ticket_detail_tpl').html());
                        var info_block;

                     
                        var todaysDate  = new Date();
                        var date        = $.datepicker.parseDate('yy/mm/dd', (ticket[0].inserted_date).replace(/-/g,'/') );
                        var newdate     = new Date((ticket[0].dept_time).replace(/-/g,'/'));
                        var day         = $.datepicker.formatDate( "dd", date);
                        var dayName     = $.datepicker.formatDate( "D", date);
                        var month       = $.datepicker.formatDate( "M", date);
                        var year        = $.datepicker.formatDate( "YY", date);

                        ticket_info_block.find('.date > .date').html(day);
                        ticket_info_block.find('.date > .month').html(month);
                        ticket_info_block.find('.date > .day').html(dayName);

                        if(newdate > todaysDate && ticket[0].ticket_status == "Y")
                        {
                            ticket_info_block.find('.status').html('UPCOMING');

                            var Cancel_btn = $('<a>').attr({ 
                                                //'class':'continueBtn cancel_ticket_btn',
                                                // 'data-ticket-id': ticket[0].ticket_id,
                                                'href' : BASE_URL + 'cancel_booking',
                                                'data-ref' :'',
                                                // 'data-pnr' : ticket[0].pnr_no,
                                                'data-opname' : ticket[0].op_name,
                                                'data-pro' : ticket[0].provider_id,
                                                'data-pro-ty' : ticket[0].provider_type,
                                                'data-opi' : ticket[0].op_id,
                                                'data-pc' : ticket[0].partial_cancellation,
                                                'data-t' : ticket[0].ticket_id,
                                                'data-pn' : ticket[0].pnr_no,
                                                'data-ei' : ticket[0].user_email_id,                                                
                                            }).html('<button class="continueBtn">Cancel Ticket</button>');

                            var print_ticket_view = $('<button>').attr({'data-ref':ticket[0].ticket_ref_no}).addClass("mr10 print_ticket_view").html("Print Ticket");

                            $(passenger_tpl).find('tfoot td').append(print_ticket_view);
                            $(passenger_tpl).find('tfoot td').addClass('text-right').append(Cancel_btn);

                        }
                        else if(ticket[0].ticket_status == "N")
                        {
                            ticket_info_block.find('.status').html('CANCELLED');
                            $(ticket_tpl).addClass("cancelled");

                            info_block  = $.parseHTML($('#info-tpl').html());

                            $(info_block)
                                .find('.info-container')
                                .html(
                                        $('<ul>').addClass('arrow')
                                                .append( $('<li>').html("Refund amount Rs."+ ticket[0].refund_amt))
                                                .append( $('<li>').html("Cancellation cahrges Rs."+ ticket[0].cancel_charge))
                                    );
                            $(passenger_tpl).find('tfoot td').append($(info_block));
                        }
                        else
                        {
                            ticket_info_block.find('.status').html('TRAVELLED');
                        }

                        var from_stop_name = (ticket[0].boarding_stop_name != "" && ticket[0].boarding_stop_name != undefined) ? ticket[0].boarding_stop_name : ticket[0].from_stop_name;
                        var till_stop_name = (ticket[0].destination_stop_name != "" && ticket[0].destination_stop_name != undefined) ? ticket[0].destination_stop_name : ticket[0].till_stop_name;
                        
                        ticket_info_block.find('.from').html(from_stop_name);
                        ticket_info_block.find('.to').html(till_stop_name);

                        ticket_info_block.find('.pnr_no').html(ticket[0].pnr_no);
                        ticket_info_block.find('.full_date').html(ticket[0].bus_dept_time);
                        /*ticket_info_block.find('.booked_on').html(ticket[0].inserted_date);*/
                        ticket_info_block.find('.bos_ref_no').html(ticket[0].boss_ref_no);
						if(ticket[0].total_without_discount == null){ ticket[0].total_without_discount = "0"; }else{ ticket[0].total_without_discount; }
                        ticket_info_block.find('.total_fare').html("Rs. "+ticket[0].tot_fare_amt_with_tax+" (Rs."+ ticket[0].total_without_discount +")");
                        ticket_info_block.find('.op_bus_service').html(ticket[0].op_name);

                        
                        $.each(ticket, function(key,value){
                            var seat_fare_charges = parseInt(value.seat_fare);

                            if(value.convey_type != null && value.convey_type.trim() == "fix")
                            {
                                seat_fare_charges = parseInt(value.seat_fare) + parseInt(value.convey_value);
                            }

                            var adult_child = (value["psgr_age"] > 11) ? "Adult" : "Child";
                            

                            var tr = $('<tr/>');
                            tr.append($('<td/>').html(value.psgr_name));
                            tr.append($('<td/>').html(value.psgr_age));
                            tr.append($('<td/>').html(value.psgr_sex));
                            tr.append($('<td/>').html(adult_child));
                            tr.append($('<td/>').html(value.seat_no));
                            tr.append($('<td/>').html(" Rs. "+seat_fare_charges));
                            if(value.ticket_detail_status == 'N')
                            {
                                 tr.append($('<td/>').html("Cancelled"));
                            }
                            else if (value.ticket_detail_status == 'Y')
                            {
                                tr.append($('<td/>').html(""));
                            }

                            $(passenger_tpl).find('tbody').append(tr);

                        });

                        if(ticket[0].discount_value != "" && ticket[0].discount_value != undefined && ticket[0].discount_value > 0)
                        {
                            var discount_tr = "";
                            discount_tr += "<tr class='theme-blue-color'>";
                            discount_tr += "<td colspan='5'><b>Total Discount</b></td>";
                            discount_tr += "<td> Rs. "+ticket[0].discount_value+"</td>";
                            discount_tr += "</tr>";
							discount_tr += "<tr class='theme-blue-color'>";
                            discount_tr += "<td colspan='5'><b>Mark Up Vlaue</b></td>";
							if(ticket[0].markup_value == null){ ticket[0].markup_value = "0"; }else{ ticket[0].markup_value; }
                            discount_tr += "<td> Rs. "+ ticket[0].markup_value+"</td>";
                            discount_tr += "</tr>";
                            $(passenger_tpl).find('tbody').append(discount_tr);
                        }

                        $(ticket_tpl).append($(passenger_tpl));

                        $(elem+" .booking-history").append($(ticket_tpl));

                   });
                }
                else
                {
                    $(elem+" .booking-history").append($('#ticket_tpl_error').html());
                }

            }, '', false);

        }
        else if(elem =='#favourite_list')
        {  
          show_fav_list();
            
            $(document).off('click', '#edit_fav').on('click', '#edit_fav', function(e) {
                var fav_id=$(this).attr("data-id");
                $('#edit_favourite_list').css('display','block');
                $('#view_list').css('display','none');
                $('#show_fav_err').css('display','none');
                $('form').find('input[type=text]').val('');
                $('form').find('input[type=label]').text('');
                var detail = {};
                var div = "";
                var ajax_url = 'front/front_favourite_list/edit_fav_detail_list';
               
                var form = 'edit-fav-form';

                detail['hiddn_fav_id'] = fav_id;
                $('.edit_add_list-body').html('');
                get_data(ajax_url, form, div, detail, function(response)
                { 
                    
                var markers = JSON.stringify(response);
            
                var tblRow = ' '; 
                tblRow +="<input type='hidden' value='" +markers+"' name='hiddn_json_string' id='hiddn_json_string' >";
                tblRow += '<input type="hidden" id="hiddn_fav_id" name="hiddn_fav_id" value="'+fav_id+'">';
                var sr_no=0;
                $('#edit_group_name').val(response.status[0]['cat_favourite_list']);
                $.each(response.status, function(i, val){
                    var selecte_f='';
                    var selecte_m='';
             
                      if(val.gender=="m" || val.gender=='male'){
                        selecte_m       ='checked';
                        selecte_f       ='';
                    }else if(val.gender=="f" || val.gender=='female'){
                       
                        selecte_f       ='checked';
                        selecte_m       ='';
                    }
                    
                   // tblRow += '<div class="row edit_fav_row"><div class="col-sms-3 col-sm-3"><input type="hidden" name="psgr_id[]" id="psgr_id" value="'+ val.id +'"><input type="text" class="input-text full-width alpha-only" value=" '+ val.fname +' " name="psgr_name[]"  id="psgr_name" placeholder="">';
                   tblRow += '<div class="row edit_fav_row"><div class="col-sms-3 col-sm-3"><input type="hidden" name="psgr_id[]" value="'+ val.id +'"><input type="text" class="input-text full-width alpha-only" value=" '+ val.fname +' " name="psgr_name[]"  id="psgr_name" placeholder="">';
                   tblRow += '<span class="errname error"></span></div><div class="col-sms-3 col-sm-3">';
                   tblRow += '<input type="text" class="input-text full-width age" value="'+ val.age +'"  name="age[]"  placeholder="" maxlength="2">';
                   tblRow += ' <span class="errage error"></span></div>';
                   tblRow += '<div class="col-sm-3"> <input type="hidden" name="fav_gen_count[]" value="'+sr_no+'"><label class="radio radio-inline"><input type="radio" name="psgr_gender['+sr_no+']" value="m" '+selecte_m+'  />';
                   tblRow += 'Male</label><label class="radio radio-inline">';
                   tblRow += '<input type="radio" name="psgr_gender['+sr_no+']" value="f" '+selecte_f+' />Female </label>';
                   tblRow += ' <span class="errgen error"></span></div>' ;
                   tblRow += '<div class="col-sm-3"><button type="button" class="btn btn-info group-view-btn mr5"  id="edit_add_fav_list"><i class="fa fa-plus-circle"></i> </button>';
                   tblRow += '<button type="button" class="btn btn-info group-view-btn" edit_data_id="'+ val.id +'" name="edit_delete_fav_list"><i class="fa fa-trash"></i> </button></div>';
                   tblRow+='</div>';
                   sr_no=sr_no+1;
                });
                tblRow+='<div class="from-group"><button type="button" id="save_edit_fav" class="btn btn-medium col-sms-6">Save Favourite List</button> ';
                tblRow+='<button type="button" id="edit_cancle-fav-btn" name="edit_cancle-fav-btn" class="btn btn-medium col-sms-6">GO BACK</button></div>';
                $('.edit_add_list-body').append(tblRow);
                }, '', false);
                 travelo_radio();
            });
            
            $(document).off('click', '#add_fav').on('click', '#add_fav', function(e) {
                $('#show_fav_err').css('display','none');
                         
                $('.add_list-body .add_fav_row').not(':first').remove();
                $('#add_favourite_list').css('display','block');
                $('form').find('input[type=text]').val('');
                $('form').find('input[type=label]').text('');
           
                $('#edit_favourite_list').css('display','none');
                $('#view_list').css('display','none');
            });
            

            $(document).off('click', '#add_fav_list').on('click', '#add_fav_list', function(e) {
                var newIndex = $('.add_fav_row').length;
                  
                if(newIndex <=6){
                    var row = $(this).parents(".add_fav_row").clone(true);
                    row.find(":text").val('')
                    row.find(":radio").attr("checked",false);
                    row.find('label').removeClass("checked");
                    row.find('input[type="radio"]').attr('name', 'psgr_gender['+newIndex+']');
                    row.find('input[type="radio"][value="m"]').attr('checked', 'true');
                     row.find("#fav_gen_count").val(newIndex);
                     $(this).parents(".add_fav_row").after(row);

                      travelo_radio();
                }else{
                     showNotification('error','Cannot add more than 6 passenger.');
                }
                
            });
            
              $(document).off('click', '#edit_add_fav_list').on('click', '#edit_add_fav_list', function(e) {
                var newIndex = $('.edit_fav_row').length;
              if(newIndex <=6){
                    var row = $(this).parents(".edit_fav_row").clone(true);
                    row.find(":text").val('');
                    row.find(":radio").attr("checked",false);
                    row.find('label').removeClass("checked");

                    row.find('input[id="psgr_id"]').val('new_data');
                    row.find('input[type="radio"]').prop('name', 'psgr_gender['+newIndex+']');
                    row.find('input[type="radio"][value="m"]').attr('checked', 'true');
                   // row.find('input[type="button"][name="edit_delete_fav_list"]').attr({'edit_data_id':''});
                   row.find('#fav_gen_count').val(newIndex);
                    $(this).parents(".edit_fav_row").after(row);
                     travelo_radio();
                 }else{
                     showNotification('error','Cannot add more than 6 passenger.');
                }
            });
            
            $(document).off('click', '#delete_fav_list').on('click', '#delete_fav_list', function(e) {
                var newIndex = $('.add_fav_row').length;
                
                $('#fav_div_id').val(newIndex) ;        
              
              
                if(newIndex <= 1) { 
                  showNotification('error','Cannot delete all the rows.');
                }
                else{
                   //  $('#fav_model').modal('show') ;   
                    $(this).closest('.row').remove();
                }

        });
        $(document).off('click', '#edit_delete_fav_list').on('click', '#edit_delete_fav_list', function(e) {
              var newIndex = $('.edit_fav_row').length;
              $('#fav_div_id').val('');
               
              var chk_del=$(this).parents(".edit_fav_row").find('input[id="psgr_id"]').val();
             // alert(chk_del);
              if(newIndex <= 1) { 
                showNotification('error','Cannot delete all the rows.');
              }else{
                  if(chk_del!='new_data'){
                  var delete_id=$(this).attr('edit_data_id');
                  
                    $('#fav_model').modal('show') ;   
                    $('#fav_div_id').val(delete_id);
                 }else{
               
                    $(this).closest('.row').remove();
                 }
                
                }

        });
        $(document).off('click', '#delete_fav_group').on('click', '#delete_fav_group', function(e) {
           
            e.preventDefault();

            var yesno = confirm("Are you sure you want to delete this favourite list?");

            if(yesno)
            {
                var elem=$(this);
                var detail = {};
                var div = "";
                var ajax_url = 'front/front_favourite_list/delete_fav_detail_list';
               
                detail['hiddn_fav_id'] = $(this).attr("data-id");
                
                get_data(ajax_url, form, div, detail, function(response)
                {             
                   if(response.flag==="@success#")
                            {
                            elem.closest('.detail-list-body').remove();
                            showNotification('success','Favourite List has been deleted!');
                           
                           }
                            else
                            {
                             showNotification('error','Favourite List has been not deleted');
                            }
                    

                }, '', false);
            }

        });
        
        $(document).off('click', '#cancle-fav-btn').on('click', '#cancle-fav-btn', function(e) {
        
            e.preventDefault();
            $("#view_list").fadeIn();
            $("#add_favourite_list").fadeOut();
        });
        
         $(document).off('click', '#edit_cancle-fav-btn').on('click', '#edit_cancle-fav-btn', function(e) {
           e.preventDefault();
           $("#edit_favourite_list").fadeOut();
           $("#view_list").fadeIn();
            
          
        });
         $(document).off('click', '.row').on('click', '.row', function(e) {
             $(this).parent().find('.group-passenger').slideToggle('medium');
        }).css({'cursor':'pointer'});
        
        $(document).off('click', '#save_edit_fav').on('click', '#save_edit_fav', function(e) {
            e.preventDefault();
            var elem=$(this);
            var detail = {};
            var div = "";
            var ajax_url = 'front/front_favourite_list/save_update_list';
            var form = 'edit-fav-form';
            
            detail['hiddn_fav_id'] = $('#hiddn_fav_id').val();
            
            get_data(ajax_url, form, div, detail, function(response)
            {             
               if(response.flag==="@#Success#@")
                {
                        elem.closest('.row').remove();
                        elem.parent().find('.group-passenger').remove();
                         $("#view_list").fadeIn();
                         $("#edit_favourite_list").fadeOut();
                        show_fav_list();
                        showNotification('success','Favourite List has been updated!');
                       
                }
                else if (response.flag == "@#error@")
                    {   
                    var disp_show_err='<ul>';
                    $.each(response.error, function(i, val){

                        disp_show_err+='<li>'+val+'</li>';

                    }); disp_show_err+='</ul>';
                    $('#edit_show_fav_err').css('display','block');
                    $(".show_err").html(disp_show_err);
                     
                    }

            }, '', false)
            });
            
           $(document).off('click', '#save_add_fav').on('click', '#save_add_fav', function(e) {
                
                e.preventDefault();
               if ($("#add-fav-form").valid())
                {
                var detail = {};
                var div = "";
                var ajax_url = 'front/front_favourite_list/save_list';

                var form = 'add-fav-form';
                get_data(ajax_url, form, div, detail, function(response)
                { 
                    if (response.flag == '@#success#@')
                    {  $("#view_list").fadeIn();
                       $("#add_favourite_list").fadeOut();
                        show_fav_list(); 
                        showNotification('success','Favourite list created successfully!');
                     }
                    else if (response.flag == "@#error@")
                    {
                        var disp_show_err='<ul>';
                        $.each(response.error, function(i, val)
                        {
                           disp_show_err+='<li>'+val+'</li>';
                        }); 

                        disp_show_err+='</ul>';
                        $('#show_fav_err').css('display','block');
                        $(".show_err").html(disp_show_err);
                    }
                });
           }
            else 
            {
                $('#add-fav-form').validate();
            }
                    
       });
          
             $('#group_name').blur(function() {
                e.preventDefault();
                var detail = {};
                var ajax_url = 'front/front_favourite_list/check_exist_favourite_group';
                var div = "";
                
                detail['fav_group'] = $('#group_name').val();
           
                get_data(ajax_url, form, div, detail, function(response)
                {
                   
                    if (response.status == '@#failed#@')
                    {
                        showNotification('error','Favourite List Already Exists');
                        $('#save_add_fav').prop('disabled', true);
                        $('#add_fav_list').prop('disabled', true);
                    }else{
                        $('#save_add_fav').prop('disabled', false);
                         $('#add_fav_list').prop('disabled', false);
                    }
                }, '', false);
            
             });
             
            $(document).off('click', '#confirm_del').on('click', '#confirm_del', function(e) {
            
                var show_id=$('#fav_div_id').val();

                $('.group-view-btn[edit_data_id="'+show_id+'"]').closest('.row').remove();
                $('#fav_model').modal('hide') ;   
                return true;
            });

            $(document).off('keyup', '#search_id').on('keyup', '#search_id', function(e) {
                
        });
    }
    else if(elem=="#settings")
    {
     $("#pemail").val($("#hddemail").val());
    }
    });


    $(document).off('click', '.print_ticket_view').on('click', '.print_ticket_view', function(e)
    {
        var ticket_ref_no = $(this).attr("data-ref");
        var url = base_url+"front/booking/view_ticket/"+ticket_ref_no;
        // $(location).attr("href", url);
        window.open(url, '_blank');
    });
           

}); // end of ready

    function show_fav_list(){
      var detail      = {};
      var div         = "";
      var ajax_url    = 'front/front_favourite_list/get_list';
      var form        = '';

     $('#view_list').css('display','block');
     $('#edit_favourite_list').css('display','none');
     $(".show_err").html('');
     $('#add_favourite_list').css('display','none');
     $('.group-list .list-body').html("");
      get_data(ajax_url, form, div, detail, function(response)
      {   
          if(response.flag=='@#success#@')
          {    var tr ='';
              var gender_name='';
              $.each(response.details_data, function(i, detail_val){

                  var group_list_tpl = $.parseHTML($('#fav_group_list_tpl').html());

                  $(group_list_tpl).find('.group-name').html(detail_val[0].cat_favourite_list.substring(0,20) );
                  $(group_list_tpl).find('.group-pass-count').html(detail_val[0].no_psgr) ;
                  $(group_list_tpl).find('.created-date').html(detail_val[0].fav_created );
                  $(group_list_tpl).find('.group-view-btn').attr({'data-id':detail_val[0].fav_id});
                  $(group_list_tpl).find('#edit_fav').attr({'data-id':detail_val[0].fav_id});

                  $.each(detail_val, function(i,value){

                      tr = $('<tr/>');
                      tr.append($('<td/>').html(value.fname.substring(0,20)));
                      tr.append($('<td/>').html(value.age));
                      if(value.gender=='f' || value.gender=='female' ) {
                          gender_name='Female';
                      }else if(value.gender=='m' || value.gender=='male'){
                           gender_name='Male';
                       }
                      tr.append($('<td/>').html(gender_name));
                      
                   $(group_list_tpl).find('.pass_details').append(tr);
                  }); 

             $('.group-list .list-body').append($(group_list_tpl));

              });
          }
          else{
                // $('.group-list').css('display','none');
                $('.list-body').html('Please add the favourite list');
              
          }
      });
    }



    function search_fav_list()
    {
        var search_val                  = $('#search_id').val();

        var detail                      = {};
        var div                         = "";
        var ajax_url                    = 'front/front_favourite_list/search_get_list';
        var form                        = '';
        detail['search_value']          =search_val;
        //detail['search_side']          =search_val;
        $('#view_list').css('display','block');
        $('#edit_favourite_list').css('display','none');
        $(".show_err").html('');
        $('#add_favourite_list').css('display','none');
        $('.group-list .list-body').html("");
        get_data(ajax_url, form, div, detail, function(response)
        {   
            if(response.flag=='@#success#@')
            {   
                var tr ='';
                var gender_name='';

                if(Object.keys(response.details_data).length)
                {
                    $.each(response.details_data, function(i, detail_val)
                    {

                        var group_list_tpl = $.parseHTML($('#fav_group_list_tpl').html());

                        $(group_list_tpl).find('.group-name').html(detail_val[0].cat_favourite_list.substring(0,20) );
                        $(group_list_tpl).find('.group-pass-count').html(detail_val[0].no_psgr) ;
                        $(group_list_tpl).find('.created-date').html(detail_val[0].inserted_date );
                        $(group_list_tpl).find('.group-view-btn').attr({'data-id':detail_val[0].fav_id});
                        $(group_list_tpl).find('#edit_fav').attr({'data-id':detail_val[0].fav_id});
                        $.each(detail_val, function(i,value)
                        {

                            tr = $('<tr/>');
                            tr.append($('<td/>').html(value.fname.substring(0,20)));
                            tr.append($('<td/>').html(value.age));
                            if(value.gender=='f' || value.gender=='female' ) {
                                gender_name='Female';
                            }else if(value.gender=='m' || value.gender=='male'){
                                 gender_name='Male';
                             }
                            tr.append($('<td/>').html(gender_name));
                         $(group_list_tpl).find('.pass_details').append(tr);
                        }); 

                        $('.group-list .list-body').append($(group_list_tpl));
                    });
                }
                else
                {
                    // $('.group-list').css('display','none');
                    $('.group-list .list-body').html('No favourite list found. Please add the favourite list.');
                }
            } 
            else
            {
                // $('.group-list').css('display','none');
                $('.group-list .list-body').html('No favourite list found. Please add the favourite list.');
            }
        });
    }         
</script>
<script type="text/javascript">
 $(document).off('keypress', '#pg_amt').on('keypress', '#pg_amt', function(e) 
        {
           var regex = new RegExp("^[0-9\b]+$");//accept only _
           var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
             key=e.charCode? e.charCode : e.keyCode;
            if(key==9 )
            {
                return true; 
            }
            if (regex.test(str)) 
            {
                return true;
            }
            e.preventDefault();
            return false;
        });
</script>

<!--  Template script for booking tab   -->
<script type="text/template" id="ticket_tpl">
    <div class="booking-info clearfix">
        <div class="ticket_info" style="cursor: pointer;">
            <div class="date">
                <label class="month"></label>
                <label class="date"></label>
                <label class="day"></label>
            </div>
            <h4 class="box-title">
                <i class="icon fa fa-bus yellow-color circle"></i>
                <span class="from"></span> to <span class="to"></span> 
                <small class='op_bus_service'></small>
            </h4>
            <dl class="info">
                <dt>Total Fare</dt>
                <dd class="total_fare"></dd>
                <dt>Travel Date</dt>
                <dd class="full_date"></dd>
            </dl>
            <dl class="info">
                <dt>PNR No</dt>
                <dd class="pnr_no"></dd>
                <dt>Rokad Ref. No.</dt>
                <dd class="bos_ref_no"></dd>
            </dl>
            <button class="btn-mini status">UPCOMMING</button>
            <div class="clearfix"></div>
        </div>
    </div>
</script>

<script type="text/template" id="ticket_tpl_error">
    <div class="info-box block">
        <p>No data found.</p>
    </div>
</script>

<script type="text/template" id="ticket_detail_tpl">
    <div class="passenger_info clearfix" style="display: none;">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Age</th>
                    <th>Gender</th>
                    <th>Adult / Child</th>
                    <th>Seat No</th>
                    <th>Fare</th>
                </tr>
            </thead>
            <tbody>  

            </tbody>
            <tfoot>
                <tr>
                    <td class="text-left" colspan="6">

                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
</script>
<script type="text/template" id="info-tpl">
    <div class="info-box">
        <span class="close"></span>
        <div class="info-container"></div>
    </div>
</script>
<!--  Template script for booking tab   -->


<script type="text/template" id="favourite_list_tpl_error">
    <div class="info-box block">
        <span class="close"></span>
        <p>No data found.</p>
    </div>
</script>
<style>
.ui-datepicker {z-index: 2000 !important;}
</style>
<script type="text/javascript">
$(".bank_details").click(function (){
    $('#bank_info').toggle('slide', { direction: "up" }, 500);
});
	
function disable_button(button_id)
{
	var temp = button_id+'_loading';
	$("#"+temp).remove();			
	var pdiv = $("#"+button_id).closest("div");
	pdiv.append( "<button class='btn-medium btnorange' id='"+temp+"' type='button' >Processing</button>" );				
	$("#"+button_id).hide();
}

function enable_button(button_id)
{
	var temp = button_id+'_loading';
	$("#"+temp).remove();				
	$("#"+button_id).show();
}
</script>