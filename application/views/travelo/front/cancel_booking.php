<script type="text/javascript" src="<?php echo base_url(); ?>js/front/cancel_booking.js" ></script>
<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Cancel Ticket</h2>
        </div>

    </div>
</div>
<section id="content">
    <div class="container">
        <div id="main" class="travelo-box">
            <div class="row">
                <div class="col-sm-3">
                    <?php 
                        $attributes = array('id' => "ticket_detail_form",'name' => "ticket_detail_form",'method' => "post");
                        echo form_open(base_url()."cancel_booking", $attributes); 
                    ?>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email Address</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" value="<?php echo $email_id ?>" data-msg-required="Please enter Email Address">
                        </div>
                        <div class="form-group">
                            <label for="pnr_no">Rokad Reference Number</label>
                            <input type="text" class="form-control bos_pnr" placeholder="Rokad Reference Number" name="bos_ref_no" id="bos_ref_no" value="<?php echo $bos_ref_no ?>">
                        </div>

                        <div class="seperator_or">
                            <label class="text-align-center white dark_gray_bg">OR</label>
                        </div>

                        <div class="form-group">
                            <label for="pnr_no">PNR Number</label>
                            <input type="text" class="form-control bos_pnr one-option-require" id="pnr_no" name="pnr_no" placeholder="PNR Number" value="<?php echo $pnr_no ?>" >
                        </div>
                        <div class="form-group">
                            <label for="">&nbsp;</label>
                            <button type="submit" class="button btn-medium <?php echo THEME_BTN; ?> btnw" name="show_ticket">Show Ticket</button>
                        </div>
                    </form>
                </div>
                <div class="col-sm-9">
                    <div style="background: #fff;" class="pad20">
                        <div style="background: #fff;" id="booking">
                            <h2>Trips You have Booked!</h2>
                            <div class="booking-history">
                                <?php if (count($ticket_detail) > 0) { ?>
                                    <?php foreach ($ticket_detail as $key => $info): ?>
                                        <?php 
                                            $attributes = array('class' => 'cancel_booking_form cancel_'.$info['boss_ref_no'], 'method' => 'post', 'onsubmit' => 'return false');
                                            echo form_open("#", $attributes); 
                                        ?>
                                            <div class="booking-info clearfix">
                                                <div class="ticket_info" style="cursor: pointer;">
                                                    <div class="date">
                                                        <label class="month"><?php echo date("M", strtotime($info["dept_time"])); ?></label>
                                                        <label class="date"><?php echo date("d", strtotime($info["dept_time"])); ?></label>
                                                        <label class="day"><?php echo date("D", strtotime($info["dept_time"])); ?></label>
                                                    </div>
                                                    <h4 class="box-title">
                                                        <i class="icon fa fa-bus yellow-color circle"></i>
                                                        <?php
                                                        /* $from_stop_name = (isset($info['boarding_stop_name']) && $info['boarding_stop_name'] != "") ? $info['boarding_stop_name'] : $info['from_stop_name'];
                                                          $till_stop_name = (isset($info['destination_stop_name']) && $info['destination_stop_name'] != "") ? $info['destination_stop_name'] : $info['till_stop_name']; */

                                                        $from_stop_name = $info['from_stop_name'];
                                                        $till_stop_name = $info['till_stop_name'];
                                                        ?>
                                                        <?php echo $from_stop_name; ?> to <?php echo $till_stop_name; ?> 
                                                        <small><?php echo $info['op_name']; ?></small>
                                                    </h4>
                                                    <dl class="info cancel_info">
                                                        <dt>Total Fare</dt>
                                                        <dd class="mb5"><?php echo $info['payment_by'] == 'csc' ? to_currency($info['total_fare_without_discount']) : to_currency($info['tot_fare_amt_with_tax']); ?></dd>
                                                        <dt>Travel Date</dt>
                                                        <dd><?php echo date("jS M y h:i A", strtotime($info['dept_time'])); ?></dd>
                                                    </dl>
                                                    <dl class="info cancel_info">
                                                        <dt>PNR No</dt>
                                                        <dd class="mb5"><?php echo $info['pnr_no']; ?></dd>
                                                        <dt>Rokad Ref No</dt>
                                                        <dd class="mb5"><?php echo $info['boss_ref_no']; ?></dd>
                                                    </dl>
                                                    <button class="btn-mini status">UPCOMMING</button> 
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="passenger_info clearfix" style="display:block;">
                                                    <table class="table table-hover table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>Name</th>
                                                                <th>Age</th>
                                                                <th>Gender</th>
                                                                <th>Seat No</th>
                                                                <th>Fare</th>
                                                                <?php
                                                                if ($info['partial_cancellation'] == 1) {
                                                                    ?>
                                                                    <th>To Cancel</th>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            foreach ($info['detail_arr'] as $td_key => $td_value) {
                                                                $td_fare_amount = $td_value->fare_amt;
                                                                if (trim($td_value->convey_type) == "fix") {
                                                                    $td_fare_amount = $td_value->fare_amt + $td_value->convey_value;
                                                                }
                                                                ?>
                                                                <tr>
                                                                    <td><?php echo $td_value->psgr_name ?></td>
                                                                    <td><?php echo $td_value->psgr_age ?></td>
                                                                    <td><?php echo $td_value->psgr_sex ?></td>
                                                                    <td><?php echo $td_value->seat_no ?></td>
                                                                    <td><?php echo to_currency($td_fare_amount); ?></td>
            <?php
            if ($info['partial_cancellation'] == 1) {
                if ($td_value->status == "Y") {
                    ?>
                                                                            <td>
                                                                                <input type="checkbox"
                                                                                       checked="true"
                                                                                       name ="seat_no[]"
                                                                                       class="seat_no cursor-pointer"                                                                   
                                                                                       id="<?php echo $td_value->id ?>"
                                                                                       value="<?php echo $td_value->seat_no ?>" name="" />
                                                                            </td>
                    <?php
                } else {
                    echo "<td>Cancelled</td>";
                }
            }

            echo "</tr>";
        }
        ?>
                                                            <tr class='theme-blue-color'>
                                                                <?php
                                                                if (isset($ticket_detail[$key]["discount_value"]) && $ticket_detail[$key]["discount_value"] > 0) {
                                                                    echo "<td colspan='4'><b>Total Discount</b></td>";
                                                                    echo "<td> Rs. " . $ticket_detail[$key]["discount_value"] . "</td>";
                                                                }
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="6" class="text-right">
                                                                <?php
                                                                $show_cancel_button = true;
                                                                eval(FME_AGENT_ROLE_ID);
                                                                eval(FME_AGENT_ROLE_NAME);
                                                                if ((in_array($ticket_detail[$key]["role_id"], $fme_agent_role_id)) && (in_array($ticket_detail[$key]["booked_by"], $fme_agent_role_name))) {
                                                                    if (!$this->session->userdata('role_id') && (!in_array($this->session->userdata('role_id'), $fme_agent_role_id)) && !$this->session->userdata('role_name') && (!in_array($this->session->userdata('role_name'), $fme_agent_role_name))) {
                                                                        $show_cancel_button = false;
                                                                    }
                                                                }

                                                                if ($show_cancel_button) {
                                                                    ?>
                                                                        <button class="continueBtn is_cancel_btn <?php echo THEME_BTN; ?>" data-pro="<?php echo $info['provider_id']; ?>" data-pro-ty="<?php echo $info['provider_type']; ?>" data-opi="<?php echo $info['op_id']; ?>" data-pc="<?php echo $info['partial_cancellation']; ?>" data-t="<?php echo $info['ticket_id']; ?>" data-pn="<?php echo $info['pnr_no']; ?>" data-ei="<?php echo $info['user_email_id']; ?>" data-bsf="<?php echo $info['boss_ref_no']; ?>">Cancel Ticket</button>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                        </tfoot>

                                                    </table>
                                                </div>
                                            </div>
                                        </form>
                                                                <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
    <?php
} else {
    ?>
                        <div class="booking-info clearfix">
                            <h3>No ticket found.<h3/>
                        </div>

                                    <?php
                                }
                                ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pagination pull-right">
                    <?php echo $links; ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade cancellation_details" id="cancellation_details" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <span class="close modal_close cursor-pointer" data-dismiss="modal">&times;</span>
            <div class="modal-body">
                <div class="modal_cancellation">
                    <center>
                        <span class="loader_25">
                            <img src="<?php echo base_url() . COMMON_ASSETS; ?>images/loading.gif" alt="Loading">
                        </span>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Print Ticket Start Here -->
<!-- Modal -->

<div class="modal fade printable autoprint" id="print_cancel_ticket" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:850px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">E- Cancellation Ticket</h4>
            </div>
            <div class="modal-body" id="modal-body1">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary" onClick="window.print();">Print</button> -->
                <button type="button" onclick="openWin()" class="btn btn-primary print_this_ticket">Print</button>
                <!--<a href="#" onclick="openWin()">click</a> -->
            </div>
        </div>
    </div>
</div>

<!-- Print Ticket Start Here -->

<script type="text/javascript">
    $(document).ready(function () {
        $('.ticket_info').click(function () {
            $(this).parent().find('.passenger_info').slideToggle('medium');
        }).css({'cursor': 'pointer'});


        $('#ticket_detail_form').validate({
            ignore: "",
            rules:
                    {
                        email:
                                {
                                    required: true,
                                    email: true,
                                },
                        pnr_no:
                                {
                                    require_from_group: [1, ".bos_pnr"]
                                },
                        bos_ref_no:
                                {
                                    require_from_group: [1, ".bos_pnr"]
                                }
                    },
            messages:
                    {
                        pnr_no:
                                {
                                    require_from_group: "Please enter either Rokad Ref No or PNR No"
                                },
                        bos_ref_no:
                                {
                                    require_from_group: "Please enter either Rokad Ref No or PNR No"
                                }
                    },
            groups:
                    {
                        mygroup: "bos_ref_no pnr_no",
                    },
            errorPlacement: function (error, element) {
                if ((element.attr('name') == 'bos_ref_no') || (element.attr('name') == 'pnr_no'))
                {
                    error.insertAfter('.one-option-require');
                } else
                {
                    error.insertAfter(element);
                }
            },

        });

        $(document).off('click', '.is_cancel_btn').on('click', '.is_cancel_btn', function (e) {

            e.preventDefault();

            var elem = $(this);
            var detail = {};
            var form = '';
            var div = '';
            var ajax_url = 'is_ticket_cancellable';
            var formElem = $(this).parents('form.cancel_booking_form');
            var seat = [];

            formElem.find('.seat_no:checked').each(function (i, chekbox) {
                seat.push($(this).val());
            });

            var bos_ref_no = $(this).attr('data-bsf');

            detail['seat_no'] = seat;
            detail['email'] = $(this).attr('data-ei');
            detail['pnr_no'] = $(this).attr('data-pn');
            detail['pro_id'] = $(this).attr('data-pro');
            detail['ticket_id'] = $(this).attr('data-t');
            detail['pc'] = $(this).attr('data-pc');
            detail.<?php echo $this->security->get_csrf_token_name(); ?> = '<?php echo $this->security->get_csrf_hash(); ?>';
            detail['proty'] = $(this).attr('data-pro-ty');
            detail['opi'] = $(this).attr('data-opi');
            if (detail['pro_id'] != "" && detail['pro_id'] != undefined)
            {
                /*var yesno = confirm("Are you sure you want to cancel ticket?");*/
                var yesno = true;

                if (yesno)
                {
                    $("#cancellation_details .modal_cancellation").html(
                            $("<center/>")
                            .html(
                                    $("<span/>").addClass("loader_25")
                                    .html(
                                            $("<img/>").attr({"src": base_url + "<?php echo COMMON_ASSETS ?>images/loading.gif"})
                                            )
                                    )
                            );

                    $('#cancellation_details').modal('show');
                    $(this).addClass('bg_disabled').removeClass('is_cancel_btn').html('Please Wait');
                    get_data(ajax_url, form, div, detail, function (response)
                    {
                        var msg_block = $.parseHTML($('#msg_box').html());
                        var ul = $('<ul/>').addClass('arrow');

                        if (response.flag == '@#success#@')
                        {
                            ul.append($('<h5/>').html($('<b/>').html("Ticket is cancellable.")));
                            ul.append($('<li/>').html('Refund amount will be Rs.' + Math.round(response.refund_amt * 100) / 100));
                            ul.append($('<li/>').html('Cancellation charges will be Rs.' + Math.round(response.cancellation_cahrge * 100) / 100));
<?php
eval(AGENT_SITE_ARRAY);
if (in_array($this->session->userdata('role_id'), $agent_site_array)) {
    ?>
                                ul.append($('<li/>').html('Amount Return To Customer Rs.' + Math.round(response.custumer_return * 100) / 100));
<?php } ?>
                            ul.append($('<br/>'));
                            ul.append($('<div/>')
                                    .html(
                                            $('<button/>').addClass("continueBtn cancel_btn <?php echo THEME_BTN; ?>")
                                            .attr({
                                                "data-pro": detail['pro_id'],
                                                "data-pro-ty": detail['proty'],
                                                "data-opi": detail['opi'],
                                                "data-pc": detail['pc'],
                                                "data-t": detail['ticket_id'],
                                                "data-pn": detail['pnr_no'],
                                                "data-ei": detail['email'],
                                                "data-bsf": bos_ref_no
                                            })
                                            .html("Cancel Ticket")
                                            )
                                    );

                            $(msg_block).find('.info_container').append(ul);
                            $(".bg_disabled").addClass('is_cancel_btn').removeClass('bg_disabled').html('Cancel Ticket');
                            $("#cancellation_details .modal_cancellation").html($(msg_block));
                            /*showNotification("success", "Ticket is cancellable.");*/
                        } else
                        {
                            $(".bg_disabled").addClass('is_cancel_btn').removeClass('bg_disabled').html('Cancel Ticket');
                            ul.append($('<h5/>').html($('<b/>').html(response.error)));
                            $(msg_block).find('.info_container').append(ul);
                            $("#cancellation_details .modal_cancellation").html($(msg_block));
                            // $('#cancellation_details').modal('hide');
                            // showNotification("danger", response.error);
                        }
                    });
                }
            } else
            {
                showNotification("danger", "Some problem occured. Please refresh page.");
                $('#cancellation_details').modal('hide');
            }
        });

        $(document).off('click', '.cancel_btn').on('click', '.cancel_btn', function (e) {

            e.preventDefault();

            var bos_ref_no = $(this).attr('data-bsf');
            /*var elem 		= $(this);
             var wrapper 	= $(this).parents('.booking-info');*/
            var wrapper = $(".cancel_" + bos_ref_no + " .booking-info");
            var detail = {};
            var form = '';
            var div = '';
            var ajax_url = 'cancel_tickets';
            var formElem = $('.cancel_' + bos_ref_no);
            var seat = [];
            var seatid = [];

            formElem.find('.seat_no:checked').each(function (i, chekbox) {
                seat.push($(this).val());
                seatid.push($(this).attr("id"));
            });


            detail['seat_no'] = seat;
            detail['email'] = $(this).attr('data-ei');
            detail['pnr_no'] = $(this).attr('data-pn');
            detail['pro_id'] = $(this).attr('data-pro');
            detail['ticket_id'] = $(this).attr('data-t');
            detail['pc'] = $(this).attr('data-pc');

            detail['proty'] = $(this).attr('data-pro-ty');
            detail['opi'] = $(this).attr('data-opi');

            if (detail['pro_id'] != "" && detail['pro_id'] != undefined)
            {
                var yesno = confirm("Are you sure you want to cancel this ticket?");

                if (yesno)
                {
                    $(this).addClass('bg_disabled').removeClass('cancel_btn').html('Please Wait');
                    get_data(ajax_url, form, div, detail, function (response)
                    {
                        if (response.flag == '@#success#@')
                        {
                            $.each(seatid, function (Cseatkey, cSeatId)
                            {
                                $("#" + cSeatId).parent('td').html("Cancelled");
                            });

                            var msg_block = $.parseHTML($('#msg_box').html());

                            var ul = $('<ul/>').addClass('arrow');
                            ul.append($('<li/>').html('Refund amount Rs.' + Math.round(response.refund_amt * 100) / 100));
                            ul.append($('<li/>').html('Cancellation charges Rs.' + Math.round(response.cancellation_cahrge * 100) / 100));

                            $(msg_block).find('.info_container').append(ul);
                            wrapper.addClass('cancelled').find('.ticket_info .status').html('CANCELLED');

                            /*elem.parent().removeClass('text-right').addClass('text-left').html($(msg_block));*/
                            $(".cancel_" + bos_ref_no + " .booking-info .passenger_info table tfoot td").removeClass('text-right').addClass('text-left').html($(msg_block));

                            showNotification("success", "Ticket cancelled successfully.");
                            $('#cancellation_details').modal('hide');

                            if (response.ticket_layout != undefined && response.ticket_layout != '')
                            {
                                $("#modal-body1").html(response.ticket_layout);
                                $("#print_cancel_ticket").modal("show");
                            }
                        } else
                        {
                            $(".bg_disabled").addClass('cancel_btn').removeClass('bg_disabled').html('Cancel Ticket');
                            showNotification("danger", response.error);
                        }

                        $('#cancellation_details').modal('hide');
                    });
                }
            } else
            {
                showNotification("danger", "Some problem occured. Please refresh page.");
            }

        });

    }) // end of document
    function openWin()
    {
        var myWindow = window.open("", "", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=700,width=550,height=550");
        var is_chrome = Boolean(myWindow.chrome);
        var divText = document.getElementById("modal-body1").outerHTML;
        myWindow.document.write(divText);
        if (is_chrome)
        {
            setTimeout(function ()
            { // wait until all resources loaded 
                myWindow.document.close(); // necessary for IE >= 10
                myWindow.focus(); // necessary for IE >= 10
                myWindow.print(); // change window to winPrint
                myWindow.close(); // change window to winPrint
            }, 250);
        } else
        {
            myWindow.document.close(); // necessary for IE >= 10
            myWindow.focus(); // necessary for IE >= 10
            myWindow.print();
            myWindow.close();
        }
    }
</script>
<style type="text/css">
    .pagination{ margin-right: 15px;}
    .pagination a{ padding: 5px; margin: 0 5px; }
</style>
<script type="text/html" id="msg_box">
    <div class="info-box auto_hide" data-time="300">
        <span class="close"></span>
        <div class="info_container"></div>
    </div>
</script>