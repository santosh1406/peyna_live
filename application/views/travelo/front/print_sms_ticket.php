<div class="page-title-container">
	<div class="container">
		<div class="page-title pull-left">
			<h2 class="entry-title">Print Ticket</h2>
		</div>
	</div>
</div>

<section id="content">
	<div class="container">
		<div id="main" class="travelo-box top_space">
			<div class="row">
				<div class="col-md-4 col-sm-3">
                    <?php 
                        $attributes = array('name' => 'ticket_detail_form', 'id' => 'ticket_detail_form', 'onsubmit' => 'return false');
                        echo form_open("#", $attributes); 
                    ?>
						<div class="form-group">
							<label for="exampleInputEmail1">Email Address</label>
							<input type="email" class="form-control" id="email" name="email" placeholder="Email Address" value="">
						</div>
						<div class="form-group">
							<label for="bos_ref_no">Rokad Reference Number</label>
							<input type="text" class="form-control" id="bos_ref_no" name="bos_ref_no" placeholder="Rokad Reference Number" value="" >
						</div>
						<div class="form-group">
							<label for="pnr_no">Get Ticket By</label>
							<label class="radio radio-inline">
								<input type="radio" name="ticket_mode" value="print" checked>
	                            Print / View
                            </label>
                            <label class="radio radio-inline">
	                            <input type="radio" name="ticket_mode" value="sms">
	                            mTicket (SMS)
                            </label>
                            <label class="radio radio-inline">
	                            <input type="radio" name="ticket_mode" value="mail">
	                            Email
                            </label>
						</div>
						<div class="form-group">
							<label for="">&nbsp;</label>
							<button type="button" class="button btn-medium <?php echo THEME_BTN;?>" name="show_ticket" id="ticket_mode">Get Ticket</button>
						</div>
					</form>
				</div>

				<div class="col-md-8 col-sm-7">
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="booking">
                                <h2>Trips You have Booked!</h2>
                                <div class="booking-history">
                                </div>
                            </div>
                        </div>
				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	$(document).ready(function()
	{
		$(document).off("click","#ticket_mode").on("click","#ticket_mode", function(e){
			
			if($('#ticket_detail_form').valid())
            {
				var detail = {};
	            var div = "";
	            var ajax_url = 'front/booking/ticket_by_sms_email';
	            var form = '';

	            detail['ticket_mode'] =  $('#ticket_detail_form input[name=ticket_mode]:checked').val();;
	            detail['bos_ref_no'] = $('#ticket_detail_form #bos_ref_no').val();
	            detail['email'] = $('#ticket_detail_form #email').val();
	            detail['is_history_task'] = 0;
                detail.<?php echo $this->security->get_csrf_token_name(); ?> = '<?php echo $this->security->get_csrf_hash(); ?>';
	            $(this).addClass("bg_gray").prop('disabled', true);

	            get_data(ajax_url, form, div, detail, function(response)
	            {
	                if (response.flag == '@#success#@')
	                {
	                	if(response.type == "print")
	                	{
	                		window.location.href = base_url+"front/booking/view_ticket/"+response.ticket_details.ticket_ref_no;
	                	}
	                	else
	                	{
	                		showNotification(response.msg_type,response.msg);
	                	}
	                }
	                else if(response.flag == '@#error#@')
	                {
	                	showNotification(response.msg_type,response.msg);
	                }
	                else
	                {
	                    alert(response.msg);
	                }

	                $("#ticket_mode").removeClass("bg_gray").prop('disabled', false);

	            }, '', false);
        	}
		});


		$('#ticket_detail_form').validate({
	        ignore: "",
	        rules: {
	            email: {
	                required: true,
	            },
	            bos_ref_no : {
	                required: true,
	            },
	            ticket_mode : {
	                required: true,
	            }
	        },
	        messages: {
	            email: {
	              required: "Please Enter Email Address",
	            },
	            bos_ref_no: {
	              required: "Please Enter Rokad Reference Number",
	            }
	            ,
	            ticket_mode: {
	              required: "Please Enter ticket mode"
	            }
	        }
	    });


	    function show_history()
	    {
	    	var detail      = {};
            var div         = "";
            var ajax_url    = 'front/booking/booking_history_json';
            var form        = '';
            detail.<?php echo $this->security->get_csrf_token_name(); ?> = '<?php echo $this->security->get_csrf_hash(); ?>';
            $(".booking-history").html("");
            get_data(ajax_url, form, div, detail, function(response)
            {
                if(Object.keys(response.tickets).length >0)
                {
                    $.each(response.tickets, function(i, ticket){
                        var ticket_tpl = $.parseHTML($('#ticket_tpl').html());
                        var ticket_info_block = $(ticket_tpl).find('.ticket_info');
                        var passenger_tpl = $.parseHTML($('#ticket_detail_tpl').html());
                        var info_block;

                        var todaysDate  = new Date();
                        var date        = $.datepicker.parseDate('yy/mm/dd', (ticket[0].inserted_date).replace(/-/g,'/') );
                        var newdate     = new Date((ticket[0].dept_time).replace(/-/g,'/'));
                        var day         = $.datepicker.formatDate( "dd", date);
                        var dayName     = $.datepicker.formatDate( "D", date);
                        var month       = $.datepicker.formatDate( "M", date);
                        var year        = $.datepicker.formatDate( "YY", date);

                        ticket_info_block.find('.date > .date').html(day);
                        ticket_info_block.find('.date > .month').html(month);
                        ticket_info_block.find('.date > .day').html(dayName);

                        if(newdate > todaysDate && ticket[0].ticket_status == "Y")
                        {
                            ticket_info_block.find('.status').html('UPCOMING');

                            /*var Cancel_btn = $('<button>').attr({ 
                                                'class':'continueBtn cancel_ticket_btn',
                                                // 'data-ticket-id': ticket[0].ticket_id,
                                                'data-ref' :'',
                                                // 'data-pnr' : ticket[0].pnr_no,
                                                'data-opname' : ticket[0].op_name,
                                                'data-pro' : ticket[0].provider_id,
                                                'data-pro-ty' : ticket[0].provider_type,
                                                'data-opi' : ticket[0].op_id,
                                                'data-pc' : ticket[0].partial_cancellation,
                                                'data-t' : ticket[0].ticket_id,
                                                'data-pn' : ticket[0].pnr_no,
                                                'data-ei' : ticket[0].user_email_id,                                                
                                            }).html('Cancel Ticket');
                            $(passenger_tpl).find('tfoot td').addClass('text-right').append(Cancel_btn);*/

                        }
                        else if(ticket[0].ticket_status == "N")
                        {
                            ticket_info_block.find('.status').html('CANCELLED');
                            $(ticket_tpl).addClass("cancelled");

                            info_block  = $.parseHTML($('#info-tpl').html());

                            $(info_block)
                                .find('.info-container')
                                .html(
                                        $('<ul>').addClass('arrow')
                                                .append( $('<li>').html("Refund amount Rs."+ ticket[0].refund_amt))
                                                .append( $('<li>').html("Cancellation cahrges Rs."+ ticket[0].cancel_charge))
                                    );
                            $(passenger_tpl).find('tfoot td').append($(info_block));
                        }
                        else
                        {
                            ticket_info_block.find('.status').html('TRAVELLED');
                        }

                        var from_stop_name = (ticket[0].boarding_stop_name != "" && ticket[0].boarding_stop_name != undefined) ? ticket[0].boarding_stop_name : ticket[0].from_stop_name;
                        var till_stop_name = (ticket[0].destination_stop_name != "" && ticket[0].destination_stop_name != undefined) ? ticket[0].destination_stop_name : ticket[0].till_stop_name;
                        
                        ticket_info_block.find('.from').html(from_stop_name);
                        ticket_info_block.find('.to').html(till_stop_name);

                        ticket_info_block.find('.pnr_no').html(ticket[0].pnr_no);
                        ticket_info_block.find('.ticket_bos_ref_no').html(ticket[0].boss_ref_no);
                        /*ticket_info_block.find('.full_date').html(ticket[0].bus_dept_time);
                        ticket_info_block.find('.booked_on').html(ticket[0].inserted_date);
                        ticket_info_block.find('.total_fare').html("Rs. "+ticket[0].tot_fare_amt_with_tax);*/
                        ticket_info_block.find('.op_bus_service').html(ticket[0].op_name);

                        
                        $.each(ticket, function(key,value){
                            var seat_fare_charges = parseInt(value.seat_fare);

                            if(value.convey_type != null && value.convey_type.trim() == "fix")
                            {
                                seat_fare_charges = parseInt(value.seat_fare) + parseInt(value.convey_value);
                            }

                            var adult_child = (value["psgr_age"] > 11) ? "Adult" : "Child";
                            var tr = $('<tr/>');
                            tr.append($('<td/>').html(value.psgr_name));
                            tr.append($('<td/>').html(value.psgr_age));
                            tr.append($('<td/>').html(value.psgr_sex));
                            tr.append($('<td/>').html(adult_child));
                            tr.append($('<td/>').html(value.seat_no));
                            tr.append($('<td/>').html(" Rs. "+seat_fare_charges));
                            if(value.ticket_detail_status == 'N')
                            {
                                 tr.append($('<td/>').html("Cancelled"));
                            }
                            else if (value.ticket_detail_status == 'Y')
                            {
                                tr.append($('<td/>').html(""));
                            }

                            $(passenger_tpl).find('tbody').append(tr);

                        });

                        if(ticket[0].discount_value != "" && ticket[0].discount_value != undefined && ticket[0].discount_value > 0)
                        {
                            var discount_tr = "";
                            discount_tr += "<tr class='theme-blue-color'>";
                            discount_tr += "<td colspan='5'><b>Total Discount</b></td>";
                            discount_tr += "<td> Rs. "+ticket[0].discount_value+"</td>";
                            discount_tr += "</tr>";
                            $(passenger_tpl).find('tbody').append(discount_tr);
                        }

                        $(ticket_tpl).append($(passenger_tpl));

                        $(".booking-history").append($(ticket_tpl));

                   });
                }
                else
                {
                    $(".booking-history").append($('#ticket_tpl_error').html());
                }

            }, '', false);
	    }

	    show_history();

        $(document).off('click', '.ticket_info').on('click', '.ticket_info', function(e)
        {
            $(this).parent().find('.passenger_info').slideToggle('medium');
        });

	});
</script>

<!--  Template script for booking tab   -->
<script type="text/template" id="ticket_tpl">
    <div class="booking-info clearfix">
        <div class="ticket_info" style="cursor: pointer;">
            <div class="date">
                <label class="month"></label>
                <label class="date"></label>
                <label class="day"></label>
            </div>
            <h4 class="box-title">
                <i class="icon fa fa-bus yellow-color circle"></i>
                <span class="from"></span> to <span class="to"></span> 
                <small class='op_bus_service'></small>
            </h4>
            <dl class="info">
                <dt>PNR No</dt>
                <dd class="pnr_no"></dd>
                <dt>Rokad Ref. #</dt>
                <dd class="ticket_bos_ref_no"></dd>
            </dl>
            <button class="btn-mini status">UPCOMMING</button>
            <div class="clearfix"></div>
        </div>
    </div>
</script>

<script type="text/template" id="ticket_tpl_error">
    <div class="info-box block">
        <p>Ticket not booked yet. Start booking your ticket.</p>
    </div>
</script>

<script type="text/template" id="ticket_detail_tpl">
    <div class="passenger_info clearfix" style="display: none;">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Age</th>
                    <th>Gender</th>
                    <th>Adult / Child</th>
                    <th>Seat No</th>
                    <th>Fare</th>
                </tr>
            </thead>
            <tbody>  

            </tbody>
            <tfoot>
                <tr>
                    <td class="text-left" colspan="6">

                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
</script>