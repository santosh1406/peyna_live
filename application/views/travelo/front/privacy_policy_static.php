<!DOCTYPE html>
    <html> <!--<![endif]-->
    <head>
        <!-- Page Title --> 
        <title><?php echo (isset($metadata_title)) ? $metadata_title : $this->config->item('title', 'home_metadata'); ?></title>
        
        <!-- Meta Tags -->
        <meta charset="utf-8">
        <meta name="keywords" content="<?php echo (isset($metadata_keywords)) ? $metadata_keywords : $this->config->item('keywords', 'home_metadata'); ?>">
        <meta name="description" content="<?php echo (isset($metadata_description)) ? $metadata_description : $this->config->item('description', 'home_metadata'); ?>" />
        <meta name="author" content="Book On Spot">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <!-- <link href="<?php /*echo base_url().COMMON_ASSETS*/ ?>css/jquery-ui.css" rel="stylesheet"> -->
        <!--<link href="<?php /*echo base_url().COMMON_ASSETS*/;?>plugins/jquery-ui-custom/jquery-ui.min.css" rel="stylesheet">-->
        <link href="<?php echo base_url().COMMON_ASSETS;?>plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
        
        <!-- Theme Styles -->
        <link rel="stylesheet" href="<?php echo base_url().FRONT_ASSETS;?>css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url().FRONT_ASSETS;?>css/font-awesome.min.css">
        
        <!-- download font later -->
        <link href='//fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <!-- <link rel="stylesheet" href="<?php /*echo base_url().COMMON_ASSETS;*/?>css/googlefonts/googlefonts.css"> -->
        <link rel="stylesheet" href="<?php echo base_url().FRONT_ASSETS;?>css/animate.min.css">
        <link rel="icon" href="<?php echo base_url().FRONT_ASSETS;?>images/icon/favicon.ico?token=04022016" type="image/x-icon">
        
        <!-- Current Page Styles -->
        <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url().FRONT_ASSETS;?>components/revolution_slider/css/settings-min.css" media="screen" />-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().FRONT_ASSETS;?>components/revolution_slider/css/style.css" media="screen" />
        
        <!-- Main Style -->
        <link id="main-style" rel="stylesheet" type="text/css"  href="<?php echo base_url().FRONT_ASSETS;?>css/style-min.css?token=280120160411">
        
        <!-- Custom Styles -->
        <link rel="stylesheet" type="text/css"  href="<?php echo base_url().FRONT_ASSETS;?>css/custom.min.css?token=20062016"> 
        <!-- <link rel="stylesheet" type="text/css"  href="<?php echo base_url().FRONT_ASSETS;?>css/red_logo_custom.css">-->
        
        <!-- Custom Styles Red Log-->
        <!-- <link rel="stylesheet" type="text/css"  href="<?php echo base_url().FRONT_ASSETS;?>css/red_logo_custom.css?token=bookonspot"> -->

        <!-- Updated Styles -->
        <!-- <link rel="stylesheet" type="text/css"  href="<?php echo base_url().FRONT_ASSETS;?>css/updates.css"> -->

        <!-- Responsive Styles -->
        <link rel="stylesheet" href="<?php echo base_url().FRONT_ASSETS;?>css/responsive.css">
       
        <script type="text/javascript">
            var base_url = "<?php echo base_url(); ?>";
            var front_assets = base_url+"<?php echo FRONT_ASSETS; ?>";
            var BASE_URL = base_url;
        </script>
        <script type="text/javascript" src="<?php echo base_url().FRONT_ASSETS;?>js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url().COMMON_ASSETS; ?>js/jquery.validate.min.js"></script>
        <!--
        <script type="text/javascript" src="<?php echo base_url().COMMON_ASSETS; ?>js/global.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url().COMMON_ASSETS; ?>js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url() . COMMON_ASSETS; ?>js/additional-methods.min.js" type="text/javascript"></script>
        -->
        <!-- Bootstrap Growl Start -->
        <!--<script src="<?php echo base_url() . COMMON_ASSETS ?>plugins/bootstrap-growl/jquery.bootstrap-growl.js"></script>-->
        <!-- Bootstrap Growl End -->

        <!-- Jquery chosen select Start -->
        <!--<script src="<?php /*echo base_url() . COMMON_ASSETS*/ ?>plugins/chosen/chosen.jquery.min.js"></script>-->

        <!-- Sufi Loader -->
        <!--<script type="text/javascript" src="<?php /*echo base_url() . COMMON_ASSETS*/ ?>plugins/sufiloader/sufiloader.js"></script>-->
        <!--
        <script>
        $(document).ready(function() {
            $(".chosen_select").chosen({width: '100%',no_results_text: "Oops, nothing found!"});
        });
        </script>
        -->
        <!-- Jquery chosen select End -->

        <!-- Jquery Cookie Start -->
        <!--<script type="text/javascript" src="<?php /*echo base_url().COMMON_ASSETS;*/ ?>plugins/cookies/jquery.cookie.js"></script>-->
        <!-- Jquery Cookie End -->

        <!-- CSS for IE -->
        <!--[if lte IE 9]>
            <link rel="stylesheet" type="text/css" href="css/ie.css" />
        <![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
          <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
        <![endif]-->

        <!-- Facebook Pixel Code -->
       <!--  <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');

            fbq('init', "<?php echo $this->config->item('facbook_pixel_id');?>");
            fbq('track', "PageView");

            //This is dynamic
            <?php
                if(isset($fb_event_code) && $fb_event_code != "" && $fb_event_code != "Purchase")
                {
            ?>
                    fbq('track', "<?php echo $fb_event_code;?>");
            <?php
                }
                else if(isset($fb_event_code) && $fb_event_code == "Purchase" && isset($fb_event_value) && $fb_event_value != "")
                {
            ?>
                    fbq('track', 'Purchase', {value: "<?php echo $fb_event_value;?>", currency: 'INR'});
            <?php
                }
                else
                {
            ?>
                    fbq('track', 'ViewContent');
            <?php
                }
            ?>
            
        </script> -->
        <noscript>
            <img height="1" width="1" alt="facebook" style="display:none" src="https://www.facebook.com/tr?id=<?php echo $this->config->item('facbook_pixel_id');?>&ev=PageView&noscript=1" />
        </noscript>
         <style type="text/css">
            section#content{
                padding-top: 0px !important;
            }
        </style>
        <!-- End Facebook Pixel Code -->
    </head>
    <body class="body-txt">
    <div id="page-wrapper">

        <!--- Header start -->
      
<!--        <div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Privacy Policy</h2>
        </div>
    </div>
</div>-->


<section id="content">
    <div class="container">
        <div id="main" class="travelo-box">
            <div class="style8 large-block box-txt">
                <!-- <ul class="image-block column-3 pull-left clearfix">
                    <li><a class="middle-block middle-block-auto-height" href="#" style="position: relative; height: 80px;"><img height="88" width="124" alt="" src="<?php echo base_url().FRONT_ASSETS ?>images/shortcodes/image/style01/1.png" class="middle-item" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -62px;"></a></li>
                    <li><a class="middle-block middle-block-auto-height" href="#" style="position: relative; height: 80px;"><img height="88" width="124" alt="" src="<?php echo base_url().FRONT_ASSETS ?>images/shortcodes/image/style01/2.png" class="middle-item" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -62px;"></a></li>
                    <li><a class="middle-block middle-block-auto-height" href="#" style="position: relative; height: 80px;"><img height="88" width="124" alt="" src="<?php echo base_url().FRONT_ASSETS ?>images/shortcodes/image/style01/3.png" class="middle-item" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -62px;"></a></li>
                    <li><a class="middle-block middle-block-auto-height" href="#" style="position: relative; height: 80px;"><img height="102" width="142" alt="" src="<?php echo base_url().FRONT_ASSETS ?>images/shortcodes/image/style01/4.png" class="middle-item" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -71px;"></a></li>
                    <li><a class="middle-block middle-block-auto-height" href="#" style="position: relative; height: 80px;"><img height="98" width="136" alt="" src="<?php echo base_url().FRONT_ASSETS ?>images/shortcodes/image/style01/5.png" class="middle-item" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -68px;"></a></li>
                    <li><a class="middle-block middle-block-auto-height" href="#" style="position: relative; height: 80px;"><img height="90" width="126" alt="" src="<?php echo base_url().FRONT_ASSETS ?>images/shortcodes/image/style01/6.png" class="middle-item" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -63px;"></a></li>
                    <li><a class="middle-block middle-block-auto-height" href="#" style="position: relative; height: 80px;"><img height="91" width="236" alt="" src="<?php echo base_url().FRONT_ASSETS ?>images/shortcodes/image/style01/7.png" class="middle-item" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -118px;"></a></li>
                    <li><a class="middle-block middle-block-auto-height" href="#" style="position: relative; height: 80px;"><img height="81" width="212" alt="" src="<?php echo base_url().FRONT_ASSETS ?>images/shortcodes/image/style01/8.png" class="middle-item" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -106px;"></a></li>
                    <li><a class="middle-block middle-block-auto-height" href="#" style="position: relative; height: 80px;"><img height="81" width="210" alt="" src="<?php echo base_url().FRONT_ASSETS ?>images/shortcodes/image/style01/9.png" class="middle-item" style="position: absolute; top: 50%; margin-top: -40px; left: 50%; margin-left: -105px;"></a></li>
                </ul> -->

                <p>Bookonspot is a reputed travel portal of India and we upmost respect the privacy of our clients. Our 'Privacy Policy' is intended to supply the users a trust associated with the usage and protection of their personal information collected by us. This personal information is comprised of their names, mailing addresses, contact numbers, e-mail IDs, credit card numbers, and several other personal details. Being a trusted name of travel and tourism industry, Bookonspot does not indulge into revealing the personal information of its clients for any reason.</p>
                <p>In general, you can visit our website without providing your personal information like mobile no or Email-id. Instead of tracking individual members interpret; we enhance the performance of our each page. In this way, we provide better services to them without knowing their identity.</p>
                <p>In some special cases, we share the personal information of our clients with other companies/parties/people. These cases may include:</p>
                <ul class="discover triangle row flh">
                    <li class="col-xs-12">We are completely allowed to share their information.</li>
                    <li class="col-xs-12">To make the products & services available what they demand.</li>
                    <li class="col-xs-12">On the demand of any government agency/authority or officer.</li>
                    <li class="col-xs-12">To follow the legal notices.</li>                    
                </ul><br>
                <p>The privacy policy of Bookonspot may vary in various specific cases; it depends on the requirements and situation. However, we make sure that the privacy of our clients do not disclose except some special circumstances.</p>
                <p>Bookonspot takes regular initiatives to protect the information of its users. Using modern technology, several security measures and strict policy guidelines, we protect your privacy and prevent from its unauthorized access or any discloser.</p>

                <div class="clearfix"></div>

        </div> <!-- end main -->
    </div>
</section>