<link rel="stylesheet" type="text/css" href="<?php echo base_url().FRONT_ASSETS;?>components/flexslider/flexslider.css" media="screen" />
        
<div class="page-title-container">
	<div class="container">
		<div class="page-title pull-left">
			<h2 class="entry-title">Testimonials</h2>
		</div>
	</div>
</div>

<section id="content">
	<div class="container">
		<div id="main" class="travelo-box">
            <!-- <h1 class="text-center white-color">Testimonial Style 03</h1> -->
		    <div class="testimonial style3 testimonial-block">
		        <ul class="slides ">
		        	<li>
		            <!-- <div class="author"><a href="#"><img src="http://placehold.it/270x270" alt="" width="74" height="74" /></a></div> -->
		            <blockquote class="description testimonial-txt">Brilliant work! The website is stunning. I have booked a ticket from your website and believe me it’s a flawless experience for me.</blockquote>
		            <!-- <h2 class="name">Lisa Kimberly</h2> -->
		            </li>
		            <li>
		            <!-- <div class="author"><a href="#"><img src="http://placehold.it/270x270" alt="" width="74" height="74" /></a></div> -->
		            <blockquote class="description testimonial-txt">Nice initiative. I think this is the only website through which we can book state transport buses online. Thanks for the totally hassle free experience.</blockquote>
		            <!-- <h2 class="name">Lisa Kimberly</h2> -->
		            </li>
		            <li>
		            <!-- <div class="author"><a href="#"><img src="http://placehold.it/270x270" alt="" width="74" height="74" /></a></div> -->
		            <blockquote class="description testimonial-txt">Booking state transport bus ticket was never so easy, especially in peak season. You have done a good job by making it online.</blockquote>
		            <!-- <h2 class="name">Jessica Brown</h2> -->
		            </li>
		        </ul>
		    </div>
		</div>
	</div>
</section>
<script type="text/javascript" src="<?php echo base_url().FRONT_ASSETS;?>components/flexslider/jquery.flexslider.js"></script> 