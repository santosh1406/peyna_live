<?php
    $arrayname = "onwardJourney";

    // $grand_total = 0;
    if(isset($returnJourney) && count($returnJourney) > 0)
    {
        if(isset($returnJourney["block_time_minutes"]))
        {
            $arrayname = (strtotime($returnJourney["block_time_minutes"]) > strtotime($onwardJourney["block_time_minutes"])) ? "onwardJourney" : "returnJourney";
        }

        // $grand_total = $onwardJourney["total_fare"] + $returnJourney["total_fare"];
    }
    // else
    // {
    //     $grand_total = $onwardJourney["total_fare"];
    // }

    $main_array = $$arrayname;
    $ttb_year = $main_array["ttb_year"];
    $ttb_month = $main_array["ttb_month"];
    $ttb_day = $main_array["ttb_day"];
    $ttb_hours = $main_array["ttb_hours"];
    $ttb_minutes = $main_array["ttb_minutes"];
    $ttb_seconds = $main_array["ttb_seconds"];
 
?>
<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Booking Payment</h2>
        </div>
    </div>
</div>
<section id="content" class="gray-area">
    <div class="container shortcode">
        <div class="travelo-box" id="main">
            <div class="row">
                <div class="col-sm-12 col-md-12 white-bg booking_payment_pg">
                    <?php
                        if(isset($onwardJourney["time_to_block"]) && $onwardJourney["time_to_block"] != "")
                        {
                            echo '<div class="alert alert-notice payment-refresh-notice bg_blue">';
                            echo 'Please do not refresh page.';
                            echo '<span class="pull-right">Please complete your booking within next  <b><span id="remaining_time" class="red">  </span></b> minutes.</span>';
                            echo '</div>';
                        }

                        if ($onwardJourney["status"] == "FAILURE")
                        {
                            echo '<div class="alert alert-error bg_red"> Transaction failure <b>' . $onwardJourney["error"] . ' </b>  <span class="pull-right"><a href="' . base_url() . 'home/" /> Home </a></span> </div>';
                        } 
                        else
                        {
                    ?>
                    <div class="col-sm-8 col-md-8 mt10 payment-tab-div">
                        <div class="block">
                            <div class="tab-container full-width-style">
                                <ul class="tabs">
                                    <li class="active"><a href="#cca-tab" data-toggle="tab"><i class="fa fa-credit-card circle"></i>Net Banking/<br/>ATM/<br/>CC Card</a></li>
                                    
                                    <?php
                                        /*WALLET/FSS IS DIABLED HERE.
                                        *WHEN WALLET/FSS ALLOWED. REMOVE BELOW IF CONDITION
                                        */
                                        if(0)
                                        {
                                            if($this->session->userdata("user_id") > 0)
                                            {
                                               echo '<li><a href="#fss-tab" data-toggle="tab"><i class="fa fa-credit-card circle"></i>Debit + Pin</a></li>';
                                               echo '<li><a href="#wallet-tab" data-toggle="tab"><i class="fa fa-google-wallet circle"></i>Wallet</a></li>'; 
                                            }
                                        }
                                    ?>
                                    
                                </ul>
                                <div class="tab-content">
                                    <?php
                                    /*FSS IS DIABLED HERE.
                                    *WHEN FSS ALLOWED. REMOVE BELOW IF CONDITION
                                    */
                                    if(0)
                                    {
                                    ?>
                                    <div class="tab-pane fade in" id="fss-tab">
                                        <h2 class="tab-content-title">Amount Payable : Rs.<?php echo $total_fare; ?></h2>
                                        <?php
                                            $attributes = array('class' => 'temp_booking', 'id' => 'temp_booking', 'method' => 'post');
                                            echo form_open(base_url().'front/booking/do_confirm_booking/' . $onwardJourney["block_key"], $attributes);
                                            echo '<label for="payment_gateway"><input type="hidden" id="payment_gateway" name="payment_type" value="payment_gateway"/> </label>';
                                            echo '<input type="hidden" id="type_of_payment" name="type_of_payment" value="fss"/>';
                                            if(isset($returnJourney["block_key"]) && $returnJourney["block_key"] != "")
                                            {
                                                echo '<input type="hidden" name="block_key_return" value="'.$returnJourney["block_key"].'" />';
                                            }
                                            echo '<button class="btn-large '.THEME_BTN.'" type="submit">MAKE PAYMENT</button>';
                                            echo form_close();
                                        ?>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                    <div class="tab-pane fade in active" id="cca-tab">
                                        <h2 class="tab-content-title">Amount Payable : Rs.<?php echo $total_fare; ?></h2>
                                        <?php
                                            $attributes = array('class' => 'temp_booking', 'id' => 'temp_booking', 'method' => 'post');
                                            echo form_open(base_url().'front/booking/do_confirm_booking/' . $onwardJourney["block_key"], $attributes);

                                            if($is_agent_markup_onward || $is_agent_markup_return)
                                            {
                                        ?>
                                        <div class="form-group row" style="margin-top:25px;"></div>
                                        <div class="form-group row">
                                            <div class="col-sm-12 col-md-12">
                                                <span class="agent_markup_label font14">Agent Service Charge: Rs. </span>
                                                <input type="text" name="agent_markup" id="agent_markup" value="" class="input-text" placeholder="Amount" required />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12 col-md-12">
                                                <span class="red"><b>Note: </b>Agent markup value is not applicable to any state transport.</span>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <div class="form-group row mt148"></div>
                                        <div class="form-group row">
                                            <div class="col-sm-12 col-md-12">
                                              <input type="checkbox" name="terms_and_conditions" id="terms_and_conditions" value="1" class="cursor-pointer" required>
                                              <span class="ml5 font13">By Clicking on Make Payment, you agree to all the <a onclick="window.open(this.href, 'Terms & Conditions','left=20,top=20,width=1000,height=500,toolbar=1,scrollbars=yes,resizable=0'); return false;" href="<?php echo base_url() ?>terms_and_condition" class="active">Terms and Conditions </a></span>
                                              <!-- <span class="ml5 font13">By Clicking on Make Payment, you agree to all the Terms and Conditions</span> -->
                                            </div>
                                        </div>
                                        <span class="error-befor-button error-message-format"></span>
                                        <?php
                                            echo '<label for="payment_gateway"><input type="hidden" id="payment_gateway" name="payment_type" value="payment_gateway"/> </label>';
                                            echo '<input type="hidden" id="type_of_payment" name="type_of_payment" value="cca"/>';
                                            if(isset($returnJourney["block_key"]) && $returnJourney["block_key"] != "")
                                            {
                                                echo '<input type="hidden" name="block_key_return" value="'.$returnJourney["block_key"].'" />';
                                            }
                                            echo '<button class="btn-large '.THEME_BTN.'" type="submit">MAKE PAYMENT</button>';
                                            echo form_close();
                                        ?>
                                    </div>
                                    <?php
                                        /*WALLET IS DIABLED HERE.
                                        *WHEN WALLET ALLOWED. REMOVE BELOW IF CONDITION
                                        */
                                        if(0)
                                        {
                                            if($this->session->userdata("user_id") > 0)
                                            {
                                    ?>
                                                <div class="tab-pane fade" id="wallet-tab">
                                                    <h2 class="tab-content-title">Amount Payable : Rs.<?php echo $total_fare; ?></h2>
                                                    <?php
                                                        if($wallet_balance > $total_fare)
                                                        {
                                                            $attributes = array('class' => 'temp_booking_wallet', 'id' => 'temp_booking_wallet', 'method' => 'post');
                                                            echo form_open(base_url().'front/booking/do_confirm_booking/' . $onwardJourney["block_key"], $attributes);
                                                            echo '<label for="payment_gateway">Wallet Balance: <b>Rs. '.$wallet_balance.'</b></label>';
                                                            echo '<input type="hidden" id="type_of_payment" name="type_of_payment" value="wallet"/>';
                                                            echo '<input type="hidden" id="wallet" name="payment_type" value="wallet"/>';
                                                            if(isset($returnJourney["block_key"]) && $returnJourney["block_key"] != "")
                                                            {
                                                                echo '<input type="hidden" name="block_key_return" value="'.$returnJourney["block_key"].'" />';
                                                            }
                                                            echo '<button class="btn-large '.THEME_BTN.'" type="submit">MAKE PAYMENT</button>';
                                                            echo form_close();
                                                        }
                                                        else
                                                        {
                                                            echo "<p>You do not have enough amount in your wallet to complete the transaction.</p>";
                                                            echo "<p>Please click <a class='green' href='".base_url()."admin/login/get_profile' ><b>here</b></a> to recharge your wallet.</p>";
                                                        }
                                                    ?>
                                                </div>
                                    <?php
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4 mt10">
                        <div class="booking-details travelo-box payment_fare_details">
                            <article class="image-box cruise listing-style1">
                              <h3 class="fare_detail_header">Fare Detail</h3>
                              <h5 class="fare_detail_subheader">Onward Journey</h5>
                                <dl class="other-details">
                                    <dt class="feature">Total Ticket:</dt><dd class="value"><?php echo $onwardJourney["total_seats"];?></dd>
                                    <dt class="feature">Ticket Fare:</dt><dd class="value">Rs. <?php echo $onwardJourney["total_fare_without_discount"];?></dd>
                                    <?php

                                        if(!empty($onwardJourney["tax_charges"]))
                                        {
                                            foreach ($onwardJourney["tax_charges"] as $tkey => $tvalue) 
                                            {   
                                                echo "<dt class='feature'>".ucwords($tvalue->convey_charge_type_name).":</dt><dd class='value'>Rs. ".$tvalue->trans_val."</dd>";
                                            }
                                        }


                                        if(isset($onwardJourney["discount_value"]) && $onwardJourney["discount_value"] != "")
                                        {
                                            echo "<dt class='feature theme-orange-color'><b>Discount</b>:</dt><dd class='value theme-orange-color'>Rs. ".$onwardJourney["discount_value"]."</dd>";
                                        }
                                    ?>
                                    <dt class="total-price">Onward Journey Price</dt><dd class="total-price-value">Rs. <?php echo $onwardJourney["total_fare"];?></dd>
                                </dl>
                              <!-- Return Journey Detail Start Here -->
                              <?php
                                
                                if(isset($returnJourney) && count($returnJourney) > 0 && isset($returnJourney["total_fare"]) && $returnJourney["total_fare"] > 0 && isset($returnJourney["ticket_fare"]) && $returnJourney["ticket_fare"] > 0)
                                {
                              ?>
                                  <br/>
                                  <h5 class="fare_detail_subheader">Return Journey</h5>
                                    <dl class="other-details">
                                        <dt class="feature">Total Ticket:</dt><dd class="value"><?php echo $returnJourney["total_seats"];?></dd>
                                        <dt class="feature">Ticket Fare:</dt><dd class="value">Rs. <?php echo $returnJourney["total_fare_without_discount"];?></dd>
                                        
                                        <?php

                                            if(isset($returnJourney["tax_charges"]) && count($returnJourney["tax_charges"]) > 0)
                                            {
                                                foreach ($returnJourney["tax_charges"] as $tkey => $tvalue) 
                                                {   
                                                    echo "<dt class='feature'>".ucwords($tvalue->convey_charge_type_name).":</dt><dd class='value'>Rs. ".$tvalue->trans_val."</dd>";
                                                }
                                            }


                                            if(isset($returnJourney["discount_value"]) && $returnJourney["discount_value"] != "")
                                            {
                                                echo "<dt class='feature theme-orange-color'><b>Discount</b>:</dt><dd class='value theme-orange-color'>Rs. ".$returnJourney["discount_value"]."</dd>";
                                            }
                                        ?>
                                        <dt class="total-price">Return Journey Price</dt><dd class="total-price-value">Rs. <?php echo $returnJourney["total_fare"];?></dd>
                                    </dl>
                              <?php
                                }
                              ?>
                                <dl class="other-details grand-total">
                                    <dt>Grand Total</dt>
                                    <dd>Rs. <?php echo $total_fare;?></dd>
                                </dl> 
                              <!-- Return Journey Detail End Here -->
                            </article>
                        </div><!--end of booking-details travelo-box -->
                    </div><!--end of col-sm-4 col-md-4 mt10 -->
                    <?php } ?>
                </div>
            </div>
        </div><!-- ID MAin End -->
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function(){

        var cbooking_rules = new Object();
        var cbooking_messages = new Object();

        $.validator.addMethod('minStrict', function (value, el, param) {
            return value <= param;
        });

        cbooking_rules["payment_type"] = {required: true };
        cbooking_messages["payment_type"] = { required : "Something unwanted happend. Please try again." };
        cbooking_rules["terms_and_conditions"] = {required: true};
        cbooking_messages["terms_and_conditions"] = { required : "Please accept terms and condition."};
        
        if("<?php echo $is_agent_markup_onward;?>" == true || "<?php echo  $is_agent_markup_return;?>" == true)
        {
            cbooking_rules["agent_markup"] = {required: true,number: true, minStrict: <?php echo ORS_AGENT_MARKUP_AMOUNT;?>};
            cbooking_messages["agent_markup"] = { required : "Please enter agent markup value.",number:"Please provide numeric value",minStrict:"Agent mark up value must be less than or equal to <?php echo ORS_AGENT_MARKUP_AMOUNT;?>" };
        }

        $('#temp_booking').validate({
            ignore: "",
            rules: cbooking_rules,
            messages: cbooking_messages,
            errorPlacement: function(error, element) {
                error.appendTo(".error-befor-button");
            }
        });

        $("#temp_booking_wallet").validate({
            rules: {
                "payment_type": {
                    required: true
                }
            }
        });
    });

    

    window.onload=function(){
        if("<?php echo $ttb_year;?>" != "")
        {
            startCountDown(<?php echo $ttb_year;?>,<?php echo $ttb_month;?>,<?php echo $ttb_day;?>,<?php echo $ttb_hours;?>,<?php echo $ttb_minutes;?>,<?php echo $ttb_seconds;?>, "remaining_time","timeup", base_url);  
        }
    };
</script>