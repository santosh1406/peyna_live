<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Thank you</h2>
        </div>
    </div>
</div>

<section id="content">
    <div class="container tour-detail-page">
        <div class="row">
            <div class="col-md-12" id="main">

                <div class="travelo-box" id="tour-details">
                    <h1 class="page-title">Thank you for visiting!</h1>
                    <h5>
                        <i class="fa fa-envelope-o green mr10"></i>Check Your Email To Activate Your Account!
                    </h5>

                </div>
            </div>
        </div>
    </div>
</section>
