<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Provide your communication details</h2>
        </div>
    </div>
</div>
<?php
  $ontmdiff = calculate_date_diff($journey_date_time,$available_detail["arrival_time"]);
  $days    = $ontmdiff->format('%d');
  $hours   = $ontmdiff->format('%h');
  $minutes = $ontmdiff->format('%i');

  // $travel_time = (($days > 0) ? $days." day " : "").(($hours > 0) ? $hours." hours " : "").(($minutes > 0) ? $minutes." minutes" : "");
  $travel_time = calculate_travel_duration($ontmdiff);

  /********** Return ticket Detail Start**************************/
  $is_return_journey = false;

  if(isset($available_detail_return) && !empty($available_detail_return))
  {
      $is_return_journey = true;
      $retmdiff = calculate_date_diff($rjourney_date_time,$available_detail_return["arrival_time"]);
      $rdays    = $retmdiff->format('%d');
      $rhours   = $retmdiff->format('%h');
      $rminutes = $retmdiff->format('%i');

      // $rtravel_time = (($rdays > 0) ? $rdays." day " : "").(($rhours > 0) ? $rhours." hours " : "").(($rminutes > 0) ? $rminutes." minutes" : "");
      $rtravel_time = calculate_travel_duration($retmdiff);
  }
  /********** Return ticket Detail End**************************/
?>
<section id="content">
    <div class="container">
        <div class="row">
            <div id="main" class="col-sm-8 col-md-9">
                <div class="travelo-box booking-section bookingdetails-box">
                    <?php 
                        $attributes = array('class' => 'booking-form booking_process', 'id' => 'booking_process', 'method' => 'post');
                        echo form_open(base_url().'front/booking/process_payment_confirm_booking', $attributes); 
                    ?>
                    <div class="col-sm-8 col-md-8">
                        <div class="person-information">
                            <h2>Booking Details</h2>
                            <br/>
                            <h4>Onward Journey Details</h4>
                                <!--<i data-target=".all_details" title="Show Favourite List" data-toggle="modal" class="pull-right soap-icon-entertainment popup_all_details circle" data-trip-no="111" data-op-id="2"></i></h2>--> 
                            <!-- <div class="col-sm-8 col-md-8"> -->
                            <?php
                              $seat_booked_count  = count($seat_booked);
                              if($seat_booked_count)
                              {
                                // for($i=0; $i < count($seat_booked[1]); $i++)
                                foreach ($seat_booked as $berthno => $seatvalue)
                                {
                                  if($seat_booked_count == 2)
                                  {
                                      echo ($berthno > 1) ? "<h6>Upper Berth</h6>" : "<h6>Lower Berth</h6>";
                                  }

                                  foreach ($seatvalue as $i => $value)
                                  {

                                    $disable_male = "";
                                    $female_checked = "";
                                    $male_checked = "checked";
                                    if(isset($available_detail["seats_details"][$berthno][$value]["is_ladies_seat"]) && $available_detail["seats_details"][$berthno][$value]["is_ladies_seat"])
                                    {
                                      $disable_male = "disabled";
                                      $female_checked = "checked";
                                      $male_checked = "";
                                    }
                            ?>
                                    <div class="onward-journey-fav">
                                      <div class="form-group row">
                                          <div class="col-sm-5">
                                              <input type="text" class="input-text full-width username alpha-only" placeholder="Name" name="seat[<?php e($berthno); ?>][<?php e($i); ?>][name]" id="seat[<?php e($berthno); ?>][<?php e($i); ?>][name]" />
                                              <input type="hidden" class="input-text full-width username alpha-only" placeholder="Name" name="seat[<?php e($berthno); ?>][<?php e($i); ?>][seat_no]" id="seat[<?php e($berthno); ?>][<?php e($i); ?>][seat_no]" value="<?php e($value); ?>"/>
                                          </div>
                                          <div class="col-sm-2">
                                              <input type="text" class="input-text full-width age" placeholder="Age" name="seat[<?php e($berthno); ?>][<?php e($i); ?>][age]" maxlength="3"/>
                                          </div>
                                          <div class="col-sm-5 gender disp_radio">
                                              <label class="radio radio-inline male_radio">
                                                  <input type="radio" class="gender" id="seat[<?php e($berthno); ?>][<?php e($i); ?>][gender]" name="seat[<?php e($berthno); ?>][<?php e($i); ?>][gender]" value="M" <?php e($disable_male." ".$male_checked) ;?> >
                                                  Male
                                              </label>
                                              <label class="radio radio-inline female_radio">
                                                  <input type="radio" class="gender" id="seat[<?php e($berthno); ?>][<?php e($i); ?>][gender]" name="seat[<?php e($berthno); ?>][<?php e($i); ?>][gender]" value="F" <?php e($female_checked) ;?>>
                                                  Female
                                              </label>
                                          </div>
                                      </div>
                                    </div>
                                  <?php 
                                    }
                                  }
                                }
                                  
                                  // if(isset($available_detail_return) && count($available_detail_return) > 0)
                                  if($is_rseat_available)
                                  {
                                      $rseat_booked_count = count($rseat_booked);
                                      if(isset($rseat_booked) &&  $rseat_booked_count)
                                      {
                                        echo "<br/>";
                                        echo "<h4>Return Journey Details</h4>";
                                        // for($i=0; $i < count($rseat_booked); $i++)
                                        foreach ($rseat_booked as $rberthno => $rseatvalue)
                                        {

                                          if($rseat_booked_count == 2)
                                          {
                                              echo ($rberthno > 1) ? "<h6>Upper Berth</h6>" : "<h6>Lower Berth</h6>";

                                              // echo "<h6>Berth No : ".$rberthno."</h6>";
                                          }

                                          foreach ($rseatvalue as $ri => $rvalue)
                                          {
                                            $rdisable_male = "";
                                            $rfemale_checked = "";
                                            $rmale_checked = "checked";
                                            if(isset($available_detail_return["seats_details"][$rberthno][$rvalue]["is_ladies_seat"]) && $available_detail_return["seats_details"][$rberthno][$rvalue]["is_ladies_seat"])
                                            {
                                              $rdisable_male = "disabled";
                                              $rfemale_checked = "checked";
                                              $rmale_checked = "";
                                            }
                                  ?>
                                            <div class="return-journey-fav">
                                              <div class="form-group row">
                                                  <div class="col-sm-5">
                                                      <input type="text" class="input-text full-width username alpha-only" placeholder="Name" name="rseat[<?php e($rberthno); ?>][<?php e($ri); ?>][name]" id="seat[<?php e($rberthno); ?>][<?php e($ri); ?>][name]" />
                                                      <input type="hidden" class="input-text full-width username alpha-only" placeholder="Name" name="rseat[<?php e($rberthno); ?>][<?php e($ri); ?>][seat_no]" id="seat[<?php e($rberthno); ?>][<?php e($ri); ?>][seat_no]" value="<?php e($rvalue); ?>"/>
                                                  </div>
                                                  <div class="col-sm-2">
                                                      <input type="text" class="input-text full-width age" placeholder="Age" name="rseat[<?php e($rberthno); ?>][<?php e($ri); ?>][age]" placeholder="Age" maxlength="3"/>
                                                  </div>
                                                  <div class="col-sm-5 gender disp_radio">
                                                      <label class="radio radio-inline male_radio">
                                                          <input type="radio" class="gender" id="rseat[<?php e($rberthno); ?>][<?php e($ri); ?>][gender]" name="rseat[<?php e($rberthno); ?>][<?php e($ri); ?>][gender]" value="M" checked>
                                                          Male
                                                      </label>
                                                      <label class="radio radio-inline female_radio">
                                                          <input type="radio" class="gender" id="rseat[<?php e($rberthno); ?>][<?php e($ri); ?>][gender]" name="rseat[<?php e($rberthno); ?>][<?php e($ri); ?>][gender]" value="F">
                                                          Female
                                                      </label>
                                                  </div>
                                                </div>
                                            </div>
                                    <?php 
                                          }
                                        }
                                      }
                                    }

                                    $current_user_email = "";
                                    $current_user_mobileno = "";
                                    if($this->session->userdata('user_id') != "" && $this->session->userdata('role_name') != "support")
                                    {
                                      $current_user_email = $this->session->userdata('email');
                                      $current_user_mobileno = $this->session->userdata('mobile_no');
                                    }
                                  ?> 
                                  <h4>Personal Details</h4>
                                  <div class="form-group row">
                                      <div class="col-sm-5">
                                          <!-- <label>email address</label> -->
                                          <input type="text" class="input-text full-width" placeholder="Email Address" id="email" name="email_id" value="<?php echo $current_user_email; ?>"/>
                                      </div>
                                      
                                      <div class="col-sm-5">
                                          <!-- <label>Mobile Number</label> -->
                                          <input type="text" class="input-text full-width numbers-only" placeholder="Mobile Number" id="mobile_no" name="mobile_no" maxlength="10" value="<?php echo $current_user_mobileno; ?>"/>
                                      </div>
                                  </div>

                                  <?php
                                  if($available_detail["id_proof_required"])
                                  {
                                  ?>
                                  <h4>Onward Journey Primary Passanger Id Proof</h4>
                                  <div class="form-group row">
                                      <div class="col-sm-5">
                                          <input type="text" class="input-text full-width" placeholder="Id Type" id="id_type" name="id_type" value=""/>
                                      </div>
                                      
                                      <div class="col-sm-5">
                                          <input type="text" class="input-text full-width alpha-only" placeholder="Name On Id" id="name_on_id" name="name_on_id"/>
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <div class="col-sm-5">
                                          <input type="text" class="input-text full-width" placeholder="Id/Card Number" id="id_card_number" name="id_card_number"/>
                                      </div>
                                  </div>
                                  <?php
                                  }

                                  if(isset($available_detail_return["id_proof_required"]) && $available_detail_return["id_proof_required"])
                                  {
                                  ?>
                                  <h4>Return Journey Primary Passanger Id Proof</h4>
                                  <div class="form-group row">
                                      <div class="col-sm-5">
                                          <input type="text" class="input-text full-width" placeholder="Id Type" id="rid_type" name="rid_type" value=""/>
                                      </div>
                                      
                                      <div class="col-sm-5">
                                          <input type="text" class="input-text full-width alpha-only" placeholder="Name On Id" id="rname_on_id" name="rname_on_id"/>
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                      <div class="col-sm-5">
                                          <input type="text" class="input-text full-width" placeholder="Id/Card Number" id="rid_card_number" name="rid_card_number"/>
                                      </div>
                                  </div>
                                  <?php
                                  }
                                  ?>
                                  <span class="error-befor-button error-message-format"></span>
                                  <div class="col-sm-6 row">
                                    <button class="full-width btn-large <?php echo THEME_BTN;?>" type="submit">CONFIRM BOOKING</button>
                                  </div>
                          </div><!-- end person-information -->
                        </div><!-- end of col-sm-8 col-md-8 -->
                        <!-- DETAIL JOURNEY START MODIFICATION -->
                        <!-- Detail Journey Start -->
                        <div class="col-sm-4 col-md-4 detailed-journey booking-detail-journey pad0">
                          <!--<h2>Onward Journey/Favourite List</h2>-->
                            <div class="travelo-box booking-details">
                              <h3>Ticket Details</h3>
                              <div class="tab-container style1">
                                <ul class="tabs full-width">
                                    <li class="active">
                                      <a href="#onward-journey" data-toggle="tab">
                                        <i class="fa fa-bus"></i>
                                      </a>
                                    </li>
                                    <?php
                                      if($is_rseat_available)
                                      {
                                    ?>
                                    <li>
                                      <a href="#return-journey" data-toggle="tab">
                                        <i class="fa fa-bus"></i>
                                      </a>
                                    </li>
                                    <?php
                                      }

                                      if($favourite_list)
                                      {
                                    ?>
                                    <li>
                                      <a href="#favourite-list" data-toggle="tab">
                                        <i class="fa fa-heart"></i>
                                      </a>
                                    </li>
                                    <?php
                                      }
                                    ?>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="onward-journey">
                                      <!-- Onward journey detail start -->
                                      <div class="onwardjourney">
                                        <h5>Onwards Journey</h5>
                                        <article class="bus-booking-details">
                                          <div class="travel-title">
                                            <h5 class="box-title">
                                              <b class='theme-green-color'><?php echo ucwords(strtolower($available_detail["from"])); ?></b> 
                                              to 
                                              <b class='theme-green-color'><?php echo ucwords(strtolower($available_detail["to"])); ?></b>
                                              <small><?php echo $available_detail["op_name"];?></small>
                                            </h5>
                                          </div>
                                          <div class="details">
                                          <div class="constant-column-3 timing clearfix">
                                            <div class="check-in">
                                              <label>Departure</label>
                                              <span class="datime"><?php echo $journey_date;?><br /><?php echo $journey_date_hs;?></span>
                                            </div>
                                            <div class="duration text-center">
                                              <i class="soap-icon-clock"></i>
                                              <span><?php echo $travel_time;?></span>
                                            </div>
                                            <div class="check-out">
                                              <label>Arrival</label>
                                              <span class="datime"><?php echo $arrival_time;?><br /><?php echo $arrival_time_hs;?></span>
                                            </div>
                                          </div>
                                          </div>
                                        </article>
                                        <h6>Other Details</h6>
                                        <dl class="other-details">
                                          <dt class="feature">Seat No(s):</dt>
                                          <dd class="value">
                                            <?php 
                                              
                                              if($seat_booked_count == 2)
                                              {

                                                if(isset($seat_booked[1]))
                                                {
                                                  echo "Lower Berth : ".implode(",", $seat_booked[1])."<br/>";
                                                }

                                                if(isset($seat_booked[2]))
                                                {
                                                  echo "Upper Berth : ".implode(",", $seat_booked[2]);
                                                }
                                                
                                              }
                                              else if($seat_booked_count)
                                              {
                                                if(isset($seat_booked[1]))
                                                {
                                                  echo implode(",", $seat_booked[1]);
                                                }

                                                if(isset($seat_booked[2]))
                                                {
                                                  echo implode(",", $seat_booked[2]);
                                                }
                                              }

                                            ?>
                                          </dd>
                                          <dt class="feature">Bus Type:</dt>
                                          <?php
                                            if(is_array($bus_type) && count($bus_type) > 0)
                                            {
                                              $bus_type_name = "";
                                              $bus_type_name .= (isset($bus_type["op_bus_type_name"])) ? $bus_type["op_bus_type_name"]."  " : "";
                                              $bus_type_name .= (isset($bus_type["bus_category"])) ? $bus_type["bus_category"] : "";
                                              echo '<dd class="value">'.ucwords($bus_type_name).'</dd>';
                                            }
                                            else
                                            {
                                              echo '<dd class="value">NA</dd>';
                                            }
                                          ?>
                                        </dl>
                                      </div>
                                      <!-- Onward journey detail end -->
                                    </div>
                                    <?php
                                      if($is_rseat_available)
                                      {
                                    ?>
                                    <div class="tab-pane fade" id="return-journey">
                                        <!-- Return journey detail start -->
                                        <div class="onwardjourney">
                                          <h5>Return Journey</h5>
                                          <article class="bus-booking-details">
                                            <div class="travel-title">
                                              <h5 class="box-title">
                                                <b class='theme-green-color'><?php echo ucwords(strtolower($available_detail_return["from"])); ?></b> 
                                                to 
                                                <b class='theme-green-color'><?php echo ucwords(strtolower($available_detail_return["to"])); ?></b>
                                                <small><?php echo $available_detail_return["op_name"];?></small>
                                              </h5>
                                            </div>
                                            <div class="details">
                                            <div class="constant-column-3 timing clearfix">
                                              <div class="check-in">
                                                <label>Departure</label>
                                                <span class="datime"><?php echo $rjourney_date;?><br /><?php echo $rjourney_date_hs;?></span>
                                              </div>
                                              <div class="duration text-center">
                                                <i class="soap-icon-clock"></i>
                                                <span><?php echo $rtravel_time;?></span>
                                              </div>
                                              <div class="check-out">
                                                <label>Arrival</label>
                                                <span class="datime"><?php echo $rarrival_time;?><br /><?php echo $rarrival_time_hs;?></span>
                                              </div>
                                            </div>
                                            </div>
                                          </article>
                                          <h6>Other Details</h6>
                                          <dl class="other-details">
                                            <dt class="feature">Seat No(s):</dt>
                                            <dd class="value">
                                              <?php 

                                                if($rseat_booked_count == 2)
                                                {

                                                  if(isset($rseat_booked[1]))
                                                  {
                                                    echo "Lower Berth : ".implode(",", $rseat_booked[1])."<br/>";
                                                  }

                                                  if(isset($rseat_booked[2]))
                                                  {
                                                    echo "Upper Berth : ".implode(",", $rseat_booked[2]);
                                                  }
                                                  
                                                }
                                                else if($rseat_booked_count)
                                                {
                                                  echo implode(",", $rseat_booked[1]);
                                                }

                                              ?>
                                            </dd>
                                            <dt class="feature">Bus Type:</dt>
                                            <?php
                                              if(is_array($rbus_type) && count($rbus_type) > 0)
                                              {
                                                $rbus_type_name = "";
                                                $rbus_type_name .= (isset($rbus_type["op_bus_type_name"])) ? $rbus_type["op_bus_type_name"]."  " : "";
                                                $rbus_type_name .= (isset($rbus_type["bus_category"])) ? $rbus_type["bus_category"] : "";
                                                echo '<dd class="value">'.ucwords($rbus_type_name).'</dd>';
                                              }
                                              else
                                              {
                                                echo '<dd class="value">NA</dd>';
                                              }
                                            ?>
                                          </dl>
                                        </div>
                                        <!-- Return journey detail end -->
                                    </div>
                                    <?php
                                      }

                                    if($favourite_list)
                                    {
                                    ?>
                                    <div class="tab-pane fade favourite-list" id="favourite-list">
                                      <!-- Favourite List Start -->
                                      <div class="toggle-container box" id="accordion1">
                                        <?php
                                          $collapsed = "";
                                          $in = "in";
                                          foreach ($favourite_list as $key => $listdetail)
                                          {
                                        ?>
                                          <div class="panel style1">
                                              <h6 class="panel-title">
                                                  <a class="<?php echo $collapsed; ?>" href="#acc<?php echo $key ;?>" data-toggle="collapse" data-parent="#accordion1"><?php echo ucwords($listdetail[0]["cat_favourite_list"]) ;?></a>
                                              </h6>
                                              <div class="panel-collapse collapse <?php echo $in; ?>" id="acc<?php echo $key ;?>">
                                                  <div class="panel-content">
                                                    <form action="#">
                                                      <?php
                                                        foreach ($listdetail as $lkey => $lvalue)
                                                        {
                                                      ?>
                                                        <div class="checkbox">
                                                          <label>
                                                            <input type="checkbox" name="list[]" data-age="<?php echo $lvalue['age']; ?>" data-gender="<?php echo ucwords($lvalue['gender']); ?>" value="<?php echo ucwords($lvalue['fname']); ?>"> <?php echo ucwords($lvalue['fname']); ?>
                                                          </label>
                                                        </div>
                                                      <?php
                                                        }
                                                      ?>
                                                      <button type="button" class="button btn-small green apply_fav_list <?php echo THEME_BTN;?>">Apply</button>
                                                    </form>
                                                  </div><!-- end content -->
                                              </div>
                                          </div>
                                        <?php
                                          $collapsed = "collapsed";
                                          $in = "";
                                          }
                                        ?>
                                      </div>
                                      <!-- favourite List End -->
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                          </div><!-- booking-details end -->
                        </div>
                        <!-- Detail Journey End -->
                        <!-- DETAIL JOURNEY START MODIFICATION -->
                        <div class="clearfix"></div>
                    <?php echo form_close(); ?>
              </div>
            </div>
            <!-- <div class="sidebar col-sm-4 col-md-3">
              <div class="travelo-box contact-box">
                  <h4>Need a Help?</h4>
                  <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
                  <address class="contact-details">
                      <i class="fa fa-envelope"></i><a href="#" class="contact-email">support@rokad.in</a>
                  </address>
              </div>
            </div> -->
        </div>
    </div>
</section>

 <!-- All Details Start Here -->

<script type="text/javascript">
    $(document).ready(function(){

      $.validator.addMethod("email", function (value, element) {
        
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return filter.test(value);

      }, 'Please enter valid email address');

      var cbooking_rules = new Object();
      var cbooking_messages = new Object();
      var cbooking_group = "";
      $('input[name^=seat]:text').each(function() {
          cbooking_rules[this.name] = { required: true };
          cbooking_messages[this.name] = { required: 'Please enter onward journey passanger details.' };
          cbooking_group += this.name+" ";
      });

      $('input[name^=rseat]:text').each(function() {
          cbooking_rules[this.name] = { required: true };
          cbooking_messages[this.name] = { required: 'Please enter return journey passanger details.' };
          cbooking_group += this.name+" ";
      });

      cbooking_rules["email_id"] = { required: true,email:true };
      cbooking_rules["mobile_no"] = { required: true, maxlength:10,minlength:10 };
      cbooking_messages["email_id"] = { required : "Please enter email address" };
      cbooking_messages["mobile_no"] = { required : "Please enter mobile number",maxlength : "Please provide mobile no with 10 digit",minlength : "Please provide mobile no with 10 digit" };

      $('#booking_process').validate({
        rules: cbooking_rules,
        messages: cbooking_messages,
        groups: {fullname: cbooking_group},
        errorPlacement: function(error, element) {
            error.appendTo(".error-befor-button");
        }
      });

      /*$.validator.addClassRules({
          username: {
            required:true,
          },
          age: {
            required:true,
            number: true,
            max : 150,
          },
          gender: {
            required:true
          }
      });

     $('#booking_process').validate({
        rules: {
          email_id: {
            required: true,
            email: true,
          },
          mobile_no : {
            required: true,
            number: true,
            minlength: 10,
            maxlength: 10

          }
        },
        groups: {
            usernameGroup: "username age gender",     
        },
        messages: {
          email_id : "Please enter Email Id",
          mobile_no: "Please enter mobile no",
          username : "Please enter name",
          age      : "Please enter age",
          gender   : "Please select gender"
        },
        errorPlacement: function(error, element) {
           error.appendTo(".error-befor-button");
       }
      });*/

     /* fav list Start Here */
     $(document).off('click', '.apply_fav_list').on('click', '.apply_fav_list', function(e) {
        var $list = $(this).closest('form').find('input:checkbox');
        var count = 0;

        $("#booking_process .onward-journey-fav .row").find("input:text").val("");
        $("#booking_process .onward-journey-fav .row").find(".gender[value='M']").attr("checked","checked");
        $("#booking_process .return-journey-fav .row").find("input:text").val("");
        $("#booking_process .return-journey-fav .row").find(".gender[value='M']").attr("checked","checked");

        $list.each(function(index, value) {
          if($(this).prop("checked"))
          {
            $("#booking_process .onward-journey-fav .row").eq(count).find("input.username").val($(this).val().trim());
            $("#booking_process .onward-journey-fav .row").eq(count).find("input.age").val($(this).attr("data-age"));
            $("#booking_process .onward-journey-fav .row").eq(count).find(".gender[value='"+$(this).attr("data-gender")+"']").attr("checked","checked");


            $("#booking_process .return-journey-fav .row").eq(count).find("input.username").val($(this).val().trim());
            $("#booking_process .return-journey-fav .row").eq(count).find("input.age").val($(this).attr("data-age"));
            $("#booking_process .return-journey-fav .row").eq(count).find(".gender[value='"+$(this).attr("data-gender")+"']").attr("checked","checked");
            count++;
          }
        });

        //for radio reload IMP
        travelo_radio();
     });
     /* fav list End Here */

}); // end of document ready
</script>
