    <div id="slideshow">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
				  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				  <li data-target="#myCarousel" data-slide-to="1"></li>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner">
					<div class="item active">
						<img src="<?php echo base_url().FRONT_ASSETS;?>images/bg2.jpg" alt=""/>
						
					</div>

					<div class="item">
						<img src="<?php echo base_url().FRONT_ASSETS;?>images/bg1.jpg" alt=""/>
						
					</div>
				</div>
            </div>
    </div>
	<section id="content" class="servicewrap">
        <div class="container section bookrokad">
            <div class="row image-box style10">
                   <div class="col-sms-6 col-sm-6 col-md-3">
                        <article class="box hover-effect billpayment">
                            <figure class="animated" data-animation-type="fadeInDown" data-animation-duration="2">
                                <a href="#" title="" class=""><img src="<?php echo base_url().FRONT_ASSETS;?>images/icon/offer1.png" alt=""  /></a>
                            </figure>
                            <div class="details bookrokaddetails">
                                <a href="#" class="button btn-mini">Tickets</a>
                               
                            </div>
                        </article>
                    </div>
                         <div class="col-sms-6 col-sm-6 col-md-3 ">
                        <article class="box hover-effect">
                            <figure class="animated" data-animation-type="fadeInDown" data-animation-duration="2">
                                <a href="#" title="" class=""><img src="<?php echo base_url().FRONT_ASSETS;?>images/icon/offer2.png" alt=""  /></a>
                            </figure>
                            <div class="details bookrokaddetails">
                                <a href="#" class="button btn-mini">Wallet</a>
                               
                            </div>
                        </article>
                    </div>
                     <div class="col-sms-6 col-sm-6 col-md-3">
                        <article class="box hover-effect">
                            <figure class="animated" data-animation-type="fadeInDown" data-animation-duration="2">
                                <a href="#" title="" class=""><img src="<?php echo base_url().FRONT_ASSETS;?>images/icon/offer3.png" alt=""  /></a>
                            </figure>
                            <div class="details bookrokaddetails">
                                <a href="#" class="button btn-mini">Recharges</a>
                              
                            </div>
                        </article>
                    </div>
                    <div class="col-sms-6 col-sm-6 col-md-3">
                        <article class="box hover-effect">
                            <figure class="animated" data-animation-type="fadeInDown" data-animation-duration="2">
                                <a href="#" title="" class=""><img src="<?php echo base_url().FRONT_ASSETS;?>images/icon/offer4.png" alt=""  /></a>
                            </figure>
                            <div class="details bookrokaddetails">
                                <a href="#" class="button btn-mini">Bill Payments</a>
                                
                            </div>
                        </article>
                    </div>
                   
               
                    
                    
            </div>
        </div>
            

        <div class="global-map-area section countersection" data-stellar-background-ratio="0.5">
			<div class="container description">
				<div class="col-sm-6 col-md-4 counterbox1">
					<div class="icon-box style6 animated" data-animation-type="slideInLeft" data-animation-delay="0">
						<img src="<?php echo base_url().FRONT_ASSETS;?>images/icon/counter1.png" alt=""/>
						<div class="description">
							<h4 class="box-title">1,00,000 <sup>+</sup></h4>
							<p>Merchants</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 counterbox1">
					<div class="icon-box style6 animated" data-animation-type="slideInDown" data-animation-delay="0.6">
					   <img src="<?php echo base_url().FRONT_ASSETS;?>images/icon/counter2.png" alt=""/>
						<div class="description">
							<h4 class="box-title">300 <sup>+</sup></h4>
							<p>Million Consumers</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 counterbox1">
					<div class="icon-box style6 animated" data-animation-type="slideInDown" data-animation-delay="0.9">
						<img src="<?php echo base_url().FRONT_ASSETS;?>images/icon/counter3.png" alt=""/>
						<div class="description">
							<h4 class="box-title">100 <sup>+</sup></h4>
							<p>New Users</p>
						</div>
					</div>
				</div>
				
			</div>
        </div>

            

		<div class="global-map-area section logoswrap" data-stellar-background-ratio="0.5">
			<div class="container">
			   
				<div class="image-carousel style3 flex-slider" data-item-width="170" data-item-margin="30">
					<ul class="slides image-box style9">
						<li class="">
							<article class="box">
								<figure>
									<a href="#" title="" class=""><img src="<?php echo base_url().FRONT_ASSETS;?>images/logo/logo1.jpg" alt="" width="170" height="160" /></a>
								</figure>
								<div class="details"><p>kadamba transport</p></div>
							</article>
						</li>
						 <li class="">
							<article class="box">
								<figure>
									<a href="#" title="" class=""><img src="<?php echo base_url().FRONT_ASSETS;?>images/logo/logo2.jpg" alt="" width="170" height="160" /></a>
								</figure>
								<div class="details"><p>upsrtc</p></div>
							</article>
						</li>
						 <li class="">
							<article class="box">
								<figure>
									<a href="#" title="" class=""><img src="<?php echo base_url().FRONT_ASSETS;?>images/logo/logo3.jpg" alt="" width="170" height="160" /></a>
								</figure>
								<div class="details"><p>hrtc</p></div>
							</article>
						</li>
						 <li class="">
							<article class="box">
								<figure>
									<a href="#" title="" class=""><img src="<?php echo base_url().FRONT_ASSETS;?>images/logo/logo4.jpg" alt="" width="140" height="160" /></a>
								</figure>
								<div class="details"><p>msrtc </p></div>
							</article>
						</li>
						 <li class="">
							<article class="box">
								<figure>
									<a href="#" title="" class=""><img src="<?php echo base_url().FRONT_ASSETS;?>images/logo/logo5.gif" alt="" width="140" height="120" /></a>
								</figure>
								<div class="details"><p>rsrtc </p></div>
							</article>
						</li>
						 <li class="">
							<article class="box">
								<figure>
									<a href="#" title="" class=""><img src="<?php echo base_url().FRONT_ASSETS;?>images/logo/logo1.jpg" alt="" width="140" height="120" /></a>
								</figure>
								<div class="details"><p>kadamba transport</p></div>
							</article>
						</li>
						 <li class="">
							<article class="box">
								<figure>
									<a href="#" title="" class=""><img src="<?php echo base_url().FRONT_ASSETS;?>images/logo/logo2.jpg" alt="" width="140" height="120" /></a>
								</figure>
								<div class="details"><p>upsrtc</p></div>
							</article>
						</li>
						 <li class="">
							<article class="box">
								<figure>
									<a href="#" title="" class=""><img src="<?php echo base_url().FRONT_ASSETS;?>images/logo/logo3.jpg" alt="" width="170" height="160" /></a>
								</figure>
								<div class="details"><p>hrtc</p></div>
							</article>
						</li>
					</ul>
				</div>
			</div>
		</div>
    </section>