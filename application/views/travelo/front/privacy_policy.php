


<section id="content" class="abouttop">
<div class="termsimg"><img src="<?php echo base_url();?>assets/travelo/front/images/privacy.jpg" >
<h2>Policies </h2>
</div>
    <div class="container">
        <div id="main" class="travelo-box">
            <div class="privacypolices">
<p class="pdata">This Privacy Policy provides you with details about the manner in which your data is collected & used by us. You are advised to read this Privacy Policy carefully. By visiting Rokad website/ Mobile Applications you expressly give us consent to use and disclose your personal information in accordance with this Privacy Policy.

Our privacy policy may change at any time without prior notification. To make sure that you are aware of any changes, kindly review the policy periodically. This Privacy Policy shall apply uniformly to Rokad desktop website and Rokad Mobile Applications.
</p>
<h3>What information does the Rokad Website / Rokad Mobile Application obtain and how is it used?</h3>
<ul>
<li>User Provided Information
<p>The information you provide when you access Rokad Website or Rokad Mobile Application while registration. Registration with us is not mandatory, it is suggested that you register with your details to avail all the services offered Rokad. </p>
<p>During registration, you are asked to provide:</p>
<ul class="subprivacy">
<li>your name, email address, age, username, password and mobile number. </li>
<li>information you provide us when you contact us for help.</li>

</ul>
<p>We may also use the information you provided us to contact you from time to time to provide you service requests related information, update notices, marketing promotions, etc. </p>
</li>


<li>Automatically Collected Information
<p>In addition, this Rokad may collect certain information automatically of your mobile device, including, but not limited to, the IMEI Number and the UDID Number (for iPhones). </p>
</li>

<li>Does the Rokad Mobile Application collect precise real-time location information?
<p>We may use your mobile device's in-built GPS system to determine your current real-time location in order to determine the city you are located in to offer location specific services. We will not share your current location with other users or partners.</p>
<p>If you do not want us to use your location for the purposes set forth above, you should turn off the location services for the mobile application located in your account settings or in your mobile phone settings and/or within the mobile application. However, by doing this you will not be able to access all the features offered by the Rokad Mobile Application.</p>
</li>

<li>Do third parties see and/or have access to information obtained by the Rokad? 

<p>Yes. We may share your information on a need-to-know basis with competent authorities in the ways that are described below: </p>
<ul class="subprivacy">
<li>Information required to meet applicable laws, rules and regulations, legal process, Enforceable Government bodies or as described by law of land.  </li>
<li>Information to help in investigations of potential violations.</li>
<li>Information to address/detect/prevent fraud, security or technical issues.</li>
<li>Information to competent authorities to protect Trimax's business interests.</li>
<li>Information with our trusted services providers who work on our behalf, do not have an independent use of the information we disclose to them, and have agreed to adhere to the rules set forth in this privacy.</li>
<li>If Rokad. is involved in a merger, acquisition, or sale of all or a portion of its assets, you will be notified via email and/or a prominent notice on our Web site of any change in ownership or uses of this information, as well as any choices you may have regarding this information.</li>
</ul>

<li>Changes
<p>This Privacy Policy may be updated from time to time for any reason. We will notify you of any changes to our Privacy Policy by posting the new Privacy Policy here. You are advised to consult this Privacy Policy regularly for any changes, as continued use is deemed approval of all changes. </p>
</li>

<li>Consent
<p>By using the Rokad platform, you are consenting to our processing of your information as set forth in this Privacy Policy now and as amended by us.</p>
</li>

</li>
</ul>

<!--new-->
<h3>Grievances Policy</h3>
<p>Rokad is committed to provide best in class value and deliver to its customers. With a rich experience with large PSU to deliver challenging and critical projects and keeping customer centricity approach has given edge to take care of the customer grievances as a top priority.</p>
<p>It will be our constant endeavor to remain committed as a payment service provider which continuously innovates and delivers exemplary service to add value for our customers. With an element of diligence, trust and transparency we believe in offering an effective and efficient Customer Grievance Policy for our customers.</p>
<p>This policy aims at ensuring customer grievances are handled professionally with due sensitivity, urgency and resolution as per our customerís expectations. </p>
<p>Rokad will be constantly devising newer and smarter mechanisms to receive and promptly redress customer grievances.  The details of grievance redress mechanism will be placed in the domain of public knowledge Keeping the urgency and sentiments of the customers we have a structured system which will ensure that customer complaints are handled seamlessly and well within the stipulated timeframe.</p>
<ul>
<li>How to reach Rokad:

<ul class="subprivacy">
<li><b>Level I:</b> You can call us on our 24*7 helpline number <helpline number> to resolve your queries </li>
<li><b>Level II:</b> In case you are not satisfied with Level I response, you may write your query to: <email ID></li>
<li><b>Level III:</b> In case you are not satisfied with Level II response, you may write to: Customer Support, 2nd Floor, Universal Mill Building, Mehra Estate, L.B.S Road, Vikhroli (W), Mumbai - 400079.</li>

</ul>
<p>The aforesaid mechanisms will be exclusively dedicated for customer complaint redressal.</p>
</li>


<li>
Estimated timelines for redressal of complaints:

<p>Please quote the Ticket Number issued for your complaint in your Customer Grievance Redress Escalation. Customers are requested to connect to the next level only if they do not receive a response or resolution from the current one within the stipulated time frame, as below:</p>
</li>


<li>
Frequent complaints

<p>Trimax periodically reviews the customer grievances to identify issues which are being faced by the customers frequently so as to address them by way of making suitable changes / amendments in the system / policies.</p>
</li>

<li>
Sensitizing staff for handling complaints

<p>Trimax staff undergoes regular training to ensure that customer queries and grievances are handled properly. They are encouraged to work in a manner which helps the Trimax build customer trust and confidence.  This reflects in both the operations as well as the customer communications.  The reasons behind customer queries are analyzed and worked upon in a way which aims at removal of such reasons from the root itself. This helps in improving the overall quality of the service levels gradually.</p>
</li>


</li>
</ul>
<h3>Refund Policy</h3>
<ul class=refund>
<li>Any amount transferred erroneously by you to any Merchant Establishment shall not be refunded in any circumstances.</li>
<li>In case a transaction is performed through Rokad platform and service is not delivered within 24 hours then you shall inform us by sending an email to our customer services email address mentioned under Contact Us page. Rokad will investigate further and if it is found that Rokad Wallet was debited and desired service was not delivered, then you will get refund within 21 working days from the date of receipt of your email. All refunds will be credited to your Rokad Wallet</li>
</ul>
</div>
    </div>
</section>