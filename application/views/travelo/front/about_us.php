<style type="text/css">
    ul.chevron li::before{top: 0 !important; margin-top: 3px !important; }
</style>
<div class="page-title-container">
    <div class="container">
       <!-- <div class="page-title pull-left">
            <h2 class="entry-title">About Us</h2>
        </div>
    </div>-->
</div>
<section id="content" class="abouttop">
<div class="termsimg"><img src="<?php echo base_url();?>assets/travelo/front/images/inner-banner.jpg" >
<h2>About Us</h2>
</div>
    <div class="container">
        <div id="main" class="travelo-box">            
            <div class="style8 large-block box-txt div-ul-about-us">
               
               <div class="adata">
<p>Trimax IT Infrastructure and services a leading Information Technology provider, with a vision to empower society through smart technology innovation. A leader in India's Bus based Automatic Fare Collection (AFC) system using handheld device and Smart Cards, is also expertise in orchestrating intelligent transit system to some of the India's biggest Bus based public transport operators</p>
<p>One of the most innovative products of the Trimax, has been a launch of electronic ticketing machine (handheld device) carried by the conductor to issue a journey ticket and validate smart card based travel pass of the on-board passenger. Today, Trimax's technology platform is used by almost 12 transport operators and on an average around 18 Million number of transactions happen using such technology.</p>
<p>Supported by a prowess leadership team and more than 2300 skilled employees working from two Tier III data centres, 15 offices and 450 + service delivery locations across India and supporting more than 600 clients Over 20 Million rural and urban travellers interact with Trimax's Transport Sector Products and Services on daily basis. The Trimax's rural reach solutions also include Wi-Fi for almost 20,000 Indian villages, providing them connectivity and value added services such, which holds a pioneer position in India's transport automation space with its products and services in B2B and B2C space covering Fleet Management, Electronic Ticketing, Universal Mobility Cards etc.,to name a few.</p>
</div>

<div class="privacypolices privacypolicesa">
<h3>Rokad Wallet</h3>
<p class="pdata">Rokad is a digital payment platform, powered by Trimax, to offer citizen centric multimodal and multipurpose services. Rokad promotes demonetization through cashless transactions and provide convenience while availing multi-utility services offered by Rokad. 
</p>


<ul class="subprivacy">
<li>Rokad would enable the passengers to use the same payment instrument across spectrum of other transport operators</li>
<li>Rokad would significantly reduce the cash requirement for travelling purposes</li>
<li>Rokad can be used in paying bills with different merchants</li>
</ul>
<p>Rokad on-board user will get dashboard and peer-to-peer transfer facilities. The dashboard facility would enable the customers to view their transactions, the wallet balance, option to raise a query / dispute, etc. The peer-to-peer transfer facilities would enable customers to remit money in an economic and secured manner.</p>













<!--new-->








</div>
                <div class="clearfix"></div>

        </div> <!-- end main -->
    </div>
</section>