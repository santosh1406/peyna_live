<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Seat Booking <?php echo (isset($response["transaction_order_status"])) ? ucwords($response["transaction_order_status"]) : "Failed";?></h2>
        </div>
    </div>
</div>
<section id="content" class="gray-area">
    <div class="container">
        <div class="row mb20 text-center">
            <!-- <img src="<?php /*echo base_url().FRONT_ASSETS;*/?>images/face_sad.gif" />
            <h1>We're sorry...</h1>
            <p>Error occured while booking ticket. Please try again or contact us on <a href="#" class="contact-email">support@rokad.in</a>.</p>
            <p><a href="<?php /*echo base_url();*/?>">Return to the homepage</a></p> transaction_order_status-->
            <center>
                <?php
                    if(strtolower($response["transaction_order_status"]) == "aborted")
                    {
                ?>
                        <img src="<?php echo base_url().FRONT_ASSETS;?>images/error/aborted.jpg">      
                <?php
                    }
                    else
                    {
                ?>
                        <img src="<?php echo base_url().FRONT_ASSETS;?>images/error/unexpected_error.jpg">
                <?php
                    }
                ?>
            </center>
        </div>
    </div>
</section>
