<style>
.error{
	margin-top:10px;
	margin-bottom:10px;
}
</style>

<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Mobile No. Verification</h2>
        </div>
    </div>
</div>

<section id="content">
    <div class="container tour-detail-page">
        <div class="row login-page">
            <div class="col-sm-8 col-md-6 col-lg-5 no-float no-padding center-block">			
				
				<?php
				$show_error = 'display:none';
				if ($msg_type == "error") 
				{	$show_error = 'display:block';	}
			
				$show_msg = 'display:none';
				if ($msg_type == "success") 
				{	$show_msg = 'display:block';	}
			
				?>				
				<div class="travelo-box" id="error_box" style="<?php echo $show_error; ?>">
					<h3 class="page-title"><i class="glyphicon glyphicon-remove red mr10"></i><span class="msg"><?php echo $msg;?></span></h3>
				</div>

				<div class="travelo-box" id="success_box" style="<?php echo $show_msg; ?>">
					<h3 class="page-title"><i class="soap-icon-check green mr10"></i><span class="msg"><?php echo $msg;?></span></h3>
				</div>				
				
				<?php
				if ($show_OTP_form) {
				?>
				
				<?php
				$attributes = array("method" => "POST", "id" => "login_form", "class" => "login-form");
				echo form_open(base_url().'admin/login/authenticate_otp', $attributes);
				?>
				<div class="form-group">
					<input type="text" placeholder="Enter OTP"  name="otp_code" id="otp_code" value="<?php echo set_value('otp_code'); ?>"
					data-msg-required="Please enter OTP code" class="input-text input-large full-width" />					
					<?php echo form_error('otp_code', '<div class="error">', '</div>'); ?>
				</div>			
				<span class="error-befor-button error-message-format"></span><br>
				
				<div class="row">
				<div class="col-sm-6"><button class="btn-large full-width btnorange" id="" type="submit">Submit</button></div>
				<div class="col-sm-6"><button class="btn-large full-width btnorange" id="resend_otp" type="button">Resend OTP</button></div>				
				</div>
				
				<?php echo form_close(); ?>
				
				<?php } ?>                
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
$(document).ready(function(){
    $('#login_form').validate({
        focusInvalid: false,
        rules: {			
            otp_code: {
                required: true,
				digits: true,                
                minlength: 6,
                maxlength: 6
            }					
        },
        messages: {				
			otp_code: {
                required: "Enter OTP you received on your RMN",
				digits: "Please enter valid OTP",
				minlength: "Please enter 6 digit OTP",
				maxlength: "Please enter 6 digit OTP"
            }			           
         },            
        errorPlacement: function(error, element) {
            error.appendTo(".error-befor-button");
        }
    });
}); // end of document ready

$(document).on("click", "#resend_otp", function() {

$("#error_box").find(".msg").html("");
$("#error_box").hide();

$("#success_box").find(".msg").html("");
$("#success_box").hide();

var request = $.ajax({
  url: "<?php echo base_url(); ?>admin/login/resend_otp",
  method: "POST",
  data: { resend_key : 'registration', rokad_token : rokad_token  } 
});
 
request.done(function( data ) {
	if(data.msg_type == 'success')
	{	
		$("#success_box").find(".msg").html("OTP is sent on your RMN");
		$("#success_box").show();
	}
	else
	{		
		$("#error_box").find(".msg").html(data.msg);
		$("#error_box").show();
	}
});
 
request.fail(function( jqXHR, textStatus ) {
  alert( "Request failed: " + textStatus );
});	
	
});
</script>