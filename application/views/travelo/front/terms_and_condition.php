

<section id="content" class="abouttop">
<div class="termsimg"><img src="<?php echo base_url();?>assets/travelo/front/images/terms.jpg" >
<h2>Terms and Conditions</h2>
</div>
    <div class="container">
        <div id="main" class="travelo-box">
            <div class="row terms-and-conditions">
               <div class="termswrap">

<h3>User of the Wallet Platform / Services</h3>
<p>Please read the following terms and conditions carefully before registering on, accessing, browsing, downloading or using the Rokad website (rokad.in), and all associated sites linked to Rokad, or the Rokad Mobile Application or any similar platform (hereinafter collectively, the Rokad Platform run by Trimax IT Infrastructure & Services Limited, having its registered office at 2nd Floor, Universal Mill Building, Mehra Estate, L.B.S Road, Vikhroli (W), Mumbai - 400079 and/or any of its affiliates (hereinafter collectively, Rokad ) on any device and/or before availing any services offered by Trimax on the Rokad Platform which may include services such as recharge or bill payment, digital products, semi-closed wallet service or any other service that may be offered by Trimax on the Rokad Platform (hereinafter individually, and collectively, the Rokad Services). For the avoidance of doubt, it is clarified that these terms and conditions shall apply to all Rokad Services, whether offered by Trimax or its affiliates.</p>
<h3>Eligibility</h3>
<p>The Rokad Services are not available to persons under the age of 18 or to anyone previously suspended or removed by Trimax from availing the Rokad Services or accessing the Rokad Platform. By accepting the T&Cs or by otherwise using the Rokad Services on the Rokad Platform, you represent that you are at least 18 years of age and have not been previously suspended or removed by Trimax, or disqualified for any other reason, from availing the Rokad Services or using the Rokad Platform. In addition, you represent and warrant that you have the right, authority and capacity to enter into this Agreement and to abide by all the T&Cs as part of this Agreement. Finally, you shall not impersonate any person or entity, or falsely state or otherwise misrepresent your identity, age or affiliation with any person or entity. Finally, in the event of any violation of the T&Cs, Trimax reserves the right to suspend or permanently prevent you from availing Rokad Services or using the Rokad Platform.</p>
<h3>Indemnification</h3>
<p>You agree to indemnify, save, and hold Trimax, its affiliates, contractors, employees, officers, directors, agents and its third party suppliers, licensors, and partners harmless from any and all claims, losses, damages, and liabilities, costs and expenses, including without limitation legal fees and expenses, arising out of or related to: (i) your use or misuse of the Rokad Services or of the Rokad Platform; (ii) any violation by you of this Agreement; or (iii) any breach of the representations, warranties, and covenants made by you herein. Trimax reserves the right, at your expense, to assume the exclusive defense and control of any matter for which you are required to indemnify Trimax, including rights to settle, and you agree to cooperate with Trimax�s defense and settlement of these claims. Trimax will use reasonable efforts to notify you of any claim, action, or proceeding brought by a third party that is subject to the foregoing indemnification upon becoming aware of it. This paragraph shall survive termination of this Agreement.</p>

</div>
            </div>
        </div>
    </div>
</section>