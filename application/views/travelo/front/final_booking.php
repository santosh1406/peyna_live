<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Booking Detail</h2>
        </div>
    </div>
</div>
<section id="content">
    <div class="container tour-detail-page">
        <div class="row">
            <div id="main" class="col-md-9">
                <div id="tour-details" class="travelo-box">
                    <!-- Ticket Detail Start-->
                    <?php
                        foreach ($ticket_view as $key => $value)
                        {
                            $user_exists = $value["user_exists"];
                            $ticket_layout = $value["ticket_layout"];
                            $ticket_details = $value["ticket_details"];

                            // Here We Checked strpos because in the begining we didn't had proper boarding and alighting time.
                            $expected_dept = (strpos($ticket_details->boarding_time, "00:00:00") === false) ? date("h:i A", strtotime($ticket_details->boarding_time)) : "NA";
                            $expected_alighting = (strpos($ticket_details->droping_time, "00:00:00") === false) ? date("h:i A", strtotime($ticket_details->droping_time)) : "NA";
                            // $expected_alighting = (strpos($ticket_details->alighting_time, "00:00:00") === false) ? date("h:i A", strtotime($ticket_details->alighting_time)) : "NA";
                            $from_stopname_full = init_cap($ticket_details->from_stop_name);
                            $to_stopname_full = init_cap($ticket_details->till_stop_name);
                            /*$from_stopname_full = init_cap($ticket_details->boarding_stop_name);
                            $to_stopname_full = init_cap($ticket_details->destination_stop_name);*/
                            $total_no_of_seats = count(array_filter(explode(",",$ticket_details->booked_seat_numbers)));
                    ?>
                        <div class="intro small-box table-wrapper full-width hidden-table-sms">
                            <div class="col-sm-4 table-cell travelo-box">
                                <h4><?php echo ucwords($value['journey_type']);?> Journey</h4>
                                <dl class="term-description">
                                    <dt>From:</dt><dd><?php echo $from_stopname_full; ?></dd>
                                    <dt>To:</dt><dd><?php echo $to_stopname_full; ?></dd>
                                    <dt>Total Seats:</dt><dd><?php echo $total_no_of_seats; ?></dd>
                                    <dt>Seat No:</dt><dd><?php echo $ticket_details->booked_seat_numbers; ?></dd>
                                    <dt>Total Price:</dt><dd>Rs. <?php echo ($ticket_details->tot_fare_amt_with_tax); ?></dd>
                                    <?php if (!empty($ticket_details->total_refund_amt)) {?>
                                    <dt id="or_div" class="seperator_or"></dt><dd class="seperator_or"></dd>
                                    <dt>Total Fare Amount:</dt><dd>Rs. <?php echo $ticket_details->tot_fare_amt_with_tax; ?></dd>
                                    <dt>Total Refund Amount:</dt><dd>Rs. <?php echo $ticket_details->total_refund_amt; ?></dd>
                                    <dt>Total Cancel Charges:</dt><dd>Rs. <?php echo $ticket_details->total_cancel_charges; ?></dd>
                                    <dt>Cancel Seat No:</dt><dd><?php echo $ticket_details->ctd_seat_no; ?></dd>


                                    <?php } ?>
                                </dl>
                            </div>
                            <div class="col-sm-8 table-cell">
                                <div class="detailed-features">
                                    <div class="price-section clearfix">
                                        <div class="details">
                                            <h4 class="box-title"><?php echo ucwords($ticket_details->psgr_name); ?></h4>
                                        </div>
                                        <div class="details">
                                            
                                            <span class="price">Rs. <?php echo ($ticket_details->tot_fare_amt_with_tax); ?></span>
                                            
                                            <a href="#" data-toggle="modal" data-target="#print_ticket_<?php echo $value['journey_type'] ;?>" class="button <?php echo THEME_BTN;?> btn-small uppercase soap-popupbox">Print Ticket</a>
                                        </div>
                                    </div>
                                    <div class="flights table-wrapper">
                                        <div class="table-row">
                                            <div class="table-cell">
                                                <h4 class="box-title">
                                                    <?php echo init_cap($ticket_details->boarding_stop_name); ?> to <?php echo init_cap($ticket_details->destination_stop_name); ?>
                                                    <small class="view_doj"><?php echo date("D, d M Y", strtotime($ticket_details->dept_time)); ?></small>
                                                </h4>
                                            </div>
                                            <div class="table-cell">
                                                <i class="icon soap-icon-passenger"></i>
                                                <dl><dt>Departs</dt><dd><?php echo $expected_dept;?></dd></dl>
                                            </div>
                                            <div class="table-cell">
                                                <i class="icon soap-icon-adventure"></i>
                                                <dl><dt>Arrives</dt><dd><?php echo $expected_alighting; ?></dd></dl>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- Ticket Detail End-->
                    <?php
                        }
                    ?>
                    <!-- Here The Google map will be shown from stop to end stop. This MUST be changed after discussion-->
                    <!-- <div class="tour-google-map block"></div> -->
                </div>
            </div>
        </div>
    </div>

    <!-- Print Ticket Start Here -->
    <!-- Modal -->
    <?php
        foreach ($ticket_view as $key => $value)
        {
            $user_exists = $value["user_exists"];
            $ticket_layout = $value["ticket_layout"];
            $ticket_details = $value["ticket_details"];
    ?>
    <div class="modal fade printable autoprint" id="print_ticket_<?php echo $value['journey_type'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width:850px">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">E-Ticket</h4>
          </div>
          <div class="modal-body" id="modal-body">
            <?php
                if(isset($ticket_layout) && count($ticket_layout) > 0 )
                {
                    echo $ticket_layout[0]->ticket;

                    if (isset($user_exists[0]->username) && $user_exists[0]->username == "" ) {
                        $link=base_url().'admin/login/signup_view?email=' . $value['email'];
                        echo "Still not have account with BoS!! <a href='".$link."'>Click Here</a> to register.";
                    }
                }
            ?>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary" onClick="window.print();">Print</button> -->
            <button type="button" onclick="openWin()" class="btn btn-primary print_this_ticket">Print</button>
            <!--<a href="#" onclick="openWin()">click</a> -->
          </div>
        </div>
      </div>
    </div>
    <?php
        }
    ?>
    <!-- Print Ticket Start Here -->
    
</section>

<script type="text/javascript">
// // This must be discussed later


</script>
<script type="text/javascript">

    function openWin() 
    {
        var myWindow =  window.open("","", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=700,width=550,height=550");     
        var is_chrome = Boolean(myWindow.chrome);
        var divText = document.getElementById("modal-body").outerHTML;
        myWindow.document.write(divText);
        if (is_chrome)
        {
            setTimeout(function()
             { // wait until all resources loaded 
                myWindow.document.close(); // necessary for IE >= 10
                myWindow.focus(); // necessary for IE >= 10
                myWindow.print(); // change window to winPrint
                myWindow.close(); // change window to winPrint
             }, 250);
        }
        else
        {
            myWindow.document.close(); // necessary for IE >= 10
            myWindow.focus(); // necessary for IE >= 10
            myWindow.print();
            myWindow.close();
        }
    }
</script>
