<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Book On Spot</title>
        <!-- Custom CSS -->
        <link href="<?php echo base_url() . FRONT_ASSETS ?>css/style.css" rel="stylesheet">
        <link href="<?php echo base_url() . COMMON_ASSETS ?>css/bootstrap/font-awesome.min.css" rel="stylesheet" type="text/css">
        <script src="<?php echo base_url() . COMMON_ASSETS ?>js/jquery.js"></script>
        <script src="<?php echo base_url() . COMMON_ASSETS; ?>js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
        <script type="text/javascript">
            function GVerifyCallback(response)
            {
                $("#forgot_password").submit();
            }

            var onloadCallback = function() {
                greverify = grecaptcha.render('g_recaptcha', {
                    'sitekey' : "<?php echo $this->config->item('google_captcha_site_key'); ?>",
                    'callback' : GVerifyCallback
                });
            };
        </script>
    </head>
    <body>
        <!----------------------- Header-Container -------------------------------->
        <div id="header-container">
            <div class="topheader">
                <div class="header">
                    <div class="lp-logo"><a class="logo" href="<?php echo base_url(); ?>"><img src="<?php echo base_url() . FRONT_ASSETS ?>images/logo.png" alt="Online Ticket Booking System" width="320" height="107"></a></div>
                    <div class="white_content" id="light" style="display: block;min-height:230px;">
                        <div class="logincontent">
                            <div class="login"> <!-- Login -->
                                <h1>Forgot Password</h1>
                                <?php
                                $attributes = array("method" => "POST", "id" => "forgot_password", "class" => "cmxform form");
                                echo form_open('login/forgot_password', $attributes);
                                ?>
                                <p class="field">
                                    <input type="text" placeholder="Username" name="username" id="username" data-rule-required="true" data-rule-username="true" data-msg-required="Please enter your username" required/>
                                    <i class="fa fa-user"></i>
                                </p>
                                <p class="field">
                                    <input type="hidden" class="hiddenRecaptcha" name="hiddenRecaptcha" id="hiddenRecaptcha" data-rule-required="true" data-rule-hiddenRecaptcha="true" data-rule-required="true" data-msg-required="Please confirm captcha" required />
                                    <div class="g-recaptcha" id="g_recaptcha"></div>
                                </p>
                                <p class="submit"><input type="submit" name="commit" value="Send me link"></p>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>

        <script>
            $(document).ready(function(){ 
                $("#forgot_password").validate({
                    ignore: "",
                    rules: {
                        username: {
                            required: true
                        },
                        hiddenRecaptcha: {
                             required: function() {
                                 if(grecaptcha.getResponse() == '') {
                                     return true;
                                 } else {
                                     return false;
                                 }
                             }
                        }
                    }
                });
            });
        </script>
        <!-- for session message notification end  -->
    </body>
</html>
