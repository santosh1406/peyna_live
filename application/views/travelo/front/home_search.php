<?php
    $this->load->view(HOME_SEARCH_TPL);

    $is_return_journey = (isset($filter_data['date_of_arr'])&& $filter_data['date_of_arr'] != "") ? DateTime::createFromFormat("d-m-Y", $filter_data['date_of_arr']) : "";

    $opacity = "";
    $inactive_link = "";
    // ($is_return_journey) ? ($divclass = "col-sm-5 col-md-5" AND $opacity = "opacity-03" AND $inactive_link = "inactive-link" AND $display = "display-block") : ($divclass = "col-sm-10 col-md-10" AND $display = "display-none");
    ($is_return_journey) ? ($divclass = "col-sm-6 col-md-6" AND $opacity = "opacity-03" AND $inactive_link = "inactive-link" AND $display = "display-block") : ($divclass = "col-sm-12 col-md-12" AND $display = "display-none");

     $ac_non_ac = "Bus Type (All)";
    if($filter_data["searchbustype"] == "ac")
    {
       $ac_non_ac =  "AC";
    }
    else if($filter_data["searchbustype"] == "non_ac")
    {
       $ac_non_ac =  "Non AC";
    }

    $to_show_operator = "Bus Operator Type (All)";
    if($filter_data["provider"] == "operator")
    {
        $to_show_operator = "State Transport Operator";
    }
    else if($filter_data["provider"] == "api")
    {
        $to_show_operator = "Private Operator";
    }
?>

<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Search Buses</h2>
        </div>
    </div>
</div>

<section id="content" class="home_search_section">
    <div class="container">
        <div id="main">
            <!-- Modify Search Start -->
            <?php
              $attributes = array("id" => "modify_search_form", "onsubmit" => "return false","autocomplete" => "off");
              echo form_open('#', $attributes);
            ?>
                <div class="row seven-cols">
                    <div class="col-sm-12">
                        <div class="form-inline clearfix searchbar">
                            <div class="col-lg-1 col-md-3 col-sm-4 col-xs-6">
                                <input type="text" class="input-text full-width frm-to-src-dest" name="from" placeholder="Boarding Stop / City" id="from1" size="10" value="<?php echo $filter_data['from'];?>"/>
                            </div>
                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 swapw">
                                <div class="form-group">
                                    <img src="https://rokad.in/assets/travelo/front/images/homepage/arrow_circle.png" class="cursor-pointer switch_cities" alt="Switch Cities" title="Switch Cities">
                                </div>
                            </div>
                            <div class="col-lg-1 col-md-3 col-sm-4 col-xs-6">
                                <input type="text" class="input-text full-width frm-to-src-dest" name="to"  placeholder="Destination City" id="to1" size="10" value="<?php echo $filter_data['to'];?>"/>
                            </div>
                            <div class="col-lg-1 col-md-3 col-sm-4 col-xs-6">
                               <div class="datepicker-wrap <?php echo THEME_CALENDER;?> show-calender-datepicker">
                                    <input type="text" class="input-text full-width date_of_jour_arr" value="<?php echo $filter_data['date_of_jour'] ?>" placeholder="Date Of Journey" name="date_of_jour" id="date_of_jour"/>
                                </div>
                            </div>
                            <div class="col-lg-1 col-md-3 col-sm-4 col-xs-6">
                                <div class="datepicker-wrap <?php echo THEME_CALENDER;?> show-calender-datepicker">
                                    <input type="text" class="input-text full-width date_of_jour_arr" value="<?php echo (isset($filter_data['date_of_arr']) && $filter_data['date_of_arr'] != '') ? $filter_data['date_of_arr'] : ''  ?>" placeholder="Return Date (Opt)" name="date_of_arr" id="date_of_arr" style="padding-left: 10px;"/>
                                </div>
                            </div>
                            <div class="col-lg-1 col-md-3 col-sm-4 col-xs-6">
                                <div class="selector">
                                    <select name="provider" id="provider_modify" class="full-width new-txt-color">
                                        <option value="all">Bus Operator Type (All)</option>
                                        <option value="operator">State Transport Operator</option>
                                        <option value="api">Private Operator</option>
                                    </select>
                                    <span class="custom-select full-width" id="provider_selected"><?php echo $to_show_operator;?></span>
                                </div>
                            </div>
                             <div class="col-lg-1 col-md-3 col-sm-4 col-xs-6">
                                <div class="selector">
                                    <select name="searchbustype" id="searchbustype" class="full-width new-txt-color">
                                        <option value="all">Bus Type (All)</option>
                                        <option value="ac">AC</option>
                                        <option value="non_ac">Non AC</option>
                                    </select>
                                    <span class="custom-select full-width" id="searchbustype_selected"><?php echo $ac_non_ac; ?></span>
                                </div>
                            </div>
                            <div class="col-lg-1 col-md-3 col-sm-4 col-xs-6 pull-right serchw">
                                <button type="submit" id="submit_filter_btn" class="btn-medium font15 full-width <?php echo THEME_BTN;?>">Search</button>
                            </div>
                        </div>                    
                    </div>
                </div>
            </form>

            <div class="row mb10 next-pre-search" id="next-pre-search" style="display:block;">
                <div class="col-sm-12">
                    <div class="form-inline clearfix searchbar">
                        <div class="form-group next-pre <?php echo $divclass; ?> text-align-center">
                            <div class="from_to_search">
                                <span class="mr10" id="show_from"><?php echo $filter_data['from'] ?></span>
                                <span class="fa fa-arrow-right"></span>
                                <span class="ml10" id="show_to"><?php echo $filter_data['to'] ?></span>
                            </div>
                            <div class="next_previous">
                                <span>Previous</span>
                                <span class="fa fa-chevron-left mr10 search_date" id="prev" data-ref="<?php echo date('d-m-Y', strtotime($filter_data['date_of_jour'].'-1 day'))  ?>" title="Previous Day"></span>
                                <span id="date_shown"><?php echo date('d-M-Y', strtotime($filter_data['date_of_jour'])) ?></span>
                                <span class="fa fa-chevron-right ml10 search_date" id="next" data-ref="<?php echo date('d-m-Y', strtotime($filter_data['date_of_jour'].'+1 day'))  ?>" title="Next Day"></span>
                                <span>Next</span>
                            </div>
                        </div>
                        <!-- Return Jourmey Start Here -->
                        <div class="form-group return_next-pre <?php echo $divclass.' '.$inactive_link.' '.$opacity.' '.$display; ?> text-align-center">
                            <div class="from_to_search">
                                <span class="mr10" id="return_show_from"><?php echo $filter_data['to'] ?></span>
                                <span class="fa fa-arrow-right"></span>
                                <span class="ml10" id="return_show_to"><?php echo $filter_data['from'] ?></span>
                            </div>
                            <div class="next_previous">
                                <span>Previous</span>
                                <span class="fa fa-chevron-left mr10 search_date" id="return_prev" data-ref="<?php echo (isset($filter_data['date_of_arr']) && $filter_data['date_of_arr'] != '') ? date('d-m-Y', strtotime($filter_data['date_of_arr'].'-1 day')) : '';  ?>" title="Previous Day"></span>
                                <span id="return_date_shown"><?php echo (isset($filter_data['date_of_arr']) && $filter_data['date_of_arr'] !="" ) ? date('d-M-Y', strtotime($filter_data['date_of_arr'])) : "" ;?></span>
                                <span class="fa fa-chevron-right ml10 search_date" id="return_next" data-ref="<?php echo (isset($filter_data['date_of_arr']) && $filter_data['date_of_arr'] != '') ? date('d-m-Y', strtotime($filter_data['date_of_arr'].'+1 day')) : '';  ?>" title="Next Day"></span>
                                <span>Next</span>
                            </div>
                        </div>
                        <!-- Return Jourmey End Here -->
                    </div>                    
                </div>
            </div>
            <!-- Modify Search End -->

            <div class="row" id="bus_service_row_id">
                <!-- filter left bar start-->
                <div class="col-sm-4 col-md-3">
                    <h4 class="search-results-title"><i class="soap-icon-search"></i><b><span id="no_of_bus_found">0</span></b> results found.</h4>
                    <div class="toggle-container filters-container">
                        <!-- Fare Range Start -->
                        <div class="panel style1 arrow-right">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#price-filter" class="collapsed">Fare</a>
                            </h4>
                            <div id="price-filter" class="panel-collapse collapse">
                                <div class="panel-content">
                                    <div id="fare_range"></div>
                                    <br/>
                                    <span class="min-price-label pull-left"></span>
                                    <span class="max-price-label pull-right"></span>
                                    <div class="clearer"></div>
                                </div><!-- end content -->
                            </div>
                        </div>
                        <!-- Fare Range End -->
                        <!-- Operator Start -->
                        <div class="panel style1 arrow-right">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#cruise-line-filter" class="collapsed">Travels</a>
                            </h4>
                            <div id="cruise-line-filter" class="panel-collapse collapse">
                                <div class="panel-content">
                                    <ul class="check-square filter filters-option" data-ref="travel" id="travel">
                                       
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- Operator End -->
                        <!-- Bus Types Start -->
                        <div class="panel style1 arrow-right">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#cruise-preference-filter" class="collapsed">Bus Types</a>
                            </h4>
                            <div id="cruise-preference-filter" class="panel-collapse collapse">
                                <div class="panel-content">
                                    <ul class="check-square filter filters-option" id="bus_type" data-ref="bus-type">
                                       
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- Bus Types End -->
                        <!-- Departure Start -->
                        <div class="panel style1 arrow-right">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#cruise-cabin-type-filter" class="collapsed">Departure Time</a>
                            </h4>
                            <div id="cruise-cabin-type-filter" class="panel-collapse collapse">
                                <div class="panel-content">
                                    <ul class="check-square filter filters-option" data-ref="dept">
                                        <li><a href="javascript:void(0);" data-attr="mid_night">00:00 AM - 03:59 AM (Mid Night)</a></li>
                                        <li><a href="javascript:void(0);" data-attr="morning">04:00 AM - 11:59 AM (Morning)</a></li>
                                        <li><a href="javascript:void(0);" data-attr="afternoon">12:00 PM - 03:59 PM (Afternoon)</a></li>
                                        <li><a href="javascript:void(0);" data-attr="evening">04:00 PM - 06:59 PM (Evening)</a></li>
                                        <li><a href="javascript:void(0);" data-attr="night">07:00 PM - 11:59 PM (Night)</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- Departure End -->
                        <!-- Total Duration Start -->
                        <div class="panel style1 arrow-right">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#cruise-duration-type-filter" class="collapsed">Duration</a>
                            </h4>
                            <div id="cruise-duration-type-filter" class="panel-collapse collapse">
                                <div class="panel-content">
                                    <ul class="check-square filter filters-option" data-ref="duration-time">
                                        <li><a href="javascript:void(0);" data-attr="six">Upto 6 Hours</a></li>
                                        <li><a href="javascript:void(0);" data-attr="twelve">6 Hours - 12 Hours</a></li>
                                        <li><a href="javascript:void(0);" data-attr="twentyfour">12 Hours - 24 Hours</a></li>
                                        <li><a href="javascript:void(0);" data-attr="more">More than a day</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- Total Duration End -->
                    </div>
                </div>
                <!-- filter left bar end-->

                <!-- bus service right panel start -->
                <div class="col-sm-8 col-md-9">
                    <!-- Sort By Start Here -->
                    <div class="sort-by-section box clearfix">
                        <h4 class="sort-by-title block-sm">Sort results by:</h4>
                        <ul class="sort-bar clearfix block-sm">
                            <li class="sort-by-name sort_col" ref="data-travel" data-sort="asc"><a class="sort-by-container" href="#"><span>Travels</span></a></li>
                            <li class="sort-by-price sort_col" ref="data-dep_tm_str" data-sort="asc"><a class="sort-by-container" href="#"><span>Departure</span></a></li>
                            <li class="clearer visible-sms"></li>
                            <li class="sort-by-date sort_col" ref="data-seat" data-sort="asc"><a class="sort-by-container" href="#"><span>Seats</span></a></li>
                            <li class="sort-by-cruise-line sort_col" ref="data-fare" data-sort="asc"><a class="sort-by-container " href="#"><span>Fare</span></a></li>
                        </ul>
                    
                    </div>
                    <!-- Sort By End Here -->

                    <div class="cruise-list listing-style3 cruise bus_services" id="bus_service_ul_id">
                        <!-- <center><span class="loader_25"> <img src="<?php echo base_url().COMMON_ASSETS ;?>images/loading.gif" alt="BOS Loading"></span></center> -->
                    </div>  
                </div>
                <!-- bus service right panel end -->
            </div>
        </div>
    </div>
    <!-- All Details Start Here -->
    <div class="modal fade all_details" id="all_details" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <span class="close modal_close cursor-pointer" data-dismiss="modal">&times;</span>
          <div class="modal-body">
            <div class="table-responsive">
                <table class="table details_all">
                    <tr>
                        <th>Bus Operator</th>
                        <th>Departure</th>
                        <th>Duration</th>
                        <th>Arrival</th>
                        <th>mTicket</th>
                        <th>Seats</th>
                        <th>Fare</th>
                    </tr>
                    <tr>
                        <td class="operator"></td>
                        <td class="departure">
                            <span class="icon fa"></span><br/>
                            <b class="time"></b>
                        </td>
                        <td class="travel_duration">
                            <span class="fa fa-arrow-right"></span><br/>
                            <span class="total_time"></span>
                        </td>
                        <td class="arrival">
                            <span class="icon fa"></span><br/>
                            <b class="time"></b>
                        </td>
                        <td class="">
                           <div class="amenities">
                              <i class="fa fa-mobile circle active"></i>
                          </div>
                        </td>
                        <td class="seats">
                            
                        </td>
                        <td class="price">
                            
                        </td>
                    </tr>
                </table>
            </div>

            <hr>
            <div class="show-loading">
                <!-- <center><span class="loader_25"><img src="<?php echo base_url().COMMON_ASSETS ;?>images/loading.gif" alt="BOS Loading"></span></center> -->
            </div>
            <!-- tab container start-->
            <div class="tab-container hide-tab-container full-width-style white-bg" style="display:none;">
                <ul class="tabs">
                    <li class="active"><a href="#pick-up-details" data-toggle="tab"><i class="soap-icon-passenger circle"></i>Pick Up</a></li>
                    <li><a href="#drop-off-details" data-toggle="tab"><i class="soap-icon-adventure circle"></i>Drop Off</a></li>
                    <li><a href="#cancellation-policy" data-toggle="tab"><i class="fa fa-exclamation-circle circle"></i>Policy</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="pick-up-details">
                        <div class="table-responsive">
                            <table class="table pick-drop-policy">
                                <thead>
                                    <tr>
                                        <th width="30%">Pickup Area</th>
                                        <th width="60%">Pickup Address</th>
                                        <th width="10%">Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="drop-off-details">
                        <div class="table-responsive">
                            <table class="table pick-drop-policy">
                                <thead>
                                    <tr>
                                        <th>Drop Off Location</th>
                                        <th>Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="cancellation-policy">
                        <div class="table-responsive">
                            <table class="table pick-drop-policy">
                                <thead>
                                    <tr>
                                        <th>Cancellation Time</th>
                                        <th>Cancellation Charges</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <div class="table-responsive">
                            <table class="table pick-drop-policy-conditions hide">
                                <thead>
                                    <tr>
                                        <th>Terms & Conditions</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- tab container end-->
          </div>
        </div>
      </div>
    </div>
    <!-- All Details End Here -->
</section>
<script type="text/Javascript" src="<?php echo base_url() ?>js/front/cities.min.js?token='21042016'"></script>
<script src="<?php echo base_url() ?>assets/common/js/sort.js"></script>
<script type="text/javascript">
    var seat_fare_detail = {};
    var global_bus_services;
    var global_stu_bus_services;
    var global_private_bus_services;
    var seat_boarding_points = {};
    var global_trip_data = {};
    var min_fare = 0;
    var max_fare = 100;
    var global_response;
    var temp_seats;
    var onwardJourny = {};
    var returnJourny = {};
    var detail_op = {};
    var enable_discount = "<?php echo $enable_discount;?>";
    var show_banner_discount = false;
    var show_discounted_fare = true;
    var discount_above = "<?php echo $this->config->item('discount_above');?>";
    var discount_till = "<?php echo $this->config->item('discount_till');?>";
    var discount_on = "<?php echo $this->config->item('discount_on');?>";
    var discount_type = "<?php echo $this->config->item('discount_type');?>";
	var discount_rate = "<?php echo $this->config->item('discount_percent_rate');?>";
	var max_dis_availed = "<?php echo $this->config->item('max_dis_availed');?>";
    var todays_date = "<?php echo CURRENT_DATE;?>"; //in Y-m-d Format


    $('.date_of_jour_arr').bind("cut copy paste drop keyup",function(e) {
        e.preventDefault();
    });

    

    $(document).ready(function(){
		
		$(document).off("click",".switch_cities").on("click",".switch_cities",function(e){
			e.preventDefault();
			var r=$("#from1").val(),t=$("#to1").val();(""!=r||""!=t)&&($("#from1").val(t),$("#to1").val(r))
			});
        //Check if discount on
        // show(discount_till.toISOString());
        /*discount_till = moment(discount_till).format('YYYY-MM-DD');
        todays_date = moment(todays_date).format('YYYY-MM-DD');*/
        $(".show-loading").hide();
        var is_discount_on = ( ( !moment(todays_date).isAfter(discount_till ) ) && enable_discount) ? true : false;

        make_autocomplete('.frm-to-src-dest', cities);
        
        // For while searching service
        /*$('#show_waiting').modal({
                                backdrop: 'static',
                                keyboard: false  // to prevent closing with Esc button (if you want this too)
                            })
*/
        var query_string = <?php echo json_encode($filter_data) ?>;
        /******* To get bus service start here *******/
        var get_bus_services = function (filter){

            var detail = filter;
            var div = "";
            var ajax_url = 'front/home/bus_services';
            var form = '';

            // $("#bus_service_ul_id").html("");
            showServiceWaiting(filter);

            get_data(ajax_url, form, div, detail, function(response)
            {
                $("#bus_service_ul_id").html("");
                var stu_grouping_structure_tpl = $.parseHTML($.trim($("#stu_grouping_structure_tpl").html()));

                $("#bus_service_ul_id").html(stu_grouping_structure_tpl);

                if(response.status == "success")
                {
                    global_response = response;

                    if(response.bus_detail != undefined)
                    {
                        var no_of_bus_found = response.bus_detail.length;
                        if(no_of_bus_found > 0)
                        {
                            $("#no_of_bus_found").html(no_of_bus_found);

                            min_fare = parseFloat(response.time_fare.bus_detail.min_fare);
                            max_fare = parseFloat(response.time_fare.bus_detail.max_fare);

                            $(".min-price-label").html( "<i class='fa fa-inr'></i> " + min_fare );
                            $(".max-price-label").html( "<i class='fa fa-inr'></i> " + max_fare);

                            $( "#fare_range" ).slider( "option", "min", min_fare );
                            $( "#fare_range" ).slider( "option", "max", max_fare );
                            $( "#fare_range" ).slider({values: [ min_fare, max_fare ]});

                            max_time = response.time_fare.bus_detail.max_dep_tm;
                            min_time = response.time_fare.bus_detail.min_dep_tm;

                            //stu grouping rendering
                            if(response.time_fare.stu_range != undefined && response.time_fare.stu_range.stu_total_seats > 0)
                            {
                                render_stu_bus_services(response.time_fare.stu_range);
                            }

                            render_bus_services(response.bus_detail, response.user_data.date); 
                        }
                        else
                        {
                            var yesno = (typeof(rejour_obj) == "undefined") ? false : true ;
                            no_result_found(yesno);
                        }
                    }
                    else
                    {
                        /*Temporary Commented*/
                        var yesno = (typeof(rejour_obj) == "undefined") ? false : true ;
                        no_result_found(yesno);
                    }
                }// end of if response.status
                else
                {
                    location.reload();
                }
            }, '', true);
        }

        /***************** render stu bus service start *******************/
        var render_stu_bus_services = function(stu_response)
        {
            var stu_grouping_tpl = $.parseHTML($.trim($("#stu_grouping_tpl").html()));
            var $stuelem = $(stu_grouping_tpl);
            var stu_op_name = "State Road Transport Corporation";
            if(stu_response.op_name_array.length == 1)
            {
                if(stu_response.op_name_array[0] == "msrtc")
                {
                    stu_op_name = "Maharashtra State Road Transport Corporation";
                }
                else if(stu_response.op_name_array[0] == "upsrtc")
                {
                    stu_op_name = "Uttar Pradesh State Road Transport Corporation";
                }
    			else if(stu_response.op_name_array[0] == "hrtc")
                {
                    stu_op_name = "Himachal Road Transport Corporation";
                }
                else if(stu_response.op_name_array[0] == "rsrtc")
                {
                    stu_op_name = "Rajasthan State Road Transport Corporation";
                }
            }
            else if(stu_response.op_name_array.length > 1)
            {
                var stu_op_name = stu_response.op_name_array.join("/").toUpperCase()+" State Road Transport Corporation"; 
            }

            $stuelem.find(".stu_op_name").html(stu_op_name);
            $stuelem.find(".total_stu_buses").html("Total Bus - "+stu_response.stu_total_buses);

            var stu_bus_range = (stu_response.min_fare != stu_response.max_fare) ? "Starting From "+stu_response.min_fare : stu_response.min_fare;
            $stuelem.find(".stu_price").html(stu_response.stu_bus_range);
            $stuelem.find(".start_at").html(stu_response.start_at);
            $stuelem.find(".travel_duration_range").html(stu_response.min_duration+"-"+stu_response.max_duration);
            $stuelem.find(".total_stu_seat_available").html(stu_response.stu_total_seats);
            if(stu_response.mticket_allowed)
            {
                $stuelem.find(".amenities-n .fa-mobile").addClass("active").attr("data-original-title","Mticket is allowed to board the bus.");
            }
            else
            {
                $stuelem.find(".amenities-n .fa-mobile").addClass("notransition").removeClass("active").attr("data-original-title","Mticket is NOT allowed to board the bus.");
            }

            $("#bus_service_ul_id #stu_bus_summary").html($stuelem);
        }

        
        /***************** render stu bus service end *******************/

        /***************** render bus service start *******************/
        var render_bus_services = function(response, service_date)
        {
            $("#noResult").hide();
            $("#travel").html("");
            $("#bus_type").html("");

            $.each(response, function(i,value)
            {
                /*global_trip_data[value.trip_no] = value;*/
                global_trip_data[value.bus_travel_id] = value;

                var bus_service_tpl = $.parseHTML($.trim($("#bus_service_tpl").html()));
                var $elem = $(bus_service_tpl);

                var is_service_discount = (value.service_discount != "" && value.service_discount != undefined && eval(value.service_discount)) ? true : false;

                $elem.attr({'data-dep_tm_str': value.str_dep_tm_hm});

                if($("#travel li a[data-attr='"+value.op_name.trim()+"']").length < 1)
                {
                    $("#travel").append("<li><a href='javascript:void(0);' data-attr='"+value.op_name.trim()+"'>"+value.op_name+"</a></li>");
                }

                if($("#bus_type li a[data-attr='"+value.bus_type_id+"']").length < 1)
                {
                    // $("#bus_type").append("<li><a href='javascript:void(0);' data-attr='"+value.bus_type_id+"'>"+value.op_bus_type_name+"</a></li>");
                    $("#bus_type").append("<li><a href='javascript:void(0);' data-attr='"+value.bus_type_id+"'>"+value.bus_type_name+"</a></li>");
                }
                
                $elem.find(".popup_all_details").attr("data-bus-travel-id",value.bus_travel_id);
                $elem.find(".popup_all_details").attr("data-trip-no",value.trip_no);
                $elem.find(".popup_all_details").attr("data-op-id",value.op_id);

                $elem.find(".op_name").text(value.op_name);
                $elem.find(".from-to-to").text(" - "+value.from_stop+" to "+value.to_stop);
                
                var data_bus_type = "";
                if(value.bus_type_id != "" && value.bus_type_id != null && value.bus_type_id != undefined)
                {
                    data_bus_type = value.bus_type_id.trim(); //not id its string
                }
                else
                {
                    data_bus_type = value.bus_type_id;
                }

                $elem.attr({"data-fare": value.seat_fare, 
                            "data-seat":value.tot_seat_available, 
                            // "data-bus-type":value.bus_type_id, 
                            "data-bus-type":data_bus_type,
                            "data-op-bus-type-cd":value.op_bus_type_cd, 
                            "data-arrival":"", 
                            "data-dept":value.shift, 
                            "data-travel" : value.op_name.trim(),
                            "data-duration-time" : value.travel_duration,
                            "data-service-discount" : is_service_discount
                            }).addClass("article_"+value.bus_travel_id);

                $elem.find(".sch_departure_date").html(value.str_dep_tm_his);
                $elem.find(".arrival_tm").html(value.arrival_time);
                $elem.find(".drop_point").html("Drop Point");
                $elem.find(".short_travel_duration").html(value.short_travel_time);

                /*if(value.api_bus_type != "" && value.api_bus_type != null && value.api_bus_type != undefined && value.api_bus_type)
                {
                    var both_bus_types = value.bus_type_name;
                }
                else
                {
                   var both_bus_types = (value.bus_type_name != "" && value.bus_type_name != null) ? (value.op_bus_type_name+" / "+value.bus_type_name) : value.op_bus_type_name;
                }*/

                var both_bus_types = (value.bus_type_name != "" && value.bus_type_name != null) ? (value.op_bus_type_name+" / "+value.bus_type_name) : value.op_bus_type_name;

                $elem.find(".bus_type").html(both_bus_types);

                /********** Cancellation Policy Start**********/
                // ttp_ticket_cancellation_policy
                if(value.cancellation_policy != "" && value.cancellation_policy != undefined)
                {
                    if(value.op_id == 0)
                    {
                        $.each(value.cancellation_policy, function(cpol,cpoldetail)
                        {
                            var cancellation_sentence = "Cancellation within <b>"+cpoldetail.cutoffTime+" hrs</b>";
                            var row = $("<tr>");
                            row.append($("<td>").html(cancellation_sentence));
                            row.append($("<td>").html(cpoldetail.refundInPercentage+ "%"));
                            $elem.find(".ttp_ticket_cancellation_policy tbody").append(row);
                        });
                    }
                    else if(value.op_id > 0 && $.inArray(value.op_name.toLowerCase(), ["msrtc","upsrtc","rsrtc"]) !== -1)
                    {
                        $.each(value.cancellation_policy, function(cpolj,cdetail)
                        {
                            var charges_percent = cdetail.type == 'percent' ? " %" : "";
                            var charges_rupees = cdetail.type == 'fixed' ? "Rs. " : "";

                            if(value.op_name.toLowerCase() != 'rsrtc')
                            {
                                var from = cdetail.from != "" ? Math.round(cdetail.from/3600) : "";
                                var till = cdetail.till != "" ? Math.round(cdetail.till/3600) : "";
                                var cancellation_sentence = "Cancellation upto <b>"+from+" hrs</b>";
                            }
                            else if (value.op_name.toLowerCase() == 'rsrtc')
                            {
                                var from = cdetail.from != "" ? (cdetail.from/3600).toFixed(2) : "";
                                var till = cdetail.till != "" ? Math.round(cdetail.till/3600) : "";
                                if(from == '0.50')
                                {
                                    var cancellation_sentence = "Cancellation upto <b>30 mins</b>";
                                }
                                else
                                {
                                    var cancellation_sentence = "Cancellation upto <b>"+from+" hrs</b>";  
                                }
                                
                            }
                            
                            var row = $("<tr>");
                            row.append($("<td>").html(cancellation_sentence));
                            row.append($("<td>").html(charges_rupees+cdetail.cancel_rate+charges_percent));
                            $elem.find(".ttp_ticket_cancellation_policy tbody").append(row);
                        });

                        if(value.cancellation_policy[0] != "" && value.cancellation_policy[0] != undefined && value.cancellation_policy[0].note != "" &&  value.cancellation_policy[0].note != undefined)
                        {
                            var cancellation_charges_conditions = value.cancellation_policy[0].note.split("|||");
                            $elem.find(".ttp_ticket_cancellation_policy").append("<table class='table font13 cancellation_policy_terms'><thead><tr><th>Terms & Conditions</th></tr></thead><tbody></tbody></table>");
                            $.each(cancellation_charges_conditions, function(ccc_index,cc_condition)
                            {
                                var ccc_row = $("<tr>");
                                ccc_row.append($("<td>").html(cc_condition));
                                $elem.find(".ttp_ticket_cancellation_policy table.cancellation_policy_terms tbody").append(ccc_row);
                            });
                        }
                    }
                }
                /********** Cancellation Policy End**********/

                //Mobile Mticket Condition
                if(value.mticket_allowed)
                {
                    $elem.find(".amenities-n .fa-mobile").addClass("active").attr("data-original-title","Mticket is allowed to board the bus.");
                }
                else
                {
                    $elem.find(".amenities-n .fa-mobile").addClass("notransition").removeClass("active").attr("data-original-title","Mticket is NOT allowed to board the bus.");
                }

                
                if(value.seat_fare.toString().indexOf(',') > -1)
                {
                    var service_fare_array = value.seat_fare.split(',');
                    var service_discount_fare_array = value.actual_seat_fare.split(',');
                }
                else
                {
                    var service_fare_array = [value.seat_fare];
                    var service_discount_fare_array = [value.actual_seat_fare];
                }

                if(value.seat_fare != "")
                {
                    if(service_fare_array.length > 1)
                    {
                        // var min_max_service_fare = Math.min.apply(Math, service_fare_array)+" - "+Math.max.apply(Math, service_fare_array);
                        // var min_max_service_fare = "&nbsp;<span class='price_starting_from'>Starting Form</span> Rs."+Math.min.apply(Math, service_fare_array).toFixed(2);
                        var min_max_service_fare = Math.min.apply(Math, service_fare_array).toFixed();
                        
                        if(is_discount_on && is_service_discount)
                        {
                            // var discount_min_max_service_fare = Math.min.apply(Math, service_discount_fare_array)+" - "+Math.max.apply(Math, service_discount_fare_array);//Remove This
                            var discount_min_max_service_fare = Math.min.apply(Math, service_discount_fare_array).toFixed();//Remove This
                            // $elem.find(".seat_fare").html('<span class="line-txt">Rs.'+discount_min_max_service_fare+'</span>  Rs.'+min_max_service_fare).addClass("right-amount-txt");
                            // $elem.find(".seat_fare").html('<span class="line-txt"> Rs.'+discount_min_max_service_fare+'</span> '+min_max_service_fare).addClass("right-amount-txt");

                            // $elem.find(".seat_fare").html($('<span />').attr({"class" : "search_fare_text padr78 price"}).html("<span class='pricefrom'>Starting From</span> Rs."+min_max_service_fare)).append($('<span />').attr({"class" : "line-txt pull-right search_discount_fare_text"}).html("Rs."+discount_min_max_service_fare));
                            if(show_banner_discount)
                            {
                                $elem.find(".box-tag-img").html($('<img />').attr({"class" : "25discount","src":"<?php echo base_url().FRONT_ASSETS;?>images/discount/5off-side-top.png"})); //remove
                                $elem.find(".seat_fare").html($('<span />').attr({"class" : "search_fare_text padr78 price"}).html("<span class='pricefrom'>Starting From</span> Rs."+min_max_service_fare)).append($('<span />').attr({"class" : "line-txt pull-right search_discount_fare_text"}).html("Rs."+discount_min_max_service_fare));
                            }
                            else
                            {
                                if(show_discounted_fare)
                                {
                                    $elem.find(".seat_fare").html($('<span />').attr({"class" : "search_fare_text price"}).html("<span class='pricefrom'>Starting From</span> Rs."+min_max_service_fare)).append($('<span />').attr({"class" : "line-txt pull-right"}).html("Rs."+discount_min_max_service_fare));
                                }
                                else
                                {
                                    $elem.find(".seat_fare").html($('<span />').attr({"class" : "search_fare_text price"}).html("<span class='pricefrom'>Starting From</span> Rs."+min_max_service_fare));  
                                }
                            }
                        }
                        else
                        {
                            $elem.find(".seat_fare").html("<span class='pricefrom'>Starting From&nbsp;&nbsp;</span> <span class='search_fare_text price floatr'>Rs."+min_max_service_fare+"</span>");
                        }
                    }
                    else
                    {
                        if(is_discount_on && is_service_discount)
                        {
                            // $elem.find(".seat_fare").html('<span class="line-txt">Rs.'+parseFloat(value.actual_seat_fare).toFixed(2)+'</span> Rs.'+parseFloat(value.seat_fare).toFixed(2)).addClass("right-amount-txt");
                            
                            // //$elem.find(".seat_fare").html($('<span />').attr({"class" : "search_fare_text padr78 price"}).html("Rs."+parseFloat(value.seat_fare).toFixed())).append($('<span />').attr({"class" : "line-txt pull-right search_discount_fare_text"}).html("Rs."+parseFloat(value.actual_seat_fare).toFixed()));
                            if(show_banner_discount)
                            {
                                $elem.find(".box-tag-img").html($('<img />').attr({"class" : "25discount","src":"<?php echo base_url().FRONT_ASSETS;?>images/discount/5off-side-top.png"})); //remove
                                $elem.find(".seat_fare").html($('<span />').attr({"class" : "search_fare_text padr78 price"}).html("Rs."+parseFloat(value.seat_fare).toFixed())).append($('<span />').attr({"class" : "line-txt pull-right search_discount_fare_text"}).html("Rs."+parseFloat(value.actual_seat_fare).toFixed()));
                            }
                            else
                            {
                                if(show_discounted_fare)
                                {
                                    $elem.find(".seat_fare").html($('<span />').attr({"class" : "search_fare_text price"}).html("Rs."+parseFloat(value.seat_fare).toFixed())).append($('<span />').attr({"class" : "line-txt pull-right"}).html("Rs."+parseFloat(value.actual_seat_fare).toFixed()));
                                }
                                else
                                {
                                    $elem.find(".seat_fare").html($('<span />').attr({"class" : "search_fare_text price"}).html("Rs."+parseFloat(value.seat_fare).toFixed()));   
                                }
                            }
                        }
                        else
                        {
                            // $elem.find(".seat_fare").html('Rs.'+parseFloat(value.seat_fare).toFixed(2));
                            $elem.find(".seat_fare").html($('<span />').attr({"class" : "search_fare_text price"}).html("Rs."+parseFloat(value.seat_fare).toFixed()));
                        }
                    }
                }
                else
                {
                    $elem.find(".seat_fare").html('NA');
                    //$elem.find(".seat_fare a.getfare").attr({'data-ref':value.trip_no+','+value.op_name});
                }

                $elem.find(".seat_fare").attr({'id': 'seat_fare_'+value.bus_travel_id});

                $elem.find(".tot_seat_available").html(value.tot_seat_available+' Seats').attr('id', 'tot_seat_'+value.bus_travel_id);
                $elem.find(".bus_seat_div").attr("id","bus_travel_id_"+value.bus_travel_id);
                
                //for boarding point and drop point
                $.each(value.barr, function(j,points)
                {
                    var stop_arrivaltime = (points.arrival_duration != "" && points.arrival_duration != "0" && points.arrival_duration != undefined) ? points.arrival_duration+" - " : "";
                    if(points.is_boarding == "Y")
                    {
                        $elem.find(".boarding_point ul.barr").append("<li>"+stop_arrivaltime+points.bus_stop_name+"</li>");
                    }
                    else if(points.is_alighting == "Y")
                    {
                        $elem.find(".droping_point ul.darr").append("<li>"+stop_arrivaltime+points.bus_stop_name+"</li>");
                    }    
                });

                //Check boarding alightning present or not
                if($elem.find(".boarding_point ul.barr").html().trim() == "")
                {
                    $elem.find(".boarding_point ul.barr").append($('<li/>').html(global_response.user_data.from));
                }

                if($elem.find(".droping_point ul.darr").html().trim() == "")
                {
                    $elem.find(".droping_point ul.darr").append($('<li/>').html(global_response.user_data.to));
                }

                if(value.op_name.toLowerCase() != "hrtc")
                {
                    $elem.find(".get_bus_seat").attr({"op-trip-no":value.op_trip_no,
                                                  "data-op-name":value.op_name,
                                                  "data-trip-no":value.trip_no,
                                                  "data-bus-travel-id":value.bus_travel_id,
                                                  "data-from":value.from_stop,
                                                  "data-to":value.to_stop,
                                                  "data-date":service_date
                                                });
                }
                else if(value.op_name.toLowerCase() == "hrtc")
                {
                    $elem.find(".get_bus_seat").attr({"op-trip-no":value.op_trip_no,
                                                  "data-op-name":value.op_name,
                                                  "data-bus-travel-id":value.bus_travel_id,
                                                  "data-trip-no":value.trip_no,
                                                  "data-from":query_string.from,
                                                  "data-to":query_string.to,
                                                  "data-date":service_date
                                                });
                }
                

                if(!value.tot_seat_available)
                {
                    $elem.find('.seat_select_button').addClass("avoid-clicks sold_get_seat_layout").removeClass("btnorange").text("Sold Out").css({"cursor":"auto","background-color":"#989898"});
                }

                $elem.attr({"op-trip-no":value.op_trip_no,
                            "data-op-name":value.op_name,
                            "data-trip-no":value.trip_no,
                            "data-bus-travel-id":value.bus_travel_id,
                            "data-from":value.from_stop,
                            "data-to":value.to_stop,
                            "data-date":service_date,
                            "data-provider-id":value.provider_id,
                            "data-provider-type":value.provider_type,
                            "data-op-id":value.op_id,
                            "data-travel-duration":(value.bus_travel_duration != undefined) ? value.bus_travel_duration : "",
                            /*"api-type" : value.api_type,
                            "api-name" : value.api_name,*/
                            "inventory-type" : value.inventory_type
                          });

                /********************* For MSRTC Start******************/
                if(value.op_name.toLowerCase() == "msrtc")
                {
                    var msrtc_boarding_alighting_tpl = $.parseHTML($.trim($("#msrtc_boarding_alighting_section").html()));
                    var $msrtc_elem = $(msrtc_boarding_alighting_tpl);
                    $msrtc_elem.attr({"id":"show_"+value.bus_travel_id});
                    
                    $elem.find(".get_bus_seat").removeClass("get_bus_seat").addClass("view_bus_service_stops");
                    $msrtc_elem.find(".msrtc_view_seats").addClass("get_bus_seat");
                    $msrtc_elem.find(".msrtc_view_seats").attr({"op-trip-no":value.op_trip_no,
                                                                "data-bus-travel-id":value.bus_travel_id,
                                                                  "data-op-name":value.op_name,
                                                                  "data-trip-no":value.trip_no,
                                                                  "data-from":value.from_stop,
                                                                  "data-to":value.to_stop,
                                                                  "data-date":service_date
                                                                }); 
                    $($msrtc_elem).insertBefore($elem.find(".bus_seat_div"));
                }
                /********************* For MSRTC End******************/
                if(value.op_id > 0)
                {
                    $elem.removeClass("sort_list").addClass("stu_sort_list");
                    $("#bus_service_ul_id #stu_bus_services").append($elem);
                }
                else
                {
                    $("#bus_service_ul_id #private_bus_services").append($elem);
                }

                global_bus_services = $("#bus_service_ul_id  article");
                if(value.provider_type == "operator")
                {
                    global_stu_bus_services = $("#bus_service_ul_id  #stu_bus_services > article");
                }
                else
                {
                    global_private_bus_services = $("#bus_service_ul_id  #private_bus_services > article")
                }
                    
                /*$("#bus_service_ul_id").append($elem);
                global_bus_services = $("#bus_service_ul_id > article");*/
            });

            // $("#show_waiting").modal("hide");
            $.loader("close");
        }
        /***************** render bus service end ********************/

        /***************** msrtc Start ********************/
        $(document).off('click', '.view_bus_service_stops').on('click', '.view_bus_service_stops', function(e) {
            
            var trip_no = $(this).attr("data-trip-no");
            var bus_travel_id = $(this).attr("data-bus-travel-id");
            
            $(".view_bus_service_stops").text("Select Seat");

            if($("#show_"+bus_travel_id).is(':hidden'))
            {
                var $current_object = $(this);
                $(".msrtc_boarding_alighting_div,.bus_seat_div").css({"display":"none"});
                $(this).text("Hide Seat");
                $(this).removeClass("btnorange").addClass("bg_gray").text("Please Wait");

                var detail = query_string;
                var div = "";
                var ajax_url = 'front/booking/get_bus_service_stop_details';
                var form = '';

                detail['date']          = $(this).attr("data-date");
                detail['op_trip_no']    = $(this).attr("op-trip-no");

                get_data(ajax_url, form, div, detail, function(response)
                {
                    $(".article_"+bus_travel_id+" .view_bus_service_stops").removeClass("bg_gray").addClass("btnorange").text("Hide Seat");
                    if(response.status == "success")
                    {
                        $.each(response.from_service_stops, function(dk,dval)
                        {
                            if($("#show_"+bus_travel_id+" .msrtc_boarding_select option[value='"+dval.from_bus_stop_code+"']").length == 0)
                            {
                                $("#show_"+bus_travel_id+" .msrtc_boarding_select").append($('<option/>').attr({"value":dval.from_bus_stop_code,"data-name":dval.from_bus_stop_name,"data-trip-no":dval.trip_no}).html(dval.from_bus_stop_name+" - "+dval.from_time));
                            }

                            $("#show_"+bus_travel_id+" select").trigger("chosen:updated");
                        });

                        $.each(response.to_service_stops, function(dtk,dtval)
                        {
                            if($("#show_"+bus_travel_id+" .msrtc_alighting_select option[value='"+dtval.to_bus_stop_code+"']").length == 0)
                            {
                               $("#show_"+bus_travel_id+" .msrtc_alighting_select").append($('<option/>').attr({"value":dtval.to_bus_stop_code,"data-name":dtval.to_bus_stop_name}).html(dtval.to_bus_stop_name+" - "+dtval.to_time));
                            }   

                            $("#show_"+bus_travel_id+" select").trigger("chosen:updated");
                        });

                        $(".chosen_select").chosen({ width: '100%' });
                        $("#show_"+bus_travel_id).css({"display":"block"});
                    }
                    else
                    {
                        $current_object.removeClass("btnorange").addClass("bg_gray").text("Please Wait");
                        fetch_new_bus_service_stops(detail,trip_no);
                        /*$(".view_bus_service_stops").text("Select Seat");
                        showNotification("danger","Seat layout not available. Please select another bus.");*/
                    }
                }, '', false);
            }
            else
            {
                $("#show_"+bus_travel_id).css({"display":"none"});
                $("#bus_travel_id_"+bus_travel_id).css({"display":"none"});
            }
        });
        /***************** msrtc End ********************/


        /***************** booking ticket / continue start here **********/
        var skip_form_url = ''
        $(document).off('click', '.continue').on('click', '.continue', function(e) {
            var form_url = '';
            var its_reference_number = '';
            var trip_no = $(this).attr("data-trip-no");
            var bus_travel_id = $(this).attr("data-bus-travel-id");
            var provider_id = $('.article_'+bus_travel_id).attr("data-provider-id");
            var provider_type = $('.article_'+bus_travel_id).attr("data-provider-type");
            if(provider_id != 'undefined' && provider_id == 3) {
                its_reference_number = $('.article_'+bus_travel_id).attr("op-trip-no");
            }
            
            if(provider_id != "" && provider_type != "") {
                if(Object.keys(onwardJourny).length == 0) {
                    if(temp_seats != "" && temp_seats != undefined && Object.keys(temp_seats).length) {
                        onwardJourny['from'] = $('.article_'+bus_travel_id).attr("data-from");
                        onwardJourny['to'] = $('.article_'+bus_travel_id).attr("data-to");
                        onwardJourny['date'] = $('#date_of_jour').val();
                        onwardJourny['date_of_jour'] = $('#date_of_jour').val();
                        if(provider_id != 'undefined' && provider_id == 3) {
                            onwardJourny['its_reference_number'] = $('.article_'+bus_travel_id).attr("op-trip-no");
                        }
                        if($('.article_'+bus_travel_id).attr("data-travel").toLowerCase() == "msrtc") {
                            onwardJourny['boarding_stop_name'] = $('#show_'+bus_travel_id+' .msrtc_boarding_input').attr("data-name");
                            onwardJourny['boarding_stop_cd'] = $('#show_'+bus_travel_id+' .msrtc_boarding_input').val();
                            onwardJourny['destination_stop_name'] = $('#show_'+bus_travel_id+' .msrtc_alighting_input').attr("data-name");
                            onwardJourny['destination_stop_cd'] = $('#show_'+bus_travel_id+' .msrtc_alighting_input').val();
                            onwardJourny['trip_no'] = $('#show_'+bus_travel_id+' select.msrtc_boarding_select option:selected').attr("data-trip-no");
                        }
                        else if($('.article_'+bus_travel_id).attr("data-travel").toLowerCase() == "hrtc") {
                            onwardJourny['boarding_stop_name'] = $('#bus_travel_id_'+bus_travel_id+' select.bp_dropdown').val();
                            onwardJourny['destination_stop_name'] = $('#bus_travel_id_'+bus_travel_id+' select.dp_dropdown').val();
                            onwardJourny['boarding_stop_cd'] = $('#bus_travel_id_'+bus_travel_id+' select.bp_dropdown option:selected').attr('data-code');
                            onwardJourny['destination_stop_cd'] = $('#bus_travel_id_'+bus_travel_id+' select.dp_dropdown option:selected').attr('data-code');
                            onwardJourny['trip_no'] = trip_no;
                        }
                        else {
                            onwardJourny['boarding_stop_name'] = $('#bus_travel_id_'+bus_travel_id+' select.bp_dropdown').val();
                            onwardJourny['destination_stop_name'] = $('#bus_travel_id_'+bus_travel_id+' select.dp_dropdown').val();
                            onwardJourny['boarding_stop_cd'] = $('#bus_travel_id_'+bus_travel_id+' select.bp_dropdown option:selected').attr('data-code');
                            onwardJourny['destination_stop_cd'] = $('#bus_travel_id_'+bus_travel_id+' select.dp_dropdown option:selected').attr('data-code');
                            onwardJourny['trip_no'] = trip_no;
                        }
                        
                        onwardJourny['seats'] = temp_seats;
                        onwardJourny['op_name'] = $(".article_"+bus_travel_id).attr("data-travel");
                        onwardJourny['op_id'] = $(".article_"+bus_travel_id).attr("data-op-id");
			            onwardJourny['bus_type_id'] = $(".article_"+bus_travel_id).attr("data-bus-type");
                        onwardJourny['bsd'] = $('#bus_travel_id_'+bus_travel_id+' select.bp_dropdown option:selected').attr('data-bsd');
                        onwardJourny['dsd'] = $('#bus_travel_id_'+bus_travel_id+' select.dp_dropdown option:selected').attr('data-dsd');
                        

                        // onwardJourny['inventory_type']          = $(".article_"+trip_no).attr("inventory-type");
                        onwardJourny['provider_id'] = provider_id;
                        onwardJourny['provider_type'] = provider_type;
                        
                        temp_seats = ''; //IMP DONT REMOVE. IF NECESSARY CHECK EFFECT AND THEN REMOVE

                        if($.trim($("#date_of_arr").val()) == "") {
                            form_url = base_url+'front/booking/temp_booking';
                        }
                        else {
                            rejour_obj = {};
                            // rejour_obj.from = $('.article_'+trip_no).attr("data-to");
                            // rejour_obj.to = $('.article_'+trip_no).attr("data-from");

                            //To Skip Return Journey
                            skip_form_url = base_url+'front/booking/temp_booking?'+$.param(onwardJourny);

                            rejour_obj.from = $('#to1').val();
                            rejour_obj.to = $('#from1').val();
                            rejour_obj.date_of_jour = $('#date_of_arr').val();
                            rejour_obj.is_return_journey = true;
                            rejour_obj.triptype = (query_string.triptype != "" && query_string.triptype != undefined) ? query_string.triptype : "";
                            rejour_obj.provider = (query_string.provider != "" && query_string.provider != undefined) ? query_string.provider : "";
                            rejour_obj.<?php echo $this->security->get_csrf_token_name(); ?> = '<?php echo $this->security->get_csrf_hash(); ?>';
                            $("#next-pre-search .next-pre").addClass( "opacity-03 inactive-link" );
                            $("#next-pre-search .return_next-pre").removeClass("opacity-03 inactive-link");
                            get_bus_services(rejour_obj);
                        }
                    }
                    else {
                        // bootstrapGrowl("danger","Please select seat", {"delay":"2000"});
                        showNotification("danger","Please select seat");
                    }
                }
                else if(Object.keys(returnJourny).length == 0) {
                    if(temp_seats != "" && temp_seats != undefined && Object.keys(temp_seats).length) {
                        returnJourny['rfrom'] = $('.article_'+bus_travel_id).attr("data-from");
                        returnJourny['rto'] = $('.article_'+bus_travel_id).attr("data-to");
                        returnJourny['rdate'] = $('#date_of_arr').val();
                        if(provider_id != 'undefined' && provider_id == 3) {
                            returnJourny['r_its_reference_number'] = $('.article_'+bus_travel_id).attr("op-trip-no");
                        }
                        if($('.article_'+bus_travel_id).attr("data-travel").toLowerCase() == "msrtc") {
                            returnJourny['rboarding_stop_name'] = $('#show_'+bus_travel_id+' select.msrtc_boarding_select option:selected').attr("data-name");
                            returnJourny['rdestination_stop_name'] = $('#show_'+bus_travel_id+' select.msrtc_alighting_select option:selected').attr("data-name");
                            returnJourny['rboarding_stop_cd'] = $('#show_'+bus_travel_id+' select.msrtc_boarding_select option:selected').val();
                            returnJourny['rdestination_stop_cd'] = $('#show_'+bus_travel_id+' select.msrtc_alighting_select option:selected').val();
                            returnJourny['rtrip_no'] = $('#show_'+bus_travel_id+' select.msrtc_boarding_select option:selected').attr("data-trip-no");
                            
                        }
                        else {
                            returnJourny['rboarding_stop_name'] = $('#bus_travel_id_'+bus_travel_id+' select.bp_dropdown').val();
                            returnJourny['rdestination_stop_name'] = $('#bus_travel_id_'+bus_travel_id+' select.dp_dropdown').val();
                            returnJourny['rboarding_stop_cd'] = $('#bus_travel_id_'+bus_travel_id+' select.bp_dropdown option:selected').attr("data-code");
                            returnJourny['rdestination_stop_cd'] = $('#bus_travel_id_'+bus_travel_id+' select.dp_dropdown option:selected').attr("data-code");
                            returnJourny['rtrip_no'] = trip_no;
                        }

                        returnJourny['rseats'] = temp_seats;
                        returnJourny['rop_name'] = $(".article_"+bus_travel_id).attr("data-travel");
                        returnJourny['rop_id'] = $(".article_"+bus_travel_id).attr("data-op-id");
			returnJourny['rbus_type_id'] = $(".article_"+bus_travel_id).attr("data-bus-type");
                        returnJourny['rbsd'] = $('#bus_travel_id_'+bus_travel_id+' select.bp_dropdown option:selected').attr('data-bsd');
                        returnJourny['rdsd'] = $('#bus_travel_id_'+bus_travel_id+' select.dp_dropdown option:selected').attr('data-dsd');
                        
                        // returnJourny['rinventory_type']         = $(".article_"+trip_no).attr("inventory-type");
                        returnJourny['rprovider_id'] = provider_id;
                        returnJourny['rprovider_type'] = provider_type;

                        
                        form_url = base_url+'front/booking/temp_booking';
                    }
                    else {
                        showNotification("danger","Please select seat");
                    }
                }

                if(form_url != "") {
                    var query_str = $.param(onwardJourny);  
                    if(Object.keys(returnJourny).length > 0) {
                        query_str += "&"+$.param( returnJourny );
                    }
                    
                    window.location.href = base_url+'front/booking/temp_booking?'+query_str;   
                }
            }
            else {
                showNotification("danger","Something went wrong. Please refresh page.");
            }
           
        });
        /***************** booking ticket / continue end here **********/

        //include date compare after
        /*var js_date = date_operations(current_date(), query_string.date_of_jour);
        js_date.date_compare ? get_bus_services(query_string) : no_result_found();*/
        //var dojiso = moment(moment(query_string.date_of_jour,"DD-MM-YYYY")).toISOString();
        //var is_service_true = moment(moment(dojiso).format("YYYY-MM-DD")).diff(todays_date) >= 0;
        // (is_service_true) ? get_bus_services(query_string) : no_result_found();
        /*if(is_service_true)
        {
            get_bus_services(query_string);
        }
        else
        {
            showNotification("error","Please check onward and return journey date.");
            no_result_found();
        }*/
        
        /******* To get bus service end here *******/


        /******* get fare start here *******/
        $(document).off('click', '.getfare').on('click', '.getfare', function(e) {
            e.preventDefault();
            var ind = $(".getfare").index(this);
            var getfare = $(this).attr('data-ref');
           
            var detail = query_string;
            var div = "";
            var ajax_url = 'front/booking/get_seat_fare';
            var form = '';

            trip_op_data = getfare.split(",");

            detail['date'] = query_string.date_of_jour;
            detail['trip_no'] = trip_op_data[0];
            detail['op_name'] = trip_op_data[1];


            get_data(ajax_url, form, div, detail, function(response)
            {
               $('#tot_seat_'+trip_op_data[0]).html( response.tot_seats + " Seats" );
               $('#seat_fare_'+trip_op_data[0]).html('Rs. '+response.fare);

            }, '', false);

        });
        /******* get fare end here *******/


        /******** Modify Search Start *********/
        $(".advanceSearch").click(function(){
            $("#"+$(this).attr("data-attr")).slideUp();
            $("#"+$(this).attr("attr-data")).slideDown();
        });


        $(document).off('click', '#submit_filter_btn').on('click', '#submit_filter_btn', function(e) {

            if($('#modify_search_form').valid())
            {
                var cFilter = {};
                onwardJourny = {}; /*VERY IMP DONT REMOVE onwardJourny*/
                var trip_type = "single";
                var sDate = $('#date_of_jour').val();
                var eDate = $('#date_of_arr').val();
                
                var token ='<?php echo $this->security->get_csrf_hash(); ?>';
                
                if(eDate != "" && eDate != undefined)
                {
                    trip_type = "round";
                }

                cFilter.date_of_jour = sDate;
                cFilter.date_of_arr = eDate;
                cFilter.from = $('#from1').val();
                cFilter.to = $('#to1').val();
                cFilter.<?php echo $this->security->get_csrf_token_name(); ?> = token;
                var ms_searchbustype = "all";
                var ms_provider = "all";

                if($('#searchbustype_selected').text().toLowerCase() == "ac")
                {
                    ms_searchbustype = "ac";
                }
                else if($('#searchbustype_selected').text().toLowerCase() == "non ac")
                {
                    ms_searchbustype = "non_ac";
                }

                if($('#provider_selected').text().toLowerCase() == "state transport operator")
                {
                    ms_provider = "operator";
                }
                else if($('#provider_selected').text().toLowerCase() == "private operator")
                {
                    ms_provider = "api";
                }

                cFilter.searchbustype = ms_searchbustype;
                cFilter.provider = ms_provider;
                //set triptype to single
                
                cFilter.triptype = trip_type;

                //set from and to value
                query_string.from = cFilter.from;
                query_string.to = cFilter.to;
                query_string.searchbustype = cFilter.searchbustype;
                query_string.provider = cFilter.provider;
                $('#from1').attr("value",cFilter.from)
                $('#to1').attr("value",cFilter.to)

                //Set Cookies
                $.cookie("travel_from", $('#from1').val(), { expires : 7776000, path    : '/'});
                $.cookie("travel_to", $('#to1').val(), { expires : 7776000, path    : '/'});

                // query_string = cFilter;
                query_string.date_of_arr = (eDate != "") ? eDate : "";
                

                var current_pageurl = window.location;
                var url_base = current_pageurl.origin+current_pageurl.pathname;
                var url_string = url_base+"?from="+cFilter.from.trim()+"&to="+cFilter.to.trim()+"&date_of_jour="+sDate.trim()+"&date_of_arr="+eDate.trim()+"&provider="+ms_provider+"&triptype="+trip_type+"&searchbustype="+ms_searchbustype+"&Search=Search";
                window.history.pushState({path:url_string},'',url_string);

                // var date_formats = DateJavascriptFormat(sDate,1,1);
                var sdateIso            = moment(moment(sDate,"DD-MM-YYYY")).toISOString();
                var sdateymd            = moment(sdateIso).format('YYYY-MM-DD');
                /****** Do not remove Commented code. It may be used later ******/
                $("#date_shown").html(moment(sdateymd).format("DD-MMM-YYYY"));
                $("#date_of_jour").val(moment(sdateymd).format("DD-MM-YYYY"));
                $("#prev").attr('data-ref',moment(moment(sdateymd).subtract(1, 'day')).format("DD-MM-YYYY"));
                $("#next").attr('data-ref',moment(moment(sdateymd).add(1, 'day')).format("DD-MM-YYYY"));
                $("#show_from").html(cFilter.from);
                $("#show_to").html(cFilter.to);

                $("#return_show_from").html(cFilter.to);
                $("#return_show_to").html(cFilter.from);

                if(eDate != "")
                {
                    // var return_date_formats = DateJavascriptFormat(eDate,1,1);
                    var edateIso            = moment(moment(eDate,"DD-MM-YYYY")).toISOString();
                    var edateymd            = moment(edateIso).format('YYYY-MM-DD');
                    /*-- Return Journey --*/
                    $("#return_date_shown").html(moment(edateymd).format("DD-MMM-YYYY"));
                    $("#date_of_arr").val(moment(edateymd).format("DD-MM-YYYY"));
                    $("#return_prev").attr('data-ref',moment(moment(edateymd).subtract(1, 'day')).format("DD-MM-YYYY"));
                    $("#return_next").attr('data-ref',moment(moment(edateymd).add(1, 'day')).format("DD-MM-YYYY"));
                    $("#return_show_from").html(cFilter.to);
                    $("#return_show_to").html(cFilter.from);

                    $("#next-pre-search .next-pre").removeClass( "col-sm-12 col-md-12" ).addClass( "col-sm-6 col-md-6" );
                    $("#next-pre-search .return_next-pre").removeClass("display-none").addClass( "col-sm-6 col-md-6 display-block opacity-03 inactive-link" );
                }
                else
                {
                    $("#next-pre-search .next-pre").addClass( "col-sm-12 col-md-12" ).removeClass( "col-sm-6 col-md-6" );
                    $("#next-pre-search .return_next-pre").removeClass( "col-sm-6 col-md-6 display-block" ).addClass("display-none"); 
                }

                //get_bus_services(cFilter);
                /*var js_date = date_operations(current_date(), sDate);
                js_date.date_compare ? get_bus_services(cFilter) : no_result_found();
                */
                var sdateiso = moment(moment(sDate,"DD-MM-YYYY")).toISOString();
                var is_bus_service = moment(sdateiso).diff(todays_date) >= 0;
                (is_bus_service) ? get_bus_services(cFilter) : no_result_found();
            }
        });
        /******** Modify Search End *********/


        /******* Next & Previous start here *******/
        $(document).off('click','.search_date').on('click','.search_date', function(){
            var from = $("#from1").val();
            var to = $("#to1").val();
            
            var sDate           = $(this).attr('data-ref');
            var date_of_jour    = $("#date_of_jour").val();
            var date_of_arr     = $("#date_of_arr").val();

            var sDate_iso           = moment(moment(sDate,"DD-MM-YYYY")).toISOString();
            var date_of_jour_iso    = moment(moment(date_of_jour,"DD-MM-YYYY")).toISOString();
            var date_of_arr_iso     = moment(moment(date_of_arr,"DD-MM-YYYY")).toISOString();

            var sDate_ymd           = moment(sDate_iso).format('YYYY-MM-DD');
            var date_of_jour_ymd    = moment(date_of_jour_iso).format('YYYY-MM-DD');
            var date_of_arr_ymd     = moment(date_of_arr_iso).format('YYYY-MM-DD');

            // var date_formats;
            var is_true = true;
            var journey_date_compare = {};

            /*show(moment(discount_till,"YYYY-MM-DD").toDate());
            show(moment(moment(moment("21-11-2015","DD-MM-YYYY")).toISOString()).format('YYYY-MM-DD'));*/

            var cFilter         = $.extend({},query_string);

            cFilter.date_of_jour    = sDate;
            cFilter.<?php echo $this->security->get_csrf_token_name(); ?> = '<?php echo $this->security->get_csrf_hash(); ?>';
            var bus_triptype        = (query_string.triptype != "" && query_string.triptype != undefined) ? query_string.triptype : "";
            var bus_provider        = (query_string.provider != "" && query_string.provider != undefined) ? query_string.provider : "";
            var searchbustype        = (query_string.searchbustype != "" && query_string.searchbustype != undefined) ? query_string.searchbustype : "";
            // date_formats = DateJavascriptFormat(sDate,1,1);

            if(Object.keys(onwardJourny).length == 0)
            {
                if(date_of_arr != "")
                {
                    /*journey_date_compare = date_operations(date_formats.full_search_date,date_of_arr)
                    is_true = journey_date_compare.date_compare;*/
                    // is_true     =   moment(date_of_arr_ymd).isAfter(sDate_ymd);
                    // show(moment('2015-11-25').diff('2015-11-24') >= 0);
                    is_true     =   moment(date_of_arr_ymd).diff(sDate_ymd) >= 0;
                }

                if(is_true)
                {
                    /*var js_date = date_operations(current_date(), sDate);
                    if(js_date.date_compare)*/
                    var date_greater_equal = moment(sDate_ymd).diff(todays_date) >= 0;
                    if(date_greater_equal)
                    {
                        // to change url
                        var current_pageurl = window.location;
                        var url_base = current_pageurl.origin+current_pageurl.pathname;
                        // var url_string = url_base+"?from="+from.trim()+"&to="+to.trim()+"&date_of_jour="+sDate.trim()+"&date_of_arr="+date_of_arr.trim()+"&Search=Search";
                        var url_string = url_base+"?from="+from.trim()+"&to="+to.trim()+"&date_of_jour="+sDate.trim()+"&date_of_arr="+date_of_arr.trim()+"&triptype="+bus_triptype.trim()+"&provider="+bus_provider.trim()+"&searchbustype="+searchbustype+"&Search=Search";
                        window.history.pushState({path:url_string},'',url_string);
                        $("#bus_service_ul_id").html("");
                        /*$("#date_shown").html(date_formats.search_full_date);
                        $("#date_of_jour").val(date_formats.full_search_date);
                        $("#prev").attr('data-ref',date_formats.prev_full_date);
                        $("#next").attr('data-ref',date_formats.next_full_date);
                        */
                        var next_date = moment(moment(sDate_ymd).add(1, 'day')).format("DD-MM-YYYY")
                        var previous_date = moment(moment(sDate_ymd).subtract(1, 'day')).format("DD-MM-YYYY")

                        $("#date_shown").html(moment(sDate_ymd).format('DD-MMM-YYYY'));
                        $("#date_of_jour").val(moment(sDate_ymd).format('DD-MM-YYYY'));
                        $("#prev").attr('data-ref',previous_date);
                        $("#next").attr('data-ref',next_date);
                    }
                    else
                    {
                        showNotification("error","Journey must be on or after today's date")
                    }
                }
                else
                {
                    showNotification("error","Onward journey must be before return journey");
                }
            }
            else
            {
                if(is_true)
                {
                    /*journey_date_compare = date_operations(date_of_jour,date_formats.full_search_date)
                    is_true = journey_date_compare.date_compare;
                    if(is_true)*/

                    // journey_date_compare = date_operations(date_of_jour,date_formats.full_search_date)
                    is_true = moment(sDate_ymd).diff(date_of_jour_ymd) >= 0;
                    if(is_true)
                    {
                        /*var js_date = date_operations(current_date(), date_of_arr);
                        if(js_date.date_compare)
                        {*/
                        var date_greater_equal = moment(date_of_arr_ymd).diff(todays_date) >= 0;
                        if(date_greater_equal)
                        {
                            cFilter.from = to;
                            cFilter.to = from;
                            var current_pageurl = window.location;
                            var url_base = current_pageurl.origin+current_pageurl.pathname;
                            // var url_string = url_base+"?from="+from.trim()+"&to="+to.trim()+"&date_of_jour="+date_of_jour.trim()+"&date_of_arr="+sDate.trim()+"&Search=Search";
                            var url_string = url_base+"?from="+from.trim()+"&to="+to.trim()+"&date_of_jour="+date_of_jour.trim()+"&date_of_arr="+sDate.trim()+"&triptype="+bus_triptype.trim()+"&provider="+bus_provider.trim()+"&searchbustype="+searchbustype+"&Search=Search";
                            window.history.pushState({path:url_string},'',url_string);
                            $("#bus_service_ul_id").html("");
                            /*$("#return_date_shown").html(date_formats.search_full_date);
                            $("#date_of_arr").val(date_formats.full_search_date);
                            $("#return_prev").attr('data-ref',date_formats.prev_full_date);
                            $("#return_next").attr('data-ref',date_formats.next_full_date);*/

                            var date_next = moment(moment(sDate_ymd).add(1, 'day')).format("DD-MM-YYYY")
                            var date_previous = moment(moment(sDate_ymd).subtract(1, 'day')).format("DD-MM-YYYY")

                            $("#return_date_shown").html(moment(sDate_ymd).format('DD-MMM-YYYY'));
                            $("#date_of_arr").val(moment(sDate_ymd).format('DD-MM-YYYY'));
                            $("#return_prev").attr('data-ref',date_previous);
                            $("#return_next").attr('data-ref',date_next);
                        }
                        else
                        {
                            showNotification("error","Return journey must be after or on today's date.")
                        }
                    }
                    else
                    {
                        showNotification("error","Return journey must be after onward journey");
                    }
                }
                else
                {
                    showNotification("error","Return journey must be after onward journey");
                }
            }

            $("#travel").html("");

            if(is_true)
            {
                date_greater_equal ? get_bus_services(cFilter) : showNotification("error","Please provide proper journey date.");
            }
            //get_bus_services(cFilter);
        });
        /******* Next & Previous end here *******/

        /******* Filter start here *******/
        // $(document).off('click','.filter li a').on('click','.filter li a', function(e){
        $(document).off('click','.filter li').on('click','.filter li', function(e){
            
            e.preventDefault();
            
            var t1 = global_bus_services;
            var stu_t1 = [];        
            var t2 = [];        
            var stu_grouping_structure_tpl = $.parseHTML($.trim($("#stu_grouping_structure_tpl").html()));
            $('#bus_service_ul_id').html('');
            $("#bus_service_ul_id").html(stu_grouping_structure_tpl);

            if($('.filter li.active').length > 0)
            {
                $('.filter').each(function(pkey, pval){

                    var search_data = new Array();

                    $(this).find("li.active a").each(function(lkey, lval){
                        search_data.push($(this).attr('data-attr'));
                    });
                    
                    var col = $(this).attr('data-ref');
                    if(search_data != null && search_data.length >0  && t1.length >0 ) 
                    {       
                        t2 = t1.filter( function(index, elem){
                            
                            if($.inArray($(this).attr('data-'+col), search_data) !== -1)
                            {
                                // return $(this);
                                if(parseFloat($(this).attr('data-op-id').trim()) > 0)
                                {
                                   stu_t1.push($(this));
                                }
                                else
                                {
                                    return $(this);
                                }
                            }                       
                        });
                    }             
                });

                $("#no_of_bus_found").html(t2.length+stu_t1.length);
                // $('#bus_service_ul_id').append(t1);
                // (t1.length) ? $('#bus_service_ul_id').append(t1) : no_result_found();
                if(t2.length)
                {
                   $('#bus_service_ul_id #private_bus_services').append(t2);
                }

                if(stu_t1.length)
                {
                    render_filtered_stu_bus_services(stu_t1);
                   $('#bus_service_ul_id #stu_bus_services').append(stu_t1);
                }

                if(!t2.length && !stu_t1.length)
                {
                    no_result_found();
                }
            }
            else
            {
                /*$("#no_of_bus_found").html(global_bus_services.length);
                $('#bus_service_ul_id').html(global_bus_services);*/
                $("#no_of_bus_found").html(global_bus_services.length)
                if(global_response.time_fare.stu_range != undefined && global_response.time_fare.stu_range.stu_total_seats > 0)
                {
                    render_stu_bus_services(global_response.time_fare.stu_range);
                }

                $('#bus_service_ul_id #stu_bus_services').html(global_stu_bus_services);
                $('#bus_service_ul_id #private_bus_services').html(global_private_bus_services);
               
                // render_bus_services(global_response.bus_detail, global_response.user_data.date); 
            }
            return false;
        });


        var render_filtered_stu_bus_services = function(filtered_stu_response)
        {
            var main_filtered_obj = {};
            var filtered_op_name = filtered_stu_response[0].attr("data-op-name");
            var fareobj = [];
            var depobj = [];
            var tradobj = [];
            var filtered_total_seats = 0;

            $.each(filtered_stu_response, function( fskey, fsval ) {
                depobj[fskey] = fsval.attr("data-dep_tm_str");
                fareobj[fskey] = fsval.attr("data-fare");
                filtered_total_seats += parseFloat(fsval.attr("data-seat").trim());
                var data_travel_duration = fsval.attr("data-travel-duration");
                tradobj[fskey] = parseFloat(data_travel_duration);
            });

            tradobj = jQuery.grep(tradobj, function(n, i){
              return (n !== "" && n != null && n > 0);
            });

            main_filtered_obj = {
                                    "min_duration":secondsToHhmm(Math.min.apply(Math,tradobj)),
                                    "max_duration":secondsToHhmm(Math.max.apply(Math,tradobj)),
                                    "min_fare":Math.min.apply(Math,fareobj),
                                    "max_fare":Math.max.apply(Math,fareobj),
                                    "min_dep_tm":Math.min.apply(Math,depobj),
                                    "max_dep_tm":Math.max.apply(Math,depobj),
                                    "start_at":moment(Math.min.apply(Math,depobj)).format('HH:mm A'),
                                    "mticket_allowed":false,
                                    "op_name_array":[filtered_op_name.toLowerCase()],
                                    "stu_total_seats":filtered_total_seats,
                                    "stu_total_buses":filtered_stu_response.length
                                };

            
            render_stu_bus_services(main_filtered_obj);

        }
        /******* Filter start end *******/


        /******** Sort Column Start**********/
        $(document).off('click', '.sort_col').on('click', '.sort_col', function(e) {
            e.preventDefault();

            $(".sort_col").removeClass("active");
            $(this).addClass("active");

            var col = $(this).attr('ref').trim();

            var stype = $(this).attr('data-sort').trim();

            $(this).attr('data-sort', stype == 'asc' ? 'desc' : 'asc');
            
            $('.sort_list').sort(function(a, b) {

                if(col != "data-travel")
                {
                    var a = parseFloat($(a).attr(col).trim());
                    var b = parseFloat($(b).attr(col).trim());
                }
                else
                {
                    var a = String($(a).attr(col).trim());
                    var b = String($(b).attr(col).trim());
                }
                
                
                if (stype == 'asc')
                {
                    return a > b ? 1 : -1;
                }
                else
                {
                    return a > b ? -1 : 1;
                }
            });


            $('.stu_sort_list').sort(function(a, b) {

                if(col != "data-travel")
                {
                    var a = parseFloat($(a).attr(col).trim());
                    var b = parseFloat($(b).attr(col).trim());
                }
                else
                {
                    var a = String($(a).attr(col).trim());
                    var b = String($(b).attr(col).trim());
                }
                
                if (stype == 'asc')
                {
                    return a > b ? 1 : -1;
                }
                else
                {
                    return a > b ? -1 : 1;
                }
            });
        });
        /******** Sort Column End************/

         /*Fare range slider start here*/
        $("#fare_range").slider({
            range: true,
            min: min_fare,
            max: max_fare,
            values: [min_fare, max_fare],
            slide: function(event, ui) {
                var temp_result = 0;
                var stu_temp_articles = [];
                $('.fare_sort_list').each(function(index) {
                    if (parseFloat($(this).attr('data-fare')) >= parseFloat(ui.values[0]) && parseFloat($(this).attr('data-fare')) <= parseFloat(ui.values[1]))
                    {
                        temp_result++;
                        
                        if(parseFloat($(this).attr('data-op-id')) > 0)
                        {
                            stu_temp_articles.push($(this));
                        }

                        if($('#bus_service_ul_id').find('div.no_routes_found').length)
                        {
                            $('#bus_service_ul_id').find('div.no_routes_found').remove();
                        }

                        $(this).show();
                    }
                    else
                    {   
                        if(!temp_result)
                        {
                            if(!($('#bus_service_ul_id').find('div.no_routes_found').length))
                            {
                                $("#bus_service_ul_id").append($("#no_routes_found_tpl").html());
                            }
                        }

                        $(this).hide();
                    }
                });

                if(!stu_temp_articles.length)
                {
                   $("#stu_bus_summary").css({"display":"none"}); 
                }
                else
                {
                    render_filtered_stu_bus_services(stu_temp_articles);
                    $("#stu_bus_summary").css({"display":"block"});
                }

                if($("#stu_bus_services article:visible").length > 0)
                {
                    $("#stu_bus_summary").find(".stu_grouping_plus_minus i.fa").addClass("fa-minus-circle");
                }

                $(".min-price-label").html( "<i class='fa fa-inr'></i> " + ui.values[ 0 ] );
                $(".max-price-label").html( "<i class='fa fa-inr'></i> " + ui.values[ 1 ]);
                $("#no_of_bus_found").html(temp_result);
            }
        });

        $(".min-price-label").html( "<i class='fa fa-inr'></i> " + $("#fare_range").slider("values", 0) );
        $(".max-price-label").html( "<i class='fa fa-inr'></i> " + $("#fare_range").slider("values", 1));
        /*Fare range slider ends here*/



        /******* Seat Layout start here *******/
        $(document).off('click', '.get_bus_seat').on('click', '.get_bus_seat', function(e) {

            // e.preventDefault();
            var trip_no = $(this).attr("data-trip-no");
            var bus_travel_id = $(this).attr("data-bus-travel-id");
            var op_name = $(this).attr("data-op-name");
            var ind = $(".get_bus_seat").index(this);
            var is_service_discount = eval($(".article_"+bus_travel_id).attr("data-service-discount"));
            
            $('ul li.seat.selected').removeClass('selected');
            $('.bus_seat_div').not($('.bus_seat_div').eq(ind)).hide();
            
            if(op_name.toLowerCase() == "msrtc") {
                if($('#show_'+bus_travel_id+' select.msrtc_boarding_select option:selected').val() == "") {
                    showNotification("danger","Please select boarding point.");
                    return false;
                }

                if($('#show_'+bus_travel_id+' select.msrtc_alighting_select option:selected').val() == "") {
                    showNotification("danger","Please select dropping point.");
                    return false;
                }
                $('.bus_seat_div').eq(ind).show();
            }
            else {
                $('.bus_seat_div').eq(ind).toggle();
            }

            //Toggle Name
            if ($(this).text().trim() == "Select Seat") {
                $.loader({
                    content:{
                        defaultText: null,
                        defaultLoaderImage: base_url+"assets/travelo/front/images/loader/bus_animate.gif"
                    },
                });
                // $(".get_bus_seat").text("Select Seat");
                $(".get_bus_seat").not(".sold_get_seat_layout").text("Select Seat");
                if(op_name.toLowerCase() != "msrtc") {
                    $(this).text("Hide Seat")
                }
            }
            else {
                $(this).text("Select Seat");
            }

            //REDUCED HIT HERE
            (!$(this).hasClass("get_bus_seat_open") || op_name.toLowerCase() == "msrtc") ? $(this).addClass("get_bus_seat_open") : $(this).removeClass("get_bus_seat_open");

            if($(this).hasClass("get_bus_seat_open")) {
                var detail = {};
                var query_string_copy = $.extend({},query_string);
                var div = "";
                var provider_id = $(".article_"+bus_travel_id).attr("data-provider-id");
                var provider_type = $(".article_"+bus_travel_id).attr("data-provider-type");
                var op_id = $(".article_"+bus_travel_id).attr("data-op-id");
		var bus_type_id = $(".article_"+bus_travel_id).attr("data-bus-type");
                var op_bus_type_cd = $(".article_"+bus_travel_id).attr("data-op-bus-type-cd");
                var form = "";
                var ajax_url = "front/booking/get_seat_layout";
                detail['trip_no'] = trip_no;
                detail['provider_id'] = provider_id;
                detail['provider_type'] = provider_type;

                detail.date = $(this).attr("data-date");
                detail.from = $(this).attr("data-from");
                detail.to = $(this).attr("data-to");
                detail['op_name'] = op_name;
                detail['op_id'] = op_id;
                detail.<?php echo $this->security->get_csrf_token_name(); ?> = '<?php echo $this->security->get_csrf_hash(); ?>';
		        detail['bus_type_id'] = bus_type_id;
                detail['op_bus_type_cd'] = op_bus_type_cd;
                detail['op_trip_no'] = $(".article_"+bus_travel_id).attr("op-trip-no");
                detail['inventory_type'] = $(".article_"+bus_travel_id).attr("inventory-type");

                if(op_name.toLowerCase() == "msrtc") {
                    detail['msrtc_boarding'] = $('#show_'+bus_travel_id+' select.msrtc_boarding_select option:selected').val();
                    detail['msrtc_alighting'] = $('#show_'+bus_travel_id+' select.msrtc_alighting_select option:selected').val();
                    $('#show_'+bus_travel_id+' .msrtc_boarding_input').val(detail['msrtc_boarding']);
                    $('#show_'+bus_travel_id+' .msrtc_boarding_input').attr("data-name",$('#show_'+bus_travel_id+' select.msrtc_boarding_select option:selected').attr("data-name"));

                    $('#show_'+bus_travel_id+' .msrtc_alighting_input').val(detail['msrtc_alighting']);
                    $('#show_'+bus_travel_id+' .msrtc_alighting_input').attr("data-name",$('#show_'+bus_travel_id+' select.msrtc_alighting_select option:selected').attr("data-name"));
                }

                get_data(ajax_url, form, div, detail, function(response) {
                    var seat_layout_tpl = $.parseHTML($.trim($("#seat_layout_tpl").html()));

                    if (response.flag == '@#success#@' && response.fare != null && response.fare_array != null) {
                        // $.loader("close");
                        $('#tot_seat_'+bus_travel_id).html( response.total_available_seats + " Seats" );
                        if(response.fare_array != "" && response.fare_array != null && response.fare_array != undefined) {
                            // $('#seat_fare_'+trip_no).html("Rs. " + response.fare_array);
                            // var basic_fare = response.fare_array;
                            
                            // Seat Fare Search Start
                            //var fare_array_filter = response.fare_array.split('/');
                            //var actual_fare_filter = response.actual_fare.split('/');//remove
                            
                            var fare_array_filter = response.fare.split('/');
                            var actual_fare_filter = response.actual_fare.split('/');//remove

                            $(seat_layout_tpl).find("#fare_tab_filter_id").html("");
                            if(fare_array_filter.length > 1) {
                                if(is_discount_on && is_service_discount) {
                                    if(show_banner_discount) {
                                        $('#seat_fare_'+bus_travel_id).html($('<span />').attr({"class" : "search_fare_text padr78 price"}).html("<span class='pricefrom'>Starting From</span> Rs."+Math.min.apply(Math, fare_array_filter).toFixed())).append($('<span />').attr({"class" : "line-txt pull-right search_discount_fare_text"}).html("Rs."+Math.min.apply(Math, actual_fare_filter).toFixed()));
                                    }
                                    else {
                                        if(show_discounted_fare) {
                                            $('#seat_fare_'+bus_travel_id).html($('<span />').attr({"class" : "search_fare_text price"}).html("<span class='pricefrom'>Starting From</span> Rs."+Math.min.apply(Math, fare_array_filter).toFixed())).append($('<span />').attr({"class" : "line-txt pull-right"}).html("Rs."+Math.min.apply(Math, actual_fare_filter).toFixed()));
                                        }
                                        else {
                                            $('#seat_fare_'+bus_travel_id).html($('<span />').attr({"class" : "search_fare_text price"}).html("<span class='pricefrom'>Starting From</span> Rs."+Math.min.apply(Math, fare_array_filter).toFixed())); 
                                        }
                                    }
                                }
                                else {
                                    $('#seat_fare_'+bus_travel_id).html("<span class='pricefrom'>Starting From&nbsp;&nbsp;</span> <span class='search_fare_text price floatr'>Rs."+Math.min.apply(Math, fare_array_filter).toFixed()+"</span>");
                                }

                                // $(seat_layout_tpl).find("#fare_tab_filter_id").append($("<li>").addClass("selected all_fare_filter").attr({"data-attr": "all"}).html("All"));
                                // $.each(fare_array_filter, function(kf,fval)
                                // {
                                //     $(seat_layout_tpl).find("#fare_tab_filter_id").append($("<li>").attr({"data-attr": fval}).html("Rs. "+fval));
                                // });
                            }
                            else {
                                if(is_discount_on && is_service_discount) {
                                    if(show_banner_discount) {
                                        $('#seat_fare_'+bus_travel_id).html($('<span />').attr({"class" : "search_fare_text padr78 price"}).html("Rs."+parseFloat(response.fare).toFixed())).append($('<span />').attr({"class" : "line-txt pull-right search_discount_fare_text"}).html("Rs."+parseFloat(response.actual_fare).toFixed()));
                                    }
                                    else {
                                        if(show_discounted_fare) {    
                                            $('#seat_fare_'+bus_travel_id).html($('<span />').attr({"class" : "search_fare_text price"}).html("Rs."+parseFloat(response.fare).toFixed())).append($('<span />').attr({"class" : "line-txt pull-right"}).html("Rs."+parseFloat(response.actual_fare).toFixed()));
                                        }
                                        else {
                                            $('#seat_fare_'+bus_travel_id).html($('<span />').attr({"class" : "search_fare_text price"}).html("Rs."+parseFloat(response.fare).toFixed()));   
                                        }
                                    }
                                }
                                else { 
                                    $('#seat_fare_'+bus_travel_id).html($('<span />').attr({"class" : "search_fare_text price"}).html("Rs."+parseFloat(response.fare).toFixed()));
                                }
                            }
                            // Seat Fare Search End
                        }

                        if(response.actual_fare != "" && response.actual_fare != null) {
                            // var basic_fare_array = response.actual_fare.split('/');
                            if (response.actual_fare.toString().indexOf(',') > -1) {
                                var basic_fare_array = response.actual_fare.split('/');
                            }
                            else {
                                var basic_fare_array = [response.actual_fare];
                            }
                            
                            if(basic_fare_array.length > 1) {
                                var basic_fare  =   Math.min.apply(Math, basic_fare_array).toFixed(2)+" - "+Math.max.apply(Math, basic_fare_array).toFixed(2);
                            }
                            else {
                                var basic_fare = parseFloat(response.actual_fare).toFixed(2);
                                // $('#seat_fare_'+trip_no).html("Rs. " + response.fare);
                            }
                        } 
                        
                        seat_fare_detail[bus_travel_id] = response.seat_fare_detail;
                        $(seat_layout_tpl).find("span b.fare").html(basic_fare);
                        $(seat_layout_tpl).find(".book_seat_btn").val(trip_no);
                        $(seat_layout_tpl).find(".continue").attr({"data-trip-no":trip_no,"data-bus-travel-id":bus_travel_id});

                        $('article.sort_list').eq(ind).attr({
                            'data-fare': parseFloat(response.fare).toFixed(2),
                            'data-seat': response.total_available_seats 
                        });
                        
                        $.each(response.seat_layout, function(jb,birth) {
                            var  seat_detail = birth;
                            var $deck_ul = $("<ul>").addClass("deckrow deckrow_"+bus_travel_id+" "+"deckrow_berth_"+jb).attr({"data-berth":jb});

                            if(Object.keys(response.seat_layout).length > 1) {   
                                var berth_name = (jb > 1) ? "Upper Berth" : "Lower Berth";
                                $deck_ul.append($("<li>").addClass("berth_header").html(berth_name));
                                // $deck_ul.append($("<li>").addClass("berth_header").html("Berth "+jb));
                            }

                            var toReverseLoop = true;
                            if(op_name.toLowerCase() == "msrtc" || op_name.toLowerCase() == "upsrtc" || op_name.toLowerCase() == "hrtc" || op_name.toLowerCase() == "rsrtc" ) {
                                var toReverseLoop = false;
                            }
                            //for (var j = 1 ; j <= response.tot_cols ; j++) //for ets
                            // for (var j = response.tot_cols; j >= 1 ; j--) //for state transport
                            // var passage_space_lower = 1;
                            // var passage_space_upper = 1;
                            var passage_space = {};
                            passage_space[jb] = 1;
                            passage_space["is_drive_seat"] = true;
                            for (var j = toReverseLoop ? 1 : response.tot_cols; toReverseLoop ? j <= response.tot_cols : j >= 1 ; toReverseLoop ? j++ : j--) {   
                                var $li = $("<li>").append("<ul>");
                                // *************For Driver Seat Starts***************
                                if(op_name.toLowerCase() != "hrtc") {
                                    var driverSeatClass = "";
                                    var driverSeatTitle = "";
                                    if(jb == 1 && !toReverseLoop && j == response.tot_cols) {
                                        var driverSeatClass = "myBusDriveSeatImage driver_booked";
                                        var driverSeatTitle = "Seat No : Driver";
                                    }
                                    else if(jb == 1 && toReverseLoop && j == 1) {
                                        var driverSeatClass = "myBusDriveSeatImage driver_booked";
                                        var driverSeatTitle = "Seat No : Driver";
                                    }
                                    var $in_li = $('<li/>').append('<a/>');
                                    $in_li
                                        .find('a')
                                        .addClass(driverSeatClass)
                                        .attr({
                                            "href": "javascript:void(0)", 
                                            "title":driverSeatTitle
                                        })
                                        .css({"width":"20px","height":"20px"});
                                    $li.find('ul').append($in_li);
                                }
                                /*************For Driver Seat Ends****************/

                                /*for (var i = 1; i <= response.tot_rows; i++) {*/
                                for (var i = 1; i <= response.tot_rows; i++) {
                                    if(typeof(seat_detail[i+'X'+j]) != "undefined") {
                                        var current_seat = seat_detail[i+'X'+j];

                                        var seat_width  = "20px"; 
                                        var seat_height = "20px"; 
                                        if(current_seat.horizontal_seat) {
                                            seat_width  = "30px";
                                            seat_height = "16px"; 
                                        }
                                        else if(current_seat.vertical_seat) {
                                            seat_width  = "18px";
                                            seat_height = "30px"; 
                                        }

                                        if(current_seat.available  == "Y") {
                                            var current_seat_class = "available";

                                            if(current_seat.current_seat_type != "") {
                                                current_seat_class = current_seat.current_seat_type+"_"+current_seat_class;
                                                var data_seat_select = current_seat.current_seat_type+"_selected";
                                            }

                                            var $in_li = $('<li/>').append('<a/>');
                                            $in_li
                                                .find('a')
                                                .addClass("seat "+current_seat_class)
                                                .attr({
                                                    "href": "javascript:void(0)", 
                                                    "data-fare" : current_seat.seat_fare, 
                                                    "data-quota" : current_seat.quota,
                                                    "data-current-seat-type" : current_seat.current_seat_type,
                                                    "data-basic-fare" : current_seat.seat_basic_fare, 
                                                    "data-service-tax" : current_seat.seat_service_tax_amt, 
                                                    "data-operator-charge" : current_seat.op_service_charge_amt, 
                                                    "data-berth" : current_seat.berth_no, 
                                                    "data-seat-no" : current_seat.seat_no, 
                                                    "data-trip-no" : trip_no,
                                                    "data-bus-travel-id" : bus_travel_id,
                                                    "data-seat-discount" : current_seat.seat_discount,
                                                    "col-x-row" : i+'X'+j,
                                                    "title":"Seat No : "+current_seat.seat_no+" | Fare: Rs."+current_seat.seat_fare + "cxr : "+i+'X'+j
                                                })
                                               .css({"width":seat_width,"height":seat_height}); 
                                        }
                                        else if(current_seat.available  == "N") {
                                            var current_seat_class = "booked";

                                            if(current_seat.current_seat_type != "") {
                                                current_seat_class = current_seat.current_seat_type+"_"+current_seat_class;
                                            }

                                            var $in_li = $('<li/>').append('<a/>');
                                            $in_li.find('a')
                                                .addClass("seat "+current_seat_class)
                                                .attr({
                                                    "href": "javascript:void(0)",
                                                    "title":"Seat No : "+current_seat.seat_no + "cxr : "+i+'X'+j
                                                })
                                                .css({"width":seat_width,"height":seat_height});
                                        }
                                    }
                                    else {
                                        var $in_li = $('<li/>').append('<a/>');
                                        $in_li.find("a").attr({"href": "javascript:void(0)"}).css({"width":"20px","height":"20px"});
                                        // $in_li.find("a").attr({"href": "javascript:void(0)"});
                                    }
                                    $li.find('ul').append($in_li);
                                }// end of j


                                if($li.find('.seat').length > 0 || $li.find('.myBusDriveSeatImage').length > 0) {
                                    $deck_ul.append($li);
                                }
                                else {
                                    if($deck_ul.find('li').length >0 && $deck_ul.find('li:last-child .seat').length > 0 ) {
                                        if(passage_space[jb] == 1) {
                                            $deck_ul.append($li);
                                            passage_space[jb] += 1;
                                        }
                                    }
                                }

                                // $deck_ul.append($li);
                                // $(seat_layout_tpl).find(".seat_block .seat_layout ul.deckrow").addClass("deckrow_"+trip_no+" "+"deckrow_berth_"+jb).attr({"data-berth":jb}).append($li);
                                // $(seat_layout_tpl).find(".seat_block .seat_layout").prepend($deck_ul);
                                $(seat_layout_tpl).find(".seat_block .bus_seat_layout").append($deck_ul);
                            }// end of i
                        }); // end of birth
                        

                        if(typeof(actual_fare_filter) != 'undefined' && actual_fare_filter.length > 1) {
                            $(seat_layout_tpl).find("#fare_tab_filter_id").append($("<li>").addClass("selected all_fare_filter").attr({"data-attr": "all"}).html("All"));
                            $.each(actual_fare_filter, function(kf,fval) {
                                $(seat_layout_tpl).find("#fare_tab_filter_id").append($("<li>").attr({"data-attr": fval}).html("Rs. "+parseFloat(fval).toFixed(2)));
                            });
                        }
                        
                        // *                      NOTICE
                        // *Stop details are not available in seat_layout method of private api.
                        // *So we have taken stop details from global_trip_data
                        // *Afterwards check if any other better logic can built to get stop detail.
                        // *So that we can remove else condition. 
                        // *                      ETRAVELSMART LOGIC
                        // *  IF invetry type 0 or 1 then get boarding stops from available bus otherwise from seat layout
                        // *  Get Dropping Point from available buses.
                        
                        var available_bus_droping_point =  global_trip_data[bus_travel_id].barr;
                        if(response.stop_detail != "") {
                            var boarding_droping_point = response.stop_detail;
                        }
                        else if(response.inventory_type == 0 || response.inventory_type == 1) {
                            var boarding_droping_point = global_trip_data[bus_travel_id].barr;
                        }
                        else {
                            if(response.boarding_points != "" && response.boarding_points != undefined) {
                                var boarding_droping_point = response.boarding_points;
                            }
                            //else {
                                //var boarding_droping_point = global_trip_data[trip_no].barr;
                            //}
                        }
                        // seat_boarding_points = boarding_droping_point;
                        seat_boarding_points[bus_travel_id] = {};
                        seat_boarding_points[bus_travel_id]["barr"] = boarding_droping_point;
                        // global_trip_data[trip_no]["barr"] = boarding_droping_point;

                        $(".article_"+bus_travel_id).find(".boarding_point ul.barr").html("");
                        // $(".article_"+trip_no).find(".droping_point ul.darr").html("");

                        if(boarding_droping_point != "" && boarding_droping_point != undefined) {
                            $.each(boarding_droping_point,function(sk, svalue) {
                                var stop_scode = (svalue.stop_code != "" && svalue.stop_code != undefined) ? "("+svalue.stop_code+")" : "";
                                var bsd = "";
                                bsd = (svalue.boarding_point_id != "" && svalue.boarding_point_id != undefined) ? svalue.boarding_point_id : "";
                                var stop_arrival_time = (svalue.arrival_duration != "" && svalue.arrival_duration != "0" && svalue.arrival_duration != undefined) ? svalue.arrival_duration+" - " : "";
                                
                                if(svalue.is_boarding == "Y") {
                                    $(seat_layout_tpl).find('.bp_dropdown').append($('<option/>').attr({"value":svalue.bus_stop_name,"data-code":svalue.stop_code,"data-bsd":bsd}).html(stop_arrival_time+ svalue.bus_stop_name + stop_scode));
                                    $(".chosen_select").chosen();
                                    $(".article_"+bus_travel_id).find(".boarding_point ul.barr").append("<li>"+stop_arrival_time+svalue.bus_stop_name+"</li>");
                                }
                                else if(svalue.is_alighting == "Y") {
                                    $(seat_layout_tpl).find('.dp_dropdown').append($('<option/>').attr({"value":svalue.bus_stop_name,"data-code":svalue.stop_code,"data-dsd":bsd}).html(stop_arrival_time+ svalue.bus_stop_name + stop_scode));
                                    $(".chosen_select").chosen();
                                    $(".article_"+bus_travel_id).find(".droping_point ul.darr").append("<li>"+stop_arrival_time+svalue.bus_stop_name+"</li>");
                                }
                                
                            });
                        }
                        
                        //check if boarding and Alighting exists or not
                        // Check when implementing private api
                        if($(seat_layout_tpl).find('.bp_dropdown').html().trim() == "") {
                            var from_stop_scode = (global_response.user_data.from_stop_code != "" && global_response.user_data.from_stop_code != undefined) ? "(" + global_response.user_data.from_stop_code + ")" : "";
                            $(seat_layout_tpl).find('.bp_dropdown').append($('<option/>').attr({"value":detail.from,"data-bsd":"","data-code":global_response.user_data.from_stop_code}).html(detail.from + from_stop_scode));
                            $(".chosen_select").chosen();

                            $(".article_"+bus_travel_id).find(".boarding_point ul.barr").append("<li>"+detail.from+"</li>");
                        }

                        //below for loop is for private api
                        if(available_bus_droping_point.length > 0 && $(seat_layout_tpl).find('.dp_dropdown').html().trim() == "") {
                            $(".article_"+bus_travel_id).find(".droping_point ul.darr").html("");
                            $.each(available_bus_droping_point, function(jkey,jpoints) {
                                var stop_arrival_time = (jpoints.arrival_duration != "" && jpoints.arrival_duration != "0" && jpoints.arrival_duration != undefined) ? jpoints.arrival_duration+" - " : "";
                                var jstop_scode = (jpoints.stop_code != "" && jpoints.stop_code != undefined) ? jpoints.stop_code : "";
                                var show_dropingpoint_stop = (jstop_scode != "" && jstop_scode != null) ? jpoints.bus_stop_name+" ("+jstop_scode+")" : jpoints.bus_stop_name;
                                var dsd = "";
                                dsd = (jpoints.boarding_point_id != "" && jpoints.boarding_point_id != undefined) ? jpoints.boarding_point_id : "";
                                
                                if(jpoints.is_alighting == "Y") {
                                    $(seat_layout_tpl).find('.dp_dropdown').append($('<option/>').attr({"value":jpoints.bus_stop_name,"data-code":jstop_scode,"data-dsd":dsd}).html(stop_arrival_time+show_dropingpoint_stop));
                                    $(".chosen_select").chosen();
                                    $(".article_"+bus_travel_id).find(".droping_point ul.darr").append("<li>"+stop_arrival_time+jpoints.bus_stop_name+"</li>");
                                }    
                            });
                        }

                        if($(seat_layout_tpl).find('.dp_dropdown').html().trim() == "") {
                            var to_stop_scode = (global_response.user_data.to_stop_code != "" && global_response.user_data.to_stop_code != undefined) ? "(" + global_response.user_data.to_stop_code + ")" : "";
                            $(seat_layout_tpl).find('.dp_dropdown').append($('<option/>').attr({"value":detail.to,"data-bsd":"","data-code":global_response.user_data.to_stop_code}).html(detail.to + to_stop_scode));
                            $(".chosen_select").chosen();
                            $(".article_"+bus_travel_id).find(".boarding_point ul.darr").append("<li>"+detail.to+"</li>");
                        }

                        if(op_name.toLowerCase() == "msrtc") {
                            $(seat_layout_tpl).find(".detail_boarding_alighting_section").remove();
                        }

                        $('.bus_seat_div').eq(ind).html($(seat_layout_tpl));
                        $( "<br/><div><h5><b class='theme-orange-color cursor-pointer check_cancellation_policy'>View Cancellation Policy.</b></h5></div>" ).insertAfter( "#bus_travel_id_"+trip_no+" .seat_layout .bus_seat_layout");
                    }
                    else if(response.flag == '@#error#@') {
                        if(response.total_available_seats != undefined && response.total_available_seats == 0) {
                            (!$(this).hasClass("get_bus_seat_open")) ? $(this).addClass("get_bus_seat_open") : $(this).removeClass("get_bus_seat_open");
                            $("#bus_travel_id_"+bus_travel_id).addClass("hide");
                            $(".article_"+bus_travel_id).find('.seat_select_button').removeClass("btnorange").addClass("avoid-clicks sold_get_seat_layout").text("Sold Out").css({"cursor":"auto","background-color":"#989898"});
                        }
                        else {
                            $(seat_layout_tpl).find(".seat_block .bus_seat_layout").html("<ul style='border:2px solid #c6c6c6; padding: 10px;font-weight:bold;color:#c51f25;'><li>Seat layout not available. Please try another bus.</li></ul>");
                            $('.bus_seat_div').eq(ind).html($(seat_layout_tpl));
                        }
                    }
                    else {
                        $(seat_layout_tpl).find(".seat_block .bus_seat_layout").html("<ul style='border:2px solid #c6c6c6; padding: 10px;font-weight:bold;color:#c51f25;'><li>Opps some thing went wrong from operator end, Please select another bus!</li></ul>");
                        $('.bus_seat_div').eq(ind).html($(seat_layout_tpl));
                    }

                    // $.scrollTo('.article_'+trip_no+' .boarding_dropping .bd_point');
                    // $('html, body').animate({scrollTop: $('.article_'+trip_no+' .seat_block .legend').offset().top}, 2000);
                    $.loader("close");
                    $(".chosen_select").chosen();

                }, '', true);
            }

        });
        /******* Seat Layout end *******/


        /******* Seat Layout Filter Start *******/
        $(document).off('click', '#fare_tab_filter_id li').on('click', '#fare_tab_filter_id li', function(e) {

            var tab_fare = $(this).attr("data-attr");
            $("#fare_tab_filter_id li").removeClass("selected");
            $(this).addClass("selected");
            $("ul.deckrow").find("ul li a").removeClass("opacity-4");

            if(tab_fare != "all")
            {
                $("ul.deckrow").find("ul li a[data-basic-fare!='"+tab_fare+"']").addClass("opacity-4");
            }
        });
        /******* Seat Layout Filter end *******/

        /******* Fare Calculation start here *******/
        $(document).off('click', '.bus_seat_div .deckrow .seat').on('click', '.bus_seat_div .deckrow .seat', function(e) {
            
            /* Seat fare Filter Start */
            /*$("#fare_tab_filter_id li").removeClass("selected");
            $("#fare_tab_filter_id li.all_fare_filter").addClass("selected");
            $("ul.deckrow").find("ul li a").removeClass("opacity-4");*/
            /* Seat fare Filter End */
            var class_prefix = "";
	    var current_op_name = "";
            var trip_no  = $(this).attr('data-trip-no');
            var bus_travel_id  = $(this).attr('data-bus-travel-id');
            var quota    = $(this).attr('data-quota');
            var current_seat_type  = $(this).attr('data-current-seat-type');
            var berth_no = $(this).attr('data-berth');
            var $par_elem = $('#bus_travel_id_'+bus_travel_id+'');
            var $deck_row = $par_elem.find('.deckrow_'+bus_travel_id+'');
            var data_seat_no= $(this).attr('data-seat-no');
            var current_op_name = $(".article_"+bus_travel_id).attr('data-op-name');
			
            class_prefix = (current_seat_type != "" && current_seat_type != undefined) ? current_seat_type+"_" : "";
            
            if(typeof(data_seat_no) != "undefined" && seat_fare_detail[bus_travel_id][berth_no][data_seat_no]["available"] == "Y")
            {
                if($(this).hasClass(class_prefix+'available'))
                {
                   $(this).removeClass(class_prefix+'available');
                   $(this).addClass(class_prefix+'selected userseatselected');
                }
                else if($(this).hasClass(class_prefix+'selected'))
                {
                   $(this).removeClass(class_prefix+'selected userseatselected');
                   $(this).addClass(class_prefix+'available');           
                } 
               
                // var total_sel_seat = $deck_row.find('.seat.selected').length + $deck_row.find('.seat.ladies_selected').length;
                
                var total_sel_seat = $deck_row.find('.seat.userseatselected').length;
                
                 if(current_op_name.toLowerCase() != "rsrtc" && total_sel_seat > 6)
                {
                    $(".bus_seat_div h3.more_seat_detail").show();
                    $(this).removeClass(class_prefix+'selected userseatselected').addClass(class_prefix+'available');
                }
                else if(current_op_name.toLowerCase() == "rsrtc" && total_sel_seat > 4)
                {
                    $(".bus_seat_div h3.rsrtc_more_seat_detail").show();
                    $(this).removeClass(class_prefix+'selected userseatselected').addClass(class_prefix+'available');
                }
                else
                {
                    $(".bus_seat_div h3.more_seat_detail").hide();
                    $(".bus_seat_div h3.rsrtc_more_seat_detail").hide();
                    cal_fare(bus_travel_id);     
                }
            }
            else if(data_seat_no == "undefined" && seat_fare_detail[bus_travel_id][berth_no][data_seat_no]["available"] == "N")
            {
                $(this).addClass('lady');
                // alert("Ticket is already booked");
                showNotification("danger","Ticket is already booked");
            }
            else
            {
                $(this).addClass('lady');
                // alert("Ticket is already booked"); 
                showNotification("danger","Ticket is already booked");
            }
        });


        function cal_fare(bus_travel_id)
        {
            var fares = 0;
            var actual_total_fares = 0;
            var total_discount = 0;
            var basic_fares = 0;
            var service_tax = 0;
            var op_service_charge = 0;
            var $par_elem = $('#bus_travel_id_'+bus_travel_id);
            var $deck_row = $par_elem.find('.deckrow_'+bus_travel_id);
            var total_sel_seat = $deck_row.find('.seat.userseatselected').length;
            var tseat = {};
            var birth_count = $(".deckrow_"+bus_travel_id).length;
			var op_id = $('.article_'+bus_travel_id+'').attr('data-op-id');
			var bus_type_id = $('.article_'+bus_travel_id+'').attr('data-bus-type');

            $par_elem.find('.sel_seat').html("");

            $(".deckrow_"+bus_travel_id).each(function(bind)
            {
                var tbirth = $(this).attr('data-berth');
                if($(this).find('.seat.userseatselected').length > 0)
                {
                    tseat[tbirth] = new Array();
                    $(this).find('.seat.userseatselected').each(function(sind)
                    {
                        if($(this).hasClass('userseatselected'))
                        {
                            var data_seat_no = $(this).attr('data-seat-no');
                            // var data_berth_no = $(this).attr('data-berth');
                            fares += parseFloat(seat_fare_detail[bus_travel_id][tbirth][data_seat_no]["seat_fare"]);
                            actual_total_fares += parseFloat(seat_fare_detail[bus_travel_id][tbirth][data_seat_no]["actual_seat_fare"]);
                            // basic_fares += parseFloat(seat_fare_detail[trip_no][tbirth][data_seat_no]["seat_basic_fare"]);
                            basic_fares += parseFloat(seat_fare_detail[bus_travel_id][tbirth][data_seat_no]["actual_seat_basic_fare"]);
                            $par_elem.find('.fare_value .fare').html(basic_fares.toFixed(2));
                            service_tax += parseFloat(seat_fare_detail[bus_travel_id][tbirth][data_seat_no]["seat_service_tax_amt"]);
                            op_service_charge += (seat_fare_detail[bus_travel_id][tbirth][data_seat_no]["op_service_charge_amt"] != null && seat_fare_detail[bus_travel_id][tbirth][data_seat_no]["op_service_charge_amt"] != undefined) ? parseFloat(seat_fare_detail[bus_travel_id][tbirth][data_seat_no]["op_service_charge_amt"]) : 0;
                            tseat[tbirth].push(data_seat_no);
                            total_discount += (seat_fare_detail[bus_travel_id][tbirth][data_seat_no]["seat_discount"] != null && seat_fare_detail[bus_travel_id][tbirth][data_seat_no]["seat_discount"] != undefined) ? parseFloat(seat_fare_detail[bus_travel_id][tbirth][data_seat_no]["seat_discount"]) : 0;
							
                        }
                    });

                    tseat[tbirth].sort(function(a, b){return a-b});

                    if(birth_count > 1)
                    {
                        var berth_name = (tbirth == 1) ? " Lower" :  (tbirth == 2) ? " Upper" : "";
                        $par_elem.find('.sel_seat').append(berth_name+" : "+tseat[tbirth].join(', '));
                    }
                    else
                    {
                        $par_elem.find('.sel_seat').append(tseat[tbirth].join(', '));
                    }
                }
            });
            
            temp_seats = tseat;
            // $par_elem.find('.tot_fare').html(fares.toFixed(2));
            $par_elem.find('.total_fare_discount_div').removeClass("hide");
            $par_elem.find('.total_fare_discount_value').html(total_discount.toFixed(2));
            
            
            if(discount_on == "seat_fare")
            {
                $par_elem.find('.total_fare_discount_div').removeClass("hide");
                $par_elem.find('.total_fare_discount_value').html(total_discount.toFixed(2));
            }
            else if(discount_on == "base_fare")
            {
				if(basic_fares >=  discount_above)
                {
					if("<?php echo $this->config->item('is_operator_based');?>" == '1' && op_id == "<?php echo $this->config->item('op_id');?>")
					{
						if(discount_type == 'flat')
						{
							total_discount = parseFloat("<?php echo $this->config->item('discount_flat_rate');?>");
							if(total_discount > max_dis_availed)
							{
								total_discount = parseFloat(max_dis_availed);
							}
							fares = fares - total_discount;
						}
						else if (discount_type == 'percentage')
						{
							
							total_discount = parseFloat((basic_fares * "<?php echo $this->config->item('discount_percent_rate');?>")/100);
							if(total_discount > max_dis_availed)
							{
								total_discount = parseFloat(max_dis_availed);
							}
							fares = fares - total_discount;
						}
					}
					else if("<?php echo $this->config->item('is_bus_type_based');?>"== '1' && bus_type_id == "<?php echo $this->config->item('bus_type');?>")
					{
						
						if(discount_type == 'flat')
						{
							total_discount = parseFloat("<?php echo $this->config->item('discount_flat_rate');?>");
							if(total_discount > max_dis_availed)
							{
								total_discount = parseFloat(max_dis_availed);
							}
							fares = fares - total_discount;
						}
						else if (discount_type == 'percentage')
						{
							
							total_discount = parseFloat((basic_fares * "<?php echo $this->config->item('discount_percent_rate');?>")/100);
							if(total_discount > max_dis_availed)
							{
								total_discount = parseFloat(max_dis_availed);
							}
							fares = fares - total_discount;
						}
					}
					else if ("<?php echo $this->config->item('is_bus_type_based');?>" == '0' && "<?php echo $this->config->item('is_operator_based');?>" == '0' && "<?php echo $this->config->item('is_transaction_based');?>" == '0' && "<?php echo $this->config->item('is_ticket_count_based');?>" == '0')
					{
						if(discount_type == 'flat')
						{
							total_discount = parseFloat("<?php echo $this->config->item('discount_flat_rate');?>");
							if(total_discount > max_dis_availed)
							{
								total_discount = parseFloat(max_dis_availed);
							}
							fares = fares - total_discount;
						}
						else if (discount_type == 'percentage')
						{
							
							total_discount = parseFloat((basic_fares * "<?php echo $this->config->item('discount_percent_rate');?>")/100);
							if(total_discount > max_dis_availed)
							{
								total_discount = parseFloat(max_dis_availed);
							}
							fares = fares - total_discount;
						}
					}
					
                    
                }
				
               $par_elem.find('.base_fare_discount_div').removeClass("hide");
               $par_elem.find('.base_fare_discount_value').html(total_discount.toFixed(2));
            }
            else if(discount_on == "total_fare")
            {
				if(actual_total_fares >=  discount_above)
                {
					if("<?php echo $this->config->item('is_operator_based');?>" == '1' && op_id == "<?php echo $this->config->item('op_id');?>")
					{
						if(discount_type == 'flat')
						{
							total_discount = parseFloat("<?php echo $this->config->item('discount_flat_rate');?>");
							if(total_discount > max_dis_availed)
							{
								total_discount = parseFloat(max_dis_availed);
							}
							fares = fares - total_discount;
						}
						else if (discount_type == 'percentage')
						{
							
							total_discount = parseFloat((actual_total_fares * "<?php echo $this->config->item('discount_percent_rate');?>")/100);
							if(total_discount > max_dis_availed)
							{
								total_discount = parseFloat(max_dis_availed);
							}
							fares = fares - total_discount;
						}
					}
					else if("<?php echo $this->config->item('is_bus_type_based');?>"== '1' && bus_type_id == "<?php echo $this->config->item('bus_type');?>")
					{
						
						if(discount_type == 'flat')
						{
							total_discount = parseFloat("<?php echo $this->config->item('discount_flat_rate');?>");
							if(total_discount > max_dis_availed)
							{
								total_discount = parseFloat(max_dis_availed);
							}
							fares = fares - total_discount;
						}
						else if (discount_type == 'percentage')
						{
							
							total_discount = parseFloat((actual_total_fares * "<?php echo $this->config->item('discount_percent_rate');?>")/100);
							if(total_discount > max_dis_availed)
							{
								total_discount = parseFloat(max_dis_availed);
							}
							fares = fares - total_discount;
						}
					}
					else if ("<?php echo $this->config->item('is_bus_type_based');?>" == '0' && "<?php echo $this->config->item('is_operator_based');?>" == '0' && "<?php echo $this->config->item('is_transaction_based');?>" == '0' && "<?php echo $this->config->item('is_ticket_count_based');?>" == '0')
					{
						if(discount_type == 'flat')
						{
							total_discount = parseFloat("<?php echo $this->config->item('discount_flat_rate');?>");
							if(total_discount > max_dis_availed)
							{
								total_discount = parseFloat(max_dis_availed);
							}
							fares = fares - total_discount;
						}
						else if (discount_type == 'percentage')
						{
							
							total_discount = parseFloat((actual_total_fares * "<?php echo $this->config->item('discount_percent_rate');?>")/100);
							if(total_discount > max_dis_availed)
							{
								total_discount = parseFloat(max_dis_availed);
							}
							fares = fares - total_discount;
						}
					}
                    
                }
               $par_elem.find('.total_fare_discount_div').removeClass("hide");
               $par_elem.find('.total_fare_discount_value').html(total_discount.toFixed(2));
            }

            $par_elem.find('.tot_fare').html(actual_total_fares.toFixed(2));
            $par_elem.find('.total_amount').html(fares.toFixed(2));
            $par_elem.find('.fare_service_tax').html(service_tax.toFixed(2));
            $par_elem.find('.fare_service_charge').html(op_service_charge.toFixed(2));
        }
    /******* Fare Calculation end here *******/

        /*$(".boarding_droping_point").hover(function(e) {
                console.log("a");
                $(this).next("div").show();
            }, function() {
                 console.log("b");
                $(this).next("div").hide();
        });*/
        
        $(document).on("mouseenter", ".boarding_droping_point", function(){
            $(this).next("div").show();
        });

        $(document).on("mouseleave", ".boarding_droping_point", function(){
            $(this).next("div").hide();
        });

        $(document).on("mouseenter", ".bus_cancellation_policy", function(){
            $(this).parent().next(".ttp_ticket_cancellation_policy").show();
        });

        $(document).on("mouseleave", ".bus_cancellation_policy", function(){
            $(this).parent().next(".ttp_ticket_cancellation_policy").hide();
        });


        $.validator.addMethod("dateFormat",
        function(value, element) {
            var currVal = value;
            if(currVal == '')
                return false;
            //Declare Regex 
            var rxDatePattern = /^(\d{1,2})(\-)(\d{1,2})(\-)(\d{4})$/;
            var dtArray = currVal.match(rxDatePattern); // is format OK?
            if (dtArray == null)
                return false;
            //Checks for dd-mm-yyyy format.
            dtDay = dtArray[1];
            dtMonth= dtArray[3];
            dtYear = dtArray[5];
            if (dtMonth < 1 || dtMonth > 12)
            {
                return false;
            }
            else if (dtDay < 1 || dtDay> 31)
            {
                return false;
            }
            else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)
            {
                return false;  
            }
            else if (dtMonth == 2)
            {
                var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
                if (dtDay> 29 || (dtDay ==29 && !isleap))
                    return false;
            }

            return true;
        },"Please enter valid date.");


    $.validator.addMethod("checkDepend",
            function(value, element) {

            if($("#date_of_arr").val() != "")
            {
                var date_diff = date_operations($("#date_of_jour").val(),$("#date_of_arr").val());
                if(date_diff.date_compare)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }

        },"Return journey must be after onward journey.");

    
    $.validator.addMethod("notEqualTo", function (value, element, param)
    {
        var target = $(param);
        if (value) return value != target.val();
        else return this.optional(element);
    }, "Repeated field");
    
    $('#modify_search_form').validate({
        ignore: "",
        rules: {
            from: {
                required: true,
                notEqualTo: "#to1"
            },
            to : {
                required: true,
                notEqualTo: "#from1"
            },
            date_of_jour : {
                required: true,
                dateFormat: true
            },
            date_of_arr : {
                checkDepend : true
            }
        },
        messages: {
            from: {
              required: "Please enter from stop",
              notEqualTo: "From & to stop must be different"
            },
            to: {
              required: "Please enter to stop",
              notEqualTo: "From & to stop must be different"
            }
            ,
            date_of_jour: {
              required: "Please enter date of journey",
              dateFormat: "Please enter date in dd-mm-yyyy format"
            }
        }
    });


    /******* Popup details start *******/
    $(document).off('click','.popup_all_details').on('click','.popup_all_details', function(){
        var pickup_tr = "";
        var dropoff_tr = "";
        var trip_no = $(this).attr("data-trip-no");
        var bus_travel_id = $(this).attr("data-bus-travel-id");
        var op_id = $(this).attr("data-op-id");
        var $trip_detail = global_trip_data[bus_travel_id];
        var temp_min_seat_fare = "";
        temp_min_seat_fare = $(".article_"+bus_travel_id).attr("data-fare");
        var shift = $trip_detail.shift.toLowerCase();
        var op_name = $trip_detail.op_name;
        var ets = ' (E)';
        var its = ' (I)';
        $("#pick-up-details table tbody").empty();
        $("#drop-off-details table tbody").empty();
        $("#cancellation-policy table.pick-drop-policy tbody").empty();
        $("#cancellation-policy table.pick-drop-policy-conditions tbody").empty();
        
        if($trip_detail.provider_id == "2" && $trip_detail.op_id == "0" && $trip_detail.provider_type != "operator" )
        {
          $("#all_details table tr").find(".operator").html(op_name.concat(ets));
        }
        else if($trip_detail.provider_id == '3' && $trip_detail.op_id == '0' && $trip_detail.provider_type != "operator")
        {
          $("#all_details table tr").find(".operator").html(op_name.concat(its));   
        }
        else
        {
          $("#all_details table tr").find(".operator").html(op_name);   
        }

        $("#all_details table tr").find(".departure .time").html($trip_detail.str_dep_tm_his);
        $("#all_details table tr").find(".travel_duration .total_time").html($trip_detail.travel_time);
        $("#all_details table tr").find(".arrival .time").html($trip_detail.arrival_time);
        $("#all_details table tr").find(".seats").html($trip_detail.tot_seat_available);

        if(temp_min_seat_fare != "" && temp_min_seat_fare != null && temp_min_seat_fare != undefined)
        {
            $("#all_details table tr").find(".price").html("Rs."+temp_min_seat_fare);
        }
        else if($trip_detail.seat_fare != null)
        {
            if(String($trip_detail.seat_fare).indexOf(',') === -1)
            {
                $("#all_details table tr").find(".price").html("Rs."+$trip_detail.seat_fare);
            }
            else
            {
                $("#all_details table tr").find(".price").html("Rs."+$trip_detail.seat_fare.replace(/,/g, "/"));
            }
        }
        else
        {
            $("#all_details table tr").find(".price").html("NA");
        }


        if($trip_detail.mticket_allowed)
            $("#all_details table tr").find(".amenities i.fa-mobile").addClass("active");
        else
            $("#all_details table tr").find(".amenities i.fa-mobile").removeClass("active");

        //for icons in arrival time
        if($trip_detail.arrival_tm >= "04:00:00" && $trip_detail.arrival_tm <= "18:00:00")
        {
            $("#all_details table tr").find(".arrival span.icon").addClass("fa-sun-o sun_color");
        }
        else
        {
            $("#all_details table tr").find(".arrival span.icon").addClass("fa-moon-o moon_color");
        }

        //for icons in departure time
        if($trip_detail.sch_departure_tm >= "04:00:00" && $trip_detail.sch_departure_tm <= "18:00:00")
        {
            $("#all_details table tr").find(".departure span.icon").addClass("fa-sun-o sun_color");
        }
        else
        {
            $("#all_details table tr").find(".departure span.icon").addClass("fa-moon-o moon_color");
        }


        if($trip_detail.cancellation_policy == "")
        {
            //To get cancellation policy
            var detail              = {};
            var div                 = "";
            var form                = "";
            var ajax_url            = "front/booking/cancellation_charges_by_op_id";
            detail['op_id']         = op_id;
            get_data(ajax_url, form, div, detail, function(response)
            {
                if(!$("#cancellation-policy table.pick-drop-policy-conditions").hasClass("hide"))
                {
                    $("#cancellation-policy table.pick-drop-policy-conditions").addClass("hide");
                }

                if(response.flag == "@#success#@")
                {
                    $.each(response.cancellation_charges, function(j,cdetail)
                    {
                        var charges_percent = cdetail.type == 'percent' ? " %" : "";
                        var charges_rupees = cdetail.type == 'fixed' ? "Rs. " : "";
                        var from = cdetail.from != "" ? Math.round(cdetail.from/3600) : "";
                        var till = cdetail.till != "" ? Math.round(cdetail.till/3600) : "";
                        var cancellation_sentence = "Cancellation upto <b>"+from+" hrs</b>";
                        // var cancellation_sentence = (from != "" || till != "") ? ((from != "" ? "More than <b>"+from+" hrs</b>" : "") +" "+(till != "" ? "within <b>"+till+" hrs</b>" : "")+" from scheduled departure time") : "";

                        var row = $("<tr>");
                        row.append($("<td>").html(cancellation_sentence));
                        row.append($("<td>").html(charges_rupees+cdetail.cancel_rate+charges_percent));
                        $("#cancellation-policy table.pick-drop-policy tbody").append(row);
                    });
                    

                    if(response.cancellation_charges[0] != "" && response.cancellation_charges[0] != undefined && response.cancellation_charges[0].note != "" &&  response.cancellation_charges[0].note != undefined)
                    {
                        var cancellation_charges_conditions = response.cancellation_charges[0].note.split("|||");

                        $.each(cancellation_charges_conditions, function(ccc_index,cc_condition)
                        {
                            var ccc_row = $("<tr>");
                            ccc_row.append($("<td>").html(cc_condition));
                            $("#cancellation-policy table.pick-drop-policy-conditions tbody").append(ccc_row);
                        });

                        if($("#cancellation-policy table.pick-drop-policy-conditions").hasClass("hide"))
                        {
                            $("#cancellation-policy table.pick-drop-policy-conditions").removeClass("hide");
                        }
                    }
                }
                else
                {
                    var row = $("<tr>");
                    row.append($("<td>").html("No cancellation policy defined."));
                    $("#cancellation-policy table.pick-drop-policy tbody").append(row);
                }

            }, '', false);
            //END To get cancellation policy
        }
        else if($trip_detail.cancellation_policy != "")
        {
            if($trip_detail.op_id > 0 && $.inArray($trip_detail.op_name.toLowerCase(), ["msrtc","upsrtc","rsrtc"]) !== -1)
            {
                $.each($trip_detail.cancellation_policy, function(j,cdetail)
                {
                    var charges_percent = cdetail.type == 'percent' ? " %" : "";
                    var charges_rupees = cdetail.type == 'fixed' ? "Rs. " : "";
                    if($trip_detail.op_name.toLowerCase() != 'rsrtc')
                    {
                        var from = cdetail.from != "" ? Math.round(cdetail.from/3600) : "";
                        var till = cdetail.till != "" ? Math.round(cdetail.till/3600) : "";
                        var cancellation_sentence = "Cancellation upto <b>"+from+" hrs</b>";
                    }
                    else if ($trip_detail.op_name.toLowerCase() == 'rsrtc')
                    {
                        var from = cdetail.from != "" ? (cdetail.from/3600).toFixed(2) : "";
                        var till = cdetail.till != "" ? Math.round(cdetail.till/3600) : "";
                        if(from == '0.50')
                        {
                            var cancellation_sentence = "Cancellation upto <b>30 mins</b>";
                        }
                        else
                        {
                            var cancellation_sentence = "Cancellation upto <b>"+from+" hrs</b>";  
                        }
                        
                    }
                    // var cancellation_sentence = (from != "" || till != "") ? ((from != "" ? "More than <b>"+from+" hrs</b>" : "") +" "+(till != "" ? "within <b>"+till+" hrs</b>" : "")+" from scheduled departure time") : "";

                    var row = $("<tr>");
                    row.append($("<td>").html(cancellation_sentence));
                    row.append($("<td>").html(charges_rupees+cdetail.cancel_rate+charges_percent));
                    $("#cancellation-policy table.pick-drop-policy tbody").append(row);
                });
                

                if($trip_detail.cancellation_policy[0] != "" && $trip_detail.cancellation_policy[0] != undefined && $trip_detail.cancellation_policy[0].note != "" &&  $trip_detail.cancellation_policy[0].note != undefined)
                {
                    var cancellation_charges_conditions = $trip_detail.cancellation_policy[0].note.split("|||");

                    $.each(cancellation_charges_conditions, function(ccc_index,cc_condition)
                    {
                        var ccc_row = $("<tr>");
                        ccc_row.append($("<td>").html(cc_condition));
                        $("#cancellation-policy table.pick-drop-policy-conditions tbody").append(ccc_row);
                    });

                    if($("#cancellation-policy table.pick-drop-policy-conditions").hasClass("hide"))
                    {
                        $("#cancellation-policy table.pick-drop-policy-conditions").removeClass("hide");
                    }
                }
            }
            else
            {
               $.each($trip_detail.cancellation_policy, function(cp,cpdetail)
                {
                    var cancellation_sentence = "Cancellation within <b>"+cpdetail.cutoffTime+" hrs</b>";
                    var row = $("<tr>");
                    row.append($("<td>").html(cancellation_sentence));
                    row.append($("<td>").html(cpdetail.refundInPercentage+ "%"));
                    $("#cancellation-policy table.pick-drop-policy tbody").append(row);
                }); 
            }
        }
        else
        {
            var row = $("<tr>");
            row.append($("<td>").html("No cancellation policy defined."));
            $("#cancellation-policy table.pick-drop-policy tbody").append(row);
        }

        //To show and hide loader/tab container
        $(".show-loading").hide();
        $(".hide-tab-container").show();
        
        var layout_boarding_points;          
        if($trip_detail.inventory_type == 0 || $trip_detail.inventory_type == 1)
        {
            layout_boarding_points  = $trip_detail.barr;
        }
        else
        {
            if(seat_boarding_points[bus_travel_id] != "" && seat_boarding_points[bus_travel_id] != undefined && seat_boarding_points[bus_travel_id]["barr"] != "" && seat_boarding_points[bus_travel_id]["barr"] != undefined)
            {
                layout_boarding_points  = seat_boarding_points[bus_travel_id]["barr"];
            }
            else
            {
                layout_boarding_points  = $trip_detail.barr;
            }
        }

        $.each(layout_boarding_points, function(i,value)
        {
            if(value.is_boarding == "Y")
            {
                var pickup_location = (value.location != "" && value.location != undefined) ? value.location : "NA";
                var pickup_time = (value.arrival_duration != "" && value.arrival_duration != "0" && value.arrival_duration != undefined) ? value.arrival_duration : "NA";

                pickup_tr += "<tr>";
                pickup_tr += "<td>"+value.bus_stop_name+"</td>";
                pickup_tr += "<td>"+pickup_location+"</td>";
                pickup_tr += "<td>"+pickup_time+"</td>";
                pickup_tr += "</tr>";
            }
            else if(value.is_alighting == "Y")
            {
                var dropoff_time = (value.arrival_duration != 0 && value.arrival_duration != undefined) ? value.arrival_duration : "NA";
                
                dropoff_tr += "<tr>";
                dropoff_tr += "<td>"+value.boarding_stop_name+"</td>";
                dropoff_tr += "<td>"+dropoff_time+"</td>";
                dropoff_tr += "</tr>";
            }
        });
        
        /*              NOTICE
         * Dropping point same even after seatlayout.
         * Take same dropping point as in bus service
         */

         if(dropoff_tr == "" && $trip_detail.barr != "" && $trip_detail.barr != undefined)
         {
            $.each($trip_detail.barr, function(di,dvalue)
            {
                if(dvalue.is_alighting == "Y")
                {
                    var dropofftime = (dvalue.arrival_duration != 0 && dvalue.arrival_duration != undefined) ? dvalue.arrival_duration : "NA";
                    
                    dropoff_tr += "<tr>";
                    dropoff_tr += "<td>"+dvalue.boarding_stop_name+"</td>";
                    dropoff_tr += "<td>"+dropofftime+"</td>";
                    dropoff_tr += "</tr>";
                }
            });
         }
        
        if(pickup_tr == "")
        {
            pickup_tr += "<tr>";
            pickup_tr += "<td>"+$trip_detail.from_stop+"</td>";
            pickup_tr += "<td>N.A.</td>";
            pickup_tr += "<td>"+$trip_detail.str_dep_tm_his+"</td>";
            pickup_tr += "</tr>";  
            // pickup_tr = "<tr><td colspan='3'>There is no pickup point specified</td></tr>";
        }

        if(dropoff_tr == "")
        {
            dropoff_tr += "<tr>";
            dropoff_tr += "<td>"+$trip_detail.to_stop+"</td>";
            dropoff_tr += "<td>"+$trip_detail.arrival_time+"</td>";
            dropoff_tr += "</tr>";
            // dropoff_tr = "<tr><td colspan='2'>There is no dropoff point specified</td></tr>";
        }
            
        $("#pick-up-details table tbody").html(pickup_tr);
        $("#drop-off-details table tbody").html(dropoff_tr);
        
    })
    /******* Popup details end here *******/

    /******* To Refresh Page Start *******/
    $(document).off('click', '.refresh').on('click', '.refresh', function(e) {
        $.cookie("is_refreshed", 1, { expires : 7776000, path    : '/'});
        window.location.reload();
    });

    
    if($.cookie("is_refreshed"))
    {
        $(".advanceSearch").trigger("click");
        $("#date_of_arr").datepicker({numberOfMonths:2}).datepicker("show");
        $.removeCookie('is_refreshed', { path: '/' });
        bootstrapGrowl("Please select return journey. ", "danger", {"delay":"3000"});
    }

    /******* To Refresh Page End *******/


    /******* To Skip Return Journey Start *******/
    $(document).off('click', '.skip').on('click', '.skip', function(e) {
        window.location.href = skip_form_url;
    });
    /******* To Skip Return Journey End *******/


    /******* STU Grouping Start*******/
    $(document).off('click', '#stu_bus_summary').on('click', '#stu_bus_summary', function(e) {
       $("#stu_bus_services").toggle("slow");
       $("#stu_bus_summary").find(".box").toggleClass("background-none");
       $("#stu_bus_summary").find(".stu_grouping_plus_minus i.fa").toggleClass("fa-plus-circle fa-minus-circle");
    });
    /******* STU Grouping End*********/

    $(document).on("mouseenter", ".check_cancellation_policy", function(){
        var trip_no = $(this).parents("article").attr("data-trip-no");
        var bus_travel_id = $(this).parents("article").attr("data-bus-travel-id");
        $(".article_"+bus_travel_id+" .ttp_ticket_cancellation_policy").css({"display":"block"});
    });

    $(document).on("mouseleave", ".check_cancellation_policy", function(){
        var trip_no = $(this).parents("article").attr("data-trip-no");
        var bus_travel_id = $(this).parents("article").attr("data-bus-travel-id");
        $(".article_"+bus_travel_id+" .ttp_ticket_cancellation_policy").css({"display":"none"});
    });

}); // Document ready ends here 


// No Result found for bus services
function no_result_found(return_journey)
{   
    var template_to_render = (return_journey) ? "return_no_routes_found_tpl" : "no_routes_found_tpl";
    
    $("#no_of_bus_found").html("0");
    $("#loading_bus_service").hide();
    $("#noResult").show();
    $("#bus_service_ul_id").html($("#"+template_to_render).html());
    // $("#show_waiting").modal("hide");
    $.loader("close");
}


function showServiceWaiting(fromtostring)
{   
    var bus_service_search_waiting = $.parseHTML($.trim($("#bus_service_search_waiting").html()));

    // show(fromtostring.date_of_jour);
    // var date_formats = DateJavascriptFormat(fromtostring.date_of_jour,1,1);

    var sDateiso           = moment(moment(fromtostring.date_of_jour,"DD-MM-YYYY")).toISOString();
    var sDateymd           = moment(sDateiso).format('YYYY-MM-DD');


    $(bus_service_search_waiting).find(".search_progress .pro_from_city").html(fromtostring.from);
    $(bus_service_search_waiting).find(".search_progress .pro_to_city").html(fromtostring.to);
    $(bus_service_search_waiting).find(".search_progress .pro_date").html(moment(sDateymd).format('DD-MMM-YYYY'));
    $(bus_service_search_waiting).find(".service_search_waiting_loader img").attr({"src":"<?php echo base_url().FRONT_ASSETS;?>images/loader/rokad.GIF"});

    $.loader({
                content:{text: bus_service_search_waiting},
            });


    /*$(".search_progress .pro_from_city").html(fromtostring.from);
    $(".search_progress .pro_to_city").html(fromtostring.to);
    $(".search_progress .pro_date").html(date_formats.search_full_date);
    $("#show_waiting").modal("show");*/
}

function fetch_new_bus_service_stops(postdata, trip_no = "")
{
    if(trip_no != "")
    {
        var detail              = {};
        var div                 = "";
        var form                = "";
        var ajax_url            =  "front/transaction/fetch_single_bus_service_stops";
        detail                  = postdata;
        get_data(ajax_url, form, div, detail, function(response)
        {
            if(response.flag == "@#success#@")
            {
                $(".article_"+trip_no+" .view_bus_service_stops").trigger("click");
            }
        }, '', false);
    }
    else
    {
        $(".view_bus_service_stops").text("Select Seat");
        showNotification("danger","Seat layout not available. Please select another bus.");
    }
}
</script>
