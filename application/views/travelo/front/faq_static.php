<!DOCTYPE html>
    <html> <!--<![endif]-->
    <head>
        <!-- Page Title --> 
        <title><?php echo (isset($metadata_title)) ? $metadata_title : $this->config->item('title', 'home_metadata'); ?></title>
        
        <!-- Meta Tags -->
        <meta charset="utf-8">
        <meta name="keywords" content="<?php echo (isset($metadata_keywords)) ? $metadata_keywords : $this->config->item('keywords', 'home_metadata'); ?>">
        <meta name="description" content="<?php echo (isset($metadata_description)) ? $metadata_description : $this->config->item('description', 'home_metadata'); ?>" />
        <meta name="author" content="Book On Spot">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo base_url().COMMON_ASSETS;?>plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
        <!-- Theme Styles -->
        <link rel="stylesheet" href="<?php echo base_url().FRONT_ASSETS;?>css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url().FRONT_ASSETS;?>css/font-awesome.min.css">
        
        <!-- download font later -->
        <link href='//fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <link rel="icon" href="<?php echo base_url().FRONT_ASSETS;?>images/icon/favicon.ico?token=04022016" type="image/x-icon">
        
        <!-- Current Page Styles -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().FRONT_ASSETS;?>components/revolution_slider/css/style.css" media="screen" />
        <!-- Main Style -->
        <link id="main-style" rel="stylesheet" type="text/css"  href="<?php echo base_url().FRONT_ASSETS;?>css/style-min.css?token=280120160411">
        
        <!-- Custom Styles -->
        <link rel="stylesheet" type="text/css"  href="<?php echo base_url().FRONT_ASSETS;?>css/custom.min.css?token=20062016"> 
        <!-- Responsive Styles -->
        <link rel="stylesheet" href="<?php echo base_url().FRONT_ASSETS;?>css/responsive.css">
       
        <script type="text/javascript">
            var base_url = "<?php echo base_url(); ?>";
            var front_assets = base_url+"<?php echo FRONT_ASSETS; ?>";
            var BASE_URL = base_url;
        </script>
        <script type="text/javascript" src="<?php echo base_url().FRONT_ASSETS;?>js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url().COMMON_ASSETS; ?>js/jquery.validate.min.js"></script>
        <style type="text/css">
            section#content{
                padding-top: 0px !important;
            }
        </style>
       
    </head>
    <body class="body-txt">
    <div id="page-wrapper">
        <!-- page wrapper start -->
<!--        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">FAQ's</h2>
                </div>
            </div> 
        </div>-->
 <section id="content" class="gray-area">
    <div class="container shortcode">
        <div class="block">
            <div class="row">
                <div class="col-sm-12">
                    <div class="toggle-container box faq_toggle" id="accordion1">
                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a href="#acc1" data-toggle="collapse" data-parent="#accordion1">
                                    What are the benefits of online bus booking?
                                </a>
                            </h4>
                            <div class="panel-collapse collapse in" id="acc1">
                                <div class="panel-content">
                                    <p>
                                        Online booking gives ample choice to passenger with regard to bus operators, departure and arrival timings, bus type, etc. Also multiple payment modes to chose from like debit card, credit card, internet banking, etc.
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>
                        
                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc2" data-toggle="collapse" data-parent="#accordion1">
                                    Is registration mandatory to use BoS portal for ticket booking?
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc2">
                                <div class="panel-content">
                                    <p>
                                        Registration is not a mandatory requirement to book tickets. However, during registration process basic details are captured like name, age, mobile number, email ID, etc. which can expedite booking process.
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>
                        
                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc3" data-toggle="collapse" data-parent="#accordion1">
                                    What do I need to prepare before booking online?
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc3">
                                <div class="panel-content">
                                    <p>
                                        All you need to have is a computer with internet connection or a smart phone with active data, valid email id and bank account with debit card or credit card for online payment.
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>


                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc4" data-toggle="collapse" data-parent="#accordion1">
                                    How do I book the seat online?
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc4">
                                <div class="panel-content">
                                    <p>It is extremely simple process:</p>
                                        <ul class="chevron box">
                                          <li>Select from and to destination</li>
                                          <li>Select the departure date and return date if you wish to book return journey and hit “Search” button</li>
                                          <li>Select trip from the list as per your suitable departure time,fare and comfort</li>
                                          <li>Give your email id and mobile number for eticket and mticket</li>
                                          <li>Provide your communication details</li>
                                          <li>Provide your Credit Card / Debit Card or net Banking details as per requirement of your Bank</li>
                                          <li>Confirm your online transaction and receiving of eticket and mticket </li>
                                        </ul>
                                    <p>
                                        Present Mticket/ SMS /eticket at boarding point and Print out of Ticket where mTicket,SMS, eticket are not accepted.
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>



                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc5" data-toggle="collapse" data-parent="#accordion1">
                                    What is a PNR?
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc5">
                                <div class="panel-content">
                                    <p>
                                        PNR is unique number and useful for faster and accurate search for various purposes, In Passenger Transport industry. A Passenger Name Record (PNR) is a record in the database of a Computer Reservation System (CRS) that contains the travel record for a passenger, or a group of passengers traveling together.
                                        When you book a ticket with BOS we generate a unique PNR number for you which will help you to track your ticket, print it or cancel it. 
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>

                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc6" data-toggle="collapse" data-parent="#accordion1">
                                    Can I book the return tickets?
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc6">
                                <div class="panel-content">
                                    <p>
                                        Booking a return ticket is similar to booking onward journey ticket. Just click on return ticket tab and follow the process just like one direction ticket booking.
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>


                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc7" data-toggle="collapse" data-parent="#accordion1">
                                    Will I see a confirmation message after I have made a booking online?
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc7">
                                <div class="panel-content">
                                    <p>
                                        Yes, you can see a confirmation page after making a successful booking. If you do not see one, please contact us at support@rokad.in
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>


                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc8" data-toggle="collapse" data-parent="#accordion1">
                                    What are the permissible limits for Luggage in bus?
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc8">
                                <div class="panel-content">
                                    <p>
                                        Cabin luggage limit is subjected to the bus operator's policy and at the risk of passenger and may vary from operator to operator; you can call our call center to obtain the specific details. Inflammable and prohibited items are not allowed to carry with luggage cabin.
                                        We request you to carry your valid ID card as some operators may need it for verification.
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>


                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc9" data-toggle="collapse" data-parent="#accordion1">
                                    Can partial cancellation be done (Eg: 3 to 2 seats)?
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc9">
                                <div class="panel-content">
                                    <p>
                                       Yes, we do allow partial cancellations. However, in certain  State Transport Undertaking like UPSRTC partial cancellation is not allowed. 
                                        However, please place your cancellation request on our website or through our call center 24 hrs prior to the departure time. 
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>


                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc10" data-toggle="collapse" data-parent="#accordion1">
                                    Will I receive intimation for successful booking of the ticket?
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc10">
                                <div class="panel-content">
                                    <p>
                                       Yes, BoS will send you a SMS, intimating you the success / failure of the ticket booking along with your PNR number. You can also view the details by login to your account.
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>



                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc11" data-toggle="collapse" data-parent="#accordion1">
                                   What if I do not get any confirmation message? 
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc11">
                                <div class="panel-content">
                                    <p>
                                       We have tried to make our system 100% error free. There might be a problem with your internet connection or some connectivity failures. Your transactions always done either completely or not done at all. In such situation we request you to contact our support team to confirm your booking. Or you can check your account balance immediately for debit and then ensure the seat is booked by you.
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>


                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc12" data-toggle="collapse" data-parent="#accordion1">
                                   Is printout of the ticket mandatory?
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc12">
                                <div class="panel-content">
                                    <p>
                                       SMS and mobile email tickets are accepted by majority of operators as valid ticket, as a go-green initiative. However, a few bus operators may insist on hard copy of ticket, this shall be informed to the passenger at the time of ticket booking.
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>


                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc13" data-toggle="collapse" data-parent="#accordion1">
                                   What should I do if I have lost my ticket?
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc13">
                                <div class="panel-content">
                                    <p>
                                       If you are a registered user then you may take print out of the ticket either from ‘Booking History’ by simply logging into your account else from the email sent to you at the time of booking ticket. You may also call any of our call centers and our executive will assist you with the same.
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>

                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc14" data-toggle="collapse" data-parent="#accordion1">
                                   I entered the wrong mobile number while booking or after booking mobile is out of service. Can I get my mobile Tickets on a different number?
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc14">
                                <div class="panel-content">
                                    <p>
                                       Please contact our call center and our executive will send you the mobile Tickets on your desired number.
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>


                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc15" data-toggle="collapse" data-parent="#accordion1">
                                   I have placed a request. How much time will it take to hear from you now?
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc15">
                                <div class="panel-content">
                                    <p>
                                       Response from BoS will depend on priority based on date of journey and queue of requests. However, for enhanced customer experience we shall respond at the earliest possible.
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>

                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc16" data-toggle="collapse" data-parent="#accordion1">
                                   Should the holder of the bank account who does the payment, be one of the passenger?
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc16">
                                <div class="panel-content">
                                    <p>
                                       Passenger can use anybody’s bank account or debit/credit card to pay for the ticket, not necessarily their own. 
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>


                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc17" data-toggle="collapse" data-parent="#accordion1">
                                   What are the various payment options available online to make payment?
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc17">
                                <div class="panel-content">
                                    <p>
                                       The different payment options are:
                                    </p>
                                    <p>
                                        <ul class="chevron box">
                                            <li>Credit card (Visa, MasterCard)</li>
                                            <li>Debit card</li>
                                            <li>Debit + Pin</li>
                                        </ul>
                                    </p>
                                    <p>
                                        Internet banking (Internet enabled online bank account)
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>


                    
                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc19" data-toggle="collapse" data-parent="#accordion1">
                                   How can I cancel my ticket?
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc19">
                                <div class="panel-content">
                                    <p>
                                       Ticket can be cancelled online by logging into your BoS profile or by calling on the call center after clearing security questions.
                                       Cancellation charges and policy depends on various bus operators individually. Passenger can check the cancellation policy at the time of booking ticket or anytime.
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>


                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc21" data-toggle="collapse" data-parent="#accordion1">
                                   What is BoS’s refund policy, if I cancel my ticket?
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc21">
                                <div class="panel-content">
                                    <p>
                                       The refund is provided as per cancellation policy of bus operator. Due refund will be credited to the bank account through which Ticket booking amount has been debited.
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>


                        <div class="panel style1">
                            <h4 class="panel-title">
                                <a class="collapsed" href="#acc22" data-toggle="collapse" data-parent="#accordion1">
                                   I have another question, apart from questions answered above.
                                </a>
                            </h4>
                            <div class="panel-collapse collapse" id="acc22">
                                <div class="panel-content">
                                    <p>
                                       For more questions and suggestions please be frank to reach us through feedback. Your questions will be answered ASAP by our support team.
                                   </p>
                                    <p>
                                       If you have other questions please <b><a class="theme-green-color" href="<?php echo base_url();?>contact_us">click here<a/></b>
                                    </p>
                                </div><!-- end content -->
                            </div>
                        </div>
                        <!-- ------------ -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
        <!-- Twitter Bootstrap -->
        <script type="text/javascript" src="<?php echo base_url().FRONT_ASSETS;?>js/bootstrap.min.js"></script>
    
     
      

      