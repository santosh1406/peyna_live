<!DOCTYPE html>
    <html> <!--<![endif]-->
    <head>
        <!-- Page Title --> 
        <title><?php echo (isset($metadata_title)) ? $metadata_title : $this->config->item('title', 'home_metadata'); ?></title>
        
        <!-- Meta Tags -->
        <meta charset="utf-8">
        <meta name="keywords" content="<?php echo (isset($metadata_keywords)) ? $metadata_keywords : $this->config->item('keywords', 'home_metadata'); ?>">
        <meta name="description" content="<?php echo (isset($metadata_description)) ? $metadata_description : $this->config->item('description', 'home_metadata'); ?>" />
        <meta name="author" content="Book On Spot">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <!-- <link href="<?php /*echo base_url().COMMON_ASSETS*/ ?>css/jquery-ui.css" rel="stylesheet"> -->
        <!--<link href="<?php /*echo base_url().COMMON_ASSETS*/;?>plugins/jquery-ui-custom/jquery-ui.min.css" rel="stylesheet">-->
        <link href="<?php echo base_url().COMMON_ASSETS;?>plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
        
        <!-- Theme Styles -->
        <link rel="stylesheet" href="<?php echo base_url().FRONT_ASSETS;?>css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url().FRONT_ASSETS;?>css/font-awesome.min.css">
        
        <!-- download font later -->
        <link href='//fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <link rel="icon" href="<?php echo base_url().FRONT_ASSETS;?>images/icon/favicon.ico?token=04022016" type="image/x-icon">
      
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().FRONT_ASSETS;?>components/revolution_slider/css/style.css" media="screen" />
        <!-- Main Style -->
        <link id="main-style" rel="stylesheet" type="text/css"  href="<?php echo base_url().FRONT_ASSETS;?>css/style-min.css?token=280120160411">
        <!-- Custom Styles -->
        <link rel="stylesheet" type="text/css"  href="<?php echo base_url().FRONT_ASSETS;?>css/custom.min.css?token=20062016"> 
        <!-- Responsive Styles -->
        <link rel="stylesheet" href="<?php echo base_url().FRONT_ASSETS;?>css/responsive.css">
       
        <script type="text/javascript">
            var base_url = "<?php echo base_url(); ?>";
            var front_assets = base_url+"<?php echo FRONT_ASSETS; ?>";
            var BASE_URL = base_url;
        </script>
        <script type="text/javascript" src="<?php echo base_url().FRONT_ASSETS;?>js/jquery-1.11.1.min.js"></script>
        <style type="text/css">
            section#content{
                padding-top: 0px !important;
            }
        </style>
    </head>
    <body class="body-txt">
    <div id="page-wrapper">
        <!--- Header start -->
<!--      <div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Terms and Condition</h2>
        </div>
    </div>
</div>-->


<section id="content">
    <div class="container">
        <div id="main" class="travelo-box">
            <div class="row terms-and-conditions">
                <div class="col-sm-12">
                    <p class="dropcap">
                        BookonSpot.com is one of the most accountable travel portals of India that takes care of the requirements of its clients. To maintain a perfect balance between the personal and professional relationship with its clients, it applies some terms and conditions.
                    </p>
                    <p>
                        This agreement (user agreement) incorporates the terms and conditions for BookonSpot.com (BoS) and its affiliate Companies to provide services to its users.
                    </p>
                    <p>
                        Both user and BookonSpot.com are individually referred as 'party' to the agreement and collective referred to as 'parties'.
                    </p>
                    <p>
                        BoS may add, change or remove any part of these Terms and Conditions at any time, without notice. Any changes to these Terms and Conditions or any terms posted on this Site apply as soon as they are posted. You shall re-visit the “Terms & Conditions” link from time to time to stay abreast of any changes that the “Site” may introduce.
                    </p>
                    <br/>
                    <center><h2><b class='underline'>TERMS AND CONDITIONS</b></h2></center>
                     <p>
                        The Terms and Conditions is a legal contract between you being, an individual customer, user, or beneficiary of this service, and Book on Spot (BoS) having its registered office at Mumbai.
                    </p>
                    <p>
                       While accessing, using, browsing or making a booking through BookonSpot.com, users have to accept that they have agreed to the terms and conditions of our portal. In case of any violation, BookonSpot reserves all the rights for taking any legal actions against them.
                    </p>
                    <p>
                        BookonSpot acts as an “Intermediary” solely to assist customers in gathering travel information, determining the availability of travel-related products and services, making legitimate reservations or otherwise transacting business with travel suppliers, and for facilitating travel requirements. You acknowledge that BoS merely provides intermediary services in order to facilitate these services. BoS is not the last mile service provider to you and therefore BoS shall not be deemed to be responsible for any lack or deficiency of services provided by any person or entity including bus operator, airline, hotel, activity provider or similar agency, you shall engage, hire from the content available on the Site.
                    </p>
                    <h5><b>Use of the Website</b></h5>
                    <p>As a condition of your use of this Website, you warrant that: </p>
                        <ul class="circle box orderlist">
                            <li>
                                You possess the legal authority to create a binding legal obligation and enter into these Terms of use
                            </li> 
                            <li>
                                You will use this Website in accordance with the Terms of use
                            </li>
                            <li>
                                You will use this Website to make only legitimate reservations for you or for another person for whom you are legally authorized to act
                            </li>
                            <li>
                                You will inform such other persons about the Terms of use that apply to the reservations you have made on their behalf, including all rules and restrictions applicable thereto
                            </li>
                            <li>
                                You will provide your proper and accurate Name (as per Passport or any other legal ID), Address, Email ID and Cell Number. You will be solely responsible for the Information provided and in case of any error or mistake in provision of information, BoS will not be liable for the same
                            </li>
                            <li>
                                If you have an online account with this Website, you will safeguard your login details – login ID and password and will supervise the same. You will be completely responsible for any use or misuse of your account by you or by any other person.
                            </li>
                            <li>
                                BoS retains the right in its sole discretion to deny access to anyone to this Website and the services we offer, at any time without notice and for any reason, including, but not limited to, for violation of these Terms of use.
                            </li>
                        </ul>
                    <h5><b>Communication Policy of the site</b></h5>
                    <p>
                        Upon transacting on the Site, you will receive an e-mail from BoS informing the status of your transaction on your registered email ID or the ID provided at the time of booking. You may also receive a SMS on your registered phone number. You are responsible for entering the proper and accurate contact details including your name, email ID, phone number to ensure that we can effectively communicate with you.
                    </p>
                    <p>
                        BoS is not responsible to provide information on any change in bus schedules, cancellation, status of bus operator etc.
                    </p>
                    <p>
                       Any grievance regarding the service should be communicated as per the grievance policy laid out herein.
                    </p>  
                    <h5><b>Content on the Site</b></h5>  
                    <p>
                        This Site is only for your personal use. You shall not copy, license, adapt, distribute, exchange, modify, sell or transmit any content or material from this Site, including but not limited to any text, images, audio, video or links for any business, commercial or public purpose.
                    </p> 
                    <p>
                        Access to certain areas of the Site may only be available to registered members. To become a registered member, you may be required to provide certain details. You represent and warrant that all information you supply to us, about yourself, and others, is true and accurate.
                    </p> 
                    <p>
                        You understand that except for information, products or services clearly indicated as being supplied by BoS, we do not operate, control, or endorse any information, products or services on the Internet in anyway. You also understand that BoS cannot and does not guarantee or warrant that files available for downloading through the Site will be free of viruses, worms or other code that may be damaging. You are responsible for implementing procedures to satisfy your particular requirements and for accuracy of data input and output.
                    </p>
                    <p>
                        BoS may add, change, discontinue, remove or suspend any Content or services posted on this Site, including features and specifications of products described or depicted on the Site, temporarily or permanently, at any time, without notice and without liability.
                    </p>
                    <h5><b>Confidentiality</b></h5>
                    <p>
                        Any information which is specifically mentioned by BookonSpot.com as Confidential shall be maintained confidentially by the user and shall not be disclosed unless as required by law or to serve the purpose of this agreement and the obligations of both the parties therein.
                    </p>
                    <h5><b>Copyright & Trademark</b></h5>
                    <p>
                        Products, services and contents including but not limited to audio, images, software, text, icons and such like (the “Content”) displaying on BookonSpot.com are the properties of this website. Any unauthorized copy & use of the trademark product & services without prior concern will be taken as larceny and strict actions may be taken against the concern person/organization/company.
                    </p>
                    <h5><b>Advertisers on BOS or linked websites</b></h5>
                    <p>
                        BoS is not responsible for any errors, omissions or representations on any of its pages or on any links or on any of the linked website pages. BoS does not endorse any advertiser on its web pages in any manner. The users are requested to verify the accuracy of all information on their own before undertaking any reliance on such information.
                    </p>
                    <p>
                        The linked sites are not under the control of BoS and BoS is not responsible for the contents of any linked site or any link contained in a linked site, or any changes or updates to such sites. BoS is providing these links to the users only as a convenience and the inclusion of any link does not imply endorsement of the site by BoS.
                    </p>
                    <h5><b>Usage of the Mobile Number of the User by BoS</b></h5>
                    <p>
                        BoS may send booking confirmation, cancellation, payment confirmation, refund status, schedule change or any such other information relevant for the transaction, via SMS or by voice call on the contact number given by the user at the time of booking; BoS may also contact the user by voice call, SMS or email in case the user couldn’t or hasn’t concluded the booking, for any reason what so ever, to know the preference of the user for concluding the booking and also to help the user for the same. The user hereby unconditionally consents that such communications via SMS and/ or voice call by BoS is (a) upon the request and authorization of the user, (b) ‘transactional’ and not an ‘unsolicited commercial communication’ as per the guidelines of Telecom Regulation Authority of India (TRAI) and (c) in compliance with the relevant guidelines of TRAI or such other authority in India and abroad. The user will indemnify BoS against all types of losses and damages incurred by BoS due to any action taken by TRAI, Access Providers (as per TRAI regulations) or any other authority due to any erroneous compliant raised by the user on BoS with respect to the intimations mentioned above or due to a wrong number or email id being provided by the user for any reason whatsoever.
                    </p>
                    <h5><b>Prices on the Website:</b></h5>
                    <p>
                        The price, which we offer on BookonSpot.com, includes the ticket cost, taxes and our fees. It never comprises any personal expense or other additional charges like accommodation, telephone calls, personal-man services.
                    </p>
                    <h5><b>Mode of Payments:</b></h5>
                    <p>
                        To take the advantage of any travel facility or services through BookonSpot.com, you have to make your payment in advance. It gives you the confirmation of the booking. To make the payments online, you can use Visa, Master cards. BookonSpot.com offers a safe transaction process and ensures you that your personal security codes will not be revealed in worst cases also.
                    </p>
                    <p>
                        For international payments, you can use Bank / Wire Transfer and transfer amount into our bank account together with the additional charges levied by the banks.
                    </p>
                    <h5><b>Amendment and Cancellation policy:</b></h5>
                    <p>
                        We assist you in making various amendments for bookings. In few cases, you need to contact the operators directly. Amendment charges on BookonSpot.com are subject to change as per the policy of the operators.
                    </p>
                    <p>
                        You can also do the amendments by directly calling to the operators Call Center.
                    </p>
                    <p>
                        For any amendment in tickets, you can contact our customer care executives.
                    </p>
                    <p>
                        In case of cancellation, you can cancel your tickets online by logging in to BookonSpot.com. Cancellation charges of the tickets may vary as per the policies the operators. Apart from the operator’s cancellation charges, BookonSpot.com levies negligible amount of cancellation service charges.
                    </p>
                    <h5><b>Refunds:</b></h5>
                    <p>
                        In case of cancellation, refunds will be affected  according to operator's cancellation policy charges along with our service charge if any as mentioned above. After departure & in case of No Show, we don't entertain any cancelation request.
                    </p>
                    <p>
                        Refund process can take duration of maximum 2-3 working days due to banking procedures. In case of Net Banking, it may take 7-8 working days.
                    </p>
                    <p>
                        Processing times for cancellation and refund requests may vary.
                    </p>
                     <p>
                       In case of unavailability of services, you will receive the complete refunds except the transaction charges.
                    </p>
                    <h5><b>Offers and Contests:</b></h5>
                     <p>
                        This site may contain contests that require you to send in material or information about yourself and offer prizes. Each offers, contest and interactions has its own rules, which you must read and agree to before you participate.
                    </p>
                    <h5><b>Disclaimer:</b></h5>
                     <p>
                        You acknowledge that Book on Spot is an intermediary and is not liable for any 3rd party (suppliers) obligations due to rates, quality, and all other instances, whether to any such subscribers or otherwise. You expressly agree that use of the services and the site is at your sole risk. It is your responsibility to evaluate the accuracy, completeness and usefulness of all opinions, advice, services, merchandise and other information provided through the site or on the internet generally. We do not warrant that the service will be uninterrupted or error-free or that defects in the site will be corrected.
                    </p>
                    <p>
                        Book on Spot may make changes or improvements at any time. The material in this Site could include technical inaccuracies or typographical errors. Book on Spot does not warrant that the functions contained in this site will be uninterrupted or error free, that defects will be corrected, or that this site or the servers that make it available are free of viruses or other harmful components, but shall endeavor to ensure your fullest satisfaction
                    </p>
                    <p>
                        Book on Spot does not accept any responsibility and will not be liable for any loss or damage whatsoever arising out of or in connection with any ability/inability to access or to use the Site.
                    </p>
                    <h5><b>Indemnification and Limitation of Liability:</b></h5>
                     <p>
                        You agree to indemnify, save, and hold BookonSpot, its affiliates, contractors, employees, officers, directors and agents harmless from any and all claims, losses, damages, and liabilities, costs and expenses, including without limitation legal fees and expenses, arising out of or related to your use or misuse of the Services or of the Site, any violation by you of this Agreement, or any breach of the representations, warranties, and covenants made by you herein.
                    </p>
                    <p>
                        In no event will BookonSpot be liable to you for any special, indirect, incidental, consequential, punitive, reliance, or exemplary damages (including without limitation lost business opportunities, lost revenues, or loss of anticipated profits or any other pecuniary or non-pecuniary loss or damage of any nature whatsoever) arising out of or relating to (i) this agreement, (ii) the services, the site or any reference site, or (iii) your use or inability to use the services, the site (including any and all materials) or any reference sites. In no event will BookonSpot or any of its contractors, directors, employees, agents, third party partners, licensors or suppliers’ total liability to you for all damages, liabilities, losses, and causes of action arising out of or relating to (i) this Agreement, (ii) the Services, (iii) your use or inability to use the Services or the Site (including any and all Materials) or any Reference Sites, or (iv) any other interactions with BookonSpot, however caused and whether arising in contract, tort including negligence, warranty or otherwise, exceed the amount paid by you, if any, for using the portion of the Services or the Site giving rise to the cause of action or One Thousand Rupees (Rs.1000), whichever is less.
                    </p>
                    <h5><b>Grievance Policy:</b></h5>
                     <p>
                        BookonSpot strongly believes in a sincere and transparent approach to its users. You trust and love us and we would never put growth before trust. This policy document aims at minimizing instances of customer complaints, grievances and disappointments via channelized approach, review and redressal.
                    </p>
                    <p>
                        You can file a grievance / share feedback if you are disappointed with the services rendered by BookonSpot or any other issues. You can give your grievance / feedback through email or phone call.
                    </p>
                    <p>
                        In order to make BookonSpot redresal channels more meaningful and effective, a structured system has been put in place. This system will ensure that the complaints are redressed seamlessly and well within the stipulated timeframe.
                    </p>
                    <h5><b>Jurisdiction:</b></h5>
                     <p>
                        BoS hereby expressly disclaims any implied warranties imputed by the laws of any jurisdiction or country other than those where it is operating its offices. BoS considers itself and intends to be subject to the jurisdiction only of the courts of Mumbai, India.
                    </p>
                    <h5><b class='underline'>Other Points:</b></h5>
                     <p>
                        BoS shall not be responsible or held liable for the behavior of operational staff in bus. BoS shall not be responsible or held liable if the bus does not stop at a pick up or drop point. BoS shall not be responsible or held liable if passenger reaches boarding point after the stipulated time. If there are any on board charges against anything by bus operator, then BoS is not responsible. BoS shall not be responsible or held liable for any bus delay due to breakdown or operational issues. BoS shall not be held liable for accident claims. BoS shall not be responsible or held liable for criminal/civil offense by the operator. Refund against cancellation shall be done as per policy of operator.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>