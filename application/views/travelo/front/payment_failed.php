<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Processing Payment</h2>
        </div>
    </div>
</div>
<section id="content">
    <!-- Wait Start Here -->
    <div class="modal fade show_waiting" id="show_waiting" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <!-- tab container start-->
            <center>
                <div class="search_progress">
                    <span>Ticket Booking Failed</span>
                    <br>
                    <span>Reason : <?php echo $msg ?></span>
                    <br>
                    <!-- <span class="loader_25"><img src="<?php echo base_url().COMMON_ASSETS ;?>images/loading.gif" alt="BOS Loading"></span> -->
                </div>
            </center>
            <!-- tab container end-->
          </div>
        </div>
      </div>
    </div>
    <!-- Wait End Here -->
</section>
<style type="text/css">
.modal {text-align: center;}
.modal:before {display: inline-block;vertical-align: middle;content: " ";height: 100%;}
.modal-dialog {display: inline-block;text-align: left;vertical-align: middle;}
</style>

<script type="text/javascript">

    $(document).ready(function(){
        // For while searching service
        $('#show_waiting').modal({
                                backdrop: 'static',
                                keyboard: false  // to prevent closing with Esc button (if you want this too)
                            })

        $("#show_waiting").modal("show");

        setTimeout(function() {
          window.location.href = "<?php echo base_url()."home"; ?>";
        }, 3000);
        //window.location.href = "<?php echo base_url()."home"; ?>";
        // $(location).attr("href", "<?php echo $redirect_url; ?>");
    }); // Document ready ends here 

</script>