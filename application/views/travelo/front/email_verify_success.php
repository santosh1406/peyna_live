<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Success</h2>
        </div>
    </div>
</div>

<section id="content">
    <div class="container tour-detail-page">
        <div class="row">
            <div class="col-md-9" id="main">
                
                <div class="travelo-box" id="tour-details">
                    <h1 class="page-title">Thank you for verifying your email address!</h1>
                </div>
            </div>
        </div>
    </div>
</section>
