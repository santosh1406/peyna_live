<?php
  $ontmdiff = calculate_date_diff($journey_date_time,$available_detail["arrival_time"]);
  $days    = $ontmdiff->format('%d');
  $hours   = $ontmdiff->format('%h');
  $minutes = $ontmdiff->format('%i');
  // $travel_time = (($days > 0) ? $days." day " : "").(($hours > 0) ? $hours." hours " : "").(($minutes > 0) ? $minutes." minutes" : "");
  $travel_time = calculate_travel_duration($ontmdiff);
  /********** Return ticket Detail Start**************************/
  
  $is_return_journey = false;
  if(isset($available_detail_return) && !empty($available_detail_return))
  {
      $is_return_journey = true;
      $retmdiff = calculate_date_diff($rjourney_date_time,$available_detail_return["arrival_time"]);
      $rdays    = $retmdiff->format('%d');
      $rhours   = $retmdiff->format('%h');
      $rminutes = $retmdiff->format('%i');
      // $rtravel_time = (($rdays > 0) ? $rdays." day " : "").(($rhours > 0) ? $rhours." hours " : "").(($rminutes > 0) ? $rminutes." minutes" : "");
      $rtravel_time = calculate_travel_duration($retmdiff);
  }
  /********** Return ticket Detail End**************************/
  $is_onward_journey_proof_required = false;
  $is_return_journey_proof_required = false;

  if(isset($available_detail['id_proof_required']) && $available_detail['id_proof_required'] == true)
  {
    $is_onward_journey_proof_required = true;
  }

  if(isset($available_detail_return['id_proof_required']) && $available_detail_return['id_proof_required'] == true)
  {
    $is_return_journey_proof_required = true;
  }
?>
<div class="page-title-container">
  <div class="container">
    <div class="page-title pull-left">
      <h2 class="entry-title">Provide your communication details</h2>
    </div>
  </div>
</div>
<section id="content">
  <div class="container">
    <div class="row">
      <div id="main" class="tickettop">
        <div class="col-md-8 personal-info-form personal-info-form">
          <?php 
              $attributes = array('class' => 'booking-form booking_process', 'id' => 'booking_process', 'method' => 'post');
              echo form_open(base_url().'front/booking/process_payment_confirm_booking', $attributes); 
          ?>
            <div class="person-information">
              <h2>Booking Details</h2>
              <div class="steps">
                <ul>
                  <li>
                    <div class="square-number-bg">1</div>
                    Select Seat & Boarding Point
                  </li>
                  <li class="active">
                    <div class="square-number-bg-active">2</div>
                    Travelers & Payment
                  </li>
                  <li>
                    <div class="square-number-bg">3</div>
                    Booking Payment
                  </li>
                </ul>
              </div><!-- steps -->
              <br>
              <h4 class="subtitle">Onward Journey Details</h4>
            <?php
              $seat_booked_count  = count($seat_booked);
              if($seat_booked_count)
              {
                foreach ($seat_booked as $berthno => $seatvalue)
                {
                  if($seat_booked_count == 2)
                  {
                    echo ($berthno > 1) ? "<h6>Upper Berth</h6>" : "<h6>Lower Berth</h6>";
                  }

                  foreach ($seatvalue as $i => $value)
                  {
                    $disable_male = "";
                    $female_checked = "";
                    $male_checked = "checked";
                    if(isset($available_detail["seats_details"][$berthno][$value]["is_ladies_seat"]) && $available_detail["seats_details"][$berthno][$value]["is_ladies_seat"])
                    {
                      $disable_male = "disabled";
                      $female_checked = "checked";
                      $male_checked = "";
                    }
              ?>
                    <div class="onward-journey-fav">
                      <div class="form-group row">
                        <div class="col-sm-5">
                          <input type="text" class="input-text full-width username reg-alpha-only" placeholder="Name" name="seat[<?php e($berthno); ?>][<?php e($i); ?>][name]" id="seat[<?php e($berthno); ?>][<?php e($i); ?>][name]" />
                          <input type="hidden" class="input-text full-width alpha-only" placeholder="Name" name="seat[<?php e($berthno); ?>][<?php e($i); ?>][seat_no]" id="seat[<?php e($berthno); ?>][<?php e($i); ?>][seat_no]" value="<?php e($value); ?>"/>
                        </div>
                        <div class="col-sm-2">
                          <input type="text" class="input-text full-width reg-number-only ticket_<?php echo strtolower($available_detail['op_name']);?>" placeholder="Age" name="seat[<?php e($berthno); ?>][<?php e($i); ?>][age]" maxlength="3"/>
                        </div>
                        <div class="col-sm-3 gender disp_radio">
                          <label class="radio radio-inline male_radio">
                              <input type="radio" class="gender" id="seat[<?php e($berthno); ?>][<?php e($i); ?>][gender]" name="seat[<?php e($berthno); ?>][<?php e($i); ?>][gender]" value="M" <?php e($disable_male." ".$male_checked) ;?> >
                              Male
                          </label>
                          <label class="radio radio-inline female_radio">
                              <input type="radio" class="gender" id="seat[<?php e($berthno); ?>][<?php e($i); ?>][gender]" name="seat[<?php e($berthno); ?>][<?php e($i); ?>][gender]" value="F" <?php e($female_checked) ;?>>
                              Female
                          </label>
                        </div>
                        <div class="col-sm-2 mt10">
                          <img src="<?php echo base_url().FRONT_IMAGES?>seats.jpg" alt="BookOnSpot Booked Seats">&nbsp;
                          <?php e($value); ?>
                        </div>
                      </div>
                    </div><!-- onward-journey-fav -->
              <?php 
                  }
                }
              }

              if($is_rseat_available)
              {
                  $rseat_booked_count = count($rseat_booked);
                  if(isset($rseat_booked) &&  $rseat_booked_count)
                  {
                    echo "<br/>";
                    echo "<h4 class='subtitle'>Return Journey Details</h4>";
                    foreach ($rseat_booked as $rberthno => $rseatvalue)
                    {
                      if($rseat_booked_count == 2)
                      {
                        echo ($rberthno > 1) ? "<h6>Upper Berth</h6>" : "<h6>Lower Berth</h6>";
                      }

                      foreach ($rseatvalue as $ri => $rvalue)
                      {
                        $rdisable_male = "";
                        $rfemale_checked = "";
                        $rmale_checked = "checked";
                        if(isset($available_detail_return["seats_details"][$rberthno][$rvalue]["is_ladies_seat"]) && $available_detail_return["seats_details"][$rberthno][$rvalue]["is_ladies_seat"])
                        {
                          $rdisable_male = "disabled";
                          $rfemale_checked = "checked";
                          $rmale_checked = "";
                        }
                ?>
                        <div class="return-journey-fav">
                          <div class="form-group row">
                            <div class="col-sm-5">
                              <input type="text" class="input-text full-width username reg-alpha-only" placeholder="Name" name="rseat[<?php e($rberthno); ?>][<?php e($ri); ?>][name]" id="seat[<?php e($rberthno); ?>][<?php e($ri); ?>][name]" />
                              <input type="hidden" class="input-text full-width alpha-only" placeholder="Name" name="rseat[<?php e($rberthno); ?>][<?php e($ri); ?>][seat_no]" id="seat[<?php e($rberthno); ?>][<?php e($ri); ?>][seat_no]" value="<?php e($rvalue); ?>"/>
                            </div>
                            <div class="col-sm-2">
                              <input type="text" class="input-text full-width reg-number-only return_ticket_<?php echo strtolower($available_detail_return['op_name']);?>" placeholder="Age" name="rseat[<?php e($rberthno); ?>][<?php e($ri); ?>][age]" placeholder="Age" maxlength="3"/>
                            </div>
                            <div class="col-sm-3 gender disp_radio">
                              <label class="radio radio-inline male_radio">
                                  <input type="radio" class="gender" id="rseat[<?php e($rberthno); ?>][<?php e($ri); ?>][gender]" name="rseat[<?php e($rberthno); ?>][<?php e($ri); ?>][gender]" value="M" <?php e($rdisable_male." ".$rmale_checked) ;?> >
                                  Male
                              </label>
                              <label class="radio radio-inline female_radio">
                                  <input type="radio" class="gender" id="rseat[<?php e($rberthno); ?>][<?php e($ri); ?>][gender]" name="rseat[<?php e($rberthno); ?>][<?php e($ri); ?>][gender]" value="F" <?php e($rfemale_checked) ;?>>
                                  Female
                              </label>
                            </div>
                            <div class="col-sm-2 mt10">
                              <img src="<?php echo base_url().FRONT_IMAGES?>seats.jpg" alt="BookOnSpot Booked Seats">&nbsp;
                              <?php e($rvalue); ?>
                            </div>
                          </div><!-- form-group row -->
                        </div ><!-- return-journey-fav -->
                <?php 
                    }
                  }
                }
              }

              $current_user_email = "";
              $current_user_mobileno = "";
              if($this->session->userdata('user_id') != "" && $this->session->userdata('role_name') != "support")
              {
                $current_user_email = $this->session->userdata('email');
                $current_user_mobileno = $this->session->userdata('mobile_no');
              }
            ?> 
            <?php
                $is_agent_id = false;
                eval(AGENT_ID_ARRAY);
                if(in_array($this->session->userdata("role_id"), $agent_id_array) || $this->session->userdata("role_id") == ORS_AGENT_USERS_ROLE_ID) {
                $is_agent_id = true;
             ?>
            <h4 class="subtitle">Customer Details</h4>
              <div class="form-group row">
                <div class="col-sm-6">
                  <input type="text" class="input-text full-width bg-change" placeholder="Email Address" id="email" name="email_id" />
                </div>
                <div class="col-sm-6">
                  <input type="text" class="input-text full-width  bg-change reg-number-only" placeholder="Mobile Number" id="mobile_no" name="mobile_no" maxlength="10"/>
                </div>
              </div>
            <h4 class="subtitle">Agent Details</h4>
              <div class="form-group row">
                <div class="col-sm-6">
                  <input type="text" class="input-text full-width bg-change" placeholder="Email Address" id="agent_email_id" name="agent_email_id" value="<?php echo $current_user_email; ?>" />
                </div>
                <div class="col-sm-6">
                  <input type="text" class="input-text full-width  bg-change reg-number-only" placeholder="Mobile Number" id="agent_mobile_no" name="agent_mobile_no" maxlength="10" value="<?php echo $current_user_mobileno; ?>" />
                </div>
              </div>
                   
            <?php } else{ ?> 
            <h4 class="subtitle">Personal Details</h4> 
              <div class="form-group row">
                <div class="col-sm-6">
                  <input type="text" class="input-text full-width bg-change" placeholder="Email Address" id="email" name="email_id" value="<?php echo $current_user_email; ?>"/>
                </div>
                <div class="col-sm-6">
                  <input type="text" class="input-text full-width  bg-change reg-number-only" placeholder="Mobile Number" id="mobile_no" name="mobile_no" maxlength="10" value="<?php echo $current_user_mobileno; ?>"/>
                </div>
              </div>
            <?php } ?>
            <!--?php eval(AGENT_ID_ARRAY_TICKET_DETAIL);
               if(in_array($this->session->userdata("role_id"), $agent_id_array_ticket_detail)) {?>
              
               <!--?php }?-->

              <?php
                if($available_detail["id_proof_required"])
                {
              ?>
                  <h4>Onward Journey Primary Passenger Id Proof</h4>
                  <div class="form-group row">
                    <div class="col-sm-5">
                      <input type="text" class="input-text full-width" placeholder="Id Type" id="id_type" name="id_type" value=""/>
                    </div>
                    <div class="col-sm-5">
                      <input type="text" class="input-text full-width reg-alpha-only" placeholder="Name On Id" id="name_on_id" name="name_on_id"/>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-5">
                      <input type="text" class="input-text full-width" placeholder="Id/Card Number" id="id_card_number" name="id_card_number"/>
                    </div>
                  </div>
              <?php
                }

              if(isset($available_detail_return["id_proof_required"]) && $available_detail_return["id_proof_required"])
              {
              ?>
                <h4>Return Journey Primary Passenger Id Proof</h4>
                <div class="form-group row">
                  <div class="col-sm-5">
                    <input type="text" class="input-text full-width" placeholder="Id Type" id="rid_type" name="rid_type" value=""/>
                  </div>
                  <div class="col-sm-5">
                    <input type="text" class="input-text full-width reg-alpha-only" placeholder="Name On Id" id="rname_on_id" name="rname_on_id"/>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-5">
                    <input type="text" class="input-text full-width" placeholder="Id/Card Number" id="rid_card_number" name="rid_card_number"/>
                  </div>
                </div>
              <?php
              }
              ?>
              <span class="error-befor-button error-message-format"></span>
              <div class="form-group">
                <button class="btn-large btnorange" type="submit">CONFIRM BOOKING</button>
              </div>
            </div><!-- end person-information -->
          <?php echo form_close(); ?>
        </div><!-- personal-info-form -->


        <div class="col-md-4">
          <div class="detailed-journey booking-detail-journey pad0 border-none">
            <div class="travelo-box booking-details">
              <h3>Ticket Details</h3>
            <div class="tab-container style1">
              <ul class="tabs full-width">
                <li class="active">
                  <a href="#onward-journey" data-toggle="tab">
                    <i class="fa fa-bus"></i>
                  </a>
                </li>
                <?php
                  if($is_rseat_available)
                  {
                ?>
                    <li>
                      <a href="#return-journey" data-toggle="tab">
                        <i class="fa fa-bus"></i>
                      </a>
                    </li>
                <?php
                  }
                ?>
              </ul>   
              <div class="panel-group acc-v1" id="accordion-1">          
                <div class="tab-content">
                  <div class="tab-pane fade in active" id="onward-journey">
                    <div class="onwardjourney">
                      <h5 class="subtitle-new">Onwards Journey</h5>                              
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-One">
                          Booking Details
                          <span class="icon-arrow-down fa fa-angle-down"></span>
                        </a>
                        </h4>
                      </div>
                      <div id="collapse-One" class="panel-collapse collapse collapse-this in">
                        <div class="panel-body">
                          <article class="bus-booking-details">
                            <div class="travel-title">
                              <div class="info_tickets_details">
                                <div class="travel-name"><?php echo $available_detail["op_name"];?></div>
                                <ul>
                                  <li>From<h4><?php echo ucwords(strtolower($available_detail["boarding_stop_name"])); ?></h4></li>
                                  <li>To <h4><?php echo ucwords(strtolower($available_detail["destination_stop_name"])); ?></h4></li>
                                </ul>
                              </div>
                              <div class="info_tickets_details">
                              <ul>
                                <li class="check-in">Departure<h4><?php echo $journey_date;?><br><?php echo $journey_date_hs;?></h4></li>
                                <li class="check-out">Arrival <h4><?php echo $arrival_time;?><br><?php echo $arrival_time_hs;?></h4></li>
                              </ul>
                              </div>
                              <div class="duration text-center">
                                <i class="soap-icon-clock"></i> &nbsp;
                                <span><?php echo $travel_time;?></span>
                              </div>
                            </div><!-- travel-title -->
                          </article>
                        </div>
                      </div><!-- collapse-One -->
                    </div><!-- panel panel-default -->
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-Two">
                            Other Details
                            <span class="icon-arrow-down fa fa-angle-down"></span>
                          </a>
                        </h4>
                      </div>
                      <div id="collapse-Two" class="panel-collapse collapse collapse-this in">
                        <div class="panel-body">
                          <dl class="other-details">
                            <dt class="feature txt-chg">Seat No(s):</dt>
                            <dd class="value">
                              <img src="<?php echo base_url().FRONT_IMAGES?>seats.jpg" alt="BookOnSpot Booked Seats">
                              <?php 
                                if($seat_booked_count == 2)
                                {
                                  if(isset($seat_booked[1]))
                                  {
                                    echo "Lower Berth : ".implode(",", $seat_booked[1])."<br/>";
                                  }

                                  if(isset($seat_booked[2]))
                                  {
                                    echo "Upper Berth : ".implode(",", $seat_booked[2]);
                                  }
                                }
                                else if($seat_booked_count)
                                {
                                  if(isset($seat_booked[1]))
                                  {
                                    echo implode(",", $seat_booked[1]);
                                  }
                                  if(isset($seat_booked[2]))
                                  {
                                    echo implode(",", $seat_booked[2]);
                                  }
                                }
                              ?>
                            </dd>
                            <dt class="feature txt-chg">Bus Type:</dt>
                            <?php
                            if(is_array($bus_type) && count($bus_type) > 0)
                            {
                              $bus_type_name = "";
                              $bus_type_name .= (isset($bus_type["op_bus_type_name"])) ? $bus_type["op_bus_type_name"]."  " : "";
                              $bus_type_name .= (isset($bus_type["bus_category"])) ? $bus_type["bus_category"] : "";
                              echo '<dd class="value">'.ucwords($bus_type_name).'</dd>';
                            }
                            else
                            {
                              echo '<dd class="value">NA</dd>';
                            }
                            ?>                                        
                          </dl>
                        </div>
                      </div>
                    </div><!-- panel panel-default -->
                  </div><!-- onward-journey -->
                  <?php
                  if($is_rseat_available)
                  {
                  ?>
                  <div class="tab-pane fade" id="return-journey">
                    <div class="onwardjourney">
                      <h5 class="subtitle-new">Return Journey</h5>
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                          <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-Four">
                            Booking Details
                            <span class="icon-arrow-down fa fa-angle-down"></span>
                          </a>
                          </h4>
                        </div>
                        <div id="collapse-Four" class="panel-collapse collapse collapse-this in">
                          <div class="panel-body">
                            <article class="bus-booking-details">
                              <div class="travel-title">
                                <div class="info_tickets_details">
                                  <div class="travel-name"><?php echo $available_detail_return["op_name"];?></div>
                                  <ul>
                                    <li>From<h4><?php echo ucwords(strtolower($available_detail_return["boarding_stop_name"])); ?></h4></li>
                                    <li>To <h4><?php echo ucwords(strtolower($available_detail_return["destination_stop_name"])); ?></h4></li>
                                  </ul>
                                </div>
                                <div class="info_tickets_details">
                                  <ul>
                                    <li class="check-in">Departure<h4><?php echo $rjourney_date;?><br><?php echo $rjourney_date_hs;?></h4></li>
                                    <li class="check-out">Arrival <h4><?php echo $rarrival_time;?><br><?php echo $rarrival_time_hs;?></h4></li>
                                  </ul>
                                </div>
                                <div class="duration text-center">
                                  <i class="soap-icon-clock"></i>
                                  <span><?php echo $rtravel_time;?></span>
                                </div>
                              </div>
                            </article>
                          </div>
                        </div>
                      </div> <!-- panel panel-default --> 
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-Five">
                              Other Details
                              <span class="icon-arrow-down fa fa-angle-down"></span>
                            </a>
                          </h4>
                        </div>
                        <div id="collapse-Five" class="panel-collapse collapse collapse-this in">
                          <div class="panel-body">
                            <dl class="other-details">
                              <dt class="feature txt-chg">Seat No(s):</dt>
                              <dd class="value">
                                <img src="<?php echo base_url().FRONT_IMAGES?>seats.jpg" alt="BookOnSpot Booked Seats">
                                <?php 
                                  if($rseat_booked_count == 2)
                                  {
                                    if(isset($rseat_booked[1]))
                                    {
                                      echo "Lower Berth : ".implode(",", $rseat_booked[1])."<br/>";
                                    }
                                    if(isset($rseat_booked[2]))
                                    {
                                      echo "Upper Berth : ".implode(",", $rseat_booked[2]);
                                    }
                                  }
                                  else if($rseat_booked_count)
                                  {
                                    echo implode(",", $rseat_booked[1]);
                                  }
                                ?>
                              </dd>
                              <dt class="feature txt-chg">Bus Type:</dt>
                              <?php
                                if(is_array($rbus_type) && count($rbus_type) > 0)
                                {
                                  $rbus_type_name = "";
                                  $rbus_type_name .= (isset($rbus_type["op_bus_type_name"])) ? $rbus_type["op_bus_type_name"]."  " : "";
                                  $rbus_type_name .= (isset($rbus_type["bus_category"])) ? $rbus_type["bus_category"] : "";
                                  echo '<dd class="value">'.ucwords($rbus_type_name).'</dd>';
                                }
                                else
                                {
                                  echo '<dd class="value">NA</dd>';
                                }
                              ?>                                        
                            </dl>
                          </div>
                        </div>
                      </div>
                      
                    </div><!-- onwardjourney -->
                  </div><!-- return-journey -->
                  <?php
                    }
                  ?>
                </div><!-- tab-content -->
              </div><!-- accordion-1 -->
            </div><!-- tab-container style1 -->
            </div><!-- travelo-box booking-details -->
          </div><!-- detailed-journey -->
        </div> <!-- col-md-4 -->
      </div><!-- id main -->
    </div><!-- row -->
  </div><!-- container -->
</section>


<script type="text/javascript">
    $(document).ready(function(){

      $.validator.addMethod("useremail", function (value, element) {
        
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if(value == '')
        {
            return true;
        }else{
        return filter.test(value);
    }

      }, 'Please enter valid email address');
      
      $.validator.addMethod("agent_email_id", function (value, element) {
        
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return filter.test(value);

      }, 'Please enter valid email address');

      $.validator.addMethod("checkAge", function (value, element) {
        return element.value > 5
      }, 'Below 5 year age ticket not required in case of MSRTC/UPSRTC');

      var cbooking_rules = new Object();
      var cbooking_messages = new Object();
      var cbooking_group = "";
      $('input[name^=seat]:text').each(function() {
          if($(this).hasClass("ticket_msrtc") || $(this).hasClass("ticket_upsrtc"))
          {
            cbooking_rules[this.name] = { required: true,checkAge:true };
            cbooking_messages[this.name] = { required:"Please enter onward journey passanger details.",checkAge: 'Below 5 year age ticket not required in case of MSRTC/UPSRTC (Onward Journey).' };
          }
          else
          {
            cbooking_rules[this.name] = { required: true };
            cbooking_messages[this.name] = { required: 'Please enter onward journey passanger details.' };
          }
          cbooking_group += this.name+" ";
      });

      $('input[name^=rseat]:text').each(function() {
          if($(this).hasClass("return_ticket_msrtc") || $(this).hasClass("return_ticket_upsrtc"))
          {
            cbooking_rules[this.name] = { required: true,checkAge:true };
            cbooking_messages[this.name] = { required:"Please enter onward journey passanger details.",checkAge: 'Below 5 year age ticket not required in case of MSRTC/UPSRTC (Return Journey).' };
          }
          else
          { 
            cbooking_rules[this.name] = { required: true };
            cbooking_messages[this.name] = { required: 'Please enter return journey passanger details.' };
          }
          cbooking_group += this.name+" ";
      });

      if("<?php echo $is_return_journey_proof_required;?>" == true)
      {
        cbooking_rules["rid_type"] = { required: true,};
        cbooking_messages["rid_type"] = { required : "Please enter id type (return journey)" };
        cbooking_rules["rname_on_id"] = { required: true,};
        cbooking_messages["rname_on_id"] = { required : "Please enter name on id (return journey)"};
        cbooking_rules["rid_card_number"] = { required: true,};
        cbooking_messages["rid_card_number"] = { required : "Please enter id number (return journey)"};
      }

      if("<?php echo $is_onward_journey_proof_required;?>" == true)
      {
        cbooking_rules["id_type"] = { required: true,};
        cbooking_messages["id_type"] = { required : "Please enter id type (onward journey)" };
        cbooking_rules["name_on_id"] = { required: true,};
        cbooking_messages["name_on_id"] = { required : "Please enter name on id (onward journey)"};
        cbooking_rules["id_card_number"] = { required: true,};
        cbooking_messages["id_card_number"] = { required : "Please enter id number (onward journey)"};
      }
      
      if("<?php echo $is_agent_id; ?>" == true)
        {
            cbooking_rules["email_id"] = { required: false,useremail:true };
            cbooking_rules["agent_email_id"] = {required: true,agent_email_id:true };
            cbooking_rules["mobile_no"] = { required: true, maxlength:10,minlength:10 };
            cbooking_rules["agent_mobile_no"] = { maxlength:10,minlength:10 };
//            cbooking_messages["email_id"] = { required : "Please enter customer email address" };
            cbooking_messages["agent_email_id"] = { required : "Please enter agent email address" };
            cbooking_messages["mobile_no"] = { required : "Please enter customer mobile number",maxlength : "Please provide mobile no with 10 digit",minlength : "Please provide mobile no with 10 digit" };
            cbooking_messages["agent_mobile_no"] = {required : "Please enter agent mobile number",maxlength : "Please provide mobile no with 10 digit",minlength : "Please provide mobile no with 10 digit" };
        }
      else
        {
            cbooking_rules["email_id"] = { required: true,agent_email_id:true };
            cbooking_rules["mobile_no"] = { required: true, maxlength:10,minlength:10 };
            cbooking_messages["email_id"] = { required : "Please enter email address" };
            cbooking_messages["mobile_no"] = { required : "Please enter mobile number",maxlength : "Please provide mobile no with 10 digit",minlength : "Please provide mobile no with 10 digit" };
        }

      $('#booking_process').validate({
        rules: cbooking_rules,
        messages: cbooking_messages,
        groups: {fullname: cbooking_group},
        errorPlacement: function(error, element) {
            error.appendTo(".error-befor-button");
        }
      });

     /* fav list Start Here */
     $(document).off('click', '.apply_fav_list').on('click', '.apply_fav_list', function(e) {
        var $list = $(this).closest('form').find('input:checkbox');
        var count = 0;

        $("#booking_process .onward-journey-fav .row").find("input:text").val("");
        $("#booking_process .onward-journey-fav .row").find(".gender[value='M']").attr("checked","checked");
        $("#booking_process .return-journey-fav .row").find("input:text").val("");
        $("#booking_process .return-journey-fav .row").find(".gender[value='M']").attr("checked","checked");

        $list.each(function(index, value) {
          if($(this).prop("checked"))
          {
            $("#booking_process .onward-journey-fav .row").eq(count).find("input:text.username").val($(this).val().trim());
            $("#booking_process .onward-journey-fav .row").eq(count).find("input:text.reg-number-only").val($(this).attr("data-age"));
            $("#booking_process .onward-journey-fav .row").eq(count).find(".gender[value='"+$(this).attr("data-gender")+"']").attr("checked","checked");


            $("#booking_process .return-journey-fav .row").eq(count).find("input:text.username").val($(this).val().trim());
            $("#booking_process .return-journey-fav .row").eq(count).find("input:text.reg-number-only").val($(this).attr("data-age"));
            $("#booking_process .return-journey-fav .row").eq(count).find(".gender[value='"+$(this).attr("data-gender")+"']").attr("checked","checked");
            count++;
          }
        });

        //for radio reload IMP
        travelo_radio();
     });
     /* fav list End Here */

     $(document).off('click', '.favourite-traveller').on('click', '.favourite-traveller', function(e) {
        
        var current = $(this).attr("data-current-ref");
        var target = $(this).attr("data-target-ref");
        $('html, body').animate({
          scrollTop: $("#accordion-1").offset().top
        }, 1000);

        $(".collapse-this").removeClass("in");
        $("."+current).hide(1000);
        $("."+target).show(1000);
     });


      /* traveller list Start Here */
     $(document).off('click', '.passangers_travellers_list div.checkbox input:checkbox').on('click', '.passangers_travellers_list div.checkbox input:checkbox', function(e) {
        var $list = $(this);
        var count = 0;

        $("#booking_process .onward-journey-fav .row").find("input:text").val("");
        $("#booking_process .onward-journey-fav .row").find(".gender[value='M']").attr("checked","checked");
        $("#booking_process .return-journey-fav .row").find("input:text").val("");
        $("#booking_process .return-journey-fav .row").find(".gender[value='M']").attr("checked","checked");

        $(".passangers_travellers_list div.checkbox input:checkbox").each(function(index, value) {
          if($(this).prop("checked"))
          {
            $("#booking_process .onward-journey-fav .row").eq(count).find("input:text.username").val($(this).val().trim());
            $("#booking_process .onward-journey-fav .row").eq(count).find("input:text.reg-number-only").val($(this).attr("data-age"));
            $("#booking_process .onward-journey-fav .row").eq(count).find(".gender[value='"+$(this).attr("data-gender")+"']").attr("checked","checked");


            $("#booking_process .return-journey-fav .row").eq(count).find("input:text.username").val($(this).val().trim());
            $("#booking_process .return-journey-fav .row").eq(count).find("input:text.reg-number-only").val($(this).attr("data-age"));
            $("#booking_process .return-journey-fav .row").eq(count).find(".gender[value='"+$(this).attr("data-gender")+"']").attr("checked","checked");
            count++;
          }
        });

        //for radio reload IMP
        travelo_radio();
     });
     /* traveller list End Here */
     

}); // end of document ready
</script>