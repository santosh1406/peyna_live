		<!-- Inner Wrapper Start -->
		<div id="page-wrapper">
            
            	<!-- Services Start -->
            	<div class="services">
                	<div class="container-fluid">
              			<div class="row">
                          <div class="col-md-12">
                                <div class="col-md-2 form-group">
                                    <div class="icon">
                                        <a href="#">
                                        <img src="<?php echo base_url().FRONT_ASSETS;?>images/cash-cards.png" class="img-responsive"/>
                                        <h5><strong>Smart Cards</strong></h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-2 form-group">
                                    <div class="icon">
                                        <a href="#">
                                        <img src="<?php echo base_url().FRONT_ASSETS;?>images/domestic-money-transfer.png" class="img-responsive"/>
                                        <h5><strong>Domestic Money Transfer</strong></h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-2 form-group">
                                    <div class="icon">
                                        <a href="#">
                                        <img src="<?php echo base_url().FRONT_ASSETS;?>images/mobile.png" class="img-responsive"/>
                                        <h5><strong>Mobile</strong></h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-2 form-group">
                                    <div class="icon">
                                        <a href="#">
                                        <img src="<?php echo base_url().FRONT_ASSETS;?>images/dth.png" class="img-responsive"/>
                                        <h5><strong>DTH Recharge</strong></h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-2 form-group">
                                    <div class="icon">
                                        <a href="#">
                                        <img src="<?php echo base_url().FRONT_ASSETS;?>images/micro-atm-service.png" class="img-responsive"/>
                                        <h5><strong>Micro ATM Service</strong></h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-2 form-group">
                                    <div class="icon">
                                        <a href="#">
                                        <img src="<?php echo base_url().FRONT_ASSETS;?>images/gold-loan.png" class="img-responsive"/>
                                        <h5><strong>Aadhaar Enabled Payment System (AEPS)</strong></h5>
                                        </a>
                                    </div>
                                </div>


                                <div class="col-md-2 form-group">
                                    <div class="icon">
                                        <a href="#">
                                        <img src="<?php echo base_url().FRONT_ASSETS;?>images/current-bus-booking.png" class="img-responsive"/>
                                        <h5><strong>Current Bus Booking</strong></h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-2 form-group">
                                    <div class="icon">
                                        <a href="#">
                                        <img src="<?php echo base_url().FRONT_ASSETS;?>images/advance-ticket-booking.png" class="img-responsive"/>
                                        <h5><strong>Advance Ticket Booking</strong></h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-2 form-group">
                                    <div class="icon">
                                        <a href="#">
                                        <img src="<?php echo base_url().FRONT_ASSETS;?>images/air-hotel-booking.png" class="img-responsive"/>
                                        <h5><strong>Air & Hotel Bookings</strong></h5>
                                        </a>
                                    </div>
                                </div>
                                
                                
                                
                                
                            </div>
                        
                        <div class="col-md-12">
                            <div class="col-md-2 form-group">
                                    <div class="icon">
                                        <a href="#">
                                        <img src="<?php echo base_url().FRONT_ASSETS;?>images/rail-booking.png" class="img-responsive"/>
                                        <h5><strong>Rail Bookings</strong></h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-2 form-group">
                                    <div class="icon">
                                        <a href="#">
                                        <img src="<?php echo base_url().FRONT_ASSETS;?>images/movies.png" class="img-responsive"/>
                                        <h5><strong>Movies</strong></h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-2 form-group">
                                    <div class="icon">
                                        <a href="#">
                                        <img src="<?php echo base_url().FRONT_ASSETS;?>images/electricity.png" class="img-responsive"/>
                                        <h5><strong>Electricity</strong></h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-2 form-group">
                                    <div class="icon">
                                        <a href="#">
                                        <img src="<?php echo base_url().FRONT_ASSETS;?>images/water.png" class="img-responsive"/>
                                        <h5><strong>Water</strong></h5>
                                        </a>
                                    </div>
                                </div>
                        	<div class="col-md-2 form-group">
                                <div class="icon">
                                    <a href="#">
                                    <img src="<?php echo base_url().FRONT_ASSETS;?>images/insurance.png" class="img-responsive"/>
                                    <h5><strong>Insurance</strong></h5>
                                    </a>
                                </div>
                            </div>
                            
                            
                            <div class="col-md-2 form-group">
                            	<div class="icon">
                                    <a href="#">
                                    <img src="<?php echo base_url().FRONT_ASSETS;?>images/kiosk-banking.png" class="img-responsive"/>
                                    <h5><strong>Kiosk Banking</strong></h5>
                                    </a>
                                </div>
                            </div>
                            
                            <div class="col-md-2 form-group">
                            	<div class="icon">
                                    <a href="https://www.epaylater.in" target="_blank">
                                    <img src="<?php echo base_url().FRONT_ASSETS;?>images/ePaylater.png" class="img-responsive"/>
                                    <h5><strong>ePaylater</strong></h5>
                                    </a>
                                </div>
                            </div>
                            
                            
                            <div class="col-md-2 form-group">
                            	<div class="icon">
                                    <a href="#">
                                    <img src="<?php echo base_url().FRONT_ASSETS;?>images/car-rental.png" class="img-responsive"/>
                                    <h5><strong>Car Rental</strong></h5>
                                    </a>
                                </div>
                            </div>
                           </div>
                      </div>
                    </div>
              	</div>
                <!-- Services End -->
          	
            	<!-- Count Start -->
                <div class="count">
                	<div class="container-fluid">
                        <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <!--<i class="fa fa-comments fa-5x"></i>-->
                                            <img src="<?php echo base_url().FRONT_ASSETS;?>images/Agents.png" class="img-responsive"/>
                                        </div>
                                        <div class="col-xs-12 text-center">
                                            <div><h1>20,000+</h1></div>
                                            <div><h3>Agents</h3></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-lg-4 col-md-6">
                            <div class="panel panel-green">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <!--<i class="fa fa-comments fa-5x"></i>-->
                                            <img src="<?php echo base_url().FRONT_ASSETS;?>images/Consumers.png" class="img-responsive"/>
                                        </div>
                                        <div class="col-xs-12 text-center">
                                            <div><h1>300+</h1></div>
                                            <div><h3>Consumers</h3></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="panel panel-yellow">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <!--<i class="fa fa-comments fa-5x"></i>-->
                                            <img src="<?php echo base_url().FRONT_ASSETS;?>images/new-users.png" class="img-responsive"/>
                                        </div>
                                        <div class="col-xs-12 text-center">
                                            <div><h1>1,000+</h1></div>
                                            <div><h3>Consumers</h3></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                	</div>
                </div>
                <!-- Count End -->
                
                <!-- Col Content Start -->
                <div class="col-content">
                	<div class="container-fluid">
                		<div class="row">
                           <div class="col-lg-6 col-md-6">
                                <div class="well">
                                    <h2 class="text-center">Advantages to Agents / Franchisee</h2>
                                    <div class="row">
                                        <div class="col-md-6">
                                        	<ul>
                                                <li> Monthly realization of profit / commission.</li>
                                                <li> Expansion in business due to increased footfalls & value added services.</li>
                                                <li> Risk free business.</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6">
                                        	<ul>
                                                <li> Increase in Revenue on the same fleet resulting in additional profits.</li>
                                                <li> Job creations in Rural & Urban localities.</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           <div class="col-lg-6 col-md-6">
                                <div class="well1">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <ul>
                                                <li> 
                                                    <h4>Income from Core Business Correspondent (BC) Activities</h4>
                                                    <p>Franchisees to be appointed as Bank's Business Correspondents and earn percentage based fees on money transfer, cash deposit, cash withdrawal, bill payments and other services to customer.</p>
                                                </li>
                                                <li> 
                                                    <h4>Income from services provided for Bus Corporation's SmartCard</h4>
                                                    <p>Franchisee will be authorised to Issue, Load & Surrender the Bus Corporation SmartCard and earn a flat fee on every activity.</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                  	</div>
                </div>
                <!-- Col Content End -->
        </div>
        <!-- Inner Wrapper End -->
        
        