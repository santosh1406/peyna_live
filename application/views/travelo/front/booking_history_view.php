<style type="text/css">
	#booking .booking-history .booking-info .action{ float: right; width: 150px;}
	#booking .booking-history .booking-info .action .btn-mini{ margin-top: 5px;}
	.booking-info  .passenger_info{ display: none}
</style>

<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Booking History</h2>
        </div>
    </div>
</div>

<section id="content">
	<div class="container">
		<div id="main">
	
<?php

	$ticket_details_array = array();
	foreach ($ticket_details as $key => $value) {
		$ticket_details_array[$value["pnr_no"]][] = $value;
	}
?>	


<div class="pad20" style="background: #fff;">
<div id="booking" style="background: #fff;">
    <h2>Trips You have Booked!</h2>
    <!-- <div class="filter-section gray-area clearfix">
    </div>
	-->
	<div class="booking-history">
	<?php
		if(count($ticket_details_array) > 0)
		{
			$count = 0;
			foreach($ticket_details_array as $pnr => $detail) {
				$count++;

            $historytask_class = "";
            $cancelticket_class = "";
            $active_class = "";
            $outdated = "";
            $cancelled = "";
            if($detail[0]['ticket_status'] == 'Y')
            {
                $historytask_class = "historytask";
                $cancelticket_class = "cancelticket";
                $active_class = "active";
            }

            $button_name = "UPCOMMING";

            if(strtotime(date('y-m-d H:i:s')) < strtotime($detail[0]['dept_time']) &&  $detail[0]['ticket_status'] == 'Y')
            {
                $button_name = "UPCOMMING";
            }
            else if($detail[0]['ticket_status'] == 'N')
            {
                $button_name = "CANCELLED";
                $cancelled = "cancelled";
            }
            else
            {
                $historytask_class = "";
                $cancelticket_class = "";
                $active_class = "";
                $button_name = "OUTDATED";
                $outdated = "outdated";
            }
	   ?>
		<div class="booking-info clearfix listing-style3 <?php echo  $cancelled.' '.$outdated; ?>">
			<div class="ticket_info">
	            <div class="date">
	                <label class="month"><?php echo date("M",strtotime($detail[0]["dept_time"])); ?></label>
	                <label class="date"><?php echo date("d",strtotime($detail[0]["dept_time"])); ?></label>
	                <label class="day"><?php echo date("D",strtotime($detail[0]["dept_time"])); ?></label>
	            </div>
	            <h4 class="box-title">
	            	<i class="icon fa fa-bus yellow-color circle"></i>
	            	<?php  echo $detail[0]["from_stop_name"] .' to '.$detail[0]["till_stop_name"] ?> <small><?php echo $detail[0]["op_name"]; ?></small>
	            </h4>
                <dl class="info">
                    <dt>Total Fare</dt>
                    <dd>Rs. <?php echo $detail[0]["tot_fare_amt_with_tax"]; ?></dd>
                    <dt>Booked on</dt>
                    <dd><?php echo date("D, d M, Y",strtotime($detail[0]["inserted_date"]))?></dd>
                </dl>
                 <dl class="info">
                    <dt>BOS Reference No</dt>
                    <dd><?php echo $detail[0]["boss_ref_no"] ?></dd>
                    <dt>Travel Date</dt>
                    <dd><?php echo date("d M Y h:i A",strtotime($detail[0]["dept_time"]))?></dd>
                </dl>

                <div  class="pull-right">
                    <div class="amenities amenities-icon" >
                      <i class="fa fa-eye circle <?php echo $historytask_class.' '.$active_class; ?>" data-attr="print" data-view="<?php echo $detail[0]['ticket_ref_no'];?>" data-toggle="tooltip" data-placement="top" title="View and print ticket"></i>
                      <i class="fa fa-envelope circle <?php echo $historytask_class.' '.$active_class;?>" data-attr="mail" data-view="<?php echo $detail[0]['ticket_ref_no'];?>" data-toggle="tooltip" data-placement="top" title="Send Email"></i>
                      <i class="fa fa-mobile circle <?php echo $historytask_class.' '.$active_class;?>" data-attr="sms" data-view="<?php echo $detail[0]['ticket_ref_no'];?>" data-toggle="tooltip" data-placement="top" title="Send mticket SMS"></i>
                      <?php //if($detail[0]['ticket_status'] == "Y"): ?>
                      <i class="fa fa-times circle <?php echo $cancelticket_class.' '.$active_class;?>" data-op-name="<?php echo $detail[0]['op_name'];?>" data-pnr-no="<?php echo $detail[0]['pnr_no'];?>" data-ticket-id="<?php echo $detail[0]['ticket_id'];?>" data-toggle="tooltip" data-placement="top" title="Cancel Ticket"></i>
                  <?php //endif; ?>
                    </div>
                    <div>
                        <button class="btn-mini status <?php echo THEME_BTN;?>">
                            <?php 
                                echo $button_name;    
                            ?>
                        </button> 
                    </div>
                </div>
	        	<div class="clearfix"></div>
	        </div>


            <?php 
            	if(count($detail) > 0)
				{
					$incount = 0;
			?>
            <div class="passenger_info clearfix">
            	<table class="table table-hover table-striped">
            		<thead>
            			<tr>
	            			<th>Name</th>
	            			<th>Age</th>
	            			<th>Gender</th>
	            			<th>Adult / Child</th>
	            			<th>Seat No</th>
                            <th>Fare</th>
	            		</tr>
            		</thead>
                    <tbody>
            		<?php

            		foreach($detail as $key => $tdetail) 
            		{
						$incount++;
						$adult_child = ($tdetail["psgr_age"] > 11) ? "Adult" : "Child";
                        $td_fare_amount = "NA";
                        if(trim($tdetail["convey_type"]) == "fix")
                        {
                            $td_fare_amount = $tdetail["fare_amt"] + $tdetail["convey_value"];
                        }
            		?>	            		
            			<tr>
	            			<td><?php echo $tdetail["psgr_name"]; ?></td>
	            			<td><?php echo $tdetail["psgr_age"]; ?></td>
	            			<td><?php echo $tdetail["psgr_sex"]; ?></td>
	            			<td><?php echo $adult_child; ?></td>
	            			<td><?php echo $tdetail["seat_no"]; ?></td>
                            <td><?php echo to_currency($td_fare_amount); ?></td>
	            		</tr>            		
        		<?php 
        			}
        		?>
                <tr class='theme-blue-color'>
                    <?php 
                        if(isset($detail[0]["discount_value"]) && $detail[0]["discount_value"] > 0)
                        {
                            echo "<td colspan='5'><b>Total Discount</b></td>";
                            echo "<td> Rs. ".$detail[0]["discount_value"]."</td>";
                        }
                    ?>
                </tr>
                </tbody>
                
                    <tfoot>
                        <tr>
                            <td colspan="6" class="text-left cancel-info">
                                <?php 
                                    if($detail[0]['ticket_status'] == "N"):
                                ?>
                                    <div class="info-box">
                                        <span class="close"></span>
                                        <div class="info-container">
                                            <ul class="arrow">
                                                <li>Refund amount  <?php echo to_currency($detail[0]['refund_amt']) ?> </li>
                                                <li>Cancellation cahrges  <?php echo to_currency($detail[0]['cancel_charge']) ?> </li>
                                            </ul>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </tfoot>
                
            	</table>


            </div>
            <?php 
        		}
            ?>
            
        </div>

	<?php 
		} 
    }
    else
    {
        echo '<div class="ticket_info">';
        echo 'No history found.';
        echo '</div>';
    }
	?>

        <!-- <div class="booking-info clearfix cancelled">
            <div class="date">
                <label class="month">NOV</label>
                <label class="date">30</label>
                <label class="day">SAT</label>
            </div>
            <h4 class="box-title"><i class="icon soap-icon-plane-right takeoff-effect circle"></i>England to Rome<small>you are flying</small></h4>
            <dl class="info">
                <dt>TRIP ID</dt>
                <dd>5754-8dk8-8ee</dd>
                <dt>booked on</dt>
                <dd>saturday, nov 30, 2013</dd>
            </dl>
            <button class="btn-mini status">CANCELLED</button>
        </div> -->
        
    </div>

</div>

	</div>
</div>





</section>
<!-- 
<script>
$(document).ready(function(){
	$('.collapse').click(function(e){
		e.preventDefault();
		var id = $(this).attr('href');
		$('.block_'+id).toggle(1000);
	});
});
</script> -->


<script type="text/javascript">
    $(document).ready(function(){

        $('.ticket_info').click(function(){
            $(this).parent().find('.passenger_info').slideToggle('medium');
        }).css({'cursor':'pointer'});

        //cancel ticket
        $(".ticket_info").off("click",".cancelticket").on("click",".cancelticket", function(e) {
            e.stopPropagation();

            var elem        = $(this);
            var wrapper     = $(this).parents('.booking-info');
            var aminities   = $(this).parents('.amenities-icon');

            var detail = {};
            var div = "";
            var ajax_url = 'cancel_tickets';
            var form = '';

            detail['ticket_id'] =  $(this).attr("data-ticket-id");
            detail['pnr_no'] = $(this).attr("data-pnr-no");
            detail['op_name'] = $(this).attr("data-op-name");
            detail['seat_no'] = "";

            var yesno = confirm("Are you sure you want to cancel this ticket?");

            if(yesno)
            {

                get_data(ajax_url, form, div, detail, function(response)
                {
                    if (response.flag == '@#success#@')
                    {
                        var info_block  = $.parseHTML($('#info-tpl').html());

                        var ul = $('<ul/>').addClass('arrow');
                        ul.append($('<li/>').html('Refund amount Rs.'+ response.refund_amt));
                        ul.append($('<li/>').html('Cancellation cahrges Rs.'+ response.cancellation_cahrge));

                        $(info_block).find('.info-container').append(ul);

                        wrapper.addClass('cancelled').find('.ticket_info .status').html('CANCELLED');

                        wrapper.find('.passenger_info .cancel-info').html($(info_block));
                        aminities.find("i").removeClass("historytask cancelticket active");
                        // elem.parent().removeClass('text-right').addClass('text-left').html($(info_block));
                    }
                    else
                    {
                        alert(response.error);
                    }
                    // if (response.flag == '@#success#@')
                    // {
                       
                    //     showNotification(response.msg_type,response.msg);
                    // }
                    // else if(response.flag == '@#error#@')
                    // {
                    //     showNotification(response.msg_type,response.msg);
                    // }
                    // else
                    // {
                    //     alert(response.msg);
                    // }
                }, '', false);
            }
        });
        //end of cancel ticket

        $(".ticket_info").off("click",".historytask").on("click",".historytask", function(e) {
            
            e.stopPropagation();
            var detail = {};
            var div = "";
            var ajax_url = 'front/booking/ticket_by_sms_email';
            var form = '';

            detail['ticket_mode'] =  $(this).attr("data-attr");
            detail['ticket_ref_no'] = $(this).attr("data-view");
            detail['is_history_task'] = 1;

            get_data(ajax_url, form, div, detail, function(response)
            {
                if (response.flag == '@#success#@')
                {
                    if(response.type == "print")
                    {
                        window.location.href = base_url+"front/booking/view_ticket/"+detail['ticket_ref_no'];
                    }
                    else
                    {
                        showNotification(response.msg_type,response.msg);
                    }
                }
                else if(response.flag == '@#error#@')
                {
                    showNotification(response.msg_type,response.msg);
                }
                else
                {
                    alert(response.msg);
                }
            }, '', false);
        });
        
    });
</script>



<script type="text/template" id="info-tpl">
    <div class="info-box">
        <span class="close"></span>
        <div class="info-container"></div>
    </div>
</script>
<!--  Template script for booking tab   -->