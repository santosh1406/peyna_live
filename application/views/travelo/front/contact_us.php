<script type="text/javascript">
(function($) {
	onloadCallback = function() {	
		greverify = grecaptcha.render('g_recaptcha', {
		'sitekey' : "<?php echo $this->config->item('google_captcha_site_key'); ?>"	
		});
	};	
})(jQuery);
</script>

<section id="content" class="abouttop">

<div class="termsimg"><img src="<?php echo base_url();?>assets/travelo/front/images/contact1.jpg" >
<h2>Contact Us  </h2>
</div>

    <div class="container">
        <div id="main">
           
            <div class="row">
            	<div class="col-sm-6 col-md-6 pb40 infowrap">
					
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="travelo-box contact-us-box">
<h2 class="info_heading">Information</h2>
								<h4 class="box-titles">Address</h4>
								<div class="contactus-address">
									<p>2nd Floor,
Universal Mill Building, <br>
Mehra Estate, L.B.S. Road,
 Vikhroli West, Mumbai, <br>
Maharashtra 400079</p>
								</div>
<h4 class="box-titles">Contact Details</h4>
								<ul class="contact-address">
									<li class="phone">
										<i class="soap-icon-phone circle"></i>
										<h5 class="title">Customer care</h5>
<p>Landline Number : 022-66813970<p>
										<p>Mobile : +91 7738343137</p>
									</li>
									<li class="email">
										<i class="soap-icon-message circle"></i>
										<h5 class="title">Email</h5>
										<p>support@rokad.in</p>
									</li>
								</ul>

							</div>

						</div>
						
					</div>
            	</div>
            	<div class="col-sm-6 col-md-6 conwrap">
            		<div class="travelo-box">
<h2 class="info_heading">Contact Form</h2>
                        <?php 
                            $attributes = array('method' => 'POST', 'class' => 'contact-form', 'id' => 'contact-form', 'name' => 'contact-form');
                            echo form_open(base_url()."front/page/contactus_message", $attributes); 
                        ?>
                            <h4 class="box-titles">Send us a message</h4>
                            <div class="row form-group">
                                <div class="col-xs-6">
                                    <label>Your Name</label>
                                    <input type="text" placeholder="Name" class="input-text full-width" name="uname" id="uname" autocomplete="off" >
                                </div>
                                <div class="col-xs-6">
                                    <label>Your Email</label>
                                    <input type="text" placeholder="Email" class="input-text full-width" name="uemail" id="uemail" autocomplete="off" >
                                </div>
                            </div>
							
							<div class="form-group">
                                <label>Your Message</label>
                                <textarea placeholder="Write your message" class="input-text full-width txtarea" rows="6" name="umessage" id="umessage" maxlength="300"></textarea>
                            </div>
							
							<div class="form-group col-md-12">
								<div class="g_recaptcha" id="g_recaptcha"></div>
								<input type="hidden" class="hiddenRecaptcha" name="hiddenRecaptcha" id="hiddenRecaptcha" data-rule-required="true" data-msg-required="Please verify captcha" autocomplete="off" required />
							</div>
							
                            <button class="btn-large full-width <?php echo THEME_BTN;?> contacus contactusMessage" type="submit">SEND MESSAGE</button>
                        </form>
                    </div>
            	</div>
            </div>
           <!-- <div class="row">
            	<div class="col-sm-12 col-md-12">
            		<div id='gmap_canvas'></div>
            	</div>-->
            </div>
            
        </div>
    </div>
</section>

<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"async defer></script>

<script type="text/javascript">
$(document).ready(function(){
    $('#contact-form').validate({
        ignore: "",
        focusInvalid: false,
        rules: {
            uname: {
                required: true,
				lettersonly: true
            },
            uemail: {
				required: true,
				email: true				
            },
			umessage: {
                required: true,
				maxlength: 300
            },
			hiddenRecaptcha: {
				required: function() {
					if(grecaptcha.getResponse(greverify) == '') {
						return true;
					} else {
						return false;
					}
				}
			}
        },
		messages:{
			uname: {
				required: "Please enter your name",
				lettersonly: "Please enter valid name"
			},
			uemail: {
				required: "Please enter email address",
				email:"Please enter valid email dddress"
			},
			umessage: {
                required: "Please enter message",
				maxlength: "Message cannot exceed 300 characters"
            },
			hiddenRecaptcha: {
				required: "Please verify captcha"			
			}
		}		
    });

	jQuery.validator.addMethod("lettersonly", function (value, element)
    {
        return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
    }, "");
	
}); // end of document ready
</script>