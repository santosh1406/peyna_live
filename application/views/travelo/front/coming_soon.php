<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title"><?php echo ucfirst($page) ?></h2>
        </div>
        <ul class="breadcrumbs pull-right">
            <li><a href="<?php echo base_url() ?>">HOME</a></li>
            <li class="active"><?echo $page?></li>
        </ul>
    </div>
</div>

<section id="content">
    <div class="container tour-detail-page">
        <div class="row">
            <div class="col-md-12" id="main">
                
                <div class="travelo-box" id="tour-details">
                    <h1 class="page-title">Coming soon!</h1>
                    
                </div>
            </div>
        </div>
    </div>
</section>
