<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Thank you</h2>
        </div>
    </div>
</div>

<section id="content">
    <div class="container tour-detail-page">
        <div class="row">
            <div class="col-md-12" id="main">

                <div class="travelo-box" id="tour-details">
                    <h1 class="page-title">Email Id verified successfully!</h1>
                    <h5>
                        <i class="icon soap-icon-check green mr10"></i>BOS team will now verify your KYC documents after which your account will be activated.  </h5><h5>
                        <i class="icon soap-icon-check green mr10"></i>Verification is normally done within 24 hrs.</h5><h5>
                        <i class="icon soap-icon-check green mr10"></i>You will receive an email once your account is activated.
                    </h5>

                </div>
            </div>
        </div>
    </div>
</section>
