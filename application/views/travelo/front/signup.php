<link href="<?php echo base_url() . COMMON_ASSETS ?>js/bootstrap/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() . COMMON_ASSETS ?>js/bootstrap/bootstrap-select.js" type="text/javascript"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

<style type="text/css">
    /* Popup container - can be anything you want */
    .popup {
        position: relative;
        display: inline-block;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* The actual popup */
    .popup .popuptext {
        /*visibility: hidden;*/
        width: 200px;
        background-color: #555;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 8px 0;
        position: absolute;
        z-index: 1;
        bottom: -80%;
        left: 80%;
        margin-left: 236px;
        height: 200;
    }

    /* Popup arrow */
    .popup .popuptext::before {
        content: "";
        position: absolute;
        top: 100%;
        left: 50%;
        margin-left: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: #555 transparent transparent transparent;
    }

    /* Toggle this class - hide and show the popup */
    .popup .show {
        /*visibility: visible;*/
        -webkit-animation: fadeIn 1s;
        animation: fadeIn 1s;
    }

    /* Add animation (fade in the popup) */
    @-webkit-keyframes fadeIn {
        from {opacity: 0;} 
        to {opacity: 1;}
    }

    @keyframes fadeIn {
        from {opacity: 0;}
        to {opacity:1 ;}
    }
</style>
<!-- Inner Wrapper Start -->
<div id="page-wrapper">

    <!-- Signin Content Start -->
    <section class="inner-wrapper" id="captcha">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <h3 class="text-center purple">Register</h3>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-12 col-sm-12 container-c">
                                <?php
                                if ($this->session->flashdata('msg_type') == 'success') {
                                    echo $this->session->flashdata('msg');
                                }
                                ?>
                                <form class="form-horizontal captcha-from" name="signup_form" id="signup_form" method="post" enctype="multipart/form-data" accept-charset="utf-8" action="<?php echo base_url() . 'admin/login/register_authenticate'; ?>">
                                    <fieldset>
                                        <legend>Login Information</legend>
                                        <div class="form-group">
                                            <div class="cols-sm-12">
                                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                                <label for="" class="control-label col-md-3 label-text">Email<span class="asteriskField">*</span> </label>
                                                <div class="controls col-md-6">
                                                    <input type="text" class="form-control" name="email" id="email"  placeholder="Email" value="<?php echo set_value('email'); ?>" autocomplete="off" maxlength="100"/>
                                                    <?php echo form_error('email'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix" style="height: 10px;clear: both;"></div>



                                        <div class="form-group">
                                            <div class="cols-sm-12">
                                                <label for="" class="control-label col-md-3 label-text">Mobile No<span class="asteriskField">*</span> </label>
                                                <div class="controls col-md-6">
                                                    <input name="mobile_no" type="text" minlength = "10" maxlength="10" id="mobile_no" value="<?php echo set_value('mobile_no'); ?>" class="form-control" placeholder="Mobile No" autocomplete="off"/>
                                                    <?php echo form_error('mobile_no'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <fieldset>
                                        <legend>General</legend>
                                       

                                        <div class="form-group">
                                            <div class="cols-sm-12">
                                                <label for="" class="control-label col-md-3 label-text">First Name<span class="asteriskField">*</span> </label>
                                                <div class="controls col-md-6">
                                                    <input name="first_name" type="text" id="first_name" maxlength="20" value="<?php echo set_value('first_name'); ?>" class="form-control"  placeholder="First Name"/>
                                                    <?php echo form_error('first_name'); ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="cols-sm-12">
                                                <label for="" class="control-label col-md-3 label-text">Last Name<span class="asteriskField">*</span> </label>
                                                <div class="controls col-md-6">
                                                    <input name="last_name" type="text" id="last_name" maxlength="20" value="<?php echo set_value('last_name'); ?>" class="form-control"  placeholder="Last Name"/>
                                                    <?php echo form_error('last_name'); ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="cols-sm-12">
                                                <label for="" class="control-label col-md-3 label-text">Gender</label>
                                                <div class="controls col-md-6">
                                                    <select class="form-control" id="gender"  name="gender"/>
                                                    <option value="" selected>Select Gender</option>
                                                    <option value="M">Male</option>
                                                    <option value="F">Female</option>
                                                    <option value="T">Transgender</option>
                                                    </select>
                                                    <span id="prov_type_msg" class="err"></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="cols-sm-12">
                                                <label for="" class="control-label col-md-3 label-text">Aadhaar Card<span class="asteriskField">*</span> </label>
                                                <div class="controls col-md-6">
                                                    <input type="text" name="adharcard" id="adharcard" value="<?php echo set_value('adharcard'); ?>" class="form-control" placeholder="Aadhaar Card" maxlength="12" autocomplete="off"/>
                                                    <?php echo form_error('adharcard'); ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="cols-sm-12">
                                                <label for="" class="control-label col-md-3 label-text">PAN / TAN Card<span class="asteriskField">*</span> </label>
                                                <div class="controls col-md-6">
                                                    <input type="text" name="pancard" id="pancard" value="<?php echo set_value('pancard'); ?>" class="form-control"  placeholder="PAN / TAN Card" maxlength="10" autocomplete="off" style="text-transform:uppercase"/>
                                                    <?php echo form_error('pancard'); ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="cols-sm-12">
                                                <label for="" class="control-label col-md-3 label-text">DOB (DD-MM-YYYY)<span class="asteriskField">*</span> </label>
                                                <div class="controls col-md-6">
                                                    <input name="dob" type="text" id="dob" value="<?php echo set_value('dob'); ?>" class="form-control" placeholder="DD-MM-YYYY"/>
                                                    <?php echo form_error('dob'); ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="cols-sm-12">
                                                <label for="" class="control-label col-md-3 label-text">Current Business (In Shop)<span class="asteriskField">*</span></label>
                                                <div class="controls col-md-6">
                                                    <input name="current_business" type="text" id="current_business" value="<?php echo set_value('current_business'); ?>" class="form-control" placeholder="Current Business (In Shop)"/>
                                                    <?php echo form_error('current_business'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <!--fieldset>
                                        <legend>Contact Information</legend>
                                        <!-- <div class="form-group">
                                            <div class="cols-sm-12">
                                                <label for="" class="control-label col-md-3 label-text">Address1<span class="asteriskField">*</span> </label>
                                                <div class="controls col-md-6">
                                                    <input name="address1" type="text" id="address1" maxlength="200" value="<?php echo set_value('address1'); ?>" class="form-control" placeholder="Address1"/>
                                        <?php echo form_error('address1'); ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="cols-sm-12">
                                                <label for="" class="control-label col-md-3 label-text">Address2</label>
                                                <div class="controls col-md-6">
                                                    <input name="address2" type="text" id="address2" maxlength="200" value="<?php echo set_value('address2'); ?>" class="form-control" placeholder="Address2" />
                                        <?php echo form_error('address2'); ?>
                                                </div>
                                            </div>
                                        </div> -->

                                        <!--div class="form-group">
                                            <div class="cols-sm-12">
                                                <label for="" class="control-label col-md-3 label-text">Address Residence <span class="asteriskField">*</span> </label>
                                                <div class="controls col-md-6">
                                                    <input name="address1" type="text" id="address1" maxlength="200" value="<?php echo set_value('address1'); ?>" class="form-control" placeholder="Address Resident"/>
                                                    <?php echo form_error('address1'); ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="cols-sm-12">
                                                <label for="" class="control-label col-md-3 label-text">State<span class="asteriskField">*</span> </label>
                                                <div class="controls col-md-6">
                                                    <select class="state form-control" name="agent_state" id="agent_state"/>
                                                    <option value="">Select State</option>
                                                    <?php
                                                    foreach ($states as $row => $value) {
                                                        echo "<option value='" . $value['intStateId'] . "'>" . $value['stState'] . "</option>";
                                                    }
                                                    ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="cols-sm-12">
                                                <label for="" class="control-label col-md-3 label-text">City<span class="asteriskField">*</span> </label>
                                                <div class="controls col-md-6">
                                                    <select class="agent_city form-control" name="agent_city" id="agent_city"/>
                                                    <option value="">Select City</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <div class="cols-sm-12">
                                                <label for="" class="control-label col-md-3 label-text">Pin Code<span class="asteriskField">*</span> </label>
                                                <div class="controls col-md-6">
                                                    <input name="pincode" type="text" id="pincode" value="<?php echo set_value('pincode'); ?>" minlength = "6" maxlength="6" class="form-control" placeholder="Pin Code"/>
                                                    <?php echo form_error('pincode'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <!--<div class="form-group" style="margin-top: 25px;"> -->
                                        <!-- div class="form-group">
                                            <div class="cols-sm-12">
                                                <label for="" class="control-label col-md-3 label-text">Company GST No<span class="asteriskField"></span> </label>
                                                <div class="controls col-md-6">
                                                    <input id="company_gst_no" name="company_gst_no" type="text" minlength = "15" maxlength="15" value="<?php echo set_value('company_gst_no'); ?>" class="form-control" placeholder="Company GST No" autocomplete="off"/>
                                                    <?php echo form_error('company_gst_no'); ?>
                                                </div>
                                            </div>
                                        </div -->
                                        <fieldset>
                                            <legend>Shop Information</legend>
                                             <div class="form-group">
                                            <div class="cols-sm-12">
                                                <label for="" class="control-label col-md-3 label-text">Name of Firm </label>
                                                <div class="controls col-md-6">
                                                    <input name="firm_name" type="text" id="firm_name" maxlength="50" value="<?php echo set_value('firm_name'); ?>" class="form-control" placeholder="Name of Firm/Company" autocomplete="off"/>
                                                    <?php echo form_error('firm_name'); ?>
                                                </div>
                                            </div>
                                        </div>
                                            <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">State<span class="asteriskField">*</span> </label>
                                                    <div class="controls col-md-6">
                                                        <select class="state1 form-control" name="state1" id="agent_state1"/>
                                                        <option value="">Select State</option>
                                                        <?php
                                                        foreach ($states as $row => $value) {
                                                            echo "<option value='" . $value['intStateId'] . "'>" . $value['stState'] . "</option>";
                                                        }
                                                        ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">City<span class="asteriskField">*</span> </label>
                                                    <div class="controls col-md-6">
                                                        <select class="agent_city1 form-control" name="city1" id="agent_city1"/>
                                                        <option value="">Select City</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-top: 00px;">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">Address Shop <span class="asteriskField">*</span> </label>
                                                    <div class="controls col-md-6">
                                                        <input name="address2" type="text" id="address2" maxlength="200" value="<?php echo set_value('address2'); ?>" class="form-control" placeholder="Address Shop"/>
                                                        <?php echo form_error('address2'); ?>
                                                    </div>
                                                </div>
                                            </div>


                                            <!-- div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">Depot Name<span  id="dp_nm" class="asteriskField">*</span> </label>
                                                    <div class="controls col-md-6">
                                                        <select class="agent_city1 form-control"  name="depot_name" id="depot_name"/>
                                                        <option value="">Select Depot</option>
                                                        </select>
                                                    </div><input type="hidden" name="depot_cd" id="depot_cd">
                                                    <input type="hidden" name="division_cd" id="division_cd">
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">Stand/Bus Station Name <span id="dp_nm1" class="asteriskField">*</span> </label>
                                                    <div class="controls col-md-6">
                                                        <input name="stand_name" type="text" id="stand_name" maxlength="200" value="<?php echo set_value('stand_name'); ?>" class="form-control" placeholder="Stand/Bus Station Name"/>
                                                        <?php echo form_error('stand_name'); ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">Shop Location <span id="dp_nm2" class="asteriskField">*</span></label>
                                                    <div class="controls col-md-6">
                                                        <select class="form-control" id="shop_location"  name="shop_location"/ >
                                                                <option value="" selected>Select Shop Location</option>
                                                            <option value="1">Inside Depot/Stand</option>
                                                            <option value="2">Within 200 Metres Outside Depot/Stand</option>
                                                            <option value="3">More than 200 Metres Outside Depot/Stand</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div -->


                                            <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">Pin Code <span class="asteriskField">*</span> </label>
                                                    <div class="controls col-md-6">
                                                        <input name="pincode1" type="text" id="pincode1" value="<?php echo set_value('pincode1'); ?>" minlength = "6" maxlength="6" class="form-control" placeholder="Pin Code"/>
                                                        <?php echo form_error('pincode1'); ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">Register for Products <span class="asteriskField">*</span> </label>
                                                    <div class="col-lg-6">  
                                                        <select name="services[]" class="form-control select2" id="services" title="" multiple="" >
                                                            <!-- <option value ="">Select Services</option> -->
                                                            <?php foreach ($serviceDetails as $service) { ?>
                                                                <option data-id="<?php echo $service->rsm_id; ?>" value="<?php echo $service->id; ?>"><?php echo $service->service_name; ?></option>
                                                            <?php } ?>
                                                            <!-- <option value ="1">MSRTC Smart Card</option>
                                                             <option value ="2">Mobile and DTH Recharge</option>
                                                             <option value ="3">Bill payments</option>
                                                             <option value ="4">Money Transfer</option>
                                                             <option value ="5">Micro ATM/AEPS</option>
                                                             <option value ="6">Advance Booking (Bus, Hotel, Air)</option> -->
                                                        </select>
                                                        <input type="hidden" name="service_id" id="service_id">
                                                    </div>
                                                </div>

                                        </fieldset>

                                        <!--fieldset>
                                            <legend>Bank Account Detail</legend>

                                            <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">Account No<span class="asteriskField">*</span> </label>
                                                    <div class="controls col-md-6">
                                                        <input type="text" name="accno" id="accno" maxlength="30" value="<?php echo set_value('accno'); ?>" autocomplete="off" class="form-control" placeholder="Account No"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">Bank Name<span class="asteriskField">*</span> </label>
                                                    <div class="controls col-md-6">
                                                        <input type="text" name="bname" id="bname" maxlength="50" class="form-control"  placeholder="Bank Name" autocomplete="off" value="<?php echo set_value('bname'); ?>"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">Account Holder Name<span class="asteriskField">*</span> </label>
                                                    <div class="controls col-md-6">
                                                        <input type="text" name="accname" value="" maxlength="50" id="accname" class="form-control" placeholder="Account Holder Name" value="<?php echo set_value('accname'); ?>"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">Branch Name<span class="asteriskField">*</span> </label>
                                                    <div class="controls col-md-6">
                                                        <input type="text" name="brname" id="brname" maxlength="30" class="form-control" placeholder="Branch Name" value="<?php echo set_value('brname'); ?>" autocomplete="off"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">IFSC Code<span class="asteriskField">*</span> </label>
                                                    <div class="controls col-md-6">
                                                        <input type="text" name="ifsc" maxlength = 11 value="" id="ifsc" class="form-control" placeholder="IFSC Code" value="<?php echo set_value('ifsc'); ?>" style="text-transform:uppercase"/>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">Payment Mode<span class="asteriskField">*</span> </label>
                                                    <div class="controls col-md-6">
                                                    <select  class="agent_city1 form-control"  name="payment_mode" id="payment_mode">
                                                             <option value ="Cheque">Cheque</option>
                                                             <option value ="Transfer">Transfer</option>
                                                             </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="cheque_div">
                                             <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">Cheque No <span class="asteriskField">*</span> </label>
                                                    <div class="controls col-md-6">
                                                        <input type="text" name="cheque_no" maxlength = 11 value="" id="cheque_no" class="form-control" placeholder="Cheque No" value="<?php echo set_value('cheque_no'); ?>" style="text-transform:uppercase"/>
                                                    </div>
                                                </div>
                                            </div>

                                             <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">Cheque Date<span class="asteriskField">*</span> </label>
                                                    <div class="controls col-md-6">
                                                        <input type="text" name="cheque_date" placeholder="DD-MM-YYYY"  id="cheque_date" class="form-control" placeholder="Cheque Date" value="<?php echo set_value('cheque_date'); ?>" style="text-transform:uppercase"/>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">Branch<span class="asteriskField">*</span> </label>
                                                    <div class="controls col-md-6">
                                                        <input type="text" name="branch" maxlength = 11 value="" id="branch" class="form-control" placeholder="branch" value="<?php echo set_value('branch'); ?>" style="text-transform:uppercase"/>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">Bank Name<span class="asteriskField">*</span> </label>
                                                    <div class="controls col-md-6">
                                                        <input type="text" name="bank_name" value="" id="bank_name" maxlength="50" class="form-control" placeholder="Bank Name" value="<?php echo set_value('bank_name'); ?>" style="text-transform:uppercase"/>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">Amount<span class="asteriskField">*</span> </label>
                                                    <div class="controls col-md-6">
                                                        <input type="text" name="amount" maxlength = 11 value="" id="amount" class="form-control" placeholder="amount" value="<?php echo set_value('amount'); ?>" style="text-transform:uppercase"/>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            <div id="transfer_div">
                                            <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">UTR No/Neft No/Transfer No<span class="asteriskField">*</span> </label>
                                                    <div class="controls col-md-6">
                                                        <input type="text" name="utr_no" value="" id="utr_no" class="form-control" placeholder="UTR Name" value="<?php echo set_value('utr_no'); ?>" style="text-transform:uppercase"/>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">Date<span class="asteriskField">*</span> </label>
                                                    <div class="controls col-md-6">
                                                        <input type="text" name="transfer_date" id="transfer_date"  placeholder="DD-MM-YYYY" class="form-control" placeholder="Date" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
<!-- <center><button class="btn btn-primary" onclick="myFunction()" href="" type="button">Click here for Bank Details</button></center>
<div class="form-group"> 
    <label class="col-lg-6 control-label" for="popup"></label>
    <div class="col-lg-3">
        <div class="popup">
                                            <!--  <b><h4>Click Here</h4></b> -->
                                            <!-- <center><span class="popuptext" id="myPopup">Bank Details 
                                                    <ul>
                                                        <li style="align:left;margin-left:-58px;">Bank Name : ICICI BANK</li>  
                                                        <li style="align:left;margin-left:-44px;">Account No : 104505002070</li>
                                                        <li style="align:left;margin-left:-56px;">IFSC Code : ICIC0001045</li>
                                                        <li style="align:left;margin-left:-28px;">Branch Name : Vikhroli Branch</li>
                                                    </ul>
                                                </span></center>
                                        </div>
                                    </div>
                                </div> -->
                                        <!--/fieldset -->

                                        <fieldset>
                                            <legend>Upload Document</legend>
                                            <!--<div class="form-group">
                                            
                                <input type="hidden" name="agid" id="agid" value="<?php echo $id; ?>" >
                                    <div class="table-responsive"   id="idproof">
    
                                        <table class="table table-bordered" id="idtable">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Document Type</th>
                                                    <th>Document Name</th>
                                                    <th>Select File</th>
                                                    <th>Add</th>
                                                    <th>Delete</th>
                                                </tr>  
                                            </thead>
    
                                            <tr>
    
                                                <td>1.</td>
                                                <td >
                                                    <select name="doccat[]" id="doccat" class="doccat form-control"     data-validation="required"  data-validation-error-msg-required="Please select Document category"><option value="">Select document category</option>
                                            <?php foreach ($res as $key => $val) : ?>
                                                                        <option value="<?php echo $val['doc_cat_id'] ?>"><?php echo $val['category_name'] ?></option>
                                            <?php endforeach; ?>
    
    
                                                    </select>
                                                </td>    
                                                <td >
                                                    <select name="docnm[]" class="form-control docnm" data-validation="required"        data-validation-error-msg-required="Please select Document Name"><option value="">Select document name</option>
    
                                                    </select>
                                                    <input type="hidden" name="dropdowntext[]" class="drotext">   
                                                </td>     
                                                <td>
                                                    <input type="file"  name="img_doc[]"
                                                           data-validation="length mime size"
                                                           data-validation-length="min1"
                                                           data-validation-allowing="jpg, png, gif"
                                                           data-validation-max-size="5000kb"
                                                           data-validation-error-msg-size="You can not upload images larger than 5000kb"
                                                           data-validation-error-msg-mime="You can only upload images"
                                                           data-validation-error-msg-length="You have to upload  image"
                                                           />
    
    
                                                </td> 
                                                <td><input type="button" class="form-control  ins_row btn btn-primary" value="Add New"></td> 
                                                <td><input type="button" class="form-control  btn btn-primary del" value="Delete"></td> 
                                                <td></td>
    
                                            </tr>
    
    
                                        </table>
    
                                    </div>
                    </div>
                </fieldset>-->                    

                                            <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">Proof Of Identification<span class="asteriskField">*</span> <br>(Self Attested)</label>
                                                    <div class="controls col-md-6">
                                                        <input type="hidden" name="doccat[]" id="doccat" value="1">
                                                        <select name="docname1" id="docnm1" class="form-control docnm" data-validation="required" data-validation-error-msg-required="Please select Document Name"><option value="">Select document name</option>
                                                            <?php foreach ($id_proof_names as $key => $val) : ?>
                                                                <option value="<?php echo $val['agent_docs_list'] ?>"><?php echo $val['docs_name'] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <input type="file" name="img_doc1"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">Proof Of Address<span class="asteriskField">*</span><br>(Self Attested) </label>
                                                    <div class="controls col-md-6">
                                                        <input type="hidden" name="doccat[]" id="doccat" value="2">
                                                        <select name="docname2" id="docnm2" class="form-control docnm" data-validation="required" data-validation-error-msg-required="Please select Document Name"><option value="">Select document name</option>
                                                            <?php foreach ($address_proof_names as $key => $val) : ?>
                                                                <option value="<?php echo $val['agent_docs_list'] ?>"><?php echo $val['docs_name'] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <input type="file"  name="img_doc2"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label for="" class="control-label col-md-3 label-text">Photo<span class="asteriskField">*</span><br>(Self Attested)</label>
                                                    <div class="controls col-md-6">
                                                        <input type="hidden" name="doccat[]" id="doccat" value="3">
                                                        <select name="docname3" id="docnm3" class="form-control docnm" data-validation="required" data-validation-error-msg-required="Please select Document Name"><option value="">Select document name</option>
                                                            <?php foreach ($photo as $key => $val) : ?>
                                                                <option value="<?php echo $val['agent_docs_list'] ?>"><?php echo $val['docs_name'] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <input type="file"  name="img_doc3"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>

                                        <!--fieldset>
                                            <legend>Reference By</legend>
                                            <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label class="control-label col-md-3 label-text" for="">FME/DH Name</label>
                                                    <div class="controls col-md-6">
                                                        <input name="emp_name" type="text" id="emp_name" maxlength="50" value="" class="form-control"  placeholder="Employee Name"/>
                                                        <?php echo form_error('emp_name'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label class="control-label col-md-3 label-text" for="">Mobile Number</label>
                                                    <div class="controls col-md-6">
                                                        <input name="emp_mob" type="text" id="emp_mob" minlength = "10" maxlength="10" value="" class="form-control"  placeholder="Employee Mobile Number"/>
                                                        <?php echo form_error('emp_mob'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label class="control-label col-md-3 label-text" for="">Designation</label>
                                                    <div class="controls col-md-6">
                                                        <input name="ref_emp_designation" type="text" id="ref_emp_designation" maxlength="50" value="" class="form-control"  placeholder="Employee Designation"/>
                                                        <?php echo form_error('ref_emp_designation'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset -->

                                        <fieldset>
                                            <legend></legend>
                                            <div class="form-group">
                                                <div class="cols-sm-12">
                                                    <label class="control-label col-md-3 label-text" for=""></label>
                                                    <div class="controls col-md-6">
                                                        <label for="" class="control-label label-text">
                                                            <input type="checkbox" id="terms"  name="terms">
                                                            <?php // echo base_url() . "terms_and_condition"; ?>
                                                            I agree <a href="#">Terms &amp; Conditions</a><span class="asteriskField"> *</span> </label>
                                                        <!--<label id="terms"  name="terms"></label-->
                                                    </div>
                                                </div>

                                            </div>
                                            <!-- div class="form-group"> 
                                                <label class="col-lg-6 control-label" for="popup"></label>
                                                <div class="col-lg-3">
                                                    <div class="popup">
                                                        <!--  <b><h4>Click Here</h4></b> -->
                                                        <!-- center><span class="popuptext" id="myPopup">Bank Details 
                                                                <ul>
                                                                    <li style="align:left;margin-left:-46px;">Bank Name : State Bank Of India</li>  
                                                                    <li style="align:left;margin-left:-44px;">Account No : 38219186281</li>
                                                                    <li style="align:left;margin-left:-56px;">IFSC Code : SBIN0011777</li>
                                                                    <li style="align:left;margin-left:-28px;">Branch Name : Capital Market Branch</li>
                                                                </ul>
                                                            </span></center>
                                                    </div>
                                                </div>
                                            </div -->
                                        </fieldset>

                                        <div class="form-group">
                                            <div class="col-sm-4 col-lg-offset-4 col-sm-offset-4 col-xs-offset-0"><button id="submit_btn" name="submit_btn" type="submit" class="btn btn-purple btn-lg btn-block login-button txt-w">Sign Up</button></div>
                                        </div>
                                </form> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Signin Content End -->             
</div>
<!-- Inner Wrapper End -->

<!-- Bootstrap Core JavaScript -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/travelo/front/js/jquery.qrcode.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/travelo/front/js/qrcode.js"></script>
<script>
// When the user clicks on div, open the popup
    /*function myFunction() {
     var popup = document.getElementById("myPopup");
     popup.classList.toggle("show");
     }*/
</script>
<script>
    function openNav() {
        if ($(window).width() < 981) {
            document.getElementById("mySidenav").style.width = "100%";
        } else {
            document.getElementById("mySidenav").style.width = "15%";
        }


        var width = $("#mySidenav").width();
        if (width !== 0) {
            document.getElementById("mySidenav").style.width = "0";
        }

    }

    function closeNav() {

        document.getElementById("mySidenav").style.width = "0";
    }
</script>
<script>
    $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
</script>
<script>
    //jQuery('#qrcode').qrcode("this plugin is great");
    jQuery('#qrcodeCanvas').qrcode({
        text: "http://jetienne.com"
    });
</script>
<!--<script src="<?php echo base_url(); ?>assets/travelo/front/js/captcha.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    $(document).ready(function() {
        $('.select2').select2({
            placeholder: "Please select products"
        });
        $("#transfer_div").hide();

        $("#signup_form").validate({
            //ignore: "none:not(select)",
            ignore: 'input[type=hidden]',
            rules: {
                firm_name: {
                    // required: true,
                    letterswithspecialchar: true
                },
                first_name: {
                    required: true,
                    lettersonly: true
                },
                last_name: {
                    required: true,
                    lettersonly: true
                },
                email: {
                    required: true,
                    email: true,
                    email_exists: true
                },
                company_gst_no: {
                    valid_gst_no: true
                },
                dob: {
                    required: true,
                },
                adharcard: {
                    required: true,
                    digits: true,
                    minlength: 12,
                    maxlength: 12,
                    valid_adharcard_no: true
                },
                pancard: {
                    required: true,
                    minlength: 10,
                    maxlength: 10,
                    valid_pancard_no: true
                },
                mobile_no: {
                    required: true,
                    minlength: 10,
                    maxlength: 10,
                    number: true,
                    valid_mobile_no: true,
                    mobileno_exists: true,
                    mobileno_start_with: true
                },
                address1: {
                    required: true,
                },
                address2: {
                    required: true,
                },
                agent_state: {
                    required: true,
                },
                state1: {
                    required: true,
                },
                agent_city: {
                    required: true,
                },
                city1: {
                    required: true,
                },
                depot_name: {
                    required: true,
                },
                stand_name: {
                    required: true,
                },
                shop_location: {
                    required: true,
                },
                pincode1: {
                    required: true,
                    number: true,
                    minlength: 6,
                    maxlength: 6,
                    pincode_start_with: true,
                },
                pincode: {
                    required: true,
                    number: true,
                    minlength: 6,
                    maxlength: 6,
                    pincode_start_with: true,
                },
                accno: {
                    required: true,
                    digits: true,
                    valid_account_no: true,
                },
                accname: {
                    required: true,
                    lettersonly: true,
                },
                brname: {
                    required: true,
                    lettersonly: true,
                },                
                ifsc: {
                    required: true,
                    lfsccode: true
                },             
                bname: {
                    required: true,
                    lettersonly: true,
                },
                "service_name[]": {
                    required: true,
                },
                "docname1": {
                    required: true,
                },
                "img_doc1": {
                    required: true,
                    extension: "image/jpg,image/jpeg,image/png",
                    filesize: 500000
                },
                "docname2": {
                    required: true,
                },
                "img_doc2": {
                    required: true,
                    extension: "image/jpg,image/jpeg,image/png",
                    filesize: 500000
                },
                "docname3": {
                    required: true,
                },
                "img_doc3": {
                    required: true,
                    extension: "image/jpg,image/jpeg,image/png",
                    filesize: 500000
                },
                emp_name: {
                    lettersonly: true
                },
                emp_mob: {
                    minlength: 10,
                    maxlength: 10,
                    number: true,
                    valid_mobile_no: true,
                    mobileno_start_with: true
                },
                terms: {
                    required: true,
                },
                current_business: {
                    required: true,
                },
                "services[]": {
                    required: true,
                },
                //added by sonali on 7-feb-2019
                cheque_no :{                
                    required: function () {                        
                        return $('#payment_mode').val() == 'Cheque';
                    }
            }, 
                cheque_date:{
                    required: function () {                        
                        return $('#payment_mode').val() == 'Cheque';
                    }
                },
                transfer_date:{
                    required: function () {                        
                        return $('#payment_mode').val() == 'Transfer';
                    }
                },
                utr_no:{
                    required: function () {                        
                        return $('#payment_mode').val() == 'Transfer';
                    }
                },
                branch:{
                    required: function () {                        
                        return $('#payment_mode').val() == 'Cheque';
                    },
                    lettersonly: function () {                        
                        return $('#payment_mode').val() == 'Cheque';
                    }
                },
                bank_name:{
                    required: function () {                        
                        return $('#payment_mode').val() == 'Cheque';
                    },
                    lettersonly: function () {                        
                        return $('#payment_mode').val() == 'Cheque';
                    }
                },
                amount:{
                    required: function () {                        
                        return $('#payment_mode').val() == 'Cheque';
                    },
                    digits: function () {                        
                        return $('#payment_mode').val() == 'Cheque';
                    }
                }
            },
            errorPlacement: function(error, element) {

                if (element.attr("name") === "agent_state")
                {
                    error.appendTo(element.parent("div"));
                }
                else if (element.attr("name") === "agent_city")
                {
                    error.appendTo(element.parent("div"));
                }
                else if (element.attr("name") === "terms") {
                    error.insertAfter(element.parent("label"));
                }
                else if (element.attr("name") === "services[]") {
                    error.insertAfter(element.siblings("span"));
                } else {
                    error.insertAfter(element);
                }

            },
            messages:
                    {
                        firm_name: {
                            required: "Please Enter Firm/Company Name",
                            letterswithspecialchar: "Please Enter Valid Firm/Company Name",
                        },
                        first_name: {
                            required: "Please Enter First Name",
                            lettersonly: "Please Enter Valid First Name",
                        },
                        last_name: {
                            required: "Please Enter Last Name",
                            lettersonly: "Please Enter Valid Last Name",
                        },
                        email: {
                            required: "Please Enter Email Address",
                            email_exists: "Email Id Already Registered With Us"
                        },
                        company_gst_no: {
                            valid_gst_no: "Please Enter Valid Company GST No"
                        },
                        adharcard: {
                            required: "Please Enter Aadhaar Card Number",
                            digits: "Please Enter 12 Digit Aadhaar Card Number",
                            minlength: "Please Enter 12 Digit Aadhaar Card Number",
                            maxlength: "Please Enter 12 Digit Aadhaar Card Number",
                            valid_adharcard_no: "Please Enter Valid Aadhaar Card Number."
                        },
                        pancard: {
                            required: "Please Enter PAN/TAN Card Number",
                            minlength: "Please Enter 10 digit pan Card Number",
                            maxlength: "Please Enter 10 digit pan Card Number",
                            valid_pancard_no: "Please Enter Valid PAN/TAN Card Number."
                        },
                        dob: {
                            required: "Please Select The DOB (DD-MM-YYYY)",
                        },
                        mobile_no: {
                            required: "Please Enter Mobile No",
                            number: "Please Enter Valid Mobile No",
                            minlength: "Please Enter 10 Digit Mobile No",
                            maxlength: "Please Enter 10 Digit Mobile No",
                            valid_mobile_no: "Please Enter Valid Mobile No",
                            mobileno_exists: 'Mobile No Already Registered with us',
                            mobileno_start_with: 'Mobile No should start with 7 or 8 or 9'
                        },
                        address1: {
                            required: "Please Enter Address Resident",
                        },
                        address2: {
                            required: "Please Enter Address Shop",
                        },
                        agent_state: {
                            required: "Please Select State Name",
                        },
                        agent_city: {
                            required: "Please Select City Name",
                        },
                        pincode: {
                            required: "Please Enter The Pin Code",
                            number: "Please Enter The Valid Pin Code",
                            minlength: "Please Enter 6 Digit Pin Code",
                            maxlength: "Please Enter 6 Digit Pin Code",
                            pincode_start_with: "Pin Code Should Start Between 1 and 9"
                        },
                        "services[]": {
                            required: "Please select at least one product",
                        },
                        accname: {
                            required: "Please Enter The Account Holder Name",
                            lettersonly: "Please Enter Valid Account Holder Name"
                        },
                        accno: {
                            required: "Please Enter The Account Number",
                            valid_account_no: "Please Enter Number Between 11 to 16 Digits"
                        },
                        brname: {
                            required: "Please Enter The Branch Name",
                            lettersonly: "Please Enter Valid Branch Name"
                        },
                        bname: {
                            required: "Please Enter Bank Name",
                            lettersonly: "Please Enter Valid Bank Name"
                        },
                        ifsc: {
                            lfsccode: "Please Enter Valid IFSC CODE ",
                        },
                        "docname1": {
                            required: "Please select document name",
                        },
                        "img_doc1": {
                            required: "Please upload document",
                            extension: "Please upload images only",
                            filesize: "You can not upload images larger than 500kb"
                        },
                        "docname2": {
                            required: "Please select document name",
                        },
                        "img_doc2": {
                            required: "Please upload document",
                            extension: "Please upload images only",
                            filesize: "You can not upload images larger than 500kb"
                        },
                        "docname3": {
                            required: "Please select document name",
                        },
                        "img_doc3": {
                            required: "Please upload document",
                            extension: "Please upload images only",
                            filesize: "You can not upload images larger than 500kb"
                        },
                        emp_name: {
                            lettersonly: "Please Enter Valid Employee Name"
                        },
                        emp_mob: {
                            number: "Please Enter Valid Mobile No",
                            minlength: "Please Enter 10 Digit Mobile No",
                            maxlength: "Please Enter 10 Digit Mobile No",
                            valid_mobile_no: "Please Enter Valid Mobile No",
                            mobileno_start_with: 'Mobile No should start with 7 or 8 or 9'
                        },
                        terms: {
                            required: "Please read and accept terms and condition"
                        },
                        depot_name: {
                            required: "Please enter depot name"
                        },
                        stand_name: {
                            required: "Please enter stand name"
                        },
                        agent_state1: {
                            required: "Please select a state"
                        },
                        agent_city1: {
                            required: "Please select a city"
                        },
                        //added by sonali on 7-feb-2019
                        amount:{
                            required: "Please Enter Amount",
                            digits: "Please enter only digits."
                        },
                        branch:{
                            required: "Please Enter The Branch Name",
                            lettersonly: "Please Enter Valid Branch Name"
                        },
                        bank_name:{
                            required: "Please Enter Bank Name",
                            lettersonly: "Please Enter Valid Bank Name"
                        },
                        cheque_no:{
                            required: "Please Enter Cheque No"
                        }
                    }

        });

        jQuery.validator.addMethod('filesize', function(value, element, param)
        {
            return this.optional(element) || (element.files[0].size <= param)
        }, "");

        jQuery.validator.addMethod('extension', function(value, element, param)
        {
            var tv = param.split(',');
            return this.optional(element) || (element.files[0].type == tv[0] || element.files[0].type == tv[1] || element.files[0].type == tv[2])
        }, "");

        jQuery.validator.addMethod("lettersonly", function(value, element)
        {
            return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
        }, "");

        jQuery.validator.addMethod("letterswithdigit", function(value, element)
        {
            return this.optional(element) || /^[a-zA-Z0-9\s]+$/i.test(value);
        }, "");

        jQuery.validator.addMethod("lfsccode", function(value, element) {
            //return this.optional(element) || /^[a-z]{4}\d{3}[A-Za-z0-9]{4}$/i.test(value);
            return this.optional(element) || /^[a-z]{4}[A-Za-z0-9]{7}$/i.test(value);
        }, "");

        $.validator.addMethod("letterswithspecialchar", function(value, element)
        {
            return this.optional(element) || /^[a-zA-Z.,@#&_\-\s]+$/i.test(value);
        }, "");

        $.validator.addMethod("valid_mobile_no", function(value, element) {
            return (value == "0000000000" || value == "9999999999") ? false : true;
        }, 'Please enter valid mobile number');
        $.validator.addMethod("mobileno_start_with", function(value, element) {
            var regex = new RegExp("^[7-9]{1}[0-9]{9}$");
            if (value.length > 0)
            {
                if (regex.test(value))
                {
                    return true;
                } else {
                    return false;
                }
            } else
            {
                return true;
            }
        }, 'Mobile No. should start with 7 or 8 or 9 ');

        $.validator.addMethod("pincode_start_with", function(value, element) {
            var regex = new RegExp("^[1-9]{1}[0-9]{5}$");
            if (value.length > 0)
            {
                if (regex.test(value))
                {
                    return true;
                } else {
                    return false;
                }
            } else
            {
                return true;
            }
        }, 'PIN Code. should start with 1-9 ');

        /******** DATE OF BIRTH DATEPICKER ********/
        var dt = new Date();
        dt.setFullYear(new Date().getFullYear() - 18);
        var endDate = dt;
        $("#dob").datepicker({
            changeMonth: true,
            changeYear: true,
            showMonthAfterYear: true,
            yearRange: "-100:-18",
            dateFormat: "dd-mm-yy",
            maxDate: endDate
        });
        $('#dob').keydown(function(event) {
            event.preventDefault();
        });

     
           //by pallavi
        $("#transfer_date").datepicker({
            changeMonth: true,
            changeYear: true,
            showMonthAfterYear: true,
            yearRange: "-100:-18",
            dateFormat: "dd-mm-yy",
            maxDate: endDate
        });
        $('#transfer_date').keydown(function(event) {
            event.preventDefault();
        });
        //edited by sonali on 7-feb-2019
        $("#cheque_date").datepicker({
            changeMonth: true,
            changeYear: true,
            showMonthAfterYear: true,
            //yearRange: "-100:-18",
            yearRange: "c-100:c+0",            
            dateFormat: "dd-mm-yy",
            //maxDate: endDate
        });
        $('#cheque_date').keydown(function(event) {
            event.preventDefault();
        });




        $.validator.addMethod("all_select", function() {
            var isvalid = true;

            $(".docnm").each(function(e) {
                console.log($(this).find('option:selected').text());
                if ($(this).find('option:selected').text() == "Select document name") {
                    isvalid = false;
                } else {
                    isvalid = true;
                }

                if (isvalid) {
                    $(this).removeClass('error');
                } else {
                    $(this).addClass('error');
                }
            });
            return isvalid;


        });

        /******** email validation start here ********/
        $.validator.addMethod("email_exists", function(value, element) {
            var flag;

            $.ajax({
                url: BASE_URL + 'admin/login/is_email_registered',
                data: {email: value, rokad_token: rokad_token},
                async: false,
                type: 'POST',
                success: function(r) {
                    console.log("r : " + r);
                    flag = (r === 'true') ? false : true;
                }
            });

            return flag;
        }, 'Email id already exsist with us');
        /******** email validation start here ********/

        /******** aadhar card Validation start here ********/
        $.validator.addMethod("valid_adharcard_no", function(value, element) {
            var regex = new RegExp("^((?![0,1])[0-9]{12})$");
            if (value.length > 0)
            {
                if (regex.test(value))
                {
                    return true;
                } else {
                    return false;
                }
            } else
            {
                return true;
            }
        }, 'Please enter valid aadharcard number');
        /******** aadhar card Validation end here ********/

        /******** account number Validation start here ********/
        $.validator.addMethod("valid_account_no", function(value, element) {
            var regex = new RegExp("^[0-9]{11,16}$");
            if (value.length > 0)
            {
                if (regex.test(value))
                {
                    return true;
                } else {
                    return false;
                }
            } else
            {
                return true;
            }
        }, 'Please enter valid account number');
        /******** account number Validation end here ********/

        /******** Pancard Validation start here ********/
        $.validator.addMethod("valid_pancard_no", function(value, element) {
            var regex = new RegExp("^[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}$");
            if (value.length > 0)
            {
                if (regex.test(value))
                {
                    return true;
                } else {
                    return false;
                }
            } else
            {
                return true;
            }
        }, 'Please enter valid pancard number');
        /******** Pancard Validation end here ********/
        /******** mobile validation start here ********/
        $.validator.addMethod("mobileno_exists", function(value, element) {
            var flag;

            $.ajax({
                url: BASE_URL + 'admin/login/is_mobile_no_registered',
                data: {mobile_no: value, rokad_token: rokad_token},
                async: false,
                type: 'POST',
                success: function(r) {
                    flag = (r === 'true') ? false : true;
                }
            });

            return flag;
        }, 'Mobile no. already exsist with us');
        /******** mobile validation start here ********/

        /******** GST number Validation start here ********/
        $.validator.addMethod("valid_gst_no", function(value, element) {
            var regex = new RegExp("^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[0-9]{1}Z[0-9 A-Z]{1}$");
            if (value.length > 0)
            {
                if (regex.test(value))
                {
                    return true;
                } else {
                    return false;
                }
            } else
            {
                return true;
            }
        }, 'Please enter valid GST number');
        /******** GST number Validation end here ********/

        /*************STATE-CITY DROPDOWN **********/
        $(document).off('change', '.state').on('change', '.state', function(e)
        {
            $("#agent_city").html("<option value=''>Please wait..</option>");

            var detail = {};
            var div = '';
            var str = "";
            var form = '';
            var ajax_url = 'admin/login/get_statewise_city';

            detail['state_id'] = $("#agent_state").find('option:selected').attr('value');

            if (detail['state_id'] != '')
            {
                $("#agent_city").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
                get_data(ajax_url, form, div, detail, function(response)
                {
                    if (response.city.length != 0)
                    {
                        $("#agent_city").html("<option value=''>Select City</option>");
                        $.each(response.city, function(i, value)
                        {
                            $("#agent_city").append("<option value=" + value.intCityId + ">" + value.stCity + "</option>");
                            $(".chosen_select").trigger("chosen:updated");
                        });
                    }
                    else
                    {
                        $("#agent_city").html("<option value=''>Select City</option>").trigger('chosen:updated');
                    }
                }, '', false);
            }
            else
            {
                $("#agent_city").html("<option value=''>Select City</option>").trigger('chosen:updated');
            }
        });

        $(document).off('change', '.state1').on('change', '.state1', function(e)
        {
            $("#agent_city1").html("<option value=''>Please wait..</option>");

            var detail = {};
            var div = '';
            var str = "";
            var form = '';
            var ajax_url = 'admin/login/get_statewise_city';

            detail['state_id'] = $("#agent_state1").find('option:selected').attr('value');

            if (detail['state_id'] != '')
            {
                $("#agent_city1").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
                get_data(ajax_url, form, div, detail, function(response)
                {
                    if (response.city.length != 0)
                    {
                        $("#agent_city1").html("<option value=''>Select City</option>");
                        $.each(response.city, function(i, value)
                        {
                            $("#agent_city1").append("<option value=" + value.intCityId + ">" + value.stCity + "</option>");
                            $(".chosen_select").trigger("chosen:updated");
                        });
                    }
                    else
                    {
                        $("#agent_city").html("<option value=''>Select City</option>").trigger('chosen:updated');
                    }
                }, '', false);
            }
            else
            {
                $("#agent_city").html("<option value=''>Select City</option>").trigger('chosen:updated');
            }
        });

      //by pallavi for validation of shop state and depot 24-01-2019
          $('#agent_state1').change(function() {
            if ($('#agent_state1').val() == '22') {
                $("#depot_name").prop("disabled", false);
        $('#shop_location').prop('disabled', false);
                $('#dp_nm').show();
                $('#dp_nm1').show();
                $('#dp_nm2').show();
                $('#depot_name').rules('add', 'required');
                $('#stand_name').rules('add', 'required');
                $('#shop_location').rules('add', 'required'); 
                $("#depot_name").html("<option value=''>Please wait..</option>");

                var detail = {};
                var div = '';
                var str = "";
                var form = '';
                detail['state_id'] = $("#agent_state1").find('option:selected').attr('value');
                var ajax_url = 'admin/login/get_depot_list';

                $("#depot_name").html("<option value=''>Please Wait....</option>").trigger('chosen:updated');
                get_data(ajax_url, form, div, detail, function(response)
                {

                    if (response.depot.length != 0)
                    {
                        $("#depot_name").html("<option value=''>Select Depot</option>");
                        $.each(response.depot, function(i, value)
                        {
                            $("#depot_name").append("<option data-division="+value.DIVISION_CD+" value=" + value.DEPOT_CD + ">" + value.DEPOT_NM + "</option>");
                            $(".chosen_select").trigger("chosen:updated");
                        });
                    }
                    else
                    {
                        $("#depot_name").html("<option value=''>Select Depot</option>").trigger('chosen:updated');

                    }
                }, '', false);
            } else {
                $("#depot_name").prop("disabled", true);
                $('#shop_location').prop('disabled', true);
                $("#depot_cd").val("");
                $("#depot_name").html("<option value=''>Select Depot</option>").trigger('chosen:updated');
                $('#dp_nm').hide();
                $('#dp_nm1').hide();
                $('#dp_nm2').hide();
                $('#depot_name').rules('remove', 'required');
                $('#stand_name').rules('remove', 'required');
                $('#shop_location').rules('remove', 'required');

            }
        });
        $('#depot_name').change(function() {
            $("#depot_cd").val($("#depot_name option:selected").text());
             $("#division_cd").val($(this).find(':selected').data('division'));
            
        });
        
        $('#services').change(function() {
            
             $("#service_id").val($(this).find(':selected').data('id'));
            
        });
        
        
        //end pallavi changes

        $("#payment_mode").change(function(){
            if($("#payment_mode").val()=='Cheque'){
                $("#cheque_div").show();
                $("#transfer_div").hide();
            }else{
                $("#cheque_div").hide();
                $("#transfer_div").show();
            }
        });


    });

    $(document.body).on("change", "#services", function() {
        if (this.value != '') {
            $('#services-error').css('display', 'none');
        } else {
            $('#services-error').css('display', 'block');
        }
    });
</script>
<style>
    .ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year{
        width: 45%;
        color:#222;
    }

    /* Popup container - can be anything you want */
    .popup {
        position: relative;
        display: inline-block;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* The actual popup */
    .popup .popuptext {
        /*visibility: hidden;*/
        width: 200px;
        background-color: #555;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 8px 0;
        position: absolute;
        z-index: 1;
        bottom: -80%;
        left: 80%;
        margin-left: 236px;
        height: 200;
    }

    /* Popup arrow */
    .popup .popuptext::before {
        content: "";
        position: absolute;
        top: 100%;
        left: 50%;
        margin-left: -5px;
        border-width: 5px;
        border-style: solid;
        border-color: #555 transparent transparent transparent;
    }

    /* Toggle this class - hide and show the popup */
    .popup .show {
        /*visibility: visible;*/
        -webkit-animation: fadeIn 1s;
        animation: fadeIn 1s;
    }

    /* Add animation (fade in the popup) */
    @-webkit-keyframes fadeIn {
        from {opacity: 0;} 
        to {opacity: 1;}
    }

    @keyframes fadeIn {
        from {opacity: 0;}
        to {opacity:1 ;}
    }
</style>

