<style>
.error{
	margin-top:10px;
	margin-bottom:10px;
}
</style>

<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Aadhar Verification</h2>
        </div>
    </div>
</div>

<section id="content">
    <div class="container tour-detail-page">
        <div class="row login-page">
            <div class="col-sm-8 col-md-6 col-lg-5 no-float center-block" style="clear:both; padding-top:20px;">			
				
				<?php
				$show_error = 'display:none';
				if ($msg_type == "error") 
				{	$show_error = 'display:block';	}
			
				$show_msg = 'display:none';
				if ($msg_type == "success") 
				{	$show_msg = 'display:block';	}
			
				?>				
				<div class="travelo-box" id="error_box" style="<?php echo $show_error; ?>">
					<h3 class="page-title"><i class="glyphicon glyphicon-remove red mr10"></i><span class="msg"><?php echo $msg;?></span></h3>
				</div>

				<div class="travelo-box" id="success_box" style="<?php echo $show_msg; ?>">
					<h3 class="page-title"><i class="soap-icon-check green mr10"></i><span class="msg"><?php echo $msg;?></span></h3>
				</div>				
				
				<?php
				if ($show_adhar_form) {
				?>
				
				<?php
				$attributes = array("method" => "POST", "id" => "login_form", "class" => "login-form");
				echo form_open(base_url().'admin/login/authenticate_adhar', $attributes);
				?>
				<div class="form-group">
					<input type="text" placeholder="Please enter Aadhar Card Number"  name="adharcard" id="adharcard" value=""
					data-msg-required="Please enter Aadhar Card Number" class="input-text input-large full-width" />					
					<?php echo form_error('adhar_card', '<div class="error">', '</div>'); ?>
				</div>			
				<span class="error-befor-button error-message-format"></span><br>
				
				<div class="row">
				<div class="col-sm-6"><button class="btn-large full-width btnorange" id="" type="submit">Submit</button></div>				
				</div>
				
				<?php echo form_close(); ?>
				
				<?php } ?>

				<?php
				if ($show_adhar_verify_link) {
				?>
					<div class="row col-md-12">
						<form method="post" action="<?php echo $adhar_details['request_initiation_url']; ?>">
						<input type="hidden" name="saCode" value="<?php echo $adhar_details['saCode']; ?>" >
						<input type="hidden" name="aadhaarId" value="<?php echo $adhar_details['aadhaarId']; ?>" >
						<input type="hidden" name="requestId" value="<?php echo $adhar_details['requestId']; ?>" >
						<input type="hidden" name="purpose" value="<?php echo $adhar_details['purpose']; ?>" >
						<input type="hidden" name="modality" value="<?php echo $adhar_details['modality']; ?>" >
						<input type="hidden" name="successUrl" value="<?php echo $adhar_details['successUrl']; ?>" >
						<input type="hidden" name="failureUrl" value="<?php echo $adhar_details['failureUrl']; ?>" >
						<input type="hidden" name="hash" value="<?php echo $adhar_details['hash']; ?>" >									
						
						<div class="form-group">
						<label style="font-size: 16px; padding: 15px;">Name : <?php echo $display_name; ?></label>
						<label style="font-size: 16px; padding: 15px;">RMN : <?php echo $mobile_no; ?></label>
						<label style="font-size: 16px; padding: 15px;">Aadhar Id : <?php echo $adhar_details['aadhaarId']; ?></label>
						<div class="col-sm-6"><button class="btn-large full-width btnorange" id="" type="submit">Proceed to KYC</button></div>
						</div>
											
						</form>
					</div>
				
				<?php } ?>
				
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
$(document).ready(function(){
    $('#login_form').validate({
        focusInvalid: false,
        rules: {			
            adharcard: {
                required: true,
				digits: true,                
                minlength: 12,
                maxlength: 12
            }					
        },
        messages: {				
			adharcard: {
                required: "Please enter Aadhar Card Number",
				digits: "Please enter 12 digit Aadhar Card Number",
				minlength: "Please enter 12 digit Aadhar Card Number",
				maxlength: "Please enter 12 digit Aadhar Card Number"
            }			           
         },            
        errorPlacement: function(error, element) {
            error.appendTo(".error-befor-button");
        }
    });
}); // end of document ready
</script>