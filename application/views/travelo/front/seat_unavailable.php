<?php
/*if(isset($seat_already_booked) && count($seat_already_booked) > 1)
{
    $concat = "are";
}
else
{
   $concat = "is"; 
}*/
?>
<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Seat Unavailable</h2>
        </div>
    </div>
</div>

<!-- <section id="content">
    <div class="container tour-detail-page">
        <div class="row mb20">
            <center>
                <img src="<?php echo base_url().FRONT_ASSETS;?>images/error/error-page-Unexpecteda-error.jpg">
            </center>
        </div>
    </div>
</section> -->
<section id="content" class="gray-area">
    <div class="container">
        <div class="row">
            <div id="main" class="col-sm-12 col-md-12">
                <div class="booking-information travelo-box">
                    <h2>Seat Unavailable</h2>
                    <hr />
                    <div class="booking-confirmation clearfix">
                        <i class="fa fa-thumbs-down icon circle"></i>
                        <div class="message">
                            <h4 class="main-message">
                                <!-- Ooopps. Seat No - <?php /*echo implode(",",$seat_already_booked)." ".$concat;*/ ?> already booked. -->
                                 Sorry! The seats that you have selected are no longer available. Please select different seats.
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>

