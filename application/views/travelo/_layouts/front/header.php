<!DOCTYPE html>
<html lang="en">

<head class='test'>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Peyna</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url().FRONT_ASSETS;?>css/bootstrap.min.css" rel="stylesheet">
    <!--link href="<?php echo base_url() . BACK_ASSETS ?>css/AdminLTE.css" rel="stylesheet" type="text/css" /-->
    <link href="<?php echo base_url() . COMMON_ASSETS ?>css/jquery-ui.css" rel="stylesheet">
    <!--link href="<?php echo base_url() . BACK_ASSETS ?>css/style_admin.css" rel="stylesheet" type="text/css" /-->

    <!-- Custom CSS -->
    <link href="<?php echo base_url().FRONT_ASSETS;?>css/style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url().FRONT_ASSETS;?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- jQuery -->
    <!--<script src="<?php echo base_url().FRONT_ASSETS;?>js/jquery.js"></script>-->
    <script type="text/javascript" src="<?php echo base_url().FRONT_ASSETS;?>js/jquery-1.11.1.min.js"></script>
    <!-- Daterange picker -->
    <link href="<?php echo base_url() . BACK_ASSETS ?>css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" >
            var BASE_URL = "<?php echo base_url(); ?>";
            var rokad_token = '<?php echo $this->security->get_csrf_hash(); ?>';
    </script>
    
</head>

<body>
<div id="wrapper">
        <!-- Navigation Start -->
        <nav class="navbar navbar-inverse navbar-fixed-top navbar-expand-lg" role="navigation">
        	<div class="topheader">
                <div class="container-fluid">
                    <div class="pull-right">                        
                        <span><i class="fa fa-mobile" aria-hidden="true" style="font-size:21px;"></i> +91-7798727137</span>
                    </div>
                </div>
            </div>
        	<div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <!--button type="button" onclick="openNav()" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button-->
            
                    <span class="navbar-toggler-icon leftmenutrigger"></span>
                    <a class="navbar-brand" href="<?php echo base_url();?>">
                        <img src="<?php echo base_url().FRONT_ASSETS;?>images/logo.png" class="img-responsive" width="168px" height="40px">
                    </a>
                    
                    <!--div id="mySidenav" class="overlay">
                     <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                        <div class="overlay-content">                       
                           
                             <ul class="nav navbar-nav">
                            <li class="active">
                                <a href="#">Menu 1</a>
                            </li>
                            <li>
                                <a href="#">Menu 2</a>
                            </li>
                            <li>
                                <a href="#">Menu 3</a>
                            </li>
                            <li>
                                <a href="#">Menu 4</a>
                            </li>
                            <li>
                                <a href="#">Menu 5</a>
                            </li>
                        </ul>
                        </div>
                    </div-->
                </div>
                 <marquee style="text-align: center;" width="67%"><b><h3 style="font-size:18px;color: #e95730;"></b><div></div></marquee>
                <div class="pull-right">                    
                    
                    <!--form class="navbar-form navbar-left">
                        <div class="form-group">
                            <i class="fa fa-search" aria-hidden="true"></i>
                          <input type="text" class="form-control search-input" placeholder="Search">
                        </div>
                    </form-->
                    
                    <ul class="nav navbar-right top-nav">
                        <!--li>
                            <p class="navbar-text icon"><a href="#">Rokad Wallet</a></p>
                        </li-->
                        <li class="dropdown" style="margin-right: 15px;">
                            <a href="<?php echo base_url();?>admin/login/login_view" class="signin"><span class="">Log-In</span>  <!--<b class="caret"></b>--></a>
                            <!--<ul class="dropdown-menu">
                                <li>	
                                    <a href="#"> Dummy Text</a>
                                </li>
                            </ul>-->
                        </li>
                        <li class="dropdown">
                            <a href="<?php echo base_url();?>admin/login/signup_view" class="signin"><span class="">Register</span>  <!--<b class="caret"></b>--></a>
                            <!--<ul class="dropdown-menu">
                                <li>	
                                    <a href="#"> Dummy Text</a>
                                </li>
                            </ul>-->
                        </li>
                      </ul>
                </div>
                <!-- Top Menu Items -->
            </div>
        </nav>
        <!-- Navigation End -->