<!-- footer Start -->
        <footer>
            <hr>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4 text-left text-lg-center">
                       
                    </div>
                    <div class="col-lg-4 text-center text-lg-center">
               
                    </div>
                    <div class="col-lg-4 text-right text-lg-center">
                        <a href="#">Terms & Conditions</a> | <a href="#">Privacy Policy</a>
                    </div>
                </div>
            </div>
        </footer>
        <!-- footer End -->
</div>

   
    <!-- /#wrapper -->
     <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url().FRONT_ASSETS;?>js/bootstrap.min.js"></script>
    
    <!-- JS files -->
    <script src="<?php echo base_url() . COMMON_ASSETS ?>js/global.js"></script>
    <script src="<?php echo base_url() . COMMON_ASSETS ?>js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.1/jquery.form-validator.min.js"></script>
    <script src="<?php echo base_url() . COMMON_ASSETS ?>js/jquery-ui.min.js" type="text/javascript"></script>


	<script>
function openNav() {
	if ($(window).width() < 981){
		document.getElementById("mySidenav").style.width = "100%";		
	}else{
		document.getElementById("mySidenav").style.width = "15%";
	}
	
	
	var width = $("#mySidenav").width();
	if (width !== 0){
		document.getElementById("mySidenav").style.width = "0";	
	}
    
}

function closeNav() {
	
    document.getElementById("mySidenav").style.width = "0";
}
</script>
</body>

</html>
