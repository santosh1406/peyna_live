<?php

/* @Author      : Vijay Ghadi
 * @function    : cyberplate
 * @param       : NA
 * @detail      : Wallet Balance Checking
 */

function cyberplate () {
        $cert = CERT;
        $passwd = CP_PASSWORD;
        $secKey = file_get_contents("private.key");
        $serverCert = file_get_contents("mycert.pem");
        $SD = CP_SD;
        $AP = CP_AP;
        $OP = CP_OP;
        $wallet_url = "https://in.cyberplat.com/cgi-bin/mts_espp/mtspay_rest.cgi";
        $date = date("DMdY");
        $sessPrefix = mt_rand(100000, 999999);
        $sess = $date . $sessPrefix;

        $querString = "CERT=" . CERT . "\r\nSD=$SD\r\nAP=$AP\r\nOP=$OP\r\nSESSION=$sess\r\nCOMMENT=Wallet Amount Checking"; //\r\nNUMBER=$phNbr\r\nAMOUNT=$amount\r\nAMOUNT_ALL=$amount\r\nCOMMENT=Test recharge";
        // make SHA1RSA signature
        $pkeyid = openssl_pkey_get_private($secKey, $passwd);
        openssl_sign($querString, $signature, $pkeyid, OPENSSL_ALGO_SHA1);
        openssl_free_key($pkeyid);

        $encoded = base64_encode($signature);
        $encoded = chunk_split($encoded, 76, "\r\n");

        $signInMsg = "BEGIN\r\n" . $querString . "\r\nEND\r\nBEGIN SIGNATURE\r\n" . $encoded . "END SIGNATURE\r\n";
        
        //==============Wallet Balance check Response===================;
        $response = get_query_result($signInMsg, $wallet_url);
        return $check_return_array = responseToArray($response);
        
}

function get_query_result($qs, $url) {
        $opts = array(
            'http' => array(
                'method' => "POST",
                'header' => array("Content-type: application/x-www-form-urlencoded\r\n" .
                    "X-CyberPlat-Proto: SHA1RSA\r\n"),
                'content' => "inputmessage=" . urlencode($qs)
            )
        );
        $context = stream_context_create($opts);
        return file_get_contents($url, false, $context);
}

function responseToArray($convertArray) {
        $store_value = array();
        $split_response = explode("\r\n", $convertArray);
        foreach ($split_response as $sr) {
            $new_key = explode("=", $sr);
            $store_value[$new_key[0]] = $new_key[1];
        }
        return $store_value;
}

function jri_balance(){
        $details = authJRI();
        $input = [
            'CorporateId' => $details['CorporateId'],
            'SecurityKey' => $details['SecurityKey'],
            'AuthKey' => $details['AuthenticationKey']            
        ];
        // show($input, 1);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://spi.justrechargeit.com/JRICorporateRecharge.svc/GetCorporateCardBalance');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50);
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($input));
        $header = array(
            'Content-Type: application/json'
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $httpCode = curl_getinfo($ch , CURLINFO_HTTP_CODE); // this results 0 every time
        $response = curl_exec($ch);
        if ($response === false) $response = curl_error($ch);


        // show($response, 1);
        // echo stripslashes($response);
        curl_close($ch);
        // show($response, 1);
        return json_decode($response, true);
}

?>
