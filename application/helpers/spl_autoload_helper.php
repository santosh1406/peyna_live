<?php
/**
 * SPL Autoload Helper
 * Elliott Brueggeman
 * http://www.ebrueggeman.com
 */
  
/*** nullify any existing autoloads ***/
// spl_autoload_register(null, false);
  
/*** specify extensions that may be loaded ***/
spl_autoload_extensions('.php, .class.php');
  
/*** class Loader ***/
// function classLoader($class)
// {
//     $filename = strtolower($class) . '.class.php';
//     $file ='system/application/controllers/' . $filename;
//     if (!file_exists($file))
//     {
//         return false;
//     }
//     include $file;
// }

function autoload_api($class)
{
	$CI 		= & get_instance();

	$filename 	= ucfirst(strtolower($class)) . '.php';
    $file 		= APPPATH.'controllers/soap_api/' . $filename;


 

    if (!file_exists($file))
    {
        return false;
    }
    include $file;
    
}
  
/*** register the loader functions ***/
spl_autoload_register('autoload_api');
  
  
/* End of file spl_autoload_helper.php */
/* Location: ./system/application/helpers/spl_autoload_helper.php */