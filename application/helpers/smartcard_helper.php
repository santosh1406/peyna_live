<?php
error_reporting(E_ALL);

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//added by shrasti bansal on 6-12-2018
if (!function_exists('curlForPost')) {

    function curlForPostold($curl_url, $request, $basicAuth, $header) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $curl_url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_USERPWD, $basicAuth['username'] . ":" . $basicAuth['password']);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $response = curl_exec($ch);
       /* if (curl_exec($ch) === FALSE) {
   die("Curl Failed: " . curl_error($ch));
} else {
   return curl_exec($ch);
}
         //print_r(curl_getinfo($ch));
        curl_close($ch);
*/
        return $response;
    } 

}




    if (!function_exists('curlForPost')) {

    function curlForPost($curl_url, $input, $sessionData, $header){
       //print_r($input);exit;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $curl_url);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $input);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        //curl_setopt($ch, CURLOPT_CAINFO, getcwd() . "/etc/SSL/*.msrtcors.com.crt");
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        curl_setopt($ch, CURLOPT_USERPWD, $sessionData['username'] . ":" . $sessionData['password']);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
         $a=curl_setopt($ch, CURLOPT_VERBOSE, true);
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            $error_msg = curl_error($ch);
        }     
        return $response;
    }

}

if (!function_exists('calculateAge')) {
    function calculateAge($dob) {
		$datetime1 = new DateTime($dob);
		$datetime2 = new DateTime(TODAY_DATE);
		$interval = $datetime1->diff($datetime2);
		$age = $interval->format('%y');
        return $age;
    }
}

if (!function_exists('getCardType')) {
    function getCardType($age, $cardType) {
        if($age >= AGE_LIMIT_CARDTYPE_CC) {
            $cardType = $cardType;
        } else {
            $cardType = CARDTYPE_U18_CC;
        }
        return $cardType;
    }
}
if (!function_exists('json_response')) {
   function json_response($json) {
       $ci =& get_instance();
       
       $response =   $ci->output
          ->set_status_header(RESPONSE_CODE_SUCCESS_CC)
          ->set_content_type('application/json', 'utf-8')
          ->set_output(json_encode($json));
       return $response;
   }
}

//Added by shrasti bansal for wallet transaction
if (!function_exists('updateWalletTxn')) {
    function updateWalletTxn($input,$comments="Smartcard Transaction"){

        $CI = &get_instance();
        $CI->load->model('wallet_model');
//        $session_id = $input['session_id'];
//
//        $CI->db->from('active_session');
//        $CI->db->where('session_id',$session_id);
//        $query = $CI->db->get()->row();
//
//        //$input = $this->post();
//        //$from_userid = 338;
//        $from_userid = $query->user_id;
        $from_userid = $input['apploginuser'];

        $transfer_amount = $input['amount'];
        $fund_transfer_remark = "Smartcard Txn";

        $from_wallet_details = $CI->wallet_model->where('user_id',$from_userid)->find_all();
        $from_wallet_id = $from_wallet_details[0]->id;
        $wallet_amt = $from_wallet_details[0]->amt;
        
        $transaction_no = substr(hexdec(uniqid()), 4, 12);
        
        
        if($wallet_amt>=$transfer_amount)
        {
            $wallet_trans_detail = [
          "w_id" => $from_wallet_id,
          "amt" => $transfer_amount,
          "merchant_amount" => '',
          "wallet_type" => "actual_wallet",
          "comment" => $comments,
          "status" => 'Debited',
          "user_id" => $from_userid,
          "amt_before_trans" => $from_wallet_details[0]->amt,
          "amt_after_trans" => $from_wallet_details[0]->amt - $transfer_amount,
          "remaining_bal_before" => 0,
          "remaining_bal_after" => 0,
          "cumulative_bal_before" => 0,
          "cumulative_bal_after" => 0,
          "amt_transfer_from" => $from_userid,
          "amt_transfer_to" => 0,
          "transaction_type_id" => '11',   //need to check this value
          "transaction_type" => 'Debited',
          "is_status" => "Y",
          "added_on" => date("Y-m-d H:i:s"),
          //"added_by" => $this->session->userdata('user_id'),
          "added_by" => $from_userid,
          "transaction_no" => $transaction_no,
          "remark" => $fund_transfer_remark
        ];
        
        $CI->db->insert('wallet_trans',$wallet_trans_detail);
        $wallet_trans_id= $CI->db->insert_id();
            
            $update_wallet = [
            "amt" => $wallet_amt - $transfer_amount,
            "last_transction_id" => $wallet_trans_id,
            "updated_by" => $from_userid,
            "updated_date" => date("Y-m-d H:i:s"),
        ];

        $CI->db->where('user_id',$from_userid);
        $update_from_wallet = $CI->db->update("wallet", $update_wallet);
        return array('wallet_trans_id'=>$wallet_trans_id,'transaction_no'=>$transaction_no );
        }
        else
        {
            $ci =& get_instance();
            $message = array('status' => 'failure', 'msg' => "You don't have sufficient wallet amount");
            $ci->set_response($message, 200, FALSE);            
            
        }
        
        


    }
    
}


if (!function_exists('checkWalletTxn')) 
 {
    function checkWalletTxn($input,$comments="Smartcard Transaction"){
        $CI = &get_instance();
        $CI->load->model('wallet_model');
        $from_userid = $input['apploginuser'];
        $transfer_amount = $input['amount'];
        $from_wallet_details = $CI->wallet_model->where('user_id',$from_userid)->find_all();
        $wallet_amt = $from_wallet_details[0]->amt;
        if($wallet_amt>=$transfer_amount)
        {
            return true ;
        }
        else
        {
            return false ;           
        }
    }
}




//Added by mohd rashid for wallet amount
if (!function_exists('getWalletAmount')) {
    function getWalletAmount($input){
        $CI = &get_instance();
        $CI->load->model('wallet_model');
        $from_userid = $input['apploginuser'];
        $from_wallet_details = $CI->wallet_model->where('user_id',$from_userid)->find_all();
        
        return $from_wallet_details[0]->amt;
         
  }
    
}

function ageCalculator($dob){
    if(!empty($dob)){
        $birthdate = new DateTime($dob);
        $today   = new DateTime('today');
        $age = $birthdate->diff($today)->y;
        return $age;
    }else{
        return 0;
    }
}


if (!function_exists('checkWalletTxn_Smartcard')) 
 {
    function checkWalletTxn_Smartcard($input,$comments="Smartcard Transaction"){
        $CI = &get_instance();
        $CI->load->model('wallet_model');
        $from_userid = $input['apploginuser'];
        $transfer_amount = $input['amount'];
        $from_wallet_details = $CI->wallet_model->where('user_id',$from_userid)->find_all();
        $wallet_amt = $from_wallet_details[0]->amt;
        if($wallet_amt<=$transfer_amount){
            $ci =& get_instance();
            $message = array('status' => 'failure', 'msg' => "You don't have sufficient wallet amount" , 'responseCode'=>'500');
            return json_encode($message);        
        }
    }
}

// not needed
if(!function_exists('checkEnglishValidation')) {

    function checkEnglishValidation($request) {

        $request['first_name'] = $request['first_name'];
        $request['middle_name'] = isset($request['middle_name']) ? $request['middle_name'] : '';
        $request['last_name'] = $request['last_name'];
        $request['first_name_lang'] = isset($request['first_name_lang']) ? $request['first_name_lang'] : '';
        $request['middle_name_lang'] = isset($request['middle_name_lang']) ? $request['middle_name_lang'] : '';
        $request['last_name_lang'] = isset($request['last_name_lang']) ? $request['last_name_lang'] : '';


        if (trim($request['first_name']) == trim($request['first_name_lang'])) {
            $responseMessage = ['responseCode' => 500, "responseMessage" => "First name parameter should not be in English"];
            // $this->response_save(array('status' => 'failure', 'msg' => 'First name parameter should not be in English', 'data'=>$responseMessage ), 200);

            $message = array('status' => 'failure', 'msg' => 'failure', 'data' => $responseMessage);
            return json_encode($message);
        }

        if (trim($request['middle_name']) == trim($request['middle_name_lang'])) {
            $responseMessage = ['responseCode' => 500, "responseMessage" => "Middle name parameter should not be in English"];
            // $this->response_save(array('status' => 'failure', 'msg' => 'Middle name parameter should not be in English', 'data'=>$responseMessage ), 200);
            $message = array('status' => 'failure', 'msg' => 'failure', 'data' => $responseMessage);
            return json_encode($message);
        }

        if (trim($request['last_name']) == trim($request['last_name_lang'])) {
            $responseMessage = ['responseCode' => 500, "responseMessage" => "Last name parameter should not be in English"];
            //  $this->response_save(array('status' => 'failure', 'msg' => 'Last name parameter should not be in English', 'data'=>$responseMessage ), 200);
            $message = array('status' => 'failure', 'msg' => 'failure', 'data' => $responseMessage);
            return json_encode($message);
        }
    }

}
    

   

