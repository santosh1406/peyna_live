<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('show_errors')) {
    function show_errors($flag = 1)
    {
        if(ENVIRONMENT == 'development'){
            error_reporting(E_ALL);
            ini_set('display_errors', $flag);
        }
        //return $CI->db->last_query()
    }
}

if (!function_exists('last_query')) {
    function last_query($flag = 0)
    {
        $CI = &get_instance();
        show($CI->db->last_query(), $flag);
        //return $CI->db->last_query()
    }
}

function is_JSON($data)
{
    if (is_string($data) && $data != '') {
        json_decode($data);
        return (json_last_error() === JSON_ERROR_NONE);
    }
    return false;
}

if (!function_exists('upload_files')) {
    function upload_files($fields, $config = array(), $flag = false)
    {
        $data = false;

        $CI = &get_instance();

        if ($flag) {
            return $_FILES;
        }

        if (is_array($config) && count($config) > 0) {
            $CI->upload->initialize($config);
        }

        if (!$CI->upload->do_upload($fields)) {
            return $CI->upload->display_errors();
        }

        return $data = $CI->upload->data();


    }
}


/* 
 * @author		: Mukesh
 * @function	: sendDataOverPost
 * @param		: Url, $fields, $auth;

 * @return		: returns response
 */

if (!function_exists('sendDataOverPost')) {
    function sendDataOverPost($url, $fields = "", $auth = 0, $wait = 90)
    {
        $CI = &get_instance();
        $fieldsData = "";

        $url = str_replace(" ", "%20", $url);

        if (is_array($fields) && count($fields) > 0) {
            $fieldsData = http_build_query($fields);
        }

        $header = array();
        $header = array(
            'X-API-KEY:abcdefghijklmn',
            '_token:2132165465'
        );

        $result = array();
        $ch = curl_init();
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $wait);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $wait);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        // curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY:abcdefghijklmn'));


        if ($auth == 1) {
            curl_setopt($ch, CURLOPT_USERPWD, "appsperts:appsperts@123");
            // curl_setopt($ch, CURLOPT_USERPWD, "ameyav:password");
            // curl_setopt($ch, CURLOPT_USERPWD, "admin:password");
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        }

        curl_setopt($ch, CURLOPT_URL, $url);
       // curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fieldsData);

        //curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: ' . $_SERVER['HTTP_USER_AGENT']));
        $result['response'] = curl_exec($ch);
        $result['info'] = curl_getinfo($ch);
        $result['info']['errno'] = curl_errno($ch);
        $result['info']['errmsg'] = curl_error($ch);
        // show($url);
        // show($result,1);
        return $result['response'];
    }
}


/* 
 * @author      : Sagar
 * @function    : sendDataOverPost
 * @param       : Url, $fields, $auth;

 * @return      : returns response
 */

if (!function_exists('BosSendDataOverPost')) {
    function BosSendDataOverPost($url, $fields = "", $auth = 0, $wait = 90)
    {
        $CI = &get_instance();
        $fieldsData = "";

        $url = str_replace(" ", "%20", $url);

        if (is_array($fields) && count($fields) > 0) {
            $fieldsData = http_build_query($fields);
        }

        $header = array();
       
        $result = array();
        $ch = curl_init();
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, $wait);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $wait);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        if ($auth == 1) {
            curl_setopt($ch, CURLOPT_USERPWD, "rokad@gmail.com:rokad@123");
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fieldsData);
        $result['response'] = curl_exec($ch);
        $result['info'] = curl_getinfo($ch);
        $result['info']['errno'] = curl_errno($ch);
        $result['info']['errmsg'] = curl_error($ch);

        //show($url);
        //show($result,1);
        return $result['response'];
    }
}

/* 
 * @author		: Suraj
 * @function	: getEtravelsmartApiData
 * @param		: Url, $fields, $auth;
 * @return		: returns response
 */

if (!function_exists('getEtravelsmartApiData')) {
    function getEtravelsmartApiData($methodName, $fields = "", $getPost = "GET")
    {
        $CI = &get_instance();
        $result = array();
        $result['response'] = "";
        $fields_to_sent = "";
        $method = $getPost;
        $username = $CI->config->item("etravelsmart_api_username");
        $password = $CI->config->item("etravelsmart_api_password");

        $url = $CI->config->item("etravelsmart_api_url") . trim($methodName);

        if ($method == "GET") {
            if (is_array($fields)) {
                foreach ($fields as $key => $value) {
                    $fields_to_sent .= $key . '=' . $value . '&';
                }

                $fields_to_sent = trim($fields_to_sent, '&');
            } else if ($fields != "") {
                $fields_to_sent = $fields;
            }

            if ($fields_to_sent != "") {
                $url .= "?" . $fields_to_sent;
            }
        }

        /*
         *DO NOT REMOVE BELOW CCOMMENTED LINE.
         *IN preg_match I have used $first_response[3]. 
         *But In future parameter/response may be changed. Index/key of 'WWW-Authenticate: Digest' may be changed.
         *At that time use below commented code and commented preg_match and comment this line of code "$first_response = get_headers($url);".
         */

        /*$ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch,CURLOPT_TIMEOUT, 30);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 30);
        //IF REMOVED BELOW LINE "curl_setopt($ch, CURLOPT_HEADER, 1)" THEN CODE WILL NOT RUN
        curl_setopt($ch, CURLOPT_HEADER, 1); //IMP DON'T REMOVE
        $first_response = curl_exec($ch);
        $info = curl_getinfo($ch);*/

        $first_response = get_headers($url);
        preg_match('/WWW-Authenticate: Digest (.*)/', $first_response[3], $matches);
        // preg_match('/WWW-Authenticate: Digest (.*)/', $first_response, $matches);

        if (!empty($matches)) {
            $auth_header = $matches[1];
            $auth_header_array = explode(',', $auth_header);
            $parsed = array();

            foreach ($auth_header_array as $pair) {
                $vals = explode('=', $pair);
                $parsed[trim($vals[0])] = trim($vals[1], '" ');
            }

            /*
             *$parsed array contains
             *Array("realm" => "Contacts Realm via Digest Authentication","qop" => "auth","nonce" => "MTQzODg2NTI2MjE5MToxNTRhNjE4ZTQxMWE1OGJmYjI5MzkwYjljMmVhYzYyYQ")
             */

            $response_realm = (isset($parsed['realm'])) ? $parsed['realm'] : "";
            $response_nonce = (isset($parsed['nonce'])) ? $parsed['nonce'] : "";
            $response_opaque = (isset($parsed['opaque'])) ? $parsed['opaque'] : "";

            $authenticate1 = md5($username . ":" . $response_realm . ":" . $password);
            $authenticate2 = md5($method . ":" . $url);
            // $A2 = md5("$method:$uriPath");

            $authenticate_response = md5($authenticate1 . ":" . $response_nonce . ":" . $authenticate2);

            $request = sprintf('Authorization: Digest username="%s", realm="%s", nonce="%s", opaque="%s", uri="%s", response="%s"',
                $username, $response_realm, $response_nonce, $response_opaque, $url, $authenticate_response);

            $request_header = array($request);
            $request_header[] = 'Content-Type:application/json';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
            curl_setopt($ch, CURLOPT_TIMEOUT, 100);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 100);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $request_header);

            if ($method == 'POST')
            {
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            }

            $result['response'] = curl_exec($ch);
            $result['info'] = curl_getinfo($ch);
            $result['info']['errno'] = curl_errno($ch);
            $result['info']['errmsg'] = curl_error($ch);
        }

        return $result['response'];
    }
}


if (!function_exists('getDataFromRemote')) {
    function getDataFromRemote($url, $fields = array(), $auth = 0)
    {
        $CI = &get_instance();

        $url = str_replace(" ", "%20", $url);
        $result = array();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_URL, $url);


        $result['response'] = curl_exec($ch);

        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200) {
            return true;
        } else {
            $result['info'] = curl_getinfo($ch);
            $result['info']['errno'] = curl_errno($ch);
            $result['info']['errmsg'] = curl_error($ch);
            curl_close($ch);
            return false;
        }
    }
}


if (!function_exists('load_model')) {
    function load_model($model, $alias = true)
    {
        $CI = &get_instance();

        if (is_array($model) && count($model) > 0) {
            foreach ($model as $key => $value) {
                if ($alias) {
                    $alias_name = str_replace('_model', '', $value);
                    $CI->load->model($value, $alias_name);
                } else {
                    $CI->load->model($value);
                }

            }

        } else {
            if ($alias) {
                $alias_name = str_replace('_model', '', $model);
                $CI->load->model($model, $alias_name);
            } else {
                $CI->load->model($model);
            }
        }
    }
}


/*
 * @access	public
 * @func 	show
 * @param	string
 * @param	integer
 * @return	integer
 */
if (!function_exists('show')) {
    function show($var, $flag = 0, $msg = '')
    {
        $CI = &get_instance();

//        if (in_array($CI->input->ip_address())) {


            echo "<pre> <b>------------------------  START $msg  ------------------------   </b> <br/> <br/>";
            if (is_array($var) || is_object($var)) {
                print_r($var);
            } else {
                echo $var;
            }

            echo " <br/>   <br/>  <b>------------------------  END $msg  ------------------------   </b></pre>";

            if ($flag > 0)
                exit;
//        }
    }
}

/* 
 * @author		: Mukesh
 * @function	: data2json
 * @param		: $data => array to be ecoded
 * @return		: Json_encode_data
 */
if (!function_exists('data2json')) {

    function data2json($data)
    {
        $CI = &get_instance();

        $CI->output->set_content_type('application/json')
            ->set_output(json_encode($data));

    }
}

/* 
 * @author		: Mukesh
 * @function    : Send_mail
 * @param		: Server datafor config,  2nd  param array containing replacement text for the mail
 * @return		: success count
 */

function send_mail($array, $replacements = array())
{
    $CI = &get_instance();
    require_once APPPATH . 'libraries/Swift/lib/swift_required.php';

    $server = ((isset($array['server'])) ? ($array['server']) : ($CI->config->item('common_smtp_server')));
    $username = ((isset($array['username'])) ? ($array['username']) : ($CI->config->item('common_smtp_username')));
    $password = ((isset($array['password'])) ? ($array['password']) : ($CI->config->item('common_smtp_password')));
    $from = ((isset($array['from'])) ? ($array['from']) : (COMMON_MAIL_ADDRESS));
    $return_email = ((isset($array['return_email'])) ? ($array['return_email']) : (COMMON_MAIL_BOUNCE_EMAIL));
    $mail_port_no = $CI->config->item('mail_port_no');


    // Decryption
    //$password = cryptastic_decode($password);
    //Create the Transport


    $transport = Swift_SmtpTransport::newInstance($server)
        ->setPort($mail_port_no)
        ->setUsername($username)
        ->setPassword($password);
    //Create the Mailer using your created Transport
    $mailer = Swift_Mailer::newInstance($transport);

    $message = Swift_Message::newInstance($array['subject'])
        ->setFrom($from)
        ->setBody($array['message'], 'text/html')
        ->setReturnPath($return_email);

    if (array_key_exists('cc', $array) && is_array($array['cc'])) {
        $message->setCc($array['cc']);
    }

    if (is_array($array['to'])) {
        $to_array = $array['to'];
    } else {
        $to_array = explode(',', $array['to']);
    }

    foreach ($to_array as $email) {
        $message->addTo($email);
        $replacements[$email] = isset($replacements[$email]) ? $replacements[$email] : array();
    }

    // register decorator plugin for replacement
    $decorator = new Swift_Plugins_DecoratorPlugin($replacements);
    $mailer->registerPlugin($decorator);

    if (isset($array['replyto'])) {
        $message->setReplyTo($array['replyto']);
    }

//    if (array_key_exists('attachment', $array)) {
//        if (count($array['attachment']) > 0) {
//            foreach ($array['attachment'] as $key => $attachment) {
//                $message->attach(Swift_Attachment::newInstance($attachment['content'], $attachment['attachment_name'], $attachment['attachment_type']));
//            }
//        }
//    }
    
    if (array_key_exists('attachment', $array)) {
			//$attachmentPath = $contactForm->getAttachmentPath();
                        $attachmentPath = $array['attachment'];
			 $message->attach(Swift_Attachment::fromPath($attachmentPath));
		}

    $result = $mailer->send($message);

    return $result;
}


/* 
 * @author		: Suraj Rathod
 * @function    : setup_mail
 * @param		: setup mail content to send
 * @return		: success count
 */

function setup_mail($mailbody_array)
{
    $CI = &get_instance();
    if (!$CI->config->item('enable_app_mail')) {
        return 'true';
    }

    $mail_array = array();
    $replacements = array();
    $mail_content = getMailTemplate($mailbody_array["mail_title"]);
    if (!empty($mail_content)) {
        $mail_content = $mail_content[0];
        $client_email = $mailbody_array["mail_client"];
        $body = $mail_content->body;

        if (is_array($client_email)) {
            $to_array = $client_email;
        } else {
            $to_array = explode(',', $client_email);
        }

        foreach ($to_array as $email) {
            $replacements[$email] = $mailbody_array['mail_replacement_array'];
        }

        $mail_array['subject'] = $mail_content->subject;
        $mail_array['from'] = array(COMMON_MAIL_ADDRESS => COMMON_MAIL_LABEL);
        $mail_array['to'] = $client_email;
        $mail_array['message'] = html_entity_decode($body);

        $result = send_mail($mail_array, $replacements);

        return $result;
    } else {
        return " ";
    }
}


if (!function_exists('add_to_session')) {
    function add_to_session($sessionitemarray = array(), $session_entity = ADMIN_ENTITY)
    {
        $object = &get_instance();
        $array_previous_session = ((isset($object->session->userdata[$session_entity]) ? ($object->session->userdata[$session_entity]) : ""));

        if ($array_previous_session != "") {
            $array_current_session = array_merge($array_previous_session, $sessionitemarray);
        } else {
            $array_current_session = $sessionitemarray;
        }

        $object->session->set_userdata($session_entity, $array_current_session);
    }
}

if (!function_exists('is_ajax_request')) {
    function is_ajax_request()
    {
        $CI = &get_instance();

        if ($CI->input->is_ajax_request() == false) {
            show_404();
            // die('Please send the ajax request');
        }

    }
}


if (!function_exists('is_ajax')) {
    function is_ajax()
    {
        $CI = &get_instance();

        return $CI->input->is_ajax_request();
    }
}

if (!function_exists('show_404')) {
    function show_404()
    {
        $CI = &get_instance();
        load_front_view('show_404');
    }
}

/*
 * function    : remove_html_entities
 * description : function will return result in form of array which remove htmlentities
 *
 * @param 		$result array , which is result of mysql query

 */
function remove_html_entities($result = array())
{

    if (is_array($result)) {
        foreach ($result as $key => $val) {
            if (is_array($val)) {
                $result[$key] = remove_html_entities($val);
            } elseif (is_object($val)) {
                $result[$key] = remove_html_entities($val);
            } else {
                $result[$key] = html_entity_decode($val);
            }
        }
    } elseif (is_object($result)) {
        foreach ($result as $key => $val) {
            if (is_object($val)) {
                $result->$key = remove_html_entities($val);
            } elseif (is_array($val)) {
                $result->$key = remove_html_entities($val);
            } else {
                $result->$key = html_entity_decode($val);
            }
        }
    }

    return $result;
}

/*
 * function    : remove_html_entities
 * description : function will return result in form of array which remove htmlentities
 *
 * @param 		$result array , which is result of mysql query

 */
if (!function_exists('log_me')) {
    function log_me($path, $data)
    {
        $CI = &get_instance();
        $CI->load->helper('file');

        if (is_array($data)) {
            $data = implode(' | ', $data);
        }

        $data .= "\n";


        file_put_contents($path, $data, FILE_APPEND);
    }
}


/*
 * @author      : Suraj Rathod
 * @function    : getRandomId
 * @description : function will return numeric/alphabetic/alphanumeric random number
 * @param       : $numchars -> length of random string
 *               $type -> type of random string    
 */
if (!function_exists('getRandomId')) {
    function getRandomId($numchars = 8, $type = "alphanumeric")
    {
        $numeric = "0,1,2,3,4,5,6,7,8,9";
        $alphabetic = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z";

        if ($type == "numeric")
            $char_str = $numeric;
        else if ($type == "alphabetic")
            $char_str = $alphabetic;
        else
            $char_str = $numeric . "," . $alphabetic;

        $chars = explode(',', $char_str);
        $randid = '';

        for ($i = 0; $i < $numchars; $i++)
            $randid .= $chars[rand(0, count($chars) - 1)];

        return $randid;
    }
}

/*
 * @author      : Suraj Rathod
 * @function    : google_captcha
 * @description : function return captcha detail
 * @param       : $response -> captcha verify google response to sent
 */

if (!function_exists('google_captcha')) {
    function google_captcha($g_recaptcha_response)
    { //print_r($g_recaptcha_response);die;
        $CI = &get_instance();

	$url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . $CI->config->item("google_captcha_secret_key") . '&response=' . $g_recaptcha_response;
	
	$response = sendDataOverGet($url);
	
	return json_decode($response);

    }
}
/*
if (!function_exists('google_captcha')) {
    function google_captcha_old($g_recaptcha_response)
    {
        $CI = &get_instance();
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . $CI->config->item("google_captcha_secret_key") . '&response=' . $g_recaptcha_response;

        $fields['secret'] = $CI->config->item("google_captcha_secret_key");
        $fields['response'] = $g_recaptcha_response;

        $response = sendDataOverPost($url, $fields);
        return json_decode($response);

    }
}*/

# Transform hours like "1:45" into the total number of minutes, "105".
function hoursToMinutes($hours)
{
    if (strstr($hours, ':')) {
        # Split hours and minutes.
        $separatedData = explode(":", $hours); //split(':', $hours);

        $minutesInHours = $separatedData[0] * 60;
        $minutesInDecimals = $separatedData[1];

        $totalMinutes = $minutesInHours + $minutesInDecimals;
    } else {
        $totalMinutes = $hours * 60;
    }

    return $totalMinutes;
}

# Transform minutes like "105" into hours like "1:45".
function minutesToHours($minutes)
{
    $hours = floor($minutes / 60);
    $decimalMinutes = $minutes - floor($minutes / 60) * 60;

    # Put it together.
    $hoursMinutes = sprintf("%d:%02.0f", $hours, $decimalMinutes);
    return $hoursMinutes;
}

//echo hoursToMinutes("1:45");

//save the page wise tracking
function save_log_pagewise()
{
    $CI =& get_instance();
    $CI->load->model('logger_model');
    $CI->logger_model->save_data();
}


if (!function_exists('to_currency')) {
    function to_currency($number)
    {
        $CI = &get_instance();

        $number = (empty($number)) ? 0 : $number;
        
        $currency_symbol = $CI->config->item('currency_symbol') ? $CI->config->item('currency_symbol') : '$';

        if ($number >= 0) {
            if ($CI->config->item('currency_side') !== 'right')
                return $currency_symbol . ' ' . number_format($number, 2, '.', '');
            else
                return number_format($number, 2, '.', '') . ' ' . $currency_symbol;
        } else {
            if ($CI->config->item('currency_side') !== 'right')
                return '-' . $currency_symbol . ' ' . number_format(abs($number), 2, '.', '');
            else
                return '-' . number_format(abs($number), 2, '.', '') . ' ' . $currency_symbol;
        }
    }
}
/** END MODIFIED **/

if (!function_exists('to_currency_no_money')) {
    function to_currency_no_money($number)
    {
        $number = (empty($number)) ? 0 : $number;
        return number_format($number, 2, '.', '');
    }
}

if (!function_exists('init_cap')) {
    function init_cap($str)
    {
        return ucfirst(strtolower($str));
    }
}


/*
 * @author      : Suraj rathod
 * @function    : getBossRefNo
 * @description : This function will generate bos reference number and return
 */

function getBossRefNo($tid)
{
    $CI = &get_instance();

    // To generate next max bos ticket digit and bos reference number
    $bos_ticket_digit = $tid;
    $bos_ticket_string = date("y") . str_pad($bos_ticket_digit, 8, "0", STR_PAD_LEFT);

    $bos["bos_ticket_digit"] = $bos_ticket_digit;
    $bos["bos_ticket_string"] = $bos_ticket_string;
    $bos["bos_ref_no"] = dechex($bos_ticket_string);

    return $bos;
}


if (!function_exists('toArray')) {
    function toArray($object)
    {
        if (!is_object($object) && !is_array($object)) {
            return $object;
        }
        if (is_object($object)) {
            $object = get_object_vars($object);
        }
        return array_map('toArray', $object);
    }
}

if (!function_exists('e')) {
    /**
     * A convenience function to ensure output is safe to display. Helps to
     * defeat XSS attacks by running the text through htmlspecialchars().
     *
     * Should be used anywhere user-submitted text is displayed.
     *
     * @param String $str The text to process and output.
     *
     * @return void
     */
    function e($str)
    {
        echo htmlspecialchars(trim($str), ENT_QUOTES, 'UTF-8');
    }
}

if (!function_exists('escape')) {
    
    function escape($str)
    {
        return htmlspecialchars(trim($str), ENT_QUOTES, 'UTF-8');
    }
}


if (!function_exists('calculate_date_diff')) {
    function calculate_date_diff($d1, $d2)
    {
        if ($d1 != "" && $d1 != "") {
            $time_one = new DateTime(date("Y-m-d H:i:s", strtotime($d1)));
            $time_two = new DateTime(date("Y-m-d H:i:s", strtotime($d2)));
            $difference = $time_one->diff($time_two);
            return $difference;
        }
    }
}


if (!function_exists('calculate_travel_duration')) {
    function calculate_travel_duration($tmdiff)
    {
        $new_time_to_show = "";

        if ($tmdiff != "")
        {
            $days    = $tmdiff->format('%d');
            $hours   = $tmdiff->format('%h');
            $minutes = $tmdiff->format('%i');

            $days_converted_to_hr = 0;
            $calculated_hrs = 0;
            if($days > 0)
            {
                $days_converted_to_hr = ($days * 24);
            }

            if($hours > 0 || $days_converted_to_hr > 0)
            {
                $calculated_hrs = $days_converted_to_hr + $hours;

                if(strlen((string) $calculated_hrs) == 1)
                {
                    $calculated_hrs = sprintf("%02d", $calculated_hrs);
                }

                $new_time_to_show = $calculated_hrs.":".sprintf("%02d", $minutes)." Hrs";
            }
            else
            {
                $new_time_to_show = "00:".sprintf("%02d", $minutes)."Hrs";
            }
        }

        return $new_time_to_show;
    }
}

if (!function_exists('make_dir')) {
    function make_dir($path, $mode = 0755, $recursive = false)
    {
        $isfile = strpos(basename($path), '.');

        if ($isfile) {
            $path = substr($path, 0, strrpos($path, '/'));
        }


        if (!file_exists($path)) {
            mkdir($path, $mode, $recursive);
            chmod($path, 0770);
        }
    }
}


if (!function_exists('log_data')) {
    function log_data($file, $data = NULL, $commet = '')
    {
        if ($file != '' && $data != NULL) {
            $file = FCPATH . 'log/' . $file;
            make_dir($file, 0770, true);

            $data = print_r($data, true);

            file_put_contents($file, PHP_EOL . '/******** start ' . $commet . ' **********/ ' . PHP_EOL, FILE_APPEND);
            file_put_contents($file, $data, FILE_APPEND);
            file_put_contents($file, PHP_EOL . '/******** end ' . date("H:i:s") . "********" . $commet . ' **********/' . PHP_EOL . PHP_EOL, FILE_APPEND);
        }
    }
}


if (!function_exists('logging_data')) {
    function logging_data($file, $data = NULL, $commet = '')
    {
        if ($file != '' && $data != NULL) {
            $file = FCPATH . 'log/'. date('Y') .'/'.date("M").'/'.date("d").'/' . $file;

            make_dir($file, 0770, true);
            
            $data = print_r($data, true);

            file_put_contents($file, PHP_EOL . '/******** start ' . $commet . ' **********/ ' . PHP_EOL, FILE_APPEND);
            file_put_contents($file, $data, FILE_APPEND);
            file_put_contents($file, PHP_EOL . '/******** end ' . date("H:i:s") . "********" . $commet . ' **********/' . PHP_EOL . PHP_EOL, FILE_APPEND);
        }
    }
}

/*********Image uplaod from API************/

if (!function_exists('save_remote_image')) {
    function save_remote_image($remote_path, $local_path)
    {
        $data = sendDataOverPost($remote_path, array());

        // file_put_contents($local_path, $data);
        copy($remote_path, $local_path);
    }
}


/***** Image get size************/
if (!function_exists('remote_file_size')) {
    function remote_file_size($remote_image)
    {
        $data = get_headers($remote_image, true);

        if (isset($data['Content-Length']))
            return byte_to_kb((int)$data['Content-Length']);
    }
}

if (!function_exists('byte_to_kb')) {
    function byte_to_kb($size)
    {
        # size smaller then 1kb
        return round(sprintf("%4.2f", $size / 1024));

    }
}


/*
 * function    : create_pagination
 * description : function will return pagination links
 *
 * @param 		$config				array containing configuration values for pagination
 */
function create_pagination($config = array())
{
    $ci = &get_instance();
    $ci->load->library('ajax_pagination');

    // $config = $ci->config->item('pagination');

    // show($config,1);

    // $config['full_tag_open'] 	= "<ul class='pagination'>";
    // $config['full_tag_close'] 	="</ul>";
    // $config['num_tag_open'] 	= '<li>';
    // $config['num_tag_close'] 	= '</li>';
    $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
    $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
    // $config['next_tag_open'] 	= "<li>";
    // $config['next_tagl_close'] 	= "</li>";
    // $config['prev_tag_open'] 	= "<li>";
    // $config['prev_tagl_close'] 	= "</li>";
    // $config['first_tag_open'] 	= "<li>";
    // $config['first_tagl_close'] = "</li>";
    // $config['last_tag_open'] 	= "<li>";
    // $config['last_tagl_close'] 	= "</li>";
    // $config['postVar'] 			= "page";

    $config['cur_page'] = true;
    $config['prev_link'] = "&#8249;";
    $config['next_link'] = "&#8250;";

    /**/
    $config['first_link'] = '&laquo;';
    $config['first_tag_close'] = '';
    $config['last_tag_close'] = '';
    $config['last_link'] = '&raquo;';

    $config['cur_tag_open'] = '<span class="active"><b>';
    $config['cur_tag_close'] = '</b></span>';

    $config['prev_link'] = '&#8249;';
    $config['next_link'] = '&#8250;';


    $config['page_query_string'] = true;


    $ci->ajax_pagination->initialize($config);
    return $ci->ajax_pagination->create_twig_links();
}


if (!function_exists('change_column_to_key')) {
    function change_column_to_key($data, $col)
    {
        $new_array = array();
        foreach ($data as $key => $value) {
            $new_array[$value[$col]] = $value;
        }
        return $new_array;
    }
}

if (!function_exists('obj2array')) {
    function obj2array($data)
    {
        $result = false;
        if (is_object($data)) {
            $result = json_decode(json_encode($data), true);
        } else if (is_array($data)) {
            $result = $data;
        }

        return $result;
    }
}

if (!function_exists('to_array')) {
    function to_array($data, $make_array =true)
    {
        if($data)        
        {
            return $result = json_decode(json_encode($data), $make_array);
        }
    }
}


/*
 * @author      : Suraj Rathod
 * @function    : getGeoLocation
 * @description : function will return detail about IP Address, Ex. City,State,Country
 * @param       : $ipaddress -> IP Address
 *               
 */
if (!function_exists('getGeoLocation')) {
    function getGeoLocation($ipaddress)
    {
        return @unserialize(file_get_contents('http://ip-api.com/php/' . $ipaddress));
    }
}

function get_database($db)
{
    $ci = &get_instance();
       
    $db = $ci->load->database($db, true);
    $data = $db->database;
    return $data;
}

if (!function_exists('convert_str_to_array')) {
    function convert_str_to_array($str, $delimiter)
    {
        $str = str_ireplace($delimiter, '&', $str);
        parse_str($str, $data);
        return $data;
    }
}


/*
 * @author      : Suraj Rathod
 * @function    : getConveyCharges
 * @description : function will return seat fare with convey charges
 * @param       : $ipaddress -> IP Address
 *               
 */
if ( ! function_exists('getConveyCharges'))
{
	function getConveyCharges($seat_fare, $operator = "upsrtc",$extra_value = "")
	{
        /*
        *NOTICE: MOST IMP
        *
        *
        *BELOW WRITTEN LOGIC IS STATIC CONVEY CHARGES LOGIC.
        *THE LOGIC WRITTEN BELOW IS TEMP ALTERNATIVE FOR DYNAMIC LOGIC
        *CHANGE THIS AFTER DISCUSSION
        */
        /**************CONVEY CHARGE STATIC LOGIC START *****************/
       
		if(strtolower($operator) == "upsrtc")
		{
	        /*
	        *NOTICE: MOST IMP
	        *
	        *
	        *BELOW WRITTEN LOGIC IS STATIC CONVEY CHARGES LOGIC.
	        *THE LOGIC WRITTEN BELOW IS TEMP ALTERNATIVE FOR DYNAMIC LOGIC
	        *CHANGE THIS AFTER DISCUSSION
	        */
	        /**************CONVEY CHARGE STATIC LOGIC START *****************/
	        $fare_with_conveycharge = $seat_fare;
	        $three_percent_tot_fare = $seat_fare + (($seat_fare * PERCENT_TICKET_CONVEY_CHARGE)/100);
	        $fifty_per_ticket = ($seat_fare + PER_TICKET_CONVEY_CHARGE);
	   
	        if($three_percent_tot_fare > $fifty_per_ticket)
	        {
	          $fare_with_conveycharge = $three_percent_tot_fare;
	        }
	        else
	        {
	          $fare_with_conveycharge = $fifty_per_ticket;
	        }

	        return $fare_with_conveycharge;
	    }
        else if(strtolower($operator) == "rsrtc")
        {
            if(!empty($extra_value))
            {
                eval(RSRTC_BUS_TYPES_AC);
                if(in_array($extra_value['op_bus_type_cd'], $rsrtc_bus_types_ac_array))
                {
                    $fare_with_conveycharge = ($seat_fare + RSRTC_PER_TICKET_CONVEY_CHARGE_FOR_AC);
                }
                else
                {
                    $fare_with_conveycharge = ($seat_fare + RSRTC_PER_TICKET_CONVEY_CHARGE_FOR_NON_AC);
                } 

                return $fare_with_conveycharge; 
            }
            else
            {
                return $seat_fare;
            }
        }
	    else
	    {
	    	return $seat_fare;
	    }
    }
}


/*
 * @author      : Suraj Rathod
 * @function    : etravelsmarfarediscount
 * @description : Old discount calculation function . Right now use in apiGetAvailabeBus function
 * @param       : 
 *               
 */
if (!function_exists('etravelsmarfarediscount')) {
    function etravelsmarfarediscount($total_fare)
    {
        $CI = &get_instance();
        /*******temp discount logic start******/
        if($CI->config->item('discount_type') == "percentage")
        {
            $discount_percentage = $CI->config->item('discount_percent_rate');
            $discount_value = ($total_fare * $discount_percentage)/100;
			if($discount_value > $CI->config->item('max_dis_availed'))
			{
				$discount_value = $CI->config->item('max_dis_availed');
			}
            $fare = $total_fare - $discount_value;
            return $fare;
        }
        else if($CI->config->item('discount_type') == "flat")
        {
            if((float)$total_fare >= $CI->config->item('discount_above'))
            {
                $discount_value = $CI->config->item('discount_flat_rate');
				if($discount_value > $CI->config->item('max_dis_availed'))
				{
					$discount_value = $CI->config->item('max_dis_availed');
				}
                $fare = $total_fare - $discount_value;
                return $fare;     
            }
            else
            {
                return $total_fare;
            }
        }
        /*******temp discount logic end******/
    }
}

/*
 * @author      : Mihir Shah
 * @function    : etravelsmarfarediscount_new
 * @description : new  discount calculation function.
 * @param       : 
 *               
 */
if (!function_exists('etravelsmarfarediscount_new')) {
    function etravelsmarfarediscount_new(&$total_fare, $key, &$discount_amt)
    {
        $CI = &get_instance();
        /*******temp discount logic start******/
        $total_fare = $total_fare - $discount_amt;
        /*******temp discount logic end******/
    }
}

/*
 * @author      : Suraj Rathod
 * @function    : hrtcfarediscount
 * @description : 
 * @param       : 
 *               
 */
if (!function_exists('hrtcfarediscount')) {
    function hrtcfarediscount($total_fare)
    {
        $CI = &get_instance();
        /*******temp discount logic start******/
        if($CI->config->item('discount_type') == "percentage")
        {
            $discount_percentage = $CI->config->item('discount_percent_rate');
            $discount_value = ($total_fare * $discount_percentage)/100;
            $fare = $total_fare - $discount_value;
            return $fare;
        }
        else if($CI->config->item('discount_type') == "flat")
        {
            if((float)$total_fare >= $CI->config->item('discount_above'))
            {
                $discount_value = $CI->config->item('discount_flat_rate');
                $fare = $total_fare - $discount_value;
                return $fare;     
            }
            else
            {
                return $total_fare;
            }
        }
        /*******temp discount logic end******/
    }
}

/*
 * @author      : Suraj Rathod
 * @function    : upsrtcfarediscount
 * @description : 
 * @param       : 
 *               
 */
if (!function_exists('upsrtcfarediscount')) {
    function upsrtcfarediscount($total_fare)
    {
        $CI = &get_instance();
        /*******temp discount logic start******/
        /*$discount_percentage = DISCOUNT_PERCENT_RATE;
        $discount_value = ($total_fare * $discount_percentage)/100;
        $fare = $total_fare - $discount_value;
        return $fare;*/
        if($CI->config->item('discount_type') == "percentage")
        {
            $discount_percentage = $CI->config->item('discount_percent_rate');
            $discount_value = ($total_fare * $discount_percentage)/100;
            $fare = $total_fare - $discount_value;
            return $fare;
        }
        else if($CI->config->item('discount_type') == "flat")
        {
            if((float)$total_fare >= $CI->config->item('discount_above'))
            {
                $discount_value = $CI->config->item('discount_flat_rate');
                $fare = $total_fare - $discount_value;
                return $fare;     
            }
            else
            {
                return $total_fare;
            }
        }
        /*******temp discount logic end******/
    }
}

/*
 * @author      : Suraj Rathod
 * @function    : commonfarediscount
 * @description : 
 * @param       : 
 *               
 */
if (!function_exists('commonfarediscount')) {
    function commonfarediscount($total_fare)
    {
        $CI = &get_instance();
        /*******temp discount logic start******/
        $discount_percentage = $CI->config->item('discount_percent_rate');
        $discount_value = ($total_fare * $discount_percentage)/100;
        $fare = $total_fare - $discount_value;
        return $fare;
        /*******temp discount logic end******/
    }
}


if ( ! function_exists('mail_ticket'))
{
    function mail_ticket($pnr, $user_id)
    {

        $ci = & get_instance();

        $ticket    = $ci->tickets_model
                                    // ->where('transaction_status', 'success')
                                    ->find_by(array('pnr_no' => $pnr));

        if(!$ticket){
            return array( 'status' => 'failed' ,'msg' => 'No ticket found');
        }

        // $ticket_detail = $this->ticket_details_model
        //                         ->where(array('ticket_id' => $ticket->ticket_id))
        //                         // ->as_array()
        //                         ->find_all();
        
        // show($ticket_detail,;            
        
        $details = $ci->common_model->get_provider_lib($ticket->provider_id,$ticket->provider_type,$ticket->op_id);
        $library_name =  strtolower(trim($details['library'])).'_api';

        return $data = $ci->buses_api->{$library_name}->emailTicket($ticket, $user_id);
    }
}



if ( ! function_exists('sms_ticket'))
{
    function sms_ticket($pnr)
    {
        $ci = & get_instance();
        $ticket    = $ci->tickets_model
                                // ->where('transaction_status', 'success')
                                ->find_by(array('pnr_no' => $pnr ));

        if($ticket)
        {
            $ticket_detail = $ci->ticket_details_model
                                    ->where(array('ticket_id' => $ticket->ticket_id))
                                    // ->as_array()
                                    ->find_all();
            // show($ticket_detail,;
            if(strtotime($ticket->dept_time) > strtotime(date('Y-m-d H:i:s')) )
            {
                if($ticket_detail)
                { 
                    $seat_with_berth = '';

                    foreach ($ticket_detail as $key => $value) {
                        $seat[$value->berth_no][] = $value->seat_no;
                    }


                    foreach ($seat as $key => $value) {
                        $seat_with_berth = "Berth $key: ". rtrim(implode(',',$value), ",");
                        if(count($seat) >1 ){ $seat_with_berth .= '|'; }
                    }

                    // show($seat,1);
                    // show($ticket, 1);

                    $ticket_sms_detail = array(
                                        "{{pnr_no}}" => $ticket->pnr_no,
                                        "{{bos_ref_no}}" => $ticket->boss_ref_no,
                                        "{{amount_to_pay}}" => $ticket->tot_fare_amt_with_tax,
                                        "{{from_stop}}" => $ticket->from_stop_name,
                                        "{{to_stop}}" => $ticket->till_stop_name,
                                        "{{seat_no}}" =>  $seat_with_berth,
                                        "{{doj}}" => $ticket->dept_time
                                      );

                    $temp_sms_msg = getSMS("ticket_booking");
                    $msg_to_sent = str_replace(array_keys($ticket_sms_detail),array_values($ticket_sms_detail),$temp_sms_msg[0]->sms_template_content);

                    // show($msg_to_sent,1);

                    $is_send = sendSMS($ticket->mobile_no, $msg_to_sent, array('ticket_id' => $ticket->ticket_id));

                    if($is_send){
                        return array( 'status' => 'success' ,'msg' => 'Sms Sent Successfully');
                    }
                    else
                    {
                        return array( 'status' => 'success' ,'msg' => 'sms failed, Please contact the vendor');
                    }

                }
                else
                {
                    return array('status' => 'failed' ,'msg' => 'Issue with ci ticket Please contact the vendor');
                }
            }
            else
            {
                return array( 'status' => 'failed' ,'msg' => 'ticket has been expired');
            }
        }
        else
        {
            return array( 'status' => 'failed' ,'msg' => 'No ticket found');
        }
    }
}

if ( ! function_exists('to_url'))
{
    function to_url($path)
    {
        if(empty($path)) return false;
        
        return base_url().str_replace(FCPATH, '',$path);
    }
}




if (!function_exists('sec2hms'))
{
    function sec2hms($sec, $padHours = true) 
    {
        // start with a blank string
        $hms = "";
        $hours = intval(intval($sec) / 3600); 
        $hms .= ($padHours) ? str_pad($hours, 2, "0", STR_PAD_LEFT). ":" : $hours. ":";
        $minutes = intval(($sec / 60) % 60); 
        $hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT);

        // $seconds = intval($sec % 60); 

        // $hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT);
        return $hms;

    }
}

if (!function_exists('makeArrayunique'))
{
    function makeArrayUnique($array) 
    {
        if(is_array($array) && !empty($array))
        {
            return array_values(array_unique(array_filter($array)));
        }
    }
}


if (!function_exists('is_ors_agent_ticket'))
{
    function is_ors_agent_ticket($available_detail) 
    {
        $ci = & get_instance();
        if($available_detail)
        {
            $provider_type = isset($available_detail["provider_type"]) ? $available_detail["provider_type"] : "";
            
            if($provider_type != "")
            {  
                // Old Where only etravel ticket add in to agent tickets table 
				eval(FME_AGENT_ROLE_NAME);
                if($ci->session->userdata("role_name") != "" && in_array(strtolower($ci->session->userdata("role_name")),$fme_agent_role_name) && strtolower($provider_type) == "api_provider")
                {
                    return true;
                }
                /*if($ci->session->userdata("role_name") != "" && ORS_AGENT_ROLE_NAME == strtolower($ci->session->userdata("role_name")))
                {
                    return true;
                }*/
            }
        }

        return false;
    }
}

if (!function_exists('ors_agent_ticket_entry'))
{
    function ors_agent_ticket_entry($available_detail) 
    {
        $ci = & get_instance();
        if($available_detail)
        {
            $provider_type = isset($available_detail["provider_type"]) ? $available_detail["provider_type"] : "";
            
            if($provider_type != "")
            {  
                // Old Where only etravel ticket add in to agent tickets table 
                /*if($ci->session->userdata("role_name") != "" && ORS_AGENT_ROLE_NAME == strtolower($ci->session->userdata("role_name")) && strtolower($provider_type) == "api_provider")
                {
                    return true;
                }*/
				eval(AGENT_SITE_ARRAY);
                if($ci->session->userdata("role_name") != "" && in_array(strtolower($ci->session->userdata("role_id")),$agent_site_array))
                {
                    return true;
                }
            }
        }

        return false;
    }
}


if (!function_exists('check_search_bus_type'))
{
    function check_search_bus_type($fields, $extra_array)
    {
        eval(BOS_BUS_TYPES_AC);
        $to_proceed = false;
        $busType = isset($extra_array["busType"]) ? $extra_array["busType"] : "";
        $mapped_bus_types = $extra_array["mapped_bus_types"];
        
        if(!empty($extra_array) && !empty($fields) && $busType != "")
        {
            if(array_key_exists(trim($busType), $mapped_bus_types))
            {
              if($fields['searchbustype'] == "ac" && in_array($mapped_bus_types[trim($busType)], $bos_bus_types_ac_array))
              {
                $to_proceed = true;
              }
              else if($fields['searchbustype'] == "non_ac" && !in_array($mapped_bus_types[trim($busType)], $bos_bus_types_ac_array))
              {
                $to_proceed = true;
              }
              else
              {
                $to_proceed = false;
              }
            }
            else
            {
              if($fields['searchbustype'] == "ac")
              {
                 $to_proceed = false;
              }
            }
        }

        return $to_proceed;
    }
}


if (!function_exists('make_ucwords'))
{
    function make_ucwords($name) 
    {
        return ucwords(strtolower($name));
    }
}

if(!function_exists('insert_request_response_log'))
{
    function insert_request_response_log($data) 
    {
        $ci = & get_instance();
        $ci->load->model(["tracking_request_response_model"]);
        $ci->tracking_request_response_model->insert($data);
    }
}

if(!function_exists('update_request_response_log'))
{
    function update_request_response_log($data) 
    {
        
    }
}



/* 
 * @author      : Suraj
 * @function    : CcaSendDataOverPost
 * @param       : Url, $fields, $auth;
 * @return      : returns response
 */

if(!function_exists('CcaSendDataOverPost'))
{
    function CcaSendDataOverPost($url, $fields = "", $auth = 0, $wait = 90)
    {
        if (!is_array($fields) || empty($fields))
        {
            return "error";
        }

        $CI = &get_instance();
        $fieldsData = "";

        $url = str_replace(" ", "%20", $url);

        $postvars   =   '';
        $sep        =   '';

        foreach($fields as $key=>$value)
        {
            $postvars.= $sep.urlencode($key).'='.urlencode($value);
            $sep='&';
        }

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_POST,count($fields));
        curl_setopt($ch,CURLOPT_CAINFO, CCAVENUE_CACERT_FILE_PATH);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$postvars);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result['response']  = curl_exec($ch);
        $result['info'] = curl_getinfo($ch);
        $result['info']['errno'] = curl_errno($ch);
        $result['info']['errmsg'] = curl_error($ch);
        return $result['response'];
    }
}



if(!function_exists('change_key_of_array'))
{
    function change_key_of_array($array = array(), $value_to_key = "") 
    {
        $array_to_return = [];
        if(!empty($array) && $value_to_key != "")
        {
            foreach ($array as $key => $value)
            {
                $array_to_return[$value[$value_to_key]] = $value;
            }
        }

        return $array_to_return;
    }
}

if(!function_exists('FindValueFromArray'))
{
    function FindValueFromArray($array,$ID,$columnName = '',$discountOn = '',$discountcolumn = '') {
    
        return array_values(array_filter($array, function($arrayValue) use($columnName,$ID,$discountOn,$discountcolumn) 
        { 
            if($discountcolumn != '')
            {
                if($columnName != '')
                {
                    if($arrayValue[$columnName] == $ID && $arrayValue[$discountcolumn] == $discountOn)
                    {
                        
                        return $arrayValue[$columnName] == $ID;
                    }
                }
                else
                {
                    if($arrayValue['is_bus_type_based'] == $ID && $arrayValue['is_operator_based'] == $ID && $arrayValue['is_transaction_based'] == $ID && $arrayValue['is_ticket_count_based'] == $ID && $arrayValue[$discountcolumn] == $discountOn)
                    {
                        return $arrayValue;
                    }
                }
            }
            else if($columnName != '')
            {
                return $arrayValue[$columnName] == $ID;
            }

            else
            {
                if($arrayValue['is_bus_type_based'] == $ID && $arrayValue['is_operator_based'] == $ID && $arrayValue['is_transaction_based'] == $ID && $arrayValue['is_ticket_count_based'] == $ID)
                {
                    return $arrayValue;
                }
            }
             
        } 
        ));
    }
}

   /*
    * @Author      : Santosh Warang
    * @function    : getDefinedDataTable()
    * @param       : $aColumns
    * @detail      : Get User Friendly Data Table Data.
    *                 
    */ 
	
 function getDefinedDataTable($aColumns)
 {
     /** Paging */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".stripslashes( $_GET['iDisplayStart'] ).", ".
			stripslashes( $_GET['iDisplayLength'] );
	}
	$data['sLimit'] = $sLimit;
	
	/** Ordering */
    $sOrder='';
	if ( isset( $_GET['iSortCol_0'] ) )
	{
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
		{
			if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
			{
				$sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
				 	".stripslashes( $_GET['sSortDir_'.$i] ) .", ";
			}
		}
		
		$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" )
		{
			$sOrder = "";
		}
	}
	$data['sOrder'] = $sOrder;
	
	/* 
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	$sWhere = "";
	if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
	{
		$sWhere = "WHERE (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= $aColumns[$i]." LIKE '%".stripslashes( $_GET['sSearch'] )."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
	}
	
	/* Individual column filtering */
	for ( $i=0 ; $i<count($aColumns) ; $i++ )
	{
		if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
		{
			if ( $sWhere == "" )
			{
				$sWhere = "WHERE ";
			}
			else
			{
				$sWhere .= " AND ";
			}
			$sWhere .= $aColumns[$i]." LIKE '%".stripslashes($_GET['sSearch_'.$i])."%' ";
		}
	}
        $data['sWhere'] = $sWhere;
        return $data;
  }
    /*
    * @Author      : Santosh Warang
    * @function    : getUserWalletAmount()
    * @param       : $ID
    * @detail      : Get User Wallet Amount.
    *                 
    */ 
 
    function getUserWalletAmount($userid) 
    {   
	    $ci = & get_instance();
        $wallet_bs= $ci->wallet_model->where(array('user_id' => $userid))->find_all();
		$actual_wallet_balance   = (!empty($wallet_bs)) ? $wallet_bs[0]->amt : 0;
		return $actual_wallet_balance;
    }
	
    /*
    * @Author      : Santosh Warang
    * @function    : checkUserAlreadyLoggedIn()
    * @param       : $sessionid,$userid
    * @detail      : Logout user if he is already logged in any other browser or any other place.
    *                 
    */ 
 
    function checkUserAlreadyLoggedIn($sessionid,$userid) 
    {   
	    $ci = & get_instance();
		if($userid){
        $Session_ID= $ci->common_model->get_value('users','session_id','id='.$userid);
		if($Session_ID->session_id == $sessionid || $Session_ID == NULL ||  $Session_ID == ''){
		$Flag = 0;	
		}else{
		$Flag = 1;	
		}
		return $Flag;
		}
    }
	
	/*
    * @Author      : Santosh Warang
    * @function    : convertToArray()
    * @param       : $str,$delimiter
    * @detail      : Conver String To Array.
    *                 
    */ 
 
    function convertToArray($str, $delimiter){
        $str =  str_ireplace($delimiter, '&', $str);
        parse_str($str, $data);
        return $data;$returnval = ($per / 100) * $total;
    }

if ( ! function_exists('getPercentageAmount'))
{
    function getPercentageAmount($per,$total)
    {
         $returnval = ($per / 100) * $total;
         return round($returnval,2); 
    }
}

if ( ! function_exists('getGstTdsAmount'))
{
    function getGstTdsAmount($perAmt)
    {
        $valGst = ($perAmt / 1.18) * (GST_APPLICABLE_MONTHLY_COMMISSION / 100);
        $minusGst = $perAmt - $valGst;
        $valTds = getPercentageAmount($minusGst,TDS_APPLICABLE_MONTHLY_COMMISSION);
        $returnval = $minusGst - $valTds;
        return $returnval; 
    }
}


if ( ! function_exists('getDataFromMsrtc'))
{
    function getDataFromMsrtc($url,$fields = "" , $auth=1) 
    {
        $CI             = &get_instance();
        $fieldsData     = "";
        
        if(is_array($fields) && count($fields) >0)
        {
            $fieldsData = http_build_query($fields);
        }
    
        $result=array();
        $ch = curl_init();      
        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch,CURLOPT_TIMEOUT, 30);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, 30);
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-API-KEY:abcdefghijklmn'));

        if($auth == 1){         
            curl_setopt($ch, CURLOPT_USERPWD, "superadmin:BoS@S123");
            // curl_setopt($ch, CURLOPT_USERPWD, "admin:password");
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        }
        
        curl_setopt($ch,CURLOPT_URL, $url);
        // curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fieldsData);
    
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: ' . $_SERVER['HTTP_USER_AGENT']));
        $result['response']         = curl_exec($ch);
        $result['info']             = curl_getinfo ($ch);
        $result['info']['errno']    = curl_errno($ch);
        $result['info']['errmsg']   = curl_error($ch);
        
        return $result['response'];
    }
}

if (!function_exists('sendDataOverGet')) {
    function sendDataOverGet($url, $wait = 90)
    {
        $CI = &get_instance();
        $fieldsData = "";

        $url = str_replace(" ", "%20", $url);

        
        $result = array();
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);   
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
       
        $result['response'] = curl_exec($ch);
        $result['info'] = curl_getinfo($ch);
        $result['info']['errno'] = curl_errno($ch);
        $result['info']['errmsg'] = curl_error($ch);

        // show($url);
        // show($result,1);
        return $result['response'];
    }
}


/* End of file date_helper.php */
/* Location: ./system/helpers/date_helper.php */
