<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!function_exists('curlForPostData_HOTEL')) {

    function curlForPostData_HOTEL($curl_url, $input,$headers) {
     $api_headers = array(
                'x-api-key:'. $headers['xApiKey'] 
            );
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $curl_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50);
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $input);
        curl_setopt($ch, CURLOPT_USERPWD, $headers['username'] . ":" . $headers['password']);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $api_headers);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        // curl_setopt($ch, CURLOPT_STDERR, $out );
        $httpCode = curl_getinfo($ch , CURLINFO_HTTP_CODE); // this results 0 every time
        $response = curl_exec($ch);
        if ($response === false)
        $response = curl_error($ch);
        return $response;
    }

}

if (!function_exists('curlForPostData_BOS_Hotel')) {

    function curlForPostData_BOS_Hotel($curl_url,$input,$headers) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $curl_url);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $input);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_USERPWD, $headers['username'] . ":" . $headers['password']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);
       //curl_setopt($ch, CURLOPT_SSLVERSION, 6);
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            $error_msg = curl_error($ch);
            return $error_msg;
        } 
        curl_close($ch); 
        return $response;
    }

}

if (!function_exists('timeDiff')) {
    function timeDiff($date1, $date2) {
        $date_a = new DateTime($date1);
        $date_b = new DateTime($date2);

        $interval = date_diff($date_a,$date_b);
        //show($interval) ;
        return $interval->format('%d');
    }
}