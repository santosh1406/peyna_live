<?php

if (!function_exists('curlForPostDataTSO')) {

    function curlForPostDataTSO($curl_url, $input, $sessionData, $header) {
        // show($sessionData['password'], 1);
	
	// ob_start();  
        // $out = fopen('php://temp', 'w+');
        
	$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $curl_url);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $input);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSLVERSION, 6);
	// curl_setopt($ch, CURLOPT_VERBOSE, true);
        // curl_setopt($ch, CURLOPT_STDERR, $out );

        // curl_setopt($ch, CURLOPT_USERPWD, $sessionData['username'] . ":Trimax@123" . $sessionData['password']);
        curl_setopt($ch, CURLOPT_USERPWD, "7878787801:Trimax@123");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $response = curl_exec($ch);
        $result['info'] = curl_getinfo($ch);
        $result['info']['errorno'] = curl_errno($ch);
        $result['info']['errormsg'] = curl_error($ch);
	// show($result, 1);
	if (curl_error($ch)) {
            $error_msg = curl_error($ch);
            print_r($error_msg);die;
            // print_r($response);die;
        }
        curl_close($ch);
	   // show($response, 1);
	// rewind($out);
        //$verboseLog = stream_get_contents($out);
        // echo "<br/>Verbose information:\n<pre>", htmlspecialchars($verboseLog), "</pre>\n";
	//show($response, 1);
        return $response;
    }

}

if (!function_exists('recharge_with_TSO')) {

    function recharge_with_TSO($input) {
        
        $creds = json_decode(TSO_CREDS, true);
        $soapUrl = $creds['soapUrl'];
        $soapUser = $creds['soapUser']; //API User ID
        $soapPassword = $creds['soapPassword']; //API Password
        $mobileno = $creds['mobileno']; //Registered Mobile No
        $smspin = $creds['smspin']; //SMS PIN

        $customerno = $input['mobile_number'];
        $amount = $input['recharge_amount'];
        $stv = $input['recharge_type'];
        $recharge_id = mt_rand(100000, 999999); //Your unique transaction no (Numeric)

        $is_postpaid = $input['plan_type'] == 'Prepaid Mobile' ? 'N' : 'Y';
        //show($is_postpaid, 1);

        if ($is_postpaid == 'N') {
            $operatorcode = $input['pre_operator'];
            $data = '<MRREQ><REQTYPE>MRCH</REQTYPE><UID>' . $mobileno . '</UID><PWD>' . $smspin . '</PWD><OPCODE>' . $operatorcode . '</OPCODE><CMOBNO>' . $customerno . '</CMOBNO><AMT>' . $amount . '</AMT><STV>' . $stv . '</STV><TRNREFNO>' . $recharge_id . '</TRNREFNO></MRREQ>';
            $raw_data = htmlentities($data);
            $xml_post_string = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Header><APIAuthHeader xmlns="http://tempuri.org/"><UserID>' . $soapUser . '</UserID><Password>' . $soapPassword . '</Password></APIAuthHeader></soap:Header><soap:Body><MobileRecharge xmlns="http://tempuri.org/"><sRequest>' . $raw_data . '</sRequest></MobileRecharge></soap:Body></soap:Envelope>';

            $headers = array(
                "Content-type: text/xml;charset=\"utf-8\"",
                "Accept: text/xml",
                "Cache-Control: no-cache",
                "Pragma: no-cache",
                "SOAPAction: http://tempuri.org/MobileRecharge",
                "Content-length: " . strlen($xml_post_string),
                "UserID: " . $soapUser,
                "Password: " . $soapPassword,
                "Host: www.theservicesonline.in",
                "POST: /mRechargeAPI/Service.asmx HTTP/1.1",
            );
        } else {
            $operatorcode = $input['post_operator'];
            $data = '<MRREQ><REQTYPE>PPB</REQTYPE><UID>' . $mobileno . '</UID><PWD>' . $smspin . '</PWD><OPCODE>' . $operatorcode . '</OPCODE><CMOBNO>' . $customerno . '</CMOBNO><AMT>' . $amount . '</AMT><TRNREFNO>' . $recharge_id . '</TRNREFNO></MRREQ>';
            $raw_data = htmlentities($data);
            $xml_post_string = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Header><APIAuthHeader xmlns="http://tempuri.org/"><UserID>' . $soapUser . '</UserID><Password>' . $soapPassword . '</Password></APIAuthHeader></soap:Header><soap:Body><PostPaidBillPay xmlns="http://tempuri.org/"><sRequest>' . $raw_data . '</sRequest></PostPaidBillPay></soap:Body></soap:Envelope>';

            $headers = array(
                "Content-type: text/xml;charset=\"utf-8\"",
                "Accept: text/xml",
                "Cache-Control: no-cache",
                "Pragma: no-cache",
                "SOAPAction: http://tempuri.org/PostPaidBillPay",
                "Content-length: " . strlen($xml_post_string),
                "UserID: " . $soapUser,
                "Password: " . $soapPassword,
                "Host: www.theservicesonline.in",
                "POST: /mRechargeAPI/Service.asmx HTTP/1.1",
            );
        }

        $url = $soapUrl;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser . ":" . $soapPassword);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); //SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);
        
        //$response = '<MRRESP><RESPTYPE>MRCH</RESPTYPE><STCODE>6</STCODE><STMSG>Trn No: 5121378, Cust No: 9967244431, Amt: 10.00 is In Queue</STMSG><TRNID>5121378</TRNID><CUSTNO>9967244431</CUSTNO><AMT>10.00</AMT><TRNSTATUS>0</TRNSTATUS><STATUSTEXT>Pending</STATUSTEXT><TRNREFNO>230407</TRNREFNO><OPRID></OPRID><BAL>430.98</BAL></MRRESP>';           
        //$p1 = strpos($response_str, 'MRRESP');
        //$p2 = strpos($response_str, '/MRRESP');
        //$p2 = $p2+11;        
        //$response_str = substr($response_str, $p1, $p2 - $p1);         
        //$response_str = '&lt;'.$response_str;

        // show($response, 0, 'api response');
        $response = str_replace('9819578112578112', '', $response);
        $res = xmlResponseToArray($response);
        // show($res, 1);
        if ($is_postpaid == 'N') { 
            $response = $res['soapBody']['MobileRechargeResponse']['MobileRechargeResult'];
        } else {
            $response = $res['soapBody']['PostPaidBillPayResponse']['PostPaidBillPayResult'];
        }
        
        // $result['info'] = curl_getinfo($ch);
        // $result['info']['errorno'] = curl_errno($ch);
        // $result['info']['errormsg'] = curl_error($ch);
	// show($response, 1);
        curl_close($ch);
        return $response;
    }

}


// if (!function_exists('xmlResponseToArray')) {

//     function xmlResponseToArray($response) {
// 	// show($response,1);
//         // replace soap:Envelope, soap:Header, soap:Body with soapEnvelope, soapHeader, soapBody
//         $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $response);
//         // convert all html entities to tag
//         $response = html_entity_decode($response);
//         // show($response, 1);
//         // $xml = new SimpleXMLElement($response);
//         // $json = json_decode($response);
//         // show($json, 1);
//         $responseArray = json_decode($response, true);
//         return $responseArray;
//     }

// }

if (!function_exists('TransStatus')) {

    function TransStatus($input) {

        //show($input,1);
        $creds = json_decode(TSO_CREDS, true);
        $soapUrl = $creds['soapUrl'];
        $soapUser = $creds['soapUser']; //API User ID
        $soapPassword = $creds['soapPassword']; //API Password
        $mobileno = $creds['mobileno']; //Registered Mobile No
        $smspin = $creds['smspin']; //SMS PIN
        $trnsno = $input['trans_no'];
        $custno = $input['mobile_no'];
        
        //$operatorcode = $input['pre_operator'];
        $data = '<MRREQ><REQTYPE>TRNST</REQTYPE><UID>' . $mobileno . '</UID><PWD>' . $smspin . '</PWD><TRNNO>' . $trnsno . '</TRNNO><CNO>' . $custno . '</CNO></MRREQ>';
        $raw_data = htmlentities($data);
        $xml_post_string = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Header><APIAuthHeader xmlns="http://tempuri.org/"><UserID>' . $soapUser . '</UserID><Password>' . $soapPassword . '</Password></APIAuthHeader></soap:Header><soap:Body><GetTransactionStatus xmlns="http://tempuri.org/"><sRequest>' . $raw_data . '</sRequest></GetTransactionStatus></soap:Body></soap:Envelope>';

        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "SOAPAction: http://tempuri.org/GetTransactionStatus",
            "Content-length: " . strlen($xml_post_string),
            "UserID: " . $soapUser,
            "Password: " . $soapPassword,
            "Host: www.theservicesonline.in",
            "POST: /mRechargeAPI/Service.asmx HTTP/1.1",
        );

        $url = $soapUrl;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $soapUser . ":" . $soapPassword);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); //SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);

        $res = xmlResponseToArray($response);
        
        $response = $res['soapBody']['GetTransactionStatusResponse']['GetTransactionStatusResult'];
        
        $result['info'] = curl_getinfo($ch);
        $result['info']['errorno'] = curl_errno($ch);
        $result['info']['errormsg'] = curl_error($ch);

        curl_close($ch);
        return $response;
    }
}

if (!function_exists('getTsoPostpaidOperators')) {

    function getTsoPostpaidOperators($operator){
        switch ($operator) {
            case 'VB':
                return 'Vodafone';
                break;
            case 'BB':
                return 'BSNL';
                break;
            case 'IB':
                return 'Idea';
                break;
            case 'AB':
                return 'Airtel';
                break;
            case 'RB':
                return 'Reliance';
                break;
            case 'TB':
                return 'Tata';
                break;
            default:
                # code...
                break;
        }
    }
}

if (!function_exists('getTsoPrepaidOperators')) {

    function getTsoPrepaidOperators($operator){
        switch ($operator) {
            case 'A':
                return 'AIRTEL';
                break;
            case 'B':
                return 'BSNL';
                break;
            case 'I':
                return 'Idea';
                break;
	   case 'V':
                return 'Vodafone';
                break;
            case 'RG':
            case 'RB':
                return 'Reliance';
                break;
            case 'TI':
                return 'TataIndicom';
                break;
            case 'TD':
                return 'TataDocomo';
                break;
            case 'AI':
                return 'Aircel';
                break;
            case 'VI':
                return 'Videocon';
                break;
            case 'JIO':
                return 'Jio';
                break;
            default:
                # code...
                break;
        }
    }
}

?>
