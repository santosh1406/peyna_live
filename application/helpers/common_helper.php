<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('show')) {
    function show($var, $flag = 0, $msg = '') {
        $CI = &get_instance();

//        if (in_array($CI->input->ip_address(), $CI->config->item('developer_ip'))) {


            echo "<pre> <b>------------------------  START $msg  ------------------------   </b> <br/> <br/>";
            if (is_array($var) || is_object($var)) {
                print_r($var);
            } else {
                echo $var;
            }

            echo " <br/>   <br/>  <b>------------------------  END $msg  ------------------------   </b></pre>";

            if ($flag > 0)
                exit;
//        }
    }
}

if ( ! function_exists('get_user')) {
    function get_user($var = 'id') {
        $CI =& get_instance(); 
        $sessionValues = $CI->session->userdata('loggedInUser');
        $sessionValues['full_name'] = $sessionValues['first_name']." ".$sessionValues['last_name'];
        return (isset($sessionValues[$var])) ? $sessionValues[$var] : false;
    }   
}

if (!function_exists('last_query')) {
    function last_query($flag = 0) {
        $CI = &get_instance();
        show($CI->db->last_query(),1);
        //return $CI->db->last_query()
    }
}
 
if (!function_exists('data2json')) {
    function data2json($data) {
        $CI = &get_instance();
        $CI->output->set_content_type('application/json')->set_output(json_encode($data));
    }
}

if (!function_exists('sendDataOverPost')) {
    function sendDataOverPost($url, $header ='', $fields = "", $auth = 0, $wait = 90, $ccasFlag = 0) {
        $CI = &get_instance();

        $url = str_replace(" ", "%20", $url);        

        $result = array();
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_TIMEOUT, $wait);
        // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $wait);
        
        if($header){
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }

        if ($auth == 1) {
            curl_setopt($ch, CURLOPT_USERPWD, "appsperts:appsperts@123");
            // curl_setopt($ch, CURLOPT_USERPWD, "ameyav:password");
            // curl_setopt($ch, CURLOPT_USERPWD, "admin:password");
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        // curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        
        $result['response'] = curl_exec($ch);
        $result['info'] = curl_getinfo($ch);
        $result['info']['errno'] = curl_errno($ch);
        $result['info']['errmsg'] = curl_error($ch);
        $CI->load->model('Common_api_model');
        $header = json_encode($header);
        $response = $result['response'];
        
        if($ccasFlag == 1){
            $fields = $fields;
        }else{
            $fields = json_encode($fields);
        }
        $CI->Common_api_model->insertRequestData($url, $header, $fields, $response);
        return $result['response'];
    }
}

if (!function_exists('calculateAge')) {
    function calculateAge($dob) {
		$datetime1 = new DateTime($dob);
		$datetime2 = new DateTime(TODAY_DATE);
		$interval = $datetime1->diff($datetime2);
		$age = $interval->format('%y');
        return $age;
    }
}

if (!function_exists('getCardType')) {
    function getCardType($age, $cardType) {
        if($age >= AGE_LIMIT_CARDTYPE_CC) {
            $cardType = $cardType;
        } else {
            $cardType = CARDTYPE_U18_CC;
        }
        return $cardType;
    }
}

if (!function_exists('getCcasApiHeader')) {
    function getCcasApiHeader() {
        $header = array(
                "Content-Type:application/json",
                "Authorization:token 67b3889e211c607d39b8c3edb3c66c70adfe3d46"
//                "Authorization:token f4fc432bcad842da3259107e764473b55d711f06"
            );
        return $header;
    }
}

if (!function_exists('getCurrentFinancialYear')) {
    function getCurrentFinancialYear() {
        if (date('m') >= 4) { //april
            $year = date('Y') + 1;
        } else {
            $year = date('Y');
        }
        return $year;
    }
}

if (!function_exists('getDateFormat')) {
    function getDateFormat($cur_format,$date,$req_format) {
        $date = DateTime::createFromFormat($cur_format, $date);
		$new_format = $date->format($req_format);
		return $new_format;
    }
}

if (!function_exists('m_round_calc')) {
    function m_round_calc($amt,$roundingFactor) {
        $m_round_fare = round($amt/$roundingFactor)*$roundingFactor;
        return $m_round_fare;
    }
}

if (!function_exists('getIndianCurrency')) {
    function getIndianCurrency($number)
    {
        $decimal = round($number - ($no = floor($number)), 2) * 100;
        $hundred = null;
        $digits_length = strlen($no);
        $i = 0;
        $str = array();
        $words = array(0 => '', 1 => 'one', 2 => 'two',
            3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
            7 => 'seven', 8 => 'eight', 9 => 'nine',
            10 => 'ten', 11 => 'eleven', 12 => 'twelve',
            13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
            16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
            19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
            40 => 'forty', 50 => 'fifty', 60 => 'sixty',
            70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
        $digits = array('', 'hundred','thousand','lakh', 'crore');
        while( $i < $digits_length ) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += $divider == 10 ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
            } else $str[] = null;
        }
        $Rupees = implode('', array_reverse($str));
        $paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
        return ($Rupees ? $Rupees . 'Rupees ' : '') . $paise ;
    }
}
