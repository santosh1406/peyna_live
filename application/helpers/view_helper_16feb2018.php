<?php

if (!function_exists('create_pagination')) {

    function create_pagination($config = array()) {
        $CI = & get_instance();
        $CI->load->library('pagination');

        $CI->pagination->initialize($config);

        return $CI->pagination->create_links();
    }

}


if (!function_exists('enable_profiler')) {

    function enable_profiler() {

        $CI = & get_instance();
        if(in_array($CI->input->ip_address(), $CI->config->item('developer_ip') ) && strtolower(ENVIRONMENT) != 'production' )
        {
            $true_false = is_ajax();
            $CI->output->enable_profiler(!$true_false);
        }
    }
}

/*
 * @function    : load_front_view
 * @param       : $view, $data
 * @detail      : To load the front view.
 *                $view     -> name fo the view file
 *                $data     -> data for the view file
 */

if (!function_exists('load_front_view')) 
{
    function load_front_view($view, $data = array()) {
        $CI = & get_instance();

        /*$CI->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $CI->output->set_header("Pragma: no-cache");*/
        
        $CI->output->set_header("HTTP/1.0 200 OK");
        $CI->output->set_header("HTTP/1.1 200 OK");
        $CI->output->set_header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
        $CI->output->set_header('Last-Modified: '.gmdate('D, d M Y H:i:s', time()).' GMT');
        $CI->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $CI->output->set_header("Cache-Control: post-check=0, pre-check=0");
        $CI->output->set_header("Pragma: no-cache");
           
        $CI->load->view(FRONT_LAYOUTS . 'header',$data);
        $CI->load->view($view, $data);
        $CI->load->view(FRONT_LAYOUTS . 'footer');
    }

}


/*
 * @function    : load_back_view
 * @param       : $view, $data
 * @detail      : To load the back end view.
 *                $view     -> name fo the view file
 *                $data     -> data for the view file
 */
if (!function_exists('load_back_view')) {

    function load_back_view($view, $data = array()) {
        $CI = & get_instance();

        $CI->load->view(BACK_LAYOUTS . 'header');

        $view = str_replace(BACK_VIEW, '', $view);

        $CI->load->view(BACK_VIEW.$view, $data);
        $CI->load->view(BACK_LAYOUTS . 'footer');
    }

}


/*
 * @function    : get_tags
 * @param       : $file_name, $sub_folder_path, $tags_type, $tag_deteails
 * @detail      : To load the back end view.
 *                $file_name            => name of Script files,
 *                $sub_folder_path      => path to script folder,
 *                $tag_type             => tag type eg. 'Script'.
 */

if (!function_exists('get_tags')) {

    function get_tags($file_name = '', $sub_folder_path = '', $tags_type = "script", $tags_details = array()) {
        $filepath = '';
        $tags_details_string = '';


        $filepath .= $file_name;

        $filepath .= $sub_folder_path != '' ? "/" . $sub_folder_path : '';

        if (count($tags_details) > 0) {
            foreach ($tags_details as $key => $value) {
                $tags_details_string .= $key . "='" . $value . "' ";
            }
        }


        switch ($tags_type) {
            case 'image':
                $tag = '<img src="' . $filepath . '" ' . $tags_details_string . '/>';
                break;

            case 'script':
                $tag = '<script type="text/javascript" src="' . base_url() . "/render/" . $filepath . '" ' . $tags_details_string . '></script>';
                break;

            case 'css':
                $tag = '<link rel="stylesheet" type="text/css" href="' . $filepath . '" ' . $tags_details_string . '/>';
                break;

            default:
                $tag = $filepath;
        }
        return $tag;
    }

}


/*
 * @function    : file_read
 * @param       : $filepath
 * @detail      : To read file.
 *                $filepath -> path of the file
 */
if (!function_exists('file_read')) {

    function file_read($filepath) {
        $CI = &get_instance();
        //$CI->load->helper('file');

        return read_file($filepath);
    }

}

/*
 * @function    : file_write
 * @param       : $filepath, $data
 * @detail      : To write file.
 *                $filepath -> path of the file
 *                $data -> data to write in file
 */
if (!function_exists('file_write')) {

    function file_write($filepath, $data) {
        $CI = &get_instance();
        //$CI->load->helper('file');

        return write_file($filepath, $data, "a+");
    }

}


/*
 * @function    : get_back_menu
 * @param       : $parent, $menu
 * @detail      : To load and create the dynamic admin menu.
 *                $menu     -> all menu in array format (root,child,subchild)
 *                $parent   -> parent menu position
 */
if (!function_exists('get_back_menu')) {

    function get_back_menu($parent = 0) {
        $CI = &get_instance();

        return get_menu();

        $user_id = $CI->session->userdata("user_id");
        $role_name = strtolower($CI->session->userdata("role_name"));

        $menu = json_decode(file_read(TMP_FOLDER . "/" . $user_id . "_menu.txt"), TRUE);
        $user_menu_permission = json_decode(file_read(TMP_FOLDER . "/" . $user_id . "_menu_permission.txt"), TRUE);
        // echo'<pre>';print_r($user_menu_permission);die('check');
        $html = "";
        if (isset($menu['parents'][$parent])) {
            //$html .= "<ul>";
            foreach ($menu['parents'][$parent] as $itemId) {

                $icon_class = "";
                if ($role_name != "super admin") {
                    if (isset($user_menu_permission[$itemId]) and $user_menu_permission[$itemId]["view"] != "") {

                        if ($user_menu_permission[$itemId]["view"] == "N") {
                            continue;
                        }
                    } else {
                        continue;
                    }
                }

                /*
                 * this is for menu icons.
                 * in menu["parents"] array 0 contain all root menu id.
                 */
                if (in_array($itemId, $menu['parents'][0])) {
                    $icon_class = "<i class='fa ".$menu['items'][$itemId]['class']."'></i>";
                } else if (!array_key_exists($itemId, $menu['parents'])) {
                    $icon_class = "<i class='fa fa-angle-double-right'></i>";
                }
                /******************menu icons end *****************************/
                
                if (!isset($menu['parents'][$itemId])) {
                    $html .= "<li><a href='" . base_url() . $menu['items'][$itemId]['link'] . "'>" . $icon_class . "<span>" . ucwords($menu['items'][$itemId]['display_name']) . "</span></a></li>";
                }

                if (isset($menu['parents'][$itemId])) {
                    /* $html .= "
                      <li class='treeview'>
                      <a href='" . base_url() . $menu['items'][$itemId]['link'] . "'>" .
                      ucwords($menu['items'][$itemId]['display_name']) . "<i class='fa fa-angle-left pull-right'></i>" .
                      "</a>"; */

                    $html .= "
                    <li class='treeview'>
                        <a href='#'>" . $icon_class .
                            "<span>" . ucwords($menu['items'][$itemId]['display_name']) . "</span><i class='fa fa-angle-left pull-right'></i>" .
                            "</a>";
                    $html .= "<ul class='treeview-menu'>";
                    $html .= get_back_menu($itemId);
                    $html .= "</ul>";
                    $html .= "</li>";
                }
            }
            // $html .= "</ul>";
        }
        return $html;
    }

}


/*
 * @function    : generate_menu_file
 * @param       : $user_id, $role_id
 * @detail      : to create file for menu,link and permission.
 *                $user_id -> id of user.
 *                $role_id -> role id of the user.
 */
if (!function_exists('generate_menu_file')) {

    function generate_menu_file($user_id, $role_id) {

        $CI = &get_instance();
        $permission = "";// check if error comes
        if ($user_id != "" and $role_id != "") {
            $CI->load->model('access_permission_model');
            $permission_array = $CI->access_permission_model->get_menu_permission($role_id);

            foreach ($permission_array as $key => $value) {
                $permission[$value["id"]] = $value;
            }

            $CI->load->model('menu_model');
            $acl_menu_array = $CI->menu_model->get_menu();

            $menu = array('items' => array(), 'parents' => array());
            // Builds the array lists with data from the menu table
            foreach ($acl_menu_array as $key => $value) {
                // Creates entry into items array with current menu item id ie. $menu['items'][1]
                $menu['items'][$value['id']] = $value;
                $menu['parents'][$value['parent']][] = $value['id'];

                if ($value['link'] != "") {
                    $menu_link[$value['link']] = $value['id'];
                }
            }

            file_write(TMP_FOLDER . "/" . $user_id . "_menu_permission.txt", json_encode($permission)); //user menu permission array
            file_write(TMP_FOLDER . "/" . $user_id . "_menu.txt", json_encode($menu)); //user menu array
            file_write(TMP_FOLDER . "/" . $user_id . "_menu_link.txt", json_encode($menu_link)); //user menu link array
        }
    }

}


/*
 * @function    : check_usermenu_files
 * @param       : $user_id
 * @detail      : to check menu,link and permission file exists.
 *                $user_id -> id of user.
 *               
 */
if (!function_exists('check_usermenu_files')) {

    function check_usermenu_files($user_id) {

        return true;
        $file_menu_permission = file_exists(TMP_FOLDER . "/" . $user_id . "_menu_permission.txt");
        $file_menu = file_exists(TMP_FOLDER . "/" . $user_id . "_menu.txt");
        $file_menu_link = file_exists(TMP_FOLDER . "/" . $user_id . "_menu_link.txt");

        if ($file_menu_permission and $file_menu and $file_menu_link) {
            return true;
        } else {
            return false;
        }
    }

}

/*
 * @function    : is_logged_in
 * @param       : 
 * @detail      : To check if user is logged in and all three file created 
 *                and exists on server.
 *               
 */
if (!function_exists('is_logged_in'))
{
    function is_logged_in()
    {
        $CI = &get_instance();
        if (!$CI->session->userdata("user_id") && $CI->session->userdata("user_id") == "")
        {
            redirect(base_url());
        }
        else {
            return true;
        }
    }
}


/*
 * @function    : is_system_users
 * @param       : 
 * @detail      : To check if user is logged in & It is a system user i.e. Superadmin,Admin,Report 
 *               
 */
if (!function_exists('is_system_users'))
{
    function is_system_users($custom_role = array())
    {
        is_logged_in();
        eval(SYSTEM_USERS_ARRAY);
        eval(OFFICE_USERS_ROLL_ARRAY);

        $user_role_name = ""; 
        $CI = &get_instance();
        $user_role_name = $CI->session->userdata("role_name");
        

        if($user_role_name == "" || (!in_array(strtolower($user_role_name), $system_users_array)) && !in_array(strtolower($user_role_name), $office_users_roll_array))
        {
            if(!empty($custom_role))
            {
                if(!in_array($user_role_name, $custom_role))
                {
                   redirect(base_url());
                }
            }
            else
            {
                redirect(base_url());
            }
            
        }
    }
}


/*
 * @function    : get_child_menus
 * @param       : 
 * @detail      : To get child menu.
 *               
 */
if (!function_exists('get_child_menus')) {

    function get_child_menus($parent = 0, $menu, $permission) {
        $CI = &get_instance();
        $html = "";
        if (isset($menu['parents'][$parent])) {
            foreach ($menu['parents'][$parent] as $itemId) {
                $display_none = "";
                $parent_category = "";
                $view_checked = "";
                $add_checked = "";
                $edit_checked = "";
                $delete_checked = "";
                $export_checked = "";
                $approve_checked="";
                $pdf_checked ="";
                if ($parent != 0) {
                    $parent_category = "parent-category='" . $parent . "'";
                    $display_none = "style='display:none;'";
                    // to check if parent has view permission then show child

                    if (isset($permission[$parent]["view"]) and $permission[$parent]["view"] == "Y") {
                        $display_none = "style='display:block;'";
                    }
                }

                // to check menu permissions
                if (isset($permission[$itemId]["view"]) and $permission[$itemId]["view"] == "Y") {
                    $display_none = "style='display:block;'";
                    $view_checked = "checked";
                    if (isset($permission[$itemId]["add"]) and $permission[$itemId]["add"] == "Y") {
                        $add_checked = "checked";
                    }

                    if (isset($permission[$itemId]["edit"]) and $permission[$itemId]["edit"] == "Y") {
                        $edit_checked = "checked";
                    }

                    if (isset($permission[$itemId]["delete"]) and $permission[$itemId]["delete"] == "Y") {
                        $delete_checked = "checked";
                    }
                     if (isset($permission[$itemId]["export"]) and $permission[$itemId]["export"] == "Y") {
                        $export_checked = "checked";
                    }
                     if (isset($permission[$itemId]["pdf"]) and $permission[$itemId]["pdf"] == "Y") {
                        $export_checked = "checked";
                    }
                     if (isset($permission[$itemId]["approve"]) and $permission[$itemId]["approve"] == "Y") {
                        $export_checked = "checked";
                    }
                } /* else if (isset($permission[$itemId]["view"]) and $permission[$itemId]["view"] == "N") {
                  //$display_none = "style='display:none;'";
                  } */

                if (!isset($menu['parents'][$itemId])) {
                    $html .= "<ul><li " . $display_none . " " . $parent_category . " data-category='" . $itemId . "' >";
                    //$html .= "<label style='width:30%;background: white' class='col-md-3'>"."<i class='fa fa-angle-right' style='margin-left:44px'>"."&nbsp;"."</i>"."&nbsp;". ucwords($menu['items'][$itemId]["display_name"]). "</label>";
                    $html .= "<div class='row'>"; 
                    $html .= "<div style='width:40%;background: white;' class='col-md-4'>"."<i class='fa fa-angle-right' style='margin-left:44px'>"."&nbsp;</i>".'<b>'. ucwords($menu['items'][$itemId]["display_name"]) .'&nbsp;&nbsp;'."</div>";
                    $html .= "<div style='margin-left: 40%;margin-right: -4%'>";
                    $html .= "<input type='checkbox' name='view' class='parent-check sub_menu_view_".$parent."' " . $view_checked . "/>";
                    $html .= "<input type='checkbox' name='add' " . $add_checked . " class='parent-check sub_menu_add_".$parent."'".$view_checked ."'/>";
                    $html .= "<input type='checkbox' name='edit' " . $edit_checked . " class='parent-check sub_menu_edit_".$parent."'".$view_checked ."''/>";
                    $html .= "<input type='checkbox' name='delete' " . $delete_checked . " class='parent-check sub_menu_delete_".$parent."'".$view_checked ."''/>";
                    $html .= "<input type='checkbox' name='export' " . $export_checked . " class='parent-check sub_menu_export_".$parent."'".$view_checked ."''/>";
                    $html .= "<input type='checkbox' name='pdf' " . $pdf_checked . " class='parent-check sub_menu_pdf_".$parent."'".$view_checked ."''/>";
                    $html .= "<input type='checkbox' name='approve' " . $approve_checked . " class='parent-check sub_menu_pdf_".$parent."'".$view_checked ."''/>";
                    $html .= "</div>";
                    $html .= "</div>";
                    $html .= "</li></ul>";
                }

                if (isset($menu['parents'][$itemId])) {
                    $html .= "<ul>";
                    $html .= "<li " . $display_none . " " . $parent_category . " data-category='" . $itemId . "'>";
                    $html .= "<div class='row'>";
                    $html .= "<div style='width:40%;background: wheat;cursor:pointer;' class='col-md-4 show_sub'>"."<input type='checkbox' class='all_per' name='parent_per'></input>&nbsp;&nbsp;".'<b>'. ucwords($menu['items'][$itemId]["display_name"]) .'&nbsp;&nbsp;'.'<b class="fa fa-angle-down"></b>'.'</b>'."</div>";
                    $html .= "<div style='margin-left: 40%;margin-right: -4%'>";
                    $html .= "<input type='checkbox' name='view' class='parent-check menu_view_".$itemId." ' " . $view_checked . " data-parent-id=".$itemId." />" ;
                    $html .= "<input type='checkbox' name='add' " . $add_checked . " class='parent-check menu_add_".$itemId."'/>";
                    $html .= "<input type='checkbox' name='edit' " . $edit_checked . " class='parent-check menu_edit_".$itemId."'/>";
                    $html .= "<input type='checkbox' name='delete' " . $delete_checked . " class='parent-check menu_delete_".$itemId."'/>";
                    $html .= "<input type='checkbox' name='export' " . $export_checked . " class='parent-check menu_export_".$itemId."'/>";
                    $html .= "<input type='checkbox' name='pdf' " . $pdf_checked .       "class='parent-check menu_pdf_".$itemId."'/>";
                    $html .= "<input type='checkbox' name='approve' " . $approve_checked . "class='parent-check menu_pdf_".$itemId."'/>";
                    $html .= "</div>";
                    $html .= "</div>";
                    $html .= get_child_menus($itemId, $menu, $permission);
                    $html .= "</li>";
                    $html .= "</ul>";
                }
            }
        }
        return $html;
    }

}



/*
 * @Author      : Suraj Rathod
 * @Function    : getMailTemplate
 * @Description : function will mail subject and mail body
 * @param       : $mailtype -> type of mail
 */
if (!function_exists('getMailTemplate')) {

    function getMailTemplate($mailtype) {
        $CI = &get_instance();
        $CI->load->model('mail_template_model');
        $CI->mail_template_model->title = $mailtype;
        $mail_data = $CI->mail_template_model->select();
        return $mail_data;
    }

}



/*
 * @Author      : Suraj Rathod
 * @Function    : remember_login_check
 * @Description : check if login is remembered
 * @param       : 
 */
if (!function_exists('remember_login_check')) {

    function remember_login_check() {
        $CI = &get_instance();
        if(!$CI->session->userdata('user_id') && $CI->input->cookie("remember_me") != "")
        {
            $CI->load->model(array('users_model', 'role_model'));
            $CI->users_model->remember_me = $CI->input->cookie("remember_me");
            $users_array = $CI->users_model->select();
            $usersarray = $users_array[0];
            generate_menu_file($usersarray->user_id, $usersarray->role_id);
            //if user already exists then set session
            $CI->role_model->role_id = $usersarray->role_id;
            $roledata = $CI->role_model->select();

            if (check_usermenu_files($usersarray->user_id)) {
                $user_data = array(
                    "id" => $usersarray->id,
                    "user_id" => $usersarray->user_id,
                    "role_id" => $usersarray->role_id,
                    "role_name" => $roledata[0]->role_name,
                    "username" => $usersarray->username,
                    "display_name" => $usersarray->display_name
                );

                $CI->session->set_userdata($user_data);

                //redirect("admin/users/profile/" . $usersarray->id);
            }
        }
    }

}


/*
 * @Author      : Suraj Rathod
 * @Function    : users_action_log
 * @Description : enter user action in table
 * @param       : $array -> array to insert in database
 */
if (!function_exists('users_action_log')) {

    function users_action_log($action_log_array) {
        $CI = &get_instance();
        //insert into user action log
        $CI->users_action_log_model->user_id = $action_log_array["user_id"];
        $CI->users_action_log_model->url = $action_log_array["url"];
        $CI->users_action_log_model->action = $action_log_array["action"];
        $CI->users_action_log_model->message = $action_log_array["message"];
        $CI->users_action_log_model->name = $action_log_array["name"];
        $CI->users_action_log_model->ip_address = $CI->input->ip_address();
        $CI->users_action_log_model->user_agent = $CI->session->userdata('user_agent');
        $CI->users_action_log_model->save();
    }
}



/*
 * @Author      : Suraj Rathod
 * @Function    : getSMS
 * @Description : This Function is temparary. This must be changed after discussion
 * @param       : 
 */
if (!function_exists('getSMS')) {

    function getSMS($sms_name) {

        $CI = &get_instance();
        $CI->load->model(array('sms_template_model'));
        $msg = $CI->sms_template_model->where("sms_template_name",$sms_name)->find_all();
        return $msg;
    }
}


/*
 * @Author      : Suraj Rathod
 * @Function    : getCancelSMS
 * @Description : This Function is temparary. This must be changed after discussion
 * @param       : 
 */
if (!function_exists('getCancelSMS')) {

    function getCancelSMS($cancel_detail) {
        $msg="Your ticket for ".$cancel_detail["alighting_stop"]." with PNR Number: ".$cancel_detail["pnr_no"]." is cancelled. Refund Amount: Rs.".$cancel_detail["refund_amount"]." only.";
        return $msg;
    }
}


if (!function_exists('validate_mobile_no')) {

    function validate_mobile_no($mobile){
        $phone_number = "";
        $phone_number = str_replace("+91","",$mobile);
        $phone_number = str_replace("+","",$phone_number);

        if(strlen($phone_number) >= 12 )
        {
            if(substr($phone_number, 0,2) == '91')
            {
                $phone_number = substr($phone_number, 2);
            }
        }

        return $phone_number;

    }
}


/*
 * @Author      : Suraj Rathod
 * @Function    : sendSMS
 * @Description : This Function is temparary. This must be changed after discussion
 * @param       : 
 */

if (!function_exists('sendSMS')) {

    function sendSMS($contact_no, $msg, $extra = array()) {

        //Replace $msg -> VALIDATION REMAINING.
        $CI = &get_instance();
        $CI->load->model('sms_log_model');
        $sms_log = array();

        if(!empty($extra))
        {
            $sms_log["ticket_id"] = $extra["ticket_id"];
        }
        
        if(is_array($contact_no))
        {
            $sms_log["supplied_mobile_number"] = implode(",",$contact_no);

            foreach ($contact_no as $key => $value)
            {
                $phone_number = "";
                // $phone_number = str_replace("+91","",$value);
                // $phone_number = str_replace("+","",$phone_number);
                // $phone_number = preg_replace('/91/', '', $phone_number, 1);
                $phone_number = validate_mobile_no($value);
                $contact_no[$key] = "91".ltrim($phone_number, '0');
            }

            $mobile_number = implode(",",$contact_no);
        }
        else
        {
            $sms_log["supplied_mobile_number"] = $contact_no;
            // $phone_number = str_replace("+91","",$contact_no);
            // $phone_number = str_replace("+","",$phone_number);
            // $phone_number = preg_replace('/91/', '', $phone_number, 1);
            $phone_number = validate_mobile_no($contact_no);
            $mobile_number = "91".ltrim($phone_number, '0');
        }

        $url = $CI->config->item("sms_gateway_url");
        $encrypt_msg = sms_encrypt('enc '.$msg);

        $sms_log["mobile_number"] = $mobile_number;
        $sms_log["message"] = rokad_encrypt($msg,$CI->config->item("pos_encryption_key"));
        $sms_log["encrypt_message"] = $encrypt_msg;
        
        $fields = array(
                        'feedid' => $CI->config->item("sms_gateway_feed_id"),  
                        'username' => $CI->config->item("sms_gateway_user_id"), 
                        'password' => $CI->config->item("sms_gateway_password"), 
                        "mtype" => $CI->config->item("sms_gateway_mtype"),
                        'to' => $mobile_number, 
                        'Text' => $msg,
                        'ssl' => false 
                        );


        log_data("sms/sms_".date("d-m-Y")."_log.log",$fields,'data send to sms gateway');

        $sms_response = sendDataOverPost($url,$fields);
        log_data("sms/sms_".date("d-m-Y")."_log.log",simplexml_load_string($sms_response),'response from sms gateway');

        if (isset($sms_response))
        {
            $sms_response = simplexml_load_string($sms_response) or false;

            if($sms_response)
            {
                $sms_log["request_id"] = $sms_response["REQID"];
                $sms_log["submit_date"] = $sms_response->MID["SUBMITDATE"];
                $sms_log["transaction_id"] = (isset($sms_response->MID["TID"])) ? $sms_response->MID["TID"] : "";

                if(isset($sms_response->MID->ERROR->ERROR->CODE) || isset($sms_response->MID->ERROR->CODE))
               {
                    $sms_log["error_code"] = isset($sms_response->MID->ERROR->ERROR->CODE) ? $sms_response->MID->ERROR->ERROR->CODE : $sms_response->MID->ERROR->CODE;
                    $sms_log["status"] = "failed";
                    $sms_log["failed_reason"] = isset($sms_response->MID->ERROR->ERROR->CODE) ? $sms_response->MID->ERROR->ERROR->DESC : $sms_response->MID->ERROR->DESC;
                }
                else
                {
                    $sms_log["status"] = "success";
                }
                $is_insert = $CI->sms_log_model->save_sms_log($sms_log);
                
                return ($is_insert) ?  true : false;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}

    /*
     * @function    : getTicketTemplate
     * @description : function will mail subject and mail body
     * @param       : $mailtype -> type of template
     */
    if (!function_exists('getTicketTemplate')) {

        function getTicketTemplate($mailtype) {
            // echo "template".$mailtype;
            // exit;
            $CI = &get_instance();
            $CI->load->model('op_ticket_format_model');
            $CI->op_ticket_format_model->tkt_format_name = $mailtype;
            $mail_data = $CI->op_ticket_format_model->select();
            // print_r($mail_data);
            return $mail_data;
        }

    }
function create_user_folder($uid, $folder, $subfolder) 
 {


    if (!file_exists(UPLOAD_FOLDER . '/' . $uid . '/' . $folder)) {
         $old = umask(0);
        mkdir(UPLOAD_FOLDER . '/' . $uid . '/' . $folder, 0770, true);
        umask($old);
    }
    if (!empty($subfolder)) {
        for ($c = 0; $c < count($subfolder); $c++) {
            if (!file_exists(UPLOAD_FOLDER . '/' . $uid . '/' . $folder . '/' . $subfolder[$c])) {
                mkdir(UPLOAD_FOLDER . '/' . $uid . '/' . $folder . '/' . $subfolder[$c], 0770, true);
            }
        }
    }
}

function create_main_folder($folder) {
    if (!file_exists(FCPATH . '/' . $folder)) {
        $old = umask(0);
        mkdir(FCPATH . '/' . $folder, 0770);
        umask($old);
    }
}

/*********Image uplaod from API************/
function save_image_from_url($im,$path)
{
    
    $ch = curl_init($im);
    
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
    $rawdata = curl_exec($ch);
    curl_close($ch);

    $fp = fopen($path, 'w');
    fwrite($fp, $rawdata);
    fclose($fp);
}



/************ CC AVENUE START********/
if (!function_exists('encrypt')) {

    function encrypt($plainText,$key)
    {

        $secretKey = hextobin(md5($key));
        $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
        $openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', 'cbc', '');
        $blockSize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
        $plainPad = pkcs5_pad($plainText, $blockSize);
        if (mcrypt_generic_init($openMode, $secretKey, $initVector) != -1)
        {
            $encryptedText = mcrypt_generic($openMode, $plainPad);
            mcrypt_generic_deinit($openMode);
        } 
        return bin2hex($encryptedText);
    }
}

if (!function_exists('decrypt')) {

    function decrypt($encryptedText,$key)
    {
        $secretKey = hextobin(md5($key));
        $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
        $encryptedText = hextobin($encryptedText);
        $encryptedText = rtrim($encryptedText, "\0");
        $openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', 'cbc', '');
        mcrypt_generic_init($openMode, $secretKey, $initVector);
        $decryptedText = mdecrypt_generic($openMode, $encryptedText);
        $decryptedText = rtrim($decryptedText, "\0");
        mcrypt_generic_deinit($openMode);
        return $decryptedText;
        
    }
}

//*********** Padding  function *********************
if (!function_exists('pkcs5_pad')) {
    function pkcs5_pad($plainText, $blockSize)
    {
        // $pad = $blockSize - (strlen($plainText) % $blockSize);
        // return $plainText . str_repeat(chr($pad), $pad);

        $pad = $blockSize - (strlen($plainText) % $blockSize);
        return $plainText . str_repeat(chr($pad), $pad);
    }
}
//********** Hexadecimal to Binary  function for php 4.0 version ********
if (!function_exists('hextobin')) {

    function hextobin($hexString) 
    { 
        error_reporting(0);
        // $length = strlen($hexString); 
        // $binString="";   
        // $count=0; 
        // while($count<$length) 
        // {       
        //     $subString =substr($hexString,$count,2);           
        //     $packedString = pack("H*",$subString); 
        //     if ($count==0)
        //     {
        //         $binString=$packedString;
        //     }  
        //     else 
        //     {
        //         $binString.=$packedString;
        //     } 
            
        //     $count+=2; 
        // } 
        // return $binString; 

        $length = strlen($hexString);
        $binString = "";
        $count = 0;
        while ($count < $length)
        { 
            $subString = substr($hexString, $count, 2);
            $packedString = pack("H*", $subString);
            if ($count == 0)
            { 
                $binString = $packedString;
            } 
            else
            { 
                $binString.=$packedString;
            } 
            $count+=2;
        } 
        return $binString;
    }
}
/************ CC AVENUE END*******/

if (!function_exists('get_menu')) {

    function get_menu() {
        $CI = &get_instance();

        $menu_data = array();
        $user_id = $CI->session->userdata("user_id");
        $role_name = strtolower($CI->session->userdata("role_name"));
        //echo "<pre>"; print_r($CI->session->userdata("user_menu"));die;
        if($CI->session->userdata("user_menu"))
        {
            $menu_data = $CI->session->userdata('user_menu');
        }
        else
        {
            $CI->load->model(array('menu_model','permission_model'));

            $menus = false;
            // if ($role_name == "super admin") 
            // {
            //     $menus = $CI->menu_model->where('record_status','Y')->order_by('seq_no','asc')->as_array()->find_all();
            // }
            // else
            // {
            $role_menu = $CI->db->query("select group_concat(menu_id) as menu_id from permission where view = 'Y' and group_id = ? group by group_id", array($CI->session->userdata('role_id')))->row();

            if($role_menu)
            {
                $CI->menu_model->where_in('id', explode(',',$role_menu->menu_id));
                $menus = $CI->menu_model->where('record_status','Y')->order_by('seq_no','asc')->as_array()->find_all();
            }
            //}

            if($menus)
            {
                $child_menu = getChildren($menus, 0);

                $CI->session->set_userdata(array("user_menu" => $child_menu));
                $menu_data = $child_menu;
            }
            
        }

        return '<ul class="sidebar-menu">'.generate_menu($menu_data).'</ul>';
        
    }
}


function getChildren($menus, $p) {
  $r = array();
  foreach($menus as $row) {
    if ($row['parent']== $p) {
        $row['child'] = getChildren($menus, $row['id']);
      $r[$row['id']] = $row;
    }
  }
  return $r;
}

function generate_menu($menus) {
    $html =''; 
    foreach($menus as $key => $value) {
        if($value['parent'])
        {
            $icon_class = "<i class='fa fa-angle-double-right'></i>";
        }
        else
        {
            $icon_class = "<i class='fa ".$value['class']."'></i>";   
        }


        if(count($value['child']) >0 )
        {
            $html .= "<li class='treeview'>
                        <a href='" . base_url() . $value['link'] . "'>" . $icon_class . "</i><span>" . ucwords($value['display_name']) . "</span>
                        <i class='fa fa-angle-left pull-right'></i>
                        </a>
                        <ul class='treeview-menu'>
                        ";

            $html .= generate_menu($value['child']);
            $html .= "</ul> </li>";
        }
        else{
            $html .= "<li><a href='" . base_url() . $value['link'] . "'>" . $icon_class . "<span>" . ucwords($value['display_name']) . "</span></a></li>";
        }    
    }


    return $html;
}


/*
 * @Author      : Suraj Rathod
 * @function    : daysInMonth
 * @description : function will return number of days in particular month,year
 * @param       : $month,$year
 */
if (!function_exists('daysInMonth'))
{
    function daysInMonth($month = "",$year = "")
    {
        $no_of_days = 0;

        if($month != "" && $year != "")
        {
            $no_of_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        }

        return $no_of_days;
    }
}

/*
 * @Author      : 
 * @function    : multidimensional_array_sort
 * @description : function will return array in sorted order
 * @param       : $array,$on,$order
 */
if (!function_exists('multidimensional_array_sort'))
{
    function multidimensional_array_sort($array, $on, $order=SORT_ASC){

        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                    break;
                case SORT_DESC:
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }
}


/*
 * @Author      : Suraj
 * @function    : getRefundAmount
 * @description : function will return amount to be returned to the user
 * @param       : $ticket_cost -> Ticket Cost of ticket ( In case of STU without convey charges)
 *                $discount_percentage -> Total Discount On Ticket
 *                $cancellation_charges -> Cancellation charges from Operator/Api Provider
 */
if (!function_exists('getRefundAmount'))
{
    function getRefundAmount($ticket_cost, $discount_percentage, $cancellation_charges)
    {
        $amount_to_refund = 0;
        if($ticket_cost != "" && $discount_percentage != "" && $cancellation_charges != "")
        {
            $amount_to_refund = $ticket_cost-($ticket_cost*$discount_percentage/100)-$cancellation_charges;
        }

        return $amount_to_refund;
    }
}


if (!function_exists('getRefundAmountToPay'))
{
    function getRefundAmountToPay($pg_amount_paid, $cancellation_charges)
    {
        $amount_to_refund = 0;
        if($pg_amount_paid !== "" && $cancellation_charges !== "")
        {
            $amount_to_refund = $pg_amount_paid - $cancellation_charges;
        }

        return $amount_to_refund;
    }
}


/*
 * @Author      : Suraj Rathod
 * @Function    : array_sort_by_column
 * @Description : sort array by column
 * @param       : $array_to_sort -> array to sort
 *                $sort_column -> array on which u want to apply sort
 *                $direction -> SORT_ASC,SORT_DESC
 */
if (!function_exists('array_sort_by_column')) {

    function array_sort_by_column($array_to_sort, $sort_column, $direction = SORT_ASC)
    {
        $temp_array = array();
        foreach($array_to_sort as $key => $value)
        {
            $temp_array[$key] = $value;
        }

        array_multisort($temp_array, $direction, $array_to_sort);
        return $temp_array;
    }
}


/*
 * @Author      : Suraj Rathod
 * @Function    : usort_array_date
 * @Description : sort array by date
 * @param       :
 */
if(!function_exists('usort_array_date'))
{
    function usort_array_date($a, $b)
    {
        return(strtotime($b['date']) - strtotime($a['date'])); 
    }
}



/*
 * @Author      : Suraj Rathod
 * @Function    : mcrypt_encrypt_e
 * @Description : hrtc encryption/decription algorithm provided by HRTC
 * @param       : $ky  - The secret key
 *                $input  - Input parameter that needs to be encrypted.
 */
if(!function_exists('mcrypt_encrypt_e'))
{
    function mcrypt_encrypt_e($input, $ky)
    {
        $key = $ky;
        $size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
        $input = mcrypt_pkcs5_pad_e($input, $size);
        $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', 'cbc', '');
        $iv = $ky;
        mcrypt_generic_init($td, $key, $iv);
        $data = mcrypt_generic($td, $input);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td); 
        $data = base64_encode($data);
        return $data;
    }
}

/*
 * @Author      : Suraj Rathod
 * @Function    : mcrypt_pkcs5_pad_e
 * @Description : hrtc encryption/decription algorithm provided by HRTC
 * @param       : $text
 *                $blocksize
 */
if(!function_exists('mcrypt_pkcs5_pad_e'))
{
    function mcrypt_pkcs5_pad_e($text, $blocksize)
    {
        $pad = $blocksize - (strlen($text) % $blocksize); 
        return $text . str_repeat(chr($pad), $pad);
    }
}


/*
 * @Author      : Suraj Rathod
 * @Function    : mcrypt_decrypt_e
 * @Description : hrtc encryption/decription algorithm provided by HRTC
 * @param       : $ky  - The secret key
 *                $crypt  - The encrypted data which is received from HRTC server.
 */
if(!function_exists('mcrypt_decrypt_e'))
{
    function mcrypt_decrypt_e($crypt, $ky)
    {
        $crypt = base64_decode($crypt);
        $key = $ky;
        $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', 'cbc', '');
        $iv = $ky;
        mcrypt_generic_init($td, $key, $iv);
        $decrypted_data = mdecrypt_generic($td,$crypt);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        $decrypted_data = mcrypt_pkcs5_unpad_e($decrypted_data);
        $decrypted_data = rtrim($decrypted_data);
        return $decrypted_data;
    } 
}

/*
 * @Author      : Suraj Rathod
 * @Function    : mcrypt_pkcs5_unpad_e
 * @Description : hrtc encryption/decription algorithm provided by HRTC
 * @param       : $ky  - The secret key
 *                $crypt  - The encrypted data which is received from HRTC server.
 */
if(!function_exists('mcrypt_pkcs5_unpad_e'))
{
    function mcrypt_pkcs5_unpad_e($text)
    {
        $pad = ord($text{strlen($text) - 1});
        if ($pad > strlen($text))
            return false; 
        return substr($text, 0, -1 * $pad);
    }
}


/*
 * @Author      : Suraj Rathod
 * @Function    : xml_to_array
 * @Description : 
 */
if(!function_exists('xml_to_array'))
{
    function xml_to_array($string)
    {
        $xml = simplexml_load_string($string);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);
        return $array;
    }
}


if(!function_exists('getHrtcChildFare'))
{
    function getHrtcChildFare($is_true = false, $data = array())
    {
        $total_fare = 0;
        if($is_true && $data)
        {
            $total_fare += $data["stage_hp_fare"]/2;
            $total_fare += $data["stage_hry_fare"]/2;
            $total_fare += $data["stage_chd_fare"]/2;
            $total_fare += $data["stage_pb_fare"]/2;
            $total_fare += $data["stage_dlh_fare"]/2;
            $total_fare += $data["stage_rj_fare"]/2;
            $total_fare += $data["stage_jk_fare"]/2;
            $total_fare += $data["stage_up_fare"]/2;
            $total_fare += $data["stage_uk_fare"]/2;
            $total_fare += $data["stage_oth1_fare"]/2;
            $total_fare += $data["stage_oth2_fare"]/2;
            $total_fare += $data["stage_oth3_fare"]/2;
            $total_fare += $data["stage_toll_tax"]/2;
            $total_fare += $data["stage_lic"]/2;
            $total_fare += $data["stage_misc"]/2;

            return round($total_fare);
        }

        return $data["Fare"];
    }
}

if(!function_exists('getHrtcFareWithServiceCharge'))
{
    function getHrtcFareWithServiceCharge($fare = "")
    {
        if($fare != "")
        {
            return ceil($fare + (($fare * HRTC_SERVICE_CHARGE_PERCENTAGE)/100));
        }
        
        return false;
    }

}
    

/*
 * @Author      : Mihir Shah
 * @Function    : make_csv
 * @Description : This function is use for make csv file .
 */

if(!function_exists('make_csv'))
{
    function make_csv($filename,$title,$val)
    {
        // File Path 
        $file = FCPATH . 'log/' . $filename;
        make_dir($file, 0770, true);

        //Check if file is already exists or not and write data
        if(!file_exists($file))
        {
            $file_write = fopen($file,"a");
            fputcsv($file_write,$title);
        }

        $file_write = fopen($file,"a");
        fputcsv($file_write,$val);

        fclose($file_write);
    }
}


if(!function_exists('sendTicketBookedSms'))
{
    function sendTicketBookedSms($smsData, $mobile_number_array = array())
    {   
        $data = $smsData["data"];
        $seat_with_berth = $smsData["seat_with_berth"];
        $passenger_count = '';
        if($data['passenger_ticket']->num_passgr > 1) {
            $passenger_count = '+'.$data['passenger_ticket']->num_passgr - 1;
        }
        $ticket_sms_detail = array(
                                  "{{departure_stop}}" => $data['passenger_ticket']->from_stop_name,
                                  "{{alightning_stop}}" => $data['passenger_ticket']->till_stop_name,
                                  "{{operator_bus_type}}" => $data['passenger_ticket']->bus_type,
                                  "{{pnr_no}}" => $data['passenger_ticket']->pnr_no,
                                  "{{operator_name}}" => $data['passenger_ticket']->op_name,
                                  "{{date_of_journey}}" => date("d-M-Y",strtotime($data['passenger_ticket']->boarding_time)),
                                  "{{boarding_point}}" => $data['passenger_ticket']->boarding_stop_name,
                                  "{{boarding_point_time}}" => date("H:i",strtotime($data['passenger_ticket']->boarding_time)),
                                  "{{destination_stop}}" => $data['passenger_ticket']->destination_stop_name,
                                  "{{seat_no}}" =>  $seat_with_berth,
                                  "{{primary_passanger_name}}" =>  $data['passenger_ticket_details'][0]->psgr_name,
                                  "{{primary_passanger_gender}}" =>  ($data['passenger_ticket_details'][0]->psgr_sex == "M") ? "Male" : "Female",
                                  "{{bus_service_no}}" => $data['passenger_ticket']->bus_service_no,
                                  "{{passanger_count}}" => $passenger_count,
                                  "{{ticket_fare}}" => $data['passenger_ticket']->tot_fare_amt_with_tax,
                                  "{{operator_contact_number}}" => '02266813930'
                                );

        log_data("sms/sms_".date("d-m-Y")."_log.log",$ticket_sms_detail, 'ticket_sms_detail array');

        $msg_to_sent = str_replace(array_keys($ticket_sms_detail),array_values($ticket_sms_detail), BOS_ORS_TICKET_BOOKED_SMS_TEMPLATE);

        $is_send = sendSMS($mobile_number_array , $msg_to_sent, array('ticket_id' => $data['passenger_ticket']->ticket_id));

        return $is_send;
    }

}


if(!function_exists('actv_dactv_virtual_account'))
{
    function actv_dactv_virtual_account($id,$status)
    {
        if($status == '')
        {
            $return = '<font>Account Not Created</font>';
        }
        elseif($status == 1)
        {
            $return = '<a href='.base_url() . 'admin/virtual_credit/deactive_credit_account/'.$id.' title="Deactivate Credit Account" onClick="return doconfirm_deactivate();" ref="'.$id.'"><font>Deactivate Credit Account</font> </a>';
        }
        else 
        {
            $return = '<a href='.base_url() . 'admin/virtual_credit/deactive_credit_account/'.$id.' title="Activate Credit Account" onClick="return doconfirm_activate();" ref="'.$id.'"><font>Activate Credit Account</font> </a>';
        }
        
        return $return;
    }
}


if(!function_exists('get_bossref_no'))
{
    function get_bossref_no($pnr)
    {
        
    }
}

  
    /*
 * @Author      : Maunica shah
 * @Function    : actv_dactv_users
 * @Description : This function is used for agent activation process with kyc check.
 */
    
 /*********USER ACTIVATION NEW PROCESS CHANGES STARTS HERE **********/
if(!function_exists('actv_dactv_users'))
{
    function actv_dactv_users($id,$email_status,$status)
    {
        if($status == '')
        {
            $return = '<font>Account Not Activated</font>';
        }
        elseif($email_status == "N" && $status =="N")
        {
//            $return = 'N<a href="javascript:void"  class="user_actv_btn margin" ref="'.$id.'"><i  class="glyphicon glyphicon-pencil"></i> </a>';
            $return = 'N<a title = "Resend Verification Mail" href="javascript:void" class="resend_mail margin btn btn-info btn-xs" ref="'.base64_encode($id).'"><i  class="fa fa-envelope"></i> </a>';
          
        }
        elseif($email_status == "Y" && $status =="Y")
        {
            $return = 'Y';
        }
        elseif($email_status == "Y" && $status =="N")
        {
            $return = 'Y';
        }
        else 
        {
            $return = 'N';
        }
        
        return $return;
    }
}
/*********USER ACTIVATION NEW PROCESS CHANGES ENDS HERE **********/


/*********USER ACTIVATION NEW PROCESS CHANGES ENDS HERE **********/
  /*
 * @Author      : Rajkumar Shukla
 * @Function    : check_agent_users_status
 * @Description : This function is used for check  agent user  status according to that edit option will display.
 */
    
 /*********USER ACTIVATION NEW PROCESS CHANGES STARTS HERE **********/
if(!function_exists('check_agent_users_status'))
{
    function check_agent_users_status($id,$status,$kyc_status,$email_verify_status,$role_name='',$is_delete,$mobile_no_verify)
    {   
		$CI = &get_instance();
        if($mobile_no_verify =="No" || $email_verify_status == "N")
        {
              $return = '<font>Please verify email and mobile no</font>';
        }
        elseif($status =="Y")
        {
			$btn_tmpl = '';
			$role_id = strtolower($CI->session->userdata('role_id'));
			if($role_id == SUPPORT_ROLE_ID)		
			{
				$return =  '<a title = "View detail" href=" '. base_url('admin/registration/view_details').'/?ref='.base64_encode($id).'" class="btn btn-primary btn-xs edit_btn" ref="'.$id.'"><i  class="fa fa-eye"></i></a> <a href="javascript:void" title= "Reset RMN" class="btn btn-primary btn-xs reset_rmn" ref="'.$id.'" data-ref="'.$status.'">  <i  class=" fa fa-mobile "></i> </a>';
			}
			else
			{
				$return =  '<a title = "Edit detail" href=" '. base_url('admin/registration/edit_details').'/'.base64_encode($id).'" class="edit_btn btn btn-primary btn-xs edit_btn" ref="'.$id.'"><i  class="fa fa-edit"></i></a> <a title = "View detail" href=" '. base_url('admin/registration/view_details').'/?ref='.base64_encode($id).'" class="btn btn-primary btn-xs edit_btn" ref="'.$id.'"><i  class="fa fa-eye"></i></a> <a title = "Upload Document" href=" '. base_url('admin/registration/upload_document').'/?ref='.base64_encode($id).'" class="btn btn-primary btn-xs edit_btn" ref="'.$id.'"> <i  class="glyphicon glyphicon-file"></i> </a> <a href="javascript:void" title= "Deactivate User" class="soft_del_btn btn btn-primary btn-xs edit_btn" ref="'.$id.'" data-ref="'.$status.'">  <i  class="fa fa-check-square-o"></i> </a> <a href="javascript:void" title= "Delete User" class="del_btn btn btn-primary btn-xs edit_btn" ref="'.$id.'" data-ref="'.$is_delete.'">  <i  class="fa fa-trash-o "></i> </a><a href="javascript:void" title= "Reset RMN" class="btn btn-primary btn-xs reset_rmn" ref="'.$id.'" data-ref="'.$status.'">  <i  class=" fa fa-mobile "></i> </a>';
			}
		}
        elseif($mobile_no_verify =="Yes" && $email_verify_status == "Y")
        {
            $return = ' <a href="javascript:void" title= "Activate User" class="soft_del_btn" ref="'.$id.'" data-ref="'.$status.'"> <i  class="fa fa-square-o"></i></a>';
        }
        return $return;
    }
}
/*********USER ACTIVATION NEW PROCESS CHANGES ENDS HERE **********/ 

if ( ! function_exists('getGstAmount'))
{
    function getGstAmount($perAmt)
    {
        $valGst = ($perAmt / 1.18) * (GST_APPLICABLE_MONTHLY_COMMISSION / 100);
        $gstAmt = round($valGst,2);
        return $gstAmt;
    }
}

if ( ! function_exists('getGstCommAmount'))
{
    function getGstCommAmount($commAmt,$gstAmt)
    {
        $minusGst = $commAmt - $gstAmt;
        $minusGst = round($minusGst,2);
        return $minusGst;
    }
}

if(!function_exists('get_users_for_ors_agent'))
{
	function get_users_for_ors_agent()
	{
		$users = array();
		$CI = &get_instance();
		$user_id = strtolower($CI->session->userdata('user_id'));		
		$agent_users = $CI->users_model->column('group_concat(id) as ids')->where(array('parent_id' =>$user_id))->find_all();
        $users = explode(',',$agent_users[0]->ids);
        return $users;
	}
}
/*
 * @Author      : Rajkumar Shukla
 * @function    : get_users_for_same_office
 * @param       : 
 * @detail      : Here u will get users of the logged in users  belong to same office.               
 */
 
if(!function_exists('get_users_for_same_office'))
{
	function get_users_for_same_office()
	{
		$office_users = array();
		$CI = &get_instance();
		$user_id = strtolower($CI->session->userdata('user_id'));
				$multi_office_code =  $CI->users_model->column('office_code')->where(array('id' => $user_id))->find_all();
				$user_office = $multi_office_code[0]->office_code;
				if($multi_office_code) {
					$agent_users = $CI->users_model->column('group_concat(id) as ids')->where(array('office_code' =>$user_office))->find_all();
					$office_users = explode(',',$agent_users[0]->ids);               
				}
		return $office_users;
	}
}

if(!function_exists('calculate_commission_new'))
{   
    function calculate_commission_new($ticket_detail,$provider_id,$op_id,$provider_type,$ticket_id,$commission_id,$bus_type,$update_data)
    {
        $data = array();
        logging_data("commission/calculate_commission.log",$commission_id,"commission id for ticket :".$ticket_id);
        $CI   = & get_instance();
        $CI->load->model(array('commission_model','commission_detail_model','agent_tickets_model','tickets_model','users_model','commission_calculation_model'));     

        if($bus_type != '') {
            $bus_type_id = $CI->bus_type_model->column('bus_type_id')->where(array("id" => $bus_type))->as_array()->find_all();   
        }
        if(!empty($bus_type_id)) {
            $bus_type_id = $bus_type_id[0]['bus_type_id'];
        }
        logging_data("commission/calculate_commission.log",$bus_type,"bus type id  for ticket :".$ticket_id);
        $where_array = array('p.id'=> $provider_id,'p.is_status' => 1,'p.is_deleted' => 0,'c.status' => '1','bc.status' => '1');
        $CI->db->select("c.id AS commission_id,
                       c.name AS commission_name,
                       c.from AS commission_from,
                       c.till AS commission_till,
                       c.commission_with_service_tax,
                       c.service_tax,
                       c.commission_type as comm_type,
                       cd.id AS commission_detail_id,
                       cd.base_commission_id,
                       cd.op_id as operator_id,
                       cd.provider_operator_id AS provider_operator_id,
                       cd.provider_id AS provider_id,
                       cd.base_rate_flag,
                       cd.percentage_base,
                       cd.fixed_base,
                       cd.rookad_commission,
                       cd.md_commission,
                       cd.ad_commission,
                       cd.dist_commission,
                       cd.retailer_commission,
                       p.name AS provider_name,
                       o.op_name AS provider_operator_name,
                       btm.name as bus_type_name,
                       bc.commission_on as commission_on");
        $CI->db->from("commission_detail cd");
        $CI->db->join("commission c", "c.id = cd.commission_id ", "inner");
        $CI->db->join("base_commission bc", "bc.id = cd.base_commission_id", "inner");
        $CI->db->join("api_provider p", "p.id = cd.provider_id", "inner");
        $CI->db->join("api_provider_operator po", "po.id = cd.provider_operator_id", "left");
        $CI->db->join("op_master o", "o.op_id = po.op_id", "left");
        $CI->db->join("bus_type_home btm","btm.id=bc.bus_type","left");
        $CI->db->where($where_array);
        if($provider_id == 1)
        {
            if($op_id == $CI->config->item("msrtc_operator_id") || $op_id == $CI->config->item("rsrtc_operator_id"))
            {
                $CI->db->where(array('o.op_id' => $op_id,'o.is_status' => 1,'o.is_deleted' => 0,'bc.bus_type'=>$bus_type_id));
            }
            else
            {
                 $CI->db->where(array('o.op_id' => $op_id,'o.is_status' => 1,'o.is_deleted' => 0,));
            }
        }
        $CI->db->where('cd.commission_id',$commission_id);
        $query = $CI->db->get();
        logging_data("commission/calculate_commission.log",$CI->db->last_query(),"Query For ticket : ".$ticket_id);
        if($query->num_rows()) {
            $result  = $query->result();
            if(is_array($result) && count($result) > 0) {
                if($result) {
                    $final_retailer_commission = 0;
                    $retailer_commission = 0;
                    $tds_on_retailer_commission = 0;
                    foreach($ticket_detail as $detail) {
                        $calculated_data = get_calcualted_data($result[0],$detail,$ticket_id,$update_data);
                        
                        if($update_data) {
                            $final_retailer_commission += $calculated_data['final_retailer_commission'];
                            $retailer_commission += $calculated_data['retailer_commission'];
                            $tds_on_retailer_commission += $calculated_data['tds_on_retailer_commission'];
                        
                            $chain_detail = $CI->users_model->retailer_chain_detail();
                            logging_data("commission/calculate_commission.log",$chain_detail,"user chain detail For ticket : ".$ticket_id);
                            $chain_user_id_detail = explode(' ',$chain_detail[0]['ids']);
                            $chain_level_detail = explode(' ',$chain_detail[0]['level']);

                            foreach($chain_level_detail as $key => $level_detail) {
                                if($level_detail == COMPANY_LEVEL) {
                                    $calculated_com_array = array(
                                                                "ticket_id" => $ticket_id,
                                                                "ticket_detail_id" => $detail['id'],
                                                                "user_id" => $chain_user_id_detail[$key],
                                                                "commission_per" => $calculated_data['rookad_commission_per'],
                                                                "commission" => $calculated_data['rookad_commission'],
                                                                "tds_on_commission" => $calculated_data['tds_on_rookad_commission'],
                                                                "final_commission" => $calculated_data['final_rookad_commission'],
                                                                "added_on" => date('Y-m-d H:i:s')
                                                            );
                                    $CI->commission_calculation_model->insert($calculated_com_array);
                                }
                                else if ($level_detail == MASTER_DISTRIBUTOR_LEVEL) {
                                    $calculated_com_array = array(
                                                                "ticket_id" => $ticket_id,
                                                                "ticket_detail_id" => $detail['id'],
                                                                "user_id" => $chain_user_id_detail[$key],
                                                                "commission_per" => $calculated_data['md_commission_per'],
                                                                "commission" => $calculated_data['md_commission'],
                                                                "tds_on_commission" => $calculated_data['tds_on_md_commission'],
                                                                "final_commission" => $calculated_data['final_md_commission'],
                                                                "added_on" => date('Y-m-d H:i:s')
                                                            );
                                    $CI->commission_calculation_model->insert($calculated_com_array);
                                }
                                else if ($level_detail == AREA_DISTRIBUTOR_LEVEL) {
                                    $calculated_com_array = array(
                                                                "ticket_id" => $ticket_id,
                                                                "ticket_detail_id" => $detail['id'],
                                                                "user_id" => $chain_user_id_detail[$key],
                                                                "commission_per" => $calculated_data['ad_commission_per'],
                                                                "commission" => $calculated_data['ad_commission'],
                                                                "tds_on_commission" => $calculated_data['tds_on_ad_commission'],
                                                                "final_commission" => $calculated_data['final_ad_commission'],
                                                                "added_on" => date('Y-m-d H:i:s')
                                                            );
                                    $CI->commission_calculation_model->insert($calculated_com_array);
                                }
                                else if ($level_detail == POS_DISTRIBUTOR_LEVEL) {
                                    $calculated_com_array = array(
                                                                "ticket_id" => $ticket_id,
                                                                "ticket_detail_id" => $detail['id'],
                                                                "user_id" => $chain_user_id_detail[$key],
                                                                "commission_per" => $calculated_data['dist_commission_per'],
                                                                "commission" => $calculated_data['dist_commission'],
                                                                "tds_on_commission" => $calculated_data['tds_on_dist_commission'],
                                                                "final_commission" => $calculated_data['final_dist_commission'],
                                                                "added_on" => date('Y-m-d H:i:s')
                                                            );
                                    $CI->commission_calculation_model->insert($calculated_com_array);
                                }
                                else if ($level_detail == RETAILOR_LEVEL) {
                                    $calculated_com_array = array(
                                                                "ticket_id" => $ticket_id,
                                                                "ticket_detail_id" => $detail['id'],
                                                                "user_id" => $chain_user_id_detail[$key],
                                                                "commission_per" => $calculated_data['retailer_commission_per'],
                                                                "commission" => $calculated_data['retailer_commission'],
                                                                "tds_on_commission" => $calculated_data['tds_on_retailer_commission'],
                                                                "final_commission" => $calculated_data['final_retailer_commission'],
                                                                "added_on" => date('Y-m-d H:i:s')
                                                            );
                                    $CI->commission_calculation_model->insert($calculated_com_array);
                                }
                            }

                            $update_ticket_detail_array = array(
                                                                "commission_id" => $calculated_data['commission_id'],
                                                                "commission_detail_id" => $calculated_data['commission_detail_id']  
                                                            );
                        }
                        else {
                            $final_retailer_commission += $calculated_data['final_retailer_commission'];
                            $retailer_commission += $calculated_data['retailer_commission'];
                            $tds_on_retailer_commission += $calculated_data['tds_on_retailer_commission'];
                        }
                    }
                    if($update_data) {
                        $data['final_retailer_commission'] = $final_retailer_commission;
                        $data['retailer_commission'] = $retailer_commission;
                        $data['tds_on_retailer_commission'] = $tds_on_retailer_commission;
                    }
                    else {
                        $data['final_retailer_commission'] = $final_retailer_commission;
                        $data['retailer_commission'] = $retailer_commission;
                        $data['tds_on_retailer_commission'] = $tds_on_retailer_commission;

                    }
                }
            }
        }
        return $data;
    }       
}

if(!function_exists('get_calcualted_data'))
{
    function get_calcualted_data($com_detail,$detail,$ticket_id,$update_data) {
        $CI = & get_instance();
        logging_data("commission/calculate_commission.log",$com_detail,"commission detail For ticket : ".$ticket_id);
        logging_data("commission/calculate_commission.log",$detail,"ticket details For ticket : ".$ticket_id);
        
        //Commission Id
        if(!empty($com_detail->commission_id)) {
           $data['commission_id']       = $com_detail->commission_id;
        }

        //commission detail id    
        if(!empty($com_detail->commission_detail_id)) {
           $data['commission_detail_id']    = $com_detail->commission_detail_id;
        }
        $data['com_received']               = $detail['final_commission'];
        
        if($update_data) {
            $data['rookad_commission_per']      = $com_detail->rookad_commission;
            $data['md_commission_per']          = $com_detail->md_commission;
            $data['ad_commission_per']          = $com_detail->ad_commission;
            $data['dist_commission_per']        = $com_detail->dist_commission;

            $data['rookad_commission']          = round(($detail['final_commission'] * $com_detail->rookad_commission)/100 ,5);
            $data['md_commission']              = round(($detail['final_commission'] * $com_detail->md_commission)/100 ,5);
            $data['ad_commission']              = round(($detail['final_commission'] * $com_detail->ad_commission)/100 ,5);
            $data['dist_commission']            = round(($detail['final_commission'] * $com_detail->dist_commission)/100 ,5);
        
            $data['tds_on_rookad_commission']   = round($data['rookad_commission'] * ((float)($CI->config->item('tds')/100)),5);
            $data['tds_on_md_commission']       = round($data['md_commission'] * ((float)($CI->config->item('tds')/100)),5);
            $data['tds_on_ad_commission']       = round($data['ad_commission'] * ((float)($CI->config->item('tds')/100)),5);
            $data['tds_on_dist_commission']     = round($data['dist_commission'] * ((float)($CI->config->item('tds')/100)),5);
        
            $data['final_rookad_commission']    = $data['rookad_commission'] - $data['tds_on_rookad_commission'];
            $data['final_md_commission']        = $data['md_commission'] - $data['tds_on_md_commission'];
            $data['final_ad_commission']        = $data['ad_commission'] - $data['tds_on_ad_commission'];
            $data['final_dist_commission']      = $data['dist_commission'] - $data['tds_on_dist_commission'];
        }
        $data['retailer_commission_per']    = $com_detail->retailer_commission;

        $data['retailer_commission']        = round(($detail['final_commission'] * $com_detail->retailer_commission)/100 ,5);

        $data['tds_on_retailer_commission'] = round($data['retailer_commission'] * ((float)($CI->config->item('tds')/100)),5);
        
        $data['final_retailer_commission']  = $data['retailer_commission'] - $data['tds_on_retailer_commission'];
        
        logging_data("commission/calculate_commission.log",$data,"calculated data details For ticket : ".$ticket_id." and ticket detail id : ". $detail['id']);
        return $data;
    }
}

if(!function_exists('get_my_menu'))
{
    function get_my_menu($u1,$u2,$u3)
    {
        $CI = &get_instance();  
        $role_id = $CI->session->userdata('role_id');
        $role_name = $CI->session->userdata('role_name');
         
        if(!empty($u3))
        {
            $url = $u1 . "/" . $u2."/".$u3;
        }else
        {
            $url = $u1 . "/" . $u2;
        }
        $menu_data = [];
        if(!empty($role_id))
        {
            if($role_name != 'Super Admin')
            {
              $CI->load->model(array('permission_model', 'menu_model'));
              $where_array = array("group_id" => $role_id);
              $find_menu_data = $CI->permission_model->column('menu_id')->where($where_array)->as_array()->find_all();
              foreach ($find_menu_data as $key => $value) 
              {
                    $where_array = array("id" => $value,"link" => $url);
                    $find_menu = $CI->menu_model->where('link',$url)->as_array()->find_all();
                    $got_menu = $find_menu;
                    $menu_id = $got_menu['0']['id'];
                    
                    $wh_array = array("menu_id" => $menu_id,"group_id" => $role_id);
                    $menu_details = $CI->permission_model->where($wh_array)->as_array()->find_all();
                    if($menu_details)
                    {
                       $menu_data = explode(",",$menu_details['0']['action_name']);
                       return $menu_data;     
                    }
              }
            }
            else
            {
                $full_acesss= 'full_acesss';
                return $full_acesss;
            }
        }
        return $menu_data;
    }
}
/* End of file date_helper.php */
/* Location: ./system/helpers/date_helper.php */


/*
 * @Author      : Mihir shah
 * @Function    : emitra_encrypt
 * @Description : This function is used for encrypt emitra data.
 */


if(!function_exists('rokad_encrypt'))
{
    function rokad_encrypt($plainText,$key)
    {
        // Hashing Api key
        $hash = hash( "sha256", $key , true );  

        // Convert into 16 chr string
        $string = substr($hash, 0,16);

        $blocksize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $paddingSize = $blocksize - (strlen($plainText) % $blocksize);
        $plainText .= str_repeat(chr($paddingSize), $paddingSize);

        $rtn = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $string, $plainText, MCRYPT_MODE_CBC, $string);
        $rtn = base64_encode($rtn);

        return  $rtn;
    }
}

/*
 * @Author      : Mihir shah
 * @Function    : emitra_decrypt
 * @Description : This function is used for decrypt emitra data.
 */

if(!function_exists('rokad_decrypt'))
{
    function rokad_decrypt($encryptedText,$key)
    {
        // decode encrypted Text
        $decoded = base64_decode($encryptedText);
       
        // Hashing Api key
        $hash = hash( "sha256", $key , true );
        // Convert into 16 chr string
        $string = substr($hash, 0,16);

        $plainText = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $string, $decoded, MCRYPT_MODE_CBC, $string);

        $len = strlen($plainText);
        $pad = ord($plainText[$len-1]);

        return substr($plainText, 0, $len - $pad);
    }
}

/*
 * @function    : is_logged_in_home_page
 * @param       : 
 * @detail      : To check if user is logged in and all three file created 
 *                and exists on server.
 *               
 */
if (!function_exists('is_logged_in_home_page'))
{
    function is_logged_in_home_page()
    {
        $CI = &get_instance();
        if ($CI->session->userdata("user_id"))
        {
            $roledata = $CI->role_model->find($CI->session->userdata("role_id"));
            if($roledata->front_back_access == 'both') {
                if($roledata->home_page == 'dashboard') {

                    redirect(base_url() . "admin/dashboard");
                }
                else if ($roledata->home_page == 'home') {
                    redirect(base_url()."front/home/search_new");
                }
                else {
                    redirect(base_url()."home");
                }
            }
            else if($roledata->front_back_access == 'back') {
                redirect(base_url() . "admin/dashboard");
            }
            else if ($roledata->front_back_access == 'front'){
                redirect(base_url()."front/home/search_new");
            }
            else {
                redirect(base_url()."home");
            }
        }
    }
}


if(!function_exists('save_n_send_activation_key'))
{
    function save_n_send_activation_key($user_id, $user_email, $display_name, $reason)
    {       
		$CI = &get_instance();
		$response['msg_type'] = "";
		$response['msg'] = "";
		$mail_result = false;		
		
		if($reason == 'email_verification' || $reason == 'change_email_verification' || $reason == 'reset_username')
		{			
			$activation_key = getRandomId(8);
			$user_email_encrypted = rokad_encrypt($user_email, $CI->config->item('pos_encryption_key'));
			$activate_data = array(
			"user_id" => $user_id,
			"user_mail" => $user_email_encrypted,
			"mail_activate_key" => $activation_key,
			"verified" => 'N',
			"verify_date" => '',
			"reason" => $reason,
			"sent_date" => date('Y-m-d H:i:s')
			);
			
			$last_insert_id = $CI->user_email_model->save_activation_key($activate_data);
			if($last_insert_id)
			{			
				// log email activation_key saved
				$email_verify_log_data = array(
				"user_id" => $user_id,
				"user_mail" => $user_email,
				"encrypted_user_mail" => $user_email_encrypted,
				"mail_activate_key" => $activation_key,
				"verified" => 'N',
				"verify_date" => '',
				"reason" => $reason,
				"sent_date" => $activate_data['sent_date']
				);		
				
				log_data("email_verify/email_".date("d-m-Y")."_log.log", $email_verify_log_data, 'otp_data array');
				// log email activation_key saved				
				
				if($reason == 'email_verification')
				{
					$mailbody_array = array(
					  "mail_title" => "email_verification",	  
					  "mail_client" => $user_email,
					  "mail_replacement_array" => 
					   array(
							'{{fullname}}' => ucwords($display_name),
							'{{verification_mail_url}}' => base_url() . 'admin/login/user_verify/' . $activation_key . '/'. sha1($last_insert_id),
							'{{rokad_support_mail}}' => ROKAD_SUPPORT_MAIL,
							'{{home_page_url}}' => base_url(),
							'{{logo}}' => '<img src="'.DOMAIN_NAME_VAL.'/assets/travelo/front/images/logo.png" style="width:104px;"  />'					
							)
					);				
					//show($mailbody_array, 1);				
					$mail_result = setup_mail($mailbody_array);				
				}
				else if($reason == 'change_email_verification')
				{
					$mailbody_array = array(
					  "mail_title" => "change_email_verification",	  
					  "mail_client" => $user_email,
					  "mail_replacement_array" => 
					   array(
							'{{fullname}}' => ucwords($display_name),
							'{{verification_mail_url}}' => base_url() . 'admin/login/change_email_verify/' . $activation_key . '/'. sha1($last_insert_id),
							'{{header}}' => '<img src="http://www.bookonspot.com/img/logo.png" width="300px;" />',
							'{{rokad_support_mail}}' => ROKAD_SUPPORT_MAIL,
							'{{home_page_url}}' => base_url(),
							'{{logo}}' => '<img src="'.DOMAIN_NAME_VAL.'/assets/travelo/front/images/logo.png" style="width:104px;"  />'
							)
					);				
					//show($mailbody_array, 1);				
					$mail_result = setup_mail($mailbody_array);				
				}
				else if($reason == 'reset_username')
				{
					$mailbody_array = array(
					  "mail_title" => "reset_username",	  
					  "mail_client" => $user_email,
					  "mail_replacement_array" => 
					   array(
							'{{fullname}}' => ucwords($display_name),
							'{{verification_mail_url}}' => base_url() . 'admin/login/reset_username_verify/' . $activation_key . '/'. sha1($last_insert_id),
							'{{rokad_support_mail}}' => ROKAD_SUPPORT_MAIL,
							'{{home_page_url}}' => base_url(),
							'{{logo}}' => '<img src="'.DOMAIN_NAME_VAL.'/assets/travelo/front/images/logo.png" style="width:104px;"  />'					
							)
					);				
					//show($mailbody_array, 1);				
					$mail_result = setup_mail($mailbody_array);				
				}
				
				if($mail_result)
				{
					$response['msg_type'] = "success";
					$response['msg'] = "success";
				}
				else
				{
					$response['msg_type'] = "error";
					$response['msg'] = "failed to send activation key";
				}
			}
			else
			{
				$response['msg_type'] = "error";
				$response['msg'] = "failed to save activation key";
			}			
		}
		else
		{
			$response['msg_type'] = "error";
			$response['msg'] = "invalid reason";
		}
			
		return $response;
	}
}


if (!function_exists('get_difference_in_time'))
{
    function get_difference_in_time($first_date, $second_date, $val)
    {
        $val = intval($val) ? intval($val) : 1;		
		$datetime1 = strtotime($first_date);
		$datetime2 = strtotime($second_date);
		$interval  = abs($datetime2 - $datetime1);
		$diff   = round($interval / $val);
		return $diff;
    }
}

if (!function_exists('generate_otp'))
{
    function generate_otp()
    {
       $otp_code = mt_rand(100000, 999999);
	   return $otp_code;
    }
}

if (!function_exists('generate_tpin'))
{
    function generate_tpin()
    {
       $tpin = mt_rand(1000, 9999);
		return $tpin;
    }
}

if (!function_exists('save_n_send_otp'))
{
    function save_n_send_otp($user_id, $mobile_no, $display_name, $sms_template, $reason)
    {
		$CI = &get_instance();
		$encrypted_mobile_no = rokad_encrypt($mobile_no, $CI->config->item('pos_encryption_key'));
					
		$otp_code = generate_otp();		
		$encrypted_otp_code = rokad_encrypt($otp_code, $CI->config->item('pos_encryption_key'));
		
		$otp_data = array("user_id" => $user_id,
		"mobile_no" => $encrypted_mobile_no,
		"otp_code" => $encrypted_otp_code,
		"verified" => 'N',
		"verify_date" => '',
		"reason" => $reason,
		"sent_date" => date('Y-m-d H:i:s')
		);
		
		if($CI->user_otp_model->save_otp($otp_data))
		{
			// log otp saved
			$otp_log_data = array(
			"user_id" => $user_id,
			"mobile_no" => $mobile_no,
			"encrypted_mobile_no" => $encrypted_mobile_no,
			"otp_code" => $otp_code,
			"encrypted_otp_code" => $encrypted_otp_code,
			"verified" => 'N',
			"verify_date" => '',
			"reason" => $reason,
			"sent_date" => $otp_data['sent_date']
			);		
			
			log_data("otp/otp_".date("d-m-Y")."_log.log",$otp_log_data, 'otp_data array');
			// log otp saved
			
			// send OTP via sms to user mobile number
			$response['msg_type'] = "success";
			$response['msg'] = "success";			
						
			$reg_sms_array = array("{{username}}" => ucwords($display_name), "{{otp}}" => $otp_code);
			$temp_sms_msg = str_replace(array_keys($reg_sms_array), array_values($reg_sms_array), $sms_template);
			$is_send = sendSMS($mobile_no, $temp_sms_msg);	  		
		}
		else
		{ // failed to save otp
			$response['msg_type'] = "error";
			$response['msg'] = "Failed to generate OTP. Please try again";
		}			
		
        return $response;
    }
}

/*
 * @function    : check_front_access
 * @param       : 
 * @detail      : To check user has access for front side of site.
 *               
 */
if (!function_exists('check_front_access'))
{
    function check_front_access()
    {
        $CI = &get_instance();
        if ($CI->session->userdata("user_id"))
        {
            $roledata = $CI->role_model->find($CI->session->userdata("role_id"));
            if($roledata->front_back_access == 'back') {
                redirect(base_url() . "admin/dashboard");
            }
        }
        else {
            redirect(base_url());
        }
    }

}

if (!function_exists('validate_username'))
{
    function validate_username($username="")
    {
        $flag = false;
		if(ctype_digit($username) && strlen($username) == 10)
		{ $flag = true; }
		return $flag;
    }
}

if (!function_exists('validate_otp'))
{
    function validate_otp($otp="")
    {
        $flag = false;
		if(ctype_digit($otp) && strlen($otp) == 6)
		{ $flag = true; }
		return $flag;
    }
}

/*@author       :Maunica Shah
 * @function    : kyc_approval_users
 * @param       : id,kyc_status,rolename
 * @detail      : TO give kyc verification link to users.
 *               
 */

if (!function_exists('kyc_approval_users'))
{
    function kyc_approval_users($id,$kyc_status,$role_name,$status,$email_verify_status,$mobile_no_verify)
    {
		$CI = &get_instance();
       eval(SYSTEM_USERS_ARRAY);
       if($kyc_status == '')
        {
            $return = '<font>Account Not Created</font>';
        }
        elseif($kyc_status == 'N' && (!in_array($role_name,$system_users_array) || $role_name == 'company'))
        {
			$return = 'N ';
			$role_id = strtolower($CI->session->userdata('role_id'));
			if($role_id == SUPPORT_ROLE_ID)		
			{
				$return .=  '';
			}
			else
			{
                $kyc_clk = '';
                $val = base_url() . 'admin/agent/agent_docs_view?agid='.base64_encode($id);
                if($status == 'N' && ($mobile_no_verify =="Yes" || $email_verify_status == "Y")) { 
                    $kyc_clk = 'onclick="return false;"';
                    $val = '#';
                }
				$return .=  '<a '.$kyc_clk.' href='.$val.' title="Verify KYC"  ref="'.$id.'"><input type="hidden" name="agid" id="agid" value= "'.base64_encode($id).'"><i class="btn btn-info btn-xs fa fa-key kyc_st"></i> </a>';
			}
		}
        else  if($kyc_status =="Y")
        {
            $return = 'Y';
        }

        return $return;
    }
}

if (!function_exists('verify_email'))
{
	function verify_email($verify_key="", $hashid="")
	{
		$CI = &get_instance();		
		if(!empty($verify_key) && !empty($hashid))
		{
			$data = array();
			$mail_verification_expiry = 24;
			$user_email_details = $CI->user_email_model->get_userby_key_n_id($verify_key, $hashid);
			//show($user_email_details, null, 'user_email_details');
			if (count($user_email_details) > 0) {
				if($user_email_details['verified'] == 'N')
				{
					$user_id = $user_email_details['user_id'];
					$user_email_encrypted = $user_email_details['user_mail'];
					
					$curr_date = date('Y-m-d H:i:s');
					$diff = get_difference_in_time($user_email_details['sent_date'], $curr_date, 3600);
					//show($diff, 1, 'diff');
					if($diff <= $mail_verification_expiry)
					{
						$CI->user_email_model->update_verify_status($user_email_details['id']);
						
						$response['msg_type'] = "success";
						$response['msg'] = $user_id;
					}
					else
					{
						$response['msg_type'] = "error";
						$response['msg'] = "Verification link expired";
					}					
				}
				else
				{
					$response['msg_type'] = "error";
					$response['msg'] = "Verification link already verified";
				}
				
			} else {
				$response['msg_type'] = "error";
				$response['msg'] = "Invalid Link";
			}
		}
		else
		{
			$response['msg_type'] = "error";
			$response['msg'] = "Invalid Parameters in url";
		}
		
		return $response;
	}
}

if (!function_exists('verify_otp'))
{
    function verify_otp($user_id, $mobile, $otp)
    {	
		$CI = &get_instance();
		$otp_verification_expiry = 5;
		if(validate_otp($otp))
		{			
			$user_data = $CI->users_model->where('id', $user_id)->find_all();
			if(count($user_data) > 0)
			{
				$user_data = $user_data[0];
				$encrypted_new_otp = rokad_encrypt($otp, $CI->config->item('pos_encryption_key'));
				$encrypted_mobile = rokad_encrypt($mobile, $CI->config->item('pos_encryption_key'));
				

				$user_otp_data = $CI->user_otp_model->get_last_sent($user_id, $encrypted_mobile);					
						
				if(count($user_otp_data) > 0)
				{				
					if($encrypted_new_otp == $user_otp_data['otp_code'])
					{
						//show("in if", 1);
						$curr_date = date('Y-m-d H:i:s');
						$diff = get_difference_in_time($user_otp_data['sent_date'], $curr_date, 60);					
						if($diff <= $otp_verification_expiry)
						{
							$data['msg_type']='success';
							$data['msg']='success';
						}
						else
						{						
							$data['msg_type'] = "error";
							$data['msg'] = "OTP you have entered is expired.";										
						}
					}
					else
					{
						//show("in else", 1);
						// invalid otp
						$data['msg_type'] = "error";
						$data['msg'] = "Please enter valid OTP.";					
					}				
				}
				else
				{ // invalid user or mobile no
					$data['msg_type'] = "error";
					$data['msg'] = "Invalid mobile no";				
				}
			}
			else
			{ // invalid user or mobile no
				$data['msg_type'] = "error";
				$data['msg'] = "Invalid user";			
			}			
		}			
		else
		{ // invalid user or mobile no
			$data['msg_type'] = "error";
			$data['msg'] = "Enter valid 6 digit otp number";			
		}
		
		return $data;
	}	
}

if (!function_exists('check_otp_attempts'))
{
    function check_otp_attempts()
    {
		$CI = &get_instance();
		$response['msg_type'] = "success";
		$response['msg'] = "success";
		$max_otp_attempts = 3;		
		
		$otp_count = $CI->session->userdata('max_otp_attempts');
		if(empty($otp_count))
		{			
			$CI->session->set_userdata('max_otp_attempts', 1);
		}
		else
		{				
			if($otp_count < $max_otp_attempts)
			{				
				$CI->session->set_userdata('max_otp_attempts', $otp_count+1); 
			}
			else
			{
				$response['msg_type'] = "error";
				$response['msg'] = "OTP can't be sent more than 3 times in an hour";
			}
		}
		
		return $response;
	}	
}

if (!function_exists('clear_otp_attempts'))
{
    function clear_otp_attempts()
    {
		$CI = &get_instance();
		$CI->session->unset_userdata('max_otp_attempts');		
	}	
}

if (!function_exists('users_action_log_new')) {

    function users_action_log_new($action_log_array) {
        $CI = &get_instance();
        //insert into user action log
		$ip_address = $CI->input->ip_address();
		$user_agent = $CI->input->user_agent();
		
        $CI->users_activity_log_model->user_id 		 = isset($action_log_array['user_id']) ? $action_log_array['user_id'] : NULL;
		$CI->users_activity_log_model->name 		 = isset($action_log_array['name']) ? $action_log_array['name'] : NULL;
        $CI->users_activity_log_model->rmn 			 = isset($action_log_array['rmn']) ? $action_log_array['rmn'] : NULL;
        $CI->users_activity_log_model->rollname 	 = isset($action_log_array['rollname']) ? $action_log_array['rollname'] : NULL;
		$CI->users_activity_log_model->url 			 = isset($action_log_array['url']) ? $action_log_array['url'] : NULL;
        $CI->users_activity_log_model->db_function 	 = isset($action_log_array['db_function']) ? $action_log_array['db_function'] : NULL;
		$CI->users_activity_log_model->db_table_name = isset($action_log_array['db_table_name']) ? $action_log_array['db_table_name'] : NULL;
		$CI->users_activity_log_model->function_name = isset($action_log_array['function_name']) ? $action_log_array['function_name'] : NULL;
		$CI->users_activity_log_model->input_data 	 = isset($action_log_array['input_data']) ? $action_log_array['input_data'] : NULL;
		$CI->users_activity_log_model->message 		 = isset($action_log_array['message']) ? $action_log_array['message'] : NULL;
		$CI->users_activity_log_model->ip_address 	 = isset($ip_address) ? $ip_address : NULL;
		$CI->users_activity_log_model->user_agent 	 = isset($user_agent) ? $user_agent : NULL;
		$CI->users_activity_log_model->datetime 	 = date("Y-m-d H:i:s");
        $CI->users_activity_log_model->save();
    }
}

if (!function_exists('is_strong_password'))
{
    function is_strong_password($str = "")
    {
		$response['msg_type'] = "error";
		$response['msg'] = "";		
		$allowed_characters = '$([^a-zA-Z0-9!@#%&._\$\*])$';		
		
		// first check if string contains any character which is not in the allowed_characters set
		if(preg_match_all($allowed_characters, $str))
		{
			// show error if found invalid character
			$response['msg_type'] = "error";
			$response['msg'] = "invalid_character";				
		}
		else
		{	
			// now check if string satisfies policy of atleast length 8, atleast 1 lowercase, uppercase, digit and special character from
			// set of allowed special characters
			if (!preg_match_all('$\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[0-9])(?=\S*[!@#%&._\$\*])\S*$', $str))
			{
				$response['msg_type'] = "error";
				$response['msg'] = "not_matched_with_policy";		
			}
			else
			{
				$response['msg_type'] = "success";
				$response['msg'] = "success";
			}	
		}
		
		return $response;
	}	
}

if (!function_exists('validate_tpin'))
{
    function validate_tpin($tpin="")
    {
        $flag = false;
		if(ctype_digit($tpin) && strlen($tpin) == 4)
		{ $flag = true; }
		return $flag;
    }
}

if(!function_exists('sms_encrypt'))
{
    function sms_encrypt($data)
    {
       //echo '<pre>'; print_r($data); echo '</pre>';exit;
       $CI                  = & get_instance();
       $cscurl              = $CI->config->item("sms_encrypt_url");
       $command             = "java -jar $cscurl ".$data;
       $result =exec($command,$output);
       $string =implode('',$output);
       return $string;
    }
}

if(!function_exists('validate_aadhar'))
{
	function validate_aadhar($aadhaarId)
	{	
		if (ctype_digit($aadhaarId)) {			
			if(strlen($aadhaarId) == 12)
			{return true;}
			else
			{return false;}
		}
		else 
		{return false;}	
	}
}

if(!function_exists('generate_requestid'))
{
	function generate_requestid()
	{
		$CI            = & get_instance();
		$digits_needed = $CI->config->item("aadhar_requestidlength");
		$random_number = '';
		$count = 0;
		while ( $count < $digits_needed ) {
			$random_digit = mt_rand(0, 9);			
			$random_number .= $random_digit;
			$count++;
		}
		return $random_number;		
	}
}

if(!function_exists('get_adhar_details'))
{
	function get_adhar_details($aadhaarId = "")
	{		
		$CI          = & get_instance();
		$concat_char = '|';
		$response['msg_type'] = "error";
		$response['msg'] = "";		
		
		if(validate_aadhar($aadhaarId))
		{
			$salt		= $CI->config->item("aadhar_salt");			
			$adhar_details	= array(
					"request_initiation_url" => $CI->config->item("aadhar_initiation_url"),
					"saCode" 	 => $CI->config->item("aadhar_saCode"),
					"aadhaarId"  => $aadhaarId,
					"requestId"  => generate_requestid(),
					"purpose"    => $CI->config->item("aadhar_purpose"),
					"modality"   => $CI->config->item("aadhar_modality"),
					"successUrl" => base_url().$CI->config->item("aadhar_successUrl"),
					"failureUrl" => base_url().$CI->config->item("aadhar_failureUrl")
					);
			
			$Hash_Sequence = $adhar_details['saCode'] . $concat_char . $aadhaarId . $concat_char . $adhar_details['requestId'] . $concat_char . $salt;
			$hash          = hash('sha256', $Hash_Sequence);
			
			$adhar_details['hash'] = $hash;				
			$response['msg_type'] = "success";
			$response['msg'] = $adhar_details;
		}
		else
		{
			$response['msg_type'] = "error";
			$response['msg'] = "Invalid aadhaar card number";
		}			
		return $response;
	}
}

if(!function_exists('get_aadhar_error_msg'))
{
	function get_aadhar_error_msg($error_code)
	{
		$error_msg = "error";
		$error_msg_arr = array(
			"AB-201" => "Malformed Json request",
			"AB-202" => "Mandatory attribute missing",
			"AB-203" => "Invalid Sub-AUA code",
			"AB-204" => "Hash mismatch",
			"AB-205" => "No Txn found for your request",			
			"AB-207" => "Txn has expired",
			"AB-208" => "Invalid environment",
			"AB-209" => "Contact us"
		);
		
		if(isset($error_msg[$error_code]))
		{
			$error_msg = $error_msg[$error_code];
		}
		
		return $error_msg;
	}
}

if(!function_exists('get_sample_response'))
{
	function get_sample_response()
	{
		$sample_success_response = '
			  {
				"kyc":{
				 	"photo":"/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCADwANsDAREAAhEBAxEB/8QAHAABAAICAwEAAAAAAAAAAAAAAAcIBQYBAgQD/8QAQxAAAQMDAQMJBgMECAcAAAAAAAECAwQFEQYSITEHF0FRVnGBotIIEyJhkaEUIzIVRHKxFkJDYmOCkrI0UnPBwtHw/8QAFgEBAQEAAAAAAAAAAAAAAAAAAAEC/8QAFhEBAQEAAAAAAAAAAAAAAAAAAAER/9oADAMBAAIRAxEAPwC7ZtgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHznnjpYXzTSMhiYmXSSORrW96ruQDSLxy1aVtL3MbWyXCROLaKNXp/qXCfcuDAO9oy0I/DbPXub1q+NF+mRg9dJ7QenJlRJ6W4UvzWJr0T6OGDJVHK7Z5YW1NrqoLlGxMz0SqsNUjel0bX4R+OlvHqXoGDI2TlFtt7vkVvikY5lXB+Joahi/DO1Nz2Ki72yNVFy1eKbxg2wgAAAAAAAAAAAAAAAAAAAAAAANE5QeVi3aKR1JC1Lhd8f8O12GxdSyL0fwpv7i4K/wCp9ZXfV9T7251j5mouWQN+GJn8LE3eK5X5lGEAAAHED02+4VNqrYKukmdBUwO245GcWO60AlHkt1Ddb9cPcS63qqSuV35dHVwJOydPkrncfkmF6iCeIElSFiTOY+XHxOjRUaq9aIqqqfUg+gAAAAAAAAAAAAAAAAAAAAI+5XeUR2jbWyjoXol3rGr7t3H3EfBZO/O5PnlegsFbJJHzSOkkc573qrnOcuVcq8VVelSjqAAAAAADsx7o3texzmPaqOa5q4VFTgqL1gWu5OdTu1bo+gr5XI6qwsNR/wBRu5V8dy+JkbMAAAAAAAAAAAAAAAAAAAHCqjUVXLstTeqr0J1gVG1tqN+q9UXC5OVVjlkVsKL/AFYm7mJ9N/ipoYMAAAAAAAABOPs43FXUd7oFXcySOoanVtIrV/2oSiZSAAAAAAAAAAAAAAAAAAAMNrOuht2k7vUVEskMLaV6OkiTL0ymz8Kde/Cd4FRJFasjlYz3bM/Cza2tlOhM9PeaHUAAAAAAAABLHs6zK3U11j6H0SL9JE/9kon4gAAAAAAAAAAAAAAAAAADTeWBFXk3vez/AMjFXu943Igq4vE0OAAAAAAAAAEqezu1V1dcHdCUK58ZGEosEQAAAAAAAAAAAAAAAAAABi9UWf8ApBpu523+tVU74m/xKnw/dEAp/JG+J7mSNVkjVVrmrxRU3Kn1NDqAAAAAAAAAmf2cbc5Z75cFb8CMip2r88q5fsjfqSibyAAAAAAAAAAAAAAAAAAAOkz1jhke1NpzWq5G9aomcAU6vd3kv12qrjNDFBNUvWSRkDVaxHLxwiqvH+ZoeEAAAAAAAABYX2fLi6q0nWUywRRMpanDZGJhZFc3aVXdapuTuwSiUSAAAAAAAAAAAAAAAAAAADcbSZ4Z3gU51DQrbL/c6NUx7iqljx8keuPsaGPAAAAAAAAAWQ5BaH8LoJsyph1VVSyeCYYn+1SUSMQAAAAAAAAAAAAAAAAAABwBVrldo/wfKNe2omEklbMn+ZjV/nk0NPAAAAAAAA5AtbyYUf4Hk+sMeMKtK2Re9yq7/uZo2gAAAAAAAAAAAAAAAAAAAAEccrmgrZdrNdL+6ORLnS0a7DmPw12yuUVzelUTKFgriUcAAAAAAA3zki0TQa3u9xp7k2VaeCnbInuZNhdpXonHuyKLLU8EdLBFBCxI4omIxjE4NaiYRPoZH0AAAAAAAAAAAAAAAAAAAABj9Q0f7QsFzpcZ99Syx472KBThv6UzxwaHIAAAAAAJu9nCjxBfqtU4vhhRe5HOX+aEomggAAAAAAAAAAAAAAAAAAAAAbulMp0p1gVH1vpmbSWpq23StXYa9XwvxufEq5aqeG7vRTQwIAAAAAALO8jemZtM6LiSqYsdVWyLVPjcmFYioiMRfnsoi+JKN5IAAAAAAAAAAAAAAAAAAAAAAELe0esSx2HCsWdHTIqIqbSNVG4z04zn7lghIoAAAADNaKSF2sLIlRse4/GxbfvcbONpOOd2ALeZzvznO/JkAAAAAAAAAAAAAAAAAAAAAAHECp3KNfk1LrW61zHbcPvfcwr/AIbPhb9cKviaGtAAAAAAVEVFRd6LuUC1PJXf/wCkOhbZM5+3PAz8LNv37TNyZ727K+JmjbQAAAAAAAAAAAAAAAAAAAAAIV5ZeVJESbT1mnRVVNmsqondHTE1U8yp3dZZBCZQAAAAAABs+gNdVehbwlTFmajlw2ppc4SRvWnU5OhfDgoForPeKO/W2GvoJ21FLK1HNe3o+Sp0L1opke0AAAAAAAAAAAAAAAAA8N3vlvsFN+IuVbBQw9Dp3o3PcnFfACMtSe0JbqPbistFJcJOCT1GYovBP1L9i4Ir1Jyk6h1VtMrbg9lM792pvyovFE3r4qpRrHAAAAAAAAAAA91ovlwsNT+It1bPRTdLoXq3PenBfECTtN+0JcKTZivVFHXx8Fnp8RS+Lf0r9iYJU03ykae1VssorixlQ792qfypfBF4+CqTBsyphcLuUAAAAAAAAAAAcKqIiqq4RN6qvQBpepOV/TWm1fGtZ+0Kpu73FFiTC/N36U+owRXqTl7vt02o7ZHFZ4F3I5n5kyp/EqYTwTxNYI5ra6puVS6oq6iWqndxlmer3L4qB8AAAAAAAAAAAAAAAHEDbtN8qmpNMbEcFe6qpW/u1Z+azHUirvTwUCVNN+0BaLhsRXemktUy7llZmWH7fEn0XvJgkq23SjvFMlRQ1UNZAv8AaQPR6eOOHiQeoAAAAAOFVERVVcInSoES6v5fqO2zS0tjpUuMrFVq1czlbDn+6ib3d+5C4Ik1Hr6/arVyXG4yvhXhTxflxJ/lTj45KNf4JjoAAAAAAAAAAAAAAAAAAAAAA9dtutbZqpKmgq5qKdP7SB6tXxxx8QJL017QF1oNiK80sd0hTcs0WIpk/wDF30QmCatM6ot2rrWyvts3vYVXZc1yYfG7pa5Ohf8A5CDLAAAGictGoFsOhapkb1ZUVzkpGKi78Lvev+lFTxLBWQoAAAAAAAAAAAAAAAAAAAAAAAAACSuQbUC2vWLre9+ILjEseznd7xvxMXvxtJ4iixhkAAFffaDv343U1Ha2OzHQw7b0/wAR+/7NRv1LBFZQAAAAAAAAAAAAAAAAAAAAAAAAAHqtlxltFypa6BcTU0rZmY62rkC4tDWRXGip6uBcw1EbZWL/AHXJlP5mR9wOr3siY58i7MbUVzlXoRN6r9AKe6kvL9Q6guNyeu+qndInyaq/CngmDQxoAAAAAAAAAAAAAAAAAAAAAAAAAAALK8h18/a2hIKdzsy2+R1Mv8P6mfZceBKJBINe1+24S6OusNrppKquni9xHHFja+JcOXf1NVRBXXmp1d2fq/L6jQc1Or+z9X5fUA5qdX9n6vy+oBzU6v7P1fl9QDmp1f2fq/L6gHNTq/s/V+X1AOanV/Z+r8vqAc1Or+z9X5fUA5qdX9n6vy+oBzU6v7P1fl9QDmp1f2fq/L6gHNTq/s/V+X1AOanV/Z+r8vqAc1Or+z9X5fUA5qdX9n6vy+oBzU6v7P1fl9QDmp1f2fq/L6gHNTq/s/V+X1AOanV/Z+r8vqAc1Or+z9X5fUA5qdX9n6vy+oBzU6v7P1fl9QDmp1f2fq/L6gHNTq/s/V+X1AOanV/Z+r8vqAkrkR05qHS11uMNztVRR0VVC1ySSbOykjV3cF6Ucv0JRMJAA4wAwAwAwAwAwAwAwAwAwAwAwAwAwAwAwAwAwAwAwAwAwAwAwAwAA5A//9k=",
					 "poi":{
						"name":"Test User",
						"dob":"01-05-1990",
						"gender":"M",
						"phone":"1234567890",
						"email":"asdf@gmail.com"
					 },
					 "poa":{
						"co":"User Father",
						"house":"101",
						"lm":"Hill Church",
						"lc":"Navya Colony",
						"vtc":"",
						"subdist":"Hindmata",
						"dist":"Mumbai",
						"state":"Maharashtra",
						"country":"India",
						"pc":"400001",
						"po":"Dadar East"
					 },
				 	"local-data":{},
				 	"raw-CmpResp":"Raw kyc compress data signed by UIDAI"
				},
				"aadhaar-id":"123456789012",
				"success":true,
				"aadhaar-reference-code":"3e8922e886e44d62a2317fc84b287e1b",
				"hash":"41f33fdc0aadeaeefe232b7918074de1743181e8ec1ed20963d412687de63dd0",
				"uuid":"test_uuid",
				"requestId":"test_requestid"
			 }';
		
		return $sample_success_response;
	}
}