<?php
//Created by harshada kulkarni on 24-07-2018
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function mobile_recharge($mobile_number, $recharge_amount, $comment, $params) {

    $cert = CERT;
    $passwd = CP_PASSWORD;
    $secKey = file_get_contents("private.key");
    $serverCert = file_get_contents("mycert.pem");
    $SD = CP_SD;
    $AP = CP_AP;
    $OP = CP_OP;
    $date = date("DMdY");
    $sessPrefix = mt_rand(100000, 999999);
    $sess = $date . $sessPrefix;
    $phNbr = $mobile_number;
    $amount = $recharge_amount;
    $extra_params = json_decode($params[0]['extra_params']);

    //echo "<pre>";print_r($params);die;

    $query_params = $extra_params->query_params;

    //echo $query_params;die;
    //$query_params = explode(" ",$query_params);
    //echo "<pre>"; print_r($query_params);die;

    $account = $extra_params->account;
    if ($comment == "") {
        $comment = $extra_params->comment;
    }
    $check_url = $params[0]['check_url'];
    $pay_url = $params[0]['pay_url'];
    $verify_url = $params[0]['verify_url'];

    $querString = "CERT=$cert\r\nSD=$SD\r\nAP=$AP\r\nOP=$OP\r\nSESSION=$sess\r\nNUMBER=$phNbr\r\nACCOUNT=$account\r\nAMOUNT=$amount\r\nCOMMENT=$comment";

    $pkeyid = openssl_pkey_get_private($secKey, $passwd);
    openssl_sign($querString, $signature, $pkeyid, OPENSSL_ALGO_SHA1);
    openssl_free_key($pkeyid);

    $encoded = base64_encode($signature);
    $encoded = chunk_split($encoded, 76, "\r\n");

    $signInMsg = "BEGIN\r\n" . $querString . "\r\nEND\r\nBEGIN SIGNATURE\r\n" . $encoded . "END SIGNATURE\r\n";
    $recharge_response_check = get_query_result($signInMsg, $check_url);

    if ($recharge_response_check) {

        $check_return_array = responseToArray($recharge_response_check);

        $result['check_result']['CERT'] = $check_return_array['CERT'];
        $result['check_result']['DATE'] = $check_return_array['DATE'];
        $result['check_result']['SESSION'] = $check_return_array['SESSION'];
        $result['check_result']['ERROR'] = $check_return_array['ERROR'];
        $result['check_result']['RESULT'] = $check_return_array['RESULT'];
        $result['check_result']['TRANSID'] = $check_return_array['TRANSID'];

        if ($check_return_array['ERROR'] == 0) {
            $recharge_response_pay = get_query_result($signInMsg, $pay_url);

            if ($recharge_response_pay) {

                $pay_return_array = responseToArray($recharge_response_pay);

                $result['pay_result']['CERT'] = $pay_return_array['CERT'];
                $result['pay_result']['DATE'] = $pay_return_array['DATE'];
                $result['pay_result']['SESSION'] = $pay_return_array['SESSION'];
                $result['pay_result']['ERROR'] = $pay_return_array['ERROR'];
                $result['pay_result']['RESULT'] = $pay_return_array['RESULT'];
                $result['pay_result']['TRANSID'] = $pay_return_array['TRANSID'];
                $result['pay_result']['AUTHCODE'] = $pay_return_array['AUTHCODE'];
                $result['pay_result']['TRNXSTATUS'] = $pay_return_array['TRNXSTATUS'];
            }
        }
    }
    return $result;
}

/**
 * @description - To get make bill transaction.
 * @param phone_number, account_number, comment, $params
 * @return int array
 * @added by Sachin on 14-08-2018
 */
function bbps_recharge($number, $account_number, $comment, $params, $operator, $recharge_amount, $authenticator='', $billdate = '', $duedate = '', $billnum ='', $custname = '') {

    $cert = BBPS_CERT;
    $passwd = BBPS_CP_PASSWORD;
    $secKey = file_get_contents("private.key");
    $serverCert = file_get_contents("mycert.pem");
    $SD = BBPS_CP_SD;
    $AP = BBPS_CP_AP;
    $OP = BBPS_CP_OP;
    $term_id = BBPS_CP_AP;
    $AgentId = BBPS_AGENTID;
    $channel = BBPS_CHANNEL;
    $IP = BBPS_IP_ADDR;
    $MAC = BBPS_MAC_ADDR;
    $terminal_id = BBPS_TERMINAL_ID;
    $date = date("DMdY");
    $sessPrefix = mt_rand(100000, 999999);
    $sess = $date . $sessPrefix;
    $numbr = $number;
    $account = $account_number;
    $amount = '10.00';
    $time = date('DMdY');
    if (!empty($recharge_amount)) {
        $amount = number_format($recharge_amount, 2, '.','');
    }

    if ($comment == "") {
        $comment = '';
    }
    $check_url = $params[0]['check_url'];
    $pay_url = $params[0]['pay_url'];
    $verify_url = $params[0]['verify_url'];

    $querString = "CERT=$cert\r\nSD=$SD\r\nAP=$AP\r\nOP=$OP\r\nSESSION=$sess\r\nAgentId=$AgentId\r\nNUMBER=$numbr\r\nACCOUNT=$account\r\nAuthenticator3=$authenticator\r\nAMOUNT=$amount\r\nAMOUNT_ALL=$amount\r\nTERM_ID=$term_id\r\nCOMMENT=$comment\r\nChannel=$channel\r\nIP=$IP\r\nMAC=$MAC\r\nTERMINAL_ID=$terminal_id";
    
    $pkeyid = openssl_pkey_get_private($secKey, $passwd);
    openssl_sign($querString, $signature, $pkeyid, OPENSSL_ALGO_SHA1);
    openssl_free_key($pkeyid);

    $encoded = base64_encode($signature);
    $encoded = chunk_split($encoded, 76, "\r\n");

    $signInMsg = "BEGIN\r\n" . $querString . "\r\nEND\r\nBEGIN SIGNATURE\r\n" . $encoded . "END SIGNATURE\r\n";
    $response_check = get_query_result($signInMsg, $check_url);
    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'landline_bbps.log', $response_check, 'response_check');
    $response_check = "BEGIN\r\nCERT=EF494107467CDC28D5CD3EEBC0F154BC63C3CD4A\r\nDATE=$time\r\nSESSION=$sess
        \r\nERROR=0\r\nRESULT=0\r\nTRANSID=1000797835960\r\nADDINFO=<$billnum> <$billdate> <$duedate> <$recharge_amount> <N> <$custname>
        \r\nEND\r\nBEGIN SIGNATURE\r\nWySzRVz0S9Blv+xb+Z9u2fKWwoP/QnIm1FlQESDPOyq7Yn4IDwmgNVT2mX5Oxl8z Ob8UtUcrOL37akv46kGsemC8RRBTJY5Dv/10MK5ka8CePmmm4O+VerXIGEKwnzAM wYHBGHSXIK/eSB7WE9RLLxVas3y0SqNQozgMSZXcTLY=\r\nEND SIGNATURE ";
   
    if ($response_check) { 

        $check_return_array = responseToArray($response_check);

        $result['check_result']['CERT'] = $check_return_array['CERT'];
        $result['check_result']['DATE'] = $check_return_array['DATE'];
        $result['check_result']['SESSION'] = $check_return_array['SESSION'];
        $result['check_result']['ERROR'] = $check_return_array['ERROR'];
        $result['check_result']['RESULT'] = $check_return_array['RESULT'];
        $result['check_result']['TRANSID'] = $check_return_array['TRANSID'];
        $result['check_result']['ADDINFO'] = $check_return_array['ADDINFO'];
        
        $addInfoString = $result['check_result']['ADDINFO'];
        $splitResponseArr = explodeAddInfo($addInfoString);
        //print_r($splitResponseArr);die;
        $result['check_result']['BILL_NUMBER'] = $splitResponseArr[0];
        $result['check_result']['BILL_DATE'] = $splitResponseArr[1];
        $result['check_result']['BILL_DUE_DATE'] = $splitResponseArr[2];
        $result['check_result']['AMOUNT'] = $splitResponseArr[3];
        $result['check_result']['CUSTOMER_NAME'] = $splitResponseArr[5];
        
        $result['check_result']['PRICE'] = $check_return_array['PRICE'];
        
        $result['number'] = $number;
        $result['account_number'] = $account_number;
        $result['comment'] = $comment;
        $result['operator'] = $operator;
        $result['authenticator'] = $authenticator;
        
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'bbps_recharge.log', $check_return_array, '$check_return_array');
        
        if (!empty($recharge_amount)) {
            if ($check_return_array['ERROR'] == 0) {

                $recharge_response_pay = get_query_result($signInMsg, $pay_url);
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'landline_bbps.log', $recharge_response_pay, 'recharge_response_pay');
                $recharge_response_pay = "BEGIN\r\nCERT=EF494107467CDC28D5CD3EEBC0F154BC63C3CD4A\r\nDATE=$date\r\nSESSION=$sess\r\nERROR=0\r\nRESULT=0\r\nTRANSID=1000797835960\r\AUTHCODE=CYB1000930628946\r\nTRNXSTATUS=7\r\nERRMSG=Success\r\nEND\r\nBEGIN SIGNATURE\r\nWySzRVz0S9Blv+xb+Z9u2fKWwoP/QnIm1FlQESDPOyq7Yn4IDwmgNVT2mX5Oxl8z Ob8UtUcrOL37akv46kGsemC8RRBTJY5Dv/10MK5ka8CePmmm4O+VerXIGEKwnzAM wYHBGHSXIK/eSB7WE9RLLxVas3y0SqNQozgMSZXcTLY=\r\nEND SIGNATURE ";
                
                if ($recharge_response_pay) {

                    $pay_return_array = responseToArray($recharge_response_pay);
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'bbps_recharge.log', $recharge_response_pay, 'recharge_response_pay');
                    $result['pay_result']['CERT'] = $pay_return_array['CERT'];
                    $result['pay_result']['DATE'] = $pay_return_array['DATE'];
                    $result['pay_result']['SESSION'] = $pay_return_array['SESSION'];
                    $result['pay_result']['ERROR'] = $pay_return_array['ERROR'];
                    $result['pay_result']['RESULT'] = $pay_return_array['RESULT'];
                    $result['pay_result']['TRANSID'] = $pay_return_array['TRANSID'];
                    $result['pay_result']['AUTHCODE'] = $pay_return_array['AUTHCODE'];
                    $result['pay_result']['TRNXSTATUS'] = $pay_return_array['TRNXSTATUS'];
                }
            }
        }
    }
    //print_r($result);
    return $result;
}

function responseToArray($convertArray) {
    $store_value = array();
    $split_response = explode("\r\n", $convertArray);
    foreach ($split_response as $sr) {
        $new_key = explode("=", $sr);
        $store_value[$new_key[0]] = $new_key[1];
    }
    return $store_value;
}

/**
 * @description - To explode additional information about bill
 * @param array of additional info
 * @return int array
 * @added by Sachin on 14-08-2018
 */
function explodeAddInfo($str) {
    $splitResponseArr = explode(" ", $str);
    foreach($splitResponseArr as $key => $value){
        $value = trim($value,'<');
        $value = trim($value,'>');
        $returnArr[] = $value;
      }
    return $returnArr;
}

function get_query_result($qs, $url) {
    $opts = array(
        'http' => array(
            'method' => "POST",
            'header' => array("Content-type: application/x-www-form-urlencoded\r\n" .
                "X-CyberPlat-Proto: SHA1RSA\r\n"),
            'content' => "inputmessage=" . urlencode($qs)
        )
    );
    $context = stream_context_create($opts);
    return file_get_contents($url, false, $context);
}

function check_signature($response, $serverCert) {
    $fields = preg_split("/END\r\nBEGIN SIGNATURE\r\n|END SIGNATURE\r\n|BEGIN\r\n/", $response, NULL, PREG_SPLIT_NO_EMPTY);
    if (count($fields) != 2) {
        echo "<br>";
        print "Bad response for check signature.\r\n";
        echo "<br>";
        return;
    }

    $pubkeyid = openssl_pkey_get_public($serverCert);
    $ok = openssl_verify(trim($fields[0]), base64_decode($fields[1]), $pubkeyid);
    echo "<br>";
    print "Signature is ";
    if ($ok == 1) {
        print "good";
    } elseif ($ok == 0) {
        print "bad";
    } else {
        print "ugly, error checking signature";
    }
    echo "<br>";
    openssl_free_key($pubkeyid);
}

function giftcard($postData,$params) {

    $cert = CERT;
    $passwd = CP_PASSWORD;
    $secKey = file_get_contents("private.key");
    $serverCert = file_get_contents("mycert.pem");
    $SD = CP_SD;
    $AP = CP_AP;
    $OP = CP_OP;
    $date = date("DMdY");
    $sessPrefix = mt_rand(100000, 999999);
    $sess = $date . $sessPrefix;
    
    $type = isset($postData['type']) ? $postData['type'] : 1;
    $category_id = $postData['category_id'];
    $sender_mobile_no = $postData['sender_mobile_no'];
    $sender_f_name = $postData['sender_f_name'];
    $sender_l_name = $postData['sender_l_name'];
    $sender_mail = $postData['sender_mail_name'];
    
    $receiver_mobile_no = RECEIVER_MOBILE_NO;
    $receiver_f_name = RECEIVER_FIRST_NAME;
    $receiver_l_name = RECEIVER_LAST_NAME;
    $receiver_mail = RECEIVER_MAIL_NAME;
    
    $amount = isset($postData['amount']) ? $postData['amount'] : '10.00';
    $amount_all = isset($postData['amount_all']) ? $postData['amount_all'] : '10.00';

    $product_id = $postData['product_id'];
    $price = isset($postData['price']) ? $postData['price'] : '100';
    $qty = $postData['qty'];
    $theme = $postData['theme'];
    
    $product_type = PRODUCT_TYPE;
    
    $check_url = $params[0]['check_url'];
    $pay_url = $params[0]['pay_url'];
    $verify_url = $params[0]['verify_url'];

    if($type == 1){
        $querString = "CERT=$cert\r\nSD=$SD\r\nAP=$AP\r\nOP=$OP\r\nSESSION=$sess\r\nType=$type\r\nAMOUNT=$amount\r\nAMOUNT_ALL=$amount_all";
    }else{
        $querString = "CERT=$cert\r\nSD=$SD\r\nAP=$AP\r\nOP=$OP\r\nSESSION=$sess\r\nType=$type\r\nCategory_id =$category_id\r\nNUMBER=$sender_mobile_no\r\nfName=$sender_f_name\r\nlName=$sender_l_name\r\nbilling_email=$sender_mail\r\nbenfName=$receiver_f_name\r\nbenlName=$receiver_l_name\r\nEmail=$receiver_mail\r\nbenMobile=$receiver_mobile_no\r\nAmount=$amount\r\nAMOUNT_ALL=$amount_all\r\nProduct_id=$product_id\r\nprice=$price\r\nqty=$qty\r\ntheme=$theme\r\nProducttype=$product_type";
    }
//    print_r($check_url);die;
    $pkeyid = openssl_pkey_get_private($secKey, $passwd);
    openssl_sign($querString, $signature, $pkeyid, OPENSSL_ALGO_SHA1);
    openssl_free_key($pkeyid);

    $encoded = base64_encode($signature);
    $encoded = chunk_split($encoded, 76, "\r\n");

    $signInMsg = "BEGIN\r\n" . $querString . "\r\nEND\r\nBEGIN SIGNATURE\r\n" . $encoded . "END SIGNATURE\r\n";
    $recharge_response_check = get_query_result($signInMsg, $check_url);
//    print_r($recharge_response_check);die;
    if ($recharge_response_check) {

        $check_return_array = responseToArray($recharge_response_check);

        $result['check_result']['CERT'] = $check_return_array['CERT'];
        $result['check_result']['DATE'] = $check_return_array['DATE'];
        $result['check_result']['SESSION'] = $check_return_array['SESSION'];
        $result['check_result']['ERROR'] = $check_return_array['ERROR'];
        $result['check_result']['RESULT'] = $check_return_array['RESULT'];
        $result['check_result']['TRANSID'] = $check_return_array['TRANSID'];

        if ($check_return_array['ERROR'] == 0) {
            $recharge_response_pay = get_query_result($signInMsg, $pay_url);

            if ($recharge_response_pay) {

                $pay_return_array = responseToArray($recharge_response_pay);

                $result['pay_result']['CERT'] = $pay_return_array['CERT'];
                $result['pay_result']['DATE'] = $pay_return_array['DATE'];
                $result['pay_result']['SESSION'] = $pay_return_array['SESSION'];
                $result['pay_result']['ERROR'] = $pay_return_array['ERROR'];
                $result['pay_result']['RESULT'] = $pay_return_array['RESULT'];
                $result['pay_result']['TRANSID'] = $pay_return_array['TRANSID'];
                $result['pay_result']['AUTHCODE'] = $pay_return_array['AUTHCODE'];
                $result['pay_result']['TRNXSTATUS'] = $pay_return_array['TRNXSTATUS'];
                $result['pay_result']['ADDINFO'] = $pay_return_array['ADDINFO'];
            }
        }
    }
    return $result;
}
