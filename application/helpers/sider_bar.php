<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <!--<img src="<?php echo base_url().PROFILE_IMAGE.$this->session->userdata('profile_image'); ?>" class="img-circle" alt="User Image" />-->
                <img src="<?php echo ($this->session->userdata('role_id') == RSRTC_USER_ROLE_ID) ? base_url().PROFILE_IMAGE.'rsrtc_logo.jpg' : $this->session->userdata('profile_image'); ?>" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p><?php echo ucwords($this->session->userdata("display_name")); ?></p>
                <p><?php echo ucwords($this->session->userdata("role_name")); ?></p>

                <!--<a href="#"><i class="fa fa-circle text-success"></i>  Agent  Booking</a>-->
            </div>
        </div>
        <!-- search form -->
		<!--
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>-->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li>
                   <a href="<?php echo  base_url('admin/dashboard') ?>">
                       <i class="fa fa-dashboard"></i>
                       <span>Dashboard</span>
                   </a>
            </li>
            <?php echo get_back_menu(); ?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<script>
    $(window).bind("load", function() {
        var urlmenu = window.location.pathname;
        if(urlmenu.substr(-1) == '/') {
            urlmenu = urlmenu.substr(0, urlmenu.length - 1);
        }
        
        var rest_url = urlmenu.substring(0, urlmenu.lastIndexOf("/") + 1);
        var url_last_part = urlmenu.substring(urlmenu.lastIndexOf("/") + 1, urlmenu.length);
        $('.sidebar-menu li a').each(function(){
            var sidebar_href = $(this).attr("href");
            if(sidebar_href.substr(-1) == '/') {
                sidebar_href = sidebar_href.substr(0, sidebar_href.length - 1);
            }
        
            var sidebar_rest_url = sidebar_href.substring(0, sidebar_href.lastIndexOf("/") + 1);
            var sidebar_url_last_part = sidebar_href.substring(sidebar_href.lastIndexOf("/") + 1, sidebar_href.length);
            if(sidebar_url_last_part == url_last_part)
            {
                $(this).parent("li").addClass('active');
                $(this).parents("li").addClass('active');
                $(this).parent("li").parents("ul").css({"display":"block"}); 
            }
        });
    })
</script>