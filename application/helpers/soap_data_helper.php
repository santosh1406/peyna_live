<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function get_soap_data($url, $func, $xml_str)
{
    $CI = & get_instance();    
    $CI->load->library("Nusoap_lib");
       
    $client = new nusoap_client($url,true);
    

    $client->operation = $func;
    $result = $client->send($xml_str,$func);
    
    return $result;
}


function get_soap_data_hrtc($url, $func, $xml_str, $action)
{
    $CI = &get_instance();    
    $CI->load->library("Nusoap_lib");
       
    $client = new nusoap_client($url,true);
    $client->operation = $func;
   
    $result = $client->send($xml_str,$action);
    
    return $result;
}


function get_soap_data_its($url, $func, $xml_str, $action)
{
    $CI = &get_instance();    
    $CI->load->library("Nusoap_lib");
       
    $client = new nusoap_client($url,true);
    $client->operation = $func;
   
    $result = $client->send($xml_str,$action);
    
    return $result;
}
// function add_soap_function($arr = NULL, $input, return)
// {
// 	$CI = & get_instance();

// 	if($arr == NULL)
// 	{
// 		return 'No parameter passed';
// 	}

// 	if()

// }

function register_soap_function($func, $input){
	$CI = & get_instance();

	$CI->nusoap_server->register($func, 
                                $input, 
                                // array("return" => "tns:".$func."Array"),
                                array("return" => "tns:".$func."Array"),  //output 
                                "urn:SOAPServerWSDL", 
                                false, 
                                "rpc", 
                                "encoded", 
                                "Upsrtc");
}