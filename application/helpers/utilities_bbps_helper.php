<?php
//Created by harshada kulkarni on 24-07-2018
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function mobile_recharge($mobile_number, $recharge_amount, $comment, $params) {

    $cert = CERT;
    $passwd = CP_PASSWORD;
    $secKey = file_get_contents("private.key");
    $serverCert = file_get_contents("mycert.pem");
    $SD = CP_SD;
    $AP = CP_AP;
    $OP = CP_OP;
    $date = date("DMdY");
    $sessPrefix = mt_rand(100000, 999999);
    $sess = $date . $sessPrefix;
    $phNbr = $mobile_number;
    $amount = $recharge_amount;
    $extra_params = json_decode($params[0]['extra_params']);

    //echo "<pre>";print_r($params);die;

    $query_params = $extra_params->query_params;

    //echo $query_params;die;
    //$query_params = explode(" ",$query_params);
    //echo "<pre>"; print_r($query_params);die;

    $account = $extra_params->account;
    if ($comment == "") {
        $comment = $extra_params->comment;
    }
    $check_url = $params[0]['check_url'];
    $pay_url = $params[0]['pay_url'];
    $verify_url = $params[0]['verify_url'];

    $querString = "CERT=$cert\r\nSD=$SD\r\nAP=$AP\r\nOP=$OP\r\nSESSION=$sess\r\nNUMBER=$phNbr\r\nACCOUNT=$account\r\nAMOUNT=$amount\r\nCOMMENT=$comment";

    $pkeyid = openssl_pkey_get_private($secKey, $passwd);
    openssl_sign($querString, $signature, $pkeyid, OPENSSL_ALGO_SHA1);
    openssl_free_key($pkeyid);

    $encoded = base64_encode($signature);
    $encoded = chunk_split($encoded, 76, "\r\n");

    $signInMsg = "BEGIN\r\n" . $querString . "\r\nEND\r\nBEGIN SIGNATURE\r\n" . $encoded . "END SIGNATURE\r\n";
    $recharge_response_check = get_query_result($signInMsg, $check_url);

    if ($recharge_response_check) {

        $check_return_array = responseToArray($recharge_response_check);

        $result['check_result']['CERT'] = $check_return_array['CERT'];
        $result['check_result']['DATE'] = $check_return_array['DATE'];
        $result['check_result']['SESSION'] = $check_return_array['SESSION'];
        $result['check_result']['ERROR'] = $check_return_array['ERROR'];
        $result['check_result']['RESULT'] = $check_return_array['RESULT'];
        $result['check_result']['TRANSID'] = $check_return_array['TRANSID'];

        if ($check_return_array['ERROR'] == 0) {
            $recharge_response_pay = get_query_result($signInMsg, $pay_url);

            if ($recharge_response_pay) {

                $pay_return_array = responseToArray($recharge_response_pay);

                $result['pay_result']['CERT'] = $pay_return_array['CERT'];
                $result['pay_result']['DATE'] = $pay_return_array['DATE'];
                $result['pay_result']['SESSION'] = $pay_return_array['SESSION'];
                $result['pay_result']['ERROR'] = $pay_return_array['ERROR'];
                $result['pay_result']['RESULT'] = $pay_return_array['RESULT'];
                $result['pay_result']['TRANSID'] = $pay_return_array['TRANSID'];
                $result['pay_result']['AUTHCODE'] = $pay_return_array['AUTHCODE'];
                $result['pay_result']['TRNXSTATUS'] = $pay_return_array['TRNXSTATUS'];
            }
        }
    }
    return $result;
}

/**
 * @description - To get make bill transaction.
 * @param phone_number, account_number, comment, $params
 * @return int array
 * @added by Sachin on 14-08-2018
 */
function bbps_recharge($number, $account_number, $comment, $params, $operator, $recharge_amount, $authenticator='', $billdate = '', $duedate = '', $billnum ='', $custname = '') {

    $cert = BBPS_CERT;
    $passwd = BBPS_CP_PASSWORD;
    $secKey = file_get_contents("private.key");
    $serverCert = file_get_contents("mycert.pem");
    $SD = BBPS_CP_SD;
    $AP = BBPS_CP_AP;
    $OP = BBPS_CP_OP;
    $term_id = BBPS_CP_AP;
    $AgentId = BBPS_AGENTID;
    $channel = BBPS_CHANNEL;
    $IP = BBPS_IP_ADDR;
    $MAC = BBPS_MAC_ADDR;
    $terminal_id = BBPS_TERMINAL_ID;
    $date = date("DMdY");
    $sessPrefix = mt_rand(100000, 999999);
    $sess = $date . $sessPrefix;
    $numbr = $number;
    $account = $account_number;
    $amount = '10.00';
    $time = date('DMdY');
    if (!empty($recharge_amount)) {
        $amount = number_format($recharge_amount, 2, '.','');
    }

    if ($comment == "") {
        $comment = '';
    }
    $check_url = $params[0]['check_url'];
    $pay_url = $params[0]['pay_url'];
    $verify_url = $params[0]['verify_url'];

    $querString = "CERT=$cert\r\nSD=$SD\r\nAP=$AP\r\nOP=$OP\r\nSESSION=$sess\r\nAgentId=$AgentId\r\nNUMBER=$numbr\r\nACCOUNT=$account\r\nAuthenticator3=$authenticator\r\nAMOUNT=$amount\r\nAMOUNT_ALL=$amount\r\nTERM_ID=$term_id\r\nCOMMENT=$comment\r\nChannel=$channel\r\nIP=$IP\r\nMAC=$MAC\r\nTERMINAL_ID=$terminal_id";
    
    $pkeyid = openssl_pkey_get_private($secKey, $passwd);
    openssl_sign($querString, $signature, $pkeyid, OPENSSL_ALGO_SHA1);
    openssl_free_key($pkeyid);

    $encoded = base64_encode($signature);
    $encoded = chunk_split($encoded, 76, "\r\n");

    $signInMsg = "BEGIN\r\n" . $querString . "\r\nEND\r\nBEGIN SIGNATURE\r\n" . $encoded . "END SIGNATURE\r\n";
    $response_check = get_query_result($signInMsg, $check_url);
    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'landline_bbps.log', $response_check, 'response_check');
    $response_check = "BEGIN\r\nCERT=EF494107467CDC28D5CD3EEBC0F154BC63C3CD4A\r\nDATE=$time\r\nSESSION=$sess
        \r\nERROR=0\r\nRESULT=0\r\nTRANSID=1000797835960\r\nADDINFO=<$billnum> <$billdate> <$duedate> <$recharge_amount> <N> <$custname>
        \r\nEND\r\nBEGIN SIGNATURE\r\nWySzRVz0S9Blv+xb+Z9u2fKWwoP/QnIm1FlQESDPOyq7Yn4IDwmgNVT2mX5Oxl8z Ob8UtUcrOL37akv46kGsemC8RRBTJY5Dv/10MK5ka8CePmmm4O+VerXIGEKwnzAM wYHBGHSXIK/eSB7WE9RLLxVas3y0SqNQozgMSZXcTLY=\r\nEND SIGNATURE ";
   
    if ($response_check) { 

        $check_return_array = responseToArray($response_check);

        $result['check_result']['CERT'] = $check_return_array['CERT'];
        $result['check_result']['DATE'] = $check_return_array['DATE'];
        $result['check_result']['SESSION'] = $check_return_array['SESSION'];
        $result['check_result']['ERROR'] = $check_return_array['ERROR'];
        $result['check_result']['RESULT'] = $check_return_array['RESULT'];
        $result['check_result']['TRANSID'] = $check_return_array['TRANSID'];
        $result['check_result']['ADDINFO'] = $check_return_array['ADDINFO'];
        
        $addInfoString = $result['check_result']['ADDINFO'];
        $splitResponseArr = explodeAddInfo($addInfoString);
        //print_r($splitResponseArr);die;
        $result['check_result']['BILL_NUMBER'] = $splitResponseArr[0];
        $result['check_result']['BILL_DATE'] = $splitResponseArr[1];
        $result['check_result']['BILL_DUE_DATE'] = $splitResponseArr[2];
        $result['check_result']['AMOUNT'] = $splitResponseArr[3];
        $result['check_result']['CUSTOMER_NAME'] = $splitResponseArr[5];
        
        $result['check_result']['PRICE'] = $check_return_array['PRICE'];
        
        $result['number'] = $number;
        $result['account_number'] = $account_number;
        $result['comment'] = $comment;
        $result['operator'] = $operator;
        $result['authenticator'] = $authenticator;
        
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'bbps_recharge.log', $check_return_array, '$check_return_array');
        
        if (!empty($recharge_amount)) {
            if ($check_return_array['ERROR'] == 0) {

                $recharge_response_pay = get_query_result($signInMsg, $pay_url);
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'landline_bbps.log', $recharge_response_pay, 'recharge_response_pay');
                $recharge_response_pay = "BEGIN\r\nCERT=EF494107467CDC28D5CD3EEBC0F154BC63C3CD4A\r\nDATE=$date\r\nSESSION=$sess\r\nERROR=0\r\nRESULT=0\r\nTRANSID=1000797835960\r\AUTHCODE=CYB1000930628946\r\nTRNXSTATUS=7\r\nERRMSG=Success\r\nEND\r\nBEGIN SIGNATURE\r\nWySzRVz0S9Blv+xb+Z9u2fKWwoP/QnIm1FlQESDPOyq7Yn4IDwmgNVT2mX5Oxl8z Ob8UtUcrOL37akv46kGsemC8RRBTJY5Dv/10MK5ka8CePmmm4O+VerXIGEKwnzAM wYHBGHSXIK/eSB7WE9RLLxVas3y0SqNQozgMSZXcTLY=\r\nEND SIGNATURE ";
                
                if ($recharge_response_pay) {

                    $pay_return_array = responseToArray($recharge_response_pay);
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'bbps_recharge.log', $recharge_response_pay, 'recharge_response_pay');
                    $result['pay_result']['CERT'] = $pay_return_array['CERT'];
                    $result['pay_result']['DATE'] = $pay_return_array['DATE'];
                    $result['pay_result']['SESSION'] = $pay_return_array['SESSION'];
                    $result['pay_result']['ERROR'] = $pay_return_array['ERROR'];
                    $result['pay_result']['RESULT'] = $pay_return_array['RESULT'];
                    $result['pay_result']['TRANSID'] = $pay_return_array['TRANSID'];
                    $result['pay_result']['AUTHCODE'] = $pay_return_array['AUTHCODE'];
                    $result['pay_result']['TRNXSTATUS'] = $pay_return_array['TRNXSTATUS'];
                }
            }
        }
    }
    //print_r($result);
    return $result;
}

function responseToArray($convertArray) {
    $store_value = array();
    $split_response = explode("\r\n", $convertArray);
    foreach ($split_response as $sr) {
        $new_key = explode("=", $sr);
        $store_value[$new_key[0]] = $new_key[1];
    }
    return $store_value;
}

/**
 * @description - To explode additional information about bill
 * @param array of additional info
 * @return int array
 * @added by Sachin on 14-08-2018
 */
function explodeAddInfo($str) {
    $splitResponseArr = explode(" ", $str);
    foreach($splitResponseArr as $key => $value){
        $value = trim($value,'<');
        $value = trim($value,'>');
        $returnArr[] = $value;
      }
    return $returnArr;
}

function get_query_result($qs, $url) {
    $opts = array(
        'http' => array(
            'method' => "POST",
            'header' => array("Content-type: application/x-www-form-urlencoded\r\n" .
                "X-CyberPlat-Proto: SHA1RSA\r\n"),
            'content' => "inputmessage=" . urlencode($qs)
        )
    );
    $context = stream_context_create($opts);
    return file_get_contents($url, false, $context);
}

function check_signature($response, $serverCert) {
    $fields = preg_split("/END\r\nBEGIN SIGNATURE\r\n|END SIGNATURE\r\n|BEGIN\r\n/", $response, NULL, PREG_SPLIT_NO_EMPTY);
    if (count($fields) != 2) {
        echo "<br>";
        print "Bad response for check signature.\r\n";
        echo "<br>";
        return;
    }

    $pubkeyid = openssl_pkey_get_public($serverCert);
    $ok = openssl_verify(trim($fields[0]), base64_decode($fields[1]), $pubkeyid);
    echo "<br>";
    print "Signature is ";
    if ($ok == 1) {
        print "good";
    } elseif ($ok == 0) {
        print "bad";
    } else {
        print "ugly, error checking signature";
    }
    echo "<br>";
    openssl_free_key($pubkeyid);
}

function giftcard($postData,$params) {

    $cert = CERT;
    $passwd = CP_PASSWORD;
    $secKey = file_get_contents("private.key");
    $serverCert = file_get_contents("mycert.pem");
    $SD = CP_SD;
    $AP = CP_AP;
    $OP = CP_OP;
    $date = date("DMdY");
    $sessPrefix = mt_rand(100000, 999999);
    $sess = $date . $sessPrefix;
    
    $type = isset($postData['type']) ? $postData['type'] : 1;
    $category_id = $postData['category_id'];
    $sender_mobile_no = $postData['sender_mobile_no'];
    $sender_f_name = $postData['sender_f_name'];
    $sender_l_name = $postData['sender_l_name'];
    $sender_mail = $postData['sender_mail_name'];
    
    $receiver_mobile_no = RECEIVER_MOBILE_NO;
    $receiver_f_name = RECEIVER_FIRST_NAME;
    $receiver_l_name = RECEIVER_LAST_NAME;
    $receiver_mail = RECEIVER_MAIL_NAME;
    
    $amount = isset($postData['amount']) ? $postData['amount'] : '10.00';
    $amount_all = isset($postData['amount_all']) ? $postData['amount_all'] : '10.00';

    $product_id = $postData['product_id'];
    $price = isset($postData['price']) ? $postData['price'] : '100';
    $qty = $postData['qty'];
    $theme = $postData['theme'];
    
    $product_type = PRODUCT_TYPE;
    
    $check_url = $params[0]['check_url'];
    $pay_url = $params[0]['pay_url'];
    $verify_url = $params[0]['verify_url'];

    if($type == 1){
        $querString = "CERT=$cert\r\nSD=$SD\r\nAP=$AP\r\nOP=$OP\r\nSESSION=$sess\r\nType=$type\r\nAMOUNT=$amount\r\nAMOUNT_ALL=$amount_all";
    }else{
        $querString = "CERT=$cert\r\nSD=$SD\r\nAP=$AP\r\nOP=$OP\r\nSESSION=$sess\r\nType=$type\r\nCategory_id =$category_id\r\nNUMBER=$sender_mobile_no\r\nfName=$sender_f_name\r\nlName=$sender_l_name\r\nbilling_email=$sender_mail\r\nbenfName=$receiver_f_name\r\nbenlName=$receiver_l_name\r\nEmail=$receiver_mail\r\nbenMobile=$receiver_mobile_no\r\nAmount=$amount\r\nAMOUNT_ALL=$amount_all\r\nProduct_id=$product_id\r\nprice=$price\r\nqty=$qty\r\ntheme=$theme\r\nProducttype=$product_type";
    }
//    print_r($check_url);die;
    $pkeyid = openssl_pkey_get_private($secKey, $passwd);
    openssl_sign($querString, $signature, $pkeyid, OPENSSL_ALGO_SHA1);
    openssl_free_key($pkeyid);

    $encoded = base64_encode($signature);
    $encoded = chunk_split($encoded, 76, "\r\n");

    $signInMsg = "BEGIN\r\n" . $querString . "\r\nEND\r\nBEGIN SIGNATURE\r\n" . $encoded . "END SIGNATURE\r\n";
    $recharge_response_check = get_query_result($signInMsg, $check_url);
//    print_r($recharge_response_check);die;
    if ($recharge_response_check) {

        $check_return_array = responseToArray($recharge_response_check);

        $result['check_result']['CERT'] = $check_return_array['CERT'];
        $result['check_result']['DATE'] = $check_return_array['DATE'];
        $result['check_result']['SESSION'] = $check_return_array['SESSION'];
        $result['check_result']['ERROR'] = $check_return_array['ERROR'];
        $result['check_result']['RESULT'] = $check_return_array['RESULT'];
        $result['check_result']['TRANSID'] = $check_return_array['TRANSID'];

        if ($check_return_array['ERROR'] == 0) {
            $recharge_response_pay = get_query_result($signInMsg, $pay_url);

            if ($recharge_response_pay) {

                $pay_return_array = responseToArray($recharge_response_pay);

                $result['pay_result']['CERT'] = $pay_return_array['CERT'];
                $result['pay_result']['DATE'] = $pay_return_array['DATE'];
                $result['pay_result']['SESSION'] = $pay_return_array['SESSION'];
                $result['pay_result']['ERROR'] = $pay_return_array['ERROR'];
                $result['pay_result']['RESULT'] = $pay_return_array['RESULT'];
                $result['pay_result']['TRANSID'] = $pay_return_array['TRANSID'];
                $result['pay_result']['AUTHCODE'] = $pay_return_array['AUTHCODE'];
                $result['pay_result']['TRNXSTATUS'] = $pay_return_array['TRNXSTATUS'];
                $result['pay_result']['ADDINFO'] = $pay_return_array['ADDINFO'];
            }
        }
    }
    return $result;
}

//added by sonali on 17-may-19 for BBPS 
    if(!function_exists('encode_url')){
    function encode_url($baseurl,$curl_url){
        
        $encodeurl = urlencode($curl_url);
        $encodedurl = $baseurl . $encodeurl;
        
        return $encodedurl;
        
    }
}

     if (!function_exists('getbillertags')) {

        function getbillertags($tags, $type, $operatorId, $account_num, $mobile_num,$email ='', $consumer_email = '', $consumer_mob = '', $amount = '', $acc_num ='', $city ='') {
            $biller_tags = '';
            
            $billertags = explode(";", $tags);
            $biller_tags .= ' <billDetails><Biller id="' . $operatorId . '"/> <CustomerParams>';
            
            switch ($type) {
                case 'Gas':
                    foreach ($billertags as $key => $tags) {
                     if(!empty($tags)){
                            $biller_tags .= '<Tag name="' . $tags . '" value="' . $account_num . '"/>';
                     }
                    }
                    $biller_tags .= ' </CustomerParams> </billDetails> ';
                    return $biller_tags;
                    
                    break;
                case 'Broadband Postpaid':
                    foreach ($billertags as $key => $tags) {
                     if(!empty($tags)){
                       if ($tags == 'Mobile Number') {
                            $biller_tags .= '<Tag name="' . $tags . '" value="' . $mobile_num . '"/>';
                        } else {
                            $biller_tags .= '<Tag name="' . $tags . '" value="' . $account_num . '"/>';
                        }   
                     }
                    }
                    $biller_tags .= ' </CustomerParams> </billDetails> ';
                    return $biller_tags;
                    
                    break;
                case 'Mobile Postpaid':
                    foreach ($billertags as $key => $tags) {
                     if(!empty($tags)){
                       if ($tags == 'Mobile Number') {
                            $biller_tags .= '<Tag name="' . $tags . '" value="' . $mobile_num . '"/>';
                        }
                      }
                    }
                    $biller_tags .= ' </CustomerParams> </billDetails> ';
                    return $biller_tags;
                    
                    break;
                case 'Water':
                    foreach ($billertags as $key => $tags) {
                     if(!empty($tags)){
                       if ($tags == 'Mobile Number') {
                            $biller_tags .= '<Tag name="' . $tags . '" value="' . $mobile_num . '"/>';
                        }else if($tags == 'Consumer Email ID' || $tags == 'Email Id'){
                            $biller_tags .= '<Tag name="' . $tags . '" value="' . $consumer_email . '"/>';
                        }else if($tags == 'Consumer Mobile No'){
                            $biller_tags .= '<Tag name="' . $tags . '" value="' . $consumer_mob . '"/>';
                        }else{
                            $biller_tags .= '<Tag name="' . $tags . '" value="' . $account_num . '"/>';
                        } 
                       }
                    }
                    $biller_tags .= ' </CustomerParams> </billDetails> ';
                    return $biller_tags;
                    
                    break;
                case 'DTH' :
                     foreach ($billertags as $key => $tags) {
                     if(!empty($tags)){
                        if ($tags == 'Amount') {
                            $biller_tags .= '<Tag name="' . $tags . '" value="' . $amount . '"/>';
                        }else{
                            $biller_tags .= '<Tag name="' . $tags . '" value="' . $account_num . '"/>';
                        }  
                     }
                    }
                    $biller_tags .= ' </CustomerParams> </billDetails> ';
                    return $biller_tags;
                    break;
                case 'Electricity':
                     foreach ($billertags as $key => $tags) {
                     if(!empty($tags)){
                       if ($tags == 'Subdivision Code' || $tags == 'BU') {
                           $biller_tags .= '<Tag name="' . $tags . '" value="' . $acc_num . '"/>';
                        }else if($tags == 'Mobile Number'){
                            $biller_tags .= '<Tag name="' . $tags . '" value="' . $mobile_num . '"/>';
                        }else if($tags == 'City'){
                            $biller_tags .= '<Tag name="' . $tags . '" value="' . $city . '"/>';
                        }else{
                            $biller_tags .= '<Tag name="' . $tags . '" value="' . $account_num . '"/>';
                        } 
                    }
                }
                    $biller_tags .= ' </CustomerParams> </billDetails> ';
                    return $biller_tags;
                    break;
                case 'Landline Postpaid':
                     foreach ($billertags as $key => $tags) {
                     if(!empty($tags)){
                       if ($tags == 'Telephone Number') {
                            $biller_tags .= '<Tag name="' . $tags . '" value="' . $mobile_num . '"/>';
                        }else if($tags == 'Number with STD Code (without 0)' || $tags == 'Landline Number with STD Code (without 0)'){
                            
                            $biller_tags .= '<Tag name="' . $tags . '" value="' . $mobile_num . '"/>';
                        }else if($tags == 'Landline Number with STD code'){
                            
                            $biller_tags .= '<Tag name="' . $tags . '" value="' . $account_num . '"/>';
                        }else{
                            
                            $biller_tags .= '<Tag name="' . $tags . '" value="' . $account_num . '"/>';
                        } 
                       }
                    }
                    $biller_tags .= ' </CustomerParams> </billDetails> ';
                    return $biller_tags;
                    break;
                
                default:
                    # code...
                    break;

            }
        }

    }

    if (!function_exists('curlPostData')) {

        function curlPostData($curl_url, $input = '', $header) {
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $curl_url);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $input);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            //curl_setopt($ch, CURLOPT_USERPWD, $sessionData['username'] . ":" . $sessionData['password']);
            //curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            $response = curl_exec($ch);  
//            print_r($response);
//            die;
            $err = curl_error($ch);
            curl_close($ch);
            
            if($err){
                return 'Error fetching response : '.$err;
            }else{
                return $response;
            }
        }

    }
    
    /**
    * @description - Load bbps payment request. 
    * @added by Vaidehi on 18-12-2018
    * @edited by sonali
    */
    
    if (!function_exists('recharge_with_bbps')) {

    function recharge_with_bbps($input, $getOperatorDetails, $adhoc, $billertags) {
       // show($input,1);
        $curl_url = '';
        $response = '';
        $quickpay = 'Yes';
        $refTag = '';
        $unique_system_ref = uniqid();
        $details['unique_system_ref'] = $unique_system_ref;
        $details['account_number'] = $input['number'];
        $details['mobile_number'] = $mobile = $input['mobile_number'];
        $details['comments'] = $input['comments'];
        //$details['op_details'] = $getOperatorDetails;
        $details['recharge_amount'] =  convert_into_paise1($input['recharge_amount']); 
        $details['bill_amount'] =  $input['bill_amount'];
        $details['authenticator'] = $input['authenticator'];
        $details['bill_number'] = $input['bill_number_val'];
        $details['due_date'] = $input['dueDate'];
        $details['bill_date'] = $input['billDate'];
        $details['billNumber'] = $input['billNumber'];
        $details['customer_name'] = $input['customerName'];
        $details['ref_id'] = $input['refID'];
        $details['operator_id'] = $biller = $input['operator_id'];
        $details['useremail'] = $input['useremail'];
        $password  = BBPS_PASSWORD;
        
        $header = array(
            'Content-Type: application/xml'
        );
        // show($input, 1);
        
       if(strtolower($adhoc) == 'false'){
            $quickpay = 'No';
            $refTag ='<refId>'.$details['ref_id'].'</refId>';
       }
        
        $url = '<ns2:PaymentRequest xmlns:ns2="http://bbps.rssoftware.co.in/schema"><paymentInformation>'.
            '<Tag name="Remarks" value="Remarks"/></paymentInformation>'.  
            '<amount>'.     
            '<amount>'.$details['recharge_amount'].'</amount>'.    
            '<splitPayAmount>0</splitPayAmount>'.  
            '<customerConvinienceFee>0</customerConvinienceFee>'. 
            '</amount>'.    
            '<paymentMethod paymentMode="Cash" splitPay="No" quickPay="'.$quickpay.'"/>'.$billertags.' <agent id="'.BBPS_AGENT_ID.'">'.  
                '<Device>'.       
                '<Tag name="INITIATING_CHANNEL" value="AGT"/>'.      
                '<Tag name="POSTAL_CODE" value="400013"/>'.     
                '<Tag name="TERMINAL_ID" value="1111111111"/>'.   
                '<Tag name="MOBILE" value="9920980050"/>'.    
                '<Tag name="GEOCODE" value="19.2303,72.8264"/>'.       
                '</Device>'.   
            '</agent>'.  
            '<customer mobile="'.$details['mobile_number'].'"/>'.    
            '<riskScores>'.        
            '<Score provider="IT01" type="TXNRISK" value="000"/> </riskScores>'.
            $refTag.
            '<finTxnDetails><txnRefId>'.$input['user_id']  . 'BBPS' . time().'</txnRefId>'.      
            '<paymentDetails>'.$details['bill_amount'].'</paymentDetails>'.  
            '<paymentStatus>Success</paymentStatus>'. 
            '</finTxnDetails><dealerCode>'.BBPS_DEALER_CODE.'</dealerCode><username>'.BBPS_USERNAME.'</username><password>'.$password.'</password></ns2:PaymentRequest>';
        
       log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'bbps_reposnse.log', $url, 'BBPS Recharge - url');
        $curl_url = encode_url(ITZCASHURL.'?requestType=BILLPAY&email='.$details['useremail'].'&custMobile='.$details['mobile_number'].'&payType=B2B&billpayrequest=',$url).'&sourceId={'.SOURCEID.'}';
        // show($curl_url,1);       
       // $response = curlPostData($curl_url, '', $header);
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'bbps_reposnse.log', $response, 'BBPS Recharge - curl API response');
        $response = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'.
                    '<ns2:PaymentReceipt xmlns:ns2="http://bbps.rssoftware.co.in/schema">'.
                        '<customerName>ARVIND SHIVAJI JADHAV</customerName>'.
                        '<transactionRefId>IT0107614930</transactionRefId>'.
                        '<transactionDateTime>2019-05-10+05:30</transactionDateTime>'.
                        '<billDate>2019-04-26+05:30</billDate>'.
                        '<billerId>MAHA00000MAH01</billerId>'.
                        '<billNumber>314412919051010</billNumber>'.
                        '<billPeriod>MONTHLY</billPeriod>'.
                        '<billerName>Maharashtra State Electricity Distbn Co Ltd</billerName>'.
                        '<amount>26000</amount>'.
                        '<ccf>0</ccf>'.
                        '<paymentChannel>Agent</paymentChannel>'.
                        '<paymentMode>Cash</paymentMode>'.
                        '<customerMobile>9892650971</customerMobile>'.
                        '<refId>IT010000000000000000000000007614908</refId>'.
                        '<clientRefId>BBPS00000185904</clientRefId>'.
                    '</ns2:PaymentReceipt>';
        
        //$response = 'Error:Account(s) is either blocked/inactive/not found or there is insufficient balance in the account(s).';
       
        return $response;
        
        
    }

}

function convert_into_paise1($rupees)
    {
        $paise = $rupees * 100;
        return $paise;
    }
    
    if(!function_exists(checkStatus)){
        function checkStatus($id,$reason){
          if($reason == 'Successful'){
            $action = '<a href="test/'.base64_encode($id).'" ref="'.$id.'" class="view_details">View Details</a>';
          }
          
          return $action;
       }
    }
    