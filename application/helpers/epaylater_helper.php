<?php

function epay_request_arr($data)
{
    $requestData = array();
    $uniqueId = substr(hexdec(uniqid()), 4, 12);
    $requestData['redirectType'] = 'WEBPAGE';
    $requestData['marketplaceOrderId'] = $data['trimax_epay_transaction_id'];
    //$requestData['merchantOrderId'] = $data['trimax_epay_transaction_id'];
    $requestData['mCode'] = EPAY_MERCHANT_CODE;
    $requestData['callbackUrl'] = base_url() . 'admin/Epaylater/epay_redirect/';
    $requestData['customerEmailVerified'] = FALSE;
    $requestData['customerTelephoneNumberVerified'] = TRUE;
    $requestData['customerLoggedin'] = TRUE;
    $requestData['amount'] = convert_into_paise($data['amount']);
    $requestData['currencyCode'] = 'INR';
    $requestData['date'] = $data['epay_trans_date'];
    $requestData['category'] = $data['category'];
    $requestData['customer'] = get_customer_info($data['customer_mobile']);
    $requestData['device'] = get_client_device_info($data['device_client']);
    $requestData['marketplaceSpecificSection'] = marketplace_specific_section($uniqueId);

    return $requestData;
}

function get_current_date($format = null)
{
    if (strtoupper($format) == 'ISO8601') {
        return date(DATE_ISO8601);
    } else {
        return date('Y-m-d H:i:s');
    }
}

function get_customer_info($mobile)
{
    return array(
        'firstName' => true,
        'telephoneNumber' => $mobile
    );
}

function get_client_device_info($device_client)
{
    return array(
        'deviceType' => 'DESKTOP',
        'deviceClient' => $device_client,
	'deviceNumber' => $_SERVER['REMOTE_ADDR']
    );
}

function marketplace_specific_section($uniqueId)
{
    return array(
        'marketplaceCustomerId' => $uniqueId,
    );
}

function convert_into_paise($rupees)
{
    $paise = $rupees * 100;
    return $paise;
}

function convert_into_rupees($paise)
{
    $rupees = $paise / 100;
    return $rupees;
}

function get_current_formatted_date($timeStamp = null, $flag = 0)
{
    if ($timeStamp) {
        $currentDate = new DateTime(date('r', $timeStamp));
    } else {
        $currentDate = new DateTime(date('r', strtotime("now")));
    }

    if ($flag) {
        return $currentDate->format('Y-m-d\TH:i:s\Z');
    } else {
        return $currentDate->format('Y-m-d H:i:s');
    }
}

function convert_date_to_mysql_format($date_str)
{
    if ($date_str) {
        $date = str_replace('/', '-', $date_str);
        return date('Y-m-d', strtotime($date));
    }
    return false;
}

function get_image_name($str)
{
    if (strtoupper($str) == 'YES')
        return 'verify.png';
    elseif (strtoupper($str) == 'NO')
        return 'notverify.png';
    else
        return 'error.png';
}
