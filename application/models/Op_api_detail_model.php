<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Op_api_detail_model extends MY_Model {

    var $table  = 'op_api_detail';
    var $fields = array("op_api_detail_id","op_api_id","op_id","url","username","password","is_status","is_deleted","inserted_by","updated_by","inserted_date","updated_date");
    var $key    = 'op_api_detail_id';

    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
     function get_api_details_all_data($data) 
    {
        $this->db->select('op_api_detail_id,op_api_id,op_id,url,username,password');
        $this->db->from('op_api_detail op');
        $this->db->where('op_api_detail_id', $data['api_id']);
        $this->db->where('is_deleted', '0');
        $result_data = $this->db->get();
        return $result_data->result_array();
    }
     
}
?>