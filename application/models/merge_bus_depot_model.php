<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Merge_bus_depot_model extends CI_Model {
    var $table  = 'cb_bus_stops';
    var $fields = array("BUS_STOP_CD", "BUS_STOP_NM");
    // var $key    = 'BUS_STOP_CD';
    
    function __construct() {
        parent::__construct();
	    //$this->db2 = $this->load->database('local_up_db', TRUE);	
    }

    public function get_all_data(){
         $data = $this->db->query("SELECT BUS_STOP_CD, BUS_STOP_NM, DEPOT_CD
                FROM cb_bus_stops");        

        return $data->result();
    }

    public function getBusStopInfo($bus_stop_cd){
        $data = $this->db->query("SELECT BUS_STOP_CD, BUS_STOP_NM, DEPOT_CD
                FROM cb_bus_stops where BUS_STOP_CD = '$bus_stop_cd'");        

        return $data->result_array();
    }

    public function getAllBusStops($search = ''){

        $msrtc_replication = MSRTC_REPLICATION;
        $data = $this->db->query("SELECT 
            CONCAT(t1.BUS_STOP_NM, ' (', t1.BUS_STOP_CD, ')') as BUS_STOP_NM, 
            t1.BUS_STOP_CD
                FROM " . $msrtc_replication . ".bus_stops t1
                WHERE NOT EXISTS (SELECT t2.BUS_STOP_CD FROM cb_bus_stops t2 WHERE t1.BUS_STOP_CD = t2.BUS_STOP_CD)
                AND (t1.BUS_STOP_CD LIKE '" . $search . "%'
                OR t1.BUS_STOP_NM LIKE '" . $search . "%'
                )
                ");

        return $data->result_array();
    }

    public function getAllBusDepots($search = ''){
        $msrtc_replication = MSRTC_REPLICATION;
        $data = $this->db->query("SELECT t1.DEPOT_CD, 
            CONCAT(t1.DEPOT_NM, ' (', t1.DEPOT_CD, ')') as DEPOT_NM
                FROM " . $msrtc_replication . ".depots t1
                WHERE t1.DEPOT_NM LIKE '" . $search . "%'
                OR t1.DEPOT_CD LIKE '" . $search . "%'
                ");        

        return $data->result_array();
    }

    public function addBusStop($input){
        $msrtc_replication = MSRTC_REPLICATION;
        $data = $this->db->from($msrtc_replication . '.bus_stops')
                            ->where('BUS_STOP_CD', $input['bus_stops'])
                            ->get()
                            ->result_array()[0];

        return $data;
    }

    public function insert_data($data){

        $data = $this->db->insert('cb_bus_stops', $data);
        if($data){
            return true;
        }else{
            return false;
        }
    }

    public function update_data($input){

        $bus_stop_cd = $input['bus_stop_cd'];
        $old_bus_depot = $input['old_bus_depot'];
        $new_depot_cd = $input['depot_cd']; 

        if($input['depot_cd'] != ''){
            $id = $this->db->query("UPDATE cb_bus_stops 
            SET DEPOT_CD = '" . $new_depot_cd . "'
            ,UPDATED_DT = '" . $input['UPDATED_DT'] . "'
            ,UPDATED_BY = '" . $input['UPDATED_BY'] . "'
            WHERE BUS_STOP_CD = '$bus_stop_cd'");        

            if ($id > 0) {
                    $data["flag"] = '@#success#@';
                    $data["msg_type"] = 'success';
                    $data["msg"] = 'Depot Updated Successfully.';
            }else {
                    $data["flag"] = '@#failed#@';
                    $data["msg_type"] = 'error';
                    $data["msg"] = 'Depot Could Not Be Edited ';
            }  
        }else{
            $data["flag"] = '@#failed#@';
            $data["msg_type"] = 'error';
            $data["msg"] = 'Depot cannot be empty ';
        }
        
        echo json_encode($data);
    }

    public function delete_record($bus_stop_cd){
        $id = $this->db->query("DELETE FROM cb_bus_stops WHERE BUS_STOP_CD = '" . $bus_stop_cd . "'"); 

        return $id;
    }
    
}
?>