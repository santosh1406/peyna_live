<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Disbursement_details_model extends MY_Model {
    
    var $table  = 'incentive_disbursement_detail';
    var $fields = array("id","disbursement_id" ,"incentive_id","slab_id","incentive_value");
    var $key    = "id";
 
    public function __construct() {
        parent::__construct();
        $this->_init();
    }
  }  
  ?>
