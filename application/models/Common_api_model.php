<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common_api_model extends MY_Model
{
	function __construct() {
		parent::__construct();
	}

       public function get_passcategory() {
        $this->db->select('id,category_code,category_name');
        $this->db->from('ps_pass_category');
        $this->db->where(array('is_active' => '1'));
        $query = $this->db->get();
        return $result = $query->result_array();
    }
    
     public function insertData($data,$tableName){
        $this->db->insert($tableName,$data);
        return $this->db->insert_id();
    }

    public function updateLTPOSResponse($data){
        $this->db->where('request_id',$data['requestId']);
        $result = $this->db->update('api_request_response',array('response'=>$data['response']));
        return $result;
    }
    
    public function getDashboardDetails($user_name) {
        
        $data['trans_details'] = $this->getTransCountByTransType($user_name);
        //show($data,1);
        $result['reg_amt'] = 0; //REGISTRATION OF OTC TRAVEL CARD 
        $result['reg_count'] = 0;

        $result['sw_topup_amt'] = 0; //SHOPPING WALLET LOAD 
        $result['sw_topup_count'] = 0;

        $result['tpts_topup_amt'] = 0;  //travel wallet by sw
        $result['tpts_topup_count'] =0 ; 

        $result['tw_topup_amt'] = 0; //TRAVEL WALLET LOAD VIA CASH
        $result['tw_topup_count'] = 0;

        $result['bpr_topup_amt'] = 0;  //BUS PASS RENEWAL VIA CASH
        $result['bpr_topup_count'] = 0;

        $result['ppc_reg_amt'] = 0; //REGISTRATION OF PERSONALIZED CARD 
        $result['ppc_reg_count'] = 0;

        $result['bpsw_topup_amt'] = 0;  //BUS PASS RENEWAL VIA WALLET
        $result['bpsw_topup_count'] = 0;

        $result['ri_amt'] = 0; //REISSUE PHYSICALLY PRESONALIZED CARDS
        $result['ri_count'] = 0;

        $result['riot_amt'] = 0; //REISSUE OTC TRAVEL CARDS
        $result['riot_count'] = 0;

        $result['ra_amt'] = 0; //CARD REACTIVATION SD   
        $result['ra_count'] = 0;

        $result['sd_amt'] = 0; //SURRENDER PERSONALIZED CARD
        $result['sd_count'] = 0;

        $result['sdot_amt'] = 0; // SURRENDER OTC CARD
        $result['sdot_count'] = 0;

        $result['uc_amt'] = 0; // UPGRADE CARD 
        $result['uc_count'] = 0;

        $result['rl_amt'] = 0; //REPORT LOSS OF CARD
        $result['rl_count'] = 0;  

        $result['bp_renewal_amt'] = 0; //total
        $result['bp_renewal_count'] = 0;

        $result['reg_amt_t'] = 0;  //REGISTRATION OF OTC TRAVEL CARD
        $result['reg_count_c'] = 0;

        $result['others_amt'] = 0; 
        $result['others_count'] = 0;

        $result['total_amt'] = 0; 
        $result['total_count'] = 0;
        
        //print_r($data); exit;
         
        if(isset($data['trans_details']) && !empty($data['trans_details'])) {
            //unset($data['trans_details'][1]);
            foreach ($data['trans_details'] as $key => &$value) {

                $result['total_amt'] += $value['total_amount'];
                $result['total_count'] += $value['total_count'];

                switch ($value['code']) {
                    case 'RG':
                        $result['ppc_reg_amt'] += $value['total_amount'];  
                        $result['ppc_reg_count'] += $value['total_count'];
                        break;
                    case 'RGOT':
                        $result['reg_amt_t'] += $value['total_amount'];  
                        $result['reg_count_c'] += $value['total_count'];
                        break;
                    
                    case 'TPSW':
                        $result['sw_topup_amt'] += $value['total_amount'];  
                        $result['sw_topup_count'] += $value['total_count'];  
                        break;

                    case 'TPBP':
                        $result['bpr_topup_amt'] += $value['total_amount'];  
                        $result['bpr_topup_count'] += $value['total_count'];  
                        break;    

                    case 'TPTC':
                        $result['tw_topup_amt'] += $value['total_amount'];  
                        $result['tw_topup_count'] += $value['total_count'];  
                        break;

                    case 'TPTS':
                        $result['tpts_topup_amt'] += $value['total_amount'];  
                        $result['tpts_topup_count'] += $value['total_count'];  
                        break;    

                    case 'BPSW':
                        $result['bpsw_topup_amt'] += $value['total_amount'];  
                        $result['bpsw_topup_count'] += $value['total_count'];  
                        break;

                    case 'RI':
                        $result['ri_amt'] += $value['total_amount'];  
                        $result['ri_count'] += $value['total_count'];  
                        break;  

                    case 'RIOT':
                        $result['riot_amt'] += $value['total_amount'];  
                        $result['riot_count'] += $value['total_count'];  
                        break;          

                    case 'RA':
                        $result['ra_amt'] += $value['total_amount'];  
                        $result['ra_count'] += $value['total_count'];  
                        break;   

                    case 'SD':
                        $result['sd_amt'] += $value['total_amount'];  
                        $result['sd_count'] += $value['total_count'];  
                        break; 

                    case 'SDOT':
                        $result['sdot_amt'] += $value['total_amount'];  
                        $result['sdot_count'] += $value['total_count'];  
                        break;    

                    case 'UC':
                        $result['uc_amt'] += $value['total_amount'];  
                        $result['uc_count'] += $value['total_count'];  
                        break;

                    case 'RL':
                        $result['rl_amt'] += $value['total_amount'];  
                        $result['rl_count'] += $value['total_count'];  
                        break;                                           
                            
                    default:
                        break;
                }

                //registration RG + RGOT
                $result['reg_amt'] = $result['ppc_reg_amt']+$result['reg_amt_t'];
                $result['reg_count'] = $result['ppc_reg_count']+$result['reg_count_c'];
                //bus Pass Renewal TPBP+BPSW
                $result['bp_renewal_amt'] = $result['bpr_topup_amt']+$result['bpsw_topup_amt'];
                $result['bp_renewal_count'] = $result['bpr_topup_count']+$result['bpsw_topup_count'];

                //others
                $result['others_amt'] = $result['ri_amt']+$result['riot_amt']+$result['ra_amt']+$result['sd_amt']+$result['sdot_amt']+$result['uc_amt']+$result['tpts_topup_amt']+$result['rl_amt'];
                $result['others_count'] = $result['ri_count']+$result['riot_count']+$result['ra_count']+$result['sd_count']+$result['sdot_count']+$result['uc_count']+$result['tpts_topup_count']+$result['rl_count'];
            }
        }
        $result['lastTrans'] = $this->getLastTransDetails($user_name);
        return $result;
    }
 
    
       public function getTransCountByTransType($user_name) {
//        $queryStr = "SELECT ctt.ctrans_type_id as id, ctt.ctrans_type_code as code, ctt.ctrans_type_name as name, count(ct.id) as total_count, sum(at.total_amount) as total_amount
//                     FROM ps_card_transaction ct, ps_card_trans_type ctt, ps_amount_transaction at
//                     where ct.transaction_type_id = ctt.ctrans_type_id
//                     AND ct.created_by = ".$user_name." 
//                     AND DATE_FORMAT(ct.created_date, '%d/%m/%Y') = DATE_FORMAT(now(), '%d/%m/%Y')
//                     AND ct.id = at.card_transaction_id
//                     AND ct.is_active = '1'
//                     group by ct.transaction_type_id";
           
             $queryStr = "SELECT  ct.transaction_type_code as code, ct.transaction_type_code as name, count(ct.id) as total_count, sum(at.total_amount) as total_amount
                     FROM ps_card_transaction ct, ps_amount_transaction at
                     
                     where 1
                     AND ct.created_by = ".$user_name." 
                     AND DATE_FORMAT(ct.created_date, '%d/%m/%Y') = DATE_FORMAT(now(), '%d/%m/%Y')
                     AND ct.id = at.card_transaction_id
                     AND ct.is_active = '1'
                     group by ct.transaction_type_code";;  
           
        $query = $this->db->query($queryStr);
        $returnArr = $query->result_array();
        return isset($returnArr[0]) ? $returnArr : '';
    }
    
     public function getLastTransDetails($user_name = '', $transactionFlag = '', $custCardId = '', $limit = 1) {
        $this->db->select("CONCAT(cm.first_name,' ',cm.middle_name,' ',cm.last_name) as name, cm.mobile_number, ccm.trimax_card_id,
                     ccm.cust_card_id, at.total_amount, ct.transaction_type_code as trans_name, 
                     ct.concession_nm, ct.span_name as span_name,
                     
                     d.DEPOT_NM as from_stop, dd.DEPOT_NM as till_stop,
                     
                     DATE_FORMAT(ct.created_date, '%d/%m/%Y %k:%i:%s') as created_date,
                     DATE_FORMAT(ct.activation_date, '%d/%m/%Y') as validity_date,
                     DATE_FORMAT(ct.expiry_date, '%d/%m/%Y') as expiry_date");
       
        
        $this->db->from('ps_customer_master cm');
        $this->db->join('ps_customer_card_master ccm','cm.id = ccm.customer_id','inner');
        $this->db->join('ps_card_transaction ct','ct.trimax_card_id = ccm.trimax_card_id','inner');
        $this->db->join('ps_card_transaction_details ctd','ctd.card_transaction_id = ct.id','inner');
        
        $this->db->join('msrtc_replication.depots d','d.DEPOT_CD = ctd.from_stop_code','left');
        $this->db->join('msrtc_replication.depots dd','dd.DEPOT_CD = ctd.till_stop_code','left');
        
        $this->db->join('ps_amount_transaction at','ct.id = at.card_transaction_id','inner');
        //$this->db->join('ps_card_trans_type ctt','ct.transaction_type_id = ctt.ctrans_type_id','inner');
       // $this->db->join('ps_concession_master p_conm','ct.pass_concession_id = p_conm.id','inner');
        //$this->db->join('ps_rfidmaster_span p_rs','ct.span_id = p_rs.id','inner');
        if($custCardId != ''){
            $this->db->where('ccm.cust_card_id', $custCardId);
        }
        
        if($user_name != ''){
            $this->db->where('ct.created_by', $user_name);
        }
        
        if($transactionFlag != '' && $transactionFlag == 'RG'){
                $this->db->where('ct.transaction_type_id', '1'); // RG
        }
        if($transactionFlag != '' && $transactionFlag == 'SW'){
            $this->db->where('ct.transaction_type_id', '4'); // SW
        }
        if($transactionFlag != '' && $transactionFlag == 'TW'){
            $this->db->where('ct.transaction_type_id', '5'); // TW
        }
        if($transactionFlag != '' && $transactionFlag == 'TWSW'){
            $this->db->where('ct.transaction_type_id', '6'); // TW via SW
        }

        $this->db->where(array('cm.is_active' => '1','ccm.is_active' => '1','ct.is_active' => '1','ctd.is_active' => '1','at.is_active' => '1'));
        
        $this->db->limit($limit);
        $this->db->order_by('ct.id','DESC');
        $query = $this->db->get();
        // show($this->db->last_query(),1);
        $result = $query->result_array();
        return $result;
    }

   public function get_remittance($data){ 
        $sql_query = "SELECT
                         (CASE
                            WHEN ct.transaction_type_code = 'RG' THEN 'Registration' 
							WHEN ct.transaction_type_code = 'RGOT' THEN 'OTC Registration'
                            WHEN ct.transaction_type_code = 'TPBP' THEN 'Pass Renewal'
                            WHEN ct.transaction_type_code = 'TPTC' THEN 'Travel Wallet Topup'
                            WHEN ct.transaction_type_code = 'TPTS' THEN 'TW via SW' 
							WHEN ct.transaction_type_code = 'TPSW' THEN 'SHOPPING WALLET' END
                        ) AS transaction_type,
                        (select count(pct1.id) from ps_card_transaction pct1
                            WHERE pct1.transaction_type_code = ct.transaction_type_code
                            AND pct1.created_by = '".$data['apploginuser']."' 
                            AND DATE_FORMAT(pct1.created_date, '%d/%m/%Y') = DATE_FORMAT('".$data['date']."', '%d/%m/%Y')
                            AND pct1.is_active = '1'
                            GROUP BY pct1.transaction_type_code
                        ) AS transaction_count,
                        SUM(CASE WHEN atd.amount_code = 'SCF' THEN atd.amount ELSE 0.00 END) AS smart_card_fee,
                        SUM(CASE WHEN atd.amount_code = 'GST' THEN atd.amount ELSE 0.00 END) AS gst_fee ,
                        SUM(CASE WHEN atd.amount_code = 'AF' THEN atd.amount ELSE 0.00 END) AS application_fee ,
                        if(ct.transaction_type_code = 'TPBP',sum(at.total_amount),0.00) as pass_amt,
                        #if(ct.transaction_type_code = 'TPTC',sum(at.total_amount),0.00) as topup_amt,
                        #SUM(at.total_amount) as total_received,

                        (select sum(pat1.total_amount) from ps_amount_transaction pat1
                            WHERE pat1.transaction_type_code = at.transaction_type_code
                            AND pat1.created_by = '".$data['apploginuser']."' 
                            AND DATE_FORMAT(pat1.created_date, '%d/%m/%Y') = DATE_FORMAT('".$data['date']."', '%d/%m/%Y')
                            AND pat1.is_active = '1'
                            GROUP BY pat1.transaction_type_code
                        ) AS topup_amt,
						
						((select sum(pat1.total_amount) from ps_amount_transaction pat1
                            WHERE pat1.transaction_type_code = at.transaction_type_code
                            AND pat1.created_by = '".$data['apploginuser']."' 
                            AND DATE_FORMAT(pat1.created_date, '%d/%m/%Y') = DATE_FORMAT('".$data['date']."', '%d/%m/%Y')
                            AND pat1.is_active = '1'
                            GROUP BY pat1.transaction_type_code
                        )+SUM(CASE WHEN atd.amount_code = 'BonusAmt' THEN atd.amount ELSE 0.00 END)) AS total_received,

                        SUM(CASE WHEN atd.amount_code = 'BonusAmt' THEN atd.amount ELSE 0.00 END) AS bonus_amt 
                    FROM ps_card_transaction ct, ps_amount_transaction at 
                    LEFT JOIN ps_amount_transaction_details atd on atd.amount_transaction_id = at.id
                    WHERE 
                         ct.transaction_type_code IN ('RG','TPBP','TPTC','TPTS','RGOT','TPSW')
                        AND ct.created_by = '".$data['apploginuser']."' 
                        AND DATE_FORMAT(ct.created_date, '%d/%m/%Y') = DATE_FORMAT('".$data['date']."', '%d/%m/%Y')
                        AND ct.id = at.card_transaction_id
                        AND ct.is_active = '1'
                    GROUP BY ct.transaction_type_code
                ";

        $query = $this->db->query($sql_query);
        return $query->result_array();
    }
                    
     public function get_query_result($sql_query) {
        $query = $this->db->query($sql_query);
        return $query->result_array();
    }
    
}
