<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Wallet_transfer_model extends MY_Model {

    var $table  = 'wallet_transfer';
    var $fields = array("id","agent_id","wallet_id","wallet_trans_id","transfer_wallet_amount","outstanding_amount","transfer_status","transfer_date");
    var $key    = 'id';

    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
}

?>
