<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_login_count_model extends MY_Model {

    /**
     * Instanciar o CI
     */
    var $table = 'user_login_count';
    var $fields = array("id", "user_id", "visit_status");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    public function check_first_time_visit() {
      
        $user_id = $this->session->userdata('user_id');
        $this->db->select('id,user_id,visit_status,boarding_stop_id');
        $this->db->where("user_id", $user_id);
        $this->db->from('user_login_count');
        $query = $this->db->get();
        $data = $query->row_array();
    
        if (count($data) >= 1) {
           
            $data['visit'] = 1;
        } else {

            //$data = array("visit_status" => 'Y');
            //$this->db->where('user_id', $user_id);
            //$this->db->update('user_login_count', $data);
            $this->user_login_count_model->user_id = $user_id;
            $this->user_login_count_model->visit_status = 'Y';
            $this->user_login_count_model->save();
            $data['visit'] = 0;
        }
        return $data;
    }

}
