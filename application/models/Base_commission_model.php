<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Base_commission_model extends MY_Model {

    protected $table = 'base_commission';
    var $key = 'id';

    public function __construct() {
        parent::__construct();
    }

    public function base_commission_detail($input) {
        $this->db->select("p.id       AS provider_id,
                           p.name     AS provider_name,
                           o.op_id    AS operator_id,
                           o.op_name  AS operator_name,
                           po.id      AS provider_operator_id,
                           bc.id      AS base_commission_id,
                           bc.type    AS commission_type ,
                           bc.base_rate_flag AS base_flag,
                           bc.fixed_value,
                           bc.percentage_value,
                           bc.commission_on,
                          (case when bc.bus_type=0 then 'All' when bc.bus_type=2 then 'NonAC' else bt.name end) bus_type");
        $this->db->from("base_commission bc");
        $this->db->join("api_provider_operator po", "po.id = bc.provider_operator_id", "left");
        $this->db->join("api_provider p", "p.id = bc.provider_id", "left");
        $this->db->join("op_master o", "o.op_id = po.op_id", "left");
        $this->db->join("bus_type_home bt", "bc.bus_type = bt.id", "left");
        $this->db->where("p.is_status", 1);
        if ($input['op_id'] != "") {
            $this->db->where("po.op_id", $input['op_id']);
        }
        if ($input['api_provider_id'] != "") {
            $this->db->where("po.api_provider_id", $input['api_provider_id']);
        }
        if (isset($input['bus_type_id']) &&  $input['bus_type_id'] != "") {
            $this->db->where("bc.bus_type", $input['bus_type_id']);
        }
        $this->db->order_by("o.op_id","desc");
        $query = $this->db->get();
        
        return $query->result();
    }

}
