<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reservation_model extends MY_Model {

    var $table = 'current_booking_bus_selection';
    var $fields = array("id", "user_id", "op_id", "trip_no", "dept_date", "dept_time", "route_no", "bus_no", "bus_type_id", "is_available");
    // var $fields = "id,w_id,user_id,amt,status,expire_at,comment,added_by,added_on";
    var $key = 'id';

    public function __construct() {
        parent::__construct();
        $this->db2 = $this->load->database('upsrtc', TRUE);
    }

    function get_search($res = array()) {
        $data = array();
        $this->db->select("om.op_name,t.trip_no,tm.op_id, bsm.bus_stop_name,bd.bus_stop_name,"
                . "concat(t.sch_departure_date, ' ', t.sch_departure_tm) as dept_tm,"
                . " t2.arrival_date, t2.arrival_tm, t2.day,"
                . "t.sch_departure_tm,t.arrival_tm,bs.op_bus_type_name,t.tot_seat_available,t.sch_departure_date, t.seat_fare", false);

        $this->db->from("travel_destinations t");
        $this->db->join("travel_destinations t2", "t.boarding_stop_id = t2.boarding_stop_id", "inner");

        $this->db->join("bus_stop_master bsm", "t.boarding_stop_id = bsm.bus_stop_id", "inner");
        $this->db->join("trip_master tm", "tm.trip_no = t.trip_no", "inner");
        $this->db->join("bus_stop_master bd", "t.destination_stop_id = bd.bus_stop_id", "inner");
        $this->db->join("op_master om", "om.op_id = tm.op_id", "inner");
        $this->db->join("op_bus_types bs", "bs.op_bus_type_id = tm.op_bus_type_id", "inner");
        // $this->db->join("bus_type bt", "bt.bus_type_id = tm.bus_type_id", "inner");

        $this->db->where("bsm.bus_stop_name", $res['from']);
        $this->db->where("bd.bus_stop_name", $res['to']);
        $this->db->where("DATE_FORMAT(t.sch_departure_date,'%Y-%m-%d')", $res['date']);

        if (isset($res['travel']) && count($res['travel']) > 0) {
            $this->db->where_in('om.op_name', $res['travel']);
        }

        if (isset($res['bus_type_cat']) && count($res['bus_type_cat']) > 0) {
            $this->db->where_in('tm.bus_type_id', $res['bus_type_cat']);
        }

        if (isset($res['dept_tm']) && count($res['dept_tm']) > 0) {
            $this->db->where_in('t.sch_departure_tm', $res['dept_tm']);
        }
        $this->db->group_by("t.trip_no");
        $query = $this->db->get();
        // $this->db->cache_off();
        //show($this->db->last_query(), 1);
        return $query->result_array();
    }

}
