<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Op_seat_layout_model extends MY_Model {

    /**
     * Instanciar o CI
     */
    var $table = 'op_seat_layout';
    var $fields = "id,ttl_seats,op_id,layout_name,no_rows,no_cols,no_rows2,no_cols2,no_berth,created_by,created_date,updated_by,updated_date,record_status";
    var $key = "id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    
    function get_bus_format_count() {
        $where_clause = array('op_bus_type_name' => $this->op_bus_type_name,
            'op_id' => $this->op_id
        );
        $this->db->where($where_clause);
        $data = $this->db->get($this->table);
        return $data->row();
    }

    function get_seat_layout($seat_layout_id)
    {
        $this->db->select("old.seat_type,old.quota_type,osl.no_rows2,osl.no_cols2,osl.ttl_seats,osl.op_id, osl.layout_name, osl.no_rows, osl.no_cols, old.row_no, old.col_no, old.berth_no, old.seat_no");
        $this->db->from("op_seat_layout osl");
        $this->db->join("op_layout_details old", "old.op_seat_layout_id = osl.id", "inner");
        $this->db->where("osl.id",$seat_layout_id);
        $query = $this->db->get();
        return $query->result();
    }
    function get_all_seat_layout()
    {
        $this->db->select("id,ttl_seats,op_id,layout_name");
        $this->db->from("op_seat_layout osl");
        
        $this->db->where("osl.record_status",'Y');
        $query = $this->db->get();
        return $query->result();
    }


    function get_seat_layout_for_operator($seat_layout_name)
    {
        $this->db->select("old.seat_type,old.quota_type,osl.no_rows2,osl.no_cols2,osl.ttl_seats,osl.op_id, osl.layout_name, osl.no_rows, osl.no_cols, old.row_no, old.col_no, old.berth_no, old.seat_no");
        $this->db->from("op_seat_layout osl");
        $this->db->join("op_layout_details old", "old.op_seat_layout_id = osl.id", "inner");
        $this->db->where("osl.layout_name",$seat_layout_name);
        $query = $this->db->get();
        return $query->result();
    }
    
}
