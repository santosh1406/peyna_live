<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bus_type_model extends MY_Model {

    /**
     * Instanciar o CI
     */
    var $table  = 'bus_type';
    var $fields = array("id","bus_type_name","bus_category","created_date","created_by","updated_date","updated_by","record_status");
    var $key    = 'id'; 
    public function __construct() {
         parent::__construct();
        $this->_init();      
    }

   

    function getbustype() {
        $this->db->select('id,bus_type_name,bus_category');
        $bus_type = $this->db->get('bus_type');
        return $bus_type->result_array();
    }

    function get_bustypedetail_category($cat) {

        $this->db->where('bus_category', $cat);
        $data = $this->db->get($this->table);
        return $data->row();
    }

    function bus_type_cat() {
        $this->db->cache_on();
        $this->db->select("*");
        $this->db->from("bus_type");
        $query = $this->db->get();
        $this->db->cache_off();
             
        return $query->result_array();
    }
    function get_type_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('bus_type');
        $this->db->where('id',$id);
        $data=$this->db->get();
        return $data->result_array();
    }

    function get_op_bus_type($op_id)
    {
        $result = array();

        $this->db->from('bus_type bt');
        $this->db->join('op_bus_type obt',"obt.bus_type_id=bt.bus_type_id");

        $query = $this->db->get();

        $result = $query->result_array();
        return $result;
    }


    function get_op_bus_type_details($op_id)
    {
        $result = array();

        $this->db->from('bus_type bt');
        $this->db->join('op_bus_type obt',"obt.bus_type_id=bt.bus_type_id");
        $this->db->where('obt.op_id',$op_id);
        $query = $this->db->get();
       
        $result = $query->result_array();
        return $result;
    }


    function get_api_bus_type($provider_id = "")
    {
        $result = array();
        $data = array();
        if($provider_id != "")
        {
            $this->db->select('bt.bus_type_name,abt.api_bus_type');
            $this->db->from('bus_type bt');
            $this->db->join('api_bus_types abt',"abt.bus_type_id = bt.id","inner");
            $this->db->where('abt.provider_id',$provider_id);
            $query = $this->db->get();
           
            $result = $query->result_array();

            if($result)
            {
                $mapped_type = array();
                foreach ($result as $key => $value)
                {
                    $mapped_type[$value["api_bus_type"]] = $value["bus_type_name"];
                }

                $data["mapped_type"] = $mapped_type;
                $data["result"] = $result;
            }
        }

        return $data;
    }
    
    function get_bus_type_by_api_bus_type($api_bus_type){
        $this->db->select('bt.id, bt.bus_type_name');
        $this->db->from('api_bus_types abt');
        $this->db->join('bus_type bt',"bt.id = abt.bus_type_id","left");
        $this->db->where('abt.api_bus_type',$api_bus_type);
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }

    function get_operator_bus_types($operator_id = "")
    {
        $result = array();
        $data = array();
        if($operator_id != "")
        {
            $this->db->select('bt.bus_type_name,obt.op_bus_type_name');
            $this->db->from('bus_type bt');
            $this->db->join('op_bus_types obt',"obt.bus_type_id = bt.id","inner");
            $this->db->where('obt.op_id',$operator_id);
            $query = $this->db->get();
           
            $result = $query->result_array();

            if($result)
            {
                $mapped_type = array();
                foreach ($result as $key => $value)
                {
                    $mapped_type[trim($value["op_bus_type_name"])] = trim($value["bus_type_name"]);
                }

                $data["mapped_type"] = $mapped_type;
                $data["result"] = $result;
            }
        }

        return $data;
    }


    function get_operator_bus_type_detail($conditions = array())
    {
        $result = array();
        $data = array();
        if(!empty($conditions))
        {
            $this->db->select('bt.id as bus_type_id,bt.bus_type_name,obt.op_bus_type_name,bt.bus_category,obt.op_bus_type_cd');
            $this->db->from('bus_type bt');
            $this->db->join('op_bus_types obt',"obt.bus_type_id = bt.id","inner");

            if(isset($conditions["operator_bus_type_name"]) && $conditions["operator_bus_type_name"] != "")
                $this->db->where('obt.op_bus_type_name',$conditions["operator_bus_type_name"]);

            if(isset($conditions["operator_id"]) && $conditions["operator_id"] != "")
                $this->db->where('obt.op_id',$conditions["operator_id"]);

            $query = $this->db->get();
           
            $result = $query->result_array();

            return $result;
        }
        return $data;
    }
}
