<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Agent_commission_pattern_model extends MY_Model {
    
	var $table  = 'agent_commission_pattern';
    var $fields = array("id","name","op_id","service_id","bus_type","ctype","l1","l2","l3","l4");
    var $key    = 'id';
    
    protected $set_modified = false;


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

}

// End of Agent_model class