<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agent_service_mapping_model extends MY_Model {
    
    var $table  = 'agent_service_mapping';
    var $fields = array("id","agent_id" ,"user_id","created_by","updated_by","created_at","updated_at");
    var $key    = "id";
 
    public function __construct() {
        parent::__construct();
        $this->_init();
    }
  }  
  ?>
