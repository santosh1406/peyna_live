<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Report extends CI_Controller {

    public function __construct() {
        parent::__construct();
        is_logged_in();
	set_time_limit(0);
        
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);
        $this->load->model(array('tickets_model', 'users_model', 'state_master_model', 'agent_model', 'ticket_details_model', 'routes_model', 'bus_stop_master_model', 'op_master_model', 'booking_agent_model', 'report_model', 'api_provider_model', 'city_master_model','base_commission_model','office_master_model','Ors_agent_report_model'));
        $this->load->library(array('Excel','Datatables','Pdf','pagination'));
        $this->load->helper("url");
    }

    public function index() {
        $data = array();
        $data['tran_type_data'] = array("failed" => "Failed Transaction",
            "success" => "Success Transaction",
            "incomplete" => "Incomplete Transaction",
            "cancel" => "Cancel Transaction",
            "pfailed" => "Payment Failed",
            "psuccess" => "Payment Success"
        );
        load_back_view(RESERVATION_REPORT_VIEW, $data);
    }

    public function filter_get_list() {

        $input = $this->input->post();

        $data['from_date'] = $input['from_date'];
        $data['to_date'] = $input['to_date'];
        $data['from_stop_name'] = $input['txt_frm_stop_cd'];
        $data['till_stop_name'] = $input['txt_to_stop_cd'];
        $data['tran_type'] = isset($input['tran_type']) ? $input['tran_type'] : "";

        $data['tran_type_data'] = array("failed" => "Failed Transaction",
            "success" => "Success Transaction",
            "incomplete" => "Incomplete Transaction",
            "cancel" => "Cancel Transaction",
            "pfailed" => "Payment Failed",
            "psuccess" => "Payment Success"
        );

        if (isset($input) && count($input) > 0) {

            $input['user_id'] = $this->session->userdata('user_id');
            $input['op_id'] = $this->session->userdata('op_id');
            $data['result_data'] = $this->tickets_model->rpt_reservation_filter($input);
            if (isset($input['from_date']) && $input['from_date'] != '') {
                $data['from_date'] = $input['from_date'];
            }

            if (isset($input['to_date']) && $input['to_date'] != '') {
                $data['to_date'] = $input['to_date'];
            }
            if (isset($input['txt_frm_stop_cd']) && $input['txt_frm_stop_cd'] != '') {
                $data['from_stop_name'] = $input['txt_frm_stop_cd'];
            }
            if (isset($input['txt_to_stop_cd']) && $input['txt_to_stop_cd'] != '') {
                $data['till_stop_name'] = $input['txt_to_stop_cd'];
            }
        }
        load_back_view(RESERVATION_REPORT_VIEW, $data);
    }

    public function autocomplete_bus_stop() {
        $data = $this->bus_stop_master_model->autocomplete_bus_stops();

        if (count($data) == 0) {
            $data['status'] = 'Invalid Stop Code';
        }
        data2json($data);
    }

    public function reservation_export_excel() {

        $data = $this->input->get();
        $data['user_id'] = $this->session->userdata('user_id');
        $data['op_id'] = $this->session->userdata('op_id');
        $data['from_date'] = $data['from_date'];
        $data['to_date'] = $data['to_date'];
        $data['txt_frm_stop_cd'] = $data['from_stop_cd'];
        $data['txt_to_stop_cd'] = $data['till_stop_cd'];
        $data['tran_type'] = $data['transaction_type'];

        $result_data = $this->tickets_model->rpt_reservation_filter($data);
        $sr_no = 1;
        $data = array();
        foreach ($result_data as $key => $value) {
            $data[] = array('Id' => $sr_no, 'PNR NO.' => $value['pnr_no'], 'From Stop' => $value['from_stop_name'], 'End Stop' => $value['till_stop_name'], 'Dept. Time' => $value['dept_time'], 'No.of Passenger' => $value['num_passgr'], 'Total Fare' => $value['tot_fare_amt'], 'Transaction Status' => $value['Tran_type']);
            $sr_no++;
        }
        $this->excel->data_2_excel('rpt_reservation_' . time() . '.xlsx', $data);
        die;
    }

    public function reservation_export_pdf() {

        $input = $this->input->get();
        $input['user_id'] = $this->session->userdata('user_id');
        $input['op_id'] = $this->session->userdata('op_id');
        $data['from_date'] = $input['from_date'];
        $data['to_date'] = $input['to_date'];
        $data['txt_frm_stop_cd'] = $input['from_stop_cd'];
        $data['txt_to_stop_cd'] = $input['till_stop_cd'];
        $data['tran_type'] = $input['transaction_type'];

        $dateRangeStr = date('Y-m-d');
        $result_data = $this->tickets_model->rpt_reservation_filter($data);
        $pdf_gen = $this->generatePdf($result_data);
    }

    public function generatePdf($result) {

        ini_set("memory_limit", "512M");
        ob_start();
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('Reservation Report');
        $pdf->SetHeaderMargin(20);
        $pdf->SetTopMargin(10);
        $pdf->setFooterMargin(20);
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');
        $pdf->AddPage();
        $html = '<center>Reservation Report:</center><br><br><table width="100%" border="1" cellspacing="0" cellpadding="0"  class="table table-hover">
                      <thead><tr style="background-color:gray; font-weight:bold;">
                                    <th>Sr No.</th><th >PNR No.</th>
                                    <th>From Stop</th>
                                    <th>End Stop</th>
                                    <th>Dept. Time</th>
                                     <th>No. of Passenger</th>
                                     <th>Total Fare</th>
                                     <th>Transaction Status</th>
                                   </tr></thead><tbody>';
        $sr_no = 1;
        foreach ($result as $key => $value):
            $html .= "<tr><td>" . $sr_no . "</td>";
            $html .= "<td>" . $value['pnr_no'] . "</td>";
            $html .= "<td>" . $value['from_stop_name'] . "</td>";
            $html .= "<td>" . $value['till_stop_name'] . "</td>";
            $html .= "<td>" . $value['dept_time'] . "</td>";
            $html .= "<td>" . $value['num_passgr'] . "</td>";
            $html .= "<td>" . $value['tot_fare_amt'] . "</td>";
            $html .= "<td>" . $value['Tran_type'] . "</td></tr>";
            $sr_no++;
        endforeach;
        $html .= '</tbody></table>';
        $pdf->writeHTML($html, true, false, true, false, '');

        $pdf->Output('Reservation Report.pdf', 'D');
        die;
    }

    function operator_view() {

        $data = array();
        $data['op_name'] = $this->op_master_model->get_op_names();
        load_back_view(OPERATOR_REPORT_VIEW, $data);
    }

    function operator_list() {
        $this->datatables->select('op.op_name,op.op_type,cm.stCity,sm.stState,op.op_country,op.op_email,op.op_phone_no,4,"API","FIXED","saving",9,opd.bank_name,opd.acc_no,opd.ifsc_code,"aa",op.created_date', false);
        $this->datatables->from('op_master op');
        $this->datatables->join('op_bank_detail opd', 'opd.op_id=op.op_id', 'left');
        $this->datatables->join('city_master cm', 'cm.intCityId=op.op_city', 'left');
        $this->datatables->join('state_master sm', 'sm.intStateId=op.op_state', 'left');
        $this->datatables->join('country_master com', 'com.intCountryId=op.op_country', 'left');
        $this->datatables->where('op.is_status', 1);
        $this->datatables->where('op.is_deleted', 0);


        if ($this->input->post('op_nm') && $this->input->post('op_nm') != 'all') {
            $this->datatables->where('op.op_id', $this->input->post('op_nm'));
        }
        if ($this->input->post('from_date') && $this->input->post('from_date') != '') {
            $this->datatables->where('om.created_date >=', date('Y-m-d', strtotime($this->input->post('from_date'))));
        }
        if ($this->input->post('to_date') && $this->input->post('to_date') != '') {
            $this->datatables->where('om.created_date <=', date('Y-m-d', strtotime($this->input->post('to_date'))));
        }

        $data = $this->datatables->generate('json');
        //show($this->db->last_query(),1);
        echo $data;
    }

    public function operator_excel() {

        $input = $this->input->get();

        $data['user_id'] = $this->session->userdata('user_id');
        //  $data['op_id']   =$this->session->userdata('op_id');
        $result_data = $this->op_master_model->operator_detail_filter($input);
        $sr_no = 1;
        $data = array();
        foreach ($result_data as $key => $value) {
            $data[] = array('Sr no' => $sr_no,
                'Name' => $value['op_name'],
                'Brand' => $value['op_name'],
                'Type' => $value['op_type'],
                'City' => $value['stCity'],
                'State' => $value['stState'],
                'Country' => $value['op_country'],
                'No of Trips' => 5,
                'API/BOS' => $value['API'],
                'Commission Type' => 'Fixed',
                'Account Type' => 'Saving',
                'Remittance days' => 2,
                'Bank Name' => $value['bank_name'],
                'Account No' => $value['acc_no'],
                'IFSC' => $value['op_phone_no'],
                'NIFT detail' => $value['ifsc_code'],
                'Operator Created Date' => $value['created_date']);
            $sr_no++;
        }
        $this->excel->data_2_excel('rpt_operator_report' . time() . '.xlsx', $data);
        die;
    }

    function operator_pdf() {
        ini_set("memory_limit", "512M");
        ob_start();
        $data = $this->input->get();
        $result_data = $this->op_master_model->operator_detail_filter($data);

        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('Operator Report');
        $pdf->SetHeaderMargin(20);
        $pdf->SetTopMargin(10);
        $pdf->setFooterMargin(20);
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');
        $pdf->AddPage();
        $x = $pdf->pixelsToUnits('10');
        $y = $pdf->pixelsToUnits('10');
        $font_size = $pdf->pixelsToUnits('18');
        $txt = '';

        $pdf->SetFont('helvetica', '', $font_size, '', 'default', true);
        $pdf->Text($x, $y, $txt, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false);

        $html = '<center>Operator Report:</center><br><br><table width="100%" border="1" cellspacing="0" cellpadding="0"  class="table table-hover">
                      <thead><tr style="background-color:gray; font-weight:bold;">
                                    <th >Sr No.</th>
                                    <th >Name</th>
                                    <th >Brand</th>
                                    <th >Type</th>
                                     <th>City</th>
                                     <th >State</th>
                                     <th >country</th>
                                     <th>No of Trips</th>
                                    <th>API</th>
                              <th>Commission Type</th>
                                <th>Account Type</th>
                             <th>Remittance days</th>
                              <th>Bank Name</th>
                               <th>Account No</th>
                                
                                <th>Operator Created Date</th>
                                   </tr></thead><tbody>';
        $sr_no = 1;
        foreach ($result_data as $key => $value):
            $html .= "<tr><td>" . $sr_no . "</td>";
            $html .= "<td>" . $value['op_name'] . "</td>";
            $html .= "<td>" . $value['op_name'] . "</td>";
            $html .= "<td>" . $value['op_type'] . "</td>";
            $html .= "<td>" . $value['stCity'] . "</td>";
            $html .= "<td>" . $value['stState'] . "</td>";
            $html .= "<td>5</td>";
            $html .= "<td>" . $value['API'] . "</td>";
            $html .= "<td>FIXED</td>";
            $html .= "<td>Saving</td>";
            $html .= "<td>2</td>";
            $html .= "<td>" . $value['bank_name'] . "</td>";
            $html .= "<td>" . $value['acc_no'] . "</td>";
            $html .= "<td>" . $value['acc_no'] . "</td>";
            $html .= "<td>" . $value['created_date'] . "</td></tr>";
//                        
            $sr_no++;
        endforeach;
        $html .= '</tbody></table>';
        //show($html,1);
        $pdf->writeHTML($html, true, false, true, false, '');

        $pdf->Output('Operator_Report.pdf', 'D');
        die;
    }

    function daily_tkt_sales() {
        $data['agent_name'] = $this->agent_model
                ->as_array()
                ->find_all();
        $data['op_name'] = $this->op_master_model
                ->as_array()
                ->find_all();

        // show($agent_names,1);

        load_back_view(DAILY_TKT_SALES, $data);
    }
    /* daily advance tkt booking report */

    function daily_ad_tkt_view() {

        $data['tran_type_data'] = array("failed" => "Failed Transaction",
            "success" => "Success Transaction",
            "incomplete" => "Incomplete Transaction",
            "cancel" => "Cancel Transaction",
            "pfailed" => "Payment Failed",
            "psuccess" => "Payment Success"
        );

        $data['pnr_no'] = $this->tickets_model->column('pnr_no')
                ->where('pnr_no!=', '')
                ->as_array()
                ->find_all();

        load_back_view(DAILY_ADVANCE_TICKET_SALES, $data);
    }

    function daily_ad_tkt() {

        $this->datatables->select("1,CASE tk.transaction_status when 'success' then 'Success Transaction' when 'cancel' then 'Cancel Transaction' when "
                . "'pfailed' then 'Payment Failed'   when 'failed' then 'Failed' "
                . "when 'psuccess' then 'Payment Success' else 'Incomplete' end as Tran_type,"
                . "tk.issue_time,tk.boss_ref_no,tk.op_name,tk.pnr_no,tk.from_stop_name,"
                . "tk.till_stop_name,tk.num_passgr,tk.tot_fare_amt,tk.fare_reservationCharge,"
                . "(tk.tot_fare_amt_with_tax-tk.tot_fare_amt) as Total_tax,ifnull(ctd.refund_amt,'0'),"
                . "ctd.cancel_charge,count(tds.seat_no),tk.status");
        $this->datatables->from('tickets tk');
        $data = $this->input->post();
        $from_date = date('Y-m-d', strtotime($data['from_date']));
        $to_date = date('Y-m-d', strtotime($data['to_date']));
        if ($this->input->post('op_name')) {
            $this->datatables->where('tk.op_name', $this->input->post('op_name'));
        }

        if ($this->input->post('tran_type')) {
            $this->datatables->where('tk.transaction_status', $this->input->post('tran_type'));
        }

        if ($this->input->post('pnr_no')) {
            $this->datatables->where('tk.pnr_no', $this->input->post('pnr_no'));
        }


        if ((isset($data['from_date']) && isset($data['to_date'])) && (($data['from_date'] != "") && ($data['to_date'] != ""))) {
            if ($data['from_date'] != $data['to_date']) {
                $this->datatables->where("DATE_FORMAT(issue_time, '%Y-%m-%d') BETWEEN '" . $from_date . "' AND '" . $to_date . "' ");
            } else {
                $this->datatables->where("DATE_FORMAT(issue_time, '%Y-%m-%d')=", $from_date);
            }
        } else if (isset($data['from_date']) && $data['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(issue_time, '%Y-%m-%d')=", $from_date);
        } else if (isset($data['to_date']) && $data['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(issue_time, '%Y-%m-%d')=", $to_date);
        }

        $this->datatables->join('cancel_ticket_data ctd', 'tk.ticket_id=ctd.ticket_id', 'left');
        $this->datatables->join('ticket_details   tds', 'tds.ticket_id=tk.ticket_id', 'inner');
        $this->datatables->group_by('tds.ticket_id');
        $data1 = $this->datatables->generate('json');
        echo $data1;
    }

    /* date wise current booking */

    function date_wise_current() {
        $this->datatables->select("issue_time,");
        $this->datatables->from('tickets tk');
        $data = $this->input->post();
        $from_date = date('Y-m-d', strtotime($data['from_date']));
        $to_date = date('Y-m-d', strtotime($data['to_date']));
        if ($this->input->post('op_name')) {
            $this->datatables->where('tk.op_name', $this->input->post('op_name'));
        }

        if ($this->input->post('tran_type')) {
            $this->datatables->where('tk.transaction_status', $this->input->post('tran_type'));
        }

        if ($this->input->post('pnr_no')) {
            $this->datatables->where('tk.pnr_no', $this->input->post('pnr_no'));
        }


        if ((isset($data['from_date']) && isset($data['to_date'])) && (($data['from_date'] != "") && ($data['to_date'] != ""))) {
            if ($data['from_date'] != $data['to_date']) {
                $this->datatables->where("DATE_FORMAT(issue_time, '%Y-%m-%d') BETWEEN '" . $from_date . "' AND '" . $to_date . "' ");
            } else {
                $this->datatables->where("DATE_FORMAT(issue_time, '%Y-%m-%d')=", $from_date);
            }
        } else if ($data['from_date'] != '' && $data['to_date'] == '') {
            $this->datatables->where("DATE_FORMAT(issue_time, '%Y-%m-%d')=", $from_date);
        } else if ($data['from_date'] == '' && $data['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(issue_time, '%Y-%m-%d')=", $to_date);
        }

        $this->datatables->join('cancel_ticket_data ctd', 'tk.ticket_id=ctd.ticket_id', 'left');
        $this->datatables->join('ticket_details   tds', 'tds.ticket_id=tk.ticket_id', 'inner');
        $this->datatables->group_by('tds.ticket_id');
        $data1 = $this->datatables->generate('json');
        echo $data1;
    }

    /* daily ticket report api provider/operator wise */

    function daily_ad_tkt_op_api_view() {

        $data['api_nm'] = $this->api_provider_model->column('name,id')
                ->as_array()
                ->find_all();

        $data['op_nm'] = $this->op_master_model->column('op_id,op_name')
                ->as_array()
                ->find_all();

        load_back_view(DAILY_ADVANCE_API_OP_WISE, $data);
    }

    function daily_advance_tkt_excel() {
        $input = $this->input->get();
        $result_data = $this->report_model->daily_advance_ticket($input);
        $sr_no = 1;
        $data = array();
        foreach ($result_data as $key => $value) {
            $data[] = array('Sr no' => $sr_no,
                'Transaction status' => $value['Tran_type'],
                'Ticket Issue time' => $value['issue_time'],
                'Rokad Ref No' => $value['boss_ref_no'],
                'API Provider Name' => $value['RED BUS'],
                'Operator Name' => $value['op_name'],
                'Operator Ticket No' => $value['ticket_ref_no'],
                'From Stop Name' => $value['from_stop_name'],
                'Till Stop Name' => $value['till_stop_name'],
                'No of Passenger' => $value['num_passgr'],
                'Total Fare amount' => $value['tot_fare_amt'],
                'Reservation charges' => $value['fare_reservationCharge'],
                'Total Tax' => $value['Total_tax'],
                'Total Refund' => $value['refund_amt'],
                'Total Cancel Charge' => $value['cancel_charge'],
                'Status' => $value['status'],
            );
            $sr_no++;
        }
        $this->excel->data_2_excel('daily_advance_ticket__booking_report' . time() . '.xlsx', $data);
        die;
    }

    function daily_advance_tkt_pdf() {
        ob_start();
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);

        $pdf->SetTitle('Daily Advance ticket sales report');
        $pdf->SetHeaderMargin(20);
        $pdf->SetTopMargin(10);
        $pdf->setFooterMargin(20);
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');
        $pdf->AddPage();
        $x = $pdf->pixelsToUnits('10');
        $y = $pdf->pixelsToUnits('10');
        $font_size = $pdf->pixelsToUnits('4');
        $sr_no = 1;

        $tot_fare_amt = 0;
        $tot_res_amt = 0;
        $total_tax = 0;
        $total_booking_amt = 0;
        $total_booked_pas = 0;
        $total_can_tran = 0;
        $total_can_char = 0;
        $toral_refund_amt = 0;
        $total_failed_tran = 0;
        $total_failed_tran_amt = 0;
        $total_seat_can = 0;

        $input = $this->input->get();

        $result_data = $this->report_model->daily_advance_ticket($input);

        foreach ($result_data as $key => $val) {
            $tot_fare_amt = $tot_fare_amt + $val['tot_fare_amt'];

            if ($val['Tran_type'] == 'Success Transaction') {
                $tot_res_amt = $tot_res_amt + $val['fare_reservationCharge'];
                $total_booking_amt = $total_booking_amt + $val['tot_fare_amt_with_tax'];
                $total_booked_pas = $total_booked_pas + $val['num_passgr'];
                $total_tax = $total_tax + $val['total_charges'];
            }
            if ($val['Tran_type'] == 'Cancel Transaction') {
                $total_can_tran++;
                $total_can_char = $total_can_char + $val['cancel_charge'];
                $toral_refund_amt = $toral_refund_amt + $val['refund_amt'];
                $total_seat_can = $total_seat_can + $val['seat_can'];
            }
            if ($val['Tran_type'] == 'Payment Failed' || $val['Tran_type'] == 'Failed') {
                $total_failed_tran++;
                $total_failed_tran_amt = $total_failed_tran_amt + ($val['tot_fare_amt'] + $val['Total_tax']);
            }
        }
        // text on center

        $html = "<p style=text-align:center>Daily Advance ticket booking wise  report</p>";
        if (isset($input['op_name']) && ($input['op_name'] != "")) {
            $html .= "<p margin:50px auto;>Operator:" . $input['op_name'] . "</p>";
        }

        $html .= "<table cellspacong=5px cellpadding=18px>";
        $html .= "<tr align=right><td></td><td></td><td></td><td>" . date('Y-m-d H:i:s') . "</td></tr>
                    <tr>
                    <td>Total Fare Amount -:</td><td>" . $tot_fare_amt . "</td>
                    <td>Total Reservation Charges :</td><td>" . $tot_res_amt . "</td></tr>
                    <tr><td>Total Taxes / other Charges:</td><td>" . $total_tax . "</td>
                    <td>Total Booking Amt (Receivable) :</td><td>" . $total_booking_amt . "</td></tr>
                   
                    <tr><td>Total booked Passengers-:</td><td>" . $total_booked_pas . "</td>
                    <td>Total Cancellation Transactions-:</td><td>" . $total_can_tran . "</td></tr> 
                    
                    
                    <tr><td>Total Cancellation Charges-:</td><td>" . $total_can_char . "</td>
                    <td>Total Seat Canceled</td><td>" . $total_seat_can . "</td></tr>
                        
                   <tr>
                    <td>Total Failed Transaction-:</td><td>" . $total_failed_tran . "</td><td>Failed Transaction amount-:</td><td>" . $total_failed_tran_amt . "</td></tr>

               </table>";


        $html .= "<br><br>";
        $html .= '<table width="100%" border="2px" cellspacing="0" cellpadding="0"  class="table table-hover">
                <thead><tr style="background-color:gray; font-weight:bold;">
                <th>Sr No</th>
                 <th>Transaction status</th>
                <th>Ticket Issue time</th>
                 <th>Rokad Ref No</th>
                 <th>API Provider Name</th>
                <th>Operator Name</th>
                <th>Operator Ticket No</th>
                <th>From Stop Name</th>
                <th>Till Stop Name</th>
                <th>No of Passenger</th>
                <th>Reservation charges</th>
                <th>Reservation charges</th>
                <th>Total Tax</th>
                <th>Total Refund</th> 
                <th>Total Cancel Charge</th>
                </tr></thead><tbody>';

        foreach ($result_data as $key => $value) {
            $html .= "<tr>";
            $html .= "<td>" . $sr_no . "</td>";
            $html .= "<td>" . $value['Tran_type'] . "</td>";
            $html .= "<td>" . $value['issue_time'] . "</td>";
            $html .= "<td>" . $value['boss_ref_no'] . "</td>";
            $html .= "<td>" . $value['RED BUS'] . "</td>";
            $html .= "<td>" . $value['op_name'] . "</td>";
            $html .= "<td>" . $value['ticket_ref_no'] . "</td>";
            $html .= "<td>" . $value['from_stop_name'] . "</td>";
            $html .= "<td>" . $value['till_stop_name'] . "</td>";
            $html .= "<td>" . $value['num_passgr'] . "</td>";
            $html .= "<td>" . $value['tot_fare_amt'] . "</td>";
            $html .= "<td>" . $value['fare_reservationCharge'] . "</td>";
            $html .= "<td>" . $value['Total_tax'] . "</td>";
            $html .= "<td>" . $value['refund_amt'] . "</td>";
            $html .= "<td>" . $value['cancel_charge'] . "</td>";
            $html .= "</tr>";
            $sr_no++;
        }
        $html .= '</tbody></table>';
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output('Daily Advance ticket sales report.pdf', 'D');
        die;
    }

    /* list of users */

    function list_of_users_view() {
        load_back_view(USER_REPORT_VIEW);
    }

    function list_of_users() {

        $data = $this->input->post();       
        $from_date = date('Y-m-d', strtotime($data['from_date']));
        $to_date = date('Y-m-d', strtotime($data['to_date']));
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');

        $this->datatables->select('1,DATE_FORMAT(created_date,"%d-%m-%Y %h:%i:%s") as created_date,concat(first_name," ",last_name),ur.role_name as role_name,sm.stState,cm.stCity,mobile_no,email');
        $this->datatables->from('users u');
        $this->datatables->join('state_master sm', 'u.state=sm.intStateId', 'left');
        $this->datatables->join('city_master cm', 'cm.intCityId=u.city', 'left');
        $this->datatables->join('user_roles ur', 'u.role_id=ur.id', 'left');

        if (isset($data['from_date']) && $data['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(u.created_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($data['from_date'])));
        }

        if (isset($data['to_date']) && $data['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(u.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($data['to_date'])));
        }
        if(!empty($session_level)){
        	if($session_level > TRIMAX_LEVEL)
        	{
            	$this->datatables->where('level_'.$session_level,$session_id);
        	}
        	else
        	{
        		$this->datatables->where('level_'.TRIMAX_LEVEL, TRIMAX_LEVEL);
        	}
        }
        $this->datatables->where("u.status = 'Y'");
        $this->datatables->where("u.is_deleted = 'N'");
        
        if ($data["user_type"] == COMPANY_ROLE_ID) {
        	$this->datatables->where("u.role_id",COMPANY_ROLE_ID);
        } else if ($data["user_type"] == MASTER_DISTRIBUTOR_ROLE_ID) { 
            $this->datatables->where("u.role_id",MASTER_DISTRIBUTOR_ROLE_ID);
        } else if ($data["user_type"] == AREA_DISTRIBUTOR_ROLE_ID) {
            $this->datatables->where("u.role_id",AREA_DISTRIBUTOR_ROLE_ID);
        } else if ($data["user_type"] == DISTRIBUTOR) {
            $this->datatables->where("u.role_id",DISTRIBUTOR);
        } else if ($data["user_type"] == RETAILER_ROLE_ID) {
            $this->datatables->where("u.role_id",RETAILER_ROLE_ID);
        }
        $data1 = $this->datatables->generate('json');
        $newdata_array = json_decode($data1);
            if (!empty($newdata_array->data)) {                
                foreach ($newdata_array->data as &$detail) {
                    $detail['6'] = rokad_decrypt($detail['6'], $this->config->item('pos_encryption_key'));
                    $detail['7'] = rokad_decrypt($detail['7'], $this->config->item('pos_encryption_key'));
                }
            }
            
            echo json_encode($newdata_array);
        //7223
    }

    function user_excel() {

        $input = $this->input->get();
        $user_result = $this->report_model->user_registeraion_detail($input);
        $fields = array("Sr No.", "Registration Date", "UserName", "UserType", "State", "City", "Mobile", "Email");
        $stockArr = array();
        $sr_no = 1;

        foreach ($user_result as $key => $value) {

            $data[] = array('Sr no' => $sr_no,
                'Registration Date' => $value['created_date'],
                'Username' => $value['UserName'],
				'User Type' => $value['UserType'],
                'State' => $value['State'],
                'City' => $value['City'],
                'Mobile' => rokad_decrypt($value['Mobile'], $this->config->item("pos_encryption_key")),
                'Email' => rokad_decrypt($value['Email'], $this->config->item("pos_encryption_key"))
            );
            $sr_no++;
        }
        $this->excel->data_2_excel('user_registration_detail_excel' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
    }
    /* date wise ticket */

    function date_wise_ticket_booking_report() {

        $data['api_nm'] = $this->api_provider_model->column('name,id')
                ->as_array()
                ->find_all();

        $data['op_nm'] = $this->op_master_model->column('op_id,op_name')
                ->as_array()
                ->find_all();
        load_back_view(DATEWISE_ORS_TICKET_BOOKING_REPORT_VIEW, $data);
    }

    function date_wise_advance_booking() {

        $data = $this->input->post();
        $from_date = date('Y-m-d', strtotime($data['from_date']));
        $to_date = date('Y-m-d', strtotime($data['to_date']));
        $this->datatables->select('1,tk.issue_time,sum(if(tk.transaction_status="success",1,"0")) as "sucess Tran",'
                . 'sum(tk.tot_fare_amt) as "Total Fare",'
                . 'sum(if(tk.transaction_status="success",tk.fare_reservationCharge,"0")) as "Total Resrvaion charges",'
                . 'sum(if(tk.transaction_status="success",(tk.fare_acc+tk.fare_hr+tk.fare_it+tk.fare_octroi+tk.fare_toll+tk.fare_convey_charge),"0")) as "Total Tax",'
                . 'sum(if(tk.transaction_status="success",tk.num_passgr,"0")) as "Total_Pass",'
                . 'sum(if(tk.transaction_status="cancel",1,"0")) as "Total Cancel transaction",'
                . 'if(tk.transaction_status="cancel",count(tds.seat_no),"0") as "Total Cancel seats",'
                . 'sum(if(tk.transaction_status="cancel",ctd.cancel_charge,"0")) as "Total Cancel charges",'
                . 'sum(if(tk.transaction_status="cancel",ctd.refund_amt,"0")) as "Total refund amt",'
                . 'sum(if(tk.transaction_status in("failed","pfailed"),1,"0")) as "Total Failed Tran",'
                . 'sum(if(tk.transaction_status in("failed","pfailed"),tot_fare_amt_with_tax,"0")) as "Total Failed amount",'
                . 'sum(if(tk.transaction_status="success",1,"0")) as "Ticket sold"');

        $this->datatables->from('tickets tk');
        $this->datatables->join('ticket_details tds', 'tds.ticket_id=tk.ticket_id', 'inner');
        $this->datatables->join('cancel_ticket_data ctd', 'ctd.ticket_id=tk.ticket_id', 'left');
        if ((isset($data['from_date']) && isset($data['to_date'])) && (($data['from_date'] != "") && ($data['to_date'] != ""))) {
            if ($data['from_date'] != $data['to_date']) {
                $this->datatables->where("DATE_FORMAT(tk.issue_time, '%Y-%m-%d') BETWEEN '" . $from_date . "' AND '" . $to_date . "' ");
            } else {
                $this->datatables->where("DATE_FORMAT(tk.issue_time, '%Y-%m-%d')=", $from_date);
            }
        } else if ($data['from_date'] != '' && $data['to_date'] == '') {
            $this->datatables->where("DATE_FORMAT(tk.issue_time, '%Y-%m-%d')=", $from_date);
        } else if ($data['from_date'] == '' && $data['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(tk.issue_time, '%Y-%m-%d')=", $to_date);
        }
        if ($data['op_name'] != '') {
            $this->datatables->where("tk.op_name", $data['op_name']);
        }

        $this->datatables->group_by("DATE_FORMAT(issue_time,'%Y-%m-%d')");
        $data1 = $this->datatables->generate('json');
        echo $data1;
    }

    function date_wise_advance_booking_summary() {

        $sucess_tran = 0;
        $total_fare = 0;
        $reservation_charges = 0;
        $total_tax = 0;
        $total_Pass = 0;
        $total_cancel_transaction = 0;
        $total_cancel_seats = 0;

        $total_cancel_charges = 0;
        $total_refund_amt = 0;
        $total_failed_tran = 0;
        $total_book_amt = 0;
        $total_failed_amt = 0;
        $tkt_sold = 0;

        $data = $this->input->post();
        $res = $this->report_model->daily_ticket_wise_summary($data);
        foreach ($res as $key => $val) {
            $sucess_tran = $sucess_tran + $val['sucess Tran'];
            $total_fare = $total_fare + $val['Total Fare'];
            $reservation_charges = $reservation_charges + $val['Total Resrvaion charges'];
            $total_tax = $total_tax + $val['Total Tax'];
            $total_Pass = $total_Pass + $val['Total_Pass'];
            $total_cancel_transaction = $total_cancel_transaction + $val['Total Cancel transaction'];
            $total_cancel_seats = $total_cancel_seats + $val['Total Cancel seats'];
            $total_book_amt = $total_book_amt + $val['Total_booked_amt'];
            $total_cancel_charges = $total_cancel_charges + $val['Total Cancel charges'];
            $total_refund_amt = $total_refund_amt + $val['Total refund amt'];
            $total_failed_tran = $total_failed_tran + $val['Total Failed Tran'];
            $total_failed_amt = $total_failed_amt + $val['Total Failed amount'];
            $tkt_sold = $tkt_sold + $val['Ticket sold'];
        }
        $data['sucess_tran'] = $sucess_tran;
        $data['total_fare'] = $total_fare;
        $data['reservation_charges'] = $reservation_charges;
        $data['total_booked_amt'] = $total_book_amt;
        $data['total_tax'] = $total_tax;
        $data['total_Pass'] = $total_Pass;
        $data['total_cancel_transaction'] = $total_cancel_transaction;
        $data['total_cancel_seats'] = $total_cancel_seats;
        $data['total_cancel_charges'] = $total_cancel_charges;
        $data['total_refund_amt'] = $total_refund_amt;
        $data['total_failed_tran'] = $total_failed_tran;
        $data['total_failed_amt'] = $total_failed_amt;
        $data['tkt_sold'] = $tkt_sold;

        data2json($data);
    }

    function date_wise_advance_booking_pdf() {

        ob_start();
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);

        $pdf->SetTitle('Date Wise Advance ticket sales report');
        $pdf->SetHeaderMargin(20);
        $pdf->SetTopMargin(10);
        $pdf->setFooterMargin(20);
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');
        $pdf->AddPage();
        $x = $pdf->pixelsToUnits('10');
        $y = $pdf->pixelsToUnits('10');
        $font_size = $pdf->pixelsToUnits('4');
        $sr_no = 1;

        $sucess_tran = 0;
        $total_fare = 0;
        $reservation_charges = 0;
        $total_tax = 0;
        $total_Pass = 0;
        $total_cancel_transaction = 0;
        $total_cancel_seats = 0;
        $total_cancel_charges = 0;
        $total_refund_amt = 0;
        $total_failed_tran = 0;
        $total_book_amt = 0;
        $total_failed_amt = 0;
        $tkt_sold = 0;
        $input = $this->input->get();

        $res = $this->report_model->daily_ticket_wise_summary($input);
        foreach ($res as $key => $val) {
            $sucess_tran = $sucess_tran + $val['sucess Tran'];
            $total_fare = $total_fare + $val['Total Fare'];
            $reservation_charges = $reservation_charges + $val['Total Resrvaion charges'];
            $total_tax = $total_tax + $val['Total Tax'];
            $total_Pass = $total_Pass + $val['Total_Pass'];
            $total_cancel_transaction = $total_cancel_transaction + $val['Total Cancel transaction'];
            $total_cancel_seats = $total_cancel_seats + $val['Total Cancel seats'];
            $total_book_amt = $total_book_amt + $val['Total_booked_amt'];
            $total_cancel_charges = $total_cancel_charges + $val['Total Cancel charges'];
            $total_refund_amt = $total_refund_amt + $val['Total refund amt'];
            $total_failed_tran = $total_failed_tran + $val['Total Failed Tran'];
            $total_failed_amt = $total_failed_amt + $val['Total Failed amount'];
            $tkt_sold = $tkt_sold + $val['Ticket sold'];
        }

        // text on center

        $html = "<p style=text-align:center>Daily Advance ticket booking wise  report</p>";
        if (isset($input['op_name']) && ($input['op_name'] != "")) {
            $html .= "<p margin:50px auto;>Operator:" . $input['op_name'] . "</p>";
        }

        $html .= "<table cellspacong=5px cellpadding=18px>";
        $html .= "<tr align=right>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>" . date('Y-m-d H:i:s') . "</td>
                    </tr>
                    <tr>
                    <td>Total Success Transaction:- -:</td><td>" . $sucess_tran . "</td>
                    <td>Total Fare Amount:- </td><td>" . $total_fare . "</td>
                   </tr>
                   
                    <tr>
                    <td>Total Reservation Charges:-</td><td>" . $reservation_charges . "</td>
                    <td>Total Taxes / other Charges:-</td><td>" . $total_tax . "</td>
                    </tr>
                  
                    
                    <tr>
                    <td>Total Booking Amt (Receivable):-</td><td>" . $total_book_amt . "</td>
                    <td>Total booked Passengers:</td><td>" . $total_Pass . "</td>
                    </tr>
                        
                     <tr>
                      <td>Total Cancellation Transactions:-</td><td>" . $total_cancel_transaction . "</td>
                       <td>Total Cancellation Charges:-</td><td>" . $total_cancel_charges . "</td>
                      </tr>

                    <tr>
                        <td>Total Refund Amt (Payable):-</td><td>" . $total_refund_amt . "</td>
                        <td>Total Number of seats Cancelled:-</td><td>" . $total_cancel_seats . "</td>
                     </tr>
                     
                    <tr>
                    <td>Total Failed Transaction:-</td><td>" . $total_failed_tran . "</td>
                    <td>Amt involved in Failed Transactions:-</td><td>" . $total_failed_amt . "</td>
                    </tr>     
                    

                      <tr>
                        <td>Prepaid A/c Opening Balance:-</td><td>NAN</td>
                        <td>Prepaid A/c Day Top Up Amount:-</td><td>NAN</td>
                     </tr>
                     
                      <tr>
                            <td>Prepaid A/c Day Top Up Amount:-</td><td>NAN</td>
                            <td>Prepaid A/c Closing Balance:-</td><td>NAN</td>
                      </tr> 
                      
                    <tr>
                        <td>Cumulative Ticket sold:-</td><td>" . $tkt_sold . "</td>
                        <td>Cumulative Booking Amount:-</td><td>NAN</td>
                    </tr>         
                    <tr>
                     <td>Cumulative Refund Amount:-:-</td><td>NAN</td>
                     <td></td><td></td>
                    </tr> 
               </table>";
        $html .= "<br><br>";
        $html .= '<table width="100%" border="2px" cellspacing="0" cellpadding="0"  class="table table-hover">
                        <thead>
                        <tr style="background-color:gray; font-weight:bold;">
                        <th>Sr No</th>
                        <th>Issue Date</th>
                        <th>Success Transaction</th>
                        <th>Total Fare</th>
                        <th>Total Booking Amount</th>
                        <th>Total Reservation charges</th>
                        <th>Total Tax</th>
                        <th>Total Passenger</th>
                        <th>Total Cancellation Transactions</th>
                        <th>Total Cancel seats</th>
                        <th>Total Cancellation Charges</th>
                        <th>Total Refund</th> 
                        <th>Total Failed Transaction</th>
                        <th>Total Failed Transaction Amount</th>
                        <th>Ticket Sold</th>
                        </tr>
                        </thead><tbody>';

        foreach ($res as $key => $value) {
            $html .= "<tr>";
            $html .= "<td>" . $sr_no . "</td>";
            $html .= "<td>" . $value['issue_time'] . "</td>";
            $html .= "<td>" . $value['sucess Tran'] . "</td>";
            $html .= "<td>" . $value['Total Fare'] . "</td>";
            $html .= "<td>" . $value['Total_booked_amt'] . "</td>";
            $html .= "<td>" . $value['Total Resrvaion charges'] . "</td>";
            $html .= "<td>" . $value['Total Tax'] . "</td>";
            $html .= "<td>" . $value['Total_Pass'] . "</td>";
            $html .= "<td>" . $value['Total Cancel transaction'] . "</td>";
            $html .= "<td>" . $value['Total Cancel seats'] . "</td>";
            $html .= "<td>" . $value['Total Cancel charges'] . "</td>";
            $html .= "<td>" . $value['Total refund amt'] . "</td>";
            $html .= "<td>" . $value['Total Failed Tran'] . "</td>";
            $html .= "<td>" . $value['Total Failed amount'] . "</td>";
            $html .= "<td>" . $value['Ticket sold'] . "</td>";
            $html .= "</tr>";
            $sr_no++;
        }
        $html .= '</tbody></table>';
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output('Date Wise Advance ticket sales report.pdf', 'D');
        die;
    }

    function date_wise_advance_booking_excel() {
        $data = $this->input->get();
        $res = $this->report_model->daily_ticket_wise_summary($data);
        $sr_no = 1;
        foreach ($res as $key => $value) {
            $data[] = array("Sr No" => $sr_no,
                "Issue Date" => $value['issue_time'],
                "Success Transaction" => $value['sucess Tran'],
                "Total Fare" => $value['Total Fare'],
                "Total Booking Amount" => $value['Total_booked_amt'],
                "Total Reservation Charges" => $value['Total Resrvaion charges'],
                "Total Tax" => $value['Total Tax'],
                "Total Passenger" => $value['Total_Pass'],
                "Total Cancel Transaction" => $value['Total Cancel transaction'],
                "Total Cancel seats" => $value['Total Cancel seats'],
                "Total Cancel charges" => $value['Total Cancel charges'],
                "Total Refund Amount" => $value['Total refund amt'],
                "Total Failed Transaction" => $value['Total Failed Tran'],
                "Total Failed Amount" => $value['Total Failed amount'],
                "Total Ticket sold" => $value['Ticket sold']);

            $sr_no++;
        }
        $this->excel->data_2_excel('Date_wise_advance_booking' . time() . '.xlsx', $data);
    }

    function daily_advance_tkt() {
        $input = $this->input->post();
        $res['summary'] = $this->report_model->daily_advance_ticket($input);
        data2json($res);
    }

    function bos_tkt_details_view() {
        $array_input = array();
        $data['tran_type_data'] = array(
            "success" => "Success",
            "incomplete" => "Incomplete",
            "pfailed" => "Payment Initiate",
            "aborted" => "Aborted",
            "failure" => "Payment Failed",
            "failed" => "Failed",
            "cancel" => "Cancel"
        );
        //  show($this->input->post(),1);
        $data['pnr_no'] = $this->tickets_model->column('pnr_no')
                ->where('pnr_no!=', '')
                ->as_array()
                ->find_all();

        $data['summary_details'] = $this->report_model->cal_summary_bos_tkt_details_rpt($array_input);
        //$this->bos_tkt_details();
        load_back_view(BOS_TICKET_DETAILS_VIEW, $data);
    }

    function bos_tkt_details() {
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');
        $this->datatables->select('1,
                        t.ticket_id,
                        t.`transaction_status` AS "Transaction Status",
                        t.`boss_ref_no` AS "Boss Reference No",
                        t.pnr_no As "PNR NO",
                        t.ticket_ref_no AS "Ticket Ref No",
                        
                        ap.name as "Povider Name",
                        t.`op_name` AS "Operator Name",

                        t.from_stop_name as "From Stop",                      

                        t.till_stop_name as "To Stop",
                        
                       
                        t.boarding_stop_name as "Boarding Stop",
                        DATE_FORMAT(t.inserted_date,"%d-%m-%Y %h:%i:%s") as "issued date",

                        DATE_FORMAT(t.dept_time,"%d-%m-%Y %h:%i:%s") as doj,
                        t.num_passgr as "No of passenger",

                        IFNULL(t.`tot_fare_amt`,0) AS "Basic Fare",
                        IFNULL(round(sum(td.service_tax),2),0) AS "Service Tax",
                        IFNULL(t.`total_fare_without_discount`,0) AS "Total Fare Without Discount",   
                       
                        
                        CASE t.`booked_from`    WHEN "upsrtc" THEN
        IF(t.`total_fare_without_discount` IS NOT NULL,(t.`total_fare_without_discount` - t.`tot_fare_amt`),0) ELSE 0 END AS "UPSRTC Additional Charges (3% or Rs.50)",
                        IFNULL(t.`discount_on`,"-") AS "Discount On",
                        IFNULL(t.`discount_type`,"-") AS  "Discount Type",
                        t.`discount_per` AS "Discount Percentage",
                        t.`discount_value` AS "Discount Price",
                        t.`tot_fare_amt_with_tax` AS "Total Fare Paid",
                        
                        IFNULL(ctd.`pg_refund_amount`,0) AS "Refund Amount",
                         IF(ctd.is_refund=1,(ctd.`cancel_charge`),0) AS "Cancel Charge",
                        t.pg_tracking_id AS "PG Track Id",if(t.`transaction_status` = "cancel",count(t.ticket_id),0) cancel_ticket,
                        if(t.`transaction_status` = "cancel",count(t.num_passgr),0) cancel_psgre
                    ');
        $this->datatables->from('tickets t');

        $this->datatables->join('users u', 'u.id = t.inserted_by', 'inner');
        $this->datatables->join('ticket_details td', 'td.ticket_id = t.ticket_id', 'inner');
        $this->datatables->join('payment_transaction_fss ptf', 'ptf.ticket_id=t.ticket_id', 'left');
        $this->datatables->join('api_provider ap', 't.provider_id = ap.id', 'left');
        $this->datatables->join('cancel_ticket_data ctd', 'ctd.ticket_id = t.ticket_id', 'left');
        
        if(!empty($session_level)){
            $this->datatables->where('t.inserted_by',$session_id);
        }
        if ($this->input->post('operator_id')) {
            $this->datatables->where("booked_from", $this->input->post('operator_id'));
        }

        if ($this->input->post('from_date')) {
            $this->datatables->where("DATE_FORMAT(issue_time, '%Y-%m-%d')>=", date('Y-m-d', strtotime($this->input->post('from_date'))));
        }

        if ($this->input->post('till_date')) {
            $this->datatables->where("DATE_FORMAT(issue_time, '%Y-%m-%d')<=", date('Y-m-d', strtotime($this->input->post('till_date'))));
        }

        if ($this->input->post('tran_type') && $this->input->post('tran_type') != 'select_status') {
            $status = $this->input->post('tran_type');
            if ($status == 'failed') {
                $status = array('psuccess', 'pfailed', 'failed');
            }
            $this->datatables->where_in('t.transaction_status', $status);
        }

        if ($this->input->post('pnr_no')) {
            $this->datatables->where('t.pnr_no', $this->input->post('pnr_no'));
        }

        $this->datatables->group_by('t.ticket_id');

        $data = $this->datatables->generate('json');

        //  show($this->db->last_query(),1);
        echo $data;
    }

    function bos_tkt_details_count_view() {

        $data['tran_type_data'] = array(
            "success" => "Success",
            "incomplete" => "Incomplete",
            "pfailed" => "Payment Initiate",
            "aborted" => "Aborted",
            "failure" => "Payment Failed",
            "failed" => "Failed",
            "cancel" => "Cancel"
        );
        load_back_view(BOS_TICKET_DETAILS_COUNT_VIEW, $data);
    }

    function bos_tkt_details_count() {
        $data = $this->input->post();
        $from_date = date('Y-m-d', strtotime($data['from_date']));
        $till_date = date('Y-m-d', strtotime($data['till_date']));
        $status = $data['tran_type'];
        $is_ors_agent = false;
        $filter_by_user_id = "";

        eval(FME_AGENT_ROLE_NAME);
        if (in_array(strtolower($this->session->userdata("role_name")), $fme_agent_role_name)) {
        //if(strtolower($this->session->userdata("role_name")) == ORS_AGENT_ROLE_NAME)
            $is_ors_agent = true;
            $filter_by_user_id = $this->session->userdata("user_id");
        }

        if (!$is_ors_agent && !empty($this->input->post('user_id')) && $this->input->post('user_id') != '') {
            $filter_by_user_id = $this->input->post('user_id');
        }

        $user_tickets = $this->input->post('user_tickets');
        if ($user_tickets == "trimax") {

            $this->db->select('GROUP_CONCAT(id) as employee_id', false);
            $this->db->from("users");
            $this->db->where("is_employee", '1');
            $employee = $this->db->get();
            $employeeids = $employee->result_array();
            // show($employeeids,1);
            if ($employeeids) {
                $employeeids = $employeeids[0];
            }
        }
        if ($this->input->post('tran_type') && $this->input->post('tran_type') != 'select_status') {
            $status = $this->input->post('tran_type');

            if ($status == 'failed') {

                $status = array('psuccess', 'pfailed', 'failed');
            }
        }



        $this->datatables->select("0,date_format(date,'%d-%m-%Y'),IFNULL(count_uid,0), IFNULL(count_ticketId,0),IFNULL(count_mobileticket,0), IFNULL(sum_num_psnger,0) ,IFNULL(if_total_fare,0), 
            IFNULL(sum_disc_val,0), IFNULL(if_can_charge,0), IFNULL(if_refund_amt,0),
                                    IFNULL(commission_value,0), IFNULL(Reveue,0),FORMAT(IFNULL(actual_revenue,0),2) ");
        $sql_query = "(select (case when dd is null then created_date else dd end) date, `count_uid`, `count_ticketId`, `count_mobileticket`,
                    `sum_num_psnger`, `if_total_fare`, `sum_disc_val`, `if_can_charge`, `if_refund_amt`, `commission_value`, 
                    Reveue,actual_revenue   
                    from (
                    select date(u.created_date) created_date, count(u.id) count_uid  from users u where u.status = 'Y'";
        if ($this->input->post('role_id') != '' && $this->input->post('role_id') != '0') {
            if ($this->input->post('role_id') == 'trimax') {
                $sql_query .= " AND u.is_employee ='1' and u.salt !='' and u.password != ''";
                if ($filter_by_user_id != '') {
                    $sql_query .= " AND u.id ='" . $filter_by_user_id . "' ";
                }
            } else {
                eval(FME_AGENT_ROLE_ID);
                if (in_array($this->input->post('role_id'), $fme_agent_role_id)) {
                    // if($this->input->post('role_id')=='16'){
                    $sql_query .= " AND u.verify_status ='Y' ";
                }
                $sql_query .= " AND u.role_id = '" . $this->input->post('role_id') . "' and u.salt !='' and u.password != '' ";
//                              if( $this->input->post('user_id')!=''){
//                                $sql_query.= " AND u.id ='".$this->input->post('user_id')."' "; 
//                            }
            }
        } else {
            if ($this->input->post('role_id') == '0') {
                $sql_query .= " AND u.role_id = '7' and u.salt ='' and u.password = '' ";
            } else {
                $sql_query .= "  and u.salt !='' and u.password != ''";
            }
        }
        if ($filter_by_user_id != '') {
            $sql_query .= " AND u.id ='" . $filter_by_user_id . "' ";
        }
        //  $sql_query.= " AND u.role_id = 7 and u.salt !='' and u.password != '' ";
        $sql_query .= " group by  DATE_FORMAT(u.created_date, '%Y-%m-%d') ) a                                                                        
                    left join (

                    SELECT DATE_FORMAT(t.inserted_date, '%Y-%m-%d') as dd, COUNT(t.ticket_id) count_ticketId,SUM(if(`booked_by` = 'bos_mobile_api_user',1,0)) count_mobileticket, 
                    SUM(t.num_passgr) sum_num_psnger, 
                   
                    SUM(t.`tot_fare_amt`) AS if_total_fare,
                    sum(t.discount_value) sum_disc_val, 
                   SUM(IF(ctd.is_refund='1',ctd.cancel_charge,'0')) if_can_charge,
                    /*IFNULL(SUM(ctd.`pg_refund_amount`), 0) if_refund_amt, */
                     SUM(IF(ctd.is_refund='1',ctd.`pg_refund_amount,'0')) if_refund_amt,
                  
                    FORMAT(SUM(IF(t.status = 'Y',
                            IF(op_name='UPSRTC',
                               t.fare_convey_charge,
                               (t.total_fare_without_discount * t.commision_percentage/100)),
                              '0')
                           ),2
                       ) AS commission_value,

                   
                    SUM(t.tot_fare_amt - t.`discount_value`) AS Reveue,
                    SUM(t.tot_fare_amt - t.`discount_value` - IFNULL(ctd.`pg_refund_amount`,'0')) AS actual_revenue
                     FROM `tickets` `t` 
                    LEFT JOIN `cancel_ticket_data` `ctd` ON `ctd`.`ticket_id` = `t`.`ticket_id`";

        if ($this->input->post('tran_type') && $this->input->post('tran_type') != 'select_status') {
            $status = $this->input->post('tran_type');
            //$sql_query.= " WHERE t.`transaction_status`IN ('".$status."')  ";
            if ($status == 'failed') {
                $status = array('psuccess', 'pfailed', 'failed');
                $sql_query .= " WHERE t.`transaction_status`IN ('psuccess','pfailed','failed')  ";
                if ($filter_by_user_id != '') {
                    $sql_query .= " AND t.inserted_by ='" . $filter_by_user_id . "' ";
                }
            } else {
                $sql_query .= " WHERE t.`transaction_status`IN ('" . $status . "')  ";
                if ($filter_by_user_id != '') {
                    $sql_query .= " AND t.inserted_by ='" . $filter_by_user_id . "' ";
                }
            }
        }

        //  $sql_query.= " WHERE t.`transaction_status`IN ('".$status."')  ";
        if ($this->input->post('role_id') != '') {
            $sql_query .= " and t.role_id= '" . $this->input->post('role_id') . "' ";
        }

        $sql_query .= " GROUP BY  DATE_FORMAT(t.`inserted_date`, '%Y-%m-%d')

                    ) b on a.created_date = b.dd                                                                        

                    union all                                                                        
                    select (case when created_date is null then dd else created_date end) date, `count_uid`, `count_ticketId`,`count_mobileticket`, `sum_num_psnger`, 
                    `if_total_fare`, `sum_disc_val`, `if_can_charge`, `if_refund_amt`, `commission_value`, Reveue,actual_revenue                                    
                    from (select date(u.created_date) created_date, count(u.id) count_uid  from users u 
                    where u.status = 'Y' ";
        // $sql_query.= " AND u.role_id = 7 and u.salt !='' and u.password != '' ";

        if ($this->input->post('role_id') != '' && $this->input->post('role_id') != '0') {
            if ($this->input->post('role_id') == 'trimax') {
                $sql_query .= " AND u.is_employee ='1' and u.salt !='' and u.password != ''";
//                            if( $this->input->post('user_id')!=''){
//                                $sql_query.= " AND u.id ='".$this->input->post('user_id')."' "; 
//                            }
            } else {
                eval(FME_AGENT_ROLE_ID);
                if (in_array($this->input->post('role_id'), $fme_agent_role_id)) {
                    //if($this->input->post('role_id')=='16'){
                    $sql_query .= " AND u.verify_status ='Y' ";
                }
                $sql_query .= " AND u.role_id = '" . $this->input->post('role_id') . "' and u.salt !='' and u.password != '' ";
            }
        } else {
            if ($this->input->post('role_id') == '0') {
                $sql_query .= " AND u.role_id = '7' and u.salt ='' and u.password = '' ";
            } else {
                $sql_query .= "  and u.salt !='' and u.password != ''";
            }
        }
        if ($this->input->post('user_id') != '') {
            $sql_query .= " AND u.id ='" . $this->input->post('user_id') . "' ";
        }
        $sql_query .= " group by  DATE_FORMAT(u.created_date, '%Y-%m-%d') ) a                                                                        

                    right join (
                    SELECT DATE_FORMAT(t.inserted_date, '%Y-%m-%d') as dd, COUNT(t.ticket_id) count_ticketId, SUM(if(`booked_by` = 'bos_mobile_api_user',1,0))  count_mobileticket,
                    SUM(t.num_passgr) sum_num_psnger,SUM(t.`tot_fare_amt`) AS if_total_fare,sum(t.discount_value) sum_disc_val, 
                    SUM(IF(ctd.is_refund='1',ctd.cancel_charge,'0')) if_can_charge,
                  /*  IFNULL(SUM(ctd.`pg_refund_amount`), 0) if_refund_amt,*/
                    SUM(IF(ctd.is_refund='1',ctd.`pg_refund_amount,'0')) if_refund_amt,
                    
                    FORMAT(SUM(IF(t.status = 'Y',
                            IF(op_name='UPSRTC',
                               t.fare_convey_charge,
                               (t.total_fare_without_discount * t.commision_percentage/100)),
                              '0')
                           ),2
                       ) AS commission_value,
                
                    SUM(t.tot_fare_amt - t.`discount_value`) AS Reveue,
                    SUM(t.tot_fare_amt - t.`discount_value` - IFNULL(ctd.`pg_refund_amount`,'0')) AS actual_revenue
                    FROM `tickets` `t` 
                    LEFT JOIN `cancel_ticket_data` `ctd` ON `ctd`.`ticket_id` = `t`.`ticket_id`";
        //  $sql_query.= "WHERE t.`transaction_status`IN ('".$status."')  ";

        if ($this->input->post('tran_type') && $this->input->post('tran_type') != 'select_status') {
            $status = $this->input->post('tran_type');

            if ($status == 'failed') {
                $status = array('psuccess', 'pfailed', 'failed');
                $sql_query .= " WHERE t.`transaction_status`IN ('psuccess','pfailed','failed')  ";

                if ($filter_by_user_id != '') {
                    $sql_query .= " AND t.inserted_by ='" . $filter_by_user_id . "' ";
                }
            } else {
                $sql_query .= " WHERE t.`transaction_status`IN ('" . $status . "')  ";
                if ($filter_by_user_id != '') {
                    $sql_query .= " AND t.inserted_by ='" . $filter_by_user_id . "' ";
                }
            }
        }
        //$sql_query.= " and t.role_id= '7' ";
        if ($this->input->post('role_id') != '') {
            $sql_query .= " and t.role_id= '" . $this->input->post('role_id') . "' ";
        }
        $sql_query .= " GROUP BY  DATE_FORMAT(t.`inserted_date`, '%Y-%m-%d')

                    ) b on a.created_date = b.dd ) abc  ";

        /* $sql_query.= " GROUP BY substr(t.inserted_date, 1, 10)) b on a.created_date = b.dd ) abc "; */
        if ($this->input->post('from_date')) {
            $sql_query .= "where date >='$from_date'";
        }
        if ($this->input->post('till_date')) {
            $sql_query .= " and date <='$till_date'";
        }

        $sql_query .= "group by date ";
        $this->db->order_by("date", "desc");
        $this->datatables->from($sql_query);

        $data = $this->datatables->generate('json');


        echo $data;
    }

    public function bos_tkt_details_count_export_excel() {
        $input = $this->input->get();

        $ticket_result = $this->report_model->bos_tkt_details_count_excel($input);
        //show($ticket_result,1);
        $fields = array("Sr No.", "Date", "No of Registered Users", "No of Tickets Booked", "No of Passenger", "Actual Fare Amount", "Discounted Amount", "Cancellation Charges", "Refund Amount", "Commission Amount", "Total Reveue", "Actual Revenue");
        $stockArr = array();
        $sr_no = 1;
        if (count($ticket_result) > 0) {

            foreach ($ticket_result as $key => $value) {
                $act_rev = $value['Reveue'] - $value['tot_can'];
                $data[] = array('Sr no' => $sr_no,
                    'Date' => $value['date'],
                    'No of Registered Users' => $value['tot_reg_users'],
                    'No of Tickets Booked' => $value['tot_tick'],
                    'No of Passenger' => $value['tot_psgr'],
                    'Actual Fare Amount' => $value['tot_basic'],
                    'Discounted Amount' => $value['tot_disc_val'],
                    'Cancellation Charges' => $value['tot_can'],
                    'Refund Amount' => $value['tot_ref_amt'],
                    'Commission Amount' => $value['commission_value'],
                    'Total Reveue' => $value['Reveue'],
                    'Actual Revenue' => $value['actual_revenue']);
                $sr_no++;
            }
            $this->excel->data_2_excel('bos_ticket_details_count' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
            die;
        }
    }

    function bos_tkt_details_export_excel() {
        $input = $this->input->get();

        $ticket_result = $this->report_model->bos_tkt_details_excel_sheet($input);
        //show($ticket_result,1);
        $stockArr = array();
        $sr_no = 1;
        if (count($ticket_result) > 0) {

            foreach ($ticket_result as $key => $value) {

                //$act_rev=$value['reveue']-$value['tot_can'];
                $data[] = array('Sr no' => $sr_no,
                    'Ticket id' => $value['ticket_id'],
                    'Transaction Status' => $value['Transaction Status'],
                    'Rokad Ref No' => $value['Boss Reference No'],
                    'PNR NO' => $value['PNR NO'],
                    'Ticket Ref No' => $value['Ticket Ref No'],
                    'Povider Name' => $value['Povider Name'],
                    'Operator Name' => $value['Operator Name'],
                    'From Stop' => $value['From Stop'],
                    'To Stop' => $value['To Stop'],
                    'Boarding Stop' => $value['Boarding Stop'],
                    'Issued date' => $value['issued date'],
                    'DOJ' => $value['doj'],
                    'No of passenger' => $value['No of passenger'],
                    'Basic Fare' => $value['Basic Fare'],
                    'Service Tax' => $value['Service Tax'],
                    'Total Fare Without Discount' => $value['Total Fare Without Discount'],
                    'UPSRTC Charges (3% or Rs.50)' => $value['UPSRTC Additional Charges (3% or Rs.50)'],
                    'Discount On' => $value['Discount On'],
                    'Discount Type' => $value['Discount Type'],
                    'Discount Percentage' => $value['Discount Percentage'],
                    'Discount Price' => $value['Discount Price'],
                    'Total Fare Paid' => $value['Total Fare Paid'],
                    'Refund Amount' => $value['Refund Amount'],
                    'Cancel Charge' => $value['Cancel Charge'],
                    'PG Track Id' => $value['PG Track Id']
                );
                $sr_no++;
            }
            $this->excel->data_2_excel('bos_ticket_details_report' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
            die;
        }
    }

    function commission_offer_view() {
        $array_input = array();
        $input = $this->input->get();
        $data['status_id'] = isset($input['id']) ? $input['id'] : '';
        $data['tran_type_data'] = array(
            "success" => "Success",
            "incomplete" => "Incomplete",
            "pfailed" => "Payment Initiate",
            "aborted" => "Aborted",
            "failure" => "Payment Failed",
            "failed" => "Failed",
            "cancel" => "Cancel"
        );
        $data['state_name'] = $this->state_master_model->get_all_state();
        $data['city_name']  = $this->city_master_model->get_all_city();
        load_back_view(BOS_COMMISSION_OFFER_VIEW, $data);
    }
    public function get_statewise_city()
    {
      $data=array();
      $input=array();
      $input = $this->input->post();
      $data['city'] = $this->city_master_model->get_city($input['state_id']);
      data2json($data);
    }

    function bos_commission_offer() {

        $role_id_name = "";
        $role_id_name = $this->input->post('role_id');
        $employees_in = [];

        if ($role_id_name == "trimax") {
            $this->db->select('group_concat(id) as employees')->from('users')->where("is_employee", "1");
            $employee_query = $this->db->get();
            if ($employee_query->num_rows()) {
                $emp_data = $employee_query->result_array();
                $emp_data = $emp_data[0]["employees"];
                $employees_in = explode(",", $emp_data);
            }
        }
        
        $group_by = true;
        $provider_operator_details = $this->base_commission_model->base_commission_detail($group_by);
        $mid_query = '';
        foreach($provider_operator_details as $val)
        {
            if($val->operator_name != '')
            {
                $name = strtolower($val->operator_name);
            }
            else
            {
                $name = strtolower($val->provider_name);
            }
            
            $mid_query  .=  'WHEN "'.$name.'" THEN sum(td.service_tax)';
        }
        $this->datatables->select('1,
                        t.`boss_ref_no` AS "Boss Reference No",
                        t.pnr_no As "PNR NO",t.api_ticket_no,
                        t.`booked_by` AS "user type",
                        td.psgr_name as first_name,
                        t.user_email_id as email,
                        u.id as agent_id,
                        t.mobile_no,
                        sm.stState,
                        cm.stCity,
                        t.`op_name` AS "Operator Name",
                        ap.name as "Povider Name",
                        t.from_stop_name as "From Stop",                      
                        t.till_stop_name as "To Stop",
                        DATE_FORMAT(t.inserted_date,"%d-%m-%Y %h:%i:%s") as "issued date",
                        DATE_FORMAT(t.dept_time,"%d-%m-%Y %h:%i:%s") as doj,
                        t.num_passgr as "No of passenger",
                        IFNULL(t.`tot_fare_amt`,0) AS "Basic Fare",
                        CASE t.booked_from 
                        '.$mid_query.'
                        END AS "Service Tax",
                        IFNULL(t.`total_fare_without_discount`,0) AS "Total Fare Without Discount",   
                        IF(t.`booked_from` = "upsrtc", t.`fare_convey_charge`, "0") AS "UPSRTC Additional Charges (3% or Rs.50)",
                        IFNULL(t.`discount_on`,"-") AS "Discount On",
                        IFNULL(t.`discount_type`,"-") AS  "Discount Type",
                        t.`discount_per` AS "Discount Percentage",
                        t.`discount_value` AS "Discount Price",
                        t.`tot_fare_amt_with_tax` AS "Total Fare Paid",
                        IFNULL(ctd.`pg_refund_amount`,0) AS "Refund Amount",
                        IF(ctd.is_refund = "1",(ctd.`cancel_charge`),0)  AS "Cancel Charge",
                        t.pg_tracking_id AS "pg_track_id",
                        t.payment_by as payment_by,
                        t.`transaction_status` AS "transaction_status",
                        t.ticket_ref_no,
                        t.ticket_id,
                        
                        CASE t.booked_from 
                        WHEN "etravelsmart" THEN t.commision_percentage 
                        WHEN "upsrtc" THEN "3 % or 50 "
                        WHEN "msrtc" THEN (' . MSRTC_COMMISSION_PERCENTAGE . ')  
                        WHEN "hrtc" THEN (' . HRTC_COMMISSION_PERCENTAGE . ')
                        WHEN "rsrtc" THEN t.fare_convey_charge
                        WHEN "infinity" THEN t.commision_percentage
                        END AS "commission_percentage",
                        
                        CASE t.booked_from 
                        WHEN "etravelsmart" THEN round(((t.tot_fare_amt * t.commision_percentage)/ 100),2)
                        WHEN "upsrtc" THEN round(t.fare_convey_charge ,2) 
                        WHEN "msrtc" THEN round(t.tot_fare_amt * ' . MSRTC_COMMISSION_PERCENTAGE . ' / 100 ,2)  
                        WHEN "hrtc" THEN round(t.tot_fare_amt * ' . HRTC_COMMISSION_PERCENTAGE . ' / 100,2)
                        WHEN "rsrtc" THEN t.fare_convey_charge
                        WHEN "infinity" THEN round(((t.tot_fare_amt * t.commision_percentage)/ 100),2)
                        ELSE 0
                        END AS "commission_value" '
        );

        $this->datatables->from('tickets t');
        $this->datatables->join('ticket_details td', 'td.ticket_id = t.ticket_id', 'inner');
        /* $this->datatables->join('payment_transaction_fss ptf', 'ptf.ticket_id=t.ticket_id', 'left'); */
        $this->datatables->join('api_provider ap', 't.provider_id = ap.id', 'left');
        $this->datatables->join('users u', 't.inserted_by = u.id', 'left');
        $this->datatables->join('state_master sm', 'u.state = sm.intStateId', 'left');
        $this->datatables->join('city_master cm', 'u.city = cm.intCityId', 'left');
        $this->datatables->join('cancel_ticket_data ctd', 'ctd.ticket_id = t.ticket_id', 'left');

        if ($this->input->post('provider_id')) {
            $this->datatables->where("provider_id", $this->input->post('provider_id'));
        }
        if ($this->input->post('state_type')) {
            $this->datatables->where('sm.intStateId', $this->input->post('state_type'));
        }
        if ($this->input->post('city_type')) {
            $this->datatables->where('cm.intCityId', $this->input->post('city_type'));
        }

        if ($this->input->post('from_date')) {
            $this->datatables->where("DATE_FORMAT(issue_time, '%Y-%m-%d')>=", date('Y-m-d', strtotime($this->input->post('from_date'))));
        }

        if ($this->input->post('till_date')) {
            $this->datatables->where("DATE_FORMAT(issue_time, '%Y-%m-%d')<=", date('Y-m-d', strtotime($this->input->post('till_date'))));
        }

        if ($this->input->post('tran_type') && $this->input->post('tran_type') != 'select_status') {
            $status = $this->input->post('tran_type');
            if ($status == 'failed') {
                $status = array('psuccess', 'pfailed', 'failed');
            } else if ($status == 'success' || $status == 'cancel') {
                $this->datatables->where('t.pnr_no != ', "");
            }
            $this->datatables->where_in('t.transaction_status', $status);
        }
        if ($this->input->post('payment_by') && $this->input->post('payment_by') !='') {
            $this->datatables->where('t.payment_by', $this->input->post('payment_by'));
        }

        if ($this->input->post('pnr_no')) {
            $this->datatables->where('t.pnr_no', $this->input->post('pnr_no'));
        }

        eval(SYSTEM_USERS_ARRAY);
        eval(FME_AGENT_ROLE_NAME);
        if (in_array(strtolower($this->session->userdata("role_name")), $system_users_array)) {
            if ($role_id_name != '' && !$this->input->post('user_id')) {
                if (!in_array($role_id_name, array("trimax", "sys_user"))) {
                    $this->datatables->where("t.role_id", $this->input->post('role_id'));
                } else if ($role_id_name == "trimax") {
                    $this->datatables->where_in("t.inserted_by", $employees_in);
                } else if ($role_id_name == "sys_user") {
                    eval(SYSTEM_USERS_ID_ARRAY);
                    $this->datatables->where_in("t.role_id", $system_users_id_array);
                }
            }

            if ($this->input->post('user_id')) {
                $this->datatables->where("t.inserted_by", $this->input->post('user_id'));
            }
        } else if (in_array(strtolower($this->session->userdata("role_name")), $fme_agent_role_name)) {
            $this->datatables->where("t.inserted_by", $this->session->userdata("user_id"));
        }

        $this->datatables->group_by('t.ticket_id');
        $this->datatables->add_column('t.ticket_id', '<a href="#" ref="$1" class="passen_details">Passenger Detail</a>', 'ticket_id');

        $data = $this->datatables->generate('json');

        log_data("report_trash/" . date("M") . "/" . date("d-m-Y") . "_log.log", $this->db->last_query(), 'bos_commission_offer');
        // show($this->db->last_query(),1);
        echo $data;
    }

    public function commission_offer_export_excel() {
        $input = $this->input->get();
        $ticket_result = $this->report_model->bos_commission_offer($input);
        $fields = array("Sr No.", "Rokad Ref No", "PNR No", "ETS Ref No", "User Type", "User Name", "Email Id","User/Agent Id" ,"Mobile No","State","City", "Operator Name", "Provider Name",
            "From Stop", "To Stop", "Issued date", "DOJ", "No of passenger", "Basic Fare", "Service Tax", "Total Fare Without Discount", "UPSRTC Charges (3% or Rs.50)",
            "Discount On", "Discount Type", "Discount Percentage", "Discount Price", "Total Fare Paid", "Refund Amount", "Cancel Charge", "PG Track Id","Payment By",
            "Transaction Status", "Ticket RefNo", "Commision Percentage", "Commission Value");
        $stockArr = array();
        $sr_no = 1;

        if (!empty($ticket_result) && count($ticket_result) > 0 && is_array($ticket_result)){

            foreach ($ticket_result as $key => $value) {
                // $act_rev = $value['Reveue'] - $value['tot_can'];
                $data[] = array('Sr no' => $sr_no,
                    'Rokad Ref No' => $value['Boss Reference No'],
                    'PNR No' => $value['PNR NO'],
                    'ETS Ref No' => $value['api_ticket_no'],
                    'User Type' => $value['user type'],
                    'User Name' => $value['first_name'],
                    'Email Id' => $value['email'],
                    'User/Agent Id' => $value['agent_id'],
                    'Mobile No' => $value['mobile_no'],
                    'State'     => $value['stState'],
                    'City'      => $value['stCity'],
                    'Operator Name' => $value['Operator Name'],
                    'Provider Name' => $value['Povider Name'],
                    'From Stop' => $value['From Stop'],
                    'To Stop' => $value['To Stop'],
                    'Issued date' => $value['issued date'],
                    'DOJ' => $value['doj'],
                    'No of passenger' => $value['No of passenger'],
                    'Basic Fare' => $value['Basic Fare'],
                    'Service Tax' => $value['Service Tax'],
                    'Total Fare Without Discount' => $value['Total Fare Without Discount'],
                    'UPSRTC Charges (3% or Rs.50)' => $value['UPSRTC Additional Charges (3% or Rs.50)'],
                    'Discount On' => $value['Discount On'],
                    'Discount Type' => $value['Discount Type'],
                    'Discount Percentage' => $value['Discount Percentage'],
                    'Discount Price' => $value['Discount Price'],
                    'Total Fare Paid' => $value['Total Fare Paid'],
                    'Refund Amount' => $value['Refund Amount'],
                    'Cancel Charge' => $value['Cancel Charge'],
                    'PG Track Id' => $value['PG Track Id'],
                    'Payment By' => $value['payment_by'],
                    'Transaction Status' => $value['Transaction Status'],
                    'Ticket RefNo' => $value['ticket_ref_no'],
                    'Commision Percentage' => $value['commission_percentage'],
                    'Commission Value' => $value['commission_value']
                );
                $sr_no++;
            }
            $this->excel->data_2_excel('bos_commission_offer_report' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
        } else {
            $data = array();
            $this->excel->data_2_excel('bos_commission_offer_report' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
        }
    }

    function incomplete_transactions() {
        $role_id_name = "";
        $role_id_name = $this->input->post('role_id');
        $employees_in = [];

        $this->datatables->select('1,
                        t.ticket_id As "ticket_id",
                        t.`booked_by` AS "user_type",
                        td.psgr_name as first_name,
                        t.user_email_id as email,
                        t.mobile_no,
                        t.`op_name` AS "operator_name",
                        ap.name as "provider_name",
                        t.from_stop_name as "from_stop",                      
                        t.till_stop_name as "to_stop",
                        DATE_FORMAT(t.inserted_date,"%d-%m-%Y %h:%i:%s") as "issued_date",
                        DATE_FORMAT(t.dept_time,"%d-%m-%Y %h:%i:%s") as doj,
                        t.num_passgr as "no_of_passenger",
                        t.`tot_fare_amt_with_tax` AS "ticket_fare",
                        t.`transaction_status` AS "transaction_Status"'
        );
        $this->datatables->from('tickets t');
        $this->datatables->join('ticket_details td', 'td.ticket_id = t.ticket_id', 'inner');
        $this->datatables->join('api_provider ap', 't.provider_id = ap.id', 'left');
        if ($this->input->post('operator_id') && $this->input->post('operator_id') != 'Etravelsmart') {
            $this->datatables->where("op_id", $this->input->post('operator_id'));
        }
        if ($this->input->post('operator_id') && $this->input->post('operator_id') == 'Etravelsmart') {
            $this->datatables->where("provider_id", ETRAVELSMART_PROVIDER_ID);
        }

        if ($this->input->post('from_date') && $this->input->post('till_date')) {
            $this->datatables->where("issue_time between '" . date('Y-m-d 00:00:01', strtotime($this->input->post('from_date'))) . "' and '" . date('Y-m-d 23:59:59', strtotime($this->input->post('till_date'))) . "'");
        }
        if ($this->input->post('tran_type') && $this->input->post('tran_type') != 'select_status') {
            $status = $this->input->post('tran_type');
            if ($status == 'failed') {
                $status = array('psuccess', 'pfailed', 'failed');
            }
            $this->datatables->where_in('t.transaction_status', $status);
        } else {
            $this->datatables->where('t.transaction_status != "success" and t.transaction_status != "cancel"');
        }
        if ($this->input->post('pnr_no')) {
            $this->datatables->where('t.pnr_no', $this->input->post('pnr_no'));
        }
       
        
        $this->datatables->group_by('t.ticket_id');
        $data = $this->datatables->generate('json');
        log_data("report_trash/" . date("M") . "/" . date("d-m-Y") . "_log.log", $this->db->last_query(), 'incomplete_transactions_report');
        echo $data;
    }

    function incomplete_transactions_report() {
        $array_input = array();
        $data['tran_type_data'] = array(
            "incomplete" => "Incomplete",
            "pfailed" => "Payment Initiate",
            "aborted" => "Aborted",
            "failure" => "Payment Failed",
            "failed" => "Failed"
        );
        /*$data['op_id'] = $this->op_master_model->column('op_id, op_name')->where(array('is_status' => '1', 'is_deleted' => '0'))->as_array()->find_all();*/

        load_back_view(BOS_INCOMPLETE_TRANSACTION_REPORT, $data);
    }

    public function incomplete_transactions_export_excel() {
        $input = $this->input->get();

        $ticket_result = $this->report_model->incomplete_trasactions_data($input);
        $fields = array("Sr No.", "Ticket Id", "User Type", "User Name", "Email Id", "Mobile No", "Operator Name", "Provider Name",
            "From Stop", "To Stop", "Issued date", "DOJ", "No of passenger", "Ticket Fare", "Transaction Status");
        $stockArr = array();
        $sr_no = 1;
        // if (count($ticket_result) > 0) {

        foreach ($ticket_result as $key => $value) {
            // $act_rev = $value['Reveue'] - $value['tot_can'];
            $data[] = array('Sr no' => $sr_no,
                'Ticket Id' => $value['ticket_id'],
                'User Type' => $value['user_type'],
                'User Name' => $value['first_name'],
                'Email Id' => $value['email'],
                'Mobile No' => $value['mobile_no'],
                'Operator Name' => $value['operator_name'],
                'Provider Name' => $value['provider_name'],
                'From Stop' => $value['from_stop'],
                'To Stop' => $value['to_stop'],
                'Issued date' => $value['issued_date'],
                'DOJ' => $value['doj'],
                'No of passenger' => $value['no_of_passenger'],
                'Ticket Fare' => $value['ticket_fare'],
                'Transaction Status' => $value['transaction_Status']
            );
            $sr_no++;
        }
        $this->excel->data_2_excel('incomplete_transactions_report' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
        die;
        //}
    }

    public function user_type_view() {
        $input = $this->input->post();
        $res = $this->report_model->user_type_data($input);
        data2json($res);
    }

    public function user_type_view_support() {
        $input = $this->input->post();
        $res = $this->report_model->user_type_data_support($input);
        data2json($res);
    }

    public function users_view() {
        $input = $this->input->post();
        $res = $this->report_model->users_data($input);
        data2json($res);
    }

    function support_summary_report_view() {
        $user_type = '';
        $user_type = $this->input->post('user_type');
        $employees_in = [];
        $data['tran_type_data'] = array(
            "all" => "All",
            "success" => "Success",
            "incomplete" => "Incomplete",
            "pfailed" => "Payment Initiate",
            "aborted" => "Aborted",
            "failure" => "Payment Failed",
            "failed" => "Failed",
            "cancel" => "Cancel"
        );
        $data['state_name'] = $this->state_master_model->get_all_state();
        $input = $this->input->post();
        $summary = $this->report_model->summary_bos_tkt_detail_support($input);

        if ($summary) {
            $data["summary"] = $summary[0];
        }
        $data['input'] = $input;

        load_back_view(SUPPORT_SUMMARY_REPORT_VIEW, $data);
    }

    function bos_tkt_details_count_view_summary() {

        $user_type = '';
        $user_type = $this->input->post('user_type');
        $employees_in = [];

        $data["summary"] = ["total_ticket_booked" => 0,
            "total_passangers" => 0,
            "total_fare_without_discount" => 0,
            "discount_value" => 0,
            "total_commission" => 0,
            "total_cancel_charge" => 0,
            "total_refund_amount" => 0
        ];

        $data ['tran_type_data'] = array(
            "all" => "All",
            "success" => "Success",
            "incomplete" => "Incomplete",
            "pfailed" => "Payment Initiate",
            "aborted" => "Aborted",
            "failure" => "Payment Failed",
            "failed" => "Failed",
            "cancel" => "Cancel"
        );

        $input = $this->input->post();
        $summary = $this->report_model->tkt_details_view_summary($input);
        $data["ucount"] = $this->report_model->get_registerd_users($input);
        if ($summary) {
            $data["summary"] = $summary[0];
        }

        $data['input'] = $input;
        load_back_view(BOS_TICKET_DETAILS_COUNT_VIEW_SUMMARY, $data);
    }

    function list_of_agents_view() {
        $data = [];
        $data['states'] = $this->state_master_model->column('intStateId, stState')->as_array()->find_all();
        $data['cities'] = $this->city_master_model->column('intCityId, stCity')->as_array()->find_all();
        load_back_view(AGENT_REPORT_VIEW, $data);
    }

    function list_of_agents() {
        $role_id_name = "";
        $role_id_name = $this->input->post('role_id');

        $input = $this->input->post();

        $ids = array('16', '17', '18', '27');

        $this->datatables->select('1,DATE_FORMAT(created_date,"%d-%m-%Y %h:%i:%s") as "created_date",concat(first_name," ",last_name),u.id,rm.referrer_name,rm.referrer_code,sm.stState,cm.stCity,mobile_no,verify_status,email');
        $this->datatables->from('users u');
        $this->datatables->join('state_master sm', 'u.state=sm.intStateId', 'left');
        $this->datatables->join('city_master cm', 'cm.intCityId=u.city', 'left');
        $this->datatables->join('referrer_master rm', 'rm.referrer_code = u.referrer_code', 'left');
        $this->datatables->where_in('u.role_id ', $ids);

        if (isset($input['from_date']) && $input['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(u.created_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['to_date']) && $input['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(u.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }

        $this->datatables->where("u.salt !='' and u.password != ''");

        if (isset($input['user_type']) && $input['user_type'] != "") {
            $this->datatables->where("u.role_id", $input['user_type']);
        }

        if (isset($input['user_id']) && $input['user_id'] != '') {
            $this->datatables->where("u.id", $input['user_id']);
        }
        if (isset($input['select_state']) && $input['select_state'] != '') {
            $this->datatables->where("u.state", $input['select_state']);
        }
        if (isset($input['select_city']) && $input['select_city'] != '') {
            $this->datatables->where("u.city", $input['select_city']);
        }

        $data1 = $this->datatables->generate('json');
        echo $data1;
    }

    function agent_type_view() {
        $input = $this->input->post();
        $res = $this->report_model->agent_type_data($input);
        data2json($res);
    }

    public function agent_registration_report_excel() {
        $input = $this->input->get();

        $agent_result = $this->report_model->agent_registeraion_detail($input);
        $fields = array("Sr No.", "Registration Date", "Agent Name", "Referrer Name", "Referrer Code", "State", "City", "Mobile No", "Verify Status", "Email");
        $stockArr = array();
        $sr_no = 1;

        foreach ($agent_result as $key => $value) {

            $data[] = array('Sr No.' => $sr_no,
                'Registration Date' => $value['Reg_Date'],
                'Agent Name' => $value['AgentName'],
                'Agent Code' => $value['AgentId'],
                'Referrer Name' => $value['ReferrerName'],
                'Referrer Code' => $value['ReferrerCode'],
                'State' => $value['State'],
                'City' => $value['City'],
                'Mobile No' => $value['MobileNo'],
                'Verify Status' => $value['VerifyStatus'],
                'Email' => $value['Email']
            );
            $sr_no++;
        }
        $this->excel->data_2_excel('agent_registration_detail_excel' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
    }

    function ticket_transaction_view() {
        $data = [];
        $ticket_status = [];
        $input = $this->input->post();
        $default_status_array = ['incomplete' => 0, 'temp_booked' => 0, 'success' => 0, 'failed' => 0, 'cancel' => 0, 'pfailed' => 0, 'psuccess' => 0, 'aborted' => 0, 'failure' => 0];

        $tickets = $this->report_model->tickets_status_analytics($input);

        foreach ($tickets as $key => $value) {
            $ticket_status[$value["date"]] = $default_status_array;
        }
        foreach ($tickets as $key => $value) {
            $ticket_status[$value["date"]][$value["transaction_status"]] = $value["total"];
        }
        $data["transaction_report"] = $ticket_status;
        $data["input"] = $input;
        load_back_view(TICKET_TRANSACTION_REPORT_VIEW, $data);
    }

    function stop_wise_ticket_count_report($id = '') {
        $this->load->library('Pagination');
        $data = array();
        $per_page = 50;
        $uri = $id;
        $page = (isset($uri) && $uri > 0 ) ? $uri : 0;
        $index = $per_page * ($page - 1);
        if ($index <= 0) {
            $index = 0;
        }
        $input = $this->input->post();
        if (empty($this->session->userdata('trans_type'))) {
            $this->session->set_userdata('trans_type', 'success');
        }
        if (empty($this->session->userdata('select_user_type'))) {
            $this->session->set_userdata('select_user_type', 'ors_agent');
        }
        if (!empty($input['trans_type'])) {
            $this->session->set_userdata('trans_type', $input['trans_type']);
        }
        if (!empty($input['from_date'])) {
            $this->session->set_userdata('from_date', trim($input['from_date']));
        } else if (empty($id)) {
            $this->session->unset_userdata('from_date');
        }
        if (!empty($input['till_date'])) {
            $this->session->set_userdata('till_date', trim($input['till_date']));
        } else if (empty($id)) {
            $this->session->unset_userdata('till_date');
        }

        if (!empty($input['select_user_type'])) {
            $this->session->set_userdata('select_user_type', trim($input['select_user_type']));
        }

        $array = array('transaction_status' => $this->session->userdata('trans_type'));
        if (!empty($this->session->userdata('from_date'))) {
            $like = "tickets.issue_time between '" . date('Y-m-d 00:00:01', strtotime($this->session->userdata('from_date'))) . "' and '" . date('Y-m-d 23:59:59', strtotime($this->session->userdata('till_date'))) . "'";
        }


        if (!empty($this->session->userdata('select_user_type'))) {
            $array['booked_by'] = $this->session->userdata('select_user_type');
        }

        $page = ($page > 0 ? $page : 1);
        $offset = $per_page * ($page - 1);
        //echo  $this->session->userdata('status'); exit;
        $data['stop_wise_ticket_counts'] = $this->tickets_model->column("from_stop_name, till_stop_name, case booked_by when 'users' then 'Registered User' when 'ors_agent' then 'Agent' else 'Guest User' end as user, count(ticket_id)as count, sum(num_passgr) as nopassenger")->where($array)->where($like)->as_array()->group_by('from_stop_name, till_stop_name, booked_by')->limit($per_page, $offset)->find_all();

        $config["base_url"] = base_url() . "report/report/stop_wise_ticket_count_report";
        $data['total_rows'] = $this->tickets_model->column("from_stop_name, till_stop_name, case booked_by when 'users' then 'Registered User' when 'ors_agent' then 'Agent' else 'Guest User' end as user, count(ticket_id)as count, sum(num_passgr) as nopassenger")->where($array)->where($like)->as_array()->group_by('from_stop_name, till_stop_name, booked_by')->find_all();

        if (!empty($this->session->userdata('trans_type'))) {
            $data['trans_type'] = $this->session->userdata('trans_type');
        }

        if (!empty($this->session->userdata('from_date'))) {
            $data['from_date'] = $this->session->userdata('from_date');
        }

        if (!empty($this->session->userdata('till_date'))) {
            $data['till_date'] = $this->session->userdata('till_date');
        }
        if (!empty($this->session->userdata('select_user_type'))) {
            $data['select_user_type'] = $this->session->userdata('select_user_type');
        }
        if (empty($data['total_rows'])) {
            $data['total'] = 0;
        } else {
            $data['total'] = count($data['total_rows']);
        }
        $data['index'] = $index;

        $config["total_rows"] = count($data['total_rows']);
        $config['use_page_numbers'] = TRUE;
        $config["per_page"] = $per_page;
        $config['num_links'] = count($data['total_rows']);
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['first_link'] = '&lt;&lt;';
        $config['last_link'] = '&gt;&gt;';
        $this->pagination->initialize($config);
        load_back_view(STOPWISE_TICKET_COUNT_REPORT_VIEW, $data);
    }

    function referrer_wise_summary_report_view() {
        $data['states'] = $this->state_master_model->column('intStateId, stState')->as_array()->find_all();
        $data['cities'] = $this->city_master_model->column('intCityId, stCity')->as_array()->find_all();
        load_back_view(REFERRER_WISE_SUMMARY_REPORT_VIEW, $data);
    }

    function referrer_wise_summary_report() {
        eval(FME_AGENT_ROLE_ID);
        $input = $this->input->post();

        $this->datatables->select("1,rm.referrer_code,rm.referrer_name,rm.referrer_mobile,COUNT(ticket_id) AS 'No of Tickets',SUM(num_passgr) AS 'No Of Passenger',SUM(tot_fare_amt_with_tax) AS 'Revenue',SUM(discount_value) AS 'Discount'");
        $this->datatables->from('referrer_master AS rm');
        $this->datatables->join('users u', 'u.referrer_code=rm.referrer_code and u.status="Y"', 'inner join');
        $this->datatables->join('city_master cm', 'cm.intCityId=u.city', 'left');
        $this->datatables->join('state_master sm', 'sm.intStateId=u.state', 'left');

        $this->datatables->join('tickets t', ' t.inserted_by = u.id and t.transaction_status = "success"', 'inner join');
        $this->datatables->where_in("t.role_id", $fme_agent_role_id);
        $this->datatables->where("rm.is_status", 'Y');
        $this->datatables->where("rm.is_deleted", 'N');
        $this->datatables->group_by("rm.referrer_code");
        $this->db->order_by("COUNT(ticket_id)", "desc");
        if (isset($input['from_date']) && $input['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(t.inserted_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['till_date']) && $input['till_date'] != '') {
            $this->datatables->where("DATE_FORMAT(t.inserted_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['till_date'])));
        }

        if (isset($input['select_city']) && $input['select_city'] != '') {
            $this->datatables->where("cm.intCityId", $input['select_city']);
        }
        if (isset($input['select_state']) && $input['select_state'] != '') {
            $this->datatables->where("sm.intStateId", $input['select_state']);
        }

        $data1 = $this->datatables->generate('json');

        echo $data1;
//        $this->datatables->order_by("COUNT(ticket_id)", "desc");
    }
    function reconcillation_report_view() {
        load_back_view(RECONSILLATION_REPORT_VIEW);
    }

    function reconsilation_list() {
        $this->datatables->select('1,
                        t.ticket_id,
                        t.`transaction_status` AS "Transaction Status",
                        t.`boss_ref_no` AS "Boss Reference No",
                        t.pnr_no As "PNR NO",
                        t.ticket_ref_no,
                        ap.name as "Povider Name", 
                        t.`op_name` AS "Operator Name",
                        DATE_FORMAT(t.inserted_date,"%d-%m-%Y %h:%i:%s") as "issued date",
                        DATE_FORMAT(t.dept_time,"%d-%m-%Y %h:%i:%s") as doj,
                        t.num_passgr as "No of passenger",
                        IFNULL(t.`tot_fare_amt`,0) AS "Basic Fare",
                        sum(td.service_tax) AS "Service Tax",
                        IFNULL(t.`total_fare_without_discount`,0) AS "Total Fare Without Discount",   
                        IF(t.`booked_from` = "upsrtc", t.`fare_convey_charge`, "0") AS "UPSRTC Additional Charges (3% or Rs.50)",
                        IFNULL(t.`discount_on`,tdd.`discount_on`) AS "Discount On",
                        IFNULL(t.`discount_type`,tdd.`discount_type`) AS  "Discount Type",
                        IFNULL(t.`discount_per`,tdd.`discount_per`) AS "Discount Percentage",
                        IFNULL(t.`discount_value`,tdd.`discount_flat_value`) AS "Discount Price",
                        IFNULL(at.`agent_commission_percentage`,"-") AS "Agent Commission Percentage",
                        at.`agent_commission_amount` AS "agent_commission_amount_with_tds",
                        at.`tds_value_on_commission_amount` AS "agent_commission_tds",
                        at.`agent_commission_rs` AS "Agent Commission Amount",
                        t.`tot_fare_amt_with_tax` AS "Total Fare Paid",
                        IFNULL(ctd.`actual_refund_paid`,0) AS "Refund Amount",
                        IF(ctd.is_refund = "1",(ctd.`cancel_charge`),0)  AS "Cancel Charge",
                        t.`pg_tracking_id` AS "PG Track Id",
                        IFNULL(t.`tot_fare_amt_with_tax`,0) as "Amount",
                        pgt.`order_fee_perc_value` as "Fee",
                        pgt.`order_tax` as "Tax on Fees",
                        IFNULL(t.`merchant_amount`,0) as "Payable Amount",
                        "-" as "Auth Code",
                        IFNULL(t.`payment_mode`,0) as "td_type",
                        IFNULL(wt.`id`,"-") AS  "Wallet_id",
                        t.`api_ticket_no` as "ets_no",
                        tad.`actual_amount` as "et_amt",
                        tad.`total_discount_amount` as "ets_dscnt",
                        if(t.booked_from = "etravelsmart", ifnull(tad.`service_tax_amount`,t.`service_tax` + t.`op_service_charge`),"") as "ets_ser_tax",
                        tad.`tds_charges` as "ets_tds_charge",
                        tad.`paid_amount` as "ets_paid_amt"'
        );

        $this->datatables->from('tickets t');
        $this->datatables->join('ticket_details td', 'td.ticket_id = t.ticket_id', 'inner');
        $this->datatables->join('api_provider ap', 't.provider_id = ap.id', 'left');
        $this->datatables->join('agent_tickets at', 'at.ticket_id = t.ticket_id', 'left');
        $this->datatables->join('wallet_trans wt', 'wt.ticket_id = t.ticket_id', 'left');
        $this->datatables->join('cancel_ticket_data ctd', 'ctd.ticket_id = t.ticket_id', 'left');
        $this->datatables->join('tickets_additional_data tad', 'tad.ticket_id = t.ticket_id', 'left');
        $this->datatables->join('payment_gateway_transaction pgt', 'pgt.pg_tracking_id = t.pg_tracking_id', 'left');
        $this->datatables->join('ticket_discount_description tdd', 'tdd.ticket_id = t.ticket_id', 'left');
        $this->datatables->where('t.pnr_no != ""');
        $this->datatables->where_in('t.transaction_status', array('success', 'cancel'));

        if ($this->input->post('provider_id')) {
            $this->datatables->where("t.booked_from", $this->input->post('provider_id'));
        }
        if ($this->input->post('from_date')) {
            $this->datatables->where("DATE_FORMAT(t.inserted_date, '%Y-%m-%d')>=", date('Y-m-d', strtotime($this->input->post('from_date'))));
        }
        if ($this->input->post('to_date')) {
            $this->datatables->where("DATE_FORMAT(t.inserted_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($this->input->post('to_date'))));
        }
        $this->datatables->group_by('t.ticket_id');
        $data = $this->datatables->generate('json');

        echo $data;
    }

    function reconcillation_report_excel() {
        $input = $this->input->get();

        $excel_result = $this->report_model->reconsillation_detail_excel($input);
        $fields = array("Sr No.", "ticket_id", "Transaction Status", "Boss Reference No", "PNR NO", "ticket_ref_no", "Povider Name", "Operator Name", "issued date", "Verify Status", "doj", "No of passenger", "Basic Fare", "Service Tax", "Total Fare Without Discount", "UPSRTC Additional Charges (3% or Rs.50)", "Discount On", "Discount Type", "Discount Percentage", "Discount Price", "Agent Commission Percentage", "Agent Commission Amount", "agent_commission_tds", "agent_commission_amount_with_tds", "Total Fare Paid", "Refund Amount", "Cancel Charge", "PG Track Id", "Amount", "TDR%", "TDR Amount", "Payable Amount", "Auth Code", "td_type", "Wallet_id", "Ets_no", "Et_amt", "Ets_dscnt", "Ets_ser_tax", "Ets_tds_charge", "Ets_paid_amt");
        $stockArr = array();
        $sr_no = 1;

        foreach ($excel_result as $key => $value) {

            $data[] = array('Sr no' => $sr_no,
                'ticket_id' => $value['ticket_id'],
                'Transaction Status' => $value['Transaction Status'],
                'Boss Reference No' => $value['Boss Reference No'],
                'PNR NO' => $value['PNR NO'],
                'ticket_ref_no' => $value['ticket_ref_no'],
                'Povider Name' => $value['Povider Name'],
                'Operator Name' => $value['Operator Name'],
                'issued date' => $value['issued date'],
                'doj' => $value['doj'],
                'No of passenger' => $value['No of passenger'],
                'Basic Fare' => $value['Basic Fare'],
                'Service Tax' => $value['Service Tax'],
                'Total Fare Without Discount' => $value['Total Fare Without Discount'],
                'UPSRTC Additional Charges (3% or Rs.50)' => $value['UPSRTC Additional Charges (3% or Rs.50)'],
                'Discount On' => $value['Discount On'],
                'Discount Type' => $value['Discount Type'],
                'Discount Percentage' => $value['Discount Percentage'],
                'Discount Price' => $value['Discount Price'],
                'Agent Commission Percentage' => $value['agent_commission_percenatge'],
                'agent_commission_amount_with_tds' => $value['agent_commission_amount_with_tds'],
                'agent_commission_tds' => $value['agent_commission_tds'],
                'Agent Commission Amount' => $value['Agent Commission Amount'],
                'Total Fare Paid' => $value['Total Fare Paid'],
                'Refund Amount' => $value['Refund Amount'],
                'Cancel Charge' => $value['Cancel Charge'],
                'PG Track Id' => $value['PG Track Id'],
                'Amount' => $value['Amount'],
                'Fee' => $value['Fee'],
                'Tax on Fees ' => $value['Tax_on_Fees'],
                'Payable Amount' => $value['Payable Amount'],
                'Auth Code' => $value['Auth Code'],
                'Td type' => $value['td_type'],
                'Wallet_id' => $value['Wallet_id'],
                'Ets_no' => $value['Ets_no'],
                'Et_amt' => $value['Et_amt'],
                'Ets_dscnt' => $value['Ets_dscnt'],
                'Ets_ser_tax' => $value['Ets_ser_tax'],
                'Ets_tds_charge' => $value['Ets_tds_charge'],
                'Ets_paid_amt' => $value['Ets_paid_amt'],
            );
            $sr_no++;
        }
        $this->excel->data_2_excel('reconsillation_detail_excel' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
    }

    public function rsrtc_reconciliation_report() {
        load_back_view(RSRTC_RECONSILIATION_REPORT);
    }

    function reconcillation_cancel_report() {
        load_back_view(RECONSILLATION_CANCEL_REPORT_VIEW);
    }

    function cancel_reconsilation_list() {
        $this->datatables->select('1,
                        t.ticket_id,
                        t.`transaction_status` AS "Transaction Status",
                        t.`boss_ref_no` AS "Boss Reference No",
                        t.pnr_no As "PNR NO",
                        t.ticket_ref_no,
                        ap.name as "Povider Name", 
                        t.`op_name` AS "Operator Name",
                        t.`inserted_date` as "issued date",
                        t.dept_time as doj,
                        t.num_passgr as "No of passenger",
                        IFNULL(t.`tot_fare_amt`,0) AS "Basic Fare",
                        sum(td.service_tax) AS "Service Tax",
                        IFNULL(t.`total_fare_without_discount`,0) AS "Total Fare Without Discount",   
                        IF(t.`booked_from` = "upsrtc", t.`fare_convey_charge`, "0") AS "UPSRTC Additional Charges (3% or Rs.50)",
                        IFNULL(t.`discount_on`,tdd.`discount_on`) AS "Discount On",
                        IFNULL(t.`discount_type`,tdd.`discount_type`) AS  "Discount Type",
                        IFNULL(t.`discount_per`,tdd.`discount_per`) AS "Discount Percentage",
                        IFNULL(t.`discount_value`,tdd.`discount_flat_value`) AS  "Discount Price",
                        t.`tot_fare_amt_with_tax` AS "Total Fare Paid",
                        IFNULL(ctd.`pg_refund_amount`,0) AS "Refund Amount",
                        IF(ctd.is_refund = "1",(ctd.`cancel_charge`),0)  AS "Cancel Charge",
                        t.`pg_tracking_id` AS "PG Track Id",
                        IFNULL(t.`tot_fare_amt_with_tax`,0) as "Amount",
                        pgt.`order_fee_perc_value` as "Fee",
                        pgt.`order_tax` as "Tax on Fees",
                        IFNULL(t.`merchant_amount`,0) as "Payable Amount",
                        "-" as "Auth Code",
                        IFNULL(t.`payment_mode`,0) as "td_type",
                        t.`api_ticket_no` as "ets_no",
                        tad.`actual_amount` as "et_amt",
                        tad.`total_discount_amount` as "ets_dscnt",
                        tad.`service_tax_amount` as "ets_ser_tax",
                        tad.`tds_charges` as "ets_tds_charge",
                        tad.`paid_amount` as "ets_paid_amt"'
        );

        $this->datatables->from('tickets t');
        $this->datatables->join('ticket_details td', 'td.ticket_id = t.ticket_id', 'inner');
        $this->datatables->join('api_provider ap', 't.provider_id = ap.id', 'left');
        $this->datatables->join('cancel_ticket_data ctd', 'ctd.ticket_id = t.ticket_id', 'left');
        $this->datatables->join('tickets_additional_data tad', 'tad.ticket_id = t.ticket_id', 'left');
        $this->datatables->join('ticket_discount_description tdd', 'tdd.ticket_id = t.ticket_id', 'left');
        $this->datatables->join('payment_gateway_transaction pgt', 'pgt.pg_tracking_id = t.pg_tracking_id', 'left');

        $this->datatables->where('t.pnr_no != ""');
        $this->datatables->where_in('t.transaction_status', array('cancel'));

        if ($this->input->post('provider_id')) {
            $this->datatables->where("t.booked_from", $this->input->post('provider_id'));
        }
        if ($this->input->post('from_date')) {
            $this->datatables->where("DATE_FORMAT(t.inserted_date, '%Y-%m-%d')>=", date('Y-m-d', strtotime($this->input->post('from_date'))));
        }
        if ($this->input->post('to_date')) {
            $this->datatables->where("DATE_FORMAT(t.inserted_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($this->input->post('to_date'))));
        }
        $this->datatables->group_by('t.ticket_id');
        $data = $this->datatables->generate('json');

        echo $data;
    }

    function cancel_reconcillation_excel() {
        $input = $this->input->get();

        $excel_result = $this->report_model->cancel_reconcillation_excel($input);

        $fields = array("Sr No.", "ticket_id", "Transaction Status", "Boss Reference No", "PNR NO", "ticket_ref_no", "Povider Name", "Operator Name", "issued date", "Verify Status", "doj", "No of passenger", "Basic Fare", "Service Tax", "Total Fare Without Discount", "UPSRTC Additional Charges (3% or Rs.50)", "Discount On", "Discount Type", "Discount Percentage", "Discount Price", "Total Fare Paid", "Refund Amount", "Cancel Charge", "PG Track Id", "Amount", "TDR%", "TDR Amount", "Payable Amount", "Auth Code", "td_type", "Ets_no", "Et_amt", "Ets_dscnt", "Ets_ser_tax", "Ets_tds_charge", "Ets_paid_amt");
        $stockArr = array();
        $sr_no = 1;

        foreach ($excel_result as $key => $value) {

            $data[] = array('Sr no' => $sr_no,
                'ticket_id' => $value['ticket_id'],
                'Transaction Status' => $value['Transaction Status'],
                'Boss Reference No' => $value['Boss Reference No'],
                'PNR NO' => $value['PNR NO'],
                'ticket_ref_no' => $value['ticket_ref_no'],
                'Povider Name' => $value['Povider Name'],
                'Operator Name' => $value['Operator Name'],
                'issued date' => $value['issued date'],
                'doj' => $value['doj'],
                'No of passenger' => $value['No of passenger'],
                'Basic Fare' => $value['Basic Fare'],
                'Service Tax' => $value['Service Tax'],
                'Total Fare Without Discount' => $value['Total Fare Without Discount'],
                'UPSRTC Additional Charges (3% or Rs.50)' => $value['UPSRTC Additional Charges (3% or Rs.50)'],
                'Discount On' => $value['Discount On'],
                'Discount Type' => $value['Discount Type'],
                'Discount Percentage' => $value['Discount Percentage'],
                'Discount Price' => $value['Discount Price'],
                'Total Fare Paid' => $value['Total Fare Paid'],
                'Refund Amount' => $value['Refund Amount'],
                'Cancel Charge' => $value['Cancel Charge'],
                'PG Track Id' => $value['PG Track Id'],
                'Amount' => $value['Amount'],
                'Fee' => $value['Fee'],
                'Tax on Fees ' => $value['Tax_on_Fees'],
                'Payable Amount' => $value['Payable Amount'],
                'Auth Code' => $value['Auth Code'],
                'Ets_no' => $value['Ets_no'],
                'Et_amt' => $value['Et_amt'],
                'Ets_dscnt' => $value['Ets_dscnt'],
                'Ets_ser_tax' => $value['Ets_ser_tax'],
                'Ets_tds_charge' => $value['Ets_tds_charge'],
                'Ets_paid_amt' => $value['Ets_paid_amt'],
            );
            $sr_no++;
        }
        $this->excel->data_2_excel('reconsillation_detail_excel' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
    }

    function ticket_Details_report() {
		
        $data = array();		
        $array_input = array();
        $data['tran_type_data'] = array(
            "success" => "Success",
            "incomplete" => "Incomplete",
            "pfailed" => "Payment Initiate",
            "aborted" => "Aborted",
            "failure" => "Payment Failed",
            "failed" => "Failed",
            "cancel" => "Cancel"
        );        
		
		$user_id = strtolower($this->session->userdata('user_id'));
		$role_name = strtolower($this->session->userdata('role_name'));
		$data["offices"] = array();
		$data["multi_office_agent"] = "";
		// For Ors Agent
		if ($role_name == ORS_AGENT_ROLE_NAME) {			
			$data["offices"] = $this->office_master_model->get_office_agentwise($user_id);
			$data["multi_office_agent"] =  $this->users_model->where(array('id' => $user_id,'multioffice_flag' =>'Y'))->find_all();
			load_back_view(BOS_TICKET_DATA_VIEW, $data);
		}
        // For Ors Agent User
		else if ($role_name == ORS_AGENT_USERS_ROLE_NAME){
			$users = get_users_for_same_office();
			$data['office_users'] = $this->users_model->column('id,concat(first_name," ",last_name) as name')->where_in('id',$users)->find_all();
			load_back_view(BOS_AGENT_USERS_TICKET_DATA_VIEW, $data);
		}
        // For Admin 
		else{
			$data['agent_name'] = $this->users_model->ors_agent_details();	
			load_back_view(BOS_ADMIN_TICKET_DATA_VIEW, $data);
		}
	}

    function bos_ticket_details() {		
		$input=$this->input->post();
        $summary = $this->report_model->bos_ticket_details_report($input,'json');
	    echo $summary;
    }
	
	function bos_agent_users_ticket_details() {
		$input=$this->input->post();
		$summary = $this->report_model->bos_agent_users_ticket_details_report($input,'json');
		echo $summary;
	}
	
	function bos_admin_ticket_details() {
		$input=$this->input->post();
        $summary = $this->report_model->bos_admin_ticket_details_report($input,'json');
	    echo $summary;
	}

    function bos_ticket_details_report_excel() {
		$input = $this->input->get();
		$ticket_result = $this->report_model->bos_ticket_details_report($input,'array');
		$sr_no = 1;
        $user_id = strtolower($this->session->userdata('user_id'));
        $multi_office_agent =  $this->users_model->where(array('id' => $user_id,'multioffice_flag' =>'Y'))->find_all();
		if(!empty($ticket_result))
		{
			foreach ($ticket_result['aaData'] as $key => $value) {
			   $data[] = array('Sr no' => $sr_no,
                    'Booking date' => $value['booking_date'],
					'Booked By' => $value['inserted_by'],
					'Office Name' => $value['office_name'],
                    'Journey date' => $value['journey_date'],
                    'Operator Name' => $value['operator_name'],
                    'From Stop' => $value['from_stop'],
                    'To Stop' => $value['to_stop'],
                    'Boss Reference No' => $value['boss_ref_no'],
                    'PNR NO' => $value['pnr_no'],
                    'Name' => $value['first_name'],
                    'Email Id' => $value['email'],
                    'Mobile' => $value['mobile_no'],
                    'Basic Fare' => $value['basic_fare'],
                    'Tax Amt' => $value['service_tax'],
                    'Operator service charge' => $value['op_service_charge'],
                    'Agent service charge' => $value['agent_service_tax'],
                    'Other Tax' => $value['other_taxes'],
                    'Total Ticket Fair' => $value['total_ticket_fare'],
                    'commission' => $value['commission'],
                    'TDS' => $value['tds'],
                    'Net amount paid' => $value['net_paid'],
                    'Payment Mode' => $value['payment_mode'],
                    'Txn ref no' => $value['txn_ref_no'],
                    'Cancellation Charge' => $value['cancel_charges'],
                    'Refund Amount' => $value['pg_refund_amount'],
                    'Profit earned' => $value['profit_earned'],
                    'Wallet opening balance' => $value['wallet_opening_bal'],
                    'wallet closing balance' => $value['wallet_closing_bal'],
                    'Credit opening balance' => $value['Credit_opening_balance'],
					'Credit closing balance' => $value['Credit_closing_balance'],
                    'Ticket id' => $value['ticket_id'],
                    'Transaction Status' => $value['transaction_status']
                        );
				$sr_no++;
			}
		}
		else
		{
				$data = array(array());
		}
		$this->excel->data_2_excel('Agent_ticket_details_report_excel' . date('Y-m-d') . '_' . time() . '.xls', $data);
		die;	
    }
	
	function bos_agent_users_ticket_details_report_excel() {
		$input = $this->input->get();
		$ticket_result = $this->report_model->bos_agent_users_ticket_details_report($input,'array');
		$sr_no = 1;

		if(!empty($ticket_result))
		{
			foreach ($ticket_result['aaData'] as $key => $value) {
			   // $act_rev = $value['Reveue'] - $value['tot_can'];
				$data[] = array('Sr no' => $sr_no,
                    'Booking date' => $value['booking_date'],
					'Booked By' => $value['inserted_by'],
					'Office Name' => $value['office_name'],
                    'Journey date' => $value['journey_date'],
                    'Operator Name' => $value['operator_name'],
                    'From Stop' => $value['from_stop'],
                    'To Stop' => $value['to_stop'],
                    'Boss Reference No' => $value['boss_ref_no'],
                    'PNR NO' => $value['pnr_no'],
                    'Name' => $value['first_name'],
                    'Email Id' => $value['email'],
                    'Mobile' => $value['mobile_no'],
                    'Basic Fare' => $value['basic_fare'],
                    'Tax Amt' => $value['service_tax'],
                    'Operator service charge' => $value['op_service_charge'],
                    'Agent service charge' => $value['agent_service_tax'],
                    'Other Tax' => $value['other_taxes'],
                    'Total Ticket Fair' => $value['total_ticket_fare'],
                    'commission' => $value['commission'],
                    'TDS' => $value['tds'],
                    'Net amount paid' => $value['net_paid'],
                    'Payment Mode' => $value['payment_mode'],
                    'Txn ref no' => $value['txn_ref_no'],
                    'Cancellation Charge' => $value['cancel_charges'],
                    'Refund Amount' => $value['pg_refund_amount'],
                    'Profit earned' => $value['profit_earned'],
                    'Wallet opening balance' => $value['wallet_opening_bal'],
                    'wallet closing balance' => $value['wallet_closing_bal'],
                    'Credit opening balance' => $value['Credit_opening_balance'],
					'Credit closing balance' => $value['Credit_closing_balance'],
                    'Ticket id' => $value['ticket_id'],
                    'Transaction Status' => $value['transaction_status']
                        );
				$sr_no++;
			}
		}
		else
		{
				$data = array(array());
		}
		$this->excel->data_2_excel('Agent_ticket_details_report_excel' . date('Y-m-d') . '_' . time() . '.xls', $data);
		die;
	}
	
	function bos_admin_ticket_details_report_excel() {
		$input = $this->input->get();
		$ticket_result = $this->report_model->bos_admin_ticket_details_report($input,'array');
		$sr_no = 1;

		if(!empty($ticket_result))
		{
			foreach ($ticket_result['aaData'] as $key => $value) {
			   // $act_rev = $value['Reveue'] - $value['tot_can'];
				$data[] = array('Sr no' => $sr_no,
                    'Booking date' => $value['Booking_date'],
					'Journey date' => $value['Journey_date'],
                    'Operator Name' => $value['Operator Name'],
                    'From Stop' => $value['From Stop'],
                    'To Stop' => $value['To Stop'],
                    'Boss Reference No' => $value['Boss Reference No'],
                    'PNR NO' => $value['PNR NO'],
                    'Name' => $value['first_name'],
                    'email' => $value['email'],
                    'Mobile' => $value['Mobile'],
                    'Basic Fare' => $value['Total_Fare'],
                    'Tax Amt' => $value['Service Tax'],
                    'Operator service charge' => $value['op_service_charge'],
                    'Agent service charge' => $value['agent_service_tax'],
                    'Other Tax' => $value['other_taxes'],
                    'Total Ticket Fair' => $value['Total_Ticket_Fare'],
                    'commission' => $value['commission'],
                    'TDS' => $value['tds'],
                    'Net amount paid' => $value['net_paid'],
                    'Payment Mode' => $value['payment_mode'],
                    'Txn ref no' => $value['Txn_ref_no'],
                    'Cancellation Charge' => $value['Cancel Charge'],
                    'Refund Amount' => $value['pg_refund_amount'],
                    'Profit earned' => $value['profit_earned'],
                    'Wallet opening balance' => $value['wallet_opening_bal'],
                    'wallet closing balance' => $value['wallet_closing_bal'],
                    'Credit opening balance' => $value['Credit_opening_balance'],
					'Credit closing balance' => $value['Credit_closing_balance'],
                    'Ticket id' => $value['ticket_id'],
                    'Transaction Status' => $value['Transaction Status']
                        );
				$sr_no++;
			}
		}
		else
		{
				$data = array(array());
		}
		$this->excel->data_2_excel('Agent_ticket_details_report_excel' . date('Y-m-d') . '_' . time() . '.xls', $data);
		die;
	}

    /* code added for exporting referrer wise summary report in excel format starts here */

    public function referrer_summary_report_excel() {
        $input = $this->input->get();
        $result = $this->report_model->referrer_wise_summary_report_excel($input);

        $sr_no = 1;
        foreach ($result as $key => $value) {
            // $act_rev = $value['Reveue'] - $value['tot_can'];
            $data[] = array('Sr no' => $sr_no,
                'Referrer Code' => $value['referrer_code'],
                'Referrer Name' => $value['referrer_name'],
                'Mobile No' => $value['referrer_mobile'],
                'Total ticket count' => $value['No of Tickets'],
                'Total passenger count' => $value['No Of Passenger'],
                'Revenue' => $value['Revenue'],
                'Commission' => $value['Discount']
            );
            $sr_no++;
        }
        $this->excel->data_2_excel('Referrence_wise_report_excel_' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
        die;
    }

    /* code added for exporting referrer wise summary report in excel ends here */

    /* code for exporting stop wise ticket in excel format starts here */

    public function stopwise_ticket_report_excel() {
        $input = $this->input->get();
        $result = $this->report_model->stop_wise_ticket_report_excel($input);

        $sr_no = 1;
        foreach ($result as $key => $value) {

            $data[] = array('Sr no' => $sr_no,
                'From Stop' => $value['from_stop_name'],
                'To Stop' => $value['till_stop_name'],
                'Booked By' => $value['user'],
                'No of Tickets' => $value['count'],
                'No of Passengers' => $value['nopassenger']
            );
            $sr_no++;
        }
        $this->excel->data_2_excel('Stopwise_ticket_report_excel_' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
        die;
    }

    /* code for exporting stop wise ticket in excel format starts here */

    public function tkt_details_count_summary_excel() {

        $input = $this->input->get();
        $result = $this->report_model->tkt_details_view_summary($input);
        $nUsercount = $this->report_model->get_registerd_users($input);

        foreach ($result as $key => $value) {
            $data[] = array('No of Registered User:' => $nUsercount,
                'Total No of Tickets Booked:' => ($value['total_ticket_booked'] != '' ? $value['total_ticket_booked'] : '0'),
                'Total No of Passenger:' => ($value['total_passangers'] != '' ? $value['total_passangers'] : 0),
                'Total Actual Fare Amount:' => ($value['total_fare_without_discount'] != '' ? $value['total_fare_without_discount'] : 0),
                'Total Discounted Amount:' => ($value['discount_value'] != '' ? $value['discount_value'] : 0),
                'Total Cancellation Charges:' => ($value['total_cancel_charge'] != '' ? $value['total_cancel_charge'] : 0),
                'Total Refund Amount:' => ($value['total_refund_amount'] != '' ? $value['total_refund_amount'] : 0),
                'Total Commission Amount:' => ($value['total_commission'] != '' ? $value['total_commission'] : 0),
                'Total Revenue:' => number_format(round($value['total_fare_without_discount'] - $value['discount_value'], 2), 2),
                'Total Actual Revenue:' => number_format(round(($value['total_fare_without_discount'] - $value['discount_value']) - $value['total_refund_amount'], 2), 2)
            );
        }

        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Rokad ticket count summary report');
        $this->excel->getActiveSheet()->getStyle('A2:A12')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->mergeCells('A1:B1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Summary Under Report');
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $rownumber = 2;

        foreach ($data[0] as $key => $val) {
            $this->excel->getActiveSheet()->getCell('A' . $rownumber)->setValue($key);
            $rownumber++;
        }

        $rowNumber = 2;
        foreach ($data as $row) {
            foreach ($row as $cell) {
                $this->excel->getActiveSheet()->setCellValue('B' . $rowNumber, $cell);
                $rowNumber++;
            }
        }

        $filename = "Bos_ticket_details_count_summary_" . date('Y-m-d') . '_' . time() . ".xlsx";
        $file = "export/$filename";

        if (!file_exists(dirname($file))) {
            mkdir(dirname($file), 0770, true);
        }

        header('Content-type: application/ms-excel');
        header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
        header("Cache-control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save("export/$filename");
        header("location: " . base_url() . $file);
    }

    public function support_summary_report_excel() {
        $input = $this->input->get();
        $result = $this->report_model->summary_bos_tkt_detail_support($input);

        foreach ($result as $key => $value) {
            $data[] = array('Total Tickets:' => ($value['total_ticket_booked'] == '' ? 0 : number_format(round($value['total_ticket_booked'], 2))),
                'Total Passenegrs:' => ($value['total_passangers'] == '' ? 0 : number_format(round($value['total_passangers'], 2))),
                'Total Basic Fare:' => ($value['total_basic_fare'] == '' ? 0 : number_format(round($value['total_basic_fare'], 2))),
                'Total UPSRTC Addinal Charges per Passenger (3% or Rs.50):' => ($value['upsrtc_additional_charges'] == '' ? 0 : number_format(round($value['upsrtc_additional_charges'], 2), 2)),
                'Final Fare:' => number_format(round((($value['total_basic_fare'] + $value['upsrtc_additional_charges']) - $value['discount_value']), 2), 2),
                'Total Discount Amount:' => ($value['discount_value'] == '' ? 0 : number_format(round($value['discount_value'], 2), 2)),
                'Total Cancellation Charges:' => ($value['total_cancel_charge'] == '' ? 0 : number_format(round($value['total_cancel_charge'], 2), 2)),
                'Total Refunded Amount:' => ($value['total_refund_amount'] == '' ? 0 : number_format(round($value['total_refund_amount'], 2), 2))
            );
        }

        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Ticket details summary report');
        $this->excel->getActiveSheet()->getStyle('A2:A12')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->mergeCells('A1:B1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Summary Under Report');
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $rownumber = 2;

        foreach ($data[0] as $key => $val) {
            $this->excel->getActiveSheet()->getCell('A' . $rownumber)->setValue($key);
            $rownumber++;
        }

        $rowNumber = 2;
        foreach ($data as $row) {
            foreach ($row as $cell) {
                $this->excel->getActiveSheet()->setCellValue('B' . $rowNumber, $cell);
                $rowNumber++;
            }
        }

        $filename = "Ticket_details_summary_report" . date('Y-m-d') . '_' . time() . ".xlsx";
        $file = "export/$filename";

        if (!file_exists(dirname($file))) {
            mkdir(dirname($file), 0770, true);
        }

        header('Content-type: application/ms-excel');
        header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
        header("Cache-control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save("export/$filename");
        header("location: " . base_url() . $file);
    }
    function pending_user() {
        load_back_view(PENDING_USER_REPORT_VIEW);
    }

    function list_of_pending_user() {

        $data = $this->input->post();
        $from_date = date('Y-m-d', strtotime($data['from_date']));
        $to_date = date('Y-m-d', strtotime($data['to_date']));
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');

        $this->datatables->select('1,DATE_FORMAT(created_date,"%d-%m-%Y %h:%i:%s") as created_date,concat(first_name," ",last_name),sm.stState,cm.stCity,mobile_no,email');
        $this->datatables->from('users u');
        $this->datatables->join('state_master sm', 'u.state=sm.intStateId', 'left');
        $this->datatables->join('city_master cm', 'cm.intCityId=u.city', 'left');

        if (isset($data['from_date']) && $data['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(u.created_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($data['from_date'])));
        }

        if (isset($data['to_date']) && $data['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(u.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($data['to_date'])));
        }       
        if(!empty($session_level)){
        	if($session_level > TRIMAX_LEVEL)
        	{
        		$this->datatables->where('level_'.$session_level,$session_id);
        	}
        	else
        	{
        		$this->datatables->where('level_'.$session_level, TRIMAX_LEVEL);
        	}
        }
        $this->datatables->where("u.email_verify_status = 'N'");
        $this->datatables->where("u.otp_verify_status = 'N'");
		if ($data["user_type"] == COMPANY_ROLE_ID) {
        	$this->datatables->where("u.role_id",COMPANY_ROLE_ID);
        } else if ($data["user_type"] == MASTER_DISTRIBUTOR_ROLE_ID) {
        	$this->datatables->where("u.role_id",MASTER_DISTRIBUTOR_ROLE_ID);
        } else if ($data["user_type"] == AREA_DISTRIBUTOR_ROLE_ID) {
        	$this->datatables->where("u.role_id",AREA_DISTRIBUTOR_ROLE_ID);
        } else if ($data["user_type"] == DISTRIBUTOR) {
        	$this->datatables->where("u.role_id",DISTRIBUTOR);
        } else if ($data["user_type"] == RETAILER_ROLE_ID) {
        	$this->datatables->where("u.role_id",RETAILER_ROLE_ID);
        }
        $data1 = $this->datatables->generate('json');
        
        $newdata_array = json_decode($data1);
            if (!empty($newdata_array->data)) {                
                foreach ($newdata_array->data as &$detail) {
                    $detail['6'] = rokad_decrypt($detail['6'], $this->config->item('pos_encryption_key'));
                    $detail['5'] = rokad_decrypt($detail['5'], $this->config->item('pos_encryption_key'));
                }
            }
            
            echo json_encode($newdata_array);
    }

    function pending_user_excel() {

        $input = $this->input->get();

        $user_result = $this->report_model->pending_user_excel($input);
        $fields = array("Sr No.", "Registration Date", "Username", "State", "City", "Mobile", "Email");
        $stockArr = array();
        $sr_no = 1;

        foreach ($user_result as $key => $value) {

            $data[] = array('Sr no' => $sr_no,
                'Registration Date' => $value['created_date'],
                'Username' => $value['Username'],
                'State' => $value['State'],
                'City' => $value['City'],
                'Mobile' => rokad_decrypt($value['Mobile'], $this->config->item("pos_encryption_key")),
                'Email' => rokad_decrypt($value['Email'], $this->config->item("pos_encryption_key"))
            );
            $sr_no++;
        }
        $this->excel->data_2_excel('user_registration_detail_excel' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
    }
     function pending_kyc() {
        load_back_view(PENDINGKYC_REPORT_VIEW);
    }

    function list_of_pending_kyc() {

        $data = $this->input->post();
        $from_date = date('Y-m-d', strtotime($data['from_date']));
        $to_date = date('Y-m-d', strtotime($data['to_date']));
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');
        
        $this->datatables->select('1,DATE_FORMAT(created_date,"%d-%m-%Y %h:%i:%s") as created_date,concat(first_name," ",last_name),sm.stState,cm.stCity,mobile_no,email');
        $this->datatables->from('users u');
        $this->datatables->join('state_master sm', 'u.state=sm.intStateId', 'left');
        $this->datatables->join('city_master cm', 'cm.intCityId=u.city', 'left');

        if (isset($data['from_date']) && $data['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(u.created_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($data['from_date'])));
        }

        if (isset($data['to_date']) && $data['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(u.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($data['to_date'])));
        }
        if(!empty($session_level)){
        	if($session_level > TRIMAX_LEVEL)
        	{
        		$this->datatables->where('level_'.$session_level,$session_id);
        	}
        	else
        	{
        		$this->datatables->where('level_'.$session_level, TRIMAX_LEVEL);
        	}
        }
        $this->datatables->where("u.kyc_verify_status = 'N'");        
        if ($data["user_type"] == COMPANY_ROLE_ID) {
        	$this->datatables->where("u.role_id",COMPANY_ROLE_ID);
        } else if ($data["user_type"] == MASTER_DISTRIBUTOR_ROLE_ID) {
        	$this->datatables->where("u.role_id",MASTER_DISTRIBUTOR_ROLE_ID);
        } else if ($data["user_type"] == AREA_DISTRIBUTOR_ROLE_ID) {
        	$this->datatables->where("u.role_id",AREA_DISTRIBUTOR_ROLE_ID);
        } else if ($data["user_type"] == DISTRIBUTOR) {
        	$this->datatables->where("u.role_id",DISTRIBUTOR);
        } else if ($data["user_type"] == RETAILER_ROLE_ID) {
        	$this->datatables->where("u.role_id",RETAILER_ROLE_ID);
        }
        $data1 = $this->datatables->generate('json');
        $newdata_array = json_decode($data1);
            if (!empty($newdata_array->data)) {                
                foreach ($newdata_array->data as &$detail) {
                    $detail['5'] = rokad_decrypt($detail['5'], $this->config->item('pos_encryption_key'));
                    $detail['6'] = rokad_decrypt($detail['6'], $this->config->item('pos_encryption_key'));
                }
            }
            echo json_encode($newdata_array);
    }

    function pending_kyc_excel() {

        $input = $this->input->get();
        $user_result = $this->report_model->pending_kyc_excel($input);
        $fields = array("Sr No.", "Registration Date", "Username", "State", "City", "Mobile", "Email");
        $stockArr = array();
        $sr_no = 1;

        foreach ($user_result as $key => $value) {

            $data[] = array('Sr no' => $sr_no,
                'Registration Date' => $value['created_date'],
                'Username' => $value['Username'],
                'State' => $value['State'],
                'City' => $value['City'],
                'Mobile' => rokad_decrypt($value['Mobile'], $this->config->item("pos_encryption_key")),
                'Email' => rokad_decrypt($value['Email'], $this->config->item("pos_encryption_key"))
            );
            $sr_no++;
        }
        $this->excel->data_2_excel('user_registration_detail_excel' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
    }
    public function encrypt_email_mob()
    {
        $input = $this->input->post();
        $mob = $input['mob'];
        $email = $input['email'];  

        $pos_encryption_key = $this->config->item("pos_encryption_key");
        $decrypt = rokad_decrypt($mob,$pos_encryption_key);
        $data['mob'] = $decrypt;

        $pos_encryption_key = $this->config->item("pos_encryption_key");
        $decrypt_email = rokad_decrypt($email,$pos_encryption_key);
        $data['email'] = $decrypt_email;
          
        data2json($data);
    }
    public function fund_transfer_report()
    {
        load_back_view(FUND_TRANSFER_REPORT, $data);
    }
    public function fund_transfer_list()
    {
        $role_id_name = "";
        $role_id_name = $this->input->post('role_id');

        $input = $this->input->post();

        $this->datatables->select('1,DATE_FORMAT(wt.added_on,"%d-%m-%Y %h:%i:%s") as "created_date",concat(first_name," ",last_name),wt.user_id,wt.amt_transfer_from,wt.amt,wt.amt_transfer_to');
        $this->datatables->from('wallet_trans wt');
        $this->datatables->join('users u', 'u.id=wt.user_id', 'left');

        $this->datatables->where_in('u.role_id ', $ids);

        if (isset($input['from_date']) && $input['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(wt.added_on, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['to_date']) && $input['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(wt.added_on, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }

       
        if ($data["user_type"] == "4") { 
            $this->datatables->where("u.role_id",MASTER_DISTRIBUTOR_ROLE_ID);
        } else if ($data["user_type"] == "5") {
            $this->datatables->where("u.role_id",AREA_DISTRIBUTOR_ROLE_ID);
        } else if ($data["user_type"] == "6") {
            $this->datatables->where("u.role_id",DISTRIBUTOR);
        } else if ($data["user_type"] == "7") {
            $this->datatables->where("u.role_id",RETAILER_ROLE_ID);
        }else{
           $this->datatables->where("wt.amt_transfer_from != '0' and wt.amt_transfer_to != '0' ");
        }
        
        $this->datatables->where("wt.amt_transfer_from != '0' and wt.amt_transfer_to != '0' ");

        $data1 = $this->datatables->generate('json');
        echo $data1;
    }
        public function balance_request_report()
    {
        load_back_view(FUND_TRANSFER_REPORT, $data);
    }
    public function balance_request_list()
    {
        $role_id_name = "";
        $role_id_name = $this->input->post('role_id');

        $input = $this->input->post();

        $this->datatables->select('1,DATE_FORMAT(wt.added_on,"%d-%m-%Y %h:%i:%s") as "created_date",concat(first_name," ",last_name),wt.user_id,wt.amt_transfer_from,wt.amt,wt.amt_transfer_to');
        $this->datatables->from('wallet_trans wt');
        $this->datatables->join('users u', 'u.id=wt.user_id', 'left');

        $this->datatables->where_in('u.role_id ', $ids);

        if (isset($input['from_date']) && $input['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(wt.added_on, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['to_date']) && $input['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(wt.added_on, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }

       
        if ($data["user_type"] == "4") { 
            $this->datatables->where("u.role_id",MASTER_DISTRIBUTOR_ROLE_ID);
        } else if ($data["user_type"] == "5") {
            $this->datatables->where("u.role_id",AREA_DISTRIBUTOR_ROLE_ID);
        } else if ($data["user_type"] == "6") {
            $this->datatables->where("u.role_id",DISTRIBUTOR);
        } else if ($data["user_type"] == "7") {
            $this->datatables->where("u.role_id",RETAILER_ROLE_ID);
        }else{
           $this->datatables->where("wt.amt_transfer_from != '0' and wt.amt_transfer_to != '0' ");
        }
        
        $this->datatables->where("wt.amt_transfer_from != '0' and wt.amt_transfer_to != '0' ");

        $data1 = $this->datatables->generate('json');
        echo $data1;
    }

}
