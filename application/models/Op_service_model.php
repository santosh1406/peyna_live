<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Op_service_model extends MY_Model {

    /**
     * Instanciar o CI
     */
    var $table = 'op_services';
    var $fields = "id,op_id,service_id,is_status,created_by,created_date,updated_by,updated_date";
    var $key = "id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    
}
