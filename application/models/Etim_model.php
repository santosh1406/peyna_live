<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Etim_model extends MY_Model {
    
    public function __construct() {
            parent::__construct();
            $this->_init();      
    }
    
    function transAmountEtim($wallet) {
        
        //check if this etim already have entry in etim_balance table
        
        $this->db->select('id, wallet_id, user_id, etim_id, amt');
        $this->db->from('etim_balance');
        $this->db->where(array('wallet_id'=>$wallet['wallet_id'], 'user_id'=>$wallet['user_id'], 'etim_id'=>$wallet['etim_id']));
        $etim_amt_exist = $this->db->get();
        
        if($etim_amt_exist->num_rows()>0) {
            
            $etim_details = $etim_amt_exist->row_array();
            $updated_amt  = $etim_details['amt']+$wallet['transfered_amt'];
            
            $update_amt   = array(
                                'amt'         =>  $updated_amt,
                                'status'      =>  'Credited',
                                'comment'     =>  'Transfered by Agent',
                                'updated_by'  =>  $wallet['added_by'],
                                'updated_date'  =>  date('Y-m-d H:i:s'),
                            );
            $this->db->where(array('wallet_id'=>$etim_details['wallet_id'], 'user_id'=>$etim_details['user_id'], 'etim_id'=>$etim_details['etim_id']));
            $this->db->update('etim_balance',$update_amt);
            
            //Insert record in etim_balance_trans
            $updated_by  = $wallet['added_by'];
            $date        = date("Y-m-d H:i:s");
            
            $insert_etim_trans = array(
                    'w_id'              => $wallet['wallet_id'],
                    'etim_balance_id'   => $etim_details['id'],
                    'amt'               => $wallet['transfered_amt'],
                    'comment'           => 'Transfered Amount to ETIM',
                    'status'            => 'Credited',
                    'added_by'          => $updated_by,
                    'added_on'          => $date,
                    'amt_before_trans'  => $etim_details['amt'],
                    'amt_after_trans'   => $etim_details['amt']+$wallet['transfered_amt']
                    );
            
            $this->db->insert('etim_balance_trans',$insert_etim_trans);
            
        } else {
            
            $insert_amt = array(
                                'wallet_id' =>  $wallet['wallet_id'],
                                'user_id'   =>  $wallet['user_id'],
                                'etim_id'   =>  $wallet['etim_id'],
                                'amt'       =>  $wallet['transfered_amt'],
                                'status'    =>  'Credited',
                                'comment'   =>  'Transfered by Agent',
                                'added_by'  =>  $wallet['added_by'],
                                'added_on'  =>  date('Y-m-d H:i:s'),
                            );
            
            $this->db->insert('etim_balance',$insert_amt);
            $insert_id = $this->db->insert_id(); 
            
            //Insert record in etim_balance_trans
            
            $updated_by  = $wallet['added_by'];
            $date        = date("Y-m-d H:i:s");
            
            $insert_etim_trans = array(
                    'w_id'              => $wallet['wallet_id'],
                    'etim_balance_id'   => $insert_id,
                    'amt'               => $wallet['transfered_amt'],
                    'comment'           => 'Transfered Amount to ETIM',
                    'status'            => 'Credited',
                    'added_by'          => $updated_by,
                    'added_on'          => $date,
                    'amt_before_trans'  => 0.00,
                    'amt_after_trans'   => $wallet['transfered_amt']
                    );
            
            $this->db->insert('etim_balance_trans',$insert_etim_trans);

              
        } 

                //Update data in Wallet data

                $updated_by  = $wallet['added_by'];
                $date        = date("Y-m-d H:i:s");
                $new_amt     = $wallet['wallet_amount']-$wallet['transfered_amt'];
                $comment     = 'Transfered to Etim';
                $status      = 'Debited';
                
                $update_data = array(
                                'amt'      => $new_amt,
                                'status'   => $status,
                                'comment'  => $comment,
                                'added_by' => $updated_by,
                                'added_on' => $date
                                );
                
                $this->db->where('id',$wallet['wallet_id']);
                $this->db->update('wallet',$update_data);


                //select amt after Updating
                $this->db->select('amt');
                $this->db->from('wallet');
                $this->db->where('id',$wallet['wallet_id']);
                $amt_after_add = $this->db->get();
                
                if($amt_after_add->num_rows()>0){
                    
                    $amount_after_add  = $amt_after_add->row_array();
                    $amt_aft_add = $amount_after_add['amt'];

                //Insert same data in wallet_trans table

                $trans_data = array(
                                'w_id'              => $wallet['wallet_id'],
                                'amt'               => $wallet['transfered_amt'],
                                'comment'           => 'Transfered Amount to ETIM',
                                'status'            => 'Debited',
                                'user_id'           => $wallet['user_id'],
                                'added_by'          => $updated_by,
                                'added_on'          => $date,
                                'amt_before_trans'  => $wallet['wallet_amount'],
                                'amt_after_trans'   => $amt_aft_add
                                );

                $this->db->insert('wallet_trans',$trans_data);
                return $last_id = $this->db->insert_id();
                }
                return false;
    }
    
    /*
     * Check ETIM with given serial numbers is available
     */
    
    function check_etim_serial_no($etim_seriale_no) {
        $this->db->select('id as etim_id');
        $this->db->from('etim_master');
        $this->db->where('serial_no',$etim_seriale_no);
        $etimSerialres = $this->db->get();
        if($etimSerialres->num_rows()>0) {
            return $etimSerialres->row_array();
        }
        return false;
    }
    
    /*
     * check EITM balance and transations
     */
    
    function get_etim_balance($agentAvailability) {
        $this->db->select('eb.amt, eb.id as etim_balance_id');
        $this->db->from('etim_balance eb');
        $this->db->where(array('eb.user_id'=>$agentAvailability['user_id'], 'eb.wallet_id'=>$agentAvailability['wallet_id']));
        $etimBalData = $this->db->get();
        if($etimBalData->num_rows()>0) {
            $etimBal['etim_balance'] = $etimBalData->row_array();
            $etim_id = $etimBal['etim_balance']['etim_balance_id'];
            //Transaction data
            $this->db->select('et.amt, et.comment, et.status, et.amt_before_trans, et.amt_after_trans, et.added_on');
            $this->db->from('etim_balance eb');
            $this->db->join('etim_balance_trans et','et.etim_balance_id=eb.id');
            $this->db->where('et.etim_balance_id',$etim_id);
            $this->db->order_by('et.id','DESC');
            $this->db->limit(10);
            $etimTransData = $this->db->get();
            if($etimTransData->num_rows()>0) {
                $etimBal['etim_transaction'] = $etimTransData->result_array();
                
            } else {
                $etimBal['etim_transaction'] = "No Transactions";
            }
            return $etimBal;
        }
        return false;
    }
}
