<?php

class Agent_pattern_commission_detail_model extends MY_Model {

    var $table 	= 'agent_pattern_comm_detail';
    var $fields = array("id","pattern_id","agent_id","op_id","service_id","bus_type");
    var $key 	= "id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

}