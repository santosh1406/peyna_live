<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Package_master_model extends MY_Model {
    
    protected $table = 'package_master';
    var $fields = array("id", "package_name","from","till","default", "status", "created_date", "updated_date", "created_by", "updated_by");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
        
    }
    
     //used in package view
    public function package_detail_by_id($package_id) {
        $this->db->select("
                           pm.id AS package_id,
                           pm.package_name,
                           pm.from AS package_from,
                           pm.till AS package_till,
                           pm.default,
                           pd.id AS package_detail_id,
                           pd.service_id AS service_id,
                           pd.service_name AS service_name,
                           pd.commission_id AS commission_id,
                           pd.commission_name AS commission_name
                          ");
        $this->db->from("package_master pm");
        $this->db->where("pm.id", $package_id);
        $this->db->join("package_detail pd", "pm.id = pd.package_id ", "inner");
        $query = $this->db->get();
        return $query->result();
    }
}