<?php

class Op_fare_coll_detail_model extends MY_Model {

    var $table  = 'op_fare_collection';
    var $fields = array("id","op_id","collection_type","created_by","created_date","updated_by","updated_date");
    
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
    function get_op_notin_fare_coll() 
    {

        $this->db->select('om.op_id,om.op_name');
        $this->db->from('op_master om');
        $this->db->where('`op_id` NOT IN (SELECT `op_id` FROM `op_fare_collection`)', NULL, FALSE);
       
        $state = $this->db->get();


        return $state->result_array();
    }
    
}
?>
