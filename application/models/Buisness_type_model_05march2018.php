<?php

class Buisness_type_model extends MY_Model
{
        var $table = 'buisness_type';
        var $fields = array("btype_id","btype","status");
        var $key = 'btype_id';

        public function __construct() 
        {
            parent::__construct();
            $this->_init();
        }
    
        
        public function get_buisnessType(){
          
          $sQuery="SELECT btype_id,btype FROM buisness_type WHERE status=1";
          $query = $this->db->query($sQuery);
          return $query->result();
        }
        public function get_schemes(){
          
          $sQuery="SELECT scheme_id,scheme FROM agent_scheme WHERE status=1";
          $query = $this->db->query($sQuery);
          return $query->result();
        }
        public function get_sellerpoints(){
          
          $sQuery="SELECT BUS_STOP_CD,BUS_STOP_NM FROM cb_bus_stops where flag = 'Y' order by BUS_STOP_NM";
          $query = $this->db->query($sQuery);
          return $query->result();
        }
        public function get_depots(){
          
          $sQuery="SELECT DEPOT_CD,DEPOT_NM FROM cb_depots";
          $query = $this->db->query($sQuery);
          return $query->result();
        }
         public function get_service(){
          
          $sQuery="SELECT id,service_name FROM service_master WHERE status=1 and id=2";
          $query = $this->db->query($sQuery);
          return $query->result();
        }

}

?>
