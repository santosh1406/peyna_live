<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_registration_model extends CI_Model
{
	function __construct() {
		parent::__construct();
	}

	public function insert_customer_reg_data($data,$table_nm) {
		$this->db->insert($table_nm,$data);
		return $this->db->insert_id();
	}

	public function insert_customer_reg_bulk_data($data,$table_nm) {

		$this->db->insert_batch($table_nm,$data);
		return $this->db->insert_id();
	}

	public function update_trimax_id($where_data,$trimax_id,$receipt_number,$modified_by) {

		$set_arr = array('trimax_card_id' => $trimax_id, 'modified_by' => $modified_by);

		$this->db->where('id',$where_data['customer_card_master_id']);
		$this->db->update(CUSTOMER_CARD_MASTER,array('master_card_id' => $where_data['master_card_id'], 'trimax_card_id' => $trimax_id, 'display_card_id' => $trimax_id));

		$this->db->where('id',$where_data['card_trans_id']);
		$this->db->update(CARD_TRANS,array('trimax_card_id' => $trimax_id, 'receipt_number' => $receipt_number, 'modified_by' => $modified_by));

		$this->db->where('id',$where_data['card_trans_details_id_1']);
        $this->db->update(CARD_TRANS_DETAILS,$set_arr);

        if(isset($where_data['card_trans_details_id_2'])) {
            $this->db->where('id',$where_data['card_trans_details_id_2']);
    		$this->db->update(CARD_TRANS_DETAILS,$set_arr);
        }

		$this->db->where('id',$where_data['amt_trans_id']);
        $this->db->update(AMOUNT_TRANS,$set_arr);

        $this->db->where('id',$where_data['master_card_id']);
        $this->db->update(MASTER_CARD,array('customer_id' => $where_data['customer_master_id'], 'trimax_card_id' => $trimax_id, 'display_card_id' => $trimax_id));

        $this->db->where('id',$where_data['inventory_master_id']);
        $this->db->update(INVENTORY,array('user_id' => $where_data['customer_master_id'], 'trimax_card_id' => $trimax_id, 'card_no' => $trimax_id));
	}

	public function get_card_val_exp_date($conc_id) {
	
            $this->db->select('expiry_type, valid_from, valid_to, valid_days');
            $this->db->from('ps_concession_master');
            $this->db->where(array('id' => $conc_id, 'is_active' => '1'));
            $query = $this->db->get();
            $result = $query->row_array();
            return $result;
	}

	public function get_vendor_id($app_user_id) {
            
            $this->db->select('vendor_id');
            $this->db->from('users');
            //$this->db->where(array('id' => $app_user_id, 'is_active' => '1'));
            $this->db->where(array('id' => $app_user_id));
            $query = $this->db->get();
            $result = $query->row_array();
            return $result;
	}

	public function get_card_category_id($card_catgry_cd) {

		$this->db->select('id');
		$this->db->from('ps_card_category');
		$this->db->where(array('card_category_code' => $card_catgry_cd, 'is_active' => '1'));
		$query = $this->db->get();
    	$result = $query->row_array();
    	return $result;
	}

//	public function get_card_fee_master_id($data) {
//
//		$this->db->select('id,ctrans_type_id');
//		$this->db->from('ps_card_fee_master');
//		$this->db->where(array('concession_id' => $data['pass_concession_id'], 'span_id' => $data['span_id'], 'ctrans_type_map' => $data['trans_type'], 'is_active' => '1'));
//		$this->db->where("'".TODAY_DATE."' BETWEEN effective_from and effective_till");
//		$query = $this->db->get();
//    	$result = $query->row_array();
//        /*echo $this->db->last_query();
//        log_data('rest/smart_card_customer_registration_api_' . date('d-m-Y') . '.log', $this->db->last_query(), 'api');*/
//    	return $result;
//	}
        
        public function get_card_fee_master_id($data) {
		$this->db->select('p_cfm.id, p_cfm.ctrans_type_id, p_cfm.effective_from, p_cfm.effective_till, p_rs.span_period_days');
		$this->db->from('ps_card_fee_master p_cfm');
        $this->db->join('ps_rfidmaster_span p_rs','p_rs.id = p_cfm.span_id');
		$this->db->where(array('p_cfm.concession_id' => $data['pass_concession_id'], 'p_cfm.span_id' => $data['span_id'], 'p_cfm.ctrans_type_map' => $data['trans_type'], 'p_cfm.is_active' => '1'));
        if($data['pass_concession_id'] == TAL_STUDENT_STATE || $data['pass_concession_id'] == TAL_STUDENT_INTER_STATE ){
            $this->db->where("'".$data['activation_date']."' BETWEEN p_cfm.effective_from and p_cfm.effective_till");
        }else{
            $this->db->where("'".TODAY_DATE."' BETWEEN p_cfm.effective_from and p_cfm.effective_till");
        }
		$query = $this->db->get();
    	$result = $query->row_array();
//        echo $this->db->last_query();
//        die();
    	return $result;
	}

	public function get_card_fee_details($id) {

		$this->db->select('
							p_cfd.fee_type_value,
							p_ftm.fee_type_nm,
							p_ftm.fee_type_cd,
							p_ftm.operand
						');
		$this->db->from('ps_card_fee_details p_cfd');
		$this->db->join('ps_fee_type_master p_ftm','p_ftm.id = p_cfd.fee_type_id','inner');
		$this->db->where(array('p_cfd.card_fee_mas_id' => $id, 'p_cfd.is_active' => '1', 'p_ftm.is_active' => '1'));
		$query = $this->db->get();
    	$result = $query->result_array();
    	return $result;
	}

	public function get_concs_service_id($req_val) 
        {
            
		$this->db->select('p_csr.id');
		if(isset($req_val['bus_type_cd']) && !empty($req_val['bus_type_cd'])) {
			$this->db->join('bus_types b_typs','b_typs.BUS_TYPE_ID = p_csr.bus_service_type_id','inner');
			$this->db->where(array('p_csr.concession_id' => $req_val['pass_concession_id'], 'b_typs.BUS_TYPE_CD' => $req_val['bus_type_cd'], 'p_csr.is_active' => '1'));
		} else {
			$this->db->where(array('p_csr.concession_id' => $req_val['pass_concession_id'], 'p_csr.bus_service_type_id' => '0', 'p_csr.is_active' => '1'));
		}
		$this->db->where("'".TODAY_DATE."' BETWEEN p_csr.effective_from and p_csr.effective_till");
		$query = $this->db->get('ps_concession_service_rel p_csr');
    	$result = $query->row_array();
     
    	return $result;
	}

	public function get_route_num($where) {

        $DB2 = $this->load->database('msrtcors',TRUE);
        // $DB2->select('route_no,route_nm,from_stop_nm,from_stop_cd,till_stop_nm,till_stop_cd,till_stop_nm,via_stop_cd');
        // $DB2->from('v_routes');
        // $DB2->where($where);
        // $DB2->limit(5);
        // $DB2->order_by('route_no','asc');
        // $query = $DB2->get();
        // $result = $query->result_array();
        // return $result;

        $DB2->select('vr.route_no,vr.route_nm,vr.from_stop_nm,vr.from_stop_cd,vr.till_stop_nm,vr.till_stop_cd,vr.till_stop_nm,vr.via_stop_cd');
        $DB2->from('v_routes vr');
        $DB2->join('route_stops rs','rs.ROUTE_NO = vr.route_no','inner');
        $DB2->join('route_stops rs1','rs1.ROUTE_NO = vr.route_no','inner');
        $DB2->where(array('rs.BUS_STOP_CD' => $where['from_stop_cd'], 'rs1.BUS_STOP_CD' => $where['till_stop_cd']));
        $DB2->where('rs.STOP_SEQ < rs1.STOP_SEQ');
        if(isset($where['via_stop_cd']) && !empty($where['via_stop_cd'])) {
            $DB2->where('vr.via_stop_cd', $where['via_stop_cd']);
        }
        $DB2->limit(10);
        $DB2->order_by('vr.route_no','asc');
        $query = $DB2->get();
        $result = $query->result_array();
        return $result;
	}
        
    public function getRegistrationDetails($trimax_card_id, $flag) {
        $this->db->select("
                CONCAT(p_cm.first_name,' ',p_cm.middle_name,' ',p_cm.last_name) as full_name,
                       DATE_FORMAT(p_cm.date_of_birth, '%d-%m-%Y') as date_of_birth, 
                       p_cm.mobile_number as mobileNo,
                       p_cm.address,
                       p_cm.pincode,
                       p_ccm.trimax_card_id,
                       p_ct.receipt_number,
                       p_ct.opening_balance as existing_bal,
                       p_ct.closing_balance as total_wallet_bal,
                       p_ct.amount as topup_amt,
                       (p_ct.id) as card_transaction_id,
                       p_ct.pass_category_id as passCategoryId,
                       p_conm.concession_nm,
                       p_rs.span_period_days as span_name,
                       rd.DEPOT_NM as registered_from,
                       cd.DEPOT_NM as collect_from,
                       pcc.card_category_name as cardType,
                       p_ctd.route_no,
                       at.terminal_code as terminalCode,
                       au.user_name as registeredBy,
                       bt.BUS_TYPE_NM as busServiceType,
                       p_pc.category_name as category_name,
                       p_ct.transaction_type_id,
                       DATE_FORMAT(p_ct.activation_date, '%d/%m/%Y') as validity_date,
                       DATE_FORMAT(p_ct.expiry_date, '%d/%m/%Y') as expiry_date,
                       DATE_FORMAT(p_cm.created_date, '%d/%m/%Y %k:%i:%s') as registration_date");
        
        $this->db->from('ps_customer_master p_cm ');
        $this->db->join('ps_customer_card_master p_ccm','p_cm.id = p_ccm.customer_id','inner');
        $this->db->join('app_user au','au.id = p_ccm.created_by','inner');
        $this->db->join('ps_card_category pcc','pcc.card_category_code = p_ccm.card_category_cd','inner');
        $this->db->join('depots rd','rd.DEPOT_CD = p_ccm.registration_depo_cd','inner');
        $this->db->join('depots cd','cd.DEPOT_CD = p_ccm.collection_depo_cd','inner');
        $this->db->join('ps_card_transaction p_ct','p_ct.trimax_card_id = p_ccm.trimax_card_id','inner');
        $this->db->join('ps_card_transaction_details p_ctd','p_ctd.card_transaction_id = p_ct.id','inner');
        $this->db->join('retailer_service_mapping rsm','rsm.terminal_id = p_ct.terminal_id','inner');
        //$this->db->join('app_terminals at','at.terminal_id = p_ct.terminal_id','inner');
        $this->db->join('bus_types bt','bt.BUS_TYPE_CD = p_ct.bus_type_cd','left outer');
        $this->db->join('ps_concession_master p_conm','p_ct.pass_concession_id = p_conm.id','inner');
        $this->db->join('ps_pass_category p_pc','p_pc.id = p_conm.pass_category_id','inner');
        $this->db->join('ps_rfidmaster_span p_rs','p_ct.span_id = p_rs.id','inner');
        $this->db->where('p_ccm.trimax_card_id', $trimax_card_id);
        if($flag == 'RG'){
            $this->db->where('p_ct.transaction_type_id', REG_PPC_TRANS_TYPE_ID); // RG
            $this->db->where('p_ct.write_flag', '1');
        }
        if($flag == 'SW'){
            $this->db->where('p_ct.transaction_type_id', SW_LOAD_TRANS_TYPE_ID); // SW
        }
        if($flag == 'TW'){
            $this->db->where('p_ct.transaction_type_id', TW_LOAD_CASH_TRANS_TYPE_ID); // TW
        }
        if($flag == 'TWSW'){
            $this->db->where('p_ct.transaction_type_id', TW_LOAD_SW_TRANS_TYPE_ID); // TW via SW
        }
        if($flag == 'RGOT'){
            $this->db->where('p_ct.transaction_type_id', REG_OTC_TRANS_TYPE_ID); // RG OTC
            $this->db->where('p_ct.write_flag', '1'); // RG OTC
        }
        $this->db->limit(1);
        $this->db->order_by('p_ct.created_date','DESC');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
        
    public function getCustomerReg($trimax_card_id, $flag) {
        
        $returnArr = $this->getRegistrationDetails($trimax_card_id, $flag);
        if(!empty($returnArr)){
            foreach ($returnArr as $key => &$value) {
                $amtArr = $this->getTotalAmtByTrimaxId($value['card_transaction_id']);
                $value['total_amt'] = $amtArr['total_amount'];
                $amtDetailsArr = $this->getAmtDetailsByAmtTransId($amtArr['id'],$flag);
                $value['card_fee_details'] = $amtDetailsArr;
                if($flag == 'RG' && $value['transaction_type_id'] == REG_PPC_TRANS_TYPE_ID){
                    $value['passRoute'] = $this->getPassRoute($value['route_no']);
                    $registration_date = str_replace('/', '-', $value['registration_date']); 
                    $value['tentative_delivery_date'] = date('d-m-Y', strtotime($registration_date. ' + 10 days'));
                    $value['span_name'] = "".$value['span_name']." Days";
                    $value['receiptFor'] = '';
                }
                unset($value['transaction_type_id']);
            }
        }
        return $returnArr;
    }
        
    public function getPassRoute($route_no) {
        $msrtc_ors_db = $this->load->database('msrtcors', TRUE);
        $msrtc_ors_db->select('route_nm');
        $msrtc_ors_db->from('v_routes');
        $msrtc_ors_db->where('route_no',$route_no);
        $query = $msrtc_ors_db->get();
        $result = $query->result_array();
        return isset($result[0]['route_nm']) ? $result[0]['route_nm'] :'';
    }
    
    public function getTotalAmtByTrimaxId($cardTransactionId) {
        $this->db->select('id, total_amount');
        $this->db->from('ps_amount_transaction');
        $this->db->where('card_transaction_id',$cardTransactionId);
        $this->db->where('is_active','1');
        $query = $this->db->get();
        $result = $query->result_array();
        return isset($result[0]) ? $result[0] :'';
    }
        
    public function getAmtDetailsByAmtTransId($amtTransactionId,$flag) {
        $this->db->select('ps_atd.amount_code, ps_atd.amount, p_ftm.fee_type_nm');
        $this->db->from('ps_amount_transaction_details ps_atd');
        $this->db->join('ps_fee_type_master p_ftm','p_ftm.fee_type_cd = ps_atd.amount_code','left');
        $this->db->where('ps_atd.amount_transaction_id',$amtTransactionId);
        $this->db->where('ps_atd.is_active','1');
        $query = $this->db->get();
        $result = $query->result_array();
        $i = 0;
        $data = array();
        if(isset($result[0])){
            foreach ($result as $key => &$value) {
                $data[$i]['fee_type_value'] = $value['amount'];
                if($value['amount_code'] === 'BF'){
                    $data[$i]['fee_type_nm'] = 'Base Fare';
                }else if($value['amount_code'] === 'PF'){
                    $data[$i]['fee_type_nm'] = 'Processing Fee';
                }else{
                    if($flag == 'RG'){
                        $data[$i]['fee_type_nm'] = $value['fee_type_nm'];
                    }else{
                        $data[$i]['fee_type_nm'] = $value['amount_code'];
                    }
                }
                $i++;
            }
        }
        return $data;
    }

    public function get_inventory_card_data($cust_card_id) {
        $this->db->select('id,trimax_card_id');
        $this->db->from(INVENTORY);
        $this->db->where(array('cust_card_id' => $cust_card_id));
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }

    public function get_card_data($cust_card_id) {
        $this->db->select('id,trimax_card_id');
        $this->db->from(MASTER_CARD);
        $this->db->where(array('cust_card_id' => $cust_card_id));
        // $this->db->where_in('card_status',array('Y','A'));
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }

    public function get_customer_data($trimax_id) {
        $this->db->select('id');
        $this->db->from(CUSTOMER_CARD_MASTER);
        $this->db->where(array('trimax_card_id' => $trimax_id));
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }
     public function getCardTransDetails($c_id){
        $this->db->select('*');
        $this->db->from('ps_card_transaction_details ctd');
        $this->db->where('ctd.card_transaction_id',$c_id);
        $this->db->where('ctd.is_active','1');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    
    public function insert_itms_cust_reg_data($data,$table_nm) { 
        $this->db->insert($table_nm,$data); 
        return $this->db->insert_id(); 
    }
}
