<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Payment_gateway_master_model extends MY_Model {

    var $table = 'payment_gateway_master';
    var $fields = array("id", "payment_gateway_name", "status", "added_on", "added_by", "updated_on", "updated_by");
    var $key = 'id';
    protected $set_created = false;
    protected $set_modified = false;

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    function pg_wallet_balance($pg_name) {
    	$this->db->select('pgw.id,pgw.amount,pgm.id as pg_id');
    	$this->db->from('payment_gateway_master pgm');
        $this->db->join('payment_gateway_wallet pgw','pgw.payment_gateway_id=pgm.id');
        $this->db->where(array("pgm.payment_gateway_name" => $pg_name,"pgm.status" => "1"));
        $PgData = $this->db->get();
        if($PgData->num_rows()>0) {
        	return $PgData->row_array();
        }
        return false;
    }

}

?>