<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_otp_model extends MY_Model {
    
    var $table  = 'user_otp';
    var $fields = array("id","user_id","mobile_no","otp_code","verified","verify_date","reason","sent_date");
    var $key    = 'id';
	
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
	
	public function get_last_sent($user_id, $mobile_no) {        
		$this->db->select('*');
        $this->db->from('user_otp');
        $this->db->where('user_id', $user_id);
		$this->db->where('mobile_no', $mobile_no);
		$this->db->order_by('id', 'desc');
		$this->db->limit(1);
		$query = $this->db->get();
        return $query->row_array();
    }

	public function save_otp($data) {        
		$flag = false;
        if (!empty($data))
		{
			if ($this->db->insert('user_otp', $data)) {				
				$flag= $this->db->insert_id();
			} 
		}
		return $flag;
    }
	
	
	function update_verify_status($id)
	{
		$data=array("verified"=>"Y", "verify_date"=> date('Y-m-d H:i:s'));
		$this->db->where('id',$id);          
		$this->db->update('user_otp',$data);
	}
	
	function get_records_for_time($user_id, $mobile_no)
	{
		$count = 0;
		$current = date('Y-m-d H:i:s');		
		$date_past = date("Y-m-d H:i:s", strtotime("-60 minutes", strtotime($current)));		
		
		$this->db->select('*');
        $this->db->from('user_otp');
		$this->db->where('user_id', $user_id);
		$this->db->where('mobile_no', $mobile_no);
		$this->db->where('sent_date >', $date_past);		
		$query = $this->db->get();
        $count = $query->num_rows();			
		return $count;
	}

}