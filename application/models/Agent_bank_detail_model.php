<?php

class  Agent_bank_detail_model extends MY_Model 
{
    
    var $table = 'agent_bank_detail';
    var $fields = array("agent_bk_id","agent_id","bank_acc_no","bank_name","bank_acc_name","bank_branch","bank_ifsc_code","payment_mode","inserted_by","inserted_date","updated_by","updated_date","is_status","is_deleted");
    
    var $key = "agent_bk_id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
    
    /*
     * @function    : get_alluser
     * @param       : 
     * @detail      : To fetch all users from user table.
     */

    function get_data($id) {
        $this->db->select("*");
        $this->db->from("agent_bank_detail");
        if ($id != "") {
            $this->db->where('agent_id', $id);
        }

        $user_result = $this->db->get();

        $user_result_array = $user_result->result_array();
        return $user_result_array;
    }
}

?>