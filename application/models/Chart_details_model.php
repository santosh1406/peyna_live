<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Chart_details_model extends MY_Model {

    /**
     * Instanciar o CI
     */
    var $table = 'chart_details';
    var $fields = array("id", "chart_mst_id", "route_no", "from_stop", "till_stop", "from_sequence", "till_sequence", "fare");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    public function get_chart_details_stop_list($route_id) {
        $data = array();
        $this->db->select("id,from_stop,till_stop,from_sequence,till_sequence");
        $this->db->from("chart_details");
        $this->db->where("route_no", $route_id);
        $this->db->where("status", "Y");
        $query = $this->db->get();
        $data = $query->result_array();
        //$data['cnt'] = count($data);
    
        if (!empty($data)) {
            foreach ($data as $value) {
                $from_stop[] = $value['from_stop'];
                $till_stop[] = $value['till_stop'];
                $from_sequence[] = $value['from_sequence'];
                $till_sequence[] = $value['till_sequence'];
            }
            $from_sequence_max = max($from_sequence);
            $till_sequence_max = max($till_sequence);

            //$a1 = array_filter(array_unique($from_stop));
            //$a2 = array_filter(array_unique($till_stop));
            //$data['cnt'] = count(array_unique(array_merge($a1, $a2)));
            $data['cnt'] = max($from_sequence_max, $till_sequence_max);
        } else {
            $data['cnt'] = 0;
        }
        return $data;
    }

    public function chk_route_chart_details($route_no, $route_id) {
        $res_route_stops = $this->route_stop_model->get_route_stop_list($route_id, $route_no);
        $res_chart_details_stops = $this->chart_details_model->get_chart_details_stop_list($route_id);
        if ($res_route_stops['cnt'] != $res_chart_details_stops['cnt']) {
            $data_staus['update'] = 1;
        } else {

            $data_staus['update'] = 0;
        }
        return $data_staus;
    }

}
