<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Zip_route_stop_model extends CI_Model {
   
    function __construct() {
        parent::__construct();
	    //$this->db2 = $this->load->database('local_up_db', TRUE);	
    }
    
    public function getBusStopCode(){
        
        $this->db->distinct();
        $this->db->select('BUS_STOP_CD');
        $this->db->where('STATUS','Y');
        $this->db->order_by('BUS_STOP_CD', "asc");
        $query = $this->db->get( MSRTC_DB_NAME.'.bus_stop_routes_link');
        if($query->num_rows() > 0){
            return $query->result();
        }
    }
    
    public function get_route($input){
        $this->db->select('ROUTE_NO');
        $this->db->where('BUS_STOP_CD', $input['seller_cd']);
        $this->db->where('STATUS','Y');
        $query = $this->db->get( MSRTC_DB_NAME.'.bus_stop_routes_link');
        if($query->num_rows() > 0){
            return $query->result();
        }
    }
    
    public function header_of_data($table,$head,$extra_name){
        $header .=$head."\r\n";; 
        $col_header =$this->db->list_fields($table);
        $header .= "'".implode("','", $col_header) . "','".$extra_name."'\r\n";
        return $header;
    }
    
    public function create_route($bus_stop_cd,$route_no){

         $route_details = "select r.*,vr.route_nm as route_name "
                 . "FROM msrtc_replication.routes r "
                 . "INNER JOIN msrtc_replication.v_routes vr ON r.ROUTE_NO = vr.route_no "
                 . "WHERE r.ROUTE_NO = '" . $route_no . "'";
        $route_details_prepare = $this->db->query($route_details);
        $route_details_array = $route_details_prepare->result_array();

        if (count($route_details_array) > 0) {
            foreach ($route_details_array as $value) {
                $route_details_array_keys .= "'" . implode("','", $value) . "'#\r\n";
            }
        }

        return $route_details_array_keys;
    } 
    
    public function create_route_stop_csv_file($bus_stop_cd,$route_no) {

        $route_stop_details = "select rs.*,bs.DEVNAGIRI_NM"
                . " FROM msrtc_replication.route_stops rs "
                . "LEFT JOIN msrtc_replication.bus_stops bs ON bs.BUS_STOP_CD = rs.BUS_STOP_CD "
                . "WHERE rs.ROUTE_NO = '" . $route_no . "'";
        $route_stop_details_prepare = $this->db->query($route_stop_details);
        $route_stop_details_array = $route_stop_details_prepare->result_array();
        
        if (count($route_stop_details_array) > 0) {
//            $route_stop_details_array_key .= "\r\n####Route Stop Details####\r\n";
//            $route_stop_details_array_key .= "'" . implode("','", array_keys($route_stop_details_array[0])) . "','DEVNAGIRI_NM'\r\n";
            foreach ($route_stop_details_array as $value) {
                $route_stop_details_array_keys .= "'" . implode("','", $value) . "'#\r\n";
            }  
            //return $route_stop_details_array_key.$route_stop_details_array_keys;
            return $route_stop_details_array_keys;
        }

    }
   
}    

