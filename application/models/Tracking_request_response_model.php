<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Tracking_request_response_model extends MY_Model
{
    /**
     * Instanciar o CI
     */
    var $table    = 'tracking_request_response';
    var $fields		= array("id","ticket_id","type","pg_tracking_id","request","response","created_at");
    var $key    = 'id';

    protected $set_created = false;

    protected $set_modified = false;
    
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
    
}
