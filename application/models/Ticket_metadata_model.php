<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ticket_metadata_model extends MY_Model {

    /**
     * Instanciar o CI
     */
    var $table = 'ticket_metadata';
    var $fields = array("id","ticket_id","temp_booking_response","confirm_booking_response","temp_booking_request","inserted_date");

    public function __construct()
    {
        parent::__construct();
        $this->_init();
    }
}
