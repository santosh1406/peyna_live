<?php

//Created by Prabhat on 21-12-2018
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Search_bus_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    // added by harshada kulkarni on 24-07-2018
    public function getStops($stop = "") {
        $queryStr = "select *"
                . " FROM all_bus_stops"
                . " WHERE BUS_STOP_NM LIKE '$stop%' group by BUS_STOP_NM limit 10";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        $nCount = count($result);

        if ($nCount > 0) {
            for ($i = 0; $i < $nCount; $i++) {
                $aResult[$i]['label'] = $result[$i]['BUS_STOP_NM'];
                $aResult[$i]['name'] = $result[$i]['BUS_STOP_NM'];
                $aResult[$i]['value'] = $result[$i]['id'];
                $aResult[$i]['id'] = $result[$i]['id'];
            }
        } else {
            $aResult[0]['label'] = 'No result found';
            $aResult[0]['name'] = 'No result found';
            $aResult[0]['value'] = '';
            $aResult[0]['id'] = '';
        }
        //$aResult_json = json_encode($aResult);
        return $aResult;
    }

    // added by Sonali Kamble on 13-11-2018
    public function saveWalletTransactionData($data = "") {
        $this->db->insert('wallet_trans', $data);
        $insert_id = $this->db->insert_id();
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'Wallet transaction.log', $data, 'Debited/credited');
        return  $insert_id;
        
    }

    public function saveTicketData($data = "") {
        if ($data) {
            $this->db->insert('tickets', $data);
            $insert_id = $this->db->insert_id();
            return  $insert_id;
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'tickets_tbl.log', $data, 'tickets tbl');
            return true;
        }
        return false;
    }

    public function updateTicketData($block_key, $data = array()) {
        $this->db->where('ticket_ref_no', $block_key);
        $query = $this->db->update('tickets', $data);
        return $query;
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'update_tickets_tbl.log', $data, 'update tickets tbl');
    }

    public function newUpdateTicketData($table, $updateData, $blockKey) {
        
        $this->db->where('ticket_ref_no', $blockKey);
        $query = $this->db->update('tickets', $updateData);
        return $query;
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'update_tickets_tbl.log', $updateData, 'update tickets tbl');
    }

    public function batchInsertTicketDetails($table, $data)
    {
       return $this->db->insert_batch($table, $data);
    }
    
    public function saveTicketDetailsData($data = "") {
        $this->db->insert_batch('ticket_details', $data);
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ticket_details.log', $data, 'ticket details');
    }

    public function saveTicketDumpData($data = "") {
        $this->db->insert('ticket_dump', $data);
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ticket_dump.log', $data, 'ticket dump');
    }

    public function saveCancelTicketData($data) {
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'Cancel_ticket.log', $data, '3m Cancel ticket details.log');
        $this->db->insert('cancel_ticket_data', $data);
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'cancel_ticket_tbl.log', $data, 'cancel ticket');
        return true;
    }
   public function getTicketsDetails($bos_key) {
       $this->db->select('*');
       $this->db->from('tickets');
       $this->db->where('boss_ref_no', $bos_key);
       $result = $this->db->get();
       return $result->result_array();
   }
   
   public function getTicketDetails($blockKey)
   {
       $this->db->select('ticket_id, ticket_ref_no, tot_fare_amt_with_tax, total_fare_without_discount, wallet_trans_id');
       $this->db->from('tickets');
       $this->db->where('ticket_ref_no', $blockKey);
       $this->db->limit(1);
       $result = $this->db->get();
       return $result->result_array();
   }
   public function insertTicketDump($table, $data)
   {
       $this->db->insert($table, $data);
   }
   public function getTickets($bos_key) {
       $this->db->select('ticket');
       $this->db->from('ticket_dump');
       $this->db->where('bos_ref_no', $bos_key);
       $result = $this->db->get();
       return $result->result();
   }
   
   public function getPassengerDetails($ticketId)
   {
       $this->db->select('*');
       $this->db->from('ticket_details');
       $this->db->where('ticket_id', trim($ticketId));
       $result = $this->db->get();
       return $result->result_array();
   }
   
   public function checkExistTicket($ticketId)
   {
       $result = false;
       $this->db->select('ticket_id');
       $this->db->from('ticket_dump');
       $this->db->where('ticket_id', trim($ticketId));
       $query = $this->db->get();
       $result = $query->result_array();
       if(count($result) > 0)
       {
           $result = true;
       }
       return $result;
   }
   
   public function cancelUpdateTicketData($ticket_id, $data = array()) {
        $this->db->where('ticket_id', $ticket_id);
        $query = $this->db->update('tickets', $data);
        return $query;
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'update_tickets_tbl.log', $data, 'update tickets tbl');
    }
    
    public function cancelUpdateVasData($rokad_ref_no, $data = array()) {
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'update_tickets_tbl.log', $data, 'update tickets tbl');
        $this->db->where('transaction_no', $rokad_ref_no);
        $query = $this->db->update('vas_commission_detail', $data);
        return $query;
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'update_tickets_tbl.log', $data, 'update tickets tbl');
}
    
    public function getReturnTicketId($blockKey) {
       $this->db->select('ticket_id');
       $this->db->from('tickets');
       $this->db->where('ticket_ref_no', $blockKey);
       $query = $this->db->get();
       $result = $query->result_array();
       
       return $result;
}
}
