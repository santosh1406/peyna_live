<?php

class Booking_agpra_model extends MY_Model 
{
    
    var $table = 'agent_parameter';
    var $fields = array("agent_para_id","agent_id","comission_type","amount","bank_acc_name","credit_limit","credit_limit_days","contract_start","contract_end","bank_acc_no","bank_name","bank_branch","bank_ifsc_code","payment_mode","payment_freq","created_by","created_date","updated_by","updated_date","is_status","is_deleted");
    
    var $key = "agent_para_id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
    
}

?>