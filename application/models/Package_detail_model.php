<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Package_detail_model extends MY_Model {

    public $variable;
    protected $table = 'package_detail';
    var $fields = array("id", "package_id", "service_id", "service_name", "commission_id", "commission_name", "status", "created_date", "created_by", "updated_date", "updated_by");
    var $key = 'id';

    public function __construct() {

        parent::__construct();
    }
    
    // edited by harshada kulkarni on 17-08-2018
    function getpackagedetails($service_id) {
        $now = date('Y-m-d H:i:s', time());
        $this->db->select("pm.id,pm.package_name,(CASE WHEN pm.default = '0' THEN DATE_FORMAT(pm.till,'%Y-%m-%d') >= now() END) status");
        $this->db->from("package_detail pd");
        $this->db->join("package_master pm", "pd.package_id=pm.id", "left");
        $this->db->where("pd.service_id =", $service_id);
        $this->db->where("pm.status =", '1');
        $this->db->where("pm.is_deleted =", '0');    
        
        if ($this->session->userdata('role_id') == COMPANY_ROLE_ID) {
            $this->db->where('pm.created_by', $this->session->userdata('id'));
        }
        
        $user_result = $this->db->get();

        $user_result_array = $user_result->result_array();
        return $user_result_array;
    }

    public function get_assign_pkg_comm($comm_id) {
        $this->db->select('pm.id');
        $this->db->from("package_detail pd");
        $this->db->join("package_master pm", "pm.id = pd.package_id and pd.commission_id=$comm_id", "inner");
        $this->db->where("pm.is_deleted =", '0');
        $user_result = $this->db->get();
        $user_result_array = $user_result->result_array();
        return $user_result_array;
    }

    public function get_default_pkg_comm() {
        $this->db->select("cm.name as commission_name, cd.company_commission as commission_percentage, cm.commission_default_flag,cd.*");
        $this->db->from('package_detail as pd');
        $this->db->join('package_master as pm', 'pm.id=pd.package_id', 'inner');
        $this->db->join('2_commission_master as cm', 'cm.id=pd.commission_id', 'inner');
        $this->db->join('2_commission_detail as cd', 'cd.commission_id=cm.id', 'inner');
        $this->db->where(array('pm.default' => '1', 'pm.status' => '1', 'pm.is_deleted' => '0'));
        $this->db->where(array('pd.status' => '1'));
        $this->db->where(array('cm.commission_default_flag' => '1', 'cm.status' => '1', 'cm.is_deleted' => '0'));
        $result = $this->db->get();
        $result_array = $result->result_array();
        return $result_array;
    }
    
    function get_serivce_list()
    {
       $this->db->select('*');
       $this->db->from('service_master');
       $data=$this->db->get();
       return $data->result_array();
    }
}

/* End of file  */
/* Location: ./application/models/ */
