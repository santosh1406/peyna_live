<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Payout_summary_details_model extends MY_Model
{
    var $table    = 'payout_summary_details';
    var $fields		= array("id","pay_id","pay_amount","settlement_bank","trans_currency"
    	,"sub_acc_Id","settlement_date","settlement_currency","error_code","error_desc","inserted_date");
    var $key    = 'id';

    protected $set_created = false;

    protected $set_modified = false;
    
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
     
 public function insert_payout_data($payout_data)
 {
     if (!empty($payout_data))
        {
              $query = $this->db->get_where('payout_summary_details', 
                array('settlement_date' => $payout_data['settlement_date'] )); 
              $count = $query->num_rows();
              if ($count === 0) 
              {
                if($this->db->insert('payout_summary_details', $payout_data))
                  {
                    return true;
                  }
              }
        }
        else
        {
            return false;
        } 
 }

  public function pay_id_details($date)
  {  
     if(!empty($date))
     {
        $this->db->select('pay_id');
        $this->db->from('payout_summary_details');
        $this->db->where("DATE_FORMAT(inserted_date, '%Y-%m-%d') >=", $date);
        $query = $this->db->get();
        $data = $query->result();
        if($data)
        {
           return $data;
        }
      }

      return false;
  }
 

}

