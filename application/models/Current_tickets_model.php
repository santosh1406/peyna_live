<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Current_Tickets_model extends MY_Model
{
    /**
     * Instanciar o CI
     */
    var $table    = 'current_tickets';
//    var $fields	= "ticket_no,ticket_no,pnr_no,ticket_type,request_type,ticket_status,bus_service_no,dept_time,boarding_time,alighting_time,from_stop_cd,till_stop_cd,boarding_stop_cd,destination_stop_cd,num_passgr,tot_fare_amt,cancellation_rate,ticket_ref_no,issue_time,pay_id,session_no,op_name,guid,user_email_id,mobile_no,dept_date,inserted_date,inserted_by,status";
    var $fields		= array("ticket_id","ticket_no","current_booking_bus_sel_id","pnr_no","ticket_type","request_type","bus_type","ticket_status","bus_service_no","dept_time","boarding_time","alighting_time","from_stop_cd","till_stop_cd", "from_stop_name","till_stop_name" ,"boarding_stop_cd","destination_stop_cd","num_passgr","tot_fare_amt","cancellation_rate","ticket_ref_no","seat_no","issue_time","pay_id","session_no","op_name","guid","user_email_id","mobile_no","dept_date","inserted_date","inserted_by","status");
    var $key    = 'ticket_id';
    
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
    function get_search($data,$from,$to)
    {        
        $curr_date=date('Y-m-d');
        $this->db->select("distinct tm.op_name,tm.ticket_ref_no,tm.ticket_no,tm.dept_time,tm.dept_time,td.psgr_name,tm.ticket_ref_no,tm.from_stop_cd,tm.till_stop_cd,tm.pnr_no,td.psgr_age,td.psgr_sex,tm.num_passgr,tot_fare_amt",false);
        $this->db->from($this->table."  tm");
        $this->db->join("users u", "tm.inserted_by =u.user_id", "left");
        $this->db->join("ticket_details td", "td.ticket_no = tm.ticket_no ", "inner");
        $this->db->where("u.email",$data['email_id']);
        $this->db->where('tm.dept_time >=' , $curr_date);
        $this->db->limit($to,$from);
        $this->db->where("tm.status",'Y');
        $this->db->where("tm.ticket_type <>",'X');
        $this->db->group_by("tm.ticket_no");
     
        $query = $this->db->get();
    //show($this->db->last_query(),1);
        return $query->result_array();

    }
    
    
     function getpass_model($tkt_ref) {
       $this->db->select("tkt.ticket_ref_no,tkt.ticket_no,tkt.pnr_no,tkt.ticket_no,tde.psgr_name,tde.psgr_age,tde.psgr_sex,tde.seat_no,tde.fare_amt,tde.ticket_det_id", false);
       $this->db->from("tickets tkt");
       $this->db->join("ticket_details tde", "tde.ticket_no = tkt.ticket_no and tde.status='Y'", "inner");
       $this->db->where("tkt.ticket_ref_no",$tkt_ref);
       $this->db->where("tkt.status",'Y');
       $query = $this->db->get();
    //  show($this->db->last_query(),1);
       return $query->result_array();

    }

    function getcount($data) {
    
            
        $curr_date=date('Y-m-d');
        $this->db->select("distinct tm.ticket_ref_no,tm.ticket_no,tm.dept_time,tm.dept_time,td.psgr_name,tm.ticket_ref_no,tm.from_stop_cd,tm.till_stop_cd,tm.pnr_no,td.psgr_age,td.psgr_sex,tm.num_passgr,tot_fare_amt",false);
        $this->db->from($this->table." tm");
        $this->db->join("users u", "tm.inserted_by =u.user_id", "left");
        $this->db->join("ticket_details td", "td.ticket_no = tm.ticket_no ", "inner");
        $this->db->where("u.email",$data['email_id']);
        $this->db->where('tm.dept_time >=' , $curr_date);
        $this->db->where("tm.status",'Y');
        $this->db->group_by("tm.ticket_no");
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    
          
    function cancel_booking($update_travel_data) {
       // show($update_travel_data,1);
           if(isset($update_travel_data) && count($update_travel_data) > 0){
            $this->db->select('*');
            $this->db->from('tickets t');
            $this->db->where('t.status','Y');
            $this->db->where('td.status','Y');
            $this->db->where('t.pnr_no',$update_travel_data['pnr_no']);
            $this->db->join('ticket_details td','t.ticket_no=td.ticket_no',"inner");
            $query = $this->db->get();
            $data = $query->result_array();
            $update = array();
            //echo count($data);
          //  show($this->db->last_query(),1);
            if(count($data) > 0)
            {
                $update = array();
                $update_data = array(
                'status' => 'N' );

                $this->db->where('ticket_no', $update_travel_data['ticket_no']);
                $this->db->where_in('seat_no', explode(',',$update_travel_data['var_seat_no']));
                $this->db->update('ticket_details  ', $update_data); 
            }
       // show($this->db->last_query());
                if(count($data) ==1){
                    $update = array();
                    $mast_update_data = array(
                    'status' => 'N',
                    'ticket_status'=>'D');

                    $this->db->where('pnr_no',$update_travel_data['pnr_no']);

                    $this->db->update('tickets  ', $mast_update_data); 
                }
            
            

        }
    }
    
    function current_booking_data($data){
       $this->db->select("tkt.*,tkt.from_stop_cd,tkt.till_stop_cd", false);
       $this->db->from("current_tickets tkt");
       //$this->db->join("current_booking_bus_selection cb", "cb.bus_service_no=tkt.bus_service_no", "inner");
      // $this->db->join("routes r", " cb.route_no=r.route_no", "inner");
       $this->db->where("tkt.ticket_id",$data);
      
       $this->db->where("tkt.status",'Y');
       $query = $this->db->get();
    
       $data = $query->result_array();
       return $data;

    }
    
     /*
     * @Author      : Suraj Rathod
     * @function    : get_ticket_detail()
     * @param       : $user_id -> Id of user
     * @detail      : return booked ticket detail by specified user id.
     *                 
     */

    function get_ticket_detail($user_id, $id) {
        if ($user_id != "") {
            $this->db->select('cs.dsa_print,t.ticket_no,t.num_passgr,t.tot_fare_amt,t.ticket_ref_no,t.pnr_no,t.status as ticket_status,
                           t.user_email_id,t.mobile_no,t.boarding_stop_cd,t.destination_stop_cd,t.ticket_ref_no,t.current_booking_bus_sel_id
                           ');
            $this->db->from('current_tickets t');
            $this->db->join('current_booking_bus_selection cs', 't.current_booking_bus_sel_id = cs.id', "inner");
            $this->db->where('t.inserted_by', $user_id);
            $this->db->where('t.current_booking_bus_sel_id', $id);
            //$this->db->group_by("t.ticket_no,t.status");
            //current_booking_bus_selection
            $query = $this->db->get();
            
            $data = $query->result_array();
            return $data;
        }
    }

    /*function get_ticket_detail($user_id) {
        if ($user_id != "") {
            $this->db->select('t.ticket_no,t.num_passgr,t.tot_fare_amt,t.ticket_ref_no,t.pnr_no,t.status as ticket_status,td.status as ticket_detail_status,
                           t.user_email_id,t.mobile_no,t.boarding_stop_cd,t.destination_stop_cd,t.ticket_ref_no,
                           td.psgr_name,td.psgr_age,td.psgr_sex,td.fare_amt,td.seat_no');
            $this->db->from('current_tickets t');
            $this->db->join('current_ticket_details td', 't.ticket_no = td.ticket_no', "inner");
            $this->db->where('t.inserted_by', $user_id);
            //$this->db->group_by("t.ticket_no,t.status");
            $query = $this->db->get();
            
            $data = $query->result_array();
            return $data;
        }
    }
    */



    function current_booking_tot_psgr_data($saved_data){
     //  $this->db->select("tkt.till_stop_name,tkt.from_stop_name,td.psgr_age,td.psgr_no,om.op_stop_name as from_name,om1.op_stop_name as till_name", false);
      $this->db->select("tkt.*,td.*,usr.display_name");
        $this->db->from("current_tickets tkt");
       //$this->db->join("op_stop_master om", "om.op_stop_cd=tkt.from_stop_cd", "inner");
       //$this->db->join("op_stop_master om1", "om1.op_stop_cd=tkt.till_stop_cd", "inner");
       $this->db->join("current_ticket_details td", "tkt.ticket_no=td.ticket_no", "inner");
       $this->db->join("users usr", "tkt.inserted_by=usr.user_id", "inner");
       $this->db->where("tkt.ticket_no",$saved_data);
      // $this->db->where("om.op_id",$op_id);
      // $this->db->where("om1.op_id",$op_id);
      // $this->db->where("tkt.status",'Y');
       $query = $this->db->get();
     // show($this->db->last_query(),1);
       $data = $query->result_array();
       return $data;
    }
      
	
}
