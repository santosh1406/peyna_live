<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of users
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Cb_assign_machine_agent_model extends MY_Model {
    
    var $table  = 'cb_assign_machine_agent';
    var $fields = array("id","machine_master_id", "user_id", "is_status", "is_deleted","created_by","created_date","updated_by","updated_date");
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

   
    
}
