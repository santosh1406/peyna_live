<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rokad_topup_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function LoadRokadDataTable($sWhere, $sOrder, $sLimit) {
        $lcSqlsStr = "SELECT id,DATE_FORMAT(created_date,'%d-%m-%Y %H:%i:%s') as Date,amount,topup_by,transaction_remark,is_approvel,user_id,request_touserid from wallet_topup ";
        $lcSqlsStr .= "$sWhere $sOrder $sLimit";
		//echo $lcSqlsStr;
        $query = $this->db->query($lcSqlsStr);

        $data['ResultSet'] = $query->result_array();

        $data['iFilteredTotal'] = count($data['ResultSet']);

        /* Total data set length */
        $sQuery = " SELECT COUNT(id) as countRid  FROM wallet_topup $sWhere";
        $query = $this->db->query($sQuery);
        $ResultSet = $query->result_array();
        $data['iTotal'] = $ResultSet[0]['countRid'];
        return $data;
    }
	
	public function getRokadRequestData($id){
		$sQuery="SELECT request_touserid,user_id,amount,topup_by,bank_name,bank_acc_no,transaction_no,transaction_date,transaction_remark,transaction_status FROM wallet_topup WHERE id =".$id."";
        $query = $this->db->query($sQuery);
        return $query->row(); 
	}
	
    public function LoadRokadApprovalDataTable($sWhere, $sOrder, $sLimit) {
        $lcSqlsStr = "SELECT id,DATE_FORMAT(created_date,'%d-%m-%Y %H:%i:%s') as Date,round(amount,2) as amount,user_id,request_touserid,topup_by,transaction_remark,is_approvel FROM wallet_topup ";
        $lcSqlsStr .= "$sWhere $sOrder $sLimit";
		//echo $lcSqlsStr;
        $query = $this->db->query($lcSqlsStr);

        $data['ResultSet'] = $query->result_array();

        $data['iFilteredTotal'] = count($data['ResultSet']);

        /* Total data set length */
        $sQuery = " SELECT COUNT(id) as countRid  FROM wallet_topup $sWhere";
        $query = $this->db->query($sQuery);
        $ResultSet = $query->result_array();
        $data['iTotal'] = $ResultSet[0]['countRid'];
        return $data;
    }

	public function getRokadRequestApprovalData($id){
		$sQuery="SELECT a.id,DATE_FORMAT(a.created_date,'%d-%m-%Y %H:%i:%s') as Date,a.request_touserid,a.amount,a.user_id,a.topup_by,a.transaction_remark,a.transaction_no,a.bank_name,a.bank_acc_no,a.is_approvel,a.reject_reason,b.amt FROM wallet_topup a LEFT JOIN wallet b ON a.request_touserid=b.user_id WHERE a.id =".$id."";
        $query = $this->db->query($sQuery);
        return $query->row();
	}
}
?>