<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Agent
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cancellation_charges_model extends MY_Model {
    
    var $table  = 'cancellation_charges';
    var $fields = array("id","op_id","type","cancel_rate","from","till","effective_from","effective_till");
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }


    public function cancellation_charges($op_id)
    {
    	$data = array();
    	
    	if($op_id)
    	{
	        $this->db->select("cc.*,ccd.note as note");
	        $this->db->from("cancellation_charges cc");
	        $this->db->join("cancellation_charges_detail ccd", "ccd.op_id = cc.op_id", "left");
	        $this->db->where("cc.op_id", $op_id);
	        $query = $this->db->get();
	        $data = $query->result();
    	}

    	return $data;
    }

}

// End of Agent_model class