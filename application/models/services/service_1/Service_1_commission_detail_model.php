<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Service_1_commission_detail_model extends MY_Model {

    protected $table = 'commission_detail';
    var $fields = array("id", "commission_id", "base_commission_id", "provider_id", "provider_operator_id", "type", "base_rate_flag", "total_commission", "agent_commission", "fixed_base", "percentage_base", "partner_fixed_value", "partner_percentage_value", "additional_base_commission");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
    }

}
