<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Service_1_commission_model extends MY_Model {

    protected $table = '1_commission_master';
    var $fields = array("id", "name", "from", "till", "service_tax", "commission_with_service_tax", "commission_default_flag", "commission_type", "created_at", "updated_at", "created_by", "updated_by", "status");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
        $this->load->model(array('services/service_1/service_1_commission_model'));
    }

    //used in commission view
    public function commission_detail_by_id($commission_id) {
        $this->db->select("
                           c.id AS commission_id,
                           c.name AS commission_name,
                           c.from AS commission_from,
                           c.till AS commission_till,
                           c.commission_with_service_tax,
                           c.commission_default_flag,
                           c.service_tax,
                           cd.id AS commission_detail_id,
                           cd.base_commission_id,
                           cd.op_id as operator_id,
                           cd.provider_operator_id AS provider_operator_id,
                           cd.provider_id AS provider_id,
                           p.name AS provider_name,
                           o.op_name AS provider_operator_name,
                           cd.base_rate_flag,
                           cd.percentage_base as base_perc,
                           cd.fixed_base as base_fixed,
                           (case when bc.bus_type=0 then 'All' when bc.bus_type=2 then 'NonAC' else bt.name end) bus_type,
                           cd.rookad_commission,
                           cd.md_commission,
                           cd.ad_commission,
                           cd.dist_commission,
                           cd.retailer_commission
                          ");
        $this->db->from("commission_detail cd");
        $this->db->where("cd.commission_id", $commission_id);
        $this->db->join("commission c", "c.id = cd.commission_id ", "inner");
        $this->db->join("base_commission bc", "bc.id = cd.base_commission_id", "inner");
        $this->db->join("api_provider p", "p.id = cd.provider_id", "inner");
        $this->db->join("api_provider_operator po", "po.id = cd.provider_operator_id", "left");
        $this->db->join("op_master o", "o.op_id = po.op_id", "left");
        $this->db->join("bus_type_home bt", "bc.bus_type = bt.id", "left");
        $this->db->order_by("o.op_id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    //used in commission insert
    public function insert_slab_wise_commission($data = array()) {
        $ret = true;
        $provider_op_array = array_merge($data['ets_details'], $data['its_details'], $data['details']);
        if (!empty($provider_op_array)) {
            foreach ($provider_op_array as $operators => $operator_value) {

                foreach ($operator_value as $key => $value) {

                    $parset2 = implode("','", $value);
                    $parset = "'" . $data['commission_id'] . "','" . $parset2 . "'";
                    $this->db->trans_begin();
                    $s = "insert into 1_commission_detail (commission_id,base_commission_id,op_id,provider_id,provider_operator_id,base_rate_flag,percentage_base,fixed_base,trimax_commission,company_commission,md_commission,ad_commission,dist_commission,retailer_commission) values(" . $parset . ")";
                    $this->db->query($s);
                }
            }

            $this->db->trans_commit();
        }

        return $ret;
    }

    //commision id to insert in users table
    public function commission_id() {
        $date = date("Y-m-d");
        $this->db->select('id,name,created_at,status,from,till');
        $this->db->from("commission");
        $this->db->where("DATE_FORMAT(`from`, '%Y-%m-%d') <= ", date('Y-m-d', strtotime($date)));
        $this->db->where("DATE_FORMAT(till, '%Y-%m-%d') >= ", date('Y-m-d', strtotime($date)));
        $this->db->where('status', '1');
        $this->db->where('commission_default_flag', '1');
        $this->db->order_by('id', 'desc');
        $this->db->limit('1');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function get_active_commission($input)
    {
        $date = date("Y-m-d");
        $this->db->select('agent_commission_template_log.commission_id as active_commission_id');
        $this->db->from('agent_commission_template_log');        
        $this->db->join("commission c", "c.id = agent_commission_template_log.commission_id", "left");
        $this->db->where("user_id",$input);
        $this->db->where("agent_commission_template_log.status","1");
        $this->db->where("DATE_FORMAT(commission_from, '%Y-%m-%d') <= ", date('Y-m-d', strtotime($date)));
        $this->db->where("DATE_FORMAT(commission_to, '%Y-%m-%d') >= ", date('Y-m-d', strtotime($date)));
        $this->db->order_by("agent_commission_template_log.id", "desc");
        $this->db->limit('1');
        $query = $this->db->get();
        return $query->result();
    }

}
