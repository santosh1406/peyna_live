<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Service_5_commission_detail_model extends MY_Model {

    protected $table = '5_commission_detail';
    var $fields = array("id", "commission_id", "trimax_commission", "company_commission", "md_commission", "ad_commission", "dist_commission", "retailer_commission");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
    }

}
