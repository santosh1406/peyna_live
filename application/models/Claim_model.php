<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Claim_model extends MY_Model
{
	function __construct() {
		parent::__construct();
	}

    public function saveClaimData($input) {
        $data = array(
            'cust_card_id' => $input['cust_card_id'],
            'mobile_number' => $input['mobile_number'],
            'opening_balance' => $input['opening_balance'],
            'closing_balance' => $input['closing_balance'],
            'amount' => $input['amount'],
            'expiry_date' => date('Y-m-d', strtotime($input['expiry_date'])),
            'transaction_id' => $input['transaction_id'],
            'topup_flag' => $input['topup_flag'],
            'terminal_id'=> (isset($input['terminal_id']) ? $input['terminal_id'] : ''),
            'request_id' => isset($input['requestId']) ? $input['requestId'] : '',
            'txn_ref_no' => isset($input['txnRefNo']) ? $input['txnRefNo'] : '',
            'pos_request_id' => isset($input['posRequestId']) ? $input['posRequestId'] : '',
            'pos_txn_ref_no' => isset($input['posTxnRefNo']) ? $input['posTxnRefNo'] : '',
            'session_id'=> $input['session_id'],
            'created_by' => $input['apploginuser'],
            'is_active' => '1'
        );
        $this->db->insert('ps_claim_wallet_details',$data);
        return $this->db->insert_id();
    }

    public function savePassClaimData($input) {
        $data = array(
            'cust_card_id' => $input['cust_card_id'],
            'mobile_number' => $input['mobile_number'],
            'pass_data' => $input['pass_data'],
            'terminal_id'=> (isset($input['terminal_id']) ? $input['terminal_id'] : ''),
            'request_id' => isset($input['requestId']) ? $input['requestId'] : '',
            'txn_ref_no' => isset($input['txnRefNo']) ? $input['txnRefNo'] : '',
            'pos_request_id' => isset($input['posRequestId']) ? $input['posRequestId'] : '',
            'pos_txn_ref_no' => isset($input['posTxnRefNo']) ? $input['posTxnRefNo'] : '',
            'session_id'=> $input['session_id'],
            'created_by' => $input['apploginuser'],
            'is_active' => '1'
        );
        $this->db->insert('ps_claim_pass_details',$data);
        return $this->db->insert_id();
    }
}
