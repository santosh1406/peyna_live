<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Providers_master_model extends MY_Model {

    var $table  = 'providers_master';
    var $fields = array("id","provider_name","created_by","created_at", "deleted_at");
    var $key    = 'id';
   
    public function __construct() {
        parent::__construct();

    }

}
