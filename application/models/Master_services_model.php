<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Master_services_model extends MY_Model
{

    /**
     * Instanciar o CI
     */
    var $table = 'master_services';
    var $fields = array("id","op_id","boarding_stop_id","boarding_stop_name","destination_stop_id","destination_stop_name","trip_no","sch_departure_date","sch_departure_tm","arrival_date","arrival_tm","day","start_stop_sch_departure_date","end_stop_arrival_date","service_effective_from","service_effective_till","tot_seat_available","seat_fare","senior_citizen_fare","child_seat_fare","sleeper_fare","created_date","updated_date","record_status");

    public function __construct()
    {
        parent::__construct();
        $this->_init();
    }

}