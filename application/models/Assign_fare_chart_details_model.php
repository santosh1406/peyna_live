<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Assign_fare_chart_details_model extends MY_Model
{
    /**
     * Instanciar o CI
     */
    var $table = 'assign_fare_chart_details';
    var $fields		   = array("id","route_id","bus_type_id","effective_from","effective_till","chart_id","dayid","status","approve_status");
    var $key    = 'id';  
    
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
  
        
}
