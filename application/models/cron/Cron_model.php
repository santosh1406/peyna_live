<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cron_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();     
        $this->load->model(array('tickets_model','travel_destinations_model','trip_master_model'));
        $this->db = $this->load->database('default', true);
        $this->msrtc_db = $this->load->database('msrtc_app', true);
    }
    
    /*
    *Author   : Suraj Rathod
    *Function : cancelBusService()
    *Detail : To cancel bus services
    */
    
    public function cancelBusService($service_id){

        $where = array("op_trip_no" => $service_id, "status" => "Y");
        $update = array("status" => "N");

        return $this->trip_master_model->update_where($where, $update);
    }


    /*
    *Author : Suraj Rathod
    *Function : cancelTicket()
    *Detail : To cancel ticket
    */
    
    public function cancelTicket($service_id){

        $where = array("bus_service_no" => $service_id, "status" => "Y");
        $update = array("status" => "N");

        return $this->tickets_model->update_where($where, $update);
    }

    /*
    *Author : Suraj Rathod
    *Function : getTicketDetails()
    *Detail : To fetch ticket Detail
    */
    
    public function getTicketDetails($service_id){

        return $this->tickets_model->where(array('bus_service_no' => $service_id))->find_all();
    }
    
    public function get_cancel_refund_data(){
        $yest_date= date('Y-m-d',  strtotime("-1 days"));
        
        $this->db->select('sum(refund_amt) as refund_amt ,t.op_id ,om.user_id,t.inserted_date,t.inserted_by');
        $this->db->join("cancel_ticket_data cd",'cd.ticket_id=t.ticket_id',"inner") ;
         $this->db->join("op_master om",'om.op_id=t.op_id',"inner") ;
        $this->db->where('cd.is_refund','1');
     //   $this->db->where("DATE_FORMAT(t.inserted_date,'%Y-%m-%d')",$yest_date);
        $this->db->where("DATE_FORMAT(t.inserted_date,'%Y-%m-%d')",'2015-08-04');
        
        $this->db->from('tickets t');
        $this->db->group_by('t.op_id');
        $res=$this->db->get();
        return $ref_data=$res->result_array();
    }
    
    public function get_ors_collection_data(){
        $yest_date= date('Y-m-d',  strtotime("-1 days"));
        $this->db->select('sum(tot_fare_amt) as refund_amt ,t.op_id ,om.user_id,t.inserted_date,t.inserted_by');
      //  $this->db->where("DATE_FORMAT(t.inserted_date,'%Y-%m-%d')",$yest_date);
         $this->db->join("op_master om",'om.op_id=t.op_id',"inner") ;
        $this->db->where("DATE_FORMAT(t.inserted_date,'%Y-%m-%d')",'2015-08-04');
        $this->db->where("t.transaction_status",'success');
        $this->db->where("t.status",'N');
        $this->db->from('tickets t');
        $this->db->group_by('t.op_id');
        $res=$this->db->get();
       return  $ref_data=$res->result_array();
         
    }
    
    public function get_current_booking_collection_data(){
        $yest_date = date('Y-m-d',  strtotime("-1 days"));
        $this->db->select('sum(total_amount) as refund_amt ,w.OP_ID as op_name,w.DUTY_DT,om.user_id');
        $this->db->join("waybillprogramming w",'w.waybill_no=t.waybill_no',"inner") ;
       // $this->db->where("DATE_FORMAT(w.COLLECTION_TM,'%Y-%m-%d')",$yest_date);
        $this->db->join("op_master om",'om.op_name=w.OP_ID',"inner") ;
        $this->db->where("DATE_FORMAT(w.COLLECTION_TM,'%Y-%m-%d')",'2015-08-08');
        $this->db->from('ticket_data t');
        $this->db->group_by('w.OP_ID');
        $res=$this->db->get();
       return  $ref_data=$res->result_array();
         
    }
    public function getCommissionData()
    {
        $yest_date= date('Y-m-d',  strtotime("-1 days"));
        $this->msrtc_db->select('*');
        // $this->msrtc_db->where("DATE_FORMAT(created_date,'%Y-%m-%d')",$yest_date);
        $this->msrtc_db->where("status","N");
        $this->msrtc_db->from('cb_commission_detail');
        $res=$this->msrtc_db->get();
        return  $ref_data=$res->result_array();
    }
    public function getMonthlyCommData($user_id,$role_id)
    {
        $Today = date('Y-m');
        $last_month = date("Y-m",strtotime("-1 month"));
        if($role_id)
        {
            if($role_id == TRIMAX_ROLE_ID){
             $this->db->select("sum(trimax_comm) as comm_amount");
             $this->db->select("sum(trimax_actual_comm) as comm_gst_tds_amount");
            }elseif ($role_id == COMPANY_ROLE_ID) {
              $this->db->select("sum(company_comm) as comm_amount" );
              $this->db->select("sum(company_actual_comm) as comm_gst_tds_amount");
            }elseif ($role_id == MASTER_DISTRIBUTOR_ROLE_ID) {
              $this->db->select("sum(md_comm) as comm_amount");
              $this->db->select("sum(md_actual_comm) as comm_gst_tds_amount");
            }elseif ($role_id == AREA_DISTRIBUTOR_ROLE_ID) {
               $this->db->select("sum(ad_comm) as comm_amount");
               $this->db->select("sum(ad_actual_comm) as comm_gst_tds_amount");
            }elseif ($role_id == DISTRIBUTOR) {
               $this->db->select("sum(di_comm) as comm_amount");
               $this->db->select("sum(di_actual_comm) as comm_gst_tds_amount");
            }elseif ($role_id == RETAILER_ROLE_ID) {
               $this->db->select("sum(agent_comm) as comm_amount");
               $this->db->select("sum(agent_actual_comm) as comm_gst_tds_amount");
            }  
        }
        $this->db->from("cb_commission_distribution");
        // $this->db->where("DATE_FORMAT(created_at,'%Y-%m') =",$last_month); 
        if($role_id)
        {
            if($role_id == TRIMAX_ROLE_ID){
             $this->db->where("trimax_id =",$user_id);  
             $this->db->where("comm_distrubute_trimax =","N");
    
            }elseif ($role_id == COMPANY_ROLE_ID) {
              $this->db->where("company_id =",$user_id); 
              $this->db->where("comm_distrubute_company =","N");

            }elseif ($role_id == MASTER_DISTRIBUTOR_ROLE_ID) {
              $this->db->where("md_id =",$user_id);
              $this->db->where("comm_distrubute_md =","N");

            }elseif ($role_id == AREA_DISTRIBUTOR_ROLE_ID) {
               $this->db->where("ad_id =",$user_id);
               $this->db->where("comm_distrubute_ad =","N");

            }elseif ($role_id == DISTRIBUTOR) {
               $this->db->where("di_id =",$user_id);
               $this->db->where("comm_distrubute_di =","N");

            }elseif ($role_id == RETAILER_ROLE_ID) {
               $this->db->where("agent_id =",$user_id);
               $this->db->where("comm_distrubute_agent =","N");
            }  
        }             
        $user_result = $this->db->get();
        //show($this->db->last_query(),1);
        //log_data('msrtc/monthly_commission_Distribution/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/commission_record'.'.log',$this->db->last_query(),'Wallet transaction Array');    

        $user_result_array = $user_result->result_array();
        return $user_result_array;
    }

    public function monthlyCommissionDetail($inserted_data) {
        $this->db->insert('cb_monthly_commission_detail',$inserted_data);
    }
    
    public function UpdateCommStatus($id)
    {
        $data = array('status' => 'Y'); 
        $this->msrtc_db->where('id', $id);
        if($this->msrtc_db->update('cb_commission_detail', $data))
        {
           return true;
        } 
        else
        {
           return false;
        } 
    }

     public function getTicketCommissionData()
    {
        $yest_date= date('Y-m-d',  strtotime("-1 days"));
        $this->msrtc_db->select('*');
        // $this->msrtc_db->where("DATE_FORMAT(created_date,'%Y-%m-%d')",$yest_date);
        $this->msrtc_db->where("ticket_comm_status","N");
        $this->msrtc_db->from('cb_ticket_wise_commission_detail');
        $res=$this->msrtc_db->get();
        return  $ref_data=$res->result_array();
    }

    public function UpdateTicketCommStatus($id)
    {
        $data = array('ticket_comm_status' => 'Y'); 
        $this->msrtc_db->where('id', $id);
        if($this->msrtc_db->update('cb_ticket_wise_commission_detail', $data))
        {
           return true;
        } 
        else
        {
           return false;
        } 
    }


    public function getMonthlyTicketwiseCommData($user_id,$role_id)
    {
        $Today = date('Y-m');
        $last_month = date("Y-m",strtotime("-1 month"));
        if($role_id)
        {
            if($role_id == TRIMAX_ROLE_ID){
             $this->db->select("sum(trimax_comm) as comm_amount");
             $this->db->select("sum(trimax_actual_comm) as comm_gst_tds_amount");
            }elseif ($role_id == COMPANY_ROLE_ID) {
              $this->db->select("sum(company_comm) as comm_amount" );
              $this->db->select("sum(company_actual_comm) as comm_gst_tds_amount");
            }elseif ($role_id == MASTER_DISTRIBUTOR_ROLE_ID) {
              $this->db->select("sum(md_comm) as comm_amount");
              $this->db->select("sum(md_actual_comm) as comm_gst_tds_amount");
            }elseif ($role_id == AREA_DISTRIBUTOR_ROLE_ID) {
               $this->db->select("sum(ad_comm) as comm_amount");
               $this->db->select("sum(ad_actual_comm) as comm_gst_tds_amount");
            }elseif ($role_id == DISTRIBUTOR) {
               $this->db->select("sum(di_comm) as comm_amount");
               $this->db->select("sum(di_actual_comm) as comm_gst_tds_amount");
            }elseif ($role_id == RETAILER_ROLE_ID) {
               $this->db->select("sum(agent_comm) as comm_amount");
               $this->db->select("sum(agent_actual_comm) as comm_gst_tds_amount");
            }  
        }
        $this->db->from("cb_ticketwise_commission_distribution");
        // $this->db->where("DATE_FORMAT(created_at,'%Y-%m') =",$last_month); 
        if($role_id)
        {
            if($role_id == TRIMAX_ROLE_ID){
             $this->db->where("trimax_id =",$user_id);  
             $this->db->where("comm_distrubute_trimax =","N");
    
            }elseif ($role_id == COMPANY_ROLE_ID) {
              $this->db->where("company_id =",$user_id); 
              $this->db->where("comm_distrubute_company =","N");

            }elseif ($role_id == MASTER_DISTRIBUTOR_ROLE_ID) {
              $this->db->where("md_id =",$user_id);
              $this->db->where("comm_distrubute_md =","N");

            }elseif ($role_id == AREA_DISTRIBUTOR_ROLE_ID) {
               $this->db->where("ad_id =",$user_id);
               $this->db->where("comm_distrubute_ad =","N");

            }elseif ($role_id == DISTRIBUTOR) {
               $this->db->where("di_id =",$user_id);
               $this->db->where("comm_distrubute_di =","N");

            }elseif ($role_id == RETAILER_ROLE_ID) {
               $this->db->where("agent_id =",$user_id);
               $this->db->where("comm_distrubute_agent =","N");
            }  
        }             
        $user_result = $this->db->get();
        $user_result_array = $user_result->result_array();
        return $user_result_array;
    }

    public function monthlyTicketwiseCommissionDetail($inserted_data) {
        $this->db->insert('cb_monthly_ticketwise_commission_detail',$inserted_data);
    }
    
 
}//End OF Class
?>
