<?php
class Payment_gateway_key_model extends MY_Model
{
    var $table = 'payment_gateway_key';
    var $fields = array("id","ticket_id","ticket_ref_no","rsa_key_for","user_id","pg_name","key","inserted_date","updated_date","ip_address","user_agent");
    var $key = 'id';

    public function __construct() 
    {
        parent::__construct();
        $this->_init();
    }
}

?>
