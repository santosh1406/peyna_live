<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Newsletter_model
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contactus_model extends MY_Model {
    
    var $table  = 'contactus';
    var $fields = array("id","name","email","message","ip_address","user_agent","timestamp");
    var $key    = 'id';


    public function __construct() {
        parent::__construct(); 
    }
}
