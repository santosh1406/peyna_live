<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Current_booking_bus_selection_model extends MY_Model {

    var $table = 'current_booking_bus_selection';
    var $fields = array("id", "user_id", "op_id", "trip_no", "dept_date", "dept_time", "route_no", "bus_no", "bus_type_id", "is_available");
    // var $fields = "id,w_id,user_id,amt,status,expire_at,comment,added_by,added_on";
    var $key = 'id';

    public function __construct() {
        parent::__construct();
        $this->db2 = $this->load->database('upsrtc', TRUE);
    }

    public function save_bus() {
        $user_id = $this->session->userdata('user_id');
        $op_id = $this->session->userdata('op_id');
        $input = $this->input->post();
        if (preg_match("/-/", $input['boarding_stop_id'])) {
            $boarding_stop_cd = explode('-', $input['boarding_stop_id']);
            $boarding_stop_cd = trim($boarding_stop_cd[1]);
        } else {
            $boarding_stop_cd = $input['boarding_stop_id'];
        }
        $boarding_stop_id = $this->up_bus_stops_model->get_boarding_stop_id($boarding_stop_cd);
        $bus_service_no = explode(',', $input['bus_service_no']);
        $this->db2->select('bs.BUS_SERVICE_NO,bs.ROUTE_NO,bs.BUS_TYPE_CD,bs.SCHEDULED_TM,DATE_FORMAT(SCHEDULED_TM, "%Y-%m-%d") DATEONLY, DATE_FORMAT(SCHEDULED_TM,"%H:%i:%s") TIMEONLY,bs.TRIP_NO,bt.BUS_TYPE_ID');
        $this->db2->from('bus_services bs');
        $this->db2->join("bus_types bt", "bt.BUS_TYPE_CD = bs.BUS_TYPE_CD", "inner");
        $this->db2->where_in('BUS_SERVICE_NO', $bus_service_no);
        $query = $this->db2->get();
        $res = $query->result_array();

        foreach ($res as $r) {
            $insert_array = array(
                'user_id' => $user_id,
                'bus_service_no' => $r['BUS_SERVICE_NO'],
                'op_id' => $op_id,
                'trip_no' => $r['TRIP_NO'],
                'dept_date' => $r['DATEONLY'],
                'dept_time' => $r['TIMEONLY'],
                'route_no' => $r['ROUTE_NO'],
                'bus_type_id' => $r['BUS_TYPE_ID'],
                'boarding_stop_id' => $boarding_stop_id,
                'is_available' => 'Y'
            );
            $insert[] = $insert_array;
        }
        $this->db->insert_batch('current_booking_bus_selection', $insert);
        $cnt = $this->db->affected_rows();

        $data = array("boarding_stop_id" => $boarding_stop_cd);
        $this->db->where_in('user_id', $user_id);
        $this->db->update('user_login_count', $data);
        return $cnt;
    }

    function check_existing_bus_services($input) {

        $schedule_tm = explode(" ", $input['schedule_tm']);
        $op_id = $this->session->userdata('op_id');
        $date = $schedule_tm[0];
        $time = $schedule_tm[1];
        $this->db->select('op_id,trip_no,bus_service_no,route_no,dept_date,dept_time');
        $this->db->from('current_booking_bus_selection');

        $this->db->where('op_id', $op_id);
        $this->db->where('bus_type_id', $input['bus_type_id']);

        $this->db->where('route_no', $input['route_no']);
        $this->db->where('trip_no', $input['trip_no']);
        $this->db->where('bus_service_no', $input['bus_service_no']);
        $this->db->where('dept_date', $date);
        $this->db->where('dept_time', $time);
        $query = $this->db->get();

        $data = $query->result_array();
        return count($data);
    }

    function save_buses_byroutes() {

        $input = $this->input->post();
        $user_id = $this->session->userdata('user_id');
        $op_id = $this->session->userdata('op_id');
        $boarding_stop_cd = $this->session->userdata('boarding_stop_id');
        $boarding_stop_id = $this->up_bus_stops_model->get_boarding_stop_id($boarding_stop_cd);

        $old_dept_date = $input['deptdate'];
        // $dept_date = date("Y-m-d", strtotime($dept_date));

        $dateArr = explode("/", $old_dept_date);
        $timeStamp = mktime(0, 0, 0, $dateArr[1], $dateArr[0], $dateArr[2]);
        $dept_date = date("Y-m-d", $timeStamp);

        $dept_time = $input['depttime'];
        $route_no = $input['route'];
        $bus_type_id = $input['bus_type'];
        $boarding_stop_id = $boarding_stop_id;
        $is_available = 'Y';

        $data_to_insert = array('user_id' => $user_id, 'op_id' => $op_id,
            'dept_date' => $dept_date, 'dept_time' => $dept_time,
            'route_no' => $route_no, 'bus_type_id' => $bus_type_id,
            'boarding_stop_id' => $boarding_stop_id, 'is_available' => $is_available);

        $data_to_compare = array('op_id' => $op_id,
            'dept_date' => $dept_date, 'dept_time' => $dept_time,
            'route_no' => $route_no, 'bus_type_id' => $bus_type_id,
            'boarding_stop_id' => $boarding_stop_id);

        $data = array("boarding_stop_id" => $boarding_stop_cd);
        $this->db->where_in('user_id', $user_id);
        $this->db->update('user_login_count', $data);

        $check_existing_bus_service = $this->check_existing_bus_service_byroute($data_to_compare,$data_to_insert);
        return $check_existing_bus_service;
    }

    function check_existing_bus_service_byroute($data_to_compare,$data_to_insert) {
        $op_id = $this->session->userdata('op_id');
        $this->db->select('op_id,route_no,dept_date,dept_time');
        $this->db->from('current_booking_bus_selection');

        $this->db->where('op_id', $op_id);
        $this->db->where('bus_type_id', $data_to_compare['bus_type_id']);

        $this->db->where('route_no', $data_to_compare['route_no']);
        $this->db->where('dept_date', $data_to_compare['dept_date']);
        $this->db->where('dept_time', $data_to_compare['dept_time']);
        $query = $this->db->get();
        $data = $query->result_array();
        $cnt = count($data);
        if ($cnt >= 1) {
            $data_status['exits'] = 'exits';
        } else {
            $this->db->insert("current_booking_bus_selection", $data_to_insert);
            $data_status['exits'] = 'insert';
        }
         return $data_status;
    }

//    function check_dept_time_interval() {
//        $input = $this->input->post();
//        $op_id = $this->session->userdata('op_id');
//        $this->db->select('op_id,route_no,dept_date,dept_time');
//        $this->db->from('current_booking_bus_selection');
//        $dept_date = date("Y-m-d", strtotime($input['deptdate']));
//        $this->db->where('op_id', $op_id);
//        $this->db->where('bus_type_id', $input['bus_type']);
//        $this->db->where('route_no', $input['route']);
//        $this->db->where('dept_date', $dept_date);
//        $this->db->where('dept_time', $input['depttime']);
//        $query = $this->db->get();
//        $data = $query->result_array();
//        $cnt = count($data);
//        return $cnt;
//    }

    


    public function get_routes_stops($data) {
        $this->db2->select('r.ROUTE_NO,rs.BUS_STOP_NM as bus_stop_name,rs.BUS_STOP_CD as bus_stop_cd');
        $this->db2->from('routes r');

        $this->db2->join('route_stops rs', 'r.ROUTE_NO=rs.ROUTE_NO', "inner");
        $this->db2->where('r.ROUTE_NO', $data['route_no']);
        $this->db->order_by('rs.STOP_SEQ', 'asc');
        $query = $this->db2->get();
        //  show($this->db2->last_query(),1);
        // $this->db2->where('rs.status', 'Y');
        return $data = $query->result_array();
    }

    public function select_current_booking($user_id) {

        $this->db->select('cb.*,cb.bus_type_id,om.op_name,cb.bus_service_no');
        $this->db->from('current_booking_bus_selection cb');
        $this->db->join('op_master om', 'om.op_id=cb.op_id', "inner");
        $this->db->where('dsa_print','N');
      
        $this->db->where('cb.user_id', $user_id);
       
        $query = $this->db->get();
       
        $data = $query->result_array();
        if (count($data) > 0) {
            // show($data,1);
            foreach ($data as $key => $value):
                $this->db2->distinct("");
                $this->db2->select('r.ROUTE_NO,r.FROM_STOP_CD,r.TILL_STOP_CD,rs.BUS_STOP_NM as from_stop,rs1.BUS_STOP_NM as end_stop,bt.BUS_TYPE_ID,bt.BUS_TYPE_CD,bt.BUS_TYPE_NM');
                $this->db2->from('routes r');
                $this->db2->join('bus_types bt', 'bt.BUS_TYPE_ID=' . $value['bus_type_id'], "inner");
                $this->db2->join('route_stops rs', 'rs.BUS_STOP_CD=r.FROM_STOP_CD', "inner");
                $this->db2->join('route_stops rs1', 'rs1.BUS_STOP_CD=r.TILL_STOP_CD', "inner");
                $this->db2->where('r.ROUTE_NO', $value['route_no']);
                // $this->db2->order_by('STOP_SEQ', 'asc');
                $query2 = $this->db2->get();
                //    show($this->db2->last_query(),1);
                $server_data[] = $query2->result_array();
            endforeach;


            foreach ($server_data as $key => $value) {

                $act_server_data[$value[0]['ROUTE_NO']] = array($value[0]['FROM_STOP_CD'],
                    $value[0]['TILL_STOP_CD'], $value[0]['from_stop'], $value[0]['end_stop'], $value[0]['BUS_TYPE_ID'],
                    $value[0]['BUS_TYPE_CD'], $value[0]['BUS_TYPE_NM']);
            }
            foreach ($data as $key => $value) {
                //  show($value,1);
                if (isset($act_server_data[$value['route_no']])) {
                    $local_server_data = array();
                }

                $local_data[$value['id']] = array(
                    'id' => $value['id'],
                    'route_no' => $value['route_no'],
                    'bus_no' => $value['bus_no'],
                    'bus_type_id' => $act_server_data[$value['route_no']]['4'],
                    'op_name' => $value['op_name'],
                    'bus_type_name' => $act_server_data[$value['route_no']]['6'],
                    'bus_category' => $act_server_data[$value['route_no']]['5'],
                    'op_id' => $value['op_id'],
                    'dept_date' => $value['dept_date'],
                    'dept_time' => $value['dept_time'],
                    'bus_service_no' => $value['bus_service_no'],
                    'from_stop' => $act_server_data[$value['route_no']]['0'],
                    'end_stop' => $act_server_data[$value['route_no']]['1'],
                    'from_stop_name' => $act_server_data[$value['route_no']]['2'],
                    'end_stop_name' => $act_server_data[$value['route_no']]['3']);
            }
           
            return $local_data;
        }
    }


    public function update_cur_bok_bus_selection($data)
    {
        if($data["id"] != "")
        {
            $this->db->where('id = '.$data["id"]);
            if(isset($data["user_id"]) and $data["user_id"] != "")
                $this->db->where('user_id = '.$data["user_id"]);

            if($this->db->update('current_booking_bus_selection', $data["data"]))
                return true;
            else
                return false;
        }
    }
    
    /*function written by pankaj
     * parameter boarding stop,alighting stop
     * return all buses
     */
    
        function  select_bus()
    {
     
   $this->db->select("cbb.bus_Service_no,concat(rs.from_stop_cd,'-',rs.till_stop_cd) as route_nm,cbb.DEPT_TM,cbb.BUS_TYPE_CD"); 
   $this->db->from('current_booking_bus_selection cbb');
    $this->db->join('bus_services bsn','cbb.bus_service_no=bsn.bus_service_no','inner');
   $this->db->join('bus_service_stops bss','bsn.bus_service_no=bss.bus_service_no','inner');
   $this->db->join('routes rs','bsn.route_no=rs.route_no','inner'); 
   $this->db->where('cbb.booking_status','C');
   $this->db->where('cbb.dsa_print','N');
   
   $this->db->where('cbb.DEPT_TM  > NOW()');
   $this->db->where('bss.IS_BOARDING_STOP=Y');
   $this->db->where('bss.BUS_STOP_CD in(AJM)');
   $res=$this->db->get();
   $res->result_array();
  
        
     }
    
    
    
}
