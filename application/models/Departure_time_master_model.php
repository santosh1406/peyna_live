<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Departure_time_master_model extends MY_Model
{
  
    var $table    = 'departure_time_master';
    var $fields		= array("id", "display_name", "start_time", "end_time",	"is_status", "is_deleted", "created_by", "created_date", "updated_by", "updated_date");
    var $key    = 'id';
    
    public function __construct() {
        parent::__construct();
        $this->_init();      
        
    }
}
