<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cancel_failure_transaction_log_model extends MY_Model {

    var $table  = 'cancel_failure_transaction_log';
    var $fields = array("id","ticket_id","pg_tracking_id","ticket_transaction_status","ticket_issuing_date","ticket_fare","merchant_amount","refund_amount","refund_date","refund_reason","type_id","wallet_id","wallet_trans_id_db","wallet_trans_id_cr");
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
}
?>
