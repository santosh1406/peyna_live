<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Favourite_list_model extends MY_Model {

    var $table  = 'favourite_list';
    var $fields = array("fav_id","user_id","role_id","cat_favourite_list","no_psgr","inserted_date","modified_date","inserted_by","modified_by","is_deleted","status");
    // var $fields = "id,w_id,user_id,amt,status,expire_at,comment,added_by,added_on";
    var $key    = 'fav_id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
    function check_exist_favourite_list($input) {
       
        $this->db->select('cat_favourite_list');
        $this->db->where("cat_favourite_list", $input['fav_group']);
        $this->db->where("inserted_by",$input['user_id']);
        $this->db->where("is_deleted",'N');
        $this->db->where("status",'Y');
        $this->db->from('favourite_list');
        $query = $this->db->get();
        $data = $query->row();
     
        $cnt = count($data);
        return $cnt;
    }
    
    public function edit_fav_detail_list($input) {
        
        $data = array();
        $this->db->select("fl.cat_favourite_list,fld.id,fl.fav_id,fld.fname,fld.age,fld.gender");
        $this->db->from("favourite_list_detail fld");
        $this->db->join("favourite_list fl", "fld.fav_id=fl.fav_id", "inner");
        $this->db->where("fld.fav_id", $input['hiddn_fav_id']);
        $this->db->where("fld.is_deleted", "N");
        $this->db->where("fld.status", "Y");
        $query = $this->db->get();
      //  show($this->db->last_query(), 1);
        $data = $query->result_array();
        return $data;
    }
    function count_exist_favourite_list_details($input) {
       //show($input,1);
       if(isset($input['hiddn_fav_id']) && count($input['hiddn_fav_id'])>0){
            $this->db->select('fav_id');
            $this->db->where("fav_id", $input['hiddn_fav_id']);
            $this->db->where("is_deleted",'N');
            $this->db->where("status",'Y');
            $this->db->from('favourite_list_detail');
            $query = $this->db->get();
            $data_cnt = $query->result_array(); 
     
            $data = array("no_psgr" => count($data_cnt));
            $this->db->where('fav_id',$input['hiddn_fav_id']);
            $this->db->where('status', 'Y');
            $this->db->update('favourite_list', $data);
            return true;
        
       }else{
           return false;
       }
    }
    
     function delete_fav_detail_list($input,$user_id){
        
       
        $data = array("is_deleted" => 'Y');
        $this->db->where('status', 'Y');
        $this->db->where('fav_id', $input['hiddn_fav_id']);
        $this->db->update('favourite_list_detail', $data);
        
        $data = array("is_deleted" => 'Y',"modified_date"=>date('Y-m-d H:m:s'),"modified_by" =>$user_id);
        $this->db->where('fav_id', $input['hiddn_fav_id']);
        $this->db->where('status', 'Y');
        $this->db->update('favourite_list', $data);
        $msg['flag'] = "@success#";
        
        return $msg;
            
    }
    
    function favourite_list($user_id){
        $this->db->select('fl.fav_id, fl.cat_favourite_list,fld.id,fl.no_psgr,fl.inserted_date,fld.fname,fld.age,fld.gender');
        $this->db->from('favourite_list fl');
        $this->db->join("favourite_list_detail fld", "fld.fav_id=fl.fav_id", "inner");
        $this->db->where("fl.is_deleted", 'N');
        $this->db->where("fl.status", 'Y');
        $this->db->where("fld.status", 'Y');
        $this->db->where("fld.is_deleted",'N');
        $this->db->where("fl.user_id",$user_id);
        $this->db->order_by("fld.id", "asc"); 
        $query = $this->db->get();
        $data = $query->result_array();
        return $data;
    }

    function search_front_favourite_list($input){
        $user_id = $this->session->userdata("user_id");
        $this->db->select('fl.fav_id, fl.cat_favourite_list, fld.id,fl.no_psgr, fl.inserted_date,fld.fname,fld.age	,fld.gender');
        $this->db->from('favourite_list fl');
        $this->db->join("favourite_list_detail fld", "fld.fav_id=fl.fav_id", "inner");
        $this->db->where("fl.is_deleted", 'N');
        if($input['search_value'] != "")
            $this->db->like("fl.cat_favourite_list" ,$input['search_value']);
        $this->db->where("fl.status", 'Y');
        $this->db->where("fld.status", 'Y');
        $this->db->where("fld.is_deleted",'N');
        $this->db->where("fl.inserted_by",$user_id);
        $this->db->order_by("fld.id", "asc"); 
        $query = $this->db->get();
        $data = $query->result_array();
      //  show($this->db->last_query(),1);
        return $data;
    }
    
        function update_fav_group($input){
        $user_id = $this->session->userdata("user_id");
        $data = array("cat_favourite_list" =>  $input['edit_group_name'],"modified_date"=>date('Y-m-d H:m:s'),"modified_by" =>$user_id);
        $this->db->where('fav_id', $input['hiddn_fav_id']);
        $this->db->where('status', 'Y');
        $this->db->update('favourite_list', $data);
        $msg['flag'] = "@success#";
        
        return $msg;
            
    }
    
    
}
?>