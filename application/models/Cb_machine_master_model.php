<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of users
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Cb_machine_master_model extends MY_Model {
    
    var $table  = 'cb_machine_master';
    var $fields = array("id","IMEI_NO", "SIM_NO", "mobile_no","is_status", "is_deleted","created_by","created_date","updated_by","updated_date","IMEI_NO_2"); //BG 20258 - 3.1  
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

    public function getRolename($id)
    {

    	$this->db->select('role_name');
    	$this->db->from('user_roles');
    	$this->db->where('id',$id);
    	$res = $this->db->get();
    	
    	return $res->result_array();
    }
    
    public function get_imei_no() {
        //$query="Select distinct m.id,m.IMEI_NO from cb_machine_master m left join cb_assign_machine_agent a on m.id=a.machine_master_id  where m.is_status='Y'        and a.machine_master_id is NULL";

        $query="Select distinct m.id,m.IMEI_NO from cb_machine_master m 
                left join cb_assign_machine_agent a on m.id=a.machine_master_id  
                where m.is_status='Y' and a.machine_master_id is null or a.is_status='N'";

        $result = $this->db->query($query);
        if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function getAgentDetails($id) {
        $query="Select concat(u.first_name,' ',u.last_name) as agent_name from users u inner join cb_assign_machine_agent a on u.id=a.user_id where u.id='$id' limit 1";
        $result = $this->db->query($query);
        if(!empty($result->row())) {
            return $result->row();
        } else {
            return false;
        }
        
    }
}
