<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Agent_commission_model
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agent_commission_model extends MY_Model {
    
    
    var $table  = 'agent_commission';
    var $fields = array("id","agent_id","op_id","service_id","bus_type","commission_type","commission_value","actual_value","ptype","pattern_id","status","deleted");
    var $key    = 'id';
    protected $set_created =false;
    protected $set_modified =false;



    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

    public function get_commission(){

    	$this->db->get($this->table);
    }

    public function get_agent_commission($agent_id)
    {
        $result = false;
        $this->db->where('agent_id', $agent_id);
        $query = $this->db->get($this->table);

        $result = $query->result_array();

        return $result;
    }
    
  
}

// End of Agent_commission_model class