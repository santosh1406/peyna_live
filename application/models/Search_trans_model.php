<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Search_trans_model extends MY_Model {

    var $table  = 'last_search_result';
    var $fields = array("id","from","to","datetime","dateofsearch","userid","updated_datetime");
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
}



