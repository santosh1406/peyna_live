<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Wallet_model extends MY_Model {

    var $table  = 'wallet';
    var $fields = array("id","w_id","amt","actual_wallet_balance","remaining_bal","cummulative_bal","virtual_balance","current_outstanding","last_transction_id","comment","status","user_id","added_by","added_on");
    // var $fields = "id,w_id,user_id,amt,status,expire_at,comment,added_by,added_on";
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
   function check_wallet_rec($user_id)
    {
        $this->db->select('id,user_id,amt,expire_at');
        $this->db->from('wallet');
        $this->db->where('user_id',$user_id);
        $data=$this->db->get();
        return $data->result_array();
    }
    
    function get_agents_list()
    {   
        //select all agent whose entry is in wallet table
        $agent_arr = array();
        $this->db->select('user_id');
        $this->db->from('wallet');
        $agents = $this->db->get();
        if($agents->num_rows()>0)
        {
            $agents_data = $agents->result_array();
            foreach($agents_data as $key=>$val)
            {
                array_push($agent_arr, $val['user_id']);
            }
        }
        
        
        $this->db->select('agent_id, user_id, fname, lname');
        if($this->session->userdata('role_id')=='1')
        {
            $this->db->where(array('level1 !='=>'NULL'));
        }
        else if($this->session->userdata('role_id')=='18')
        {
            $this->db->where(array('level2 !='=>'NULL'));
        }
        else if($this->session->userdata('role_id')=='19')
        {
            $this->db->where(array('level3 !='=>'NULL'));
        }
        else if($this->session->userdata('role_id')=='17')
        {
            $this->db->where(array('level4 !='=>'NULL'));
        }
        $this->db->where(array('is_status'=>'1','is_deleted'=>'0'));
        
        if(!empty($agent_arr))
        {
            $this->db->where_not_in('user_id',$agent_arr);
        } 
        
        $this->db->where('user_id<>',0);
        
        
        $query = $this->db->get('agent_master');
        //show($this->db->last_query(),1);
        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        return false;
    }
    
    /*
     * Add amount to wallet
     */
    
    function add_amount_wallet($amt_data)
    {
        $user_id = $amt_data['user_id'];    
        $amount  = $amt_data['amount'];    
        $comment = 'Added by BOS';
        $added_by = $this->session->userdata('role_id');
        $status = 'Credit';
        $date = date("Y-m-d H:i:s");
        $insert_data = array(
                        'user_id'=> $user_id,
                        'amt' => $amount,
                        'status' => $status,
                        'comment' => $comment,
                        'added_by' => $added_by,
                        'added_on' => $date
                        );
        $this->db->insert('wallet',$insert_data);
        $wallet_id = $this->db->insert_id();
        
        //Insert data in wallet_trans table
        
        $wallet_trans_insert = array(
            'w_id'              =>  $wallet_id,
            'amt'               =>  $amount,
            'comment'           =>  $comment,
            'status'            =>  $status,
            'user_id'           =>  $user_id,
            'added_by'          =>  $added_by,
            'added_on'          =>  $date,
            'amt_before_trans'  =>  0,
            'amt_after_trans'   =>  $amount
        );
        $this->db->insert('wallet_trans',$wallet_trans_insert);
        
    }
    
    /*
     * Update amount
     */
    function get_agent_amount($w_id, $amt) {
        $this->db->select('amt');
        $this->db->from('wallet');
        $this->db->where('id',$w_id);
        $amt_data = $this->db->get();
        if($amt_data->num_rows()>0){
            $amount  = $amt_data->row_array();
            $new_amt = $amount['amt']+$amt;
            $comment = 'Added by BOS';
            $updated_by = $this->session->userdata('role_id');
            $status = 'Credited';
            $date = date("Y-m-d H:i:s");
            $update_data = array(
                            'amt'      => $new_amt,
                            'status'   => $status,
                            'comment'  => $comment,
                            'added_by' => $updated_by,
                            'added_on' => $date
                            );
            $this->db->where('id',$w_id);
            $this->db->update('wallet',$update_data);
            
            //select amt after adding
            $this->db->select('amt, user_id');
            $this->db->from('wallet');
            $this->db->where('id',$w_id);
            $amt_after_add = $this->db->get();
            if($amt_after_add->num_rows()>0){
                $amount_after_add  = $amt_after_add->row_array();
                $amt_aft_add = $amount_after_add['amt'];
                $user_id = $amount_after_add['user_id'];
            //Insert same data in wallet_trans table
            $trans_data = array(
                            'w_id'=> $w_id,
                            'amt' => $amt,
                            'comment' => 'Added by BOS',
                            'status'  => 'Credited',
                            'user_id' => $user_id,
                            'added_by'=> $updated_by,
                            'added_on'=> $date,
                            'amt_before_trans' => $amount['amt'],
                            'amt_after_trans' => $amt_aft_add
                            );
            
            $this->db->insert('wallet_trans',$trans_data);
            return $last_id = $this->db->insert_id();
            }
        }
        return false;
    }
    
    /*
     * Transaction history
     */
    function transaction_history($id)
    {
        $this->db->select('amt, status, DATE_FORMAT(added_on,"%d-%m-%Y %h:%i:%s") as added_date, amt_before_trans, amt_after_trans',false);
        $this->db->from('wallet_trans');
        $this->db->where('w_id',$id);
        $trans_data = $this->db->get();
        if($trans_data->num_rows()>0)
        {
            return $trans_data->result_array();
        }
        return false;
    }
    
    ///////////////////
		function get_walletid($ticket_id)
		{
			$where = "t.ticket_id='$ticket_id' OR t.return_ticket_id='$ticket_id'";
			$this->db->select('w.id as w_id,w.user_id');
			$this->db->from('wallet w');
            $this->db->join('tickets t', "t.inserted_by = w.user_id",'left');
			$this->db->where($where);
			$wallet_id = $this->db->get();
            
			if($wallet_id->num_rows()>0)
			{
				return $wallet_id->result_array();
			}
            
			return false;
		}
	
	////////////////////
    /* Queries for API starts Here */
    
    /*
     * Check Agent id available in wallet table
     */
    
    function check_agent_available($agent_id) {
        $this->db->select(' w.id as wallet_id, w.user_id,w.amt as wallet_amount');
        $this->db->from('wallet w');
        $this->db->join('users u','u.id=w.user_id');
        $this->db->where(array('amt >'=>'0','w.user_id'=>$agent_id, 'u.status'=>'Y','u.verify_status'=>'Y'));
        $agentData = $this->db->get();
        if($agentData->num_rows()>0) {
            return $agentData->row_array();
        }
        return false;
    }
    
    /* Queries for API ends Here */
	
	function update_wallet($data) {

        $this->db->where('user_id', $data["user_id"]);
        //$this->db->update("users", $userdata["user_array"]);
        // die($this->db->last_query());

        if ($this->db->update("wallet", $data["data_array"])) {
            return true;
        } else {
            return false;
        }
    }

    public function wallet_calculation()
    {
        $this->db->select("w.id as wallet_id,w.user_id,u.first_name,u.last_name,w.amt as wallet_amount,sum(if(wt.transaction_type='Credited',wt.amt,0)) as credited_amount, 
            sum(if(wt.transaction_type='Debited',wt.amt,0)) as debited_amount,
            (sum(if(wt.transaction_type='Credited',wt.amt,0)) - sum(if(wt.transaction_type='Debited',wt.amt,0)) ) as remain_amount,
            (w.amt- (sum(if(wt.transaction_type='Credited',wt.amt,0)) - sum(if(wt.transaction_type='Debited',wt.amt,0))) ) as difference");
        $this->db->from('wallet  w');
        $this->db->join('wallet_trans  wt', "wt.user_id = w.user_id",'left');
        $this->db->join('users   u', "u.id = w.user_id",'left');
        $this->db->group_by('w.user_id');
        $this->db->or_having(array('wallet_amount >' => '0','credited_amount  >' => '0','debited_amount > ' => '0' ));

        $walletData = $this->db->get();
        if ($walletData->num_rows() > 0)
        {
            return $walletData->result_array();
        }
        return FALSE;
    }

/*code for exporting report in excel format starts here*/
    public function wallet_transaction_report_excel($input,$id)
    {
         $this->db->select('wt.amt_before_trans,wt.transaction_type,wt.amt_after_trans,wt.amt,DATE_FORMAT(wt.added_on,"%d-%m-%Y %h:%i:%s") as added_date,t.boss_ref_no,wt.comment,',false);
        $this->db->from('wallet_trans wt');
        $this->db->join('tickets t', "t.ticket_id = wt.ticket_id",'left');
        $this->db->where('wt.user_id',$id);
        
        if($input['tran_type'])
        {
           $this->db->where('wt.transaction_type',$input['tran_type']);
        }
        if($input['from_date'])
        {
           $this->db->where("DATE_FORMAT(wt.added_on, '%Y-%m-%d')>=", date('Y-m-d', strtotime($input['from_date'])));
        }
        if($input['till_date'])
        {
           $this->db->where("DATE_FORMAT(wt.added_on, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['till_date'])));
        }
        if($input['tran_reason'])
        {
           $this->db->where('wt.transaction_type_id', $input['tran_reason']);
        }
        $this->db->order_by('wt.id','desc');
        $res = $this->db->get();
        $detail = $res->result_array();
        return $detail;
    }

    public function get_user_wallet_in_excel()
    {
        eval(FME_AGENT_ROLE_ID);
        $this->db->select("concat(u.first_name,' ',u.last_name) as name,u.email as email,u.id as agent_code,w.amt");
        $this->db->from('wallet w');
        $this->db->join('users u','w.user_id=u.id','inner');
        $this->db->where_in('u.role_id',$fme_agent_role_id);
        $this->db->order_by('w.amt','desc');

        $res = $this->db->get();
        $detail = $res->result_array();
        return $detail;  
    }
    /*code for exporting report in excel format end here*/

    public function get_wallet_detail($user_id) {
        $this->db->select('*');
        $this->db->from('wallet w');
        $this->db->where("user_id",$user_id);
        $query = $this->db->get();
        return $query->result();
    }
    public function updateWalletData($data,$id)
    {
        $this->db->where('user_id', $id);
        $update_wallet=$this->db->update('wallet', $data);

        if($update_wallet)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

?>
