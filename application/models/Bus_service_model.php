<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bus_service_model extends MY_Model {

    var $table  = 'bus_services';
    var $fields = array("bus_service_no","route_no","bus_type_id","seat_layout_id","scheduled_tm","op_id","effective_from","effective_till","schedule_cycle_type","schedule_cycle_days","max_waiting_seats","arrival_tm","arrival_day","trip_cycle","is_status","is_deleted","created_by","created_date","updated_by","updated_date");
    var $key    = 'bus_service_no'; 
    
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

     function get_service_data($data_array) {
        if(isset($data_array) && count($data_array)>0){
            $this->db->distinct();
            $this->db->select("ol.layout_name,bs.bus_service_no,r.route_id,r.km,r.from_stop,r.end_stop,r.route_no,bus_type_id,bs.seat_layout_id,scheduled_tm,r.op_id,effective_from,effective_till,schedule_cycle_type,schedule_cycle_days,max_waiting_seats,bs.arrival_tm,bs.arrival_day,trip_cycle");
            $this->db->where('r.route_no', $data_array['route_no']);
            $this->db->join("routes r", "r.route_no=bs.route_no and r.record_status='Y'", "left");
            $this->db->join("bus_service_stops bss", "bss.bus_service_no=bs.bus_service_no", "left");
            $this->db->join("op_seat_layout ol", "ol.id=bs.seat_layout_id and ol.record_status='Y'", "left");
            $this->db->from("bus_services bs");
            $this->db->where("bs.is_status", 'Y');
            $this->db->where("bs.is_deleted",'N');
            $this->db->order_by("bus_service_no", "desc"); 
            $query = $this->db->get();//show($this->db->last_query(),1);
            return $query->result_array();
        }
    }
    
    function get_bus_service_detail($data) {
        
        $this->db->select("cdm.cycle_days_id,cdm.cycle_days_name,bs.*,r.*");
        $this->db->from("bus_services bs");
        $this->db->join("routes r", "r.route_no=bs.route_no", "inner");
        $this->db->join("cycle_days_master cdm", "cdm.cycle_days_id=bs.schedule_cycle_days", "inner");
        $this->db->where("r.record_status", 'Y');
        $this->db->where("bs.is_status", 'Y');
        $this->db->where("bs.is_deleted",'N');
        $this->db->where("bus_service_no", $data);
        $query = $this->db->get();
//show($this->db->last_query(),1);
        return $query->result_array();
    }
    function update_bus_service($input){
         
        $update = array();
        $mast_update_data = array(
        'is_status' => 'N',
        'is_deleted'=>'Y');

        $this->db->where('bs.bus_service_no',$input['bus_service_no']);
        $this->db->where('bs.route_no',$input['route_no']);
        $this->db->where("bs.is_status", 'Y');
        $this->db->where("bs.is_deleted",'N');
        if($this->db->update('bus_services bs', $mast_update_data)){
            return true;
        } else {
            return false;
        }
    }
    
    public function bus_service_route_info($route_no) {
        
        $this->db->distinct();
        $this->db->select("bss.stop_seq as seq_no,bss.is_halt,bss.id,bss.arrival_tm,bss.is_boarding_stop,bss.departure_tm,bss.arrival_day,bss.is_alighting,bss.km,bss.route_no,bss.bus_stop_name,r.from_stop,r.end_stop");
        $this->db->from("bus_service_stops bss");
        $this->db->join("routes r",'r.route_no=bss.route_no', "inner");
        $this->db->where("bss.bus_service_no", $route_no['old_service_id']);
        $this->db->order_by("bss.stop_seq", "asc"); 
        $query = $this->db->get();
   
        return $query->result_array();
    }
    
     function delete_bus_service($input){
         
        $update = array();
        $mast_update_data = array(
        'is_status' => 'N',
        'is_deleted'=>'Y');

        $this->db->where('bs.bus_service_no',$input['service_id']);
     
        $this->db->where("bs.is_status", 'Y');
        $this->db->where("bs.is_deleted",'N');
        if($this->db->update('bus_services bs', $mast_update_data)){
            $input['old_service_id']=$input['service_id'];
            $this->update_bus_stops($input);
             return true;
        } else {
            return false;
        }
    }
    
    function update_bus_stops($input){
      $update = array();
      $bus_stops_update_data = array(
        'is_status' => 'N',
        'is_deleted'=>'Y');

        $this->db->where('bss.bus_service_no',$input['old_service_id']);
// $this->db->where('bs.route_no',$input['route_no']);
        $this->db->where("bss.is_status", 'Y');
        $this->db->where("bss.is_deleted",'N');
        if($this->db->update('bus_service_stops bss', $bus_stops_update_data)){
            //show($this->db->last_query(),1);
            return true;
        } else {
            return false;
        }
    }
    
    public function bus_service_info($bus_service_no) {
        
        $this->db->distinct();
        $this->db->select("bs.*");
        $this->db->from("bus_services bs");

        $this->db->where("bs.bus_service_no", $bus_service_no);
        
        $query = $this->db->get();
   
        return $query->result_array();
    }
}
