<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Summary_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }


    public function getSummaryData($data) {
        $created_by = isset($data['apploginuser']) ? $data['apploginuser'] :'';
        $summary_date = isset($data['summary_date']) ? $data['summary_date'] :"";
        $session_id = isset($data['session_id']) ? $data['session_id'] :"";
        $depot_cd = isset($data['depot_cd']) ? $data['depot_cd'] :"";
        $terminal_id = isset($data['terminal_id']) ? $data['terminal_id'] :"";
        $summary_satus = isset($data['summary_status']) ? $data['summary_status'] :"";
        //return true;
        $summaryExistArr = $this->getSummary($created_by,$summary_date,'','');
        return $summaryExistArr;
		//print_r($summaryExistArr);
        $transactionArr = $this->getTransationDetails($created_by,$summary_date, $depot_cd);
        if(count($summaryExistArr) > 0  ){
            $summery_id = isset($summaryExistArr[0]['id']) ? $summaryExistArr[0]['id'] :'';
            $update = $this->updateSummaryDetails($transactionArr, $created_by, $summery_id);
            $summaryArr = $this->getSummary($created_by,$summary_date,'','');
        }else{
            $insertId = $this->insertSummaryDetails($transactionArr,$created_by,$summary_date,$session_id,$depot_cd,$terminal_id);
            if($insertId > 0){
                $this->updateSummaryNumber($insertId,$created_by);
            }
            $summaryArr = $this->getSummary($created_by,$summary_date,'','');
        }
        return $summaryArr;
    }
    
    public function updateSummaryNumber($summaryId,$created_by) {
        $CI =& get_instance();
        $CI->load->model('Utility_model');
        $summary_no = $CI->Utility_model->summary_number('SUMMARY',$created_by);
        
        $data = array(
                'summary_no' => $summary_no,
                'modified_date' => date('Y-m-d H:i:s'),
                'modified_by' => $created_by
        );
        $this->db->where('id',$summaryId);
        return $this->db->update('ps_summary_master',$data);
    }
    
    public function getSummary($created_by,$summary_date,$summary_satus,$summary_no) {
	
 
        $this->db->select("s.id, s.summary_no ,s.summary_for_date, s.summary_status,
                            s.collected_by, amount, s.collected_amount,
                            s.summary_collect_date, s.no_of_registration, s.no_of_renewal, s.no_of_transactions,
                            s.no_of_dispatch_for_namt, s.no_of_faulty_card_for_namt,
                            d.DEPOT_NM depot_nm,
                            pt.terminal_code,
                            CASE
                                WHEN s.summary_status = '' THEN 'Not Generated'
                                WHEN s.summary_status = 'P' THEN 'Generated'
                                WHEN s.summary_status = 'C' THEN 'Closed'
                            END summary_status_val");
        $this->db->from('ps_summary_master s');
        $this->db->join('msrtc_replication.depots d', 'd.DEPOT_CD = s.depot_cd','inner');
       // $this->db->join('app_terminals pt', 'pt.terminal_id = s.terminal_id','inner');
        $this->db->join('retailer_service_mapping pt', 'pt.terminal_id = s.terminal_id','inner');
        if($summary_no != ''){
            $this->db->where('s.summary_no',$summary_no);
        }else{
            $this->db->where('s.created_by',$created_by);
            $this->db->where('s.summary_for_date',$summary_date);
            if($summary_satus != ''){
                $this->db->where('s.summary_status',$summary_satus);
            }else{
                $this->db->where("s.summary_status != 'C' ");
            }
        }
        
        $this->db->where('s.is_active','1');
        $query = $this->db->get();
        //return true;
        $returnArr = $query->result_array();
		/*echo $this->db->last_query();
		exit;*/
        return $returnArr;
    }
    
    public function getSummaryDeatils($created_by, $summary_date) {
        $this->db->select("s.summary_no ,s.summary_for_date, s.summary_status,
                            s.collected_by, amount, s.collected_amount,
                            s.summary_collect_date, s.no_of_registration, s.no_of_renewal, s.no_of_transactions,
                            s.no_of_dispatch_for_namt, s.no_of_faulty_card_for_namt,
                            d.DEPOT_NM depot_nm,
                            pt.terminal_code,
                            CASE
                                WHEN s.summary_status = '' THEN 'Not Generated'
                                WHEN s.summary_status = 'P' THEN 'Generated'
                                WHEN s.summary_status = 'C' THEN 'Closed'
                            END summary_status_val");
        $this->db->from('ps_summary_master s');
        $this->db->join('msrtc_replication.depots d', 'd.DEPOT_CD = s.depot_cd', 'inner');
        $this->db->join('app_terminals pt', 'pt.terminal_id = s.terminal_id', 'inner');
        $this->db->where('s.created_by', $created_by);
        $this->db->where('s.summary_for_date', $summary_date);
        $this->db->where('s.is_active', '1');
        $query = $this->db->get();
        $returnArr = $query->result_array();
        return $returnArr;
    }

    public function getTransationDetails($created_by,$summary_date, $depot_cd) {
        $this->db->select('sum(total_amount) as total_amount');
        $this->db->from('ps_amount_transaction');
        $this->db->where('depot_cd',$depot_cd);
        $this->db->where('created_by',$created_by);
        $this->db->where('DATE(created_date)',$summary_date);
        $this->db->where("summary_status != 'C' ");
        $this->db->where('is_active','1');
        $query = $this->db->get();
        $returnArr = $query->result_array();
         return isset($returnArr[0]) ? $returnArr[0] : '';
    }
    
    public function updateSummaryDetails($transactionArr, $created_by, $summery_id) {
        $total_amount = isset($transactionArr['total_amount']) ? $transactionArr['total_amount'] :'';
        
        $data = array(
                'amount' => $total_amount,
                'modified_date' => date('Y-m-d H:i:s'),
                'modified_by' => $created_by
        );
        $this->db->where('created_by',$created_by);
        $this->db->where('id',$summery_id);
        return $this->db->update('ps_summary_master',$data);
    }
    
    public function insertSummaryDetails($transactionArr, $created_by, $summary_date, $session_id,$depot_cd,$terminal_id) {
        $total_amount = isset($transactionArr['total_amount']) ? $transactionArr['total_amount'] :'';
        $financial_year = getCurrentFinancialYear();
        $summary_status = '';
        $session_id = $session_id;
        $data = array(
                'financial_year' => $financial_year,
                'summary_no' => '',
                'summary_status' => $summary_status,
                'summary_for_date' => $summary_date,
                'depot_cd' => $depot_cd,
                'terminal_id' => $terminal_id,
                'collected_by' => '',
                'amount' => $total_amount,
                'collected_amount' => '',
                'summary_collect_date' => '',
                'no_of_registration' => '',
                'no_of_renewal' => '',
                'no_of_transactions' => '',
                'no_of_dispatch_for_namt' => '',
                'no_of_faulty_card_for_namt' => '',
                'session_id' => $session_id,
                'created_date' => date('Y-m-d H:i:s'),
                'created_by' => $created_by,
                'modified_date' => date('Y-m-d H:i:s'),
                'modified_by' => $created_by,
                'is_active' => '1'
        );
        $this->db->insert('ps_summary_master',$data);      
        return $this->db->insert_id();
    }
    
    public function updateSummaryStatusInSummary($created_by, $summary_date, $status) {
        $data = array(
                'summary_status' => $status,
                'modified_date' => date('Y-m-d H:i:s'),
                'modified_by' => $created_by
        );
        $this->db->where('created_by',$created_by);
        $this->db->where('summary_for_date',$summary_date);
        $this->db->where("summary_status != 'C' ");
        return $this->db->update('ps_summary_master',$data);
    }
    
    public function updateSummaryStatusInOther($created_by, $summary_date, $summary_no, $status,$tableName) {
        $data = array(
                'summary_status' => $status,
                'summary_no' => $summary_no,
                'modified_date' => date('Y-m-d H:i:s'),
                'modified_by' => $created_by
        );
        $this->db->where('created_by',$created_by);
        $this->db->where('DATE(created_date)',$summary_date);
//        $this->db->where('created_date',$summary_date);
        if($status == 'P'){
            $this->db->where("summary_status ='' ");
            $this->db->where("summary_no ='' ");
        }else if($status == 'C'){
            $this->db->where("summary_status ='P' ");
            $this->db->where("summary_no != '' ");
        }
	$this->db->where("is_active = '1' ");
        return $this->db->update($tableName, $data);
    }
    
    public function updateCashierData($created_by, $summary_date, $collected_amount, $collected_date, $collected_by) {
        $data = array(
                'collected_by' => $collected_by,
                'collected_amount' => $collected_amount,
                'summary_collect_date' => $collected_date,
                'modified_date' => date('Y-m-d H:i:s'),
                'modified_by' => $created_by
        );
        $this->db->where('created_by',$created_by);
        $this->db->where('summary_for_date',$summary_date);
        return $this->db->update('ps_summary_master',$data);
    }
    
    public function updateSummaryData($data) {
        $created_by = isset($data['apploginuser']) ? $data['apploginuser'] :'';
        $summary_date = isset($data['summary_date']) ? $data['summary_date'] :'';
        $summary_no = isset($data['summary_no']) ? $data['summary_no'] :'';
        $summary_status = isset($data['summary_status']) ? $data['summary_status'] :'';
        $updateId = $this->updateSummaryStatusInOther($created_by, $summary_date, $summary_no, $summary_status, 'ps_amount_transaction');
        return $updateId;
    }
}
