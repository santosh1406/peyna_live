<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Etravels_bus_service_model
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Etravels_bus_service_model extends MY_Model {

    var $table = 'etravels_bus_service';
    var $fields = array("id", "from", "to", "date", "bus_service", "timestamp");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
}

// End of Etravels_bus_service_model class