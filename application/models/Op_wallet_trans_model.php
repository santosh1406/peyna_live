<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Op_wallet_trans_model extends MY_Model {

    var $table  = 'op_wallet_trans';
    var $fields = array("id","op_wallet_id","opening_bal","closing_bal","amount","actual_wallet_amount","virtual_amount","outstanding_amount","total_bal_amt_before_trans","total_bal_amt_after_trans","virtual_amt_before_trans","virtual_amt_after_trans","wallet_type","credit_detail_id","transaction_type","topup_id","comment","status","transfer","created_by","created_date");
    
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    function get_all_services() {
        
            $this->db->select("*");
            $this->db->from("services");
            $this->db->order_by('id asc'); 
            $result = $this->db->get();
            $result_array = $result->result_array();
            return $result_array;
    }
    
  function op_wallet_report($input){
        $this->db->select(
			                       'wt.id,
                                   	wt.created_date, 
			                        om.op_name,
			                        s.name as service,
                                    IFNULL(amm.`fname`,"-") AS "agent_name",
                                    IFNULL(amm.`zone_name`,"-") AS "Zone",
                                    IFNULL(amm.`depot_name`,"-") AS "depot",
			                        wt.opening_bal, 
			                        wt.amount,
			                        wt.closing_bal,amm.agent_code,amm.fname,
                                    wt.virtual_amt_before_trans,
                                    wt.virtual_amt_after_trans,
                                    wt.outstanding_amount,
			                        wt.comment,
			                        wt.status',
			                    false);
     
        $this->db->from('op_wallet w'); 
        $this->db->join('op_wallet_trans wt', 'w.id = wt.op_wallet_id'); 
        $this->db->join('agent_master amm', "amm.agent_id =  wt.transfer",'left');
        if ($this->session->has_userdata('depot_cd') && !empty($this->session->userdata('depot_cd'))) {
            $this->db->join('op_master om', 'om.op_id = w.user_id and w.user_type = "a" ');
            $this->db->join('agent_master am', "am.agent_id = w.user_id and am.depot_cd = '".$this->session->userdata('depot_cd')."'");
        }
        else {
            $this->db->join('op_master om', 'om.op_id = w.user_id and w.user_type = "o" ');
        }
        
        $this->db->join('services s', 's.id = w.service_id');

        if($input['status']!="")
        {
            $this->db->where('wt.status', $input['status']);
        }
        
        //if ($this->input->post('from_date') && $this->input->post('from_date') != "") {
        if (!empty($input['from_date'])) {
            $this->db->where('DATE_FORMAT(wt.created_date , "%Y-%m-%d")>=', date('Y-m-d', strtotime($input['from_date'])));
        }
        if (!empty($input['till_date'])) {
            $this->db->where('DATE_FORMAT(wt.created_date, "%Y-%m-%d") <=', date('Y-m-d', strtotime($input['till_date'])));
        }
         if (!empty($input['zone_sel'])) {
            $this->db->where('amm.zone_cd', $input['zone_sel'], 'left');
        }
        if (!empty($input['depot_sel'])) {
            $this->db->where('amm.depot_cd', $input['depot_sel'], 'left');
        }
	    if (!empty($input['agent_id'])) {
            $this->db->where('wt.transfer',$input['agent_id']);
        }

       // $this->db->where('wt.op_wallet_id', $id);//show(last_query());
       //  $this->db->order_by('wt.created_date', 'desc');
	$this->db->order_by('wt.id', 'desc');
        $res=$this->db->get(); //show(last_query(),1);
         return $res->result_array();
    }


     function op_trans_report($input){
        $this->db->select(
                           'wt.id,
                          wt.created_date , 
                           if(SUBSTRING(wt.comment FROM 1 FOR 16) like "%Amount allocated%","Waybill Allocation",
                                    if(SUBSTRING(wt.comment FROM 1 FOR 16) like "%Closing balance %","Waybill Remaining balance Credit",
                                    if(SUBSTRING(wt.comment FROM 1 FOR 16) like "%Commission on to%","Commission to Trimax",
                                    if(SUBSTRING(wt.comment FROM 1 FOR 16) like "%Amount credited %","Topup by Trimax"," ")))) tran_status,
                            wt.status,
                            wt.opening_bal, 
                            wt.amount,
                            wt.closing_bal,
                            ifnull(am.agent_code,"-") as agent_code,
                            ifnull(am.fname,"-") as agent_name,ifnull((select ad.agent_code from agent_master ad where am.parent=ad.agent_id),"-") as Ad_code,
                            ifnull((select ad.fname from agent_master ad where am.parent=ad.agent_id),"-") as Ad_name,
                            ifnull(if(SUBSTRING(wt.comment FROM 1 FOR 16) like "%Amount allocated%",SUBSTRING(wt.comment FROM -10 FOR 10),"-"),"-") as waybill_no,
                            ',
                                false);
     
        $this->db->from('op_wallet w'); 
        $this->db->join('op_wallet_trans wt', 'w.id = wt.op_wallet_id'); 
        $this->db->join('agent_master am', 'am.agent_id=wt.transfer', 'left');
      
       //$this->datatables->join('agent_master amm', "amm.agent_id =  wt.transfer",'left');
        if ($this->session->has_userdata('depot_cd') && !empty($this->session->userdata('depot_cd'))) {
            $this->db->join('op_master om', 'om.op_id = w.user_id and w.user_type = "a" ');
            $this->db->join('agent_master am', "am.agent_id = w.user_id and am.depot_cd = '".$this->session->userdata('depot_cd')."'");
        }
        else {
            $this->db->join('op_master om', 'om.op_id = w.user_id and w.user_type = "o" ');
        }

        if($input['tran_type'] && $input['tran_type']!='all')
        {
            $status=$input['tran_type'];
            if($status=='Commission to Trimax')
            {
                $this->db->like('wt.comment','Commission on topup');
            }
            else if($status=='Waybill Allocation')
            {
                $this->db->like('wt.comment','Amount allocated to ETIM');
            }
            else if ($status=='Waybill Remaining balance Credit') {
                $this->db->like('wt.comment','Closing balance of Agent');
            }
            else
            {
                $this->db->like('wt.comment','Amount credited to Trimax');
            }
        }else{
            $this->db->where('wt.op_wallet_id',1);
        }
    
        if (!empty($input['from_date'])) {
            $this->db->where('DATE_FORMAT(wt.created_date , "%Y-%m-%d")>=', date('Y-m-d', strtotime($input['from_date'])));
        }
        if (!empty($input['till_date'])) {
            $this->db->where('DATE_FORMAT(wt.created_date, "%Y-%m-%d") <=', date('Y-m-d', strtotime($input['till_date'])));
        }
         if (!empty($input['agent_id'])) {
                $this->db->where('wt.transfer',$input['agent_id']);
            }

        $this->db->order_by('wt.created_date', 'desc');
        $res=$this->db->get(); //show(last_query(),1);
         return $res->result_array();
    }
         function op_trans_report1($input){
        $this->db->select(
                           'wt.id,
                           DATE_FORMAT(wt.created_date, "%d-%m-%Y %h:%i:%s") as created_date, 
                            wt.status,
                            wt.opening_bal, 
                            wt.amount,
                            wt.closing_bal,
                            ifnull(am.agent_code,"-") as agent_code,
                            ifnull(am.fname,"-") as agent_name,ifnull((select ad.agent_code from agent_master ad where am.parent=ad.agent_id),"-") as Ad_code,
                            ifnull((select ad.fname from agent_master ad where am.parent=ad.agent_id),"-") as Ad_name,
                            ifnull(if(SUBSTRING(wt.comment FROM 1 FOR 16) like "%Amount allocated%",SUBSTRING(wt.comment FROM -10 FOR 10),"-"),"-") as waybill_no,
                            ',
                                false);
     
        $this->db->from('op_wallet w'); 
        $this->db->join('op_wallet_trans wt', 'w.id = wt.op_wallet_id'); 
        $this->db->join('agent_master am', 'am.agent_id=wt.transfer', 'left');
      
       //$this->datatables->join('agent_master amm', "amm.agent_id =  wt.transfer",'left');
        if ($this->session->has_userdata('depot_cd') && !empty($this->session->userdata('depot_cd'))) {
            $this->db->join('op_master om', 'om.op_id = w.user_id and w.user_type = "a" ');
            $this->db->join('agent_master am', "am.agent_id = w.user_id and am.depot_cd = '".$this->session->userdata('depot_cd')."'");
        }
        else {
            $this->db->join('op_master om', 'om.op_id = w.user_id and w.user_type = "o" ');
        }

        if($input['tran_type'] && $input['tran_type']!='')
        {
            $status=$input['tran_type'];
            if($status=='Commission to Trimax')
            {
                $this->db->like('wt.comment','Commission on topup');
            }
            else if($status=='Waybill Allocation')
            {
                $this->db->like('wt.comment','Amount allocated to ETIM');
            }
            else if ($status=='Waybill Remaining balance Credit') {
                $this->db->like('wt.comment','Closing balance of Agent');
            }
            else
            {
                $this->db->like('wt.comment','Amount credited to Trimax');
            }
        }
    
        if (!empty($input['from_date'])) {
            $this->db->where('DATE_FORMAT(wt.created_date , "%Y-%m-%d")>=', date('Y-m-d', strtotime($input['from_date'])));
        }
        if (!empty($input['till_date'])) {
            $this->db->where('DATE_FORMAT(wt.created_date, "%Y-%m-%d") <=', date('Y-m-d', strtotime($input['till_date'])));
        }
         if (!empty($input['agent_id'])) {
                $this->db->where('wt.transfer',$input['agent_id']);
            }

        $this->db->order_by('wt.id', 'desc');
        $res=$this->db->get(); //show(last_query(),1);
         return $res->result_array();
    }

    function waybill_trans_report($input){
        
       
        $this->datatables->select(
                                   'w.WAYBILL_ID,
                                    ifnull(am.agent_code,"-") as agent_code,
                                    ifnull(am.fname,"-") as fname,
                                    ifnull(am.zone_name,"-") as zone_name,
                                    ifnull(am.depot_name,"-") as depot_name,
                                    w.ETIM_NO as ETIM_NO,
                                    w.DUTY_DT as depot,
                                    w.TOP_UP_BAL as TOP_UP_BAL,
                                    w.TICKET_CNT_PER_DAY as actual,
                                    ifnull(w.TICKET_CNT_PER_DAY - w.TOP_UP_BAL,"-") as remaing_bal
                                    ',
                                false);
        $this->db->from('waybillprogramming w'); 
        //$this->datatables->join('op_wallet_trans wt', 'w.id = wt.op_wallet_id'); 
      //  $this->datatables->join('op_master com', 'com.op_id = w.user_id ');
        $this->db->join('agent_master am', 'am.agent_id=w.AGENT_ID', 'left');
          
        if (!empty($input['from_date'])) {
            $this->db->where('DATE_FORMAT(w.DUTY_DT , "%Y-%m-%d")>=', date('Y-m-d', strtotime($input['from_date'])));
        }
        if (!empty($input['till_date'])) {
            $this->db->where('DATE_FORMAT(w.DUTY_DT, "%Y-%m-%d") <=', date('Y-m-d', strtotime($input['till_date'])));
        }
        if (!empty($input['agent_id'])) {
                $this->db->where('w.AGENT_ID',$input['agent_id']);
         }
         $this->datatables->where('w.WAYBILL_STATUS','OPEN');
        $this->db->order_by('w.WAYBILL_ID','desc');
        $res=$this->db->get(); 
         return $res->result_array();
    }

    /* @Author    : Aparna chalke
     * @function  : export_waybillclosed_report
     * @param     : 
     * @detail    : To fetch waybill closed data in excel format
     */

    function export_waybillclosed_report($input)
    {
        
            $this->db->select('0,am.agent_id,am.agent_code,am.fname, am.zone_name, am.depot_name,wb.ETIM_NO,wb.DUTY_DT,
            wb.collection_date,wb.allocation_amt,wat.hhm_consumed_amt,
            wat.hhm_remaining_amt');
            $this->db->from('agent_master am');
            $this->db->join('(select a.ETIM_NO,a.DUTY_DT,a.COLLECTION_TM as collection_date,a.WAYBILL_STATUS,a.AGENT_ID,a.WAYBILL_NO,a.TOP_UP_BAL as allocation_amt,a.UPDATE_DT
              from waybillprogramming as a where a.WAYBILL_NO IN (select max(WAYBILL_NO)as last_waybill 
              from waybillprogramming as b group by  b.AGENT_ID)) wb','wb.AGENT_ID = am.agent_id','inner');
            $this->db->join('waybill_amt_transaction as wat','wat.waybill_no = wb.WAYBILL_NO','LEFT');
            $this->db->where('wb.WAYBILL_STATUS','CLOSED');
            if (!empty($input['from_date'])) {
            $this->db->where('DATE_FORMAT(wb.collection_date , "%Y-%m-%d")>=', date('Y-m-d', strtotime($input['from_date'])));
            }
            if (!empty($input['till_date'])) {
                $this->db->where('DATE_FORMAT(wb.collection_date, "%Y-%m-%d") <=', date('Y-m-d', strtotime($input['till_date'])));
            }   
            if (!empty($input['agent_id'])) {
                $this->db->where('wb.AGENT_ID', $input['agent_id']);
            }        
            $this->db->order_by('wb.AGENT_ID','asc');
        $res = $this->db->get();
        return $res->result_array();
    }

    function export_waybillclosed_report_query($input)  
    {
            $this->db->select('0,am.agent_id,am.agent_code,am.fname, am.zone_name, am.depot_name,wb.ETIM_NO,wb.DUTY_DT,
            wb.collection_date,wb.allocation_amt,wat.hhm_consumed_amt,
            wat.hhm_remaining_amt');
            $this->db->from('agent_master am');
            $this->db->join('(select a.ETIM_NO,a.DUTY_DT,a.COLLECTION_TM as collection_date,a.WAYBILL_STATUS,a.AGENT_ID,a.WAYBILL_NO,a.TOP_UP_BAL as allocation_amt,a.UPDATE_DT
              from waybillprogramming as a where a.WAYBILL_NO IN (select max(WAYBILL_NO)as last_waybill 
              from waybillprogramming as b group by  b.AGENT_ID)) wb','wb.AGENT_ID = am.agent_id','inner');
            $this->db->join('waybill_amt_transaction as wat','wat.waybill_no = wb.WAYBILL_NO','LEFT');
            $this->db->where('wb.WAYBILL_STATUS','CLOSED');
            if (!empty($input['from_date'])) {
            $this->db->where('DATE_FORMAT(wb.collection_date , "%Y-%m-%d")>=', date('Y-m-d', strtotime($input['from_date'])));
            }
            if (!empty($input['till_date'])) {
                $this->db->where('DATE_FORMAT(wb.collection_date, "%Y-%m-%d") <=', date('Y-m-d', strtotime(
                $input['till_date'])));
            }   
            if (!empty($input['agent_id'])) {
                $this->db->where('wb.AGENT_ID', $input['agent_id']);
            }        
            $this->db->order_by('wb.AGENT_ID','asc');
        $res = $this->db->get();
        return $res->result_array();
    }

     function cancelled_waybills_report($input)
    {
        $this->db->select('0,wb.WAYBILL_NO,am.fname,wb.ETIM_NO,wb.TOP_UP_BAL as allocation_amt');
        $this->db->from('waybillprogramming wb'); 
        $this->db->join('agent_master am', 'wb.AGENT_ID = am.agent_id' ,'left');
        $this->db->join('op_wallet_trans owt', 'owt.waybill_no = wb.WAYBILL_NO');
        $this->db->join('cancelled_waybills_wallet_data cwd', 'owt.id = cwd.op_wallet_trans_id','Left outer');
        $this->db->where('cwd.waybill_no IS NULL');
        if (!empty($input['from_date'])) {
            $this->db->where('DATE_FORMAT(wb.DUTY_DT , "%Y-%m-%d")>=', date('Y-m-d', strtotime($input['from_date'])));
        }
        if (!empty($input['till_date'])) {
            $this->db->where('DATE_FORMAT(wb.DUTY_DT, "%Y-%m-%d") <=', date('Y-m-d', strtotime($input['till_date'])));
        }
        $this->db->where('wb.WAYBILL_STATUS', 'CANCELED');
        $res = $this->db->get();
        return $res->result_array();
    }

    function update_amount_trans_model($waybill_no)
    {  
        $this->db->select('id,op_wallet_id,amount');
        $this->db->from('op_wallet_trans'); 
        $this->db->where('waybill_no',$waybill_no);
        $res = $this->db->get();
        $res = $res->result();
        $amount = $res[0]->amount;
        $id = $res[0]->id;
        $op_wallet_id = $res[0]->op_wallet_id;
         $data = array(
               'amount' => 0
            );
      $this->db->where('id', $id);
      $this->db->update('op_wallet_trans', $data); 
      $data['amount'] = $amount;
      $data['op_wallet_id'] =$op_wallet_id; 
      $data['op_wallet_trans_id'] = $id;
      return $data;

    }

    function update_amount_wallet_model($result)
    {
        $data = array();
        if(count($result)>0)
        {
            if($result["op_wallet_id"] != '')
            {
                 $this->db->select('opening_bal,amount,total_balance');
                 $this->db->from('op_wallet');
                 $this->db->where('id', $result["op_wallet_id"]);
                 $resultobj = $this->db->get();
                 $resarr = $resultobj->result_array();
                    
                 if($resarr != '')
                 {
                    $opening_bal_before_trans = $resarr[0]['opening_bal'];
                    $closing_bal_before_trans = $resarr[0]['amount'];
                    $total_amt_before_trans = $resarr[0]['total_balance'];
                 }
            }

            if( $result["amount"] != '')
            {
                if($result["amount"] < 0)
                {
                     $result_amount = abs($result["amount"]); 
                 }
                 else
                 {
                    $result_amount = $result["amount"]; 
                 }
                $this->db->set('amount', 'amount +'.$result_amount,FALSE);
                $this->db->where('id', $result["op_wallet_id"]);
                $flag = $this->db->update('op_wallet');

            }
           
            if($flag)
            {
                 $this->db->set('total_balance', 'amount + virtual_balance',false);
                 $this->db->where('id', $result["op_wallet_id"]);
                 $flag1 = $this->db->update('op_wallet');
                 if($flag1)
                 {
                    $this->db->select('total_balance');
                    $this->db->from('op_wallet');
                    $this->db->where('id', $result["op_wallet_id"]);
                    $objTotalamt = $this->db->get();
                    $arrTotalamt = $objTotalamt->result_array();
                   
                    $data['op_wallet_trans_id'] = $result["op_wallet_trans_id"]; 
                    $data['op_wallet_id'] = $result["op_wallet_id"];    
                    $data['closing_bal_after_trans'] = $arrTotalamt[0]['total_balance'];
                    $data['opening_bal_before_trans'] =  $opening_bal_before_trans;
                    $data['closing_bal_before_trans'] =  $closing_bal_before_trans;
                    $data['trans_amount']             =  $result_amount; 
                   // show($data,1);
                    return $data;
                }
            }
            else
            {
                return $data;
            }
            
        }  
    }

    function fetch_agent($input)
    {

         $this->db->select('am.agent_id,am.agent_code,am.fname');
            $this->db->from('agent_master am');
            $this->db->join('(select a.AGENT_ID,a.WAYBILL_NO,a.COLLECTION_TM as collection_date,a.WAYBILL_STATUS from waybillprogramming as a where a.WAYBILL_NO IN (select max(WAYBILL_NO)as last_waybill 
              from waybillprogramming as b group by  b.AGENT_ID)) wb','wb.AGENT_ID = am.agent_id','inner');
            $this->db->join('waybill_amt_transaction as wat','wat.waybill_no = wb.WAYBILL_NO','LEFT');
            $this->db->where('wb.WAYBILL_STATUS','CLOSED');
            if (!empty($input['from_date'])) {
            $this->db->where('DATE_FORMAT(wb.collection_date , "%Y-%m-%d")>=', date('Y-m-d', strtotime($input['from_date'])));
            }
            if (!empty($input['to_date'])) {
                $this->db->where('DATE_FORMAT(wb.collection_date, "%Y-%m-%d") <=', date('Y-m-d', strtotime(
                $input['to_date'])));
            }        
            $this->db->order_by('wb.AGENT_ID','asc');
        $res = $this->db->get();
        
        return $res->result_array();

       /* $subquery = "select distinct(AGENT_ID) from waybillprogramming where 1 ";
         if (!empty($input['from_date'])) {
        $subquery .= "and  (DATE_FORMAT(UPDATE_DT , '%Y-%m-%d')>='".date('Y-m-d', strtotime($input['from_date']))."')";
         }
         if (!empty($input['to_date'])) {
        $subquery .= "and (DATE_FORMAT(UPDATE_DT, '%Y-%m-%d') <='".date('Y-m-d', strtotime(
                                $input['to_date']))."')";
            } 

        $cQuery = "select am.agent_id,am.agent_code,am.fname,am.mname,am.lname from agent_master am where am.agent_id
        in (".$subquery.")";

        $res = $this->db->query($cQuery);
       // show(last_query(),1);
        return $res->result_array();*/
    }
}
