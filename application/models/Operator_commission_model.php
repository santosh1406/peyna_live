<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Operator_model
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Operator_commission_model extends MY_Model {
    
    
    var $table  = 'commission_operator';
    var $fields = array("id","commission_id","operator_id","type","commission","from_date","till_date","record_status");
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
  
}

// End of commission_operator class