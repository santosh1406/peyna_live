<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Rokad_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
    public function getLoginDetails($aUser)
    {

        $this->db->select("users.id,users.first_name,users.last_name,users.email,users.gender,
                          concat(address1,',',address2) as address,users.pincode,users.mobile_no,
                          users.role_id,user_roles.role_name,users.kyc_verify_status,users.status
                          ,users.agent_code,wallet.remaining_bal,
                          IF(tickets.inserted_date Like '$date', count(tickets.ticket_id),0) as total_transaction,IF(tickets.inserted_date Like '$date', sum(tickets.tot_fare_amt),0) as total_transaction_amt");
        $this->db->from("users");
        $this->db->join('user_roles','user_roles.id = users.role_id','left');
        $this->db->join('wallet','wallet.user_id = users.id','left');
        $this->db->join('tickets','tickets.inserted_by = users.id','left');
        $this->db->where('users.id',$aUser[0]->id);
        $query = $this->db->get();
        $data = $query->result_array();
        return $data;
    }



}