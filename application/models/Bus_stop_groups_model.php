<?php

class Bus_stop_groups_model extends CI_Model 
{
    
    var $table = 'bus_stop_groups';
    var $fields = array("id","suggested_nm","bus_stop_cd","op_id","status","preference");
    
    var $key = "";

    public function __construct() {
        parent::__construct();
        // $this->_init();
    }

    public function get_stop($stop)
    {
    	$result = array();
    	$this->db->from('bus_stop_groups');
        $this->db->where('suggested_nm',$stop);
        $query =  $this->db->get();

        $result = $query->result_array();

        return $result;
    }
    
}

?>