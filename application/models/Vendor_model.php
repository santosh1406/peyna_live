<?php


if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Vendor_model extends MY_Model {
    
    var $table  = 'vendor';
    var $fields = array("id","user_id" ,"name","library","email","phone_no","is_status","is_deleted","created_by","created_date","updated_by","updated_date");
    var $key    = "id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
}