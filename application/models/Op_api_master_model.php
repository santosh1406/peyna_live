<?php

class Op_api_master_model extends MY_Model {

    var $table = 'op_api_master';
    var $fields = array("op_api_id", "op_id", "api_url", "username", "password","user_type","created_by","created_date","updated_by","updated_date","is_status","is_deleted");
    var $key = 'op_api_id';

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
    
        function get_op_notin_apidetail() 
    {

        $this->db->select('om.op_id,om.op_name');
        $this->db->from('op_master om');
        $this->db->where('`op_id` NOT IN (SELECT `op_id` FROM `op_api_master`)', NULL, FALSE);
       
        $state = $this->db->get();


        return $state->result_array();
    }
    function add_api($data)
    {
        $this->db->insert($this->table,$data);
    }
    
    
}



?>