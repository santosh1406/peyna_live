<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Incentive_type_lookup_model extends MY_Model {
 
    var $table    = 'incentive_type_lookup';
    var $fields = array("id","type_name","is_status");
    var $key    = 'id'; 
   
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

    
}

