<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Report_Model extends CI_Model {

    var $default_db;



    function __construct() { 
        parent::__construct();
        $this->default_db = get_database('default');
    }

    function daily_advance_ticket($input) {

        $from_date = !empty($input['from_date']) ? date('Y-m-d', strtotime($input['from_date'])) : '';
        $to_date = !empty($input['to_date']) ? date('Y-m-d', strtotime($input['to_date'])) : '';


        $this->db->select("CASE tk.transaction_status when 'success' then 'Success Transaction' when 'cancel' then 'Cancel Transaction' "
                . "when 'pfailed' then 'Payment Failed'   when 'failed' then 'Failed' when 'Incomplete' then "
                . "'Incomplete Transaction' when 'psuccess' then 'Payment Success' else '' end as Tran_type,"
                . " `tk`.`issue_time`, `tk`.`boss_ref_no`, 'RED BUS', `tk`.`op_name`, `tk`.`ticket_ref_no`, "
                . "`tk`.`from_stop_name`, `tk`.`till_stop_name`, `tk`.`num_passgr`, `tk`.`tot_fare_amt`, "
                . "`tk`.`fare_reservationCharge`, (tk.tot_fare_amt_with_tax-tk.tot_fare_amt) as Total_tax, "
                . "ctd.refund_amt,IF(ctd.is_refund=1,sum(ctd.`cancel_charge`),0)  cancel_charge, count(tds.seat_no) as seat_can,"
                . "tk.tot_fare_amt_with_tax,"
                . "sum(tk.fare_acc+tk.fare_hr+tk.fare_it+tk.fare_octroi+tk.fare_toll+tk.fare_convey_charge) as total_charges,tk.status");
        $this->db->FROM('tickets tk');
        if (isset($input['op_name']) && $input['op_name'] != '') {
            $this->db->where('op_name', $input['op_name']);
        }

        if (isset($input['tran_type']) && $input['tran_type'] != '') {
            $this->db->where('tk.transaction_status', $input['tran_type']);
        }

        if (isset($input['pnr_no']) && $input['pnr_no'] != '') {
            $this->db->where('tk.pnr_no', $input['pnr_no']);
        }

        if ((isset($from_date) && isset($to_date)) && (($from_date != "") && ($to_date != ""))) {
            if ($from_date != $to_date) {
                $this->db->where("DATE_FORMAT(issue_time, '%Y-%m-%d') BETWEEN '" . $from_date . "' AND '" . $to_date . "' ");
            } else {
                $this->db->where("DATE_FORMAT(issue_time, '%Y-%m-%d')=", $from_date);
            }
        } else if ($from_date != '' && $to_date == '') {
            $this->db->where("DATE_FORMAT(issue_time, '%Y-%m-%d')=", $from_date);
        } else if ($from_date == '' && $to_date != '') {
            $this->db->where("DATE_FORMAT(issue_time, '%Y-%m-%d')=", $to_date);
        }

        $this->db->group_by('tds.ticket_id');
        $this->db->join('cancel_ticket_data ctd', 'ctd.ticket_id=tk.ticket_id', 'left');
        $this->db->join('ticket_details   tds', 'tds.ticket_id=tk.ticket_id', 'inner');
        $res = $this->db->get();
        return $res->result_array();
    }

    function user_registeraion_detail($input) {
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');
       $this->db->select('1,
                        DATE_FORMAT(created_date,"%d-%m-%Y %h:%i:%s") as created_date,concat(first_name," ",last_name) AS UserName,ur.role_name as UserType,
                        sm.stState AS State,
                        cm.stCity AS City,
                        mobile_no AS Mobile,
                        email AS Email');
        $this->db->from('users u');
        $this->db->join('state_master sm', 'u.state=sm.intStateId', 'left');
        $this->db->join('city_master cm', 'cm.intCityId=u.city', 'left');
        $this->db->join('user_roles ur', 'u.role_id=ur.id', 'left');

        if (isset($input['from_date']) && $input['from_date'] != '') {
            $this->db->where("DATE_FORMAT(u.created_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['to_date']) && $input['to_date'] != '') {
            $this->db->where("DATE_FORMAT(u.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }
       // $this->datatables->where("u.status = 'Y' and u.verify_status = 'Y' and u.salt !='' and u.password != '' ");

        $this->db->where("u.status = 'Y'");
        $this->db->where("u.is_deleted = 'N'");
                
        if(!empty($session_level)){
        	if($session_level > TRIMAX_LEVEL)
        	{
        		$this->db->where('level_'.$session_level,$session_id);
        	}
        	else
        	{
        		$this->db->where('level_'.TRIMAX_LEVEL, $session_id);
        	}
        }
        
        if ($input["user_type"] == COMPANY_ROLE_ID) {
        	$this->db->where("u.role_id",COMPANY_ROLE_ID);
        } else if ($input["user_type"] == MASTER_DISTRIBUTOR_ROLE_ID) {
        	$this->db->where("u.role_id",MASTER_DISTRIBUTOR_ROLE_ID);
        } else if ($input["user_type"] == AREA_DISTRIBUTOR_ROLE_ID) {
        	$this->db->where("u.role_id",AREA_DISTRIBUTOR_ROLE_ID);
        } else if ($input["user_type"] == DISTRIBUTOR) {
        	$this->db->where("u.role_id",DISTRIBUTOR);
        } else if ($input["user_type"] == RETAILER_ROLE_ID) {
        	$this->db->where("u.role_id",RETAILER_ROLE_ID);
        }

        $this->db->order_by('created_date', 'DESC');
        $res = $this->db->get();
        //show($this->db->last_query());
        return $res->result_array();
        
    }

    function daily_ticket_wise_summary($data) {


        $from_date = date('Y-m-d', strtotime($data['from_date']));
        $to_date = date('Y-m-d', strtotime($data['to_date']));

        $this->db->select('1,tk.issue_time,sum(if(tk.transaction_status="success",1,"0")) as "sucess Tran",'
                . 'sum(tk.tot_fare_amt) as "Total Fare",'
                . 'sum(if(tk.transaction_status="success",tk.tot_fare_amt,"0")) as "Total_booked_amt",'
                . 'sum(if(tk.transaction_status="success",tk.fare_reservationCharge,"0")) as "Total Resrvaion charges",'
                . 'sum(if(tk.transaction_status="success",(tk.fare_acc+tk.fare_hr+tk.fare_it+tk.fare_octroi+tk.fare_toll+tk.fare_convey_charge),"0")) as "Total Tax",'
                . 'sum(if(tk.transaction_status="success",tk.num_passgr,"0")) as "Total_Pass",'
                . 'sum(if(tk.transaction_status="cancel",1,"0")) as "Total Cancel transaction",'
                . 'if(tk.transaction_status="cancel",count(tds.seat_no),"0") as "Total Cancel seats",'
                . 'sum(if(tk.transaction_status="cancel",ctd.cancel_charge,"0")) as "Total Cancel charges",'
                . 'sum(if(tk.transaction_status="cancel",ctd.refund_amt,"0")) as "Total refund amt",'
                . 'sum(if(tk.transaction_status in("failed","pfailed"),1,"0")) as "Total Failed Tran",'
                . 'sum(if(tk.transaction_status in("failed","pfailed"),tot_fare_amt_with_tax,"0")) as "Total Failed amount",'
                . 'sum(if(tk.transaction_status="success",1,"0")) as "Ticket sold"');

        $this->db->from('tickets tk');
        $this->db->join('ticket_details tds', 'tds.ticket_id=tk.ticket_id', 'inner');
        $this->db->join('cancel_ticket_data ctd', 'ctd.ticket_id=tk.ticket_id', 'left');
        if ((isset($data['from_date']) && isset($data['to_date'])) && (($data['from_date'] != "") && ($data['to_date'] != ""))) {
            if ($data['from_date'] != $data['to_date']) {
                $this->db->where("DATE_FORMAT(tk.issue_time, '%Y-%m-%d') BETWEEN '" . $from_date . "' AND '" . $to_date . "' ");
            } else {
                $this->db->where("DATE_FORMAT(tk.issue_time, '%Y-%m-%d')=", $from_date);
            }
        } else if ($data['from_date'] != '' && $data['to_date'] == '') {
            $this->db->where("DATE_FORMAT(tk.issue_time, '%Y-%m-%d')=", $from_date);
        } else if ($data['from_date'] == '' && $data['to_date'] != '') {
            $this->db->where("DATE_FORMAT(tk.issue_time, '%Y-%m-%d')=", $to_date);
        }
        if ($data['op_name'] != '') {
            $this->db->where("tk.op_name", $data['op_name']);
        }


        $this->db->group_by("DATE_FORMAT(issue_time,'%Y-%m-%d')");
        $res = $this->db->get();
        return $res->result_array();
    }

    function cal_summary_bos_tkt_details_rpt($input_array) {
        $this->db->select("CASE t.booked_from WHEN  'upsrtc' THEN IF(t.total_fare_without_discount IS NOT NULL,(t.total_fare_without_discount - t.tot_fare_amt),0) 
                        ELSE 0 END AS 'UPSRTC Additional Charges (3% or Rs.50)',
                        COUNT(t.ticket_id) as tot_ticket,sum(ctd.pg_refund_amount) pg_refund_amount,
                        if(t.`transaction_status` = 'cancel',SUM(t.`tot_fare_amt`),0) cancel_ticket_fare,
                        if(t.`transaction_status` = 'cancel',count(t.ticket_id),0) cancel_ticket,
                        if(t.`transaction_status` = 'cancel',count(t.num_passgr),0) cancel_psgre,
                        COUNT(t.ticket_id) AS 'No of Tickets Booked ',SUM(t.num_passgr) AS 'No of Passenger',
                        IF(t.`total_fare_without_discount` IS NOT NULL,SUM(t.`total_fare_without_discount`),
                        SUM(t.`tot_fare_amt`)) AS 'tot_fare_amt',SUM(t.`discount_value`) AS 'Discounted Amount',
                        SUM(t.`tot_fare_amt_with_tax`) AS 'Reveue'");

        $this->db->from('tickets t');
        $this->db->join('users u', 'u.id = t.inserted_by', 'inner');
        $this->db->join('cancel_ticket_data ctd', 'ctd.ticket_id=t.ticket_id', 'left');
        $this->db->where("t.`booked_from` IN ('etravelsmart', 'UPSRTC')");
        $this->db->join('payment_transaction_fss ptf', 'ptf.ticket_id=t.ticket_id', 'left');
        $this->db->join('api_provider ap', 't.provider_id = ap.id', 'inner');
        $this->db->where("t.`transaction_status` IN ('success', 'cancel') ");
        //   $this->db->where("t.`transaction_status` = 'success' OR t.`transaction_status` = 'cancel' ");


        /*  if(isset($input_array['provider_id']) && $input_array['provider_id']!='')
          {
          $this->datatables->where("provider_id", $input_array['provider_id'] );
          }

          if(isset($input_array['from_date']) && $input_array['from_date']!='')
          {
          $this->datatables->where("DATE_FORMAT(issue_time, '%Y-%m-%d')>=", date('Y-m-d', strtotime($input_array['from_date']) ) );
          }

          if(isset($input_array['till_date']) && $input_array['till_date']!='')
          {
          $this->datatables->where("DATE_FORMAT(issue_time, '%Y-%m-%d')<=", date('Y-m-d', strtotime( $input_array['till_date']) ) );
          }

          if(isset($input_array['tran_type']) && $input_array['tran_type']!='')
          {
          $this->datatables->where('t.transaction_status',$input_array['tran_type']);
          }

          if(isset($input_array['pnr_no']) && $input_array['pnr_no']!='')
          {
          $this->datatables->where('t.pnr_no',$input_array['pnr_no']);
          } */
        $res = $this->db->get();

        return $res->result_array();
    }

    function cal_summary_bos_tkt_count_details_rpt() {
        $this->db->select("DATE_FORMAT(t.inserted_date, '%Y-%m-%d'),(select COUNT(distinct u.id) from users u where u.salt !='' and u.password != '' and DATE_FORMAT(u.created_date, '%Y-%m-%d') = DATE_FORMAT(t.inserted_date, '%Y-%m-%d')) aa,"
                . "COUNT(t.ticket_id),SUM(t.num_passgr) ,SUM(t.tot_fare_amt) ,sum(t.discount_value),(sum(t.total_fare_without_discount) * sum(t.commision_percentage)/100) AS commission_value,"
                . "((sum(t.total_fare_without_discount) * sum(t.commision_percentage)/100) - (sum(t.discount_value)) ) AS Reveue");

        $this->db->from('tickets t');
        //  $this->db->join('users u', 'u.id = t.inserted_by', 'left');

        $this->db->group_by("DATE_FORMAT(inserted_date, '%Y-%m-%d')");
        $res = $this->db->get();

        return $res->result_array();
    }

    
    function bos_tkt_user_cnt() {
        $yest_date = date('Y-m-d', strtotime('-1 days'));
        $this->db->select("COUNT(distinct u.id) as dd ");
        $this->db->from('users u');
        $this->db->like("u.created_date", $yest_date);
        $this->db->where("u.salt !='' ");
        $this->db->where("u.password != '' ");
        $this->db->where("u.role_id= '7' ");
        $this->db->where("u.status= 'Y' ");
        $res = $this->db->get();
        //  show($this->db->last_query(),1);
        return $res->result_array();
    }

    function bos_tkt_details_count_excel($input) {//((sum(t.total_fare_without_discount) * sum(t.commision_percentage)/100) - (sum(t.discount_value)) ) 
        $from_date = date('Y-m-d', strtotime($input['from_date']));
        $till_date = date('Y-m-d', strtotime($input['till_date']));
        $this->db->select("0,date , IFNULL(count_uid,0) as tot_reg_users, IFNULL(count_ticketId,0) tot_tick, IFNULL(sum_num_psnger,0) tot_psgr,IFNULL(if_total_fare,0) tot_basic, IFNULL(sum_disc_val,0) tot_disc_val, IFNULL(if_can_charge,0) tot_can, IFNULL(if_refund_amt,0) tot_ref_amt,
                                    IFNULL(commission_value,0) commission_value, IFNULL(Reveue,0) Reveue ,actual_revenue");
        /* $sql_query = "(select (case when dd is null then created_date else dd end) date, count_uid, count_ticketId, sum_num_psnger ,if_total_fare, sum_disc_val, if_can_charge, if_refund_amt,
          commission_value, Reveue
          from (select date(u.created_date) created_date,count(u.id) count_uid  from users u where u.status = 'Y' AND u.role_id = 7 and u.salt !='' and u.password != '' group by substr(u.created_date, 1, 10)
          ) a

          left join (
          SELECT DATE_FORMAT(t.inserted_date, '%Y-%m-%d') as dd,COUNT(t.ticket_id) count_ticketId, SUM(t.num_passgr) sum_num_psnger ,
          IF(t.`total_fare_without_discount` IS NOT NULL,SUM(t.`total_fare_without_discount`),SUM(t.`tot_fare_amt`)) if_total_fare, sum(t.discount_value) sum_disc_val,
          IFNULL(sum(ctd.`cancel_charge`),0) if_can_charge, IFNULL(sum(ctd.`pg_refund_amount`),0) if_refund_amt,
          if(op_name='UPSRTC',sum(t.fare_convey_charge),format(sum(t.total_fare_without_discount * t.commision_percentage/100),2)) as commission_value,sum(t.tot_fare_amt_with_tax)  AS Reveue
          FROM `tickets` `t` LEFT JOIN `cancel_ticket_data` `ctd` ON `ctd`.`ticket_id` = `t`.`ticket_id`
          WHERE t.`transaction_status`IN ('success', 'cancel')  GROUP BY substr(t.inserted_date, 1, 10)
          ) b on a.created_date = b.dd

          union all

          select (case when created_date is null then dd else created_date end) date, count_uid, count_ticketId, sum_num_psnger ,if_total_fare, sum_disc_val, if_can_charge, if_refund_amt,
          commission_value, Reveue
          from (select date(u.created_date) created_date,count(u.id) count_uid  from users u where u.status = 'Y' AND u.role_id = 7 and u.salt !='' and u.password != '' group by substr(u.created_date, 1, 10)
          ) a

          right join (
          SELECT DATE_FORMAT(t.inserted_date, '%Y-%m-%d') as dd,COUNT(t.ticket_id) count_ticketId, SUM(t.num_passgr) sum_num_psnger ,IF(t.`total_fare_without_discount` IS NOT NULL,SUM(t.`total_fare_without_discount`),
          SUM(t.`tot_fare_amt`)) if_total_fare, sum(t.discount_value) sum_disc_val,  IFNULL(sum(ctd.`cancel_charge`),0) if_can_charge, IFNULL(sum(ctd.`pg_refund_amount`),0) if_refund_amt,
          if(op_name='UPSRTC',sum(t.fare_convey_charge),format(sum(t.total_fare_without_discount * t.commision_percentage/100),2)) as commission_value,
          sum(t.tot_fare_amt_with_tax) AS Reveue FROM `tickets` `t` LEFT JOIN `cancel_ticket_data` `ctd` ON `ctd`.`ticket_id` = `t`.`ticket_id`
          WHERE t.`transaction_status`IN ('success', 'cancel') "; */



        $sql_query = "(select (case when dd is null then created_date else dd end) date, `count_uid`, `count_ticketId`, 
                    `sum_num_psnger`, `if_total_fare`, `sum_disc_val`, `if_can_charge`, `if_refund_amt`, `commission_value`, 
                    Reveue,actual_revenue   
                    from (
                    select date(u.created_date) created_date, count(u.id) count_uid  from users u where u.status = 'Y'";
        /* AND u.role_id = 7 and u.salt !='' and u.password != '' */
        if ($input['role_id'] != '' && $input['role_id'] != '0') {
            if ($input['role_id'] == 'trimax') {
                $sql_query.= " AND u.is_employee ='1' and u.salt !='' and u.password != ''";
            } else {
				eval(FME_AGENT_ROLE_ID);
				if(in_array($input['role_id'],$fme_agent_role_id)){
               // if ($input['role_id'] == '16') {
                    $sql_query.= " AND u.verify_status ='Y' ";
                }
                $sql_query.= " AND u.role_id = '" . $input['role_id'] . "' and u.salt !='' and u.password != '' ";
//                if ($input['user_id'] != '') {
//                    $sql_query.= " AND u.id ='" . $input['user_id'] . "' ";
//                }
            }
        } else {

            if ($input['role_id'] == '0') {
                $sql_query.= " AND u.role_id = '7' and u.salt ='' and u.password = '' ";
            } else {
                $sql_query.= "  and u.salt !='' and u.password != ''";
            }
        }
        if ($input['user_id'] != '') {
            $sql_query.= " AND u.id ='" . $input['user_id'] . "' ";
        }
        $sql_query.= " group by  DATE_FORMAT(u.created_date, '%Y-%m-%d') ) a                                                              
                    left join (

                    SELECT DATE_FORMAT(t.inserted_date, '%Y-%m-%d') as dd, COUNT(t.ticket_id) count_ticketId, 
                    SUM(t.num_passgr) sum_num_psnger, 
                    /*IF(t.`total_fare_without_discount` IS NOT NULL, SUM(t.`total_fare_without_discount`), SUM(t.`tot_fare_amt`)) if_total_fare, */
                    SUM(t.`tot_fare_amt`) AS if_total_fare,
                    sum(t.discount_value) sum_disc_val, 
                    IF(ctd.is_refund=1,sum(ctd.`cancel_charge`),0) if_can_charge, 
                    IFNULL(sum(ctd.`pg_refund_amount`), 0) if_refund_amt, 
                    /*if(op_name='UPSRTC', sum(t.fare_convey_charge), 
                    format(sum(t.total_fare_without_discount * t.commision_percentage/100), 2)) as commission_value, */
                    FORMAT(SUM(IF(t.status = 'Y',
                            IF(op_name='UPSRTC',
                               t.fare_convey_charge,
                               (t.total_fare_without_discount * t.commision_percentage/100)),
                              '0')
                           ),2
                       ) AS commission_value,

                    /*sum(t.tot_fare_amt_with_tax)  AS Reveue  */
                    SUM(t.tot_fare_amt - t.`discount_value`) AS Reveue,
                    SUM(t.tot_fare_amt - t.`discount_value` - IFNULL(ctd.`pg_refund_amount`,'0')) AS actual_revenue
                     FROM `tickets` `t` 
                    LEFT JOIN `cancel_ticket_data` `ctd` ON `ctd`.`ticket_id` = `t`.`ticket_id`   ";

        if ($input['tran_type'] && $input['tran_type'] != 'select_status') {
            $status = $input['tran_type'];
            //$sql_query.= " WHERE t.`transaction_status`IN ('".$status."')  ";
            if ($status == 'failed') {
                $status = array('psuccess', 'pfailed', 'failed');
                $sql_query.= " WHERE t.`transaction_status`IN ('psuccess','pfailed','failed')  ";
                if ($input['user_id'] != '') {
                    $sql_query.= " AND t.inserted_by ='" . $input['user_id'] . "' ";
                }
            } else {
                $sql_query.= " WHERE t.`transaction_status`IN ('" . $status . "')  ";
                if ($input['user_id'] != '') {
                    $sql_query.= " AND t.inserted_by ='" . $input['user_id'] . "' ";
                }
            }
        }

        //  $sql_query.= " WHERE t.`transaction_status`IN ('".$status."')  ";
        if ($input['role_id'] != '') {
            $sql_query.= " and t.role_id= '" . $input['role_id'] . "' ";
        }
        $sql_query.= "GROUP BY  DATE_FORMAT(t.`inserted_date`, '%Y-%m-%d')

                    ) b on a.created_date = b.dd                                                                        

                    union all                                                                        
                    select (case when created_date is null then dd else created_date end) date, `count_uid`, `count_ticketId`, `sum_num_psnger`, 
                    `if_total_fare`, `sum_disc_val`, `if_can_charge`, `if_refund_amt`, `commission_value`, Reveue,actual_revenue                                    
                    from (select date(u.created_date) created_date, count(u.id) count_uid  from users u where u.status = 'Y'";
        /* AND u.role_id = 7 and u.salt !='' and u.password != '' */
        if ($input['role_id'] != '' && $input['role_id'] != '0') {
            if ($input['role_id'] == 'trimax') {
                $sql_query.= " AND u.is_employee ='1' and u.salt !='' and u.password != ''";
//                if ($input['user_id'] != '') {
//                    $sql_query.= " AND u.id ='" . $input['user_id'] . "' ";
//                }
            } else {
				eval(FME_AGENT_ROLE_ID);
				if(in_array($input['role_id'],$fme_agent_role_id)){
                //if ($input['role_id'] == '16') {
                    $sql_query.= " AND u.verify_status ='Y' ";
                }
                $sql_query.= " AND u.role_id = '" . $input['role_id'] . "' and u.salt !='' and u.password != '' ";
            }
        } else {
            if ($input['role_id'] == '0') {
                $sql_query.= " AND u.role_id = '7' and u.salt ='' and u.password = '' ";
            } else {
                $sql_query.= "  and u.salt !='' and u.password != ''";
            }
        }
        if ($input['user_id'] != '') {
            $sql_query.= " AND u.id ='" . $input['user_id'] . "' ";
        }
        $sql_query.= " group by  DATE_FORMAT(u.created_date, '%Y-%m-%d') ) a                                                           

                    right join (
                    SELECT DATE_FORMAT(t.inserted_date, '%Y-%m-%d') as dd, COUNT(t.ticket_id) count_ticketId, 
                    SUM(t.num_passgr) sum_num_psnger, 
                    /*IF(t.`total_fare_without_discount` IS NOT NULL, SUM(t.`total_fare_without_discount`), SUM(t.`tot_fare_amt`)) if_total_fare,*/ 
                    SUM(t.`tot_fare_amt`) AS if_total_fare,
                    sum(t.discount_value) sum_disc_val, 
                   IF(ctd.is_refund=1,sum(ctd.`cancel_charge`),0) if_can_charge, 
                    IFNULL(sum(ctd.`pg_refund_amount`), 0) if_refund_amt,

                    /*if(op_name='UPSRTC', sum(t.fare_convey_charge), format(sum(t.total_fare_without_discount * t.commision_percentage/100), 2)) as commission_value, */
                    FORMAT(SUM(IF(t.status = 'Y',
                            IF(op_name='UPSRTC',
                               t.fare_convey_charge,
                               (t.total_fare_without_discount * t.commision_percentage/100)),
                              '0')
                           ),2
                       ) AS commission_value,
                    /*sum(t.tot_fare_amt_with_tax) AS Reveue */
                    SUM(t.tot_fare_amt - t.`discount_value`) AS Reveue,
                    SUM(t.tot_fare_amt - t.`discount_value` - IFNULL(ctd.`pg_refund_amount`,'0')) AS actual_revenue
                    FROM `tickets` `t` 
                    LEFT JOIN `cancel_ticket_data` `ctd` ON `ctd`.`ticket_id` = `t`.`ticket_id` ";

        /* WHERE t.`transaction_status`IN ('success', 'cancel')  */
        if ($input['tran_type'] && $input['tran_type'] != 'select_status') {
            $status = $input['tran_type'];

            if ($status == 'failed') {
                $status = array('psuccess', 'pfailed', 'failed');
                $sql_query.= " WHERE t.`transaction_status`IN ('psuccess','pfailed','failed')  ";

                if ($input['user_id'] != '') {
                    $sql_query.= " AND t.inserted_by ='" . $input['user_id'] . "' ";
                }
            } else {
                $sql_query.= " WHERE t.`transaction_status`IN ('" . $status . "')  ";
                if ($input['user_id'] != '') {
                    $sql_query.= " AND t.inserted_by ='" . $input['user_id'] . "' ";
                }
            }
        }
        //$sql_query.= " and t.role_id= '7' ";
        if ($input['role_id'] != '') {
            $sql_query.= " and t.role_id= '" . $input['role_id'] . "' ";
        }
        $sql_query.= " GROUP BY  DATE_FORMAT(t.`inserted_date`, '%Y-%m-%d')) b on a.created_date = b.dd ) abc";

        if ($input['from_date']) {
            $sql_query.= " where date >='$from_date'";
        }
        if ($input['till_date']) {
            $sql_query.= " and date <='$till_date'";
        }

        $sql_query.= " group by date ";
        $this->db->from($sql_query);
        $res = $this->db->get();
        //  show($this->db->last_query());

        return $res->result_array();
    }

    function bos_tkt_details_excel_sheet($input) {

        $this->db->select('1,
                        t.ticket_id,
                        t.`transaction_status` AS "Transaction Status",
                        t.`boss_ref_no` AS "Boss Reference No",
                        t.pnr_no As "PNR NO",
                        t.ticket_ref_no AS "Ticket Ref No",
                        
                        ap.name as "Povider Name",
                        t.`op_name` AS "Operator Name",

                        t.from_stop_name as "From Stop",                      

                        t.till_stop_name as "To Stop",
                        

                        t.boarding_stop_name as "Boarding Stop",


                        date_format(t.`inserted_date`, "%Y-%m-%d") as "issued date",
                        dept_time as doj,

                        t.num_passgr as "No of passenger",

                        IFNULL(t.`tot_fare_amt`,0) AS "Basic Fare",
                        IFNULL(sum(td.service_tax),0) AS "Service Tax",
                        IFNULL(t.`total_fare_without_discount`,0) AS "Total Fare Without Discount",   
                       
                        
                        CASE t.`booked_from`    WHEN "upsrtc" THEN
        IF(t.`total_fare_without_discount` IS NOT NULL,(t.`total_fare_without_discount` - t.`tot_fare_amt`),0) ELSE 0 END AS "UPSRTC Additional Charges (3% or Rs.50)",
                        IFNULL(t.`discount_on`,"-") AS "Discount On",
                        IFNULL(t.`discount_type`,"-") AS  "Discount Type",
                        t.`discount_per` AS "Discount Percentage",
                        t.`discount_value` AS "Discount Price",
                        t.`tot_fare_amt_with_tax` AS "Total Fare Paid",
                        
                        IFNULL(ctd.`pg_refund_amount`,0) AS "Refund Amount",
                        IF(ctd.is_refund=1,sum(ctd.`cancel_charge`),0)  AS "Cancel Charge",
                        ptf.`pg_track_id` AS "PG Track Id",if(t.`transaction_status` = "cancel",count(t.ticket_id),0) cancel_ticket,
                        if(t.`transaction_status` = "cancel",count(t.num_passgr),0) cancel_psgre
                    ');
        $this->db->from('tickets t');

        $this->db->join('users u', 'u.id = t.inserted_by', 'inner');
        $this->db->join('ticket_details td', 'td.ticket_id = t.ticket_id', 'inner');
        $this->db->join('payment_transaction_fss ptf', 'ptf.ticket_id=t.ticket_id', 'left');
        $this->db->join('api_provider ap', 't.provider_id = ap.id', 'left');
        $this->db->join('cancel_ticket_data ctd', 'ctd.ticket_id = t.ticket_id', 'left');

        if ($input['operator_id']) {
            $this->db->where("booked_from", $input['operator_id']);
        }

        if ($input['from_date']) {
            $this->db->where("DATE_FORMAT(issue_time, '%Y-%m-%d')>=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if ($input['till_date']) {
            $this->db->where("DATE_FORMAT(issue_time, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['till_date'])));
        }


        if (isset($input['trans_type']) && $input['trans_type'] != 'select_status') {
            $status = $input['trans_type'];
            if ($status == 'failed') {
                $status = array('psuccess', 'pfailed');
            }
            $this->db->where_in('t.transaction_status', $status);
            //  $this->db->where('t.transaction_status', $input['trans_type']);
        }
        $this->db->order_by('t.inserted_date', 'desc');

        $this->db->group_by('t.ticket_id');
        $res = $this->db->get();
        return $res->result_array();
    }
    
	    

    function bos_tkt_details_count_cron($date,$till_date = "",$for="day")
    {
        $default_array = array(
                                'success_ticket_cnt' => '0',
                                'cancel_ticket_cnt' => '0',
                                'success_total_passangers' => '0',
                                'cancel_total_passangers' => '0',
                                'total_fare_without_discount' => '0',
                                'discount_value' => '0',
                                'total_commission' => '0',
                                'total_cancel_charge' => '0',
                                'total_refund_amount' => '0',
                                'agent_ticket_cnt'    => '0',
                                'reg_usr_ticket_cnt' => '0',
                                'other_ticket_cnt' => '0',
                                'agent_passangers' => '0',
                                'registerd_passangers' => '0',
                                'other_passangers' => '0',
                              );

        $this->db->select('DATE_FORMAT(t.inserted_date, "%d-%m-%Y") AS booking_date,'
                . 'SUM(if(t.transaction_status = "success",1,0)) AS success_ticket_cnt, '
                . 'SUM(if(t.transaction_status = "success" and t.role_id = "16",1,0)) AS  agent_ticket_cnt, '
                . 'SUM(if(t.transaction_status = "success" and t.role_id = "7",1,0)) AS  reg_usr_ticket_cnt, '
				 . 'SUM(if(t.transaction_status = "success" and t.role_id = "27",1,0)) AS  fme_ticket_cnt, '
                . 'SUM(if(t.transaction_status = "success" and t.role_id not in ("16","7","27"),1,0)) AS other_ticket_cnt, '
                . 'SUM(if(t.transaction_status = "cancel",1,0)) AS cancel_ticket_cnt, '
                . 'SUM(if(t.transaction_status = "success",t.num_passgr,0)) AS success_total_passangers, '
                . 'SUM(if(t.transaction_status = "success" and t.role_id = "16", t.num_passgr,0)) AS agent_passangers, '
                . 'SUM(if(t.transaction_status = "success" and t.role_id = "7",t.num_passgr,0)) AS registerd_passangers, '
				 . 'SUM(if(t.transaction_status = "success" and t.role_id = "27",t.num_passgr,0)) AS  fme_passangers, '
                . 'SUM(if(t.transaction_status = "success" and t.role_id not in ("16","7","27") , t.num_passgr,0)) AS other_passangers, '
                . 'SUM(if(t.transaction_status = "cancel",t.num_passgr,0)) AS cancel_total_passangers, '
                . 'ROUND(SUM(t.total_fare_without_discount),2) AS total_fare_without_discount, '
                . 'ROUND(SUM(t.discount_value),2) AS discount_value, '
                . 'ROUND(SUM(IF(t.booked_from = "upsrtc", t.fare_convey_charge, IF(t.booked_from = "etravelsmart", (t.total_fare_without_discount*t.commision_percentage/100), "0"))), 2) AS total_commission, '
                . 'ROUND(SUM(IF(ctd.is_refund = "1", ctd.cancel_charge, "0")),2) AS total_cancel_charge, '
                . 'ROUND(SUM(IF(ctd.is_refund = "1", ctd.actual_refund_paid, "0")), 2) AS total_refund_amount',false); 
        $this->db->from('tickets t');
        $this->db->join('cancel_ticket_data ctd', 'ctd.ticket_id = t.ticket_id', 'left');
        if($for == "day")
        {
            $this->db->like("t.inserted_date",$date);
            $this->db->group_by("DATE_FORMAT(t.inserted_date, '%d-%m-%Y')");    
        }
        else if($for == "month" && $date != "" && $till_date != "")
        {
            $this->db->where('t.inserted_date >=', $date." 00:00:00");
            $this->db->where('t.inserted_date <=', $till_date." 23:59:59");
            $this->db->group_by("DATE_FORMAT(t.inserted_date, '%m-%Y')");
        }
        else if($for == "year")
        {
            $this->db->where('t.inserted_date >=', $date." 00:00:00");
            $this->db->where('t.inserted_date <=', $till_date." 23:59:59");
            $this->db->group_by("DATE_FORMAT(t.inserted_date, '%Y')");
        }
        
        $res = $this->db->get();

        $report = $res->result_array();
        if($report)
        {
            return $report[0];
        }
        else
        {
            return $default_array;
        }
    }

    function registered_user($date,$till_date = "",$for="day")
    {
        eval(REGISTERED_USERS_ID_ARRAY);
        $users_where = [
                            "username != " => "",
                            "password != " => "",
                            "salt != " => "",
                            "status" => "Y",
                            "verify_status" => "Y",
                        ];

        $this->db->select('count(id) as total_users',false);
        $this->db->from('users');
        $this->db->where($users_where);
        $this->db->where_in($registered_users_id_array);
        if($for == "day")
        {
            $this->db->like("created_date",$date);
            $this->db->group_by("DATE_FORMAT(created_date, '%d-%m-%Y')");    
        }
        else if($for == "month" && $date != "" && $till_date != "")
        {
            $this->db->where('created_date >=', $date." 00:00:00");
            $this->db->where('created_date <=', $till_date." 23:59:59");
            $this->db->group_by("DATE_FORMAT(created_date, '%m-%Y')");
        }
        else if($for == "year")
        {
            $this->db->where('created_date >=', $date." 00:00:00");
            $this->db->where('created_date <=', $till_date." 23:59:59");
            $this->db->group_by("DATE_FORMAT(created_date, '%Y')");
        }

        $res = $this->db->get();
        $user_report = $res->result_array();
        if($user_report)
        {
            return $user_report[0];
        }
        else
        {
            return array('total_users' => 0);
        }
    }

    function user_type_data($data) {
        $this->db->distinct();
        $this->db->select(" u.id,concat(u.first_name,' ',u.last_name) as user_name,u.email ");
        $this->db->from("users u");
        $this->db->join("tickets t", 'u.id=t.inserted_by', 'inner');
        $this->db->order_by("u.first_name");

        if (isset($data['role_id']) && $data['role_id'] != '' && ($data['role_id'] == 'trimax'))
        {
            $this->db->where("u.is_employee", '1');
        } 
        else if (isset($data['role_id']) && $data['role_id'] != '' && $data['role_id'] != 'trimax')
        {
            if ($data['role_id'] == 'sys_user')
            {
                $data = array(1, 2, 13);
                $this->db->where_in("u.role_id", $data);
            } 
            else
            {
                $this->db->where("t.role_id", $data['role_id']);
            }
        }
        $menu_result = $this->db->get();

        $menu_array = $menu_result->result_array();

        return $menu_array;
        
    }


    function users_data($data) {
        $this->db->distinct();
        $this->db->select(" u.id,concat(u.first_name,' ',u.last_name) as user_name,u.email ");
        $this->db->from("users u");
        $this->db->join("tickets t", 'u.id=t.inserted_by', 'left');
        $this->db->order_by("u.first_name", "asc");

        if (isset($data['role_id']) && $data['role_id'] != '' && ($data['role_id'] == 'trimax')) {
            $this->db->where("u.is_employee", '1');
        }
        if (isset($data['role_id']) && $data['role_id'] != '' && $data['role_id'] != 'trimax') {

            $this->db->where_in("t.role_id", $data['role_id']);   //FME TEST THIS BEFORE 
        }
        $menu_result = $this->db->get();
        $menu_array = $menu_result->result_array();

        return $menu_array;
        
    }
   function bos_commission_offer($input) {
      
        $role_id_name = ""; 
        $role_id_name = $input['role_id'];
        $employees_in = [];

        if($role_id_name == "trimax")
        {
            $this->db->select('group_concat(id) as employees')->from('users')->where("is_employee","1");
            $employee_query = $this->db->get();
            if($employee_query->num_rows())
            {
                $emp_data = $employee_query->result_array();
                $emp_data = $emp_data[0]["employees"];
                $employees_in = explode(",", $emp_data);
            } 
        }

        $this->db->select('1,
                        t.`boss_ref_no` AS "Boss Reference No",
                        t.pnr_no As "PNR NO",t.api_ticket_no,
                        t.`booked_by` AS "user type",
                        td.psgr_name as first_name,
                        t.user_email_id as email,
                        u.id as agent_id,
                        t.mobile_no,
                        sm.stState,
                        cm.stCity,
                        t.`op_name` AS "Operator Name",
                        ap.name as "Povider Name",
                        t.from_stop_name as "From Stop",                      
                        t.till_stop_name as "To Stop",
                        t.`inserted_date` as "issued date",
                        t.dept_time as doj,
                        t.num_passgr as "No of passenger",
                        IFNULL(t.`tot_fare_amt`,0) AS "Basic Fare",
                        CASE t.booked_from 
                        WHEN "etravelsmart" THEN sum(td.service_tax)  
                        WHEN "upsrtc" THEN sum(td.service_tax)
                        WHEN "msrtc" THEN sum(td.service_tax)  
                        WHEN "hrtc" THEN (t.service_tax)
                        WHEN "rsrtc" THEN sum(td.service_tax)
                        WHEN "infinity" THEN sum(td.service_tax)
                        END AS "Service Tax",
                        IFNULL(t.`total_fare_without_discount`,0) AS "Total Fare Without Discount",   
                        CASE t.`booked_from`  
                        WHEN "upsrtc" THEN
                        IF(t.`total_fare_without_discount` IS NOT NULL,(t.`total_fare_without_discount` - t.`tot_fare_amt`),0) ELSE 0 END AS "UPSRTC Additional Charges (3% or Rs.50)",
                        IFNULL(t.`discount_on`,"-") AS "Discount On",
                        IFNULL(t.`discount_type`,"-") AS  "Discount Type",
                        t.`discount_per` AS "Discount Percentage",
                        t.`discount_value` AS "Discount Price",
                        t.`tot_fare_amt_with_tax` AS "Total Fare Paid",
                        
                        IFNULL(ctd.`pg_refund_amount`,0) AS "Refund Amount",
                       
                        IF(ctd.is_refund=1,sum(ctd.`cancel_charge`),0) "Cancel Charge",
                       t.pg_tracking_id AS "PG Track Id",
                       t.payment_by as payment_by,
                       t.`transaction_status` AS "Transaction Status",  
                       t.ticket_ref_no,
                        CASE t.booked_from 
                        WHEN "etravelsmart" THEN t.commision_percentage 
                        WHEN "upsrtc" THEN "3 % or 50 "
                        WHEN "msrtc" THEN ('.MSRTC_COMMISSION_PERCENTAGE.')  
                        WHEN "hrtc" THEN ('.HRTC_COMMISSION_PERCENTAGE.')
                        WHEN "rsrtc" THEN t.fare_convey_charge
                        WHEN "infinity" THEN t.commision_percentage
                        END AS "commission_percentage",
                       
                        CASE t.booked_from 
                        WHEN "etravelsmart" THEN round(((t.tot_fare_amt * t.commision_percentage)/ 100),2)
                        WHEN "upsrtc" THEN round(t.fare_convey_charge ,2) 
                        WHEN "msrtc" THEN round(t.tot_fare_amt * '.MSRTC_COMMISSION_PERCENTAGE.' / 100 ,2)  
                        WHEN "hrtc" THEN round(t.tot_fare_amt * '.HRTC_COMMISSION_PERCENTAGE.' / 100,2)
                        WHEN "rsrtc" THEN t.fare_convey_charge
                        WHEN "infinity" THEN round(((t.tot_fare_amt * t.commision_percentage)/ 100),2)
                        ELSE 0
                        END AS "commission_value"
                    ');
        $this->db->from('tickets t');

        /*$this->db->join('users u', 'u.id = t.inserted_by', 'inner');*/
        $this->db->join('ticket_details td', 'td.ticket_id = t.ticket_id', 'inner');
        $this->db->join('payment_transaction_fss ptf', 'ptf.ticket_id=t.ticket_id', 'left');
        $this->db->join('users u', 't.inserted_by = u.id', 'left');
        $this->db->join('state_master sm', 'u.state = sm.intStateId', 'left');
        $this->db->join('city_master cm', 'u.city = cm.intCityId', 'left');
        $this->db->join('api_provider ap', 't.provider_id = ap.id', 'left');
        $this->db->join('cancel_ticket_data ctd', 'ctd.ticket_id = t.ticket_id', 'left');

        if($input['provider_id'])
        {
            $this->db->where("provider_id", $input['provider_id']);
        }
         if($input['state_type'])
        {
            $this->db->where('sm.intStateId', $input['state_type']);
        }
        if ($input['city_type']) 
        {
            $this->db->where('cm.intCityId', $input['city_type']);
        }
        if($input['from_date'])
        {
            $this->db->where("DATE_FORMAT(issue_time, '%Y-%m-%d')>=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if($input['till_date'])
        {
            $this->db->where("DATE_FORMAT(issue_time, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['till_date'])));
        }

        if($input['tran_type'] && $input['tran_type']!='select_status')
        {
            $status=$input['tran_type'];
            if($status=='failed')
            {
                $status=array('psuccess','pfailed','failed');
            }
             else if($status=='success' || $status=='cancel')
            {
                $this->db->where('t.pnr_no != ', "");
            }
            $this->db->where_in('t.transaction_status', $status);
        }
        if($input['payment_by'] && $input['payment_by'] != '')
        {
            $this->db->where('t.payment_by', $input['payment_by']);
        }

        
       eval(SYSTEM_USERS_ARRAY);
        eval(FME_AGENT_ROLE_NAME);
        if(in_array(strtolower($this->session->userdata("role_name")), $system_users_array))
        {
            if($role_id_name!='' && !$input['user_id'])
            {
                if(!in_array($role_id_name, array("trimax","sys_user")))
                {
                    $this->db->where("t.role_id", $input['role_id']);
                }
                else if($role_id_name == "trimax")
                {
                    $this->db->where_in("t.inserted_by", $employees_in);
                }
                else if($role_id_name == "sys_user")
                {
                    eval(SYSTEM_USERS_ID_ARRAY);
                    $this->db->where_in("t.role_id", $system_users_id_array);
                }
            }

            if($input['user_id'])
            {
                $this->db->where("t.inserted_by", $input['user_id']);
            }
        }

        else if(in_array(strtolower($role_id_name),$fme_agent_role_name))
        {
            $this->db->where("t.inserted_by", $input['user_id']);
        }
        
        $this->db->group_by('t.ticket_id');

        $res = $this->db->get();
        //show(last_query(),1);
        return $res->result_array();
       
    }
    
     /* @Author      : Sagar Bangar
      * @function    : incomplete_trasactions_data
      * @param       : get param transaction_status, from_date, to_date, user_type
      * @detail      : incomplete_trasactions_data
      *                 
      */
    
   function incomplete_trasactions_data($input) {
      
        $role_id_name = ""; 
        $role_id_name = $input['role_id'];
        $employees_in = [];

        if($role_id_name == "trimax")
        {
            $this->db->select('group_concat(id) as employees')->from('users')->where("is_employee","1");
            $employee_query = $this->db->get();
            if($employee_query->num_rows())
            {
                $emp_data = $employee_query->result_array();
                $emp_data = $emp_data[0]["employees"];
                $employees_in = explode(",", $emp_data);
            } 
        }

        $this->db->select('1,  t.ticket_id As "ticket_id",
                    t.`booked_by` AS "user_type",
                    td.psgr_name as first_name,
                    t.user_email_id as email,
                    t.mobile_no,
                    t.`op_name` AS "operator_name",
                    ap.name as "provider_name",
                    t.from_stop_name as "from_stop",                      
                    t.till_stop_name as "to_stop",
                    t.`inserted_date` as "issued_date",
                    t.dept_time as doj,
                    t.num_passgr as "no_of_passenger",
                    t.`tot_fare_amt_with_tax` AS "ticket_fare",
                    t.`transaction_status` AS "transaction_Status"');
        $this->db->from('tickets t');

        /*$this->db->join('users u', 'u.id = t.inserted_by', 'inner');*/
        $this->db->join('ticket_details td', 'td.ticket_id = t.ticket_id', 'inner');
        $this->db->join('api_provider ap', 't.provider_id = ap.id', 'left');
        
        if($input['operator_id'] && $input['operator_id'] != 'Etravelsmart')
        {
            $this->datatables->where("op_id", $input['operator_id']);
        }
        if($input['operator_id'] && $input['operator_id'] == 'Etravelsmart')
        {
            $this->datatables->where("provider_id", ETRAVELSMART_PROVIDER_ID);
        }
        
        if($input['from_date'] && $input['till_date'])
        {
            $this->datatables->where("issue_time between '".date('Y-m-d 00:00:01', strtotime($input['from_date']))."' and '".date('Y-m-d 23:59:59', strtotime($input['till_date']))."'");
        }

        if($input['tran_type'] && $input['tran_type']!='select_status')
        {
            $status=$input['tran_type'];
            if($status=='failed')
            {
                $status=array('psuccess','pfailed','failed');
            }
            $this->db->where_in('t.transaction_status', $status);
        }

        if($role_id_name && !$input['user_id'])
        {
            if(!in_array($role_id_name, array("trimax","sys_user")))
            {
                $this->db->where("t.role_id", $input['role_id']);
            }
            else if($role_id_name == "trimax")
            {
                $this->db->where_in("t.inserted_by", $employees_in);
            }
            else if($role_id_name == "sys_user")
            {
                eval(SYSTEM_USERS_ID_ARRAY);
                $this->db->where_in("t.role_id", $system_users_id_array);
            }
        }

        if(!empty($input['user_id']))
        {
            $this->db->where("t.inserted_by", $input['user_id']);
        }
        
        $this->db->group_by('t.ticket_id');

        $res = $this->db->get();
        //show(last_query(),1);
        return $res->result_array();
       
    }


     /* @Author      : Rajkumar Shukla
      * @function    : bos_tkt_details_count_summary
      * @param       : request_type, user_type, user, From_date, Till_date
      * @detail      : bos_tkt_details_count_summary
      *                 
      */
            
    
    function tkt_details_view_summary($input) {
            $employees_in = [];
            if($input && isset($input['user_type']) && $input['user_type'] == "trimax" && $input['select_user'] == "")
            {   
                $this->db->select ('GROUP_CONCAT(u.id) as employee_id',false);
                $this->db->from('users u');
                $this->db->where("is_employee" ,'1');
                $employee = $this->db->get();
                $employeeids = $employee->result_array();
                if($employeeids)
                {
                    $employeeids = $employeeids[0]["employee_id"];
                    $employees_in = explode(",", $employeeids);
                }
            }
            else if($input && isset($input['select_user']) && $input['select_user'] != "")
            {
              $employees_in = [$input['select_user']];
            }
            
            $this->db->select(
                                'COUNT(t.`ticket_id`) AS total_ticket_booked,
                                SUM(t.`num_passgr`) AS total_passangers,
                                SUM(t.`total_fare_without_discount`) AS total_fare_without_discount,
                                SUM(t.`discount_value`) AS discount_value,
                                SUM(IF(t.`booked_from` = "upsrtc", t.`fare_convey_charge`,
                                         IF(t.`booked_from` = "etravelsmart", (t.`total_fare_without_discount`*t.`commision_percentage`/100), "0")
                                       )) AS total_commission,
                                SUM(IF(ctd.`is_refund` = "1",ctd.`cancel_charge`,"0")) AS total_cancel_charge,
                                SUM(IF(ctd.is_refund = "1",ctd.`actual_refund_paid`,"0")) AS total_refund_amount',FALSE);
            
             $this->db->from('tickets t'); 
             $this->db->join("cancel_ticket_data ctd", "ctd.ticket_id = t.ticket_id","left");
            
            if (isset($input['from_date']) && $input['from_date'] != "" && isset($input['till_date']) && $input['till_date'] != "" )
            {
                $this->db->where("DATE_FORMAT(issue_time, '%Y-%m-%d')>=", date('Y-m-d', strtotime($input['from_date'])));
                $this->db->where("DATE_FORMAT(issue_time, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['till_date'])));
            }

            if(isset($input['trans_type']) && $input['trans_type'] != "" && $input['trans_type'] != "all" )
            {
                 $this->db->where("t.transaction_status", $input['trans_type']);
            }
             
            if($input && $input['user_type'] != "" && !in_array($input['user_type'], ["trimax","all"]))
            {
                $this->db->where("t.role_id", $input['user_type']); 
            }
             
            if($employees_in)
            {
                $this->db->where_in("t.inserted_by", $employees_in);
            }
             
            /*if($input && $input['select_user'] != "" && $input['user_type'] != "trimax")
            {
                $this->db->where("t.inserted_by", $input['select_user']);
            }*/

            $query = $this->db->get();
            $result['data'] = $query->result_array();
            return $result['data'] ;
        }
    /* @Author      : Rajkumar Shukla
     * @function    : get_alluser
     * @param       : 
     * @detail      : To fetch all users from user table.
     */
    function get_registerd_users($input) 
    {
        if(isset($input['from_date']) && $input['from_date'] != "" && isset($input['till_date']) && $input['till_date'] != "" )
        {
            $this->db->where("DATE_FORMAT(created_date, '%Y-%m-%d')>=", date('Y-m-d', strtotime($input['from_date'])));
            $this->db->where("DATE_FORMAT(created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['till_date'])));
        }

        
        $this->db->where ("password != ''");
        $this->db->where ("salt != ''");
        $this->db->where ("verify_status = 'Y'");
        $this->db->where ("role_id", USER_ROLE_ID);
        $this->db->from  ('users');
        $result['data'] = $this->db->count_all_results();
        return $result['data'];
    }

    function summary_bos_tkt_detail_support($input)
    {
       $employees_in = [];
        if($input && isset($input['user_type']) && $input['user_type'] == "trimax" && $input['select_user'] == "")
        {   
            $this->db->select ('GROUP_CONCAT(u.id) as employee_id',false);
            $this->db->from('users u');
            $this->db->where("is_employee" ,'1');
            $employee = $this->db->get();
            $employeeids = $employee->result_array();
            if($employeeids)
            {
               $employeeids = $employeeids[0]["employee_id"];
               $employees_in = explode(",", $employeeids);
            }
        }
        else if($input && isset($input['select_user']) && $input['select_user'] != "")
        {
          $employees_in = [$input['select_user']];
        }

        $this->db->select(
                          'COUNT(t.`ticket_id`) AS total_ticket_booked,
                          SUM(t.`num_passgr`) AS total_passangers,
                          SUM(t.`tot_fare_amt`) AS total_basic_fare,
                          SUM(IF(t.`booked_from` = "upsrtc", t.`fare_convey_charge`,"0")) AS upsrtc_additional_charges,
                          SUM(t.`discount_value`) AS discount_value,
                          SUM(IF(ctd.`is_refund` = "1",ctd.`cancel_charge`,"0")) AS total_cancel_charge,
                          SUM(IF(ctd.`is_refund` = "1",ctd.`actual_refund_paid`,"0")) AS total_refund_amount'
                         );

        $this->db->from('tickets t');
        $this->db->join('api_provider ap', 't.provider_id = ap.id', 'left');
        $this->db->join('users u', 't.inserted_by = u.id', 'left');
        $this->db->join('state_master sm', 'u.state = sm.intStateId', 'left');
        $this->db->join("cancel_ticket_data ctd", "ctd.ticket_id = t.ticket_id","left");

        if(isset($input['provider_id']) && $input['provider_id'] !="")
        {
            $this->db->where("t.provider_id", $input['provider_id']);
        }
        if(isset($input['state_type']) && $input['state_type'] !="")
        {
            $this->db->where('sm.intStateId', $input['state_type']);
        }
        if(isset($input['from_date']) && $input['from_date'] != "" && isset($input['till_date']) && $input['till_date'] != "")
        {
            $this->db->where("DATE_FORMAT(inserted_date, '%Y-%m-%d') >= ", date('Y-m-d', strtotime($input['from_date'])));
            $this->db->where("DATE_FORMAT(inserted_date, '%Y-%m-%d') <= ", date('Y-m-d', strtotime($input['till_date'])));
        }

        if(isset($input['trans_type']) && $input['trans_type'] != "" && $input['trans_type'] != "all" )
        {
            $this->db->where("t.transaction_status", $input['trans_type']);
        }

        if(isset($input['user_type']) && !in_array($input['user_type'], ["trimax","all"]))
        {
            if($input['user_type'] == "sys_user")
            {
                eval(SYSTEM_USERS_ID_ARRAY);
                $this->db->where_in("t.role_id", $system_users_id_array); 
            }
            else
            {
                $this->db->where("t.role_id", $input['user_type']); 
            }

            if(!empty($input) && isset($input['select_user']) && $input['select_user'] != "")
            {
                $this->db->where("t.inserted_by", $input['select_user']);
            }
        }
        else if($employees_in && isset($input['user_type']) && in_array($input['user_type'], ["trimax","sys_user"]))
        {
            $this->db->where_in("t.inserted_by", $employees_in);
        }

        /*if(!empty($input) && isset($input['select_user']) && $input['select_user'] != "" && isset($input['user_type']) && !in_array($input['user_type'], ["trimax","sys_user"]))
        {
            $this->db->where("t.inserted_by", $input['select_user']);
        }*/

        $query = $this->db->get();
        $result = $query->result_array();

        return $result;
        
    }

    function user_type_data_support($data) {
        $this->db->distinct();
        $this->db->select(" u.id,concat(u.first_name,' ',u.last_name) as user_name,u.email ");
        $this->db->from("users u");
        $this->db->join("tickets t", 'u.id=t.inserted_by', 'inner');
        $this->db->order_by("u.first_name");

        if (isset($data['role_id']) && $data['role_id'] != '' && ($data['role_id'] == 'trimax'))
        {
            $this->db->where("u.is_employee", '1');
        } 
        else if (isset($data['role_id']) && $data['role_id'] != '' && $data['role_id'] != 'trimax')
        {
            if ($data['role_id'] == 'sys_user')
            {
                eval(SYSTEM_USERS_ID_ARRAY);
                $this->db->where_in("u.role_id", $system_users_id_array);
            } 
            else
            {
                $this->db->where("t.role_id", $data['role_id']);
            }
        }
        $menu_result = $this->db->get();

        $menu_array = $menu_result->result_array();
        return $menu_array;
        
    }
     /* @Author      : Sagar Bangar
      * @function    : get_agent_ticket_count_report
      * @param       : pagination param
      * @detail      : get payment ticket
      *                 
      */
    function get_agent_ticket_count_report($per_page, $page){
		eval(FME_AGENT_ROLE_ID);
       $page	= ($page>0?$page:1);		
        $offset	= $per_page*($page-1);
        $this->db->select('concat(users.first_name," ",users.last_name) as Agent_Name,users.id,users.mobile_no,cm.stCity as city_name,sm.stState as state_name,rm.referrer_name, max(tickets.issue_time) as Last_Ticket_Booked_Date, sum(agent_tickets.markup_Value) as Till_Date_Markup_Value, count(tickets.ticket_id) as Till_Date_Booking_Count, sum(tickets.total_fare_without_discount - tickets.tot_fare_amt_with_tax - IF(agent_tickets.markup_value != NULL, agent_tickets.markup_value, 0) ) as Till_Date_Commission, sum(tot_fare_amt_with_tax) as Till_Date_Revenue, tickets.inserted_by,sum(num_passgr) as passengers');
	$this->db->from('tickets');
        $this->db->join('users','users.id = tickets.inserted_by','left');
        $this->db->join('agent_tickets','agent_tickets.ticket_id = tickets.ticket_id','left');
        $this->db->join('referrer_master rm','rm.referrer_code = users.referrer_code','left');
        $this->db->join('city_master cm','cm.intCityId = users.city','left');
        $this->db->join('state_master sm','sm.intStateId = users.state','left');
        $this->db->where_in('tickets.role_id',$fme_agent_role_id);
        if(!empty($this->session->userdata('trans_type'))){
          $this->db->where('tickets.transaction_status', $this->session->userdata('trans_type'));
        }
        if(!empty($this->session->userdata('select_agent'))){
            $this->db->where('users.id',$this->session->userdata('select_agent'));
        }
        if(!empty($this->session->userdata('select_state'))){
            $this->db->where('users.state', $this->session->userdata('select_state'));
        }
        if(!empty($this->session->userdata('select_city'))){
          $this->db->where('users.city', $this->session->userdata('select_city'));
        }
        if(!empty($this->session->userdata('select_referrer'))){
          $this->db->where('rm.id', $this->session->userdata('select_referrer'));
        }
        if(empty($this->session->userdata('trans_type'))){
          $this->db->where('tickets.transaction_status','success');
        }
      
        $this->db->group_by('tickets.inserted_by');
        $this->db->order_by('Till_Date_Booking_Count','desc');
        if(!empty($this->session->userdata('select_limit'))){
          $this->db->limit($this->session->userdata('select_limit'), 0);
        }else if($per_page > 0)
        {
          //$this->db->limit($per_page, $offset);
        }
        $result = $this->db->get();
        return $result->result_array();
        
    }
    
     /* @Author      : Sagar Bangar
      * @function    : get_agent_ticket_count_report_date
      * @param       : pagination param
      * @detail      : get payment ticket
      *                 
      */
    function get_agent_ticket_count_report_date($inserted_by){
		eval(FME_AGENT_ROLE_ID);
        $this->db->select('sum(agent_tickets.markup_value) as Till_Date_Markup_Value_Range, count(tickets.ticket_id) as Till_Date_Booking_Count_Range, sum(tickets.total_fare_without_discount - tickets.tot_fare_amt_with_tax - IF(agent_tickets.markup_value != NULL, agent_tickets.markup_value, 0) ) as Till_Date_Commission_Range, sum(tot_fare_amt_with_tax) as Till_Date_Revenue_Range, tickets.inserted_by,sum(num_passgr) as till_date_passengers');
        $this->db->from('tickets');
        $this->db->join('users','users.id = tickets.inserted_by','left');
        $this->db->join('agent_tickets','agent_tickets.ticket_id = tickets.ticket_id','left');
        if(!empty($this->session->userdata('select_agent'))){
            $this->db->where('users.id',$this->session->userdata('select_agent'));
        }
        $this->db->where_in('tickets.role_id', $fme_agent_role_id);
        $this->db->where_in('tickets.inserted_by', $inserted_by);
        if(empty($this->session->userdata('till_date'))){
            $this->session->set_userdata('till_date',date('Y-m-d 23:59:59'));
        }
            
        if(!empty($this->session->userdata('trans_type'))){
            $this->db->where('tickets.transaction_status', $this->session->userdata('trans_type'));
        }
        if(!empty($this->session->userdata('select_state'))){
            $this->db->where('users.state', $this->session->userdata('select_state'));
        }
        
        if(!empty($this->session->userdata('select_city'))){
            $this->db->where('users.city', $this->session->userdata('select_city'));
        }
        
        if(!empty($this->session->userdata('from_date')) && !empty($this->session->userdata('till_date')))
         $this->db->where("tickets.issue_time between '".date('Y-m-d 00:00:01', strtotime($this->session->userdata('from_date')))."' and '".date('Y-m-d 23:59:59', strtotime($this->session->userdata('till_date')))."'");
        $this->db->group_by('tickets.inserted_by');
        $result = $this->db->get();
        return $result->result_array();
        
    }
    function agent_type_data($input)
    {
        $this->db->distinct();
        $this->db->select(" u.id,concat(u.first_name,' ',u.last_name) as user_name,u.email ");
        $this->db->from("users u");
        $this->db->order_by("u.first_name");

        if (isset($input['role_id']) && $input['role_id'] != '')
        {
            $this->db->where("u.role_id", $input['role_id']);
        } 
        
        $menu_result = $this->db->get();
        $menu_array = $menu_result->result_array();
  
        return $menu_array;
         
    }

     function agent_registeraion_detail($input) {
       
        $ids = array('16','17', '18','27');
        
        $this->db->select('1,DATE_FORMAT(created_date,"%d-%m-%Y %h:%i:%s") as "Reg_Date"
                            ,concat(first_name," ",last_name) AS "AgentName"
                            ,u.id as AgentId
                            ,rm.referrer_name AS "ReferrerName"
                            ,rm.referrer_code AS "ReferrerCode"
                            ,sm.stState AS "State"
                            ,cm.stCity AS "City"
                            ,mobile_no AS "MobileNo"
                            ,verify_status AS "VerifyStatus"
                            ,email AS "Email"');
        $this->db->from('users u');
        $this->db->join('state_master sm', 'u.state=sm.intStateId', 'left');
        $this->db->join('city_master cm', 'cm.intCityId=u.city', 'left');
        $this->db->join('referrer_master rm', 'rm.referrer_code=u.referrer_code', 'left');
        $this->db->where_in('u.role_id ',$ids);

        $this->db->where("u.salt !='' and u.password != ''");
      
        if (isset($input['from_date']) && $input['from_date']!='') 
        {
            $this->db->where("DATE_FORMAT(u.created_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['to_date']) && $input['to_date']!='') 
        {
            $this->db->where("DATE_FORMAT(u.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }
        if(isset($input['role_id']) && $input['role_id'] !='')
        {
            $this->db->where("u.role_id", $input['role_id']);
        } 
        if($input['user_id']) 
        {
            $this->db->where("u.id", $input['user_id']);
        }
      
        $res = $this->db->get();
        
        return $res->result_array();
    }


    public function tickets_status_analytics($input)
    {
        $this->db->select("DATE_FORMAT(inserted_date,'%d-%m-%Y') AS date ,COUNT(ticket_id) AS total,transaction_status",false);
        $this->db->from('tickets');

        if(isset($input['from_date']) && $input['from_date'] != "" && isset($input['till_date']) && $input['till_date'] != "")
        {
            $this->db->where("DATE_FORMAT(inserted_date, '%Y-%m-%d') >= ", date('Y-m-d', strtotime($input['from_date'])));
            $this->db->where("DATE_FORMAT(inserted_date, '%Y-%m-%d') <= ", date('Y-m-d', strtotime($input['till_date'])));
        }
        
        $this->db->group_by(array("transaction_status", "DATE_FORMAT(inserted_date,'%d-%m-%Y')")); 
        $this->db->order_by('inserted_date DESC, transaction_status'); 
        $res = $this->db->get();
        return $res->result_array();
    }
    function reconsillation_detail_excel($input)
    {
        
       $this->db->select('1,
                        t.ticket_id,
                        t.`transaction_status` AS "Transaction Status",
                        t.`boss_ref_no` AS "Boss Reference No",
                        t.pnr_no As "PNR NO",
                        t.ticket_ref_no,
                        ap.name as "Povider Name", 
                        t.`op_name` AS "Operator Name",
                        t.`inserted_date` as "issued date",
                        t.dept_time as doj,
                        t.num_passgr as "No of passenger",
                        IFNULL(t.`tot_fare_amt`,0) AS "Basic Fare",
                        sum(td.service_tax) AS "Service Tax",
                        IFNULL(t.`total_fare_without_discount`,0) AS "Total Fare Without Discount",   
                        IF(t.`booked_from` = "upsrtc", t.`fare_convey_charge`, "0") AS "UPSRTC Additional Charges (3% or Rs.50)",
                        IFNULL(t.`discount_on`,tdd.`discount_on`) AS "Discount On",
                        IFNULL(t.`discount_type`,tdd.`discount_type`) AS  "Discount Type",
                        IFNULL(t.`discount_per`,tdd.`discount_per`) AS "Discount Percentage",
                        IFNULL(t.`discount_value`,tdd.`discount_flat_value`) AS  "Discount Price",
                        IFNULL(at.`agent_commission_percentage`,"-") as "agent_commission_percenatge",
                        at.`agent_commission_rs` AS "Agent Commission Amount",
                        at.`tds_value_on_commission_amount` AS "agent_commission_tds",
                        at.`agent_commission_amount` AS "agent_commission_amount_with_tds",
                        t.`tot_fare_amt_with_tax` AS "Total Fare Paid",
                        IFNULL(ctd.`actual_refund_paid`,0) AS "Refund Amount",
                        IF(ctd.is_refund = "1",(ctd.`cancel_charge`),0)  AS "Cancel Charge",
                        t.`pg_tracking_id` AS "PG Track Id",
                        IFNULL(t.`tot_fare_amt_with_tax`,0) as "Amount",
                        pgt.`order_fee_perc_value` as "Fee",
                        pgt.`order_tax` as "Tax on Fees",
                        IFNULL(t.`merchant_amount`,0) as "Payable Amount",
                        "-" as "Auth Code",
                        IFNULL(t.`payment_mode`,0) as "td_type",
                        IFNULL(wt.`id`,"-") AS  "Wallet_id",
                        t.`api_ticket_no` as "Ets_no",
                        tad.`actual_amount` as "Et_amt",
                        tad.`total_discount_amount` as "Ets_dscnt",
                        if(t.booked_from = "etravelsmart", ifnull(tad.`service_tax_amount`,t.`service_tax` + t.`op_service_charge`),"") as Ets_ser_tax,
                        tad.`tds_charges` as "Ets_tds_charge",
                        tad.`paid_amount` as "Ets_paid_amt"'
                     );
        $this->db->from('tickets t');
        $this->db->join('ticket_details td', 'td.ticket_id = t.ticket_id', 'inner');
        $this->db->join('api_provider ap', 't.provider_id = ap.id', 'left');
        $this->db->join('agent_tickets at', 'at.ticket_id = t.ticket_id', 'left');
        $this->db->join('cancel_ticket_data ctd', 'ctd.ticket_id = t.ticket_id', 'left');
        $this->db->join('tickets_additional_data tad', 'tad.ticket_id = t.ticket_id', 'left');
        $this->db->join('payment_gateway_transaction pgt', 'pgt.pg_tracking_id = t.pg_tracking_id', 'left');
        $this->db->join('ticket_discount_description tdd', 'tdd.ticket_id = t.ticket_id', 'left');
        $this->db->join('wallet_trans wt', 'wt.ticket_id = t.ticket_id', 'left');
        $this->db->where('t.pnr_no != ""');
        $this->db->where_in('t.transaction_status', array('success','cancel'));
       
        if ($input['from_date']) 
        {
            $this->db->where("DATE_FORMAT(t.inserted_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if ($input['to_date']) 
        {
            $this->db->where("DATE_FORMAT(t.inserted_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        } 
        if ($input['provider_id']) 
        {
            $this->db->where("t.booked_from", $input['provider_id']);
        }
        $this->db->group_by('t.ticket_id');
        $res = $this->db->get();
        
        return $res->result_array();
    }
    function cancel_reconcillation_excel($input)
    {
        $this->db->select('1,
                        t.ticket_id,
                        t.`transaction_status` AS "Transaction Status",
                        t.`boss_ref_no` AS "Boss Reference No",
                        t.pnr_no As "PNR NO",
                        t.ticket_ref_no,
                        ap.name as "Povider Name", 
                        t.`op_name` AS "Operator Name",
                        t.`inserted_date` as "issued date",
                        t.dept_time as doj,
                        t.num_passgr as "No of passenger",
                        IFNULL(t.`tot_fare_amt`,0) AS "Basic Fare",
                        sum(td.service_tax) AS "Service Tax",
                        IFNULL(t.`total_fare_without_discount`,0) AS "Total Fare Without Discount",   
                        IF(t.`booked_from` = "upsrtc", t.`fare_convey_charge`, "0") AS "UPSRTC Additional Charges (3% or Rs.50)",
                        IFNULL(t.`discount_on`,"-") AS "Discount On",
                        IFNULL(t.`discount_type`,"-") AS  "Discount Type",
                        t.`discount_per` AS "Discount Percentage",
                        t.`discount_value` AS "Discount Price",
                        t.`tot_fare_amt_with_tax` AS "Total Fare Paid",
                        IFNULL(ctd.`pg_refund_amount`,0) AS "Refund Amount",
                        IF(ctd.is_refund = "1",(ctd.`cancel_charge`),0)  AS "Cancel Charge",
                        t.`pg_tracking_id` AS "PG Track Id",
                        IFNULL(t.`tot_fare_amt_with_tax`,0) as "Amount",
                        pgt.`order_fee_perc_value` as "Fee",
                        pgt.`order_tax` as "Tax on Fees",
                        IFNULL(t.`merchant_amount`,0) as "Payable Amount",
                        "-" as "Auth Code",
                        IFNULL(t.`payment_mode`,0) as "td_type",
                        t.`api_ticket_no` as "Ets_no",
                        tad.`total_amount` as "Et_amt",
                        tad.`total_discount_amount` as "Ets_dscnt",
                        tad.`service_tax_amount` as "Ets_ser_tax",
                        tad.`tds_charges` as "Ets_tds_charge",
                        tad.`paid_amount` as "Ets_paid_amt"'
                     );
        $this->db->from('tickets t');
        $this->db->join('ticket_details td', 'td.ticket_id = t.ticket_id', 'inner');
        $this->db->join('api_provider ap', 't.provider_id = ap.id', 'left');
        $this->db->join('cancel_ticket_data ctd', 'ctd.ticket_id = t.ticket_id', 'left');
        $this->db->join('tickets_additional_data tad', 'tad.ticket_id = t.ticket_id', 'left');
        $this->db->join('payment_gateway_transaction pgt', 'pgt.pg_tracking_id = t.pg_tracking_id', 'left');
        $this->db->where('t.pnr_no != ""');
        $this->db->where_in('t.transaction_status', array('cancel'));
        
        if($input['from_date']) 
        {
            $this->db->where("DATE_FORMAT(t.inserted_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if ($input['to_date']) 
        {
            $this->db->where("DATE_FORMAT(t.inserted_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        } 
        if ($input['provider_id']) 
        {
            $this->db->where("t.booked_from", $input['provider_id']);
        }
        $this->db->group_by('t.ticket_id');
        $res = $this->db->get();
        
        return $res->result_array();
    }
    public function bos_ticket_details_report($input,$response_type='')
    {
		  $sub_agents = array();
		  $sub_agents = get_users_for_ors_agent();  
	   
        $this->datatables->select('1,
                        DATE_FORMAT(t.inserted_date,"%d-%m-%Y %h:%i:%s") as "booking_date",
						concat(u.`first_name`," ",u.`last_name`) as inserted_by,
						om.office_name,
                        DATE_FORMAT(t.dept_time,"%d-%m-%Y %h:%i:%s") AS "journey_date",
                        t.`op_name` AS "operator_name",
                        t.from_stop_name as "from_stop",                      
                        t.till_stop_name   as "to_stop",
                        t.`boss_ref_no` AS "boss_ref_no",
                        t.pnr_no As "pnr_no",
                        td.psgr_name as "first_name",
                        t.user_email_id as "email",
                        t.mobile_no,
                        IFNULL(t.`total_basic_amount`,0.00)AS "basic_fare",
                        t.service_tax AS "service_tax",
                        IFNULL(t.`op_service_charge`,0.00) AS "op_service_charge",						
                        IFNULL(at.`markup_value`,0.00) AS "agent_service_tax",
                        (t.total_fare_without_discount - t.total_basic_amount - t.service_tax - IFNULL(t.`op_service_charge`,0)) as "other_taxes",
                        (t.total_fare_without_discount + IFNULL(at.`markup_value`,0.00)) as total_ticket_fare,
                        (SELECT (SUM(td.agent_commission_amount) - SUM(td.tds_value_on_commission_amount)) FROM ticket_details td WHERE td.`ticket_id` = t.`ticket_id` GROUP BY td.`ticket_id`) AS "commission",
                        (SELECT SUM(td.tds_value_on_commission_amount) FROM ticket_details td WHERE td.`ticket_id` = t.`ticket_id` GROUP BY td.`ticket_id`) AS "tds",
                        t.`tot_fare_amt_with_tax` AS "net_paid",
                        t.`payment_by` as "payment_mode" ,
                        t.`ticket_ref_no` AS "txn_ref_no",
                        ( SELECT IFNULL(SUM(ctd.`cancel_charge`), 0) FROM cancel_ticket_data ctd WHERE ctd.`ticket_id` = t.`ticket_id` GROUP BY ctd.`ticket_id`) AS "cancel_charges",
                        ( SELECT IFNULL(SUM(ctd.`actual_refund_paid`), 0) FROM cancel_ticket_data ctd WHERE ctd.`ticket_id` = t.`ticket_id` GROUP BY ctd.`ticket_id`) AS "pg_refund_amount",
                        ((( t.total_fare_without_discount + IFNULL(at.`markup_value`, 0.00)) - t.`tot_fare_amt_with_tax`) - IFNULL(( SELECT (IFNULL(SUM(ctd.`refund_amt`), 0.00) - IFNULL(SUM(ctd.`actual_refund_paid`),0.00)) AS cancel_ticket_cal FROM cancel_ticket_data ctd WHERE ctd.`ticket_id` = t.`ticket_id` GROUP BY ctd.`ticket_id`),0.00)) AS profit_earned,
                        IFNULL(wt.`wallet_amt_before_trans`,0) AS "wallet_opening_bal",
                        IFNULL(wt.`wallet_amt_after_trans`,0) AS "wallet_closing_bal",
                        IFNULL(wt.`virtual_amt_before_trans`,0) as "Credit_opening_balance",
                        IFNULL(wt.`virtual_amt_after_trans`,0) as "Credit_closing_balance",
                        t.ticket_id,
                        t.`transaction_status` AS "transaction_status"'
        );

        $this->datatables->from('tickets t');
        $this->datatables->join('ticket_details td', 'td.ticket_id = t.ticket_id', 'inner');
        $this->datatables->join('agent_tickets at', 'at.ticket_id=t.ticket_id', 'left');
        $this->datatables->join('api_provider ap', 't.provider_id = ap.id', 'left');
        $this->datatables->join('cancel_ticket_data ctd', 'ctd.ticket_id = t.ticket_id', 'left');
        $this->datatables->join('wallet_trans wt', 'wt.ticket_id = t.ticket_id and wt.transaction_type_id = 5', 'left');
        $this->datatables->join('users u', 'u.id = t.inserted_by', 'left');		
		    $this->datatables->join('office_master om', 'om.office_code = u.office_code', 'left');		
		
		if($input['provider_id'])
        {
            $this->datatables->where("t.payment_by", $input['provider_id']);
        }
        if($input['select_agent'])
        {
            $this->datatables->where("u.id", $input['select_agent']);
        }
        if($input['from_date'])
        {
            $this->datatables->where("DATE_FORMAT(issue_time, '%Y-%m-%d')>=", date('Y-m-d', strtotime($input['from_date'])));
        }
        if($input['till_date'])
        {
            $this->datatables->where("DATE_FORMAT(issue_time, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['till_date'])));
        }

        if($input['tran_type'] && $input['tran_type']!='select_status')
        {
            $status=$input['tran_type'];
            if($status=='failed')
            {
                $status=array('psuccess','pfailed','failed');
            }
            else if($status=='success' || $status=='cancel')
            {
                $this->datatables->where('t.pnr_no != ', "");
            }
            $this->datatables->where_in('t.transaction_status', $status);
        }

		if(array_key_exists("office_select",$input))
		{
			if(isset($input['office_select']) && $input['office_select'] != "")
			{
				$this->datatables->where("u.office_code", $input['office_select']);
			}	
		}

		if(array_key_exists("ors_agent_user",$input))
		{
			if(isset($input['ors_agent_user']) && $input['ors_agent_user'] != "")			
			{	
				$this->datatables->where("t.inserted_by", $input['ors_agent_user']);					 
			}
			else
			{				
				$this->datatables->where_in('t.inserted_by', $sub_agents);				
			}
		}				
		$this->datatables->group_by('ctd.ticket_id');
		$this->datatables->group_by('wt.ticket_id'); 
        	$this->datatables->group_by('t.ticket_id');
             
		if ($response_type != '') {
			if ($response_type == 'json') {
				$this->datatables->add_column('t.ticket_id', '<a href="#" ref="$1" class="passen_details">Passenger Detail </a>', 'ticket_id');
			}			
			$data = $this->datatables->generate($response_type);									
		} else {
			$data = $this->datatables->generate();										
		}
		
		//$temp_det = json_decode($data);
		//show($temp_det, 1);		
		return $data;		
    }
	
	public function bos_agent_users_ticket_details_report($input,$response_type='')
    {
		$sub_agents = array();
		$sub_agents = get_users_for_same_office();  
	   
        $this->datatables->select('1,
                        DATE_FORMAT(t.inserted_date,"%d-%m-%Y %h:%i:%s") as "booking_date",
						concat(u.`first_name`," ",u.`last_name`) as inserted_by,
						om.office_name,
                        DATE_FORMAT(t.dept_time,"%d-%m-%Y %h:%i:%s") AS "journey_date",
                        t.`op_name` AS "operator_name",
                        t.from_stop_name as "from_stop",                      
                        t.till_stop_name   as "to_stop",
                        t.`boss_ref_no` AS "boss_ref_no",
                        t.pnr_no As "pnr_no",
                        td.psgr_name as "first_name",
                        t.user_email_id as "email",
                        t.mobile_no,
                        IFNULL(t.`total_basic_amount`,0.00)AS "basic_fare",
                        t.service_tax AS "service_tax",
                        IFNULL(t.`op_service_charge`,0.00) AS "op_service_charge",                      
                        IFNULL(at.`markup_value`,0.00) AS "agent_service_tax",
                        (t.total_fare_without_discount - t.total_basic_amount - t.service_tax - IFNULL(t.`op_service_charge`,0)) as "other_taxes",
                        (t.total_fare_without_discount + IFNULL(at.`markup_value`,0.00)) as total_ticket_fare,
                        (SELECT (SUM(td.agent_commission_amount) - SUM(td.tds_value_on_commission_amount)) FROM ticket_details td WHERE td.`ticket_id` = t.`ticket_id` GROUP BY td.`ticket_id`) AS "commission",
                        (SELECT SUM(td.tds_value_on_commission_amount) FROM ticket_details td WHERE td.`ticket_id` = t.`ticket_id` GROUP BY td.`ticket_id`) AS "tds",
                        t.`tot_fare_amt_with_tax` AS "net_paid",
                        t.`payment_by` as "payment_mode" ,
                        t.`ticket_ref_no` AS "txn_ref_no",
                        ( SELECT IFNULL(SUM(ctd.`cancel_charge`), 0) FROM cancel_ticket_data ctd WHERE ctd.`ticket_id` = t.`ticket_id` GROUP BY ctd.`ticket_id`) AS "cancel_charges",
                        ( SELECT IFNULL(SUM(ctd.`actual_refund_paid`), 0) FROM cancel_ticket_data ctd WHERE ctd.`ticket_id` = t.`ticket_id` GROUP BY ctd.`ticket_id`) AS "pg_refund_amount",
                        ((( t.total_fare_without_discount + IFNULL(at.`markup_value`, 0.00)) - t.`tot_fare_amt_with_tax`) - IFNULL(( SELECT (IFNULL(SUM(ctd.`refund_amt`), 0.00) - IFNULL(SUM(ctd.`actual_refund_paid`),0.00)) AS cancel_ticket_cal FROM cancel_ticket_data ctd WHERE ctd.`ticket_id` = t.`ticket_id` GROUP BY ctd.`ticket_id`),0.00)) AS profit_earned,
                        IFNULL(wt.`wallet_amt_before_trans`,0) AS "wallet_opening_bal",
                        IFNULL(wt.`wallet_amt_after_trans`,0) AS "wallet_closing_bal",
                        IFNULL(wt.`virtual_amt_before_trans`,0) as "Credit_opening_balance",
                        IFNULL(wt.`virtual_amt_after_trans`,0) as "Credit_closing_balance",
                        t.ticket_id,
                        t.`transaction_status` AS "transaction_status"'
        );

        $this->datatables->from('tickets t');
        $this->datatables->join('ticket_details td', 'td.ticket_id = t.ticket_id', 'inner');
        $this->datatables->join('agent_tickets at', 'at.ticket_id=t.ticket_id', 'left');
        $this->datatables->join('api_provider ap', 't.provider_id = ap.id', 'left');
        $this->datatables->join('cancel_ticket_data ctd', 'ctd.ticket_id = t.ticket_id', 'left');
        $this->datatables->join('wallet_trans wt', 'wt.ticket_id = t.ticket_id', 'left');
        $this->datatables->join('users u', 'u.id = t.inserted_by', 'left');		
		$this->datatables->join('office_master om', 'om.office_code = u.office_code', 'left');		
		
		if($input['provider_id'])
        {
            $this->datatables->where("t.payment_by", $input['provider_id']);
        }
        if($input['select_agent'])
        {
            $this->datatables->where("u.id", $input['select_agent']);
        }
        if($input['from_date'])
        {
            $this->datatables->where("DATE_FORMAT(issue_time, '%Y-%m-%d')>=", date('Y-m-d', strtotime($input['from_date'])));
        }
        if($input['till_date'])
        {
            $this->datatables->where("DATE_FORMAT(issue_time, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['till_date'])));
        }

        if($input['tran_type'] && $input['tran_type']!='select_status')
        {
            $status=$input['tran_type'];
            if($status=='failed')
            {
                $status=array('psuccess','pfailed','failed');
            }
            else if($status=='success' || $status=='cancel')
            {
                $this->datatables->where('t.pnr_no != ', "");
            }
            $this->datatables->where_in('t.transaction_status', $status);
        }

		if(array_key_exists("office_select",$input))
		{
			if(isset($input['office_select']) && $input['office_select'] != "")
			{
				$this->datatables->where("u.office_code", $input['office_select']);
			}	
		}

		if(array_key_exists("ors_agent_user",$input))
		{
			if(isset($input['ors_agent_user']) && $input['ors_agent_user'] != "")			
			{	
				$this->datatables->where("t.inserted_by", $input['ors_agent_user']);					 
			}
			else
			{				
				$this->datatables->where_in('t.inserted_by', $sub_agents);				
			}
		}				
		
		$this->datatables->group_by('t.ticket_id');      
		if ($response_type != '') {
			if ($response_type == 'json') {
				$this->datatables->add_column('t.ticket_id', '<a href="#" ref="$1" class="passen_details">Passenger Detail </a>', 'ticket_id');
			}			
			$data = $this->datatables->generate($response_type);									
		} else {
			$data = $this->datatables->generate();										
		}
		
		//$temp_det = json_decode($data);
		//show($temp_det, 1);		
		return $data;	
    }
	
	public function bos_admin_ticket_details_report($input,$response_type='')
    {
		$sub_agents = array();
		$sub_agents = get_users_for_ors_agent();  
	   
        $this->datatables->select('1,
                        DATE_FORMAT(t.inserted_date,"%d-%m-%Y %h:%i:%s") as "Booking_date",
						DATE_FORMAT(t.dept_time,"%d-%m-%Y %h:%i:%s") AS "Journey_date",
                        t.`op_name` AS "Operator Name",
                        t.from_stop_name as "From Stop",                      
                        t.till_stop_name   as "To Stop",
                        t.`boss_ref_no` AS "Boss Reference No",
                        t.pnr_no As "PNR NO",
                        td.psgr_name as "first_name",
                        t.user_email_id as "email",
                        t.mobile_no,
                        
                        if(t.transaction_status = "success",sum(if(td.status="Y",(td.adult_basic_fare),0)),t.total_basic_amount) AS "Total_Fare",

                        t.service_tax AS "Service Tax",
                        IFNULL(t.`op_service_charge`,0.00) AS "op_service_charge",						
                        IFNULL(at.`markup_value`,0.00) AS "agent_service_tax",
                        if(t.transaction_status = "success",sum(if(td.status="Y",(td.fare_amt - td.adult_basic_fare),0)),(t.total_fare_without_discount - t.total_basic_amount - t.service_tax - IFNULL(t.`op_service_charge`,0))) AS "other_taxes",
                        if(t.transaction_status = "success",sum(if(td.status="Y",(td.fare_amt),0)),t.total_fare_without_discount) AS "Total_Ticket_Fare",
                        if(t.transaction_status = "success",sum(if(td.status="Y",(td.agent_commission_amount),0)),at.agent_commission_amount) as  "commission",
                        if(t.transaction_status = "success",sum(if(td.status="Y",(td.tds_value_on_commission_amount),0)),at.tds_value_on_commission_amount) AS "tds",
                        
                        CASE t.booked_from 
                        WHEN "etravelsmart" THEN (t.`tot_fare_amt_with_tax`) 
                        WHEN "upsrtc" THEN (t.`tot_fare_amt_with_tax`)
                        WHEN "msrtc" THEN if(t.transaction_status = "success",
                        (sum(if(td.status="Y",(td.`fare_amt`),"0"))-sum(if(td.status="Y",(td.`agent_commission_amount`),"0"))
                        -sum(if(td.status="Y",(td.`discount_value`),"0")) + sum((if(td.status="Y",(td.`tds_value_on_commission_amount`),"0")))),t.tot_fare_amt_with_tax) 
                        WHEN "hrtc" THEN (t.`tot_fare_amt_with_tax`)
			WHEN "rsrtc" THEN (t.`tot_fare_amt_with_tax`)
			WHEN "infinity" THEN (t.`tot_fare_amt_with_tax`)
			END AS "net_paid",

                        t.`payment_by` as "payment_mode" ,
                        t.`ticket_ref_no` AS "Txn_ref_no",
                        IF(ctd.is_refund = "1",(ctd.`cancel_charge`),0)  AS "Cancel Charge",
                        IFNULL(ctd.`pg_refund_amount`,0) AS "pg_refund_amount",
                        if(t.transaction_status = "success",sum(if(td.status="Y",((td.agent_commission_amount+at.markup_value) -td.tds_value_on_commission_amount),0)),at.markup_value) AS "profit_earned",
                        
                        
                        IFNULL(wt.`wallet_amt_before_trans`,0) AS "wallet_opening_bal",
                        IFNULL(wt.`wallet_amt_after_trans`,0) AS "wallet_closing_bal",
                        IFNULL(wt.`virtual_amt_before_trans`,0) as "Credit_opening_balance",
                        IFNULL(wt.`virtual_amt_after_trans`,0) as "Credit_closing_balance",
                        t.ticket_id,
                        t.`transaction_status` AS "Transaction Status"'
        );

        $this->datatables->from('tickets t');
        $this->datatables->join('ticket_details td', 'td.ticket_id = t.ticket_id', 'inner');
        $this->datatables->join('agent_tickets at', 'at.ticket_id=t.ticket_id', 'left');
        $this->datatables->join('api_provider ap', 't.provider_id = ap.id', 'left');
        $this->datatables->join('cancel_ticket_data ctd', 'ctd.ticket_id = t.ticket_id', 'left');
        $this->datatables->join('wallet_trans wt', 'wt.ticket_id = t.ticket_id', 'left');
        $this->datatables->join('users u', 'u.id = t.inserted_by', 'left');	
		
		if($input['provider_id'])
        {
            $this->datatables->where("t.payment_by", $input['provider_id']);
        }
        if($input['select_agent'])
        {
            $this->datatables->where("u.id", $input['select_agent']);
        }
        if($input['from_date'])
        {
            $this->datatables->where("DATE_FORMAT(issue_time, '%Y-%m-%d')>=", date('Y-m-d', strtotime($input['from_date'])));
        }
        if($input['till_date'])
        {
            $this->datatables->where("DATE_FORMAT(issue_time, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['till_date'])));
        }

        if($input['tran_type'] && $input['tran_type']!='select_status')
        {
            $status=$input['tran_type'];
            if($status=='failed')
            {
                $status=array('psuccess','pfailed','failed');
            }
            else if($status=='success' || $status=='cancel')
            {
                $this->datatables->where('t.pnr_no != ', "");
            }
            $this->datatables->where_in('t.transaction_status', $status);
        }
		
		$this->datatables->group_by('t.ticket_id');      
		if ($response_type != '') {
			if ($response_type == 'json') {
				$this->datatables->add_column('t.ticket_id', '<a href="#" ref="$1" class="passen_details">Passenger Detail </a>', 'ticket_id');
			}			
			$data = $this->datatables->generate($response_type);									
		} else {
			$data = $this->datatables->generate();										
		}
		
		//$temp_det = json_decode($data);
		//show($temp_det, 1);		
		return $data;	
    }
   
    public function cross_sale_report($input)
    {
        eval(FME_AGENT_ROLE_ID);
        $this->datatables->select("1,sm.stState,cm.stCity,rm.referrer_name as ref_name, u.display_name as agent_name,DATE_FORMAT(u.created_date,'%d-%m-%Y') as registration_date,count(t.ticket_id) as ticket,sum(t.num_passgr) as passengers,sum(t.tot_fare_amt_with_tax) as fares,DATE_FORMAT(max(t.inserted_date),'%d-%m-%Y') as last_transaction_date,u.verify_status");
        $this->datatables->from('users u');
        $this->datatables->join('state_master sm', 'sm.intStateid = u.state','left');
        $this->datatables->join('city_master cm', 'cm.intCityid = u.city','left');
        $this->datatables->join('referrer_master rm', 'rm.referrer_code=u.referrer_code', 'left');
        $this->datatables->join('tickets t', 't.inserted_by = u.id', 'left');
        $this->datatables->where('t.transaction_status','success');
	    $this->datatables->where_in('t.role_id',$fme_agent_role_id);    
        $this->datatables->where_in('u.role_id',$fme_agent_role_id);      
      

        if(!empty($input['agent_name'])){
			
            $this->datatables->where('u.id', $input['agent_name']);
        }
        if(!empty($input['select_referrer'])){
			
            $this->datatables->where('rm.id', $input['select_referrer']);
        }
        if(!empty($input['citynm'])){
			
            $this->datatables->where('u.city', $input['citynm']);
        }
       if(!empty($input['select_state'])){
			
            $this->datatables->where('u.state', $input['select_state']);
        }
        
        if(!empty($input['from_date']))
        {
            $this->datatables->where("DATE_FORMAT(u.created_date, '%Y-%m-%d')>=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if(!empty($input['till_date']))
        {
            $this->datatables->where("DATE_FORMAT(u.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['till_date'])));
        }

        $this->datatables->group_by('t.inserted_by');
        $this->db->order_by('rm.id');
        
        $data = $this->datatables->generate('json');
        return $data;
    } 

     /*@Author     : Aparna chalke
     * @function    : get_agent_ticket_count_excel
     * @param       : 
     * @detail      : To export report in excel format
     */
	
    function get_agent_ticket_count_excel($input){
	eval(FME_AGENT_ROLE_ID);
    $this->db->select('concat(users.first_name," ",users.last_name) as Agent_Name,users.id as Agent_Code,users.mobile_no,cm.stCity as city_name,sm.stState as state_name,rm.referrer_name, max(tickets.issue_time) as Last_Ticket_Booked_Date, sum(agent_tickets.markup_Value) as Till_Date_Markup_Value, count(tickets.ticket_id) as Till_Date_Booking_Count, sum(tickets.total_fare_without_discount - tickets.tot_fare_amt_with_tax - IF(agent_tickets.markup_value != NULL, agent_tickets.markup_value, 0) ) as Till_Date_Commission, sum(tot_fare_amt_with_tax) as Till_Date_Revenue, tickets.inserted_by,sum(num_passgr) as passengers');
        $this->db->from('tickets');
        $this->db->join('users','users.id = tickets.inserted_by','left');
        $this->db->join('agent_tickets','agent_tickets.ticket_id = tickets.ticket_id','left');
        $this->db->join('referrer_master rm','rm.referrer_code = users.referrer_code','left');
        $this->db->join('city_master cm','cm.intCityId = users.city','left');
        $this->db->join('state_master sm','sm.intStateId = users.state','left');
        $this->db->where_in('tickets.role_id', $fme_agent_role_id);

        if(!empty($input['trans_type'])){
          $this->db->where('tickets.transaction_status', $input['trans_type']);
        }
        if(!empty($input['select_agent'])){
            $this->db->where('users.id',$input['select_agent']);
        }
        if(!empty($input['select_state'])){
            $this->db->where('users.state', $input['select_state']);
        }
        if(!empty($input['select_city'])){
          $this->db->where('users.city', $input['select_city']);
        }
        if(!empty($input['select_referrer'])){
          $this->db->where('rm.id',$input['select_referrer']);
        }
        if(empty($input['trans_type'])){
          $this->db->where('tickets.transaction_status','success');
        }
      
        $this->db->group_by('tickets.inserted_by');
        $this->db->order_by('Till_Date_Booking_Count','desc');

        $result = $this->db->get();
        $data['ticket_report_result'] = $result->result_array();
        
        $inserted_by = array_column($data['ticket_report_result'], 'inserted_by');

        if(!empty($input['from_date']) || !empty($input['till_date']))
        {
            if(!empty($inserted_by))
            {
                $data['ticket_report_result_range'] = $this->get_agent_ticket_count_date_excel($inserted_by,$input);
            }
            $i = 0;
                foreach($data['ticket_report_result'] as $key1 => $value1){
                     foreach($data['ticket_report_result_range'] as $key2 => $value2){
                         if($data['ticket_report_result'][$key1]['inserted_by'] == $data['ticket_report_result_range'][$key2]['inserted_by']){     
                              $data['ticket_report_result'][$i] = array_merge($data['ticket_report_result'][$key1], $data['ticket_report_result_range'][$key2]);
                          }
                     }
                     $i++;
                } 
        }
        
       return $data;
    }

    /*@Author     : Aparna chalke
     * @function  : get_agent_ticket_count_date_excel
     * @param     : 
     * @detail    : To get ticket count id wise
     */
	
    public function get_agent_ticket_count_date_excel($inserted_by,$input)
    {
		 eval(FME_AGENT_ROLE_ID);
         $this->db->select('sum(agent_tickets.markup_value) as Till_Date_Markup_Value_Range, count(tickets.ticket_id) as Till_Date_Booking_Count_Range, sum(tickets.total_fare_without_discount - tickets.tot_fare_amt_with_tax - IF(agent_tickets.markup_value != NULL, agent_tickets.markup_value, 0) ) as Till_Date_Commission_Range, sum(tot_fare_amt_with_tax) as Till_Date_Revenue_Range, tickets.inserted_by,sum(num_passgr) as till_date_passengers');
        $this->db->from('tickets');
        $this->db->join('users','users.id = tickets.inserted_by','left');
        $this->db->join('agent_tickets','agent_tickets.ticket_id = tickets.ticket_id','left');
        if(!empty($input['select_agent'])){
            $this->db->where('users.id',$input['select_agent']);
        }
        $this->db->where_in('tickets.role_id',$fme_agent_role_id);
        $this->db->where_in('tickets.inserted_by', $inserted_by);        
        
        if(empty($input['till_date'])){
            $this->session->set_userdata('till_date',date('Y-m-d 23:59:59'));
        }
            
        if(!empty($input['trans_type'])){
            $this->db->where('tickets.transaction_status', $input['trans_type']);
        }

        if(!empty($input['select_state'])){
            $this->db->where('users.state', $input['select_state']);
        }
        
        if(!empty($input['select_city'])){
            $this->db->where('users.city', $input['select_city']);
        }
        
    if(!empty($input['from_date']) && !empty($this->session->userdata('till_date')))
         $this->db->where("tickets.issue_time between '".date('Y-m-d 00:00:01', strtotime($input['from_date']))."' and '".date('Y-m-d 23:59:59', strtotime($input['till_date']))."'");
        $this->db->group_by('tickets.inserted_by');
        $result = $this->db->get();
        return $result->result_array();
    }

    /* @Author    : Aparna chalke
     * @function  : referrer_wise_summary_report_excel
     * @param     : 
     * @detail    : To fetch referrer wise summary report
     */
	
    public function referrer_wise_summary_report_excel($input)
    {
	eval(FME_AGENT_ROLE_ID);
        $this->db->select("1,rm.referrer_code,rm.referrer_name,rm.referrer_mobile,COUNT(ticket_id) AS 'No of Tickets',SUM(num_passgr) AS 'No Of Passenger',SUM(tot_fare_amt_with_tax) AS 'Revenue',SUM(discount_value) AS 'Discount'");
        $this->db->from('referrer_master AS rm');
        $this->db->join('users u', 'u.referrer_code=rm.referrer_code and u.status="Y"', 'inner join');
        $this->db->join('city_master cm', 'cm.intCityId=u.city', 'left');
        $this->db->join('tickets t', ' t.inserted_by = u.id and t.transaction_status = "success"', 'inner join');
		$this->db->where_in("t.role_id",$fme_agent_role_id);
        $this->db->where("rm.is_status", 'Y');
        $this->db->where("rm.is_deleted",  'N');
        $this->db->group_by("rm.referrer_code");
        $this->db->order_by("COUNT(ticket_id)", "desc");
        if (isset($input['from_date']) && $input['from_date']!='') {
            $this->db->where("DATE_FORMAT(t.inserted_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['till_date']) && $input['till_date']!='') {
            $this->db->where("DATE_FORMAT(t.inserted_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['till_date'])));
        }
        
        if (isset($input['select_city']) && $input['select_city']!='') {
            $this->db->where("cm.intCityId", $input['select_city']);
        }
    
       $result = $this->db->get();
       return $result->result_array();
    }


    /* @Author    : Aparna chalke
     * @function  : stop_wise_ticket_report_excel
     * @param     : 
     * @detail    : To fetch stop wise ticket summary report
     */
    public function stop_wise_ticket_report_excel($input)
    {
    
        $array = array('transaction_status'=>$input['trans_type']);

       if(!empty($input['from_date'])){
              $like = "tickets.issue_time between '".date('Y-m-d 00:00:01', strtotime($input['from_date']))."' and '".date('Y-m-d 23:59:59', strtotime($input['till_date']))."'";
        }
    
         if(!empty($input['select_user_type'])){
            $array['booked_by']= $input['select_user_type'];
       }
      
        $data = $this->tickets_model->column("from_stop_name, till_stop_name, case booked_by when 'users' then 'Registered User' when 'ors_agent' then 'Agent' else 'Guest User' end as user, count(ticket_id)as count, sum(num_passgr) as nopassenger")->where($array)->where($like)->as_array()->group_by('from_stop_name, till_stop_name, booked_by')->find_all();
             
        return $data;
    }
    function pending_user_excel($input) {
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');
       $this->db->select('1,
                        DATE_FORMAT(created_date,"%d-%m-%Y %h:%i:%s") as created_date,concat(first_name," ",last_name) AS Username,ur.role_name as UserType,
                        sm.stState AS State,
                        cm.stCity AS City,
                        mobile_no AS Mobile,
                        email AS Email');
        $this->db->from('users u');
        $this->db->join('state_master sm', 'u.state=sm.intStateId', 'left');
        $this->db->join('city_master cm', 'cm.intCityId=u.city', 'left');
        $this->db->join('user_roles ur', 'u.role_id=ur.id', 'left');

        if (isset($input['from_date']) && $input['from_date'] != '') {
            $this->db->where("DATE_FORMAT(u.created_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['to_date']) && $input['to_date'] != '') {
            $this->db->where("DATE_FORMAT(u.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }
           $this->db->where("u.email_verify_status = 'N'");
           $this->db->where("u.otp_verify_status = 'N'");
         if(!empty($session_level)){
           	if($session_level > TRIMAX_LEVEL)
           	{
           		$this->db->where('level_'.$session_level,$session_id);
           	}
           	else
           	{
           		$this->db->where('level_'.TRIMAX_LEVEL, TRIMAX_LEVEL);
           	}
           }
        
        if ($input["user_type"] == COMPANY_ROLE_ID) {
        	$this->db->where("u.role_id",COMPANY_ROLE_ID);
        } else if ($input["user_type"] == MASTER_DISTRIBUTOR_ROLE_ID) {
        	$this->db->where("u.role_id",MASTER_DISTRIBUTOR_ROLE_ID);
        } else if ($input["user_type"] == AREA_DISTRIBUTOR_ROLE_ID) {
        	$this->db->where("u.role_id",AREA_DISTRIBUTOR_ROLE_ID);
        } else if ($input["user_type"] == DISTRIBUTOR) {
        	$this->db->where("u.role_id",DISTRIBUTOR);
        } else if ($input["user_type"] == RETAILER_ROLE_ID) {
        	$this->db->where("u.role_id",RETAILER_ROLE_ID);
        }

        $this->db->order_by('created_date', 'DESC');
        $res = $this->db->get();
        return $res->result_array();
        
    }
    function pending_kyc_excel($input) {
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');
       $this->db->select('1,
                        DATE_FORMAT(created_date,"%d-%m-%Y %h:%i:%s") as created_date,concat(first_name," ",last_name) AS Username,ur.role_name as UserType,
                        sm.stState AS State,
                        cm.stCity AS City,
                        mobile_no AS Mobile,
                        email AS Email');
        $this->db->from('users u');
        $this->db->join('state_master sm', 'u.state=sm.intStateId', 'left');
        $this->db->join('city_master cm', 'cm.intCityId=u.city', 'left');
        $this->db->join('user_roles ur', 'u.role_id=ur.id', 'left');

        if (isset($input['from_date']) && $input['from_date'] != '') {
            $this->db->where("DATE_FORMAT(u.created_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['to_date']) && $input['to_date'] != '') {
            $this->db->where("DATE_FORMAT(u.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }
        $this->db->where("u.kyc_verify_status = 'N'");
        if(!empty($session_level)){
        	if($session_level > TRIMAX_LEVEL)
        	{
        		$this->db->where('level_'.$session_level,$session_id);
        	}
        	else
        	{
        		// $this->db->where('level_'.TRIMAX_LEVEL, TRIMAX_LEVEL);
                $this->db->where('level_'.TRIMAX_LEVEL,$session_id);
        	}
        }        
        
        if ($input["user_type"] == COMPANY_ROLE_ID) {
        	$this->db->where("u.role_id",COMPANY_ROLE_ID);
        } else if ($input["user_type"] == MASTER_DISTRIBUTOR_ROLE_ID) {
        	$this->db->where("u.role_id",MASTER_DISTRIBUTOR_ROLE_ID);
        } else if ($input["user_type"] == AREA_DISTRIBUTOR_ROLE_ID) {
        	$this->db->where("u.role_id",AREA_DISTRIBUTOR_ROLE_ID);
        } else if ($input["user_type"] == DISTRIBUTOR) {
        	$this->db->where("u.role_id",DISTRIBUTOR);
        } else if ($input["user_type"] == RETAILER_ROLE_ID) {
        	$this->db->where("u.role_id",RETAILER_ROLE_ID);
        }

        $this->db->order_by('created_date', 'DESC');
        $res = $this->db->get();
        return $res->result_array();
        
    }

    public function get_sms_log_list($input,$response_type) {

        $this->datatables->select('1,
                                sl.supplied_mobile_number,
                                sl.message,
                                sl.status,
                                DATE_FORMAT(sl.submit_date,"%d-%m-%Y %h:%i:%s") as date
                                ');
        $this->datatables->from('sms_log sl');

        if (isset($input['from_date']) && $input['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(sl.submit_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['to_date']) && $input['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(sl.submit_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }
        
        $data = $this->datatables->generate($response_type);
        return $data;
    }

 }

?>
