<?php

class Bus_operator_model extends MY_Model {

    var $table = 'bus_operator';
    var $fields = array("id", "name", "is_status", "is_deleted", "created_by", "created_date", "updated_by", "updated_date");
    var $key = "id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
}
?>
