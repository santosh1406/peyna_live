<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Providers_utilities_model extends MY_Model {

    var $table  = 'providers_utilities';
    var $fields = array("id","provider_id","utilities_id","created_at", "updated_at");
    var $key    = 'id';
   
    public function __construct() {
        parent::__construct();

    }

    public function findproviders($provider_id){
        return $this->db->from('providers_utilities')->where('provider_id', $provider_id)->get();
    }

    public function findservices($service_id){
    	return $this->db->from('providers_utilities')->where('utilities_id', $service_id)->get();
    }

    public function deleterecords($service_id){
    	return $this->db->from('providers_utilities')->where('utilities_id', $service_id)->delete();
    }

    // public function deleterecords($provider_id){
    //     return $this->db->from('providers_utilities')->truncate();
    // }

}
