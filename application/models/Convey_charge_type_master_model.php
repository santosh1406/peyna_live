<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Convey_charge_type_master_model extends MY_Model {

    var $table = 'convey_charge_type_master';
    var $fields = array("convey_charge_type_id", "convey_charge_type_name", "is_status", "is_deleted", "created_by", "created_date", "updated_by", "updated_date");
    var $key = 'convey_charge_type_id';

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
    function get_convey_charge_types() {
        $this->db->select('convey_charge_type_id,convey_charge_type_name');
         $this->db->from('convey_charge_type_master');
        $this->db->where('is_status', 'Y');
         $this->db->where('is_deleted', 'N');
        $this->db->order_by("convey_charge_type_id", "ASC");
        $data = $this->db->get();
        return $data->result_array();
    }

}
