<?php

class Cb_op_master_details_model extends MY_Model {

    var $table = 'cb_op_master';
    var $fields = array("op_id", "user_id", "op_name","op_address1" ,"op_address2","op_city","op_state","op_country","op_pincode","op_contact_name","op_email", "op_phone_no","is_status", "is_deleted", "created_by", "created_date", "updated_by", "updated_date", "is_grouping_applicable");
    var $key = "op_id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
     
    /*
     * Get the operator data on basis for operator id.
     * Used for updating Operator data.
     */
    
    function get_operator_detail($operator_id) {
        $this->db->select('om.op_id,user_id,om.is_status,om.op_name,om.op_address1,om.op_address2,om.op_country,om.op_state,om.op_city,cm.stCity,sm.stState,co_m.stCountryName ,om.op_pincode,om.op_contact_name,om.op_email,om.op_phone_no', false);
        $this->db->join("country_master co_m",'om.op_country=co_m.intCountryId','left') ;
        $this->db->join("state_master sm",'om.op_state=sm.intStateId','left');
        $this->db->join("city_master cm",'om.op_city=cm.intCityId','left');
        $this->db->from('op_master om');
        $this->db->where(array('is_status'=>'1','om.op_id'=>$operator_id));
        $res=$this->db->get();
        if($res->num_rows()>0)
        {
            return $res->row_array();
        }
        return false;
    }
    
    function getOperatorId($operator_name) {
            $this->db->select('op_id');
            $this->db->from('op_master');
            $this->db->where('op_name',$operator_name);
            $op_name_data = $this->db->get();
            if($op_name_data->num_rows()>0){
                return $op_name_data->row_array();
            }
            return false;
    }
}
?>
