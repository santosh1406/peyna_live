<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utility_model extends MY_Model
{
	function __construct() {
		parent::__construct();
	}

	public function gen_card_number($issue_name,$app_code,$created_by) {
        /*
        $this->load->model(array('Mainmodel','Utility_model'));
        $this->Utility_model->gen_card_number('TRIMAX_ID','MCT','1');
        Req : 
        issue_name : TRIMAX_ID
        app_code : depot code
        created_by : Created by
        */
        if($app_code == '999') {
            $depot_number = $app_code;
        } else {
            $depot_number = $this->get_depot_number($app_code);
        }

        $data=array('app_code'=>$depot_number,
            'issue_name'=>$issue_name,
            'created_by'=>$created_by);
		$this->db->insert('seq_generator',$data);
        //echo $this->db->insert_id();
        return str_pad($depot_number,3,"0",STR_PAD_LEFT).str_pad($this->db->insert_id(),13,"0",STR_PAD_LEFT);
        
    }


    public function summary_number($issue_name,$created_by) {
        /*
        $this->load->model(array('Mainmodel','Utility_model'));
        $this->Utility_model->summary_number('SUMMARY','1');
        Req : 
        issue_name : SUMMARY
        created_by : Created by
        */

        $app_code = date('ymd');
        $data=array('app_code'=>$app_code,
            'issue_name'=>$issue_name,
            'created_by'=>$created_by);
		$this->db->insert('seq_generator',$data);
        //echo $this->db->insert_id();
        return str_pad($app_code,6,"0",STR_PAD_RIGHT).str_pad($this->db->insert_id(),7,"0",STR_PAD_LEFT);
        
    }

    public function gen_pos_code($issue_name,$depot_code,$vendor_shortcode,$created_by) {
        /*
        $this->load->model(array('Mainmodel','Utility_model'));
        $this->Utility_model->pos_number('POS','MCT','MS','1');
        Req : 
        issue_name : POS
       
        */

        $data=array('app_code'=>$vendor_shortcode.$depot_code,
            'issue_name'=>$issue_name,
            'created_by'=>$created_by);
		$this->db->insert('seq_generator',$data);
        //echo $this->db->insert_id();
        return str_pad($vendor_shortcode.$depot_code,8,"0",STR_PAD_RIGHT).str_pad($this->db->insert_id(),4,"0",STR_PAD_LEFT);
        
    }

    public function gen_terminal_code($issue_name,$pos_code,$created_by) {
        /*
        $this->load->model(array('Mainmodel','Utility_model'));
        $this->Utility_model->pos_number('TERMINAL','MSMCT0000001','1');
        Req : 
        issue_name : TERMINAL
       
        */

        $data=array('app_code'=>$pos_code,
            'issue_name'=>$issue_name,
            'created_by'=>$created_by);
		$this->db->insert('seq_generator',$data);
        //echo $this->db->insert_id();
        return str_pad($pos_code,12,"0",STR_PAD_RIGHT).str_pad($this->db->insert_id(),2,"0",STR_PAD_LEFT);
        
    }

    public function get_depot_number($depot_code)
    {
    	$this->db->select('depot_number')->from('smartcard_depots')->where('DEPOT_CD',$depot_code);
        $query = $this->db->get();
        return $result = $query->result_array()[0]['depot_number'];
        
    }

    public function save_command_data($t_id,$mode){
       
        if($mode == "REGISTRATION"){
            $cmd_data = $this->save_command_data_register($t_id);
            return $cmd_data;
        }else if($mode == "RENEW"){
            $cmd_data = $this->save_command_data_renew($t_id);
            //print_r($cmd_data);
            return $cmd_data;
        }else{
            return array('status' => 'failure', 'msg' => 'No data found');
        }
    }

    public function save_command_data_register($t_id){

        $SQL = "SELECT ct.id as card_tran_id, ct.depot_cd,
        date_format(cm.date_of_birth,'%d%m%y') as date_of_birth,
        date_format(ccm.card_expiry_date,'%d%m%y') as card_expiry_date,
        cm.gender,conm.concession_cd,conm.concession_nm,
        ccm.trimax_card_id,ct.bus_type_cd, 
        date_format(IFNULL(ct.activation_date,'0000-00-00'),'%d%m%y') as activation_date,
        date_format(IFNULL(ct.expiry_date,'0000-00-00'),'%d%m%y') as expiry_date,
        ctd.from_stop_code,ctd.till_stop_code,ctd.via_stop_code, cfm.allowed_trip_count, ct.span_id
        from ps_card_transaction ct
        inner join ps_customer_card_master ccm on ccm.trimax_card_id = ct.trimax_card_id
        inner join ps_customer_master cm on cm.id = ccm.customer_id 
        inner join ps_concession_master conm on conm.id = ct.pass_concession_id	
        inner join ps_card_transaction_details ctd on ctd.card_transaction_id = ct.id
        inner join ps_card_fee_master cfm on cfm.id = ct.card_fee_master_id
        where ct.trimax_card_id = '".$t_id."'
        and ct.write_flag = '1'
        and ct.is_active = '1';";
        
        $query = $this->db->query($SQL);
		
		$result = $query->result_array();
        //print_r($result);
        
        if(count($result) == 1){
            $cmd_data=array();

            //Name of Corporation
            $temp = str_pad("MSRTC",16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"0",
                                "cmd_sector"=>"01",
                                "cmd_block"=>"04",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //Depot Code                    
            $temp =  str_pad("".$result[0]['depot_cd'],16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"1",
                                "cmd_sector"=>"01",
                                "cmd_block"=>"05",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //DOB[6] + Card expiry[6] + Gender[1]+Card Type[1]([N]/[C]) + SPACE [2]   
            $temp =  str_pad($result[0]['date_of_birth'].$result[0]['card_expiry_date'].$result[0]['gender'].'N',16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"2",
                                "cmd_sector"=>"01",
                                "cmd_block"=>"06",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //Pass Type
            $temp = str_pad(substr($result[0]['concession_nm'],0,16),16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"3",
                                "cmd_sector"=>"02",
                                "cmd_block"=>"08",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //RFID CARD NO 
            $temp = str_pad(substr($result[0]['trimax_card_id'],0,16),16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"4",
                                "cmd_sector"=>"02",
                                "cmd_block"=>"09",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //CUSTOMER CARD ID
            $temp = str_pad("",16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"5",
                                "cmd_sector"=>"02",
                                "cmd_block"=>"0A",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //Bus Type
            $temp = str_pad(substr($result[0]['bus_type_cd'],0,16),16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"6",
                                "cmd_sector"=>"03",
                                "cmd_block"=>"0C",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //Valid From(Date)                    
            $temp = str_pad(substr($result[0]['activation_date'],0,16),16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"7",
                                "cmd_sector"=>"03",
                                "cmd_block"=>"0D",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //Valid Till(Date)
            $temp = str_pad(substr($result[0]['expiry_date'],0,16),16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"8",
                                "cmd_sector"=>"03",
                                "cmd_block"=>"0E",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //Route From1
            $temp = str_pad(substr($result[0]['from_stop_code'],0,16),16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"9",
                                "cmd_sector"=>"04",
                                "cmd_block"=>"10",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //Route Till 1
            $temp = str_pad(substr($result[0]['till_stop_code'],0,16),16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"10",
                                "cmd_sector"=>"04",
                                "cmd_block"=>"11",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //Via Stop
            $temp = str_pad(substr($result[0]['via_stop_code'],0,16),16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"11",
                                "cmd_sector"=>"04",
                                "cmd_block"=>"12",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //TRIP COUNT PER DAY [3] + ACTUAL FARE IN Rs. [7] + SPACE [6] // blank 
            $temp = str_pad("",16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"12",
                                "cmd_sector"=>"05",
                                "cmd_block"=>"14",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //Trip Date[6] + MACHINE NO.[10] // blank 
            $temp = str_pad("",16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"13",
                                "cmd_sector"=>"05",
                                "cmd_block"=>"15",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //TRIP VALID PER DAY [3] + DAILY FARE IN Rs. [7] + CONCESSION CODE [4] + SPAN ID [3]
            $temp = str_pad(substr($result[0]['allowed_trip_count'],0,3),3,"0",STR_PAD_LEFT).str_pad(substr("",0,7),7,"0",STR_PAD_LEFT).str_pad(substr($result[0]['concession_cd'],0,4),4," ",STR_PAD_RIGHT).str_pad(substr($result[0]['span_id'],0,2),2," ",STR_PAD_LEFT);
            $cmd_data_row = array("cmd_seq"=>"14",
                                "cmd_sector"=>"05",
                                "cmd_block"=>"16",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            
            //MACHIN TRIP NO  [10] + STATE/INTERSTATE CODE[1] // blank 
            $temp = str_pad("",16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"15",
                                "cmd_sector"=>"06",
                                "cmd_block"=>"08",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //ROUTE No. [8] + SOURCE STG No. [3] + DEST. STAGE No. [3] + BUS SERVICE TYPE [2] // blank 
            $temp = str_pad("",16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"16",
                                "cmd_sector"=>"06",
                                "cmd_block"=>"19",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //CURRENT TICKET POINTS [6] + DATE  + TIME // blank 
            $temp = str_pad("",16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"17",
                                "cmd_sector"=>"06",
                                "cmd_block"=>"1A",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //CURRENT WAYBILL No. [10]+ DEPOT[6]  // blank 
            $temp = str_pad("",16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"18",
                                "cmd_sector"=>"07",
                                "cmd_block"=>"1C",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //CURRENT TICKET NO   // blank 
            $temp = str_pad("",16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"19",
                                "cmd_sector"=>"07",
                                "cmd_block"=>"1D",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //LAST WAYBILL No.[10] + DEPOT [6] // blank 
            $temp = str_pad("",16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"20",
                                "cmd_sector"=>"07",
                                "cmd_block"=>"1E",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //LAST TICKET NO // blank 
            $temp = str_pad("",16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"21",
                                "cmd_sector"=>"08",
                                "cmd_block"=>"20",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //Route From 2(UNUSED BLOCK) // blank 
            $temp = str_pad("",16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"22",
                                "cmd_sector"=>"08",
                                "cmd_block"=>"21",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //Route Till 2(UNUSED BLOCK) // blank 
            $temp = str_pad("",16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"23",
                                "cmd_sector"=>"08",
                                "cmd_block"=>"22",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);

            //print_r($cmd_data);            die();
            $this->db->insert_batch('ps_command_master',$cmd_data);
            //die();
            return array('status' => 'success', 'msg' => 'Command inserted Successfully.');
        }else{
            return array('status' => 'failure', 'msg' => 'No data found');
        }    
        
        //return array('data' => $cmd_data, 'card_tran_id'=>$result[0]['card_tran_id']);
    }

   

    public function save_command_data_renew($t_id){

        $SQL = "SELECT ct.id as card_tran_id, ct.depot_cd,
        date_format(cm.date_of_birth,'%d%m%y') as date_of_birth,
        date_format(ccm.card_expiry_date,'%d%m%y') as card_expiry_date,
        cm.gender,conm.concession_cd,conm.concession_nm,
        ccm.trimax_card_id,ct.bus_type_cd, 
        date_format(IFNULL(ct.activation_date,'0000-00-00'),'%d%m%y') as activation_date,
        date_format(IFNULL(ct.expiry_date,'0000-00-00'),'%d%m%y') as expiry_date,
        ctd.from_stop_code,ctd.till_stop_code,ctd.via_stop_code, cfm.allowed_trip_count, ct.span_id
        from ps_card_transaction ct
        inner join ps_customer_card_master ccm on ccm.trimax_card_id = ct.trimax_card_id
        inner join ps_customer_master cm on cm.id = ccm.customer_id 
        inner join ps_concession_master conm on conm.id = ct.pass_concession_id	
        inner join ps_card_transaction_details ctd on ctd.card_transaction_id = ct.id
        inner join ps_card_fee_master cfm on cfm.id = ct.card_fee_master_id
        where ct.trimax_card_id = '".$t_id."'
        and ct.write_flag = '1'
        and ct.is_active = '1';";
        
        $query = $this->db->query($SQL);
		
		$result = $query->result_array();
        //print_r($result);
        
        if(count($result) == 1){
            //echo "hi";die();
            $cmd_data=array();

            //DOB[6] + Card expiry[6] + Gender[1]+Card Type[1]([N]/[C]) + SPACE [2]   
            $temp =  str_pad($result[0]['date_of_birth'].$result[0]['card_expiry_date'].$result[0]['gender'].'N',16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"2",
                                "cmd_sector"=>"01",
                                "cmd_block"=>"06",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //Pass Type
            $temp = str_pad(substr($result[0]['concession_nm'],0,16),16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"3",
                                "cmd_sector"=>"02",
                                "cmd_block"=>"08",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //Bus Type
            $temp = str_pad(substr($result[0]['bus_type_cd'],0,16),16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"6",
                                "cmd_sector"=>"03",
                                "cmd_block"=>"0C",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);            
            //Valid From(Date)                    
            $temp = str_pad(substr($result[0]['activation_date'],0,16),16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"7",
                                "cmd_sector"=>"03",
                                "cmd_block"=>"0D",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //Valid Till(Date)
            $temp = str_pad(substr($result[0]['expiry_date'],0,16),16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"8",
                                "cmd_sector"=>"03",
                                "cmd_block"=>"0E",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //Route From1
            $temp = str_pad(substr($result[0]['from_stop_code'],0,16),16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"9",
                                "cmd_sector"=>"04",
                                "cmd_block"=>"10",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //Route Till 1
            $temp = str_pad(substr($result[0]['till_stop_code'],0,16),16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"10",
                                "cmd_sector"=>"04",
                                "cmd_block"=>"11",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);
            //Via Stop
            $temp = str_pad(substr($result[0]['via_stop_code'],0,16),16," ",STR_PAD_RIGHT);
            $cmd_data_row = array("cmd_seq"=>"11",
                                "cmd_sector"=>"04",
                                "cmd_block"=>"12",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);

            $temp = str_pad(substr($result[0]['allowed_trip_count'],0,3),3,"0",STR_PAD_LEFT).str_pad(substr("",0,7),7,"0",STR_PAD_LEFT).str_pad(substr($result[0]['concession_cd'],0,4),4," ",STR_PAD_RIGHT).str_pad(substr($result[0]['span_id'],0,2),2," ",STR_PAD_LEFT);
            $cmd_data_row = array("cmd_seq"=>"14",
                                "cmd_sector"=>"05",
                                "cmd_block"=>"16",
                                "cmd_value"=>"".$temp,
                                "cmd_base64"=>"".base64_encode($temp),
                                "card_tran_id"=>$result[0]['card_tran_id'],
                                "trimax_card_id"=>$t_id);
            $temp="";
            array_push($cmd_data,$cmd_data_row);


            //print_r($cmd_data);            
            $this->db->insert_batch('ps_command_master',$cmd_data);
            // "hi";die();
            return array('status' => 'success', 'msg' => 'Command inserted Successfully.');
        }else{
            return array('status' => 'failure', 'msg' => 'No data found');
        }    
        
        //return array('data' => $cmd_data, 'card_tran_id'=>$result[0]['card_tran_id']);
    }
    

    public function receipt_number($issue_name,$created_by) {
        /*
        $this->load->model(array('Mainmodel','Utility_model'));
        $this->Utility_model->receipt_number('RECEIPT','1');
        Req : 
        issue_name : RECEIPT
        created_by : Created by
        */

        $app_code = date('ymd');
        $data=array('app_code'=>$app_code,
            'issue_name'=>$issue_name,
            'created_by'=>$created_by);
		$this->db->insert('seq_generator',$data);
        //echo $this->db->insert_id();
        return str_pad($app_code,6,"0",STR_PAD_RIGHT).str_pad($this->db->insert_id(),7,"0",STR_PAD_LEFT);
        
    }
    
    
}