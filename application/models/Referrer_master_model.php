<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Referrer_master_model extends MY_Model {

    var $table = 'referrer_master';
    var $fields = array("id", "referrer_name", "referrer_mobile", "referrer_email", "referrer_code", "is_status", "is_deleted");
    var $key = "id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    function get_referrer() {
        $this->db->select("referrer_name, referrer_mobile, referrer_code, concat(referrer_name, ' (',referrer_code,') ',' - ', referrer_mobile) AS value", FALSE);

        $referrer = $this->db->get($this->table);
        if ($referrer->num_rows() > 0) {
            return $referrer->result_array();
        }
        return false;
    }

    function is_referrer_exist($input) {

        $this->db->select("referrer_name, referrer_mobile, referrer_code");
        $this->db->where('referrer_code', $input['referrer_code']);
        $this->db->or_where('referrer_mobile', $input['referrer_code']);
        $this->db->where('is_status = "Y"');
        $this->db->where('is_deleted = "N"');
        $referrer_result = $this->db->get($this->table);
        if ($referrer_result->num_rows() > 0) {
            return $referrer_result->row_array();
        }
        return false;
    }

    public function get_employees() {
        $this->db->select('rm.referrer_code,rm.referrer_name', false);
        $this->db->from('referrer_master rm');
        $this->db->join('users u', 'u.referrer_code=rm.referrer_code', 'left');
        $this->db->group_by('u.referrer_code');
        $this->db->order_by('rm.referrer_name');
        $data = $this->db->get();
        if ($data->num_rows() > 0) {
            return $data->result_array();
        } else {
            return false;
        }
    }

    public function ref_wise_agents($input) {


        $this->db->select("rm.id as ref_id,rm.referrer_code,rm.referrer_name,u.id,concat(u.first_name,' ',u.last_name) as username,COUNT(t.ticket_id) AS total_tickets,sum(tot_fare_amt_with_tax) AS transaction_amount", FALSE);
        $this->db->from('users u');
        $this->db->join('referrer_master rm', 'rm.referrer_code = u.referrer_code');
        $this->db->join('tickets t', 't.inserted_by = u.id');
        $this->db->where_in('u.role_id', $fme_agent_role_id);
        $this->db->where_in('t.role_id', $fme_agent_role_id);
        $this->db->where('t.ticket_id !=', '');
        $this->db->where('u.referrer_code !=', "");
        $this->db->where('rm.is_status', 'Y');
        $this->db->where('rm.incentive_applicable', 'Y');
        $this->db->where('t.transaction_status', 'success');
        if ($input['from_date'] != "") {
            $this->db->where('t.inserted_date >= ', $input['from_date']);
        }

        if ($input['to_date'] != "") {
            $this->db->where('t.inserted_date <=', $input['to_date']);
        }

        if (($this->session->userdata('select_referrer'))) {
            //echo  $this->session->userdata('select_referrer');exit;
            $this->db->where('rm.id', $this->session->userdata('select_referrer'));
        }

        $this->db->group_by('u.display_name');
        $this->db->order_by('rm.referrer_name');

        $query = $this->db->get();
        $dis_referrer = $query->result_array();
        return $dis_referrer;
    }

    public function get_all_agents() {

        $this->db->select("rm.id as ref_id,rm.referrer_code,rm.referrer_name,u.id,concat(u.first_name,' ',u.last_name) as username,COUNT(t.ticket_id) AS total_tickets,sum(tot_fare_amt_with_tax) AS transaction_amount", FALSE);
        $this->db->from('users u');
        $this->db->join('referrer_master rm', 'rm.referrer_code = u.referrer_code');
        $this->db->join('tickets t', 't.inserted_by = u.id');
        $this->db->where_in('u.role_id', $fme_agent_role_id);
        $this->db->where_in('t.role_id', $fme_agent_role_id);
        $this->db->where('t.ticket_id !=', '');
        $this->db->where('u.referrer_code !=', "");
        $this->db->where('rm.is_status', 'Y');
        $this->db->where('t.transaction_status', 'success');

        $this->db->group_by('u.display_name');
        $this->db->order_by('rm.referrer_name');

        $query_result = $this->db->get();
        $dis_referrer = $query_result->result_array();

        return $dis_referrer;
    }

    public function get_all_agents_for_employee($input) {

        $this->db->select("rm.id as ref_id,rm.referrer_code,rm.referrer_name,u.id as user_id,u.display_name,COUNT(t.ticket_id) AS total_tickets,sum(tot_fare_amt_with_tax) AS transaction_amount", FALSE);
        $this->db->from('users u');
        $this->db->join('referrer_master rm', 'rm.referrer_code = u.referrer_code');
        $this->db->join('tickets t', 't.inserted_by = u.id');
        if ($input['ref_code'] != "") {
            $this->db->where('rm.referrer_code =', $input['ref_code']);
        }

        $this->db->where_in('u.role_id', $fme_agent_role_id);
        $this->db->where_in('t.role_id', $fme_agent_role_id);
        $this->db->where('t.ticket_id !=', '');
        $this->db->where('u.referrer_code !=', "");
        $this->db->where('rm.is_status', 'Y');
        $this->db->where('t.transaction_status', 'success');
        $this->db->group_by('u.display_name');
        $this->db->order_by('rm.referrer_name');

        $query_result = $this->db->get();
        $dis_referrer = $query_result->result_array();

        return $dis_referrer;
    }

    public function get_active_incentives() {
        $this->db->select("isd.id as slab_id,im.id as incentive_master_id,itl.type_name as incentive_type_name,im.incentive_type,im.incentive_name,im.valid_from,im.valid_to,isd.incentive_id as slab_incentive_id,isd.from_slab,isd.to_slab,im.incentive_type_id,(case when isd.from_slab is null then im.incentive_value else isd.incentive_value end) as incentive_value,isd.incentive_type_applicable_id,isd.is_status", FALSE);
        $this->db->from('incentive_master im');
        $this->db->join('incentive_slab_details  isd', 'im.id = isd.incentive_id', 'left');
        $this->db->join('incentive_type_lookup  itl', 'im.incentive_type_id = itl.id');
        $this->db->where('im.is_status = "Y"');

        $query_result = $this->db->get();
        $incentive_result = $query_result->result_array();

        return $incentive_result;
    }

    public function get_active_incentives_slabs() {
        $this->db->select("isd.to_slab,(case when isd.from_slab is null then im.incentive_value else isd.incentive_value end) as incentive_value", FALSE);
        $this->db->from('incentive_master im');
        $this->db->join('incentive_slab_details  isd', 'im.id = isd.incentive_id', 'left');
        $this->db->join('incentive_type_lookup  itl', 'im.incentive_type_id = itl.id');
        $this->db->where('im.is_status = "Y"');
        $this->db->where('im.incentive_type_id = "3"');
        $query_result = $this->db->get();
        $incentive_result = $query_result->result_array();

        return $incentive_result;
    }

    
    public function agent_first_ticket_date($from_date) {
        eval(FME_AGENT_ROLE_ID);
        $this->db->select("u.id,min(t.inserted_date) as first_date", FALSE);
        $this->db->from('users u');
        $this->db->join('referrer_master rm', 'rm.referrer_code = u.referrer_code');
        $this->db->join('tickets t', 't.inserted_by = u.id');
        $this->db->where_in('u.role_id', $fme_agent_role_id);
        $this->db->where('t.transaction_status', 'success');

        if ($from_date != "") {
            $this->db->where('t.inserted_date <', $from_date);
        }
        $this->db->where('u.referrer_code !=', "");
        $this->db->where('rm.is_status', 'Y');
        $this->db->group_by('u.display_name');
        $this->db->order_by('rm.referrer_name');


        $query_result = $this->db->get();
        $dis_referrer = $query_result->result_array();

        return $dis_referrer;
    }

    public function first_ticket_date() {
        eval(FME_AGENT_ROLE_ID);
        $this->db->select("u.id,min(t.inserted_date) as first_date,tot_fare_amt_with_tax", FALSE);
        $this->db->from('users u');
        $this->db->join('referrer_master rm', 'rm.referrer_code = u.referrer_code');
        $this->db->join('tickets t', 't.inserted_by = u.id');
        $this->db->where_in('u.role_id', $fme_agent_role_id);
        $this->db->where('t.transaction_status', 'success');
        $this->db->where('u.referrer_code !=', "");
        $this->db->where('rm.is_status', 'Y');
        $this->db->group_by('u.display_name');
        $this->db->order_by('rm.referrer_name');
        $query_result = $this->db->get();
        $dis_referrer = $query_result->result_array();
        return $dis_referrer;
    }
    
     public function agent_total_tickets($from_date) {
        eval(FME_AGENT_ROLE_ID);
        $this->db->select("u.id,count(t.ticket_id) as total_ticket_count", FALSE);
        $this->db->from('users u');
       // $this->db->join('referrer_master rm', 'rm.referrer_code = u.referrer_code');
        $this->db->join('tickets t', 't.inserted_by = u.id');
        $this->db->where_in('u.role_id', $fme_agent_role_id);
        $this->db->where('t.transaction_status', 'success');
        $this->db->where('u.referrer_code !=', "");
        $this->db->where('u.status', 'Y');
        if ($from_date != "") {
            $this->db->where('t.inserted_date >', $from_date);
        }
        //$this->db->order_by('u.id');
        $this->db->group_by('u.display_name');
        $query_result = $this->db->get();
        $dis_referrer = $query_result->result_array();
        return $dis_referrer;
    }

    public function get_disbursement_details() {
        $this->db->select("idb.disbursement_amount,idb.disbursement_date,idb.agent_id,idb.total_tickets,idb.	total_transaction_amt,idb.total_disbursement_till_date,idb.inserted_date,idb.total_tickets_till_date,idb.total_trans_amt_till_date,dd.incentive_id,dd.slab_id,dd.incentive_value,dd.till_date,dd.incentive_id,inc.incentive_type_id", FALSE);
        $this->db->from('incentive_disbursement idb');
        $this->db->join('incentive_disbursement_detail dd', 'idb.id = dd.disbursement_id');
        $this->db->join('incentive_master inc', 'inc.id = dd.incentive_id');
        $query_result = $this->db->get();
        $dis_referrer = $query_result->result_array();
        return $dis_referrer;
    }

    public function get_fme_data($email) {
        $this->db->select("rm.*,u.username,u.first_name,u.last_name,u.mobile_no,u.email,u.role_id,u.gender,DATE_FORMAT(u.dob,'%d-%m-%Y') as dob,u.address1,u.address2,u.pincode,u.state,u.city");
        $this->db->from('referrer_master rm');
        $this->db->join('users u', 'u.email = rm.referrer_email', 'left');
        $this->db->where('rm.referrer_email', $email);
        $this->db->where('rm.is_status', 'Y');
        $qury_result = $this->db->get();
        return $qury_result->row_array();
    }

}
