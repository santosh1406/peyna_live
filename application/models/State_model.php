<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class State_model extends CI_Model
{
    /**
     * Instanciar o CI
     */
    var $table 	= 'state';
    var $fields		= "state_id,state_name,state_code";

    public function __construct()
    {
            parent::__construct();
            $CI 			=	& get_instance();
            $this->fields	=	explode(",", $this->fields);
            $this->key 	    =	"id";
            $this->reset();
    }

    /*
     * this may create unexpected result if following values are set,
     * better use function above;
     */
    function reset()
    {
        foreach($this->fields as $key)
        {
                $this->$key = NULL;
        }
    }
    
    function select_cond($userdata = array())
    {
        $result = false;		

        foreach($this->fields as $field)
        {
            if($this->$field)
            {
                    $this->$field = htmlentities($this->$field);
                    $this->db->where($field, $this->$field);
            }
        }

        if(isset($userdata['searchtext']) && $userdata['searchtext'] != '' && $userdata['searchtext'] != 'no')
        {
                $this->db->like('option_name',$userdata['searchtext']); 
        }

        if($userdata['sorting_field'] != '' && $userdata['sorting_order'] != '')
        {
                $this->db->order_by($userdata['sorting_field'], $userdata['sorting_order']); 
        }

        if( isset($userdata['start']) && isset($userdata['per_page']) )
        {
            $userdata['start'] = $userdata['start'] ==''? 0 : $userdata['start'];
            $this->db->limit($userdata['per_page'],$userdata['start']);
        }



        $query = $this->db->get($this->table);

        if($this->id)
        {
                $result = $query->row_object();
        }
        else
        {
                $result = $query->result();
        }
        $this->reset();

         // show($this->db->last_query(),1);
//		$result['total_rows']   = $this->select_count($userdata);
//                show($result['total_rows'],1);
//		  show($result,1);
        return remove_html_entities($result);
    }
        
        function select_count($userdata = array())
	{
		$result = false;		
		
                $this->db->select('count(*) as count', FALSE);
		foreach($this->fields as $field)
		{
			if($this->$field)
			{
				$this->$field = htmlentities($this->$field);
				$this->db->where($field, $this->$field);
			}
		}
                
                if($userdata['searchtext'] != '' && $userdata['searchtext'] != 'no')
                {
                    // $this->db->like('option_name',$userdata['searchtext']); 
                }

		$query = $this->db->get($this->table);

		if($this->id)
		{
			$result = $query->row_object();
		}
		else
		{
			$result = $query->result();
		}
		$this->reset();
		
//		 show($this->db->last_query(),1);
//		 show($result,1);
		
		return remove_html_entities($result[0]->count);
	}

    function select()
    {
        $result = false;		

        foreach($this->fields as $field)
        {
            if($this->$field)
            {
                $this->$field = htmlentities($this->$field);
                $this->db->where($field, $this->$field);
            }
        }
        
        $this->db->order_by('id','DESC');

        $query = $this->db->get($this->table);

        if($this->id)
        {
            $result = $query->row_object();
        }
        else
        {
            $result = $query->result();
        }
        $this->reset();

        // show($this->db->last_query(),1);

        return remove_html_entities($result);
    }

    /*updates only by ID**/
    function save()
      {
            $result = false;

                    foreach($this->fields as $field)
                    {
                            if($this->$field !== NULL)
                            {
                                    $entry[$field] = htmlentities($this->$field);////htmlentities added mysql_real_escape_string not needed
                            }
                    }

                    if($this->id)
                    {
                            $this->id 	= htmlentities($this->id);////htmlentities added mysql_real_escape_string not needed
                            $this->db->where($this->key, $this->id);
                            $this->db->update($this->table, $entry);

                            $result 	= $this->id;
                    }
                    else
                    {
                            $this->db->insert($this->table, $entry);
                            $result 	= $this->db->insert_id();
                    }

        return $result;
    }
    
    function get_state()
    {
        $this->db->select('state_id,state_name,state_code');
        
        $data = $this->db->get('state');
        
        return $data->result_array();
    }
    
    function getcountry()
    {
        $this->db->select('intCountryId,stCountryName');
        $contry_data=$this->db->get('country_master');
        return  $contry_data->result_array();
    }
     function getstate($country_id)
    {
        
         $this->db->where('intCountryId',$country_id);
         $contry_data=$this->db->get('state_master');
       
        return  $contry_data->result_array();
    }  
    
         function getcity($state_id)
    {
        $this->db->where('intStateId',$state_id);
        $contry_data=$this->db->get('city_master');
        return  $contry_data->result_array();
    }  
    
	
}
