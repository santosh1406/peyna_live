<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Travel_destinations_model extends MY_Model {

    /**
     * Instanciar o CI
     */
    var $table = 'travel_destinations';
    var $fields = array("id","boarding_stop_id","boarding_stop_name","destination_stop_id","destination_stop_name","trip_no","sch_departure_date","sch_departure_tm","arrival_date","arrival_tm","day","start_stop_sch_departure_date","end_stop_arrival_date","tot_seat_available","seat_fare","senior_citizen_fare","child_seat_fare","sleeper_fare","created_date","updated_date","record_status");

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    function update_arrival_tm() {
        $this->db->select('t1.id, t1.travel_id, t1.boarding_stop_id, t1.destination_stop_id, t1.sch_departure_date, t1.sch_departure_tm, 
                t2.sch_departure_date as t2_sch_departure_date, t2.sch_departure_tm as t2_sch_departure_tm,
                t2.arrival_date, t2.arrival_tm, t2.day');
        $this->db->from('travel_destinations t1');
        // $this->db->where('t2.day >', '1');

        $this->db->join('travel_destinations t2', 't1.destination_stop_id = t2.boarding_stop_id');
        $this->db->group_by("t1.travel_id");
        // $this->db->limit(2,0);
        $query = $this->db->get();

        $data = $query->result_array();

        $update = array();

        if (count($data) > 0) {
            foreach ($data as $key => $value) {

                //if ($value['arrival_date'] == '0000-00-00 00:00:00' && $value['arrival_tm'] !== '00:00:00') {
                if ($value['day'] > 1) {
                    $arrival_date = date('Y-m-d H:i:s', strtotime($value['sch_departure_date'] . ' ' . $value['arrival_tm'] . '+' . ($value['day'] - 1) . 'day'));
                } else {
                    if (strtotime($value['arrival_tm']) < strtotime($value['sch_departure_tm'])) {
                        $arrival_date = date('Y-m-d H:i:s', strtotime($value['sch_departure_date'] . ' ' . $value['arrival_tm'] . '+1 day'));
                    } else {
                        $arrival_date = date('Y-m-d H:i:s', strtotime($value['sch_departure_date'] . ' ' . $value['arrival_tm']));
                    }
                }

                $this->id = $value['id'];
                $this->arrival_date = $arrival_date;
                $this->save();
                $this->reset();
                //}

                if (($key % 1000) == 0) {
                    sleep(1);
                    $this->db->reconnect();
                }
            }
        }
    }

    function get_search($res = array())
    {
        $data = array();
        $make_query = "";
        $trip_master_make_query = "";
         /*check to stop count in grouping table*/
        $this->db->select('suggested_nm,bus_stop_cd');
        $this->db->from('bus_stop_groups');
        $this->db->like('suggested_nm',$res['from']);
        $from_data = $this->db->get();
        $from_stop = $from_data->result_array();
        $from_stop_cnt = count($from_stop);

        /*check to stop count in grouping table*/
        $this->db->select('suggested_nm,bus_stop_cd');
        $this->db->from('bus_stop_groups');
        $this->db->like('suggested_nm',$res['to']);
        $to_data = $this->db->get();

        $to_stop = $to_data->result_array();
        $to_stop_cnt = count($to_stop);

        if($to_stop_cnt && !$from_stop_cnt)
        {
            $make_query = " t1.boarding_stop_name = '".$res['from']."'";
        }
        else if($from_stop_cnt && !$to_stop_cnt)
        {
            $make_query = " t1.destination_stop_name = '".$res['to']."'";
        }
        else if($to_stop_cnt && $from_stop_cnt )
        {
            $make_query = "";
        }
        else
        {
            $make_query = " t1.boarding_stop_name = '".$res['from']."' and t1.destination_stop_name = '".$res['to']."'";
        }

        // $make_query_date = " t1.sch_departure_date = '".date("Y-m-d",strtotime($res['date']))."' and CONCAT(DATE_FORMAT(t1.sch_departure_date,'%Y-%m-%d'),' ', DATE_FORMAT(t1.sch_departure_tm,'%H:%i-%s')) > '".date("Y-m-d H:i:s")."'";
        $make_query_date = " t1.sch_departure_date = '".date("Y-m-d",strtotime($res['date']))."' and CONCAT(DATE_FORMAT(t1.sch_departure_date,'%Y-%m-%d'),' ', DATE_FORMAT(t1.sch_departure_tm,'%H:%i:%s')) > '".date("Y-m-d H:i:s")."'";
        
        if($make_query != "")
        {
            $make_query .= " and ".$make_query_date;
        }
        else
        {
            $make_query = $make_query_date;
        }
        
        $make_query = " where ".$make_query;

        if (isset($res['trip_no']) && count($res['trip_no']) != "")
        {
            $trip_master_make_query = " and tm.trip_no = '".$res['trip_no']."'";
        }

        if (isset($res['op_trip_no']) && count($res['op_trip_no']) != "")
        {
            $trip_master_make_query .= " and tm.op_trip_no = '".$res['op_trip_no']."'";
        }


        $this->db->select("
                    t.id as bus_travel_id,
                    om.user_id as op_user_id,
                    from_osm.op_stop_cd as from_stop_code, 
                    to_osm.op_stop_cd as to_stop_code,
                    om.op_name,
                    t.trip_no,
                    tm.op_id, 
                    tm.route_no_name,
                    bsm.bus_stop_name,
                    tm.op_trip_no,
                    tm.op_trip_no as service_id,
                    tm.seat_layout_id,
                    osl.layout_name, 
                    t.boarding_stop_name as from_stop, 
                    t.destination_stop_name as to_stop,
                    concat(t.sch_departure_date, ' ', t.sch_departure_tm) as dept_tm, 
                    DATE_FORMAT(t.sch_departure_date, '%d-%m-%Y') as  sch_departure_date,
                    t.arrival_date, 
                    t.arrival_tm, 
                    t.day,
                    bt.id as bus_type_id,
                    bt.bus_type_name,
                    t.sch_departure_tm,
                    t.arrival_tm,
                    obt.op_bus_type_name,
                    obt.op_bus_type_cd,
                    obt.is_child_concession,
                    if(ISNULL(t.tot_seat_available),osl.ttl_seats,t.tot_seat_available) as tot_seat_available, t.seat_fare, t.child_seat_fare, t.senior_citizen_fare,
                    if(t.day>0,t.day -1, t.day) as arrival_day,
                    t.arrival_date as arrival_time,
                    tm.provider_id,
                    tm.provider_type
                ", false);

        $this->db->from(
                        "(SELECT 
                            t1.*, tm1.op_id AS grouping_op_id 
                            FROM travel_destinations t1 
                            INNER JOIN trip_master tm1 
                            ON t1.trip_no = tm1.trip_no
                            ".$make_query."
                            ) 
                            as t");
        $this->db->join("trip_master tm", "tm.trip_no = t.trip_no ".$trip_master_make_query, "inner");
        $this->db->join("bus_stop_master bsm", "t.destination_stop_id = bsm.bus_stop_id", "inner");
        $this->db->join("op_master om", "om.op_id = tm.op_id", "inner");
        $this->db->join("op_bus_types obt", "obt.id = tm.op_bus_type_id", "inner");
        $this->db->join("op_seat_layout osl", "osl.id = tm.seat_layout_id", "inner");
        $this->db->join("bus_type bt", "bt.id = obt.bus_type_id", "left");

        $this->db->join("op_stop_master from_osm", "t.boarding_stop_id = from_osm.bus_stop_id and from_osm.op_id = tm.op_id");
        $this->db->join("op_stop_master to_osm", "t.destination_stop_id = to_osm.bus_stop_id and to_osm.op_id = tm.op_id");
        // $this->db->join("op_stop_master osm", "bsm.bus_stop_id = osm.bus_stop_id");

        if($to_stop_cnt && !$from_stop_cnt)
        {
            $this->db->join("(select bsg.suggested_nm,osm.op_stop_name as stop_name, bsg.op_id from bus_stop_groups  bsg inner join op_stop_master  osm on osm.op_stop_cd=bsg.bus_stop_cd where suggested_nm like  '%".$res['to']."%') bsg1",'t.destination_stop_name = bsg1.stop_name and bsg1.op_id = t.grouping_op_id', 'left');
            $this->db->like('bsg1.suggested_nm',$res['to'],'both');
            // $this->db->where('t.boarding_stop_name',$res['from']);
        }
        else if($from_stop_cnt && !$to_stop_cnt)
        {
            $this->db->join("(select bsg.suggested_nm,osm.op_stop_name as stop_name, bsg.op_id from bus_stop_groups  bsg inner join op_stop_master  osm on osm.op_stop_cd=bsg.bus_stop_cd where suggested_nm like '%".$res['from']."%') bsg1",'t.boarding_stop_name = bsg1.stop_name and bsg1.op_id = t.grouping_op_id', 'left');
            $this->db->like('bsg1.suggested_nm',$res['from'],'both');
            // $this->db->where('t.destination_stop_name',$res['to']);
        }
        else if($to_stop_cnt && $from_stop_cnt )
        {
            $this->db->join("(select bsg.suggested_nm,osm.op_stop_name as stop_name, bsg.op_id from bus_stop_groups  bsg inner join op_stop_master  osm on osm.op_stop_cd = bsg.bus_stop_cd where suggested_nm like  '%".$res['from']."%') bsg1",'t.boarding_stop_name = bsg1.stop_name  and bsg1.op_id = t.grouping_op_id', 'left');
            $this->db->join("(select bsg.suggested_nm,osm.op_stop_name as stop_name, bsg.op_id from bus_stop_groups  bsg inner join op_stop_master  osm on osm.op_stop_cd = bsg.bus_stop_cd where suggested_nm like  '%".$res['to']."%') bsg2",'t.destination_stop_name = bsg2.stop_name  and bsg2.op_id = t.grouping_op_id', 'left');
            $this->db->like('bsg1.suggested_nm',$res['from'],'both');
            $this->db->like('bsg2.suggested_nm',$res['to'],'both');
        }
        else
        {
            // $this->db->where('t.boarding_stop_name',$res['from']);
            // $this->db->where('t.destination_stop_name',$res['to']);
        }

       /* if (isset($res['trip_no']) && count($res['trip_no']) != "")
        {
            $this->db->where('tm.trip_no', $res['trip_no']);
        }

        if (isset($res['op_trip_no']) && count($res['op_trip_no']) != "")
        {
            $this->db->where('tm.op_trip_no', $res['op_trip_no']);
        }*/

        /*$this->db->where("DATE_FORMAT(t.sch_departure_date,'%Y-%m-%d')", date("Y-m-d",strtotime($res['date'])));
        $this->db->where("CONCAT(DATE_FORMAT(t.sch_departure_date,'%Y-%m-%d'),' ', DATE_FORMAT(t.sch_departure_tm,'%H:%i-%s')) > ", date("Y-m-d H:i:s"));*/

        /*$this->db->where("t.sch_departure_date", date("Y-m-d",strtotime($res['date'])));
        $this->db->where("CONCAT(DATE_FORMAT(t.sch_departure_date,'%Y-%m-%d'),' ', DATE_FORMAT(t.sch_departure_tm,'%H:%i-%s')) > ", date("Y-m-d H:i:s"));*/
        // $this->db->where("CONCAT(t.sch_departure_date,' ',t.sch_departure_tm) > ", date("Y-m-d H:i:s"));
        $this->db->where("tm.status", "Y");

        // $this->db->group_by("t.trip_no");
        $this->db->group_by("t.trip_no,t.boarding_stop_id,t.destination_stop_id");
        $this->db->order_by("t.sch_departure_tm");
        $query = $this->db->get();
        
        log_data("tmp/query/".date("Y-m-d").".log",$this->db->last_query(),"test query");

        return $query->result_array();
    }


    function get_search_op_wise($res = array(), $operator = NULL)
    {
        $data = array();
        $make_query = "";
        $trip_master_make_query = "";
         /*check to stop count in grouping table*/
        $this->db->select('suggested_nm,bus_stop_cd');
        $this->db->from('bus_stop_groups');
        $this->db->like('suggested_nm',$res['from']);
        $from_data = $this->db->get();
        $from_stop = $from_data->result_array();
        $from_stop_cnt = count($from_stop);

        /*check to stop count in grouping table*/
        $this->db->select('suggested_nm,bus_stop_cd');
        $this->db->from('bus_stop_groups');
        $this->db->like('suggested_nm',$res['to']);
        $to_data = $this->db->get();

        $to_stop = $to_data->result_array();
        $to_stop_cnt = count($to_stop);

        if($to_stop_cnt && !$from_stop_cnt)
        {
            $make_query = " t1.boarding_stop_name = '".$res['from']."'";
        }
        else if($from_stop_cnt && !$to_stop_cnt)
        {
            $make_query = " t1.destination_stop_name = '".$res['to']."'";
        }
        else if($to_stop_cnt && $from_stop_cnt )
        {
            $make_query = "";
        }
        else
        {
            $make_query = " t1.boarding_stop_name = '".$res['from']."' and t1.destination_stop_name = '".$res['to']."'";
        }

        $make_query_date = " t1.sch_departure_date = '".date("Y-m-d",strtotime($res['date']))."' and CONCAT(DATE_FORMAT(t1.sch_departure_date,'%Y-%m-%d'),' ', DATE_FORMAT(t1.sch_departure_tm,'%H:%i-%s')) > '".date("Y-m-d H:i:s")."'";
        
        if($make_query != "")
        {
            $make_query .= " and ".$make_query_date;
        }
        else
        {
            $make_query = $make_query_date;
        }
        
        $make_query = " where ".$make_query;

        if (isset($res['trip_no']) && count($res['trip_no']) != "")
        {
            $trip_master_make_query = " and tm.trip_no = '".$res['trip_no']."'";
        }

        if (isset($res['op_trip_no']) && count($res['op_trip_no']) != "")
        {
            $trip_master_make_query .= " and tm.op_trip_no = '".$res['op_trip_no']."'";
        }


        $this->db->select("
                    om.user_id as op_user_id,
                    from_osm.op_stop_cd as from_stop_code, 
                    to_osm.op_stop_cd as to_stop_code,
                    om.op_name,
                    t.trip_no,
                    tm.op_id, 
                    tm.route_no_name,
                    bsm.bus_stop_name,
                    tm.op_trip_no,
                    tm.op_trip_no as service_id,
                    tm.seat_layout_id,
                    osl.layout_name, 
                    t.boarding_stop_name as from_stop, 
                    t.destination_stop_name as to_stop,
                    concat(t.sch_departure_date, ' ', t.sch_departure_tm) as dept_tm, 
                    DATE_FORMAT(t.sch_departure_date, '%d-%m-%Y') as  sch_departure_date,
                    t.arrival_date, 
                    t.arrival_tm, 
                    t.day,
                    bt.id as bus_type_id,
                    bt.bus_type_name,
                    t.sch_departure_tm,
                    t.arrival_tm,
                    obt.op_bus_type_name,
                    obt.op_bus_type_cd,
                    obt.is_child_concession,
                    if(ISNULL(t.tot_seat_available),osl.ttl_seats,t.tot_seat_available) as tot_seat_available, t.seat_fare,
                    if(t.day>0,t.day -1, t.day) as arrival_day,
                    t.arrival_date as arrival_time,
                    tm.provider_id,
                    tm.provider_type
                ", false);

        $this->db->from(
                        "(SELECT 
                            t1.*, tm1.op_id AS grouping_op_id 
                            FROM travel_destinations t1 
                            INNER JOIN trip_master tm1 
                            ON t1.trip_no = tm1.trip_no
                            ".$make_query."
                            ) 
                            as t");
        $this->db->join("trip_master tm", "tm.trip_no = t.trip_no ".$trip_master_make_query, "inner");
        $this->db->join("bus_stop_master bsm", "t.destination_stop_id = bsm.bus_stop_id", "inner");
        $this->db->join("op_master om", "om.op_id = tm.op_id", "inner");
        $this->db->join("op_bus_types obt", "obt.id = tm.op_bus_type_id", "inner");
        $this->db->join("op_seat_layout osl", "osl.id = tm.seat_layout_id", "inner");
        $this->db->join("bus_type bt", "bt.id = obt.bus_type_id", "left");

        $this->db->join("op_stop_master from_osm", "t.boarding_stop_id = from_osm.bus_stop_id and from_osm.op_id = tm.op_id");
        $this->db->join("op_stop_master to_osm", "t.destination_stop_id = to_osm.bus_stop_id and to_osm.op_id = tm.op_id");
        // $this->db->join("op_stop_master osm", "bsm.bus_stop_id = osm.bus_stop_id");

        if($to_stop_cnt && !$from_stop_cnt)
        {
            $this->db->join("(select bsg.suggested_nm,osm.op_stop_name as stop_name, bsg.op_id from bus_stop_groups  bsg inner join op_stop_master  osm on osm.op_stop_cd=bsg.bus_stop_cd where suggested_nm like  '%".$res['to']."%') bsg1",'t.destination_stop_name = bsg1.stop_name and bsg1.op_id = t.grouping_op_id', 'left');
            $this->db->like('bsg1.suggested_nm',$res['to'],'both');
            // $this->db->where('t.boarding_stop_name',$res['from']);
        }
        else if($from_stop_cnt && !$to_stop_cnt)
        {
            $this->db->join("(select bsg.suggested_nm,osm.op_stop_name as stop_name, bsg.op_id from bus_stop_groups  bsg inner join op_stop_master  osm on osm.op_stop_cd=bsg.bus_stop_cd where suggested_nm like '%".$res['from']."%') bsg1",'t.boarding_stop_name = bsg1.stop_name and bsg1.op_id = t.grouping_op_id', 'left');
            $this->db->like('bsg1.suggested_nm',$res['from'],'both');
            // $this->db->where('t.destination_stop_name',$res['to']);
        }
        else if($to_stop_cnt && $from_stop_cnt )
        {
            $this->db->join("(select bsg.suggested_nm,osm.op_stop_name as stop_name, bsg.op_id from bus_stop_groups  bsg inner join op_stop_master  osm on osm.op_stop_cd = bsg.bus_stop_cd where suggested_nm like  '%".$res['from']."%') bsg1",'t.boarding_stop_name = bsg1.stop_name  and bsg1.op_id = t.grouping_op_id', 'left');
            $this->db->join("(select bsg.suggested_nm,osm.op_stop_name as stop_name, bsg.op_id from bus_stop_groups  bsg inner join op_stop_master  osm on osm.op_stop_cd = bsg.bus_stop_cd where suggested_nm like  '%".$res['to']."%') bsg2",'t.destination_stop_name = bsg2.stop_name  and bsg2.op_id = t.grouping_op_id', 'left');
            $this->db->like('bsg1.suggested_nm',$res['from'],'both');
            $this->db->like('bsg2.suggested_nm',$res['to'],'both');
        }
        else
        {
            // $this->db->where('t.boarding_stop_name',$res['from']);
            // $this->db->where('t.destination_stop_name',$res['to']);
        }

        if (!empty($operator))
        {
            // $this->db->where('tm.op_id', $operator);
            $this->db->where('om.op_name', $operator);
        }
        
       /* if (isset($res['trip_no']) && count($res['trip_no']) != "")
        {
            $this->db->where('tm.trip_no', $res['trip_no']);
        }

        if (isset($res['op_trip_no']) && count($res['op_trip_no']) != "")
        {
            $this->db->where('tm.op_trip_no', $res['op_trip_no']);
        }*/

        /*$this->db->where("DATE_FORMAT(t.sch_departure_date,'%Y-%m-%d')", date("Y-m-d",strtotime($res['date'])));
        $this->db->where("CONCAT(DATE_FORMAT(t.sch_departure_date,'%Y-%m-%d'),' ', DATE_FORMAT(t.sch_departure_tm,'%H:%i-%s')) > ", date("Y-m-d H:i:s"));*/

        /*$this->db->where("t.sch_departure_date", date("Y-m-d",strtotime($res['date'])));
        $this->db->where("CONCAT(DATE_FORMAT(t.sch_departure_date,'%Y-%m-%d'),' ', DATE_FORMAT(t.sch_departure_tm,'%H:%i-%s')) > ", date("Y-m-d H:i:s"));*/
        // $this->db->where("CONCAT(t.sch_departure_date,' ',t.sch_departure_tm) > ", date("Y-m-d H:i:s"));
        $this->db->where("tm.status", "Y");

        $this->db->group_by("t.trip_no");
        // $this->db->group_by("t.trip_no,t.boarding_stop_id,t.destination_stop_id");
        $query = $this->db->get();
        
        log_data("tmp/query/test.log",$this->db->last_query(),"test query");

        return $query->result_array(); 
    }

    function get_search_op_wise_previous($res = array(), $operator = NULL)
    {
        $data = array();

         /*check to stop count in grouping table*/
        $this->db->select('suggested_nm,bus_stop_cd');
        $this->db->from('bus_stop_groups');
        $this->db->like('suggested_nm',$res['from']);
        $from_data = $this->db->get();
        $from_stop = $from_data->result_array();
        $from_stop_cnt = count($from_stop);

        /*check to stop count in grouping table*/
        $this->db->select('suggested_nm,bus_stop_cd');
        $this->db->from('bus_stop_groups');
        $this->db->like('suggested_nm',$res['to']);
        $to_data = $this->db->get();

        $to_stop = $to_data->result_array();
        $to_stop_cnt = count($to_stop);

        $this->db->select("
                    from_osm.op_stop_cd as from_stop_code, 
                    to_osm.op_stop_cd as to_stop_code,
                    om.op_name,
                    t.trip_no,
                    tm.op_id, 
                    tm.route_no_name,
                    bsm.bus_stop_name,
                    tm.op_trip_no,
                    tm.op_trip_no as service_id,
                    tm.seat_layout_id,
                    osl.layout_name, 
                    t.boarding_stop_name as from_stop, 
                    t.destination_stop_name as to_stop,
                    concat(t.sch_departure_date, ' ', t.sch_departure_tm) as dept_tm, 
                    DATE_FORMAT(t.sch_departure_date, '%d-%m-%Y') as  sch_departure_date,
                    t.arrival_date, 
                    t.arrival_tm, 
                    t.day,
                    bt.id as bus_type_id,
                    bt.bus_type_name,
                    t.sch_departure_tm,
                    t.arrival_tm,
                    obt.op_bus_type_name,
                    obt.op_bus_type_cd,
                    obt.is_child_concession,
                    if(ISNULL(t.tot_seat_available),osl.ttl_seats,t.tot_seat_available) as tot_seat_available, t.seat_fare,
                    if(t.day>0,t.day -1, t.day) as arrival_day,
                    t.arrival_date as arrival_time,
                    tm.provider_id,
                    tm.provider_type
                ", false);

        $this->db->from("travel_destinations t");
        $this->db->join("trip_master tm", "tm.trip_no = t.trip_no", "inner");
        $this->db->join("bus_stop_master bsm", "t.destination_stop_id = bsm.bus_stop_id", "inner");
        $this->db->join("op_master om", "om.op_id = tm.op_id", "inner");
        $this->db->join("op_bus_types obt", "obt.id = tm.op_bus_type_id", "inner");
        $this->db->join("op_seat_layout osl", "osl.id = tm.seat_layout_id", "inner");
        $this->db->join("bus_type bt", "bt.id = obt.bus_type_id", "left");

        $this->db->join("op_stop_master from_osm", "t.boarding_stop_id = from_osm.bus_stop_id");
        $this->db->join("op_stop_master to_osm", "t.destination_stop_id = to_osm.bus_stop_id");
        // $this->db->join("op_stop_master osm", "bsm.bus_stop_id = osm.bus_stop_id");

        if($to_stop_cnt && !$from_stop_cnt)
        {
            $this->db->join("(select bsg.suggested_nm,osm.op_stop_name as stop_name from bus_stop_groups  bsg inner join op_stop_master  osm on osm.op_stop_cd=bsg.bus_stop_cd where suggested_nm like  '%".$res['to']."%') bsg1",'t.destination_stop_name = bsg1.stop_name', 'left');
            $this->db->like('bsg1.suggested_nm',$res['to'],'both');
            $this->db->where('t.boarding_stop_name',$res['from']);
        }
        else if($from_stop_cnt && !$to_stop_cnt)
        {
            $this->db->join("(select bsg.suggested_nm,osm.op_stop_name as stop_name from bus_stop_groups  bsg inner join op_stop_master  osm on osm.op_stop_cd=bsg.bus_stop_cd where suggested_nm like '%".$res['from']."%') bsg1",'t.boarding_stop_name = bsg1.stop_name', 'left');
            $this->db->like('bsg1.suggested_nm',$res['from'],'both');
            $this->db->where('t.destination_stop_name',$res['to']);
        }
        else if($to_stop_cnt && $from_stop_cnt )
        {
            $this->db->join("(select bsg.suggested_nm,osm.op_stop_name as stop_name from bus_stop_groups  bsg inner join op_stop_master  osm on osm.op_stop_cd = bsg.bus_stop_cd where suggested_nm like  '%".$res['from']."%') bsg1",'t.boarding_stop_name = bsg1.stop_name', 'left');
            $this->db->join("(select bsg.suggested_nm,osm.op_stop_name as stop_name from bus_stop_groups  bsg inner join op_stop_master  osm on osm.op_stop_cd = bsg.bus_stop_cd where suggested_nm like  '%".$res['to']."%') bsg2",'t.destination_stop_name = bsg2.stop_name', 'left');
            $this->db->like('bsg1.suggested_nm',$res['from'],'both');
            $this->db->like('bsg2.suggested_nm',$res['to'],'both');
        }
        else
        {
            $this->db->where('t.boarding_stop_name',$res['from']);
            $this->db->where('t.destination_stop_name',$res['to']);
        }

        

        if (!empty($operator))
        {
            // $this->db->where('tm.op_id', $operator);
            $this->db->where('om.op_name', $operator);
        }

        if (isset($res['trip_no']) && count($res['trip_no']) != "")
        {
            $this->db->where('tm.trip_no', $res['trip_no']);
        }

        if (isset($res['op_trip_no']) && count($res['op_trip_no']) != "")
        {
            $this->db->where('tm.op_trip_no', $res['op_trip_no']);
        }

        $this->db->where("DATE_FORMAT(t.sch_departure_date,'%Y-%m-%d')", date("Y-m-d",strtotime($res['date'])));
        $this->db->where("CONCAT(DATE_FORMAT(t.sch_departure_date,'%Y-%m-%d'),' ', DATE_FORMAT(t.sch_departure_tm,'%H:%i-%s')) > ", date("Y-m-d H:i:s"));
        $this->db->where("tm.status", "Y");

        $this->db->group_by("t.trip_no");
        $query = $this->db->get();
        
        return $query->result_array();
    }


    function get_search_previous($res = array()) {

        //$this->load->model(array('bus_stop_groups_model'));
        

        // $from_data  =  $this->bus_stop_groups_model->get_stop($res['from']);
        // $to_data    =  $this->bus_stop_groups_model->get_stop($res['to']);        


        $data = array();
        /*
        $this->db->select("om.op_name,t.trip_no,tm.op_id, bsm.bus_stop_name,tm.op_trip_no,tm.seat_layout_id,"
                . "concat(t.sch_departure_date, ' ', t.sch_departure_tm) as dept_tm, DATE_FORMAT(t.sch_departure_date, '%d-%m-%Y') as  sch_departure_date,"
                . " t.arrival_date, t.arrival_tm, t.day,bt.bus_type_id,bt.bus_type_name,"
                . "t.sch_departure_tm,t.arrival_tm,bs.op_bus_type_name,if(ISNULL(t.tot_seat_available),osl.ttl_seats,t.tot_seat_available) as tot_seat_available, t.seat_fare,"
                ."if(t.day>0,t.day -1, t.day) as arrival_day," 
                ."concat(date_add(t.sch_departure_date,interval if(t.day>0,t.day -1, t.day) day ),' ',t.arrival_tm) as arrival_time", false);
            */

        $this->db->select("om.op_name,t.trip_no,tm.op_id, bsm.bus_stop_name,tm.op_trip_no,tm.seat_layout_id,"
                . "t.boarding_stop_name as from_stop, t.destination_stop_name as to_stop,"
                . "concat(t.sch_departure_date, ' ', t.sch_departure_tm) as dept_tm, DATE_FORMAT(t.sch_departure_date, '%d-%m-%Y') as  sch_departure_date,"
                . " t.arrival_date, t.arrival_tm, t.day,bt.id as bus_type_id,bt.bus_type_name,"
                . "t.sch_departure_tm,t.arrival_tm,obs.op_bus_type_name,if(ISNULL(t.tot_seat_available),osl.ttl_seats,t.tot_seat_available) as tot_seat_available, t.seat_fare,"
                ."if(t.day>0,t.day -1, t.day) as arrival_day," 
                ."t.arrival_date as arrival_time", false);


        $this->db->from("travel_destinations t");
        //$this->db->join("travel_destinations t2", "t.boarding_stop_id = t2.boarding_stop_id", "inner");
        //$this->db->join("bus_stop_master bsm", "t.boarding_stop_id = bsm.bus_stop_id", "inner");
        $this->db->join("trip_master tm", "tm.trip_no = t.trip_no", "inner");
        $this->db->join("bus_stop_master bsm", "t.destination_stop_id = bsm.bus_stop_id", "inner");
        $this->db->join("op_master om", "om.op_id = tm.op_id", "inner");
        $this->db->join("op_bus_types obs", "obs.id = tm.op_bus_type_id", "inner");
        $this->db->join("op_seat_layout osl", "osl.id = tm.seat_layout_id", "inner");
        $this->db->join("bus_type bt", "bt.id = obs.bus_type_id", "left");
        $this->db->join("op_stop_master osm", "bsm.bus_stop_id = osm.bus_stop_id");

        $this->db->where("DATE_FORMAT(t.sch_departure_date,'%Y-%m-%d')", date("Y-m-d",strtotime($res['date'])));
        $this->db->where("CONCAT(DATE_FORMAT(t.sch_departure_date,'%Y-%m-%d'),' ', DATE_FORMAT(t.sch_departure_tm,'%H:%i-%s')) > ", date("Y-m-d H:i:s"));
        $this->db->where("tm.status", "Y");
        // if(count($from_data) > 0 && count($to_data) > 0)
        // {
        //     $this->db->join("bus_stop_groups bsg", "bsg.bus_stop_cd = osm.op_stop_cd", "left");
        //     $this->db->where("bsg.suggested_nm", $from_data[0]);
        // }
        // else if(count($from_data) > 0)
        // {

        // }
        // else if(count($to_data) > 0)
        // {
        //     $this->db->join("bus_stop_groups bsg", "bsg.bus_stop_cd = osm.op_stop_cd", "left");
        //     $this->db->where("bsg.suggested_nm", $from_data[0]);
        // }
        // else
        // {

        // }


        $this->db->where("t.boarding_stop_name", $res['from']);
        $this->db->where("t.destination_stop_name", $res['to']);

        if (isset($res['trip_no']) && count($res['trip_no']) != "") {
            $this->db->where('tm.trip_no', $res['trip_no']);
        }

        // if (isset($res['travel']) && count($res['travel']) > 0) {
        //     $this->db->where_in('om.op_name', $res['travel']);
        // }

        // if (isset($res['bus_type_cat']) && count($res['bus_type_cat']) > 0) {
        //     $this->db->where_in('bt.bus_type_id', $res['bus_type_cat']);
        // }

        // if (isset($res['dept_tm']) && count($res['dept_tm']) > 0) {
        //     $this->db->where_in('t.sch_departure_tm', $res['dept_tm']);
        // }
       

        $this->db->group_by("t.trip_no");

        $query = $this->db->get();
       //show($this->db->last_query(), 1);
        return $query->result_array();
    }

    function update_fare_totseats($update_travel_data) {


        if (isset($update_travel_data) && count($update_travel_data) > 0)
        {
                
            $update_data = array(
                'tot_seat_available' => $update_travel_data['tot_seats'],
                'seat_fare' => $update_travel_data['fare'],
                'child_seat_fare' => $update_travel_data['child_seat_fare'],
                'senior_citizen_fare' => isset($update_travel_data['senior_citizen_fare']) ? $update_travel_data['senior_citizen_fare'] : 0
            );

            $this->db->where('trip_no', $update_travel_data['trip_no'] );
            $this->db->where('boarding_stop_name', $update_travel_data['from'] );
            $this->db->where('destination_stop_name', $update_travel_data['to'] );
            $this->db->where('sch_departure_date', date('Y-m-d',strtotime($update_travel_data['search_date'])));
            $this->db->update($this->table, $update_data);
        }
    }

    public function get_bus_type_by_trip_no($trip_no)
    {
        $this->db->select("obt.id,tm.trip_no,obt.op_bus_type_name,bt.bus_type_name,bt.bus_category,obt.op_bus_type_cd");
        $this->db->from("trip_master tm");
        $this->db->join("op_bus_types obt", "obt.id = tm.op_bus_type_id", "inner");
        $this->db->join("bus_type bt", "bt.id = obt.bus_type_id", "inner");
        $this->db->where("tm.trip_no", $trip_no);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_last_service_date($op_id)
    {
        $this->db->select('sch_departure_date');
        $this->db->from($this->table);
        $this->db->order_by('sch_departure_date',"desc");
        $this->db->limit(1,0);


        $query  = $this->db->get();
        $result = $query->row(); 

        return $result->sch_departure_date;

    }

    public function get_last_sync_date($op_id)
    {
        $data = $this->column('travel_destinations.sch_departure_date')
                     ->join('trip_master', 'trip_master.trip_no = travel_destinations.trip_no')
                     ->where('trip_master.op_id',$op_id)
                     ->limit(1)
                     ->order_by('travel_destinations.sch_departure_date', 'DESC')
                     ->find_all();

        // show($data,1);
        return $data;
    }

    public function getTripDetail_optripno($op_id,$op_trip_no,$from,$to,$date)
    {
        $this->db->select('t.*,td.start_stop_sch_departure_date,td.end_stop_arrival_date');
        $this->db->from("trip_master t");
        $this->db->join("travel_destinations td","td.trip_no = t.trip_no","inner");
        $this->db->where("t.op_id",$op_id);
        $this->db->where("t.op_trip_no",$op_trip_no);

        $this->db->where("td.boarding_stop_name",$from);
        $this->db->where("td.destination_stop_name",$to);
        $this->db->where("td.sch_departure_date",date("Y-m-d",strtotime($date)));

        $query  = $this->db->get();
        $result = $query->row_array(); 
        return $result;
    }  

    public function get_seat_type_by_trip_no_seat_no($trip_no,$seat_no) {
        
        $this->db->select("old.seat_type");
        $this->db->from("trip_master tm");
        $this->db->join("op_layout_details old", "old.op_seat_layout_id = tm.seat_layout_id", "inner");
        $this->db->where("tm.trip_no", $trip_no);
        $this->db->where("old.seat_no", $seat_no);
        $query = $this->db->get();
        return $query->result_array(); 
    } 
 
}
