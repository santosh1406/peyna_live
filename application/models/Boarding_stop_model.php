<?php

class Boarding_stop_model extends MY_Model {

    var $table = 'bord_ali_stop_master';
    var $fields = array("id","from","to","boarding_stop_id","bus_stop_id","bus_stop_name","stop_code","route_no","trip_no","boarding_stop_name","arrival_duration", "departure_duration", "is_boarding","is_alighting","seq","stop_status");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    function get_routes() {
        $this->db->select('route_id,from_stop,end_stop');
        $this->db->where('record_status', 'Y');
        $this->db->from('routes');
        $data = $this->db->get();
        return $data->result_array();
    }

    function route_stops($routeno) {
        $this->db->select('seq_no,bus_stop_name,bus_stop_id,route_id');

        $this->db->from('route_stops');
        $this->db->where('route_id', $routeno);

        $this->db->order_by("seq_no", "asc");
        $data = $this->db->get();
        return $data->result_array();
    }

    function insertrt($data, $data1, $busstop, $busstnm, $route_no, $sttype) {

        if ($sttype == "alight") {
            $st1 = "Y";
            $st = "N";
        } else {
            $st1 = "N";
            $st = "Y";
        }

          for ($i = 0; $i < count($data1); $i++) {
            $this->db->insert('bord_ali_stop_master', array('boarding_stop_id' => $this->generateid_model->getRequiredId('bord_ali_stop_master'), 
                'boarding_stop_name' =>$data[$i], 
                'arrival_duration' => $data1[$i], 
                'bus_stop_id' => $busstop, 
                'bus_stop_name' =>$busstnm, 
                "route_no" => $route_no, 
                "stop_status" => 'Y', 
                "is_boarding" => $st, 
                "is_alighting" => $st1));
        }

        return $this->db->affected_rows() > 0;
    }

    function getboard($route_no, $stop_no) {
        $this->db->select('boarding_stop_name,arrival_duration');
        $this->db->from('bord_ali_stop_master');
        $this->db->where('stop_status', 'Y');
        $this->db->where('is_boarding', 'Y');
        $this->db->order_by("arrival_duration", "asc");
        $this->db->where('route_no', $route_no);
        $this->db->where('bus_stop_id', $stop_no);
        $data = $this->db->get();
        return $data->result_array();
    }

    function getalight($route_no, $stop_no) {
        $this->db->select('boarding_stop_name,arrival_duration');
        $this->db->from('bord_ali_stop_master');
        $this->db->where('stop_status', 'Y');
        $this->db->where('is_alighting', 'Y');
        $this->db->order_by("arrival_duration", "asc");
        $this->db->where('route_no', $route_no);
        $this->db->where('bus_stop_id', $stop_no);
        $data = $this->db->get();
        return $data->result_array();
    }

    function getstop($id) {
        $this->db->select('bus_stop_name');
        $this->db->from('bus_stop_master');
        $this->db->where('bus_stop_id', $id);
        $btname = $this->db->get();
        $ret = $btname->row();
        return $ret->bus_stop_name;
    }

    function updatepre($route_no, $stop_ty) {
        $this->db->where('route_no', $route_no);
        if ($stop_ty == "bord") {
            $this->db->where('is_boarding', 'Y');
        } else {
            $this->db->where('is_alighting', 'Y');
        }
        $this->db->update('bord_ali_stop_master', array("stop_status" => "N"));
    }

 function getstops($trip, $from_stop_code,$op_bus_type_cd) {
        $this->db->select('bs.*');
        $this->db->from('bord_ali_stop_master bs');
        $this->db->where_in('bs.trip_no', $trip);
        $this->db->where('bs.op_trip_no != ', '');
        $this->db->where('bs.from', $from_stop_code);
        $this->db->where('bs.bus_type', $op_bus_type_cd);
        $this->db->group_by("bs.trip_no , bs.bus_stop_name");
        $res = $this->db->get();
        return $res->result_array();
    }
}

?>
