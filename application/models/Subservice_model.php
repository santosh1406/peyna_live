<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Subservice_model extends MY_Model {
    
    protected $table = 'sub_services';
    var $fields = array("id", "service_name", "service_code", "service_id");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
        
    }

    function get_all_services(){

    $response = array();
 
    // Select record
    $this->db->select('*');
    $q = $this->db->get('service_master');
    $response = $q->result_array();

    return $response;
  }

  function get_jri_id(){
    $this->db->select('*');
    $q = $this->db->get('service_master')->where('service_name', 'JRI');
    $response = $q->result_array();
  }

  // Get sub services by services wise
    function getSubServicebyServices($postData){
    $response = array();   

    $q =$this->db->query("SELECT * FROM service_mapping  m  
    							right join sub_services s 
    							on m.sub_service_id = s.id 
    							where s.service_id = $postData[service]
    							") ;

    $response = $q->result_array();
 	return $response;

     
  }

  	public function saveServiceDetails($data)
    {
     	$insert_query = $this->db->insert_string('service_mapping', $data);
    	$insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);

	    $this->db->query($insert_query);
	    /*echo $this->db->last_query();
	    die();*/

    }

    public function get_agent_services() {
        
            $sQuery = "SELECT * FROM service_mapping  m
			right join sub_services s on m.sub_service_id = s.id
			where s.service_id='1' ";
            $query = $this->db->query($sQuery);
            return $query->result_array();
       
    }

      // Get sub services by services id
    function getSubServicebyServiceID($id){
    $response = array();   

    $q = $this->db->query("SELECT id, service_name as text
        FROM sub_services s 
        WHERE s.status = '1' and s.service_id = '". $id . "'");

    $response = $q->result_array(); 
    //echo $this->db->last_query();
    return $response;

     
  }
}
