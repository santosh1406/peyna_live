<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Rfid_model extends MY_Model {
    
    var $table  = "rfid_unique_card_numbers";
    var $fields = array("rfid_card_id","op_id","unique_card_numbers","is_status", "is_deleted", "is_assigned","created_by", "created_date", "updated_by", "updated_date");
    var $key    = "rfid_card_id";
    
    public function __construct() {
      parent::__construct();
      $this->_init();
    }
    
    public function unique_card_number() 
    {
        $last_id    = $this->get_last_id();
        $month      = date('m');
        $year       = date('y'); 
            

        // $code = "BOSUP".$month.$year.str_pad(($last_id+1), 5, '0', STR_PAD_LEFT);
        $code = "1234";

        $ins_arr = array(
                            'unique_card_numbers'   =>  $code,
                            'created_by'            =>  $this->session->userdata('user_id'),
                            'created_date'          =>  date('Y-m-d H:i:s'),
                        );

        return $rfid =  $this->insert($ins_arr);
    }

    public function unique_card_number_for_api_user($user_id) 
    {
        $last_id    = $this->get_last_id();
        $month      = date('m');
        $year       = date('y'); 
            

        // $code = "BOSUP".$month.$year.str_pad(($last_id+1), 5, '0', STR_PAD_LEFT);
        $code = "1234";

        $ins_arr = array(
                            'unique_card_numbers'   =>  $code,
//                            'unique_card_numbers'   =>  '1234',
                            'is_assigned'           =>  'Y',
                            'created_by'            =>  $user_id,
                            'created_date'          =>  date('Y-m-d H:i:s'),
                        );
        // if(ENVIRONMENT == 'production')
        // {
        //     $ins_arr['unique_card_numbers'] = '1234';
        // }
        
        return $rfid =  $this->insert($ins_arr);        
    }

    
          
}
?>