<?php

class Op_stop_master_model extends MY_Model {

    /**
     * Instanciar o CI
     */
    var $table      = 'op_stop_master';
    var $fields     = array("id","op_stop_id","op_id","op_stop_cd","op_stop_name","bus_stop_id","created_by","created_date","updated_by","updated_date","record_status");
    var $key        = "id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
    
    function get_id() {
        $pid = $this->key;

        $result = false;

        foreach ($this->fields as $field) {
            if ($this->$field) {
                $this->$field = htmlentities($this->$field);
                $this->db->where($field, $this->$field);
            }
        }

        $query = $this->db->get($this->table);

        $result = $query->row_object();
        $this->reset();

        if (count($result) > 0) {
            return remove_html_entities($result->op_stop_id);
        }

        return '';
    }

    function get_busstop_name($bus_stop_cd) {
        $this->db->select("op_stop_name");
        $this->db->where('op_stop_cd', $bus_stop_cd);
        $this->db->from("op_stop_master");
        $query = $this->db->get();
        return $query->row()->op_stop_name;
    }
    function bus_stop_cd($bus_stop_name,$op_id){
        $this->db->select("op_stop_cd");
        $this->db->like('op_stop_name', $bus_stop_name);
        $this->db->where('op_id', $op_id);
        $this->db->from("op_stop_master");
        $query = $this->db->get();
     //  show($this->db->last_query());
        return $query->row()->op_stop_cd;
    } 

}

?>
