<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Dashboard_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->_init();      

        $this->load->model(array('tickets_model','users_model'));
    }
    

    /* @Author 		: Suraj Rathod
	 * @function    : daysInMonth
	 * @description : function will return number of days in particular month,year
	 * @param       : $month,$year
	 */
    function get_ticket_commission($filters)
    {
       /*				NOTICE
    	*Here We have taken where condition static i.e. last 30 days only.
    	*HERE WE CAN MAKE THIS FUCTION FULL DYNAMIC
    	*DO THIS WHEN YOU GET FREE TIME
    	*/
		/*$this->db->select("COUNT(DISTINCT(u.id)) AS total_users,
						   COUNT(t.ticket_id) AS total_tickets,
						   SUM(t.num_passgr) AS number_of_passanger,
						   SUM(t.total_fare_without_discount) AS total_ticket_fare,
						   SUM(t.total_fare_without_discount * (t.commision_percentage/100)) AS commission_value,
						   DATE_FORMAT(t.inserted_date, '%Y-%m-%d') AS ticket_date,
						   SUM(t.tot_fare_amt_with_tax) AS total_amount_paid,
						   SUM(t.discount_value) AS total_discount_value,
						   ((SUM(t.total_fare_without_discount * (t.commision_percentage/100))) - ( SUM(t.discount_value) )) AS cross_check_profit",false);
		$this->db->from("tickets t");
		$this->db->join("users u", "u.id = t.inserted_by", "inner");
		$this->db->group_by("DATE_FORMAT(t.inserted_date, '%Y-%m-%d')");
		$this->db->where("DATE_FORMAT(t.inserted_date, '%Y-%m-%d') >=",'2015-11-12');
		$this->db->where("DATE_FORMAT(t.inserted_date, '%Y-%m-%d') <=", '2015-11-20');
		$query = $this->db->get();
		return $query->result_array();*/

		if(!empty($filters))
		{

			$this->db->select("DATE_FORMAT(t.inserted_date, '%d-%m-%Y') AS date,
								COUNT(t.ticket_id) AS total_ticket,
								SUM(IF(t.transaction_status = 'cancel', '1', '0')) AS cancel_ticket, 
								SUM(IF(t.transaction_status = 'success', '1', '0')) AS success_ticket, 
								SUM(IF(t.transaction_status = 'success', t.num_passgr, '0'))  AS total_number_of_passanger, 
								SUM(IF(t.transaction_status = 'success', t.tot_fare_amt, '0')) AS actual_fare_amount, 
								SUM(IF(t.transaction_status = 'success', t.discount_value, '0')) AS discount_amount, 
								FORMAT(SUM(IF(t.transaction_status = 'success', 
									IF(op_name='UPSRTC',
								           t.fare_convey_charge,
								           (t.total_fare_without_discount * t.commision_percentage/100)),
								          '0')
								       ),2
								   ) AS commission_value,
								SUM(t.tot_fare_amt - t.discount_value) AS total_revenue",false);
			$this->db->from("tickets t");
			if($filters["filter"] == "last_thirty_days")
			{
				$this->db->group_by("DATE_FORMAT(t.inserted_date, '%Y-%m-%d')");
				$this->db->where("DATE_FORMAT(t.inserted_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($filters["last_thirtyth_date"])));
				$this->db->where("DATE_FORMAT(t.inserted_date, '%Y-%m-%d') <=", date('Y-m-d', strtotime($filters["current_date"])));
			}

			if(isset($filters["agent_user_id"]) && $filters["agent_user_id"] != "")
			{
				$this->db->where_in("t.inserted_by",$filters["agent_user_id"]);
			}
			$query = $this->db->get();
			return $query->result_array();
		}
    }


    /* @Author 		: Rajkumar
	 * @function    : monthwise_user_reg_report
	 * @description :
	 * @param       : 
	 */

    public function monthwise_user_reg_report()
    { 
    	
		$this->db->select("COUNT(u.id) AS total_user,DATE_FORMAT(u.created_date,'%b-%Y') AS registered_date",false);
		$this->db->from("users u");
		$this->db->where("u.role_id", USER_ROLE_ID);
		$this->db->where("u.verify_status", "Y");
		$this->db->group_by("DATE_FORMAT(u.created_date,'%m-%Y')");
		$this->db->order_by("u.created_date","ASC");
        $query = $this->db->get();
		return $query->result_array();
	 } 
    
}
