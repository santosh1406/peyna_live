<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class  Incentive_slab_details_model extends MY_Model
{
    var $table    = 'incentive_slab_details';
    var $fields	  = array("id", "incentive_id", "from_slab", "to_slab", "incentive_type_id", "incentive_type_applicable_id", "incentive_type","incentive_value","inserted_date");
    var $key      = 'id';
    
    public function __construct() {
        parent::__construct();
        $this->_init();      
        
    }
    
    
}

