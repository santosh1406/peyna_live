<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Ticket_details_model extends MY_Model
{
    /**
     * Instanciar o CI
     */
    var $table = 'ticket_details';
    var $fields = array("id","ticket_id","seat_no","berth_no","psgr_name","psgr_age","psgr_sex","psgr_type","psgr_no","concession_cd","concession_rate","discount","mahila_concession","is_home_state_only","quota_flag","adult_basic_fare","child_basic_fare","fare_acc","fare_hr","fare_it","fare_octroi","fare_other","fare_reservationCharge","fare_sleeperCharge","fare_toll","fare_convey_charge","ac_service_tax","asn_fare","service_tax_per","service_tax","op_service_charge_per","op_service_charge","commission","home_state_amt","state_amt_1","state_amt_2","state_amt_3","state_amt_4","state_amt_5","convey_name","convey_type","convey_value","additional_Amt","fare_amt","basic_with_ac","fare_amt_with_discount","refund_amt","cancel_charge","seat_status","concession_proff","proff_detail","identify_proof","identify_nm","identify_no","commission_id","commission_detail_id","type","commission_percent","op_commission","tds_on_commission","final_commission","status");
    var $key    = 'id';  

    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
    function get_passengers_details($ticket_id)
    {
        $this->db->select('td.psgr_no, td.psgr_name, td.psgr_age, td.psgr_sex, td.psgr_type, td.seat_no,td.fare_amt,c.actual_refund_paid,c.created_at');
        $this->db->from('ticket_details td');
        $this->db->join('cancel_ticket_data c','td.id = c.ticket_id','left');
        $this->db->where('td.ticket_id',$ticket_id);
        $query=$this->db->get();
//        show($this->db->last_query(),1);
        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        return false;
    }
  
        
}
