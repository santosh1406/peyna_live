<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Op_api_config_model extends MY_Model {

    var $table  = 'op_api_config';
    var $fields = array("op_api_config_id","op_api_id","op_id","service_id","commission_type","bus_type_id","commission_value","from_date","to_date","is_status","is_deleted","inserted_by","updated_by","inserted_date","updated_date");
    
    var $key    = 'op_api_config_id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
     function get_api_config_all_data($data) 
    {
        $this->db->select('op_api_config_id,op_api_id,op_id,service_id,commission_type,op.bus_type_id,bt.bus_type_name,commission_value,from_date,to_date');
        $this->db->from('op_api_config op');
        $this->db->join('bus_type bt', 'bt.id=op.bus_type_id', "inner");
        $this->db->where('op_api_config_id', $data['api_id']);
        $this->db->where('is_deleted', '0');
        $result_data = $this->db->get();
      
        return $result_data->result_array();
    }
     
}
?>