<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Processed_ticket_tax_trans_model extends MY_Model {

    var $table  = 'processed_ticket_tax_trans';
    var $fields = array("id","processed_ticket_id","convey_id","convey_name", "convey_type","convey_value", "actual_value","transaction_value", "timestamp");
    var $key    = 'id'; 

    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

}
