<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jri_reverse_push_data_model extends MY_Model {

    var $table = 'jri_reverse_push_data';
    var $fields = array("TransactionReference", "MobileNo", "Provider", "Amount", "ServiceType", "IsPostpaid", "SystemReference", "Status", "created_at");
    var $key = "id";

    public function __construct() {
        parent::__construct();
    }

    function get_all_data() {
        $this->db->select('*');
        $this->db->from('jri_reverse_push_data');
        $this->db->where('reversal_flag', '0');
        $data = $this->db->get();
        return $data->result_array();
    }

    

}

// End of Agent_model class
