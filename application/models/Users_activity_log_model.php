<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_activity_log_model extends MY_Model {
    
    var $table  = 'user_activity_log';    
	var $fields = array("id", "user_id", "name", "rmn", "rollname", "url", "db_function", "db_table_name",
	"function_name", "input_data", "message", "ip_address", "user_agent", "datetime");
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
}
