<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mail_template_model
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mail_template_model extends MY_Model {

    var $table = 'mail_template';
    var $fields = array("id", "title", "subject", "body", "added_by", "created_date", "updated_by", "updated_date");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    public function get_mail_format() {
        $this->db->select('id,title');
        $this->db->order_by("title", "ASC");
        $data = $this->db->get('mail_template');
        return $data->result_array();
    }

}

// End of Mail_template_model class