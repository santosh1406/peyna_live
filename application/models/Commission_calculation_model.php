<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Commission_calculation_model extends MY_Model {

    protected $table = 'commission_calculation';
    var $fields = array("id", "ticket_id", "ticket_detail_id", "user_id", "commission_per", "commission", "tds_on_commission", "final_commission", "added_on", "booking_flag", "cancel_flag", "cancel_flag_date");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
    }

    public function retailer_commission_calculation($ticket_id,$user_id) {
    	if($user_id != '') {
    		$this->db->select('sum(final_commission) as total_commission');
    		$this->db->from("commission_calculation");
    		$this->db->where("ticket_id",$ticket_id);
    		$this->db->where("user_id",$user_id);
    		$this->db->where("booking_flag","N");
    		$query = $this->db->get();
    		return $query->result();		
    	}
    	else {
    		return false;
    	}
    }

    public function retailer_commission_calculation_deduction($ticket_id,$user_id) {
        if($user_id != '') {
            $this->db->select('sum(final_commission) as total_commission');
            $this->db->from("commission_calculation");
            $this->db->where("ticket_id",$ticket_id);
            $this->db->where("user_id",$user_id);
            $this->db->where("cancel_flag","N");
            $query = $this->db->get();
            return $query->result();        
        }
        else {
            return false;
        }
    }

}
