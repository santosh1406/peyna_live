<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Benificiary_master_model extends MY_Model {
    
    var $table  = 'benificiary_master';
    var $fields = array("id", "code","name", "address_1", "address_2", "address_3", "location_code", "state_code", "pin_code", "email", "mobile", "over_all_limit", "payment_metod_name", "account_number", "ifsc_code", "bank_name", "branch_name", "account_type", "status", "escrow_registred_flag", "created_by", "created_at", "updated_by", "updated_at");

  
    var $key    = 'id';
    protected $set_created =false;
    protected $set_modified =false;

    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
}

// End of Agent_commission_model class