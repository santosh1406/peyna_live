<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class UP_Model extends CI_Model {

    var $local_up_db ;
    var $live_up_db ;

    function __construct() {
        parent::__construct();
       $this->local_up_db = $this->load->database('local_up_db', TRUE);
       $this->live_up_db = $this->load->database('live_up_db', TRUE);
       $this->up_db_revised = $this->load->database('upsrtc_current_booking', TRUE);
    }

}
