<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once('Upsrtc_current_booking_model.php');

class Up_bus_service_model extends Upsrtc_model {

    function __construct() {
        parent::__construct();
    }

    function get_trip_no() {
        $this->db2->distinct();
        $this->db2->select("TRIP_NO");
        $this->db2->from("bus_services");
        $query = $this->db2->get();
        return $query->result_array();
    }

    function get_trip_no_by_route($route_no) {
        /*
         * select SCHEDULED_TM from bus_services WHERE 
          SCHEDULED_TM BETWEEN date_add(NOW(),INTERVAL -30 MINUTE)
          AND date_add(NOW(),INTERVAL +30 MINUTE);
         * 
         * 
          mysql> SELECT something FROM tbl_name
          -> WHERE DATE_SUB(CURDATE(),INTERVAL 30 DAY) <= date_col;
         * 
         * 
         *          */

        /* get parameter from system_config table */
        $parameter_value = $this->system_config_model->system_config_values();
        $date = date('Y-m-d H:i:s');
        $this->db2->select("bs.BUS_SERVICE_NO,bs.ROUTE_NO,bs.TRIP_NO,DATE_FORMAT(bs.SCHEDULED_TM,'%Y-%m-%d %H:%i:%s') as SCHEDULED_TM,DATE_FORMAT(SCHEDULED_TM,'%H:%i:%s') TIMEONLY,bs.BUS_TYPE_CD,bt.BUS_TYPE_ID,vr.route_nm");
        $this->db2->from("bus_services bs");
        $this->db2->join("bus_types bt", "bt.BUS_TYPE_CD = bs.BUS_TYPE_CD", "inner");
        $this->db2->join("v_routes vr", "vr.route_no = bs.ROUTE_NO", "inner");
        $this->db2->where("bs.ROUTE_NO", $route_no);
        // $this->db2->where("SCHEDULED_TM BETWEEN DATE_ADD( '". $date."' , INTERVAL - ".$parameter_value." MINUTE) AND DATE_ADD(  '". $date."' , INTERVAL + ".$parameter_value." MINUTE)");
        $this->db2->where("time(SCHEDULED_TM) BETWEEN '00:00:00' and '23:59:59'");
        $query = $this->db2->get();
        $res = $query->result_array();
       // show($this->db2->last_query(), 1);
        
        return $res;
    }

}
