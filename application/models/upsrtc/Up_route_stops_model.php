<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once('Upsrtc_current_booking_model.php');

class Up_route_stops_model extends Upsrtc_model {

    function __construct() {
        parent::__construct();
    }

    function get_route_stops() {
        $this->db2->distinct();
        $this->db2->select("ROUTE_NO,BUS_STOP_NM");
        $this->db2->from("route_stops");
        $query = $this->db2->get();
        return $query->result_array();
    }

    function get_route_stops_by_boarding($data) {
        if ($data['boarding_stop'] != '') {
            $boarding_stop = trim($data['boarding_stop']);
        } else {
             $boarding_stop = $this->session->userdata('boarding_stop_id');
        }

        $destination_stop = $data['sel_destination'];
        //exit;
        //echo $boarding_stop_id = $this->session->userdata('boarding_stop_id');

        //exit;
        if ($destination_stop != '') {
            $new_values .= $boarding_stop . ',' . $destination_stop;
        }
        $str = explode(',', $new_values);

        if ($destination_stop != '') {
            $values = $str;
        } else {
            $values = $boarding_stop;
        }

        $boarding_stop = trim($boarding_stop);
        $this->db2->select("rs.ROUTE_NO,rs.BUS_STOP_NM,rs.BUS_STOP_CD,r1.FROM_STOP_CD,r2.TILL_STOP_CD,bs1.BUS_STOP_NM as frm_stop,bs2.BUS_STOP_NM as to_stop");
        $this->db2->distinct("rs.ROUTE_NO");
        $this->db2->from("route_stops rs");
        $this->db2->join("routes r1", "r1.ROUTE_NO = rs.ROUTE_NO", "inner");
        $this->db2->join("routes r2", "r2.ROUTE_NO = rs.ROUTE_NO", "inner");
        $this->db2->join("bus_stops bs1", "bs1.BUS_STOP_CD = r1.FROM_STOP_CD", "inner");
        $this->db2->join("bus_stops bs2", "bs2.BUS_STOP_CD = r2.TILL_STOP_CD", "inner");

        //$this->db2->where("rs.BUS_STOP_CD", $boarding_stop);
        //$this->db2->where("r2.TILL_STOP_CD<>", $boarding_stop);

        $this->db2->escape($values);
        $this->db2->where_in("rs.BUS_STOP_CD", $values);
        $this->db2->where_not_in("r2.TILL_STOP_CD", $values);


        $this->db2->group_by("rs.ROUTE_NO");
        $this->db2->order_by("rs.ROUTE_NO", "ASC");
        $query = $this->db2->get();
        $res = $query->result_array();
        // show($this->db2->last_query(), 1);
        $output = null;
        $output .= "<option value='' selected></option>";
        foreach ($res as $row) {
            //here we build a dropdown item line for each query result
            $output .= "<option value='" . $row['ROUTE_NO'] . "'>" . $row['frm_stop'] . " To " . $row['to_stop'] . " - " . $row['ROUTE_NO'] . "</option>";
        }

        return $output;
    }

}
