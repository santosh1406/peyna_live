<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once('Upsrtc_current_booking_model.php');

class Up_bus_stops_model extends Upsrtc_model {

    function __construct() {
        parent::__construct();
    }

    function get_bus_stops() {
        $this->db2->distinct();
        $this->db2->select("BUS_STOP_CD,BUS_STOP_NM");
        $this->db2->from("bus_stops");
        $this->db2->order_by("BUS_STOP_NM", "asc");
        $query = $this->db2->get();
        //   show($this->db->last_query(),1);
        return $query->result_array();
    }

    function get_auto_bus_stops($q = '') {
        $input = $this->input->get();
        //$this->db2->select("BUS_STOP_CD,BUS_STOP_NM");
        $this->db2->select("BUS_STOP_NM as label,CONCAT(BUS_STOP_NM, ' - ', BUS_STOP_CD) as value", false);
        $this->db2->distinct("BUS_STOP_NM");
        $this->db2->from("bus_stops");
        $this->db2->like('BUS_STOP_NM', $input['q'], 'after');
        $this->db2->group_by('BUS_STOP_NM');
        $this->db2->order_by("BUS_STOP_NM", "asc");
        $query = $this->db2->get();
        // show($this->db2->last_query(), 1);
        $data = $query->result_array();
        return $data;
    }

    function get_boarding_stop_id($boarding_stop_id) {
        $this->db2->select("BUS_STOP_ID");
        $this->db2->from("bus_stops");
        $this->db2->where("BUS_STOP_CD", $boarding_stop_id);
        $query = $this->db2->get();
        $row = $query->row();
        $res = $row->BUS_STOP_ID;
        return $res;
    }

    function get_auto_bus_stops_chosen($q = '') {
        //echo "herr";
        //exit;
        $input = $this->input->get();
        //$this->db2->select("BUS_STOP_CD,BUS_STOP_NM");
        $this->db2->select("BUS_STOP_CD as label,CONCAT(BUS_STOP_NM, ' - ', BUS_STOP_CD) as value", false);
       // $this->db2->select("BUS_STOP_NM as label,BUS_STOP_NM as value", false);
        $this->db2->distinct("BUS_STOP_NM");
        $this->db2->from("bus_stops");
        $this->db2->like('BUS_STOP_NM', $input['q'], 'after');
        $this->db2->group_by('BUS_STOP_NM');
        $this->db2->order_by("BUS_STOP_NM", "asc");
        $query = $this->db2->get();
        $res = $query->result_array();
        //print_r($res);
        $output = null;
        $output .= "<option value='' selected></option>";
        foreach ($res as $row) {
            //here we build a dropdown item line for each query result
            $output .= "<option value='" . $row['label'] . "'>" . $row['value'] . "</option>";
        }

        return $output;
    }

}
