<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once('Upsrtc_current_booking_model.php');

class Up_bus_service_stops_model extends Upsrtc_model {

    function __construct() {
        parent::__construct();
    }

    function get_bus_service_stops() {
        $this->db2->distinct();
        $this->db2->select("bss.BUS_STOP_CD,bs.BUS_STOP_NM");
        $this->db2->from("bus_service_stops bss");
        $this->db2->join("bus_stops bs", "bs.BUS_STOP_CD = bss.BUS_STOP_CD", "inner");
        //$this->db2->where("IS_BOARDING_STOP",'Y');
        $query = $this->db2->get();
        return $query->result_array();
       
    }

    
}
