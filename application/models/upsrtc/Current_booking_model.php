<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    require_once('UP_Model.php');
class Current_booking_Model extends UP_Model {

    var $live_up_db;

    function __construct() {
        parent::__construct();
        $this->live_up_db = $this->load->database('live_up_db', TRUE);
    }

  /*   function ge_bus_service_data($data)
    {   
             
        $this->live_up_db->select("distinct tmp.BUS_SERVICE_NO,bs.DEPOT_CD, tmp.ROUTE_NM,bs.ROUTE_NO, bs.BUS_TYPE_CD, bs.ARRIVAL_DAY, bs.BUS_FORMAT_CD, TIME(bd.DEPARTURE_TM) DEPARTURE_TM", false);
        $this->live_up_db->from('bus_departures bd');
        $this->live_up_db->join('temp_select_bus_services tmp', 'tmp.BUS_SERVICE_NO = bd.BUS_SERVICE_NO',"inner");
        $this->live_up_db->join('bus_services bs', 'bs.BUS_SERVICE_NO = bd.BUS_SERVICE_NO',"inner");
        if(isset($data) && $data['from_date']=='' ){
            $this->live_up_db->where("DEPT_TM > NOW()");
        } 
        if(isset($data) && $data['from_date']!=''){
            $this->live_up_db->where("bd.DEPARTURE_TM >=  concat(STR_TO_DATE('".$data['from_date']."' , '%d/%m/%Y'), ' 00:00:00')");
            $this->live_up_db->where("bd.DEPARTURE_TM <=  concat(STR_TO_DATE('".$data['from_date']."' , '%d/%m/%Y'), ' 23:59:59')" );
            
            $this->live_up_db->where("STR_TO_DATE(concat('".$data['from_date'] ." 00:00: 00' ,'  ', TIME(bd.DEPARTURE_TM)),' %d/%m/%Y %H:%i:%s') >  now() +  INTERVAL (select PARA_VALUE FROM system_conf where PARA_NM = 'ORS_BUS_LOCK_TM') MINUTE  ");
        
        }
      // $this->live_up_db->limit(140);
        $query = $this->live_up_db->get();
        $data_ins= $query->result_array(); 
      
        if($query->num_rows())
        {
            $result  = $query->result();
            if($result)
            {
                foreach ($result as $key => $value) {
                $data = array(
                            'BUS_SERVICE_NO'    => $value->BUS_SERVICE_NO ,
                            'DEPOT_CD'          =>  $value->DEPOT_CD ,
                            'ROUTE_NM'          =>  $value->ROUTE_NM ,
                            'ROUTE_NO'          =>  $value->ROUTE_NO ,
                            'BUS_TYPE_CD'       =>  $value->BUS_TYPE_CD,
                            'BUS_FORMAT_CD'     =>  $value->BUS_FORMAT_CD,
                            'DEPARTURE_TM'      =>  $value->DEPARTURE_TM,
                            'ARRIVAL_DAY'       =>  $value->ARRIVAL_DAY
                           
                            
                 );
                    
                $this->local_up_db->select('BUS_SERVICE_NO');
                $this->local_up_db->from('current_bus_service_data');
                $this->local_up_db->where("BUS_SERVICE_NO", $value->BUS_SERVICE_NO);
                $query = $this->local_up_db->get();
                $data_ins= $query->result_array();
                if($query->num_rows())
                {
                   
                        $this->local_up_db->where('BUS_SERVICE_NO', $value->BUS_SERVICE_NO);
                        $this->local_up_db->update('current_bus_service_data', $data);
                        $return_val[$value->BUS_SERVICE_NO]='This Bus Service no is Updated';

                    }else{
                    $this->local_up_db->insert('current_bus_service_data', $data); 
                        $return_val[$value->BUS_SERVICE_NO]='This Bus Service no is inserted';
                    }
               
         
                log_data("cron/bus_service_".date("d-m-Y h:m:s")."_log.log",$return_val,'Bus service data');
                show($return_val,1);
                }
        }

        }

    }
    */
    
     function cb_bus_data($data)
    {
        
        $this->live_up_db->select("BUS_SERVICE_NO,DATE_FORMAT(DEPT_TM,'%Y-%m-%d %H:%i:%s') as DEPT_TM,BOOKING_STATUS,VEHICLE_NO,CONDUCTOR_NM,MOBILE_NO", false);
        $this->live_up_db->from('current_booking_buses');
        $this->live_up_db->where("BOOKING_STATUS","c");
        if(isset($data) && $data['from_date']==''){
            $this->live_up_db->where("DEPT_TM > NOW()");
        } 
       
        $query = $this->live_up_db->get();
        $data_ins= $query->result_array();
        if($query->num_rows())
        {
            $result  = $query->result();
            $data = false;
            if($result)
            {
                foreach ($result as $key => $value) {
                   
                  $data = array(
                            'BUS_SERVICE_NO' => $value->BUS_SERVICE_NO ,
                            'DEPT_TM' =>  $value->DEPT_TM ,
                            'BOOKING_STATUS' =>  $value->BOOKING_STATUS,
                            'VEHICLE_NO' =>  $value->VEHICLE_NO,
                            'CONDUCTOR_NM' =>  $value->CONDUCTOR_NM,
                            'MOBILE_NO' =>  $value->MOBILE_NO
                 
                            
                 );
                    $this->local_up_db->select('*');
                    $this->local_up_db->from('current_booking_upsrtc_bus');
                    $this->local_up_db->where("BUS_SERVICE_NO", $value->BUS_SERVICE_NO);
                    $query = $this->local_up_db->get();

                   $data_ins= $query->result_array();
                   
                   if(isset($data_ins) && count($data_ins) > 0){
                       $this->local_up_db->where('BUS_SERVICE_NO', $value->BUS_SERVICE_NO);
                       $this->local_up_db->update('current_booking_upsrtc_bus', $data);
                       $return_val[$value->BUS_SERVICE_NO]='This current booking bus no is Updated';
                   }else{
                       $this->local_up_db->insert('current_booking_upsrtc_bus', $data);
                       $return_val[$value->BUS_SERVICE_NO]='This current booking bus no is Inserted';
                   }
                }    log_data("cron/current_booking_bus_".date("d-m-Y h:m:s")."_log.log",$return_val,'Current Booking Bus data');
                     show($return_val,1);
            }

        }



    }
    
    
      function cb_busstops_data($service_id)
    {
        
        $this->live_up_db->select("BUS_SERVICE_NO,BUS_STOP_CD,ARRIVAL_TM,IS_BOARDING_STOP,DEPARTURE_TM,ARRIVAL_DAY,STOP_SEQ", false);
        $this->live_up_db->from('bus_service_stops');
       
       
        if(isset($service_id) && $service_id!=''){
            $this->live_up_db->where("BUS_SERVICE_NO",$service_id);
        } 
        //$this->live_up_db->limit(3,2);
        $query = $this->live_up_db->get();
        $data_ins= $query->result_array();
        if($query->num_rows())
        {
            $services  = $query->result();
            $data = false;
            if($services)
            {
                for($i=0;$i< count($services);$i++){
                   
                  $data = array(
                            'BUS_SERVICE_NO'    =>  $services[$i]->BUS_SERVICE_NO ,
                            'BUS_STOP_CD'       =>  $services[$i]->BUS_STOP_CD ,
                            'ARRIVAL_TM'        =>  $services[$i]->ARRIVAL_TM,
                            'IS_BOARDING_STOP'  =>  $services[$i]->IS_BOARDING_STOP,
                            'DEPARTURE_TM'      =>  $services[$i]->DEPARTURE_TM,
                            'ARRIVAL_DAY'       =>  $services[$i]->ARRIVAL_DAY,
                           'STOP_SEQ'           =>  $services[$i]->STOP_SEQ
                            
                 );
                    $this->local_up_db->select('*');
                    $this->local_up_db->from('current_bus_service_stops');
                    $this->local_up_db->where("BUS_SERVICE_NO", $services[$i]->BUS_SERVICE_NO);
                    $this->local_up_db->where("BUS_STOP_CD", $services[$i]->BUS_STOP_CD);
                    $query = $this->local_up_db->get();

                   $data_ins= $query->result_array();
                
                   if(isset($data_ins) && count($data_ins) > 0){
                        $this->local_up_db->where('BUS_SERVICE_NO', $services[$i]->BUS_SERVICE_NO);
                        $this->local_up_db->where("BUS_STOP_CD", $services[$i]->BUS_STOP_CD);
                        $this->local_up_db->where("ARRIVAL_TM", $services[$i]->ARRIVAL_TM);	
                        $this->local_up_db->where("IS_BOARDING_STOP", $services[$i]->IS_BOARDING_STOP);	
                        $this->local_up_db->where("DEPARTURE_TM", $services[$i]->DEPARTURE_TM);	
                        $this->local_up_db->where("ARRIVAL_DAY", $services[$i]->ARRIVAL_DAY);	
                        $this->local_up_db->where("STOP_SEQ", $services[$i]->STOP_SEQ);
                       
                       $this->local_up_db->update('current_bus_service_stops', $data);
                       $return_val[$services[$i]->BUS_SERVICE_NO]='This current booking bus stop no is Updated';
                   }else{
                       $this->local_up_db->insert('current_bus_service_stops', $data);
                       $return_val[$services[$i]->BUS_SERVICE_NO]='This current booking bus stop is Inserted';
                   }
                }    log_data("cron/current_booking_busstops_".date("d-m-Y h:m:s")."_log.log",$return_val,'Current Booking Bus Stops data');
                     show($return_val,1);
            }

        }



    }
    
    function ge_bus_service_data_pagination($data)
    {   
        $per_page=API_PAGINATION_LIMIT;
        $k=0;
        $j=1;
        $return_val=array();
        while(1){  
          
            $this->live_up_db->select("distinct tmp.BUS_SERVICE_NO,bs.DEPOT_CD, tmp.ROUTE_NM,bs.ROUTE_NO, bs.BUS_TYPE_CD, bs.ARRIVAL_DAY, bs.BUS_FORMAT_CD, TIME(bd.DEPARTURE_TM) DEPARTURE_TM", false);
            $this->live_up_db->from('bus_departures bd');
            $this->live_up_db->join('temp_select_bus_services tmp', 'tmp.BUS_SERVICE_NO = bd.BUS_SERVICE_NO and bd.BOOKING_STATUS != "L" ',"inner");
            $this->live_up_db->join('bus_services bs', 'bs.BUS_SERVICE_NO = bd.BUS_SERVICE_NO AND bs.BUS_SERVICE_NO = tmp.BUS_SERVICE_NO',"inner");
            
            if(isset($data) && $data['from_date']!=''){
                $this->live_up_db->where("bd.DEPARTURE_TM >=  concat(STR_TO_DATE('".$data['from_date']."' , '%d/%m/%Y'), ' 00:00:00')");
                $this->live_up_db->where("bd.DEPARTURE_TM <=  concat(STR_TO_DATE('".$data['from_date']."' , '%d/%m/%Y'), ' 23:59:59')" );

                $this->live_up_db->where("STR_TO_DATE(concat('".$data['from_date']."' ,'  ', TIME(bd.DEPARTURE_TM)),' %d/%m/%Y %H:%i:%s') >  now() +  INTERVAL (select PARA_VALUE FROM system_conf where PARA_NM = 'ORS_BUS_LOCK_TM') MINUTE  ");

            }
            $this->live_up_db->limit($per_page,$k);
            
            $query = $this->live_up_db->get(); 
       
            $data_ins= $query->result_array(); 
            $result  = $query->result();
            if($query->num_rows())
            { 
                foreach ($result as $key => $value) {
                    $data = array(
                    'BUS_SERVICE_NO'    => $value->BUS_SERVICE_NO ,
                    'DEPOT_CD'          =>  $value->DEPOT_CD ,
                    'ROUTE_NM'          =>  $value->ROUTE_NM ,
                    'ROUTE_NO'          =>  $value->ROUTE_NO ,
                    'BUS_TYPE_CD'       =>  $value->BUS_TYPE_CD,
                    'BUS_FORMAT_CD'     =>  $value->BUS_FORMAT_CD,
                    'DEPARTURE_TM'      =>  $value->DEPARTURE_TM,
                    'ARRIVAL_DAY'       =>  $value->ARRIVAL_DAY );
                    
                $this->local_up_db->select('BUS_SERVICE_NO');
                $this->local_up_db->from('current_bus_service_data');
                $this->local_up_db->where("BUS_SERVICE_NO", $value->BUS_SERVICE_NO);
                $query = $this->local_up_db->get();
                $data_ins= $query->result_array();
                if($query->num_rows())
                {
                   if(isset($data_ins) && count($data_ins) > 0){
                        $this->local_up_db->where('BUS_SERVICE_NO', $value->BUS_SERVICE_NO);
                        $this->local_up_db->update('current_bus_service_data', $data);
                        $return_val[$value->BUS_SERVICE_NO]='This Bus Service no is Updated';

                    }
                 }else{
                    $this->local_up_db->insert('current_bus_service_data', $data); 
                        $return_val[$value->BUS_SERVICE_NO]='This Bus Service no is inserted';
                    }
         
               
                }
        

        }
        else{
               break;
        }
            $k=$per_page+$k;
        } 
            log_data("cron/bus_service_".date("d-m-Y h:m:s")."_log.log",$return_val,'Bus service data');
            show($return_val,1);

    }
    
    
}


?>
