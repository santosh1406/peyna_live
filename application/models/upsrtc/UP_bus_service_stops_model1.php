<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once('Upsrtc_current_booking_model.php');

class Up_bus_service_stops_model extends Upsrtc_model {

    function __construct() {
        parent::__construct();
    }

    function get_bus_service_stops() {
        $this->db2->distinct();
        $this->db2->select("BUS_STOP_CD");
        $this->db2->from("bus_service_stops");
        $query = $this->db2->get();
         //show($this->db->last_query(),1);
        return $query->result_array();
    }

}
