<?php
require_once ('UP_Model.php');
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Upsrtc_current_booking_model extends UP_Model {


    function __construct() {
        parent::__construct();

    }
    
    
    /*
     * Function to get the list of all stops
     */
    
    function get_bus_stops($q = '') {
        $input = $this->input->get();
        //$this->local_up_db->select("BUS_STOP_CD,BUS_STOP_NM");
        $this->local_up_db->select("BUS_STOP_NM as label,CONCAT(BUS_STOP_NM, ' - ', BUS_STOP_CD) as value", false);
        $this->local_up_db->distinct("BUS_STOP_NM");
        $this->local_up_db ->from("bus_stops");
        $this->local_up_db ->group_by('BUS_STOP_NM');
        $this->local_up_db ->order_by("BUS_STOP_NM", "asc");
        $query = $this->local_up_db->get();
        return $query->result_array();
    }

    /* function written by pankaj
     * parameter boarding stop,alighting stop
     * return all buses
     * also it display grouping stops
     */

    function select_bus($data) 
        {
            
       
           $from_stop_nm=$data['boarding_stop_nm'];
           $till_stop_nm=$data['alight_stop_nm'];
           
           $from_stop_cd=$data['boarding_stop'];
           $till_stop_cd=$data['alight_stop'];
           
           
           $this->local_up_db->select('suggested_nm');
           $this->local_up_db->from('bus_stop_groups');
           $this->local_up_db->like('suggested_nm',$from_stop_nm,'after');
           $que1= $this->local_up_db->get();
           $from_stop_cnt=$que1->num_rows();
           
          
             $this->local_up_db->select('suggested_nm');
             $this->local_up_db->from('bus_stop_groups');
             $this->local_up_db->like('suggested_nm',$till_stop_nm,'after');
             $que2= $this->local_up_db->get();
             $till_stop_cnt=$que2->num_rows();
             
            $this->local_up_db->select('tmp.BUS_SERVICE_NO,tmp.ROUTE_NM,bs.ROUTE_NO,cbs.VEHICLE_NO,cbs.CONDUCTOR_NM ,bs.BUS_TYPE_CD,date_format(cbs.DEPT_TM,"%d/%m/%y %H:%i") as DEPT_TM,DATE_FORMAT(cbs.DEPT_TM,"%d/%m/%y") as BUS_DEPT_DT,tmp.FROM_STOP_CD,tmp.TILL_STOP_CD');

            $this->local_up_db->from('current_booking_buses cbs');
            $this->local_up_db->join('temp_select_bus_services tmp', 'cbs.BUS_SERVICE_NO = tmp.BUS_SERVICE_NO', 'inner');
            $this->local_up_db->join('bus_services bs', 'bs.BUS_SERVICE_NO = cbs.BUS_SERVICE_NO and bs.BUS_SERVICE_NO = tmp.BUS_SERVICE_NO', 'inner');
          
            $this->local_up_db->join('bus_service_stops bss', 'bss.BUS_STOP_CD = tmp.from_stop_cd and bss.BUS_SERVICE_NO = tmp.bus_service_no', 'inner');
            
            //grouping concept starts here
             if($from_stop_cnt > 0 && $till_stop_cnt <=0)
                {
                    $this->local_up_db->join('bus_stop_groups bsg','tmp.from_stop_cd=bsg.bus_stop_cd','left');
                    $this->local_up_db->join('bus_stop_groups bsg1','tmp.till_stop_cd=bsg1.bus_stop_cd','left');
                    $this->local_up_db->like('bsg.suggested_nm',$from_stop_nm,'after');
                    $this->local_up_db->where('bsg.status','Y');
                    $this->local_up_db->where('tmp.till_stop_cd',$till_stop_cd);
               
                }
                else if($from_stop_cnt <=0 && $till_stop_cnt > 0)
                {
                    $this->local_up_db->join('bus_stop_groups bsg','tmp.from_stop_cd=bsg.bus_stop_cd','left');
                    $this->local_up_db->join('bus_stop_groups bsg1','tmp.till_stop_cd=bsg1.bus_stop_cd','left');
                    $this->local_up_db->like('bsg1.suggested_nm',$till_stop_nm,'after');
                    $this->local_up_db->where('bsg.status','Y');
                    $this->local_up_db->where('tmp.from_stop',$from_stop_cd);
                
                }
             else if($from_stop_cnt >0 && $till_stop_cnt > 0)
                {
                    $this->local_up_db->join('bus_stop_groups bsg','tmp.from_stop_cd=bsg.bus_stop_cd','left');
                    $this->local_up_db->join('bus_stop_groups bsg1','tmp.till_stop_cd=bsg1.bus_stop_cd','left');
                    $this->local_up_db->like('bsg.suggested_nm',$from_stop_nm,'after');
                    $this->local_up_db->like('bsg1.suggested_nm',$till_stop_nm,'after');
                    $this->local_up_db->where('bsg.status','Y');
                }
             else
                {
                    $this->local_up_db->where('tmp.from_stop_cd',$from_stop_cd);
                    $this->local_up_db->where('tmp.till_stop_cd',$till_stop_cd);
                }
            
            
            
            
           //end grouping concept
            $this->local_up_db->join('bus_types as bt', 'bt.BUS_TYPE_CD = bs.BUS_TYPE_CD', 'inner');
            $this->local_up_db->where('cbs.BOOKING_STATUS', 'C');
            $this->local_up_db->where('DATE(cbs.DEPT_TM)', $data['journey_date']);
           
            $this->local_up_db->group_by('tmp.BUS_SERVICE_NO');
           
            // show($this->db->last_query(),1);
            $query=$this->local_up_db->get();
            
            return  $query->result_array();
    }
    
    //gets service route stops
    
    function get_service_stops($route_no)
    {
        $this->local_up_db->select('bs.BUS_STOP_CD, bs.BUS_STOP_NM');
        $this->local_up_db->from('service_route_stops sr');
        $this->local_up_db->join('bus_stops bs','sr.BUS_STOP_CD=bs.BUS_STOP_CD');
        $this->local_up_db->where('sr.route_no',$route_no);
        $query = $this->local_up_db->get();
        if($query->num_rows()>0)
        {
            return $query->result_array();
    }
        return false;
    }
    
    function getBusServiceDetails($bus_sevice_no) {
        $this->local_up_db->select('BUS_TYPE_CD, date_format(SCHEDULED_TM,"%d/%m/%Y %H:%i") as DEPT_TM');
        $this->local_up_db->from('bus_services');
        $this->local_up_db->where('BUS_SERVICE_NO',$bus_sevice_no);
        $query = $this->local_up_db->get();
        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        return false;
    }
    
    /*
     * Get the list of concessions
     */
    
    function getConcessionList() {
        $query = $this->local_up_db->select('CONCESSION_CD, CONCESSION_NM')->get('concessions');
        if($query->num_rows()>0) {
            return $query->result_array();
        }
        return false;
        
    }
    
    /*
     * Print Ticket Functionalilty
     */
    
    function printTicket($ticket_no)
    {
        $this->db->select("DATE_FORMAT(ct.dept_time,'%d-%m-%Y %H:%i:%s') as dept_date, DATE_FORMAT(ct.boarding_time,'%d-%m-%Y %H:%i:%s') as boarding_time, DATE_FORMAT(ct.alighting_time,'%d-%m-%Y %H:%i:%s') as alighting_time, DATE_FORMAT(ct.inserted_date,'%d/%m/%Y') as ticket_date, ct.from_stop_name, till_stop_name, osm.op_stop_name AS boarding_stop, osm1.op_stop_name AS alighting_stop, ct.num_passgr, ct.tot_fare_amt");
        $this->db->from("current_tickets ct");
        $this->db->join("op_stop_master osm","osm.op_stop_cd = ct.boarding_stop_cd");
        $this->db->join("op_stop_master osm1","osm1.op_stop_cd = ct.destination_stop_cd");
        $this->db->where("ct.ticket_id",$ticket_no);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $data['ticket_details'] =  $query->row_array();
            
            $this->db->select("psgr_sex, psgr_type");
            $this->db->from("current_ticket_details");
            $this->db->where("ticket_no",$ticket_no);
            $query = $this->db->get();
            
            if($query->num_rows()>0)
            {
                $data['pass_details'] =  $query->result_array();
                $male   = 0;
                $female = 0;
                $child = 0;
                $adult = 0;
                foreach($data['pass_details'] as $key=>$val)
                {
                    if($val["psgr_sex"] == "M")
                    {
                        $male = $male+1;
                    }
                    else
                    {
                        $female = $female+1;
                    }
                    
                    if($val["psgr_type"]=="C")
                    {
                        $child = $child+1;
                    }
                    else
                    {
                        $adult = $adult+1;
                    }
                    
                }
                
                $data["passenger_details"] = array(
                                             "Male"=>$male,
                                             "Female"=>$female,
                                             "Child"=>$child,
                                             "Adult"=>$adult
                                             );
                                             return $data;
            }
        }
        return false;
    }
    
    
    /***********fetch data for current booking***************/
    
     function insert_upsrtc_bus_service($data)
    { 
        if (isset($data['BusServiceDataResponse']) && count($data['BusServiceDataResponse']) > 0 && isset($data['BusServiceDataResponse']['availableServices'])) {
            $services = $data['BusServiceDataResponse']['availableServices']['service'];

            if (!is_array($services)) {
                $services = array($services);
            }
          
            if(count($services) >0)
            {
                foreach ($services as $key => $value) {
                  $data = array(
                            'BUS_SERVICE_NO' => $value['serviceId'] ,
                            'DEPOT_CD'=>$value['depocode'] ,
                            'ROUTE_NM'=>$value['routeName'] ,
                            'ROUTE_NO' =>  $value['route_no'] ,
                            'BUS_TYPE_CD' =>  $value['busType'],
                            'ARRIVAL_DAY' =>  $value['arrivalDay'],
                            'BUS_FORMAT_CD' =>  $value['busFormatCd'],
                            'DEPARTURE_TM' =>  $value['departureTime']);
                    
                    
                $this->local_up_db->select('BUS_SERVICE_NO');
                $this->local_up_db->from('current_bus_service_data');
                $this->local_up_db->where("BUS_SERVICE_NO", $value['serviceId']);
                $query = $this->local_up_db->get();
    
               $data_ins= $query->result_array();
          
               if(isset($data_ins) && count($data_ins) > 0){
                   $this->local_up_db->where('BUS_SERVICE_NO', $value['serviceId']);
                   $this->local_up_db->update('current_bus_service_data', $data);
                     $return_val[$value['serviceId']]='This current bus service is Updated';
               }else{
                   $this->local_up_db->insert('current_bus_service_data', $data); 
                    $return_val[$value['serviceId']]='This current bus service is Inserted';
               }
                                
                }show($return_val,1);
            }
                    
        }else
        {
            echo 'Error : insert_bus_service';
            show($data);
        }
    }
     
    
    function insert_current_booking_bus($data)
    {
        if (isset($data['CurrentResponse']) && count($data['CurrentResponse']) > 0 && isset($data['CurrentResponse']['currentbusNames'])) {
            $services = $data['CurrentResponse']['currentbusNames'];

            if (!is_array($services)) {
                $services = array($services);
            }
           
            if(count($services) >0)
            {
                foreach ($services as $key => $value) {
                  $data = array(
                            'BUS_SERVICE_NO' => $value['bus_service_no'] ,
                            'DEPT_TM' => $value['dept_tm'] ,
                            'BOOKING_STATUS' =>  $value['booking_status'],
                            'VEHICLE_NO' =>  $value['vechicle_no'],
                            'CONDUCTOR_NM' =>  $value['conductor'],
                            'MOBILE_NO' =>  $value['mobile']
                            
                            
                 );
                    
                    
                $this->local_up_db->select('BUS_SERVICE_NO');
                $this->local_up_db->from('current_booking_upsrtc_bus');
                $this->local_up_db->where("BUS_SERVICE_NO", $value['bus_service_no']);
                $this->local_up_db->where("DEPT_TM", $value['dept_tm']);
                $query = $this->local_up_db->get();
    
               $data_ins= $query->result_array();
              
               if(isset($data_ins) && count($data_ins) > 0){
                   $this->local_up_db->where('BUS_SERVICE_NO', $value['bus_service_no']);
                   $this->local_up_db->where("DEPT_TM", $value['dept_tm']);
                   $this->local_up_db->update('current_booking_upsrtc_bus', $data);
                    $return_val[$value['bus_service_no']]='This current booking bus is Updated';
               }else{
                   $this->local_up_db->insert('current_booking_upsrtc_bus', $data); 
                    $return_val[$value['bus_service_no']]='This current booking bus is Inserted';
               }
                                
                }show($return_val,1);
            }
                    
        }else
        {
            echo 'Error : insert current booking bus';
            show($data);
        }
    }
    
     function insert_cb_bus_service_stops($data)
    { 
        if (isset($data['BusServiceStopsResponse']) && count($data['BusServiceStopsResponse']) > 0 && isset($data['BusServiceStopsResponse']['BusStops'])) {
            $services = $data['BusServiceStopsResponse']['BusStops']['busstop'];

            if (!is_array($services)) {
                $services = array($services);
            }
       
            if(count($services) >0)
            {
                for($i=0;$i< count($services);$i++){
           
                  
                  $data = array(
                            'BUS_SERVICE_NO'            => $services[$i]['BUS_SERVICE_NO'] ,
                            'BUS_STOP_CD'               => $services[$i]['BUS_STOP_CD'] ,
                            'ARRIVAL_TM'                =>  $services[$i]['ARRIVAL_TM'],
                            'IS_BOARDING_STOP'          =>  $services[$i]['IS_BOARDING_STOP'],
                            'DEPARTURE_TM'              =>  $services[$i]['DEPARTURE_TM'],
                            'ARRIVAL_DAY'               => $services[$i]['ARRIVAL_DAY'],
                            'STOP_SEQ'                  =>$services[$i]['STOP_SEQ']
                            
                            
                 );
                    
                    
                $this->local_up_db->select('BUS_SERVICE_NO');
                $this->local_up_db->from('current_bus_service_stops');
                $this->local_up_db->where("BUS_SERVICE_NO", $services[$i]['BUS_SERVICE_NO']);
                $this->local_up_db->where("BUS_STOP_CD", $services[$i]['BUS_STOP_CD']);
                $this->local_up_db->where("ARRIVAL_TM", $services[$i]['ARRIVAL_TM']);	
                $this->local_up_db->where("IS_BOARDING_STOP", $services[$i]['IS_BOARDING_STOP']);	
                $this->local_up_db->where("DEPARTURE_TM", $services[$i]['DEPARTURE_TM']);	
                $this->local_up_db->where("ARRIVAL_DAY", $services[$i]['ARRIVAL_DAY']);	
                $this->local_up_db->where("STOP_SEQ", $services[$i]['STOP_SEQ']);
                $query = $this->local_up_db->get();
    
                $data_ins= $query->result_array();
              
               if(isset($data_ins) && count($data_ins) > 0){
                    $this->local_up_db->where('BUS_SERVICE_NO', $services[$i]['BUS_SERVICE_NO']);
                    $this->local_up_db->where("BUS_STOP_CD", $services[$i]['BUS_STOP_CD']);
                    $this->local_up_db->where("ARRIVAL_TM", $services[$i]['ARRIVAL_TM']);	
                    $this->local_up_db->where("IS_BOARDING_STOP", $services[$i]['IS_BOARDING_STOP']);	
                    $this->local_up_db->where("DEPARTURE_TM", $services[$i]['DEPARTURE_TM']);	
                    $this->local_up_db->where("ARRIVAL_DAY", $services[$i]['ARRIVAL_DAY']);	
                    $this->local_up_db->where("STOP_SEQ", $services[$i]['STOP_SEQ']);
                    $this->local_up_db->update('current_bus_service_stops', $data);
                   $return_val[$services[$i]['BUS_SERVICE_NO']]='This Bus Services Stops is Updated';
               }else{
                   $this->local_up_db->insert('current_bus_service_stops', $data); 
                   $return_val[$services[$i]['BUS_SERVICE_NO']]='This Bus Services Stops is Inserted';
               }
                                
                
            }
            show($return_val,1);
            }
                    
        }else
        {
            echo 'Error : insert current booking bus stops';
            show($data);
        }
    }
     
    
    /****function  starts here*****/
    
    /***********fetch data for current booking***************/
    function insert_cb_routes($response)
     {
         $data=$response['RoutesResponse']['Routes']['routes'];
        
          foreach($data as $key=>$val):
             $insert_routes=array("route_no"        =>  $val['route_no'],
                                  "route_nm"        =>  $val['route_nm'],
                                  "FROM_STOP_CD"    =>  $val['from_stop_cd'],
                                  "TILL_STOP_CD"    =>  $val['till_stop_cd'],
                                  "last_sync_update"=>  date('Y-m-d',strtotime($val['SYNC_LAST_UPDATE']))
                                 );
          $this->db2->select('count(*)');
          $this->db2->where('route_no',$val['route_no']);
          $this->db2->where('last_sync_update',$response['sync_date']);
          
          $cnt=$this->db2->count_all_results('routes');
          if($cnt > 0)
            {
             
                $this->db2->where('route_num',$val['route_no']);  
                $this->db2->update('routes',$insert_routes);  
                echo "route updates successful";
            }
            else
            {
                
              $this->db2->insert('routes',$insert_routes); 
              echo "route insert successful";
            }   
          
          
          
              
          endforeach;
       
     }
     
     function  insert_cb_route_stops($response)
      {
        
         
//         $this->db1->select('rt.route_no,rt.STOP_SEQ, rt.BUS_STOP_CD, bs.BUS_STOP_NM,s.STATE_NM,rt.INTRA_STATE_DISTANCE,rt.INTER_STATE_DISTANCE,rt.KM,bs.flag');
//         $this->db1->from('route_stops rt');
//         $this->db1->join('bus_stops bs','bs.BUS_STOP_CD = rt.BUS_STOP_CD','INNER');
//         $this->db1->join('states s', 's.STATE_CD = bs.STATE_CD','inner');
//         $this->db1->where('rt.ROUTE_NO',$response);
//         $this->db1->order_by('stop_seq');
//         $data=$this->db1->get();
//         $res=  $data->result_array();
//         
//         if(!empty($res))
//            {
//                  foreach($res as $key=>$val):
//             
//                  $insert=array("route_no"      =>$val['route_no'],
//                                "stop_seq"      =>$val['STOP_SEQ'],
//                                "bus_stop_cd"   =>$val['BUS_STOP_CD'],
//                                "bus_stop_nm"   =>$val['BUS_STOP_NM'],
//                                "state_nm"      => $val['STATE_NM'],
//                                "INTRA_STATE_DISTANCE"=>$val['INTRA_STATE_DISTANCE'],
//                                "INTER_STATE_DISTANCE"=>$val['INTER_STATE_DISTANCE'],
//                                "km"=> $val['KM'],
//                                "flag"=>$val['flag'],
//                          
//               );
//                 
//                 //check route_stops exist or not
//                  $this->db2->select('route_no,bus_stop_cd');
//                  $this->db2->from('route_stops');
//                  $this->db2->where('route_no',$val['route_no']);
//                  $data=$this->db2->get();
//                  $res = $data->result_array();
//                  
//                  if(count($res)!=count($insert))
//                  {
//                    $this->db2->delete('route_stops', array('route_no' => $val['route_no']));   
//                      
//                  }
//                  
//                  $this->db2->insert('route_stops',$insert);
//                endforeach;
//            }
            
          if(empty($response) && count($response)==0)
            {
              $err_msg='No routes change for current date';
              echo  $err_msg;
              log_data('cron/route_stops_'.date('d-m-Y').'.log',$err_msg, 'route_stops');           
            }
            else
            {
                 /* check route_stops  in local database */               
                  $this->local_up_db->select('*');
                  $this->local_up_db->from('route_stops');
                  $this->local_up_db->where('route_no',$response);
                  $data=$this->local_up_db->get();
                  $res = $data->result_array();
                               
                  //fetch routes  data from central server
                  
                    $this->live_up_db->select('rt.route_no,rt.STOP_SEQ, rt.BUS_STOP_CD, bs.BUS_STOP_NM,s.STATE_NM,rt.INTRA_STATE_DISTANCE,rt.INTER_STATE_DISTANCE,rt.KM,bs.flag');
                    $this->live_up_db->from('route_stops rt');
                    $this->live_up_db->join('bus_stops bs','bs.BUS_STOP_CD = rt.BUS_STOP_CD','INNER');
                    $this->live_up_db->join('states s', 's.STATE_CD = bs.STATE_CD','inner');
                    $this->live_up_db->where('rt.ROUTE_NO',$response);
                    $this->live_up_db->order_by('stop_seq');
                    $data=$this->live_up_db->get();
                    $res_arr=  $data->result_array();
                        foreach($res_arr as $key=>$val):                     
                    $insert[]=array("route_no"              => $val['route_no'],
                                    "stop_seq"              => $val['STOP_SEQ'],
                                    "bus_stop_cd"           => $val['BUS_STOP_CD'],
                                    "bus_stop_nm"           => $val['BUS_STOP_NM'],
                                    "state_nm"              => $val['STATE_NM'],
                                    "INTRA_STATE_DISTANCE"  => $val['INTRA_STATE_DISTANCE'],
                                    "INTER_STATE_DISTANCE"  => $val['INTER_STATE_DISTANCE'],
                                    "km"                    => $val['KM'],
                                    "flag"                  => $val['flag']
                                 );
                       
                          endforeach;                                                
                if(empty($res) && count($res)==0)
                    {
                        /**insert update delete  algorithm****/              
                        $this->local_up_db->insert_batch('route_stops',$insert);  
                        $succ_msg="insert";
                       log_data('cron/route_stops_'.date('d-m-Y').'.log',$succ_msg, 'route_stops');                     
                    }
                  else
                  {                      
                     $this->local_up_db->delete('test_route_stops', array('route_no' => $response)); 
                     $this->local_up_db->insert_batch('test_route_stops',$insert);
                  }                 
            }    

      }
      
      function get_routes($date='')
         {
           
          $this->local_up_db->select('route_no');
         $this->local_up_db->from('routes');
         if($date !="")
         {
             $this->local_up_db->where('last_sync_update', date('Y-m-d', strtotime($date)));
         }
         $this->local_up_db->order_by('route_no');
         $data=$this->local_up_db->get();
         return $data->result_array();
          
         }
         
         function check_routes_flat_fare()
         {
             $this->local_up_db->select('distinct(route_no)');
             $this->local_up_db->from('flat_fare');
             $data=$this->local_up_db->get();
             return $data->result_array();
             
             
         }
         
         function  delete_flat_fare($route_no)
            {
            
             $this->local_up_db->delete('flat_fare',array('route_no'=>$route_no));
             
            }
         
         
         function get_bus_types()
         {
                $this->local_up_db->select('bus_type_cd');
               $this->local_up_db->from('bus_types');
                $data=$this->local_up_db->get();
               return $data->result_array();
         }
          
         /*function   :insert_cb_flat_fare
          * parameter :fare detail
          * insert date wise fare in database
          */
      
    
      function insert_cb_flat_fare($response)
            {
              $log_file=array();             
               $insert_arr=array();
                
                   if(isset($response) && isset($response['RouteFareResponse']) && isset($response['RouteFareResponse']['busServiceStops']) && isset($response['RouteFareResponse']['busServiceStops']['busServiceStop']))
                    {                           
                        $fare_traingle=$response['RouteFareResponse']['busServiceStops']['busServiceStop'];
                        
                         if(!empty($fare_traingle))
                          {
                        
                         if (!isset($fare_traingle[0]['route_no'])) 
                             {
                                $service_fare = array($fare_traingle);                         
                             }
                             else
                             {
                                 $service_fare =$fare_traingle;                                                           
                             }
                                                           
                            foreach($service_fare as $key=>$val):
                                
                            if(!isset($val['fare']))
                                {
                                    show($val,'','No fare');
                                    show($service_fare,1,'No fare');
                                }
                           
                        if($val['fare'] > 0)
                            {    
                            //check fare 
                                    $this->local_up_db->select('route_no');
                                    $this->local_up_db->from('flat_fare');
                                    $this->local_up_db->where('route_no',$val['route_no']);
                                    $this->local_up_db->where('from_stop_cd',$val['fromStop']['busStopcode']);
                                    $this->local_up_db->where('to_stop_cd',$val['toStop']['busStopcode']);
                                    $this->local_up_db->where('bus_type',$response['bus_type']);
                                    $this->local_up_db->where('journy_date',$response['journy_date']);
                   
                                    $data=$this->local_up_db->get();
                                    $res = $data->result_array();
              
                        if(count($res)==0 && empty($res))
                            {                                                       
                                    $insert_arr[] =array("route_no"         =>  $val['route_no'],
                                                  "from_stop_name"  =>  $val['fromStop']['busStopName'],
                                                  "from_stop_cd"    =>  $val['fromStop']['busStopcode'],
                                                  "to_stop_name"    =>  $val['toStop']['busStopName'],
                                                  "to_stop_cd"      =>  $val['toStop']['busStopcode'],
                                                  "from_stop_seq"   =>  $val['fromStop']['stopSequence'],
                                                  "till_stop_seq"   =>  $val['toStop']['stopSequence'],
                                                  "adult_fare"      =>  $val['fare'],
                                                  "child_fare"      =>  $val['childFare'],
                                                  "bus_type"        =>  $response['bus_type'],
                                                  "journy_date"     =>  $response['journy_date'],
                                                  "toll"            =>  $val['TOLL'],
                                                  "acs"             =>  $val['ACS'],
                                                  "res_charges"     =>  $val['RC'],
                                                  "sleeper_fare"    =>  $val['sleeperFare'],
                                                  "pas"             =>  $val['PAS'],
                                                  "IT"              =>  $val['IT']
                                              );
                                                       
                                $detail=$val['route_no'].'=>'.$response['bus_type']."-> Inserted successfully";

                                    if(isset($detail))
                                          {
                                            array_push($log_file,$detail);
                                          }                                     
                       }                         
                   }
                endforeach;
                /* insert fare data into database */              
                  if(!empty($insert_arr))
                     {
                            $this->local_up_db->insert_batch('flat_fare',$insert_arr);   
                     }    
                 foreach(array_unique($log_file) as $key=>$val):
                    log_data('cron_error/flat_fare_'.date('d-m-Y').'.log',$val, 'flat_fare');
                 endforeach;                    
               }
               else {
                   show($response,'','improper data');
               }             
              }
              else
              {                
                $data['error'] = 'Unable to insert fare in database';
                $data['response'] = $response;
                  
               log_data('cron_error/flat_fare_'.date('d-m-Y').'.log', $data, 'flat_fare');
              } 
                
           }
    
         
         
         
           
    
    
}
