<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once('Upsrtc_current_booking_model.php');

class Up_bus_types_model extends Upsrtc_model {

    function __construct() {
        parent::__construct();
    }

    function get_bus_types() {
        $this->db2->distinct();
        $this->db2->select("BUS_TYPE_ID,BUS_TYPE_NM");
        $this->db2->from("bus_types");
        $this->db2->order_by("BUS_TYPE_NM", "asc");
        $query = $this->db2->get();
        return $query->result_array();
    }

}
