<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once('Upsrtc_current_booking_model.php');

class Up_v_routes_model extends Upsrtc_model {

    function __construct() {
        parent::__construct();
    }

    function get_v_routes() {
        $this->db2->distinct();
        $this->db2->select("route_no,route_nm");
        $this->db2->from("v_routes");
        $this->db2->order_by("route_no", "asc");
        $query = $this->db2->get();
        return $query->result_array();
    }

}
