<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once('Upsrtc_current_booking_model.php');

class Up_routes_model extends Upsrtc_model {

    function __construct() {
        parent::__construct();
    }

    function get_routes() {
        $this->db2->distinct();
        $this->db2->select("ROUTE_NO");
        $this->db2->from("routes");
        $query = $this->db2->get();
        return $query->result_array();
    }

}
