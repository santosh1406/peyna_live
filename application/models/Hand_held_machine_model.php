<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class  Hand_held_machine_model extends My_Model
{
    /**
     * Instanciar o CI
     */
    var $table    = 'etm_master';
//    var $fields	= "ticket_no,ticket_no,pnr_no,ticket_type,request_type,ticket_status,bus_service_no,dept_time,boarding_time,alighting_time,from_stop_cd,till_stop_cd,boarding_stop_cd,destination_stop_cd,num_passgr,tot_fare_amt,cancellation_rate,ticket_ref_no,issue_time,pay_id,session_no,op_name,guid,user_email_id,mobile_no,dept_date,inserted_date,inserted_by,status";
    var $fields		= array("ETIM_NO");
    var $key    = 'WAYBILL_ID';
     var $db_con  ='upsrtc_current_booking';
    protected $set_created = false;

    protected $set_modified = false;
    
    public function __construct() {
        parent::__construct();
        $this->_init();      
        
    }
         
	public function fetch_machines($depot = array()) {
        $where  = "";
		if(!empty($depot)) {
            $where = " WHERE am.depot_cd = '".$depot['depot']."'";
        }
        $query = $this->db->query('SELECT ETIM_NO as etim_no FROM etm_master em JOIN bos_dev.agent_master am ON (em.agent_id = am.agent_id)'.$where);
        $data = $query->result_array(); //get current query record.
        return $data;
    }
 
}
