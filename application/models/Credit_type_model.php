<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Credit_type_model extends MY_Model {

    var $table  = 'credit_type';
    var $fields = array("id","name","description","status","added_by","added_on");
    var $key    = 'id';



    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
  public function credit_type_details()
  {
        $this->db->select('id,name');
        $this->db->order_by('name');
        $this->db->where('status','1');
        $data = $this->db->get($this->table);
        if($data->num_rows()>0)
        {
            return $data->result_array();
        }
        return false;
  }
}
