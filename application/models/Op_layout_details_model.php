<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Op_layout_details_model extends MY_Model
{
    /**
     * Instanciar o CI
     */
    var $table      = 'op_layout_details';
    var $fields		= "id,op_seat_layout_id,row_no,col_no,berth_no,seat_no,seat_type,quota_type,created_by,created_date,updated_by,updated_date";
    var $key        = "id";
    
    public function __construct()
    {
            parent::__construct();
            $this->_init();
    }
    
    /*
     * @function    : add_seat_layout_detail
     * @param       : $seatlayoutdetail -> layout detail to add.
     * @detail      : to add seat layout detail.
     *                 
     */

    public function add_seat_layout_detail($seatlayoutdetail) {
        if (!empty($seatlayoutdetail)) {
            if ($this->db->insert_batch('op_layout_details', $seatlayoutdetail)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

// end of add_seat_layout_detail
}
