<?php

require_once ('Rsrtc_Model.php');
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rsrtc_current_booking_model extends Rsrtc_model {

    function __construct() {
        parent::__construct();
    }

    /*     * *********fetch data for current booking************** */

    function insert_cb_allbustype($response) {
        $data = $response['AllBusTypeResponse']['allBusType']['bustypes'];
        // show($data,1);
        foreach ($data as $key => $val):
            $insert_routes = array("BUS_TYPE_ID" => $val['busTypeId'],
                "BUS_TYPE_CD" => $val['busTypeCd'],
                "BUS_TYPE_NM" => $val['busTypeName']
            );

            $this->rsrtc_current_booking->insert('bus_types', $insert_routes);


        endforeach;
        echo "bus type master insert successful";
    }

    function insert_cb_allbusstops($response) {
        $data = $response['BusStopsResponse']['busServiceStops']['busServiceStop'];
        // show($data,1);
        foreach ($data as $key => $val):
            $insert_routes = array("BUS_STOP_NM" => $val['busStopName'],
                "BUS_STOP_CD" => $val['busStopcode'],
                "TALUKA_NM" => $val['taluka'],
                "DISTRICT_NM" => $val['district'],
                "STATE_CD" => $val['state'],
                "SEARCH_FLAG" => $val['searchFlag']
            );

            $query_sel = $this->rsrtc_current_booking->query("SET FOREIGN_KEY_CHECKS=0");


            $this->rsrtc_current_booking->insert('bus_stops', $insert_routes);


        endforeach;
        echo "bus stops insert successful";
    }

    function insert_cb_alldepot($response) {
        $data = $response['AllDepotResponse']['depotMastDetail']['alldepots'];
        // show($data,1);
        foreach ($data as $key => $val):
            $insert_routes = array("DEPOT_CD" => $val['depotCd'],
                "DEPOT_ID" => $val['depotId'],
                "DEPOT_NM" => $val['depotNm']
            );


  $query_sel = $this->rsrtc_current_booking->query("SET FOREIGN_KEY_CHECKS=0");
            $this->rsrtc_current_booking->insert('depots', $insert_routes);


        endforeach;
        echo "depot insert successful";
    }

    function insert_cb_alldistrict($response) {
        $data = $response['DistrictResponse']['districtDetail']['districts'];
        foreach ($data as $key => $val):
            $insert_routes = array("DISTRICT_ID" => $val['districtId'],
                "DISTRICT_NM" => $val['districtNm'],
                "STATE_CD" => $val['stateCd']
            );


            $query_sel = $this->rsrtc_current_booking->query("SET FOREIGN_KEY_CHECKS=0"); //show($query_sel,1);
            //    $result = $query_sel->result();

            $this->rsrtc_current_booking->insert('district', $insert_routes);


        endforeach;
        echo "district insert successful";
    }

    function insert_cb_gprs($response) {
        $val = $response['GprsResponse']['gprsDetail']['gprs'];
       //show($data,1);
        //foreach ($data as $key => $val):
            $insert_routes = array("SERVICE_PROVIDER" => $val['serviceProvider'],
                "SERVER_IP" => $val['serverIp'],
                "LINK_NM" => $val['linkNm'],
                "PORT_NO" => $val['portNo'],
                "GPRS_FLAG" => $val['gprsFlag']
            );


        //    $query_sel = $this->db->query("SET FOREIGN_KEY_CHECKS=0");
            

            $this->rsrtc_current_booking->insert('gprs_config', $insert_routes);


      //  endforeach;
        echo "gprs_config insert successful";
    }
    function insert_cb_ticketfares($response) {
        $data = $response['TicketFareResponse']['ticketFareDetail']['ticketFare'];
       //show($data,1);
        foreach ($data as $key => $val):
            $insert_routes = array("BUS_TYPE_CD" => $val['busTypeCd'],
                "ADULT_MIN_FARE" => $val['adultMinFare'],
                "CHILD_MIN_FARE" => $val['childMinFare'],
                "EFFECTIVE_FROM" => $val['effectiveFrom'],
                "EFFECTIVE_TILL" => (isset($val['effectiveTill'])?$val['effectiveTill']:'0000-00-00 00:00:00'),
                 "CHILD_CONCESSION_RATE" => $val['childConRate'],
                "FOR_ROUTE_TYPE" => $val['forRouteType'],
                "ROUTE_FARE_TYPE" => $val['routeFareType']
               
            );


            $query_sel = $this->rsrtc_current_booking->query("SET FOREIGN_KEY_CHECKS=0");
            

            $this->rsrtc_current_booking->insert('ticket_fares', $insert_routes);


        endforeach;
        echo "ticket_fares insert successful";
    }
 function insert_cb_ticketfaresdetails($response) {
        $data = $response['TicketFareDetailResponse']['ticketFareDetail']['ticketFare'];
     
        foreach ($data as $key => $val):
            $insert_routes = array("FARE_ID"=>$val['fareId'],
                "BUS_TYPE_CD" => $val['busTypeCd'],
                "FROM_KM" => $val['fromKm'],
                "TILL_KM" => $val['tillKm'],
                "FARE_PER_KM" => $val['farePerKm'],
                "ACC_COMP_SURCHARGE" => $val['accSurcharge'],
                "HR_SURCHARGE" => $val['hrCharge'],
                "FACTOR_TYPE" => $val['factorType'],
                "FARE_FACTOR" => $val['fareFactor'],
                "FARE_STATUS" => $val['fareStatus']
            );


           $query_sel = $this->rsrtc_current_booking->query("SET FOREIGN_KEY_CHECKS=0");
            

            $this->rsrtc_current_booking->insert('ticket_fare_details', $insert_routes);


        endforeach;
        echo "ticket_fare_details insert successful";
    }
}
