<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Ticket_discount_description_model extends MY_Model
{
    /**
     * Instanciar o CI
     */
    var $table    = 'ticket_discount_description';
    var $fields		= array("id","ticket_id","berth_no","seat_no","seat_basic_fare","discounted_seat_basic_fare","seat_fare_with_tax","discounted_seat_fare_with_tax","service_tax_per","service_tax","discounted_service_tax","op_service_charge_per","op_service_charge","discounted_op_service_charge","discount_on","discount_type","discount_value","discount_flat_value","discount_per","description");
    var $key      = 'id';

    protected $set_created = false;

    protected $set_modified = false;
    
    public function __construct() {
        parent::__construct();
        $this->_init();      

        
    }
    

}
