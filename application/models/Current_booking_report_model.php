<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Current_booking_report_Model extends CI_Model {

    var $up_db_revised;
    var $default_db;

    function __construct() { 
        parent::__construct();
        $this->default_db = get_database('default');
        $this->up_db_revised = $this->load->database('upsrtc_current_booking', TRUE);
    }
      
    function zone_data() {
        $this->up_db_revised->distinct();
        $this->up_db_revised->select('DIVISION_NM,DIVISION_CD');
        $this->up_db_revised->from('divisions');
       
        $res = $this->up_db_revised->get();
        
        return $res->result_array();
    }
    
    function depot_data($division_cd = '') {
        $this->up_db_revised->distinct();
        $this->up_db_revised->select('DEPOT_NM ,DEPOT_CD, DIVISION_CD');
        $this->up_db_revised->from('depots');
        if(!empty($division_cd)) {
            $this->up_db_revised->where('DIVISION_CD',$division_cd);
        }
        
        $res = $this->up_db_revised->get();
        //$d = $this->up_db_revised->last_query();
        //pr($d);
        return $res->result_array();
    }
    
    function agent_depot_data($division_cd = '') {
        $this->db->distinct();
        $this->db->select('agent_id ,agent_code,fname');
        $this->db->from('agent_master');
        if(!empty($division_cd)) {
            $this->db->where('depot_cd',$division_cd);
        }
        
        $res = $this->db->get();
        //$d = $this->up_db_revised->last_query();
        //pr($d);
        return $res->result_array();
    }
function cb_agent_depot_data($input) {
       $this->db->distinct();
        $this->db->select('am.agent_id ,am.agent_code,am.fname');
        $this->db->from('agent_master am');
        if(!empty($input['depot_cd'])) {
            $this->db->where('am.depot_cd',$input['depot_cd']);
        }
        
        $this->db->join("waybillprogramming w",'w.agent_id=am.agent_id',"inner") ;
        if ($input['from_date'] && $input['from_date'] != "") {
            $this->db->where('DATE_FORMAT(w.COLLECTION_TM , "%Y-%m-%d")>=', date('Y-m-d', strtotime($input['from_date'])));
        }
        if ($input['to_date'] && $input['to_date'] != "") {
            $this->db->where('DATE_FORMAT(w.COLLECTION_TM, "%Y-%m-%d")  <=', date('Y-m-d', strtotime($input['to_date'])));
        }
        $res = $this->db->get();
        //$d = $this->up_db_revised->last_query();
        //pr($d);
        return $res->result_array();
    }
    function agent_details($id){
       
     $this->db->select('am.agent_code,am.fname name,em.ETIM_NO,c.stCity,am.depot_name,am.zone_name,am.depot_cd,am.zone_cd');
     $this->db->join("rsrtc.etm_master em",'em.agent_id =am.agent_id ',"left") ;
     $this->db->join("city_master c",'am.city_id=c.intCityId',"left") ;
     $this->db->where('am.agent_id',$id);
     $this->db->from('agent_master am');
     $res=$this->db->get();
     return $res->result_array();
                
                
    }

     function waybill_details($id){
       
     $this->db->select('am.agent_code,am.fname name,em.ETIM_NO,c.stCity,am.depot_name,am.zone_name,am.depot_cd,am.zone_cd');
     $this->db->join("rsrtc.etm_master em",'em.agent_id =am.agent_id ',"inner") ;
     $this->db->join("city_master c",'am.city_id=c.intCityId',"inner") ;
     $this->db->join("waybillprogramming w",'em.ETIM_NO=w.ETIM_NO',"inner") ;
     $this->db->where('w.WAYBILL_NO',$id);
     $this->db->from('agent_master am');
     $res=$this->db->get();
     return $res->result_array();
                
                
    }
    function agent_etim_details() {

        /* $this->up_db_revised->select('count(em.agent_id) as no_agent');
        //$this->db->join("rsrtc.etm_master em",'em.agent_id =am.agent_id ',"inner") ;

        $this->up_db_revised->from('etm_master em');
        
        if ($this->session->has_userdata('depot_user') && !empty($this->session->userdata('depot_cd'))) {
            $this->up_db_revised->join('bos_dev.agent_master am', "am.agent_id = em.agent_id");
            
            foreach($this->session->userdata('depot_cd') as $k=>$d) {
                if($k == 0) {
                    $this->up_db_revised->where('am.depot_cd', $d['depot_cd']);
                }
                else {
                    $this->up_db_revised->or_where('am.depot_cd', $d['depot_cd']);
                }
            }
            
        }
        
        $this->up_db_revised->where('em.USER_UNREGISTER', '0');
        $res = $this->up_db_revised->get();
        return $res->result_array(); */
        
        $res = $this->db->query('SELECT DISTINCT(am.AGENT_ID),cb_flag  FROM agent_master am Inner Join ticket_data td on td.agent_id=am.agent_id where am.cb_flag=1');
        $a = $res->result_array();
        $ret[0]['no_agent'] = (!empty(count($a))) ? count($a) : 0;
        return $ret;
        /* $res = $this->db->get();
        echo $this->db->last_query();
        pr($res->result_array());die;
        return $res->result_array(); */
        
    }

    function topup_agent(){
       
     $this->db->select('amount');
     //$this->db->join("rsrtc.etm_master em",'em.agent_id =am.agent_id ',"inner") ;
         
     $this->db->from('op_wallet w');
     $this->db->where('w.user_id','1');
     $res=$this->db->get();
     return $res->result_array();
                
                
    }
function total_tkt(){
       
    $this->db->select('sum(full_ticket_count+half_ticket_count) tot_tkt');
    $this->db->join('rsrtc.routes ro', 'ro.route_no=td.route_no', 'inner');
    $this->db->join('agent_master am', 'am.agent_id=td.agent_id', 'inner');
    $this->db->from('ticket_data td');
    if ($this->session->has_userdata('depot_user') && !empty($this->session->userdata('depot_cd'))) {
        //$this->db->where('am.depot_cd', $this->session->userdata('depot_cd'));
        foreach($this->session->userdata('depot_cd') as $k=>$d) {
            if($k == 0) {
                $this->db->where('am.depot_cd', $d['depot_cd']);
            }
            else {
                $this->db->or_where('am.depot_cd', $d['depot_cd']);
            }
        }
        
    }
    $res=$this->db->get();
     return $res->result_array();
                
                
                
    }
function depot_data_where($dept_cd) {
        $this->up_db_revised->distinct();
        $this->up_db_revised->select('DIVISION_CD,DEPOT_NM,DEPOT_CD');
        $this->up_db_revised->from('depots');
        $this->up_db_revised->where('DEPOT_CD',$dept_cd);
        $res = $this->up_db_revised->get();
      
        return $res->result_array();
    }
    function zone_data_where($division_cd) {
       // $this->up_db_revised->distinct();
        $this->up_db_revised->select('DIVISION_NM');
        $this->up_db_revised->from('divisions');
        $this->up_db_revised->where('DIVISION_CD',$division_cd);
        $res = $this->up_db_revised->get();
      
        return $res->result_array();
    }

    function op_topup_report($input){
     $this->db->select("0,ot.op_trans_id,ot.created_date ,ot.created_date oc,ot.transaction_code,ot.depositor_bank_name,"
                . "ot.transaction_amt,format((ot.transaction_amt * ot.commission_rate/100),2) AS GC,"
                . "format((((ot.transaction_amt *ot.commission_rate/100) * ot.tds_rate /100)),2) AS TDS,"
                . "format((ot.transaction_amt + ((ot.transaction_amt * ot.commission_rate/100) - (((ot.transaction_amt *ot.commission_rate/100) * ot.tds_rate /100)))),2) AS NC,"
                . "ot.cumulative_opening_balance,(SELECT cumulative_closing_balance FROM op_trans_details WHERE ref_op_trans_id = ot.op_trans_id) AS cb, "
                . "IF((ot.is_deleted ='1' && ot.is_status = '0'),'Rejected',  IF((ot.is_deleted ='0' && ot.is_status < '1'), 'Pending', 'Approved')) AS status");
        $this->db->from('op_topup o');
        $this->db->join('op_trans_details ot', 'o.id=ot.op_topup_id', 'inner');                                 
         
     
        if ($input['from_date'] && $input['from_date'] != "") {
            $this->db->where('DATE_FORMAT(ot.created_date , "%Y-%m-%d")>=', date('Y-m-d', strtotime($input['from_date'])));
        }
        if ($input['to_date'] && $input['to_date'] != "") {
            $this->db->where('DATE_FORMAT(ot.created_date, "%Y-%m-%d")  <=', date('Y-m-d', strtotime($input['to_date'])));
        }
        
        if ($input['status'] && $input['status'] == "approved") {
            $this->db->where('ot.is_deleted', '0');
            $this->db->where('ot.is_status', '1');
        }
        if ($input['status'] && $input['status'] == "pending") {
           $this->db->where('ot.is_deleted','0');
            $this->db->where('ot.is_status', '0');
        }
        if ($input['status'] && $input['status'] == "rejected") {
           $this->db->where('ot.is_deleted', '1');
            $this->db->where('ot.is_status', '0');
        }
        $this->db->where('ot.commission_rate_type!=', 'percentage');
        $this->db->order_by('ot.created_date', 'desc');
        $res=$this->db->get();
        return $res->result_array();
    }
    
 }

?>