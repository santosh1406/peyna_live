<?php


if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Disbursement_model extends MY_Model {
    
    var $table  = 'incentive_disbursement';
    var $fields = array("id","disbursement_amount" ,"disbursement_date","referrer_id","agent_id" ,"total_tickets","total_transaction_amt","referrence_code");
    var $key    = "id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
    
    function  get_disbursement($per_page, $page)
    {
		 $page	= ($page>0?$page:1);		
        $offset	= $per_page*($page-1);
       $this->db->select("r.referrer_name,r.id as referrer_id,r.referrer_code,r.is_deleted,r.is_status,u.role_id,u.id,concat(u.first_name,' ',u.last_name) as Agent_Name,count(t.ticket_id) as total ,coalesce(sum(t.tot_fare_amt_with_tax),0) as total_amount",false);

        $this->db->from("referrer_master r");
        // $this->db->join("users u", "tm.inserted_by = u.user_id", "inner");
        $this->db->join("users u", "r.referrer_code = u.referrer_code", "inner");
        $this->db->join("tickets t", "t.inserted_by = u.id", "left");
		
		$this->db->where("u.role_id",'16');
        $this->db->where("t.transaction_status",'success');
       // $this->db->where("t.disbursement_id =0");
      //  $this->db->order_by("tm.ticket_id","desc");
        
        if(($this->session->userdata('select_referrer'))){
          $this->db->where('r.id', $this->session->userdata('select_referrer'));
        }
	if(($this->session->userdata('till_date'))){
          $this->db->where('DATE(t.inserted_date) <=', date('Y-m-d',strtotime($this->session->userdata('till_date'))));
        }
       	$this->db->group_by("u.id"); 
		
        if($per_page > 0)
        {
          $this->db->limit($per_page, $offset);
        }
	
		
       $referrer_result = $this->db->get();
	   $result = $referrer_result->result_array();
		        
		    return $result;
				  
		
    }
     function  get_disbursement_count()
    {
       $this->db->select("r.referrer_name,r.id as referrer_id,r.referrer_code,r.is_deleted,r.is_status,u.role_id,u.id,concat(u.first_name,' ',u.last_name) as Agent_Name,count(t.ticket_id) as total_tickets ,t.inserted_by,coalesce(sum(t.tot_fare_amt_with_tax),0) as total_amount",false);

        $this->db->from('referrer_master r');
        // $this->db->join("users u", "tm.inserted_by = u.user_id", "inner");
        $this->db->join("users u", "r.referrer_code = u.referrer_code", "inner");
        $this->db->join("tickets t", "t.inserted_by = u.id", "left");
		
		$this->db->where("u.role_id",'16');
        $this->db->where("t.transaction_status",'success');
       // $this->db->where("t.disbursement_id =0");
      //  $this->db->order_by("tm.ticket_id","desc");
	  if(($this->session->userdata('select_referrer'))){
          $this->db->where('r.id', $this->session->userdata('select_referrer'));
        }
		 if(($this->session->userdata('till_date'))){
          $this->db->where('DATE(t.inserted_date) <=', date('Y-m-d',strtotime($this->session->userdata('till_date'))));
        }
        $this->db->group_by("u.id");
		 if($per_page > 0)
        {
          $this->db->limit($per_page, $offset);
        }
       $referrer_result = $this->db->get();
	   $result = $referrer_result->result_array();
	// echo '<pre>';print_r($result);echo '</pre>';exit;
		        //  return $result;
		
		          return $result;
				  
		
    }
	
	  function  get_all_incentives()
    {
       $this->db->select("im.id,im.incentive_name,im.valid_from,im.valid_to,im.incentive_type_id,im.incentive_type_applicable_id,im.incentive_type,
	   im.incentive_value,im.is_status",false);

        $this->db->from('incentive_master im');
        $this->db->where('im.is_status','Y');
       // $this->db->join("incentive_slab_details sl", "im.id = sl.incentive_id", "full");
       
       // $this->db->where("t.disbursement_id =0");
      //  $this->db->order_by("tm.ticket_id","desc");
        
       $incentives_result = $this->db->get();
        $incentives = $incentives_result->result_array();
	// echo '<pre>';print_r($incentives);echo '</pre>';exit;
		        //  return $result;
	   return $incentives;		  
		
    }
	function  get_incentive_by_id($incentive_id)
    {
       $this->db->select("im.id,s1.id,s1.from_slab,s1.to_slab,s1.incentive_value",false);

        $this->db->from('incentive_master im');
        $this->db->join("incentive_slab_details s1", "im.id = s1.incentive_id", "left");
       	
        $this->db->where("im.id =".$incentive_id);
      //  $this->db->order_by("tm.ticket_id","desc");
        
       $incentive_result = $this->db->get();
	   $incentive_slab_detail= $incentive_result->result_array();
	// echo '<pre>';print_r($incentive_slab_detail);echo '</pre>';exit;
		        //  return $result;
	   return $incentive_slab_detail;		  
		
    }
	function  get_tickets_incentive($valid_from,$valid_to,$inserted_by)
    {
       $this->db->select("count(ticket_id) as total_tickets ,coalesce(sum(tot_fare_amt_with_tax),0) as total_amount",false);

        $this->db->from("tickets");
        // $this->db->join("users u", "tm.inserted_by = u.user_id", "inner");
		
        $this->db->where("inserted_by",$inserted_by);
        $this->db->where("transaction_status",'success');
        $this->db->where('role_id', 16);
        $this->db->where('DATE(inserted_date) >=', $valid_from);
        $this->db->where('DATE(inserted_date) <=', $valid_to);
      //  $this->db->order_by("tm.ticket_id","desc");
       // $this->db->group_by("u.id");
        $agent_tickets_result = $this->db->get();
	   $agent_result = $agent_tickets_result->row();
		return $agent_result;
				  
		
    }
	
	// To get the last disbursement date
	function  get_last_disbursement_date($inserted_by)
    {
       $this->db->select("agent_id,disbursement_date,disbursement_amount,id",false);

        $this->db->from('incentive_disbursement');
      //  $this->db->join("incentive_slab_details s1", "im.id = s1.incentive_id", "left");
       	
        $this->db->where("agent_id =".$inserted_by);
       $this->db->order_by("id","desc");
        
       $disbursement_date_result = $this->db->get();
	   $disbursement_date= $disbursement_date_result->row();
		        //  return $result;
	   return $disbursement_date;		  
		
    }
	
	// To get the disbursement id from slab id
	function  get_last_disbursement_slab($agentid,$incentive_id,$slab_id='')
    {
       $this->db->select("id.agent_id,id.disbursement_date,id.disbursement_amount,id.id,s.slab_id,s.incentive_id",false);

        $this->db->from('incentive_disbursement id');
        $this->db->join("incentive_disbursement_detail s", "id.id = s.disbursement_id", "left");
       	
        $this->db->where("id.agent_id =".$agentid);
		$this->db->where("s.incentive_id =".$incentive_id);
		if($slab_id){
		$this->db->where("s.slab_id =".$slab_id);
		}
      // $this->db->order_by("id","desc");
        
       $disbursement_from_slab_result = $this->db->get();
	 
		 $dids= $disbursement_from_slab_result->num_rows();
		 if($dids>0){
		 	return $dids;
		  $disbursement_slab= $disbursement_from_slab_result->result_array();
		  // echo '<pre>';print_r($disbursement_slab);echo '</pre>';exit;
		  }	
		  else return 0;	
	   //return $disbursement_slab;		  
		
    }
	//To check if disbursent meant is already given for Incentive type 1 or type 2
	
}
