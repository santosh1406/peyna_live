<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Credit_account_cheque_detail_model extends MY_Model {

    var $table  = 'credit_account_cheque_detail';
    var $fields = array("id","user_id","credit_amount","from","to","cheque_number","cheque_amount","bank_name","ifsc_code","micr_code","account_number","expiry_date","credit_type","status","accept_reject_status","added_by","added_on","updated_by","updated_on","accept_reject_by");
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
    public function agent_credit_acc_details($id)
    {
	    $this->db->select('cacd.id,cacd.user_id,cacd.cheque_amount as credit_limit_amt,cacd.from as effective_from,cacd.till as effective_to,ct.name as credit_type,cacd.added_on,cacd.credit_amount,(CASE cacd.accept_reject_status when "NA" THEN "Pending" when "A" THEN "Accepted" when "R" THEN "Rejected" ELSE `accept_reject_status` END) as request_status,(case cacd.accept_reject_status when "NA" THEN NULL ELSE `cacd`.`updated_on` END) as accept_reject_date, cacd.status');
	  	$this->db->from('credit_account_cheque_detail cacd');
	  	$this->db->join('credit_type ct','cacd.credit_type=ct.id','left');
	  	$this->db->where('cacd.user_id', $id);
		$this->db->order_by('cacd.id','desc');
	  	$data = $this->db->get();
	  	
	  	if($data->num_rows()>0)
	    {
	        return $data->result_array();
	    }
    }
}
