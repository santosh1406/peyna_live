<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Payment_gateway_transaction_model extends MY_Model
{
    /**
     * Instanciar o CI
     */
    var $table    = 'payment_gateway_transaction';
    var $fields		= array("id","ticket_id","pg_tracking_id","order_bill_country","order_fee_flat","order_bank_ref_no","order_capt_amt","order_bill_email","order_gtw_id","order_no","order_ship_country","order_bill_state","order_ship_address","order_tax","order_bill_zip","order_bank_response","order_device_type","order_gross_amt","order_bill_city","order_fraud_status","order_bill_tel","order_discount","order_ip","order_card_name","status","order_amt","order_ship_city","order_TDS","order_option_type","order_status","order_ship_state","order_ship_tel","order_fee_perc_value","order_status_date_time","order_ship_name","order_ship_zip","order_date_time","order_notes","order_bill_address","order_currncy","order_bill_name","inserted_date");
    var $key    = 'id';

    protected $set_created = false;

    protected $set_modified = false;
    
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
    public function insert_ccavenue_data($data)
    {
       if (!empty($data))
        {
              $query = $this->db->get_where('payment_gateway_transaction', 
                array('pg_tracking_id' => $data['pg_tracking_id'] )); 
              $count = $query->num_rows();
              if ($count === 0) 
              {
                if($this->db->insert('payment_gateway_transaction', $data))
                  {
                    return true;
                  }
              }
        }
        else
        {
            return false;
        } 
    }

}
