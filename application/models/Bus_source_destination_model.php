<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bus_source_destination_model extends MY_Model {
    
    var $table  = 'bus_source_destination';
    var $fields = array("id","op_id","source_name","source_name_id","destination_name","destination_name_id","inserted_date");
    var $key    = "id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
}