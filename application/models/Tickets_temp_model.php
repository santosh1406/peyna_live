<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Tickets_temp_model extends MY_Model
{
    /**
     * Instanciar o CI
     */
    var $table    = 'tickets_temp';
    var $fields		= array("ticket_id","pnr_no","ticket_ref_no","boss_ref_no","operator_ticket_no","bos_ticket_digit","bos_ticket_string","pg_tracking_id","api_ticket_no","ticket_type","op_id","provider_id","provider_type","partial_cancellation","commision_percentage","inventory_type","bus_service_no","route_schedule_id","request_type","ticket_status","dept_time","alighting_time","boarding_time","droping_time","from_stop_cd","till_stop_cd","from_stop_name","till_stop_name","boarding_stop_name","destination_stop_name","boarding_stop_cd","destination_stop_cd","pickup_address","num_passgr","tot_fare_amt","tot_fare_amt_with_tax","total_fare_without_discount","adult_basic_fare","child_basic_fare","fare_acc","fare_hr","fare_it","fare_octroi","fare_other","fare_reservationCharge","fare_sleeperCharge","fare_toll","fare_convey_charge","service_tax","ac_service_tax","asn_fare","discount_value","discount_flat_value","discount_per","discount_type","discount_on","cancellation_rate","issue_time","pay_id","session_no","op_name","booked_from","guid","user_email_id","role_id","booked_by","user_type","mobile_no","eticket_count","payment_by","payment_mode","inserted_date","inserted_by","status","transaction_status","failed_reason","id_proof_required","is_return_journey","return_ticket_id","bus_type","mticket","cancellation_policy","payment_confirmation","is_transaction_confirmed","merchant_amount","pg_charges","mobile_api_user_id","ip_address","coupon_discount_amount","coupon_id","tds_per","agent_commission_amt","total_basic_amount","op_service_charge","discount_id");
    var $key    = 'ticket_id';

    protected $set_created = false;

    protected $set_modified = false;
    
    public function __construct() {
        parent::__construct();
        $this->_init();      

        
    }
}
