<?php

//Created by harshada kulkarni on 24-07-2018
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Utilities_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    // added by harshada kulkarni on 24-07-2018
    public function getMobileOperators($char = "", $type = "") {

        $queryStr = "select *"
                . " FROM operators_urls"
                . " WHERE operator LIKE '%$char%'"
                . " AND status = 'Y'"
                . " AND type = '$type'"
                . " AND api_operator = 1";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        $nCount = count($result);

        if ($nCount > 0) {
            for ($i = 0; $i < $nCount; $i++) {
                $aResult[$i]['label'] = $result[$i]['operator'];
                $aResult[$i]['name'] = $result[$i]['operator'];
                $aResult[$i]['value'] = $result[$i]['id'];
                $aResult[$i]['id'] = $result[$i]['id'];
            }
        } else {
            $aResult[0]['label'] = 'No result found';
            $aResult[0]['name'] = 'No result found';
            $aResult[0]['value'] = '';
            $aResult[0]['id'] = '';
        }
        //$aResult_json = json_encode($aResult);
        return $aResult;
    }

    // added by harshada kulkarni on 24-07-2018
    public function operatorDetails($operator = "", $type = "") {

        $queryStr = "select *"
                . " FROM operators_urls"
                . " WHERE operator = '" . $operator . "'"
                . " AND status = 'Y'"
                . " AND type = '" . $type . "'"
                . " AND api_operator = 1";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        return $result;
    }

    // added by harshada kulkarni on 18-08-2018
    public function getUserWalletDetails($user_id = "") {
        $queryStr = "select amt"
                . " FROM wallet"
                . " WHERE user_id = '" . $user_id . "'"
                . " AND status = 'Y'"
                . " AND is_deleted = 'N'";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        return $result;
    }

    // added by harshada kulkarni on 18-08-2018
    public function operatorDetailsByID($operatorId, $type) {

        $queryStr = "select *"
                . " FROM operators_urls"
                . " WHERE id = '" . $operatorId . "'"
                . " AND status = 'Y'"
                . " AND type = '" . $type . "'"
                . " AND api_operator = 1";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        return $result;
    }

    // added by harshada kulkarni on 18-08-2018
    public function updateUserWallet($user_id = "", $updated_wallet_amt = "", $wallet_tran_id = "") {
        $queryStr = "UPDATE wallet 
        SET amt = '" . $updated_wallet_amt . "'
        , last_transction_id = '" . $wallet_tran_id . "' 
        , updated_date = '" . date("Y-m-d H:i:s") . "' 
        WHERE user_id = '" . $user_id . "'";

        $query = $this->db->query($queryStr);

        $queryStr1 = 'SELECT id from wallet where user_id = "' . $user_id . '"';
        $insert_id = $this->db->query($queryStr1);
        $insert_id = $insert_id->result_array();
        return $insert_id;
    }

    // added by harshada kulkarni on 18-08-2018
    public function saveTransactionData($data = "") {
        $this->db->insert('recharge_transaction', $data);
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $data, 'JRI Recharge - Save Transaction Data');

    }

    public function saveJRIData($data = "") {
        $this->db->insert('jri_transaction_details', $data);
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $data, 'JRI Recharge - Save JRI');

    }

    // added by harshada kulkarni on 15-09-2018
    public function getRetailerServicesCommission($user_id = "", $service_id = "",$created_by ="") {
        $this->db->select('u.id as user_id,u.agent_code,u.level_1,u.level_2,u.level_3,u.level_4,u.level_5,rsm.service_id as service_id,sc.id as commission_id,sc.values,sc.total_commission,sc.created_by');
        $this->db->from("users u");
        $this->db->join('retailer_service_mapping rsm', 'u.id= rsm.agent_id', 'left');
        $this->db->join('services_commissions sc', 'rsm.service_id = sc.service_id', 'left');
        $this->db->where('rsm.agent_id =', $user_id);
        $this->db->where('rsm.service_id =', $service_id);
        if(!empty($created_by)){
         $this->db->where('sc.created_by =', $created_by);   
        }
        $this->db->where('sc.status', 'Y');
        $this->db->where('sc.from >=', date('Y-m-d H:i:s'));
        $this->db->where('sc.till <=', date('Y-m-d H:i:s'));
        $user_result = $this->db->get();
        $user_result_array = $user_result->result_array();
        if (empty($user_result_array)) {
            $this->db->select('u.id as user_id,u.agent_code,u.level_1,u.level_2,u.level_3,u.level_4,u.level_5,rsm.service_id as service_id,sc.id as commission_id,sc.values,sc.total_commission,sc.created_by');
            $this->db->from("users u");
            $this->db->join('retailer_service_mapping rsm', 'u.id= rsm.agent_id', 'left');
            $this->db->join('services_commissions sc', 'rsm.service_id = sc.service_id', 'left');
            $this->db->where('rsm.agent_id =', $user_id);
            $this->db->where('rsm.service_id =', $service_id);
            if(!empty($created_by)){
         	$this->db->where('sc.created_by =', $created_by);   
            }
            $this->db->where('sc.status', 'Y');
            $this->db->where('sc.default_status', 'Y');
            $user_result = $this->db->get();
            $user_result_array = $user_result->result_array();
        }
        return $user_result_array;
    }

    public function getRetailerServicesCommissionForRecharges($user_id = "", $service_id = "", $sub_service_id, $created_by ="") {
        //show($user_id);
        //show($service_id);
        //show($sub_service_id, 1);

        $this->db->select('u.id as user_id,u.agent_code,u.level_1,u.level_2,u.level_3,u.level_4,u.level_5,rsm.service_id as service_id,sc.id as commission_id,sc.values,sc.total_commission,sc.created_by');
        $this->db->from("users u");
        $this->db->join('retailer_service_mapping rsm', 'u.id= rsm.agent_id', 'left');
        $this->db->join('services_commissions sc', 'rsm.service_id = sc.service_id', 'left');
        $this->db->where('rsm.agent_id =', $user_id);
        $this->db->where('rsm.service_id =', $service_id);
        $this->db->where('sc.sub_service_id =', $sub_service_id);
	if(!empty($created_by)){
         $this->db->where('sc.created_by =', $created_by);   
        }
        $this->db->where('sc.status', 'Y');
        $this->db->where('sc.from >=', date('Y-m-d H:i:s'));
        $this->db->where('sc.till <=', date('Y-m-d H:i:s'));
        $user_result = $this->db->get();
        $user_result_array = $user_result->result_array();
        if (empty($user_result_array)) {
            $this->db->select('u.id as user_id,u.agent_code,u.level_1,u.level_2,u.level_3,u.level_4,u.level_5,rsm.service_id as service_id,sc.id as commission_id,sc.values,sc.total_commission,sc.created_by');
            $this->db->from("users u");
            $this->db->join('retailer_service_mapping rsm', 'u.id = rsm.agent_id', 'left');
            $this->db->join('services_commissions sc', 'rsm.service_id = sc.service_id', 'left');
            $this->db->where('rsm.agent_id =', $user_id);
            $this->db->where('sc.service_id =', $service_id);
            $this->db->where('sc.sub_service_id =', $sub_service_id);
            if(!empty($created_by)){
         $this->db->where('sc.created_by =', $created_by);   
        }
            $this->db->where('sc.status', 'Y');
            $this->db->where('sc.default_status', 'Y');
            $user_result = $this->db->get();
            $user_result_array = $user_result->result_array();
        }

//         show($this->db->last_query(),1);
        return $user_result_array;
    }

    // added by harshada kulkarni on 09-10-2018
    public function saveVasCommissionData($data = "") {
        $this->db->insert('vas_commission_detail', $data);
    }

    /**
     * @description - To validate data 
     * @param array of data
     * @added by Sachin on 14-08-2018
     * @return int array
     */
    public function get_validation($type, $operatorId) {
        switch ($type) {
            case 'landline':
                if ($operatorId == 16 || $operatorId == 17) { //BSNL Landline-Online or BSNL Landline-Retail
                    $config = array(
                        array('field' => 'operator', 'label' => 'Landline Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter landline operator.')),
                        array('field' => 'number', 'label' => 'Phone Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check phone number.')),
                        array('field' => 'account_number', 'label' => 'Account Number', 'rules' => 'trim|required|numeric|min_length[10]|max_length[10]|xss_clean', 'errors' => array('required' => 'Please check account number.')),
                        array('field' => 'authenticator', 'label' => 'Service Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select service type.'))
                    );
                } else if ($operatorId == 10 || $operatorId == 14) {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Landline Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter landline operator.')),
                        array('field' => 'number', 'label' => 'Phone Number', 'rules' => 'trim|required|numeric|exact_length[8]|xss_clean', 'errors' => array('required' => 'Please check phone number.')),
                        array('field' => 'account_number', 'label' => 'Account Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check account number.'))
                    );
                }
                break;
            case 'electricity':
                if ($operatorId == 18) { //Assam Power Distribution Company Ltd (APDCL)- RAPDR
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[9]|max_length[11]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 19) { //Assam Power Distribution Company Ltd (APDCL)-NON RAPDR
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[9]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 20) { //'Bangalore Electricity Supply Company'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 21) { //'Bharatpur Electricity Services Ltd'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'K Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check K number.'))
                    );
                } else if ($operatorId == 33) { //'Bikaner Electricity Supply Limited (BkESL)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'K Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check K number.'))
                    );
                } else if ($operatorId == 22) { //'Brihan Mumbai Electric Supply and Transport Undertaking'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 23) { //'BSES Rajdhani'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[9]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 24) { ////'BSES Rajdhani'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[9]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 34) { //Calcutta Electricity Supply Ltd (CESC Ltd)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[11]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 35) { //'Chhattisgarh State Electricity Board'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Business Partner Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check business partner number.'))
                    );
                } else if ($operatorId == 36) { //'Chamundeshwari Electricity Supply Corp Ltd (CESCOM)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check consumer number.')),
                        array('field' => 'account_number', 'label' => 'Account Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check account number.'))
                    );
                } else if ($operatorId == 37) { //'Daman and Diu Electricity Department'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Account Number', 'rules' => 'trim|required|numeric|min_length[1]|max_length[6]|xss_clean', 'errors' => array('required' => 'Please check Account number.'))
                    );
                } else if ($operatorId == 38) { //'Dakshin Gujarat Vij Company Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[5]|max_length[11]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 39) { //'Dakshin Haryana Bijli Vitran Nigam'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please check consumer number.')),
                        array('field' => 'account_number', 'label' => 'Account Number', 'rules' => 'trim|required|exact_length[10]|numeric|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 40) { //'DNH Power Distribution Company Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Service Connection Number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please check service connection number.'))
                    );
                } else if ($operatorId == 41) { //'Eastern Power Distribution Company of Andhra Pradesh Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Service Number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please check serivce number.'))
                    );
                } else if ($operatorId == 42) { //'Gulbarga Electricity Supply Company Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 43) { //'Hubli Electricity Supply Company Ltd (HESCOM)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[5]|max_length[10]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 44) { //'India Power Corporation Limited - Bihar'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 45) { //'India Power Corporation - West Bengal'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|min_length[10]|max_length[12]|numeric|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 46) { //'Jamshedpur Utilities and Services Company Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Business Partner Number', 'rules' => 'trim|required|min_length[6]|max_length[10]|numeric|xss_clean', 'errors' => array('required' => 'Please check business partner number.'))
                    );
                } else if ($operatorId == 47) { //'Jaipur and Ajmer Viyut Vitran Nigam'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'K Number', 'rules' => 'trim|required|exact_length[12]|numeric|xss_clean', 'errors' => array('required' => 'Please k consumer number.'))
                    );
                } else if ($operatorId == 48) { //'Jodhpur Vidyut Vitran Nigam Ltd'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'K Number', 'rules' => 'trim|required|exact_length[12]|numeric|xss_clean', 'errors' => array('required' => 'Please k consumer number.'))
                    );
                } else if ($operatorId == 49) { //'Jharkhand Bijli Vitran Nigam Limited (JBVNL)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please check consumer number.')),
                        array('field' => 'account_number', 'label' => 'Subdivision Code', 'rules' => 'trim|required|min_length[1]|max_length[3]|numeric|xss_clean', 'errors' => array('required' => 'Please check subdivision code.'))
                    );
                } else if ($operatorId == 50) { //'Kota Electricity Distribution Ltd'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'K Number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please check k number.'))
                    );
                } else if ($operatorId == 51) { //'Madhya Pradesh Paschim Kshetra Vidyut Vitaran Co. Ltd -Indore'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'IVRS Number', 'rules' => 'trim|required|numeric|min_length[2]|min_length[30]|xss_clean', 'errors' => array('required' => 'Please check IVRS number.'))
                    );
                } else if ($operatorId == 52) { //'Meghalaya Power Distribution Corporati on Ltd'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[1]|min_length[12]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 53) { //'Madhya Gujarat Vij Company Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[5]|min_length[11]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 54) { //'MSEDC Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[5]|min_length[11]|xss_clean', 'errors' => array('required' => 'Please check consumer number.')),
                        array('field' => 'account_number', 'label' => 'Billing Unit', 'rules' => 'trim|required|exact_length[4]|numeric|xss_clean', 'errors' => array('required' => 'Please check billing unit.')),
                        array('field' => 'authenticator', 'label' => 'Processing Cycle', 'rules' => 'trim|required|exact_length[2]|numeric|xss_clean', 'errors' => array('required' => 'Please check Processing Cycle.'))
                    );
                } else if ($operatorId == 55) { //'Muzaffarpur Vidyut Vitran Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 56) { //'North Delhi Power Limited (Tata Power - DDL)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[11]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 57) { //'North Bihar Power Distribution Company Ltd'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[9]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 58) { //'Noida Power Company Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 59) { //'ODISHA Discoms(B2C)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 60) { //'ODISHA Discoms(B2B)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 61) { //'Paschim Gujarat Vij Company Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[5]|min_length[11]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 62) { //'Punjab State Power Corporation Ltd (PSPCL)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Account Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 63) { //'Reliance Energy (Mumbai)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[2]|xss_clean', 'errors' => array('required' => 'Please check consumer number.')),
                        array('field' => 'account_number', 'label' => 'Cycle Number', 'rules' => 'trim|required|exact_length[2]|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.'))
                    );
                } else if ($operatorId == 64) { //'Rajasthan Vidyut Vitran Nigam Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'K Number', 'rules' => 'trim|required|numeric|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check K number.'))
                    );
                } else if ($operatorId == 65) { //'South Bihar Power Distribution Company Ltd'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'CA Number', 'rules' => 'trim|required|numeric|min_length[9]|max_length[11]|xss_clean', 'errors' => array('required' => 'Please check CA number.'))
                    );
                } else if ($operatorId == 66) { //'SNDL Nagpur'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[10]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 67) { //'Southern Power Distr of Andhra Pradesh'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Service Number', 'rules' => 'trim|required|numeric|min_length[9]|max_length[13]|xss_clean', 'errors' => array('required' => 'Please check service number.'))
                    );
                } else if ($operatorId == 68) { //'Torrent Power'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Service Number', 'rules' => 'trim|required|numeric|min_length[9]|max_length[13]|xss_clean', 'errors' => array('required' => 'Please check serivce number.')),
                        array('field' => 'account_number', 'label' => 'City Name', 'rules' => 'trim|required|min_length[1]|max_length[30]|xss_clean', 'errors' => array('required' => 'Please check city name.'))
                    );
                } else if ($operatorId == 69) { //'Tripura State Electricity Corporation Ltd'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|min_length[1]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 70) { //'Tamil Nadu Electricity Board (TNEB)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|min_length[9]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 71) { //'TP Ajmer Distribution Ltd'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'K Number', 'rules' => 'trim|required|numeric|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 72) { //'Tata Power – Mumbai'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'CA Number', 'rules' => 'trim|required|numeric|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check CA number.'))
                    );
                } else if ($operatorId == 73) { //'Uttar Gujarat Vij Company Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[5]|max_length[11]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 74) { //'Uttar Haryana Bijli Vitran Nigam'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Account Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check account number.'))
                    );
                } else if ($operatorId == 75) { //'Uttarakhand Power Corporation Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Service Connection Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check service connection number.'))
                    );
                } else if ($operatorId == 76) { //'Uttar Pradesh Power Corp Ltd (UPPCL) - URBAN'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|min_length[10]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check service connection number.'))
                    );
                } else if ($operatorId == 77) { //'Uttar Pradesh Power Corp Ltd (UPPCL) - RURAL'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check service connection number.'))
                    );
                } else if ($operatorId == 78) { //'West Bengal State Electricity'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|exact_length[9]|xss_clean', 'errors' => array('required' => 'Please check consumer number.')),
                        array('field' => 'account_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                }
                break;
            case 'gas':
                if ($operatorId == 25) { //'ADANI GAS'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 26) { //'Gujarat Gas company Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Service Number', 'rules' => 'trim|required|numeric|min_length[1]|max_length[15]|xss_clean', 'errors' => array('required' => 'Please check serivce number.'))
                    );
                } else if ($operatorId == 79) { //'IGL (Indraprasth Gas Limited)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 80) { //'Mahanagar Gas Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Customer Account Number', 'rules' => 'trim|required|numeric|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check customer account number.')),
                        array('field' => 'account_number', 'label' => 'Bill Group Number', 'rules' => 'trim|required|min_length[1]|xss_clean', 'errors' => array('required' => 'Please check bill group number.'))
                    );
                } else if ($operatorId == 81) { //'Haryana City gas'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'CRN Number', 'rules' => 'trim|required|numeric|min_length[8]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check CRN number.'))
                    );
                } else if ($operatorId == 82) { //'Siti Energy'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'ARN Number', 'rules' => 'trim|required|numeric|min_length[7]|max_length[9]|xss_clean', 'errors' => array('required' => 'Please check ARN number.'))
                    );
                } else if ($operatorId == 83) { //'Tripura Natural Gas Company Ltd'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[1]|max_length[20]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 84) { //'Sabarmati Gas Limited (SGL)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Customer ID', 'rules' => 'trim|required|numeric|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check customer ID.'))
                    );
                } else if ($operatorId == 85) { //'Unique Central Piped Gases Pvt Ltd (UCPGPL)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 86) { //'Vadodara Gas Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|exact_length[7]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                }
                break;
            case 'water':
                if ($operatorId == 27) { //'UIT Bhiwadi'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'Customer ID', 'rules' => 'trim|required|numeric|min_length[3]|max_length[20]|xss_clean', 'errors' => array('required' => 'Please check customer ID.'))
                    );
                } else if ($operatorId == 28) { //'Uttarakhand Jal Sansthan(B2B)-Retail'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[7]|max_length[22]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 29) { //'Delhi Jal Board-Retail'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'K Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check K number.'))
                    );
                } else if ($operatorId == 30) { //'Municipal Corporation of Gurugram'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'K Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check k number.'))
                    );
                } else {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                }
                break;
            case 'broadband':
                if ($operatorId == 31) { //'Connect Broadband-Retail'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter broadband operator.')),
                        array('field' => 'number', 'label' => 'Directory Number', 'rules' => 'trim|required|min_length[4]|xss_clean', 'errors' => array('required' => 'Please check directory number.'))
                    );
                } else if ($operatorId == 32) { //'Hathway Broadband - Retail'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter broadband operator.')),
                        array('field' => 'number', 'label' => 'Customer ID', 'rules' => 'trim|required|numeric|min_length[9]|max_length[15]|xss_clean', 'errors' => array('required' => 'Please check customer ID.'))
                    );
                } else {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter broadband operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                }
                break;
        }
        return $config;
    }

    public function getDthOperators($char) {

        $queryStr = "select *"
                . " FROM operators_urls"
                . " WHERE operator LIKE '%$char%'"
                . " AND type = 'DTH'"
                . " AND status = 'Y'"
                . " AND api_operator = 1";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        $nCount = count($result);

        if ($nCount > 0) {
            for ($i = 0; $i < $nCount; $i++) {
                $aResult[$i]['label'] = $result[$i]['operator'];
                $aResult[$i]['name'] = $result[$i]['operator'];
                $aResult[$i]['value'] = $result[$i]['id'];
                $aResult[$i]['id'] = $result[$i]['id'];
            }
        } else {
            $aResult[0]['label'] = 'No result found';
            $aResult[0]['name'] = 'No result found';
            $aResult[0]['value'] = '';
            $aResult[0]['id'] = '';
        }
        $aResult_json = json_encode($aResult);
        echo $aResult_json;
    }

    public function dthOperator($operator) {

        $queryStr = "select *"
                . " FROM operators_urls"
                . " WHERE operator = '" . $operator . "'"
                . " AND status = 'Y'"
                . " AND api_operator = 1";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        return $result;
    }

    //Added by prabhat pal on 22-11-2018
     public function saveTSODTHData($data) {
        $this->db->insert('tso_transaction_details', $data);
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'tso_dth_recharge.log', $data, 'TSO DTH Recharge - Save TSO');

    }

    //Added by Vaidehi
    public function commission_from($user_id){
         $queryStr = "select id,level_3"
                . " FROM users u"
                . " WHERE id = '" . $user_id . "'"
                . " AND role_id = '" . RETAILER_ROLE_ID . "'";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        
        return $result;
    }

     public function update_comm_status($vas_id){
         $update_data = array(
            'comm_dist_status' => 'Y'
        ); 
        
        $this->db->where('id', $vas_id);
        return $this->db->update('vas_commission_detail', $update_data);
    }
    
 
}
