<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Lookup_detail_model extends MY_Model {

    var $table = 'lookup_detail';
    var $fields = array("id", "title", "type");
    var $key = "id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
    
    function get_reason()
    {
        $this->db->select('id,title');
        $this->db->order_by('title');
        $data = $this->db->get($this->table);
        if($data->num_rows()>0)
        {
            return $data->result_array();
        }
        return false;
    }

    function get_relevant_reason()
    {        
        $query = "Select id,title from ".$this->table." where id in(1,11,14,15)";
        $data = $this->db->query($query);
        if($data->num_rows()>0)
        {
            return $data->result_array();
        }
        return false;
    }
    
}    
