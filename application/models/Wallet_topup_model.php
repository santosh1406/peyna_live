<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Wallet_topup_model extends MY_Model {

    var $table  = 'wallet_topup';
    var $fields = array("id","user_id","amount","payment_transaction_id","pg_tracking_id","topup_by","bank_name","bank_acc_no","transaction_no","transaction_date","transaction_remark","transaction_status","payment_confirmation","created_by","created_date","updated_by","updated_date","is_status","is_approvel","app_rej_by","app_rej_date","reject_reason","account_status");
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();
        $this->msrtc_db = $this->load->database('msrtc_app', true);      
    }
    
    /* 
     * @Author      : Rajkumar Shukla
     * @function    : Topup_request_insert_in_table
     * @param       : 
     * @detail      : to insert in topup table.               
     */
    
    function agent_topup_request($input)
    {
        if (!empty($input)){
            if($this->db->insert('wallet_topup', $input))
            {
                return true;
            }
          
          }
        else{
            return false;
        }
    
        
    }
	
	
	function update_topup($data) {

        $this->db->where('id', $data["id"]);
        //$this->db->update("users", $userdata["user_array"]);
        // die($this->db->last_query());

        if ($this->db->update("wallet_topup", $data["data_array"])) {
            return true;
        } else {
            return false;
        }
    }
     public function balance_request_info()
    {
        $this->db->select('topup_by,amount,bank_name,bank_acc_no,ifnull(pg_tracking_id,"-") as "pg_tracking_id",transaction_no,transaction_remark,transaction_date,created_date,DATE_FORMAT(updated_date,"%d-%m-%Y") as updated_date,CASE is_approvel when "NA" THEN "Pending" when "A" THEN "Accepted" when "R" THEN "Rejected" ELSE `is_approvel` END as "is_approvel"',false);
        $this->db->from('wallet_topup');
        $this->db->where('user_id',$this->session->userdata('user_id'));
        $this->db->order_by('id','desc');
        $this->db->limit(100);
        $request_topup = $this->db->get();
        if ($request_topup->num_rows() > 0)
        {
            return $request_topup->result_array();
        }
            return FALSE;
    }

    public function balance_count_info()
    {
        $this->db->select('topup_by,amount,bank_name,bank_acc_no,ifnull(pg_tracking_id,"-") as "pg_tracking_id",transaction_no,transaction_remark,transaction_date,created_date,DATE_FORMAT(updated_date,"%d-%m-%Y") as updated_date,CASE is_approvel when "NA" THEN "Pending" when "A" THEN "Accepted" when "R" THEN "Rejected" ELSE `is_approvel` END as "is_approvel"',false);
        $this->db->from('wallet_topup');
        $this->db->where('user_id',$this->session->userdata('user_id'));
        $this->db->order_by('id','desc');
        $request_topup = $this->db->get();
        if ($request_topup->num_rows() > 0)
        {
            return $request_topup->result_array();
        }
            return FALSE;
    }
    
    
    public function failure_wallet_transactions($current_date)
    {
        $this->db->select('wt.user_id,u.display_name,u.email,amount,payment_transaction_id,ptf.pg_track_id,topup_by,bank_name,transaction_date,transaction_status,ptf.track_id');
        $this->db->from('wallet_topup wt');
        $this->db->join('payment_transaction_fss  ptf', "ptf.id = wt.payment_transaction_id",'left');
        $this->db->join('users  u', "u.id = wt.user_id",'left');
        $this->db->where('transaction_status !=', 'success');
        $this->db->where('topup_by', 'PG');
        $this->db->like('transaction_date',$current_date);
        $failure_topup = $this->db->get();

        return $failure_topup->result_array();
    }
    
    public function confirm_topup_data() 
    {
        $this->db->select('w_to.user_id,w_to.id,amount,payment_transaction_id,topup_by,bank_name,transaction_date,transaction_status,pg_tracking_id,w_tr.merchant_amount');
        $this->db->from('wallet_topup w_to');
        $this->db->join('wallet_trans  w_tr', "w_tr.topup_id = w_to.id",'left');
        $this->db->where('pg_tracking_id != ',null);
        $this->db->where('topup_by', 'PG');
        $this->db->where('payment_confirmation', 'not_done');
        $this->db->where("transaction_status", 'success');
        $this->db->where("w_to.is_status", 'Y');
        $this->db->where("w_to.is_approvel", 'A');
        $failure_topup = $this->db->get();
        return $failure_topup->result();
        
    }

    public function get_wallet_amt() {

        $this->db->select('amt');
        $this->db->from('wallet');
        $this->db->where('user_id',$this->session->userdata('user_id'));
        $res = $this->db->get();
        return $res->result_array();
    }

    public function get_msrtc_wallet_amt() {
        $this->msrtc_db->select("sum(transaction_amt) as amt");
        $this->msrtc_db->from("op_trans_details");
        $this->msrtc_db->where("is_status =",'1');
        $result = $this->msrtc_db->get();
        return $result->result_array();
    }
    
    
    
    
}
