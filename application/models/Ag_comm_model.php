<?php

class Ag_comm_model extends MY_Model {

    /**
     * Instanciar o CI
     */
    var $table = 'commission_agent';
    var $fields = array("id", "commission_id","commission","agent_id","type","from_date","till_date","record_status");
    var $key = "id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    function getagent() {

        $this->db->select('agent_id,agent_name');
        $this->db->from('booking_agents ba');
        $this->db->where('`agent_id` NOT IN (SELECT `agent_id` FROM `commission_agent`)', NULL, FALSE);
       
        $state = $this->db->get();


        return $state->result_array();
    }

 

    function getdata($agid) {

        $this->db->select("bs.agent_id,bs.agent_name,ca.type,ca.commission,date_format(ca.from_date,'%d-%m-%y') as from_date,date_format(ca.till_date,'%d-%m-%y') as till_date");
        $this->db->from('booking_agents bs');
        $this->db->join('commission_agent ca', 'bs.agent_id =ca.agent_id', 'inner');
        $this->db->where('bs.agent_id', $agid);
        $query = $this->db->get();
        return $query->result_array();
    }

    function update_data($data) {

        $this->db->where('agent_id', $data['agent_id']);
        $data1=array("type"=>$data['comtype'],"commission"=>$data['agcomm'],"from_date"=>date('y-m-d',strtotime($data['cstart'])),"till_date"=>date('y-m-d',strtotime($data['cend'])) );
        $this->db->update('commission_agent', $data1);
        return $this->db->affected_rows();
    }

}

?>
