<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sms_template_model extends MY_Model {

    var $table = 'sms_template';
    var $fields = array("sms_template_id", "sms_template_name", "sms_template_content", "is_deleted", "sms_op_id");
    var $key = 'sms_template_id';

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    function get_sms_template() {
        $this->db->select('sms_template_id,sms_template_name');
        $this->db->order_by("sms_template_name", "ASC");
        $data = $this->db->get('sms_template_id');
        return $data->result_array();
        // show($this->db->last_query(), 1);
    }

    public function sms_template_byid($op_tkt_format_id) {

        $this->db->select("sms_op_id,sms_template_id,sms_template_name,sms_template_content,is_deleted");
        $this->db->from("sms_template");
        $this->db->where("sms_template_id", $op_tkt_format_id);
        $query = $this->db->get();
        //show($this->db->last_query(), 1);
        return $query->result_array();
    }

    public function update_smstemplate($input) {
        // print_r($input);
        if (isset($input['deleted_flag']) == 'Y') {
            $delete = 'Y';
        } else {
            $delete = 'N';
        }

        $config = array(
            array('field' => 'txttitle', 'label' => 'Title', 'rules' => 'trim|required'),
            array('field' => 'body_content', 'label' => 'Content', 'rules' => 'trim|required')
        );
        if (form_validate_rules($config) == FALSE) {
            $data_status['error'] = $this->form_validation->error_array();
            $data_status['status'] = "@#error@";
        } else {

            $data = array("sms_op_id" => $input['op_id'], "sms_template_name" => $input['txttitle'], "sms_template_content" => $input['body_content'], "is_deleted" => $delete);
            $this->db->where('sms_template_id', $input['hiddn_sms_template_id']);
            $this->db->update('sms_template', $data);
            // show($this->db->last_query(), 1);

            $data_status['status'] = "@#success@";
        }
        return $data_status;
    }

    public function delete_smstemplate($input) {
        $data = array("is_deleted" => 'Y');
        $this->db->where('sms_template_id', $input['id']);
        $this->db->update('sms_template', $data);
        //show($this->db->last_query(), 1);
        $cnt = $this->db->affected_rows();
        if ($cnt != 0) {
            $data["flag"] = '@#success#@';
        } else {

            $data["flag"] = '@#failed#@';
        }
        return $data;
    }

}
