<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Coupon_log_model extends MY_Model {

    protected $table    = 'coupon_log';
    var $fields = array("id","user_id","coupon_code","coupon_id","added_by","added_on","status");
    var $key    = 'id'; 
   
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }


}
