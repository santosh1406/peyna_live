<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Ticket_data_model extends MY_Model
{
    /**
     * Instanciar o CI
     */
    var $table    = 'ticket_data';
//    var $fields	= "ticket_no,ticket_no,pnr_no,ticket_type,request_type,ticket_status,bus_service_no,dept_time,boarding_time,alighting_time,from_stop_cd,till_stop_cd,boarding_stop_cd,destination_stop_cd,num_passgr,tot_fare_amt,cancellation_rate,ticket_ref_no,issue_time,pay_id,session_no,op_name,guid,user_email_id,mobile_no,dept_date,inserted_date,inserted_by,status";
    var $fields		= array("ticket_id","ticket_type","full_ticket_count","half_ticket_count","luggage_ticket_count","full_amount","half_amount","luggage_amount","total_amount","ticket_no","transaction_date","transaction_time","trip_no","trip_direction","waybill_no","route_no","bus_service_code","from_name","from_code","till_name","till_code","from_seq","till_seq","travelled_km","adult_amt","child_amt","luggage_amt_per_unit","adult_basic_fare","child_basic_fare","asc_amt","bsc_amt","toll_amt","it_amt","octroi_amt","sleeper_amt","reservation_chrgs","state_code_1","state_code_2","state_code_3","state_code_4","state_code_5","state_code_6","state_code_7","state_code_8","state_code_9","state_code_10","factor_type","fare_type","etim_no","depot_code","vehicle_no","trip_departure_time","seat_no","docket_no","concession_short_name","state_code","district_code","conductor_stand_booking","gprs_status","ticket_sr_no","trip_id","ticket_code","curr_epurse_amount","last_epurse_amount","last_epurse_waybill","last_epurse_ticket_no","epurse_card_ID","last_trip_count","curr_trip_count","e_purse_flag","sbi_concession","user_id","agent_id","update_dt","service_charge");
    
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
}