<?php 
if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Cb_bus_stops_model extends MY_Model {

    var $table  = 'cb_bus_stops';
    var $fields = array("BUS_STOP_CD","BUS_STOP_NM","DEPOT_CD","DEVNAGIRI_NM","KIRAN_NM","VILLAGE_TOWN_NM","TALUKA_NM","DISTRICT_NM","STATE_CD","PIN_CODE","DEPO_TYPE","ROAD_TYPE","flag","SEARCH_FLAG","IS_AUDIT_TRAIL","kiran_flag","UPDATED_BY","UPDATED_DT","INSERTED_BY","INSERTED_DT","EDIT_FLAG","CATEGORY");
    var $key    = 'BUS_STOP_CD'; 
    
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    public function get_bus_services() {
    	$result = $this->db->query("select distinct BUS_STOP_CD from cb_bus_stops");
    	if(!empty($result)) {
    		return $result->result_array();
        } else {
        	return false;
        }
    }

    public function get_bus_services_routes($bus_service_id) {
        $msrtc_db = MSRTC_DB_NAME;
        $result = $this->db->query("select distinct route_no  from  ".$msrtc_db.". bus_stop_routes_link where BUS_STOP_CD='".$bus_service_id."'");
        if(!empty($result)) {
            return $result->result_array();
        } else {
            return false;
        }
    }
}