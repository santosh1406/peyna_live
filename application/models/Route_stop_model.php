<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Route_stop_model extends MY_Model {

    /**
     * Instanciar o CI
     */
    var $table = 'route_stops';
    var $fields = array("id", "route_id", "bus_stop_id", "seq_no", "km", "inserted_by", "inserted_date", "modified_by", "modified_date", "status", "bus_stop_name");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    public function edit_route_stop_list() {
        $input = $this->input->post();
        $user_id = $this->session->userdata('user_id');
        $data = array();
        $this->db->select("bus_stop_name,km,seq_no");
        $this->db->from("route_stops");
        $this->db->where("route_id", $input['route_id']);
        $this->db->where("status", "Y");
        $query = $this->db->get();
        //show($this->db->last_query(), 1);
        $data = $query->result_array();
        return $data;
    }

    public function get_route_stop_list($route_id, $route_no) {
        $data = array();
        $this->db->select("bus_stop_name,km,seq_no"); // from route_stops
        $this->db->from("route_stops");
        $this->db->where("route_id", $route_no);
        $this->db->where("status", "Y");
        $query = $this->db->get();
        $data = $query->result_array();
        $data['cnt'] = count($data);

        return $data;
    }

    public function get_route_stop_deatils($route_id) {
        $this->db->select('route_id,bus_stop_id,bus_stop_name,seq_no,km,inserted_by,inserted_date,modified_by,modified_date,status');
        $this->db->from('route_stops');
        $this->db->where_in('route_id', $route_id);
        $query = $this->db->get();
        $data = $query->result_array();
        return $data;
    }

}
