<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cb_msrtc_agent_transaction_model extends MY_Model {
    
    var $table  = 'cb_msrtc_agent_transaction';
    var $fields = array("id","agent_id","waybill_no","waybill_process_date","waybill_start_date","waybill_topup_amt","collection_date","total_tkts","consumed_amt","remaining_bal","waybill_process_status","waybill_start_status","collection_status","created_at","updated_at");
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();   

        $this->msrtc_db = $this->load->database('msrtc_app', true);   
    }
    public function getRetailerTicketCollection($agent_id)
    {
        $Today = date('Y-m');
        $last_month = date("Y-m",strtotime("-1 month"));

        if(!empty($agent_id))
        {
            $this->db->select("sum(consumed_amt) as comm_amount");
            $this->db->from("cb_msrtc_agent_transaction");
            $this->db->where("agent_id =",$agent_id);
            $this->db->where("DATE_FORMAT(created_at,'%Y-%m') =",$last_month);

            $user_result = $this->db->get();

            $user_result_array = $user_result->result_array();
            return $user_result_array;
        }else
        {
            return false;
        }
    }
    public function getRetailerTicketCollectionFromCbComm($agent_code)
    {
        $Today = date('Y-m');
        $last_month = date("Y-m",strtotime("-1 month"));
        if(!empty($agent_code))
        {
            $this->db->select("round(sum(comm_amount),2) as comm_amount");
            $this->db->from("cb_commission_detail");
            $this->db->where("agent_code =",$agent_code);
            $this->db->where("comm_status =","N");
            $this->db->where("DATE_FORMAT(created_date,'%Y-%m') =",$last_month);

            $user_result = $this->db->get();
            $user_result_array = $user_result->result_array();
            return $user_result_array;
        }else
        {
            return false;
        }
    }
     public function getRetailerTicketCommission($agent_code)
    {
        $Today = date('Y-m');
        $last_month = date("Y-m",strtotime("-1 month"));
        if(!empty($agent_code))
        {
            $this->msrtc_db->select("round(sum(commission_applicable_amt),2) as comm_amount");
            $this->msrtc_db->from("cb_ticket_commission");
            $this->msrtc_db->where("agent_code =",$agent_code);
            $this->msrtc_db->where("DATE_FORMAT(created_date,'%Y-%m') =",$last_month);
            $user_result = $this->msrtc_db->get();
            $user_result_array = $user_result->result_array();
            return $user_result_array;
        }else
        {
            return false;
        }
    }

    public function UpdateCommStatus($agent_code)
    {
        $Today = date('Y-m');
        $now = date('Y-m-d H:i:s', time());
        $last_month = date("Y-m",strtotime("-1 month"));

        $data = array(
               'comm_status' =>'Y',
               'updated_date' =>$now
            );

        $this->db->where('agent_code',$agent_code);
        $this->db->where("DATE_FORMAT(created_date,'%Y-%m') =",$last_month);
        $update = $this->db->update('cb_commission_detail', $data); 

        if($update)
        {
            return true;
        }else{
            return false;
        }

    }
         public function check_master($today)
    {
        if(!empty($today))
        {
            $this->msrtc_db->select("*");
            $this->msrtc_db->from("cb_update_masters");
            $this->msrtc_db->where("DATE_FORMAT(effective_date,'%Y-%m-%d')",$today);
            $this->msrtc_db->where("is_status","Y");
            $this->msrtc_db->order_by("id", "desc");
            $this->msrtc_db->limit("1");
            $user_result = $this->msrtc_db->get();
            $user_result_array = $user_result->result_array();
            if($user_result_array)
            {
                return true;
            }
        }else
        {
            return false;
        }
    }
}
?>
