<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Provider_operator_model extends MY_Model {

    var $table         = 'api_provider';
    var $fields        = array("id","provider_id","name","created_at","updated_at","created_by","updated_by","status","deleted");
    var $key           = 'id'; 
   
    public function __construct()
    {
        parent::__construct();
        $this->_init();   
    }
}
