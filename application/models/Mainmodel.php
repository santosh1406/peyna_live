<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainmodel extends MY_Model
{
	function __construct() {
		parent::__construct();

	}

	public function insertUsermasterdata($data) {
		$this->db->insert('app_user',$data);
		return $this->db->insert_id();
	}

	public function insertUserdetaildata($data) {
		$this->db->insert('app_user_detail',$data);
		//return $this->insert_id();
	}

      public function get_loginDetails_old($username) {
    	$this->db->select('au.agent_code, '
                . 'vm.vendor_code as user_code,'
                . 'au.username,'
                . 'au.first_name,'
                . 'au.last_name,'
                . 'au.password,'
                . 'au.id,'
                . 'vm.app_type,'
                . 'divisions.DIVISION_NM as division_name,'
                . 'd.DIVISION_CD as division_code,'
                . 'au.depot_name as depo_name,'
                . 'au.depot_cd as depo_code, '
               . 'salt, '

                . 'au.role_id');
        
        $this->db->from('users au');
        $this->db->join('ps_vendor_master vm','vm.vendor_id = au.vendor_id');
        $this->db->join('msrtc_replication.depots d ','au.depot_cd = d.DEPOT_CD');
        $this->db->join('msrtc_replication.divisions ','d.DIVISION_CD = divisions.DIVISION_CD');
    	$this->db->where('au.username',$username);
        $this->db->where('au.lock_status','N');
        $this->db->where('au.is_deleted','N');
        $this->db->where('au.kyc_verify_status','Y');
        $this->db->where('au.status','Y');
        $this->db->where('au.verify_status','Y');
        $this->db->where('au.email_verify_status','Y');

    	$query = $this->db->get();
    	$result = $query->row_array();
    	return $result;
    }
    
      public function get_loginDetails($username) {
    	$this->db->select('au.agent_code, '
                . 'au.username,'
		. 'au.first_name,'
                . 'au.last_name,'
                . 'au.password,'
                . 'au.id,'
                . 'dv.DIVISION_NM as division_name,'
                . 'rsm.division_code,'
                . 'd.DEPOT_NM as depo_name,'
                . 'rsm.depot_code as depo_code, '
                . 'salt, '
                . 'au.role_id');
        
        $this->db->from('users au');
     
         $this->db->join('retailer_service_mapping rsm','au.id = rsm.agent_id','inner');
           
        $this->db->join('msrtc_replication.depots d','rsm.depot_code = d.DEPOT_CD','inner');
        $this->db->join('msrtc_replication.divisions dv','rsm.division_code = dv.DIVISION_CD','inner');
    	$this->db->where('au.username',$username);
		$this->db->where('au.lock_status','N');
        $this->db->where('au.is_deleted','N');
        $this->db->where('au.kyc_verify_status','Y');
        $this->db->where('au.status','Y');
        $this->db->where('au.verify_status','Y');
        $this->db->where('au.email_verify_status','Y');
	$this->db->where('rsm.service_id',SMART_CARD_SERVICE);
        $this->db->where('rsm.`STATUS`','Y');

				
		
    	$query = $this->db->get();
    	$result = $query->row_array();
    	return $result;
    }

    public function bus_type($bus_type_cd) {
        // $DB2 = $this->load->database('msrtcors',TRUE);
        $this->db->select('BUS_TYPE_ID');
        $this->db->from('bus_types');
        $this->db->where('BUS_TYPE_CD',$bus_type_cd);
        $query = $this->db->get();
        return $query->row_array();
    }
    
    public function get_fare_from($fromStopcode)
    {
        $DB2 = $this->load->database('msrtcors',TRUE);
        $DB2->select("rst.Route_auto AS routeauto,rst.ROUTE_NO AS routeno,rst.STOP_SEQ AS stopseq,rst.KM AS km,bs.BUS_STOP_NM AS stopname");
        $DB2->from("route_stops rst");
        $DB2->join('bus_stops bs','rst.BUS_STOP_CD = bs.BUS_STOP_CD');
        $DB2->where('rst.BUS_STOP_CD',$fromStopcode);
        $DB2->where('rst.STATE_CD','MHR');
        $query = $DB2->get();
        return $query->row_array();
    }

    public function get_fare_to($toStopcode)
    {
        $DB2 = $this->load->database('msrtcors',TRUE);
        $DB2->select("rst.Route_auto AS routeauto,rst.ROUTE_NO AS routeno,rst.STOP_SEQ AS stopseq,rst.KM AS km,bs.BUS_STOP_NM AS stopname");
        $DB2->from("route_stops rst");
        $DB2->join('bus_stops bs','rst.BUS_STOP_CD = bs.BUS_STOP_CD');
       // $DB2->where("rst.BUS_STOP_CD BETWEEN '". $fromStopcode."' and '". $toStopcode."'");
       // $DB2->where('rst.BUS_STOP_NM BETWEEN "'. $fromStopcode.'" and "'. $toStopcode);
        $DB2->where('rst.BUS_STOP_CD',$toStopcode);
        $DB2->where('rst.STATE_CD','MHR');
        //$DB2->where('rst.BUS_STOP_NM',$toStopcode);*/
        //$DB2->limit(10);
        $query = $DB2->get();
        return $query->row_array();

    }

    public function get_validity($id)
    {
        $this->db->select('id,span_name,span_period,span_add');
        $this->db->from('ps_rfidmaster_span');
        $this->db->where('id',$id);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function get_all_validity() {
        $this->db->select('id,span_name,span_period,span_add');
        $this->db->from('ps_rfidmaster_span');
        $this->db->where('is_active','1');
        $query = $this->db->get();
        return $query->result_array();
    }

    //Insert audit rfid api services data
    public function insert_audit_data($insert_data) {
        $this->db->insert('audit_rfidservices',$insert_data);
        return $this->db->insert_id();
    }    

    public function get_terminal_id_old($terminal_code, $username) {
    	$this->db->select('t.terminal_id');
        $this->db->from('app_terminals t');
        $this->db->join('app_pos_master posm','posm.pos_id = t.pos_id');
        $this->db->join('users au','au.vendor_id = posm.vendor_id');
    	$this->db->where('t.terminal_code',$terminal_code);
    	$this->db->where('au.username',$username);
    	$query = $this->db->get();
    	$result = $query->row_array();
        return $result;
    }
    public function get_terminal_id($terminal_code, $username) {
      //  $username = rokad_decrypt($username, $this->pos_encryption_key);
        $this->db->select('r.terminal_id');
        $this->db->from('retailer_service_mapping r');
        $this->db->join('users u','u.id = r.agent_id');
        $this->db->where('r.terminal_code',$terminal_code);
        $this->db->where('u.username',$username);
        $this->db->where('u.status','Y');
        $this->db->where('r.status','Y');
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }

    public function insertSessiondata($data) {
        //print_r($data);
		$this->db->insert('active_session',$data);
		return $this->db->insert_id();
	}

    public function get_card_trans_id($req_data,$order_bys) {
        $this->db->select('*');
        $this->db->from('ps_card_transaction');
        $this->db->where(array('trimax_card_id' => $req_data['trimax_card_id'], 'write_flag' => '1', 'is_active' => '1'));
        $this->db->order_by('id',$order_bys);
        $this->db->limit(1);
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }

    public function get_passissue_allow_days($data) {
        $this->db->select('issue_before_journey_days');
        $this->db->from('ps_concession_service_rel');
        $this->db->where(array('id' => $data['conc_service_rel_id'], 'is_active' => '1'));
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }

    public function get_span_period($data) {
        $this->db->select('span_period_days');
        $this->db->from('ps_rfidmaster_span');
        $this->db->where(array('id' => $data['span_id'], 'is_active' => '1'));
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }

    public function get_command_master($where) {
        $this->db->select('cmd_value,cmd_block');
        $this->db->from('ps_command_master');
        $this->db->where($where);
        $this->db->where_in('cmd_block',array('0D','0E'));
        $this->db->order_by('cmd_block','asc');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function update_card_trans($id,$update_data) {
        $this->db->where(array('id' => $id));
        $result = $this->db->update(CARD_TRANS,$update_data);
        return $result;
    }

    public function update_card_trans_detail($id,$t_id,$update_data) {
        $this->db->where(array('card_transaction_id' => $id, 'trimax_card_id' => $t_id, 'is_active' => '1'));
        $this->db->update(CARD_TRANS_DETAILS,$update_data);
    }

    public function update_master_card($where,$update_data) {
        $this->db->where($where);
        $result = $this->db->update('ps_master_card',$update_data);
        return $result;
    }

    public function update_cust_card($where,$update_data) {
        $this->db->where($where);
        $result = $this->db->update(CUSTOMER_CARD_MASTER,$update_data);
        return $result;
    }

    public function insert_inssuance_data($data,$table_nm) {
       // var_dump($data); die();
        $this->db->insert($table_nm,$data);
        return $this->db->insert_id();
    }

    public function get_cust_trimax_id($display_card_id,$date='') {
        $this->db->select('trimax_card_id,card_category_cd');
        $this->db->from('ps_customer_card_master');
        $this->db->where(array('display_card_id' => $display_card_id, 'card_status' => 'D', 'is_active' => '1'));
        if(!empty($date)) {
            $this->db->where("'".$date."' BETWEEN card_validity_date and card_expiry_date");
        }
        $res = $this->db->get();
        $res_arr = $res->row_array();
        return $res_arr;
    }

    public function get_cust_pass_detail($trimax_card_id) {
        $this->db->select("p_cm.*,
                            p_ccm.*,
                            p_ct.id as card_trans_id,
                            p_ct.*,
                            p_imgd.*,
                            p_pm.*,
                            p_csm.*,
                            p_pc.category_name,
                            m_stud.*
                        ");
        $this->db->from('ps_customer_master p_cm');
        $this->db->join('ps_customer_card_master p_ccm','p_cm.id = p_ccm.customer_id','inner');
        $this->db->join('ps_card_transaction p_ct','p_ct.trimax_card_id = p_ccm.trimax_card_id','inner');
        $this->db->join('ps_image_details p_imgd','p_imgd.customer_card_master_id = p_ccm.id','left');
        $this->db->join('ps_proof_master p_pm','p_pm.id = p_ccm.proof_type','left');
        $this->db->join('ps_customer_student_master p_csm','p_csm.customer_id = p_cm.id','left');
        $this->db->join('ps_pass_category p_pc','p_pc.id = p_ct.pass_category_id','left');
        $this->db->join(SARAL_DB.'.msrtc_student_info_aundh_block m_stud','m_stud.student_id = p_ccm.proof_ref_number','left');
        $this->db->where(array('p_ct.trimax_card_id' => $trimax_card_id, 'p_ct.write_flag' => '1', 'p_cm.is_active' => '1','p_ccm.is_active' => '1','p_ct.is_active' => '1'));
        $this->db->order_by('p_ct.id','ASC');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

  /*  public function getPassClaimData($trimax_card_id) {
        $this->db->select("p_ct.activation_date,
                            p_ct.expiry_date,
                            b_stp_1.BUS_STOP_NM as from_stop_nm,
                            b_stp_2.BUS_STOP_NM as till_stop_nm
                        ");
        $this->db->from('ps_card_transaction p_ct');
        $this->db->join('ps_card_transaction_details p_ctd','p_ctd.card_transaction_id = p_ct.id','inner');
        $this->db->join(MSRTC_DB_NAME.'.bus_stops b_stp_1','b_stp_1.BUS_STOP_CD = p_ctd.from_stop_code','inner');
        $this->db->join(MSRTC_DB_NAME.'.bus_stops b_stp_2','b_stp_2.BUS_STOP_CD = p_ctd.till_stop_code','inner');
        $this->db->where(array('p_ct.trimax_card_id' => $trimax_card_id, 'p_ct.write_flag' => '1', 'p_ctd.is_active' => '1', 'p_ct.is_active' => '1'));
        $this->db->order_by('p_ctd.id','ASC');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    } */

    public function get_screen_code($data) {
        $this->db->select("p_sm.screen_code");
        $this->db->from('ps_screen_std_map p_ssm');
        $this->db->join('ps_screen_master p_sm','p_sm.id = p_ssm.screen_master_id','inner');
        $this->db->where(array('p_ssm.is_active' => '1', 'p_sm.is_active' => '1', 'p_ssm.student_std_id' => $data['standard']));
        $this->db->order_by('p_ssm.screen_master_id','ASC');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_card_trans_detail($trimax_card_id, $card_trans_id) {
        $this->db->select("p_ctd.*");
        $this->db->from('ps_card_transaction_details p_ctd');
        $this->db->where(array('p_ctd.trimax_card_id' => $trimax_card_id, 'p_ctd.card_transaction_id' => $card_trans_id, 'p_ctd.is_active' => '1'));
        $this->db->order_by('p_ctd.id','ASC');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_route_details($where) {

        $DB2 = $this->load->database('msrtcors',TRUE);
        $DB2->select('route_no,route_cd,route_nm,from_stop_nm,from_stop_cd,till_stop_nm,till_stop_cd,till_stop_nm,via_stop_cd,via_stop_nm');
        $DB2->from('v_routes');
        $DB2->where(array('route_no' => $where['route_no']));
        $DB2->order_by('route_no','asc');
        $query = $DB2->get();
        $result = $query->result_array();
        return $result;
    }

    public function update_card_tarns_write_flag($card_transaction_id,$trimax_card_id,$update_data) {
        $this->db->where(array('trimax_card_id' => $trimax_card_id));
        $this->db->where_not_in('id',$card_transaction_id);
        $result = $this->db->update(CARD_TRANS,$update_data);
        return $result;
    }

    public function get_cust_info($data) {
        $this->db->select("
                            CONCAT(p_cm.first_name,' ',p_cm.middle_name,' ',p_cm.last_name) as full_name,
                            p_cm.mobile_number,
                            p_ccm.registration_depo_cd,
                            p_pct.category_name as pass_category_nm,
                            v_rt.route_nm,
                            p_at.total_amount
                        ");
        $this->db->from('ps_customer_master p_cm');
        $this->db->join('ps_customer_card_master p_ccm',"p_ccm.customer_id = p_cm.id",'inner');
        $this->db->join('ps_card_transaction p_ct',"p_ct.trimax_card_id = p_ccm.trimax_card_id",'inner');
        $this->db->join('ps_card_transaction_details p_ctd',"p_ctd.card_transaction_id = p_ct.id",'inner');
        $this->db->join('ps_amount_transaction p_at',"p_at.card_transaction_id = p_ct.id",'inner');
        $this->db->join('ps_pass_category p_pct',"p_pct.id = p_ct.pass_category_id",'inner');
        $this->db->join(MSRTC_DB_RE_NAME.'.v_routes v_rt','v_rt.route_no = p_ctd.route_no','inner');
        $this->db->where(array('p_cm.id' => $data['customer_id'], 'p_ct.write_flag' => '1', 'p_ct.is_active' => '1', 'p_cm.is_active' => '1', 'p_ccm.is_active' => '1', 'p_ctd.is_active' => '1', 'p_at.is_active' => '1'));
        $res = $this->db->get();
        $res_arr = $res->result_array();
        return $res_arr;
    }

    public function update_card_statusold($data,$card_status,$insert_data,$tbl_nm) {

        $this->db->where(array('trimax_card_id' => $data['trimax_card_id'], 'is_active' => '1'));
        $this->db->update(CUSTOMER_CARD_MASTER,array('card_status' => $card_status, 'modified_by' => $data['apploginuser']));

        $this->db->where(array('trimax_card_id' => $data['trimax_card_id'], 'is_active' => '1'));
        $this->db->update(MASTER_CARD,array('card_status' => $card_status, 'modified_by' => $data['apploginuser']));

        $this->db->where(array('trimax_card_id' => $data['trimax_card_id'], 'is_active' => '1'));
        $this->db->update(INVENTORY,array('status' => $card_status, 'updated_by' => $data['apploginuser']));

        $this->db->insert($tbl_nm,$insert_data);
        return $this->db->insert_id();
    }
    
     public function update_card_status($data,$card_status,$insert_data,$tbl_nm) {

        $this->db->where(array('cust_card_id' => $data['custCardId'], 'is_active' => '1'));
        $this->db->update(CUSTOMER_CARD_MASTER,array('card_status' => $card_status, 'modified_by' => $data['apploginuser']));

        $this->db->where(array('cust_card_id' => $data['custCardId'], 'is_active' => '1'));
        $this->db->update(MASTER_CARD,array('card_status' => $card_status, 'modified_by' => $data['apploginuser']));

        $this->db->where(array('cust_card_id' => $data['custCardId'], 'is_active' => '1'));
        $this->db->update(INVENTORY,array('status' => $card_status, 'updated_by' => $data['apploginuser']));

       // $this->db->insert($tbl_nm,$insert_data);
        if(isset($insert_data['status']) && $insert_data['status'] == 'update') {
            unset($insert_data['status']);
            $this->db->where(array('cust_card_id' => $data['custCardId']));
            $this->db->update($tbl_nm,$insert_data);

        } else {
            $this->db->insert($tbl_nm,$insert_data);
            return $this->db->insert_id();
        }
    }
    
    public function savePassClaimData($input) {
        $data = array(
            'cust_card_id' => $input['cust_card_id'],
            'mobile_number' => $input['mobile_number'],
            'pass_data' => $input['pass_data'],
            'created_by' => $input['apploginuser']
        );
        $this->db->insert('ps_claim_pass_details',$data);
        return $this->db->insert_id();
    }

//    public function getPassClaimData($trimax_card_id) {
//        $this->db->select("p_ct.activation_date,
//                            p_ct.expiry_date,
//                            b_stp_1.BUS_STOP_NM as from_stop_nm,
//                            b_stp_2.BUS_STOP_NM as till_stop_nm
//                        ");
//        $this->db->from('ps_card_transaction p_ct');
//        $this->db->join('ps_card_transaction_details p_ctd','p_ctd.card_transaction_id = p_ct.id','inner');
//        $this->db->join(MSRTC_DB_RE_NAME.'.bus_stops b_stp_1','b_stp_1.BUS_STOP_CD = p_ctd.from_stop_code','inner');
//        $this->db->join(MSRTC_DB_RE_NAME.'.bus_stops b_stp_2','b_stp_2.BUS_STOP_CD = p_ctd.till_stop_code','inner');
//        $this->db->where(array('p_ct.trimax_card_id' => $trimax_card_id, 'p_ct.write_flag' => '1', 'p_ctd.is_active' => '1', 'p_ct.is_active' => '1'));
//        $this->db->order_by('p_ctd.id','ASC');
//        $query = $this->db->get();
//        $result = $query->result_array();
//        return $result;
//    }
    public function getPassClaimData($trimax_card_id) {
        $this->db->select("p_ct.activation_date,
                            p_ct.expiry_date,
                            p_ctd.from_stop_code as from_stop_nm,
                            p_ctd.till_stop_code as till_stop_nm
                        ");
        $this->db->from('ps_card_transaction p_ct');
        $this->db->join('ps_card_transaction_details p_ctd','p_ctd.card_transaction_id = p_ct.id','inner');
        // $this->db->join(MSRTC_DB_NAME.'.bus_stops b_stp_1','b_stp_1.BUS_STOP_CD = p_ctd.from_stop_code','inner');
        // $this->db->join(MSRTC_DB_NAME.'.bus_stops b_stp_2','b_stp_2.BUS_STOP_CD = p_ctd.till_stop_code','inner');
        $this->db->where(array('p_ct.trimax_card_id' => $trimax_card_id, 'p_ct.write_flag' => '1', 'p_ctd.is_active' => '1', 'p_ct.is_active' => '1'));
        $this->db->order_by('p_ctd.id','ASC');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
	
	
    public function get_session_id($session_id,$user_id) {
        $this->db->select('itms_session_id');
        $this->db->from('active_session');
        $this->db->where(array('session_id' => $session_id));
        $this->db->where(array('user_id' => $user_id));
        $query = $this->db->get();
        $result = $query->row_array();
        //echo $this->db->last_query();
        return $result;
    }
    
     public function update_request_id($id,$status) {
        $this->db->where(array('id' => $id));
        $this->db->update('audit_rfidservices',array('status' => $status));
    }
    
       public function get_request_id($data) {
        $this->db->select('*');
        $this->db->from('ps_transaction_inquiry');
        $this->db->where($data);
        $this->db->order_by('id','desc');
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }
    
      public function getTermIdByTermCode($terminal_code) {
        $this->db->select('t.terminal_id');
        $this->db->from('app_terminals t');
        $this->db->where(array('t.terminal_code' => $terminal_code, 'is_active' => '1'));
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }

     
    public function checkCardData($trimax_card_id,$trans_type_code,$card_status='',$active_status) {
        $this->db->select('
                            p_ccm.id as cust_card_id,
                            p_ct.id as card_trans_id,
                            p_ctd.id as card_trans_details_id,
                            p_at.id as amt_trans_id,
                            inv.id as inv_id,
                            p_mc.id as card_master_id
                        ');
        $this->db->from('ps_customer_card_master p_ccm');
        $this->db->join('ps_card_transaction p_ct','p_ct.trimax_card_id = p_ccm.trimax_card_id','inner');
        $this->db->join('ps_card_transaction_details p_ctd','p_ctd.card_transaction_id = p_ct.id','inner');
        $this->db->join('ps_amount_transaction p_at','p_at.card_transaction_id = p_ct.id','inner');
        $this->db->join('inventory inv','inv.trimax_card_id = p_ccm.trimax_card_id','inner');
        $this->db->join('ps_master_card p_mc','p_mc.id = p_ccm.master_card_id','inner');
        $this->db->where(array('p_ct.transaction_type_code' => $trans_type_code,'p_ccm.trimax_card_id' => $trimax_card_id,'p_ccm.is_active' => $active_status,'p_ct.is_active' => $active_status,'p_ctd.is_active' => $active_status,'p_at.is_active' => $active_status,'inv.is_active' => $active_status,'p_mc.is_active' => $active_status));
        if(!empty($card_status)) {
            $this->db->where('p_ccm.card_status',$card_status);
        }
        $this->db->order_by('p_ccm.id','desc');
        $this->db->order_by('inv.id','desc');
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }
    
        public function getCustomerInfo($customer_id) {
        $this->db->select("*");
        $this->db->from('ps_customer_master');
        $this->db->where(array('id' => $customer_id, 'is_active' => '1'));
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }
    
  
    
      public function update_inventory_card($where,$update_data) {
        $this->db->where($where);
        $result = $this->db->update(INVENTORY,$update_data);
        return $result;
    }
    
    public function updateActiveStatus($where_data,$update_data) {

        $set_arr = array('is_active' => '1', 'modified_by' => $update_data['updated_by']);

        $this->db->where('id',$where_data['card_trans_id']);
        $this->db->update(CARD_TRANS,$set_arr);

        $this->db->where('id',$where_data['card_trans_details_id_1']);
        $this->db->update(CARD_TRANS_DETAILS,$set_arr);

        $this->db->where('id',$where_data['amt_trans_id']);
        $this->db->update(AMOUNT_TRANS,$set_arr);

        $this->db->where('amount_transaction_id',$where_data['amt_trans_id']);
        $this->db->update(AMOUNT_TRANS_DETAILS,$set_arr);
    }
    
    public function updateReqTxnData($update_data,$where_data) {
        $this->db->where($where_data);
        $result = $this->db->update(CARD_TRANS,$update_data);
        return $result;
    }
    
    public function getAuditData($request_id) {
        $this->db->select('status');
        $this->db->where(array('status' => REQUEST_DONE_STATUS, 'id' => $request_id));
        $this->db->from('audit_rfidservices');
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }
    
    public function updateRegActiveStatus_old($data) {

        $sql_query = "UPDATE ps_customer_master pcm INNER JOIN ps_customer_card_master pccm ON pccm.customer_id = pcm.id LEFT JOIN ps_customer_student_master pcsm ON pcsm.customer_id = pcm.id LEFT JOIN ps_image_details pid ON pid.customer_master_id = pcm.id INNER JOIN ps_card_transaction pct ON pct.trimax_card_id = pccm.trimax_card_id INNER JOIN ps_card_transaction_details pctd ON pctd.card_transaction_id = pct.id INNER JOIN ps_amount_transaction pat ON pat.card_transaction_id = pct.id INNER JOIN ps_amount_transaction_details patd ON patd.amount_transaction_id = pat.id INNER JOIN inventory inv ON inv.user_id = pcm.id INNER JOIN ps_master_card pmc ON pmc.customer_id = pcm.id SET pcm.is_active = '1',pccm.is_active = '1',pcsm.is_active = '1',pid.is_active = '1',pct.is_active = '1',pctd.is_active = '1',pat.is_active = '1',patd.is_active = '1',inv.is_active = '1',pmc.is_active = '1' WHERE pccm.trimax_card_id = '".$data['trimax_card_id']."'";
        $query = $this->db->query($sql_query);
    }
    
    
     public function updateRegActiveStatus($data) {

         $sq_arr = $this->db->query("SELECT id FROM ps_customer_card_master WHERE trimax_card_id = '".$data['trimax_card_id']."' ORDER BY id DESC LIMIT 1");
            $custCardId = $sq_arr->result_array();

            $sql_query1 = "UPDATE ps_customer_master pcm INNER JOIN ps_customer_card_master pccm ON pccm.customer_id = pcm.id LEFT JOIN ps_customer_student_master pcsm ON pcsm.customer_id = pcm.id LEFT JOIN ps_image_details pid ON pid.customer_master_id = pcm.id INNER JOIN inventory inv ON inv.user_id = pcm.id INNER JOIN ps_master_card pmc ON pmc.customer_id = pcm.id SET pcm.is_active = '1',pccm.is_active = '1',pcsm.is_active = '1',pid.is_active = '1',inv.is_active = '1',pmc.is_active = '1' WHERE pccm.id = '".$custCardId[0]['id']."'";
            $this->db->query($sql_query1);

            $sq_arr1 = $this->db->query("SELECT id FROM ps_card_transaction  WHERE trimax_card_id = '".$data['trimax_card_id']."' ORDER BY id DESC LIMIT 1");
            $card_trans_id = $sq_arr1->result_array();

            $sql_query = "UPDATE ps_card_transaction pct INNER JOIN ps_card_transaction_details pctd ON pctd.card_transaction_id = pct.id INNER JOIN ps_amount_transaction pat ON pat.card_transaction_id = pct.id INNER JOIN ps_amount_transaction_details patd ON patd.amount_transaction_id = pat.id SET pct.is_active = '1',pctd.is_active = '1',pat.is_active = '1',patd.is_active = '1' WHERE pct.id = '".$card_trans_id[0]['id']."'";
            $query = $this->db->query($sql_query);
    }
    
      public function getCustCardData($where_data) {
        $this->db->select('*');
        $this->db->where($where_data);
        $this->db->from('ps_customer_card_master');
        $this->db->order_by('id','desc');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    
   public function get_cust_data($where_data) {
        $this->db->select('ps_customer_master.date_of_birth');
        $this->db->where($where_data);
        $this->db->from('ps_customer_card_master');
        $this->db->join('ps_customer_master','ps_customer_card_master.customer_id = ps_customer_master.id');

        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    
      public function upDateLastActivityTime($session_id,$cenvertedTime){
        $this->db->set('end_tm',$cenvertedTime);
        $this->db->where('session_id',$session_id);
        $this->db->update('active_session');
        
        if($this->db->affected_rows() == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    
    
    public function check_if_already_login($user_id) 
    {
        $this->db->select('end_tm,session_id');
        $this->db->from('active_session');
 		$this->db->where(array('user_id' => $user_id));
               // $this->db->where(array('end_tm' => $user_id));
		$this->db->order_by('active_session.session_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }
    
    
    
     public function checkPassRenewalData($trimax_card_id,$trans_type_code,$passRenewalRequestId,$active_status) {
        $this->db->select(' p_ct.id as card_trans_id,
                            p_ctd.id as card_trans_details_id,
                            p_at.id as amt_trans_id,
                            p_ct.concession_cd,
                            DATE_FORMAT(p_ct.expiry_date, "%d/%m/%Y") as expiry_date
                        ');
        $this->db->from('ps_card_transaction p_ct');
        $this->db->join('ps_card_transaction_details p_ctd','p_ctd.card_transaction_id = p_ct.id','inner');
        $this->db->join('ps_amount_transaction p_at','p_at.card_transaction_id = p_ct.id','inner');
        $this->db->where(array('request_id' => $passRenewalRequestId,'p_ct.transaction_type_code' => $trans_type_code,'p_ct.trimax_card_id' => $trimax_card_id,'p_ct.is_active' => $active_status,'p_ctd.is_active' => $active_status,'p_at.is_active' => $active_status));
        $this->db->order_by('p_ct.id','desc');
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }
    
    public function getFeeType($trimax_card_id,$fee_type) {
        $this->db->select('p_atd.amount');
        $this->db->from('ps_amount_transaction p_at');
        $this->db->join('ps_amount_transaction_details p_atd','p_atd.amount_transaction_id = p_at.id','inner');
        $this->db->where(array('p_atd.amount_code' => $fee_type,
                               'p_at.trimax_card_id' => $trimax_card_id));
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }
    
      public function fetchRecieptData($trimax_card_id) {
        $this->db->select('p_ct.receipt_number,p_ct.trimax_card_id,p_ct.request_id');
        $this->db->from('ps_card_transaction p_ct');
        $this->db->where(array('p_ct.trimax_card_id' => $trimax_card_id));
		$this->db->order_by('id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
     
    }
    
     public function update_itms_request_id($id,$update_data) {
        $this->db->where(array('id' => $id));
        $result = $this->db->update('audit_rfidservices',$update_data);
        return $result;
    }
    
      public function get_userdetailsByterminal_code($terminal_code) {
        $this->db->select('r.terminal_code,u.id,u.first_name,u.last_name,u.email,u.mobile_no');
        $this->db->from('retailer_service_mapping r');
        $this->db->join('users u','u.id = r.agent_id');
        $this->db->where('r.terminal_code',$terminal_code);
        $this->db->where('r.status','Y');
        $this->db->where('u.status','Y');
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }
    
    public function insert_otp_data($data,$table_nm) {
		$this->db->insert($table_nm,$data);
		return $this->db->insert_id();
	}
        
        
       public function get_otp_data($id,$table_nm) {
        $this->db->select('id,agent_id,otp,created_at,is_expired');
        $this->db->from($table_nm);
        $this->db->where('id',$id);
        $this->db->where('is_expired','N');
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
        
       }   
      
        public function update_otp_data($id,$data,$table_nm) {
           $this->db->where(array('id' => $id));
           $result = $this->db->update($table_nm,$data);
	} 
       
      public function get_otp_data_by_otp($otp,$user_details,$table_nm) {
        $this->db->select('id,agent_id,otp,created_at,is_expired');
        $this->db->from($table_nm);
        $this->db->where('otp',$otp);
        $this->db->where('is_expired','N');
        $this->db->where('agent_id',$user_details['id']);
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
        
       }  
       
        public function get_otp_data_by_user_id($user_details,$table_nm) {
        $this->db->select('id,agent_id,otp,created_at,is_expired');
        $this->db->from($table_nm);
        $this->db->where('is_expired','N');
        $this->db->where('agent_id',$user_details['id']);
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
        
       } 
       
         public function get_terminal_id_by_userid($username) {
      //  $username = rokad_decrypt($username, $this->pos_encryption_key);
        $this->db->select('r.terminal_id');
        $this->db->from('retailer_service_mapping r');
        $this->db->join('users u','u.id = r.agent_id');
        $this->db->where('terminal_code IS NOT NULL');
        $this->db->where('u.username',$username);
        $this->db->where('r.status','Y');
        $this->db->where('u.status','Y');
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }
       

     public function getTermIdByTerminalCode($terminal_code) {
        $this->db->select('r.terminal_id');
        $this->db->from('retailer_service_mapping r');
        $this->db->join('users u','u.id = r.agent_id');
        $this->db->where(array('r.terminal_code' => $terminal_code, 'r.status' => 'Y', 'u.status' => 'Y'));
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }
    
     public function insertData($data,$tableName){
        $this->db->insert($tableName,$data);
        return $this->db->insert_id();
    }

	  
       public function getPassRenewalId($trimax_card_id,$trans_type_code,$card_status='',$active_status,$limit='1', $start='0') {
        $this->db->select(' p_ct.created_date
                            
                        ');
        $this->db->from('ps_card_transaction p_ct');
        $this->db->join('ps_card_transaction_details p_ctd','p_ctd.card_transaction_id = p_ct.id','inner');
        $this->db->join('ps_amount_transaction p_at','p_at.card_transaction_id = p_ct.id','inner');
        $this->db->where(array('p_ct.transaction_type_code' => $trans_type_code,'p_ct.trimax_card_id' => $trimax_card_id,'p_ct.is_active' => $active_status,'p_ctd.is_active' => $active_status,'p_at.is_active' => $active_status));
        $this->db->order_by('p_ct.id','desc');
        $this->db->limit($limit, $start);
      }
      
      public function getCurrentVersion($environment='') 
      {
        $this->db->select('id,version_name as deskappVersionFromAPI,updateApplicationUrl,installtionPath');
        $this->db->from('ps_version_control pvc');
        $this->db->where('pvc.is_active','1');    
        if($environment!=''){
            $this->db->where('pvc.environment',$environment);   
        }
        $query = $this->db->get();
        //echo $this->db->last_query();
        $result = $query->row_array();
        return $result;
     }

    public function CheckUserSession($request) 
    {
        $this->db->select('*');
        $this->db->from('active_session ac');
        $this->db->where(array('ac.user_id' => $request['apploginuser'],
		'ac.session_id' => $request['session_id']));
	$query = $this->db->get();
        $result = $query->row_array();
        return $result;
     }

    public function getTerminalCode($username) {
        $this->db->select('rsm.terminal_code');
        $this->db->from('users u');
        $this->db->join('retailer_service_mapping rsm','rsm.agent_id = u.id');
        $this->db->where('u.username',$username);
        $this->db->where('u.status','Y');
        $this->db->where('rsm.service_id',SMART_CARD_SERVICE);
        $this->db->where('rsm.status','Y');
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }
    
    public function checkTerminalCode($username='',$depot_cd='',$terminal='',$user_name='') {
        
        
        $this->db->select('rsm.terminal_code');
        $this->db->from('users u');
        $this->db->join('retailer_service_mapping rsm','rsm.agent_id = u.id');
        $this->db->where('u.username',$username);
        $this->db->where('rsm.service_id',SMART_CARD_SERVICE);
        $this->db->where('rsm.terminal_code',$terminal);    
        $this->db->where('rsm.status','Y');
        $this->db->where('u.status','Y');
        if($user_name!=''){
            $this->db->where('rsm.agent_id',$user_name); 
        }
        if($depot_cd!=''){
            $this->db->where('rsm.depot_code',$depot_cd);
        }

        $query = $this->db->get();
        //echo $this->db->last_query();
        $result = $query->row_array();
        return $result;
    }
    
    // added by sonali on 18-july-19 
    
    public function get_customer($where) {
        $this->db->select('`cm`.`first_name`, `cm`.`middle_name`, `cm`.`last_name`, `cm`.`mobile_number`, `cm`.`date_of_birth`, 
`img`.`image_path`, `con_m`.`concession_nm`,`name_on_card_lang`,district_id as district,city_id as city,
gender,ccm.created_date,depo.DEPOT_NM as depot_nm,ccm.trimax_card_id');
        $this->db->from('ps_customer_card_master ccm');
        $this->db->join('ps_customer_master cm', 'ccm.customer_id = cm.id', 'inner');
        $this->db->join('msrtc_replication.depots depo', 'depo.DEPOT_CD = ccm.registration_depo_cd', 'inner');
        $this->db->join('ps_image_details img', 'img.customer_master_id = cm.id', 'inner');
        $this->db->join('ps_card_transaction ct', 'ct.trimax_card_id=ccm.trimax_card_id', 'inner');
        $this->db->join('ps_concession_master con_m', 'con_m.id = ct.pass_concession_id', 'inner');
        $this->db->where(array('gender' => $where['gender'], 'ccm.is_active' => '1', 'ccm.card_status !=' => 'S', 'ct.is_active' => '1', 'cm.first_name' => $where['first_name'], 'cm.last_name' => $where['last_name'], 'cm.date_of_birth' => $where['dob'], 'ct.pass_category_id' => $where['pass_category_id']));
        $this->db->where(array('transaction_type_id' => 1));
        if (isset($where['middle_name'])) {
            $this->db->where('cm.middle_name', $where['middle_name']);
        }
//        if(isset($where['aadhar'])){
//            $this->db->where('cm.aadhar',$where['aadhar']);
//        }
        $query = $this->db->get();
//       last_query(1);
        return $query->row_array();
    }
      
}