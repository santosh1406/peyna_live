<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Payment_confirmation_model extends MY_Model {
    
    var $table  = "payment_confirmation";
    var $fields = array("id","ticket_id","return_ticket_id","wallet_topup_id","payment_confirmation_metadata_id","pg_tracking_id","amount","failed_code","failed_reason","timestamp");
    var $key    = "id";
    
    public function __construct() {
      parent::__construct();
      $this->_init();
    }
    
   
}
?>