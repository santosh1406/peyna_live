<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Passfare_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    public function getChargeableDays($spanId, $concessionId, $fromDate) {
        $queryStr = "select chargeable_days FROM ps_card_fee_master
        WHERE span_id = '" . $spanId . "'
        AND concession_id = '" . $concessionId . "'
        AND '" . $fromDate . "' BETWEEN effective_from and effective_till
        AND is_active = '1'
        AND ctrans_type_map = '".REGISTRATION_TRANS_TYPE."'";
        $query = $this->db->query($queryStr);
        $returnArr = $query->result_array();
        // show($this->db->last_query(), 1);
        return isset($returnArr[0]['chargeable_days']) ? $returnArr[0]['chargeable_days'] : '';
    }

    public function getMobileNumberByTrimaxID_old($trimaxId) {
        $queryStr = "select pcm.*, pcm.city_id as city_nm, pccm.id as cust_card_id, pccm.proof_ref_number FROM ps_customer_master pcm, ps_customer_card_master pccm
        WHERE pccm.trimax_card_id = '" . $trimaxId . "'
        AND pcm.id = pccm.customer_id
        AND pcm.is_active = '1'
        AND pccm.is_active = '1'";
        $query = $this->db->query($queryStr);
        $returnArr = $query->result_array();
        return isset($returnArr[0]) ? $returnArr[0] : '';
    }
    
    
       public function getMobileNumberByTrimaxID($trimaxId) {
        $queryStr = "select pcm.*, pcm.city_id as city_nm, pccm.id as cust_card_id, pccm.card_category_cd, pccm.proof_ref_number, pccm.wallet_proof_type, pccm.wallet_proof_ref_no FROM ps_customer_master pcm, ps_customer_card_master pccm
        WHERE pccm.trimax_card_id = '" . $trimaxId . "'
        AND pcm.id = pccm.customer_id
        ORDER BY pccm.id DESC";
        $query = $this->db->query($queryStr);
        $returnArr = $query->result_array();
        return isset($returnArr[0]) ? $returnArr[0] : '';
    }


    public function updateCustWalletProofData($cust_card_id,$data) {
        $update_data = array(
            'wallet_proof_type' => isset($data['wallet_proof_type']) ? $data['wallet_proof_type'] : '',
            'wallet_proof_type_name' => isset($data['wallet_proof_type_name']) ? $data['wallet_proof_type_name'] : '',
            'wallet_proof_ref_no' => isset($data['wallet_proof_ref_no']) ? $data['wallet_proof_ref_no'] : '',
            'modified_by' => $data['apploginuser']
        );
        $this->db->where('id', $cust_card_id);
        return $this->db->update('ps_customer_card_master', $update_data);
    }
    
    public function getCustomerByCustCardId($CustCardId,$card_status) {
        $queryStr = "select pcm.*, pcm.city_id as city_nm, p_mc.card_category_cd FROM ps_customer_master pcm, ps_customer_card_master pccm inner join ps_master_card p_mc on p_mc.customer_id = pccm.customer_id
        WHERE pccm.cust_card_id = '" . $CustCardId . "'
        AND pcm.id = pccm.customer_id
        AND pcm.is_active = '1'
        AND pccm.is_active = '1'
        AND pccm.card_status = '".$card_status."'";
        $query = $this->db->query($queryStr);
        $returnArr = $query->result_array();
        return isset($returnArr[0]) ? $returnArr[0] : '';
    }

    public function updateTWSWFlag($tw_created_status, $sw_created_status, $trimaxId, $created_by) {
        $data = array(
            'tw_created_status' => $tw_created_status,
            'sw_created_status' => $sw_created_status,
            'modified_date' => date('Y-m-d H:i:s'),
            'modified_by' => $created_by
        );
        $this->db->where(array('trimax_card_id' => $trimaxId, 'is_active' => '1'));
        return $this->db->update('ps_customer_card_master', $data);
    }

    public function getConcessionRate($concession_id, $bus_type_id, $booking_date) {
        $queryStr = "select concession_rate,is_reimbursable FROM ps_concession_service_rel
        WHERE bus_service_type_id = '" . $bus_type_id . "'
        AND concession_id = '" . $concession_id . "'
        AND '" . $booking_date . "' BETWEEN effective_from and effective_till
        AND is_active = '1'";
        $query = $this->db->query($queryStr);
        $returnArr = $query->result_array();
        return $returnArr;
    }

    public function insertTopupDetails($input, $data) {
        $returnArr['status'] = 0;
        $returnArr['msg'] = 'Success';
        $custCardId = isset($input['custCardId']) ? $input['custCardId'] : '';
        
        $transactionType = isset($input['transactionType']) ? $input['transactionType'] : '';
 
        $trimaxId = $this->getTrimaxIdByCustCardId($custCardId);
       
    // $transactionTypeId = $this->getTransactionTypeId($transactionType);
        
        $vendorId = $this->getVendorIdByAppuserId($input['apploginuser']);
     
       // $cardFeeMasterId = $this->getCardFeeMasterId(TOPUP_SPAN,$transactionTypeId);
        
        $cardTransactionArr = $this->getCardTransactionByTrimaxId($trimaxId);
        $cardTransactionDetailArr = $this->getCardTransactionDetailsByTrimaxId($trimaxId);
        
       // ctrans_type_id master table issue
       ///$cardTransactionArr['transactionTypeId'] = $transactionTypeId;
        
        $cardTransactionArr['transaction_type_code'] = $transactionType;
        
        if(empty($cardTransactionArr['trimax_card_id ']))
		  $cardTransactionArr['trimax_card_id'] = $trimaxId;
        
        $cardTransactionArr['vendor_id'] = $vendorId;
        
        //$cardTransactionArr['card_fee_master_id'] = $cardFeeMasterId;
        //$cardTransactionDetailArr['card_fee_master_id'] = $cardFeeMasterId;
        
        $cardTransactionId = $this->insertCardTransation($input, $data, $cardTransactionArr);
        $returnArr['card_id'] = $cardTransactionId;
        
        $cardTransactionDetailsId = $this->insertCardTransationDetails($input, $data, $cardTransactionDetailArr, $cardTransactionId);

        
        $amtTransactionId = $this->insertAmountTransation($input, $cardTransactionId,$trimaxId,$transactionType,$vendorId);
      
        
//        $this->load->library('Common_library');
//        $lib = new Common_library();
//        if(isset($input['tw_via_sw_flag']) && $input['tw_via_sw_flag'] == 1){
//            $total_fare_data = $lib->total_fare_calc($cardFeeMasterId, 0.00 , $input['fee_details'], $input['total_changes']);
//        }else{
//            $total_fare_data = $lib->total_fare_calc($cardFeeMasterId, $input['amount'], $input['fee_details'], $input['total_changes']);
//        }
        $total_fare_data= $input['fee_details'] ; 
       $this->insertAmountTransationDetails($input, $amtTransactionId, $total_fare_data);
      
        return $returnArr;
    }

    public function     insertCardTransation($input, $data, $cardTransactionArr) {
       
        $amt = isset($input['EffectiveAmount']) ? $input['EffectiveAmount'] : 0;
        if(isset($input['tw_via_sw_flag']) && $input['tw_via_sw_flag'] == 1) {
            $opening_balance = isset($input['total_topup_amt']) ? $input['total_topup_amt'] :0;
            $closing_balance = isset($input['updatedTwBalance']) ? $input['updatedTwBalance'] :0;
             if(!empty($input['amount'])){
                $amt = $amt + $input['amount'];
              }
        } else {
            $opening_balance = isset($input['balance']) ? $input['balance'] :0;
            $closing_balance = isset($input['EffectiveAmount']) ? ($opening_balance +$input['EffectiveAmount']) :'';
        }
        
       //  $opening_balance = isset($input['balance']) ? $input['balance'] :0;
       // $closing_balance = isset($input['EffectiveAmount']) ? ($opening_balance +$input['EffectiveAmount']) :'';
        $receipt_number = isset($input['receipt_number']) ? $input['receipt_number'] :0;
        //$CI =& get_instance();
      //  $CI->load->model('Utility_model');
        //$receipt_number = $CI->Utility_model->receipt_number('RECEIPT',$input['apploginuser']);
        if(empty($cardTransactionArr['pass_type_id']))
            $cardTransactionArr['pass_type_id'] = NULL ;
        
         if(empty($cardTransactionArr['pass_concession_id']))
            $cardTransactionArr['pass_concession_id'] = NULL ;
         
         if(empty($cardTransactionArr['pass_category_id']))
            $cardTransactionArr['pass_category_id'] = NULL ;
          
         if(empty($cardTransactionArr['transaction_type_code']))
            $cardTransactionArr['transaction_type_code'] = NULL ;
         
         if(empty($cardTransactionArr['bus_type_cd']))
            $cardTransactionArr['bus_type_cd'] = NULL ;
			
		 if(empty($cardTransactionArr['card_fee_master_id']))
            $cardTransactionArr['card_fee_master_id'] = NULL ; 
             
         if(empty($cardTransactionArr['concession_cd']))
            $cardTransactionArr['concession_cd'] = NULL ; 
          
         if(empty($cardTransactionArr['concession_nm']))
            $cardTransactionArr['concession_nm'] = NULL ; 
          
         if(empty($cardTransactionArr['kms_limit']))
            $cardTransactionArr['kms_limit'] = NULL ; 
         
         if(empty($cardTransactionArr['span_name']))
            $cardTransactionArr['span_name'] = NULL ; 
         
          if(empty($cardTransactionArr['vendor_id']))
            $cardTransactionArr['vendor_id'] = NULL ; 
			
		  if(empty($cardTransactionArr['span_period_days']))
            $cardTransactionArr['span_period_days'] = NULL ; 
        
        $data = array(
            'trimax_card_id' => $cardTransactionArr['trimax_card_id'],
            'pass_type_id' => $cardTransactionArr['pass_type_id'],
            //'pass_concession_id' => $cardTransactionArr['pass_concession_id'],
            'pass_category_id' => $cardTransactionArr['pass_category_id'],
            'transaction_type_code' => $cardTransactionArr['transaction_type_code'],
            'write_flag' => '0',
            'depot_cd' => $input['depot_cd'],
            'terminal_id' => $input['terminal_id'],
            'receipt_number' => $receipt_number,
            'dedicated_trip_id' => '0',
          //  'span_id' => $cardTransactionArr['span_id'],
            'bus_type_cd' => $cardTransactionArr['bus_type_cd'],
          //  'conc_service_rel_id' => $cardTransactionArr['conc_service_rel_id'],
          //  'card_fee_master_id' => $cardTransactionArr['card_fee_master_id'],
            'opening_balance' =>  $opening_balance,
            'closing_balance' => $closing_balance,
            'amount' => $amt,
            'online_renewal_status' => 'N',
            'session_id' => $input['session_id'],
            'vendor_id' => $cardTransactionArr['vendor_id'],
            'request_id' => isset($data['requestId']) ? $data['requestId'] : '',
            'txn_ref_no' => isset($data['txnRefNo']) ? $data['txnRefNo'] : '',
            'pos_request_id' => isset($data['posrequestId']) ? $data['posrequestId'] : '',
            'pos_txn_ref_no' => isset($data['postxnRefNo']) ? $data['postxnRefNo'] : '',
            'created_by' => $input['apploginuser'],
            'is_active' => '1',
            'concession_cd' => $cardTransactionArr['concession_cd'],
            'concession_nm' => $cardTransactionArr['concession_nm'],
            'kms_limit' => $cardTransactionArr['kms_limit'],
            'span_name' => $cardTransactionArr['span_name'],
            'span_period_days' => $cardTransactionArr['span_period_days']
        );
        $this->db->insert('ps_card_transaction',$data);
        return $this->db->insert_id();
    }
    
    public function insertCardTransationDetails($input, $data, $cardTransactionDetailArr, $card_transaction_id) {
       /* if(isset($input['tw_via_sw_flag']) && $input['tw_via_sw_flag'] == 1){
            $amt = isset($input['amount']) ? $input['amount'] : 0;
        }else{
            $amt = isset($data['amount']) ? $data['amount'] : 0;
            if(isset($input['bonus_amt']) && $input['bonus_amt'] != ''){
                $amt = ($amt + $input['bonus_amt']) - $input['total_changes'];
            }else{
                $amt = $amt - $input['total_changes'];
            }
        } */
        
       $amt = isset($input['EffectiveAmount']) ? $input['EffectiveAmount'] : 0; 
        if(isset($input['tw_via_sw_flag']) && $input['tw_via_sw_flag'] == 1) {
            $opening_balance = isset($input['total_topup_amt']) ? $input['total_topup_amt'] :0;
            $closing_balance = isset($input['updatedTwBalance']) ? $input['updatedTwBalance'] :0;
            if(!empty($input['amount'])){
            $amt = $amt + $input['amount'];
           }
        } else {
            $opening_balance = isset($input['balance']) ? $input['balance'] :0;
            $closing_balance = isset($input['EffectiveAmount']) ? ($opening_balance +$input['EffectiveAmount']) :'';
        }
        
        $cardTransData = array(
            'card_transaction_id' => $card_transaction_id,
            'trimax_card_id' => $cardTransactionDetailArr['trimax_card_id'],
          //  'card_fee_master_id' => $cardTransactionDetailArr['card_fee_master_id'],
            'route_no' => $cardTransactionDetailArr['route_no'],
            'route_type' => $cardTransactionDetailArr['route_type'],
            'fare_type_cd' => $cardTransactionDetailArr['fare_type_cd'],
            'service_number' => '0',
            'from_stop_code' => $cardTransactionDetailArr['from_stop_code'],
            'till_stop_code' => $cardTransactionDetailArr['till_stop_code'],
            'via_stop_code' => $cardTransactionDetailArr['via_stop_code'],
            'stages' => $cardTransactionDetailArr['stages'],
            'opening_balance' => $opening_balance,
            'closing_balance' => $closing_balance,
            'least_km' => $cardTransactionDetailArr['least_km'],
            'rounded_km' => $cardTransactionDetailArr['rounded_km'],
            'amount' => $amt,
            'session_id' => $input['session_id'],
            'created_by' => $input['apploginuser']
        );
        $this->db->insert('ps_card_transaction_details',$cardTransData);
        return $this->db->insert_id();
    }
    
    public function insertAmountTransation($input, $card_transaction_id,$trimaxId, $transaction_type_code,$vendorId) {
        if(isset($input['tw_via_sw_flag']) && $input['tw_via_sw_flag'] == 1){
            $amount = 0.00;
            $topup_amount = $input['amount'];
        }else{
             $amount = $input['amount'];
             $topup_amount = $input['amount'];
            $fee_arr = array_column($input['fee_details'],'fee_type_nm');
            if(in_array(BCCHARGES_KEY,$fee_arr)) {
                $key = array_search(BCCHARGES_KEY,$fee_arr);
                $amount = $amount + $input['fee_details'][$key]['fee_type_value'];
            }
        }
        $amitTransData = array(
            'card_transaction_id' => $card_transaction_id,
            'trimax_card_id' => $trimaxId,
            'total_amount' => $amount,
            'topup_amount' => $topup_amount,
            'tw_via_sw_total_amount' => $input['amount'],
            'transaction_type_code' => $transaction_type_code,
            'depot_cd' => $input['depot_cd'],
            'terminal_id' => $input['terminal_id'],
            'session_id' => $input['session_id'],
            'vendor_id' => $vendorId,
            'summary_no' => '',
            'summary_status' => '',
            'created_by' => $input['apploginuser']
        );
        $this->db->insert('ps_amount_transaction',$amitTransData);
        return $this->db->insert_id();
    }
    
    public function insertAmountTransationDetails($input, $amtTransactionId, $total_fare_data) {
        
        foreach ($total_fare_data as $key => $value) {
            $amt_trans_details_data[] = array(
                'amount_transaction_id' => $amtTransactionId,
                'amount_code' => $value['fee_type_cd'],
                'amount' => $value['fee_type_value'],
                'operand' => $value['operand'],
                'created_by' => $input['apploginuser']
            );
        }
        $this->db->insert_batch('ps_amount_transaction_details',$amt_trans_details_data);
        return $this->db->insert_id();
    }

    public function getTrimaxIdByCustCardId($custCardId) {
        $queryStr = "select trimax_card_id FROM ps_customer_card_master
        WHERE cust_card_id = '" . $custCardId . "'
        AND is_active = '1' and trimax_card_id is not null ";
        $query = $this->db->query($queryStr);
        $returnArr = $query->result_array();
        return isset($returnArr[0]) ? $returnArr[0]['trimax_card_id'] : '';
    }

    public function getTransactionTypeId($transactionType) {
        $queryStr = "select ctrans_type_id FROM ps_card_trans_type
        WHERE ctrans_type_code = '" . $transactionType . "'
        AND is_active = '1'";
        $query = $this->db->query($queryStr);
        $returnArr = $query->result_array();
        return isset($returnArr[0]) ? $returnArr[0]['ctrans_type_id'] : '';
    }

    public function getCardTransactionByTrimaxId($trimaxId) {
        $queryStr = "select * FROM ps_card_transaction
        WHERE trimax_card_id = '" . $trimaxId . "'
        AND write_flag = '1'
        AND is_active = '1'
        ORDER BY id DESC LIMIT 1";
        $query = $this->db->query($queryStr);
        $returnArr = $query->result_array();
        return isset($returnArr[0]) ? $returnArr[0] : '';
    }
    
    public function getCardTransactionDetailsByTrimaxId($trimaxId) {
        $queryStr = "select * FROM ps_card_transaction_details
        WHERE trimax_card_id = '" . $trimaxId . "'
        AND is_active = '1'
        ORDER BY id DESC LIMIT 1";
        $query = $this->db->query($queryStr);
        $returnArr = $query->result_array();
        return isset($returnArr[0]) ? $returnArr[0] : '';
    }
    
    
    public function getVendorIdByAppuserId($app_user_id) {
        $this->db->select('vendor_id');
        $this->db->from('users');
        $this->db->where(array('id' => $app_user_id));
        $query = $this->db->get();
    	$returnArr = $query->result_array();
        return isset($returnArr[0]['vendor_id']) ? $returnArr[0]['vendor_id'] : '';
    }
    
    public function getCustCardIdByTrimaxID($trimaxId) {
        $this->db->select('cust_card_id');
        $this->db->from('ps_customer_card_master');
        $this->db->where(array('trimax_card_id' => $trimaxId, 'is_active' => '1'));
        $query = $this->db->get();
    	$returnArr = $query->result_array();
        return isset($returnArr[0]['cust_card_id']) ? $returnArr[0]['cust_card_id'] : '';
    }
    
    public function getCardFeeMasterId($span, $transactionTypeId) {
        $queryStr = "select id FROM ps_card_fee_master
        WHERE ctrans_type_id = '" . $transactionTypeId . "'
        AND span_id = '" . $span . "'
        AND is_active = '1'";
        $query = $this->db->query($queryStr);
        $returnArr = $query->result_array();
        return isset($returnArr[0]) ? $returnArr[0]['id'] : '';
    }
    
    public function insertTopupData($data,$input) {
        $data = array(
            'apploginuser' => $input['apploginuser'],
            'session_id' => $input['session_id'],
            'version_id' => $input['version_id'],
            'txnType' => $data['txnType'],
//            'mobileNumber' => isset($input['mobileNumber']) ? $input['mobileNumber'] :'',
            'custCardId' => $input['custCardId'],
            'amount' => $data['amount'],
            'total_changes' => $input['total_changes'],
            'bonus_amt' => isset($input['bonus_amt']) ? $input['bonus_amt'] :'',
            'fee_details' => json_encode($input['fee_details']),
            'requestId' => $data['requestId'],
            'initiatorId' => $data['initiatorId'],
            'initiatorType' => $data['initiatorType'],
            'created_by' => $input['apploginuser'],
            'created_date' => date('Y-m-d H:i:s'),
            'is_active' => '1'
        );
        $this->db->insert('ps_card_topup_request',$data);
        return $this->db->insert_id();
    }

    public function updateTopupData($insertId,$result) {
        $data = array(
                'responseCode' => $result['responseCode'],
                'responseMessage' => $result['responseMessage'],
                'response_requestId' => $result['requestId'],
                'response_amount' => isset($result['amount']) ? $result['amount'] :"",
                'response_custCardID' => isset($result['custCardID']) ? $result['custCardID'] :"",
                'walletType' => isset($result['walletType']) ? $result['walletType'] :'',
                'twExpiry' => isset($result['twExpiry']) ? $result['twExpiry'] :'',
                'swExpiry' => isset($result['swExpiry']) ? $result['swExpiry'] :'',
                'txnRefNo' => isset($result['txnRefNo']) ? $result['txnRefNo'] :'',
                'modified_date' => date('Y-m-d H:i:s')
        );
        $this->db->where('id',$insertId);
        return $this->db->update('ps_card_topup_request',$data);
    }
    
    public function getTerminalCodeBytSessionIdOld($sessionId) {
       $this->db->select('t.terminal_code');
       $this->db->from('active_session a');
       $this->db->join('app_terminals t','a.terminal_id = t.terminal_id','inner');
       $this->db->where('a.session_id',$sessionId);
       $this->db->where('t.is_active','1');
       $query = $this->db->get();
       $returnArr = $query->result_array();
       return isset($returnArr[0]['terminal_code']) ? $returnArr[0]['terminal_code'] : ''; 
    }

    public function getTerminalCodeBytSessionId($sessionId) {
       $this->db->select('rsm.terminal_code');
       $this->db->from('active_session a');
       //$this->db->join('app_terminals t','a.terminal_id = t.terminal_id','inner');
       $this->db->join('retailer_service_mapping rsm','a.terminal_id = rsm.terminal_id','inner');
       $this->db->where('a.session_id',$sessionId);
       $this->db->where('rsm.status','Y');
       $query = $this->db->get();
       $returnArr = $query->result_array();
       return isset($returnArr[0]['terminal_code']) ? $returnArr[0]['terminal_code'] : ''; 
    }
    
    
    public function getTrimaxIdBytCustCardId($trimax_card_id) 
    {
        $this->db->select('id,is_whitelisted,trimax_card_id,card_category_cd');
        $this->db->from('ps_master_card');
        $this->db->where(array('cust_card_id' => $trimax_card_id, 'is_active' => '1'));
        $query = $this->db->get();
        $returnArr = $query->result_array();
        return $returnArr;
    }

   /* public function getTrimaxIdByCustCardId($custCardId) {
        $queryStr = "select trimax_card_id FROM ps_customer_card_master
        WHERE cust_card_id = '" . $custCardId . "'
        AND is_active = '1'";
        $query = $this->db->query($queryStr);
        $returnArr = $query->result_array();
        return isset($returnArr[0]) ? $returnArr[0]['trimax_card_id'] : '';
    } */
    
    public function get_fino_prooftypes($id) {
        $this->db->select('id,proof_order');
        $this->db->from('ps_proof_master');
        $this->db->where(array('application_type' => APPLICATION_TYPE_FINO, 'id' => $id,'is_active' => '1'));
        $query = $this->db->get();
        return $query->row_array();
    }
    
    public function getPassDetailsByTrimaxID($trimaxId) {
        $this->db->select("at.total_amount,  DATE_FORMAT(ct.expiry_date, '%d/%m/%Y') as expiry_date,at.id");
        $this->db->from('ps_customer_card_master ccm');
        $this->db->join('ps_card_transaction ct', 'ct.trimax_card_id = ccm.trimax_card_id', 'inner');
       # $this->db->join('ps_rfidmaster_span p_span', 'ct.span_id = p_span.id', 'inner');
        $this->db->join('ps_card_transaction_details ctd', 'ctd.card_transaction_id = ct.id', 'inner');
        $this->db->join('ps_amount_transaction at', 'ct.id = at.card_transaction_id', 'inner');
       # $this->db->join('ps_concession_master p_conm', 'ct.pass_concession_id = p_conm.id', 'inner');
        $this->db->where('ccm.trimax_card_id', $trimaxId);
//       $this->db->where("(`ct`.`transaction_type_id`='".REG_PPC_TRANS_TYPE_ID."' OR `ct`.`transaction_type_id`='".REG_OTC_TRANS_TYPE_ID."')"); // RG
        
        $this->db->where("(`ct`.`transaction_type_code`='".REG_PPC_TRANS_TYPE_CODE."' OR `ct`.`transaction_type_code`='".REG_OTC_TRANS_TYPE_CODE."')"); 
        
      //  $this->db->where('ccm.is_active', '1');
        $query = $this->db->get();
        $returnArr = $query->result_array();
        return isset($returnArr[0]) ? $returnArr[0] : '';
    }

    public function getTransAmtDetailsByAmtTransId($amtTransactionId) {
        $this->db->select('ps_atd.amount_code, ps_atd.amount, p_ftm.fee_type_nm');
        $this->db->from('ps_amount_transaction_details ps_atd');
        $this->db->join('ps_fee_type_master p_ftm', 'p_ftm.fee_type_cd = ps_atd.amount_code', 'inner');
        $this->db->where('ps_atd.amount_transaction_id', $amtTransactionId);
        $this->db->where('ps_atd.is_active', '1');
        $query = $this->db->get();
        $returnArr = $query->result_array();
//        show($this->db->last_query(), 1); 
        return isset($returnArr[0]) ? $returnArr : '';
    }
    
    public function getChargeListByAmtTransId($amtTransactionId) {
        $queryStr = "select ps_atd.amount_code, ps_atd.amount 
                    from ps_amount_transaction_details ps_atd
                    where ps_atd.amount_transaction_id = '".$amtTransactionId."'
                    AND ps_atd.amount_code != 'BF'
                    AND ps_atd.amount_code 
                    NOT IN (select ps_ftm.fee_type_cd from ps_fee_type_master ps_ftm where ps_ftm.  fee_type_cd!= 'GST')";
        $query = $this->db->query($queryStr);
        $returnArr = $query->result_array();
        return isset($returnArr[0]) ? $returnArr : '';
    }
    
    public function insertIntoMasterCard($custCardId, $trimaxId,$cardType, $user_name) {
        $data = array(
            'trimax_card_id' => $trimaxId,
            'cust_card_id' => $custCardId,
            'display_card_id' => $trimaxId,
            'card_status' => 'A',
            'card_category_cd' => $cardType,
            'created_by' => $user_name,
            'created_date' => date('Y-m-d H:i:s'),
            'is_active' => '1'
        );
        $this->db->insert('ps_master_card',$data);
        return $this->db->insert_id();
    }

    public function get_mob_dob_data($trimax_card_id ) {

        $this->db->select('p_cm.date_of_birth,p_cm.mobile_number');
        $this->db->from('ps_customer_card_master p_ccm');
        $this->db->join('ps_customer_master p_cm', 'p_cm.id = p_ccm.customer_id', 'inner');
        $this->db->where(array('p_ccm.trimax_card_id' => $trimax_card_id, 'p_ccm.is_active' => '1'));
        $query = $this->db->get();
        $returnArr = $query->result_array();
        return $returnArr;
    }
    
        public function getCardCustData($cust_card_id) {

        $this->db->select('p_ccm.*');
        $this->db->from('ps_customer_card_master p_ccm');
        $this->db->where(array('cust_card_id' => $cust_card_id, 'p_ccm.is_active' => '1'));
        $this->db->where_in('card_status',array(BLOCK_STATUS,SURRENDER_STATUS,LOSS_STATUS,FAULTY_STATUS));
        $query = $this->db->get();
        $returnArr = $query->result_array();
        return $returnArr;
    }
    
      public function getCardDataByTid($trimax_card_id) {
        $this->db->select('id,is_whitelisted,trimax_card_id,card_category_cd');
        $this->db->from('ps_master_card');
        $this->db->where(array('trimax_card_id' => $trimax_card_id, 'is_active' => '1', 'card_status' => INITIATED_STATUS));
        $query = $this->db->get();
    	$returnArr = $query->result_array();
        return $returnArr;
    }

    public function getCardDataByCid($custCardId) {
        $this->db->select('id,is_whitelisted,trimax_card_id,card_category_cd');
        $this->db->from('ps_master_card');
        $this->db->where(array('cust_card_id' => $custCardId, 'is_active' => '1'));
        $query = $this->db->get();
        $returnArr = $query->result_array();
        return $returnArr;
    }
    
        public function chkCardReissue($trimax_card_id) {
        $this->db->select('p_ccm.*');
        $this->db->from('ps_customer_card_master p_ccm');
        $this->db->where(array('trimax_card_id' => $trimax_card_id, 'p_ccm.is_active' => '1'));
        $this->db->where_in('card_status',array(INITIATED_STATUS,WRITING_STATUS,DISPATCH_STATUS));
        $query = $this->db->get();
        $returnArr = $query->result_array();
        return $returnArr;
    }
    
     public function insertTransInquiryData($data) {

        $insert_data = array(
                            'process_type' => $data['process_type'],
                            'user_id' => $data['apploginuser'],
                            'reference_id' => isset($data['referenceId']) ? $data['referenceId'] : '',
                            'card_id' => isset($data['card_id']) ? $data['card_id'] : '',
                            'request_id' => isset($data['request_id']) ? $data['request_id'] : '',
                            'created_by' => $data['apploginuser']
                        );
        $this->db->insert('ps_transaction_inquiry',$insert_data);
        return $this->db->insert_id();
    }
    
    public function getTransactionId($whereCardData,$wheredesc){
       
        $this->db->select('id');
        $this->db->from('ps_card_transaction');
        $this->db->order_by('id',$wheredesc['order']);
        $this->db->where($whereCardData);
        //$this->db->where(array('is_active' => '1'));
        $this->db->limit(1,0);
        $query= $this->db->get();
        //last_query(1);
        if ( $query->num_rows() > 0 )
        {
            return $query->result_array(); 

        }
    }
    
    public function getWalletTransNo($where){
       $this->db->select('transaction_no');
        $this->db->from('wallet_trans');
        $this->db->order_by("id", "desc");
        $this->db->where($where);
        $this->db->limit(1,0);
        $query= $this->db->get();
        //last_query(1);
        if ( $query->num_rows() > 0 )
        {
            return $query->result_array(); 

        } 
    }
    
     public function insertAllData($postVal,$newArr){

     
        $terminal_id = $this->Mainmodel->getTermIdByTerminalCode($postVal['terminal_code'])['terminal_id']; 

        $postVal['terminal_id'] = $terminal_id;
     
        $last_cdata = $this->getLastinsertData($postVal['trimax_card_id']);

        //insert into card transaction
        $ct_id = $this->insertdatatoCardTrans($last_cdata,$postVal);

        //insert into card transaction details
        $this->insertdatatoCardTransDetails($ct_id,$postVal);

        //insert into amount Transaction 
        $at_id = $this->insertintoAmtTrans($ct_id,$postVal);

        //insert into amount Transaction Details
        $this->insertintoAmtTransdetails($at_id,$postVal,$newArr);
    }
    
      public function getLastinsertData($trimax_card_id){
        $this->db->select('*');
        $this->db->where('trimax_card_id',$trimax_card_id);
        $this->db->from('ps_card_transaction');
        $this->db->order_by('id','desc');
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }
    
    public function insertdatatoCardTrans($last_cdata,$postVal){
        $ct_data = array(
            'trimax_card_id'=>$postVal['trimax_card_id'],
            'pass_type_id'=> $last_cdata['pass_type_id'],
            'pass_concession_id'=>$last_cdata['pass_concession_id'],
            'pass_category_id'=>$last_cdata['pass_category_id'],
            'activation_date'=>$last_cdata['activation_date'],
            'expiry_date'=>$last_cdata['expiry_date'],
            'transaction_type_code'=>$postVal['transaction_type_code'],
            'write_flag'=> 0,
            'depot_cd'=>$postVal['depot_cd'],
            'terminal_id'=>$postVal['terminal_id'], 
            'span_name'=>$postVal['span_name'],
            'session_id'=>$postVal['session_id'],
            'vendor_id'=> isset($postVal['vendor_id']) ? $postVal['vendor_id'] : '',
            'request_id' => isset($postVal['request_id']) ? $postVal['request_id'] : '',
            'txn_ref_no' => isset($postVal['txn_ref_no']) ? $postVal['txn_ref_no'] : '',
            'pos_request_id' => isset($postVal['pos_request_id']) ? $postVal['pos_request_id'] : '',
            'pos_txn_ref_no' => isset($postVal['postxnRefNo']) ? $postVal['postxnRefNo'] : '',
            'created_by'=>$postVal['apploginuser']
        );
        //show($ct_data,1);
        $ct_id = $this->Mainmodel->insertData($ct_data,CARD_TRANS);
        return $ct_id;
    }
    
     public function insertdatatoCardTransDetails($ct_id,$postVal){
        $ctd_data = array(
            'card_transaction_id' => $ct_id,
            'trimax_card_id' => $postVal['trimax_card_id'],
           // 'card_fee_master_id'=> $postVal['cardFeeMasterId'],
            'session_id'=> $postVal['session_id'],
            'created_by'=> $postVal['apploginuser']
        );
        $this->Mainmodel->insertData($ctd_data,CARD_TRANS_DETAILS);
    }
    
    public function insertintoAmtTrans($ct_id,$postVal){
        $at_data = array(
            'card_transaction_id' =>$ct_id,
            'trimax_card_id'=> $postVal['trimax_card_id'],
            'total_amount'=> $postVal['TotalCharge'],
            'transaction_type_code'=> $postVal['transaction_type_code'],
            'depot_cd' => $postVal['depot_cd'],
            'terminal_id' => $postVal['terminal_id'],
            'session_id'=> $postVal['session_id'],
            'vendor_id'=> isset($postVal['vendor_id']) ? $postVal['vendor_id'] : '',
            'created_by'=> $postVal['apploginuser'],
        );
        $at_id = $this->Mainmodel->insertData($at_data,AMOUNT_TRANS);
        return $at_id;
    }

    public function insertintoAmtTransdetails($at_id,$postVal,$newArr){
        foreach ($newArr as $key => $value) {
                $atd_data = array(
                    'amount_transaction_id' => $at_id,
                    'amount_code' => $value['fee_type_cd'],
                    'amount' => $value['fee_type_value'],
                    'operand' => $value['operand'],
                    'created_by' => $postVal['apploginuser']
                );
                $this->Mainmodel->insertData($atd_data,AMOUNT_TRANS_DETAILS);
        }   
    }
    
    
   	public function getPassRenewalRequestId($whereCardData){
       
        $this->db->select('pass_renewal_request_id');
        $this->db->from('ps_card_transaction');
        $this->db->order_by('id','desc');
        $this->db->where($whereCardData);
        $this->db->limit(1,0);
        $query= $this->db->get();
        //last_query(1);
        if ( $query->num_rows() > 0 )
        {
            return $query->result_array(); 
        }
    }
    

}
