<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bus_type_convey_charge_master_model extends MY_Model {

    var $table  = 'bus_type_convey_charge_master';
    var $fields = array("id","op_id","bus_type_id","op_bus_type_id","convey_charged_type_id","convey_charge_cal_type","minimum_amt_limit","greater_than_min_amt_limit_value","less_than_min_amt_limit_value","is_status","is_deleted","created_by","created_date","updated_by","updated_date");
            
    var $key    = 'id'; 
    
    
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

     function get_convey_charge_data($data_array) {
        
        $this->db->select("bs.id,bs.bus_type_id,bs.op_bus_type_id,bs.convey_charged_type_id,bs.convey_charge_cal_type,bs.minimum_amt_limit,bs.greater_than_min_amt_limit_value,bs.less_than_min_amt_limit_value,bs.is_status,bs.is_deleted,bs.created_by,bs.created_date,bs.updated_by,bs.updated_date,op.id,op.op_bus_type_name");
        $this->db->where('bs.id', $data_array['id']);
         $this->db->join("op_bus_types op", "op.id=bs.op_bus_type_id", "inner");
        $this->db->from("bus_type_convey_charge_master bs");
        $this->db->where('bs.is_status', '1');
        $this->db->where('bs.is_deleted','0');
        $query = $this->db->get();

        return $query->result_array();
        
    }
    
    function get_bus_service_detail($data) {
        
        $this->db->select("cdm.cycle_days_id,cdm.cycle_days_name,bs.*,r.*");
        $this->db->from("bus_services bs");
        $this->db->join("routes r", "r.route_no=bs.route_no", "inner");
        $this->db->join("cycle_days_master cdm", "cdm.cycle_days_id=bs.schedule_cycle_days", "inner");
        $this->db->where("r.record_status", 'Y');
        $this->db->where("bs.is_status", 'Y');
        $this->db->where("bs.is_deleted",'N');
        $this->db->where("bus_service_no", $data);
        $query = $this->db->get();

        return $query->result_array();
    }
    function update_bus_service($input){
         
        $update = array();
        $mast_update_data = array(
        'is_status' => 'N',
        'is_deleted'=>'Y');

        $this->db->where('bs.bus_service_no',$input['bus_service_no']);
        $this->db->where('bs.route_no',$input['route_no']);
        $this->db->where("bs.is_status", 'Y');
        $this->db->where("bs.is_deleted",'N');
        if($this->db->update('bus_services bs', $mast_update_data)){
            return true;
        } else {
            return false;
        }
    }
    
    public function bus_service_route_info($route_no) {
        
        $this->db->distinct();
        $this->db->select("bss.stop_seq as seq_no,bss.is_halt,bss.id,bss.arrival_tm,bss.is_boarding_stop,bss.departure_tm,bss.arrival_day,bss.is_alighting,bss.km,bss.route_no,bss.bus_stop_name,r.from_stop,r.end_stop");
        $this->db->from("bus_service_stops bss");
        $this->db->join("routes r",'r.route_no=bss.route_no', "inner");
        $this->db->where("bss.bus_service_no", $route_no['old_service_id']);
        $this->db->order_by("bss.stop_seq", "asc"); 
        $query = $this->db->get();
   
        return $query->result_array();
    }
    
     function delete_bus_service($input){
         
        $update = array();
        $mast_update_data = array(
        'is_status' => 'N',
        'is_deleted'=>'Y');

        $this->db->where('bs.bus_service_no',$input['service_id']);
     
        $this->db->where("bs.is_status", 'Y');
        $this->db->where("bs.is_deleted",'N');
        if($this->db->update('bus_services bs', $mast_update_data)){
            $input['old_service_id']=$input['service_id'];
            $this->update_bus_stops($input);
             return true;
        } else {
            return false;
        }
    }
    
    function update_bus_stops($input){
      $update = array();
      $bus_stops_update_data = array(
        'is_status' => 'N',
        'is_deleted'=>'Y');

        $this->db->where('bss.bus_service_no',$input['old_service_id']);

        $this->db->where("bss.is_status", 'Y');
        $this->db->where("bss.is_deleted",'N');
        if($this->db->update('bus_service_stops bss', $bus_stops_update_data)){
           
            return true;
        } else {
            return false;
        }
    }
    
    public function bus_service_info($bus_service_no) {
        
        $this->db->distinct();
        $this->db->select("bs.*");
        $this->db->from("bus_services bs");

        $this->db->where("bs.bus_service_no", $bus_service_no);
        
        $query = $this->db->get();
   
        return $query->result_array();
    }
}
