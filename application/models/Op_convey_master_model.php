<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Op_convey_master_model extends MY_Model {

    /**
     * Instanciar o CI
     */
    var $table = 'op_convey_master';
    var $fields = array("op_conv_id", "operator_id", "op_min_amt_limt", "op_conv_type", "from_date", "till_date", "is_status", "is_deleted", "created_by",
        "created_date", "updated_by", "updated_date");
    var $key = 'op_conv_id';

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    public function op_convey_byid($op_conv_id) {
        $this->db->select("op_conv_id,operator_id,op_conv_type,op_min_amt_limt,"
                . " DATE_FORMAT(from_date, '%d-%m-%Y') AS from_date,"
                . " DATE_FORMAT(till_date, '%d-%m-%Y') AS till_date", false);
        $this->db->from("op_convey_master");
        $this->db->where("op_conv_id", $op_conv_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function existing_op($existing_op) {
        $this->db->select('op_conv_id');
        $this->db->from('op_convey_master');
        $this->db->where("operator_id", $existing_op);
        $this->db->where("is_status", "1");
        $this->db->where("is_deleted", "0");
        $query = $this->db->get();
        $data = $query->row();
        $cnt = count($data);
        //show($this->db->last_query(), 1);
        return $cnt;
    }

    public function update_opconvey_master($input) {
        $exits = array_key_exists('deleted_flag', $input);
        if ($exits != '') {
            if ($input['deleted_flag'] == '1') {
                $delete = '1';
                $active = "0";
            }
        } else {
            $delete = '0';
            $active = "1";
        }
        $user_id = $this->session->userdata('user_id');

        $from_date = explode("-", $input['txtfromdate']);
        $from_date = mktime(0, 0, 0, $from_date[1], $from_date[0], $from_date[2]);
        $from_date = date("Y-m-d", $from_date);

        $to_date = explode("-", $input['txttilldate']);
        $to_date = mktime(0, 0, 0, $to_date[1], $to_date[0], $to_date[2]);
        $to_date = date("Y-m-d", $to_date);

        $user_id = $this->session->userdata('user_id');
        $data = array("operator_id" => $input['lst_op_id_chosen'], "op_min_amt_limt" => $input['txt_min_amt_limit'],
            "op_conv_type" => $input['radtype'], "from_date" => $from_date
            , "till_date" => $to_date,
            "updated_by" => $user_id,
            "is_deleted" => $delete, "is_status" => $active);
        $this->db->where('op_conv_id', $input['hiddn_op_conv_id']);
        $this->db->update('op_convey_master', $data);
        $data_status['status'] = "@#success@";
        return $data_status;
    }

    public function delete_opconvey_master($input) {
        $data = array("is_status" => '0', "is_deleted" => '1');
        $this->db->where('op_conv_id', $input['id']);
        $this->db->update('op_convey_master', $data);
        $cnt = $this->db->affected_rows();
        if ($cnt != 0) {
            $data["flag"] = '@#success#@';
        } else {

            $data["flag"] = '@#failed#@';
        }
        return $data;
    }

}
