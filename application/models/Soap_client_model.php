<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Soap_client_Model extends CI_Model {

    function __construct() {
        parent::__construct();
        
        $model_array = array(
                        'common_model',
                        'trip_master_model',
                        'bus_type_model',
                        'bus_stop_master_model',
                        'op_bus_types_model',
                        'op_seat_layout_model',
                        'op_layout_details_model',
                        'travel_destinations_model',
                        'trip_boarding_alighting_model',
                        'op_stop_master_model',
                        'boarding_stop_model',
                    );

        $this->load->model($model_array);
    }

    /*
     * @function    : insert_bus_type
     * @Desc        : insert the operator bus tuype in db
     * @table       : op_bus_types
     * 
     */

    function insert_bus_type($data, $op_id) {
//        $this->common_model->truncate_table(array('op_bus_types'));
        if (isset($data['return']) && count($data['return']) > 0) {
            $data = $data['return']['allBusType'];

            foreach ($data as $key => $value) {
                $this->op_bus_types_model->op_id = $op_id;
                $this->op_bus_types_model->op_bus_type_name = $value['busName'];
                $count = $this->op_bus_types_model->get_type_count();

                if ($count == 0) {
                    $this->op_bus_types_model->op_bus_type_id = $this->generateid_model->getRequiredId('op_bus_types');
                    $this->op_bus_types_model->op_id = $op_id;
                    $this->op_bus_types_model->op_bus_type_name = $value['busName'];
                    $this->op_bus_types_model->op_bus_type_cd = $value['busType'];


                    $this->op_bus_types_model->save();
                    $this->op_bus_types_model->reset();
                }
            }
        }
    }

    function insert_available_services($data, $op_id, $controller) {
        // $this->common_model->truncate_table(array('generateid','op_layout_details', 'op_seat_layout', 'trip_master','op_bus_types'));
        
        if (isset($data['AvailableServiceResponse']) && count($data['AvailableServiceResponse']) > 0 && isset($data['AvailableServiceResponse']['availableServices'])) {
            $services = $data['AvailableServiceResponse']['availableServices']['service'];

            if (!is_array($services)) {
                $services = array($services);
            }

            if(count($services) >0)
            {
                foreach ($services as $key => $value) {
                    
                    // if($key == 100)
                    //     break;

                    // show($key,'', $key);

                    // if($value['serviceId'] != '598')
                    //     continue;

                    $this->trip_master_model->op_id = $op_id;
                    $this->trip_master_model->op_trip_no = $value['serviceId'];

                    $count = $this->trip_master_model->get_service_count();

                    if ($count == 0) {
                        $op_bus_type_detail = $this->op_bus_types_model->get_detail_category($value['busType'], $op_id);
                       
                        $this->op_seat_layout_model->op_id = $op_id;
                        $this->op_seat_layout_model->layout_name = $value['busType'];

                        $gabf_detail = $this->op_seat_layout_model->select();

                        // show($op_bus_type_detail, '', 'bud_detail');

                        $op_bus_type_id = "";
                        $bus_type_id    = "";

                        if (count($op_bus_type_detail) == 0) {
                            $controller->GetAllBusTypes();

                            $op_bus_type_detail = $this->op_bus_types_model->get_detail_category($value['busType'], $op_id);
                        }

                        if(count($op_bus_type_detail) == 0)
                        {
                            show("Bus Cat Not  Found ".$value['busType']);
                            continue;
                        }
                        else
                        {
                            $op_bus_type_id = $op_bus_type_detail->op_bus_type_id;                            
                            $bus_type_id    = $op_bus_type_detail->bus_type_id;                            
                        }


                        $seat_layout_id = '';

                        if (count($gabf_detail) == 0) {
                            
                            $seat_layout_id = $controller->getAllBusFormats($value['busFormatCd']);

                            // show($seat_layout_id,'', '$seat_layout_id');
                        }
                        
                        if(trim($seat_layout_id) != '')
                        {                        
                            $trip_no           = $this->generateid_model->getRequiredId('trip_master');

                            $this->trip_master_model->trip_no               = $trip_no;
                            $this->trip_master_model->op_trip_no            = $value['serviceId'];
                            $this->trip_master_model->op_id                 = $op_id;
                            // $this->trip_master_model->route_no                  = $value['routeName'];
                            $this->trip_master_model->bus_type_id           = $bus_type_id;
                            $this->trip_master_model->op_bus_type_id        = $op_bus_type_id;
                            $this->trip_master_model->seat_layout_id        = $seat_layout_id;
                            $this->trip_master_model->route_no_name         = $value['routeName'];
                            $this->trip_master_model->refund_permitted      = 'Y';
                            $this->trip_master_model->fare_type             = 'C';
                            $this->trip_master_model->sch_dept_time         = $value['departureTime'];

                            $this->trip_master_model->save();

                            $controller->getBusServiceStops($value['serviceId']);
                        }
                        else{
                            show($value,'','Seat _id Not found');
                        }
                    }
                    else
                    {
                        $controller->getBusServiceStops($value['serviceId']);
                    }

                    if(($key % 300) == 0)
                    {
                        // sleep(1);
                        $this->db->reconnect();
                    }   
                }
            }
            else
            {
                echo 'empty array';
            }
        }
        else
        {
           echo show($data,1); 
        }
    }

    function insert_bus_format($response, $op_id, $bus_format_cd) {

        if (isset($response['AllBusFormatResponse']['busDetail'])) {
            $get_all_bus_format_api_data = $response['AllBusFormatResponse']['busDetail'];
            
           // show($get_all_bus_format_api_data,'','@model : insert_bus_format');
            
            if (count($get_all_bus_format_api_data) > 0) {
                
                $this->op_seat_layout_model->op_id = $op_id;
                $this->op_seat_layout_model->layout_name = $bus_format_cd;
                
                $data = $this->op_seat_layout_model->select();
                $this->op_seat_layout_model->reset();
                
               // show($data,1);
                
                if(count($data) > 0)
                {
                    $seat_layout_id = $data[0]->seat_layout_id;
                }
                else
                {
                    $seat_layout_id = $this->generateid_model->getRequiredId('op_seat_layout');
    
                    $this->op_seat_layout_model->seat_layout_id   = $seat_layout_id;
                    $this->op_seat_layout_model->op_id = $op_id;
                    $this->op_seat_layout_model->layout_name = $bus_format_cd;
                    $this->op_seat_layout_model->no_rows = $get_all_bus_format_api_data['rowCount'];
                    $this->op_seat_layout_model->no_cols = $get_all_bus_format_api_data['columnCount'];
                    $this->op_seat_layout_model->no_berth = 1;
                    $op_slm_id = $this->op_seat_layout_model->save();
                    

                    // show($get_all_bus_format_api_data['seatLayout']['seat'],'','seat');

                    if (count($get_all_bus_format_api_data['seatLayout']['seat']) > 0) {
                        foreach ($get_all_bus_format_api_data['seatLayout']['seat'] as $key => $value) {

                            $this->op_layout_details_model->op_seat_layout_id   = $seat_layout_id;
                            $this->op_layout_details_model->row_no              = $value['xCoordinate'];
                            $this->op_layout_details_model->col_no              = $value['yCoordinate'];
                            $this->op_layout_details_model->berth_no            = 1;
                            $this->op_layout_details_model->seat_no             = $value['seatNumber'];
                            $this->op_layout_details_model->seat_type           = $value['seatType']; 

                            $op_ldm =  $this->op_layout_details_model->save();

                            // show($op_ldm);
                        }
                    }
                }
                
                return $seat_layout_id;
                // $gabf_id    = $this->op_seat_layout_model->save();
            }
        }
        else
        {
            echo 'Error : insert_bus_format';
            show($data);
        }
    }

    function insert_bus_service_stops($data, $op_id,$dep_date) {
       

        $date = new DateTime(str_replace('/','-',$dep_date));
        $dep_date = $date->format('Y-m-d');
        
        if (isset($data['BusServiceStopsResponse']) && count($data['BusServiceStopsResponse']) > 0) {
            if (isset($data['BusServiceStopsResponse']['busServiceStops']) && count($data['BusServiceStopsResponse']['busServiceStops']) > 0 && isset($data['BusServiceStopsResponse']['busServiceStops']['busServiceStop'])) {
                $busServiceStop = $data['BusServiceStopsResponse']['busServiceStops']['busServiceStop'];
                
                // if(!is_array($busServiceStop))
                // {
                //     $busServiceStop = array($busServiceStop);
                // }


                if(isset($busServiceStop['serviceId']))
                {
                    $busServiceStop = array($busServiceStop);
                }

                if(count($busServiceStop) >0)
                {

                    foreach ($busServiceStop as $key => $value) {
                        // if($key == 8)
                        //     break;
                        
                        $boarding_stop_id       = $this->get_op_bus_stop_id($value['fromStop'], $op_id);
                        $destination_stop_id    = $this->get_op_bus_stop_id($value['toStop'], $op_id);
                        
                        $this->trip_master_model->op_trip_no       = $value['serviceId'];
                        $trip_data = $this->trip_master_model->select();

                        // show($trip_data,1);
                        // show($value,1);

                        $this->travel_destinations_model->reset();

                        $this->travel_destinations_model->boarding_stop_id      = $boarding_stop_id;
                        $this->travel_destinations_model->destination_stop_id   = $destination_stop_id;
                        $this->travel_destinations_model->trip_no               = $trip_data[0]->trip_no;
                        $this->travel_destinations_model->sch_departure_date    = $dep_date;
                        $this->travel_destinations_model->sch_departure_tm      = $value['departureTime'];
                        
                        $travel_destinations_data = $this->travel_destinations_model->select();
                        
                        if(count($travel_destinations_data) == 0)
                        {
                            $travel_id              = $this->generateid_model->getRequiredId('op_seat_layout');
                            
                            $this->travel_destinations_model->boarding_stop_id      = $boarding_stop_id;
                            $this->travel_destinations_model->destination_stop_id   = $destination_stop_id;
                            $this->travel_destinations_model->trip_no               = $trip_data[0]->trip_no;
                            $this->travel_destinations_model->sch_departure_date    = $dep_date;
                            $this->travel_destinations_model->sch_departure_tm      = $value['departureTime'];
                            $this->travel_destinations_model->arrival_date          = '';
                            $this->travel_destinations_model->arrival_tm            = $value['arrivalTime'];
                            $this->travel_destinations_model->day                   = $value['day'];
                            
                            $this->travel_destinations_model->travel_id             = $travel_id;
                        

                            $this->travel_destinations_model->save();    
                        }
                    }
                }
                else
                {
                    echo 'Empty array returned';
                }
            }
        }
        else
        {
            echo 'No data found';
        }
    }
    
    
    
    function get_op_bus_stop_id($data = array(), $op_id)
    {

        if(count($data) >0)
        {
            $this->op_stop_master_model->reset();
            $this->op_stop_master_model->op_id          = $op_id;
            $this->op_stop_master_model->op_stop_cd     = $data['busStopcode'];
            
            $op_stop_data                               = $this->op_stop_master_model->select();

            
            if(count($op_stop_data) == 0)
            {
                $this->bus_stop_master_model->bus_stop_name = $data['busStopName'];
                $this->bus_stop_master_model->tahsil_nm     = $data['taluka'];
                $this->bus_stop_master_model->district_nm   = $data['district'];
                
                $bus_stop_master_data = $this->bus_stop_master_model->select();
                
                if(count($bus_stop_master_data) == 0)
                {
                    $this->bus_stop_master_model->bus_stop_name = $data['busStopName'];
                    $this->bus_stop_master_model->bus_stop_name = $data['busStopName'];
                    $this->bus_stop_master_model->tahsil_nm     = $data['taluka'];
                    $this->bus_stop_master_model->district_nm   = $data['district'];
                    
                    $bus_stop_id = $this->bus_stop_master_model->save();
                    
                }
                else
                {
                    $bus_stop_id = $bus_stop_master_data[0]->bus_stop_id;
                }
                
                $this->op_stop_master_model->reset();
                $op_stop_id                                 = $this->generateid_model->getRequiredId('op_seat_layout');
                $this->op_stop_master_model->op_stop_id     = $op_stop_id;
                $this->op_stop_master_model->op_id          = $op_id;
                $this->op_stop_master_model->op_stop_cd     = $data['busStopcode'];
                $this->op_stop_master_model->op_stop_name   = ucfirst(strtolower($data['busStopName']));
                $this->op_stop_master_model->bus_stop_id    = $bus_stop_id;

                $this->op_stop_master_model->save();
                
            }
            else
            {
                $bus_stop_id                                = $op_stop_data[0]->bus_stop_id;
            }

            return $bus_stop_id; 
        }        
        
    }
    /***incomplete function not save data**********/
     function insert_bus_fare_details($data)
    {
        $this->load->model(array('fare_details_model'));
        
        
        if(isset($data['SeatAvailabilityResponse']) && count($data['SeatAvailabilityResponse'])>0)
        {
            if(isset($data['SeatAvailabilityResponse']['serviceId']) &&
                    count($data['SeatAvailabilityResponse']['serviceId'])>0)
            {
                 $value = $data['SeatAvailabilityResponse']['serviceId'];
                
//                $this->trip_boarding_alighting_model->id                = "";
                $this->fare_details_model->tba_id            = $this->generateid_model->getRequiredId('trip_boarding_alighting');
                $this->fare_details_model->trip_no           = $value['serviceId'];
                $this->fare_details_model->trip_stop_name    = $value['fromStop']['busStopName'];
                $this->fare_details_model->tba_type          = "BRD";
                
                $this->fare_details_model->sch_dept_tm       = $value['departureTime'];
                         
                $this->fare_details_model->save();
                $this->fare_details_model->reset();
                
            }
        }        
    }

    function insert_boarding_stops($stops, $ali_type, $stop_code, $op_id)
    {
        foreach($stops as $key => $value)
        {
            $this->op_stop_master_model->op_stop_cd     = $value['busStopcode'];
            $this->op_stop_master_model->op_id     = $op_id;
            $op_stop_data  = $this->op_stop_master_model->select();

            $this->op_stop_master_model->op_stop_cd     = $stop_code;
            $this->op_stop_master_model->op_id     = $op_id;
            $boarding_stop_data  = $this->op_stop_master_model->select();

            // show($op_stop_data);
            // show($boarding_stop_data);

            
            $this->boarding_stop_model->bus_stop_id         = $op_stop_data[0]->bus_stop_id;
            $this->boarding_stop_model->bus_stop_name       = $value['busStopName'];
            $this->boarding_stop_model->boarding_stop_name  = $boarding_stop_data[0]->op_stop_name;

            if( $ali_type == 'BRD')
            {
                $this->boarding_stop_model->is_boarding         = 'Y';
                $this->boarding_stop_model->is_alighting        = 'N';
            }
            else if( $ali_type == 'ALI')
            {
                $this->boarding_stop_model->is_boarding         = 'N';
                $this->boarding_stop_model->is_alighting        = 'Y';   
            }

            $bs_data = $this->boarding_stop_model->select(false);
            
            if(count($bs_data) == 0)
            {
                $boarding_stop_id = $this->generateid_model->getRequiredId('bord_ali_stop_master');

                $this->boarding_stop_model->boarding_stop_id    = $boarding_stop_id;
                // $this->boarding_stop_model->bus_stop_id         = $op_stop_data[0]->bus_stop_id;
                // $this->boarding_stop_model->bus_stop_name       = $value['busStopName'];
                // $this->boarding_stop_model->route_no            = '';
                // $this->boarding_stop_model->boarding_stop_name  = '';
                // $this->boarding_stop_model->arrival_duration    = '';
                $this->boarding_stop_model->seq                 = $value['stopSequence'];
                $this->boarding_stop_model->stop_status         = 'Y';

                $this->boarding_stop_model->save();
                show($this->boarding_stop_model,'','--------');
            }
        }
           
    }
    
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */