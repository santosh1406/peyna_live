<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cb_commission_distribution_model extends MY_Model {

    var $table  = 'cb_commission_distribution';
    var $fields = array("id","ticket_id","agent_code","waybill_no","commission_template_id","comm_applicable_amt","trimax_id","company_id","md_id","ad_id","di_id","agent_id","trimax_comm","company_comm","md_comm","ad_comm","di_comm","agent_comm","is_comm_distrubute","created_at");
    var $key    = 'id';
   
    public function __construct() {
        parent::__construct();
       // $this->_init();      
    }
    public function UpdateCommDistribution($user_id,$role_id)
    {
    	$Today = date('Y-m');
        $last_month = date("Y-m",strtotime("-1 month"));
       if(!empty($user_id) && !empty($role_id))
       {
       	    if($role_id == TRIMAX_ROLE_ID){
              $data = array('comm_distrubute_trimax' => 'Y');
              $this->db->where('trimax_id', $user_id);

            }elseif ($role_id == COMPANY_ROLE_ID) {
              $data = array('comm_distrubute_company' => 'Y'); 
              $this->db->where('company_id', $user_id);

            }elseif ($role_id == MASTER_DISTRIBUTOR_ROLE_ID) {
              $data = array('comm_distrubute_md' => 'Y'); 
              $this->db->where('md_id', $user_id);

            }elseif ($role_id == AREA_DISTRIBUTOR_ROLE_ID) {
               $data = array('comm_distrubute_ad' => 'Y');
               $this->db->where('ad_id', $user_id);

            }elseif ($role_id == DISTRIBUTOR) {
               $data = array('comm_distrubute_di' => 'Y'); 
               $this->db->where('di_id', $user_id);

            }elseif ($role_id == RETAILER_ROLE_ID) {
               $data = array('comm_distrubute_agent' => 'Y'); 
               $this->db->where('agent_id', $user_id);
            }  
        $this->db->where("DATE_FORMAT(created_at,'%Y-%m') =",$last_month);
        if($this->db->update('cb_commission_distribution', $data))
        {
           return true;
        } 
        else
        {
           return false;
        }	
       } 	
    }

}
