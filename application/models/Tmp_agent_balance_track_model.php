<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tmp_agent_balance_track_model extends MY_Model {

    var $table  = 'tmp_agent_balance_track';
    var $fields = array("id","agent_id","agent_balance","date");
    var $key    = 'id';


    public function __construct() 
    {
        parent::__construct();
        $this->_init();      
    }
    
}



