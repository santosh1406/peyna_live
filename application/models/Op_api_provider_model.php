<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Op_api_provider_model extends MY_Model {

    var $table = 'op_api_provider';
    var $fields = array("op_api_id", "api_name", "is_status", "is_deleted", "inserted_by", "updated_by", "inserted_date", "updated_date");
    // var $fields = "id,w_id,user_id,amt,status,expire_at,comment,added_by,added_on";
    var $key = 'op_api_id';

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    function get_api_data($data) {
        $this->db->select('op_api_id,api_name');
        $this->db->from('op_api_provider op');
        $this->db->where('op_api_id', $data['api_id']);
        $this->db->where('is_deleted', '0');
        $result_data = $this->db->get();
        return $result_data->result_array();
    }
      function get_api_all_data() {
        $this->db->select('op_api_id,api_name');
        $this->db->from('op_api_provider op');
        
        $this->db->where('is_deleted', '0');
        $result_data = $this->db->get();
        return $result_data->result_array();
    }

}

?>