<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Agent
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cancellation_charges_detail_model extends MY_Model {
    
    var $table  = 'cancellation_charges_detail';
    var $fields = array("id","op_id","note","timestamp");
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

}

// End of Agent_model class