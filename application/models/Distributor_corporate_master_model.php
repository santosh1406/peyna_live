<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Distributor_corporate_master_model extends MY_Model
{
    var $table    = 'distributor_corporate_master';
    var $fields   = array("id,first_name,last_name,email,mobile,uid,distributor_code,corporate_code,office_code,is_status,is_deleted");
    var $key      = 'id';

    protected $set_created = false;

    protected $set_modified = false;
    
    public function __construct() {
        parent::__construct();
        $this->_init(); 
    }
    
}