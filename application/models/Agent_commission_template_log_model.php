<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agent_commission_template_log_model extends MY_Model 
{

    protected $table         =  'agent_commission_template_log';
    var $fields		           = array("id","user_id","commission_id","added_on","added_by","commission_from","commission_to");
    var $key                 = 'id'; 
   
    public function __construct() 
    {
        parent::__construct();
        $this->load->model(array('agent_commission_template_log_model'));   
    }
    
    public function get_active_commission($id)
    {
        $today=date('Y-m-d h:i:s');
        $this->db->select("commission_id");
        $this->db->from('agent_commission_template_log');
        $this->db->where('user_id',$id);
        $this->db->where('commission_from<=',$today);
        $this->db->where('commission_to>=',$today);
        $this->db->where('status',"1");
        $this->db->order_by('id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->result();
    }

}
