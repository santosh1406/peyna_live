<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Machine_allocation_model extends CI_Model {

    function __construct() {
        parent::__construct();
        //$this->db2 = $this->load->database('local_up_db', TRUE);	
    }

    public function LoadMachineRegDataTable($sWhere, $sOrder, $sLimit) {
        $lcSqlsStr = "SELECT id,concat(first_name,' ',last_name) as Name,email,mobile_no,lock_status from users ";
        $lcSqlsStr .= "$sWhere $sOrder $sLimit";
        //echo $lcSqlsStr;
        $query = $this->db->query($lcSqlsStr);

        $data['ResultSet'] = $query->result_array();

        $data['iFilteredTotal'] = count($data['ResultSet']);

        /* Total data set length */
        $sQuery = " SELECT COUNT(id) as countRid  FROM users $sWhere";
        $query = $this->db->query($sQuery);
        $ResultSet = $query->result_array();
        $data['iTotal'] = $ResultSet[0]['countRid'];
        return $data;
    }

    public function getRetailers($id) {
        $sQuery = "SELECT id,concat(first_name,' ',last_name) as Name FROM users WHERE status='Y' AND level_5=" . $id . " AND kyc_verify_status='Y'";
        $query = $this->db->query($sQuery);
        return $query->result();
    }

    public function get_allocate_waybilll($id) {

        $sQuery = "SELECT * from users WHERE id=" . $id . "";
        $query = $this->db->query($sQuery);
        return $query->result();
    }

    public function LoadMachineCollectionDataTable($sWhere, $sOrder, $sLimit) {
        $lcSqlsStr = "SELECT u.id as id,concat(u.first_name,' ',u.last_name) as Name,u.agent_code as AgentCode,u.star_seller_name,u.depot_name,round(w.amt, 2) as amt,u.lock_status from users u LEFT JOIN wallet w ON u.id=w.user_id ";
        $lcSqlsStr .= "$sWhere $sOrder $sLimit";

        $query = $this->db->query($lcSqlsStr);

        $data['ResultSet'] = $query->result_array();

        $data['iFilteredTotal'] = count($data['ResultSet']);

        /* Total data set length */
        $sQuery = " SELECT COUNT(u.id) as countRid  FROM users u LEFT JOIN wallet w ON u.id=w.user_id $sWhere";
        $query = $this->db->query($sQuery);
        $ResultSet = $query->result_array();
        $data['iTotal'] = $ResultSet[0]['countRid'];
        return $data;
    }

    public function LoadWaybillAllocationDataTable($sWhere, $sOrder, $sLimit) {
        $lcSqlsStr = "SELECT u.id as id,concat(u.first_name,' ',u.last_name) as Name,u.agent_code as AgentCode,u.star_seller_name,u.depot_name,u.securitydeposite,u.topuplmit,w.amt,u.lock_status from users u LEFT JOIN wallet w ON u.id=w.user_id ";
        $lcSqlsStr .= "$sWhere $sOrder $sLimit";

        $query = $this->db->query($lcSqlsStr);

        $data['ResultSet'] = $query->result_array();

        $data['iFilteredTotal'] = count($data['ResultSet']);

        /* Total data set length */
        $sQuery = " SELECT COUNT(u.id) as countRid  FROM users u LEFT JOIN wallet w ON u.id=w.user_id $sWhere";
        $query = $this->db->query($sQuery);
        $ResultSet = $query->result_array();
        $data['iTotal'] = $ResultSet[0]['countRid'];
        return $data;
    }

    function check_machine_registration($id) {
        $this->db->select("user_id,unique_no,version_number,version_date");
        $this->db->from("msrtc_data.Machine_registration");
        $this->db->where("user_id= '" . $id . "'");
        $user_result = $this->db->get();
        $user_result_array = $user_result->result_array();
        if ($user_result) {
            return $user_result_array;
        } else {
            return false;
        }
    }

    public function getAgentDetails($AgntCode) {

        $sQuery = "SELECT * from users WHERE agent_code='" . $AgntCode . "'";
        $query = $this->db->query($sQuery);
        return $query->result();
    }

    public function getBusStopDetails($BusStop) {

        $sQuery = "SELECT BUS_STOP_CD,BUS_STOP_NM FROM msrtc_data.bus_stops WHERE LOWER(BUS_STOP_CD) = LOWER('" . $BusStop . "')";
        $query = $this->db->query($sQuery);
        return $query->result();
    }

    public function getRetailersByAGT($id) {
        $sQuery = "SELECT id,concat(first_name,' ',last_name) as Name FROM users WHERE status='Y'  AND kyc_verify_status='Y' and agent_code like '%AGT%'";
        $query = $this->db->query($sQuery);
        return $query->result();
    }

    public function getRetailersBywaybill($id) {
        $sQuery = "SELECT u.id,concat(u.first_name,' ',u.last_name) as Name,w.waybill_no FROM users u left join msrtc_app.cb_waybillprogramming w on u.agent_code=w.agent_code WHERE status='Y' AND level_5=" . $id . " AND kyc_verify_status='Y'";
        $query = $this->db->query($sQuery);
        return $query->result();
    }

    public function LoadUpdatedMachineRegDataTable($sWhere, $sOrder, $sLimit) {
        //$lcSqlsStr = "SELECT u.id,concat(u.first_name,' ',u.last_name) as Name,u.email,u.mobile_no from users u ";
        $msrtc_db_name = MSRTC_DB_NAME;
        $lcSqlsStr = "SELECT u.id,concat(u.first_name,' ',u.last_name) as Name,u.email,u.mobile_no,mu.machine_status,u.lock_status from users u left join $msrtc_db_name.users mu on u.agent_code = mu.agent_code ";

        $lcSqlsStr .= "$sWhere $sOrder $sLimit";
        //echo $lcSqlsStr;
        $query = $this->db->query($lcSqlsStr);

        $data['ResultSet'] = $query->result_array();

        $data['iFilteredTotal'] = count($data['ResultSet']);

        /* Total data set length */
        $sQuery = " SELECT COUNT(u.id) as countRid  FROM users u left join $msrtc_db_name.users mu on u.agent_code = mu.agent_code $sWhere";
        //$sWhere .= "and (m.status is NULL or m.status in('Y','N' )) group by u.id ";
        //$sQuery = " SELECT distinct  COUNT(u.id) as countRid  FROM users u left join msrtc_bos_dev.machine_registration m on u.id=m.user_id $sWhere";

        $query = $this->db->query($sQuery);
        //show($this->db->last_query());
        $ResultSet = $query->result_array();
        $data['iTotal'] = $ResultSet[0]['countRid'];
        //show($data['iTotal'],1);
        return $data;
    }

    //Added by Vijay Ghadi on 01/10/2018
    public function getMachineAllotedData($id) {
        $this->db->select('cmm.IMEI_NO, cmm.IMEI_NO_2, cbs.BUS_STOP_NM as seller_point, cmm.SW_Machine_No');
        $this->db->from('cb_machine_master cmm');
        $this->db->join('cb_assign_machine_agent cam', 'cam.machine_master_id = cmm.id', 'inner');
        $this->db->join('cb_locationwise_machine clm', 'clm.SW_Machine_No=cmm.SW_Machine_No', 'inner');
        $this->db->join('cb_bus_stops cbs', 'cbs.BUS_STOP_CD=clm.seller_point', 'inner');
        $this->db->where('cam.user_id', $id);
        $this->db->where('cmm.is_status', 'Y');
        $this->db->where('cam.is_status', 'Y');
        $this->db->where('clm.is_status', 'Y');

        $res = $this->db->get();
        //last_query(1);
        $result = $res->result_array();

        if ($res->num_rows() > 0) {
            $i = 0;
            foreach ($result as $key => $value) {
                $this->db->select('is_status, is_deleted');
                $this->db->from(MSRTC_CB . '.cb_agent_master_data');
                $this->db->where('SW_Machine_No', $value['SW_Machine_No']);
                $this->db->where('is_status !=', 'N');
                $this->db->where('is_deleted !=', 'Y');
                $res = $this->db->get();
                $data = $res->result_array();

                if ($res->num_rows() > 0) {
                    $testArray[$i] = array(
                        'IMEI_NO' => $value['IMEI_NO'],
                        'IMEI_NO_2' => $value['IMEI_NO_2'],
                        'seller_point' => $value['seller_point'],
                        'SW_Machine_No' => $value['SW_Machine_No'],
                        'is_status' => $data[0]['is_status'],
                        'is_deleted' => $data[0]['is_deleted']
                    );
                } else {
                    $testArray[$i] = array(
                        'IMEI_NO' => $value['IMEI_NO'],
                        'IMEI_NO_2' => $value['IMEI_NO_2'],
                        'seller_point' => $value['seller_point'],
                        'SW_Machine_No' => $value['SW_Machine_No']
                    );
                }
                $i++;
            }
            return $testArray;
        } else {
            return false;
        }
    }

    public function masterSyncData($data) {
        $this->db->insert(MSRTC_CB . '.cb_agent_master_data', $data);
        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    public function checkSyncData($id) {
        $this->db->select('is_status, is_deleted');
        $this->db->from(MSRTC_CB . '.cb_agent_master_data');
        $this->db->where('SW_Machine_No', $id);
        $this->db->where('is_status', 'P');
        $this->db->where('is_deleted', 'N');
        $res = $this->db->get();
        if ($res->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function reSyncData($id, $data) {
        $val = array('is_status' => 'N', 'is_deleted' => 'Y', 'updated_by' => $this->session->userdata('user_id'));
        $this->db->set('is_status', 'is_deleted', 'updated_by', false);
        $this->db->where('SW_Machine_No', $id);
        $this->db->where('is_status', 'P');
        $this->db->where('is_deleted', 'N');
        $this->db->update(MSRTC_CB . '.cb_agent_master_data', $val);

        if ($this->db->affected_rows()) {
            $this->db->insert(MSRTC_CB . '.cb_agent_master_data', $data);
            if ($this->db->affected_rows()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}

?>