<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Subagent_commission_model extends MY_Model {

    protected $table = 'subagent_commission';
    var $fields = array("id", "distributor_id", "dist_commission_id", "name", "from", "till", "default", "created_at", "updated_at", "created_by", "updated_by", "status");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
    }

    public function insert_slab_wise_commission($data) {
        $ret = true;
        if (!empty($data['master']) && $data['comm_details']) {
            $parset1 = implode("','", $data['master']);
            $this->db->trans_begin();
            $s = "insert into subagent_commission (distributor_id,dist_commission_id,`name`,`from`,`till`,`default`,`created_by`,created_at,status) values ('" . $parset1 . "',now(),'1')";
            $this->db->query($s);
            $sub_commission_id = $this->db->insert_id();
            if ($this->db->trans_status() === FALSE || empty($sub_commission_id)) {
                $this->db->trans_rollback();
                $ret = false;
            } else {
                foreach ($data['comm_details'] as $operators) {
                    if (!empty($operators)) {
                        $parset2 = implode("','", $operators);
                        $parset = "'" . $sub_commission_id . "','" . $parset2 . "'";
                        $s = "insert into subagent_commission_details (subagent_commission_id,dist_comm_detail_id,dist_comm_sale_slab_id,distributor_perc,subagent_perc) values(" . $parset . ")";
                        $this->db->query($s);
                        last_query();
                        $sub_commission_detail_id = $this->db->insert_id();
                    }
                }
            }
            $this->db->trans_commit();
        }
        return $ret;
    }

    public function commission_detail_by_id($commission_id) {
        $this->db->select("
                           sc.id as subagent_commission_id,
                           sc.dist_commission_id,
                           u.display_name,
                           sc.name as subagent_commission_name,
                           sc.default,
                           sc.from,
                           sc.till,
                           scd.dist_comm_detail_id,
                           scd.dist_comm_detail_id,
                           scd.dist_comm_sale_slab_id,
                           scd.distributor_perc,
                           scd.subagent_perc,
                           cd.id AS commission_detail_id,
                           cd.base_commission_id,
                           cd.op_id as operator_id,
                           cd.provider_operator_id AS provider_operator_id,
                           cd.provider_id AS provider_id,
                           p.name AS provider_name,
                           o.op_name AS provider_operator_name,
                           cd.type as commission_type,
                           cd.base_rate_flag,
                           cd.percentage_base as base_perc,
                           cd.fixed_base as base_fixed,
                           cd.commission_from as commission_from_fare,
                           cd.commission_to as commission_to_fare,
                           cd.seaticket_wise,
                           cssd.id as slab_id,
                           cssd.apply_on as slab_apply_on,
                           cssd.from as slab_from,
                           cssd.to as slab_to,
                           cssd.type as slab_type,
                           cssd.agent_commission,
                           cssd.bos_commission,
                           btm.name as bus_type_name
                          ");
        $this->db->from("subagent_commission_details scd");
        $this->db->join("subagent_commission sc", "scd.subagent_commission_id = sc.id ", "right");
        $this->db->where("sc.id", $commission_id);
        $this->db->join("users u", "sc.distributor_id = u.id ", "inner");
        $this->db->join("commission_detail cd", "cd.id=scd.dist_comm_detail_id");
        $this->db->join("base_commission bc", "bc.id = cd.base_commission_id", "inner");
        $this->db->join("api_provider p", "p.id = cd.provider_id", "inner");
        $this->db->join("api_provider_operator po", "po.id = cd.provider_operator_id", "left");
        $this->db->join("op_master o", "o.op_id = po.op_id", "left");
        $this->db->join("commission_sale_slab_details cssd", "cssd.commission_details_id=cd.id", "left");
        $this->db->join("bus_type_home btm", "btm.id=bc.bus_type", "left");
        $query = $this->db->get();
        return $query->result();
    }

}
