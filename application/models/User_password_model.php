<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_password_model extends MY_Model {
    
    var $table  = 'user_password';
    var $fields = array("id","user_id","salt","password","created_date");
    var $key    = 'id';
	
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
	
	public function save_password_log($data) {        
		$flag = false;
        if (!empty($data))
		{
			if ($this->db->insert('user_password', $data)) {				
				$flag= $this->db->insert_id();
			} 
		}
		return $flag;
    }
	
	public function get_last_passwords($user_id) {
		$max_old_password = $this->config->item('max_old_password');
		$this->db->select('salt, password');
		$this->db->from('user_password');
		$this->db->where('user_id', $user_id);
		$this->db->order_by('id', 'desc');
		$this->db->limit($max_old_password);
		$query = $this->db->get();
		return $query->result_array();            
	}
}