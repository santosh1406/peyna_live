<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  device_login_model extends MY_Model
{
    /**
     * Instanciar of CI
     */
    var $table = 'login_count';
    var $fields		   = array("id","agent_id","agent_code","login_count","date_added");
    var $key    = 'id';  
    
    public function __construct() {
        parent::__construct();
        $this->_init(); 
   
    }
        
}
