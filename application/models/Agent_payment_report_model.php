<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agent_payment_report_model extends MY_Model {
	
	public function genAgentPaymentReport($input) {
    
        $this->db->select(' UM.id AS agent_id,
          UM.is_deposit,
          RPR.amount,
          RPR.bank_name,
          RPR.pg_tracking_id,
          UM.agent_code AS agent_code, 
          UM.username AS agent_username,
          UM.first_name AS f_name,

RPR.amount,          UM.last_name AS l_name, 
          concat(UM.address1, " ", UM.address2) as address, 
          GROUP_CONCAT(SM.service_name) AS service_name, 
          DP.REGION_CD AS region, 
          RE.REGION_NM AS region_name, 
          DV.DIVISION_CD AS division, DP.DEPOT_NM AS depot,
          DP.DEPOT_CD AS depot_cd,
          RSM.terminal_code AS terminal_code, 
          DATE_FORMAT(RSM.created_at, "%d-%m-%Y") AS terminal_code_generate_date, 
          CASE WHEN UM.kyc_verify_status = "Y" THEN "Yes" ELSE "No" END AS kyc_verify_status, 
          CASE WHEN UM.email_verify_status = "Y" THEN "Yes" ELSE "No" END AS email_verify_status, 
          CASE WHEN UM.verify_status = "Y" THEN "Yes" ELSE "No" END AS verify_status, 
          CASE WHEN UM.status = "Y" THEN "Yes" ELSE "No" END AS status, 
          CASE WHEN UM.otp_verify_status = "Y" THEN "Yes" ELSE "No" END AS mobile_verify_status, 
          DATE_FORMAT(UM.created_date, "%d-%m-%Y") As signup_date',FALSE);
        $this->db->from('users UM ');
        $this->db->join('retailer_service_mapping As RSM', 'RSM.agent_id = UM.id', 'LEFT');
        $this->db->join('service_master As SM', 'SM.id = RSM.service_id', 'INNER');
        $this->db->join('msrtc_replication.depots AS DP', 'DP.DEPOT_CD = UM.signup_depot_cd', 'LEFT');
      $this->db->join('msrtc_bos_dev.regions AS RE', 'RE.REGION_CD = DP.REGION_CD', 'LEFT');
        $this->db->join('msrtc_replication.divisions AS DV','DV.DIVISION_CD = DP.DIVISION_CD', 'LEFT');
        $this->db->join('reg_pg_response AS RPR', 'RPR.user_id = UM.id', 'INNER');
        $this->db->where('RPR.transaction_status = "success"');
        $this->db->where('UM.is_deposit = "Y"');
       
        if (isset($input['from_date']) && $input['from_date'] != '') {
            $this->db->where("DATE_FORMAT(UM.created_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['to_date']) && $input['to_date'] != '') {
            $this->db->where("DATE_FORMAT(UM.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }
        $this->db->group_by('UM.id');
        $resQuery = $this->db->get();
        // last_query();
      // die();
       $resArr = $resQuery->result_array();
     
        return $resArr;
    }
function getRegionDetail(){
  $this->db->select('REGION_CD, REGION_NM');
       $this->db->from('msrtc_bos_dev.regions');
       $this->db->where('STATE_CD = "MHR"');
       $this->db->where('REGION_CD != "PG"');
        $resQuery = $this->db->get();
       //  last_query();
       $resArr = $resQuery->result_array();
     
        return $resArr;
}
   
}
?>
