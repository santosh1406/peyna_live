<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Machine_allocation_model extends CI_Model {
    
    function __construct() {
        parent::__construct();
	    //$this->db2 = $this->load->database('local_up_db', TRUE);	
    }

    public function LoadMachineRegDataTable($sWhere, $sOrder, $sLimit) {
        $lcSqlsStr = "SELECT id,concat(first_name,' ',last_name) as Name,email,mobile_no from users ";
        $lcSqlsStr .= "$sWhere $sOrder $sLimit";
		//echo $lcSqlsStr;
        $query = $this->db->query($lcSqlsStr);

        $data['ResultSet'] = $query->result_array();

        $data['iFilteredTotal'] = count($data['ResultSet']);

        /* Total data set length */
        $sQuery = " SELECT COUNT(id) as countRid  FROM users $sWhere";
        $query = $this->db->query($sQuery);
        $ResultSet = $query->result_array();
        $data['iTotal'] = $ResultSet[0]['countRid'];
        return $data;
    }

	public function getRetailers($id){
	  $sQuery="SELECT id,concat(first_name,' ',last_name) as Name FROM users WHERE status='Y' AND level_5=".$id." AND kyc_verify_status='Y'";
	  $query = $this->db->query($sQuery);
	  return $query->result();
	}
	
	public function get_allocate_waybilll($id){ 
    
		$sQuery="SELECT * from users WHERE id=".$id."";
	    $query = $this->db->query($sQuery);
	    return $query->result();
			
	}
	
	public function LoadMachineCollectionDataTable($sWhere, $sOrder, $sLimit) {
        $lcSqlsStr = "SELECT u.id as id,concat(u.first_name,' ',u.last_name) as Name,u.agent_code as AgentCode,u.hmmlimit,u.depot_name,w.amt from users u LEFT JOIN wallet w ON u.id=w.user_id ";
        $lcSqlsStr .= "$sWhere $sOrder $sLimit";
		
        $query = $this->db->query($lcSqlsStr);

        $data['ResultSet'] = $query->result_array();

        $data['iFilteredTotal'] = count($data['ResultSet']);

        /* Total data set length */
        $sQuery = " SELECT COUNT(u.id) as countRid  FROM users u LEFT JOIN wallet w ON u.id=w.user_id $sWhere";
        $query = $this->db->query($sQuery);
        $ResultSet = $query->result_array();
        $data['iTotal'] = $ResultSet[0]['countRid'];
        return $data;
    }
	
	public function LoadWaybillAllocationDataTable($sWhere, $sOrder, $sLimit) {
        $lcSqlsStr = "SELECT u.id as id,concat(u.first_name,' ',u.last_name) as Name,u.agent_code as AgentCode,u.star_seller_name,u.depot_name,u.securitydeposite,u.topuplmit,w.amt from users u LEFT JOIN wallet w ON u.id=w.user_id ";
        $lcSqlsStr .= "$sWhere $sOrder $sLimit";
		
        $query = $this->db->query($lcSqlsStr);

        $data['ResultSet'] = $query->result_array();

        $data['iFilteredTotal'] = count($data['ResultSet']);

        /* Total data set length */
        $sQuery = " SELECT COUNT(u.id) as countRid  FROM users u LEFT JOIN wallet w ON u.id=w.user_id $sWhere";
        $query = $this->db->query($sQuery);
        $ResultSet = $query->result_array();
        $data['iTotal'] = $ResultSet[0]['countRid'];
        return $data;
    }
    function check_machine_registration($id)
    {
        $this->db->select("user_id,unique_no,version_number,version_date");
        $this->db->from("msrtc_data.Machine_registration");
        $this->db->where("user_id= '" . $id . "'");
        $user_result = $this->db->get();
        $user_result_array = $user_result->result_array();
        if($user_result)
        {
            return $user_result_array;
        }else
        {
            return false;
        }
    }
	
	public function getAgentDetails($AgntCode){ 
    
		$sQuery="SELECT * from users WHERE agent_code='".$AgntCode."'";
	    $query = $this->db->query($sQuery);
	    return $query->result();
			
	}
	
	public function getBusStopDetails($BusStop){ 
    
		$sQuery="SELECT BUS_STOP_CD,BUS_STOP_NM FROM msrtc_data.bus_stops WHERE LOWER(BUS_STOP_CD) = LOWER('".$BusStop."')";
	    $query = $this->db->query($sQuery);
	    return $query->result();
			
	}
}
?>
