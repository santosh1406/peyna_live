<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Op_config_model extends MY_Model {

    /**
     * Instanciar o CI
     */
    var $table = 'op_config';
    var $fields = array("op_cofig_id", "op_id", "op_sms_flag", "op_email_flag", "op_no_of_seats_limit", "op_ticket_format_id", "is_status", "is_deleted", "api_url",
        "api_usernm", "api_password", "api_usertype", "bank_account_no", "bank_name", "bank_branch", "bank_ifsc_code",
        "settlement_type", "settlement_no_days", "operator_type", "created_by", "op_email_id",
        "stu_private", "is_own_ors_flag", "op_hit_limit", "convey_charges_flag", "service_charges_flag",
        "reservation_charges_flag", "cancellation_amt","fare_collection_type");
    var $key = 'op_cofig_id';

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
    

    public function op_config_byid($op_config_id) {
        
        $this->db->select("op_id,op_sms_flag,op_email_flag,op_no_of_seats_limit,op_ticket_format_id,is_deleted,api_url,api_usernm,api_password,api_usertype,"
                . "bank_account_no,bank_name,bank_branch,bank_ifsc_code,settlement_type,settlement_no_days,operator_type,op_email_id,stu_private,"
                . "is_own_ors_flag,op_hit_limit,convey_charges_flag,service_charges_flag,reservation_charges_flag"
                . ",cancellation_amt,fare_collection_type");
        $this->db->from("op_config");
        $this->db->where("op_cofig_id", $op_config_id);
        $query = $this->db->get();
        //show($this->db->last_query(), 1);
        return $query->result_array();
    }

    public function existing_op($existing_op) {
        $this->db->select('op_cofig_id');
        $this->db->from('op_config');
        $this->db->where("op_id", $existing_op);
        $this->db->where("is_status", "1");
        $this->db->where("is_deleted", "0");
        
        $query = $this->db->get();
        //show($this->db->last_query(), 1);
        $data = $query->row();
        $cnt = count($data);
        return $cnt;
    }

    public function update_opconfig($input) {
        $exits = array_key_exists('deleted_flag', $input);
        if ($exits != '') {
            if ($input['deleted_flag'] == '1') {
                $delete = '1';
                $active = "0";
            }
        } else {
            $delete = '0';
            $active = "1";
        }
        $user_id = $this->session->userdata('user_id');

        $user_id = $this->session->userdata('user_id');
        $data = array("op_id" => $input['op_id'], "op_sms_flag" => $input['status_sms'],
            "op_email_flag" => $input['status_email'],
            "op_no_of_seats_limit" => $input['seats_limit'],
            "op_ticket_format_id" => $input['ticket_format_id'],
            "api_url" => $input['txtapiurl'], "api_usernm" => $input['txtapiuname'],
            "api_password" => $input['txtapipwd'], "api_usertype" => $input['txtapiusertype'],
            "bank_account_no" => $input['txtaccountno'], "bank_name" => $input['txtbankname'],
            "bank_branch" => $input['txtbranch'], "bank_ifsc_code" => $input['txtifsccode'],
            "settlement_type" => $input['radtype'], "settlement_no_days" => $input['txtnofodays'],
            "operator_type" => $input['op_type'],
            "op_email_id" => $input['txtopemail'],
            "stu_private" => $input['radoptype'], "is_own_ors_flag" => $input['chkorsflag'],
            "op_hit_limit" => $input['txtophits'],
            "convey_charges_flag" => $input['status_convey_chargesflag'],
            "service_charges_flag" => $input['status_service_taxflag'],
            "reservation_charges_flag" => $input['status_res_chargesflag'],
            "cancellation_amt" => $input['txtcanclamt'],"fare_collection_type" => $input['radcollectiontype'],
            "updated_by" => $user_id,
            "is_deleted" => $delete, "is_status" => $active);
        $this->db->where('op_cofig_id', $input['hiddn_op_config_id']);
        $this->db->update('op_config', $data);
        $data_status['status'] = "@#success@";
        return $data_status;
    }

    public function delete_opconfig($input) {
        $data = array("is_status" => '0', "is_deleted" => '1');
        $this->db->where('op_cofig_id', $input['id']);
        $this->db->update('op_config', $data);
        $cnt = $this->db->affected_rows();
        if ($cnt != 0) {
            $data["flag"] = '@#success#@';
        } else {

            $data["flag"] = '@#failed#@';
        }
        return $data;
    }


    /*
     *Author    : Suraj Rathod
     *Function  : get_op_detail_by_id
     *Param     : op_id - Operator Id
     *Detail    : Return operator setting info
     */
    public function get_op_detail_by_id($op_id) {
        $this->db->select("oc.*,mt.title as mail_template_title");
        $this->db->from('op_config oc');
        $this->db->join('mail_template mt',"mt.id = oc.op_ticket_format_id", "left");
        $this->db->where("oc.op_id", $op_id);
        $query = $this->db->get();
        return $query->result();
    }

}
