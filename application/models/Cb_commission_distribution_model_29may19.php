<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cb_commission_distribution_model extends MY_Model {

    var $table  = 'cb_commission_distribution';
    var $fields = array("id","ticket_id","agent_code","waybill_no","commission_template_id","comm_applicable_amt","trimax_id","company_id","md_id","ad_id","di_id","agent_id","trimax_comm","company_comm","md_comm","ad_comm","di_comm","agent_comm","is_comm_distrubute","created_at");
    var $key    = 'id';
   
    public function __construct() {
        parent::__construct();
       // $this->_init();      
    }
    public function UpdateCommDistribution($user_id,$role_id)
    {
    	$Today = date('Y-m');
        $last_month = date("Y-m",strtotime("-1 month"));
       if(!empty($user_id) && !empty($role_id))
       {
       	    if($role_id == TRIMAX_ROLE_ID){
              $data = array('comm_distrubute_trimax' => 'Y');
              $this->db->where('trimax_id', $user_id);

            }elseif ($role_id == COMPANY_ROLE_ID) {
              $data = array('comm_distrubute_company' => 'Y'); 
              $this->db->where('company_id', $user_id);

            }elseif ($role_id == MASTER_DISTRIBUTOR_ROLE_ID) {
              $data = array('comm_distrubute_md' => 'Y'); 
              $this->db->where('md_id', $user_id);

            }elseif ($role_id == AREA_DISTRIBUTOR_ROLE_ID) {
               $data = array('comm_distrubute_ad' => 'Y');
               $this->db->where('ad_id', $user_id);

            }elseif ($role_id == DISTRIBUTOR) {
               $data = array('comm_distrubute_di' => 'Y'); 
               $this->db->where('di_id', $user_id);

            }elseif ($role_id == RETAILER_ROLE_ID) {
               $data = array('comm_distrubute_agent' => 'Y'); 
               $this->db->where('agent_id', $user_id);
            }  
        // $this->db->where("DATE_FORMAT(created_at,'%Y-%m') =",$last_month);
        if($this->db->update('cb_commission_distribution', $data))
        {
           return true;
        } 
        else
        {
           return false;
        }	
       } 	
    }

    public function getChainCommissionData($role_id,$user_id,$from_date='',$to_date='') {     
        $yesterday_date = date('Y-m-d', strtotime(' -1 day'));
        $date1  = $yesterday_date.' 00:00:00';
        $date2  = $yesterday_date.' 23:59:59';
         /*code Modified by Aparna- 2018-06-29 - CR 314*/
        if($from_date != '' && $to_date !='')
        {
          $date1  = $from_date;
          $date2  = $to_date;
        }
         /*code Modified by Aparna- 2018-06-29 - CR 314*/
        $where  = array('comm_calculate_date >=' => $date1,'comm_calculate_date <= ' => $date2);
        if ($role_id == COMPANY_ROLE_ID) {
          $this->db->select("(sum(md_actual_comm)+sum(ad_actual_comm)+sum(di_actual_comm)+sum(agent_actual_comm)) as chain_commission_masters");
           $where['company_id'] = $user_id;
        } elseif ($role_id == MASTER_DISTRIBUTOR_ROLE_ID) {
          $this->db->select("(sum(ad_actual_comm)+sum(di_actual_comm)+sum(agent_actual_comm)) as chain_commission_masters");
           $where['md_id'] = $user_id;
        } elseif($role_id == AREA_DISTRIBUTOR_ROLE_ID) {
          $this->db->select("(sum(di_actual_comm)+sum(agent_actual_comm)) as chain_commission_masters");
          $where['ad_id'] = $user_id;
        } elseif ($role_id == DISTRIBUTOR) {
          $this->db->select("sum(agent_actual_comm) as chain_commission_masters");
          $where['di_id'] = $user_id;
        }
        $this->db->from("cb_commission_distribution");       
        if ($where != "") {
            $this->db->where($where);
        }
       
        $result  = $this->db->get();
        
        if(!empty($result)) {
          return $result->result_array();
        } else {
          return false;
        }              
      }

    function getDailyEarnedCommission($role_id,$user_id,$from_date='',$to_date='')
    {
        $yesterday_date = date('Y-m-d', strtotime(' -1 day'));
        $date1  = $yesterday_date.' 00:00:00';
        $date2  = $yesterday_date.' 23:59:59';

         /*code Modified by Aparna- 2018-06-29 - CR 314*/
        if($from_date != "" && $to_date != "")  
        {
          $date1 = $from_date;
          $date2 = $to_date;
        }
         /*code Modified by Aparna- 2018-06-29 - CR 314*/
        $where  = array('comm_calculate_date >=' => $date1,'comm_calculate_date <= ' => $date2);
        if ($role_id == COMPANY_ROLE_ID) {
          //$this->db->select("sum(company_actual_comm ) as final_earned_commission");
          $this->db->select(" sum(company_comm ) as final_earned_commission");
          $where['company_id'] = $user_id;
        }elseif ($role_id == MASTER_DISTRIBUTOR_ROLE_ID) {
          //$this->db->select("sum(md_actual_comm) as final_earned_commission");
          $this->db->select("sum(md_comm) as final_earned_commission");
          $where['md_id'] = $user_id;
        } elseif($role_id == AREA_DISTRIBUTOR_ROLE_ID) {
          //$this->db->select("sum(ad_actual_comm) as final_earned_commission");
          $this->db->select("sum(ad_comm) as final_earned_commission");
          $where['ad_id'] = $user_id;
        } elseif ($role_id == DISTRIBUTOR) {
          //$this->db->select("sum(di_actual_comm) as final_earned_commission");
          $this->db->select("sum(di_comm) as final_earned_commission");
          $where['di_id'] = $user_id;
        }elseif ($role_id == RETAILER_ROLE_ID) {
          //$this->db->select("sum(agent_actual_comm) as final_earned_commission");
          $this->db->select("sum(agent_comm) as final_earned_commission");
          $where['agent_id'] = $user_id;
        }         
        $this->db->from("cb_commission_distribution ");
        
        if ($where != "") {
            $this->db->where($where);
        }
        
        $result  = $this->db->get();
        //show($this->db->last_query()); 
        if(!empty($result)) {
          return $result->result_array();
        } else {
          return false;
        }       
  }
    
    public function getDailyEarnedVasCommission($role_id,$user_id,$from_date='',$to_date='')
    { //echo"role_id: ".$role_id; echo"user: ".$user_id;
       $yesterday_date = date('Y-m-d', strtotime(' -1 day'));
        $date1  = $yesterday_date.' 00:00:00';
        $date2  = $yesterday_date.' 23:59:59';
       
        if($from_date != "" && $to_date != "")  
        {
          $date1 = $from_date;
          $date2 = $to_date;
        }
        //$where  = array('created_date >=' => $date1,'created_date <= ' => $date2);
        $where =" created_date >='".$date1."' and created_date <='".$date2."' ";
        
        if ($role_id == COMPANY_ROLE_ID) {
          $this->db->select("sum(transaction_amt + customer_charge) as final_transaction_amt, sum(company_amt ) as final_earned_commission");
          //$where['user_id'] = $user_id;
          $where .= " and user_id IN (select id from users where level_2='".$user_id."')";
        }elseif ($role_id == MASTER_DISTRIBUTOR_ROLE_ID) {
          $this->db->select("sum(transaction_amt + customer_charge) as final_transaction_amt, sum(rd_amt) as final_earned_commission");
          //$where['user_id'] = $user_id;
          $where .= " and user_id IN (select id from users where level_3='".$user_id."')";
        } elseif($role_id == AREA_DISTRIBUTOR_ROLE_ID) {
          $this->db->select("sum(transaction_amt + customer_charge) as final_transaction_amt, sum(dd_amt) as final_earned_commission");
          //$where['user_id'] = $user_id;
          $where .= " and user_id IN (select id from users where level_4='".$user_id."')";
        } elseif ($role_id == DISTRIBUTOR) {
          $this->db->select("sum(transaction_amt + customer_charge) as final_transaction_amt, sum(ex_amt) as final_earned_commission");
          //$where['user_id'] = $user_id;
          $where .= " and user_id IN (select id from users where level_5='".$user_id."')";
        }elseif ($role_id == RETAILER_ROLE_ID) {
          $this->db->select("sum(transaction_amt + customer_charge) as final_transaction_amt, sum(sa_amt) as final_earned_commission");
          //$where['user_id'] = $user_id;
          $where .= " and user_id ='".$user_id."'";
        }         
        $this->db->from("vas_commission_detail ");
          
        if ($where != "") {
            $this->db->where($where);
        }
        
        $result  = $this->db->get();
      // show($this->db->last_query(),1);

        if(!empty($result)) {
          return $result->result_array();
        } else {
          return false;
        }
   }

  //get all service type by lilawati
  public function getServiceType(){
    $result = $this->db->query("select id,service_name from service_master where status='1' and id!='10' ");
    return $result->result_array();
   }

  public function getAdCommissionDistData($from_date,$till_date){ 
   // echo" weqeqeqe"; die;
    $this->db->select("round(sum(d.commission_applicable_amt),4) as commission_applicable_amt, round(sum(c.comm_applicable_amt),4) as user_commission,
        round(SUM(`ad_comm`),4), round(SUM(`ad_actual_comm`),4),ad_id,
        (select concat(s.first_name,' ',s.last_name) as username from users s where s.id=c.ad_id limit 1) as username"); 
    $this->db->from("cb_commission_distribution c");  
    $this->db->join("users u","c.agent_code=u.agent_code","inner");
    $this->db->join(MSRTC_DB_NAME.".cb_commission_detail d","d.waybill_no=c.waybill_no","inner");
    $this->db->where('DATE_FORMAT(c.created_at , "%Y-%m-%d")>=', date('Y-m-d', strtotime($from_date)));
    $this->db->where('DATE_FORMAT(c.created_at , "%Y-%m-%d")<', date('Y-m-d', strtotime($till_date)));
   
    $this->db->group_by("ad_id");
    $result = $this->db->get();
    //print_r($this->db->last_query());
    return $result->result_array();
   }

   // insert commission data into cb_comm_distribution_deduction
  public function insertCbComDeduction($data){
    $insert = $this->db->insert('cb_comm_distribution_deduction',$data);
    return $insert;
  }
   
  public function getAdCommDistData($from_date,$till_date){
    $this->db->select("*");
    $this->db->from("cb_comm_distribution_deduction");
    $this->db->where("status","N");
    $this->db->where('DATE_FORMAT(created_at , "%Y-%m-%d")>=', date('Y-m-d', strtotime($from_date)));
    $this->db->where('DATE_FORMAT(created_at , "%Y-%m-%d")<=', date('Y-m-d', strtotime($till_date)));
    $result = $this->db->get();
    //print_r($this->db->last_query());
    return $result->result_array();
  }

  public function updateCbComDeduction($id,$data){
    $this->db->where('user_id',$id);
    $update =$this->db->update('cb_comm_distribution_deduction',$data);
    return $update;
  }
}
