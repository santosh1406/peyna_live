<?php

class Country_master_model extends MY_Model {

    var $table = 'country_master';
    var $fields = array("intCountryId", "stCountryName");
    var $key = "intCountryId";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
    
    function get_country()
    {
        $this->db->select('intCountryId,stCountryName');
        $this->db->order_by('stCountryName');
        $data = $this->db->get('country_master');
        
        return $data->result_array();
    }

    function get_insert_country_by_name($name)
    {   
        if($name != '')
        {
            $country = $this->find_by('stCountryName', $name);
            
            if($country)
            {
                return $country->intCountryId;
            }
            else
            {
                $insert_country = array('stCountryName'=> $input['country_name']);
                return $this->insert($insert_country);
            }
        }
        else
        {
            return false;
        }
    }

    function get_country_by_id($countryID)
    {
        $this->db->select('stCountryName');
        $this->db->from('country_master');
        $this->db->where('intCountryId',$countryID);
        $data = $this->db->get();
        $data= $data->result_array();

        if(!empty($data))
        {
            $country_name = $data[0]['stCountryName'] ;           
        }
        return $country_name;
    }
}    
