<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ors_agent_report_model extends CI_Model {

    function __construct() { 
        parent::__construct();
        
    }

    function summary_bos_tkt_detail_agent($input,$response_type='')
    {		
		$sub_agents = array();
		$sub_agents = get_users_for_ors_agent(); 
        
       
        $this->datatables->select(
						  '1,DATE_FORMAT(t.inserted_date, "%d-%m-%Y") AS booking_date,
                          COUNT(t.`ticket_id`) AS total_ticket,
                          SUM(t.`num_passgr`) AS total_passangers,
                          SUM(t.`total_fare_without_discount`) AS total_basic_fare,
                          SUM(t.`service_tax`) AS tax,
						  SUM(t.`op_service_charge`) AS op_service_charge,
						  SUM(at.`markup_value`) AS agent_service_tax,
						  SUM(at.`agent_commission_amount`) AS agent_commission,
						  SUM(at.`tds_value_on_commission_amount`) AS TDS,
						  SUM(t.`total_fare_without_discount`) AS total_fare,
						  SUM(t.`tot_fare_amt_with_tax`) AS amt_paid,
						  (select SUM(ctd.`cancel_charge`) from cancel_ticket_data ctd where ctd.ticket_id = t.ticket_id group by ctd.ticket_id) as cancellation_charge,
						  (select SUM(ctd.`actual_refund_paid`) from cancel_ticket_data ctd where ctd.ticket_id = t.ticket_id group by ctd.ticket_id) as refund_amt,
						  ((( SUM(t.total_fare_without_discount) + IFNULL(sum(at.`markup_value`), 0.00)) - sum(t.`tot_fare_amt_with_tax`)) - IFNULL(( SELECT (IFNULL(SUM(ctd.`refund_amt`), 0.00) - IFNULL(SUM(ctd.`actual_refund_paid`),0.00)) AS cancel_ticket_cal FROM cancel_ticket_data ctd WHERE ctd.`ticket_id` = t.`ticket_id` GROUP BY ctd.`ticket_id`),0.00)) AS profit'
						 );

        $this->datatables->from('tickets t');
        $this->datatables->join("agent_tickets at", "at.ticket_id = t.ticket_id","left");
		$this->datatables->join("users u", "u.id = t.inserted_by","left");
		if($input['ticket_type'] == "all")
			{
				$ticket_type_condition="(transaction_status='success' OR transaction_status='cancel')";
				$this->datatables->where($ticket_type_condition);
				
			}
	    else
			  $this->datatables->where("t.transaction_status", $input['ticket_type']);
        
        if(isset($input['from_date']) && $input['from_date'] != "" && isset($input['till_date']) && $input['till_date'] != "")
        {
            $this->datatables->where("DATE_FORMAT(inserted_date, '%Y-%m-%d') >= ", date('Y-m-d', strtotime($input['from_date'])));
            $this->datatables->where("DATE_FORMAT(inserted_date, '%Y-%m-%d') <= ", date('Y-m-d', strtotime($input['till_date'])));
        }
		
		if(isset($input['ors_agent_user']) && $input['ors_agent_user'] != "")
		{
			 $this->datatables->where("t.inserted_by", $input['ors_agent_user']);						 
		}
		else
		{
			if(isset($input['user_id']) && $input['user_id'] != "")
			{
				 //$this->datatables->where("t.inserted_by", $input['user_id']);
				 $this->datatables->where_in('t.inserted_by', $sub_agents);
			}			
		}
		
		if(isset($input['office_select']) && $input['office_select'] != "")
		{
			$this->datatables->where("u.office_code", $input['office_select']);
		}
		 
		 if(isset($input['agent_email']) && $input['agent_email'] != "")
        {
			 $this->datatables->where("u.email", $input['agent_email']);
		}
     
		if(isset($input['agent_mobile']) && $input['agent_mobile'] != "")
		{
			 $this->datatables->where("u.mobile_no", $input['agent_mobile']);
		}
		if(isset($input['agent_name']) && $input['agent_name'] != "")
		{
			 $this->datatables->where("u.username", $input['agent_name']);
		}
		
		if(isset($input['citynm']) && $input['citynm'] != "")
		{
			 $this->datatables->where("u.city", $input['citynm']);
		}			
		
		$this->datatables->group_by("DATE_FORMAT(inserted_date, '%Y-%m-%d')");
//        $data = $this->datatables->generate('json');
                if ($response_type != '') {					
					$data = $this->datatables->generate($response_type);									
                } else {
					$data = $this->datatables->generate();										
                }
		return $data;
        
    }	
	
	function summary_bos_tkt_detail_agent_users($input,$response_type='')
        {       $users = get_users_for_same_office();
		$this->datatables->select(
                                    '1,DATE_FORMAT(t.inserted_date, "%d-%m-%Y") AS booking_date,
                                    COUNT(t.`ticket_id`) AS total_ticket,
                                    SUM(t.`num_passgr`) AS total_passangers,
                                    SUM(t.`total_fare_without_discount`) AS total_basic_fare,
                                    SUM(t.`service_tax`) AS tax,
                                    SUM(t.`op_service_charge`) AS op_service_charge,
                                    SUM(at.`markup_value`) AS agent_service_tax,
                                    SUM(at.`agent_commission_amount`) AS agent_commission,
                                    SUM(at.`tds_value_on_commission_amount`) AS TDS,
                                    SUM(t.`total_fare_without_discount`) AS total_fare,
                                    SUM(t.`tot_fare_amt_with_tax`) AS amt_paid,
                                    (select SUM(ctd.`cancel_charge`) from cancel_ticket_data ctd where ctd.ticket_id = t.ticket_id group by ctd.ticket_id) as cancellation_charge,
                                    (select SUM(ctd.`actual_refund_paid`) from cancel_ticket_data ctd where ctd.ticket_id = t.ticket_id group by ctd.ticket_id) as refund_amt,
                                    ((( SUM(t.total_fare_without_discount) + IFNULL(sum(at.`markup_value`), 0.00)) - sum(t.`tot_fare_amt_with_tax`)) - IFNULL(( SELECT (IFNULL(SUM(ctd.`refund_amt`), 0.00) - IFNULL(SUM(ctd.`actual_refund_paid`),0.00)) AS cancel_ticket_cal FROM cancel_ticket_data ctd WHERE ctd.`ticket_id` = t.`ticket_id` GROUP BY ctd.`ticket_id`),0.00)) AS profit'
                                    );

                $this->datatables->from('tickets t');
                $this->datatables->join("cancel_ticket_data ctd", "ctd.ticket_id = t.ticket_id","left");
		$this->datatables->join("agent_tickets at", "at.ticket_id = t.ticket_id","left");
		$this->datatables->join("users u", "u.id = t.inserted_by","left");
		if($input['ticket_type'] == "all")
			{
                            $ticket_type_condition="(transaction_status='success' OR transaction_status='cancel')";
                            $this->datatables->where($ticket_type_condition);
			}
                    else{
                            $this->datatables->where("t.transaction_status", $input['ticket_type']);
                        }
                if(isset($input['from_date']) && $input['from_date'] != "" && isset($input['till_date']) && $input['till_date'] != "")
                {
                    $this->datatables->where("DATE_FORMAT(inserted_date, '%Y-%m-%d') >= ", date('Y-m-d', strtotime($input['from_date'])));
                    $this->datatables->where("DATE_FORMAT(inserted_date, '%Y-%m-%d') <= ", date('Y-m-d', strtotime($input['till_date'])));
                }
                if(isset($input['ors_agent_user']) && $input['ors_agent_user'] != "")
                {
                    $this->datatables->where('t.inserted_by', $input['ors_agent_user']);
                }
                else
                {
                    $this->datatables->where_in('t.inserted_by', $users);
                }
		
                $this->datatables->group_by("DATE_FORMAT(inserted_date, '%Y-%m-%d')");
                if ($response_type != '') {					
                        $data = $this->datatables->generate($response_type);									
                } else {
                        $data = $this->datatables->generate();										
                }
		return $data;        
        }
	
	function summary_bos_tkt_detail_agent_admin($input,$response_type='')
    {		
		$sub_agents = array();
		$sub_agents = get_users_for_ors_agent();
       
        $this->datatables->select(
						  '1,DATE_FORMAT(t.inserted_date, "%d-%m-%Y") AS booking_date,
                          COUNT(t.`ticket_id`) AS total_ticket,
                          SUM(t.`num_passgr`) AS total_passangers,
                          SUM(t.`total_fare_without_discount`) AS total_basic_fare,
                          SUM(t.`service_tax`) AS tax,
						  SUM(t.`op_service_charge`) AS op_service_charge,
						  SUM(at.`markup_value`) AS agent_service_tax,
						  SUM(at.`agent_commission_amount`) AS agent_commission,
						  SUM(at.`tds_value_on_commission_amount`) AS TDS,
						  SUM(t.`total_fare_without_discount`) AS total_fare,
						  SUM(t.`tot_fare_amt_with_tax`) AS amt_paid,
						  SUM(ctd.`cancel_charge`) AS cancellation_charge,
						  SUM(ctd.`refund_amt`) AS refund_amt,
						  (SUM(t.`total_fare_without_discount`)- SUM(t.`tot_fare_amt_with_tax`)) AS profit'
                         );

        $this->datatables->from('tickets t');
        $this->datatables->join("cancel_ticket_data ctd", "ctd.ticket_id = t.ticket_id","left");
		$this->datatables->join("agent_tickets at", "at.ticket_id = t.ticket_id","left");
		$this->datatables->join("users u", "u.id = t.inserted_by","left");
		if($input['ticket_type'] == "all")
			{
				$ticket_type_condition="(transaction_status='success' OR transaction_status='cancel')";
				$this->datatables->where($ticket_type_condition);
				
			}
	    else
			  $this->datatables->where("t.transaction_status", $input['ticket_type']);
        
        if(isset($input['from_date']) && $input['from_date'] != "" && isset($input['till_date']) && $input['till_date'] != "")
        {
            $this->datatables->where("DATE_FORMAT(inserted_date, '%Y-%m-%d') >= ", date('Y-m-d', strtotime($input['from_date'])));
            $this->datatables->where("DATE_FORMAT(inserted_date, '%Y-%m-%d') <= ", date('Y-m-d', strtotime($input['till_date'])));
        }
		
		if(isset($input['ors_agent_user']) && $input['ors_agent_user'] != "")
		{
			 $this->datatables->where("t.inserted_by", $input['ors_agent_user']);						 
		}
		else
		{
			if(isset($input['user_id']) && $input['user_id'] != "")
			{
				 //$this->datatables->where("t.inserted_by", $input['user_id']);
				 $this->datatables->where_in('t.inserted_by', $sub_agents);
			}			
		}
		
		if(isset($input['office_select']) && $input['office_select'] != "")
		{
			$this->datatables->where("u.office_code", $input['office_select']);
		}
		 
		 if(isset($input['agent_email']) && $input['agent_email'] != "")
        {
			 $this->datatables->where("u.email", $input['agent_email']);
		}
     
		if(isset($input['agent_mobile']) && $input['agent_mobile'] != "")
		{
			 $this->datatables->where("u.mobile_no", $input['agent_mobile']);
		}
		if(isset($input['agent_name']) && $input['agent_name'] != "")
		{
			 $this->datatables->where("u.username", $input['agent_name']);
		}
		
		if(isset($input['citynm']) && $input['citynm'] != "")
		{
			 $this->datatables->where("u.city", $input['citynm']);
		}			
		
		$this->datatables->group_by("DATE_FORMAT(inserted_date, '%Y-%m-%d')");
//        $data = $this->datatables->generate('json');
                if ($response_type != '') {					
					$data = $this->datatables->generate($response_type);									
                } else {
					$data = $this->datatables->generate();										
                }
		return $data;        
    }	
	
    function agent_wallet_transaction($input, $response_type = '') {
            $append1 = $append2 = '';
            $append3 = '';
            if ($input['flag'] == 0) {
                $append1 = 'wt.virtual_amt_before_trans';
                $append2 = 'wt.virtual_amt_after_trans,wt.outstanding_amount';
            }
            if ($input['flag2'] == 1) {
                $append3 = 'om.office_name';
            }
            $userIds = get_users_for_ors_agent(); // get agent and his user ids
            $userIds = implode("','", $userIds);
            $this->datatables->select('1,wt.wallet_amt_before_trans AS opening_balance,
                                           ' . $append1 . ',wt.transaction_type,ld.title,wt.amt as txn_amt,DATE_FORMAT(wt.added_on, "%d-%m-%Y") as txn_date,
                                            t.boss_ref_no,wt.wallet_amt_after_trans,' . $append2 . ',(case when wt.added_by IN (' . "'" . $userIds . "'" . ') then "Agent" else "Admin" end) as done_by,concat(u.first_name," ",u.last_name) as display_name,' . $append3 . ',wt.comment,wt.id,
                                            (select count(*) from credit_account_detail as cad where cad.user_id = wt.user_id) as credit_flag');
            $this->datatables->from('wallet_trans wt');
            $this->datatables->join("wallet w", "w.id = wt.w_id", "inner");
            $this->datatables->join("users u", "u.id = wt.added_by", "left");
            $this->datatables->join("office_master om", "u.office_code = om.office_code", "left");
            $this->datatables->join("tickets t", "t.ticket_id = wt.ticket_id", "left");
            $this->datatables->join("lookup_detail ld", "ld.id = wt.transaction_type_id", "left");
            $this->datatables->where("w.user_id", $input['user_id']);



            if (isset($input['from_date']) && $input['from_date'] != "" && isset($input['till_date']) && $input['till_date'] != "") {
                $this->datatables->where("DATE_FORMAT(wt.added_on, '%Y-%m-%d') >= ", date('Y-m-d', strtotime($input['from_date'])));
                $this->datatables->where("DATE_FORMAT(wt.added_on, '%Y-%m-%d') <= ", date('Y-m-d', strtotime($input['till_date'])));
            }
            if ($input['ticket_type'] == "all") {
                $ticket_type_condition = "(wt.transaction_type='Credited' OR wt. transaction_type='Debited')";
                $this->datatables->where($ticket_type_condition);
            } else {
                $this->datatables->where("wt.transaction_type", $input['ticket_type']);
            }
            $this->db->order_by('wt.added_on', 'asc');
//		$data = $this->datatables->generate('json');
            if ($response_type != '') {
                $data = $this->datatables->generate($response_type);
            } else {
                $data = $this->datatables->generate();
            }
            return $data;
        }

        //agent wallet transaction report for admin
	
	function agent_wallet_transaction_for_admin($input,$response_type='')
	{
                $session_user_id=$this->session->userdata('role_id');
                $this->datatables->select(
                                            '1,u.display_name,
                                            u.email,
                                            cm.stCity,
                                            wt.wallet_amt_before_trans AS opening_balance,
                                            wt.virtual_amt_before_trans AS opening_credit,
                                            wt.id,
                                            ld.title,
                                            (case when wt.transaction_type="Credited" then wt.amt else null end) as credit,
                                            (case when wt.transaction_type="Debited" then wt.amt else null end) as debit,
                                            DATE_FORMAT(wt.added_on, "%d-%m-%Y") as txn_date,
                                            (case when t.ticket_id is null then wt.topup_id else t.boss_ref_no end) as bos_ref,
                                            wt.wallet_amt_after_trans AS closing_balance,
                                            wt.virtual_amt_after_trans AS closing_credit,
                                            wt.outstanding_amount,
                                           (case when wt.added_by="'.$session_user_id.'" then "Admin" else "Agent" end) as added_by_user,
                                            wt.comment' 
                                            );

                $this->datatables->from('wallet_trans wt');
                $this->datatables->join("tickets t", "t.ticket_id = wt.ticket_id","left");
                $this->datatables->join("lookup_detail ld", "ld.id = wt.transaction_type_id","left");
                $this->datatables->join("wallet w", "w.id = wt.w_id","inner");
                $this->datatables->join("users u", "u.id = w.user_id","inner");
                $this->datatables->join("city_master cm", "u.city = cm.intCityId","inner");
                if(isset($input['from_date']) && $input['from_date'] != "" && isset($input['till_date']) && $input['till_date'] != "")
                {		
                    $this->datatables->where("DATE_FORMAT(wt.added_on, '%Y-%m-%d') >= ", date('Y-m-d', strtotime($input['from_date'])));
                    $this->datatables->where("DATE_FORMAT(wt.added_on, '%Y-%m-%d') <= ", date('Y-m-d', strtotime($input['till_date'])));
                }
                if($input['ticket_type'] == "all")
                {
                    $ticket_type_condition="(wt.transaction_type='Credited' OR wt. transaction_type='Debited')";
                    $this->datatables->where($ticket_type_condition);	
                }
                else
                {
                    $this->datatables->where("wt.transaction_type", $input['ticket_type']);
                }
		if(isset($input['select_agent']) && $input['select_agent'] != "")
		{ 
				$this->datatables->where("w.user_id",$input['select_agent']);	
		}
		
		if(isset($input['select_city']) && $input['select_city'] != "")
		{ 
				$this->datatables->where("u.city",$input['select_city']);	
		}
		
		$this->db->order_by('wt.added_on', 'desc');
//		$data = $this->datatables->generate('json');
                if ($response_type != '') {
                    $data = $this->datatables->generate($response_type);
                } else {
                    $data = $this->datatables->generate();

                }
		return $data;
        
	}
 }

?>