<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Payment_gateway_commission_model extends MY_Model {

    var $table = 'payment_gateway_commission';
    var $fields = array("id", "commission_name", "from", "till", "commission_tds_flag", "added_on", "added_by", "updated_on", "updated_by", "status");
    var $key = 'id';
    protected $set_created = false;
    protected $set_modified = false;

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    public function pg_commission_detail() {
		$this->db->select("id,payment_gateway_name");
		$this->db->from("payment_gateway_master");
		$this->db->where("status",1);
		$query = $this->db->get();
        return $query->result_array(); 
    }

    public function insert_slab_wise_commission($data = array()) {
        $ret = true;
        if(!empty($data['master']) && $data['details']) {
            $parset1 = implode("','", $data['master']);
            $this->db->trans_begin(); 
			
            $s = "insert into payment_gateway_commission (commission_name,`from`,`till`,commission_tds_flag,service_tax,commission_with_service_tax,added_by, added_on,status) values ('".$parset1."',now(),'1')";
            $this->db->query($s);
            $commission_id = $this->db->insert_id();

            if ($this->db->trans_status() === FALSE || empty($commission_id)) {
                $this->db->trans_rollback();
                $ret = false;
            }
            else {
                foreach($data['details'] as $operators) {
					$payment_id = $operators['payment_id'];
					$bos_commission = $operators['bos_commission'];
					$agent_commission = $operators['agent_commission'];
					
					$s = "insert into payment_gateway_commission_detail (commission_id,payment_gateway_id,commission_type,
					payment_gateway_commission,bos_commission,total_commission,added_on) values($commission_id,$payment_id,
					'percentage',$agent_commission,$bos_commission,100,now())";

					$this->db->query($s);
                }
                $this->db->trans_commit();
            }
        }
        return $ret;
    }

    public function commission_detail_by_id($commission_id) {
        $this->db->select("cd.id AS commission_detail_id,
						   cd.bos_commission,
						   cd.payment_gateway_commission,
						   cd.total_commission,
						   cd.commission_type as commission_type,
						   cd.commission_value,
                           c.id AS commission_id,
                           c.commission_name AS commission_name,
                           c.from AS commission_from,
                           c.till AS commission_till,
                           c.commission_with_service_tax,
                           c.service_tax,
                           c.commission_tds_flag
                          ");
        $this->db->from("payment_gateway_commission_detail cd");
        $this->db->where("cd.commission_id",$commission_id);
        $this->db->join("payment_gateway_commission c", "c.id = cd.commission_id ", "inner");
        $query = $this->db->get();
        return $query->result();  
    }

}

?>