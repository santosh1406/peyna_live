<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Routes_model extends MY_Model {

    /**
     * Instanciar o CI
     */
    var $table = 'routes';
    var $fields = array("route_id", "route_no", "from_stop", "end_stop", "km", "road_type", "from_stop_id", "end_stop_id", "record_status", "is_approved", "op_id");
    var $key = 'route_id';

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    public function route_info($route_no) {
        
        $this->db->select("rs.km,r.route_no,rs.seq_no,rs.bus_stop_id,r.from_stop,r.from_stop_id,r.end_stop,r.end_stop_id,rs.bus_stop_name");
        $this->db->from("routes r");
        $this->db->join("route_stops rs", 'r.route_id=rs.route_id', "inner");

        $this->db->where("r.route_no", $route_no['route_value']);
        $this->db->where("r.record_status", 'Y');
        $this->db->where("r.is_approved", 'N');
        $query = $this->db->get();
        
        return $query->result_array();
    }

    public function all_select_route() {

        $this->db->select("r.route_no,r.from_stop,r.from_stop_id,r.end_stop,r.end_stop_id");
        $this->db->from("routes r");
        $this->db->where("r.record_status", 'Y');
        //$this->db->where("r.is_approved",'Y');
        //$this->db->join("route_stops rs",'r.route_id=rs.route_id', "inner");


        $query = $this->db->get();
        //show($this->db->last_query(),1);
        return $query->result_array();
    }

    /* Get bus stop name */

//    function get_busstop_name($bus_stop_cd) {
//        $get_busstop_name = $this->op_stop_master_model->get_busstop_name($bus_stop_cd);
//    }
//    function get_busstop_id($bus_stop_cd) {
//        $get_busstop_id = $this->bus_stop_master_model->get_busstop_id($bus_stop_cd);
//    }

    function check_exist_route() {
        $input = $this->input->post();
        $this->db->select('from_stop,end_stop');
        $this->db->where("from_stop", $input['from']);
        $this->db->where("end_stop", $input['to']);
        $this->db->from('routes');
        $query = $this->db->get();
        $data = $query->row();
        $cnt = count($data);
        return $cnt;
    }

    function update_approved_status() {
        $input = $this->input->post();
        $id = explode(',', $input['id']);
        $id_status = explode(',', $input['id_status']);
        $date = date('Y-m-d H:i:s');
        $user_id = $this->session->userdata('user_id');

        foreach ($id as $key => $value) {
            /* Check entry in table: chart_master starts here */
            //echo "id" . $value . '<br>';
            //echo "no" . $id_status[$key];
            if ($input['button_action'] == 'Approve') {
                $res = $this->chart_model->chk_route_chart_master($value, $id_status[$key]);
                if ($res['status'] == 1) {
                    /* Check entry in table: chart_details starts here */
                    $res_chart_details = $this->chart_details_model->chk_route_chart_details($value, $id_status[$key]);
                    //print_r($res_chart_details);
                    if ($res_chart_details['update'] == 0) {
                        /* if chart_master & chart_details are updated approve route here */
                        //update previous route record_status to "N"

                        $data = array("record_status" => 'N');
                        $this->db->where_in('route_id', $value);
                        $this->db->update('routes', $data);

                        $data = array("status" => 'N', "modified_by" => $user_id);
                        $this->db->where('route_id', $value);
                        $this->db->update('route_stops', $data);

                        $data = array("is_approved" => 'Y');
                        $this->db->where('route_no', $id_status[$key]);
                        $this->db->update('chart_master', $data);

                        /* insert new route record  in table: routes with record_status='Y' and is_approved='Y' */
                        $this->db->select('route_no,from_stop,from_stop_id,end_stop,end_stop_id,km,road_type');
                        $this->db->from('routes');
                        $this->db->where_in('route_id', $value);
                        $query = $this->db->get();
                        if ($query->num_rows()) {
                            $new_route = $query->result_array();
                            foreach ($new_route as $row => $route) {
                                $this->routes_model->route_no = $route['route_no'];
                                $this->routes_model->from_stop = $route['from_stop'];
                                $this->routes_model->from_stop_id = $route['from_stop_id'];
                                $this->routes_model->end_stop = $route['end_stop'];
                                $this->routes_model->end_stop_id = $route['end_stop_id'];
                                $this->routes_model->km = $route['km'];
                                $this->routes_model->record_status = 'Y';
                                $this->routes_model->is_approved = 'Y';
                                $this->routes_model->save();
                            }
                            $insert_id = $this->db->insert_id();
                            /* insert new record in route_stops */
                            $res_route_details = $this->route_stop_model->get_route_stop_deatils($value);



                            foreach ($res_route_details as $value) {
                                //echo $value['bus_stop_id'];
                                $this->route_stop_model->route_id = $insert_id;

                                $this->route_stop_model->bus_stop_id = $value['bus_stop_id'];
                                $this->route_stop_model->bus_stop_name = $value['bus_stop_name'];
                                $this->route_stop_model->seq_no = $value['seq_no'];
                                $this->route_stop_model->status = 'Y';
                                $this->route_stop_model->km = $value['km'];
                                $this->route_stop_model->inserted_date = $date;
                                $this->route_stop_model->inserted_by = $user_id;
                                $this->route_stop_model->save();
                            }
                        }
                    } else {

                        $data_chart_details['chart_details_cnt'] = 0;
                        $data_chart_details['route_no'] = $res['route_no'];
                        $data_return =  $data_chart_details;
                    }

                    /* Check entry in table: chart_details ends here */
                } else {
                    $data_chart_master['chart_master_cnt'] = 0;
                    $data_chart_master['route_no'] = $res['route_no'];
                    $data_return =  $data_chart_master;
                }
            } else {
               // echo "reject";
              
                $data = array("is_approved" => 'N');
                $this->db->where_in('route_id', $value);
                $this->db->update('routes', $data);
                
                $data = array("is_approved" => 'N');
                $this->db->where_in('route_no', $id_status[$key]);
                $this->db->update('chart_master', $data);
                $data_update['msg'] = 'done';
                $data_return =  $data_update;
               
            }
        }
        return $data_return;
    }

    public function list_select_route($route_no) {

        $this->db->select("r.route_no as label, r.route_no as value ");
        $this->db->like('r.route_no', $route_no['q']);
        $this->db->from("routes r");
        $this->db->where("r.record_status", 'Y');
        //    $this->db->where("r.is_approved",'Y');
        $query = $this->db->get();
        return $query->result_array();
    }

    
    function get_route_data($input_data) {
        
            $this->db->select("*");
            $this->db->from("routes r");
            $this->db->like("from_stop",$input_data['from_stop']); 
            $this->db->like("end_stop",$input_data['end_stop']); 
            $this->db->where("r.record_status", 'Y');
            $role_result = $this->db->get();
            return $role_result->result_array();
    }
    
      public function bus_service_route_info($route_no) {
     
        $this->db->select("rs.km,r.route_no,rs.seq_no,rs.bus_stop_id,r.from_stop,r.from_stop_id,r.end_stop,r.end_stop_id,rs.bus_stop_name");
        $this->db->from("routes r");
        $this->db->join("route_stops rs", 'r.route_id=rs.route_id', "inner");
   
        $this->db->where("r.route_no", $route_no['route_value']);
       // $this->db->where("r.route_no",'365');
        $this->db->where("r.record_status", 'Y');
        $this->db->where("r.is_approved", 'N');
        $this->db->order_by("rs.seq_no", "asc"); 
        $query = $this->db->get();
    //    show($this->db->last_query(),1);
        return $query->result_array();
    }
   
    public function only_route_info($route_no) {
        
        $this->db->select("r.km,r.route_no,r.route_id,r.from_stop,r.from_stop_id,r.end_stop,");
        $this->db->from("routes r");
        $this->db->where("r.route_no", $route_no['route_no']);
        $this->db->where("r.record_status", 'Y');
        $this->db->where("r.is_approved", 'N');
        $query = $this->db->get();
        
        return $query->result_array();
    }
}
