<?php

class Payment_gateway_wallet_model extends MY_Model {

    var $table = 'payment_gateway_wallet';
    var $fields = array("id", "payment_gateway_id", "amount", "last_transaction_id", "status", "added_on", "added_by","updated_on","updated_by");
    var $key = 'id';
    protected $set_created = false;
    protected $set_modified = false;

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    function update_topup($data) {
        $this->db->where('payment_gateway_id', $data["payment_gateway_id"]);
        //$this->db->update("users", $userdata["user_array"]);
        // die($this->db->last_query());

        if ($this->db->update("payment_gateway_wallet", $data["wallet_updation_array"])) {
            return true;
        } else {
            return false;
        }
    }
}

?>