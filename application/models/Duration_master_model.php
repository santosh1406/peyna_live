<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Duration_master_model extends MY_Model
{
    var $table    = 'duration_master';
    var $fields		= array("id", "duration", "is_status", "is_deleted", "created_by", "created_date", "updated_by", "updated_date");
    var $key    = 'id';
    
    public function __construct() {
        parent::__construct();
        $this->_init();      
        
    }
}
