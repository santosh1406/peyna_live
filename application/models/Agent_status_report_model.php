<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of users
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agent_status_report_model extends MY_Model {
	
	public function genAgentStatusReport($input) {
    
        $this->db->select('UM.id AS agent_id,
              UM.agent_code AS agent_code,
              UM.username AS agent_username,
              UM.first_name AS f_name,
              UM.last_name AS l_name,
              concat(UM.address1, " ", UM.address2) as address, 
              RSM.terminal_code AS terminal_code,
              DP.REGION_CD AS region,
              RE.REGION_NM AS region_name,
              DV.DIVISION_CD AS division,
              DP.DEPOT_NM AS depot,
              DP.DEPOT_CD AS depot_cd,
               DATE_FORMAT(RSM.created_at,"%d-%m-%Y") AS terminal_code_generate_date,
                CASE WHEN UM.kyc_verify_status = "Y" THEN "Yes" ELSE "No" END AS kyc_verify_status,
             CASE WHEN UM.email_verify_status = "Y" THEN "Yes" ELSE "No" END AS email_verify_status, 
               CASE WHEN UM.verify_status = "Y" THEN "Yes" ELSE "No" END AS verify_status, 
               CASE WHEN UM.status = "Y" THEN "Yes" ELSE "No" END AS status, 
              CASE WHEN UM.otp_verify_status = "Y" THEN "Yes" ELSE "No" END AS mobile_verify_status,
              DATE_FORMAT(UM.created_date,"%d-%m-%Y") As signup_date',FALSE);
        $this->db->from('retailer_service_mapping AS RSM');
        $this->db->join('users As UM', 'UM.id = RSM.agent_id', 'INNER');
        $this->db->join('msrtc_replication.depots AS DP', 'DP.DEPOT_CD = RSM.depot_code', 'LEFT');
        $this->db->join('msrtc_bos_dev.regions AS RE', 'RE.REGION_CD = DP.REGION_CD', 'LEFT');
        $this->db->join('msrtc_replication.divisions AS DV','DV.DIVISION_CD = DP.DIVISION_CD', 'LEFT');
        $this->db->where('RSM.service_id = "13"');
        $this->db->where('RSM.status = "Y"');
        $this->db->where('RSM.terminal_code IS NOT NULL');

        if (isset($input['from_date']) && $input['from_date'] != '') {
            $this->db->where("DATE_FORMAT(UM.created_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['to_date']) && $input['to_date'] != '') {
            $this->db->where("DATE_FORMAT(UM.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }
        $resQuery = $this->db->get();
        // last_query();
      // die();
       $resArr = $resQuery->result_array();
     
        return $resArr;
    }
function getRegionDetail(){
  $this->db->select('REGION_CD, REGION_NM');
       $this->db->from('msrtc_bos_dev.regions');
       $this->db->where('STATE_CD = "MHR"');
       $this->db->where('REGION_CD != "PG"');
        $resQuery = $this->db->get();
       //  last_query();
       $resArr = $resQuery->result_array();
     
        return $resArr;
}
   
}
?>
