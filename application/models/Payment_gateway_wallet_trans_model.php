<?php

class Payment_gateway_wallet_trans_model extends MY_Model {

    var $table = 'payment_gateway_wallet_trans';
    var $fields = array("id", "pg_wallet_id", "amount","amount_paid_by_vle","comment", "amt_before_trans", "amt_after_trans", "ticket_id","return_ticket_id","transaction_type_id","transaction_type","added_on");
    var $key = 'id';
    protected $set_created = false;
    protected $set_modified = false;

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
    
    /* Author   : Rajkumar
     * Function : get_csc_booked_ticket
     * Param    : 
     * Desc     : Ticket Unsuccessfull booked and amount refunded or not checking.
     */
    function  get_csc_booked_ticket($tick_id)
    { 
        $this->db->select("id,pg_wallet_id,amount,amount_paid_by_vle,comment,amt_before_trans, amt_after_trans,ticket_id,return_ticket_id,transaction_type_id,transaction_type");
        $this->db->from("payment_gateway_wallet_trans");
        $this->db->where("ticket_id", $tick_id);
        $this->db->or_where("return_ticket_id", $tick_id);
        $this->db->where("transaction_type_id",5);
        $this->db->where("transaction_type_id !=",8);
        $wallet_ticket = $this->db->get();

        if($wallet_ticket -> num_rows() > 0)
        {
            return $wallet_ticket->result();
        }
        return false;
    }
}

?>