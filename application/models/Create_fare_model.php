<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Create_fare_model extends MY_Model {

    var $table  = 'wallet';
    var $fields = array("id","w_id","amt","comment","status","user_id","added_by","added_on");
    // var $fields = "id,w_id,user_id,amt,status,expire_at,comment,added_by,added_on";
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

}
