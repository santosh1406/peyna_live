<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bus_service_stops_model extends MY_Model {

    var $table  = 'bus_service_stops';
    var $fields = array("id","op_id","trip_no","op_trip_no","from_bus_stop_code","from_bus_stop_name","to_bus_stop_code","to_bus_stop_name","arrival_tm","departure_tm","arrival_day","fare","senior_fare","child_fare","is_status","is_deleted","created_by","created_date","updated_by","updated_date");
    var $key    = 'id'; 
   
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

    public function get_bus_service_number()
    {
		$this->db->select('tm.op_id,t.trip_no,tm.op_trip_no,t.sch_departure_date',false);
        $this->db->from('travel_destinations t');
        $this->db->join('trip_master tm', 'tm.trip_no = t.trip_no', "inner");
        $this->db->where("tm.op_id",3);
        $this->db->group_by(["tm.op_trip_no","t.trip_no","t.sch_departure_date"]);
        $this->db->order_by("sch_departure_date DESC");
        $data = $this->db->get();
        return $data->result_array();
    }

    public function get_bus_service_stops_by_op_id($input, $true_false = true)
    {
        $this->db->select("trip_no,from_bus_stop_code,from_bus_stop_name,to_bus_stop_code,to_bus_stop_name,date_format(departure_tm,'%H:%i') as from_time,date_format(arrival_tm,'%H:%i') as to_time",false);
        $this->db->from('bus_service_stops');
        $this->db->where("op_id",3);
        $this->db->where("op_trip_no",$input["op_trip_no"]);
        if($true_false)
        {
            $this->db->like("departure_tm",$input["date"],"both");
        }
        $data = $this->db->get();
        return $data->result_array();
    }

    public function get_from_bus_service_stops_by_op_id($input, $true_false = true)
    {
        $this->db->distinct();
        $this->db->select("trip_no,from_bus_stop_code,from_bus_stop_name,date_format(departure_tm,'%H:%i') as from_time",false);
        $this->db->from('bus_service_stops');
        $this->db->where("op_id",3);
        $this->db->where("op_trip_no",$input["op_trip_no"]);
        if($true_false)
        {
            $this->db->like("departure_tm",$input["date"],"both");
        }
        $data = $this->db->get();
        return $data->result_array();
    }


    public function get_to_bus_service_stops_by_op_id($input, $true_false = true)
    {
        $this->db->distinct();
        $this->db->select("trip_no,to_bus_stop_code,to_bus_stop_name,date_format(arrival_tm,'%H:%i') as to_time",false);
        $this->db->from('bus_service_stops');
        $this->db->where("op_id",3);
        $this->db->where("op_trip_no",$input["op_trip_no"]);
        if($true_false)
        {
            $this->db->like("departure_tm",$input["date"],"both");
        }
        $data = $this->db->get();
        return $data->result_array();
    }


    public function get_service_stop_details($trip_no,$op_trip_no,$from_code,$to_code)
    {
        $this->db->select("*",false);
        $this->db->from('bus_service_stops');
        $this->db->where("trip_no",$trip_no);
        $this->db->where("from_bus_stop_code",$from_code);
        $this->db->where("to_bus_stop_code",$to_code);
        $this->db->limit('1');
        $data = $this->db->get();
        return $data->result_array();
    }


    public function get_seat_availability_details($trip_no,$op_trip_no,$op_id)
    {
        $this->db->select("bss.arrival_day as day,tm.seat_layout_id,obt.is_child_concession",false);
        $this->db->from('bus_service_stops bss');
        $this->db->join('trip_master tm','tm.trip_no = bss.trip_no','inner');
        $this->db->join('op_bus_types obt','obt.id = tm.op_bus_type_id','inner');
        $this->db->where("bss.trip_no",$trip_no);
        $this->db->where("bss.op_trip_no",$op_trip_no);
        $this->db->where("bss.op_id",$op_id);
        $this->db->limit('1');
        $data = $this->db->get();
        return $data->result_array();
    }

}
