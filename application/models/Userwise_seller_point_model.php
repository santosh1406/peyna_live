<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Userwise_seller_point_model extends My_Model {

	protected $table='userwise_seller_point';
	var $fields = array("id","user_id","seller_code","depot_cd","status","created_date","updated_date");
	var $key = "id";

	public function __construct(){
		parent::__construct();
		$this->load->model(array('Userwise_seller_point_model'));
	}


} 

?>