<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Assign_package_log_model extends MY_Model {

	public $variable;
	protected $table = 'assign_package_log';
    var $fields = array("id", "user_id", "service_id", "package_id", "status","created_by","pkg_delete_status","updated_date","created_at");
    var $key = 'id';

	public function __construct()
	{
       parent::__construct();
	}
   public function packageDetailsForMD($id)
   {
	   	if($id)
	   	{
          $this->db->select("package_id");
	        $this->db->from("assign_package_log");
	        $this->db->where("user_id =",$id);
	        $this->db->where("service_id =",MSRTC_SERVICE_ID);
          $this->db->order_by('id','desc');
          $this->db->limit(1);
	        $user_result = $this->db->get();
	        $user_result_array = $user_result->result_array();
          return $user_result_array;
	   	}
	   	else
	    {
	    	return false;
	    }
   }
   public function defaultPackageId()
   {
   	    $now = date('Y-m-d', time());

   	    $this->db->select("id");
        $this->db->from("package_master");
        $this->db->where("default =",'1');
        $this->db->where("status =",'1');
        $this->db->where("is_deleted =",'0');
        // $this->db->where("DATE_FORMAT(till, '%Y-%m-%d') >=", $now); 
        $this->db->order_by('id','desc');
        $this->db->limit(1);
        $user_result = $this->db->get();

        $user_result_array = $user_result->result_array();
        return $user_result_array;
   }
       function updateDeletePkg($id,$data) {

        $this->db->where('package_id', $id);
        $this->db->update('assign_package_log', $data);
        return $this->db->affected_rows();
    }

    public function update_expired_pkg($m_id) {
      $now = date('Y-m-d', time());
      $this->db->query("update assign_package_log apl
                        JOIN package_master pm
                        ON pm.id = apl.package_id
                        set apl.pkg_delete_status = '1'
                        where apl.pkg_delete_status = '0'
                        and apl.user_id = $m_id
                        and pm.till < NOW()
                        ");
    }
}

