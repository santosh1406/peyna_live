<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Matm_transaction_model extends MY_Model
{    
    public function __construct()
    {
        parent::__construct();
        $this->_init();
    }

    public function insert_data($table,$data)
    {
        //show($data,1);
        if($data['SERVICEID']==MATM_CASH_WITHDRAW)
        {
            $data['Service_name'] = 'Cash Withdraw';
        }
        elseif($data['SERVICEID']==MATM_BALANCE_ENQUIRY)
        {
            $data['Service_name'] = 'Balance Enquiry';
        }
        elseif($data['SERVICEID']==MATM_TRANSACTION_STATUS)
        {
            $data['Service_name'] = 'Transaction Status';
        }
        
        $data['created_by'] = $this->session->userdata('user_id');
        
        $this->db->insert($table, $data);      
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/micro_atm/' . 'matm.log', $data, 'Input-Operator');
    }
    
    public function get_last_trans_id() {
        $this->db->select('id');
        $this->db->where('created_by', $this->session->userdata('user_id'));
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('micro_atm_transaction_request', 1, 0);

        // echo $this->db->last_query();
        return $query->row(0);
    }
    
    public function insert_settlement_data($batch_data) {
        if (!empty($batch_data)) {
            $this->db->insert_batch('matm_settlement_sheet', $batch_data);
            return $this->db->affected_rows();
        }
    }

    public function insert_agentsettlement_data($batch_data) {
        if (!empty($batch_data)) {
            $this->db->insert_batch('matm_agent_settlement', $batch_data);
            return $this->db->affected_rows();
        }
    }

    public function update_agent_id() {
        $query_str = "UPDATE `matm_settlement_sheet` mss
            INNER JOIN  micro_atm_transaction_request mt
            SET mss.agent_id = mt.created_by
            WHERE mss.agent_id = 0
            AND mss.client_txn_id = mt.ClientRefID";

        $query = $this->db->query($query_str);
        return $this->db->affected_rows();
    }

    public function get_trans_amount($start_date, $end_date) {
        $sql = "SELECT agent_id,txn_date, SUM(txn_amount) as txn_amount FROM matm_settlement_sheet
            WHERE txn_date >= '$start_date' AND txn_date < '$end_date'
            AND txn_mode LIKE '%MATM Withdrawal%' AND txn_status = 0
            GROUP BY agent_id,txn_date";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else
            return FALSE;
    }

    public function get_matm_transfer_amount($limit = NULL, $start = NULL, $where = '') {
        $this->db->select('*');
        $this->db->from('matm_transfer_amount');

        if ($where) {
            $this->db->where($where);
        }

        if (isset($start) && isset($limit)) {
            $this->db->limit($limit, $start);
        }

        $this->db->order_by('transaction_date', 'DESC');

        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else
            return FALSE;
    }

    public function get_user_details($user_id = null) {
        if ($user_id) {
            $sql = 'SELECT id, user_id, amt FROM wallet WHERE user_id = "' . $user_id . '"';
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                return $query->row_array();
            } else
                return FALSE;
        }
        return false;
    }
    
    public function update_user_wallet($new_amount, $user_id){
        $queryStr = "UPDATE wallet SET
            amt = '" . $new_amount . "',
            updated_date = '" . date("Y-m-d H:i:s") . "'
            WHERE user_id = $user_id
        ";

        $query = $this->db->query($queryStr);
        return $this->db->affected_rows();
    }
    
    public function get_all_rows($limit = NULL, $start = NULL, $where = '') {
        $this->db->select('mas.*, u.display_name');
        $this->db->from('matm_agent_settlement mas');
        $this->db->join('users u', 'u.id = mas.user_id');

        if ($where) {
            $this->db->where($where);
        }

        if (isset($start) && isset($limit)) {
            $this->db->limit($limit, $start);
        }

        $this->db->order_by('id', 'DESC');

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else
            return FALSE;
    }
    
     public function update_cron_status_for_transfer_amount($col, $val, $where){
        if ($where) {
            $this->db->where($where);
            return $this->db->update('matm_transfer_amount', array($col => $val));
        }
    }
    
    public function update_status($id, $status, $comment,$trans_no){
        $queryStr = "UPDATE matm_agent_settlement SET  `status` = '" . $status . "', `comment` =  '" . $comment . "', transaction_no = $trans_no WHERE `id` = $id";
        $query = $this->db->query($queryStr);
        return $this->db->affected_rows();
    }
    
    public function get_user_amount_by_date($date){
        if ($date) {
             $queryStr = "SELECT * FROM matm_settlement_sheet WHERE `transaction_date` = '$date' and user_id != 0  and status != 'Y'";
           // $queryStr = "SELECT * FROM matm_agent_settlement WHERE `transaction_date` = '$date' and user_id != 0";
            $query = $this->db->query($queryStr);
            $result = $query->result_array();
            return $result;
        }
        return false;
    }
    
    public function get_total_amount($where, $condition) {
        $this->db->select('SUM(amount) as amount');
        $this->db->from('matm_agent_settlement');

        if ($where) {
            $this->db->where($where);
        }
        if ($condition) {
            $this->db->where($condition);
        }

        $query = $this->db->get();
// show($this->db->last_query(),1);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result['amount'];
        } else
            return FALSE;
    }

    public function get_trimax_order_ids($agent_id, $trans_date) {
        $queryStr = "SELECT *
            FROM `matm_settlement_sheet` ess
            INNER JOIN `micro_atm_transaction_response` et
            ON et.ClientRefID  = ess.client_txn_id	
            INNER JOIN users 
            ON users.id = `ess`.`agent_id`
            WHERE `ess`.`agent_id` = $agent_id
            AND `ess`.`txn_date` = '$trans_date'
            AND ess.txn_mode ='MATM Withdrawal'
            AND et.TxnStatus = 'Card Transaction Successfully'
            AND ess.txn_status ='0'
            ORDER BY `ess`.`id` DESC";

        $query = $this->db->query($queryStr);
//        return $this->db->last_query();
        $result = $query->result_array();
        return $result;
    }

    public function insert_transac_amount($data) {
        $this->db->insert('matm_transfer_amount', $data);
        return $this->db->insert_id();
    }

    public function get_count($where = '') {
        $this->db->select('id');
        $this->db->from('matm_transfer_amount');

        if ($where) {
            $this->db->where($where);
        }

        return $this->db->count_all_results();
    }

    public function get_amt_transfer_info($where) {
        if ($where) {
            $this->db->select('*');
            $this->db->from('matm_transfer_amount');
            $this->db->where($where);

            $query = $this->db->get();

            if ($query->num_rows() > 0) {
                return $query->row_array();
            } else
                return FALSE;
        }
        return false;
    }
    
     public function check_data_exist($client_unique_Id,$date) {
        if(!empty($client_unique_Id)){
           $this->db->select('id');
           $this->db->from('matm_settlement_sheet');
           $this->db->where('client_txn_id',$client_unique_Id);
           $this->db->where('date',$date);
           $query = $this->db->get();
//           show($this->db->last_query(),1);
           if ($query->num_rows() > 0) {
               return '1';
           } else
               return '0';
           }
    }
    
    public function checkDataExists($user_id,$date,$amt) {
        if(!empty($user_id)){
           $this->db->select('id');
           $this->db->from('matm_agent_settlement');
           $this->db->where('user_id',$user_id);
           $this->db->where('transaction_date',$date);
           $this->db->where('amount',$amt);
           $query = $this->db->get();

           if ($query->num_rows() > 0) {
               return $query->row_array();
           } else
               return '0';
        }
    }
    
    public function getTransactionReceipt($user_id,$limit='1', $start='0'){
        $this->db->select('treq.id,u.display_name,u.username,tres.TxnAmt,tres.TransactionDatetime,tres.RRN,tres.CardNumber,treq.Service_name,wt.transaction_no');
        $this->db->from('micro_atm_transaction_request treq');
        $this->db->join('micro_atm_transaction_response tres','treq.id = tres.Transaction_id','inner');  
        $this->db->join('vas_commission_detail cd','tres.created_by = cd.user_id','inner');
        $this->db->join('users u','u.id = cd.user_id','inner');
        $this->db->join('wallet_trans wt','wt.transaction_no = tres.WalletTransTxnNo','inner');
        $this->db->where('tres.created_by',$user_id);      
        $this->db->where('treq.SERVICEID',MATM_CASH_WITHDRAW);
        $this->db->where('cd.service_id',MICRO_ATM_SERVICE_ID);
        $this->db->where('tres.TxnStatus','Card Transaction Successfully');
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        //last_query(1);
       // echo $this->db->last_query();
        return $query->result_array();  
    }

}



