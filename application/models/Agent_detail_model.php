<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agent_detail_model extends MY_Model {
    
    var $table  = 'agent_details';
    var $fields = array("agent_detail_id","agent_id","agent_sms_status","agent_email_status","agent_daily_max_amt_limit","agent_topup_amt_limit","agent_security_deposit_amt","agent_cap_amount","agent_total_available_credit_amount","account_type","prepaid_minimum_level","prepaid_maximum_level","prepaid_threshold_level","postpaid_limit","postpaid_period_type_id","agent_applicable_for","agent_registration_date","agent_expiry_date","agent_smart_card_code");
    var $key    = 'agent_detail_id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
    public function add_agent_details($data){
        
      $this->db->insert('agent_details',$data);
      return $this->db->affected_rows();
        
    }
    public function edit_agent_details($data,$agent_id)
    {
    $this->db->where('agent_id', $agent_id);
    $this->db->update('agent_details', $data); 
    return $this->db->affected_rows();
    } 
    public function delete_agent_details($agent_id)
    {
    $data=array('is_deleted'=>'1');    
    $this->db->where('agent_id', $agent_id);
    $this->db->update('agent_details',$data); 
    return $this->db->affected_rows();   
        
    }
    public function get_agent_details()
    {
        $this->db->select("ad.agent_detail_id,ad.agent_id,ad.agent_sms_status,ad.agent_email_status,ad.agent_daily_max_amt_limit,ad.agent_topup_amt_limit,ad.agent_security_deposit_amt,ad.agent_cap_amount,ad.account_type,ad.prepaid_minimum_level,ad.prepaid_maximum_level,ad.prepaid_threshold_level,ad.postpaid_limit,ad.postpaid_period_type_id,ad.agent_applicable_for,ad.agent_registration_date,ad.agent_expiry_date,ad.agent_smart_card_code");
        $this->db->from('agent_details ad');
        $this->db->join('agent_master am', 'agent_id');
        $this->db->where('ad.is_deleted', 0);
        $query = $this->db->get();
      //print_r($query);exit;
       return $query->result_array();  
    }
    
    public function get_agent($agent_id)
    {
       $query = $this->db->get_where('agent_details', array('agent_id' =>$agent_id));  
       return $query->result();
    }
   
     

}
?>
