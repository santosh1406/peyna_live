<?php

class Smart_card_model extends MY_Model{
    public function __construct() {
        parent::__construct();
       // $this->_init();      
    }
    
    public function get_service_pass_reg_data($filter) {
        
        $this->db->select("
                            u.agent_code,
                            concat(u.first_name,' ',u.last_name) as agent_name,
                            d.DEPOT_CD,
                            d.DEPOT_NM,
                            DATE_FORMAT(cm.created_date, '%d-%m-%Y') as reg_date,
                            ccm.name_on_card,                            
                            DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(cm.date_of_birth, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(cm.date_of_birth, '00-%m-%d')) AS dob,
                            IF(ccm.trimax_card_id IS NULL or ccm.trimax_card_id = '', 'NA', ccm.trimax_card_id) as cust_card_id,
                            IF(ccm.card_status='D','Issued','Not Issued') as card_status,
                            ctd.from_stop_code,
                            ctd.till_stop_code,
                            ct.amount as pass_amount,
                            max(CASE WHEN atd.amount_code = '".SMART_CARD_FEE_CD."' THEN atd.amount ELSE 0.00 END) as smart_card_fee,
                            max(CASE WHEN atd.amount_code = '".APPLICATION_FEE."' THEN atd.amount ELSE 0.00 END) as application_fee,
                            max(CASE WHEN atd.amount_code = '".GST_FEE."' THEN atd.amount ELSE 0.00 END) as gst_fee,
                            at.total_amount,
                            DATE_FORMAT(ct.activation_date, '%d-%m-%Y') as act_date,
                            DATE_FORMAT(ct.expiry_date, '%d-%m-%Y') as exp_date
                        ");
        $this->db->from('ps_customer_master cm');
        $this->db->join('ps_customer_card_master ccm', 'ccm.customer_id = cm.id', 'left');
        $this->db->join('ps_card_transaction ct', 'ct.trimax_card_id = ccm.trimax_card_id', 'innner');
        $this->db->join('ps_card_transaction_details ctd', 'ctd.card_transaction_id = ct.id', 'inner');
        $this->db->join('ps_amount_transaction at', 'at.card_transaction_id = ct.id', 'inner');
        $this->db->join('ps_amount_transaction_details atd', 'atd.amount_transaction_id = at.id', 'inner');       
        $this->db->join('users u', 'u.id = ct.created_by', 'inner'); 
        $this->db->join('retailer_service_mapping rsm','u.id = rsm.agent_id','left');
        $this->db->join('msrtc_replication.depots d','d.DEPOT_CD = rsm.depot_code','inner');

        if($filter['from_date']=='' and $filter['to_date']==''){
            $filter['from_date'] = date('Y-m-d');
            $filter['to_date'] = date('Y-m-d');
        }

        
        if (isset($filter['from_date']) && $filter['from_date'] != '') {
            $this->db->where("DATE_FORMAT(cm.created_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($filter['from_date'])));
        }

        if (isset($filter['to_date']) && $filter['to_date'] != '') {
            $this->db->where("DATE_FORMAT(cm.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($filter['to_date'])));
        }
        
//        if (isset($filter['depots_arr']) && $filter['depots_arr'] != '') {
//            $this->db->where_in("ccm.registration_depo_cd",$filter['depots_arr']);
//        }
        
        $this->db->where(array("ct.transaction_type_code" => REG_PPC_TRANS_TYPE_VALUE, "ct.pass_category_id" => REGULAR_CARD)); // RG - Registration
        $this->db->where("cm.is_active = '1'");
        $this->db->where("ccm.is_active = '1'");
        $this->db->where("ct.is_active = '1'");
        $this->db->where("ctd.is_active = '1'");
        $this->db->where("at.is_active = '1'");
        $this->db->where("atd.is_active = '1'");
        $session_level = $this->session->userdata('level');
        if ($session_level == RETAILOR_LEVEL) {
            $this->db->where("ct.created_by = ".$this->session->userdata('user_id')."");
            //$this->db->where("ct.created_by = ".'407'."");
        }
        $this->db->group_by('atd.amount_transaction_id');

        $newdata = $this->db->get();
        //echo $this->db->last_query();
       
        $newdata_array = $newdata->result_array();
//        print_r($newdata);
       // die;
        return $newdata_array;
    }
    
     public function get_concession_pass_reg_data($filter) {

        $this->db->select(" 
                        u.agent_code,
                        concat(u.first_name,' ',u.last_name) as agent_name,
                        ccm.registration_depo_cd as DEPOT_CD,
                        '' as DEPOT_NM,
                        
                        DATE_FORMAT(cm.created_date, '%d-%m-%Y') as reg_date,
                   
                        ccm.name_on_card,                        
                        cm.date_of_birth,
                        IF(ccm.trimax_card_id IS NULL or ccm.trimax_card_id = '', 'NA', ccm.trimax_card_id) as cust_card_id,
                 
                        ccm.dispatch_date,
                 
                        DATE_FORMAT(ct.expiry_date, '%d-%m-%Y') as exp_date,
                        max(CASE WHEN atd.amount_code = '".SMART_CARD_FEE."' THEN atd.amount ELSE 0.00 END) as smart_card_fee,
                        max(CASE WHEN atd.amount_code = '".APPLICATION_FEE."' THEN atd.amount ELSE 0.00 END) as application_fee,
                        max(CASE WHEN atd.amount_code = '".GST_FEE."' THEN atd.amount ELSE 0.00 END) as gst_fee,                 
                           at.total_amount,
                           ct.concession_cd,
                           ct.concession_nm, 
                           ct.kms_limit,
                           ct.span_name as card_validity,
                          
                    ");
        
         //    p_cm.concession_cd,
                   //     p_cm.concession_nm, 
          //     p_cm.kms_limit,
          //     // //      p_rf.span_name as card_validity,
        // 
        $this->db->from('ps_customer_master cm');
        $this->db->join('ps_customer_card_master ccm', 'ccm.customer_id = cm.id', 'left');
        $this->db->join('ps_card_transaction ct', 'ct.trimax_card_id = ccm.trimax_card_id', 'innner');
        $this->db->join('ps_card_transaction_details ctd', 'ctd.card_transaction_id = ct.id', 'inner');
        $this->db->join('ps_amount_transaction at', 'at.card_transaction_id = ct.id', 'inner');
        $this->db->join('ps_amount_transaction_details atd', 'atd.amount_transaction_id = at.id', 'inner');
       // $this->db->join('ps_concession_master p_cm', 'ct.pass_concession_id = p_cm.id', 'inner');
       /// $this->db->join('ps_rfidmaster_span p_rf', 'p_rf.id = ct.span_id', 'inner');
        
        $this->db->join('users u', 'u.id = ct.created_by', 'inner'); 
        #$this->db->join('retailer_service_mapping rsm','u.id = rsm.agent_id','left');
        #$this->db->join('msrtc_replication.depots d','d.DEPOT_CD = rsm.depot_code','inner');

        if($filter['from_date']=='' and $filter['to_date']==''){
            $filter['from_date'] = date('Y-m-d');
            $filter['to_date'] = date('Y-m-d');
        }

        if (isset($filter['from_date']) && $filter['from_date'] != '') {
            $this->db->where("DATE_FORMAT(cm.created_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($filter['from_date'])));
        }

        if (isset($filter['to_date']) && $filter['to_date'] != '') {
            $this->db->where("DATE_FORMAT(cm.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($filter['to_date'])));
        }
        
        if (isset($filter['depots_arr']) && $filter['depots_arr'] != '') {
            $this->db->where_in("ccm.registration_depo_cd",$filter['depots_arr']);
        }
        
        $this->db->where(array("ct.transaction_type_code" => REG_PPC_TRANS_TYPE_CODE, "ct.pass_category_id" => CONC_CARD            )); // RG - Registration
        $this->db->where("cm.is_active = '1'");
        $this->db->where("ccm.is_active = '1'");
        $this->db->where("ct.is_active = '1'");
        $this->db->where("ctd.is_active = '1'");
        $this->db->where("at.is_active = '1'");
        $this->db->where("atd.is_active = '1'");
        $session_level = $this->session->userdata('level');
        if ($session_level == RETAILOR_LEVEL) {
            $this->db->where("ct.created_by = ".$this->session->userdata('user_id')."");
            //$this->db->where("ct.created_by = ".'407'."");
        }
        $this->db->group_by('atd.amount_transaction_id');
        $newdata = $this->db->get();
        $newdata_array = $newdata->result_array();
        last_query(1);
        return $newdata_array;
    }

    public function get_concession_pass_reg_cnt($filter) {

        $this->db->select("
                        p_cm.concession_cd,
                        p_cm.concession_nm ,
                        count(p_ct.id) as cnt ,
                        sum(p_at.total_amount) as amt
                    ");
        $this->db->from('ps_concession_master p_cm');
        $this->db->join('ps_card_transaction p_ct', 'p_ct.pass_concession_id = p_cm.id', 'inner');
        $this->db->join('ps_customer_card_master ccm', 'ccm.trimax_card_id = p_ct.trimax_card_id', 'inner');
        $this->db->join('ps_amount_transaction p_at', 'p_at.trimax_card_id = p_ct.trimax_card_id', 'inner');
        
        if($filter['from_date']=='' and $filter['to_date']==''){
            $filter['from_date'] = date('Y-m-d');
            $filter['to_date'] = date('Y-m-d');
        }
        
        if (isset($filter['from_date']) && $filter['from_date'] != '') {
            $this->db->where("DATE_FORMAT(p_ct.created_date, '%Y-%m-%d') >=", $filter['from_date']);
        }

        if (isset($filter['to_date']) && $filter['to_date'] != '') {
            $this->db->where("DATE_FORMAT(p_ct.created_date, '%Y-%m-%d')<=", $filter['to_date']);
        }
        
        if (isset($filter['depots_arr']) && $filter['depots_arr'] != '') {
            $this->db->where_in("ccm.registration_depo_cd",$filter['depots_arr']);
        }
        
        $this->db->where(array("p_ct.transaction_type_id" => REG_PPC_TRANS_TYPE_ID, "p_ct.pass_category_id" => CONC_CARD)); // RG - Registration
        $this->db->where("ccm.is_active = '1'");
        $this->db->where("p_ct.is_active = '1'");
        $this->db->where("p_at.is_active = '1'");
        $session_level = $this->session->userdata('level');
        if ($session_level == RETAILOR_LEVEL) {
            $this->db->where("p_cm.created_by = ".$this->session->userdata('user_id')."");
        }
        
        $this->db->group_by('p_ct.pass_concession_id ');

        $newdata = $this->db->get();
        $newdata_array = $newdata->result_array();
        return $newdata_array;
    }
    
    public function sw_topup_data($filter) {
        $session_level = $this->session->userdata('level');
        
        $this->db->select("
                            u.agent_code,
                            concat(u.first_name,' ',u.last_name) as agent_name,
                            d.DEPOT_CD,
                            d.DEPOT_NM,
                            IF(ccm.trimax_card_id IS NULL or ccm.trimax_card_id = '', 'NA', ccm.trimax_card_id) as cust_card_id,
                            ccm.name_on_card as holder_name,
                            DATE_FORMAT(ct.created_date, '%d-%m-%Y') as topup_date,
                            at.total_amount as topup_amt,
                            max(CASE WHEN atd.amount_code = '".BANK_FEE."' THEN atd.amount ELSE 0.00 END) as loading_fee,
                            max(CASE WHEN atd.amount_code = '".GST_FEE."' THEN atd.amount ELSE 0.00 END) as gst_fee,
                            ct.amount as load_amt,
                            ct.opening_balance as bal_before_topup,
                            ct.closing_balance as bal_after_topup
                        ");
        $this->db->from('ps_customer_card_master ccm');
        $this->db->join('ps_card_transaction ct', 'ccm.trimax_card_id = ct.trimax_card_id', 'inner');
        $this->db->join('ps_amount_transaction at', 'at.card_transaction_id = ct.id', 'inner');
        $this->db->join('ps_amount_transaction_details atd', 'atd.amount_transaction_id = at.id', 'inner');
        
        $this->db->join('users u', 'u.id = ct.created_by', 'inner'); 
        $this->db->join('retailer_service_mapping rsm','u.id = rsm.agent_id','left');
        $this->db->join('msrtc_replication.depots d','d.DEPOT_CD = rsm.depot_code','inner');

        if($filter['from_date']=='' and $filter['to_date']==''){
            $filter['from_date'] = date('Y-m-d');
            $filter['to_date'] = date('Y-m-d');
        }

        // if (isset($filter['from_date']) && $filter['from_date'] != '') {
        //     $this->db->where("DATE_FORMAT(ct.created_date, '%Y-%m-%d') >=", $filter['from_date']);
        // }
        
        if (isset($filter['from_date']) && $filter['from_date'] != '') {
            $this->db->where("DATE_FORMAT(ct.created_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($filter['from_date'])));
        }

        if (isset($filter['to_date']) && $filter['to_date'] != '') {
            $this->db->where("DATE_FORMAT(ct.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($filter['to_date'])));
        }

//        if (isset($filter['to_date']) && $filter['to_date'] != '') {
//            $this->db->where("DATE_FORMAT(ct.created_date, '%Y-%m-%d')=", date('Y-m-d', strtotime($filter['to_date'])));
//        }
        
        

//        if (isset($filter['userName']) && $filter['userName'] != '') {
//            $this->db->where("ct.created_by",$filter['userName']);
//        }
        
        $this->db->where("ct.transaction_type_code",RG); // SW - Registration
        $this->db->where("ccm.is_active = '1'");
        $this->db->where("ct.is_active = '1'");
        $this->db->where("at.is_active = '1'");
        $this->db->where("atd.is_active = '1'");
        if ($session_level == RETAILOR_LEVEL) {
            $this->db->where("ct.created_by = ".$this->session->userdata('user_id')."");
            //$this->db->where("ct.created_by = ".'407'."");
        }
        $this->db->group_by('atd.amount_transaction_id');

        $newdata = $this->db->get();
        //echo $this->db->last_query();
        $newdata_array = $newdata->result_array();
        
        return $newdata_array;
    }
    
    public function tw_topup_data($filter) {
        //show($filter,1);
        
        $this->db->select("
                            u.agent_code,
                            concat(u.first_name,' ',u.last_name) as agent_name,
                            d.DEPOT_CD,
                            d.DEPOT_NM,
                            
                            IF(ccm.trimax_card_id IS NULL or ccm.trimax_card_id = '', 'NA', ccm.trimax_card_id) as cust_card_id,
                            ccm.name_on_card as holder_name,
                            DATE_FORMAT(ct.created_date, '%d-%m-%Y') as topup_date,
                            at.total_amount as topup_amt,
                            max(CASE WHEN atd.amount_code = '".BANK_FEE."' THEN atd.amount ELSE 0.00 END) as loading_fee,
                            max(CASE WHEN atd.amount_code = '".GST_FEE."' THEN atd.amount ELSE 0.00 END) as gst_fee,
                            max(CASE WHEN atd.amount_code = '".BONUS_FEE."' THEN atd.amount ELSE 0.00 END) as bonus_fee,
                            ct.amount as load_amt,
                            ct.opening_balance as bal_before_topup,
                            ct.closing_balance as bal_after_topup
                        ");
        $this->db->from('ps_customer_card_master ccm');
        $this->db->join('ps_card_transaction ct', 'ccm.trimax_card_id = ct.trimax_card_id', 'inner');
        $this->db->join('ps_amount_transaction at', 'at.card_transaction_id = ct.id', 'inner');
        $this->db->join('ps_amount_transaction_details atd', 'atd.amount_transaction_id = at.id', 'inner');
        
        $this->db->join('users u', 'u.id = ct.created_by', 'inner'); 
        $this->db->join('retailer_service_mapping rsm','u.id = rsm.agent_id','left');
        $this->db->join('msrtc_replication.depots d','d.DEPOT_CD = rsm.depot_code','inner');

        if($filter['from_date']=='' and $filter['to_date']==''){
            $filter['from_date'] = date('Y-m-d');
            $filter['to_date'] = date('Y-m-d');
        }

         if (isset($filter['from_date']) && $filter['from_date'] != '') {
             $this->db->where("DATE_FORMAT(ct.created_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($filter['from_date'])));
         }
        
        
        if (isset($filter['to_date']) && $filter['to_date'] != '') {
            $this->db->where("DATE_FORMAT(ct.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($filter['to_date'])));
        }
        
      if (isset($filter['userName']) && $filter['userName'] != '') {
            $this->db->where("ct.created_by",$filter['userName']);
        }
       
       
       $this->db->where_in("ct.transaction_type_code",array('TPTC','TPTS')); // TW - Registration
       $this->db->where("ccm.is_active = '1'");
        $this->db->where("ct.is_active = '1'");
        $this->db->where("at.is_active = '1'");
        $this->db->where("atd.is_active = '1'");
        $session_level = $this->session->userdata('level');
        if ($session_level == RETAILOR_LEVEL) {
            $this->db->where("ct.created_by = ".$this->session->userdata('user_id')."");
            //$this->db->where("ct.created_by = ".'407'."");
        }
         
        $this->db->group_by('atd.amount_transaction_id');

        $newdata = $this->db->get();
//       echo $this->db->last_query();
//        die;
        $newdata_array = $newdata->result_array();
        return $newdata_array;
    }
    
    public function get_otc_card_data($filter) {
        
        $this->db->select("
                            u.agent_code,
                            concat(u.first_name,' ',u.last_name) as agent_name,
                            d.DEPOT_CD,
                            d.DEPOT_NM,
                            DATE_FORMAT(cm.created_date, '%d-%m-%Y') as reg_date,
                            ccm.name_on_card,                            
                            DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(cm.date_of_birth, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(cm.date_of_birth, '00-%m-%d')) AS dob,
                            IF(ccm.trimax_card_id IS NULL or ccm.trimax_card_id = '', 'NA', ccm.trimax_card_id) as cust_card_id,
                            IF(ccm.card_status='D','Issued','Not Issued') as card_status,
                            ctd.from_stop_code,
                            ctd.till_stop_code,
                            ct.amount as pass_amount,
                            max(CASE WHEN atd.amount_code = '".SMART_CARD_FEE_CD."' THEN atd.amount ELSE 0.00 END) as smart_card_fee,
                            max(CASE WHEN atd.amount_code = '".APPLICATION_FEE."' THEN atd.amount ELSE 0.00 END) as application_fee,
                            max(CASE WHEN atd.amount_code = '".GST_FEE."' THEN atd.amount ELSE 0.00 END) as gst_fee,
                            at.total_amount,
                            DATE_FORMAT(ct.activation_date, '%d-%m-%Y') as act_date,
                            DATE_FORMAT(ct.expiry_date, '%d-%m-%Y') as exp_date
                        ");
        $this->db->from('ps_customer_master cm');
        $this->db->join('ps_customer_card_master ccm', 'ccm.customer_id = cm.id', 'left');
        $this->db->join('ps_card_transaction ct', 'ct.trimax_card_id = ccm.trimax_card_id', 'innner');
        $this->db->join('ps_card_transaction_details ctd', 'ctd.card_transaction_id = ct.id', 'inner');
        $this->db->join('ps_amount_transaction at', 'at.card_transaction_id = ct.id', 'inner');
        $this->db->join('ps_amount_transaction_details atd', 'atd.amount_transaction_id = at.id', 'inner');       
        $this->db->join('users u', 'u.id = ct.created_by', 'inner'); 
        $this->db->join('retailer_service_mapping rsm','u.id = rsm.agent_id','left');
        $this->db->join('msrtc_replication.depots d','d.DEPOT_CD = rsm.depot_code','inner');

        if($filter['from_date']=='' and $filter['to_date']==''){
            $filter['from_date'] = date('Y-m-d');
            $filter['to_date'] = date('Y-m-d');
        }

        
        if (isset($filter['from_date']) && $filter['from_date'] != '') {
            $this->db->where("DATE_FORMAT(cm.created_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($filter['from_date'])));
        }

        if (isset($filter['to_date']) && $filter['to_date'] != '') {
            $this->db->where("DATE_FORMAT(cm.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($filter['to_date'])));
        }
        
//        if (isset($filter['depots_arr']) && $filter['depots_arr'] != '') {
//            $this->db->where_in("ccm.registration_depo_cd",$filter['depots_arr']);
//        }
        
        $this->db->where(array("ct.transaction_type_code" => REG_OTC_TRANS_TYPE_CODE, "ct.pass_category_id" => OTC_CARD)); // OTC - Registration
        $this->db->where("cm.is_active = '1'");
        $this->db->where("ccm.is_active = '1'");
        $this->db->where("ct.is_active = '1'");
        $this->db->where("ctd.is_active = '1'");
        $this->db->where("at.is_active = '1'");
        $this->db->where("atd.is_active = '1'");
        $this->db->where("atd.is_valid = 'Y'");
        $session_level = $this->session->userdata('level');
        if ($session_level == RETAILOR_LEVEL) {
            $this->db->where("ct.created_by = ".$this->session->userdata('user_id')."");
            //$this->db->where("ct.created_by = ".'407'."");
        }
        $this->db->group_by('atd.amount_transaction_id');

        $newdata = $this->db->get();
        //echo $this->db->last_query();
       
        $newdata_array = $newdata->result_array();
//        print_r($newdata);
       // die;
        return $newdata_array;
    }
    
}

