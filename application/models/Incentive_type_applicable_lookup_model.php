<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Incentive_type_applicable_lookup_model extends MY_Model {
 
    var $table    = 'incentive_type_applicable_lookup';
    var $fields = array("id","incentive_type_id","applicable_on_name","is_status");
    var $key    = 'id'; 
   
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

}
