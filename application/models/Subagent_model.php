<?php

class Subagent_model extends My_Model {

    var $table = 'booking_agents';
    var $fields = array("id", "agent_id", "agent_name", "parent", "agent_type", "agent_address", "email","contact_no", "b2b_permit", "agent_status","created_by","created_date");
    var $key = "id";

    function __construct() {
        parent::__construct();
    }

    function index() {
        
    }

    function pntamt_check1($agid) {
        $this->db->select('ap.amount as amt ');
        $this->db->from('booking_agents ba');
        $this->db->join('agent_para ap', 'ba.agent_id =ap.agent_id');
        $this->db->where('ba.agent_id', $agid);
        $query = $this->db->get();
        return $query->row()->amt;
    }

    /* fucntion agent para
     * description check parant  agent amount limit
     * parameter id
     */

    function pag_amt($id) {
        $this->db->select('amount');
        $this->db->from('agent_para');
        $this->db->where('agent_id', $id);
        $query = $this->db->get();
        return $query->row()->amount;
    }


    /* function is_subag
     * checks whether is it sub agent
     * param:agent id
     */

    function is_parent($email) {
        $this->db->select('parent');
        $this->db->from('booking_agents');
        $this->db->where('email', $email);
        $query = $this->db->get();
        return $query->result_array();
        
    }

    function subagentlist($id) {
        $this->db->select('agent_name,agent_id,id');
        $this->db->from('booking_agents');
        $this->db->where('parent', $id);
        $this->db->where('agent_status','Y');
        $agname = $this->db->get();
        return $agname->result_array();
    }

    function edit_data($id) {
        $this->db->select("ba.id,ba.agent_name,ba.email,ba.agent_address,ba.contact_no,ap.amount,date_format(ap.contract_start,'%d-%m-%y') as contract_start ,date_format(ap.contract_end,'%d-%m-%y') as contract_end,ba.agent_status");
        $this->db->from('booking_agents ba');
        $this->db->join('agent_para ap', 'ba.agent_id=ap.agent_id', 'inner');
        $this->db->where('ba.record_status', 'Y');
        $this->db->where('ba.id', $id);
        $data = $this->db->get();
        return $data->result_array();
    }

    function update_data($data) {
        $ba = array("agent_name" => $data['subagname'], "agent_address" => $data['subagadd'], "contact_no" => $data['mobno']);
        $this->db->where('id', $data['id']);
        $this->db->update("booking_agents", $ba);

        $ap = array("contract_start" => date('Y-m-d', strtotime($data['cstart'])), "contract_end"=>date('Y-m-d', strtotime($data['cend'])));
        $this->db->where('agent_id', $data['agid']);
        $this->db->update("agent_para", $ap);
    }

    function pntamt_check($agid) {
        $this->db->select('amt');
        $this->db->from('wallet w');
        $this->db->join('users u', 'w.user_id =u.user_id');
        $this->db->where('w.user_id', $agid);
        $query = $this->db->get();
        return $query->row();
    }

    function check_par_amt($agid) {
        $this->db->select("IFNULL((sum(ag.amount)),'0')AS balance");
        $this->db->from('agent_para ag');
        $this->db->join('booking_agents bg', 'bg.agent_id =ag.agent_id');
        $this->db->where('bg.parent', $agid);
        $query = $this->db->get();
        $this->db->last_query();
        return $query->row()->balance;
    }
    function getemail($uid)
    {
        $this->db->select('email');
        $this->db->from("users u");
        $this->db->where("user_id",$uid);
         $query = $this->db->get();
        
        return $query->row()->email;
        
    }
      function getid($email)
    {
        $this->db->select('agent_id');
        $this->db->from("booking_agents bg");
        $this->db->where("email",$email);
        $query = $this->db->get();
        
        return $query->result_array();
        
    }
}

?>
