<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Servicetype_model extends MY_Model {
    
    protected $table = 'service_type';
    var $fields = array("id", "type");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
        
    }

    function get_all_service_types(){

    $response = array();
 
    // Select record
    $this->db->select('*');
    $q = $this->db->get('service_type');
    $response = $q->result_array();

    return $response;
  }

}