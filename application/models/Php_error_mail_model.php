<?php

class php_error_mail_model extends MY_Model {

    var $table = 'php_error_mail';
    var $fields = array("id", "error_type","message","filepath", "ip_address", "created_date","Url_accessed","line","Occured_on");
    var $key = "id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
}
?>
