<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tso_report_model extends CI_Model {
    
    public function statusWiseReport($from_date, $to_date)
    {
        $from_date = date('Y-m-d', strtotime($from_date));
        $to_date = date('Y-m-d', strtotime($to_date));
        
        $this->db->select('count("status") as count, status');
        $this->db->from("tso_transaction_details");
        $this->db->where("DATE_FORMAT(created_at, '%Y-%m-%d') BETWEEN '" . $from_date . "' AND '" . $to_date . "' ");
        $this->db->group_by("status");
        $res = $this->db->get();
        if($res->result_array()) {
            return $res->result_array();
        } else
        {
            return false;
        }
    }
    
    
    public function operatorsWiseReport($from_date, $to_date)
    {
        $from_date = date('Y-m-d', strtotime($from_date));
        $to_date = date('Y-m-d', strtotime($to_date));
        
        $this->db->select('count("status") as count, status, operator');
        $this->db->from("tso_transaction_details");
        $this->db->where("DATE_FORMAT(created_at, '%Y-%m-%d') BETWEEN '" . $from_date . "' AND '" . $to_date . "' ");
        $this->db->group_by("operator, status");
        $res = $this->db->get();
        if($res->result_array()) {
            return $res->result_array();
        } else
        {
            return false;
        }
    }
    
    public function reasonsWiseReport($from_date, $to_date)
    {
        $from_date = date('Y-m-d', strtotime($from_date));
        $to_date = date('Y-m-d', strtotime($to_date));
        
        $this->db->select('count("reason") as count, reason');
        $this->db->from("jri_transaction_details");
        $this->db->where_not_in("reason", '');
        $this->db->where("DATE_FORMAT(created_at, '%Y-%m-%d') BETWEEN '" . $from_date . "' AND '" . $to_date . "' ");
        
        $this->db->group_by("reason");
        $res = $this->db->get();
        if($res->result_array()) {
            return $res->result_array();
        } else
        {
            return false;
        }
    }
}
