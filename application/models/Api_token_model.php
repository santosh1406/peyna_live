<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api_token_model extends MY_Model {

	/**
     * Instanciar o CI
     */
    var $table  = 'api_token';
    var $fields = array("id","user_id","token","status","created_at","created_by");
    var $key    = "id";

    public function __construct() {
        parent::__construct();
        //$this->_init();
    }

}
    