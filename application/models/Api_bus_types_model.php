<?php


if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api_bus_types_model extends MY_Model {
    
    var $table  = 'api_bus_types';
    var $fields = array("id","api_bus_type","bus_type_id","provider_id","timestamp");
    var $key    = "";

    public function __construct() {
        parent::__construct();
       
    }

    
}
// End of Temp_bus_types_model class