<?php
class Op_trans_details_model extends MY_Model {
    var $table  = 'op_trans_details';
    var $fields = array("op_trans_id", "op_topup_id", "user_id", "provider_flag", "service_type", "transaction_type","date", "transaction_amt", "opening_balance", "closing_balance", "payment_type", "payment_mode", "depositor_bank_name", "depositor_acc_no", "operator_bank_name", "operator_acc_no", "transaction_code", "transaction_date","pg_transaction_status", "transaction_for","topup_by", "agent_code", "commission_rate_type", "commission_rate","tds_type","tds_rate","tds", "ref_op_trans_id", "credit", "debit", "remark", "is_deleted", "is_status", "created_by", "updated_by", "created_date", "updated_date","cumulative_opening_balance","cumulative_closing_balance");
    var $key    = "op_trans_id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
}
?>
