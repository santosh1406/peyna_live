<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Current_ticket_details_model extends MY_Model
{
    /**
     * Instanciar o CI
     */
    var $table = 'current_ticket_details';
    var $fields		   = array("ticket_details_id","ticket_no","psgr_no","psgr_name","psgr_age","psgr_sex","psgr_type","concession_cd","concession_rate","discount","is_home_state_only","quota_flag","home_state_amt","state_amt_1","state_amt_2","state_amt_3","state_amt_4","state_amt_5","additional_Amt","fare_amt","seat_no","seat_status","concession_proff","proff_detail","identify_proof","identify_nm","identify_no","status");
    var $key    = 'ticket_det_id';  
    
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
  
        
}
