<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class System_config_model extends MY_Model {

    /**
     * Instanciar o CI
     */
    var $table  = 'system_config';
    var $fields = array("para_id","parameter_name","parameter_value","effective_from","effective_till");
    var $key    = 'id'; 
    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    function system_config_values() {
        $this->db->select("parameter_name,parameter_value");
        $this->db->from("system_config");
        $this->db->where("parameter_name", 'MAX_INTERVAL_TIME_CURRENT_BOOKING_SELECT');
        $query = $this->db->get();
        $parameter_value = $query->row()->parameter_value;
        return  $parameter_value;
    }     

}
