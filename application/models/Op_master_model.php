<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Op_master_model extends MY_Model {

    var $table = 'op_master';
    var $fields = array("id", "op_id", "op_name", "op_type", "op_status", "is_grouping_applicable");
    var $key = 'op_id';

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    function get_op_names() {
        $this->db->select('op_id,op_name,op_type,is_status');
        $this->db->where('is_status', '1');
      //  $this->db->where('is_deleted', '0');
        $this->db->order_by("op_name", "ASC");
        $data = $this->db->get('op_master');
      
        return $data->result_array();
    }

    function getop_edit($op_id) {
        $this->db->select('*');
        $this->db->from('op_master om');
        $this->db->where('om.op_id', $op_id);
        $op_data = $this->db->get();
        return $op_data->result_array();
    }

    function get_op_names_chosen() {
        $whereclause = 'SELECT op_id FROM op_config '
                . ' WHERE convey_charges_flag="1" '
                . ' AND is_status="1" '
                . ' AND is_deleted="0"';
        $this->db->select("op_id,op_name,op_type,op_status");
        $this->db->from("op_master");
        if ($q != '') {
            $this->db->where("op_id IN ($whereclause)", NULL, FALSE);
        }
        $this->db->order_by("op_name", "ASC");
        $query = $this->db->get();
        $res = $query->result_array();
        $output = null;
        $output .= "<option value='' selected></option>";
        foreach ($res as $row) {
            //here we build a dropdown item line for each query result
            $output .= "<option value='" . $row['op_id'] . "'>" . $row['op_name'] . "</option>";
        }
        return $output;
    }
    
    
    function op_details_filter($input) {
       
        $this->db->select('*');
        $this->db->from('op_master om');
        $this->db->where('om.is_status', '1');
        $this->db->where('om.is_deleted', '0');
        if(isset($input['op_id']) && $input['op_id']!=''){
            if($input['op_id']=='all'){
                
            }else{
                $this->db->where('om.op_id',$input['op_id']);  
            }
           
         }
         if(isset($input['from_date']) && $input['from_date']!=''){
           $this->db->where('om.created_date >=',date('Y-m-d',  strtotime($input['from_date'])));  
         }
         if(isset($input['to_date']) && $input['to_date']!=''){
           $this->db->where('om.created_date <=',date('Y-m-d',  strtotime($input['to_date'])));  
         }
        $op_data = $this->db->get();
       // show($this->db->last_query(),1);
        return $op_data->result_array();
    }
    
     function operator_detail_filter($data) {
       
        
        $this->db->select('op.op_name,op.op_type,cm.stCity,sm.stState,op.op_country,op.op_email,op.op_phone_no,oam.api_url as API,opd.bank_name,opd.acc_no,opd.ifsc_code,op.created_date');
        $this->db->from('op_master op');
        $this->db->join('op_bank_detail opd','opd.op_id=op.op_id', 'left');
        $this->db->join('op_api_master oam','oam.op_id=op.op_id', 'left');
        $this->db->join('city_master cm','cm.intCityId=op.op_city', 'left');
        $this->db->join('state_master sm','sm.intStateId=op.op_state', 'left');
        $this->db->join('country_master com','com.intCountryId=op.op_country', 'left');
        if($data['from_date']!="")
        {
          $this->db->where('op.created_date >=',$data['from_date']);
        }
          if($data['to_date']!="")
        {
          $this->db->where('op.created_date <=',$data['to_date']);
        }
        $this->db->where('opd.is_deleted', 0);
        $this->db->where('opd.is_status',1);
        $this->db->where('op.is_status',1);
        $this->db->where('op.is_deleted',0);
     
        $op_data = $this->db->get();
        return $op_data->result_array();
    }
        public function get_op_name_by_id($op_id)
    {
        $this->db->select('op_id,op_name');
        $this->db->order_by('op_name');
        $this->db->where('op_id=',$op_id);
        $data = $this->db->get($this->table);
        if($data->num_rows()>0)
        {
            return $data->row_array();
        }
        return false;
    }
    public function get_op_name_for_outstanding($op_id)
    {
        $this->db->select('om.op_id,om.op_name');
        $this->db->from('op_master om');
        $this->db->join('cb_credit_account_detail ccad','ccad.op_id=om.op_id', 'inner');
        $this->db->where('om.op_id=',$op_id);
        $this->db->order_by('op_name');
        $data = $this->db->get($this->table);
        if($data->num_rows()>0)
        {
            return $data->row_array();
        }
        return false;
    }
       public function get_op_name_for_pendinglist($op_id)
    {
        $this->db->select('om.op_id,om.op_name');
        $this->db->from('op_master om');
        $this->db->join('cb_credit_account_cheque_detail ccacd','ccacd.op_id=om.op_id', 'inner');
        $this->db->where('om.op_id=',$op_id);
        $this->db->order_by('op_name');
        $data = $this->db->get($this->table);
        if($data->num_rows()>0)
        {
            return $data->row_array();
        }
        return false;
    }
    public function get_op_name()
    {
        $this->db->select('op_id,op_name');
        $this->db->order_by('op_name');
        $data = $this->db->get($this->table);
        if($data->num_rows()>0)
        {
            return $data->result_array();
        }
        return false;
    }
    
    
}
