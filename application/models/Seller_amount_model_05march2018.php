<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Seller_amount_model extends CI_Model {
    var $table  = 'cb_seller_amount';
    var $fields = array("seller_amount_id","BUS_STOP_CD","min_amount");
    var $key    = 'seller_amount_id';
    
    function __construct() {
        parent::__construct();
	    //$this->db2 = $this->load->database('local_up_db', TRUE);	
    }

    public function getAllSellerAmtInfo(){
	$this->db->select('seller_amount_id,BUS_STOP_CD,min_amount');
	$this->db->where('is_active', 'Y');
	$this->db->where('is_deleted', 'N');
        $query = $this->db->get('cb_seller_amount');
        if($query->num_rows() > 0){
            return $query->result_array();
        }
                 
    }
    
    public function getBusStopCode(){
        $this->db->select('BUS_STOP_CD');
        $this->db->where('BUS_STOP_CD NOT IN(select BUS_STOP_CD from cb_seller_amount where is_active = "Y" and is_deleted = "N" )');
        $query = $this->db->get('cb_bus_stops');
        if($query->num_rows() > 0){
            return $query->result();
        }
    }
    
    public function insert_seller_data($Table,$data){
	  $this->db->insert($Table, $data);
	  $insert_id = $this->db->insert_id();
	  return $insert_id;
    }
    
    public function getSellerAmtInfo($id){
	$this->db->select('seller_amount_id,BUS_STOP_CD,min_amount');
	$this->db->where('is_active', 'Y');
	$this->db->where('is_deleted', 'N');
	$this->db->where('seller_amount_id', $id);
        $query = $this->db->get('cb_seller_amount');
        if($query->num_rows() > 0){
            return $query->result_array();
        }
                 
    }
    
    public function update_seller_data($Table,$id,$data){
        $this->db->where('seller_amount_id', $id);
        $this->db->update($Table, $data);
        if($this->db->affected_rows() > 0){
            return $id;
        }
    }


    
}
?>