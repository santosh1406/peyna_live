<?php

class State_master_model extends MY_Model {

    var $table = 'state_master';
    var $fields = array("intStateId", "intCountryId", "stState");
    var $key = "intStateId";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
    
    function get_state($country_id)
    {
        $this->db->select('intStateId,stState');
        $this->db->where('intCountryId',$country_id);
        $this->db->order_by('stState');
        $data = $this->db->get($this->table);
        if($data->num_rows()>0)
        {
            return $data->result_array();
        }
        return false;
    }

    function get_insert_state_by_name($name, $country_id)
    {
        if($name != '')
        {
            $filter_arr = array('stState' => $name, 'intCountryId' => $country_id);

            $state = $this->where($filter_arr)
                          ->find_all();
            if($state)
            {
                return $state[0]->intStateId;
            }
            else
            {
                return $this->insert($filter_arr);
            }
        }
        else
        {
            return false;
        }
    }
    
    function get_all_state()
    {
        $this->db->select('intStateId,stState');
        $this->db->where('intCountryId', '1');
        $this->db->order_by('stState');
        $data = $this->db->get($this->table);
        if($data->num_rows()>0)
        {
            return $data->result_array();
        }
        return false;
    }


    function get_stateno($stat_name,$country_id)
    {
        $this->db->select('intStateId,stState');
        $this->db->where('stState',$stat_name);
        $this->db->where('intCountryId',$country_id);
        $this->db->order_by('stState');
        $data = $this->db->get($this->table);
        if($data->num_rows()>0)
        {
            return $data->result_array();
        }
        return false;
    }
    function get_state_by_id($stateID)
    {
        $this->db->select('stState');
        $this->db->from('state_master');
        $this->db->where('intStateId',$stateID);
        $data = $this->db->get();
        $data= $data->result_array();

        if(!empty($data))
        {
            $state_name = $data[0]['stState'] ;           
        }
        return $state_name;
    }
}    
