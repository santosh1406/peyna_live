<?php
 
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Upsrtc_Model extends CI_Model {

    function __construct() {
        parent::__construct();
        
        $model_array = array('common_model','trip_master_model','bus_type_model','bus_stop_master_model','op_bus_types_model','op_seat_layout_model','op_layout_details_model','travel_destinations_model','trip_boarding_alighting_model','op_stop_master_model','boarding_stop_model');
        $this->load->model($model_array);
    }

    /*
     * @function    : insert_bus_type
     * @Desc        : insert the operator bus tuype in db
     * @table       : op_bus_types
     * 
     */

    function insert_bus_type($data, $op_id) {
        if (isset($data['return']) && count($data['return']) > 0) {
            $data = $data['return']['allBusType'];

            foreach ($data as $key => $value) {

                $op_bus_types   = array(
                                        'op_id'=> $op_id, 
                                        "op_bus_type_name" => $value['busName']
                                        );

                $count = $this->op_bus_types_model
                            ->where( $op_bus_types )
                            ->count_all();

                if ($count == 0) {
                    $op_bus_types['op_bus_type_cd'] = $value['busType'];
                    $op_bus_types['is_child_concession'] = trim($value['ischildconcession']);
                    $op_bus_types['is_seat_layout'] = trim($value['isseatlayout']);
                    $op_bus_types['is_seat_number'] = trim($value['isseatnumber']);
                    $op_bus_types['created_date'] = date("Y-m-d H:i:s");
                    $this->op_bus_types_model->insert($op_bus_types);
                }
            }
        }
    }

    function insert_available_services($data, $op_id, $controller) {
        // $this->common_model->truncate_table(array('generateid','op_layout_details', 'op_seat_layout', 'trip_master','op_bus_types'));
        
        if (isset($data['AvailableServiceResponse']) && count($data['AvailableServiceResponse']) > 0 && isset($data['AvailableServiceResponse']['availableServices'])) {
            $services = $data['AvailableServiceResponse']['availableServices']['service'];

            if (!is_array($services)) {
                $services = array($services);
            }

            if(count($services) >0)
            {
                foreach ($services as $key => $value) {
                    
                    eval(UPSRTC_BUS_TYPES_NON_AC);
                    if(!in_array($value['busType'], $upsrtc_bus_types_non_ac_array)) {
                        if(empty($value['serviceId'])){
                            logging_data('cron/bus/missinng_service'.date('d-m-y').'.log',$value,'missing_services');
                        }

                        $this->trip_master_model->op_id = $op_id;
                        $this->trip_master_model->op_trip_no = $value['serviceId'];

                        $trip_data = $this->trip_master_model->select();

                        if (count($trip_data) == 0) {
                            $op_bus_type_detail = $this->op_bus_types_model->get_detail_category($value['busType'], $op_id);
                            
                            $op_seat_layout_arr = array(
                                                            "op_id" => $op_id,
                                                            "layout_name" => $value['busType']
                                                        );

                            $op_seat_layout = $this->op_seat_layout_model
                                                    ->where($op_seat_layout_arr)
                                                    ->find_all();

                            // show($op_bus_type_detail, '', 'bud_detail');

                            $op_bus_type_id = "";
                            $bus_type_id    = "";

                            if (count($op_bus_type_detail) == 0) {
                                $controller->GetAllBusTypes();

                                $op_bus_type_detail = $this->op_bus_types_model->get_detail_category($value['busType'], $op_id);
                            }

                            if(count($op_bus_type_detail) == 0)
                            {
                                show("Bus Cat Not  Found ".$value['busType']);
                                continue;
                            }
                            else
                            {
                                $op_bus_type_id = $op_bus_type_detail->id;                            
                                $bus_type_id    = $op_bus_type_detail->bus_type_id;                            
                            }


                            $seat_layout_id = false;

                            if ($op_seat_layout)
                            {
                                $seat_layout_id = $op_seat_layout[0]->id;
                            }
                            else
                            {
                                $seat_layout_id = $controller->getAllBusFormats($value['busFormatCd']);
                            }
                            

                            if($seat_layout_id)
                            {

                                // $trip_no           = $this->generateid_model->getRequiredId('trip_master');

                                // $this->trip_master_model->trip_no               = $trip_no;
                                $trip_inst_arr = array(
                                                        'op_trip_no'        => $value['serviceId'],
                                                        'provider_id'       => '1',
                                                        'op_id'             => $op_id,
                                                        'route_no'          => $value['routeName'],
                                                        'bus_type_id'       => $bus_type_id,
                                                        'op_bus_type_id'    => $op_bus_type_id,
                                                        'seat_layout_id'    => $seat_layout_id,
                                                        'route_no_name'     => $value['routeName'],
                                                        'refund_permitted'  => 'Y',
                                                        'fare_type'         => 'C',
                                                        'sch_dept_time'     => $value['departureTime'],
                                                    );

                                $trip_no  = $this->trip_master_model->insert($trip_inst_arr);

                                if($trip_no)
                                {
                                    $controller->getBusServiceStops($value['serviceId'], $trip_no, $value['busType']);
                                }

                            }
                            else{
                                logging_data('cron/bus/bus_'.date('d-m-y').'.log',$seat_layout_id,'op_seat_layout');
                                logging_data('cron/bus/bus_'.date('d-m-y').'.log',$value,'insert_available_services');
                                show($value,'','Seat _id Not found');
                            }
                        }
                        else
                        {
                            $trip_no = $trip_data[0]->trip_no;
                            $controller->getBusServiceStops($value['serviceId'], $trip_no, $value['busType']);
                        }

                        if(($key % 300) == 0)
                        {
                            // sleep(1);
                            $this->db->reconnect();
                        } 
                    }  
                }
            }
            else
            {
                logging_data('cron/bus/bus_'.date('d-m-y').'.log',$services,' Empty Service array');
                echo 'empty array';
            }
        }
        else
        {
            logging_data('cron/bus/bus_'.date('d-m-y').'.log',$data,' Empty Service array');
            show($data,''); 
        }
    }

    function insert_bus_format($response, $op_id, $bus_format_cd) {

        if (isset($response['AllBusFormatResponse']['busDetail'])) {
            $get_all_bus_format_api_data = $response['AllBusFormatResponse']['busDetail'];
            
           // show($get_all_bus_format_api_data,'','@model : insert_bus_format');
            
            if (count($get_all_bus_format_api_data) > 0) {
                
                $data = $this->op_seat_layout_model
                                ->where(
                                        array(
                                                'op_id' => $op_id,
                                                'layout_name' => $bus_format_cd,
                                        )
                                    )
                                ->find_all();
                
               // show($data,1);
                
                if($data)
                {
                    $seat_layout_id = $data[0]->id;
                }
                else
                {
                    // $seat_layout_id = $this->generateid_model->getRequiredId('op_seat_layout');
    
                    // $this->op_seat_layout_model->seat_layout_id     = $seat_layout_id;
                    $this->op_seat_layout_model->op_id              = $op_id;
                    $this->op_seat_layout_model->layout_name        = $bus_format_cd;
                    $this->op_seat_layout_model->ttl_seats          = count($get_all_bus_format_api_data['seatLayout']['seat']);
                    $this->op_seat_layout_model->no_rows            = $get_all_bus_format_api_data['rowCount'];
                    $this->op_seat_layout_model->no_cols            = $get_all_bus_format_api_data['columnCount'];
                    $this->op_seat_layout_model->no_berth           = 1;
                    $this->op_seat_layout_model->created_date       = date("Y-m-d H:i:s");
                    $seat_layout_id = $this->op_seat_layout_model->save();

                    // show($get_all_bus_format_api_data['seatLayout']['seat'],'','seat');

                    if (count($get_all_bus_format_api_data['seatLayout']['seat']) > 0) {
                        foreach ($get_all_bus_format_api_data['seatLayout']['seat'] as $key => $value) {
                            
                            
                            $this->op_layout_details_model->op_seat_layout_id   = $seat_layout_id;
                            $this->op_layout_details_model->row_no              = $value['xCoordinate'];
                            $this->op_layout_details_model->col_no              = $value['yCoordinate'];
                            $this->op_layout_details_model->berth_no            = 1;
                            $this->op_layout_details_model->seat_no             = $value['seatNumber'];
                            $this->op_layout_details_model->seat_type           = $value['seatType']; 
                            $this->op_layout_details_model->quota_type          = $value['quota']; 
                            $this->op_layout_details_model->created_date       = date("Y-m-d H:i:s");
                            $op_ldm =  $this->op_layout_details_model->save();

                            // show($op_ldm);
                        }
                    }
                }

                return $seat_layout_id;
                // $gabf_id    = $this->op_seat_layout_model->save();
            }
            else
            {
                logging_data('cron/bus/bus_'.date('d-m-y').'.log',$get_all_bus_format_api_data,' empty insert_bus_format ---  get_all_bus_format_api_data ');
            }
        }
        else
        {
            echo 'Error : insert_bus_format';
            logging_data('cron/bus/bus_'.date('d-m-y').'.log',$data,' insert_bus_format');

            show($data);
        }
    }


    function insert_bus_service_stops($data, $op_id,$dep_date, $controller, $bstop) {       

        $date = new DateTime(str_replace('/','-',$dep_date));
        $dep_date = $date->format('Y-m-d');
        
        if (isset($data['BusServiceStopsResponse']) && count($data['BusServiceStopsResponse']) > 0) {
            if (isset($data['BusServiceStopsResponse']['busServiceStops']) && count($data['BusServiceStopsResponse']['busServiceStops']) > 0 && isset($data['BusServiceStopsResponse']['busServiceStops']['busServiceStop'])) {
                $busServiceStop = $data['BusServiceStopsResponse']['busServiceStops']['busServiceStop'];
                
                // if(!is_array($busServiceStop))
                // {
                //     $busServiceStop = array($busServiceStop);
                // }


                if(isset($busServiceStop['serviceId']))
                {
                    $busServiceStop = array($busServiceStop);
                }

                if(count($busServiceStop) >0)
                {
                    foreach ($busServiceStop as $key => $value) {
                        // if($key == 8)
                        //     break;
                        if(empty($value['arrivalTime']))
                        {
                            logging_data('cron/bus/bus_'.date('d-m-y').'.log',$value,' insert_bus_service_stops empty arrival time');
                        }   

                        $boarding_stop_id       = $this->get_op_bus_stop_id($value['fromStop'], $op_id);
                        $destination_stop_id    = $this->get_op_bus_stop_id($value['toStop'], $op_id);
                        
                        $trip_master_arr = array();

                        $trip_master_arr["op_trip_no"] = $value['serviceId'];
                        $trip_master_arr["op_id"] = $op_id;
                        $trip_data = $this->trip_master_model->where($trip_master_arr)->find_all();

                        $travel_destinations_arr = array();
                        $travel_destinations_arr["trip_no"]             = $trip_data[0]->trip_no;
                        $travel_destinations_arr["boarding_stop_id"]    = $boarding_stop_id;
                        $travel_destinations_arr["destination_stop_id"] = $destination_stop_id;
                        $travel_destinations_arr["sch_departure_date"]  = $dep_date;
                        $travel_destinations_arr["sch_departure_tm"]    = $value['departureTime'];
                        

                        $travel_destinations_data = $this->travel_destinations_model->where($travel_destinations_arr)->find_all();
                        
                        if(!$travel_destinations_data)
                        {
                            $travel_destinations_arr["boarding_stop_name"]      = init_cap($value['fromStop']['busStopName']);
                            $travel_destinations_arr["destination_stop_name"]   = init_cap($value['toStop']['busStopName']);
                            $travel_destinations_arr["arrival_tm"]              = isset($value['arrivalTime']) ? $value['arrivalTime'] : '00:00:00';
                            $travel_destinations_arr["seat_fare"]               = $value['fare'];
                            $travel_destinations_arr["child_seat_fare"]         = $value['childFare'];
                            $travel_destinations_arr["sleeper_fare"]            = $value['sleeperFare'];
                            $travel_destinations_arr["day"]                     = $value['day'];
                            $travel_destinations_arr["created_date"]            = date('Y-m-d H:i:s');
                            $arr_day = ($value['day'] > 0) ? ($value['day'] -1 ) : $value['day'];
                            $travel_destinations_arr["arrival_date"]          = date('Y-m-d H:i:s', strtotime($dep_date.' '.$value['arrivalTime']. '+'.$arr_day.'day') );
                            
                            $this->travel_destinations_model->insert($travel_destinations_arr);
                        }

                        
                        $bstopdata['service_id']      = $bstop['service_id'];
                        $bstopdata['from']           = $value['fromStop']['busStopcode'];
                        $bstopdata['to']             = $value['toStop']['busStopcode'];
                        $bstopdata['bus_type_code']  = $bstop['op_bus_type'];
                        $bstopdata['trip_no']        = $bstop['trip_no'];

                        $controller->getBoardingStops($bstopdata, 'BRD');
                    }
                }
                else
                {
                    echo 'Empty array returned';
                }
            }
        }
        else
        {
            logging_data('cron/bus/bus_'.date('d-m-y').'.log',$data,' insert_bus_service_stops');
            show($data,'','No data found for insert_bus_service_stops');

        }
    }
    
    
    
    function get_op_bus_stop_id($data = array(), $op_id)
    {
        if(count($data) >0)
        {
            $op_stop_master_arr  = array(
                                            "op_id" => $op_id, 
                                            "op_stop_cd" => $data['busStopcode']
                                        );
            $op_stop_data        = $this->op_stop_master_model->where($op_stop_master_arr)->find_all();
            
            if($op_stop_data > 0)
            {
                $bus_stop_id                                = $op_stop_data[0]->bus_stop_id;
            }
            else
            {
                $bus_stop_master_arr = array();

                $bus_stop_master_arr["bus_stop_name"]   = ucfirst(strtolower($data['busStopName']));
                $bus_stop_master_arr["tahsil_nm"]       = isset($data['taluka']) ? ucfirst(strtolower($data['taluka'])) : '';
                $bus_stop_master_arr["state_nm"]        = ucfirst(strtolower($data['state']));
                $bus_stop_master_arr["district_nm"]     = isset($data['district']) ? ucfirst(strtolower($data['district'])) : '';

                $bus_stop_master_data = $this->bus_stop_master_model->where($bus_stop_master_arr)->find_all();
                
                if(!$bus_stop_master_data)
                {   
                    $bus_stop_master_arr["insert_date"]     = date("Y-m-d H:i:s");                 
                    $bus_stop_id = $this->bus_stop_master_model->insert($bus_stop_master_arr);                    
                }
                else
                {
                    $bus_stop_id = $bus_stop_master_data[0]->bus_stop_id;
                }
                

                $op_stop_master_arr['op_stop_name'] = ucfirst(strtolower($data['busStopName']));
                $op_stop_master_arr['bus_stop_id']  = $bus_stop_id;
                $op_stop_master_arr['created_date']  = date('Y-m-d H:i:s');
                $this->op_stop_master_model->insert($op_stop_master_arr);
                
            }

            return $bus_stop_id; 
        }        
        
    }
    /***incomplete function not save data**********/
     function insert_bus_fare_details($data)
    {
        $this->load->model(array('fare_details_model'));
        
        
        if(isset($data['SeatAvailabilityResponse']) && count($data['SeatAvailabilityResponse'])>0)
        {
            if(isset($data['SeatAvailabilityResponse']['serviceId']) &&
                    count($data['SeatAvailabilityResponse']['serviceId'])>0)
            {
                 $value = $data['SeatAvailabilityResponse']['serviceId'];
                
//                $this->trip_boarding_alighting_model->id                = "";
                $this->fare_details_model->tba_id            = $this->generateid_model->getRequiredId('trip_boarding_alighting');
                $this->fare_details_model->trip_no           = $value['serviceId'];
                $this->fare_details_model->trip_stop_name    = $value['fromStop']['busStopName'];
                $this->fare_details_model->tba_type          = "BRD";
                
                $this->fare_details_model->sch_dept_tm       = $value['departureTime'];
                         
                $this->fare_details_model->save();
                $this->fare_details_model->reset();
                
            }
        }        
    }

    
    function insert_boarding_stops($stops, $ali_type, $stop_code, $op_id, $data)
    {
        $trip_no = $data['trip_no'];

        foreach($stops as $key => $value)
        {            
            $boarding_stop_data  = $this->op_stop_master_model
                                        ->where(array("op_id" => $op_id , "op_stop_cd" => $stop_code))
                                        ->find_all();
            
            $boarding_stop_arr = array();

            $boarding_stop_arr['bus_stop_id']           = $boarding_stop_data[0]->bus_stop_id;
            $boarding_stop_arr['trip_no']               = $trip_no;
            $boarding_stop_arr['op_trip_no']            = $data['service_id'];
            $boarding_stop_arr['from']                  = $data['from'];
            // $boarding_stop_arr['to']                    = $data['to'];
            
            $boarding_stop_arr['bus_type']              = $data['bus_type_code'];
            $boarding_stop_arr['bus_stop_name']         = $value['busStopName'];
            $boarding_stop_arr['boarding_stop_name']    = $boarding_stop_data[0]->op_stop_name;
            $boarding_stop_arr['is_boarding']           = ( $ali_type == 'BRD') ? 'Y' : 'N';
            $boarding_stop_arr['is_alighting']          = ( $ali_type == 'BRD') ? 'N' : 'Y';
            
            $bs_data = $this->boarding_stop_model->where($boarding_stop_arr)->find_all();
            
            if(!$bs_data)
            {
                $boarding_stop_arr['stop_code']     = $value['busStopcode'];
                $boarding_stop_arr['route_no']      = "";
                $boarding_stop_arr['seq']           = $value['stopSequence'];
                $boarding_stop_arr['stop_status']   = 'Y';

                $this->boarding_stop_model->insert($boarding_stop_arr);
            }
        }
           
    }
    
    function insert_bus_service_stops_bus_service_wise($data, $op_id,$dep_date, $service_id, $current_bus_service) 
    {      
        $date = new DateTime(str_replace('/','-',$dep_date));
        $dep_date = $date->format('Y-m-d');
        
        $fetch_trip_master_arr = array();
        $fetch_trip_master_arr["op_trip_no"] = $service_id;
        $fetch_trip_master_arr["op_id"] = $op_id;
        
        $trip_master_arr = array();
        $trip_master_arr["op_trip_no"] = $service_id;
        $trip_master_arr["op_id"] = $op_id;
        $trip_data = $this->trip_master_model->where($trip_master_arr)->find_all();

        $total_journey_day = ($current_bus_service["arrivalDay"] > 0) ? ($current_bus_service["arrivalDay"] -1 ) : $current_bus_service["arrivalDay"];
        $trip_start_stop_departure_time =  $dep_date." ".$current_bus_service['departureTime'];
        $trip_end_stop_arrival_time = date('Y-m-d H:i:s', strtotime($dep_date.' '.$current_bus_service['arrivalTime']. '+'.$total_journey_day.'day') );
                
        if(!$trip_data)
        {   
            $busServiceStop = $data['BusServiceStopsResponse']['busServiceStops']['busServiceStop'];

            if(isset($busServiceStop['serviceId']))
            {
                $busServiceStop = array($busServiceStop);
            }

            if(count($busServiceStop) >0)
            {
                // $total_journey_day = (end($busServiceStop)['day'] > 0) ? (end($busServiceStop)['day'] -1 ) : end($busServiceStop)['day'];
                /*$total_journey_day = ($current_bus_service["arrivalDay"] > 0) ? ($current_bus_service["arrivalDay"] -1 ) : $current_bus_service["arrivalDay"];
                $trip_start_stop_departure_time =  $dep_date." ".$current_bus_service['departureTime'];
                $trip_end_stop_arrival_time = date('Y-m-d H:i:s', strtotime($dep_date.' '.$current_bus_service['arrivalTime']. '+'.$total_journey_day.'day') );*/
                
                $op_bus_type_detail = $this->op_bus_types_model->where(["op_bus_type_cd" => $current_bus_service['busType'], "op_id" => $op_id])->find_all();
 
                $op_seat_layout_arr = array(
                                                "op_id" => $op_id,
                                                "layout_name" => $current_bus_service['busFormatCd']
                                            );

                $op_seat_layout = $this->op_seat_layout_model
                                        ->where($op_seat_layout_arr)
                                        ->find_all();

                $op_bus_type_id = "";
                $bus_type_id    = "";
                $seat_layout_id    = "";

                if ($op_bus_type_detail)
                {
                    $op_bus_type_detail = $op_bus_type_detail[0];
                    $op_bus_type_id = $op_bus_type_detail->id;
                    $bus_type_id    = $op_bus_type_detail->bus_type_id;
                }
                
                if ($op_seat_layout)
                {
                    $seat_layout_id = $op_seat_layout[0]->id;
                }
                
                $trip_master_arr["from_stop_name"] = $busServiceStop[0]['fromStop']['busStopName'];
                $trip_master_arr["from_stop_cd"] = $busServiceStop[0]['fromStop']['busStopcode'];
                $trip_master_arr["departure_time"] =  $dep_date." ".$current_bus_service['departureTime'];
                $trip_master_arr["sch_dept_time"] =  date("H:i:s", strtotime($current_bus_service['departureTime']));
                $trip_master_arr["to_stop_name"] = end($busServiceStop)['toStop']['busStopName'];
                $trip_master_arr["to_stop_cd"] = end($busServiceStop)['toStop']['busStopcode'];
                $trip_master_arr["arrival_time"] = date('Y-m-d H:i:s', strtotime($trip_end_stop_arrival_time));
                $trip_master_arr['bus_type_id'] = $bus_type_id;
                $trip_master_arr['op_bus_type_id'] = $op_bus_type_id;
                $trip_master_arr['seat_layout_id'] = $seat_layout_id;
                $trip_master_arr['refund_permitted']  = 'Y';
                $trip_master_arr['fare_type']         = 'C';
                $trip_master_arr['provider_id']       = 1;
                $trip_master_arr['provider_type']     = 'operator';

                $trip_master_arr['route_no']            = $current_bus_service["routeName"];
                $trip_master_arr['route_no_name']     = $current_bus_service["routeName"];

                $this->trip_master_model->insert($trip_master_arr);
                $trip_data = $this->trip_master_model->where($fetch_trip_master_arr)->find_all();
            }
       
            else
            {
                echo 'Empty array returned';
            }
        }

        if (isset($data['BusServiceStopsResponse']) && count($data['BusServiceStopsResponse']) > 0) {
            if (isset($data['BusServiceStopsResponse']['busServiceStops']) && count($data['BusServiceStopsResponse']['busServiceStops']) > 0 && isset($data['BusServiceStopsResponse']['busServiceStops']['busServiceStop'])) {
                $busServiceStop = $data['BusServiceStopsResponse']['busServiceStops']['busServiceStop'];
                
                // if(!is_array($busServiceStop))
                // {
                //     $busServiceStop = array($busServiceStop);
                // }
                if(isset($busServiceStop['serviceId']))
                {
                    $busServiceStop = array($busServiceStop);
                }

                if(count($busServiceStop) >0)
                {
                    foreach ($busServiceStop as $key => $value) {

                        if(empty($value['arrivalTime']))
                        {
                            logging_data('cron/bus/bus_'.date('d-m-y').'.log',$value,' insert_bus_service_stops empty arrival time');
                        }   

                        $boarding_stop_id       = $this->get_op_bus_stop_id($value['fromStop'], $op_id);
                        $destination_stop_id    = $this->get_op_bus_stop_id($value['toStop'], $op_id);
                        
//                        $trip_master_arr = array();
//
//                        $trip_master_arr["op_trip_no"] = $value['serviceId'];
//                        $trip_master_arr["op_id"] = $op_id;
//                        $trip_data = $this->trip_master_model->where($trip_master_arr)->find_all();

                        $travel_destinations_arr = array();
                        $travel_destinations_arr["trip_no"]             = $trip_data[0]->trip_no;
                        $travel_destinations_arr["boarding_stop_id"]    = $boarding_stop_id;
                        $travel_destinations_arr["destination_stop_id"] = $destination_stop_id;
                        $travel_destinations_arr["sch_departure_date"]  = $dep_date;
                        $travel_destinations_arr["sch_departure_tm"]    = $value['departureTime'];
                        

                        $travel_destinations_data = $this->travel_destinations_model->where($travel_destinations_arr)->find_all();
                        
                        if(!$travel_destinations_data)
                        {
                            $travel_destinations_arr["boarding_stop_name"]      = init_cap($value['fromStop']['busStopName']);
                            $travel_destinations_arr["destination_stop_name"]   = init_cap($value['toStop']['busStopName']);
                            $travel_destinations_arr["arrival_tm"]              = isset($value['arrivalTime']) ? $value['arrivalTime'] : '00:00:00';
                            $travel_destinations_arr["seat_fare"]               = $value['fare'];
                            $travel_destinations_arr["child_seat_fare"]         = $value['childFare'];
                            $travel_destinations_arr["sleeper_fare"]            = $value['sleeperFare'];
                            $travel_destinations_arr["day"]                     = $value['day'];

                            $arr_day = ($value['day'] > 0) ? ($value['day'] -1 ) : $value['day'];
                            $travel_destinations_arr["arrival_date"]          = date('Y-m-d H:i:s', strtotime($dep_date.' '.$value['arrivalTime']. '+'.$arr_day.'day') );

                            $this->travel_destinations_model->insert($travel_destinations_arr);
                        }
 
                    }
                }
                else
                {
                    echo 'Empty array returned';
                }
            }
        }
        else
        {
            logging_data('cron/bus/bus_'.date('d-m-y').'.log',$data,' insert_bus_service_stops');
            show($data,'','No data found for insert_bus_service_stops');

        }
    }

    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */