<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rsrtc_Model extends CI_Model {

    function __construct() {
        parent::__construct();
        
        $model_array = array( 'common_model','trip_master_model','bus_type_model','bus_stop_master_model','op_bus_types_model','op_seat_layout_model','op_layout_details_model','travel_destinations_model','trip_boarding_alighting_model','op_stop_master_model','boarding_stop_model');
        $this->load->model($model_array);
    }

    /*
     * @function    : insert_bus_type
     * @Desc        : insert the operator bus tuype in db
     * @table       : op_bus_types
     * 
     */

    function insert_bus_type($data, $op_id) {
        if (isset($data['return']) && count($data['return']) > 0) {
            $data = $data['return']['allBusType'];

            foreach ($data as $key => $value)
            {   
                $op_bus_types   = array(
                                        'op_id'=> $op_id, 
                                        "op_bus_type_name" => $value['busName']
                                        );

                $count = $this->op_bus_types_model
                            ->where( $op_bus_types )
                            ->count_all();

                if ($count == 0) {
                    $op_bus_types['op_bus_type_cd'] = $value['busType'];
                    $op_bus_types['is_child_concession'] = (isset($value['ischildconcession'])) ? trim($value['ischildconcession']) : "Y";
                    
                    $op_bus_types['is_seat_layout'] = isset($value['isseatlayout']) ? trim($value['isseatlayout']) : "N";
                    $op_bus_types['is_seat_number'] = isset($value['isseatnumber']) ? trim($value['isseatnumber']) : "N";
                    $op_bus_types['created_date'] = date("Y-m-d H:i:s");
                    $this->op_bus_types_model->insert($op_bus_types);
                }
            }
        }
    }

    function insert_available_services($data, $op_id, $controller) {
        // $this->common_model->truncate_table(array('generateid','op_layout_details', 'op_seat_layout', 'trip_master','op_bus_types'));
        
        if (isset($data['AvailableServiceResponse']) && count($data['AvailableServiceResponse']) > 0 && isset($data['AvailableServiceResponse']['availableServices'])) {
            $services = $data['AvailableServiceResponse']['availableServices']['service'];

            // Old
            /*if (!is_array($services)) {
                $services = array($services);
            }*/
            // New
            if (count($services) >0 && count($services) == count($services, COUNT_RECURSIVE)) {
                $services = array($services);
            }

            if(count($services) >0)
            {
                foreach ($services as $key => $value) {
                    
                    $trip_data = $this->trip_master_model->where(['op_id' => $op_id, 'op_trip_no' => $value['serviceId'] ])->find_all();

                    if (empty($trip_data)) {
                        $op_bus_type_detail = $this->op_bus_types_model->get_detail_category($value['busType'], $op_id);
                       
                        $op_seat_layout_arr = array(
                                                        "op_id" => $op_id,
                                                        "layout_name" => $value['busFormatCd']
                                                    );

                        $op_seat_layout = $this->op_seat_layout_model
                                                ->where($op_seat_layout_arr)
                                                ->find_all();

                        
                        $op_bus_type_id = "";
                        $bus_type_id    = "";

                        if (count($op_bus_type_detail) == 0) {
                            $controller->GetAllBusTypes();

                            $op_bus_type_detail = $this->op_bus_types_model->get_detail_category($value['busType'], $op_id);
                        }

                        if(count($op_bus_type_detail) == 0)
                        {
                            show("Bus Cat Not  Found ".$value['busType']);
                            continue;
                        }
                        else
                        {
                            $op_bus_type_id = $op_bus_type_detail->id;                            
                            $bus_type_id    = $op_bus_type_detail->bus_type_id;                            
                        }


                        $seat_layout_id = false;

                        
                        if ($op_seat_layout)
                        {
                            $seat_layout_id = $op_seat_layout[0]->id;
                        }
                        else
                        {
                            $seat_layout_id = $controller->getAllBusFormats($value['busFormatCd']);
                        }
                        
                        if($seat_layout_id)
                        {  
                            //Explode depotCode(Code and Name)
                            $depot = explode('-',$value['depotCode']);
                            //Explode depotdetail (Email Id and Contact Number)  
                            $depot_detail = explode(',',$value['depotContactDetails']);                   
                            $trip_inst_arr = array(
                                                    'op_trip_no'        => $value['serviceId'],
                                                    'op_id'             => $op_id,
                                                    'route_no'          => $value['routeName'],
                                                    'bus_type_id'       => $bus_type_id,
                                                    'op_bus_type_id'    => $op_bus_type_id,
                                                    'seat_layout_id'    => $seat_layout_id,
                                                    'route_no_name'     => $value['routeName'],
                                                    'refund_permitted'  => 'Y',
                                                    'fare_type'         => 'C',
                                                    'sch_dept_time'     => $value['departureTime'],
                                                    'provider_id'       => 1,
                                                    'depot_code'        => trim($depot[0]),
                                                    'depot_name'        => trim($depot[1]),
                                                    'depot_email_id'    => trim($depot_detail[0]),
                                                    'depot_contact_no'  => trim($depot_detail[1]),
                                                );

                            $trip_no  = $this->trip_master_model->insert($trip_inst_arr);

                            if($trip_no)
                            {
                                $controller->getBusServiceStops($value['serviceId'], $trip_no, $value['busType'], $value);
                            }

                        }
                        else{
                            show($value,'','Seat _id Not found');
                        }
                    }
                    else
                    {
                        $trip_no = $trip_data[0]->trip_no;
                        $controller->getBusServiceStops($value['serviceId'], $trip_no, $value['busType'],$value);
                    }

                    if(($key % 300) == 0)
                    {
                        // sleep(1);
                        $this->db->reconnect();
                    }   
                }
            }
            else
            {
                echo 'empty array';
            }
        }
        else
        {
           echo show($data,''); 
        }
    }

    function insert_bus_format($response, $op_id, $bus_format_cd) {

        if (isset($response['AllBusFormatResponse']['busDetail'])) {
            $get_all_bus_format_api_data = $response['AllBusFormatResponse']['busDetail'];
            
           // show($get_all_bus_format_api_data,'','@model : insert_bus_format');
            
            if (count($get_all_bus_format_api_data) > 0) {
                
                $data = $this->op_seat_layout_model
                                ->where(
                                        array(
                                                'op_id' => $op_id,
                                                'layout_name' => $bus_format_cd,
                                        )
                                    )
                                ->find_all();
                
                if($data)
                {
                    $seat_layout_id = $data[0]->seat_layout_id;
                }
                else
                {
                    // $seat_layout_id = $this->generateid_model->getRequiredId('op_seat_layout');
    
                    // $this->op_seat_layout_model->seat_layout_id     = $seat_layout_id;
                    $this->op_seat_layout_model->op_id              = $op_id;
                    $this->op_seat_layout_model->layout_name        = $bus_format_cd;
                    $this->op_seat_layout_model->ttl_seats          = count($get_all_bus_format_api_data['seatLayout']['seat']);
                    $this->op_seat_layout_model->no_rows            = $get_all_bus_format_api_data['rowCount'];
                    $this->op_seat_layout_model->no_cols            = $get_all_bus_format_api_data['columnCount'];
                    $this->op_seat_layout_model->no_berth           = 1;
                    $this->op_seat_layout_model->created_date       = date("Y-m-d H:i:s");
                    $seat_layout_id = $this->op_seat_layout_model->save();
                    

                    // show($get_all_bus_format_api_data['seatLayout']['seat'],'','seat');

                    if (count($get_all_bus_format_api_data['seatLayout']['seat']) > 0) {
                        foreach ($get_all_bus_format_api_data['seatLayout']['seat'] as $key => $value) {
                            $this->op_layout_details_model->op_seat_layout_id   = $seat_layout_id;
                            $this->op_layout_details_model->row_no              = $value['xCoordinate'];
                            $this->op_layout_details_model->col_no              = $value['yCoordinate'];
                            $this->op_layout_details_model->berth_no            = 1;
                            $this->op_layout_details_model->seat_no             = $value['seatNumber'];
                            $this->op_layout_details_model->seat_type           = $value['seatType']; 
                            $this->op_layout_details_model->quota_type          = $value['quota']; 
                            $this->op_layout_details_model->created_date        = date("Y-m-d H:i:s"); 
                            $op_ldm =  $this->op_layout_details_model->save();

                            // show($op_ldm);
                        }
                    }
                }
                
                return $seat_layout_id;
                // $gabf_id    = $this->op_seat_layout_model->save();
            }
        }
        else
        {
            echo 'Error : insert_bus_format';
            show($data);
        }
    }


    function insert_bus_service_stops($data, $op_id,$dep_date, $controller, $bstop, $service_data) {
        $date = new DateTime(str_replace('/','-',$dep_date));
        $dep_date = $date->format('Y-m-d');
        
        if (isset($data['BusServiceStopsResponse']) && count($data['BusServiceStopsResponse']) > 0) {
            if (isset($data['BusServiceStopsResponse']['busServiceStops']) && count($data['BusServiceStopsResponse']['busServiceStops']) > 0 && isset($data['BusServiceStopsResponse']['busServiceStops']['busServiceStop'])) {
                $busServiceStop = $data['BusServiceStopsResponse']['busServiceStops']['busServiceStop'];
                
                // if(!is_array($busServiceStop))
                // {
                //     $busServiceStop = array($busServiceStop);
                // }


                if(isset($busServiceStop['serviceId']))
                {
                    $busServiceStop = array($busServiceStop);
                }

                if(count($busServiceStop) >0)
                {
                    foreach ($busServiceStop as $key => $value)
                    {
                        $mainpulated_busStopcode = "";
                        $mainpulated_busStopcode = strtolower(trim($value["fromStop"]["busStopcode"]));
                        $manipulate_busServiceStopTime[$mainpulated_busStopcode] = [
                                                                                    "arrivalTime" => $value["arrivalTime"],
                                                                                    "day" => $value["day"],
                                                                                   ];
                    }

                    $update_arrival_day = (end($busServiceStop)['day'] > 0) ? (end($busServiceStop)['day'] -1 ) : end($busServiceStop)['day'];
                    $trip_masterarrival_date = date('Y-m-d H:i:s', strtotime($dep_date.' '.end($busServiceStop)['arrivalTime']. '+'.$update_arrival_day.'day') );

                    $update_trip_master = array();
                    $update_trip_master["from_stop_name"] = $busServiceStop[0]['fromStop']['busStopName'];
                    $update_trip_master["from_stop_cd"] = $busServiceStop[0]['fromStop']['busStopcode'];
                    $update_trip_master["departure_time"] =  $dep_date." ".$busServiceStop[0]['departureTime'];
                    
                    $update_trip_master["to_stop_name"] = end($busServiceStop)['toStop']['busStopName'];
                    $update_trip_master["to_stop_cd"] = end($busServiceStop)['toStop']['busStopcode'];
                    $update_trip_master["arrival_time"] = $trip_masterarrival_date;

                    $this->trip_master_model->update(array("trip_no" => $bstop['trip_no'],"op_id" => $op_id),$update_trip_master);

                    foreach ($busServiceStop as $key => $value) {
                        // if($key == 8)
                        //     break;       
                        $boarding_stop_id       = $this->get_op_bus_stop_id($value['fromStop'], $op_id);
                        $destination_stop_id    = $this->get_op_bus_stop_id($value['toStop'], $op_id);
                        
                        $trip_master_arr = array();

                        $trip_master_arr["op_trip_no"] = $value['serviceId'];
                        $trip_master_arr["op_id"] = $op_id;
                        $trip_data = $this->trip_master_model->where($trip_master_arr)->find_all();

                    
                        $travel_destinations_arr = array();
                        $travel_destinations_arr["trip_no"]             = $trip_data[0]->trip_no;
                        $travel_destinations_arr["boarding_stop_id"]    = $boarding_stop_id;
                        $travel_destinations_arr["destination_stop_id"] = $destination_stop_id;
                        $travel_destinations_arr["sch_departure_date"]  = $dep_date;
                        $travel_destinations_arr["sch_departure_tm"]    = $value['departureTime'];

                        $travel_destinations_arr["start_stop_sch_departure_date"]    = date('Y-m-d H:i:s', strtotime($dep_date.' '.$service_data['departureTime']) );

                        $searvice_arr_day = ($service_data['arrivalDay'] > 0) ? ($service_data['arrivalDay'] -1 ) : $service_data['arrivalDay'];
                        $travel_destinations_arr["end_stop_arrival_date"]          = date('Y-m-d H:i:s', strtotime($dep_date.' '.$service_data['arrivalTime']. '+'.$searvice_arr_day.'day') );
                        
                        $travel_destinations_data = $this->travel_destinations_model
                                                         ->where($travel_destinations_arr)
                                                         ->find_all();

                        if(!$travel_destinations_data  && $value['fare'] > 0)
                        {
                            logging_data("rsrtcdebug/rsrtc/".date("F")."/debug_".date("d-m-Y")."_log.log",$value, 'value');
                            if(!empty($manipulate_busServiceStopTime[strtolower(trim($value["toStop"]["busStopcode"]))]))
                            {
                                $manipulated_day = $manipulate_busServiceStopTime[strtolower(trim($value["toStop"]["busStopcode"]))]["day"];
                                $manipulated_arrival_time = $manipulate_busServiceStopTime[strtolower(trim($value["toStop"]["busStopcode"]))]["arrivalTime"];
                            }
                            else
                            {
                                $manipulated_day = $service_data['arrivalDay'];
                                $manipulated_arrival_time = $service_data['arrivalTime'];
                            }
                            
                            $travel_destinations_arr["boarding_stop_name"]      = init_cap($value['fromStop']['busStopName']);
                            $travel_destinations_arr["destination_stop_name"]   = init_cap($value['toStop']['busStopName']);
                            /*$travel_destinations_arr["arrival_tm"]              = isset($value['arrivalTime']) ? $value['arrivalTime'] : '00:00:00';*/
                            $travel_destinations_arr["arrival_tm"]              = isset($manipulated_arrival_time) ? $manipulated_arrival_time : '00:00:00';
                            $travel_destinations_arr["seat_fare"]               = $value['fare'];
                            $travel_destinations_arr["senior_citizen_fare"]     = $value['seniorfare'];
                            $travel_destinations_arr["child_seat_fare"]         = $value['childFare'];
                            $travel_destinations_arr["sleeper_fare"]            = isset($value['sleeperFare']) ? $value['sleeperFare'] : 0.00;
                            $travel_destinations_arr["day"]                     = $manipulated_day;
                            $travel_destinations_arr["created_date"]            = date('Y-m-d H:i:s');
                            /*$arr_day = ($value['day'] > 0) ? ($value['day'] -1 ) : $value['day'];
                            $travel_destinations_arr["arrival_date"]          = date('Y-m-d H:i:s', strtotime($dep_date.' '.$value['arrivalTime']. '+'.$arr_day.'day') );*/

                            $arr_day = ($manipulated_day > 0) ? ($manipulated_day - 1) : $manipulated_day;
                            $travel_destinations_arr["arrival_date"]          = date('Y-m-d H:i:s', strtotime($dep_date.' '.$manipulated_arrival_time. '+'.$arr_day.'day') );
                            
                            /*logging_data("rsrtcdebug/rsrtc/".date("F")."/debug_".date("d-m-Y")."_log.log",$manipulated_day, 'manipulated_day');
                            logging_data("rsrtcdebug/rsrtc/".date("F")."/debug_".date("d-m-Y")."_log.log",$manipulated_arrival_time, 'manipulated_arrival_time');
                            logging_data("rsrtcdebug/rsrtc/".date("F")."/debug_".date("d-m-Y")."_log.log",$travel_destinations_arr, 'travel_destinations_arr');
			    */

                            $this->travel_destinations_model->insert($travel_destinations_arr);
                            //logging_data("rsrtcdebug/rsrtc/".date("F")."/debug_".date("d-m-Y")."_log.log",$this->db->last_query(), 'last_query');
                        }

                        
                        $bstopdata['service_id']        = $bstop['service_id'];
                        $bstopdata['from']              = $value['fromStop']['busStopcode'];
                        $bstopdata['to']                = $value['toStop']['busStopcode'];
                        $bstopdata['bus_type_code']     = $bstop['op_bus_type'];
                        $bstopdata['trip_no']           = $bstop['trip_no'];

                        $controller->getBoardingStops($bstopdata, 'BRD');
                    }
                }
                else
                {
                    echo 'Empty array returned';
                }
            }
        }
        else
        {
            show($data,'','No data found for insert_bus_service_stops');

        }
    }
    
    
    
    function get_op_bus_stop_id($data = array(), $op_id)
    {
        if(count($data) >0)
        {            
             $op_stop_master_arr  = array(
                                            "op_id" => $op_id, 
                                            "op_stop_cd" => $data['busStopcode']
                                        );
            $op_stop_data        = $this->op_stop_master_model->where($op_stop_master_arr)->find_all();
            // $op_stop_data = $this->db->query('select * from op_stop_master where op_id = ? and op_stop_cd = ?', array($op_id, $data['busStopcode'] ) );
            // $op_stop_data = $op_stop_data->result();
            
            if($op_stop_data > 0)
            {
                $bus_stop_id                                = $op_stop_data[0]->bus_stop_id;
            }
            else
            {
                $bus_stop_master_arr = array();

                $bus_stop_master_arr["bus_stop_name"]   = ucfirst(strtolower($data['busStopName']));
                $bus_stop_master_arr["tahsil_nm"]       = isset($data['taluka']) ? ucfirst(strtolower($data['taluka'])) : '';
                $bus_stop_master_arr["state_nm"]        = ucfirst(strtolower($data['state']));
                $bus_stop_master_arr["district_nm"]     = isset($data['district']) ? ucfirst(strtolower($data['district'])) : '';

                $bus_stop_master_data = $this->bus_stop_master_model->where($bus_stop_master_arr)->find_all();
                
                if(!$bus_stop_master_data)
                {
                    $bus_stop_master_arr["insert_date"]     = date("Y-m-d H:i:s");                    
                    $bus_stop_id = $this->bus_stop_master_model->insert($bus_stop_master_arr);
                    
                }
                else
                {
                    $bus_stop_id = $bus_stop_master_data[0]->bus_stop_id;
                }
                
                $op_stop_master_arr['op_stop_name'] = ucfirst(strtolower($data['busStopName']));
                $op_stop_master_arr['bus_stop_id']  = $bus_stop_id;
                $op_stop_master_arr['created_date']  = date('Y-m-d H:i:s');

                $this->op_stop_master_model->insert($op_stop_master_arr);
                
            }
            

            return $bus_stop_id; 
        }        
        
    }
    /***incomplete function not save data**********/
     function insert_bus_fare_details($data)
    {
        $this->load->model(array('fare_details_model'));
        
        
        if(isset($data['SeatAvailabilityResponse']) && count($data['SeatAvailabilityResponse'])>0)
        {
            if(isset($data['SeatAvailabilityResponse']['serviceId']) &&
                    count($data['SeatAvailabilityResponse']['serviceId'])>0)
            {
                 $value = $data['SeatAvailabilityResponse']['serviceId'];
                
//                $this->trip_boarding_alighting_model->id                = "";
                $this->fare_details_model->tba_id            = $this->generateid_model->getRequiredId('trip_boarding_alighting');
                $this->fare_details_model->trip_no           = $value['serviceId'];
                $this->fare_details_model->trip_stop_name    = $value['fromStop']['busStopName'];
                $this->fare_details_model->tba_type          = "BRD";
                
                $this->fare_details_model->sch_dept_tm       = $value['departureTime'];
                         
                $this->fare_details_model->save();
                $this->fare_details_model->reset();
                
            }
        }        
    }

    
    function insert_boarding_stops($stops, $ali_type, $stop_code, $op_id, $data)
    {
        /*$trip_no = $data['trip_no'];
        foreach($stops as $key => $value)
        {            
            $this->op_stop_master_model->op_stop_cd     = $stop_code;
            $this->op_stop_master_model->op_id     = $op_id;
            $boarding_stop_data  = $this->op_stop_master_model->select();
            
            $this->boarding_stop_model->bus_stop_id         = $boarding_stop_data[0]->bus_stop_id;
            $this->boarding_stop_model->trip_no             = $trip_no;
            $this->boarding_stop_model->bus_stop_name       = $value['busStopName'];
            $this->boarding_stop_model->boarding_stop_name  = $boarding_stop_data[0]->op_stop_name;

            $this->boarding_stop_model->is_boarding         = ( $ali_type == 'BRD') ? 'Y' : 'N';
            $this->boarding_stop_model->is_alighting        = ( $ali_type == 'BRD') ? 'N' : 'y';
            
            $bs_data = $this->boarding_stop_model->select(false);
            
            if(count($bs_data) == 0)
            {
                $boarding_stop_id = $this->generateid_model->getRequiredId('bord_ali_stop_master');

                $this->boarding_stop_model->boarding_stop_id    = $boarding_stop_id;
                // $this->boarding_stop_model->bus_stop_id         = $op_stop_data[0]->bus_stop_id;
                // $this->boarding_stop_model->bus_stop_name       = $value['busStopName'];
                // $this->boarding_stop_model->route_no            = '';
                // $this->boarding_stop_model->boarding_stop_name  = '';
                // $this->boarding_stop_model->arrival_duration    = '';
                $this->boarding_stop_model->seq                 = $value['stopSequence'];
                $this->boarding_stop_model->stop_status         = 'Y';

                $this->boarding_stop_model->save();
                // show($this->boarding_stop_model,'','--------');
            }
        }*/
        $trip_no = $data['trip_no'];

        foreach($stops as $key => $value)
        {            
            $boarding_stop_data  = $this->op_stop_master_model
                                        ->where(array("op_id" => $op_id , "op_stop_cd" => $stop_code))
                                        ->find_all();
            
            $boarding_stop_arr = array();

            $boarding_stop_arr['bus_stop_id']           = $boarding_stop_data[0]->bus_stop_id;
            $boarding_stop_arr['trip_no']               = $trip_no;
            $boarding_stop_arr['op_trip_no']            = $data['service_id'];
            $boarding_stop_arr['bus_stop_name']         = $value['busStopName'];
            $boarding_stop_arr['from']                  = $data['from'];
           /* $boarding_stop_arr['arrival_duration']      = (isset($value['arrivalTime'])) ? $value['arrivalTime'] : "";
            $boarding_stop_arr['departure_duration']    = (isset($value['departureTime'])) ? $value['departureTime'] : "";
            */
            
            $boarding_stop_arr['bus_type']              = $data['bus_type_code'];
            $boarding_stop_arr['bus_stop_name']         = $value['busStopName'];
            $boarding_stop_arr['boarding_stop_name']    = $boarding_stop_data[0]->op_stop_name;
            /*$boarding_stop_arr['is_boarding']           = ($ali_type == 'BRD') ? 'Y' : 'N';
            $boarding_stop_arr['is_alighting']          = ($ali_type == 'BRD') ? 'N' : 'y';*/
            $boarding_stop_arr['is_boarding']           = ($ali_type == 'ALI') ? 'N' : 'Y';
            $boarding_stop_arr['is_alighting']          = ($ali_type == 'ALI') ? 'Y' : 'N';
            
            $bs_data = $this->boarding_stop_model->where($boarding_stop_arr)->find_all();
            
            if(!$bs_data)
            {
                /*$boarding_stop_arr['stop_code']     = $value['busStopcode'];*/
                $boarding_stop_arr['stop_code']     = (!empty($value['busStopcode'])) ? $value['busStopcode'] : "0";
                $boarding_stop_arr['route_no']      = "";
                $boarding_stop_arr['seq']           = $value['stopSequence'];
                $boarding_stop_arr['stop_status']   = 'Y';

                $this->boarding_stop_model->insert($boarding_stop_arr);
            }
        }
           
    }    
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
