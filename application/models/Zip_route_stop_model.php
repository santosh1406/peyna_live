<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Zip_route_stop_model extends CI_Model {
   
    function __construct() {
        parent::__construct();
	    //$this->db2 = $this->load->database('local_up_db', TRUE);	
    }
    
    public function getBusStopCode(){
        
       /* $this->db->distinct();
        $this->db->select('BUS_STOP_CD');
        $this->db->where('STATUS','Y');
        $this->db->order_by('BUS_STOP_CD', "asc");
        $query = $this->db->get( MSRTC_DB_NAME.'.bus_stop_routes_link');*/
        $result = $this->db->query("select distinct star_seller_code,star_seller_name from users where star_seller_code !=''");
        if(!empty($result)) {
            return $result->result_array();
        } else {
            return false;
        }
    }
    
    public function get_route($input){
        $this->db->distinct();
        $this->db->select('ROUTE_NO');
        $this->db->where('BUS_STOP_CD', $input['seller_cd']);
        $this->db->where('STATUS','Y');
        $query = $this->db->get( MSRTC_DB_NAME.'.bus_stop_routes_link');
        if($query->num_rows() > 0){
            return $query->result();
        }
    }
    
    public function header_of_data($table,$head,$extra_name){
        $header .=$head."\r\n";; 
        $col_header =$this->db->list_fields($table);
        $header .= "'".implode("','", $col_header) . "','".$extra_name."'\r\n";
        return $header;
    }
    
    public function create_route($bus_stop_cd,$route_no){

         $route_details = "select r.*,vr.route_nm as route_name "
                 . "FROM msrtc_replication.routes r "
                 . "INNER JOIN msrtc_replication.v_routes vr ON r.ROUTE_NO = vr.route_no "
                 . "WHERE r.ROUTE_NO = '" . $route_no . "'";
        $route_details_prepare = $this->db->query($route_details);
        $route_details_array = $route_details_prepare->result_array();

        if (count($route_details_array) > 0) {
            foreach ($route_details_array as $value) {
                $route_details_array_keys .= "'" . implode("','", $value) . "'#\r\n";
            }
        }

        return $route_details_array_keys;
    } 
    
    public function create_route_stop_csv_file($bus_stop_cd,$route_no) {

        $route_stop_details = "select rs.*,bs.DEVNAGIRI_NM"
                . " FROM msrtc_replication.route_stops rs "
                . "LEFT JOIN msrtc_replication.bus_stops bs ON bs.BUS_STOP_CD = rs.BUS_STOP_CD "
                . "WHERE rs.ROUTE_NO = '" . $route_no . "'";
        $route_stop_details_prepare = $this->db->query($route_stop_details);
        $route_stop_details_array = $route_stop_details_prepare->result_array();
        
        if (count($route_stop_details_array) > 0) {
//            $route_stop_details_array_key .= "\r\n####Route Stop Details####\r\n";
//            $route_stop_details_array_key .= "'" . implode("','", array_keys($route_stop_details_array[0])) . "','DEVNAGIRI_NM'\r\n";
            foreach ($route_stop_details_array as $value) {
                $route_stop_details_array_keys .= "'" . implode("','", $value) . "'#\r\n";
            }  
            //return $route_stop_details_array_key.$route_stop_details_array_keys;
            return $route_stop_details_array_keys;
        }

    }
    
    public function create_header_fare($route_no,$seller_pt){
        $fare_header_details_prepare = "select * FROM ".MSRTC_DB_NAME.".route_header_details_prepare WHERE `route_no` = '".$route_no."'";
        $header_details_prepare = $this->db->query($fare_header_details_prepare);
        $header_array = $header_details_prepare->result_array();

        if (count($header_array) > 0) {
            foreach ($header_array as $value) {
                $header_array_keys .= "'" . implode("','", $value) . "'#\r\n";
            }
        }

        return $header_array_keys;
    }
    
    public function create_season_fare($route_no,$seller_pt){
        $fare_season_details_prepare = "select * FROM ".MSRTC_DB_NAME.".route_season_details_prepare WHERE `route_no` = '".$route_no."'";
        $season_details_prepare = $this->db->query($fare_season_details_prepare);
        $season_array = $season_details_prepare->result_array();

        if (count($season_array) > 0) {
            foreach ($season_array as $value) {
                $season_array_keys .= "'" . implode("','", $value) . "'#\r\n";
            }
        }

        return $season_array_keys;
    }
    
    public function create_RouteDetails_fare($route_no,$seller_pt){
        $fare_route_details_prepare = "select * FROM ".MSRTC_DB_NAME.".route_details_prepare WHERE route_no = '".$route_no."'";
        $route_details_prepare = $this->db->query($fare_route_details_prepare);
        $route_array = $route_details_prepare->result_array();

        if (count($route_array) > 0) {
            foreach ($route_array as $value) {
                $route_array_keys .= "'" . implode("','", $value) . "'#\r\n";
            }
        }

        return $route_array_keys;
    }
   
}    
