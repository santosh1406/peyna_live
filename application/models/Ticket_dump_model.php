<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Ticket_dump_model extends MY_Model
{
    /**
     * Instanciar o CI
     */
    var $table  = 'ticket_dump';
    var $fields = array("id", "ticket_id","bos_ref_no", "ticket_ref_no", "pnr_no", "email", "mobile", "ticket","cancel_ticket", "journey_date", "created_by", "created_date");
    var $key    = "id";
    protected $set_modified = false;

    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
}
