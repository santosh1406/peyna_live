<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cancelled_trips_model extends MY_Model
{
    /**
     * Instanciar o CI
     */
    var $table 	= 'cancelled_trips';
    // var $fields     = "trip_no,op_trip_no,op_id,route_no,bus_type_id,op_bus_type_id,seat_layout_id,route_no_name,effective_from,effective_till,trip_type,refund_permitted,fare_type,sch_dept_time,sch_dept_loc,trip_running_time,operator_id,commision_type";
    var $fields		= array("id","trip_no","op_trip_no","op_id","provider_type","provider_id","route_no","bus_type_id","op_bus_type_id","seat_layout_id","from_stop_name","from_stop_cd","to_stop_name","to_stop_cd","departure_time","arrival_time","route_no_name","effective_from","effective_till","trip_type","refund_permitted","fare_type","sch_dept_time","sch_dept_loc","trip_running_time","operator_id","commision_type","status");
    var $key        = "id";

    public function __construct()
    {
        parent::__construct();
        $this->_init();
    }   
}
