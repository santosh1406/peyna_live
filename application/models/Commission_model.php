<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Commission_model extends MY_Model {

    protected $table = 'commission';
    var $fields = array("id", "name", "from", "till", "service_tax", "commission_with_service_tax", "commission_default_flag", "commission_type", "created_at", "updated_at", "created_by", "updated_by", "status");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
        $this->load->model(array('commission_model'));
    }

    //used in commission view
    public function commission_detail_by_id($commission_id) {
        $this->db->select("
                           c.id AS commission_id,
                           c.name AS commission_name,
                           c.from AS commission_from,
                           c.till AS commission_till,
                           c.commission_with_service_tax,
                           c.commission_default_flag,
                           c.service_tax,
                           cd.id AS commission_detail_id,
                           cd.base_commission_id,
                           cd.op_id as operator_id,
                           cd.provider_operator_id AS provider_operator_id,
                           cd.provider_id AS provider_id,
                           p.name AS provider_name,
                           o.op_name AS provider_operator_name,
                           cd.base_rate_flag,
                           cd.percentage_base as base_perc,
                           cd.fixed_base as base_fixed,
                           (case when bc.bus_type=0 then 'All' when bc.bus_type=2 then 'NonAC' else bt.name end) bus_type,
                           cd.rookad_commission,
                           cd.md_commission,
                           cd.ad_commission,
                           cd.dist_commission,
                           cd.retailer_commission
                          ");
        $this->db->from("commission_detail cd");
        $this->db->where("cd.commission_id", $commission_id);
        $this->db->join("commission c", "c.id = cd.commission_id ", "inner");
        $this->db->join("base_commission bc", "bc.id = cd.base_commission_id", "inner");
        $this->db->join("api_provider p", "p.id = cd.provider_id", "inner");
        $this->db->join("api_provider_operator po", "po.id = cd.provider_operator_id", "left");
        $this->db->join("op_master o", "o.op_id = po.op_id", "left");
        $this->db->join("bus_type_home bt", "bc.bus_type = bt.id", "left");
        $this->db->order_by("o.op_id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function msrtc_commission_detail_by_id($commission_id) {
        $this->db->select("2cd.commission_id,
                           2cd.trimax_commission,
                           2cd.company_commission,
                           2cd.md_commission,
                           2cd.ad_commission,
                           2cd.dist_commission,
                           2cd.retailer_commission,
                           2cm.name as commission_name,
                           2cm.from as commission_from,
                           2cm.till as commission_till,
                           2cm.commission_default_flag
                          ");
        $this->db->from("2_commission_detail 2cd");
        $this->db->where("commission_id", $commission_id);
        $this->db->join("2_commission_master 2cm", "2cm.id = 2cd.commission_id ", "left");
        $query = $this->db->get();
        return $query->result();
    }

    //used in commission insert
    public function insert_slab_wise_commission($data = array()) {
        $ret = true;
        $provider_op_array = array_merge($data['ets_details'], $data['its_details'], $data['details']);

        if (!empty($provider_op_array)) {
            foreach ($provider_op_array as $operators => $operator_value) {

                foreach ($operator_value as $key => $value) {

                    $parset2 = implode("','", $value);
                    $parset = "'" . $data['commission_id'] . "','" . $parset2 . "'";
                    $this->db->trans_begin();
                    $s = "insert into commission_detail (commission_id,base_commission_id,op_id,provider_id,provider_operator_id,base_rate_flag,percentage_base,fixed_base,rookad_commission,md_commission,ad_commission,dist_commission,retailer_commission) values(" . $parset . ")";
                    $this->db->query($s);
                }
            }

            $this->db->trans_commit();
        }

        return $ret;
    }

    //commision id to insert in users table
    public function commission_id() {
        $date = date("Y-m-d");
        $this->db->select('id,name,created_at,status,from,till');
        $this->db->from("commission");
        $this->db->where("DATE_FORMAT(`from`, '%Y-%m-%d') <= ", date('Y-m-d', strtotime($date)));
        $this->db->where("DATE_FORMAT(till, '%Y-%m-%d') >= ", date('Y-m-d', strtotime($date)));
        $this->db->where('status', '1');
        $this->db->where('commission_default_flag', '1');
        $this->db->order_by('id', 'desc');
        $this->db->limit('1');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_active_commission($input) {
        $date = date("Y-m-d");
        $this->db->select('agent_commission_template_log.commission_id as active_commission_id');
        $this->db->from('agent_commission_template_log');
        $this->db->join("commission c", "c.id = agent_commission_template_log.commission_id", "left");
        $this->db->where("user_id", $input);
        $this->db->where("agent_commission_template_log.status", "1");
        $this->db->where("DATE_FORMAT(commission_from, '%Y-%m-%d') <= ", date('Y-m-d', strtotime($date)));
        $this->db->where("DATE_FORMAT(commission_to, '%Y-%m-%d') >= ", date('Y-m-d', strtotime($date)));
        $this->db->order_by("agent_commission_template_log.id", "desc");
        $this->db->limit('1');
        $query = $this->db->get();
        return $query->result();
    }

    //added by harshada kulkarni on 31-08-2018
    public function getDynamicFields() {
        $this->db->select('*');
        $this->db->from('dynamic_fields_master');
        $query = $this->db->get();
        return $query->result();
    }

    //added by harshada kulkarni on 03-09-2018
    public function saveCommissionOptions($data) {
        $this->db->insert('service_commission_fields', $data);
        return $id = $this->db->insert_id();
    }

    //added by harshada kulkarni on 04-09-2018
    public function getServiceCommissionFields($serviceId = "", $created_by = "") {
        $this->db->select('scf.id,scf.service_id,sm.service_name,scf.values');
        $this->db->from('service_commission_fields scf');
        $this->db->join("service_master sm", "sm.id = scf.service_id", "left");
        $this->db->where("scf.status", 'Y');
        if ($serviceId != "") {
            $this->db->where("scf.service_id", $serviceId);
        }
        $query = $this->db->get();
        return $query->result();
    }

    //added by harshada kulkarni on 06-09-2018
    public function getServicesCommissions($serviceCommissionId = "") {
        $this->db->select('*');
        $this->db->from('services_commissions');
        $this->db->where("scf_id", $serviceCommissionId);
        $this->db->where("status", 'Y');
        $query = $this->db->get();
        return $query->result();
    }

    //added by harshada kulkarni on 06-09-2018
    public function updateCommissionOptions($input = "") {
        $this->db->where('service_id', $input['service_id']);
        if ($this->db->update('service_commission_fields', $input)) {
            return true;
        } else {
            return false;
        }
    }

    //added by harshada kulkarni on 10-09-2018
    public function saveServicesCommissions($post_data = "") {
        $this->db->insert('services_commissions', $post_data);
        return $this->db->insert_id();
    }

    //added by harshada kulkarni on 10-09-2018
    public function getServicesCommissionsDetails($services_commission_id = "", $service_commission_field_id = "", $service_id = "", $default_status = "", $name = "", $created_by = "") {

        $this->db->select('id,service_id,commission_name,default_status,from,till, total_commission,values,status');
        $this->db->from('services_commissions');
        if ($services_commission_id != "") {
            $this->db->where("id", $services_commission_id);
        }
        if ($service_commission_field_id != "") {
            $this->db->where("scf_id", $service_commission_field_id);
        }
        if ($service_id != "") {
            $this->db->where("service_id", $service_id);
        }
        if ($default_status != "") {
            $this->db->where("default_status", "Y");
            $this->db->where("status", 'Y');
        }
        if ($name != "") {
            $this->db->where("commission_name", $name);
        }
        if ($created_by != "") {
            $this->db->where("created_by", $created_by);
        }
        $query = $this->db->get();
        return $query->result();
    }

    //added by harshada kulkarni on 14-09-2018
    public function checkCommissionNameEdit($services_commission_id = "", $name = "",$created_by = "") {
        $this->db->select('id,commission_name,default_status,from,till,values');
        $this->db->from('services_commissions');
        $this->db->where("id<>", $services_commission_id);
        $this->db->where("commission_name", $name);
        $this->db->where("created_by", $created_by);
        $query = $this->db->get();
        return $query->result();
    }

    //added by harshada kulkarni on 14-09-2018
    public function checkCommissionDefaultEdit($services_commission_id = "", $services_id = "", $status = "") {
        $this->db->select('id,commission_name,default_status,from,till,values');
        $this->db->from('services_commissions');
        $this->db->where("id<>", $services_commission_id);
        $this->db->where("service_id", $services_id);
        $this->db->where("default_status", $status);
        $this->db->where("status", "Y");
        $query = $this->db->get();
        return $query->result();
    }

    //added by harshada kulkarni on 06-09-2018
    public function updateServicesCommissions($input = "") {
        $this->db->where('id', $input['id']);
        if ($this->db->update('services_commissions', $input)) {
            return true;
        } else {
            return false;
        }
    }

    public function getServicesCommissionDetails($service_id = "",$sub_service_id= "", $default_status = "", $created_by = "") {

        $this->db->select('*');
        $this->db->from('services_commissions');
        
        if ($service_id != "") {
            $this->db->where("service_id", $service_id);
        }
        
        if ($sub_service_id != "") {
            $this->db->where("sub_service_id", $sub_service_id);
        }
        if ($default_status != "") {
            $this->db->where("default_status", "Y");
            $this->db->where("status", 'Y');
        }
         if ($created_by != "") {
            $this->db->where("created_by", $created_by);
        }
        $query = $this->db->get();
        
        return $query->result();
    }

     //added by vaidehi
    public function getCommissionbyRD($service_id = "", $cretaed_by) {
        $this->db->select('sub_service_id');
        $this->db->from('services_commissions');
        $this->db->where("service_id", $service_id);
        $this->db->where("created_by", $cretaed_by);
       // $this->db->where("default_status", "Y");
        $this->db->where("status", "Y");
        $query = $this->db->get();
       // show($this->db->last_query(),1);
        return $query->result();
    }

    // added by lilawati
    public function getCommissionbyId($commission_id){
        $result = $this->db->query("select * from services_commissions where id='".$commission_id."'");
        return $result->row();

    }

    //added by lilawati for mailtain log netry for update commission
    public function insertCommissionLog($data) {
       $id = $this->db->insert('services_commissions_log',$data);
    }

    //check commission exists for rd
    public function checkcommissionforRd($service_id,$input){
        $this->db->select('sub_service_id');
        $this->db->from('services_commissions');
        $this->db->where('service_id',$service_id);
        $this->db->where('sub_service_id',$input['subservice_id']);
        $this->db->where('status','Y');
        $this->db->where('created_by',$input['created_by']);
        $result = $this->db->get()->row();
        return $result;
    }
}
