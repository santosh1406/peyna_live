<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Logger_model extends MY_Model
{
    /**
     * Instanciar o CI
     */
    var $table = 'logger';
    var $fields		   = array("id","url","ip_addr","user_agent","status");
    var $key    = 'id';  
    
    public function __construct() {
        parent::__construct();
        $this->_init(); 
   
    }
    public function save_data(){
               
       $ip=$this->input->ip_address();
       $url=current_url();
       $user_agent=$this->input->user_agent();
       $data=array('url' => $url,
            'ip_addr' =>$ip,
            'user_agent'=>$user_agent,
           'inserted_date' =>date('Y-m-d'),
            'status' => 'Y');

        $this->db->insert('logger', $data); 
     

    }
    

  
        
}
