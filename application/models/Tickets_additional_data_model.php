<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tickets_additional_data_model extends MY_Model
{
    /**
     * Instanciar o CI
     */
    var $table    = 'tickets_additional_data';
    var $fields		= array("id","service_tax_amount","ticket_id","ticket_number","actual_amount","total_amount","paid_amount","tds_charges","total_discount_amount","commission_percentage","refund_amount","ticket_status","insurance_amount","traveller_contact","response","inserted_date");
    var $key      = 'id';

    protected $set_created = false;

    protected $set_modified = false;
    
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
}
