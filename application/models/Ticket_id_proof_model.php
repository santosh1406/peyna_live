<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ticket_id_proof_model extends MY_Model {

    /**
     * Instanciar o CI
     */
    var $table  = 'ticket_id_proof';
    var $fields = array("id","ticket_id","id_type","id_number","name","timestamp");
    var $key    = 'id'; 

    public function __construct() {
         parent::__construct();
        // $this->_init();      
    }

}//End of model
