<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_tpin_model extends MY_Model {
    
    var $table  = 'user_tpin';
    var $fields = array("id","user_id","tpin","created_date");
    var $key    = 'id';
	
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
	
	public function save_tpin_log($data) {        
		$flag = false;
        if (!empty($data))
		{
			if ($this->db->insert('user_tpin', $data)) {				
				$flag= $this->db->insert_id();
			} 
		}
		return $flag;
    }
	
	public function get_last_tpins($user_id) {
			$max_old_tpin = $this->config->item('max_old_tpin');
            $this->db->select('tpin');
			 $this->db->from('user_tpin');
            $this->db->where('user_id', $user_id);
			$this->db->order_by('id', 'desc');
			$this->db->limit($max_old_tpin);
			$query = $this->db->get();
			return $query->result_array();            
        }

}