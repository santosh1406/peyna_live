<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Revenue_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
    }
    
    function bos_commission_offer($input)
    {
      
        $role_id_name = ""; 
        $role_id_name = $input['role_id'];
        $employees_in = [];

        if($role_id_name == "trimax")
        {
            $this->db->select('group_concat(id) as employees')->from('users')->where("is_employee","1");
            $employee_query = $this->db->get();
            if($employee_query->num_rows())
            {
                $emp_data = $employee_query->result_array();
                $emp_data = $emp_data[0]["employees"];
                $employees_in = explode(",", $emp_data);
            } 
        }

        $this->db->select('1,
                        t.boss_ref_no AS "Boss Reference No",
                        t.pnr_no As "PNR NO",t.api_ticket_no,
                        t.booked_by AS "user type",
                        td.psgr_name as first_name,
                        t.user_email_id as email,
                        t.mobile_no,
                        t.op_name AS "Operator Name",
                        ap.name as "Povider Name",
                        t.from_stop_name as "From Stop",                      
                        t.till_stop_name as "To Stop",
                        t.inserted_date as "issued date",
                        t.dept_time as doj,
                        t.num_passgr as "No of passenger",
                        IFNULL(t.tot_fare_amt,0) AS "Basic Fare",
                        IFNULL(sum(td.service_tax),0) AS "Service Tax",
                        IFNULL(t.total_fare_without_discount,0) AS "Total Fare Without Discount",   
                        CASE t.booked_from  
                        WHEN "upsrtc" THEN
                        IF(t.total_fare_without_discount IS NOT NULL,(t.total_fare_without_discount - t.tot_fare_amt),0) ELSE 0 END AS "UPSRTC Additional Charges (3% or Rs.50)",
                        IFNULL(t.discount_on,"-") AS "Discount On",
                        IFNULL(t.discount_type,"-") AS  "Discount Type",
                        t.discount_per AS "Discount Percentage",
                        t.discount_value AS "Discount Price",
                        t.tot_fare_amt_with_tax AS "Total Fare Paid",
                        
                        IFNULL(ctd.pg_refund_amount,0) AS "Refund Amount",
                       
                        IF(ctd.is_refund=1,sum(ctd.cancel_charge),0) "Cancel Charge",
                       t.pg_tracking_id AS "PG Track Id",
                       
                       t.transaction_status AS "Transaction Status",  
                       t.ticket_ref_no
                    ');
        $this->db->from('tickets t');

        /*$this->db->join('users u', 'u.id = t.inserted_by', 'inner');*/
        $this->db->join('ticket_details td', 'td.ticket_id = t.ticket_id', 'inner');
        $this->db->join('payment_transaction_fss ptf', 'ptf.ticket_id=t.ticket_id', 'left');
        $this->db->join('api_provider ap', 't.provider_id = ap.id', 'left');
        $this->db->join('cancel_ticket_data ctd', 'ctd.ticket_id = t.ticket_id', 'left');

        if($input['provider_id'])
        {
            $this->db->where("provider_id", $input['provider_id']);
        }

        if($input['from_date'])
        {
            $this->db->where("DATE_FORMAT(issue_time, '%Y-%m-%d')>=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if($input['till_date'])
        {
            $this->db->where("DATE_FORMAT(issue_time, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['till_date'])));
        }

        if($input['tran_type'] && $input['tran_type']!='select_status')
        {
            $status=$input['tran_type'];
            if($status=='failed')
            {
                $status=array('psuccess','pfailed','failed');
            }
            $this->db->where_in('t.transaction_status', $status);
        }

      

        if($role_id_name && !$input['user_id'])
        {
            if(!in_array($role_id_name, array("trimax","sys_user")))
            {
                $this->db->where("t.role_id", $input['role_id']);
            }
            else if($role_id_name == "trimax")
            {
                $this->db->where_in("t.inserted_by", $employees_in);
            }
            else if($role_id_name == "sys_user")
            {
                eval(SYSTEM_USERS_ID_ARRAY);
                $this->db->where_in("t.role_id", $system_users_id_array);
            }
        }

        if($input['user_id'])
        {
            $this->db->where("t.inserted_by", $input['user_id']);
        }
        
        $this->db->group_by('t.ticket_id');

        $res = $this->db->get();
        //show(last_query(),1);
        return $res->result_array();
       
    }


    public function revenue_report($per_page, $page)
    {
        $this->db->start_cache();
        $data = [];
        $page   = ($page>0?$page:1);        
        $offset = $per_page*($page-1);

        if(!empty($this->session->userdata('user_type'))  && ($this->session->userdata('user_type')) == "trimax" && ($this->session->userdata('select_user') == ""))
            {   
                $this->db->select ('GROUP_CONCAT(u.id) as employee_id',false);
                $this->db->from('users u');
                $this->db->where("is_employee" ,'1');
                $employee = $this->db->get();
                $employeeids = $employee->result_array();
                if($employeeids)
                {
                    $employeeids = $employeeids[0]["employee_id"];
                    $employees_in = explode(",", $employeeids);
                }
            }
            else if(!empty($this->session->userdata('select_user')))
            {
              $employees_in = [($this->session->userdata('select_user'))];
            }//show($employees_in,1);

        $this->db->select('DATE_FORMAT(t.inserted_date, "%d-%m-%Y") AS booking_date,
                            COUNT(t.ticket_id) AS total_ticket_booked,
                            SUM(t.num_passgr) AS total_passangers,
                            SUM(t.total_fare_without_discount) AS total_fare_without_discount,
                            SUM(t.discount_value) AS discount_value,
                             ROUND(SUM(IF(t.booked_from = "upsrtc", t.fare_convey_charge, 
                                     IF(t.booked_from = "etravelsmart", (t.total_fare_without_discount*t.commision_percentage/100), "0")
                                   )
                                ),2) AS total_commission,
                            ROUND(SUM(IF(ctd.is_refund = "1",ctd.cancel_charge,"0")),2) AS total_cancel_charge,
                            ROUND(SUM(IF(ctd.is_refund = "1",ctd.actual_refund_paid,"0")),2) AS total_refund_amount',false);
        $this->db->from('tickets t');
        $this->db->join('cancel_ticket_data ctd', 'ctd.ticket_id = t.ticket_id', 'left');
        $this->db->group_by("DATE_FORMAT(t.inserted_date, '%d-%m-%Y')");
        $this->db->order_by("t.inserted_date","DESC");

        // if($input) 
        // { 
            if(!empty($this->session->userdata('trans_type')) && ($this->session->userdata('trans_type')) != "all")
            {
                $this->db->where("t.transaction_status",($this->session->userdata('trans_type')));
            }

            if(!empty($this->session->userdata('from_date')) && !empty($this->session->userdata('till_date')))
            {
                $this->db->where('t.inserted_date >=', date("Y-m-d H:i:s",strtotime($this->session->userdata('from_date'))));
                $this->db->where('t.inserted_date <=', date("Y-m-d H:i:s",strtotime($this->session->userdata('till_date'))));
            }

            if(!empty($this->session->userdata('user_type')) && !in_array(($this->session->userdata('user_type')), ["trimax","all"]))
            {
                $this->db->where("t.role_id", ($this->session->userdata('user_type'))); 
            }

//             if(!empty($this->session->userdata('select_user')) && !in_array(($this->session->userdata('select_user')), ["trimax","all"]))
//             {
//                 $this->db->where("t.inserted_by", ($this->session->userdata('select_user'))); 
//             }

            if($employees_in)
            {
                $this->db->where_in("t.inserted_by", $employees_in);
            }

        // }
        // else
        // {
        //     $this->db->where("t.transaction_status","success");
        // }

        $res = $this->db->get();
        $data['count'] = $res->num_rows();
        //$this->db->stop_cache();
        if($per_page > 0)
        {
          $this->db->limit($per_page, $offset);
        }
        $res = $this->db->get();
        /*$res = $this->db->get();
        $data['count'] = $res->num_rows();*/
        $data['data'] = $res->result();
        //$this->db->flush_cache();
        return $data;
    }

 }


?>