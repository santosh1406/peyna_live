<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Op_refund_status_model extends MY_Model {

    var $table  = 'op_refund_status';
    var $fields = array("id","user_id","service_id","transaction_date","amount","status","created_by","created_date");
    
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
    
  
}
