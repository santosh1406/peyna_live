<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Op_wallet_model extends MY_Model {

    var $table  = 'op_wallet';
    var $fields = array("user_id"," user_type","service_id","amount","virtual_balance","total_balance","current_outstanding","comment","status","created_by","created_date","updated_by","updated_date");
    
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    function get_all_services() {
        
            $this->db->select("*");
            $this->db->from("services");
            $this->db->order_by('id asc'); 
            $result = $this->db->get();
            $result_array = $result->result_array();
            return $result_array;
    }

    
    // function do_trans($op_id, $data = array(), $trans_type = 'credit'){
    function do_trans($user_id, $user_type, $data = array(), $trans_type = 'credit', $created_by = false ){
        $this->load->model(array('op_master_model','op_wallet_trans_model','op_wallet_trans_log_model'));
        log_data("track_comissi/a.log",$data,"data do_trans");
        // $user = $this->users_model->where('id', $user_id)->find_all();
        if($created_by)
        {

            $wallet     = $this->where(
                                        array(
                                                'user_id' => $user_id,
                                                'user_type' => $user_type,
                                                'service_id' => $data['service_id']
                                            )
                                    )
                                ->find_all();
             log_data("track_comissi/a.log",$wallet,"wallet_details do_trans");
               
           $amount =  ($trans_type == 'debit') ?  -(int)$data['amount'] : $data['amount'];
           

           if(isset($wallet[0]) && $wallet[0]->virtual_balance) {
                    $update_virtual_balance = $wallet[0]->virtual_balance;
                    $last_virtual_balance   = $wallet[0]->virtual_balance;
                }else{
                    $update_virtual_balance = '0';
                    $last_virtual_balance = '0';
			}
				if(isset($wallet[0]) && $wallet[0]->current_outstanding){
                     $update_current_outstanding=$wallet[0]->current_outstanding;
                }else{
                     $update_current_outstanding='0';
            }
           if(isset($wallet[0]))
           {
               $total_bal_updated = $wallet[0]->total_balance;
               $total_balance_before = $wallet[0]->total_balance;
           }else{
               $total_bal_updated = 0;
               $total_balance_before = 0;
           } 
            //show($wallet,1);
			/*code for caluculating outstanding amount- starts here*/
			if($wallet[0]->current_outstanding > 0)
			{
				if($wallet[0]->current_outstanding >= $amount)
				{
					$update_current_outstanding = $wallet[0]->current_outstanding - $amount;
					$update_virtual_balance = $wallet[0]->virtual_balance +  $amount;
					$update_actual_wallet = $wallet[0]->amount;
                    $total_bal_updated = $update_virtual_balance + $update_actual_wallet;
				}
				else
				{
					$current_outstanding = $wallet[0]->current_outstanding;
					//$virtual_balance     = $wallet[0]->virtual_balance;
					$update_current_outstanding = $wallet[0]->current_outstanding -$current_outstanding;
					
					$update_virtual_balance = $wallet[0]->virtual_balance + $current_outstanding ;
				
					$update_actual_wallet = $wallet[0]->amount + ($amount - $current_outstanding);

					$total_bal_updated = $update_virtual_balance + $update_actual_wallet;
				}
			}
			
			$op_wallet_arr = array(  
                                       // 'op_id'         => $op_id,
                                       'user_id'       => $user_id,
                                       'user_type'     => $user_type,
                                       'service_id'    => $data['service_id'],
                                       'opening_bal'   => $wallet ? $wallet[0]->amount: 0,
                                       'trans_amount'  => $amount,
                                       'amount'        => $data['amount'],
                    				   'virtual_balance' => $update_virtual_balance,
                    				   'current_outstanding'=>$update_current_outstanding,
                                       'comment'       => $data['comment'], 
                                       'total_balance' => $total_bal_updated,
                                       'status'        => $trans_type,
                                       'created_by'    => $created_by,
                                       'created_date'  => date('Y-m-d H:i:s'),
                                    );						
            $w_id = false; 
            if($wallet)
            {
				$opening_bal = $wallet[0]->amount;
				if($wallet[0]->current_outstanding > 0)
				{
					$op_wallet_arr['amount'] = $update_actual_wallet;
                    $op_wallet_arr['total_balance'] = $total_bal_updated;

				}
				else
				{
					$op_wallet_arr['amount'] = $wallet[0]->amount + $amount;
                    $op_wallet_arr['total_balance'] = $op_wallet_arr['amount'];
				}
                if($this->update($wallet[0]->id, $op_wallet_arr))
                {
                    $w_id = $wallet[0]->id;          
                    log_data("track_comissi/a.log",$this->db->last_query(),"last query_for w_id");

                }
            }
            else
            {
                $opening_bal = 0;
                $op_wallet_arr['amount'] = $amount;
                $op_wallet_arr['total_balance'] = $op_wallet_arr['amount'];
				$w_id = $this->insert($op_wallet_arr);
                log_data("track_comissi/a.log",$this->db->last_query(),"last query_wid");
            }
			/*code for caluculating outstanding amount- ends here*/
            if($w_id)
            {
				
                $op_trans_insert = array(
                                    'op_wallet_id'  => $w_id,
                                    'opening_bal'   => $opening_bal,
                                    'amount'        => $amount,
                                    'closing_bal'   => $op_wallet_arr['amount'],
                                    'comment'       => $data['comment'],
                                    'transfer'      => (isset($data['transfer'])) ? $data['transfer'] : 0,
                                    'status'        => $trans_type,
                                    'created_by'    => $created_by,
                                    'created_date'  => date('Y-m-d H:i:s'),
                                    'virtual_amount' => '0',
                                    'outstanding_amount'=>$update_current_outstanding,
                                    'total_bal_amt_before_trans'=>$total_balance_before,
                                    'total_bal_amt_after_trans'=>$op_wallet_arr['total_balance'],
                                    'virtual_amt_before_trans'=> $last_virtual_balance,
                                    'virtual_amt_after_trans'=> $update_virtual_balance
                                        );
										
                $op_trans_log_insert = array(
                                     'op_wallet_id'  => $w_id,
                                    'opening_bal'   => $opening_bal,
                                    'amount'        => $amount,
                                    'closing_bal'   => $op_wallet_arr['amount'],
                                    'comment'       => $data['comment'],
                                    'transfer'      => (isset($data['transfer'])) ? $data['transfer'] : 0,
                                    'status'        => $trans_type,
                                    'created_by'    => $created_by,
                                    'created_date'  => date('Y-m-d H:i:s'),
                                    'virtual_amount' => '0',
                                    'outstanding_amount'=>$update_current_outstanding,
                                    'total_bal_amt_before_trans'=>$total_balance_before,
                                    'total_bal_amt_after_trans'=>$op_wallet_arr['total_balance'],
                                    'virtual_amt_before_trans'=> $last_virtual_balance,
                                    'virtual_amt_after_trans'=> $update_virtual_balance
                                );
               $op_log_id = $this->op_wallet_trans_log_model->insert($op_trans_log_insert);
               $op_wallet_id = $this->op_wallet_trans_model->insert($op_trans_insert);
                log_data("track_comissi/a.log",$this->db->last_query(),"last query_op_wallet_id");
                return $w_id;
            }
            else
            {
                return false;
            }
            
        }
        else
        {
            return false; 
        }
    }
    
    // function do_trans($op_id, $data = array(), $trans_type = 'credit'){
    function do_trans_live($user_id, $user_type, $data = array(), $trans_type = 'credit', $created_by = false ){
        $this->load->model(array('op_master_model','op_wallet_trans_model'));
        log_data("track_comissi/a.log",$data,"data do_trans");
        // $user = $this->users_model->where('id', $user_id)->find_all();

        if($created_by)
        {

            $wallet     = $this->where(
                                        array(
                                                'user_id' => $user_id,
                                                'user_type' => $user_type,
                                                'service_id' => $data['service_id']
                                            )
                                    )
                                ->find_all();

            $amount =  ($trans_type == 'debit') ?  -(int)$data['amount'] : $data['amount'];            

            // show($amount,1);

            $op_wallet_arr = array(  
                                       // 'op_id'         => $op_id,
                                       'user_id'       => $user_id,
                                       'user_type'     => $user_type,
                                       'service_id'    => $data['service_id'],
                                       'opening_bal'   => $wallet ? $wallet[0]->amount: 0,
                                       'trans_amount'  => $amount,
                                       'amount'        => $data['amount'],
                                       'comment'       => $data['comment'], //'ors debit on '.date('Y-m-d H:i:s', time()),
                                       'status'        => $trans_type,
                                       'created_by'    => $created_by,
                                       'created_date'  => date('Y-m-d H:i:s'),
                                    );

            $w_id = false; 
            if($wallet){
                $opening_bal = $wallet[0]->amount;
                $op_wallet_arr['amount'] = $wallet[0]->amount + $amount;
                if($this->update($wallet[0]->id, $op_wallet_arr))
                {
                    $w_id = $wallet[0]->id;
                }
            }
            else
            {
                $opening_bal = 0;
                $op_wallet_arr['amount'] = $amount;
                $w_id = $this->insert($op_wallet_arr);
                log_data("track_comissi/a.log",$this->db->last_query(),"last query");
            }

            if($w_id)
            {
                $op_trans_insert = array(
                                            'op_wallet_id'  => $w_id,
                                            'opening_bal'   => $opening_bal,
                                            'amount'        => $amount,
                                            'closing_bal'   => $op_wallet_arr['amount'],
                                            'comment'       => $data['comment'],
                                            'transfer'      => (isset($data['transfer'])) ? $data['transfer'] : 0,
                                            'status'        => $trans_type,
                                            'created_by'    => $created_by,
                                            'created_date'  => date('Y-m-d H:i:s')
                                        );

                $op_wallet_id = $this->op_wallet_trans_model->insert($op_trans_insert);
                log_data("track_comissi/a.log",$this->db->last_query(),"last query");
                return $w_id;
            }
            else
            {
                return false;
            }
            
        }
        else
        {
            return false; 
        }
    }

    function collection($user_id, $user_type, $data = array(), $collection_type, $created_by = false)
    {
        $this->load->model(array('op_refund_status_model'));

        $op_refund_status_insert = array(
                                            'user_id'           => $user_id,
                                            'user_type'         => $user_type,
                                            'service_id'        => $data['service_id'],
                                            'transaction_date'  => date('Y-m-d',  strtotime("-1 days")),
                                            
                                            'status'            => $collection_type,
                                            // 'created_by'        => $created_by,
                                            // 'created_date'      => date('Y-m-d'),
                                        );

        $op_refund_status = $this->op_refund_status_model
                                    ->where($op_refund_status_insert)
                                    ->find_all();

        if(!$op_refund_status)
        {
            
            if($collection_type == 'collection')
            {
                $trans_type = 'debit';

            }
            else if($collection_type == 'refund')
            {
              $trans_type = 'credit';
            }
            else
            {
                $trans_type = false;
            }

            if($trans_type)
            {
                $op_refund_status_insert['created_date']    = date('Y-m-d');
                $op_refund_status_insert['amount']          = $data['amount'];
                $op_refund_status_insert['created_by']      = $created_by;

                // show($data,1);

                $done =  $this->do_trans($user_id, $user_type, $data, $trans_type, $created_by);
               
                if($done)
                {
                    return $op_refund_status_id = $this->op_refund_status_model->insert($op_refund_status_insert);
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
           // return false;
            return 'Already collection';
        }

        
    }

	function do_trans_debit($user_id, $user_type, $data = array(), $trans_type = 'debit', $debited_by = false )
	{
		$this->load->model(array('op_master_model','op_wallet_trans_model','op_wallet_trans_log_model'));
		//$this->load->model(array('op_master_model','op_wallet_trans_model'));
        log_data("track_comissi/a.log",$data,"data do_trans");
		$wallet = array();
        if($debited_by)
        {
			

            $wallet     = $this->where(
                                        array(
                                                'user_id' => $user_id,
                                                'user_type' => $user_type,
                                                'service_id' => $data['service_id']
                                            )
                                    )
                                ->find_all();
								
			if(!empty($wallet))
			{
							
				$amount = $data['amount'];            

                if(isset($wallet[0]) && $wallet[0]->virtual_balance) {
                    $update_actual_wallet = $wallet[0]->amount;
                    $update_virtual_balance = $wallet[0]->virtual_balance;
					$update_current_outstanding=$wallet[0]->current_outstanding;
                } 
			
				//if($wallet[0]->amount < $amount && $wallet[0]->virtual_balance >= $amount)
//				{	
//						$update_virtual_balance = $wallet[0]->virtual_balance - $amount;
//						$update_current_outstanding = $wallet[0]->current_outstanding + $amount;
//			     }
//			else
//				{	
//					if($wallet[0]->amount >= $amount){
//					
//						$update_actual_wallet = $wallet[0]->amount - $amount;
//						$update_total_balance = $wallet[0]->total_balance - $amount;
//					}
//					else
//					{
//						return "Insufficient balance";
//					}	
//				}

            //if($amount >= $wallet[0]->amount && $wallet[0]->virtual_balance > ($amount - $wallet[0]->amount))
            if($amount > $wallet[0]->amount)
            {          
                if($wallet[0]->amount != 0 && $wallet[0]->virtual_balance >= ($amount - $wallet[0]->amount))
                {
                        $update_actual_wallet = $wallet[0]->amount - $wallet[0]->amount;
                    	$update_virtual_balance = $wallet[0]->virtual_balance - ($amount - $wallet[0]->amount);
                        $update_current_outstanding = $wallet[0]->current_outstanding + ($amount - $wallet[0]->amount);
                }
                else if($wallet[0]->amount == 0 && $wallet[0]->virtual_balance >= $amount)
                {
                        $update_virtual_balance = $wallet[0]->virtual_balance - $amount;
                        $update_current_outstanding = $wallet[0]->current_outstanding + $amount;
                }
                else
                {
                    return "Insufficient Balance";
                }
                        
            }
            else
            {
                if($wallet[0]->amount >= $amount)
                {
                    $update_actual_wallet = $wallet[0]->amount - $amount;
                }
                else
                {
                    return "Insufficient balance";
                }
                
            }
		
			$op_wallet_arr = array(  
                                       // 'op_id'         => $op_id,
                                       'user_id'       => $user_id,
                                       'user_type'     => $user_type,
                                       'service_id'    => $data['service_id'],
                                       'opening_bal'   => $wallet ? $wallet[0]->amount: 0,
                                       'trans_amount'  => -(int)$amount,
                                       'amount'        => $data['amount'],
									   'virtual_balance' => $update_virtual_balance,
									   'current_outstanding'=>$update_current_outstanding,
                                       'comment'       => $data['comment'], 
                                       'status'        => $trans_type,
                                       'created_by'    => $created_by,
                                       'created_date'  => date('Y-m-d H:i:s'),
                                    );
									
		
            $w_id = false; 
			
            if($wallet){
				
				if($wallet[0]->amount > 0)
				{
					$opening_bal = $wallet[0]->amount;
					$op_wallet_arr['opening_bal'] = $wallet[0]->amount;
				}
				else
				{
					$opening_bal = $wallet[0]->opening_bal;
					$op_wallet_arr['opening_bal'] = $wallet[0]->opening_bal;
				}
				
			//	if($wallet[0]->amount >= $amount)
//				{
//					$op_wallet_arr['amount'] = $update_actual_wallet;
//				}
//				else
//				{
//					$op_wallet_arr['amount'] = $wallet[0]->amount;
//				}

                $op_wallet_arr['amount'] = $update_actual_wallet;
				
				$op_wallet_arr['total_balance'] = $op_wallet_arr['amount']+$op_wallet_arr['virtual_balance'];
				
                if($this->update($wallet[0]->id, $op_wallet_arr))
                {
                    $w_id = $wallet[0]->id;
                }
            }
            
			
            if($w_id)
            {
			
            $arr_opwallet     = $this->where(
                                        array(
                                                'id' => $w_id
                                            )
                                    )
                                ->find_all();
								
			/* $this->db->select('total_bal_amt_before_trans, total_bal_amt_after_trans,virtual_amt_before_trans,virtual_amt_after_trans')
			->from('op_wallet_trans')
			->where('op_wallet_id',$w_id)
			->order_by("id","desc")->limit('1');
			
			$query = $this->db->get();
			$arr_opwallettrans = $query->result();*/
			
			$wallet_amount_before_trans = $wallet[0]->total_balance;
			$wallet_amount_after_trans  = $arr_opwallet[0]->total_balance;
			$virtual_amt_before_trans   = $wallet[0]->virtual_balance;
			$virtual_amount_after_trans = $arr_opwallet[0]->virtual_balance;
			
			/*if($wallet[0]->amount < $amount && $wallet[0]->virtual_balance >= $amount)
			{
				$wallet_amount_before_trans = $wallet[0]->total_balance;
				$wallet_amount_after_trans  = $arr_opwallet[0]->total_balance;
				$virtual_amt_before_trans   = $wallet[0]->virtual_balance;
				$virtual_amount_after_trans = $arr_opwallet[0]->virtual_balance;
			}
			else
			{
				$wallet_amount_before_trans = $wallet[0]->total_balance;
				$wallet_amount_after_trans  = $arr_opwallet[0]->amount;
				$virtual_amt_before_trans   = $arr_opwallettrans[0]->virtual_amt_before_trans;
				$virtual_amount_after_trans = $arr_opwallettrans[0]->virtual_amt_after_trans;
			}*/
			$actual_wallet_amount = $arr_opwallet[0]->amount;
			$virtual_amount       = $arr_opwallet[0]->virtual_balance;
			$outstanding_amount   = $arr_opwallet[0]->current_outstanding;
				
                $op_trans_insert = array(
                                            'op_wallet_id'  => $w_id,
                                            'opening_bal'   => $opening_bal,
                                            'amount'        => -(int)$amount,
                                            'closing_bal'   => $op_wallet_arr['amount'],
											'actual_wallet_amount' =>$actual_wallet_amount,
											'virtual_amount'=>$virtual_amount,
											'outstanding_amount'=>$outstanding_amount,
											'total_bal_amt_before_trans'=>$wallet_amount_before_trans,
											'total_bal_amt_after_trans'=>$wallet_amount_after_trans,
											'virtual_amt_before_trans'=>$virtual_amt_before_trans,
											'virtual_amt_after_trans'=>$virtual_amount_after_trans,
                                            'comment'       => $data['comment'],
                                            'waybill_no'    =>$data['waybill_no'],
                                            'transfer'      => (isset($data['transfer'])) ? $data['transfer'] : 0,
                                            'status'        => $trans_type,
                                            'created_by'    => $created_by,
                                            'created_date'  => date('Y-m-d H:i:s')
                                        );
							
	
               $op_wallet_id = $this->op_wallet_trans_model->insert($op_trans_insert);
			  
                log_data("track_comissi/a.log",$this->db->last_query(),"last query");
                return $w_id;
            }
            else
            {
                return false;
            }
		}
                
        }
        else
        {
            return false; 
        }
	}



    
  
}
