<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Op_bank_detail_model extends MY_Model {

    var $table  = 'op_bank_detail';
    var $fields = array("op_bank_detail_id","op_id","bank_name","branch_address","acc_no","branch_name","branch_state","branch_city","acc_name","ifsc_code","is_status","is_deleted","created_by","created_date","updated_by","updated_date");
    
    var $key    = 'op_bank_detail_id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

    function get_op_notin_bankdetail() 
    {

        $this->db->select('om.op_id,om.op_name');
        $this->db->from('op_master om');
        $this->db->where('`op_id` NOT IN (SELECT `op_id` FROM `op_bank_detail` where `is_status`=1 and `is_deleted`=0)', NULL, FALSE);
       
        $state = $this->db->get();


        return $state->result_array();
    }
    
    
  
}

?>
