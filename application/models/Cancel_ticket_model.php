<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Role
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cancel_ticket_model extends MY_Model {

    var $table  = 'cancel_ticket_data';
    var $fields = array("id","ticket_id","td_id","pnr_no","new_pnr","seat_no","refund_amt","actual_refund_paid","cancel_charge","cancel_charge_operator","cancel_percentage","pg_refund_amount","is_manual_refund","manual_refund_reason","raw","is_status","is_deleted","created_by","created_at","updated_by","updated_at","sent_cancel_settlement_fss","refund_process_date","refund_amount_date","is_refund","cancelticket_unique");
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
    public function refund_amt($input){
         
        $data = array();
        $this->db->select("ctd.id,t.mobile_no,t.user_email_id,DATE_FORMAT(t.boarding_time,'%d-%m-%Y %h:%i:%s') as boarding_time,DATE_FORMAT(t.alighting_time,'%d-%m-%Y %h:%i:%s') as alighting_time,t.tot_fare_amt,t.op_name,t.from_stop_name,DATE_FORMAT(t.inserted_date,'%d-%m-%Y %h:%i:%s') as inserted_date,t.till_stop_name,ctd.ticket_id,ctd.pnr_no,ctd.seat_no,ctd.refund_amt,ctd.cancel_charge,td.psgr_no,DATE_FORMAT(ctd.created_at,'%d-%m-%Y %h:%is:%s') as created_at,ptf.track_id,ptf.transaction_id,ctd.sent_cancel_settlement_fss,t.boss_ref_no,DATE_FORMAT(ctd.refund_process_date,'%d-%m-%Y %h:%i:%s') as refund_process_date,DATE_FORMAT(ctd.refund_amount_date,'%d-%m-%Y %h:%i:%s') as refund_amount_date,ctd.sent_cancel_settlement_fss,if(ctd.sent_cancel_settlement_fss='0','yes','no') as text_file");
        $this->db->from("cancel_ticket_data ctd");
        $this->db->join("tickets t", "ctd.ticket_id=t.ticket_id", "inner");
        $this->db->join("ticket_details td", "td.ticket_id=t.ticket_id", "inner");
        $this->db->join("payment_transaction_fss ptf", "ptf.ticket_id=t.ticket_id", "left");
        $this->db->where("ctd.is_status", '1');
         
        if(isset($input['op_id']) && $input['op_id']!='all'){
           {
                $this->db->where('t.op_name',$input['op_id']);  
            }
           
         }
         if(isset($input['from_date']) && $input['from_date']!=''){
           $this->db->where('t.boarding_time >=',date('Y-m-d',  strtotime($input['from_date'])));  
         }
         if(isset($input['to_date']) && $input['to_date']!=''){
           $this->db->where('t.boarding_time <=',date('Y-m-d',  strtotime($input['to_date'])));  
         }
         $this->db->order_by('t.dept_time','desc');
         $this->db->group_by('ctd.id');
        $query = $this->db->get();
        $res_data = $query->result_array();
   
        if(isset($res_data) && count($res_data) <=0){
            $res_data='Data Not Found';
        }
      
        return $res_data;
       
    }
     
         
}
