<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Waybill_extra_topup_model extends MY_Model{
    
    public function check_waybill_status($waybill_no) {
        
        $this->db->select('WAYBILL_ID');
        $this->db->from(MSRTC_CB.'.cb_waybillprogramming wp');
        $this->db->join(MSRTC_CB.'.cb_duty_allocation da', 'wp.WAYBILL_NO= da.WAYBILL_NO', 'inner');
        $this->db->where('wp.WAYBILL_NO',$waybill_no);
        $this->db->where('wp.WAYBILL_STATUS','OPEN'); 
        $this->db->where('da.STATUS','STARTED');

        $data=$this->db->get();
        //echo $this->db->last_query();
        return $data->result_array();
        
    }
    
}

