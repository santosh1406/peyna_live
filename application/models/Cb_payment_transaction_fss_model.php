<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cb_payment_transaction_fss_model extends MY_Model {

    var $table  = 'cb_payment_transaction_fss';
    // var $fields = array("id","track_id","pg_track_id","payment_by","payment_id","authorization_code", "reference_no","transaction_id", "udf5","user_id","process_ticket_id", "ticket_id","total_amount","status", "ip_address", "user_agent", "response","timestamp");
    var $fields = array("id","track_id","pg_track_id",  "bank_ref_no","payment_by","payment_id","authorization_code","reference_no","transaction_id","udf5","user_id","ticket_id","return_ticket_id","total_amount","status","response","response_dv","ip_address","user_agent","timestamp","recon_log","recon_log_response");
    var $key    = 'id';
    protected $set_modified = false; 

    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

}
