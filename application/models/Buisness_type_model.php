<?php

class Buisness_type_model extends MY_Model {

    var $table = 'buisness_type';
    var $fields = array("btype_id", "btype", "status");
    var $key = 'btype_id';

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    public function get_buisnessType() {

        $sQuery = "SELECT btype_id,btype FROM buisness_type WHERE status=1";
        $query = $this->db->query($sQuery);
        return $query->result();
    }

    public function get_schemes() {

        $sQuery = "SELECT scheme_id,scheme FROM agent_scheme WHERE status=1";
        $query = $this->db->query($sQuery);
        return $query->result();
    }

    public function get_sellerpoints() {

        $sQuery = "SELECT BUS_STOP_CD,BUS_STOP_NM FROM cb_bus_stops";
        $query = $this->db->query($sQuery);
        return $query->result();
    }

    public function get_depots() {

        $sQuery = "SELECT DEPOT_CD,DEPOT_NM FROM cb_depots";
        $query = $this->db->query($sQuery);
        return $query->result();
    }

    public function get_depots_data($depot_cd) {
        if (!empty($depot_cd)) {
            $sQuery = "SELECT DEPOT_CD,DEPOT_NM FROM cb_depots WHERE DEPOT_CD='" . $depot_cd . "'";
        } else {
            $sQuery = "SELECT DEPOT_CD,DEPOT_NM FROM cb_depots";
        }
        $query = $this->db->query($sQuery);
        return $query->result();
    }

    public function get_service() {

        //$sQuery="SELECT id,service_name FROM service_master WHERE status=1 and id=2"; //commented by harshada kulkarni on 10-07-2017
        $sQuery = "SELECT id,service_name FROM service_master WHERE status=1 and id NOT IN ('5','6')"; //edited by harshada kulkarni on 10-07-2017
        //$sQuery = "SELECT id,service_name FROM static_service_master WHERE status=1"; //edited by prabhat Pal on 4--01-2019
        $query = $this->db->query($sQuery);
        return $query->result();
    }

    
    
     public function get_static_service() {

        
        $sQuery = "SELECT id,service_name,rsm_id FROM static_service_master WHERE status=1"; //edited by prabhat Pal on 4--01-2019
        $query = $this->db->query($sQuery);
        return $query->result();
    }

    // added by harshada kulkarni on 12-07-2018 -start
    // edited by sonali kamble on 21-jan-2019 -start
    public function get_agent_services($id) {
        if (!empty($id)) {
            $sQuery = "SELECT id,agent_id,service_id,depot_code,division_code FROM retailer_service_mapping WHERE agent_id='" . $id . "' and status='Y'  ";
            $query = $this->db->query($sQuery);
            return $query->result();
        } else {
            return false;
        }
    }
    // added by harshada kulkarni on 12-07-2018 -end
    
    /*BY pallavi birje on 17-01-2019 for API*/
    
    public function get_static_servicelist() {

        
        $sQuery = "SELECT `id`, `service_name`, `service_detail`, `status` FROM `service_master`"; //edited by prabhat Pal on 4--01-2019
        $query = $this->db->query($sQuery);
        return $query->result();
    }
    
       public function get_sub_servicelist() {

        
        $sQuery = "SELECT `id`, `provider_id`, `service_name`, `type`, `service_code`, `service_id`, `status` FROM `sub_services`";
        $query = $this->db->query($sQuery);
        return $query->result();
    }
    
      public function get_service_masterlist() {

        
        $sQuery ="SELECT `id`, `service_name`, `service_detail`, `status` FROM `service_master`";
        $query = $this->db->query($sQuery);
        return $query->result();
    }
    //end pallavi's changes
}

?>
