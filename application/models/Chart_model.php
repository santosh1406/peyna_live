<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Chart_model extends MY_Model {

    /**
     * Instanciar o CI
     */
    var $table = 'chart_master';
    var $fields = array("id", "route_no", "bus_type_id", "chart_name", "effective_from", "effective_till", "status");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    public function chart_info($route_no) {

        $this->db->select("cm.id,cm.route_no,cm.bus_type_id,cm.chart_name,cm.effective_from,cm.effective_till");
        $this->db->from("chart_master cm");
        $this->db->where("cm.route_no", $route_no['route_value']);
        $this->db->where("cm.bus_type_id", $route_no['bus_type']);
        if ($route_no['period_type'] == 'past_fare') {
            $this->db->where("cm.effective_till <", date('Y-m-d'));
        }if ($route_no['period_type'] == 'proposed_fare') {
            $this->db->where("cm.effective_from >", date('Y-m-d'));
        }if ($route_no['period_type'] == 'current_fare') {
            $this->db->where("cm.effective_from <=", date('Y-m-d'));
            $this->db->where("cm.effective_till >=", date('Y-m-d'));
        }
        $query = $this->db->get();

        return $query->result_array();
    }

    public function chart_details_info($route_no) {

        $this->db->select("cm.id, bsm.bus_stop_name as from_stop_name,bm.bus_stop_name as till_stop_name,cm.route_no,cm.bus_type_id,cm.chart_name,cm.effective_from,"
                . "cm.effective_till,cd.from_stop,cd.till_stop,cd.from_sequence,cd.fare,cd.till_sequence,cd.fare");
        $this->db->from("chart_master cm");
        $this->db->join("chart_details cd", 'cd.chart_mst_id=cm.id', "inner");
        $this->db->join("route_stops bsm", "bsm.bus_stop_id= cd.from_stop", "inner");
        $this->db->join("route_stops bm", "bm.bus_stop_id= cd.till_stop", "inner");
        $this->db->where("cm.route_no", $route_no['route_value']);
        $this->db->where("cm.bus_type_id", $route_no['bus_type']);
        
        if ($route_no['period_type'] == 'past_fare') {
            $this->db->where("cm.effective_till <", date('Y-m-d'));
        }if ($route_no['period_type'] == 'proposed_fare') {
            $this->db->where("cm.effective_from >", date('Y-m-d'));
        }if ($route_no['period_type'] == 'current_fare') {
            $this->db->where("cm.effective_from<=", date('Y-m-d'));
            $this->db->where("cm.effective_till >=", date('Y-m-d'));
        }

        $query = $this->db->get();
       //  show($this->db->last_query(),1);
        if (count($query) > 0) {
            return $query->result_array();
        }
    }

    public function cnt_tot_from($route_no) {
        $this->db->distinct();
        $this->db->select("cd.from_stop as from_stop ,bsm.bus_stop_name as from_stop_name,bm.bus_stop_name as till_stop_name");
        $this->db->from("chart_master cm");
        $this->db->join("chart_details cd", 'cd.chart_mst_id=cm.id', "inner");
        $this->db->join("bus_stop_master bsm", "bsm.bus_stop_id= cd.from_stop", "left");
        $this->db->join("bus_stop_master bm", "bm.bus_stop_id= cd.till_stop", "left");
        $this->db->where("cm.route_no", $route_no['route_value']);
        $this->db->where("cm.bus_type_id", $route_no['bus_type']);
        $query = $this->db->get();

        return $query->result_array();
    }

    public function chk_route_chart_master($route_id, $route_no) {
        $this->db->select('id,route_no');
        $this->db->where("route_no", $route_no);
        $this->db->from('chart_master');
        $query = $this->db->get();
        $data = $query->result_array();
        $data_staus['route_no'] = $route_no;
        if (count($data) >= 1) {
            $data_staus['status'] = '1';
        } else {

            $data_staus['status'] = '0';
        }
        return $data_staus;
    }

}
