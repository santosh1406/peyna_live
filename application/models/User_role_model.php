<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of users
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class User_role_model extends MY_Model {
    
    var $table  = 'user_roles';
    var $fields = array("id","role_name", "record_status", "home_page", "front_back_access");
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

    public function getRolename($id)
    {

    	$this->db->select('role_name');
    	$this->db->from('user_roles');
    	$this->db->where('id',$id);
    	$res = $this->db->get();
    	
    	return $res->result_array();
    }
    
}
