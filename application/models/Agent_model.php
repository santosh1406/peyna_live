<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agent_model extends MY_Model {

    var $table = 'agent_master';
    var $fields = array("agent_id", "agent_code", "fname", "parent_agent_id", "profile_img", "added_by", "user_id", "agent_hierarchy", "parent", "level", "level1", "level2", "level3", "level4", "mname", "lname", "dob", "address", "gender", "bank_acc_name", "email", "contact_no", "pincode", "country_id", "state_id", "depot_name", "zone_name", "depot_cd", "zone_cd", "city_id", "level_id", "added_by", "is_status", "is_deleted", "created_by", "created_date", "updated_by", "updated_date");
    var $key = "agent_id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    function doc_type() {
        $this->db->select('doc_cat_id,category_name');
        $this->db->from('doc_category');
        $data = $this->db->get();
        return $data->result_array();
    }

    function doc_name($id) {
        $this->db->select("agent_docs_list,category,docs_name");
        $this->db->where('category', $id);

        $this->db->from('agent_docs_list');
        $data = $this->db->get();
        return $data->result_array();
    }

    function get_doc_by_category_id($id) {

        $this->db->select("agent_docs_list,docs_name");
        $this->db->where('category', $id);
        $this->db->from('agent_docs_list');
        $result = $this->db->get();

        $data = array();

        foreach ($result->result_array() as $key => $value) {
            $data[$value['agent_docs_list']] = $value;
        }

        return $data;
    }

   
    function get_agent_data($id) {
        $this->db->where('adm.agent_id', $id);
        $this->db->join('agent_docs_list adl', 'adm.docs_name=adl.agent_docs_list', 'inner');
        $this->db->where('adm.is_status', 1);
        $this->db->where('adm.is_deleted', 0);
        $this->db->from('agent_docs_master adm');
        $res = $this->db->get();
        return $res->result_array();
    }

   

    function delete_doc($doc_nm, $id) {


        $data = array("is_status" => 0, "is_deleted" => 1, "updated_by" => $this->session->userdata('user_id'));
        $this->db->where('docs_name=', $doc_nm);
        $this->db->where('agent_id=', $id);
        $this->db->update('agent_docs_master', $data);
    }

    function get_image_name($doc_id, $ag_id) {
        $this->db->select('*');
        $this->db->from('agent_docs_master');
        $this->db->where('agent_id', $ag_id);
        $this->db->where('docs_name', $doc_id);
        $this->db->where('is_status', 1);
        $this->db->where('is_deleted', 0);
        $data = $this->db->get();
        return $data->result_array();
    }
    
    function save_agent_docs_detail_batch($data) {

        $this->db->insert_batch('agent_docs_master', $data);
    }
    
    function selected_doc_name($doccat,$doc_nm) {
        $testArray = array();
        for ($i = 0; $i < count($doccat); $i++) {
            $this->db->select("docs_name");
            $this->db->where('category', $doccat[$i]);
            $this->db->where('agent_docs_list', $doc_nm[$i]);
            
            $this->db->from('agent_docs_list');
            $data = $this->db->get();
            $val = $data->result_array();
            $testArray[$i] = $val[0]['docs_name'];
        }
        return $testArray;
    }
}

// End of Agent_model class
