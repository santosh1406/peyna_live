<?php
/**
 * Description of user_depot
 * 
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_depot_model extends MY_Model {
    public $table = 'user_depot';
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
}
