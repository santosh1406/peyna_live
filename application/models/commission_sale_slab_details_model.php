<?php


if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Commission_sale_slab_details_model extends MY_Model {

    protected $table = 'commission_sale_slab_details';
    var $key = 'id';
    var $fields = array("id", "commission_details_id", "apply_on", "from", "to", "type", "bos_commission","agent_commission");

    public function __construct() {
        parent::__construct();
    }

}

?>
