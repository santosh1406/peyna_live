<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Wallet_trans_model extends MY_Model {

    var $table  = 'wallet_trans';
    var $fields = array("id","w_id","amt","merchant_amount","actual_wallet_amount","virtual_amount","outstanding_amount","wallet_type","comment","status","user_id","amt_before_trans","amt_after_trans","remaining_bal_before","remaining_bal_before","cumulative_bal_before","cumulative_bal_after","amt_transfer_from","amt_transfer_to","wallet_amt_after_trans","virtual_amt_before_trans","virtual_amt_after_trans","credit_detail_id","extra","ticket_id","return_ticket_id","topup_id","transaction_type_id","transaction_type_id","is_status","added_on","added_by");
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    // to fetch complete detail.
    function wallet_trans_detail()
    {        
        $user_id = $this->session->userdata('user_id');
        //$this->db->select('wt.id,wt.amt, wt.status,transaction_type, DATE_FORMAT(wt.added_on,"%d-%m-%Y %h:%i:%s") as added_date,wt.amt_before_trans,wt.comment,wt.amt_after_trans,t.boss_ref_no',false);

        $this->db->select('wt.id,wt.amt_before_trans,transaction_type,wt.comment,wt.status,ld.title,wt.amt, DATE_FORMAT(wt.added_on, "%d-%m-%Y")as added_date,t.boss_ref_no,wt.amt_after_trans');
        $this->db->from('wallet_trans wt');
        $this->db->join('lookup_detail ld', "ld.id = wt.transaction_type_id",'left');
        $this->db->join('tickets t', "t.ticket_id = wt.ticket_id",'left');
        $this->db->where('user_id',$this->session->userdata('user_id'));
		
        if (isset($input['from_date']) && $input['from_date']!='')
        {
          $this->db->where("DATE_FORMAT(added_on, '%Y-%m-%d') >=",date('Y-m-d', strtotime($input['from_date'])));
        }
        if (isset($input['till_date']) && $input['till_date']!='') 
        {
          $this->db->where("DATE_FORMAT(added_on, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['till_date'])));
        }
        $this->db->order_by('id','desc');
        $this->db->limit(20);

        $agentData = $this->db->get();
        if ($agentData->num_rows() > 0)
        {
            return $agentData->result_array();
        }
            return FALSE;
    }
	
	function agent_topup_request($input)
    {
        if (!empty($input)){
			if($this->db->insert('wallet_trans', $input))
			{
				return $this->db->insert_id();
			}
				
          }
        else{
            return false;
        }
    
        
    }

    function transaction_mismatch_ticketDetail()
    {
        $this->db->select("sum(case when t.ticket_id then 1 else 0 end) as ticket_count,t.ticket_id,wt.user_id,t.ticket_ref_no,t.tot_fare_amt,t.tot_fare_amt_with_tax,t.booked_from,t.booked_by,t.inserted_date,t.transaction_status,wt.id,wt.w_id,wt.amt");
        $this->db->from('wallet_trans  wt');
        $this->db->join('tickets  t', "t.ticket_id = wt.ticket_id",'left');
        $this->db->where_in('transaction_status', array('failed','psuccess'));
        $this->db->group_by('t.ticket_id');
        $TicketDetail = $this->db->get();
        if ($TicketDetail->num_rows() > 0)
        {
            return $TicketDetail->result_array();
        }
        return false;
    }
    
    function  get_wallet_booked_ticket($tick_id)
    { 
        $this->db->select("id,w_id,amt,merchant_amount,user_id,ticket_id,transaction_type_id,transaction_type,amt_before_trans,amt_before_trans,amt_after_trans");
        $this->db->from("wallet_trans");
        $this->db->where("ticket_id", $tick_id);
        $this->db->or_where("return_ticket_id", $tick_id);
        $this->db->where("transaction_type_id",5);
        $this->db->where("transaction_type_id !=",8);
        $wallet_ticket = $this->db->get();

        if($wallet_ticket -> num_rows() > 0)
        {
            return $wallet_ticket->result();
        }
        return false;
    }
}



