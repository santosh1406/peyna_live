<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Merchant_model extends MY_Model {
    
    var $table  = 'merchants';
    
    var $fields = array("id","type","company_name","first_name","last_name","address_line_1","address_line_2","state_id","city_id","pincode","pan_card","email", "mobile", "alternate_contact_no");

    var $key    = "id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

}