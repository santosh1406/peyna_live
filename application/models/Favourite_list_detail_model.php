<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Favourite_list_detail_model extends MY_Model {

    var $table  = 'favourite_list_detail';
    var $fields = array("id","fav_id","fname","age","gender","is_deleted","status");
    // var $fields = "id,w_id,user_id,amt,status,expire_at,comment,added_by,added_on";
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
    public function update_detail_list($someArray){
        foreach ($someArray["status"] as $key => $value) {
            $data = array("is_deleted"=>'Y');
            $this->db->where('id',$value['id']);
            $this->db->update('favourite_list_detail', $data);
              
        }
    }
    public function update_detail_data($value,$data){
        $this->db->where('id',$value);
       if($this->db->update('favourite_list_detail', $data)){
            return true;
        } else {
            return false;
        }
    }
 
            
}
?>