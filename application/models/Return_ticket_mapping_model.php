<?php

class Return_ticket_mapping_model extends MY_Model {

    /**
     * Instanciar o CI
     */
    var $table  = 'return_ticket_mapping';
    var $fields = array("id", "ticket_id" ,"return_ticket_id", "timestamp");
    var $key    = "id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }


    public function get_rpt_mapping($parent_ticket_id)
    {
        if ($parent_ticket_id != "") {
            $this->db->select("t.*");
            $this->db->from("return_ticket_mapping rtm");
            $this->db->join('tickets t', 't.ticket_id = rtm.return_ticket_id', 'inner');
            $this->db->where("rtm.ticket_id = '" . $parent_ticket_id . "'");
            $query = $this->db->get();
            $result = $query->result();
            return $result;
        }
    }


    public function check_is_return_ticket_refund($ticket_id)
    {
        if ($ticket_id != "")
        {
            $this->db->select("t.ticket_id AS onward_ticket_id,
                                t.transaction_status AS onward_transaction_status,
                                t1.ticket_id AS return_ticket_id,
                                t1.transaction_status AS return_transaction_status");
            $this->db->from("return_ticket_mapping rtm");
            $this->db->join('tickets t', 't.ticket_id = rtm.ticket_id', 'left');
            $this->db->join('tickets t1', 't1.ticket_id = rtm.return_ticket_id', 'left');
            $this->db->where('rtm.ticket_id', $ticket_id);
            $this->db->or_where('rtm.return_ticket_id', $ticket_id);
            $query = $this->db->get();
            $result = $query->result();
            return $result;
        }
        else
        {
            return false;
        }
    }

	public function check_is_return_ticket_cancellable($ticket_id)
    {
        if ($ticket_id != "")
        {
            $this->db->select("t.ticket_id AS onward_ticket_id,
                                t.transaction_status AS onward_transaction_status,
                                t1.ticket_id AS return_ticket_id,
                                t1.transaction_status AS return_transaction_status");
            $this->db->from("return_ticket_mapping rtm");
            $this->db->join('tickets t', 't.ticket_id = rtm.ticket_id', 'left');
            $this->db->join('tickets t1', 't1.ticket_id = rtm.return_ticket_id', 'left');
            $this->db->where('rtm.ticket_id', $ticket_id);
            $this->db->or_where('rtm.return_ticket_id', $ticket_id);
            $query = $this->db->get();
            $result = $query->result();
            return $result;
        }
        else
        {
            return false;
        }
    }

    public function check_return_ticket_refund_condition($ticket_id)
    {
        if ($ticket_id != "")
        {
            $this->db->select("t.ticket_id AS onward_ticket_id,
                                t.transaction_status AS onward_transaction_status,
                                t1.ticket_id AS return_ticket_id,
                                t1.transaction_status AS return_transaction_status,
                                ctd.created_at AS onward_cancel_date,
                                ctd1.created_at AS return_cancel_date"
                             );
            $this->db->from("return_ticket_mapping rtm");
            $this->db->join('tickets t', 't.ticket_id = rtm.ticket_id', 'left');
            $this->db->join('tickets t1', 't1.ticket_id = rtm.return_ticket_id', 'left');
            $this->db->join('cancel_ticket_data ctd', 'ctd.ticket_id = t.ticket_id', 'left');
            $this->db->join('cancel_ticket_data ctd1', 'ctd1.ticket_id = t1.ticket_id', 'left');
            $this->db->where('rtm.ticket_id', $ticket_id);
            $this->db->or_where('rtm.return_ticket_id', $ticket_id);
            $query = $this->db->get();
            $result = $query->result();
            return $result;
        }
        else
        {
            return false;
        }
    }
	
	 public function get_parent_ticketid_mapping($return_ticket_id)
    {
        if ($return_ticket_id != "") {
            $this->db->select("t.*");
            $this->db->from("return_ticket_mapping rtm");
            $this->db->join('tickets t', 't.ticket_id = rtm.ticket_id', 'inner');
            $this->db->where("rtm.return_ticket_id = '" . $return_ticket_id . "'");
            $query = $this->db->get();
            $result = $query->result();
            return $result;
        }
    }

}

?>
