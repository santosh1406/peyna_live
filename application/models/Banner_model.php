<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Banner_model extends MY_Model {
    
    var $table  = 'banner';
    var $fields = array("id","type","name","path","filename","from","till","status","deleted","created_at","updated_at");
    var $key    = "id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

}