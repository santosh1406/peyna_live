<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Tickets_model extends MY_Model
{
    /**
     * Instanciar o CI
     */
    var $table    = 'tickets';
    var $fields		= array("ticket_id","pnr_no","ticket_ref_no","boss_ref_no","operator_ticket_no","bos_ticket_digit","bos_ticket_string","pg_tracking_id","api_ticket_no","ticket_type","op_id","provider_id","provider_type","partial_cancellation","commision_percentage","inventory_type","bus_service_no","route_schedule_id","request_type","ticket_status","dept_time","alighting_time","boarding_time","droping_time","from_stop_cd","till_stop_cd","from_stop_name","till_stop_name","boarding_stop_name","destination_stop_name","boarding_stop_cd","destination_stop_cd","pickup_address","num_passgr","tot_fare_amt","tot_fare_amt_with_tax","total_fare_without_discount","adult_basic_fare","child_basic_fare","fare_acc","fare_hr","fare_it","fare_octroi","fare_other","fare_reservationCharge","fare_sleeperCharge","fare_toll","fare_convey_charge","service_tax","ac_service_tax","asn_fare","discount_value","discount_flat_value","discount_per","discount_type","discount_on","cancellation_rate","issue_time","pay_id","session_no","op_name","booked_from","guid","user_email_id","role_id","booked_by","user_type","mobile_no","agent_email_id","agent_mobile_no","eticket_count","payment_by","payment_mode","inserted_date","inserted_by","status","transaction_status","is_partially_cancelled","failed_reason","id_proof_required","is_return_journey","return_ticket_id","bus_type","mticket","cancellation_policy","payment_confirmation","is_transaction_confirmed","merchant_amount","pg_charges","mobile_api_user_id","ip_address","coupon_discount_amount","coupon_id","tds_per","agent_commission_amt","total_basic_amount","op_service_charge","discount_id","total_basic_with_ac");
    var $key    = 'ticket_id';

    protected $set_created = false;

    protected $set_modified = false;
    
    public function __construct() {
        parent::__construct();
        $this->_init();      

        $this->load->model(array('ticket_details_model'));

      //  $this->msrtc_db = $this->load->database('msrtc_app', true);
    }
    
    function get_user_id_by_ticket_detail($email,$pnr)
    {
      $this->db->select("u.user_id");
      $this->db->from("users u");
      $this->db->join("tickets t", "t.user_email_id = u.email", "inner");
      $this->db->where("t.user_email_id",$email);
      $this->db->where("t.pnr_no",$pnr);
      $this->db->where("t.dept_time >= " , date("Y-m-d"));
      $this->db->where("t.status",'Y');
      $this->db->where("t.ticket_status <>",'D');
      $query = $this->db->get();
      // /show($this->db->last_query(),1);
      return $query->result_array();
    }
	
    function get_search($data,$page = 0 ,$per_page = NULL)
    { 
      $result = array();

      $result['data'] = array();
      $result['count'] = 0;
    
      if($data['user_id'] > 0 ||($data['email_id'] !='' && ($data['pnr_no'] !='' || $data['bos_ref_no'] !='' || $data['ticket_id'] !='')) ) 
      {
		
        $curr_date=date('Y-m-d H:i:s');

        $this->db->start_cache();

        //$this->db->select("distinct tm.op_name, tm.ticket_id,tm.ticket_ref_no,tm.ticket_no,tm.dept_time,tm.dept_time,td.psgr_name,tm.boss_ref_no,tm.from_stop_cd,tm.till_stop_cd,tm.pnr_no,td.psgr_age,td.psgr_sex,tm.num_passgr,tot_fare_amt",false);
        $this->db->select("distinct tm.op_name, tm.destination_stop_name,tm.role_id,tm.booked_by, tm.boarding_stop_name,tm.discount_value, tm.provider_id,tm.op_id,tm.provider_type, tm.user_email_id, tm.partial_cancellation, tm.inserted_date,tm.ticket_id,tm.ticket_ref_no,tm.dept_time,tm.dept_time,td.psgr_name,tm.boss_ref_no,tm.from_stop_cd,tm.till_stop_cd,tm.from_stop_name,tm.till_stop_name,tm.pnr_no,td.psgr_age,td.psgr_sex,tm.num_passgr,tm.tot_fare_amt,tm.tot_fare_amt_with_tax,tm.payment_by,tm.total_fare_without_discount",false);

        $this->db->from($this->table."  tm");
        // $this->db->join("users u", "tm.inserted_by = u.user_id", "inner");
        $this->db->join("ticket_details td", "td.ticket_id = tm.ticket_id ", "inner");
        $this->db->join("cancel_ticket_data ct", "tm.ticket_id = ct.ticket_id", "left");

        if($data['user_id'] !='' && $data['user_id'] > 0 )
        {
          //if(isset($data['is_support']))
            $this->db->where("tm.inserted_by",$data['user_id']);
        }
        
         if(isset($data['ticket_id']) && $data['ticket_id'] !='')
            $this->db->where("tm.ticket_id",$data['ticket_id']);
         
        if($data['email_id'] !='' && ($data['pnr_no'] !='' || $data['bos_ref_no'] !=''))
        {
          $this->db->where("tm.user_email_id", $data['email_id']);

          if($data['pnr_no'] !='')
            $this->db->where("tm.pnr_no",$data['pnr_no']);

          if($data['bos_ref_no'] !='')
            $this->db->where("tm.boss_ref_no",$data['bos_ref_no']);
           
        }

        $this->db->where('tm.dept_time >=' , $curr_date);
        $this->db->where("tm.status",'Y');
        $this->db->where("tm.transaction_status",'success');
        /*$this->db->where("tm.ticket_type <>",'X');*/
        $this->db->order_by("tm.ticket_id","desc");
        $this->db->group_by("tm.ticket_id");
        $query = $this->db->get();
        $result['count'] = $query->num_rows();

        $this->db->stop_cache();

       if($per_page > 0)
        {
          $this->db->limit($per_page, $page);
        }

        $query = $this->db->get();

        $result['data'] = $query->result_array();
        $this->db->flush_cache();

        
        foreach ($result['data'] as $key => $value) {
          $this->ticket_details_model->ticket_id = $value['ticket_id'];          
          $result['data'][$key]['detail_arr'] =  $this->ticket_details_model->select();
        }
      }      

      $this->db->flush_cache();
      return $result;
    }
    
    
     function getpass_model($tkt_ref) {
       $this->db->select("tkt.ticket_ref_no,tkt.pnr_no,tde.psgr_name,tde.psgr_age,tde.psgr_sex,tde.seat_no,tde.fare_amt,tde.id", false);
       $this->db->from("tickets tkt");
       $this->db->join("ticket_details tde", "tde.ticket_id = tkt.ticket_id and tde.status='Y'", "inner");
       $this->db->where("tkt.ticket_ref_no",$tkt_ref);
       $this->db->where("tkt.status",'Y');
       $query = $this->db->get();
    //  show($this->db->last_query(),1);
       return $query->result_array();

    }    
    
          
    function cancel_booking($update_travel_data) {
       // show($update_travel_data,1);
       if(isset($update_travel_data) && count($update_travel_data) > 0){
            $this->db->select('*');
            $this->db->from('tickets t');
            $this->db->where('t.status','Y');
            $this->db->where('td.status','Y');
            $this->db->where('t.pnr_no',$update_travel_data['pnr_no']);
            $this->db->join('ticket_details td','t.ticket_id=td.ticket_id',"inner");
            $query = $this->db->get();
            $data = $query->result_array();
            $update = array();
            if(count($data) > 0)
            {
                $update = array();
                $update_data = array(
                'status' => 'N' );

                $this->db->where('ticket_id', $update_travel_data['ticket_id']);
                $this->db->where_in('seat_no', explode(',',$update_travel_data['var_seat_no']));
                $this->db->update('ticket_details', $update_data); 
            }
      
            if(count($data) ==1){
                $update = array();
                $mast_update_data = array(
                'status' => 'N',
                'ticket_status'=>'D');

                $this->db->where('pnr_no',$update_travel_data['pnr_no']);

                $this->db->update('tickets', $mast_update_data); 
            }
           
        }
    }
    
    function current_booking_data($data){
       $this->db->select("tkt.*,cb.route_no,r.from_stop,r.end_stop", false);
       $this->db->from("tickets tkt");
       $this->db->join("current_booking_bus_selection cb", "cb.bus_service_no=tkt.bus_service_no", "inner");
       $this->db->join("routes r", " cb.route_no=r.route_no", "inner");
       $this->db->where("tkt.ticket_id",$data);
      
       $this->db->where("tkt.status",'Y');
       $query = $this->db->get();
    //  show($this->db->last_query());
       $data = $query->result_array();
       return $data;

    }
    
     /*
     * @Author      : Suraj Rathod
     * @function    : get_ticket_detail()
     * @param       : $user_id -> Id of user
     * @detail      : return booked ticket detail by specified user id.
     *                 
     */

    function get_ticket_detail($user_id) {
        if ($user_id != "") {
            $this->db->select('t.ticket_id,t.boss_ref_no,t.num_passgr,t.tot_fare_amt,t.ticket_ref_no,t.pnr_no,t.status as ticket_status,td.status as ticket_detail_status,
                           t.user_email_id,t.mobile_no,t.boarding_stop_cd,t.destination_stop_cd,t.ticket_ref_no,
                           td.psgr_name,td.psgr_age,td.psgr_sex,td.fare_amt,td.seat_no');
            $this->db->from('tickets t');
            $this->db->join('ticket_details td', 't.ticket_id = td.ticket_id', "inner");
            $this->db->where('t.inserted_by', $user_id);
            //$this->db->group_by("t.ticket_no,t.status");
            $query = $this->db->get();
            $data = $query->result_array();
            return $data;
        }
    }
    
    function get_booked_tickets_count($input)
    {
        $this->db->select('t.pnr_no, t.dept_time, t.from_stop_name, t.till_stop_name, t.num_passgr, t.tot_fare_amt, t.op_name');
        
            if(isset($input['op_id']) && $input['op_id']!='' && $input['op_id']!='all')
                {
                    $this->db->where('op_name',$input['op_id']);
                }
      
          $from_date  = date('Y-m-d',strtotime($input['from_date']));

          $to_date    = date('Y-m-d',strtotime($input['to_date']));
         
        if(isset($input['from_date']) && $input['from_date']!='')
             {
                    $this->db->where("DATE_FORMAT(dept_time, '%Y-%m-%d') >=",$from_date);              
             }
        if(isset($input['to_date']) && $input['to_date']!='')
             {
                     $this->db->where("DATE_FORMAT(dept_time, '%Y-%m-%d') <=",$to_date);
             }
        
        if(isset($input['ticket_status']) && $input['ticket_status']!='' && $input['ticket_status']!='all')
            {
                $this->db->where('status',$input['ticket_status']);
            }
        $this->db->where('pnr_no!=" "');
        $query      = $this->db->get('tickets t');
            //show($this->db->last_query(),1);
        if($query->num_rows()>0) 
            {
                return $query->num_rows();
            }
        return false;
       }
    
    function get_booked_tickets($input,$per_page, $page) {
        
        $page	= ($page>0?$page:1);		
        $offset	= $per_page*($page-1);
        $from_date  = date('Y-m-d',strtotime($input['from_date']));
        $to_date    = date('Y-m-d',strtotime($input['to_date']));
        $this->db->select('t.boss_ref_no,t.boarding_stop_name,t.destination_stop_name,t.ticket_id, t.pnr_no, t.ticket_id, t.dept_time, t.from_stop_name, t.till_stop_name, t.num_passgr, t.tot_fare_amt, t.op_name,t.transaction_status');
        
        if(isset($input['op_id']) && $input['op_id']!='' && $input['op_id']!=''){
            $this->db->where('op_name',$input['op_id']);
        }
        
        if(isset($input['ticket_status']) && $input['ticket_status']!='' && $input['ticket_status']!=''){
            $this->db->where('t.transaction_status',$input['ticket_status']);
        }
        
          
        if((isset($input['from_date']) && isset($input['to_date'])) && (($input['from_date']!="") && ($input['to_date']!="")))
            { 
            
                $this->db->where("DATE_FORMAT(inserted_date, '%Y-%m-%d') BETWEEN '".$from_date."' AND '".$to_date."' ");
               
          }
          else if($input['from_date']!='' && $input['to_date']=='')
            {
                $this->db->where("DATE_FORMAT(inserted_date, '%Y-%m-%d') >=",$from_date);
              
            }
         else if($input['from_date']=='' && $input['to_date']!='')
            {
                $this->db->where("DATE_FORMAT(inserted_date, '%Y-%m-%d') <=",$to_date);
            }
       
        $this->db->where('pnr_no!=" "');
        eval(FME_AGENT_ROLE_NAME);
        if(in_array($this->session->userdata("role_name"), $fme_agent_role_name))
        {
            $this->db->where("t.inserted_by", $this->session->userdata("user_id"));
        }
        //$this->db->limit($per_page,$offset);
         $this->db->order_by("t.ticket_id","DESC");
         $query = $this->db->get('tickets t');
       // last_query(1);
        if($query->num_rows()>0) {
            return $query->result_array();
        }
        return "Data Not Found";
    }
    


    function current_booking_tot_psgr_data($saved_data){
       $this->db->select("td.psgr_age,td.psgr_no", false);
       $this->db->from("tickets tkt");
       $this->db->join("current_booking_bus_selection cb", "cb.bus_service_no=tkt.bus_service_no", "inner");
       $this->db->join("ticket_details td", "tkt.ticket_id=td.ticket_id", "inner");
       $this->db->where("tkt.ticket_id",$saved_data);
       
       $this->db->where("tkt.status",'Y');
       $query = $this->db->get();
   
       $data = $query->result_array();
       return $data;
    }


    function get_booking_history_by_userid($user_id){
       $this->db->select("t.ticket_id,t.destination_stop_name,t.boarding_stop_name,t.user_email_id,t.provider_id,t.provider_type,t.partial_cancellation,t.op_id,t.discount_value,t.pnr_no,t.boss_ref_no,t.ticket_ref_no,t.dept_time,t.boarding_stop_cd,t.destination_stop_cd,t.num_passgr,t.op_name,t.boss_ref_no,t.status as ticket_status,td.psgr_no,td.psgr_name,td.psgr_age,td.psgr_sex,td.psgr_type,td.seat_no, td.fare_amt as seat_fare, td.status as ticket_detail_status,t.from_stop_name,t.till_stop_name, ctd.pg_refund_amount as refund_amt, ctd.cancel_charge,td.fare_amt,t.tot_fare_amt_with_tax,t.inserted_date,td.convey_name,td.convey_type,td.convey_value,ag.markup_value,ag.total_without_discount,t.total_basic_amount,t.agent_commission_amt,t.tot_fare_amt");
       $this->db->from("tickets t");
       $this->db->join("ticket_details td", "td.ticket_id = t.ticket_id  and td.status='Y'", "inner");
       $this->db->join("cancel_ticket_data ctd", "t.ticket_id = ctd.ticket_id","left");
       $this->db->join("agent_tickets ag", "t.ticket_id = ag.ticket_id","left");
       $this->db->where("t.inserted_by",$user_id);
       // $this->db->where("t.transaction_status","success");
       $this->db->where_in("t.transaction_status",array("success","cancel"));
       $this->db->group_by("td.id");
       $this->db->order_by("t.inserted_date","desc");
       

       $query = $this->db->get();
       $data = $query->result_array();
       return $data;
    }
    
     public function rpt_reservation_filter($data) {
            $res_data='';
            $st='Y';
            //show($data,1);
            $from_date  = date('Y-m-d',strtotime($data['from_date']));
            $to_date    = date('Y-m-d',strtotime($data['to_date']));
       $this->db->select("ticket_id,pnr_no,from_stop_name ,till_stop_name ,dept_time,num_passgr,tot_fare_amt,CASE transaction_status when 'failed' then 'Failed Transaction' when 'temp_booked' then 'Temporary Booked' when 'incomplete' then 'Incomplete Transaction' when 'success' then 'Success Transaction' when 'cancel' then 'Cancel Transaction' when 'pfailed' then 'Payment Failed'  else 'Payment Success' end as Tran_type");
        $this->db->from('tickets');
           
        if((isset($data['from_date']) && isset($data['to_date'])) && (($data['from_date']!="") && ($data['to_date']!="")))
            { 
            if($data['from_date']!=$data['to_date'])
            {
                $this->db->where("DATE_FORMAT(dept_time, '%Y-%m-%d') BETWEEN '".$from_date."' AND '".$to_date."' ");
            }  
            else
            {
                $this->db->where("DATE_FORMAT(dept_time, '%Y-%m-%d')=",$from_date);
            }    
          }
          else if($data['from_date']!='' && $data['to_date']=='')
            {
                $this->db->where("DATE_FORMAT(dept_time, '%Y-%m-%d')=",$from_date);
              
            }
         else if($data['from_date']=='' && $data['to_date']!='')
            {
                $this->db->where("DATE_FORMAT(dept_time, '%Y-%m-%d')=",$to_date);
            }
       
             if(isset($data['txt_frm_stop_cd']) && $data['txt_frm_stop_cd']!='' ) {
                $this->db->like("from_stop_name", trim($data['txt_frm_stop_cd']));
            }
             if(isset($data['txt_to_stop_cd']) && $data['txt_to_stop_cd']!='' ) {
                $this->db->like("till_stop_name", trim($data['txt_to_stop_cd']));
            }
             if(isset($data['tran_type']) && $data['tran_type']!='' ) {
                $this->db->where("transaction_status", trim($data['tran_type']));
                if($data['tran_type']=='cancel')
                   {
                   $st='N'; 
                   }
            }

         //   $this->db->where("inserted_by", $data['user_id']);
            $this->db->where("status",$st);
             $this->db->order_by("dept_time",'DESC');
            $query = $this->db->get(); 
          //  show($this->db->last_query(),1)
            $res_data = $query->result_array();
        if(isset($res_data) && count($res_data) <=0){
            $res_data='Data Not Found';
         //   show($res_data,1);
        }
    //  show($res_data,1);
        return $res_data;
    
    }


    /*
     * @Author      : Suraj Rathod
     * @function    : get_ticket_detail_by_refno()
     * @param       : $ticket_ref_no -> Ticket Reference Number
     * @detail      : return booked ticket detail by specified ticket_ref_no.
     *                 
     */

    function get_ticket_detail_by_refno($ticket_ref_no) {
        if ($ticket_ref_no != "") {
            //$this->db->select('td.psgr_name,GROUP_CONCAT(td.seat_no) as booked_seat_numbers,t.*');
          
            $this->db->select('td.psgr_name,GROUP_CONCAT(IF(td.status = "Y",td.seat_no,NULL)) as booked_seat_numbers,t.*,sum(td.fare_amt) as td_total_fare_amt,sum(ctd.actual_refund_paid) as total_refund_amt,sum(ctd.cancel_charge) as total_cancel_charges,GROUP_CONCAT(ctd.seat_no) as ctd_seat_no');
            $this->db->from('tickets t');
            $this->db->join('ticket_details td', 't.ticket_id = td.ticket_id', "inner");
            $this->db->join('cancel_ticket_data ctd', 't.ticket_id = ctd.ticket_id', "left");
            $this->db->where('t.ticket_ref_no', $ticket_ref_no);
             $this->db->where('td.status', "Y");
            //$this->db->group_by("t.ticket_no,t.status");
            $query = $this->db->get();
            $data = $query->result();
            return $data;
        }
    }


    /*
     * @Author      : Suraj Rathod
     * @function    : get_ticket_sms_email()
     * @param       : $pnr -> Ticket Reference Number, $email (optional)
     * @detail      : return Ticket Detail.
     *                 
     */

    function get_ticket_sms_email($bos_ref_no,$email = "") {
        if ($bos_ref_no != "") {
            $this->db->select('td.ticket,t.*');
            $this->db->from('tickets t');
            $this->db->join('ticket_dump td', 'td.bos_ref_no = t.boss_ref_no', "inner");
            $this->db->where('t.boss_ref_no', $bos_ref_no);
            if($email != "")
            {
              $this->db->where('t.user_email_id', $email);
            }
            $query = $this->db->get();
            $data = $query->result();
            return $data;
        }
    }



    /*
     * @Author      : Suraj Rathod
     * @function    : get_seat_nos()
     * @param       : $ticket_id -> Ticket Reference Number, $email (optional)
     * @detail      : return Ticket Detail.
     *                 
     */
    function get_seat_nos_by_tid($tid) {
        if ($tid != "") {
            $this->db->select('GROUP_CONCAT(t.seat_no) as seat_no',false);
            $this->db->from('ticket_details t');
            $this->db->where('t.ticket_id', $tid);
            $this->db->where('t.status', "Y");
            $this->db->group_by('t.berth_no');
            $this->db->order_by('t.berth_no');
            $query = $this->db->get();
            $data = $query->result();
            return $data;
        }
    }



    /*
     * @Author      : Suraj Rathod
     * @function    : get_history_ticket_sms_email()
     * @param       : $pnr -> Ticket Reference Number, $user_id
     * @detail      : return Ticket Detail.
     *                 
     */

    function get_history_ticket_sms_email($ticket_ref_no,$user_id = "") {
        if ($ticket_ref_no != "") {
            $this->db->select('td.ticket,t.*');
            $this->db->from('ticket_dump td');
            $this->db->join('tickets t', 't.ticket_ref_no = td.ticket_ref_no', "inner");
            $this->db->where('t.ticket_ref_no', $ticket_ref_no);
            
            if($user_id != "")
            {
              $this->db->where('t.inserted_by', $user_id);
            }

            $query = $this->db->get();
            $data = $query->result();
            return $data;
        }
    }

    /*
     * @Author      : Suraj Rathod
     * @function    : get_last_bos_ref
     * @param       : 
     * @detail      : return only last bos ticket digit.
     *                 
     */

    function get_last_bos_ref() {
      $this->db->select('max(t.bos_ticket_digit) as bos_ticket_digit');
      $this->db->from('tickets t');
      $query = $this->db->get();
      $data = $query->result();
      return $data;
    }
    
    function get_bus_services(){
      $this->db->distinct();
      $this->db->select('bus_service_no');
      $this->db->from('tickets t');
      $this->db->where("status", 'Y');
      $this->db->where("t.transaction_status",'success');
      $this->db->order_by("bus_service_no", 'asc');
      $query = $this->db->get();
      $data = $query->result_array();
      return $data;
    }
    
    function bus_services_data($data){
     
      $this->db->select('t.bus_service_no, sum(tot_fare_amt) tot_fare_amt, sum(num_passgr) num_psgr');
      $this->db->from('tickets t');
      
            if(isset($data['from_date']) && $data['from_date']!='' ) {
                $this->db->where("dept_time >=", date('Y-m-d',  strtotime($data['from_date'])));
            }
        
            if(isset($data['to_date']) && $data['to_date']!='' ) {
                $this->db->where("dept_time <=", date('Y-m-d',  strtotime($data['to_date'])));
            }
            if(isset($data['bus_service_no']) && $data['bus_service_no']!='all' ) {
                $this->db->where("bus_service_no ", $data['bus_service_no']);
                $this->db->group_by("t.bus_service_no");
            }
           
            $this->db->where("t.status", 'Y');
             $this->db->where("t.transaction_status",'success');
            $this->db->order_by("dept_time",'DESC');
           
           
     
            $query = $this->db->get(); 
            $data = $query->result_array();
            
            return $data;
    }
    
    function get_bus_serviceswise_all_data($data){
     
      $this->db->select('*');
      $this->db->from('tickets t');
      
            if(isset($data['from_date']) && $data['from_date']!='' ) {
                   $this->db->where("dept_time >=", date('Y-m-d',  strtotime($data['from_date'])));
            }
            if(isset($data['to_date']) && $data['to_date']!='' ) {
                $this->db->where("dept_time <=", date('Y-m-d',  strtotime($data['to_date'])));
            }
             
            if(isset($data['bus_service_no']) && $data['bus_service_no']!='all' ) {
                $this->db->where("bus_service_no", $data['bus_service_no']);
           }

            if(isset($data['bus_service_no']) && $data['bus_service_no']=='all' ) {
                $this->db->group_by("t.bus_service_no");
            }

            $this->db->where("status", 'Y');
            $this->db->where("t.transaction_status",'success');
            $this->db->order_by("dept_time",'DESC');
            
            $query = $this->db->get();
          //show($this->db->last_query(),1);
            $res_data = $query->result_array();
            return $res_data;
    }


    public function get_failed_transaction($user_id)
    {
      $this->db->select("*");
      $this->db->from("tickets");
      $this->db->where("transaction_status != 'success'");
      $this->db->where("inserted_by = ".$user_id);
      $failed_transaction = $this->db->get();
      $transaction_failed = $failed_transaction->result_array();
      return $transaction_failed;
    }



    /*
     * @Author      : Suraj Rathod
     * @function    : ticket_detail_by_refno()
     * @param       : $ticket_ref_no -> Ticket Reference Number
     * @detail      : return booked ticket detail by specified ticket_ref_no.
     *                 
     */

    public function ticket_detail_by_refno($ticket_ref_no) 
    {
      if ($ticket_ref_no != "") 
      {
        $this->db->select('td.psgr_name,t.*');
        $this->db->from('tickets t');
        $this->db->join('ticket_details td', 't.ticket_id = td.ticket_id', "left");
        $this->db->where('t.ticket_ref_no', $ticket_ref_no);
        $this->db->group_by("t.ticket_id");
        $query = $this->db->get();
        $data = $query->result();
        return $data;
      }
    }


    /*
     * @Author      : Suraj Rathod
     * @function    : ticket_detail_with_pg()
     * @param       : $ticket_id -> Ticket Id
     * @detail      : return ticket detail with payment gateway track id.
     *                 
     */

    public function ticket_detail_with_pg($ticket_id)
    {
      if ($ticket_id != "")
      {
        $this->db->select('t.*,ptf.pg_track_id,rptf.pg_track_id AS return_pg_track_id');
        $this->db->from('tickets t');
        $this->db->join('payment_transaction_fss ptf', 'ptf.ticket_id = t.ticket_id', "left");
        $this->db->join('payment_transaction_fss rptf', 'rptf.ticket_id = t.ticket_id', "left");
        $this->db->where('t.ticket_id', $ticket_id);
        $query = $this->db->get();
        $data = $query->result();
        return $data;
      }
    }


    /* @Author      : Suraj Rathod
     * @function    : get_ticket_and_detail
     * @param       : 
     * @detail      : 
     *                 
     */
    public function get_ticket_and_detail($email,$pnr)
    {
      if ($email != "" && $pnr != "")
      {
        $this->db->select("GROUP_CONCAT(td.seat_no) AS seat_no,td.psgr_name,t.*",false);
        $this->db->from("tickets t");
        $this->db->join('ticket_details td', 'td.ticket_id = t.ticket_id', 'inner');
        $this->db->where("t.pnr_no = '".$pnr."' AND t.user_email_id = '".$email."'");
        $ticket_result = $this->db->get();
        if($ticket_result->num_rows())
        {
          $ticket_result_array = $ticket_result->result_array();
          return $ticket_result_array[0];
        }
        else
        {
          return false;
        }
      } 
      else
      {
        return false;
      }
    }

    function get_ticket_trans($input, $type = NULL){
      $this->load->model(array('ticket_details_model'));
      $status = array('success', 'cancel');
      switch ($type) {
        case 'cancelled':
        case 'cancel':
          $this->where('transaction_status', 'cancel');
          break;

        case 'success':
          $this->where('transaction_status', 'success');
          break;        
        default:
              $this->where_in('transaction_status', $status);
              
      }

      if(!empty($input['mobile']))
      {
        $this->where('mobile_no', $input['mobile']);
      }
        

      $tickets = $this->where('user_email_id', $input['email'])
                      // ->where("DATE_FORMAT(issue_time,'%Y-%m-%d %H:%i:%s') >=", date('Y-m-d H:i:s',strtotime($input['from_date'])))
                      // ->where("DATE_FORMAT(issue_time,'%Y-%m-%d %H:%i:%s') <=", date('Y-m-d H:i:s',strtotime($input['till_date'])))
                      ->where("DATE_FORMAT(dept_time,'%Y-%m-%d') >=", date('Y-m-d',strtotime($input['from_date'])))
                      ->where("DATE_FORMAT(dept_time,'%Y-%m-%d') <=", date('Y-m-d',strtotime($input['till_date'])))
                      ->order_by('ticket_id','desc')
                      ->find_all();

      // last_query(1);

      if($tickets)
      {
        foreach ($tickets as $key => $value) {
          $tickets_detail = $this->ticket_details_model->where('ticket_id', $value->ticket_id)->find_all();
          
          if($tickets_detail)
          {
            $tickets[$key]->passenger_details = $tickets_detail;
          }
        }
      }

      return $tickets;
    }

    function get_cancellable_ticket_trans($input, $type = NULL){
      $this->load->model(array('ticket_details_model'));
      
      if(isset($input['mobile_no']) &&  !empty($input['mobile_no']) && $input['mobile_no']!='')
      {
        $this->where('mobile_no', $input['mobile_no']);
      }
        
      if(isset($input['email']) && !empty($input['email']) && $input['email']!='' )
      {
        $this->where('user_email_id', $input['email']);
      }
      if(isset($input['pnr_no']) && !empty($input['pnr_no']) && $input['pnr_no']!='')
      {
        $this->where('pnr_no', $input['pnr_no']);
      }
      if(isset($input['bos_ref_no']) && !empty($input['bos_ref_no']) && $input['bos_ref_no']!='')
      {
        $this->where('boss_ref_no', $input['bos_ref_no']);
      }
      if(isset($input['ticket_ref_no']) && !empty($input['ticket_ref_no']) && $input['ticket_ref_no']!='')
      {
         $this->where('ticket_ref_no', $input['ticket_ref_no']);
      }
     /* $tickets = $this->where('user_email_id', $input['email'])
                      ->where('transaction_status', 'success')
                      ->where("DATE_FORMAT(dept_time,'%Y-%m-%d %H:%i:%s') >=", date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))))
                      ->order_by('ticket_id','desc')
                      ->find_all();*/
      $tickets = $this->where('transaction_status', 'success')
                      ->where("DATE_FORMAT(dept_time,'%Y-%m-%d %H:%i:%s') >=", date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))))
                      ->order_by('ticket_id','desc')
                      ->find_all();


      if($tickets)
      {
        foreach ($tickets as $key => $value) {
          $tickets_detail = $this->ticket_details_model->where('ticket_id', $value->ticket_id)->find_all();
          
          if($tickets_detail)
          {
            $tickets[$key]->passenger_details = $tickets_detail;
          }
        }
      }

      return $tickets;
    }


    /* @Author      : Suraj Rathod
     * @function    : ticket_report
     * @param       : 
     * @detail      : 
     *                 
     */
    public function ticket_report($agent_user_id = "", $from_date = "", $to_date = "")
    {
        $this->db->select("t.user_email_id,t.booked_from,td.psgr_name,t.api_ticket_no,t.boss_ref_no,t.op_name,t.tot_fare_amt_with_tax,t.tot_fare_amt,t.transaction_status,ctd.refund_amt,ctd.cancel_charge,ptf.pg_track_id,t.agent_commission_amt AS agent_commission",false);
        $this->db->from("tickets t");
        $this->db->join('cancel_ticket_data ctd', 'ctd.ticket_id = t.ticket_id', 'left');
        $this->db->join('ticket_details td', 'td.ticket_id = t.ticket_id', 'inner');
        $this->db->join('users u', 'u.id = t.inserted_by', 'inner');
        $this->db->join('payment_transaction_fss ptf', 'ptf.ticket_id = t.ticket_id', 'left');
      
        if($agent_user_id != "")
        {
          $this->db->where_in("t.inserted_by",$agent_user_id);
        }

        if($from_date != '')
        {
          $this->db->where("DATE_FORMAT(t.inserted_date, '%Y-%m-%d') >=",date('Y-m-d', strtotime($from_date)));
        }

        if($to_date != '')
        {
          $this->db->where("DATE_FORMAT(t.inserted_date, '%Y-%m-%d') <=",date('Y-m-d', strtotime($to_date)));
        }
        
        $this->db->group_by('t.ticket_id');
        $ticket_result = $this->db->get();
     
        if($ticket_result->num_rows())
        {
          return $ticket_result->result();
        }
        else
        {
          return false;
        }
    }

    public function get_last_traveller($user_id = "", $limit = 6)
    {
      if($user_id != "")
      {
        $this->db->distinct();
        $this->db->select('td.psgr_name as "name",td.psgr_age as "age",td.psgr_sex as "gender"');
        $this->db->from('tickets t');
        $this->db->join('ticket_details td', 't.ticket_id = td.ticket_id', "inner");
        $this->db->where('t.inserted_by', $user_id);
        $this->db->order_by('t.ticket_id',"DESC");
        $this->db->group_by('td.psgr_name');
        $this->db->limit($limit, 0);
        $query = $this->db->get();
        $data = $query->result_array();
        return $data;
      }
    }
    
     /* @Author      : Sagar Bangar
     * @function    : getTicket
     * @param       : email, pnr, boss_ref_no
     * @detail      : Get Ticket
     *                 
     */
    public function getTicket($email, $boss_ref_no, $pnr_no){
      $this->db->select("t.user_email_id as email, t.ticket_id, t.boss_ref_no, t.pnr_no, t.issue_time as booking_date");
      $this->db->from("tickets t");
      $this->db->where("t.user_email_id",$email);
      if($pnr_no)
        $this->db->where("t.pnr_no",$pnr_no);
      if($boss_ref_no)
        $this->db->where("t.boss_ref_no" , $boss_ref_no);
      $this->db->where("t.status" , 'N');
      $this->db->where("t.transaction_status" , 'cancel');
      $query = $this->db->get();
      // /show($this->db->last_query(),1);
      return $query->row_array();
    }
    
     /* @Author      : Sagar Bangar
     * @function    : getTicketPaymentIssue
     * @param       : email, pnr, boss_ref_no
     * @detail      : Get Ticket
     *                 
     */
    public function getTicketPaymentIssue($email, $mobile_no, $booking_date){
        $date = new DateTime($booking_date); 
           $booking_date = $date->format('Y-m-d');
      $this->db->select("t.user_email_id as email, t.ticket_id, t.boss_ref_no, t.pnr_no, t.issue_time as booking_date");
      $this->db->from("tickets t");
      $this->db->where("t.user_email_id",$email);
        $this->db->where("t.mobile_no",$mobile_no);
        $this->db->like("t.issue_time" , $booking_date);
      
      $this->db->where("t.status" , 'N');
      $this->db->where("t.transaction_status" , 'cancel');
      $query = $this->db->get();
      return $query->row_array();
    }
    
     /* @Author      : Sagar Bangar
     * @function    : updateTicket
     * @param       : email, pnr, boss_ref_no
     * @detail      : Get Ticket
     *                 
     */
    public function updateTicket($ticket, $ticket_id){
      //  $update_data = array('email'=>$mail, 'boss_ref_no' =>$boss_ref_no, 'pnr_no' => $pnr_no);
        $this->db->where('ticket_id', $ticket_id);
        if($this->db->update('tickets', $ticket)){
            return true;
        }else{
            return false;
        }
    }

    public function paymentgateway_ref_no($date)
    {
        if(!empty($date))
        {
          $this->db->select('pg_tracking_id,ticket_id');
          $this->db->from('tickets');
          $this->db->where("DATE_FORMAT(inserted_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($date)));
          $this->db->where('pg_tracking_id != "null" ');
          $query = $this->db->get();
          $data = $query->result();
          if($data)
          {
             return $data;
          }
        }
        
        return false;
    }


    /*****get ticket function *******/
  function get_ticket($data,$page = 0 ,$per_page = NULL)
    { 
  
      $result = array();

      $result['data'] = array();
      $result['count'] = 0;
    
      if($data['user_id'] > 0 ||($data['email_id'] !='' && ($data['pnr_no'] !='' || $data['bos_ref_no'] !='' || $data['ticket_id'] !='')) ) 
      {
    
        $curr_date=date('Y-m-d H:i:s');

        $this->db->start_cache();

        //$this->db->select("distinct tm.op_name, tm.ticket_id,tm.ticket_ref_no,tm.ticket_no,tm.dept_time,tm.dept_time,td.psgr_name,tm.boss_ref_no,tm.from_stop_cd,tm.till_stop_cd,tm.pnr_no,td.psgr_age,td.psgr_sex,tm.num_passgr,tot_fare_amt",false);
        $this->db->select("distinct tm.op_name, tm.destination_stop_name,tm.role_id,tm.booked_by, tm.boarding_stop_name,tm.discount_value, tm.provider_id,tm.op_id,tm.provider_type, tm.user_email_id, tm.partial_cancellation, tm.inserted_date,tm.ticket_id,tm.ticket_ref_no,tm.dept_time,tm.dept_time,td.psgr_name,tm.boss_ref_no,tm.from_stop_cd,tm.till_stop_cd,tm.from_stop_name,tm.till_stop_name,tm.pnr_no,td.psgr_age,td.psgr_sex,tm.num_passgr,tm.tot_fare_amt,tm.tot_fare_amt_with_tax",false);

        $this->db->from($this->table."  tm");
        // $this->db->join("users u", "tm.inserted_by = u.user_id", "inner");
        $this->db->join("ticket_details td", "td.ticket_id = tm.ticket_id ", "inner");
        $this->db->join("cancel_ticket_data ct", "tm.ticket_id = ct.ticket_id", "left");

        
         if(isset($data['ticket_id']) && $data['ticket_id'] !='')
            $this->db->where("tm.ticket_id",$data['ticket_id']);
         
        if($data['email_id'] !='' && ($data['pnr_no'] !='' || $data['bos_ref_no'] !=''))
        {
          $this->db->where("tm.user_email_id", $data['email_id']);

          if($data['pnr_no'] !='')
            $this->db->where("tm.pnr_no",$data['pnr_no']);

          if($data['bos_ref_no'] !='')
            $this->db->where("tm.boss_ref_no",$data['bos_ref_no']);
           
        }

       // $this->db->where('tm.dept_time >=' , $curr_date);
        $this->db->where("tm.status",'Y');
        $this->db->where("tm.transaction_status",'success');
        /*$this->db->where("tm.ticket_type <>",'X');*/
        $this->db->order_by("tm.ticket_id","desc");
        $this->db->group_by("tm.ticket_id");
        $query = $this->db->get();
        $result['count'] = $query->num_rows();

        $this->db->stop_cache();

       if($per_page > 0)
        {
          $this->db->limit($per_page, $page);
        }

        $query = $this->db->get();

        $result['data'] = $query->result_array();
        $this->db->flush_cache();

        
        foreach ($result['data'] as $key => $value) {
          $this->ticket_details_model->ticket_id = $value['ticket_id'];          
          $result['data'][$key]['detail_arr'] =  $this->ticket_details_model->select();
        }
      }      

      $this->db->flush_cache();
    ////print_r($result);exit;
      return $result;
    }
    
    /* @Author      : Rajkumar Shukla
     * @function    : get_ticket_and_detail
     * @param       : 
     * @detail      : 
     *                 
     */
    public function get_unsuccess_booked_tkt_detail($ticket_id)
    {
      $curr_date = date('Y-m-d H:i:s');
      if(!empty($ticket_id))
      {
        $this->db->select("t.*,wt.w_id,wt.amt,wt.id,wt.topup_id,wt.ticket_id as trasn_ticket_id,wt.transaction_type_id",false);
        $this->db->from("tickets t");
        $this->db->join('wallet_trans wt', 'wt.ticket_id = t.ticket_id or wt.return_ticket_id = t.ticket_id' , 'left');
        $this->db->where("t.ticket_id ",$ticket_id);
        $ticket_result = $this->db->get();
        if($ticket_result->num_rows())
        {
          $ticket_result_array = $ticket_result->result_array();
          return $ticket_result_array[0];
        }
      }

      return false;
    } // end of get_unsuccess_booked_tkt_detail
    
    function ticket_report_success($from_date='',$to_date='')
    {
        $date   = date('Y-m-d');
        $date1  = $date.' 00:00:00';
        $date2  = $date.' 23:59:59';

        if($from_date != '' && $to_date !='')
        {
          $date1  = $from_date;
          $date2  = $to_date;
        }

        $where  = array("transaction_status"=>"success",'inserted_date >=' => $date1,'inserted_date <= ' => $date2);
        $this->db->select("count(*) as ticketsucess");
        $this->db->from("tickets");
        if ($where != "") {
            $this->db->where($where);
        }
        $ticketsucess          = $this->db->get();
        $ticketsucess_array    = $ticketsucess->result_array();
        //show($this->db->last_query(), 1);
        return $ticketsucess_array;
    }
    
    function ticket_total_amount_success()
    {
        $date   = date('Y-m-d');
        $date1  = $date.' 00:00:00';
        $date2  = $date.' 23:59:59';
        $where  = array("transaction_status"=>"success",'inserted_date >=' => $date1,'inserted_date <= ' => $date2);
        $this->db->select("sum(tot_fare_amt_with_tax) AS total_tickets_amt");
        $this->db->from("tickets");
        if ($where != "") {
            $this->db->where($where);
        }
        $ticketsucess          = $this->db->get();
        $ticketsucess_array    = $ticketsucess->result_array();
        //show($this->db->last_query(), 1);
        return $ticketsucess_array;
    }

    function get_total_ticket_msrtc_amt($from_date='',$to_date='') {

        if($from_date != "" && $to_date != "")
        {
          $from_date = $from_date;
          $to_date   = $to_date;
        }
        $ticketamt_array = array();
        $role_name = $this->session->userdata('role_name');
        $user_id = $this->session->userdata('user_id');
        $level = $this->session->userdata('level');

        $this->db->select("u.agent_code");
        $this->db->from("users u");
        if($level != RETAILOR_LEVEL && ($this->session->userdata('role_id') != SUPERADMIN_ROLE_ID && $this->session->userdata('role_id') != TRIMAX_ROLE_ID)) {
          $this->db->where(array('level_'.$level => $user_id));
        } elseif($level == RETAILOR_LEVEL) {
          $this->db->where(array('id' => $user_id));
        }
        $agent_result = $this->db->get();
        $agent_result_array = $agent_result->result_array();

        $agent_code_arr = array_filter(array_column($agent_result_array,'agent_code'));
        // if(count($agent_code_arr) > 0) {
        //   $this->msrtc_db->select("sum(consume_amount) AS total_tickets_amt_master");
        //   $this->msrtc_db->where_in('agent_code',$agent_code_arr);
        //   $this->msrtc_db->where('WAYBILL_STATUS', 'CLOSED');
        //   if($from_date != "" && $to_date != "")                //code added by Aparna
        //   {
        //     $this->msrtc_db->where('COLLECTION_TM >=',$from_date);
        //     $this->msrtc_db->where('COLLECTION_TM <=',$to_date);
        //   }
        //   $this->msrtc_db->where('WAYBILL_STATUS', 'CLOSED');
        //   $this->msrtc_db->from('cb_waybillprogramming');
        //   $ticketamt          = $this->msrtc_db->get();
        //   $ticketamt_array    = $ticketamt->result_array();
       // }

        return $ticketamt_array;
    }
    
    function commission_earned_today($from_date='',$to_date='')
    {
        $date   = date('Y-m-d');
        $date1  = $date.' 00:00:00';
        $date2  = $date.' 23:59:59';

        if($from_date != '' && $to_date !='')
        {
          $date1  = $from_date;
          $date2  = $to_date;
        }
        $where  = array("transaction_status"=>"success",'c.user_id'=> COMPANY_ID ,'inserted_date >=' => $date1,'inserted_date <= ' => $date2);
        $this->db->select("sum(c.final_commission) as final_commission");
        $this->db->from("tickets t");
        $this->db->join("commission_calculation c", "t.ticket_id = c.ticket_id", "left");
        if ($where != "") {
            $this->db->where($where);
        }
        $ticketsucess          = $this->db->get();
        $ticketsucess_array    = $ticketsucess->result_array();
        //show($this->db->last_query(), 1);
        return $ticketsucess_array;
    }
    
    function commission_earned_chained($from_date='',$to_date='')
    {
        $date   = date('Y-m-d');
        $date1  = $date.' 00:00:00';
        $date2  = $date.' 23:59:59';

        if($from_date != '' && $to_date !='')
        {
          $date1  = $from_date;
          $date2  = $to_date;
        }
        $where  = array("transaction_status"=>"success",'c.user_id !='=> COMPANY_ID ,'inserted_date >=' => $date1,'inserted_date <= ' => $date2);
        $this->db->select("sum(c.final_commission) as final_commission_chained");
        $this->db->from("tickets t");
        $this->db->join("commission_calculation c", "t.ticket_id = c.ticket_id", "left");
        if ($where != "") {
            $this->db->where($where);
        }
        $ticketsucess          = $this->db->get();
        $ticketsucess_array    = $ticketsucess->result_array();
        //show($this->db->last_query(), 1);
        return $ticketsucess_array;
    }
    
    function commission_earned_masters($res,$column,$user_id)
    {
        $date   = date('Y-m-d');
        $date1  = $date.' 00:00:00';
        $date2  = $date.' 23:59:59';
        $where  = array("transaction_status"=>"success",'c.user_id'=> $user_id ,'inserted_date >=' => $date1,'inserted_date <= ' => $date2,'c.booking_flag' => 'Y','c.cancel_flag' => 'N');
        $this->db->select("sum(c.final_commission) as final_commission_masters");
        $this->db->from("tickets t");
        $this->db->join("commission_calculation c", "t.ticket_id = c.ticket_id", "left");
        if ($where != "") {
            $this->db->where($where);
        }
        $this->db->where_in('t.inserted_by',$res);
        $ticketsucess          = $this->db->get();
        $ticketsucess_array    = $ticketsucess->result_array();
        //show($this->db->last_query(), 1);
        return $ticketsucess_array;
    }
    
    function total_transactn_amt($res)
    {
        $date   = date('Y-m-d');
        $date1  = $date.' 00:00:00';
        $date2  = $date.' 23:59:59';
        $where  = array("transaction_status"=>"success",'inserted_date >=' => $date1,'inserted_date <= ' => $date2);
        $this->db->select("sum(tot_fare_amt_with_tax) AS total_tickets_amt_master");
        $this->db->from("tickets t");
        if ($where != "") {
            $this->db->where($where);
        }
        $this->db->where_in('inserted_by',$res);
        
        $ticketsucess          = $this->db->get();
        $ticketsucess_array    = $ticketsucess->result_array();
        //show($this->db->last_query(), 1);
        return $ticketsucess_array;
    }
    
    function total_transactn_cnt($res,$column,$user_id)
    {
        $date   = date('Y-m-d');
        $date1  = $date.' 00:00:00';
        $date2  = $date.' 23:59:59';
        $where  = array("transaction_status"=>"success",'inserted_date >=' => $date1,'inserted_date <= ' => $date2);
        $this->db->select("count(*) AS total_tickets_cnt_master");
        $this->db->from("tickets t");
        if ($where != "") {
            $this->db->where($where);
        }
        $this->db->where_in('inserted_by',$res);
        
        $ticketsucess          = $this->db->get();
        $ticketsucess_array    = $ticketsucess->result_array();
        //show($this->db->last_query(), 1);
        return $ticketsucess_array;
    }
    
    function network_transactn_commission($res,$column,$user_id)
    {
        $date   = date('Y-m-d');
        $date1  = $date.' 00:00:00';
        $date2  = $date.' 23:59:59';
        $where  = array("transaction_status"=>"success",'c.user_id !='=> $user_id ,'inserted_date >=' => $date1,'inserted_date <= ' => $date2,'c.booking_flag' => 'Y','c.cancel_flag' => 'N');
        $this->db->select("sum(c.final_commission) as network_commission_masters");
        $this->db->from("tickets t");
        $this->db->join("commission_calculation c", "t.ticket_id = c.ticket_id", "left");
        if ($where != "") {
            $this->db->where($where);
        }
        $this->db->where_in('t.inserted_by',$res);
        $this->db->where_in('c.user_id',$res);
        $ticketsucess          = $this->db->get();
        $ticketsucess_array    = $ticketsucess->result_array();
        //show($this->db->last_query(), 1);
        return $ticketsucess_array;
    }
    
    function ticket_report_success_master($result,$from_date='',$to_date='')
    {
        $date   = date('Y-m-d');
        $date1  = $date.' 00:00:00';
        $date2  = $date.' 23:59:59';
        if($from_date != '' && $to_date !='')
        {
          $date1  = $from_date;
          $date2  = $to_date;
        }
        
        $where  = array("transaction_status"=>"success",'inserted_date >=' => $date1,'inserted_date <= ' => $date2);
        $this->db->select("*");
        $this->db->from("tickets");
        if ($where != "")
        {
            $this->db->where($where);
        }
        $this->db->where_in('inserted_by',$result);
        $ticketsucess          = $this->db->get();
        $ticketsucess_array    = $ticketsucess->result_array();
        //show($this->db->last_query(), 1);
        return $ticketsucess_array;
    }
	
	/*
	 * Author : Santosh Warang
	 * Function : getCancelTicketData()
	 * Detail : get cancel tickets data
	 */
	public function getCancelTicketData(){
	  $sQuery="SELECT ct.id,ct.ticket_id,ct.actual_refund_paid,t.inserted_by FROM cancel_ticket_data ct LEFT JOIN tickets t ON ct.ticket_id=t.ticket_id WHERE ct.is_refund='0'";
	  $query = $this->db->query($sQuery);
	  return $query->result();
	}

  /**
   * Author : Aparna Chalke
   * Function : get_total_passenger()
   * Detail : get total Passenger count - CR-314 - 2018-06-29
   * 
   */
     function get_total_passenger($from_date='',$to_date='') {


        if($from_date != "" && $to_date != "")
        {
          $from_date = $from_date;
          $to_date   = $to_date;
        }
        
        $ticketpass_array = array();
        $role_name = $this->session->userdata('role_name');
        $user_id = $this->session->userdata('user_id');
        $level = $this->session->userdata('level');

        $this->db->select("u.agent_code");
        $this->db->from("users u");
        if($level != RETAILOR_LEVEL && ($this->session->userdata('role_id') != SUPERADMIN_ROLE_ID && $this->session->userdata('role_id') != TRIMAX_ROLE_ID)) {
          $this->db->where(array('level_'.$level => $user_id));
        } elseif($level == RETAILOR_LEVEL) {
          $this->db->where(array('id' => $user_id));
        }
        $agent_result = $this->db->get();
        $agent_result_array = $agent_result->result_array();
        $agent_code_arr = array_filter(array_column($agent_result_array,'agent_code'));

        // if(count($agent_code_arr) > 0) {
        //   $this->msrtc_db->select("(sum(full_ticket)+sum(half_ticket)) AS total_passenger");
        //   $this->msrtc_db->where_in('agent_code',$agent_code_arr);
        //   if($from_date != "" && $to_date != "")                //code added by Aparna
        //   {
        //     $this->msrtc_db->where('collection_time >=',$from_date);
        //     $this->msrtc_db->where('collection_time <=',$to_date);
        //   }
        //   $this->msrtc_db->from('cb_ticket');
        //   $ticketpass          = $this->msrtc_db->get();
        //   $ticketpass_array    = $ticketpass->result_array(); 
        // }
        return $ticketpass_array;
    }

    /**
   *Author : Aparna Chalke
   * Function : get_total_ticket_number()
   * Detail : get total ticket count- CR-314 - 2018-06-29
   */
     function get_total_ticket_number($from_date='',$to_date='') {

        if($from_date != "" && $to_date != "")
        {
          $from_date = $from_date;
          $to_date   = $to_date;
        }
      
        $ticketamt_array = array();
        $role_name = $this->session->userdata('role_name');
        $user_id = $this->session->userdata('user_id');
        $level = $this->session->userdata('level');

        $this->db->select("u.agent_code");
        $this->db->from("users u");
        if($level != RETAILOR_LEVEL && ($this->session->userdata('role_id') != SUPERADMIN_ROLE_ID && $this->session->userdata('role_id') != TRIMAX_ROLE_ID)) {
          $this->db->where(array('level_'.$level => $user_id));
        } elseif($level == RETAILOR_LEVEL) {
          $this->db->where(array('id' => $user_id));
        }
        $agent_result = $this->db->get();
        $agent_result_array = $agent_result->result_array();
        $agent_code_arr = array_filter(array_column($agent_result_array,'agent_code'));

        // if(count($agent_code_arr) > 0) {
        //   $this->msrtc_db->select("count(distinct ticket_number) AS total_tkt_cnt");
        //   $this->msrtc_db->where_in('agent_code',$agent_code_arr);
        //   if($from_date != "" && $to_date != "") {               //code added by Aparna
        //     $this->msrtc_db->where('collection_time >=',$from_date);
        //     $this->msrtc_db->where('collection_time <=',$to_date);
        //   }
        //   $this->msrtc_db->from('cb_ticket');
        //   $this->msrtc_db->group_by('waybill_no');
        //   $this->msrtc_db->get();
        //   $sql_query = '('.$this->msrtc_db->last_query().') as r';

        //   $this->msrtc_db->select('sum(total_tkt_cnt) as total_tickets');
        //   $this->msrtc_db->from($sql_query);
        //   $ticketamt = $this->msrtc_db->get();
        //   $ticketamt_array    = $ticketamt->result_array();
        // }
        return $ticketamt_array;
    }
    
}
