<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Role
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Role_model extends MY_Model {

    var $table  = 'user_roles';
    var $fields = array("id","role_name","record_status","home_page","added_by","added_on","updated_on");
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    /*
     * @function    : get_all_roles
     * @param       : 
     * @detail      : To fetch all roles. get_role_by_id
     */

    function get_all_roles() {
        
            $this->db->select("*");
            $this->db->from("user_roles");
            $this->db->order_by('added_on desc, id desc'); 
            $role_result = $this->db->get();
            $role_result_array = $role_result->result_array();
            return $role_result_array;
    }

// get_all_roles End   
    
    
    /*
     * @function    : get_role_by_ids
     * @param       : $id -> role id
     * @detail      : To fetch data by  role_id
     */

    function get_role_by_id($id) {
        
            $this->db->select("*");
            $this->db->from("user_roles");
            $this->db->where('id = '.$id); 
            $role_result = $this->db->get();
            $role_result_array = $role_result->row_array();
            return $role_result_array;
    }

// get_role_by_id End   
    
    
    /*
     * @function    : get_roleid_by_name
     * @param       : $name -> name of role
     * @detail      : To fetch data by  role name
     */

    function get_roleid_by_name($name) {
        
            $this->db->select("*");
            $this->db->from("user_roles");
            $this->db->where("role_name = '".$name."'"); 
            $role_result = $this->db->get();
            $role_result_array = $role_result->row_array();
            return $role_result_array;
    }

// get_role_by_id End   



    /*
     * @function    : add_role
     * @param       : $roledata -> data to add.
     * @detail      : to add role in user_roles table.
     *                 
     */

    public function add_role($roledata) {
        if (!empty($roledata)) {
            if ($this->db->insert('user_roles', $roledata)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

// end of add role

    function update_role_by_id($roledata) {
       
        $this->db->where('id', $roledata["id"]);
        if ($this->db->update("user_roles", $roledata["roledata"])) {
            return true;
        } else {
            return false;
        }
    }

    // update_role_by_id End  
}

// End of Role_model class