<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payment_transaction_metadata_model extends MY_Model {

    var $table  = 'payment_transaction_metadata';
    var $fields = array("id","payment_transaction_id","order_id","tracking_id","bank_ref_no","order_status","failure_message","payment_mode","card_name","status_code","status_message","currency","amount","billing_ name","billing_ address","billing_ city","billing_ state","billing_ zip","billing_ country","billing_ tel","billing_ email","delivery_ name","delivery_ address","delivery_ city","delivery_ state","delivery_ zip","delivery_ country","delivery_ tel","merchant_param1","merchant_param2","merchant_param3","merchant_param4","merchant_param5","vault","offer_type","offer_code","discount_value","timestamp");
    var $key    = 'id';
    protected $set_modified = false; 

    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

}
