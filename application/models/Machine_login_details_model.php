<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Machine_login_details_model extends MY_Model
{
    /**
     * Instanciar of CI
     */
    var $table = 'machine_login_details';
    var $fields	= array("id","agent_id","login_start","login_end","created_at", "updated_at");
    var $key    = 'id';  
    
    public function __construct() {
        parent::__construct();
        $this->_init(); 
   
    }
        
}
