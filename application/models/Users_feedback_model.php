<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of users
 * 
 * @author 
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_feedback_model extends MY_Model {
    
    var $table  = 'users_feedback';
    var $fields = array("id","user_id","name","email");
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
     
}
