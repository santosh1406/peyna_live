<?php

//Created by harshada kulkarni on 06-10-2018
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ccas_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    // added by harshada kulkarni on 18-08-2018
    public function saveTransactionData($data = "") {
        $this->db->insert('ccas_transaction_history', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    // added by harshada kulkarni on 18-08-2018
    public function updateTransactionData($transaction_id = "", $json_response = "", $status = "") {
        $queryStr = "UPDATE ccas_transaction_history SET response = '" . $json_response . " AND status =' " . $status . " WHERE id = '" . $transaction_id . "'";
        $query = $this->db->query($queryStr);
    }

}
