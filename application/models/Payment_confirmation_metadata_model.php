<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Payment_confirmation_metadata_model extends MY_Model {
    
    var $table  = "payment_confirmation_metadata";
    var $fields = array("id","request_parameter","response_parameter","total_transaction_sent","total_success_count","total_failed_count","timestamp");
    var $key    = "id";
    
    public function __construct() {
      parent::__construct();
      $this->_init();
    }
    
   
}
?>