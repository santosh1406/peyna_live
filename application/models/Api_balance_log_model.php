<?php

class Api_balance_log_model extends MY_Model
{
        var $table = 'api_balance_log';
        var $fields = array("id","api_provider_id","api_provider_name","op_id","op_name","min_balance","api_balance","user_email","api_balance_date");
        var $key = 'id';

        public function __construct() 
             {
                    parent::__construct();
                    $this->_init();
             }
    
             
        
       /**
        * Api balances day wise midnight cron
        *
        * @author Rajkumar
        */
        function api_balance_cron_daywise_upsrtc($current_date) 
           {
            $this->db->select('op_name, id, op_id,api_balance');
            $this->db->from('api_balance_log');
            $this->db->where('op_name','upsrtc');
            $this->db->like('api_balance_date',$current_date);
            $this->db->order_by('api_balance_date','desc');
            $this->db->limit('1');
            $result = $this->db->get();
            return $result->result_array();
          }
          function api_balance_cron_daywise_msrtc($current_date) {
           
            $this->db->select('op_name, id, op_id,api_balance');
            $this->db->from('api_balance_log');
            $this->db->where('op_name','msrtc');
            $this->db->like('api_balance_date',$current_date);
            $this->db->order_by('api_balance_date','desc');
            $this->db->limit('1');
            $result = $this->db->get();
           return $result->result_array();
          }
          function api_balance_cron_daywise_hrtc($current_date) 
          {
            $this->db->select('op_name, op_id,api_balance');
            $this->db->from('api_balance_log');
            $this->db->where('op_name','hrtc');
            $this->db->like('api_balance_date',$current_date);
            $this->db->order_by('api_balance_date','desc');
            $this->db->limit('1');
            $result = $this->db->get();
           return $result->result_array();
          }
        function msrtc_balance() {
            $this->db->select('op_name,api_balance,api_balance_date');
            $this->db->from('api_balance_log');
            $this->db->order_by('id','desc');
            $this->db->where('op_name','msrtc');
            $this->db->limit('1');
            $result = $this->db->get();
            return $result->result_array();
          }
        function upsrtc_balance() {
            $this->db->select('op_name,api_balance,api_balance_date');
            $this->db->from('api_balance_log');
            $this->db->order_by('id','desc');
            $this->db->limit('1');
            $this->db->where('op_name','upsrtc');
            $result = $this->db->get();
            return $result->result_array();
        }
         function hrtc_balance() {
            $this->db->select('op_name,api_balance,api_balance_date');
            $this->db->from('api_balance_log');
            $this->db->order_by('id','desc');
            $this->db->limit('1');
            $this->db->where('op_name','hrtc');
            $result = $this->db->get();
            return $result->result_array();
        }
         function rsrtc_balance() {
            $this->db->select('op_name,api_balance,api_balance_date');
            $this->db->from('api_balance_log');
            $this->db->order_by('id','desc');
            $this->db->limit('1');
            $this->db->where('op_name','rsrtc');
            $result = $this->db->get();
            return $result->result_array();
        }
        function ets_balance() {
            $this->db->select('op_name,api_balance,api_balance_date');
            $this->db->from('api_balance_log');
            $this->db->order_by('id','desc');
            $this->db->limit('1');
            $this->db->where('op_name','Etravelsmart');
            $result = $this->db->get();
            return $result->result_array();
        }
         function its_balance() {
            $this->db->select('op_name,api_balance,api_balance_date');
            $this->db->from('api_balance_log');
            $this->db->order_by('id','desc');
            $this->db->limit('1');
            $this->db->where('op_name','its');
            $result = $this->db->get();
            return $result->result_array();
        }
        function min_msrtc()
        {
            $this->db->select('op_name,api_balance,min_balance,user_email');
            $this->db->from('api_balance_log');
            $this->db->order_by('id','desc');
            $this->db->limit('1');
            $this->db->where('op_name','msrtc');
            $this->db->where("min_balance!='' ");
            $result = $this->db->get();
            return $result->result_array();
        }
            function min_upsrtc()
        {
            $this->db->select('op_name,api_balance,min_balance');
            $this->db->from('api_balance_log');
            $this->db->order_by('id','desc');
            $this->db->limit('1');
            $this->db->where('op_name','upsrtc');
            $this->db->where("min_balance!='' ");
            $result = $this->db->get();
            return $result->result_array();
        }
            function min_rsrtc()
        {
            $this->db->select('op_name,api_balance,min_balance');
            $this->db->from('api_balance_log');
            $this->db->order_by('id','desc');
            $this->db->limit('1');
            $this->db->where('op_name','rsrtc');
            $this->db->where("min_balance!='' ");
            $result = $this->db->get();
            return $result->result_array();
        }
            function min_hrtc()
        {
            $this->db->select('op_name,api_balance,min_balance');
            $this->db->from('api_balance_log');
            $this->db->order_by('id','desc');
            $this->db->limit('1');
            $this->db->where('op_name','hrtc');
            $this->db->where("min_balance!='' ");
            $result = $this->db->get();
            return $result->result_array();
        }
            function min_ets()
        {
            $this->db->select('op_name,api_balance,min_balance');
            $this->db->from('api_balance_log');
            $this->db->order_by('id','desc');
            $this->db->limit('1');
            $this->db->where('op_name','Etravelsmart');
            $this->db->where("min_balance!='' ");
            $result = $this->db->get();
            return $result->result_array();
          }
         function api_balance_cron_daywise_rsrtc($current_date) 
           {
            $this->db->select('op_name, id, op_id,api_balance');
            $this->db->from('api_balance_log');
            $this->db->where('op_name','rsrtc');
            $this->db->like('api_balance_date',$current_date);
            $this->db->order_by('api_balance_date','desc');
            $this->db->limit('1');
            $result = $this->db->get();
            return $result->result_array();
          }
          function api_balance_cron_daywise_its($current_date) {
           
            $this->db->select('op_name, id, op_id,api_balance');
            $this->db->from('api_balance_log');
            $this->db->where('op_name','its');
            $this->db->like('api_balance_date',$current_date);
            $this->db->order_by('api_balance_date','desc');
            $this->db->limit('1');
            $result = $this->db->get();
           return $result->result_array();
          }
          function api_balance_cron_daywise_etravelsmart($current_date) 
          {
            $this->db->select('op_name, op_id,api_balance');
            $this->db->from('api_balance_log');
            $this->db->where('op_name','Etravelsmart');
            $this->db->like('api_balance_date',$current_date);
            $this->db->order_by('api_balance_date','desc');
            $this->db->limit('1');
            $result = $this->db->get();
           return $result->result_array();
          }

}

?>
