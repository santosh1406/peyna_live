<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Payment_gateway_wallet_topup_model extends MY_Model {

    var $table = 'payment_gateway_wallet_topup';
    var $fields = array("id", "payment_gateway_id", "amount", "payment_transaction_id", "pg_tracking_id", "topup_by", "bank_name", "bank_acc_no", "transaction_no", "transaction_date", "transaction_remark", "transaction_status", "payment_confirmation", "created_by", "created_date", "updated_by", "updated_date", "is_status", "is_approvel", "app_rej_by", "app_rej_date", "reject_reason");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
    
    function pg_topup_request($input)
    {
        if (!empty($input)){
            if($this->db->insert('payment_gateway_wallet_topup', $input))
            {
                return true;
            }
          
          }
        else{
            return false;
        }
    
        
    }
    
    function update_topup($data) {

        $this->db->where('id', $data["id"]);
        //$this->db->update("users", $userdata["user_array"]);
        // die($this->db->last_query());

        if ($this->db->update("payment_gateway_wallet_topup", $data["data_array"])) {
            return true;
        } else {
            return false;
        }
    }

}
