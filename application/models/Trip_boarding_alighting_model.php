<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Trip_boarding_alighting_model extends CI_Model
{
    /**
     * Instanciar o CI
     */
    var $table 	= 'trip_boarding_alighting';
    var $fields		= "id,tba_id,trip_no,trip_stop_name,tba_type,haut_duration,sch_arrival_tm,sch_dept_tm,travel_tm,tba_description,created_by,created_date,updated_by,updated_date,record_status";

    public function __construct()
    {
            parent::__construct();
            $CI 		= & get_instance();
            $this->fields	= explode(",", $this->fields);
            $this->key          = "id";
            $this->reset();
    }

    /*
     * this may create unexpected result if following values are set,
     * better use function above;
     */
    function reset()
    {
        foreach($this->fields as $key)
        {
                $this->$key = NULL;
        }
    }

    function select()
    {
        $pid = $this->key;
        
        $result = false;		

        foreach($this->fields as $field)
        {
            if($this->$field)
            {
                $this->$field = htmlentities($this->$field);
                $this->db->where($field, $this->$field);
            }
        }
        
        $this->db->order_by('trip_id','DESC');

        $query = $this->db->get($this->table);

        if($this->$pid)
        {
            $result = $query->row_object();
        }
        else
        {
            $result = $query->result();
        }
        $this->reset();

        // show($this->db->last_query(),1);

        return remove_html_entities($result);
    }

    /*updates only by ID**/
    function save()
      {
            $pid = $this->key;
            
            $result = false;

                    foreach($this->fields as $field)
                    {
                            if($this->$field !== NULL)
                            {
                                    $entry[$field] = htmlentities($this->$field);////htmlentities added mysql_real_escape_string not needed
                            }
                    }

                    if($this->$pid)
                    {
                            $this->$pid 	= htmlentities($this->$pid);////htmlentities added mysql_real_escape_string not needed
                            $this->db->where($this->key, $this->$pid);
                            $this->db->update($this->table, $entry);

                            $result 	= $this->$pid;
                    }
                    else
                    {
                            $this->db->insert($this->table, $entry);
                            $result 	= $this->db->insert_id();
                    }

        return $result;
    }
         
    
   
      
	
}
