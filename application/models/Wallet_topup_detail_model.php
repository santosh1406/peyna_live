<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Wallet_topup_detail_model extends MY_Model {

    var $table  = 'wallet_topup_detail';
    var $fields = array("topup_id","agent_name","user_id","user_mobile_no","user_email","topup_by","pg_tracking","topup_amount","bank_name","bank_acc_no","transaction_no","transaction_remark","topup_trans_date","topup_created_date","topup_approvel","topup_transaction_status","topup_status","topup_request_status","topup_response_date","topup_reject_reason","trans_merchant_amount","ptf_pg_track_id","ptf_track_id");
    var $key    = 'topup_id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
}
