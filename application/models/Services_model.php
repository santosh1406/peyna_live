<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Services_model extends MY_Model {

    var $table  = 'services';
    var $fields = array("id","name","code","status","deleted","created_by","created_at","updated_by","updated_at");
    
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    function get_all_services() {
        
            $this->db->select("*");
            $this->db->from("services");
            $this->db->order_by('id asc'); 
            $result = $this->db->get();
            $result_array = $result->result_array();
            return $result_array;
    }
    
  
}
