<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Refund_request_model extends MY_Model {
    
    var $table  = 'refund_request';
    var $fields = array("id","request_type","email","mobile","booking_date","boss_ref_no","pnr_no","ticket_id","comment","inserted_date","inserted_by","is_status","is_deleted","reason_id","is_exists");
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
    public function insert_request($insert_data) {
           if (!empty($insert_data)){
               if ($this->db->insert('refund_request', $insert_data)) {
                   return true; 
               }
               else {
                   return false;
               }
           }
           else{
               return false;
           }
           
       }
       
    function check_is_requested($email,$bos_ref_no= '',$pnr_no='') 
    {
        $this->db->select("ticket_id , boss_ref_no, pnr_no, user_email_id, issue_time");
        $this->db->from("tickets");
        //$this->db->where("user_email_id" ,$email);
        if ($bos_ref_no != "") {
            $this->db->where("boss_ref_no" ,$bos_ref_no);
        }
        if($pnr_no != "")
            {
            $this->db->where("pnr_no" , $pnr_no);
        }
        
        $query = $this->db->get();
       return  $query->row_array();
    }
    
     function check_payment_issue($email, $mobile_no='', $booking_date='') 
    {
        $date = new DateTime($booking_date); 
        $booking_date = $date->format('Y-m-d');
        $this->db->select("ticket_id , boss_ref_no, pnr_no, user_email_id, issue_time");
        $this->db->where("user_email_id" ,$email);
        if ($mobile_no != "") {
            $this->db->where("mobile_no" ,$mobile_no);
        }
        if($booking_date != "")
            {
            $this->db->like("issue_time " , $booking_date);
        }
        
        $this->db->from("tickets");
        $query = $this->db->get();
       return  $query->row_array();
    }
    
    function request_is_exist($email,$bos_ref_no,$pnr_no, $request_type, $booking_date)
    {
      $this->db->select("email, boss_ref_no, pnr_no, reason, booking_date");
      $this->db->from("refund_request");
      $this->db->join("refund_reason_master rr", "rr.reason_id = refund_request.reason_id ", "left");
        $this->db->where("email" ,$email);
        if ($bos_ref_no != "") {
            $this->db->where("boss_ref_no" ,$bos_ref_no);
        }
        if($pnr_no != "")
            {
            $this->db->where("pnr_no" , $pnr_no);
        }
        if($request_type != "")
            {
            $this->db->where("request_type" , $request_type);
        }
        if($booking_date != "")
            {$date = new DateTime($booking_date); 
           $booking_date = $date->format('Y-m-d');
            $this->db->where("booking_date" , $booking_date);
        }
        $query = $this->db->get();
       return  $query->row_array();
    }  
    
    function update_ticket_record($email,$bos_ref_no,$pnr_no,$ticket_id)
    {
        if (!empty($request_result)){
               if ($this->db->set('refund_request', $insert_data['insert_data'])) {
                   return true; 
               }
               else {
                   return false;
               }
           }
           else{
               return false;
           }
        
    }
        
      
       
    function get_search($data,$page = 0 ,$per_page = NULL)
    { 
      
      $result = array();

      $result['data'] = array();
      $result['count'] = 0;
      
        if($data['user_id'] > 0 || ($data['email_id'] !='' && ($data['pnr_no'] !='' || $data['bos_ref_no'] !='' || $data['ticket_id'] !='')) ) 
      {

        $curr_date=date('Y-m-d H:i:s');

       // $this->db->start_cache();

        //$this->db->select("distinct tm.op_name, tm.ticket_id,tm.ticket_ref_no,tm.ticket_no,tm.dept_time,tm.dept_time,td.psgr_name,tm.boss_ref_no,tm.from_stop_cd,tm.till_stop_cd,tm.pnr_no,td.psgr_age,td.psgr_sex,tm.num_passgr,tot_fare_amt",false);
        $this->db->select("distinct tm.op_name, tm.destination_stop_name, tm.boarding_stop_name,tm.discount_value, tm.provider_id,tm.op_id,tm.provider_type, tm.user_email_id, tm.partial_cancellation, tm.inserted_date,tm.ticket_id,tm.ticket_ref_no,tm.dept_time,tm.dept_time,td.psgr_name,tm.boss_ref_no,tm.from_stop_cd,tm.till_stop_cd,tm.from_stop_name,tm.till_stop_name,tm.pnr_no,td.psgr_age,td.psgr_sex,tm.num_passgr,tm.tot_fare_amt,tm.tot_fare_amt_with_tax",false);

        $this->db->from('tickets'."  tm");
        // $this->db->join("users u", "tm.inserted_by = u.user_id", "inner");
        $this->db->join("ticket_details td", "td.ticket_id = tm.ticket_id ", "inner");
        $this->db->join("cancel_ticket_data ct", "tm.ticket_id = ct.ticket_id", "left");

        if($data['user_id'] !='' && $data['user_id'] > 0 )
        {
          if(isset($data['user_id']))
            $this->db->where("tm.inserted_by",$data['user_id']);
        }
        
        if($data['email_id'] !='' && ($data['pnr_no'] !='' || $data['bos_ref_no'] !=''))
        {
          $this->db->where("tm.user_email_id", $data['email_id']);

          if($data['pnr_no'] !='')
            $this->db->where("tm.pnr_no",$data['pnr_no']);

          if($data['bos_ref_no'] !='')
            $this->db->where("tm.boss_ref_no",$data['bos_ref_no']);
          
        }

          if($data['ticket_id'])
            $this->db->where("tm.ticket_id",$data['ticket_id']);
           
      
        $this->db->where("tm.status",'N');

        $this->db->where("tm.transaction_status",'cancel');
        $this->db->where("tm.ticket_type <>",'X');
        $this->db->order_by("tm.ticket_id","desc");
        $this->db->group_by("tm.ticket_id");
        
       // $query = $this->db->get('tickets');
        //$result['count'] = $query->num_rows();

       // $this->db->stop_cache();

       if($per_page > 0)
        {
          $this->db->limit($per_page, $page);
        }

        $query = $this->db->get('tickets');

        $result['data'] = $query->result_array();
      //  $this->db->flush_cache();

        
        foreach ($result['data'] as $key => $value) {
          $this->ticket_details_model->ticket_id = $value['ticket_id'];          
          $result['data'][$key]['detail_arr'] =  $this->ticket_details_model->select();
        }
      }      

      $this->db->flush_cache();
      return $result;
    }
    
    
    function refund_amount($ticket_id){
        
        $this->db->select("is_refund, ticket_id , cancel_charge,actual_refund_paid,is_refund,created_at");
        $this->db->from("cancel_ticket_data");
        $this->db->where("ticket_id" ,$ticket_id);
        $this->db->where("is_refund" ,'1');
        $query = $this->db->get();
       return  $query->row_array();
   }

 function update_by_id($input,$userdata) {

        $this->db->where('id', $input);

        if ($this->db->update("refund_request", $userdata)) {
            return true;
        } else {
            return false;
        }
    } 
      function isExist($ticket_no)
  {
        $this->db->where('ticket_id', $ticket_no);
        $query = $this->db->get('refund_request');

    if ($query->num_rows() > 0) 
    {
        return true;
    } 
    else 
    {
        return false;
    }
 } 


 function get_ticketdetails_by_ticket_id($ticket_id)
 {
        $this->db->select("boarding_time,pnr_no,boarding_stop_name,destination_stop_name,boss_ref_no,boarding_time,discount_value,tot_fare_amt_with_tax");
        $this->db->from("tickets");
        $this->db->where("ticket_id = " . $ticket_id);
        $query = $this->db->get();
        $result_array = $query->row_array();
        return $result_array;
 }

function get_passengerdetails_by_ticket_id($ticket_id)
 {
        $this->db->select("psgr_name,psgr_age,psgr_sex,psgr_type,seat_no,fare_amt,");
        $this->db->from("ticket_details");
        $this->db->where("ticket_id = " . $ticket_id);
        $query = $this->db->get();
        $result_array = $query->result_array();
        return $result_array;
 }
 
    /* @Author      : Sagar Bangar
      * @function    : request_check
      * @param       : request_type, email, ticket_id
      * @detail      : check refund request
      *                 
      */
    public function check_refund_request($email, $ticket_id){
        if($ticket_id)
        $this->db->where('ticket_id', $ticket_id);
        
        $this->db->where('email', $email);
        $query = $this->db->get('refund_request');

        if ($query->num_rows() > 0) 
        {
            return $query->row_array();
        } 
        else 
        {
            return false;
        }
    }
    
     
     /* @Author      : Sagar Bangar
      * @function    : insert_refund_request
      * @param       : request_type, email, ticket_id
      * @detail      : check refund request
      *                 
      */
    public function insert_refund_request($insert_data){
        if (!empty($insert_data)){
            if ($this->db->insert($this->table, $insert_data)) {
                return $this->db->insert_id(); 
            }
            else {
                return false;
            }
        }
        else{
            return false;
        }
    }
    
    
 
   
     /* @Author      : Sagar Bangar
      * @function    : check_refund_payment_request
      * @param       : request_type, email, ticket_id
      * @detail      : check refund request
      *                 
      */
    public function check_refund_payment_request($email, $mobile_no, $booking_date){
        
        $date = new DateTime($booking_date); 
        $booking_date = $date->format('Y-m-d');

        if($booking_date)
            $this->db->like('booking_date', $booking_date);
        
        if($mobile_no)
            $this->db->where('mobile', $mobile_no);
          
        
        $this->db->where('email', $email);
        $query = $this->db->get('refund_request');

        if ($query->num_rows() > 0) 
        {
            return $query->row_array();
        } 
        else 
        {
            return false;
        }
    }
    
     /* @Author      : Sagar Bangar
      * @function    : get_payement_ticket
      * @param       : request_type, email, ticket_id
      * @detail      : get payment ticket
      *                 
      */
    public function get_payement_ticket($data)
    { 
      
      $result = array();

      $result['data'] = array();
      $result['count'] = 0;
      
    if($data['ticket_id']) 
      {

        $curr_date=date('Y-m-d H:i:s');

        $this->db->select("distinct tm.op_name, tm.destination_stop_name, tm.boarding_stop_name,tm.discount_value, tm.provider_id,tm.op_id,tm.provider_type, tm.user_email_id, tm.partial_cancellation, tm.inserted_date,tm.ticket_id,tm.ticket_ref_no,tm.dept_time,tm.dept_time,td.psgr_name,tm.boss_ref_no,tm.from_stop_cd,tm.till_stop_cd,tm.from_stop_name,tm.till_stop_name,tm.pnr_no,td.psgr_age,td.psgr_sex,tm.num_passgr,tm.tot_fare_amt,tm.tot_fare_amt_with_tax",false);

        $this->db->from('tickets'."  tm");
        // $this->db->join("users u", "tm.inserted_by = u.user_id", "inner");
        $this->db->join("ticket_details td", "td.ticket_id = tm.ticket_id ", "inner");
        $this->db->join("cancel_ticket_data ct", "tm.ticket_id = ct.ticket_id", "left");

       
          if($data['ticket_id'])
            $this->db->where("tm.ticket_id",$data['ticket_id']);

        $this->db->where("tm.transaction_status <>",'cancel');
        $this->db->where("tm.transaction_status <>",'incomplete');
        $this->db->where("tm.transaction_status <>",'success');
        $this->db->order_by("tm.ticket_id","desc");
        $this->db->group_by("tm.ticket_id");
        
       // $query = $this->db->get('tickets');
        //$result['count'] = $query->num_rows();

       // $this->db->stop_cache();

       if($per_page > 0)
        {
          $this->db->limit($per_page, $page);
        }

        $query = $this->db->get('tickets');

        $result['data'] = $query->result_array();
      //  $this->db->flush_cache();

        
        foreach ($result['data'] as $key => $value) {
          $this->ticket_details_model->ticket_id = $value['ticket_id'];          
          $result['data'][$key]['detail_arr'] =  $this->ticket_details_model->select();
        }
      }      

      return $result;
    }
    
}

