<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_email_model extends MY_Model {
    
    var $table  = 'user_email';
    var $fields = array("id","user_id","user_mail","mail_activate_key","verified","verify_date","reason","sent_date");
    var $key    = 'id';
	
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
	
	public function get_last_sent($user_id, $user_mail) {        
		$this->db->select('*');
        $this->db->from('user_email');
        $this->db->where('user_id', $user_id);
		$this->db->where('user_mail', $user_mail);
		$this->db->order_by('id', 'desc');
		$this->db->limit(1);
		$query = $this->db->get();
        return $query->row_array();
    }

	public function save_activation_key($data) {        
		$flag = false;
        if (!empty($data))
		{
			if ($this->db->insert('user_email', $data)) {				
				$flag= $this->db->insert_id();
			} 
		}
		return $flag;
    }
	
	public function get_userby_key_n_id($verify_key, $hashid) {        
		$this->db->select('*');
        $this->db->from('user_email');        
		$this->db->where('sha1(id)',$hashid);
		$this->db->where('mail_activate_key', $verify_key);
		$this->db->limit(1);
		$query = $this->db->get();
        return $query->row_array();
    }
	
	function update_verify_status($id)
	{
		$data=array("verified"=>"Y", "verify_date"=> date('Y-m-d H:i:s'));
		$this->db->where('id',$id);          
		$this->db->update('user_email',$data);
	}
}