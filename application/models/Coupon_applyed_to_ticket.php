<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Coupon_applyed_to_ticket extends MY_Model {

	var $table  = 'coupon_applyed_to_ticket';
    var $fields = array("id","coupon_id","user_id","ticket_id","coupon_amount","discount_on","discount_above","total_fare_with_discount","total_fare_without_discount","grand_total_with_discount","grand_fare_without_discount","user_email_id","user_mobile_no","status","added_by","added_on");
    var $key    = 'id';
	
	public function __construct() {
        parent::__construct();
        $this->_init();      
    }
}