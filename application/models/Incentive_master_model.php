<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Incentive_master_model extends MY_Model
{
    var $table    = 'incentive_master';
    var $fields	  = array("id", "incentive_name", "valid_from", "valid_to", "incentive_type_id", "incentive_type_applicable_id", "incentive_type","incentive_value","inserted_date","is_status");
    var $key      = 'id';
    
    public function __construct() {
        parent::__construct();
        $this->_init();      
        
    }
    function  get_incentives()
    {
        $this->db->select("im.id,im.incentive_name,im.valid_from,im.valid_to,im.incentive_type_id,im.incentive_type_applicable_id,im.incentive_type,im.incentive_value,im.is_status",false);
        $this->db->from('incentive_master im');
        $this->db->where('im.is_status','Y');
        $incentives_result = $this->db->get();
        $incentives = $incentives_result->result_array();
        return $incentives;        
    }

    function  get_slab_incentives()
    {
        $this->db->select('im.id,im.valid_from,im.valid_to,isd.incentive_id,isd.from_slab,isd.to_slab,isd.incentive_type_id,isd.incentive_value,isd.is_status');
        $this->db->from('incentive_master im');
        $this->db->join("incentive_slab_details isd","im.id = isd.incentive_id","left");
        $this->db->where('im.is_status','Y');
        $this->db->where('incentive_id !=','');
        $incentives_result = $this->db->get();
        $incentives = $incentives_result->result_array();
      return $incentives;        
    }

    public function referr_wise_agents($per_page, $page)
        {
        $page	= ($page>0?$page:1);		
        $offset	= $per_page*($page-1);
        
        $this->db->select("u.id,u.display_name,rm.referrer_name,rm.referrer_code,rm.id as referrer_id,COUNT(t.ticket_id) AS total_tickets,coalesce(sum(tot_fare_amt_with_tax),0) AS transaction_amount,GROUP_CONCAT(t.ticket_id) AS tickets,GROUP_CONCAT(t.tot_fare_amt_with_tax) AS fares",FALSE);
        $this->db->from('users u');
        $this->db->join('tickets t','t.inserted_by = u.id and t.role_id = "16" and t.transaction_status = "success" ','left'  );
        $this->db->join('referrer_master rm','rm.referrer_code = u.referrer_code','left');
        $this->db->where('u.role_id =','16');
        $this->db->where('t.ticket_id !=','');
        $this->db->where('u.referrer_code !=',"");
        $this->db->where('rm.is_status','Y');
        $this->db->group_by('t.inserted_by');
        $this->db->order_by('total_tickets','desc');
//        if(!empty($select_referrer))
//        {
//          $this->db->where('rm.id', $select_referrer);
//        }
//        if(!empty($till_date))
//        {
//          $this->db->where('DATE(t.inserted_date) <=', date('Y-m-d',strtotime($till_date)));
//        }
        if(!empty($this->session->userdata('select_referrer'))){
          $this->db->where('rm.id', $this->session->userdata('select_referrer'));
        }
	if(!empty($this->session->userdata('till_date'))){
          $this->db->where('DATE(t.inserted_date) <=', date('Y-m-d',strtotime($this->session->userdata('till_date'))));
        }
        if($per_page > 0)
        {
          $this->db->limit($per_page, $offset);
        }
//        show($this->session->userdata('select_referrer'));
        
	$query_result = $this->db->get();
        $dis_referrer = $query_result->result_array();
//        last_query(1);
        return $dis_referrer;
    }
    
    public function view($id){
        $this->db->select('im.id,im.incentive_name,im.valid_from,im.valid_to,itl.type_name,ital.applicable_on_name,im.incentive_type,im.incentive_value,isd.from_slab,isd.to_slab,isd.incentive_value');
        $this->db->from('incentive_master im');
        $this->db->join('incentive_slab_details isd','im.id = isd.incentive_id','left');
        $this->db->join('incentive_type_lookup itl','itl.id = im.incentive_type_id','left');
        $this->db->join('incentive_type_applicable_lookup ital', 'ital.id = im.incentive_type_applicable_id','left');
        $this->db->where('im.id',$id);
		$this->db->where('isd.is_status','Y');
        $query_reult = $this->db->get();
        $query_reult_array = $query_reult->result_array();
        return $query_reult_array;
        
    }


    public function get_master_details()
    {
        $this->db->select('im.*,itl.type_name AS incentive_type_lookup,ital.applicable_on_name,isd.id as slab_id,isd.from_slab,isd.to_slab,isd.incentive_value as slab_incentive_value,im.incentive_value');
        $this->db->from('incentive_master im');
        $this->db->join('incentive_type_lookup itl','itl.id = im.incentive_type_id','left');
        $this->db->join('incentive_type_applicable_lookup ital', 'ital.incentive_type_id = itl.id','left');
        $this->db->join('incentive_slab_details isd','isd.incentive_id = im.id','left');
        $this->db->where('im.is_status','Y');
        $query_reult = $this->db->get();
        $query_reult_array = $query_reult->result_array();
        return $query_reult_array;
    }
}
