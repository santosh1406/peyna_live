<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Utilities_bbps_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getbbpsMobileOperators($char = "", $type = "") {
        
        $queryStr = "select *"
                . " FROM bbps_operator_urls"
                . " WHERE operator LIKE '%$char%'"
                . " AND status = 'Y'"
                . " AND type = '$type'"
                . " AND api_operator = 1";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        $nCount = count($result);

        if ($nCount > 0) {
            for ($i = 0; $i < $nCount; $i++) {
                $aResult[$i]['label'] = $result[$i]['operator'];
                $aResult[$i]['name'] = $result[$i]['operator'];
                $aResult[$i]['value'] = $result[$i]['operator_id'];
                $aResult[$i]['id'] = $result[$i]['operator_id'];
            }
        } else {
            $aResult[0]['label'] = 'No result found';
            $aResult[0]['name'] = 'No result found';
            $aResult[0]['value'] = '';
            $aResult[0]['id'] = '';
        }
        //$aResult_json = json_encode($aResult);
        return $aResult;
    }

    public function operatorDetails($operator = "", $type = "") {

        $queryStr = "select *"
                . " FROM bbps_operator_urls"
                . " WHERE operator_id = '" . $operator . "'"
                . " AND status = 'Y'"
                . " AND type = '" . $type . "'"
                . " AND api_operator = 1";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        return $result;
    }

    public function getUserWalletDetails($user_id = "") {
        $queryStr = "select amt"
                . " FROM wallet"
                . " WHERE user_id = '" . $user_id . "'"
                . " AND status = 'Y'"
                . " AND is_deleted = 'N'";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        return $result;
    }

    public function operatorDetailsByID($operatorId, $type) {

        $queryStr = "select *"
                . " FROM bbps_operator_urls"
                . " WHERE operator_id = '" . $operatorId . "'"
                . " AND status = 'Y'"
                . " AND type = '" . $type . "'"
                . " AND api_operator = 1";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        return $result;
    }
    
     public function getSubservices($range, $servicename = "", $servicecode = "") {

        $queryStr = "SELECT * from sub_services  
            WHERE service_code = '" . $servicecode . "' AND service_name = '" .$servicename .' '.$range ."'";
        
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        //echo $this->db->last_query();
        return $result;
    }
    
    public function getServicesCommission($range, $service_id , $subservice_id, $type,$created_by){
        
       
        $queryStr = "SELECT * from services_commissions 
            WHERE `values` NOT LIKE '{\"Trimax\":\"0\",\"Rokad\":\"0\",\"MD\":\"0\",\"AD\":\"0\",\"Distributor\":\"0\",\"Retailer\":\"0\"}'
            AND commission_name LIKE '%". $type.' '. str_replace(" ", "", $range) . "%'  
            AND service_id = '" . $service_id . "'
            AND sub_service_id = '" . $subservice_id . "'
            AND created_by = '" . $created_by . "'";
       
        $query = $this->db->query($queryStr);
        $result = $query->result();
//        echo $this->db->last_query();
//        die;
        return $result;
    }

    public function updateUserWallet($user_id = "", $updated_wallet_amt = "") {
        $queryStr = "UPDATE wallet SET amt = '" . $updated_wallet_amt . "' WHERE user_id = '" . $user_id . "'";
        $query = $this->db->query($queryStr);

        $queryStr1 = 'SELECT id from wallet where user_id = "' . $user_id . '"';
        $insert_id = $this->db->query($queryStr1);
        $insert_id = $insert_id->result_array();
        return $insert_id;
    }

    public function saveTransactionData($data = "") {
        $this->db->insert('bbps_recharge_transaction', $data);
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'JRI_recharge.log', $data, 'BBPS Recharge - Save Transaction Data');
    }

    public function saveBBPSData($data = "") {
        $this->db->insert('bbps_transaction_details', $data);
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'BBPS_recharge.log', $data, 'BBPS Transaction - Save BBPS');
    }

    public function bbps_getRetailerServicesCommission($user_id, $service_id, $sub_service_id, $created_by) {
        
        $this->db->select('u.id as user_id,u.agent_code,u.level_1,u.level_2,u.level_3,u.level_4,u.level_5,rsm.service_id as service_id,sc.id as commission_id,sc.values,sc.total_commission,sc.created_by,sc.customer_charge');
        $this->db->from("users u");
        $this->db->join('retailer_service_mapping rsm', 'u.id= rsm.agent_id', 'left');
        $this->db->join('services_commissions sc', 'rsm.service_id = sc.service_id', 'left');
        $this->db->where('rsm.agent_id =', $user_id);
        $this->db->where('rsm.service_id =', $service_id);
        $this->db->where('sc.sub_service_id =', $sub_service_id);
        $this->db->where('sc.created_by =', $created_by);
        $this->db->where('sc.status', 'Y');
        $this->db->where('sc.from >=', date('Y-m-d H:i:s'));
        $this->db->where('sc.till <=', date('Y-m-d H:i:s'));
        $user_result = $this->db->get();
         
        $user_result_array = $user_result->result_array();
        if (empty($user_result_array)) {
            $this->db->select('u.id as user_id,u.agent_code,u.level_1,u.level_2,u.level_3,u.level_4,u.level_5,rsm.service_id as service_id,sc.id as commission_id,sc.values,sc.total_commission,sc.created_by,sc.customer_charge');
            $this->db->from("users u");
            $this->db->join('retailer_service_mapping rsm', 'u.id= rsm.agent_id', 'left');
            $this->db->join('services_commissions sc', 'rsm.service_id = sc.service_id', 'left');
            $this->db->where('rsm.agent_id =', $user_id);
            $this->db->where('rsm.service_id =', $service_id);
            $this->db->where('sc.sub_service_id =', $sub_service_id);
            $this->db->where('sc.created_by =', $created_by);
            $this->db->where('sc.status', 'Y');
            $user_result = $this->db->get();
            $user_result_array = $user_result->result_array();
        }
        return $user_result_array;
    }

    public function saveVasCommissionData($data = "") {
        $this->db->insert('vas_commission_detail', $data);
    }
    
     public function operatorBillerTags($operatorId, $type) {

        $queryStr = "select tag_name"
                . " FROM bbps_operator_tags"
                . " WHERE operator_id = '" . $operatorId . "'"
                . " AND type = '" . $type . "'";
        $query = $this->db->query($queryStr);  
        $result = $query->result_array();
                
        return $result;
    }
    
    
    public function get_validation($type, $operatorId) {
        switch ($type) {
            case 'Broadband Postpaid':
                if ($operatorId == "ACT000000NAT01") { //ACT Fibernet
                    $config = array(
                        array('field' => 'operator', 'label' => 'Broadband Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select operator.')),
                        array('field' => 'number', 'label' => 'Account Number', 'rules' => 'trim|required|min_length[1]|max_length[50]|xss_clean', 'errors' => array('required' => 'Please check account number or name.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "ATBROAD00NAT01") {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Broadband Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select operator.')),
                        array('field' => 'number', 'label' => 'Landline Number with STD Code', 'rules' => 'trim|required|numeric|exact_length[11]|xss_clean', 'errors' => array('required' => 'Please check the landline number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "CONBB0000PUN01") {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Broadband Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select operator.')),
                        array('field' => 'number', 'label' => 'Directory Number', 'rules' => 'trim|required|min_length[4]|max_length[11]|xss_clean', 'errors' => array('required' => 'Please check the directory number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "HATHWAY00NAT01") {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Broadband Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select operator.')),
                        array('field' => 'number', 'label' => 'Customer ID', 'rules' => 'trim|required|numeric|min_length[9]|max_length[15]|xss_clean', 'errors' => array('required' => 'Please check the customer id.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "NEXTRA000NAT01") {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Broadband Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select operator.')),
                        array('field' => 'number', 'label' => 'Consumer ID', 'rules' => 'trim|required|numeric|exact_length[7]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "SPENET000NAT01") {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Broadband Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select operator.')),
                        array('field' => 'number', 'label' => 'CAN/Account ID', 'rules' => 'trim|required|min_length[6]|max_length[30]|xss_clean', 'errors' => array('required' => 'Please check the account number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "TTN000000NAT01") {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Broadband Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select operator.')),
                        array('field' => 'number', 'label' => 'User Name', 'rules' => 'trim|required|min_length[3]|max_length[64]|xss_clean', 'errors' => array('required' => 'Please check username.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "TIKO00000NAT01") {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Broadband Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select operator.')),
                        array('field' => 'number', 'label' => 'Service Id', 'rules' => 'trim|required|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check service number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Broadband Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select operator.')),
                        array('field' => 'number', 'label' => 'Account number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please check service number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                }
                break;
            case 'Electricity':
                if ($operatorId == "EPDCLOB00ANP01" || $operatorId == "SPDCLOB00ANP01") { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Service Number', 'rules' => 'trim|required|numeric|min_length[8]|max_length[20]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                } else if ($operatorId == "RELI00000MUM03" || $operatorId ==  "RELI00000MUM01") { //Assam Power Distribution Company Ltd (APDCL)-NON RAPDR
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[9]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                } else if ($operatorId == "AVVNL0000RAJ01") { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'K Number', 'rules' => 'trim|required|numeric|exact_length[20]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                } else if ($operatorId == "APDCL0000ASM02") { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer ID', 'rules' => 'trim|required|numeric|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if ($operatorId == "APDCL0000ASM01") { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer ID', 'rules' => 'trim|required|numeric|exact_length[11]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if ($operatorId == "BEST00000MUM01") { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer ID', 'rules' => 'trim|required|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if ($operatorId == "BSESRAJPLDEL01" || $operatorId =="BSESYAMPLDEL01") { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'CA Number', 'rules' => 'trim|required|numeric|exact_length[9]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if ($operatorId == "BESCOM000KAR01" || $operatorId =="KESCO0000UTP01") { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer ID/Account Number', 'rules' => 'trim|required|numeric|min_length[1]|max_length[10]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if ($operatorId =="JDVVNL000RAJ01" || $operatorId =="BKESL0000RAJ02" || $operatorId == "JVVNL0000RAJ01" || $operatorId == "KEDLOB000RAJ02" || $operatorId == "KEDLOB000KTA01" 
                            || $operatorId == 'TPADL0000AJM02' || $operatorId == 'TPADL0000AJM01') { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'K Number', 'rules' => 'trim|required|numeric|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if ($operatorId == "BESLOB000RAJ02" || $operatorId =="BESLOB000BRT01" || $operatorId =="BKESL0000BKR01") { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'K Number', 'rules' => 'trim|required|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if ($operatorId == "CESC00000KOL01") { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Customer ID (Not Consumer No)', 'rules' => 'trim|required|numeric|exact_length[11]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if ($operatorId == "CESCOM000KAR01" || $operatorId == "GESCOM000KAR01" || $operatorId == 'NPCL00000NOI01') { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Account ID (RAPDRP) OR Consumer Number / Connection ID (Non-RAPDRP)', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if ($operatorId == "CSPDCL000CHH01" || $operatorId == "JUSC00000JAM01") { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Business Partner Number', 'rules' => 'trim|required|numeric|min_length[6]|max__length[10]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                } else if ($operatorId == "DNHPDCL0DNH001") { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Service Connection Number', 'rules' => 'trim|required|min_length[1]|max_length[20]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if ($operatorId == "DGVCL0000GUJ01" || $operatorId == 'PGVCL0000GUJ01' || $operatorId == 'UGVCL0000GUJ01') { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[5]|max_length[11]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if ($operatorId == "NESCO0000ODI01" || $operatorId == 'SOUTHCO00ODI01' || $operatorId == 'UPPCL0000UTP02' || $operatorId == 'WESCO0000ODI01') { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if ($operatorId == "DHBVN0000HAR01" || $operatorId == 'PSPCL0000PUN01') { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Account Number', 'rules' => 'trim|required|min_length[9]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if ($operatorId == "DDED00000DAD01") { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Account Number', 'rules' => 'trim|required|numeric|min_length[1]|max_length[6]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if ($operatorId == "TNEB00000TND01") { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Account Number', 'rules' => 'trim|required|numeric|min_length[9]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if ($operatorId == "HPSEB0000HIP01") { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'K Number', 'rules' => 'trim|required|numeric|min_length[10]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if ($operatorId == "HESCOM000KAR01") { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Account ID (RAPDRP) OR Consumer Number / Connection ID (Non-RAPDRP)', 'rules' => 'trim|required|numeric|min_length[5]|max_length[10]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if ($operatorId == "JBVNL0000JHA01" || $operatorId == "MGVCL0000GUJ01") { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|min_length[3]|max_length[15]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'account_number', 'label' => 'Sub division Code', 'rules' => 'trim|required|min_length[1]|max_length[3]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if ($operatorId == "MPCZ00000MAP02" || $operatorId =="MPCZ00000MAP01" || $operatorId == "MPEZ00000MAP02" || $operatorId == "MPEZ00000MAP01"
                            || $operatorId =='NDMC00000DEL02') { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number/IVRS', 'rules' => 'trim|required|numeric|min_length[7]|max_length[20]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if($operatorId =='MAHA00000MAH01' || $operatorId == 'TATAPWR00MUM01'){
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer No', 'rules' => 'trim|required|numeric|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'account_number', 'label' => 'BU', 'rules' => 'trim|required|numeric|exact_length[4]|xss_clean', 'errors' => array('required' => 'Please check BU number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if($operatorId == 'MPDC00000MEG01' || $operatorId == 'TSEC00000TRI01'){
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer ID', 'rules' => 'trim|required|numeric|min_length[1]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check the number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if($operatorId == 'NBPDCL000BHI01'){
                     $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'CA Number', 'rules' => 'trim|required|numeric|min_length[9]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check CA number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if($operatorId == 'SNDL00000NAG01'){
                     $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer No', 'rules' => 'trim|required|numeric|min_length[10]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if($operatorId == 'SKPR00000SIK01'){
                     $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Contract Acc Number', 'rules' => 'trim|required|numeric|min_length[1]|max_length[9]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if($operatorId == 'SBPDCL000BHI01'){
                     $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'CA Number', 'rules' => 'trim|required|numeric|min_length[8]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check CA number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if($operatorId == 'TATAPWR00DEL01'){
                     $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'CA Number', 'rules' => 'trim|required|numeric|min_length[11]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check CA number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if($operatorId == 'TORR00000AGR01' || $operatorId == 'TORR00000AHM02' || $operatorId == 'TORR00000BHW03' || $operatorId == 'TORR00000SUR04'){
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Service Number', 'rules' => 'trim|required|numeric|min_length[0]|max_length[0]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'city', 'label' => 'City', 'rules' => 'trim|required|min_length[0]|max_length[0]|xss_clean', 'errors' => array('required' => 'Please check city.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if($operatorId == 'UHBVN0000HAR01'){
                     $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Account Number', 'rules' => 'trim|required|numeric|min_length[10]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))

                    );
                }else if($operatorId == 'UPPCL0000UTP01'){
                     $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[10]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                }else if($operatorId == 'UPCL00000UTT01'){
                     $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Service Connection Number', 'rules' => 'trim|required|min_length[13]|max_length[13]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                }else if($operatorId == 'WBSEDCL00WBL01'){
                     $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer ID', 'rules' => 'trim|required|numeric|exact_length[9]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                }else {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please check consumer number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                }
                break;
            case 'Gas':
                if ($operatorId == "AGL000000MAP01") { //AVANTIKA GAS
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Customer No', 'rules' => 'trim|required|min_length[10]|max_length[15]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "ADAN00000NAT01") { // ADANI GAS
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Customer ID', 'rules' => 'trim|required|numeric|min_length[10]|max_length[10]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "CUGL00000UTP01") { // Central U.P. Gas Limited
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Customer Code/CRN Number', 'rules' => 'trim|required|min_length[6]|max_length[10]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "CGSM00000GUJ01") { // Charotar Gas Sahakari Mandali Ltd
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Customer Number', 'rules' => 'trim|required|numeric|min_length[1]|max_length[5]|xss_clean', 'errors' => array('required' => 'Please check  number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "GUJGAS000GUJ01") { // Gujrat Gas
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Customer ID', 'rules' => 'trim|required|numeric|min_length[1]|max_length[15]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "HCG000000HAR02" || $operatorId == "HCG000000HAR01") { // Haryana City Gas - Kapil Chopra Enterprise
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'CRN Number', 'rules' => 'trim|required|numeric|min_length[10]|max_length[10]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "IOAG00000DEL01") { // Indian Oil
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Customer ID', 'rules' => 'trim|required|numeric|min_length[10]|max_length[10]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "INDRAPGASDEL02") { // Indraprastha Gas
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'BP Number', 'rules' => 'trim|required|numeric|min_length[10]|max_length[10]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "MAHA00000MUM01") { //  Mahanagar Gas Limited
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'CA Number', 'rules' => 'trim|required|numeric|min_length[12]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "MNGL00000MAH02") { // Maharashtra Natural Gas Limited (MNGL)
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'BP Number', 'rules' => 'trim|required|numeric|min_length[7]|max_length[10]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "MNGL00000MAH01") { // Maharashtra Natural Gas Limited (MNGL) - Old
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'BP Number', 'rules' => 'trim|required|numeric|min_length[7]|max_length[10]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "SGL000000GUJ01") { // Sabarmati Gas Limited (SGL)
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Customer ID', 'rules' => 'trim|required|numeric|min_length[12]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "SITI00000UTP03") { // Siti energy
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'ARN Number', 'rules' => 'trim|required|numeric|min_length[7]|max_length[9]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "TNGCLOB00TRI01") { // Tripura natural gas
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[1]|max_length[20]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "UCPGPL000MAH01") { // Unique Central Piped Gases Pvt Ltd (UCPGPL)
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Customer Number', 'rules' => 'trim|required|min_length[8]|max_length[8]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "VGL000000GUJ01") { // Vadodara Gas Ltd
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[7]|max_length[7]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Customer ID', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check customer id.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                }
                break;
            case 'Water':
                if ($operatorId == "BWSSB0000KAR01") { //'UIT Bhiwadi'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Water Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'RR Number', 'rules' => 'trim|required|min_length[8]|max_length[8]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "BMC000000MAP01") { //'Uttarakhand Jal Sansthan(B2B)-Retail'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Water Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'Connection Id', 'rules' => 'trim|required|numeric|min_length[8]|max_length[8]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "GMC000000MAP01") { //'Uttarakhand Jal Sansthan(B2B)-Retail'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Water Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'Connection Id', 'rules' => 'trim|required|numeric|min_length[1]|max_length[8]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "HMWSS0000HYD01") { //'Uttarakhand Jal Sansthan(B2B)-Retail'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Water Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'CAN Number', 'rules' => 'trim|required|min_length[2]|max_length[25]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "DLJB00000DEL01" || $operatorId == "MCG000000GUR01") { //'Delhi Jal Board-Retail'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Water Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => ' Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "NDMC00000DEL01") { //'Municipal Corporation of Gurugram'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Water Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|min_length[1]|max_length[20]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                }else if($operatorId == 'PUNE00000MAHSE'){
                     $config = array(
                        array('field' => 'operator', 'label' => 'Water Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|min_length[1]|max_length[20]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "IMC000000MAP01" || $operatorId == 'JMC000000MAP01') { //'Municipal Corporation of Gurugram'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Water Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'Service Number', 'rules' => 'trim|required|numeric|min_length[6]|max_length[15]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "MCJ000000PUN01") { //'Municipal Corporation of Gurugram'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Water Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'Account No', 'rules' => 'trim|required|numeric|min_length[1]|max_length[9]|xss_clean', 'errors' => array('required' => 'Please check account number.')),
//                        array('field' => 'c_mob_number', 'label' => 'Consumer Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check consumer mobile number.')),
//                        array('field' => 'useremail', 'label' => 'Consumer Email ID', 'rules' => 'trim|required|min_length[5]|max_length[75]|xss_clean', 'errors' => array('required' => 'Please check email..')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "MCL000000PUN01") { //'Municipal Corporation of Gurugram'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Water Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[1]|max_length[10]|xss_clean', 'errors' => array('required' => 'Please check number.')),
//                        array('field' => 'c_mob_number', 'label' => 'Consumer Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check consumer mobile number.')),
//                        array('field' => 'c_email_id', 'label' => 'Consumer Email ID', 'rules' => 'trim|required|min_length[5]|max_length[100]|xss_clean', 'errors' => array('required' => 'Please check email.')),
                    );
                } else if ($operatorId == "SMC000000GUJ01") {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Water Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'Connection Number', 'rules' => 'trim|required|numeric|min_length[1]|max_length[20]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == 'UNN000000MAP01') {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Water Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'Business Partner Number', 'rules' => 'trim|required|numeric|min_length[8]|max_length[10]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == 'UITWOB000BHW02' || $operatorId == 'UITWOB000BHW01') {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Water Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'Customer ID', 'rules' => 'trim|required|numeric|min_length[3]|max_length[20]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "UJS000000UTT01") {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Water Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number (Last 7 Digits)', 'rules' => 'trim|required|numeric|exact_length[7]|xss_clean', 'errors' => array('required' => 'Consumer number must be exactly 7 digits.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check consumer number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                }
                break;
            case 'Mobile Postpaid':
                if ($operatorId == "ATPOST000NAT01" || $operatorId == "BSNLMOB00NAT01" || $operatorId == "IDEA00000NAT01" || $operatorId == "JIO000000NAT01"
                        || $operatorId == "TATADCDMANAT01" || $operatorId == "TATADGSM0NAT01" || $operatorId == "VODA00000NAT01") { //'Connect Mobile-Postpaid'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Mobile Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter mobile operator.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Mobile Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter mobile operator.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check Mobile number.'))
                    );
                }
                break;
            case 'DTH':
                if ($operatorId == "DISH00000NAT01") {
                    $config = array(
                        array('field' => 'operator', 'label' => 'DTH Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Registered Mobile Number / Viewing Card Number', 'rules' => 'trim|required|numeric|min_length[10]|max_length[11]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "SUND00000NAT01") { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'DTH Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Subscriber Number', 'rules' => 'trim|required|exact_length[11]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "SUND00000NAT02") {
                    $config = array(
                        array('field' => 'operator', 'label' => 'DTH Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Subscriber Number', 'rules' => 'trim|required|exact_length[11]|xss_clean', 'errors' => array('required' => 'Please check subscriber number.')),
                        array('field' => 'amount', 'label' => 'Amount', 'rules' => 'trim|required|numeric|min_length[1]|max_length[4]|xss_clean', 'errors' => array('required' => 'Please check the amount.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "TATASKY00NAT01") { 
                    $config = array(
                        array('field' => 'operator', 'label' => 'DTH Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Subscriber Number', 'rules' => 'trim|required|numeric|min_length[10]|max_length[11]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "VIDEOCON0NAT01") {
                    $config = array(
                        array('field' => 'operator', 'label' => 'DTH Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Customer ID / Registered Telephone No', 'rules' => 'trim|required|numeric|min_length[3]|max_length[15]|xss_clean', 'errors' => array('required' => 'Please check number.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else {
                    $config = array(
                         array('field' => 'operator', 'label' => 'DTH Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Customer ID', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check customer id.')),
                        array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                }
                break;
            case 'Landline Postpaid':
                if ($operatorId == "ATLLI0000NAT01") { //ACT Fibernet
                    $config = array(
                        array('field' => 'operator', 'label' => 'Landline Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select operator.')),
                        array('field' => 'number', 'label' => 'Landline Number with STD code', 'rules' => 'trim|required|numeric|exact_length[11]|xss_clean', 'errors' => array('required' => 'Please check number')),
                       // array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "BSNLLLCORNAT01") {
                   $config = array(
                        array('field' => 'operator', 'label' => 'Landline Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select operator.')),
                        array('field' => 'number', 'label' => 'Account Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check number')),
                        //array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "BSNLLLINDNAT01") {
                   $config = array(
                        array('field' => 'operator', 'label' => 'Landline Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select operator.')),
                        array('field' => 'number', 'label' => 'Account Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check number')),
                        array('field' => 'mobile_number', 'label' => 'Number with STD Code (without 0)', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check number.'))
                       //array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "MTNL00000DEL01" || $operatorId == 'MTNL00000MUM01') {
                   $config = array(
                        array('field' => 'operator', 'label' => 'Landline Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select operator.')),
                        array('field' => 'number', 'label' => 'Account Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check number')),
                       array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[8]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else if ($operatorId == "TATADLLI0NAT01") {
                   $config = array(
                        array('field' => 'operator', 'label' => 'Landline Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select operator.')),
                       array('field' => 'mobile_number', 'label' => 'Landline Number with STD Code (without 0)', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check number.'))
                    );
                }  else {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Broadband Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select operator.')),
                        array('field' => 'number', 'label' => 'Account number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please check service number.')),
                      //  array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                }
           break;
        }
        return $config;
    }
    
    public function getCustomerCharge($value)
    {
        $quStr = "select customer_charge"
        . " FROM services_commissions"
        . " WHERE '" . $value . "' between lower_slab and upper_slab"
        . " AND service_id =".BBPS_UTILITIES_SERVICE_ID
        ;

        $rev = $this->db->query($quStr);
        $result = $rev->result();
        return $result[0]->customer_charge;
    }
    
    public function getTransactionReceipt($user_id,$limit='1', $start='0'){
        
        $this->db->select('td.customerName,td.amount,td.customer_charge,td.type,td.mobileno,td.transactionReference,td.wallet_transaction_no');
        $this->db->from('bbps_transaction_details td');        
        $this->db->join('bbps_recharge_transaction rt','td.wallet_transaction_no = rt.transaction_no','inner'); 
        $this->db->join('vas_commission_detail cd','rt.transaction_no = cd.transaction_no','inner');
        $this->db->join('wallet_trans wt','wt.transaction_no = cd.transaction_no','inner');
        $this->db->where('cd.service_id',BBPS_UTILITIES_SERVICE_ID);   
        $this->db->where('cd.status','Y');
        $this->db->where('td.status','Successful');     
        $this->db->where('rt.created_by',$user_id);
        $this->db->order_by('td.id DESC');

     
        $this->db->limit($limit, $start);
        $query = $this->db->get();
       // last_query(1);
       //echo $this->db->last_query();
        return $query->result_array();  
    }
    
//    public function print_ticket($wallet_trans_id){
//        
//        $this->db->select('td.customerName,td.amount,td.customer_charge,td.type,td.mobileno,td.transactionReference,td.wallet_transaction_no');
//        $this->db->from('bbps_transaction_details td');        
//        $this->db->join('bbps_recharge_transaction rt','td.wallet_transaction_no = rt.transaction_no','inner'); 
//        $this->db->join('vas_commission_detail cd','rt.transaction_no = cd.transaction_no','inner');
//        $this->db->join('wallet_trans wt','wt.transaction_no = cd.transaction_no','inner');
//        $this->db->where('cd.service_id',BBPS_UTILITIES_SERVICE_ID);   
//        $this->db->where('cd.status','Y');
//        $this->db->where('td.status','Successful');     
//        $this->db->where('td.wallet_transaction_no',$wallet_trans_id);
//        $this->db->order_by('td.id DESC');
//
//     
//        $this->db->limit($limit, $start);
//        $query = $this->db->get();
//       // last_query(1);
//       //echo $this->db->last_query();
//        return $query->result_array();  
//    }
    
    public function getTransactionLastId($user_id, $limit='1', $start='0'){
        $this->db->select('id');
        $this->db->from('bbps_transaction_details');
        $this->db->where('created_by',$user_id);
        $this->db->order_by('id DESC');
        $this->db->limit($limit, $start);
        $query = $this->db->get()->result_array();
       // last_query(1);
       //echo $this->db->last_query();
        return $query[0]['id'];  
    }
    
    public function getRechargeLastId($user_id, $limit='1', $start='0'){
        $this->db->select('id');
        $this->db->from('bbps_recharge_transaction');
        $this->db->where('created_by',$user_id);
        $this->db->order_by('id DESC');
        $this->db->limit($limit, $start);
        $query = $this->db->get()->result_array();
       // last_query(1);
       //echo $this->db->last_query();
        return $query[0]['id'];  
    }
    
    public function updateRechargeTransactionData($updateData = '', $id=''){
        
        $this->db->where('id',$id);
        $this->db->update("bbps_recharge_transaction", $updateData);
       
    }
    
     
}
