<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Op_details_model extends MY_Model
{
    var $table    = 'op_details';
    var $fields	  = array("id", "op_id", "op_name", "email", "mobile");
    var $key      = 'id';
    
    public function __construct() {
        parent::__construct();
        $this->_init();      
        
    }
}
