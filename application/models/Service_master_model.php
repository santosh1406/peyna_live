<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Service_master_model extends MY_Model {
    
    protected $table = 'service_master';
    var $fields = array("id", "service_name", "service_detail", "library_name", "status", "created_date", "updated_date", "created_by", "updated_by");
    var $key = 'id';

    public function __construct() {
        parent::__construct();
        
    }

    function get_all_services(){

    $response = array();
 
    // Select record
    $this->db->select('*');
    $q = $this->db->get('service_master');
    $response = $q->result_array();

    return $response;
  }

  function get_jri_id(){
    $this->db->select('*');
    $q = $this->db->get('service_master')->where('service_name', 'JRI');
    $response = $q->result_array();
  }

  // Get sub services by services wise
    function getSubServicebyServices($postData){
    $response = array();   

    $q =$this->db->query("SELECT * FROM service_mapping  m  
    							right join sub_services s 
    							on m.sub_service_id = s.id 
    							where s.service_id = $postData[service]
    							") ;

    $response = $q->result_array();
 	return $response;

     
  }

  	public function saveServiceDetails($data)
    {
     	$insert_query = $this->db->insert_string('service_mapping', $data);
    	$insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);

	    $this->db->query($insert_query);
	    /*echo $this->db->last_query();
	    die();*/

    }

    public function get_agent_services() {
        
            $sQuery = "SELECT * FROM service_mapping  m
			right join sub_services s on m.sub_service_id = s.id
			where s.service_id='1' ";
            $query = $this->db->query($sQuery);
            return $query->result_array();
       
    }

    public function last_insert_id(){
        $sQuery = "SELECT * FROM service_master order by id desc LIMIT 1";
        $query = $this->db->query($sQuery);
        $query = $query->result_array();
        return $query[0]['id'];
    }



    public function get_aggregator($type = ''){
        // show($type, 1);
        $sQuery = "SELECT providers_master.provider_name FROM service_master 
        join providers_utilities on providers_utilities.utilities_id = service_master.id
        join providers_master on providers_master.id = providers_utilities.provider_id
        where service_master.service_name = '" . $type . "'";
        $query = $this->db->query($sQuery);
        $q = $query->result_array();
        // $q = $q->db->where('service_name', 'Recharge');
        // $response = $q;
        return $q[0]['provider_name'];
    }
}