<?php

class Bus_stop_master_model extends MY_Model {

    /**
     * Instanciar o CI
     */
    var $table  = 'bus_stop_master';
    var $fields = array("bus_stop_id", "bus_stop_name" ,"tahsil_nm", "district_nm", "nearest_place", "state_nm","pincode","is_onroad","is_boarding","is_status","is_delete","inserted_by","insert_date","updated by","update_date");
    var $key    = "bus_stop_id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    function getstop() {
        $bus_stop = $this->db->get('bus_stop_master');
        return $bus_stop->result_array();
    }

    function get_stops_detail($stop_name, $op_id = "",$stop_code="") {
        // $name_stops = array($from_name, $to_name);
        // $this->db->select("osm.op_id,bsn.bus_stop_id,bsn.bus_stop_name,osm.op_stop_cd,om.op_name");
        // $this->db->from("bus_stop_master bsn");
        // $this->db->join('op_stop_master osm', 'osm.bus_stop_id = bsn.bus_stop_id ', "inner");
        // $this->db->join('op_master om', 'om.op_id=osm.op_id ', "inner");
        // $this->db->where_in('bsn.bus_stop_name ', $name_stops);
        // $this->db->where('om.op_name', $op_name);
        // $query = $this->db->get();
        // return $query->result_array();

        $this->db->select("osm.*,bsm.bus_stop_name");
        $this->db->from("bus_stop_master bsm");
        $this->db->join('op_stop_master osm', 'osm.bus_stop_id = bsm.bus_stop_id ', "inner");
        $this->db->where('bsm.bus_stop_name', $stop_name);
        $this->db->where('osm.record_status', 'Y');
        if($op_id != "")
        {
            $this->db->where('osm.op_id', $op_id);
        }
        if($stop_code != "")
        {
            $this->db->where('osm.op_stop_cd', $stop_code);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_detail_by_name($bus_stop, $op_name)
    {
        $this->db->select("osm.op_id,bsn.bus_stop_id,bsn.bus_stop_name,osm.op_stop_cd,om.op_name");
        $this->db->from("bus_stop_master bsn");
        $this->db->join('op_stop_master osm', 'osm.bus_stop_id = bsn.bus_stop_id ', "inner");
        $this->db->join('op_master om', 'om.op_id=osm.op_id ', "inner");
        
        $this->db->where('bsn.bus_stop_name ', $bus_stop);
        $this->db->or_where('osm.op_stop_cd ', $bus_stop);

        $this->db->where('om.op_name', $op_name);
        $query = $this->db->get();
        return $query->row();   
    }

    /* function :edit_stops
     * param
      detail  :to fetch data for edit by id
     */

    function edit_stops($a) {
        $this->db->where('bus_stop_id', $a);
        $bus_stop = $this->db->get('bus_stop_master');

        return $bus_stop->result_array();
    }

    /* function :update_stops
     * param
      detail:this function update bus stops record
     */

    function update_stops($post_data) {

        $data = array("bus_stop_name" => $post_data['stop_name'], "tahsil_nm" => $post_data['tahsil_name'], "state_nm" => $post_data['state_name'], "district_nm" => $post_data['district_name'], "nearest_place" => $post_data['nearst_place']);
        $this->db->where('bus_stop_id', $post_data['id']);
        $this->db->update('bus_stop_master', $data);
    }

    /* function :delete_stops
     * param
      detail  :to delete data by id
     */

    function delete_stop($id) {
        $this->db->where('bus_stop_id', $id);
        $this->db->delete('bus_stop_master');
        return $this->db->last_query();
    }

    /* function :getstate
     * param
      detail  :retrives state name in autocomplete  text
     */

    function getstate($st) {
        $this->db->select('state_name,state_code');
        $this->db->like('state_name', $st, 'after');
        $state = $this->db->get('state');

        return $state->result_array();
    }

    /* function :getdistrict
     * param
      detail  :retrives district name in dropdown
     */

    function getdistrict($value) {
        $this->db->select('district_name');
        $this->db->where('state_cd', $value);
        $state = $this->db->get('district');

        return $state->result_array();
    }

    function addstop($data) {
        $this->db->insert('bus_stop_master', $data);
        return $this->db->last_query();
    }

    function checkstop($st) {
        $this->db->select('*');
        $this->db->from('bus_stop_master');
        $this->db->where('bus_stop_name', $st);
        $stop_name = $this->db->get();
        return $stop_name->result_array();
    }

    function get_bus_id_or_insert($where = array()) {
        $result = false;
        if (count($where) > 0) {
            foreach ($where as $key => $value) {
                $this->$key = ucfirst(strtolower($value));
            }

            $data = $this->select(false);

            if (count($data) > 0) {
                return $data[0]->bus_stop_id;
            } else {
                return $result = $this->save();
            }
        }
        return $result;
    }

    function autocomplete_bus_stops() {
        
        $input = $this->input->get();
        $this->db->select('bus_stop_name as value,bus_stop_name as label');
        $this->db->like('bus_stop_name', $input['q'], 'after');
        $this->db->from('bus_stop_master');
        $this->db->order_by('bus_stop_name','asc');
        $query = $this->db->get();
        $data = $query->result_array();
        return $data;
    }

    function get_busstop_id($bus_stop_cd) {
        $this->db->select("bus_stop_id");
        $this->db->where('bus_stop_name', $bus_stop_cd);
        $this->db->from("bus_stop_master");
        $query = $this->db->get();
        return $query->row()->bus_stop_id;
    }

    function chk_byid($btid, $btname) {
        $this->db->select('bus_stop_name');
        $this->db->where('bus_stop_name', $btname);
        $this->db->where_not_in('bus_stop_id', $btid);
        $this->db->from("bus_stop_master");
        $query = $this->db->get();
        $data = $query->result_array();
        return $data;
    }

    function autocomplete_states() {
        $input = $this->input->get();
        $this->db->select('state_name as value,state_name as label');
        $this->db->like('state_name', $input['q'], 'after');
        $this->db->from('state');
        $query = $this->db->get();
        $data = $query->result_array();
        return $data;
      
    }

    function chk_stop_inrt($btid) {
        $this->db->select('bus_stop_name	');
        $this->db->from('route_stops');
        $this->db->where('bus_stop_id', $btid);
        $this->db->where('status', 'N');
        $data = $this->db->get();
        return $data->result_array();
    }

}

?>
