<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

 
class generateid_model extends CI_Model {

 

    public function __construct() {

    }

 
    public function getUnqId() {

        $data = array('ID_CATEGORY' => date("ym"),

            "CREATED_DATE" => date("Y-m-d H:i:s"));

        $this->db->insert("generateid", $data);

        return $this->db->insert_id();

    }

 

    public function getCardId() {

        $data = array('ID_CATEGORY' => "CARD",

            "CREATED_DATE" => date("Y-m-d H:i:s"));

        $this->db->insert("generateid", $data);

        return $this->db->insert_id();

    }

 

    public function getRequiredId($ID_TYPE) {

        $data = array('ID_CATEGORY' => $ID_TYPE,

            "CREATED_DATE" => date("Y-m-d H:i:s"));

        $this->db->insert("generateid", $data);

        return $this->db->insert_id();

    }

 

}

 

?>
