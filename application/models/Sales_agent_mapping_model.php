<?php

class Sales_agent_mapping_model extends MY_Model {

    var $table = 'depots';
    var $fields = array("DEPOT_CD", "DIVISION_CD", "REGION_CD","STATE_CD","DEPOT_NM","IS_AUDIT_TRAIL","KIRAN_NM","DEVNAGIRI_NM","MACHINE_CD","BMTC","CITY","MOFF","INTERSTATE");
    var $key = 'DEPOT_CD';

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
    
    public function get_depot_data_by_service($id)
    {      
        
        if (!empty($id)) {
            $sQuery = "SELECT depot_code, division_code FROM retailer_service_mapping WHERE agent_id='" . $id . "' and service_id = '11' and status = 'Y' ";
            $query = $this->db->query($sQuery);
            return $query->result();
        } else {
            return false;
        }
    
    }

    public function get_depots_list() {

        $msrtc_replication = MSRTC_REPLICATION;
        $this->db->select('DEPOT_CD,DEPOT_NM');
        $this->db->from($msrtc_replication.".depots");
        $this->db->order_by("DEPOT_NM", "asc");
        $res = $this->db->get();
        return $res->result_array();   
    }

    public function get_division_data($depot_cd) {

        $msrtc_replication = MSRTC_REPLICATION;
        $this->db->select('divi.DIVISION_NM,divi.DIVISION_CD');
        $this->db->from($msrtc_replication.".depots as dept");
        $this->db->join($msrtc_replication.".divisions as divi", 'dept.DIVISION_CD = divi.DIVISION_CD', 'inner');
        $this->db->where("dept.DEPOT_CD", $depot_cd);
        $res = $this->db->get()->row();
        return $res;   
    }
    
    public function get_all_division_data() {

        $msrtc_replication = MSRTC_REPLICATION;
        $this->db->select('divi.DIVISION_NM,divi.DIVISION_CD,dept.DEPOT_CD');
        $this->db->from($msrtc_replication.".depots as dept");
        $this->db->join($msrtc_replication.".divisions as divi", 'dept.DIVISION_CD = divi.DIVISION_CD', 'inner');        
        $res = $this->db->get()->result();
        return $res;   
    }


    public function get_smartcard_id() {

        $this->db->select('id');
        $this->db->from("service_master");
        $this->db->where("service_name", "SmartCard");
        $res = $this->db->get();
        return $res->result_array();   
    }
    
    public function get_agent_division_code($id) {
        if (!empty($id)) {
            $msrtc_replication = MSRTC_REPLICATION;
            
            $sQuery = "SELECT rsm.depot_code,rsm.division_code ,divi.DIVISION_NM"
                    . " FROM retailer_service_mapping rsm "
                    . " join ".$msrtc_replication.".divisions as divi on rsm.division_code=divi.DIVISION_CD"                    
                    . " WHERE rsm.agent_id='" . $id . "' and rsm.status='Y'  ";
            $query = $this->db->query($sQuery);
            return $query->result();
        } else {
            return false;
        }
    }

}

?>
