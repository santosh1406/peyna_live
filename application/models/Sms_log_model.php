<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Agent_commission_model
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sms_log_model extends MY_Model {
    
    
    var $table  = 'sms_log';
    var $fields = array("id","request_id","transaction_id","submit_date","mobile_number","supplied_mobile_number","message","ticket_id","status","error_code","failed_reason","timestamp");
    var $key    = 'id';
    protected $set_created =false;
    protected $set_modified =false;



    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

    public function save_sms_log($sms_log)
    {
       return $this->db->insert('sms_log', $sms_log);
    }
    
  
}

// End of Agent_commission_model class