<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Credit_account_detail_model extends MY_Model {

    var $table  = 'credit_account_detail';
    var $fields = array("id","user_id","credit_limit_amt","effective_from","effective_to","credit_type","status","accept_reject_status","added_by","added_on","updated_by","updated_on","accept_reject_by");
    var $key    = 'id';

    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
}


?>