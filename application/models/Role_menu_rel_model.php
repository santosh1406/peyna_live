<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Role_menu_rel_model
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Role_menu_rel_model extends MY_Model {

    var $table = "role_menu_rel";
    var $fields = array("id", "role_id", "menu_id", "view", "add", "edit", "delete","export","pdf","approve","record_status", "created_by", "created_date");
    var $key = "id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    /*
     * @function    : delete_role_menu_rel
     * @param       : role_id,menu_ids
     * @detail      : return true or false 
     *                 
     */

    public function delete_role_menu_rel($role_id, $menu_ids) {
        $this->db->where('role_id', $role_id);
        $this->db->where_in('menu_id', $menu_ids);
        return $this->db->delete("role_menu_rel");
    }

     public function delete_all_menu_rel($role_id) {
        $this->db->where('role_id', $role_id);
        return $this->db->delete("role_menu_rel");
    }
    public function delete_menu_rel_permission($role_id,$parent_id)
    {
        $this->db->where('role_id', $role_id);
        $this->db->where_in('menu_id', $parent_id);
        return $this->db->delete("role_menu_rel");
    }
}
