<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Op_wallet_trans_log_model extends MY_Model {

    var $table  = 'op_wallet_trans_log';
    var $fields = array("id","op_wallet_id","opening_bal","closing_bal","amount","actual_wallet_amount","virtual_amount","outstanding_amount","total_bal_amt_before_trans","total_bal_amt_after_trans","virtual_amt_before_trans","virtual_amt_after_trans","wallet_type","credit_detail_id","transaction_type","comment","status","transfer","created_by","created_date");
    
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
  
}
