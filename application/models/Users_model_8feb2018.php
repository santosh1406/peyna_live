<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of users
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Users_model extends MY_Model {
    
    var $table  = 'users';
    var $fields = array("id","role_id","agent_code","username","first_name","last_name","display_name","email","sec_email","gender","salt","password","tpin","address1","address2","pincode","firm_name","pancard", "adharcard", "mobile_no", "alternate_no","country","state","city","dob","profile_image","forgot_password","ip_address", "activate_key","remember_me","inserted_by","commission_id","parent_id","kyc_verify_status","email_verify_status", "verify_status", "status", "is_deleted", "level","level_1","level_2","level_3","level_4","level_5","depot_name","depot_cd","star_seller_code","star_seller_name","location","btypeid","schemeid","topuplmit","hmmlimit","securitydeposite","created_date","updated_by","updated_date","otp_verify_status","company_gst_no","lock_status","lock_reason","lock_user_id","unlock_user_id");
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    /*
     * @function    : get_user_by_username
     * @param       : 
     * @detail      : To fetch particular user data.
     */

    function get_user_by_username($username) {
        if ($username != "") {
            $this->db->select("u.*,ur.role_name");
            $this->db->from("users u");
            $this->db->join('user_roles ur', 'ur.role_id = u.role_id', 'left');
            $this->db->where("username = '" . $username . "'");
            $user_result = $this->db->get();
            $user_result_array = $user_result->row_array();
            return $user_result_array;
        }
    }

// get_user_by_username End   

    /*
     * @function    : get_alluser
     * @param       : 
     * @detail      : To fetch all users from user table.
     */

    function get_alluser($where = "") {
        $this->db->select("*");
        $this->db->from("users");
        if ($where != "") {
            $this->db->where($where);
        }

        $user_result = $this->db->get();

        $user_result_array = $user_result->result_array();
        return $user_result_array;
    }

// get_alluser End   

    /*
     * @function    : check_user_exists
     * @param       : $username, $email
     * @detail      : To check if use exists.
     */

    function check_user_exists($username = "", $email = "")
    {
        $this->db->select("*");
        $this->db->from("users");
        if ($username != "" and $email != "") {
            $this->db->where("username = '" . $username . "' or email = '" . $email . "'");
        } else if ($username != "") {
            $this->db->where("username = '" . $username . "'");
        } else if ($email != "") {
            $this->db->where("email = '" . $email . "'");
        }
        $query = $this->db->get();
        $total = $query->num_rows();
        return $total;
    }



    /* @Author      : Suraj Rathod
     * @function    : check_user_registered
     * @param       : $username, $email
     * @detail      : To check if use exists.
     */

    function check_user_registered($username = "", $email = "")
    {
        $this->db->select("*");
        $this->db->from("users");
        if ($username != "" and $email != "") {
            $this->db->where("username = '" . $username . "' or email = '" . $email . "'");
        } else if ($username != "") {
            $this->db->where("username = '" . $username . "'");
        } else if ($email != "") {
            $this->db->where("email = '" . $email . "'");
        }
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }


    /* @Author      : Suraj Rathod
     * @function    : check_user_exists
     * @param       : $username, $email
     * @detail      : To check if use exists.
     */

    function check_is_registered($username = "", $email = "")
    {
        $this->db->select("*");
        $this->db->from("users");

        if ($username != "" and $email != "")
        {
            $this->db->where("username = '" . $username . "' or email = '" . $email . "'");
        }
        else if ($username != "")
        {
            $this->db->where("username = '" . $username . "'");
        } 
        else if ($email != "")
        {
            $this->db->where("email = '" . $email . "'");
        }

        $this->db->where("password != ''");
        $this->db->where("salt != ''");

        $query = $this->db->get();
        $total = $query->num_rows();
        return $total;
    }


    // check_user_exists End
    /*
     * @function    : get_user_by_userid
     * @param       : $userid
     * @detail      : return user detail by user id.
     */

    function get_user_by_id($id) {
        $this->db->select("*");
        $this->db->from("users");
        $this->db->where("id = " . $id);
        $query = $this->db->get();
        $result_array = $query->row_array();
        return $result_array;
    }

    // get_user_by_userid End   


    /*
     * @function    : update_user_by_id
     * @param       : $userdata -> associative array 1) array field to update 2) id to update
     * @detail      : return true or fasle.
     */

    function update_user_by_id($userdata) {

        $this->db->where('id', $userdata["id"]);
        //$this->db->update("users", $userdata["user_array"]);
        // die($this->db->last_query());

        if ($this->db->update("users", $userdata["user_array"])) {
            return true;
        } else {
            return false;
        }
    }

    // update_user_by_id End  

    /*
     * @function    : add_user
     * @param       : $userdata -> data to add.
     * @detail      : to add user in user table.
     *                 
     */

    public function add_user($userdata) {
        // show($userdata,1);

        if (!empty($userdata)) {
            if ($this->db->insert('users', $userdata)) {

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

// end of add user


    /*
     * @function    : get_user_by_email
     * @param       : $email -> email id of user
     * @detail      :to get user by email id.
     *                 
     */

    public function get_user_by_email($email) {
        if ($email != "") {
            $this->db->select("*");
            $this->db->from("users");
            $this->db->where("email = '" . $email . "'");
            $user_result = $this->db->get();
            $user_result_array = $user_result->row_array();
            return $user_result_array;
        }
    }

    // end of get_user_by_email


    /*
     * @function    : add_users_metadata
     * @param       : $metadata
     * @detail      : enters google login return data to user_metadata table.
     *                 
     */

    public function add_users_metadata($metadata) {

        if (!empty($metadata)) {
            if ($this->db->insert('user_metadata', $metadata)) {

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    // end of add_users_metadata

    public function frm_booking_add_user($userdata) {

        $this->db->select("*");
        $this->db->from("users");
        $this->db->where("email", $userdata[0]);
        $query = $this->db->get();
        $result_array = $query->row_array();
        if (count($result_array) <= 0) {

            $data = array(
                'email' => $userdata[0],
                'mobile_no' => $userdata[1]
            );
            $this->db->insert('users', $data);
            $user_id = $this->db->insert_id();
        } else {
            $user_id = $result_array['id'];
        }
        return $user_id;
    }

    public  function getuserid($id)
    {
        $this->db->select('user_id');
        $this->db->from('users');
        $this->db->where('id',$id);
        $query = $this->db->get();
        return $query->row()->user_id;
        
    }
    public function check_mail($email,$id = "")
    {
        $this->db->select('email');
        $this->db->where('email', $email);
        if($id !="")
        {
            $this->db->where_not_in('id', $id);
        }
        $this->db->from("users");
        $query = $this->db->get();
        $data = $query->result_array();
        return $data;
    }
    
    function search_user_by_mail($email,$urlkey)
    {
        $this->db->select('email,verify_status');
        $this->db->from("users");
        $this->db->where('sha1(email)',$email);
        $this->db->where('activate_key',$urlkey);
        $data=$this->db->get();
        return $data->result_array();
        
    }
    function  verify_user($email,$user_key)
    {
          $data=array("verify_status"=>"Y");
          $this->db->where('sha1(email)',$email);
          $this->db->where('activate_key',$user_key);
          $this->db->update('users',$data);
      
    }
    
    function check_username_exist($user)
    {
        $this->db->select('username');
        $this->db->from('users');
        $this->db->where('username',$user);
        $uname=$this->db->get();
        return $uname->result_array();
    }
    
    function get_user_detail($uid)
    {
       $this->db->select('u.*,cm.stCountryName as countrynm,sm.stState as statenm,cim.stCity as citynm');
       $this->db->where('u.id',$uid);
       $this->db->from('users u');
       $this->db->join('country_master cm','u.country=cm.intCountryId','left');
       $this->db->join('state_master sm','u.state=sm.intStateId','left');
       $this->db->join('city_master cim','u.city=cim.intCityId','left');
       
        $res=$this->db->get();
        return $res->result_array();
    }
    //to fetch and display users in the backend dashboard edit users.
    function get_user_by_user_id($uid)
    {
       $this->db->select("u.*,DATE_FORMAT(u.dob,'%d-%m-%Y') as dob,cm.stCountryName as countrynm,sm.stState as statenm,cim.stCity as citynm");
       $this->db->where('u.id',$uid);
       $this->db->from('users u');
       $this->db->join('country_master cm','u.country=cm.intCountryId','left');
       $this->db->join('state_master sm','u.state=sm.intStateId','left');
       $this->db->join('city_master cim','u.city=cim.intCityId','left');
       
        $query = $this->db->get();
        $result_array = $query->row_array();
        return $result_array;
    }


    /* @Author      : Suraj Rathod
     * @function    : get_user_lib_delete
     * @param       : $metadata
     * @detail      : 
     *                 
     */

    public function get_user_lib_delete($user_id) {

        if ($user_id != "")
        {
            $this->db->select("u.*,ur.role_name");
            $this->db->from("users u");
            $this->db->join('user_roles ur', 'ur.id = u.role_id', 'inner');
            $this->db->where("u.id = '" . $user_id . "'");
            $user_result = $this->db->get();
            if($user_result->num_rows())
            {
              $user_result_array = $user_result->result_array();
              $role_array =  $user_result_array[0];
              if(strtolower($role_array["role_name"]) == "api provider")
              {
                $this->db->select("*");
                $this->db->from("api_provider");
                $this->db->where("user_id = '" . $user_id . "'");
                $api_result = $this->db->get();
                $api_result_array = $api_result->result_array();
                $api_result_array[0]["role_name"] = $role_array["role_name"];
                return $api_result_array[0];
              }
              else if(strtolower($role_array["role_name"]) == "operator")
              {
                $this->db->select("*");
                $this->db->from("op_master");
                $this->db->where("user_id = '" . $user_id . "'");
                $api_result = $this->db->get();
                $api_result_array = $api_result->result_array();
                $api_result_array[0]["role_name"] = $role_array["role_name"];
                return $api_result_array[0];
              }
            }
            else
            {
              return false;
            }
        } 
        else
        {
            return false;
        }
    } 
    // $show_distributor parameter is used in User_wallet controller
    public function get_agent($city = 0,$show_distributor = 0)
    {
        eval(FME_AGENT_ROLE_ID);
        $this->db->select('users.id,concat(first_name," ",last_name) as agent_name,commission_id,ur.role_name as role_name');
        $this->db->where_in('role_id',$fme_agent_role_id);
        
        if($city > 0)
        {
          $this->db->where('city',$city); 
        }
        if($show_distributor){
            $this->db->or_where('role_id',DISTRIBUTOR_ROLE_ID);
        }
        
        $this->datatables->join('user_roles ur','ur.id=users.role_id','inner');
        $this->db->order_by('first_name');
        $data = $this->db->get($this->table);
        if($data->num_rows()>0)
        {
            return $data->result_array();
        }
       else {
          return false;
        }
    }


    function get_agent_details()
    {
        eval(FME_AGENT_ROLE_ID);
       $this->db->select('u.id,rm.referrer_name,u.first_name,u.last_name,u.display_name');
       $this->db->from('users u');
       $this->db->join('referrer_master rm','rm.referrer_code = u.referrer_code','left');
       $this->db->where_in('u.role_id',$fme_agent_role_id);
       $this->db->where('u.referrer_code != ',"");
      $res=$this->db->get();
        return $res->result_array();
    }
     function ors_agent_details()
    {
       eval(FME_AGENT_ROLE_ID);
       $this->db->select('u.id,u.display_name,u.mobile_no,email');
       $this->db->from('users u');
       $this->db->where_in('u.role_id',$fme_agent_role_id);
       $this->db->where('u.status',"Y");
       $this->db->order_by('u.display_name');
       $res=$this->db->get();
       return $res->result_array();
    }
    
    function user_referrer_details($id)
    {
        eval(FME_AGENT_ROLE_ID);
        $this->db->select('u.id,concat(u.first_name," ",u.last_name) as agent_name,u.mobile_no,u.email,r.id as referrer_id,r.referrer_name as referrer_name,r.referrer_mobile as referrer_mobile,r.referrer_email as referrer_email');
        $this->db->from('users u');
        $this->db->join('referrer_master r','u.referrer_code=r.referrer_code','left');
        $this->db->where(array('u.id='=> $id,'u.status'=>'Y','u.verify_status'=>'Y','r.is_status'=>'Y'));
        $this->db->where_in('u.role_id',$fme_agent_role_id);
        $datas = $this->db->get();
        if($datas->num_rows() > 0)
        {
            return  $datas->result_array();
        }
        else
        {
            return false;
        }
    }

	
	function verify_user_email($user_id)
    {
	  $data=array("email_verify_status"=>"Y");
	  $this->db->where('id',$user_id);          
	  $this->db->update('users',$data);
    }
	
	   /*
         * @Author      : Rajkumar Shukla
         * @function    : check_mobile_no_registered
         * @param       : mobile no.
         * @detail      : To check the mobile no is unique.          
         */

        function check_mobile_no_registered($input) {
            if ($input) {
                $new_mobile = rokad_encrypt($input['mobile_no'],$this->config->item("pos_encryption_key"));
                $where_user = array("mobile_no" => $new_mobile,
                    "is_deleted" => 'N'
                );

                $count = $this->users_model
                        ->where($where_user)
                        ->count_all();

                $flag = ($count) ? 'true' : 'false';
                return $flag;
            } else {
                redirect(base_url());
            }
        }

        /*
         * @Author      : Rajkumar Shukla
         * @function    : chcek_email_registered
         * @param       : email id.
         * @detail      : To check the email id is unique.          
         */

        function check_email_registered($input) {
            if ($input) {
                $new_mail = rokad_encrypt($input['email'],$this->config->item("pos_encryption_key"));
                $where_user = array("email" => $new_mail,
                );

                $count = $this->users_model
                        ->where($where_user)
                        ->count_all();

                $flag = ($count) ? 'true' : 'false';
                echo $flag;
            } else {
                redirect(base_url());
            }
        }
        
        function get_md_data($id) {
        	$rollid = MASTER_DISTRIBUTOR_ROLE_ID;
        	$sQuery = "SELECT id,display_name,mobile_no FROM users WHERE level_2=" . $id . " and role_id=" . $rollid . " and status ='Y' and is_deleted ='N' order by display_name";
        	$query = $this->db->query($sQuery);
        	return $query->result();
        }
        
      function get_ad_data($id) {
            $rollid = AREA_DISTRIBUTOR_ROLE_ID;
            $sQuery = "SELECT id,display_name,mobile_no FROM users WHERE level_3=" . $id . " and role_id=" . $rollid . " and status ='Y' and is_deleted ='N' order by display_name";
            $query = $this->db->query($sQuery);
            return $query->result();
        }

        function get_distributor_data($id) {
            $rollid = DISTRIBUTOR;
            $sQuery = "SELECT id,display_name,mobile_no FROM users WHERE level_4=" . $id . " and role_id=" . $rollid . " and status ='Y' and is_deleted ='N' order by display_name";
            $query = $this->db->query($sQuery);
            return $query->result();
        }

        function get_retailer_data($id) {
            $rollid = RETAILER_ROLE_ID;
            $sQuery = "SELECT id,display_name,mobile_no FROM users WHERE level_5=" . $id . " and role_id=" . $rollid . " and status ='Y' and is_deleted ='N' and kyc_verify_status = 'Y' order by display_name";
            $query = $this->db->query($sQuery);
            return $query->result();
        }

        /*
         * @Author      : Rajkumar Shukla
         * @function    : fetch_all_md
         * @param       : md role id.
         * @detail      : To fetchall the mds.          
         */

        public function fetch_all_md($id="") {
            $this->db->select('id,display_name,level,mobile_no');
            if($id)
            {
               $this->db->where('level_2', $id);               
            }
            $this->db->where('role_id', MASTER_DISTRIBUTOR_ROLE_ID);
            $this->db->where('status','Y');
            $this->db->where('verify_status','Y');
            $this->db->where('is_deleted','N');
            $data = $this->db->get($this->table);
            if ($data->num_rows() > 0) {
                return $data->result_array();
            }
            return false;		
        }

        /*
         * @Author      : Rajkumar Shukla
         * @function    : fetch_all_rokad_users
         * @param       : 
         * @detail      : To fetchall rokad users.          
         */

        public function fetch_all_rokad_users($input, $response_type = '') {
            $role_name = $this->session->userdata('role_name');
            $user_id = $this->session->userdata('user_id');
            $where = array('id' => $user_id);
            $userdata = $this->get_alluser($where);
            $this->datatables->select("u.id as id,u.display_name,ur.role_name as role_name,u.mobile_no,u.status,(CASE when u.otp_verify_status = 'N' then 'No' when u.otp_verify_status = 'Y' then 'Yes' Else u.otp_verify_status END) as mobile_verified,u.kyc_verify_status as kyc_status,u.email_verify_status");
            $this->datatables->from("users u");
            if(!empty($userdata[0]['level'])){
                $this->datatables->where('level_'.$userdata[0]['level'],$user_id);
            }
            $this->datatables->where('is_deleted','N');
            $this->datatables->where('role_id !=',SUPERADMIN_ROLE_ID);
            $this->datatables->where('role_id !=',TRIMAX_ROLE_ID);
            $this->datatables->where('role_id !=',SUPPORT_ROLE_ID);
            $this->datatables->where('role_id !=',ACCOUNT_ROLE_ID);
            $this->datatables->where('role_id !=',ADMIN_ROLE_ID);
            $this->datatables->join('user_roles ur', 'u.role_id=ur.id', 'left');
            
            if(!empty($input['user_type'])){ 
                $this->datatables->where('role_id',$input['user_type']);
            }
            if(!empty($input['rmn_verified'])){
                $this->datatables->where('otp_verify_status',$input['rmn_verified']);
            }
            if(!empty($input['status'])){
                $this->datatables->where('status',$input['status']);
            }
            if(!empty($input['kyc_status'])){
                $this->datatables->where('kyc_verify_status',$input['kyc_status']);
            }
            if(!empty($input['email_status'])){
                $this->datatables->where('email_verify_status',$input['email_status']);
            }

             if ($response_type == 'json') {
                
                $this->datatables->add_column('kyc_verify_status', '$1', 'kyc_approval_users(id,kyc_status,role_name,status,email_verify_status,mobile_verified)');
                $this->datatables->add_column('email_verify_status', '$1', 'actv_dactv_users(id,email_verify_status,status)');
                $this->datatables->add_column('action', '$1', 'check_agent_users_status(id,status,kyc_status,email_verify_status,role_id,is_deleted,mobile_verified)');
                $newdata = $this->datatables->generate($response_type);
                $newdata_array = json_decode($newdata);
                if (!empty($newdata_array->data)) {                
                    foreach ($newdata_array->data as &$detail) {
                        $detail['3'] = rokad_decrypt($detail['3'], $this->config->item('pos_encryption_key'));
                    }
                }
            
                return json_encode($newdata_array);
            } else {
                $newdata = $this->datatables->generate('array');
                return $newdata;
            }
           
        }
		
		
		
	function verify_user_mobile($user_id)
	{
		$data=array("otp_verify_status"=>"Y");
		$this->db->where('id',$user_id);          
		$this->db->update('users',$data);
	}

    function agent_details($input) {
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');
            $this->db->select('u.id,u.display_name,u.mobile_no,email');
            $this->db->from('users u');
            $this->db->where('u.status', "Y");
            if (!empty($session_level)) {
                $this->datatables->where('level_' . $session_level, $session_id);
            }
            if (!empty($input['level'])) {
                $this->datatables->where('u.level', $input['level']);
            }
            $this->db->order_by('u.display_name');
            $res = $this->db->get();
            return $res->result_array();
        }

    function update_username($user_id, $username)
	{
		$data=array("username"=>$username, "mobile_no"=>$username);
		$this->db->where('id',$user_id);          
		$this->db->update('users',$data);
	}
    
	function check_username_exists($username)
	{
		$this->db->select("*");
        $this->db->from("users");
        $this->db->where("username", $username);
		$this->db->where("is_deleted", 'N');
        $query = $this->db->get();
        $result_array = $query->row_array();
		if(count($result_array) > 0)
		{
			return true;
		}
		else
		{
			return false;
		}        
	}
	
	function getKycDocument($id)
    {
		$sQuery = "select a.agent_docs_id,a.agent_id,a.large_img_link,a.thum_img_link,b.docs_name,c.category_name
				  from agent_docs_master a left join agent_docs_list b ON a.docs_name=b.agent_docs_list left join doc_category c ON a.docs_cat=c.doc_cat_id where a.agent_id=".$id." and is_deleted=0 and is_status=1 order by a.docs_cat";
		$query = $this->db->query($sQuery);
		return $query->result();
    }
	function auto_activate_user($user_id)
	{
		$data=array("verify_status"=>"Y", "status"=>"Y");
		$this->db->where('id',$user_id);          
		$this->db->update('users',$data);
	}
	
	function save_password($user_id, $pass, $salt)
	{
		$data=array("password"=>$pass, "salt"=>$salt);
		$this->db->where('id',$user_id);	
		if ($this->db->update('users',$data)) {
            return true;
        } else {
            return false;
        }
	}
    
    function active_user()
    {
        $where  =  array("status"=>"Y","is_deleted"=>'N');
        $this->db->select("count(*) as totalusersactive");
        $this->db->from("users");
        if ($where != "") {
            $this->db->where($where);
        }
        $user_result       = $this->db->get();
        $user_result_array = $user_result->result_array();
        
        return $user_result_array;  
    }
    
    function pending_users()
    {
        $where  = array("email_verify_status"=>"N","otp_verify_status" => "N");
        $this->db->select("count(*) as totaluserspending");
        $this->db->from("users");
        if ($where != "") {
            $this->db->where($where);
        }
        $pending_users       = $this->db->get();
        $pending_users_array = $pending_users->result_array();
       
        return $pending_users_array;
    }
    
    function kyc_pending()
    {
        $where  = array("kyc_verify_status"=>"N");
        $this->db->select("count(*) as kycpending");
        $this->db->from("users");
        if ($where != "") {
            $this->db->where($where);
        }
        $kycpending          = $this->db->get();
        $kycpending_array    = $kycpending->result_array();
        
        return $kycpending_array;
    }
    
    function get_users_under_master($column,$user_id)
    {
        $where  = array("'".$column."' is not"  => NULL,$column=>$user_id);
        $this->db->select("id");
        $this->db->from("users");
        if ($where != "") 
        {
            $this->db->where($where);
        }
        $master              = $this->db->get();
        //show($this->db->last_query(), 1);
        $master_array        = $master->result_array();
        
        //show($master_array, 1);
        
        return $master_array;
    }	
    
    function pending_kyc_masters($result)
    {
        $where  = array("kyc_verify_status"=>"N",'is_deleted' => 'N');
        $this->db->select("count(*) as pendingkyc_masters");
        $this->db->from("users");
        if ($where != "")
        {
            $this->db->where($where);
        }
        $this->db->where_in('id',$result);
        $ticketsucess          = $this->db->get();
        //show($this->db->last_query(), 1);
        $ticketsucess_array    = $ticketsucess->result_array();
        return $ticketsucess_array;
    }
    
    function pending_activation_masters($result)
    {
    	$where  = array("verify_status"=>"N","status"=>"N");
        $this->db->select("count(*) as pending_activation_masters");
        $this->db->from("users");
        if ($where != "")
        {
            $this->db->where($where);
        }
        $this->db->where_in('id',$result);
        $ticketsucess          = $this->db->get();
        //show($this->db->last_query(), 1);
        $ticketsucess_array    = $ticketsucess->result_array();
        return $ticketsucess_array;
    }
	
	function save_tpin($user_id, $tpin)
	{
		$data=array("tpin"=>$tpin);	
		$this->db->where('id',$user_id);	
		if ($this->db->update('users',$data)) {
            return true;
        } else {
            return false;
        }
	}

    function retailer_chain_detail () {
      $this->db->select('concat(u.id," ",u1.id," ",u2.id," ",u3.id," ",u4.id) as ids,concat(u.level," ",u1.level," ",u2.level," ",u3.level," ",u4.level) as level');
      $this->db->from('users u');
      $this->db->join('users u1','u1.id = u.level_1');
      $this->db->join('users u2','u2.id = u.level_2');
      $this->db->join('users u3','u3.id = u.level_3');
      $this->db->join('users u4','u4.id = u.level_4');
      $this->db->where('u.id', $this->session->userdata('user_id'));
      $datas = $this->db->get();
      if($datas->num_rows() > 0)
      {
          return  $datas->result_array();
      }
      else
      {
          return false;
      }
    }
	
	/*
     * @Author      : Santosh Warang
     * @function    : Get Count Of MD,AD,Distributor,Retailer
     * @param       : ID level.
     * @detail      : To fetchall the MD,AD,Distributor,Retailer.          
     */
    function getCountUsers($Id,$Lvl) {
        $level = $this->session->userdata('level');
        $sQuery = "SELECT count(id) as TotalVal FROM users WHERE level_".$level." =" . $Id . " and level=".$Lvl." and status ='Y' and is_deleted ='N'";
        $query = $this->db->query($sQuery);
        return $query->row();
    }

    /*
     * @Author      : Rajkumar Shukla
     * @function    : fetch_all_ad
     * @param       : ad role id.
     * @detail      : To fetch all the ad.          
     */

    public function fetch_all_ad($id) {
    	$this->db->select('id,display_name,level,mobile_no');        
		$this->db->where('level_3', $id);
        $this->db->where('role_id', AREA_DISTRIBUTOR_ROLE_ID);
        $this->db->where('status','Y');
        $this->db->where('is_deleted','N');
        $data = $this->db->get($this->table);
        if ($data->num_rows() > 0) {
            return $data->result_array();
        }
		return false;
	}	
		
	function inactivate_user_email($user_id)
    {
	  $data=array("email_verify_status"=>"N");
	  $this->db->where('id',$user_id);          
	  $this->db->update('users',$data);
    }
	
	function update_user_email($user_id, $email)
    {
	  $data=array("email"=>$email);
	  $this->db->where('id',$user_id);          
	  $this->db->update('users',$data);
    }
 
    /*
     * @Author      : Rajkumar Shukla
     * @function    : get_ad_data_by_id
     * @param       : md role id.
     * @detail      : To fetchall the mds gives the data in array format.          
     */
    function get_ad_data_by_id($id) {
        $rollid = AREA_DISTRIBUTOR_ROLE_ID;
        $level = AREA_DISTRIBUTOR_LEVEL;
        $sQuery = "SELECT id,display_name,mobile_no FROM users WHERE level_".$level."=" . $id . " and role_id=" . $rollid . " and status ='Y' and is_deleted ='N' order by display_name";
        $query = $this->db->query($sQuery);
        return $query->result_array();
    }
    /*
     * @Author      : Rajkumar Shukla
     * @function    : get_ad_for_distributor
     * @param       : user id.
     * @detail      : To fetch the ad gives the data in array format.          
     */
    function get_ad_for_distributor($id) {
        $level = AREA_DISTRIBUTOR_LEVEL;
        $sQuery = "SELECT id,display_name,mobile_no,level_".$level." FROM users WHERE id = $id and status ='Y' and is_deleted ='N' order by display_name";
        $query = $this->db->query($sQuery);
        return $query->result_array();
    }
    /*
     * @Author      : Rajkumar Shukla
     * @function    : get_ad_data_by_id
     * @param       : md role id.
     * @detail      : To fetchall the distributor gives the data in array format.          
     */
    function get_distributor_data_by_id($id) {
        $rollid = DISTRIBUTOR;
        $level = $this->session->userdata('level');
        $sQuery = "SELECT id,display_name,mobile_no FROM users WHERE level_".$level." =" . $id . " and role_id=" . $rollid . " and status ='Y' and is_deleted ='N' order by display_name";
        $query = $this->db->query($sQuery);
        return $query->result_array();
    }
	
	/*
     * @Author      : Santosh Warang
     * @function    : getProfileImg
     * @param       : userid doc_category.
     * @detail      : To fetch profile photo.          
     */
    function getProfileImg($Id,$Dcat) {
        $sQuery = "SELECT large_img_link FROM agent_docs_master WHERE agent_id = ".$Id." and docs_cat=".$Dcat." and is_deleted =0";
        $query = $this->db->query($sQuery);
        return $query->row();
    }
    
    function get_parent_id($id, $parent_level) {
    	$returnval = false;
    	$parent_level = 'level_'.$parent_level;    	
    	$this->db->select($parent_level);
    	$this->db->from("users");
    	$this->db->where("id", $id);    	
    	$query = $this->db->get();
    	$result_array = $query->row_array();
    	if(count($result_array) > 0){
    		if(($result_array[$parent_level] != NULL) && ($result_array[$parent_level] != ""))
    		{
    			$returnval = $result_array[$parent_level];
    		}    		
    	}
    	return $returnval;
    }
    
    
    public function fetch_all_cmp() {
    	$this->db->select('id,display_name,level,mobile_no');
    	$this->db->where('level', COMPANY_LEVEL);
    	$this->db->where('role_id', COMPANY_ROLE_ID);
    	$this->db->where('status','Y');
    	$this->db->where('verify_status','Y');
    	$this->db->where('is_deleted','N');
    	$data = $this->db->get($this->table);
    	if ($data->num_rows() > 0) {
    		return $data->result_array();
    	}
    	return false;
    }
    
    
    public function fetch_all_distributors($id) {
    	$this->db->select('id,display_name,level,mobile_no');    	
		$this->db->where('level_4', $id);
    	$this->db->where('role_id', DISTRIBUTOR);
    	$this->db->where('status','Y');
    	$this->db->where('verify_status','Y');
    	$this->db->where('is_deleted','N');
    	$data = $this->db->get($this->table);
    	if ($data->num_rows() > 0) {
    		return $data->result_array();
    	}
    	return false;
    }
    
    public function get_trimax_user_id() {
    	$this->db->select('id');    	
    	$this->db->from("users");
    	$this->db->where('role_id', TRIMAX_ROLE_ID); 
    	$this->db->limit(1);
    	$query = $this->db->get();
    	$result_array = $query->row_array();    	
    	if(count($result_array) > 0)
    	{
    		return $result_array['id'];
    	}    	
    	return false;    	
    }
	
	function verify_user_adhar($user_id, $adharcard, $profile_image)
	{
		$data=array("adharcard"=>$adharcard, "adhar_verify_status"=>"Y", "profile_image"=>$profile_image);
		$this->db->where('id',$user_id);          
		$this->db->update('users',$data);
	}
	
	function get_adhar_status($user_id)
	{
		$this->db->select('adhar_verify_status');    	
    	$this->db->from("users");
    	$this->db->where('id', $user_id); 
    	$this->db->limit(1);
    	$query = $this->db->get();
    	$result_array = $query->row_array();    	
    	if(count($result_array) > 0)
    	{
    		return $result_array['adhar_verify_status'];
    	}    	
    	return false;		
	}
    function  generate_agent_id()
    {
        $last_id = $this->get_last_id();
        
        return $code = "AGT".str_pad(($last_id+1),5,0, STR_PAD_LEFT);
        
    }
    public function getMsrtcRetailer()
    {
        $this->db->select('u.*');
        $this->db->from('users u');
        $this->db->join('retailer_service_mapping rsm','u.id= rsm.agent_id','left');       
        $this->db->where('u.role_id', RETAILER_ROLE_ID);
        $this->db->where('u.status','Y');
        $this->db->where('rsm.service_id',MSRTC_SERVICE_ID);
        $this->db->where('u.verify_status','Y');
        $this->db->where('u.is_deleted','N');
        $data = $this->db->get();
        if ($data->num_rows() > 0) {
            return $data->result_array();
        }
        return false;
    }

    function active_user_list($session_level,$session_id)
    {
        $where  =  array("status"=>"Y","is_deleted"=>'N');
        $this->db->select("count(*) as totalusersactive");
        $this->db->from("users");
        if ($where != "") {
            $this->db->where($where);
        }
        if(!empty($session_level)){
            $this->db->where('level_'.$session_level,$session_id);
        }
        $user_result       = $this->db->get();
        $user_result_array = $user_result->result_array();
        
        return $user_result_array;  
    }

     function pending_users_list($session_level,$session_id)
    {
        $where  = array("email_verify_status"=>"N","otp_verify_status" => "N");
        $this->db->select("count(*) as totaluserspending");
        $this->db->from("users");
        if ($where != "") {
            $this->db->where($where);
        }
        if(!empty($session_level)){
            $this->db->where('level_'.$session_level,$session_id);  
        }
        $pending_users       = $this->db->get();
        $pending_users_array = $pending_users->result_array();
       
        return $pending_users_array;
    }
    
    function kyc_pending_list($session_level,$session_id)
    {
        $where  = array("kyc_verify_status"=>"N", 'is_deleted' => 'N');
        $this->db->select("count(*) as kycpending");
        $this->db->from("users");
        $this->db->where($where);
         if(!empty($session_level)){
            $this->db->where('level_'.$session_level,$session_id);
        }
        $kycpending          = $this->db->get();
        $kycpending_array    = $kycpending->result_array();
        return $kycpending_array;
    }

    public function get_wallet_comment() {
        $this->db->select('comment');
        $this->db->distinct('comment');
        $this->db->like('comment','waybill');
        $this->db->from("wallet_trans");
        
        $comment = $this->db->get();
        $comment_arr = $comment->result_array();
        
        return $comment_arr;
    }

    public function fetch_all_company_users() {
        $this->db->select('id');
        $this->db->where(array('is_deleted' => 'N','status' => 'Y', 'role_id' => COMPANY_ROLE_ID));
        $this->db->from("users");
        $comp_data = $this->db->get();
        $comp_data_arr = $comp_data->result_array();
        return $comp_data_arr;
    }

    public function fetch_all_technical_users() {
        $this->datatables->select("1,
                                    concat(u.first_name,' ',u.last_name) as user_name,
                                    ur.role_name as role_name,
                                    u.mobile_no,
                                    u.email,
                                    (CASE when u.email_verify_status = 'N' then 'Not Verified' when u.email_verify_status = 'Y' then 'Verified' Else u.email_verify_status END) as email_verify_status,
                                    DATE_FORMAT(u.created_date,'%d-%m-%Y %h:%i:%s') as created_date"
                                );
        $this->datatables->from("users u");
        
        $this->datatables->where('role_id !=',RETAILER_ROLE_ID);
        $this->datatables->where('role_id !=',DISTRIBUTOR);
        $this->datatables->where('role_id !=',AREA_DISTRIBUTOR_ROLE_ID);
        $this->datatables->where('role_id !=',MASTER_DISTRIBUTOR_ROLE_ID);
        $this->datatables->where('role_id !=',COMPANY_ROLE_ID);
        $this->datatables->where('role_id !=',TRIMAX_ROLE_ID);
        $this->datatables->where('role_id !=',SUPERADMIN_ROLE_ID);
        $this->datatables->join('user_roles ur', 'u.role_id=ur.id', 'inner');
       
        $newdata = $this->datatables->generate('json');
        $newdata_array = json_decode($newdata);

        if (!empty($newdata_array->data)) {                
            foreach ($newdata_array->data as &$detail) {
                $detail['3'] = rokad_decrypt($detail['3'], $this->config->item('pos_encryption_key'));
                $detail['4'] = rokad_decrypt($detail['4'], $this->config->item('pos_encryption_key'));
            }
        }

        return json_encode($newdata_array);

    }
}
