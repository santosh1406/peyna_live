<?php

//Created by harshada kulkarni on 24-07-2018
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Utilities_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    // added by harshada kulkarni on 24-07-2018
    public function getMobileOperators($char = "", $type = "") {

        $queryStr = "select *"
                . " FROM operators_urls"
                . " WHERE operator LIKE '%$char%'"
                . " AND status = 'Y'"
                . " AND type = '$type'"
                . " AND api_operator = 1";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        $nCount = count($result);

        if ($nCount > 0) {
            for ($i = 0; $i < $nCount; $i++) {
                $aResult[$i]['label'] = $result[$i]['operator'];
                $aResult[$i]['name'] = $result[$i]['operator'];
                $aResult[$i]['value'] = $result[$i]['id'];
                $aResult[$i]['id'] = $result[$i]['id'];
            }
        } else {
            $aResult[0]['label'] = 'No result found';
            $aResult[0]['name'] = 'No result found';
            $aResult[0]['value'] = '';
            $aResult[0]['id'] = '';
        }
        //$aResult_json = json_encode($aResult);
        return $aResult;
    }

    // added by harshada kulkarni on 24-07-2018
    public function operatorDetails($operator = "", $type = "") {

        $queryStr = "select *"
                . " FROM operators_urls"
                . " WHERE operator = '" . $operator . "'"
                . " AND status = 'Y'"
                . " AND type = '" . $type . "'"
                . " AND api_operator = 1";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        return $result;
    }

    // added by harshada kulkarni on 18-08-2018
    public function getUserWalletDetails($user_id = "") {
        $queryStr = "select amt"
                . " FROM wallet"
                . " WHERE user_id = '" . $user_id . "'"
                . " AND status = 'Y'"
                . " AND is_deleted = 'N'";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        return $result;
    }

    // added by harshada kulkarni on 18-08-2018
    public function operatorDetailsByID($operatorId, $type) {

        $queryStr = "select *"
                . " FROM operators_urls"
                . " WHERE id = '" . $operatorId . "'"
                . " AND status = 'Y'"
                . " AND type = '" . $type . "'"
                . " AND api_operator = 1";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        return $result;
    }

    // added by harshada kulkarni on 18-08-2018
    public function updateUserWallet($user_id = "", $updated_wallet_amt = "", $wallet_tran_id = "") {
        $queryStr = "UPDATE wallet 
        SET amt = '" . $updated_wallet_amt . "'
        , last_transction_id = '" . $wallet_tran_id . "' 
        , updated_date = '" . date("Y-m-d H:i:s") . "' 
        WHERE user_id = '" . $user_id . "'";

        $query = $this->db->query($queryStr);

        $queryStr1 = 'SELECT id from wallet where user_id = "' . $user_id . '"';
        $insert_id = $this->db->query($queryStr1);
        $insert_id = $insert_id->result_array();
        return $insert_id;
    }

    // added by harshada kulkarni on 18-08-2018
    public function saveTransactionData($data = "") {
        $this->db->insert('recharge_transaction', $data);
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $data, 'JRI Recharge - Save Transaction Data');

    }

    public function saveJRIData($data = "") {
        $this->db->insert('jri_transaction_details', $data);
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $data, 'JRI Recharge - Save JRI');

    }

    // added by harshada kulkarni on 15-09-2018
    public function getRetailerServicesCommission($user_id = "", $service_id = "",$created_by ="") {
        $this->db->select('u.id as user_id,u.agent_code,u.level_1,u.level_2,u.level_3,u.level_4,u.level_5,rsm.service_id as service_id,sc.id as commission_id,sc.values,sc.total_commission,sc.created_by');
        $this->db->from("users u");
        $this->db->join('retailer_service_mapping rsm', 'u.id= rsm.agent_id', 'left');
        $this->db->join('services_commissions sc', 'rsm.service_id = sc.service_id', 'left');
        $this->db->where('rsm.agent_id =', $user_id);
        $this->db->where('rsm.service_id =', $service_id);
        if(!empty($created_by)){
         $this->db->where('sc.created_by =', $created_by);   
        }
        $this->db->where('sc.status', 'Y');
        $this->db->where('sc.from >=', date('Y-m-d H:i:s'));
        $this->db->where('sc.till <=', date('Y-m-d H:i:s'));
        $user_result = $this->db->get();
        $user_result_array = $user_result->result_array();
        if (empty($user_result_array)) {
            $this->db->select('u.id as user_id,u.agent_code,u.level_1,u.level_2,u.level_3,u.level_4,u.level_5,rsm.service_id as service_id,sc.id as commission_id,sc.values,sc.total_commission,sc.created_by');
            $this->db->from("users u");
            $this->db->join('retailer_service_mapping rsm', 'u.id= rsm.agent_id', 'left');
            $this->db->join('services_commissions sc', 'rsm.service_id = sc.service_id', 'left');
            $this->db->where('rsm.agent_id =', $user_id);
            $this->db->where('rsm.service_id =', $service_id);
            if(!empty($created_by)){
         	$this->db->where('sc.created_by =', $created_by);   
            }
            $this->db->where('sc.status', 'Y');
            $this->db->where('sc.default_status', 'Y');
            $user_result = $this->db->get();
            $user_result_array = $user_result->result_array();
        }
        return $user_result_array;
    }

    public function getRetailerServicesCommissionForRecharges($user_id = "", $service_id = "", $sub_service_id, $created_by ="") {
        //show($user_id);
        //show($service_id);
        //show($sub_service_id, 1);

        $this->db->select('u.id as user_id,u.agent_code,u.level_1,u.level_2,u.level_3,u.level_4,u.level_5,rsm.service_id as service_id,sc.id as commission_id,sc.values,sc.total_commission,sc.created_by');
        $this->db->from("users u");
        $this->db->join('retailer_service_mapping rsm', 'u.id= rsm.agent_id', 'left');
        $this->db->join('services_commissions sc', 'rsm.service_id = sc.service_id', 'left');
        $this->db->where('rsm.agent_id =', $user_id);
        $this->db->where('rsm.service_id =', $service_id);
        $this->db->where('sc.sub_service_id =', $sub_service_id);
	if(!empty($created_by)){
         $this->db->where('sc.created_by =', $created_by);   
        }
        $this->db->where('sc.status', 'Y');
        $this->db->where('sc.from >=', date('Y-m-d H:i:s'));
        $this->db->where('sc.till <=', date('Y-m-d H:i:s'));
        $user_result = $this->db->get();
        $user_result_array = $user_result->result_array();
        if (empty($user_result_array)) {
            $this->db->select('u.id as user_id,u.agent_code,u.level_1,u.level_2,u.level_3,u.level_4,u.level_5,rsm.service_id as service_id,sc.id as commission_id,sc.values,sc.total_commission,sc.created_by');
            $this->db->from("users u");
            $this->db->join('retailer_service_mapping rsm', 'u.id = rsm.agent_id', 'left');
            $this->db->join('services_commissions sc', 'rsm.service_id = sc.service_id', 'left');
            $this->db->where('rsm.agent_id =', $user_id);
            $this->db->where('sc.service_id =', $service_id);
            $this->db->where('sc.sub_service_id =', $sub_service_id);
            if(!empty($created_by)){
         $this->db->where('sc.created_by =', $created_by);   
        }
            $this->db->where('sc.status', 'Y');
            $this->db->where('sc.default_status', 'Y');
            $user_result = $this->db->get();
            $user_result_array = $user_result->result_array();
        }

        // show($this->db->last_query(),1);
        return $user_result_array;
    }

    // added by harshada kulkarni on 09-10-2018
    public function saveVasCommissionData($data = "") {
        $this->db->insert('vas_commission_detail', $data);
    }

    /**
     * @description - To validate data 
     * @param array of data
     * @added by Sachin on 14-08-2018
     * @return int array
     */
    public function get_validation($type, $operatorId) {
        switch ($type) {
            case 'landline':
                if ($operatorId == 16 || $operatorId == 17) { //BSNL Landline-Online or BSNL Landline-Retail
                    $config = array(
                        array('field' => 'operator', 'label' => 'Landline Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter landline operator.')),
                        array('field' => 'number', 'label' => 'Phone Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check phone number.')),
                        array('field' => 'account_number', 'label' => 'Account Number', 'rules' => 'trim|required|numeric|min_length[10]|max_length[10]|xss_clean', 'errors' => array('required' => 'Please check account number.')),
                        array('field' => 'authenticator', 'label' => 'Service Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select service type.'))
                    );
                } else if ($operatorId == 10 || $operatorId == 14) {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Landline Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter landline operator.')),
                        array('field' => 'number', 'label' => 'Phone Number', 'rules' => 'trim|required|numeric|exact_length[8]|xss_clean', 'errors' => array('required' => 'Please check phone number.')),
                        array('field' => 'account_number', 'label' => 'Account Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check account number.'))
                    );
                }
                break;
            case 'electricity':
                if ($operatorId == 18) { //Assam Power Distribution Company Ltd (APDCL)- RAPDR
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[9]|max_length[11]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 19) { //Assam Power Distribution Company Ltd (APDCL)-NON RAPDR
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[9]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 20) { //'Bangalore Electricity Supply Company'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 21) { //'Bharatpur Electricity Services Ltd'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'K Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check K number.'))
                    );
                } else if ($operatorId == 33) { //'Bikaner Electricity Supply Limited (BkESL)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'K Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check K number.'))
                    );
                } else if ($operatorId == 22) { //'Brihan Mumbai Electric Supply and Transport Undertaking'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 23) { //'BSES Rajdhani'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[9]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 24) { ////'BSES Rajdhani'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[9]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 34) { //Calcutta Electricity Supply Ltd (CESC Ltd)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[11]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 35) { //'Chhattisgarh State Electricity Board'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Business Partner Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check business partner number.'))
                    );
                } else if ($operatorId == 36) { //'Chamundeshwari Electricity Supply Corp Ltd (CESCOM)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check consumer number.')),
                        array('field' => 'account_number', 'label' => 'Account Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check account number.'))
                    );
                } else if ($operatorId == 37) { //'Daman and Diu Electricity Department'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Account Number', 'rules' => 'trim|required|numeric|min_length[1]|max_length[6]|xss_clean', 'errors' => array('required' => 'Please check Account number.'))
                    );
                } else if ($operatorId == 38) { //'Dakshin Gujarat Vij Company Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[5]|max_length[11]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 39) { //'Dakshin Haryana Bijli Vitran Nigam'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please check consumer number.')),
                        array('field' => 'account_number', 'label' => 'Account Number', 'rules' => 'trim|required|exact_length[10]|numeric|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 40) { //'DNH Power Distribution Company Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Service Connection Number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please check service connection number.'))
                    );
                } else if ($operatorId == 41) { //'Eastern Power Distribution Company of Andhra Pradesh Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Service Number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please check serivce number.'))
                    );
                } else if ($operatorId == 42) { //'Gulbarga Electricity Supply Company Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 43) { //'Hubli Electricity Supply Company Ltd (HESCOM)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[5]|max_length[10]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 44) { //'India Power Corporation Limited - Bihar'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 45) { //'India Power Corporation - West Bengal'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|min_length[10]|max_length[12]|numeric|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 46) { //'Jamshedpur Utilities and Services Company Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Business Partner Number', 'rules' => 'trim|required|min_length[6]|max_length[10]|numeric|xss_clean', 'errors' => array('required' => 'Please check business partner number.'))
                    );
                } else if ($operatorId == 47) { //'Jaipur and Ajmer Viyut Vitran Nigam'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'K Number', 'rules' => 'trim|required|exact_length[12]|numeric|xss_clean', 'errors' => array('required' => 'Please k consumer number.'))
                    );
                } else if ($operatorId == 48) { //'Jodhpur Vidyut Vitran Nigam Ltd'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'K Number', 'rules' => 'trim|required|exact_length[12]|numeric|xss_clean', 'errors' => array('required' => 'Please k consumer number.'))
                    );
                } else if ($operatorId == 49) { //'Jharkhand Bijli Vitran Nigam Limited (JBVNL)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please check consumer number.')),
                        array('field' => 'account_number', 'label' => 'Subdivision Code', 'rules' => 'trim|required|min_length[1]|max_length[3]|numeric|xss_clean', 'errors' => array('required' => 'Please check subdivision code.'))
                    );
                } else if ($operatorId == 50) { //'Kota Electricity Distribution Ltd'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'K Number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please check k number.'))
                    );
                } else if ($operatorId == 51) { //'Madhya Pradesh Paschim Kshetra Vidyut Vitaran Co. Ltd -Indore'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'IVRS Number', 'rules' => 'trim|required|numeric|min_length[2]|min_length[30]|xss_clean', 'errors' => array('required' => 'Please check IVRS number.'))
                    );
                } else if ($operatorId == 52) { //'Meghalaya Power Distribution Corporati on Ltd'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[1]|min_length[12]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 53) { //'Madhya Gujarat Vij Company Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[5]|min_length[11]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 54) { //'MSEDC Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[5]|min_length[11]|xss_clean', 'errors' => array('required' => 'Please check consumer number.')),
                        array('field' => 'account_number', 'label' => 'Billing Unit', 'rules' => 'trim|required|exact_length[4]|numeric|xss_clean', 'errors' => array('required' => 'Please check billing unit.')),
                        array('field' => 'authenticator', 'label' => 'Processing Cycle', 'rules' => 'trim|required|exact_length[2]|numeric|xss_clean', 'errors' => array('required' => 'Please check Processing Cycle.'))
                    );
                } else if ($operatorId == 55) { //'Muzaffarpur Vidyut Vitran Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 56) { //'North Delhi Power Limited (Tata Power - DDL)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[11]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 57) { //'North Bihar Power Distribution Company Ltd'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[9]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 58) { //'Noida Power Company Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 59) { //'ODISHA Discoms(B2C)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 60) { //'ODISHA Discoms(B2B)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 61) { //'Paschim Gujarat Vij Company Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[5]|min_length[11]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 62) { //'Punjab State Power Corporation Ltd (PSPCL)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Account Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 63) { //'Reliance Energy (Mumbai)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[2]|xss_clean', 'errors' => array('required' => 'Please check consumer number.')),
                        array('field' => 'account_number', 'label' => 'Cycle Number', 'rules' => 'trim|required|exact_length[2]|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.'))
                    );
                } else if ($operatorId == 64) { //'Rajasthan Vidyut Vitran Nigam Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'K Number', 'rules' => 'trim|required|numeric|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check K number.'))
                    );
                } else if ($operatorId == 65) { //'South Bihar Power Distribution Company Ltd'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'CA Number', 'rules' => 'trim|required|numeric|min_length[9]|max_length[11]|xss_clean', 'errors' => array('required' => 'Please check CA number.'))
                    );
                } else if ($operatorId == 66) { //'SNDL Nagpur'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[10]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 67) { //'Southern Power Distr of Andhra Pradesh'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Service Number', 'rules' => 'trim|required|numeric|min_length[9]|max_length[13]|xss_clean', 'errors' => array('required' => 'Please check service number.'))
                    );
                } else if ($operatorId == 68) { //'Torrent Power'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Service Number', 'rules' => 'trim|required|numeric|min_length[9]|max_length[13]|xss_clean', 'errors' => array('required' => 'Please check serivce number.')),
                        array('field' => 'account_number', 'label' => 'City Name', 'rules' => 'trim|required|min_length[1]|max_length[30]|xss_clean', 'errors' => array('required' => 'Please check city name.'))
                    );
                } else if ($operatorId == 69) { //'Tripura State Electricity Corporation Ltd'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|min_length[1]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 70) { //'Tamil Nadu Electricity Board (TNEB)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|min_length[9]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 71) { //'TP Ajmer Distribution Ltd'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'K Number', 'rules' => 'trim|required|numeric|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 72) { //'Tata Power – Mumbai'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'CA Number', 'rules' => 'trim|required|numeric|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check CA number.'))
                    );
                } else if ($operatorId == 73) { //'Uttar Gujarat Vij Company Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[5]|max_length[11]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 74) { //'Uttar Haryana Bijli Vitran Nigam'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Account Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check account number.'))
                    );
                } else if ($operatorId == 75) { //'Uttarakhand Power Corporation Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Service Connection Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check service connection number.'))
                    );
                } else if ($operatorId == 76) { //'Uttar Pradesh Power Corp Ltd (UPPCL) - URBAN'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|min_length[10]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check service connection number.'))
                    );
                } else if ($operatorId == 77) { //'Uttar Pradesh Power Corp Ltd (UPPCL) - RURAL'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check service connection number.'))
                    );
                } else if ($operatorId == 78) { //'West Bengal State Electricity'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|exact_length[9]|xss_clean', 'errors' => array('required' => 'Please check consumer number.')),
                        array('field' => 'account_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
                    );
                } else {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Electricity Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter electricity operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                }
                break;
            case 'gas':
                if ($operatorId == 25) { //'ADANI GAS'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 26) { //'Gujarat Gas company Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Service Number', 'rules' => 'trim|required|numeric|min_length[1]|max_length[15]|xss_clean', 'errors' => array('required' => 'Please check serivce number.'))
                    );
                } else if ($operatorId == 79) { //'IGL (Indraprasth Gas Limited)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 80) { //'Mahanagar Gas Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Customer Account Number', 'rules' => 'trim|required|numeric|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check customer account number.')),
                        array('field' => 'account_number', 'label' => 'Bill Group Number', 'rules' => 'trim|required|min_length[1]|xss_clean', 'errors' => array('required' => 'Please check bill group number.'))
                    );
                } else if ($operatorId == 81) { //'Haryana City gas'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'CRN Number', 'rules' => 'trim|required|numeric|min_length[8]|max_length[12]|xss_clean', 'errors' => array('required' => 'Please check CRN number.'))
                    );
                } else if ($operatorId == 82) { //'Siti Energy'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'ARN Number', 'rules' => 'trim|required|numeric|min_length[7]|max_length[9]|xss_clean', 'errors' => array('required' => 'Please check ARN number.'))
                    );
                } else if ($operatorId == 83) { //'Tripura Natural Gas Company Ltd'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[1]|max_length[20]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 84) { //'Sabarmati Gas Limited (SGL)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Customer ID', 'rules' => 'trim|required|numeric|exact_length[12]|xss_clean', 'errors' => array('required' => 'Please check customer ID.'))
                    );
                } else if ($operatorId == 85) { //'Unique Central Piped Gases Pvt Ltd (UCPGPL)'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 86) { //'Vadodara Gas Limited'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|exact_length[7]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter gas operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                }
                break;
            case 'water':
                if ($operatorId == 27) { //'UIT Bhiwadi'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'Customer ID', 'rules' => 'trim|required|numeric|min_length[3]|max_length[20]|xss_clean', 'errors' => array('required' => 'Please check customer ID.'))
                    );
                } else if ($operatorId == 28) { //'Uttarakhand Jal Sansthan(B2B)-Retail'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|numeric|min_length[7]|max_length[22]|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                } else if ($operatorId == 29) { //'Delhi Jal Board-Retail'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'K Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check K number.'))
                    );
                } else if ($operatorId == 30) { //'Municipal Corporation of Gurugram'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'K Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check k number.'))
                    );
                } else {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter water operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                }
                break;
            case 'broadband':
                if ($operatorId == 31) { //'Connect Broadband-Retail'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter broadband operator.')),
                        array('field' => 'number', 'label' => 'Directory Number', 'rules' => 'trim|required|min_length[4]|xss_clean', 'errors' => array('required' => 'Please check directory number.'))
                    );
                } else if ($operatorId == 32) { //'Hathway Broadband - Retail'
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter broadband operator.')),
                        array('field' => 'number', 'label' => 'Customer ID', 'rules' => 'trim|required|numeric|min_length[9]|max_length[15]|xss_clean', 'errors' => array('required' => 'Please check customer ID.'))
                    );
                } else {
                    $config = array(
                        array('field' => 'operator', 'label' => 'Gas Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter broadband operator.')),
                        array('field' => 'number', 'label' => 'Consumer Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check consumer number.'))
                    );
                }
                break;
        }
        return $config;
    }

    public function getDthOperators($char) {

        $queryStr = "select *"
                . " FROM operators_urls"
                . " WHERE operator LIKE '%$char%'"
                . " AND type = 'DTH'"
                . " AND status = 'Y'"
                . " AND api_operator = 1";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        $nCount = count($result);

        if ($nCount > 0) {
            for ($i = 0; $i < $nCount; $i++) {
                $aResult[$i]['label'] = $result[$i]['operator'];
                $aResult[$i]['name'] = $result[$i]['operator'];
                $aResult[$i]['value'] = $result[$i]['id'];
                $aResult[$i]['id'] = $result[$i]['id'];
            }
        } else {
            $aResult[0]['label'] = 'No result found';
            $aResult[0]['name'] = 'No result found';
            $aResult[0]['value'] = '';
            $aResult[0]['id'] = '';
        }
        $aResult_json = json_encode($aResult);
        echo $aResult_json;
    }

    public function dthOperator($operator) {

        $queryStr = "select *"
                . " FROM operators_urls"
                . " WHERE operator = '" . $operator . "'"
                . " AND status = 'Y'"
                . " AND api_operator = 1";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        return $result;
    }

    //Added by prabhat pal on 22-11-2018
     public function saveTSODTHData($data) {
        $this->db->insert('tso_transaction_details', $data);
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'tso_dth_recharge.log', $data, 'TSO DTH Recharge - Save TSO');

    }

    //Added by Vaidehi
    public function commission_from($user_id){
         $queryStr = "select id,level_3"
                . " FROM users u"
                . " WHERE id = '" . $user_id . "'"
                . " AND role_id = '" . RETAILER_ROLE_ID . "'";
         
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        return $result;
    }
    
     public function get_commission_level() {
        $queryStr = "select level"
                . " FROM commission_distribute_level"
                . " WHERE status = 'Y'";
        $query = $this->db->query($queryStr);
        $result = $query->row();
        return $result;
    }
    
     public function commission_setby_level($user_id,$level=''){
               
         $queryStr = "select id,level_$level as level"
                . " FROM users u"
                . " WHERE id = '" . $user_id . "'"
                . " AND role_id = '" . RETAILER_ROLE_ID . "'";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        //last_query(1);
        return $result;
           
    }
    
     public function update_comm_status($vas_id){
         $update_data = array(
            'comm_dist_status' => 'Y'
        ); 
        
        $this->db->where('id', $vas_id);
        return $this->db->update('vas_commission_detail', $update_data);
    }
    
    public function serviceRetailerCommissionDistributionForePay($input = "", $trans_ref_no = "") {
        $user_id = $input['user_id'];
        $code = EPAY_SERVICE_NAME;
        $service_id = EPAY_SERVICE_ID;

        $LcSqlStr = "SELECT * from services_commissions 
        where status = 'Y'";
        $LcSqlStr .= " AND `values` NOT LIKE '{\"Trimax\":\"0\",\"Rokad\":\"0\",\"MD\":\"0\",\"AD\":\"0\",\"Distributor\":\"0\",\"Retailer\":\"0\"}'";
        $LcSqlStr .= " AND `commission_name` LIKE '%" . str_replace(" ", "", $code) . "%'";
        $LcSqlStr .= " AND service_id = '" . $service_id . "'";
        ;
         log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ePayLater_vas_comm_log.log', $LcSqlStr, 'epay com  query');
        $query = $this->db->query($LcSqlStr);
        $new = $query->result();

        if ($new) {
            $row = $new[0];
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ePayLater_vas_comm_log.log', $row, 'row');
            $getRetailerServicesCommission = $this->Utilities_model->getRetailerServicesCommissionForRecharges($user_id, $row->service_id, $row->sub_service_id, '');
        }
//    show($getRetailerServicesCommission,1);
        if ($getRetailerServicesCommission) {

            $agent_id = $getRetailerServicesCommission[0]['user_id'];
            $agent_code = $getRetailerServicesCommission[0]['agent_code'];
            $distributor_id = $getRetailerServicesCommission[0]['level_5'];
            $Area_dis_id = $getRetailerServicesCommission[0]['level_4'];
            $master_dis_id = $getRetailerServicesCommission[0]['level_3'];
            $company_id = $getRetailerServicesCommission[0]['level_2'];
            $trimax_id = $getRetailerServicesCommission[0]['level_1'];
            $service_id = $getRetailerServicesCommission[0]['service_id'];
            $commission_id = $getRetailerServicesCommission[0]['commission_id'];
            $total_commission = $getRetailerServicesCommission[0]['total_commission'];

            $values[0] = json_decode($getRetailerServicesCommission[0]['values']);

            $trimax_commission = '0';
            $company_commission = '0';
            $md_commission = '0';
            $ad_commission = '0';
            $dist_commission = '0';
            $retailer_commission = '0';
	    $with_gst = $without_gst = 0;
            if (!empty($values)) {
                $trimax_commission = round($values[0]->Trimax, 2);
                $company_commission = round($values[0]->Rokad, 2);
                $md_commission = round($values[0]->MD, 2);
                $ad_commission = round($values[0]->AD, 2);
                $dist_commission = round($values[0]->Distributor, 2);
                $retailer_commission = round($values[0]->Retailer, 2);
                $recharge_amount = $input['recharge_amount']; //transaction_amt              

                $commission_percent = !empty($total_commission) ? $total_commission : '0';

                $trimax_cal = calculateGstTds($recharge_amount, $commission_percent);
                $earnings = $trimax_cal['earnings']; //trimax_earnings
		log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ePayLater_vas_comm_log.log', $earnings, '$earnings');
                $gst = $trimax_cal['gst']; //trimax_gst                            
                $tds = $trimax_cal['tds']; //trimax_tds
                $final_amount = $trimax_cal['final_amount']; //trimax_final_amt                                                        

                $rokad_trimax_cal = calculateGstTds($earnings, $trimax_commission);
                $rokad_trimax_amt = $rokad_trimax_cal['earnings']; //rokad_trimax_amt
		log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ePayLater_vas_comm_log.log',$rokad_trimax_cal['earnings'], 'rok_tr_ear'); 
		 $with_gst = $with_gst + $rokad_trimax_cal['earnings'];                             
                $rokad_trimax_cal_with_tds_gst = calculateGstTds($final_amount, $trimax_commission);
                $rokad_trimax_amt_with_tds_gst = $rokad_trimax_cal_with_tds_gst['earnings']; //rokad_trimax_amt_with_tds_gst                            

                $company_cal = calculateGstTds($earnings, $company_commission);
                $company_amt = $company_cal['earnings']; //company_amt    
		log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ePayLater_vas_comm_log.log', $company_cal['earnings'], 'comp ear'); 
 $with_gst = $with_gst + $company_cal['earnings'];                        
                $company_cal_with_tds_gst = calculateGstTds($final_amount, $company_commission);
                $company_amt_with_tds_gst = $company_cal_with_tds_gst['earnings']; //company_amt_with_tds_gst                        
		
                $rd_cal = calculateGstTds($earnings, $md_commission);
                $rd_amt = $rd_cal['earnings']; //rd_amt    
		log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ePayLater_vas_comm_log.log', $rd_cal['earnings'], 'rd ear');         
		 $with_gst = $with_gst + $rd_cal['earnings'];                   
                $rd_cal_with_tds_gst = calculateGstTds($final_amount, $md_commission);
                $rd_amt_with_tds_gst = $rd_cal_with_tds_gst['earnings']; //rd_amt_with_tds_gst

                $dd_cal = calculateGstTds($earnings, $ad_commission);
                $dd_amt = $dd_cal['earnings']; //dd_amt      
		log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ePayLater_vas_comm_log.log', $dd_cal['earnings'], 'dd ear'); 
 $with_gst = $with_gst + $dd_cal['earnings'];                         
                $dd_cal_with_tds_gst = calculateGstTds($final_amount, $ad_commission);
                $dd_amt_with_tds_gst = $dd_cal_with_tds_gst['earnings']; //dd_amt_with_tds_gst

                $ex_cal = calculateGstTds($earnings, $dist_commission);
                $ex_amt = $ex_cal['earnings']; //ex_amt    
		log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ePayLater_vas_comm_log.log', $ex_cal['earnings'], 'ex ear');   
$with_gst = $with_gst + $ex_cal['earnings'];                       
                $ex_cal_with_tds_gst = calculateGstTds($final_amount, $dist_commission);
                $ex_amt_with_tds_gst = $ex_cal_with_tds_gst['earnings']; //ex_amt_with_tds_gst

                $sa_cal = calculateGstTds($earnings, $retailer_commission);
                $sa_amt = $sa_cal['earnings']; //sa_amt       
		log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ePayLater_vas_comm_log.log', $sa_cal['earnings'], 'sa ear');    
  $with_gst = $with_gst + $sa_cal['earnings'];                    
                $sa_cal_with_tds_gst = calculateGstTds($final_amount, $retailer_commission);
                $sa_amt_with_tds_gst = $sa_cal_with_tds_gst['earnings']; //sa_amt_with_tds_gst              

		log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ePayLater_vas_comm_log.log', $with_gst, '$with_gst');
		  $remaining_commission = round($with_gst, 2) - round($earnings, 2);
		log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ePayLater_vas_comm_log.log', $earnings, '$earnings');
		log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ePayLater_vas_comm_log.log', $remaining_commission, '$remaining_commission');
            }

            $comm_distribution_array['user_id'] = $agent_id;
            $comm_distribution_array['service_id'] = $service_id;
            $comm_distribution_array['commission_id'] = $commission_id;
            $comm_distribution_array['transaction_no'] = $trans_ref_no;
            $comm_distribution_array['transaction_amt'] = $recharge_amount;
            $comm_distribution_array['service_comm_percent'] = SERVICE_COMMISSION;
            $comm_distribution_array['trimax_comm_percent'] = $trimax_commission;
            $comm_distribution_array['gst_percentage'] = GST_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['tds_percentage'] = TDS_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['trimax_earnings'] = $earnings;
            $comm_distribution_array['trimax_gst'] = $gst;
            $comm_distribution_array['trimax_tds'] = $tds;
            $comm_distribution_array['trimax_final_amt'] = $final_amount;
            $comm_distribution_array['rokad_trimax_per'] = $trimax_commission;
            $comm_distribution_array['rokad_trimax_amt'] = $rokad_trimax_amt;
            $comm_distribution_array['rokad_trimax_amt_with_tds_gst'] = $rokad_trimax_amt_with_tds_gst;
            $comm_distribution_array['company_per'] = $company_commission;
            $comm_distribution_array['company_amt'] = $company_amt;
            $comm_distribution_array['company_amt_with_tds_gst'] = $company_amt_with_tds_gst;
            $comm_distribution_array['rd_per'] = $md_commission;
            $comm_distribution_array['rd_amt'] = $rd_amt;
            $comm_distribution_array['rd_amt_with_tds_gst'] = $rd_amt_with_tds_gst;
            $comm_distribution_array['dd_per'] = $ad_commission;
            $comm_distribution_array['dd_amt'] = $dd_amt;
            $comm_distribution_array['dd_amt_with_tds_gst'] = $dd_amt_with_tds_gst;
            $comm_distribution_array['ex_per'] = $dist_commission;
            $comm_distribution_array['ex_amt'] = $ex_amt;
            $comm_distribution_array['ex_amt_with_tds_gst'] = $ex_amt_with_tds_gst;
            $comm_distribution_array['sa_per'] = $retailer_commission;
            $comm_distribution_array['sa_amt'] = $sa_amt;
            $comm_distribution_array['sa_amt_with_tds_gst'] = $sa_amt_with_tds_gst;
            $comm_distribution_array['status'] = 'Y';
            $comm_distribution_array['created_by'] = $user_id;
$comm_distribution_array['commission_difference'] = $remaining_commission;
            $comm_distribution_array['created_date'] = date('Y-m-d H:i:s');

            $id = $this->Utilities_model->saveVasCommissionData($comm_distribution_array);
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ePayLater_vas_comm_log.log', $comm_distribution_array, 'Commission Distribution Response');
            return $comm_distribution_array;
        }
    }

    //added by lilawati for receipt
    public function getTransactionReceiptForPrepaid($user_id,$type){
        if($type=="jri") {
            $result = $this->db->query("select j.*, concat(u.first_name,' ',u.last_name) as agent_name,u.agent_code from jri_transaction_details j inner join users u on j.created_by=u.id where j.created_by='".$user_id."' and j.isPostpaid ='N'  and j.request_type='Mobile Recharge' order by id desc limit 1");
        } else if($type=="tso"){
            $result = $this->db->query("select t.*, concat(u.first_name,' ',u.last_name) as agent_name,u.agent_code from tso_transaction_details t inner join users u on t.created_by=u.id where t.created_by='".$user_id."' and t.plan_type='Prepaid Mobile' order by id desc limit 1");
        }
        return $result->row();
   }

    //added by lilwati for postpaid receipt data
    public function getTransactionReceiptForPostpaid($user_id,$type){
        if($type=="jri") {
            $result = $this->db->query("select j.*, concat(u.first_name,' ',u.last_name) as agent_name,u.agent_code from jri_transaction_details j inner join users u on j.created_by=u.id where j.created_by='".$user_id."' and j.isPostpaid ='Y' and j.request_type='Mobile Recharge' order by id desc limit 1");
        } else if($type=="tso"){
            $result = $this->db->query("select t.*, concat(u.first_name,' ',u.last_name) as agent_name,u.agent_code from tso_transaction_details t inner join users u on t.created_by=u.id where t.created_by='".$user_id."' and t.plan_type='Postpaid Mobile' order by id desc limit 1");
        }
        return $result->row();
   }

   //added by lilwati for dth receipt data
    public function getTransactionReceiptForDth($user_id,$type){
        if($type=="jri") {
            $result = $this->db->query("select j.*, concat(u.first_name,' ',u.last_name) as agent_name,u.agent_code from jri_transaction_details j inner join users u on j.created_by=u.id where j.created_by='".$user_id."' and j.request_type='DTH Recharge' order by id desc limit 1");
        } else if($type=="tso"){
            $result = $this->db->query("select t.*, concat(u.first_name,' ',u.last_name) as agent_name,u.agent_code from tso_transaction_details t inner join users u on t.created_by=u.id where t.created_by='".$user_id."' and t.request_type='DTH Rechagre' order by id desc limit 1");
        }        
        return $result->row();
   }

   //added by lilwati for datacard receipt data
    public function getTransactionReceiptForDatacard($user_id){
        $result = $this->db->query("select j.*, concat(u.first_name,' ',u.last_name) as agent_name,u.agent_code from jri_transaction_details j inner join users u on j.created_by=u.id where j.created_by='".$user_id."' and j.request_type='Datacard Recharge' order by id desc limit 1");
        return $result->row();
   }

   public function getTransactionDetails($input){
    $result = $this->db->query("select * from jri_transaction_details where systemReference='".$system_reference."' limit 1");
    return $result->row();
   }

    // added by lilawati karale 
    public function getUserWalletData($user_id = "") {
        $queryStr = "select *"
                . " FROM wallet"
                . " WHERE user_id = '" . $user_id . "'"
                . " AND status = 'Y'"
                . " AND is_deleted = 'N'";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        return $result;
    }
    
    // added by sonali kamble on 03-may-2019
    public function saveSmartcardCommissionData($data = "") {
        $this->db->insert('smartcard_commission_master', $data);
    }
}
