<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    

class Depot_master_model extends MY_Model {

   var $local_up_db;


    function __construct() {
        parent::__construct();
       // $this->local_up_db = $this->load->database('local_up_db', TRUE);
    }

    function get_depot() {

        $this->db->select('DEPOT_NM,DEPOT_CD,DIVISION_CD');
        $this->db->from('depots');
        $this->db->where('STATE_CD', 'MHR');
        $this->db->order_by('DEPOT_CD');
        $query = $this->db->get();
                 
        return $data= $query->result_array(); 
    }

}
