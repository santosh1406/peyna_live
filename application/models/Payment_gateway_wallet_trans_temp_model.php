<?php

class Payment_gateway_wallet_trans_temp_model extends MY_Model {

    var $table = 'payment_gateway_wallet_trans_temp';
    var $fields = array("id", "payment_gateway_id", "ticket_ref_no","amount","amount_paid_by_vle", "added_on", "status", "expired_on");
    var $key = 'id';
    protected $set_created = false;
    protected $set_modified = false;

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

}

?>