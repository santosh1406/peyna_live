<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Rfid_agent_card_model extends MY_Model {
    
    var $table  = "rfid_agent_card_detail";
    var $fields = array("rfid_agent_card_detail_id","agent_id","card_no","is_status", "is_deleted", "created_by", "created_date", "updated_by", "updated_date");
    var $key    = "rfid_agent_card_detail_id";
    
    public function __construct() {
      parent::__construct();
      $this->_init();
    }

    public function assign_card_to_agent($rfid, $agent_id, $user_id)
    {
    	if($rfid >0 && $agent_id > 0)
    	{
	    	$insert_arr = array(
		    				'card_no' => $rfid,
		    				'agent_id' => $agent_id,
		    				'created_by' => $user_id,
		    				'created_date' => date('Y-m-d H:i:s'),

	    				);
	    	
	    	$assign_id = $this->insert($insert_arr);
	    	return $assign_id;
	    }
	    else
	    {
	    	return false;
	    }
    }
}
?>