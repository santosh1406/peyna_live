<?php 
	if(!defined('BASEPATH')) exit('No direct script access allowed'); 

class Cb_msrtc_missed_transaction_model extends MY_Model {

	var $table = 'cb_msrtc_missed_transaction';
	var $fields = array("id","agent_id","agent_code","waybill_no","waybill_process_date","waybill_start_date","waybill_topup_amt","collection_date","total_tkts","consumed_amt","remaining_bal","waybill_process_status","waybill_start_status","collection_status","created_at","updated_at");
	var $key = 'id';

	public function __construct() {
		parent::__construct();
		$this->_init();
	}

}
?>