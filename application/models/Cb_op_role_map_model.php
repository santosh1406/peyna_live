<?php
class cb_op_role_map_model  extends MY_Model {
    var $table  = 'cb_op_role_map';
    var $fields = array("op_map_id", "op_id", "role_id");
   

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
    
    /*
     * Function to get list of operators
     */
    function get_operator_list($role_id) {

        $this->db->select('op_id');
		$this->db->from('cb_op_role_map');
		if($role_id != 1 && $role_id != '')
		{
			$this->db->where('role_id',$role_id);
		}
		$operatorlist= $this->db->get();
		
		$aArray = array();
        if($operatorlist->num_rows()>0){
            $operatorlist = $operatorlist->result();
			
			foreach($operatorlist as $operator)
			{
				array_push($aArray,$operator->op_id);
			}
			
        }
		return $aArray;
    }
}
?>
