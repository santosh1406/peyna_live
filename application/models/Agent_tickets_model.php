<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agent_tickets_model extends MY_Model {
    
    var $table  = 'agent_tickets';
    var $fields = array("id","ticket_id","ticket_ref_no","total_without_discount","total_paid","markup_value","agent_total","markup_type","markup_price","created_date","commission_id","commission_detail_id","commission_detail_slab_id","type","commission_percent","agent_commission","bos_commission","commission_value","agent_commision_calculated","agent_commission_amount","agent_commission_percentage","bos_commission_amount","base_fare","service_tax","total_fare","tds","tds_value_on_commission_amount","deduction_amount","agent_commission_rs");
    var $key    = "id";

    public function __construct()
    {
        parent::__construct();
        // $this->_init();
    }

}
// End of Agent_tickets_model class