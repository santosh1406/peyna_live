<?php

class Machine_reasons_model extends MY_Model {

    var $table = 'machine_reasons';
    var $fields = array("id","res_name","is_status","created_date");
    var $key = "id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
   
}    
