<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Agent_hhm_trans_model extends MY_Model {

	/**
     * Instanciar o CI
     */
    var $table = 'agent_hhm_trans';
    var $fields = array("id","agent_id","way_bill_id","no_of_tickets","allocation_date","collection_date","consumed_amount","remaining_balance","created_by","created_on","status","is_deleted");
    var $key = "id";

    public function __construct() {
        parent::__construct();
        //$this->_init();
    }

}
    