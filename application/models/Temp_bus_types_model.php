<?php


if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Temp_bus_types_model extends MY_Model {
    
    var $table  = 'temp_bus_types';
    var $fields = array("id","date","provider_id","bus_types","timestamp");
    var $key    = "";

    public function __construct() {
        parent::__construct();
       
    }

    
}
// End of Temp_bus_types_model class