<?php
class Common_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('tickets_model', 'ticket_details_model', 'users_model', 'op_config_model','mail_template_model', 'ticket_dump_model', 'ticket_tax_trans_model', 'processed_ticket_tax_trans_model','agent_tickets_model'));
    }
    
    function empty_table($table)
    {
        if(is_array($table))
        {
            foreach($table as $key => $value)
            {
                $this->db->empty_table($value);
            }
        }
        else
        {
            $this->db->empty_table($table);
        }        
    }
    
    function truncate_table($table)
    {
        if(is_array($table))
        {
            foreach($table as $key => $value)
            {
                $this->db->truncate($value);
            }
        }
        else
        {
            $this->db->truncate($table);
        }        
    }


    /***************************************************************/
    /*
    * @Author      : Suraj Rathod
    * @function    : transaction_process()
    * @param       : $response["pg_response"] -> Response from pg
                     $response["is_updated"] -> is payment_transaction_fss updated or not status
                     $response["pay_trans_fss_id"] -> payment_transaction_fss id
    * @detail      : After payment success and doconfirm booking success, book the ticket and make entry in ticket table.
    *                 
    */
    public function transaction_process($response)
    {
        $pg_success_response = array();
        $pg_result = isset($response['pg_response']['result']) ? $response['pg_response']['result'] : "";
        $wallet_result = (isset($response['is_wallet_success']) && $response['is_wallet_success']) ? true : false ;
        
        if($wallet_result || (isset($response["is_updated"]) && $response["is_updated"] && (($pg_result == "CAPTURED") || ($pg_result =="APPROVED"))))
        {
            $blockKey = $response["ticket_ref_no"];
            $ticket_id = $response["ticket_id"];

            //After Remove Below Query and pass data
            $pt_data = $this->tickets_model->where("ticket_ref_no",$blockKey)->find_all();
            
            // $pt_data = $response["ticket_data"];

            if($pt_data)
            {
                $dept_date = date("D, d M Y, h:i A",strtotime($pt_data[0]->dept_time));
                $dept_time = date("H:i:s",strtotime($pt_data[0]->dept_time));

                $data['ticket_data'] = $pt_data[0];

                //Ticket Detail Data
                // $this->ticket_details_model->ticket_id = $ticket_id;
                // $ptd_data = $this->ticket_details_model->select();
                $ptd_data = $this->ticket_details_model->where("ticket_id",$ticket_id)->find_all();
                
                //$payment_total_fare = $pt_data[0]->num_passgr * $pt_data[0]->tot_fare_amt;
                $payment_total_fare = $pt_data[0]->tot_fare_amt_with_tax;
                $ticket_total_fare_without_discount = $pt_data[0]->total_fare_without_discount;

                $data['passenger_ticket'] = $pt_data[0];
                $data['passenger_ticket_details'] = $ptd_data;
                $data['pnr'] = $response['ticket_pnr'];


                /****** To Update PNR, BOS REFERENCE NUMBER and SMS Count Start ***********/
                $bos_ref = getBossRefNo($ticket_id);
                // $this->tickets_model->boss_ref_no = $bos_ref["bos_ref_no"];
                // $bos_ref_exists = $this->tickets_model->select();
                $bos_ref_exists = $this->tickets_model->where("boss_ref_no",$bos_ref["bos_ref_no"])->find_all();
                
                if(!$bos_ref_exists)
                {
                    $bos_ticket_digit = $bos_ref["bos_ticket_digit"];
                    $bos_ticket_string = $bos_ref["bos_ticket_string"];
                    $bos_ref_no = $bos_ref["bos_ref_no"];
                }

                /*$fields_update = array("transaction_status" => "success", 
                                        "pnr_no" => $response["ticket_pnr"],
                                        "bos_ticket_digit" => $bos_ticket_digit,
                                        "bos_ticket_string" => $bos_ticket_string,
                                        "boss_ref_no" => $bos_ref_no,
                                        "eticket_count" => $eticket_count // IMP -> Placed 1 Hardcoded because. Here is the first time we are sending message.
                                        );

                $this->tickets_model->update($ticket_id,$fields_update);*/

                /****** To Update PNR,BOS REFERENCE NUMBER and SMS Count End***********/

                

                /*********************************************************************/
                /*
                *This is for testing Purpose only.
                *After discussion change this.
                */
                //THIS SEAT NO QUERY CAN BE REDUCE LATER
                $seat_no = $this->tickets_model->get_seat_nos_by_tid($ticket_id);
                if(count($seat_no) > 1)
                {
                    $seat_with_berth = "Berth 1: ".$seat_no[0]->seat_no." Berth 2: ".$seat_no[1]->seat_no;
                }
                else
                {
                    $seat_with_berth = $seat_no[0]->seat_no;
                }

                $ticket_display_fare = $payment_total_fare;
                
                /*$ticket_sms_detail = array(
                                    "{{pnr_no}}" => $response['ticket_pnr'],
                                    "{{bos_ref_no}}" => $bos_ref_no,
                                    "{{amount_to_pay}}" => $pt_data[0]->total_fare_without_discount,
                                    "{{from_stop}}" => $pt_data[0]->from_stop_name,
                                    "{{to_stop}}" => $pt_data[0]->till_stop_name,
                                    "{{seat_no}}" =>  $seat_with_berth,
                                    "{{doj}}" => $dept_date
                                  );

                log_data("sms/sms_".date("d-m-Y")."_log.log",$ticket_sms_detail, 'ticket_sms_detail array');
                $temp_sms_msg = getSMS("ticket_booking");
                $msg_to_sent = str_replace(array_keys($ticket_sms_detail),array_values($ticket_sms_detail),$temp_sms_msg[0]->sms_template_content);
                $is_send = sendSMS($pt_data[0]->mobile_no, $msg_to_sent, array('ticket_id' => $ticket_id));*/
                $smsData = [];
                $smsData["data"] = $data;
                $smsData["data"]["passenger_ticket"]->pnr_no = $response["ticket_pnr"];
                $smsData["seat_with_berth"] = $seat_with_berth;
//                $is_send = sendTicketBookedSms($smsData, $pt_data[0]->mobile_no);
                //if ticket booked by ors_agent two sms send one for agent and second for pessanger //
                $mobile_array[] = $pt_data[0]->mobile_no;
                if($pt_data[0]->agent_mobile_no != "")
                {
                    $mobile_array[] = $pt_data[0]->agent_mobile_no;
                }
                $is_send = sendTicketBookedSms($smsData, $mobile_array);
                
                
                $eticket_count = ($is_send) ? 1 : 0;


                $fields_update = array("transaction_status" => "success", 
                                        "pnr_no" => $response["ticket_pnr"],
                                        "bos_ticket_digit" => $bos_ticket_digit,
                                        "bos_ticket_string" => $bos_ticket_string,
                                        "boss_ref_no" => $bos_ref_no,
                                        "eticket_count" => $eticket_count // IMP -> Placed 1 Hardcoded because. Here is the first time we are sending message.
                                        );

                $this->tickets_model->update($ticket_id,$fields_update);
                
                
                /*
                * The Above SMS Code is temp and For testing purposeonly
                */

                  
                //to check if user exists
                $email = $pt_data[0]->user_email_id;
                $data['email']=$email;
                $this->users_model->email = $email;
                $data['user_exists'] = $this->users_model->select();

                /*********************************************************************/
                $available_detail = $this->session->userdata('available_detail');
                if (count($ptd_data) > 0)
                {
                    $passanger_details_basic_fare = 0;
                    //$basic_fare = $pt_data[0]->adult_basic_fare + $pt_data[0]->child_basic_fare;
                    $psgr_no = 1;
                    $passanger_detail = "";
                    foreach ($ptd_data as $key => $value) {

                        // $adult_child = ($value->psgr_age <= 12) ? "C" : "A";
                        /*if($value->psgr_age <= 12 && isset($available_detail['is_child_concession']) && $available_detail['is_child_concession'] == "Y")*/
                        if($value->psgr_age <= 12 && $value->psgr_type == "C")
                        {
                          $adult_child = "C";
                          $passanger_details_basic_fare += $value->child_basic_fare;
                        }
                        else
                        {
                          $adult_child = "A";
                          $passanger_details_basic_fare += $value->adult_basic_fare;
                        }

                        $passanger_detail .= "<tr>
                                                  <td align='center' valign='top' style='border:1px solid #000; padding-left:7px;'>".$value->psgr_name."</td>
                                                  <td align='center' valign='top' style='border:1px solid #000; padding-left:7px;'>".$value->psgr_age."</td>
                                                  <td align='center' valign='top' style='border:1px solid #000; padding-left:7px;'>".$adult_child."</td>
                                                  <td align='center' valign='top' style='border:1px solid #000; padding-left:7px;'>".$value->psgr_sex."</td>
                                                  <td align='center' valign='top' style='border:1px solid #000; padding-left:7px;'>".$value->seat_no."</td>
                                                  <td align='center' valign='top' style='border:1px solid #000; padding-left:7px;'> NA </td>
                                              </tr>";
                        $psgr_no++; 
                    }

                    /*$op_config_detail = array();
                    $op_config_array = $this->op_config_model->get_op_detail_by_id($available_detail["op_id"]);
                    
                    if(!empty($op_config_array))
                    {
                      $op_config_detail = $op_config_array[0];
                    }*/
                    
                    //discuss master_id_data if condition later 
                    $tr_hide_show_class = ($pt_data[0]->discount_value > 0) ? " " : "hide";
                    //$basic_fare = $pt_data[0]->adult_basic_fare + $pt_data[0]->child_basic_fare;
                    $mail_replacement_array = array('{{bank_transaction_no}}' => "NA",
                                                   "{{pnr_no}}" => $data['pnr'],
                                                  "{{ticekt_no}}" => $ticket_id,
                                                  "{{bos_ref_no}}" => $bos_ref["bos_ref_no"],
                                                   "{{service_end_place}}" => $pt_data[0]->till_stop_name,
                                                   "{{reservation_from}}" => $pt_data[0]->from_stop_name,
                                                   "{{res_frm_cd}}" => $pt_data[0]->boarding_stop_cd,
                                                   "{{reservation_to}}" => $pt_data[0]->till_stop_name,
                                                   "{{res_to_cd}}" => $pt_data[0]->till_stop_cd,
                                                   "{{boarding_pont}}" => $pt_data[0]->boarding_stop_name,
                                                   "{{boarding_cd}}" => $pt_data[0]->boarding_stop_cd,
                                                   "{{alighting_point}}" => $pt_data[0]->destination_stop_name,
                                                   "{{alighting_cd}}" => $pt_data[0]->destination_stop_cd,
                                                   "{{doj}}" => $dept_date,
                                                   "{{dpt_tm}}" => date("h:i A",strtotime($dept_time)),
                                                   "{{boarding_time}}" => date("h:i A",strtotime($dept_time)),
                                                   "{{alighting_time}}" => date("h:i A",strtotime($pt_data[0]->alighting_time)),
                                                   "{{bus_service_type}}" => $pt_data[0]->bus_service_no,
                                                   "{{no_of_seats}}" => $pt_data[0]->num_passgr,
                                                   "{{psgr_email_id}}" => $pt_data[0]->user_email_id,
                                                   "{{psgr_contact_no}}" => $pt_data[0]->mobile_no,
                                                   "{{depot_name}}" => "NA",
                                                   // This must be come from op_config table. Email field is not there in table so added hardcore value.
                                                   "{{support_email_id}}" => 'online.support@upsrtc.com',
                                                    
                                                   /*"{{tr_hide_show_class}}" => $tr_hide_show_class,
                                                   "{{discount_on_ticket}}" => $pt_data[0]->discount_value,*/
                                                   
                                                   // '{{basic_fare}}'  => $ptd_data[0]->adult_basic_fare, //this is temp. ask logic in meeting
                                                   
                                                   /*               IMP NOTICE
                                                    *After Getting time insert detailed fare in tickets table
                                                    */
                                                   '{{basic_fare}}'  => $passanger_details_basic_fare, //this is temp. ask logic in meeting
                                                   '{{res_charges}}' =>$ptd_data[0]->fare_reservationCharge*$pt_data[0]->num_passgr,
                                                   '{{pas}}'  => '0.00',
                                                   '{{acc}}'  => $ptd_data[0]->fare_acc*$pt_data[0]->num_passgr,
                                                   '{{si}}' => $ptd_data[0]->fare_sleeperCharge*$pt_data[0]->num_passgr,
                                                   '{{it}}' => $ptd_data[0]->fare_it*$pt_data[0]->num_passgr,
                                                   '{{tt}}' => $ptd_data[0]->fare_toll*$pt_data[0]->num_passgr,
                                                   '{{octroi}}' => $ptd_data[0]->fare_octroi*$pt_data[0]->num_passgr,
                                                   '{{fare_hr}}' => $ptd_data[0]->fare_hr*$pt_data[0]->num_passgr,
                                                   '{{convey_charges}}' => $pt_data[0]->fare_convey_charge,
                                                   '{{other_amt}}'  => $ptd_data[0]->fare_other*$pt_data[0]->num_passgr,
                                                   // '{{total_chargeable_amt}}' => $pt_data[0]->tot_fare_amt_with_tax,
                                                   '{{total_chargeable_amt}}' => $pt_data[0]->total_fare_without_discount,
                                                   "{{psgr_list}}" => $passanger_detail,
                                                   "{{header}}" => "<img src='http://www.bookonspot.com/app/assets/travelo/front/images/ticket_logo.png' width='300px;' />");

                        
                         /************ Mail Start Here ********/
                        $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),UPSRTC_TICKET_TEMPLATE);

                         $mailbody_array = array("subject" => "Rokad Ticket",
                                                "message" => $data['mail_content'],
                                                "to" =>  $pt_data[0]->user_email_id 
                                                );
                         
                         $mail_result = send_mail($mailbody_array);
                         //if agent booked tkt send mail to both agent as well as customer//
                        $email_agent = $pt_data[0]->agent_email_id;
                        if($email_agent != "" && ($email_agent != $email))
                        {
                            $agent_mailbody_array = array("subject" => "Rokad Ticket",
                                                  "message" => $data['mail_content'],
                                                  "to" =>  $pt_data[0]->agent_email_id
                                                  );
                             $agent_mail_result = send_mail($agent_mailbody_array);
                        }
                        /************ Mail End Here ********/

                        /************ Dump ticket Start Here ********/
                        //dump ticket in table
                        /*$this->mail_template_model->title = 'Upsrtc Ticket Template';
                        $mail_data = $this->mail_template_model->select();
                        $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),$mail_data[0]->body);*/
                        

                        $this->ticket_dump_model->bos_ref_no = $bos_ref["bos_ref_no"];
                        $this->ticket_dump_model->ticket_id = $ticket_id;
                        $this->ticket_dump_model->ticket_ref_no = $blockKey;
                        $this->ticket_dump_model->pnr_no = $data['pnr'];
                        $this->ticket_dump_model->email = $email;
                        $this->ticket_dump_model->mobile = $pt_data[0]->mobile_no;
                        $this->ticket_dump_model->journey_date = $pt_data[0]->dept_time;
                        $this->ticket_dump_model->ticket = $data['mail_content'];
                        if ($this->session->userdata('booking_user_id') != "") {
                           $this->ticket_dump_model->created_by = $this->session->userdata("booking_user_id");
                        }

                        $ticket_dump_id = $this->ticket_dump_model->save();
                        /************ Dump ticket End Here ********/
 
                        //insert into user action log
                        $users_action_log = array("user_id" => $this->session->userdata("booking_user_id"),
                                                  "name" => $this->session->userdata("display_name"),
                                                  "url" => "bos_ref_no = ".$bos_ref["bos_ref_no"]." , ticket_ref_no = ".$blockKey." , ticket_id = ".$ticket_id." , pnr_no = ".$data['pnr'],
                                                  "action" => "bus ticket booked",
                                                  "message" => "Booked bus from <span class='navy_blue location'>".ucfirst($pt_data[0]->from_stop_name)."</span> to <span class='navy_blue location'>".ucfirst($pt_data[0]->till_stop_name)."</span> for the day <span class='navy_blue date'>".date("jS F Y", strtotime($pt_data[0]->dept_time))."</span>"
                                                  );
                        
                        users_action_log($users_action_log);

                        $pg_success_response["msg_type"] = "success";
                        $pg_success_response["bos_ref_no"] = $bos_ref["bos_ref_no"];
                        $pg_success_response["ticket_id"] = $ticket_id;
                        if(isset($response['is_back']) && $response['is_back']==1){
                            $pg_success_response["redirect_url"] = base_url()."admin/reservation/view_ticket/".$blockKey;
                        }else{
                            $pg_success_response["redirect_url"] = base_url()."front/booking/view_ticket/".$blockKey;
                        }
                        
                        // redirect(base_url()."front/booking/view_ticket/".$blockKey);
                }// end of if $ptd_data condition
                else
                {
                    $custom_error_mail = array(
                                          "custom_error_subject" => "ticket data not available",
                                          "custom_error_message" => "ticket detail data not available .function name transaction_process_api"
                                        );

                    $this->developer_custom_error_mail($custom_error_mail);

                    $pg_success_response["msg_type"] = "error";
                    $pg_success_response["msg"] = "ticket detail data not available.";
                    $pg_success_response["redirect_url"] = base_url();
                }
         
            }// end of if $pt_data condition
            else
            {
                $custom_error_mail = array(
                                          "custom_error_subject" => "ticket data not available",
                                          "custom_error_message" => "function name transaction_process"
                                        );

                $this->developer_custom_error_mail($custom_error_mail);

                $pg_success_response["msg_type"] = "error";
                $pg_success_response["msg"] = "ticket data not available.";
                $pg_success_response["redirect_url"] = base_url();
            }
        }// end if payment status $response["is_updated"] && $pg_result
        else
        {
            $custom_error_mail = array(
                                          "custom_error_subject" => "payment gateway failed/error",
                                          "custom_error_message" => "payment gateway failed/error or transaction table not updated."
                                        );

            $this->developer_custom_error_mail($custom_error_mail);

            $pg_success_response["msg_type"] = "error";
            $pg_success_response["msg"] = "payment gateway failed/error or transaction table not updated.";
            $pg_success_response["redirect_url"] = base_url();
        }

        return $pg_success_response;
    } // End of transaction_process


    /*
    * @Author      : Suraj Rathod
    * @function    : transaction_process_api()
    * @param       : $response["pg_response"] -> Response from pg
                     $response["is_updated"] -> is payment_transaction_fss updated or not status
                     $response["pay_trans_fss_id"] -> payment_transaction_fss id
    * @detail      : After payment success and doconfirm booking success, book the ticket and make entry in ticket table.
    *                 
    */
    public function transaction_process_api($response)
    {
        $pg_success_response = array();
        $pg_result = isset($response['pg_response']['result']) ? $response['pg_response']['result'] : "";
        $wallet_result = (isset($response['is_wallet_success']) && $response['is_wallet_success']) ? true : false ;
        
        if($wallet_result || (isset($response["is_updated"]) && $response["is_updated"] && (($pg_result == "CAPTURED") || ($pg_result =="APPROVED"))))
        {
            $blockKey = $response["ticket_ref_no"];
            $ticket_id = $response["ticket_id"];

            //After Remove Below Query and pass data
            $pt_data = $this->tickets_model->where("ticket_ref_no",$blockKey)->find_all();
            
            // $pt_data = $response["ticket_data"];

            if($pt_data)
            {
                $dept_date = date("D, d M Y, h:i A",strtotime($pt_data[0]->dept_time));
                $dept_time = date("H:i:s",strtotime($pt_data[0]->dept_time));

                $data['ticket_data'] = $pt_data[0];

                //Ticket Detail Data
                // $this->ticket_details_model->ticket_id = $ticket_id;
                // $ptd_data = $this->ticket_details_model->select();
                $ptd_data = $this->ticket_details_model->where("ticket_id",$ticket_id)->find_all();
                
                //$payment_total_fare = $pt_data[0]->num_passgr * $pt_data[0]->tot_fare_amt;
                $payment_total_fare = $pt_data[0]->tot_fare_amt_with_tax;
                $ticket_total_fare_without_discount = $pt_data[0]->total_fare_without_discount;

                $data['passenger_ticket'] = $pt_data[0];
                $data['passenger_ticket_details'] = $ptd_data;
                $data['pnr'] = $response['ticket_pnr'];


                /****** To Update PNR, BOS REFERENCE NUMBER and SMS Count Start ***********/
                $bos_ref = getBossRefNo($ticket_id);
                // $this->tickets_model->boss_ref_no = $bos_ref["bos_ref_no"];
                // $bos_ref_exists = $this->tickets_model->select();
                $bos_ref_exists = $this->tickets_model->where("boss_ref_no",$bos_ref["bos_ref_no"])->find_all();
                
                if(!$bos_ref_exists)
                {
                    $bos_ticket_digit = $bos_ref["bos_ticket_digit"];
                    $bos_ticket_string = $bos_ref["bos_ticket_string"];
                    $bos_ref_no = $bos_ref["bos_ref_no"];
                }

                /*$fields_update = array("transaction_status" => "success", 
                                        "pnr_no" => $response["ticket_pnr"],
                                        "bos_ticket_digit" => $bos_ticket_digit,
                                        "bos_ticket_string" => $bos_ticket_string,
                                        "boss_ref_no" => $bos_ref_no,
                                        "eticket_count" => $eticket_count // IMP -> Placed 1 Hardcoded because. Here is the first time we are sending message.
                                        );

                $this->tickets_model->update($ticket_id,$fields_update);*/

                /****** To Update PNR,BOS REFERENCE NUMBER and SMS Count End***********/

                

                /*********************************************************************/
                /*
                *This is for testing Purpose only.
                *After discussion change this.
                */
                //THIS SEAT NO QUERY CAN BE REDUCE LATER
                $seat_no = $this->tickets_model->get_seat_nos_by_tid($ticket_id);
                if(count($seat_no) > 1)
                {
                    $seat_with_berth = "Berth 1: ".$seat_no[0]->seat_no." Berth 2: ".$seat_no[1]->seat_no;
                    $seat_with_berth_ticket = "Berth 1: ".$seat_no[0]->seat_no." <br/> Berth 2: ".$seat_no[1]->seat_no;
                }
                else
                {
                    $seat_with_berth = $seat_no[0]->seat_no;
                    $seat_with_berth_ticket = $seat_no[0]->seat_no;
                }

                // $ticket_display_fare = ($pt_data[0]->booked_by == "ors_agent") ? $ticket_total_fare_without_discount : $payment_total_fare;
                if($pt_data[0]->payment_by == 'csc') {
                  $ticket_display_fare = $ticket_total_fare_without_discount;
                }
                else {
                  $ticket_display_fare = $payment_total_fare;
                }
                
                eval(FME_AGENT_ROLE_NAME);
                if(in_array($pt_data[0]->booked_by, $fme_agent_role_name))
                {
                  $orsagent_ticket_data = $this->agent_tickets_model->where(array("ticket_id" => $ticket_id))->find_all();
                  if($orsagent_ticket_data)
                  {
                    $ticket_display_fare = $orsagent_ticket_data[0]->agent_total;
                  }
                }
                

                /*$ticket_sms_detail = array(
                                    "{{pnr_no}}" => $response['ticket_pnr'],
                                    "{{bos_ref_no}}" => $bos_ref_no,
                                    "{{amount_to_pay}}" => $ticket_display_fare,
                                    "{{from_stop}}" => $pt_data[0]->from_stop_name,
                                    "{{to_stop}}" => $pt_data[0]->till_stop_name,
                                    "{{seat_no}}" => $seat_with_berth,
                                    "{{doj}}" => $dept_date
                                  );

                log_data("sms/sms_".date("d-m-Y")."_log.log",$ticket_sms_detail, 'ticket_sms_detail array');

                $temp_sms_msg = getSMS("ticket_booking");
                $msg_to_sent = str_replace(array_keys($ticket_sms_detail),array_values($ticket_sms_detail),$temp_sms_msg[0]->sms_template_content);

                $is_send = sendSMS($pt_data[0]->mobile_no, $msg_to_sent, array('ticket_id' => $ticket_id));*/
                $smsData = [];
                $smsData["data"] = $data;
                $smsData["data"]["passenger_ticket"]->pnr_no = $response["ticket_pnr"];
                $smsData["seat_with_berth"] = $seat_with_berth;
//                $is_send = sendTicketBookedSms($smsData, $pt_data[0]->mobile_no);
                //if ticket booked by ors_agent two sms send one for agent and second for pessanger //
                $mobile_array[] = $pt_data[0]->mobile_no;
                if($pt_data[0]->agent_mobile_no != "")
                {
                    $mobile_array[] = $pt_data[0]->agent_mobile_no;
                }
                $is_send = sendTicketBookedSms($smsData, $mobile_array);

                $eticket_count = ($is_send) ? 1 : 0;


                $fields_update = array("transaction_status" => "success", 
                                        "pnr_no" => $response["ticket_pnr"],
                                        "bos_ticket_digit" => $bos_ticket_digit,
                                        "bos_ticket_string" => $bos_ticket_string,
                                        "boss_ref_no" => $bos_ref_no,
                                        "eticket_count" => $eticket_count // IMP -> Placed 1 Hardcoded because. Here is the first time we are sending message.
                                        );

                $this->tickets_model->update($ticket_id,$fields_update);
                
                
                /*
                * The Above SMS Code is temp and For testing purposeonly
                */

                  
                //to check if user exists
                $email = $pt_data[0]->user_email_id;
                $data['email']=$email;
                $this->users_model->email = $email;
                $data['user_exists'] = $this->users_model->select();

                /*********************************************************************/

                if (count($ptd_data) > 0)
                {
                    //$basic_fare = $pt_data[0]->adult_basic_fare + $pt_data[0]->child_basic_fare;
                    $psgr_no = 1;
                    $passanger_detail = "";
                   
                    foreach ($ptd_data as $key => $value)
                    {

                      /*$passanger_detail .= '<tr>
                                              <td valign="top">
                                                <span style="line-height:27px;"> 
                                                  '.ucwords($value->psgr_name).'
                                                </span>
                                              </td>
                                              <td valign="top">
                                                <span style="line-height:27px;">
                                                  '.ucwords($value->psgr_age).'
                                                </span>
                                              </td>
                                              <td valign="top">
                                                <span style="line-height:27px;">
                                                  '.ucwords($value->psgr_sex).'
                                                </span>
                                              </td>
                                              <td valign="top">
                                                <span style="line-height:27px;">
                                                  '.$value->seat_no.'
                                                </span>
                                              </td>
                                            </tr>';*/
                        $passanger_detail .= '<tr style="padding:7px 0;text-align: center">
                                                <td style="padding:7px;">
                                                  '.ucwords($value->psgr_name).'
                                                </td>
                                                <td style="padding:7px 0;text-align: center">
                                                  <span style="line-height:27px;">
                                                  '.ucwords($value->psgr_age).'
                                                  </span>
                                                </td>
                                                <td style="padding:7px 0;text-align: center">
                                                  '.ucwords($value->psgr_sex).'
                                                </td>
                                                <td style="padding:7px 0;text-align: center">
                                                  '.$value->seat_no.'
                                                </td>
                                              </tr>';
                        $psgr_no++; 
                    }

                    $cancellation_policy_to_show = "";
                    if(isset($pt_data[0]->cancellation_policy) && $pt_data[0]->cancellation_policy != "")
                    {
                      $cpolicy = $pt_data[0]->cancellation_policy != '' ? json_decode($pt_data[0]->cancellation_policy) : array();
                      if(!empty($cpolicy))
                      {
                        foreach ($cpolicy as $ckey => $cvalue)
                        {
                          if(!empty($cvalue))
                          {
                            $charges_in_per = "";
                            $charges_in_per = 100 - $cvalue->refundInPercentage;
                            $cancellation_policy_to_show .= '<tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;">
                                                              <td style="padding:3px;">Cancellation within '.$cvalue->cutoffTime.' hrs</td>
                                                              <td style="padding:3px;">'.$charges_in_per.'%</td>
                                                            </tr>';
                          }
                        }
                      }
                    }
                    /*$service_detail = array();
                    $service_detail["bus_type"] = "NA";

                    if(isset($response["is_return_journey"]) && $response["is_return_journey"])
                    {
                      $available_detail = $this->session->userdata('available_detail_return');
                      $provider_available_bus = $this->session->userdata('provider_available_bus_return');
                    }
                    else
                    {
                      $available_detail = $this->session->userdata('available_detail');
                      $provider_available_bus = $this->session->userdata('provider_available_bus');
                    }

                    $service_detail = $provider_available_bus[$available_detail["service_id"]];
                    */

                    $pickup_address = $pt_data[0]->boarding_stop_name;
                    $pickup_address .= ($pt_data[0]->pickup_address != "") ? " (".$pt_data[0]->pickup_address.")" : "";
                    
                    if($pt_data[0]->droping_time != "" && $pt_data[0]->droping_time != "0000-00-00 00:00:00")
                    {
                      $droping_time = date("h:i A",strtotime($pt_data[0]->droping_time));
                    }
                    else
                    {
                      $droping_time = date("h:i A",strtotime($pt_data[0]->alighting_time));
                    }

                    if($pt_data[0]->boarding_time != "" && $pt_data[0]->boarding_time != "0000-00-00 00:00:00")
                    {
                      $bus_boarding_time = date("h:i A",strtotime($pt_data[0]->boarding_time));
                    }
                    else
                    {
                      $bus_boarding_time = "NA";
                    }

                    $agent_details = "";
                    
                    if(!empty($this->session->userdata("user_id")))
                    {
                      eval(AGENT_ID_ARRAY);
                      if(in_array($this->session->userdata("role_id") , $agent_id_array))
                      {

                        $user_agent_details  = $this->users_model
                                                    ->where(["id" => $this->session->userdata("user_id")])
                                                    ->find_all();

                        if($user_agent_details)
                        {
                          $agent_firm_name = ($user_agent_details[0]->firm_name != "") ? $user_agent_details[0]->firm_name : $user_agent_details[0]->first_name." ".$user_agent_details[0]->last_name;
                          $agent_details = "<tr>
                                <td colspan=2' align='right' valign='top' style='color:#000;font-weight:bold; font-size:13px; text-align:centre; padding-top:7px;'>
                                <span style='color:#ff940a;'>
                                ".$agent_firm_name."</span><br />Agent Contact No. ".$user_agent_details[0]->mobile_no."</td>
                              </tr>";
                        }
                      }

                    }

                    
                    $mail_replacement_array = array('{{booked_date}}' => date("jS F Y",strtotime($pt_data[0]->inserted_date)),
                                                   "{{pnr_number}}" => $response["ticket_pnr"],
                                                   "{{from_to_journey}}" => $pt_data[0]->from_stop_name." to ".$pt_data[0]->till_stop_name,
                                                   "{{journey_date}}" =>  date("l \- jS F Y",strtotime($pt_data[0]->dept_time)),
                                                   "{{op_name}}" => $pt_data[0]->op_name,
                                                   "{{seat_no}}" => $pt_data[0]->num_passgr,
                                                   "{{bus_types}}" => $pt_data[0]->bus_type, //$service_detail["bus_type"],
                                                   "{{total_fare}}" => "Rs. ".$ticket_display_fare,
                                                   // "{{total_fare}}" => "Rs. ".$pt_data[0]->total_fare_without_discount,
                                                   "{{bos_ref_no}}" => $bos_ref_no,
                                                   "{{boarding_point}}" => $pickup_address,
                                                   "{{dropping_point}}" => $pt_data[0]->destination_stop_name,
                                                   "{{departure_time}}" => $bus_boarding_time,
                                                   // "{{alighting_time}}" => date("h:i A",strtotime($pt_data[0]->alighting_time)),
                                                   "{{alighting_time}}" => date("h:i A",strtotime($pt_data[0]->alighting_time)),
                                                   "{{email}}" => $email,
                                                   "{{mobile_no}}" => $pt_data[0]->mobile_no,
                                                   "{{passanger_details}}" => $passanger_detail,
                                                   "{{agent_details}}" => $agent_details,
                                                   "{{dynamic_cancellation_policy}}" => $cancellation_policy_to_show
                                                   );
                          /*"{{header}}" => "<img src='http://www.bookonspot.com/app/assets/travelo/front/images/ticket_logo.png' width='300px;' />"*/
                        /************ Mail Start Here ********/
                        $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),BOS_TICKET_TEMPLATE);

                        $mailbody_array = array("subject" => "Rokad Ticket",
                                                "message" => $data['mail_content'],
                                                "to" =>  $email 
                                                );
                         
                         $mail_result = send_mail($mailbody_array);
                      //****if agent booked tkt send mail to both agent as well as customer****//
                     $email_agent = $pt_data[0]->agent_email_id;
                        if($email_agent != "" && ($email_agent != $email))
                        {
                            $agent_mailbody_array = array("subject" => "Rokad Ticket",
                                                  "message" => $data['mail_content'],
                                                  "to" =>  $email_agent
                                                  );
                             $agent_mail_result = send_mail($agent_mailbody_array);
                        }
                        /************ Mail End Here ********/

                        /************ Dump ticket Start Here ********/
                        //dump ticket in table
                        /*$this->mail_template_model->title = 'Upsrtc Ticket Template';
                        $mail_data = $this->mail_template_model->select();
                        $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),$mail_data[0]->body);
                        */

                        $this->ticket_dump_model->bos_ref_no = $bos_ref["bos_ref_no"];
                        $this->ticket_dump_model->ticket_id = $ticket_id;
                        $this->ticket_dump_model->ticket_ref_no = $blockKey;
                        $this->ticket_dump_model->pnr_no = $data['pnr'];
                        $this->ticket_dump_model->email = $email;
                        $this->ticket_dump_model->mobile = $pt_data[0]->mobile_no;
                        $this->ticket_dump_model->journey_date = $pt_data[0]->dept_time;
                        $this->ticket_dump_model->ticket = $data['mail_content'];
                        if ($this->session->userdata('booking_user_id') != "") {
                           $this->ticket_dump_model->created_by = $this->session->userdata("booking_user_id");
                        }

                        $ticket_dump_id = $this->ticket_dump_model->save();
                        /************ Dump ticket End Here ********/
 
                        //insert into user action log
                        $users_action_log = array("user_id" => $this->session->userdata("booking_user_id"),
                                                  "name" => $this->session->userdata("display_name"),
                                                  "url" => "bos_ref_no = ".$bos_ref["bos_ref_no"]." , ticket_ref_no = ".$blockKey." , ticket_id = ".$ticket_id." , pnr_no = ".$data['pnr'],
                                                  "action" => "bus ticket booked",
                                                  "message" => "Booked bus from <span class='navy_blue location'>".ucfirst($pt_data[0]->from_stop_name)."</span> to <span class='navy_blue location'>".ucfirst($pt_data[0]->till_stop_name)."</span> for the day <span class='navy_blue date'>".date("jS F Y", strtotime($pt_data[0]->dept_time))."</span>"
                                                  );
                        
                        users_action_log($users_action_log);

                        $pg_success_response["msg_type"] = "success";
                        $pg_success_response["bos_ref_no"] = $bos_ref["bos_ref_no"];
                        $pg_success_response["ticket_id"] = $ticket_id;
                        if(isset($response['is_back']) && $response['is_back']==1){
                            $pg_success_response["redirect_url"] = base_url()."admin/reservation/view_ticket/".$blockKey;
                        }else{
                            $pg_success_response["redirect_url"] = base_url()."front/booking/view_ticket/".$blockKey;
                        }
                        
                        // redirect(base_url()."front/booking/view_ticket/".$blockKey);
                }// end of if $ptd_data condition
                else
                {
                    $custom_error_mail = array(
                                          "custom_error_subject" => "ticket data not available",
                                          "custom_error_message" => "ticket detail data not available .function name transaction_process_api"
                                        );

                    $this->developer_custom_error_mail($custom_error_mail);

                    $pg_success_response["msg_type"] = "error";
                    $pg_success_response["msg"] = "ticket detail data not available.";
                    $pg_success_response["redirect_url"] = base_url();
                }
         
            }// end of if $pt_data condition
            else
            {
                $custom_error_mail = array(
                                          "custom_error_subject" => "ticket data not available for ticket id - ".$ticket_id,
                                          "custom_error_message" => "Ticket Id : ".$ticket_id.". Function name transaction_process_api."
                                        );

                $this->developer_custom_error_mail($custom_error_mail);

                $pg_success_response["msg_type"] = "error";
                $pg_success_response["msg"] = "ticket data not available.";
                $pg_success_response["redirect_url"] = base_url();
            }
        }// end if payment status $response["is_updated"] && $pg_result
        else
        {
              $custom_error_mail = array(
                                          "custom_error_subject" => "payment gateway failed/error for ticket id - ".$response["ticket_id"],
                                          "custom_error_message" => "payment gateway failed/error or transaction table not updated for ticket id - ".$response["ticket_id"]."."
                                        );

              $this->developer_custom_error_mail($custom_error_mail);

            $pg_success_response["msg_type"] = "error";
            $pg_success_response["msg"] = "payment gateway failed/error or transaction table not updated.";
            $pg_success_response["redirect_url"] = base_url();
        }

        return $pg_success_response;
    } // End of transaction_process


    /***************************************************************/

    public function failed_trans_wallet_add($amount_to_add, $ticket_ref_no)
    {
        $data = array();
        $user_id = ($this->session->userdata("user_id") > 0) ? $this->session->userdata("user_id") : $this->session->userdata("booking_user_id");
        $wallet_data =  $this->wallet_model
                        ->where('user_id', $user_id)
                        ->find_all();

        $wallet_data = $wallet_data[0];

        $this->wallet_model->id = $wallet_data->id;
        $this->wallet_model->amt = $wallet_data->amt + $amount_to_add;
        $w_id = $this->wallet_model->save();
        $data["w_id"] = $w_id;

        if($w_id > 0)
        {
           $this->wallet_model->w_id = $w_id;
           $this->wallet_model->amt = $amount_to_add;
           $this->wallet_model->comment = "Seat book failed for transaction of ticket reference number ".$ticket_ref_no;
           $this->wallet_model->status = "credited";
           $this->wallet_model->user_id = $user_id;
           $this->wallet_model->added_by = $user_id;
           $this->wallet_model->amt_after_trans = $wallet_data->amt + $amount_to_add;
           $this->wallet_model->amt_before_trans = $wallet_data->amt;
           $this->wallet_model->extra = "ticket_ref_no = ".$ticket_ref_no;
           $wt_id = $this->wallet_model->save();
           $data["wt_id"] = $wt_id;
        }

        return $data;
    }// End of failed_trans_wallet_add



    /*
    * @Author      : Suraj Rathod
    * @function    : payment_gateway_failed()
    * @param       : $response["pg_response"] -> Response from pg
    * @detail      : After payment failed, update status in process ticket table.
    *                 
    */
    public function payment_gateway_failed($response)
    {
        // $array_where = array('ticket_ref_no' => $response["ticket_ref_no"]);
        // $fields_update = array('transaction_status' => "failed");
        // $this->processed_tickets_model->update_where($array_where,'', $fields_update);

        $this->session->set_flashdata('msg_type', 'error');
        $this->session->set_flashdata('msg', 'Error oocured while booking ticket.');
        $data["response"] = $response;
        
        load_front_view(PAYMENT_TRANSACTION_FAILED, $data);
    }


    public function backend_payment_gateway_failed($response)
    {
        // $array_where = array('ticket_ref_no' => $response["ticket_ref_no"]);
        // $fields_update = array('transaction_status' => "failed");
        // $this->processed_tickets_model->update_where($array_where,'', $fields_update);

        $this->session->set_flashdata('msg_type', 'error');
        $this->session->set_flashdata('msg', 'Error oocured while booking ticket.');
        $data["response"] = $response;

        load_back_view(RESERVATION_PAYMENT_TRANSACTION_FAILED, $data);
    }


    /* @Author      : Suraj Rathod
     * @function    : get_provider_lib
     * @param       :
     * @detail      : 
     *                 
     */

    public function get_provider_lib($provider_id, $provider_type, $op_id = NULL) {
        if ($provider_id != "" && $provider_type != "") {
            $this->db->select("ap.*,apo.library AS op_library,apo.library_path AS  op_library_path");
            $this->db->from("api_provider ap");
            $this->db->join("api_provider_operator apo", "ap.id = apo.api_provider_id and ap.id = apo.api_provider_id", "left");
            $this->db->where("ap.id = '" . $provider_id . "'");

            if ($op_id && $provider_type !== 'api_provider')
                $this->db->where("apo.op_id = '" . $op_id . "'");

            $api_result = $this->db->get();
            $api_result_array = $api_result->result_array();
            if (!empty($api_result_array)) {
                $api_result_array = $api_result_array[0];

                if ($api_result_array["op_library"] == "etravelsmart") {
                    $api_result_array["role_name"] = "api provider";
                    $api_result_array["library"] = $api_result_array["library"];
                    $api_result_array["library_path"] = $api_result_array["library_path"];
                }
                else if ($api_result_array["op_library"] == "its") {
                    $api_result_array["role_name"] = "api provider";
                    $api_result_array["library"] = $api_result_array["library"];
                    $api_result_array["library_path"] = $api_result_array["library_path"];
                }
                else {
                    $api_result_array["role_name"] = "operator";
                    $api_result_array["library"] = $api_result_array["op_library"];
                    $api_result_array["library_path"] = $api_result_array["op_library_path"];
                }

                return $api_result_array;
            } 
            else {
                return false;
            }
        } 
        else {
            return false;
        }
    }


    /* @Author      : Suraj Rathod
     * @function    : payment_gateway_failed_mail
     * @param       :
     * @detail      : 
     *                 
     */

    public function payment_gateway_failed_mail($data)
    {
       $mail_replacement_array = array('{{pg_failed_reason}}' => $data["transaction_status"],
                                         "{{ticket_id}}" => $data["ticket_id"],
                                         "{{site_url}}" => $data["site_url"]
                                         );
       
      $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),PG_FAILED_TEMPLATE);

       $mailbody_array = array("subject" => "Transaction ".$data["transaction_status"]." for ticket id ".$data["ticket_id"]." ( Environment - ".ENVIRONMENT.")",
                              "message" => $data['mail_content'],
                              "to" =>  $data['sent_to'] 
                              );
       
       $mail_result = send_mail($mailbody_array);
    }


    public function payment_success_ticket_failed_mail($data)
    {
       eval(CUSTOM_ERROR_EMAIL_ARRAY);
       $payment_gateway_response = $data["payment_gateway_response"];
       $journey_ticket_data = $data["journey_ticket_data"];
       
       $mail_replacement_array = array("{{ticket_id}}" => $data["ticket_id"],
                                       "{{ticket_amount}}" => $data["ticket_amount"],
                                       "{{ccavenue_charges}}" => $data["ccavenue_charges"],
                                       "{{total_ticket_amount}}" => $payment_gateway_response["amount"],
                                       "{{pg_tracking_id}}" => $payment_gateway_response["tracking_id"],
                                       "{{customer_name}}" => $payment_gateway_response["billing_name"],
                                       "{{customer_email}}" => $journey_ticket_data->user_email_id,
                                       "{{customer_mobile_number}}" => $journey_ticket_data->mobile_no,
                                       "{{journey_from_to}}" => $journey_ticket_data->from_stop_name." - ".$journey_ticket_data->till_stop_name,
                                       "{{journey_date}}" => $journey_ticket_data->boarding_time,
                                       "{{booking_date}}" => $journey_ticket_data->inserted_date,
                                       "{{site_url}}" => base_url(),
                                       "{{ticket_refund_amount}}" => $data["ticket_refund_amount"],
                                       "{{failed_reason}}" => $data["api_failed_reason"],
                                       "{{extra_note}}" => isset($data["extra_note"]) ? $data["extra_note"] : "",
                                      );
       
      $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),PG_SUCCESS_TICKET_FAILED_TEMPLATE);

       $mailbody_array = array("subject" => "VIMP : Payment Success & Booking Failed ( Environment - ".ENVIRONMENT.")",
                              "message" => $data['mail_content'],
                              "to" =>  $custom_error_email_array 
                              );
       
       $mail_result = send_mail($mailbody_array);
    }


    public function developer_custom_error_mail($data)
    {
       eval(CUSTOM_ERROR_EMAIL_ARRAY);
       
      $custom_mail_subject = isset($data["custom_error_subject"]) ? $data["custom_error_subject"] : "IMP Mail Please Check";
      $mail_replacement_array = array("{{custom_error_message}}" => $data["custom_error_message"]);
       
      $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),DEVELOPER_CUSTOM_ERROR_MESSAGE);

       $mailbody_array = array("subject" => "(".ENVIRONMENT.") ".$custom_mail_subject,
                                "message" => $data['mail_content'],
                                "to" =>  $custom_error_email_array 
                                );
       
       $mail_result = send_mail($mailbody_array);
    }

    /*
    * @Author      : Suraj Rathod
    * @function    : pg_confirmation()
    * @param       : 
    * @detail      : 
    *                 
    */
    public function pg_confirmation($pg_confirmation_array)
    {
      $return_ticket_id = "";
      if(isset($pg_confirmation_array["ticket_id"]) && $pg_confirmation_array["ticket_id"] > 0)
      {
        if(isset($pg_confirmation_array["return_ticket_id"]) && $pg_confirmation_array["return_ticket_id"] > 0)
        {
          $return_ticket_id = $pg_confirmation_array["return_ticket_id"];
        }

        $ticket_id = $pg_confirmation_array["ticket_id"];
        $pg_confirmation_response = $pg_confirmation_array["pg_confirmation_response"];
        
        if($pg_confirmation_response["status"] == "success")
        {
          $fields_update = array("payment_confirmation" => "full");
          $this->tickets_model->update($ticket_id,$fields_update);

          if($return_ticket_id != "")
          {
            $this->tickets_model->update($return_ticket_id,$fields_update);
          }
        }
        else
        {
          $custom_error_mail = array(
                                      "custom_error_subject" => "Payment Confirmation not done for ticket id is - ".$ticket_id,
                                      "custom_error_message" => "Please make payment confirmation for ticket id - ".$ticket_id." - ".$return_ticket_id
                                    );

          $this->developer_custom_error_mail($custom_error_mail);
        }
      }
      else
      {
        $custom_error_mail = array(
                                      "custom_error_subject" => "Payment Confirmation not done for one of the ticket",
                                      "custom_error_message" => "Please check one of the ticket payment confirmation not done."
                                    );

        $this->developer_custom_error_mail($custom_error_mail);
      }
    }

    public function developer_confirm_booking_error_mail($operator,$ticket_id)
    {
       $custom_error_mail = array(
                                      "custom_error_subject" => $operator." Confirm Booking called twice or failed for ticket ".$ticket_id,
                                      "custom_error_message" => "Please check log."
                                    );
       $this->developer_custom_error_mail($custom_error_mail);
    }


    public function management_payment_success_ticket_failed_mail($data)
    {
        if(ENVIRONMENT == "production")
        {
          eval(PAYMENT_GATEWAY_ERROR_EMAIL_ARRAY);
        }
        else
        {
          eval(CUSTOM_ERROR_EMAIL_ARRAY);
          $payment_gateway_error_email_array = $custom_error_email_array;
        }
       $payment_gateway_response = $data["payment_gateway_response"];
       $journey_ticket_data = $data["journey_ticket_data"];
       
       $mail_replacement_array = array("{{ticket_id}}" => $data["ticket_id"],
                                       "{{ticket_amount}}" => $data["ticket_amount"],
                                       "{{ccavenue_charges}}" => $data["ccavenue_charges"],
                                       "{{total_ticket_amount}}" => $payment_gateway_response["amount"],
                                       "{{pg_tracking_id}}" => $payment_gateway_response["tracking_id"],
                                       "{{customer_name}}" => $payment_gateway_response["billing_name"],
                                       "{{customer_email}}" => $journey_ticket_data->user_email_id,
                                       "{{customer_mobile_number}}" => $journey_ticket_data->mobile_no,
                                       "{{journey_from_to}}" => $journey_ticket_data->from_stop_name." - ".$journey_ticket_data->till_stop_name,
                                       "{{journey_date}}" => $journey_ticket_data->boarding_time,
                                       "{{booking_date}}" => $journey_ticket_data->inserted_date,
                                       "{{site_url}}" => base_url(),
                                       "{{ticket_refund_amount}}" => $data["ticket_refund_amount"],
                                       "{{failed_reason}}" => $data["api_failed_reason"],
                                       "{{extra_note}}" => isset($data["extra_note"]) ? $data["extra_note"] : "",
                                      );
       
      $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),PG_SUCCESS_TICKET_FAILED_TEMPLATE);

       $mailbody_array = array("subject" => "VIMP : Payment Success & Booking Failed ( Environment - ".ENVIRONMENT.")",
                              "message" => $data['mail_content'],
                              "to" =>  $payment_gateway_error_email_array 
                              );
       
       $mail_result = send_mail($mailbody_array);
    }



    public function management_custom_error_mail($data,$mail_for = "developer")
    {
      $sent_email_to = [];
      if(ENVIRONMENT == "production")
      {
        if(strtolower($mail_for) == "upsrtc" || strtolower($mail_for) == "upsrtc_api")
        {
          eval(UPSRTC_ERROR_EMAIL_ARRAY);
          $sent_email_to = $upsrtc_error_email_array;
        }
        else if(strtolower($mail_for) == "msrtc" || strtolower($mail_for) == "msrtc_api")
        {
          eval(MSRTC_ERROR_EMAIL_ARRAY);
          $sent_email_to = $msrtc_error_email_array;
        }
        else if(strtolower($mail_for) == "management")
        {
          eval(MANAGEMENT_ERROR_EMAIL_ARRAY);
          $sent_email_to = $management_error_email_array;
        }
        else
        {
          eval(CUSTOM_ERROR_EMAIL_ARRAY);
          $sent_email_to = $custom_error_email_array;
        }
      }
      else
      {
        eval(CUSTOM_ERROR_EMAIL_ARRAY);
        $sent_email_to = $custom_error_email_array;
      }
       
      $custom_mail_subject = isset($data["custom_error_subject"]) ? $data["custom_error_subject"] : "IMP Mail Please Check";
      $mail_replacement_array = array("{{custom_error_message}}" => $data["custom_error_message"]);
       
      $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),DEVELOPER_CUSTOM_ERROR_MESSAGE);

       $mailbody_array = array("subject" => "(".ENVIRONMENT.") ".$custom_mail_subject,
                                "message" => $data['mail_content'],
                                "to" =>  $sent_email_to 
                                );
       
       $mail_result = send_mail($mailbody_array);
    }


    public function management_payment_gateway_failed_mail($data)
    {

       if(ENVIRONMENT == "production")
       {
          eval(PAYMENT_GATEWAY_ERROR_EMAIL_ARRAY);
       }
       else
       {
          eval(CUSTOM_ERROR_EMAIL_ARRAY);
          $payment_gateway_error_email_array = $custom_error_email_array;
       }
       $mail_replacement_array = array('{{pg_failed_reason}}' => $data["transaction_status"],
                                         "{{ticket_id}}" => $data["ticket_id"],
                                         "{{site_url}}" => $data["site_url"]
                                         );
       
      $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),PG_FAILED_TEMPLATE);

       $mailbody_array = array("subject" => "Transaction ".$data["transaction_status"]." for ticket id ".$data["ticket_id"]." ( Environment - ".ENVIRONMENT.")",
                              "message" => $data['mail_content'],
                              "to" =>  $payment_gateway_error_email_array 
                              /*"to" =>  $data['sent_to'] */
                              );
       
       $mail_result = send_mail($mailbody_array);
    }
	
   /*
    * @Author      : Santosh Warang
    * @function    : get_value()
    * @param       : $Table,$Column,$Clause
    * @detail      : Get Specific Column From Table
    *                 
    */ 
	public function get_value($Table,$Column,$Clause)
    {
      $LcSqlStr = "SELECT ".$Column." FROM ".$Table." WHERE ".$Clause." ";
      $query = $this->db->query($LcSqlStr); 
      $row = $query->row();  
      return $row;
    }
	 
   /*
    * @Author      : Santosh Warang
    * @function    : Insert()
    * @param       : $Table,$data
    * @detail      : Insert Into Table
    *                 
    */ 
	public function insert($Table,$data)
	{
	  $this->db->insert($Table, $data);
	  $insert_id = $this->db->insert_id();
	  return $insert_id;
	}	
	
   /*
    * @Author      : Santosh Warang
    * @function    : Update()
    * @param       : $Table,$Column,$id,$data
    * @detail      : Update On Table
    *                 
    */ 
	public function update($Table,$Column,$id,$data) 
	{
	  $this->db->where($Column,$id);
	  $this->db->update($Table, $data);
	  return true;
	}

  /*
    * @Author      : Lilawati Karale
    * @function    : getCommission()
    * @param       : $Table
    * @detail      : get commission_master table details
    *                 
    */ 

  public function getCommission($Table,$default_pkg="") 
  {
    $arrayName = array();
    if($default_pkg == '1')
    {
       $arrayName = array('is_deleted' => '0','status' => '1','commission_default_flag' => '1');
    }else{
       $arrayName = array('is_deleted' => '0','status' => '1','commission_default_flag' => '0');
        if($this->session->userdata('role_id') == COMPANY_ROLE_ID) {
            $this->db->where("$Table.created_by",$this->session->userdata('id'));
        }    
    }       
    return $this->db->where($arrayName)->order_by("id","desc")->get($Table)->result_array();

  }

  public function insert_depot_log_data($input,$id) {

    $insert_data = array('user_id' => $id,
                         'old_depot_cd' => $input['old_depot_cd'],
                         'old_star_seller_cd' => $input['old_star_seller_cd'],
                         'new_depot_cd' => $input['depot_code'],
                         'new_star_seller_cd' => $input['Seller_point']
                        );
    $this->db->insert('depot_log_data', $insert_data);
  }
    
}
