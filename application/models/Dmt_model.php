<?php

//Created by pooja kambali on 29-09-2018
class Dmt_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    function neft($data) {
        $insertdata = $this->db->insert('dmt_transfer_detail', $data);
        return $insertdata;
    }


    function neftupdate($data) {

        $update = date('Y-m-d H:m:s');
        $update_data = array(
            'message' => $data['MessageString'],
            'RequestID' => $data['ResponseData'],
            'update_on' => $update
        );
        $this->db->where('Clientuniqueid', $data['ClientUniqueID']);
        $this->db->update('dmt_transfer_detail', $update_data);
    }

    public function getWalletDetails($user_id = "") {
        $queryStr = "select id,amt"
        . " FROM wallet"
        . " WHERE user_id = '" . $user_id . "'"
        . " AND status = 'Y'"
        . " AND is_deleted = 'N'";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        return $result;
    }

    public function getStatus($client_id= "") {
        $quStr = "select message"
        . " FROM dmt_transfer_detail"
        . " WHERE Clientuniqueid = '" . $client_id . "'"
        ;
        
        $rev = $this->db->query($quStr);
        //  echo $this->db->last_query();
        $result = $rev->result_array();
        return $result;
    }
    
    
    public function updateWallet($user_id = "", $updated_wallet_amt = "") {
        $queryStr = "UPDATE wallet SET amt = '" . $updated_wallet_amt . "' WHERE user_id = '" . $user_id . "'";
        $query = $this->db->query($queryStr);
    }
    
    
    public function insertTransWallet($data) {
        $insertdata = $this->db->insert('wallet_trans', $data);
        return $insertdata;
    }

    public function getAgentServicesCommission($user_id = "", $service_id = "", $amount = 0, $created_by="") {

        $this->db->select('u.id as user_id,u.agent_code,u.level_1,u.level_2,u.level_3,u.level_4,u.level_5,rsm.service_id as service_id,sc.id as commission_id,sc.values,sc.total_commission,sc.customer_charge,sc.upper_slab,sc.created_by');
        $this->db->from("users u");
        $this->db->join('retailer_service_mapping rsm', 'u.id= rsm.agent_id', 'left');
        $this->db->join('services_commissions sc', 'rsm.service_id = sc.service_id', 'left');
        $this->db->where('rsm.agent_id =', $user_id);
        $this->db->where('rsm.service_id =', $service_id);
        $this->db->where('sc.created_by =', $created_by);
        $this->db->where('sc.status', 'Y');
        // $this->db->where('sc.from >=', date('Y-m-d'));
        // $this->db->where('sc.till <=', date('Y-m-d '));  
        if($amount != 0){
            $between = '"' . $amount . '" between sc.lower_slab and sc.upper_slab';
            $this->db->where($between);
        }
        $user_result = $this->db->get();
        //echo $this->db->last_query();
        $user_result_array = $user_result->result_array();
        if (empty($user_result_array)) {
            $this->db->select('u.id as user_id,u.agent_code,u.level_1,u.level_2,u.level_3,u.level_4,u.level_5,rsm.service_id as service_id,sc.id as commission_id,sc.values,sc.total_commission,sc.customer_charge,sc.upper_slab,sc.created_by');
            $this->db->from("users u");
            $this->db->join('retailer_service_mapping rsm', 'u.id= rsm.agent_id', 'left');
            $this->db->join('services_commissions sc', 'rsm.service_id = sc.service_id', 'left');
            $this->db->where('rsm.agent_id =', $user_id);
            $this->db->where('rsm.service_id =', $service_id);
            $this->db->where('sc.created_by =', $created_by);
            $this->db->where('sc.status', 'Y');
            $this->db->where('sc.default_status', 'Y');
            if($amount != 0){
                $between = '"' . $amount . '" between sc.lower_slab and sc.upper_slab';
                $this->db->where($between);
            }
            $user_result = $this->db->get();
            $user_result_array = $user_result->result_array();
        }
         
        return $user_result_array;
    }

    public function saveCommissionData($data = "") {
        // print_r($data);exit;
        $this->db->insert('vas_commission_detail', $data);
        //echo $this->db->last_query();
    }

    public function getCustomerCharge($value)
    {
        $quStr = "select customer_charge"
        . " FROM services_commissions"
        . " WHERE '" . $value . "' between lower_slab and upper_slab"
        . " AND service_id = 4"
        ;

        $rev = $this->db->query($quStr);
        $result = $rev->result();
        return $result[0]->customer_charge;
    }

    //added by vaidehi 28jan19    
    public function transfer_limit($mob_no,$date){
      $queryStr = "SELECT sum(Amount) as Amount FROM `dmt_transfer_detail` 
          WHERE `CustomerMobileNo` = '$mob_no' and DATE_FORMAT(created_on, '%m-%Y') = '$date' and message LIKE '%Transaction Successful%' ORDER BY `id` DESC";
      $query =  $this->db->query($queryStr);
      $result = $query->result_array();
//      show($this->db->last_query(),1);
      return $result;
   }

    //Added by Lilawati for checking rd commission
    public function commission_from($user_id) {
        $query = "select id,level_3 from users where id= '".$user_id."' and role_id ='".RETAILER_ROLE_ID."'";
        $result = $this->db->query($query);
        return $result->row();
    }

     //Added by Lilawati for checking rd commission
    public function checkCommission($input,$created_by){        
        $service_id = Dmt;

        $LcSqlStr = "SELECT * from services_commissions 
        where status = 'Y'";
        $LcSqlStr .= " AND `values` NOT LIKE '{\"
        Trimax\":\"0\",\"Rokad\":\"0\",\"MD\":\"0\",\"AD\":\"0\",\"Distributor\":\"0\",\"Retailer\":\"0\"}'";
        //$LcSqlStr .= " AND `commission_name` LIKE '%" . str_replace(" ", "", $code) . "%'";
        if($input['Amount'] > 0) {
            $LcSqlStr .= " AND '". $input['Amount']."' between lower_slab and upper_slab";
        }
        $LcSqlStr .= " AND `created_by` = '". $created_by . "'";
        $LcSqlStr .= " AND service_id = '" . $service_id . "'"; 
        

        $query = $this->db->query($LcSqlStr);
       // show($this->db->last_query());
        return $query->result();
    }

/*Added by Vaidehi*/
    //Dmt settlement sheet 
      public function insert_dmt_sheet_data($batch_data){
        if (!empty($batch_data)) {
            $this->db->insert('dmt_settlement_sheet', $batch_data);
            return $this->db->affected_rows();
        }
    }
    
    public function check_data_exist($client_unique_Id,$date) {
        if(!empty($client_unique_Id)){
           $this->db->select('id');
           $this->db->from('dmt_settlement_sheet');
           $this->db->where('Clientuniqueid',$client_unique_Id);
           $this->db->where('date',$date);
           $query = $this->db->get();
//           show($this->db->last_query(),1);
           if ($query->num_rows() > 0) {
               return '1';
           } else
               return '0';
           }
    }
    
    public function insert_dmt_agent_data($batch_data)
    {
        if (!empty($batch_data)) {
            $this->db->insert_batch('dmt_agent_settlement', $batch_data);
            return $this->db->affected_rows();
        }
    }
    
    public function update_dmt_data($data){
        if(!empty($data)){
            $update_data = array(
            'cust_charge' => $data['customer_charge'],
            'user_id' => $data['user_id'],
            'final_amt' => $data['final_amount']    
            );
            $this->db->where('Clientuniqueid', $data['ClientUniqueID']);
            $this->db->update('dmt_settlement_sheet', $update_data);
        }       
    }
    
    public function get_trans_amount($start_date)
    {
        $sql = "SELECT user_id, SUM(final_amt) as fino_amount FROM dmt_settlement_sheet
            WHERE date = '$start_date'
            AND transfer_status = 'N'
            AND status IN ('REVERSAL SUCCESS','REVERSED')
            GROUP BY user_id";

        $query = $this->db->query($sql); 
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else{
            return FALSE;
        }
    }
    
      public function get_transfer_amount($limit = NULL, $start = NULL, $where = ''){
        $this->db->select('*');
        $this->db->from('dmt_settlement_sheet');
        if ($where) {
            $this->db->where($where);
        }
        $this->db->where_in('status',array('REVERSAL SUCCESS','REVERSED'));
        
        if (isset($start) && isset($limit)) {
            $this->db->limit($limit, $start);
        }
        $this->db->order_by('date', 'DESC');
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else
            return FALSE;
    }  
    
    public function get_user_amount_by_date($date){
        if ($date) {
            $queryStr = "SELECT * FROM dmt_settlement_sheet WHERE `date` = '$date'";
            $query = $this->db->query($queryStr);
            $result = $query->result_array();
            return $result;
        }
        return false;
    }
    
     public function dmtsettlement_update_transferstatus($transfer_status, $trans_no, $id)
    {
        $queryStr = "UPDATE dmt_settlement_sheet"
            . " SET transfer_status = '" . $transfer_status . "', transaction_no = '".$trans_no."'"
            . "WHERE id = '" . $id . "'";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();

        return $result;
    }
    
    public function update_data($id, $update_data)
    {
        $this->db->where('id', $id);
        return $this->db->update('dmt_settlement_sheet', $update_data);
    }
    
    public function get_data($client_id) {
        $this->db->select("dt.user_id,dt.customer_charge,dt.Amount,dt.Clientuniqueid");
        $this->db->from('dmt_transfer_detail dt');
        $this->db->where('dt.Clientuniqueid',$client_id);
        //show($this->db->last_query(),1);
        $res = $this->db->get(); 
        $detail = $res->result_array();
        if ($res->num_rows() > 0) {
               return $detail; 
        } 
    }
    
   public function update_refund_amt($id,$date) {
        $this->db->select("id,user_id,sum(final_amt) as total");
        $this->db->from('dmt_settlement_sheet');
        $this->db->where('date',"$date");
        $this->db->where('transfer_status',"Y");
        $this->db->where_in('id',  explode(',', rtrim($id,',')));
        $this->db->where_in('status',array('REVERSAL SUCCESS','REVERSED'));
        $this->db->group_by('user_id');
        $res = $this->db->get(); 
        
        //show($this->db->last_query(),1);
        $detail = $res->result_array();
        return $detail;  
    }
   
   public function get_agent_settlemnt_amt($date,$user_id) {
       $queryStr = "select id from dmt_agent_settlement where date = '$date' and user_id = '$user_id' and status ='N'";
       $query = $this->db->query($queryStr);
       $result = $query->result_array();
        //show($this->db->last_query(),1);
       return $result;
   }
   
   public function update_agent_amt($user_id,$total,$date) {
       $queryStr = "select id from dmt_agent_settlement where user_id = '$user_id' and date = '$date' and status ='N'";
       $query = $this->db->query($queryStr);
      
            if ($query->num_rows() > 0) { 
                $result = $query->result_array();
                $update_data = array(
                'amount_refunded' => $total,
                'status' => 'Y',
            );
            $this->db->where('id', $result[0]['id']);
            $this->db->update('dmt_agent_settlement', $update_data);
            //show($this->db->last_query(),1);
            return true;
        } else{ 
            return FALSE;
        }
   }

   //dmt receipt details
   public function getTransactionReceipt($user_id){
    $result = $this->db->query("select d.*, concat(u.first_name,' ',u.last_name) as agent_name,u.agent_code from dmt_transfer_detail d inner join users u on d.user_id=u.id where d.user_id='".$user_id."' order by id desc limit 1");
    return $result->row();
   }


    //insert inot Account verification by lilawati karale
    public function account_verify($data) {
        $insertdata = $this->db->insert('dmt_account_verification_detail', $data);
        return $insertdata;
    }

    public function getSubserviceId($service_id){
        $this->db->select("*");
        $this->db->from("sub_services");
        $this->db->where("service_id",$service_id);
        $this->db->where("service_name like '%DMT Account Verification%'");
        $user_result = $this->db->get();
        return $user_result->row();
    }

    public function getAgentServicesCommissionForAcctVerify($user_id = "", $service_id = "", $sub_service_id="", $created_by="") {

        $this->db->select('u.id as user_id,u.agent_code,u.level_1,u.level_2,u.level_3,u.level_4,u.level_5,rsm.service_id as service_id,sc.id as commission_id,sc.values,sc.total_commission,sc.customer_charge,sc.upper_slab,sc.created_by');
        $this->db->from("users u");
        $this->db->join('retailer_service_mapping rsm', 'u.id= rsm.agent_id', 'left');
        $this->db->join('services_commissions sc', 'rsm.service_id = sc.service_id', 'left');
        $this->db->where('rsm.agent_id =', $user_id);
        $this->db->where('rsm.service_id =', $service_id);
        $this->db->where('sc.created_by =', $created_by);
        $this->db->where('sc.status', 'Y');
        $this->db->where('sc.sub_service_id', $sub_service_id);
        // $this->db->where('sc.from >=', date('Y-m-d'));
        // $this->db->where('sc.till <=', date('Y-m-d '));  
        /*if($amount != 0){
            $between = '"' . $amount . '" between sc.lower_slab and sc.upper_slab';
            $this->db->where($between);
        }*/
        $user_result = $this->db->get();
        //echo $this->db->last_query();
        $user_result_array = $user_result->result_array();
        if (empty($user_result_array)) { 
            $this->db->select('u.id as user_id,u.agent_code,u.level_1,u.level_2,u.level_3,u.level_4,u.level_5,rsm.service_id as service_id,sc.id as commission_id,sc.values,sc.total_commission,sc.customer_charge,sc.upper_slab,sc.created_by');
            $this->db->from("users u");
            $this->db->join('retailer_service_mapping rsm', 'u.id= rsm.agent_id', 'left');
            $this->db->join('services_commissions sc', 'rsm.service_id = sc.service_id', 'left');
            $this->db->where('rsm.agent_id =', $user_id);
            $this->db->where('rsm.service_id =', $service_id);
            $this->db->where('sc.created_by =', $created_by);
            $this->db->where('sc.status', 'Y');
            $this->db->where('sc.default_status', 'Y');
            $this->db->where('sc.sub_service_id',$sub_service_id);
          /*  if($amount != 0){
                $between = '"' . $amount . '" between sc.lower_slab and sc.upper_slab';
                $this->db->where($between);
            }*/
            $user_result = $this->db->get();
            $user_result_array = $user_result->result_array();
        }
         
        return $user_result_array;
    }

    public  function acctVerifyUpdate($data) {

        $update = date('Y-m-d H:m:s');
        $update_data = array(
            'message' => $data['MessageString'],
            'RequestID' => $data['ResponseData'],
            'update_on' => $update
        );
        $this->db->where('Clientuniqueid', $data['ClientUniqueID']);
        $this->db->update('dmt_account_verification_detail', $update_data);
    }

    public function check_acct_exists($BeneAccountNo){
        $result = $this->db->query("select a.* from dmt_account_verification_detail a left join dmt_transfer_detail d on a.BeneAccountNo=d.BeneAccountNo where a.BeneAccountNo='".$BeneAccountNo."' limit 1");
        return $result->result_array();
    }

     // dmt account verification receipt details
    public function getTransactionReceiptForAcctVerify($user_id){
        $result = $this->db->query("select d.*, concat(u.first_name,' ',u.last_name) as agent_name,u.agent_code from dmt_account_verification_detail d inner join users u on d.user_id=u.id where d.user_id='".$user_id."' order by id desc limit 1");
        return $result->row();
   }
   
     public function getaccountDetails($BeneAccountNo){
        $result = $this->db->query("select * 
            from dmt_account_verification_detail where BeneAccountNo = '".$BeneAccountNo."' order by id desc limit 1");
        return $result->row();
   }
   
}
