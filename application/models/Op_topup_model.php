<?php
class Op_topup_model extends MY_Model {
    var $table  = 'op_topup';
    var $fields = array("id", "user_id", "available_balance", "available_commission", "usable_balance", "provider_flag", "service_type", "is_status", "is_deleted", "created_by", "created_date", "updated_by", "updated_date");
    var $key    = "id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
    
    /*
     * Function to get list of payment type
     */
    function get_payment_type() {
        $paymentTypeRes = $this->db->select('id, payment_type')->where('flag','o')->get('op_payment_type');
        if($paymentTypeRes->num_rows()>0){
            return $paymentTypeRes->result_array();
        }
        return false;
    }
    
    /*
     * Function to get list of payment mode
     */
    function get_payment_mode($payment_type_mode) {
        $paymentModeRes = $this->db->select('id, payment_mode')->where('op_payment_type_id',$payment_type_mode)->get('op_payment_mode');
        if($paymentModeRes->num_rows()>0){
            return $paymentModeRes->result_array();
        }
        return false;
    }
    
    /*
     * Get topup data
     */
    
    function get_topup_data($topup_id) {
        $this->db->select('otp.id,otc.id as op_trans_id, om.op_name,otp.op_id,otp.amt,otc.payment_type,otc.payment_mode,otc.depositor_bank_name,otc.depositor_acc_no ,otc.op_bank_name,otc.op_acc_no, otc.remark, otc.transaction_datetime,otc.transaction_id,otp.status, otc.transaction_datetime');
        $this->db->from('op_topup_consume_trans otc');
        $this->db->join("op_topup otp", 'otp.id=otc.op_topup_id');
        $this->db->join("op_payment_type pt", 'pt.id=otc.payment_mode','left');
        $this->db->join("op_payment_mode pm", 'pm.id=otc.payment_mode','left');
        $this->db->join("op_master om", 'om.op_id=otp.op_id');
        $this->db->where('otp.id',$topup_id);
        $query = $this->db->get();
        if($query->num_rows()>0){
            return $query->row_array();
        }
     return false;   
    }
    function topup_trans_datails($trans_id) {
        $this->db->select('om.op_name,otc.amt, otc.amt_before_trans, otc.amt_after_trans, pt.payment_type,pm.payment_mode,otc.depositor_bank_name,otc.depositor_acc_no ,otc.op_bank_name,otc.op_acc_no,otc.transaction_id,otc.created_date');
        $this->db->from('op_topup_consume_trans otc');
        $this->db->join("op_topup otp", 'otp.id=otc.op_topup_id');
        $this->db->join("op_payment_type pt", 'pt.id=otc.payment_mode','left');
        $this->db->join("op_payment_mode pm", 'pm.id=otc.payment_mode','left');
        $this->db->join("op_master om", 'om.op_id=otp.op_id');
        $this->db->where('otc.op_topup_id',$trans_id);
        $this->db->order_by('otc.id','DESC');
        $query = $this->db->get();
        if($query->num_rows()>0){
            return $query->result_array();
        }
        return false;
    }
    
    function getAgentDetails($op_id, $service_type) {
        $this->db->select("u.first_name, ot.available_balance, ot.available_commission, ot.usable_balance");
        $this->db->from('op_topup ot');
        $this->db->join('users u','ot.user_id=u.id');
        $this->db->where(array('ot.user_id'=>$op_id,'ot.service_type'=>$service_type));
        $opData = $this->db->get();  
        if($opData->num_rows()>0) {
            return $opData->row_array();
        }
        return false;
    }
    
    
function check_approved_topup() {
	
	$this->load->model('cb_op_role_map_model');
	  $aOperator = array();
	  $user_roleid = $this->session->userdata('role_id');
	 // $user_roleid = 21; // need to comment this line
	  if($user_roleid != '')
	  {
		  $aOperator = $this->cb_op_role_map_model->get_operator_list($user_roleid);
	  }

    $allData = $this->db->select('op_trans_id')->get('op_trans_details');
    if($allData->num_rows()!=0){
        $this->db->select('op_trans_id ');
        $this->db->where('is_status','0');
        $this->db->where('is_deleted','0');
		if(!empty($aOperator))
		{
			$this->db->where_in('user_id',$aOperator);
		}
       /* if($this->session->userdata('role_name')=='Operator' || $this->session->userdata('role_name'=='API Provider')){
            $this->db->where('user_id',$this->session->userdata('user_id'));
        }*/
        $notApporovedData = $this->db->get('op_trans_details');
        if($notApporovedData->num_rows()>0) {
            return "pending";
        } 
        else
        {
            return "approved";
        }
    }
    return "approved";
    
    
}
    function get_api_provider() {
        $res = $this->db->select('id,name')->where(array('is_status'=>'1', 'is_deleted'=>'0'))->get('api_provider');
        
        if($res->num_rows()>0){
            return $res->result_array();
        }
        return false;
    }
    
    function get_services($service_id=NULL) {
        $this->db->select('id,name');
        $this->db->where(array('status'=>'1', 'deleted'=>'0'));
        if($service_id) {
            $this->db->where('id',$service_id);
        }
        $res = $this->db->get('services');
        if($res->num_rows()>0){
            return $res->result_array();
        }
        return false;
    }
    
}
?>
