<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Agent_docs_master_model extends MY_Model {
    
	var $table  = 'agent_docs_master';
    var $fields = array("agent_docs_id","agent_id","docs_cat","docs_name","comment","approver_name","large_img_link","thum_img_link","is_status","is_deleted","created_by","created_date","updated_by","updated_date");
    var $key    = 'agent_docs_id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
    
}
// End of Agent_model class