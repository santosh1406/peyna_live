<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of access_permission_model
 * 
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Access_permission_model extends CI_Model {
    /*
     * @function    : get_menu_permission
     * @param       : 
     * @detail      : To fetch all menu permission.
     */

    function get_menu_permission($role_id) {
        if ($role_id != "") {
            $this->db->select("rm.*,m.display_name");
            $this->db->from("role_menu_rel rm");
            $this->db->join('menu m', 'm.id = rm.menu_id', "inner");
            $this->db->where("rm.role_id = " . $role_id);
            $permission_result = $this->db->get();
            $permission_array = $permission_result->result_array();
            return $permission_array;
        }
    }

// get_menu_permission End
}

// End of Access_permission_model class