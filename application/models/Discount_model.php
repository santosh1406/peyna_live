<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Discount_model extends MY_Model {

    protected $table    = 'discount_master';
    var $fields = array("id","unique_id","name","description","effective_from","effective_to","type_discount","discount_on","disc_value","min_ticket_value","max_dis_availed","is_transaction_based","transaction_no","is_operator_based","op_id","is_bus_type_based","bus_type","is_ticket_count_based","no_of_tickets","is_generic_coupon_based","is_freq_user","generic_coupon_code","period","no_of_transaction","coupon","created_by","inserted_date","status","is_deleted");
    var $key    = 'id'; 
   
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

	public function get_all()
	{
		return $this->db->get($this->table);
	}

}
