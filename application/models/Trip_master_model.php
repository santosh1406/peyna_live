<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Trip_master_model extends MY_Model
{
    /**
     * Instanciar o CI
     */
    var $table 	= 'trip_master';
    // var $fields     = "trip_no,op_trip_no,op_id,route_no,bus_type_id,op_bus_type_id,seat_layout_id,route_no_name,effective_from,effective_till,trip_type,refund_permitted,fare_type,sch_dept_time,sch_dept_loc,trip_running_time,operator_id,commision_type";
    var $fields		= array("trip_no","op_trip_no","op_id","provider_type","provider_id","route_no","bus_type_id","op_bus_type_id","seat_layout_id","from_stop_name","from_stop_cd","to_stop_name","to_stop_cd","departure_time","arrival_time","route_no_name","effective_from","effective_till","trip_type","refund_permitted","fare_type","sch_dept_time","sch_dept_loc","trip_running_time","operator_id","commision_type","status");
    var $key        = "trip_no";

    public function __construct()
    {
        parent::__construct();
        $this->_init();
    }   

    
    
    function get_service_count()
    {
        $where = array(
                        'op_trip_no' => $this->op_trip_no ,
                        'op_id' => $this->op_id 
                       );
        
        
        $this->db->where($where);
        return $this->db->count_all_results($this->table);
        
        
    }
    
    function get_trip_detail($trip_no)
    {
        $this->db->where('trip_no', $trip_no);
        $query = $this->db->get($this->table);
        
        $data = $query->row_array();
        
        return $data;
    }
         
    
   
      
	
}
