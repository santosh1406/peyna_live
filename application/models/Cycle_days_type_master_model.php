<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cycle_days_type_master_model extends MY_Model {

    var $table  = 'cycle_days_master';
    var $fields = array("cycle_days_id","cycle_days_name","cycle_days_type_id","is_status","is_deleted","created_by","created_date","updated_by","updated_date");
    var $key    = 'cycle_days_id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
    public function all_select(){
        $this->db->select('cycle_days_type_id,cycle_days_type_name,is_status,is_deleted');
        $this->db->where("is_status", 'Y');
        $this->db->where("is_deleted", 'N');
        $this->db->from('cycle_days_type_master');
        $query = $this->db->get();
        $data = $query->result_array();
        return $data;
    
        
    }
    
    public function show_all_data($input){
       $this->db->select('cycle_days_id,cycle_days_name');
       $this->db->from("cycle_days_master");
       $this->db->where('cycle_days_type_id', $input['cycle_id']);
       $query = $this->db->get();
        return $query->result_array();
    }

}



