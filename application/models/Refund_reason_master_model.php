<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of users
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Refund_reason_master_model extends MY_Model {
    
    var $table  = 'refund_reason_master';
    var $fields = array("reason_id","reason","reason_desc","is_status","is_deleted");
    var $key    = 'reason_id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

   
     function get_status()
    {
        $this->db->select('reason_id,reason');
        $this->db->order_by('reason_id');
        $data = $this->db->get($this->table);
        if($data->num_rows()>0)
        {
            return $data->result_array();
        }
        return false;
    }
}

