<?php

class City_master_model extends MY_Model {

    var $table = 'city_master';
    var $fields = array("intCityId", "intCountryId", "intStateId","stState","stCity");
    var $key = "intCityId";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }
    
    function get_city($state_id)
    {
        $this->db->select('intCityId,stCity');
        if($state_id != '')
        {
            $this->db->where('intStateId',$state_id);
        }
        $this->db->order_by('stCity');
        
        $data = $this->db->get($this->table);
        if($data->num_rows()>0)
        {
            return $data->result_array();
        }
        return false;
    }

    public function get_insert_city_by_name($name, $country_id, $state_id)
    {
        if($name != '')
        {
            $filter_arr = array(
                                'stCity'        => $name, 
                                'intCountryId'  => $country_id,
                                'intStateId'    => $state_id
                            );

            $city = $this->where($filter_arr)
                          ->find_all();
            if($city)
            {
                return $city[0]->intCityId;
            }
            else
            {
                return $this->insert($filter_arr);
            }
        }
        else
        {
            return false;
        }
    }
	
	function get_all_city()
    {
        $this->db->select('intCityId,stCity');
        $this->db->order_by('stCity');
        $data = $this->db->get($this->table);
        if($data->num_rows()>0)
        {
            return $data->result_array();
        }
        return false;
    }

    function get_city_by_id($cityID)
    {
        $this->db->select('stCity');
        $this->db->from('city_master');
        $this->db->where('intCityId',$cityID);
        $data = $this->db->get();
        $data= $data->result_array();
        if(!empty($data))
        {
            $city_name = $data[0]['stCity'] ;           
        }
        return $city_name;
    }
}    
