<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api_bus_services_model extends MY_Model {

    /**
     * Instanciar o CI
     */
    var $table = 'api_bus_services';
    var $fields = array("id","op_name","from","to","date","bus_service");

    public function __construct()
    {
        parent::__construct();
        $this->_init();
    }
}
