<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Payment_gateway_commission_detail_model extends MY_Model {

    var $table = 'payment_gateway_commission_detail';
    var $fields = array("id", "commission_id", "payment_gateway_id", "commission_from", "commission_to", "commission_type", "commission_value", "payment_gateway_commission", "bos_commission", "total_commission", "added_on", "added_by");
    var $key = 'id';
    protected $set_created = false;
    protected $set_modified = false;

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

}

?>
