<?php

class Booking_agent_model extends MY_Model {

    var $table  = 'agent_master';
    var $fields = array("agent_id","agent_code" ,"fname","parent_agent_id" ,"profile_img","added_by","user_id","agent_hierarchy","parent","level","level1","level2","level3","level4","mname", "lname","dob","address", "gender","bank_acc_name" ,"email", "contact_no", "pincode","country_id","state_id","city_id" ,"level_id","added_by","is_status", "is_deleted", "created_by", "created_date", "updated_by", "updated_date");
    var $key    = "agent_id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    function get_agent($agid) 
    {
        $this->db->select("*");
        $this->db->where('agent_id', $agid);
        $res1 = $this->db->get('agent_master');

        return $res1->result_array();
    }
        

    function get_ag_detail($agid,$level) {
       $this->db->select('am.agent_id,am.fname,am.mname,am.lname,am.address,am.gender,am.profile_img,am.dob,abd.bank_acc_name,am.email,am.country_id,am.state_id,am.city_id,am.contact_no,am.pincode,abd.bank_acc_no,abd.bank_name,abd.bank_branch,abd.bank_ifsc_code,am.level,am.level1,am.level2,am.level3,am.level4');
       $this->db->join("agent_bank_detail abd",'am.agent_id=abd.agent_id',"left") ;
       $this->db->where('am.agent_id',$agid);
       $this->db->where('level',$level);
       $this->db->from('agent_master am');
       $res=$this->db->get();
       return $res->result_array();
    }

    function mail_exist($email) {
        $this->db->where('email',$email);
        $query = $this->db->get('agent_master');
        return $query->num_rows();
          
    }

    function getag_names($agid)
    {
        $this->db->select('agent_id,agent_fname');
       $this->db->from('agent_master');
       $this->db->where_in('agent_id',$agid);
       $data=$this->db->get();
       return $data->result_array();
    }
    
    function doc_name($id)
    {
      $this->db->select("agent_docs_list,category,docs_name");
      $this->db->where('category',$id);
      
      $this->db->from('agent_docs_list');
      $data=$this->db->get();
      return $data->result_array();        
    }

    function get_doc_by_category_id($id)
    {

      $this->db->select("agent_docs_list,docs_name");
      $this->db->where('category',$id);      
      $this->db->from('agent_docs_list');
      $result = $this->db->get();

      $data = array();

      foreach ($result->result_array() as $key => $value) {
          $data[$value['agent_docs_list']] = $value;
      }

      return $data;        
    }

    function save_agent_docs_detail($data)
    {
  
        $this->db->insert('agent_docs_master', $data); 
        
    
    }
    
     function save_agent_docs_detail_batch($data)
    {
  
        $this->db->insert_batch('agent_docs_master', $data);
    
    }
    
    
    function update_agent_doc_detail($upd_data,$where_cla)
        {
                      
        
        $this->db->where($where_cla);
        $this->db->update('agent_docs_master',$upd_data);      
        }
   
     function check_doc_exists($doc_id,$docs_cat,$agent_id)
     {
         $this->db->select('*');
         $this->db->from('agent_docs_master');
         $this->db->where('docs_name',$doc_id);
         $this->db->where('docs_cat',$docs_cat);
         $this->db->where('agent_id',$agent_id);
         
         $data=$this->db->get();
         $res=$data->result_array();
      
         if(empty($res) || count($res)==0)
         {
             return false;
         }
         else
         {
             return true;
         }
         
     }
        
    function get_agent_data($id)
    {
        $this->db->where('adm.agent_id',$id);
        $this->db->join('agent_docs_list adl','adm.docs_name=adl.agent_docs_list','inner');
        $this->db->where('adm.is_status',1);
        $this->db->where('adm.is_deleted',0);
        $this->db->from('agent_docs_master adm');
        $res=$this->db->get();
        return $res->result_array();     
    }
    
    function get_image_name($doc_id,$ag_id)
    {
        $this->db->select('*');
        $this->db->from('agent_docs_master');
        $this->db->where('agent_id',$ag_id);
        $this->db->where('docs_name',$doc_id);
        $this->db->where('is_status',1);
        $this->db->where('is_deleted',0);
        $data=$this->db->get();
        return $data->result_array();
        
        
    }
    
    
    function delete_doc($doc_nm,$id)
    {
        
       
        $data=array("is_status"=>0,"is_deleted"=>1,"updated_by"=>$this->session->userdata('user_id'));
        $this->db->where('docs_name=',$doc_nm);
        $this->db->where('agent_id=',$id);
        $this->db->update('agent_docs_master',$data);
    }
    function delete_agent($id)
    {
        $data=array("is_deleted"=>1,'is_status'=>0);
        $this->db->where('agent_id=',$id);
        $this->db->update('agent_master',$data);
       
    }
    function  generate_agent_id()
    {
        $last_id = $this->get_last_id();
        
        return $code = "AGT".str_pad(($last_id+1),5,0, STR_PAD_LEFT);
        
    }
    
     /* Queries for API starts Here */
    
    /*
     * Check agent code exists
     */
    
    function check_agent_code_available($agent_code) {
        $this->db->select('user_id');
        $this->db->from('agent_master');
        $this->db->where(array('agent_code'=>$agent_code, 'is_status'=>'1', 'is_deleted'=>'0'));
        $agentCodeRes = $this->db->get();
        if($agentCodeRes->num_rows()>0) {
            return $agentCodeRes->row_array();
        }
        return false;
    }
     /* Queries for API Ends Here */
    
     function get_config($id)
    {
        $this->db->from('agent_config ac');
         $this->db->join('agent_master am','ac.agent_id=am.agent_id','right');
        $this->db->where('am.agent_id',$id);
        $data=$this->db->get();
        return $data->result_array();
    }
     function update_config($data,$id)
    {
       
         $this->db->where("id",$id);
         $this->db->update("agent_config",$data);
    }
    
    
    function save_config($data)
    {
       
        $this->db->insert("agent_config",$data);
    }
    
    function get_agent_detail($agid) {
       $this->db->select('am.agent_id,am.fname,am.mname,am.lname,am.address,am.gender,am.profile_img,am.dob,abd.bank_acc_name,am.email,am.country_id,am.state_id,am.city_id,am.contact_no,am.pincode,abd.bank_acc_no,abd.bank_name,abd.bank_branch,abd.bank_ifsc_code,am.level,am.level1,am.level2,am.level3,am.level4');
       $this->db->join("agent_bank_detail abd",'am.agent_id=abd.agent_id',"left") ;
       $this->db->where('am.agent_id',$agid);
     
       $this->db->from('agent_master am');
       $res=$this->db->get();
       return $res->result_array();
    }
}
?>
