<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Payid_summary_details_model extends MY_Model
{
    var $table    = 'payid_summary_details';
    var $fields		= array("id","pay_id","amt_payable","bill_email
","fees","bill_name","order_no","currency","amount","tax","ccavenue_ref_no","bank_ref_no","date_time","txn_type","sub_acc_id","error_code","error_desc","inserted_date");
    var $key    = 'id';

    protected $set_created = false;

    protected $set_modified = false;
    
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
 
  public function insert_payid_data($payid_data)
 {
 	 if (!empty($payid_data))
        {
              $query = $this->db->get_where('payid_summary_details', 
                array('pay_id' => $payid_data['pay_id'] )); 
              $count = $query->num_rows();
              if ($count === 0) 
              {
                if($this->db->insert('payid_summary_details', $payid_data))
                  {
                    return true;
                  }
              }
        }
        else
        {
            return false;
        } 
    }  
}


