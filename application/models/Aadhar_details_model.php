<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Aadhar_details_model extends MY_Model {
    
    var $table  = 'aadhar_details';
    var $fields = array(
	"id","user_id","aadhaar_id","photo_base64","photo_binary","name","dob","gender","phone","email","father_name","house_num","landmark","colony_name","subdistrict",
	"district","state","country","pincode","post_office_name","local_data","raw_CmpResp","aadhaar_reference_code","hash","uuid","requestId","date_added"	
	);
    var $key    = 'id';
	
	public function __construct() {
        parent::__construct();
        $this->_init();      
    }
	
	public function is_user_exists($user_id) {		
		$this->db->select('id');
		$this->db->from('aadhar_details');
		$this->db->where('user_id', $user_id);		
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->row_array();            
	}
}