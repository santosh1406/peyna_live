<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Refund_request_history_Model extends MY_Model {
    
    var $table  = 'refund_request_history';
    var $fields = array("id","refund_req_id","reason_id","comment","inserted_date","inserted_by","is_status","is_deleted","username");
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();  
       }
       
       
    /* @Author     : Sagar Bangar
    * @function    : insert_refund_request_history
    * @param       : request_type, email, ticket_id
    * @detail      : check refund request
    *                 
    */
    public function insert_refund_request_history($insert_data){
     if (!empty($insert_data)){
         if ($this->db->insert($this->table, $insert_data)) {
             return $this->db->insert_id(); 
         }
         else {
             return false;
         }
     }
     else{
         return false;
     }
    }
    
    /* @Author     : Sagar Bangar
    * @function    : get_refund_request_history
    * @param       : request_type, email, ticket_id
    * @detail      : check refund request
    *                 
    */
    public function get_refund_request_history($id){
        $this->db->select("comment, inserted_date, rrm.reason");
        $this->db->from($this->table);
        $this->db->join('refund_reason_master rrm', 'rrm.reason_id = refund_request_history.reason_id', "left");
        $this->db->where("refund_request_history.refund_req_id",$id);
     
        $query = $this->db->get();
        // /show($this->db->last_query(),1);
        return $query->result_array();
        }
}
?>