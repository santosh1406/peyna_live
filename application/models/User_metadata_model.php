<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of users
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_metadata_model extends MY_Model {
    
    var $table  = 'user_metadata';
    var $fields = array("id","user_id","user_key","user_value","timestamp");
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
   
}
