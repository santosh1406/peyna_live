<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Aeps_transaction_model extends MY_Model
{    
    public function __construct()
    {
        parent::__construct();
        $this->_init();
    }

    public function insert_data($table,$data){
        if($data['SERVICEID']==AEPS_CASH_WITHDRAW)
        {
            $data['Service_name'] = 'Cash Withdraw';
        }
        elseif($data['SERVICEID']==AEPS_BALANCE_ENQUIRY)
        {
            $data['Service_name'] = 'Balance Enquiry';
        }
        elseif($data['SERVICEID']==AEPS_TRANSACTION_STATUS)
        {
            $data['Service_name'] = 'Transaction Status';
        }
        
        $data['created_by'] = $this->session->userdata('user_id');
        
        $this->db->insert($table, $data);   
        return $this->db->insert_id();
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/aeps/' . 'aeps.log', $data, 'Input-Operator');
    }
    
    public function get_last_trans_id() {
        $this->db->select('id');
        $this->db->where('created_by', $this->session->userdata('user_id'));
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('aeps_request', 1, 0);

        // echo $this->db->last_query();
        return $query->row(0);
    }
    
    public function getTransactionReceipt($user_id,$limit='1', $start='0'){
        $this->db->select('treq.id,u.display_name,u.username,tres.Amount,tres.RRN,treq.Service_name,wt.transaction_no');
        $this->db->from('aeps_request treq');
        $this->db->join('aeps_response tres','treq.id = tres.Transaction_id','inner');  
        $this->db->join('vas_commission_detail cd','tres.created_by = cd.user_id','inner');
        $this->db->join('users u','u.id = cd.user_id','inner');
        $this->db->join('wallet_trans wt','wt.transaction_no = tres.WalletTransTxnNo','inner');
        $this->db->where('tres.created_by',$user_id);      
        $this->db->where('treq.SERVICEID',AEPS_CASH_WITHDRAW);
        $this->db->where('cd.service_id',AEPS_SERVICE_ID);
        $this->db->where('tres.DispalyMessage','AEPS Transaction Success');
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        //last_query(1);
       // echo $this->db->last_query();
        return $query->result_array();  
    }
   
}



