<?php

class Otc_card_inventory_model extends MY_Model{
    
    public function getAgentCode($user_id)
    {
        $this->db->select('agent_code');
        $this->db->from('users');
        $this->db->where('id',$user_id);
        
        $query = $this->db->get();
    	$result = $query->result_array();
    	return $result[0]['agent_code'];
        
    }
    
    public function getDepotcode($user_id){
        $this->db->select('depot_code');
        $this->db->from('retailer_service_mapping');
        $this->db->where('agent_id',$user_id);        
        $this->db->where('service_id',SMART_CARD_SERVICE);

        
        $query = $this->db->get();
        //echo $this->db->last_query();
    	$result = $query->result_array();
    	return $result[0]['depot_code'];
    }
    
    public function insert_data($table,$data){
        $this->db->insert($table,$data);
    }
    
    public function insert_trans_data($data){
        $this->db->insert('otc_invenory_transaction',$data);
    }
    
    public function getOtcInvenoryTransaction($agent_id){
        $this->db->select('user_id,total_cards');
        $this->db->from('otc_invenory_transaction');
        $this->db->where('user_id',$agent_id);
        $query = $this->db->get();
        //echo $this->db->last_query();
    	return $result = $query->result_array();
    	
    }
    
    public function UpdateCardsByUser($data){
        
        //$this->db->set('total_cards',$data['total_cards']);
        $this->db->where('user_id',$data['user_id']);
        $this->db->update('otc_invenory_transaction',$data);
    }
    
    //added by sonali for total cards on 1-april-19
    public function getTotalCards($input){
        
        $from_userid = $input['apploginuser'];
        $this->db->select('total_cards');
        $this->db->from('otc_invenory_transaction');
        $this->db->where('user_id',$from_userid);
        $query = $this->db->get();
        //echo $this->db->last_query();
        if ( $query->num_rows() > 0 )
        {
            $result = $query->result_array();            
            return $result[0]['total_cards'];
        }else{
            return '0';
        }
    	
         
    }
    

            
           
}

