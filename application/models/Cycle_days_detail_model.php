<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cycle_days_detail_model extends MY_Model {

    var $table  = 'wallet_trans';
    var $fields = array("cycle_days_detail_id","cycle_days_id","cycle_day","is_status","is_deleted","created_by","created_date","updated_by","updated_date");
// var $fields = "id,w_id,user_id,amt,status,expire_at,comment,added_by,added_on";
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

}



