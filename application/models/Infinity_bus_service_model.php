<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Infinity_bus_service_model extends MY_Model {

    /**
     * Instanciar o CI
     */
    var $table = 'infinity_bus_service';
    var $fields = array("id","from","to","date","bus_service","timestamp","datetime");

    public function __construct()
    {
        parent::__construct();
        $this->_init();
    }
}
