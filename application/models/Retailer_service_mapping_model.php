<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Project            : Rokad
// Version            : 3
// CR ID            : BG 20241 - 3.1, BG 20241 - 3.2
// Author            : Sachin Chavan
// Created on            : 01-08-2018
//
// Srl.        Date        Modified By            Why & What

//01    01-08-2018        <sachin chavan>         1. change in login API to return service asigned to agent(BG 20241 - 3.1).
//02    02-08-2018        <sachin chavan>         1. Service Master API to return services details(BG 20241 - 3.2).
//                                                                                                                                                              
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Retailer_service_mapping_model extends MY_Model {
    
    var $table  = 'retailer_service_mapping';
    var $fields = array("id","agent_id" ,"service_id","terminal_id","created_by","updated_by","depot_code","division_code");
    var $key    = "id";
 
    public function __construct() {
        parent::__construct();
        $this->_init();
    }
    public function saveServiceDetails($data)
    {
       $this->db->insert('retailer_service_mapping', $data);
       log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'retailer_service_mapping.log', $data, 'retailer_service_mapping');
    }
    //added by harshada kulkarni on 13-07-2018 for CR 335 -start
    public function saveServiceDetailsLog($data)
    {
       $this->db->insert('retailer_service_mapping_log', $data);
    }
    //added by harshada kulkarni on 13-07-2018 for CR 335 -end
    
    
    /**
        * @description - To get service details by agent id
        * @param $agentId user ID
        * @return array - of service ID
        * @added by Sachin Chavan on 01-08-2018 For BG 20241 - 3.1
    */
    function getServiceByAgentId($agentId){
        $this->db->select('service_id');
        $this->db->from('retailer_service_mapping');
        $this->db->where('agent_id',$agentId);   
        $this->db->where('status','Y');


        $returnArr = $this->db->get()->result_array();
        //echo $this->db->last_query();
        return $returnArr;
    }
    
    /**
        * @description - To get service details for API
        * @param $agentId user ID
        * @return string - comma separated service IDs
        * @added by Sachin Chavan on 01-08-2018 For BG 20241 - 3.1
    */
    function getServicesForApiByAgentID($agentId){
        $serviceString = '';
        $serviceArr = $this->getServiceByAgentId($agentId);
        if(count($serviceArr) > 0){
            foreach($serviceArr as $val){
                $serviceString .= $val['service_id'].',';
            }
            $serviceString = rtrim($serviceString,',');
        }
        return $serviceString;
    }
    
    /**
        * @description - To get service details
        * @param $serviceId Service ID [optional]
        * @return array - service details
        * @added by Sachin Chavan on 02-08-2018 For BG 20241 - 3.2(Services API)
    */
    function getServicesDetails($serviceId = ''){
        $this->db->select('id,service_name,service_detail,status');
        $this->db->from('service_master');
        if($serviceId != ''){
            $this->db->where('id',$serviceId);
        }
        $returnArr = $this->db->get()->result_array();
        return $returnArr;
    }
  }  
  ?>
