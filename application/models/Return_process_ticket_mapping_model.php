<?php

class Return_process_ticket_mapping_model extends MY_Model {

    /**
     * Instanciar o CI
     */
    var $table  = 'return_process_ticket_mapping';
    var $fields = array("id", "process_ticket_id" ,"return_process_ticket_id", "timestamp");
    var $key    = "id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }


    public function get_rpt_mapping($parent_ticket_id)
    {
        if ($parent_ticket_id != "") {
            $this->db->select("pt.*");
            $this->db->from("return_process_ticket_mapping rptm");
            $this->db->join('processed_tickets pt', 'pt.ticket_id = rptm.return_process_ticket_id', 'inner');
            $this->db->where("rptm.process_ticket_id = '" . $parent_ticket_id . "'");
            $query = $this->db->get();
            $result = $query->result();
            return $result;
        }
    }

}

?>
