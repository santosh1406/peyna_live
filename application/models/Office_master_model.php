<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Office_master_model extends MY_Model {

    var $table = 'office_master';
    var $fields = array("id", "office_name","office_code","user_id", "address", "head_office", "state", "city","is_status", "is_deleted");
    var $key = "id";

    public function __construct() {
        parent::__construct();
        $this->_init();
    }

    public function get_office_agentwise($user_id) {
        $this->db->select("of.id,of.office_name,of.office_code,of.address,of.head_office,of.state,of.city,of.fund_limit,of.is_status,of.is_deleted");
        $this->db->from("office_master of");
        $this->db->join('users u', 'of.user_id = u.id', "left");
        $this->db->where("u.role_id", ORS_AGENT_ROLE_ID);
        $this->db->where("of.user_id", $user_id);
        $this->db->where("of.is_status", "Y");
        $this->db->where("of.is_deleted", "N");
        $office_result = $this->db->get();
        $office_result_array = $office_result->result_array();                        
        return $office_result_array;
    }
    
    public function get_office_detail($id) {
        $this->db->select("of.id,of.office_name,of.office_code,of.address,of.head_office,of.state,of.city,of.fund_limit_flag,of.fund_limit,of.is_status,of.is_deleted");
        $this->db->from("office_master of");
        $this->db->where("of.id", $id);
        $office_result = $this->db->get();
        $office_result_array = $office_result->row_array();
        return $office_result_array;
    }
}
?>
