<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Fund_request_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function LoadFundDataTable($sWhere, $sOrder, $sLimit) {
        $lcSqlsStr = "SELECT id,DATE_FORMAT(created_date,'%d-%m-%Y %H:%i:%s') as Date,amount,topup_by,transaction_remark,is_approvel,user_id,request_touserid from wallet_topup ";
        $lcSqlsStr .= "$sWhere $sOrder $sLimit";
		//echo $lcSqlsStr;
        $query = $this->db->query($lcSqlsStr);

        $data['ResultSet'] = $query->result_array();

        $data['iFilteredTotal'] = count($data['ResultSet']);

        /* Total data set length */
        $sQuery = " SELECT COUNT(id) as countRid  FROM wallet_topup $sWhere";
        $query = $this->db->query($sQuery);
        $ResultSet = $query->result_array();
        $data['iTotal'] = $ResultSet[0]['countRid'];
        return $data;
    }
	
	public function getFundRequestData($id){
		$sQuery="SELECT request_touserid,DATE_FORMAT(created_date,'%d-%m-%Y %H:%i:%s') as Date,user_id,amount,topup_by,bank_name,bank_acc_no,transaction_no,transaction_date,transaction_remark,transaction_status,is_approvel,reject_reason FROM wallet_topup WHERE id =".$id."";
        $query = $this->db->query($sQuery);
        return $query->row(); 
	}
	
    public function LoadFundApprovalDataTable($sWhere, $sOrder, $sLimit) {
        $lcSqlsStr = "SELECT id,DATE_FORMAT(created_date,'%d-%m-%Y %H:%i:%s') as Date,amount,user_id,request_touserid,topup_by,transaction_remark,is_approvel FROM wallet_topup ";
        $lcSqlsStr .= "$sWhere $sOrder $sLimit";
		//echo $lcSqlsStr;
        $query = $this->db->query($lcSqlsStr);

        $data['ResultSet'] = $query->result_array();

        $data['iFilteredTotal'] = count($data['ResultSet']);

        /* Total data set length */
        $sQuery = " SELECT COUNT(id) as countRid  FROM wallet_topup $sWhere";
        $query = $this->db->query($sQuery);
        $ResultSet = $query->result_array();
        $data['iTotal'] = $ResultSet[0]['countRid'];
        return $data;
    }

	public function getFundRequestApprovalData($id){
		$sQuery="SELECT a.id,DATE_FORMAT(a.created_date,'%d-%m-%Y %H:%i:%s') as Date,a.request_touserid,a.amount,a.user_id,a.topup_by,a.transaction_remark,a.transaction_no,a.bank_name,a.bank_acc_no,a.is_approvel,a.reject_reason,b.amt FROM wallet_topup a LEFT JOIN wallet b ON a.request_touserid=b.user_id WHERE a.id =".$id."";
        $query = $this->db->query($sQuery);
        return $query->row();
	}
	
	public function getPaymentTransferMode(){
	  $sQuery="SELECT id,mode FROM payment_transfer_mode WHERE status=1 and deleted=0";
	  $query = $this->db->query($sQuery);
	  return $query->result();
	}
	
	public function getRokad($id){
	  $sQuery="SELECT id,first_name,last_name FROM users WHERE status='Y' AND is_deleted='N' AND level=".$id."";
	  $query = $this->db->query($sQuery);
	  return $query->result();
	}
	
	public function getMasterDistributor($id){
	  $role_id = $this->session->userdata('role_id');
	  $user_id = $this->session->userdata('user_id');
	  $level = $this->session->userdata('level');
	  if($role_id == SUPERADMIN_ROLE_ID or $role_id == COMPANY_ROLE_ID or $role_id == SUPPORT_ROLE_ID){
	  $sQuery="SELECT id,first_name,last_name FROM users WHERE status='Y' AND is_deleted='N' AND role_id=".$id."";
	  } else {
	  $sQuery="SELECT id,first_name,last_name FROM users WHERE status='Y' AND is_deleted='N' AND level=".$id." AND level_".$level." =".$user_id."";  
	  }
	  $query = $this->db->query($sQuery);
	  return $query->result();
	}
	
	public function getAreaDistributor($id){
	  $role_id = $this->session->userdata('role_id');
	  $user_id = $this->session->userdata('user_id');
	  $level = $this->session->userdata('level');
	  if($role_id == SUPERADMIN_ROLE_ID or $role_id == COMPANY_ROLE_ID or $role_id == SUPPORT_ROLE_ID){
	  $sQuery="SELECT id,first_name,last_name FROM users WHERE status='Y' AND is_deleted='N' AND role_id=".$id."";
	  } else {
	  $sQuery="SELECT id,first_name,last_name FROM users WHERE status='Y' AND is_deleted='N' AND level=".$id." AND level_".$level." =".$user_id."";  
	  }
	  $query = $this->db->query($sQuery);
	  return $query->result();
	}
	
	public function getDistributor($id){
	  $role_id = $this->session->userdata('role_id');
	  $user_id = $this->session->userdata('user_id');
	  $level = $this->session->userdata('level');
	  if($role_id == SUPERADMIN_ROLE_ID or $role_id == COMPANY_ROLE_ID or $role_id == SUPPORT_ROLE_ID){
	  $sQuery="SELECT id,first_name,last_name FROM users WHERE status='Y' AND is_deleted='N' AND role_id=".$id."";
	  } else {
	  $sQuery="SELECT id,first_name,last_name FROM users WHERE status='Y' AND is_deleted='N' AND level=".$id." AND level_".$level." =".$user_id."";  
	  }
	  $query = $this->db->query($sQuery);
	  return $query->result();
	}
	
	public function getRetailer($id){
	  $role_id = $this->session->userdata('role_id');
	  $user_id = $this->session->userdata('user_id');
	  $level = $this->session->userdata('level');
	  if($role_id == SUPERADMIN_ROLE_ID or $role_id == COMPANY_ROLE_ID or $role_id == SUPPORT_ROLE_ID){
	  $sQuery="SELECT id,first_name,last_name FROM users WHERE status='Y' AND is_deleted='N' AND role_id=".$id."";
	  } else {
	  $sQuery="SELECT id,first_name,last_name FROM users WHERE status='Y' AND is_deleted='N' AND level=".$id." AND level_".$level." =".$user_id."";  
	  }
	  $query = $this->db->query($sQuery);
	  return $query->result();
	}
	
    public function LoadFundSupportDataTable($sWhere, $sOrder, $sLimit) {
        $lcSqlsStr = "SELECT id,DATE_FORMAT(created_date,'%d-%m-%Y %H:%i:%s') as Date,amount,topup_by,transaction_remark,is_approvel,user_id,request_touserid from wallet_topup ";
        $lcSqlsStr .= "$sWhere $sOrder $sLimit";
		//echo $lcSqlsStr;
        $query = $this->db->query($lcSqlsStr);

        $data['ResultSet'] = $query->result_array();

        $data['iFilteredTotal'] = count($data['ResultSet']);

        /* Total data set length */
        $sQuery = " SELECT COUNT(id) as countRid  FROM wallet_topup $sWhere";
        $query = $this->db->query($sQuery);
        $ResultSet = $query->result_array();
        $data['iTotal'] = $ResultSet[0]['countRid'];
        return $data;
    }	

}
?>