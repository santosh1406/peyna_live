<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of acl_model
 * In this acl model in getMenu function we are returning all menu and submenu
 * in array
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Acl_model extends CI_Model {
    
    /*
    * @function    : get_menu
    * @param       : 
    * @detail      : To fetch all menu from menu table.
    */
    function get_menu() {
        $this->db->select("*");
        $this->db->from("menu");
        $this->db->where("record_status = 'Y'");
        $this->db->order_by("parent ASC, seq_no ASC");
        $acl_menu_result = $this->db->get();

        $acl_menu_array = $acl_menu_result->result_array();
        return $acl_menu_array;
    }

    // getMenu End   
}

// End of acl_model class