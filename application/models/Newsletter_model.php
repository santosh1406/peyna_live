<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Newsletter_model
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Newsletter_model extends MY_Model {
    
    var $table  = 'newsletter';
    var $fields = array("id","user_id","email","ip_address","user_agent","timestamp");
    var $key    = 'id';


    public function __construct() {
        parent::__construct(); 
    }
}
