<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Wallet_trans_log_model extends MY_Model {

    var $table  = 'wallet_trans_log';
    var $fields = array("id","user_id","wallet_id","credit_detail_id","wallet_trans_id","wallet_type","amt","outstanding_amount","merchant_amount","comment","amt_before_trans","amt_after_trans","ticket_id","return_ticket_id","topup_id","transaction_type_id","transaction_type","added_by","added_on");
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
}