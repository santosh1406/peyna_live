<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Booking_model extends CI_Model
{
    /**
     * Instanciar o CI
     */
   

    public function __construct()
    {
        parent::__construct();            
    }
   
    
   function get_search($op_trip_no,$trip_no)
    {
        $this->db->select("tm.trip_id, tm.trip_no, 	old.seat_type,osl.no_rows2,osl.no_cols2,tm.op_trip_no,  osl.ttl_seats,osl.op_id, osl.layout_name, osl.no_rows, osl.no_cols, old.row_no, old.col_no, old.berth_no, old.seat_no",false);
        $this->db->from("trip_master tm");


        $this->db->join("op_seat_layout osl", "osl.seat_layout_id = tm.seat_layout_id", "inner");
        $this->db->join("op_layout_details old", "old.op_seat_layout_id = osl.seat_layout_id", "inner");

        $this->db->where("tm.op_trip_no",$op_trip_no);
        $this->db->where("tm.trip_no",$trip_no);
        $query = $this->db->get();//print_r($query);die;
        //$this->db->last_query();
        //show($this->db->last_query(),1);

        return $query->result();
    }
    
    function get_tkt_count_within_dates($from_date,$till_date,$st)
    {
        $this->db->select("count(1) as cnt, date(issue_time) as date");
        $this->db->from('tickets');
        $this->db->group_by('date');
        if($from_date==$till_date)
        {
         $this->db->where('date(issue_time)=', $from_date);   
        }
        else
        {
        $this->db->where('date(issue_time) >=', $from_date);
        $this->db->where('date(issue_time) <=', $till_date); 
        }
        
        $this->db->where('status',$st);
       
        $data=$this->db->get();
       
        return $data->result_array();
        
        
    }
    
    
    
    

}
