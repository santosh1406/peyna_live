<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Flat_fare_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->_init();
        $this->up_db = $this->load->database('up_db', TRUE);
        
        $this->load->model(array('flat_fare_model'));
    }

    public function insertFare( $data) {
        
        //pr($data);die;
        
        if(!empty($data)) {
            $i = 1;
            foreach($data as $rno=>$v) {
                echo "\n".$i++.'. Route No => '.$rno."\n";
                $j = 1;
                $insertData = array();
                $this->db->trans_begin();
                if(!empty($v['FlatFareResponse']['flatFare']['flatFares'])) {
                    foreach ($v['FlatFareResponse']['flatFare']['flatFares'] as $d) {
                        echo ($j++).". \n"; 
                        $insertDataN = array();
                        $insertDataN['ROUTE_NO'] = $rno;
                        $insertDataN['BUS_TYPE_CD'] = $d['busTypeCd'];
                        $insertDataN['FROM_STOP_CD'] = $d['fromStopCd'];
                        $insertDataN['TILL_STOP_CD'] = $d['tillStopCd'];
                        $insertDataN['FROM_SEQ'] = $d['fromSeq'];
                        $insertDataN['TILL_SEQ'] = $d['tillSeq'];
                        $insertDataN['ADULT_AMT'] = $d['adultAmt'];
                        $insertDataN['CHILD_AMT'] = $d['childAmt'];
                        $insertDataN['HOME_ADULT_AMT'] = $d['homeAdultAmt'];
                        $insertDataN['HOME_CHILD_AMT'] = $d['homeChildAmt'];
                        $insertDataN['A_STATE_CD_1'] = $d['aStateCd1'];
                        $insertDataN['C_STATE_CD_1'] = $d['cStateCd1'];
                        $insertDataN['A_STATE_CD_2'] = $d['aStateCd2'];
                        $insertDataN['C_STATE_CD_2'] = $d['cStateCd2'];
                        $insertDataN['A_STATE_CD_3'] = $d['aStateCd3'];
                        $insertDataN['C_STATE_CD_3'] = $d['cStateCd3'];
                        $insertDataN['A_STATE_CD_4'] = $d['aStateCd4'];
                        $insertDataN['C_STATE_CD_4'] = $d['cStateCd4'];
                        $insertDataN['A_STATE_CD_5'] = $d['aStateCd5'];
                        $insertDataN['C_STATE_CD_5'] = $d['cStateCd5'];
                        $insertDataN['EFFECTIVE_FROM'] = $d['effFrom'];
                        $insertDataN['EFFECTIVE_TILL'] = $d['effTill'];
                        $insertDataN['SESSION_NO'] = $d['sessionNo'];
                        $insertDataN['FROM_ROUTE_AUTO'] = $d['fromRouteAuto'];
                        $insertDataN['TILL_ROUTE_AUTO'] = $d['tillRouteAuto'];
                        $insertDataN['TRANSACTION_TYPE'] = $d['transactionType'];
                        $this->up_db->insert('flat_fare',$insertDataN);
                        echo $this->up_db->last_query()."\n";
                    }
                    /* //pr($insertData);//die;
                    $this->up_db->insert_batch('flat_fare',$insertData);
                    echo 'Affected Rows = '.$this->up_db->affected_rows().'<br>';
                    //echo $this->up_db->last_query(); */
                    
                }
                if ($this->up_db->trans_status() === FALSE) {
                    echo '<pre>';print_r(error_get_last());echo '</pre>';
                    $this->up_db->trans_rollback();
                } else {
                    // show($this->db->queries,'', 'sucess database');
                    // $this->db->trans_rollback();
                    echo "done \n";
                    $this->up_db->trans_commit();
                }
                if(isset($v['FlatFareResponse']['serviceError'])) {
                    echo '<pre>';
                    print_r($v['FlatFareResponse']['serviceError']);
                    echo '</pre>';
                }
                echo "\n-----------------------------------------------\n";
            }
        }
    }
    
    
    public function getData() {
        $this->up_db->select("*");
        $this->up_db->from('route_stops');
        $query = $this->up_db->get();
        echo $this->up_db->last_query();
        return $query->result();
    }
    
    public function insertRouteStops ($data) {
        //pr($data);die;
        if(!empty($data)) {
            foreach($data as $rno=>$v) {
                echo '<br>'.'Route No => '.$rno.'<br>';
                $i = 0;
                $insertData = array();
                $this->db->trans_begin();
                if(!empty($v['RouteStopResponse']['routeStop']['routeStops'])) {
                    foreach ($v['RouteStopResponse']['routeStop']['routeStops'] as $d) {
                        $insertData[$i]['ROUTE_NO'] = $rno;
                        $insertData[$i]['BUS_STOP_CD'] = $d['busStopCd'];
                        $insertData[$i]['BUS_STOP_NM'] = $d['busStopNm'];
                        $insertData[$i]['STOP_SEQ'] = $d['stopSeq'];
                        $insertData[$i]['SUB_STAGE'] = $d['subStage'];
                        $insertData[$i]['KM'] = $d['km'];
                        $insertData[$i]['INTRA_STATE_DISTANCE'] = $d['intra'];
                        $insertData[$i]['INTER_STATE_DISTANCE'] = $d['inter'];
                        $insertData[$i]['STAGE_NO'] = $d['stageNo'];
                        $insertData[$i]['FARE_CHANGE_POINT'] = $d['fareChangePoint'];
                        $insertData[$i]['STATE_CD'] = $d['stateCd'];
                        $insertData[$i]['IS_INTER_STATE'] = $d['isInter'];
                        $insertData[$i]['IS_RESERVATION'] = $d['isReserv'];
                        $insertData[$i]['status'] = $d['status'];
                        $insertData[$i]['IS_AUDIT_TRAIL'] = $d['isAudit'];
                        $i++;
                    }
                    //pr($insertData);//die;
                    
                    $this->up_db->query("SET FOREIGN_KEY_CHECKS = 0");
                    $this->up_db->insert_batch('route_stops',$insertData);
                    $this->up_db->query("SET FOREIGN_KEY_CHECKS = 1");
                    echo $this->up_db->last_query();
                    echo 'Affected Rows = '.$this->up_db->affected_rows().'<br>';
                }
                if ($this->up_db->trans_status() === FALSE) {
                    echo '<pre>';print_r(error_get_last());echo '</pre>';
                    $this->up_db->trans_rollback();
                } else {
                    // show($this->db->queries,'', 'sucess database');
                    // $this->db->trans_rollback();
                    echo 'done';
                    $this->up_db->trans_commit();
                }
                if(isset($v['RouteStopResponse']['serviceError'])) {
                    echo '<pre>';
                    print_r($v['RouteStopResponse']['serviceError']);
                    echo '</pre>';
                }
                echo '<br>-----------------------------------------------<br>';
            }
        }
    }
    
    public function getDepots() {
        $this->up_db->select("DISTINCT(DEPOT_CD)");
        $this->up_db->from('depots');
        $query = $this->up_db->get();
        echo $this->up_db->last_query();
        return $query->result_array();
    }
    
    public function insertRoutes ($data) {
        //pr($data);die;
        if(!empty($data)) {
            foreach($data as $depot=>$v) {
                echo '<br>'.'Depot code => '.$depot.'<br>';
                $j = 1;
                $insertData = array();
                $this->db->trans_begin();
                if(!empty($v['RouteResponse']['depotRoutes']['routes'])) {
                    foreach ($v['RouteResponse']['depotRoutes']['routes'] as $d) {
                        echo ($j++).". \n"; 
                        $insertDataN = array();
                        $insertDataN['DEPOT_CD'] = $depot;
                        $insertDataN['ROUTE_NO'] = $d['routeno'];
                        $insertDataN['FROM_STOP_CD'] = $d['fromstopcd'];
                        $insertDataN['TILL_STOP_CD'] = $d['tillstopcd'];
                        
                        $insertDataN['VIA_STOP_CD'] = $d['viastopcd'];
                        $insertDataN['UP_DN_FLG'] = $d['updownflag'];
                        $insertDataN['ROUTE_TYPE'] = $d['routeType'];
                        
                        $this->up_db->query("SET FOREIGN_KEY_CHECKS = 0");
                        $this->up_db->insert('routes',$insertDataN);
                        echo $this->up_db->last_query()."\n";
                        $this->up_db->query("SET FOREIGN_KEY_CHECKS = 1");
                    }
                }
                else if(!empty($v['RouteResponse']['depotRoutes']['serviceError']['errorReason'])) {
                    echo $v['RouteResponse']['depotRoutes']['serviceError']['errorReason']."\n";
                }
                else {
                    echo "No particular error found. \n";
                }
                
                
                if ($this->up_db->trans_status() === FALSE) {
                    echo '<pre>';print_r(error_get_last());echo '</pre>';
                    $this->up_db->trans_rollback();
                } else {
                    // show($this->db->queries,'', 'sucess database');
                    // $this->db->trans_rollback();
                    echo "done \n";
                    $this->up_db->trans_commit();
                }
                if(isset($v['RouteResponse']['serviceError'])) {
                    echo '<pre>';
                    print_r($v['RouteResponse']['serviceError']);
                    echo '</pre>';
                }
                echo "\n-----------------------------------------------\n";
            }
        }
    }

    
    
    public function insertRouteStopsN ($data) {
        //pr($data);die;
        if(!empty($data)) {
            foreach($data as $rno=>$v) {
                echo '<br>'.'Route No => '.$rno.'<br>';
                $j = 1;
                $insertData = array();
                $this->db->trans_begin();
                if(!empty($v['RouteStopResponse']['routeStop']['routeStops'])) {
                    foreach ($v['RouteStopResponse']['routeStop']['routeStops'] as $d) {
                        echo ($j++).". \n"; 
                        $insertDataN = array();
                        $insertDataN['ROUTE_NO'] = $rno;
                        $insertDataN['BUS_STOP_CD'] = $d['busStopCd'];
                        $insertDataN['BUS_STOP_NM'] = $d['busStopNm'];
                        $insertDataN['STOP_SEQ'] = $d['stopSeq'];
                        $insertDataN['SUB_STAGE'] = $d['subStage'];
                        $insertDataN['KM'] = $d['km'];
                        $insertDataN['INTRA_STATE_DISTANCE'] = $d['intra'];
                        $insertDataN['INTER_STATE_DISTANCE'] = $d['inter'];
                        $insertDataN['STAGE_NO'] = $d['stageNo'];
                        $insertDataN['FARE_CHANGE_POINT'] = $d['fareChangePoint'];
                        $insertDataN['STATE_CD'] = $d['stateCd'];
                        $insertDataN['IS_INTER_STATE'] = $d['isInter'];
                        $insertDataN['IS_RESERVATION'] = $d['isReserv'];
                        $insertDataN['status'] = $d['status'];
                        $insertDataN['IS_AUDIT_TRAIL'] = $d['isAudit'];
                        
                        $this->up_db->query("SET FOREIGN_KEY_CHECKS = 0");
                        $this->up_db->insert('route_stops',$insertDataN);
                        echo $this->up_db->last_query()."\n";
                        $this->up_db->query("SET FOREIGN_KEY_CHECKS = 1");
                    }
                }
                if ($this->up_db->trans_status() === FALSE) {
                    echo '<pre>';print_r(error_get_last());echo '</pre>';
                    $this->up_db->trans_rollback();
                } else {
                    // show($this->db->queries,'', 'sucess database');
                    // $this->db->trans_rollback();
                    echo 'done';
                    $this->up_db->trans_commit();
                }
                if(isset($v['RouteStopResponse']['serviceError'])) {
                    echo '<pre>';
                    print_r($v['RouteStopResponse']['serviceError']);
                    echo '</pre>';
                }
                echo '<br>-----------------------------------------------<br>';
            }
        }
    }
    
    
}
