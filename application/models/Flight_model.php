<?php

//Created by pooja kambali on 29-09-2018
class Flight_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    function insert_flight_passenger($data,$deliveryvalue,$total_passanger=NULL){ 
    	   
		  $this->load->model('flight_passenger_model');
		  $this->load->model('flight_booking_temp_log_model');
		  $this->load->model('flight_passenger_details_model');
		  $this->load->model('flight_passenger_arr_model');
		  $this->load->model('flight_passenger_pnr_details_model');
		   
    	//  $this->load->model(['Flight_Passenger_model','Flight_Passenger_Details_model','Flight_Passenger_Arr_model','Flight_Passenger_Pnr_Details_model']);


    	        if(!empty($data) && !empty($deliveryvalue))
    	        {
                    //echo 'dasda';
                    //echo $data->refId ;
    	            if(isset($data->refId))
    	            {   

///echo $data->refId;die;
    	                        $flight_passanger =  $this->flight_passenger_model->insert([
    	                            'reference_id'               => $data->refId ,
    	                            'order_id'                   => isset($data->orderId) ? $data->orderId :'',        
    	                            'flight_no'                  => isset($data->items->flights[0]->flightNo) ? $data->items->flights[0]->flightNo : '',   
    	                            'ticket_pnr'                 => isset($data->ticketPnrs[0]->value) ? $data->ticketPnrs[0]->value : '' ,      
    	                            'email'                      => $deliveryvalue->email,
    	                            'mobile_number'              => $deliveryvalue->mobile,
    	                            'street'                     => $deliveryvalue->street,
    	                            'city'                       => $deliveryvalue->city,
    	                            'pincode'                    => $deliveryvalue->pincode,
    	                            'state'                      => $deliveryvalue->state,
    	                            'country'                    => $deliveryvalue->country,                       
    	                            'onward_dept_city_name'      =>  isset($data->srchQry->sectorInfos[0]->src->city) ? $data->srchQry->sectorInfos[0]->src->city : '',
    	                            'onward_arr_city_name'       => isset($data->srchQry->sectorInfos[0]->dest->city) ? $data->srchQry->sectorInfos[0]->dest->city : '',                       
    	                            'onward_date'                =>  isset($data->srchQry->sectorInfos[0]->date) ? $data->srchQry->sectorInfos[0]->date : '',   
    	                            'return_dept_city_name'      => isset($data->srchQry->sectorInfos[1]->src->city) ? $data->srchQry->sectorInfos[1]->src->city : '' ,
    	                            'return_arr_city_name'       => isset($data->srchQry->sectorInfos[1]->dest->city) ? $data->srchQry->sectorInfos[1]->dest->city : '' ,
    	                            'return_date'                => isset($data->srchQry->sectorInfos[1]->date) ? $data->srchQry->sectorInfos[1]->date : '' ,
    	                            'total_booking'              => $total_passanger,
    	                            'base_total'                 => $data->payDet->fareDet->base->amount ,                        
    	                            'tax_amount'                 => $data->payDet->fareDet->tax->amount,  
    	                            'sub_total'                  => $data->payDet->fareDet->tax->amount + $data->payDet->fareDet->base->amount , 
    	                            'total_amount'               => $data->payDet->fareDet->total->amount ,
    	                            'transaction_date'           => date('Y-m-d H:i:s',strtotime($data->bookDate)),  
    	                            'transaction_status'         => $data->status,   
    	                            'status'                     => "Y",    
    	                            'created_date'               => date('Y-m-d H:i:s'),
    	                            'ip_address'                 => $this->input->ip_address(),
    	                            'created_by'                 => $this->session->userdata('user_id'),
    	                            'operator_name'              =>'BOS',
    	                            'operator_id'                 =>1

    	                            ]);
    	                         if(!empty($flight_passanger))
    	                         {
                                     

    	                               // $flight_wallet_trans = $this->flight_passenger_model->update($flight_passanger,$flight_update_arr);
    	                         
    	                                foreach ($data->items->flights as $key => $value) {
    	                                    $flight_passanger_arr    =   $this->flight_passenger_arr_model->insert([
    	                                    'flight_id'                      => $flight_passanger,
    	                                    'ref_id'                         => $data->refId,
    	                                    'flight_code'                    => $value->carrier->code,
    	                                    'flight_name'                    => $value->carrier->name,
    	                                    'dep_date_time'                  => date('Y-m-d H:i:s',strtotime($value->depDetail->time)),
    	                                    'dep_city_code'                  => $value->depDetail->code ,   
    	                                    'dep_city_name'                  => $value->depDetail->city ,                  
    	                                    'dep_country'                    => $value->depDetail->country,
    	                                    'dep_terminal'                   => $value->depDetail->terminal,
    	                                    'arr_date_time'                  => date('Y-m-d H:i:s',strtotime($value->arrDetail->time)),
    	                                    'arr_city_name'                  => isset($value->arrDetail->name) ? $value->arrDetail->name : '',
    	                                    'arr_city_code'                  => isset($value->arrDetail->code) ? $value->arrDetail->code : '', 
    	                                    'arr_country'                    => isset($value->arrDetail->country) ? $value->arrDetail->country : '', 
    	                                    'arr_terminal'                   => isset($value->arrDetail->terminal) ? $value->arrDetail->terminal : '',
    	                                    'flight_no'                      => isset($value->flightNo) ? $value->flightNo : '',
    	                                    'flight_time'                    => isset($value->flyTime) ? $value->flyTime : '',
    	                                    'flight_layover'                 => isset($value->layover) ? $value->layover : '', 
    	                                    'refundable'                     => isset($value->refundable) ? $value->refundable : '',
    	                                    'cabin_class'                    => isset($value->cabinClass) ? $value->cabinClass : '',
    	                                    'created_date'                   => date('Y-m-d H:i:s'),                   
    	                                    'created_by'                     => $this->session->userdata('user_id')
    	                                    ]);                   
    	                                }

    	                                $items = $data->items->travellerDataList;                    
    	                        
    	                                foreach ($items as $key => $value)
    	                                {                            
    	                                    $flight_passanger_details =  $this->flight_passenger_details_model->insert([
    	                                            'flight_id'               => $flight_passanger,
    	                                            'order_id'                  => $data->orderId,        
    	                                            'reference_id'            => $data->refId,   
    	                                            'passenger_type'          => $value->pType,      
    	                                            'tittle'                  => $value->title,
    	                                            'first_name'              => $value->firstName,
    	                                            'last_name'               => $value->lastName,
    	                                            'age'                     => $value->age,                        
    	                                            'dob'                     => $value->dob,
    	                                            'status'                  => "Y",
    	                                            'created_date'            => date('Y-m-d H:i:s')
    	                                        ]);

    	                                    foreach ($value->segmentBookingDetails as $travellerdetails) {
    	                                      		$number = NULL ;
													if(!empty($travellerdetails->ticketNumber))
													{
													  $number = $travellerdetails->ticketNumber;
													}
													$gdsPnr=NULL ;
													if(!empty($travellerdetails->gdsPnr))
													{
													  $gdsPnr=$data->gdsPnr ;
													}
													$pnr=NULL  ;
													if(!empty($travellerdetails->pnr ))
													{
													  $pnr=$travellerdetails->pnr ;
													}
													$refId=NULL  ;
													if(!empty($data->refId ))
													{
													  $refId=$data->refId;
													}
												    $flight_passanger_pnr_details =  $this->flight_passenger_pnr_details_model->insert([
    	                                            'flight_id'               => $flight_passanger,
    	                                            'flight_passenger_id'     => $flight_passanger_details,
    	                                            'ref_id'                  => $refId,    
												    'pnr'                     => $pnr,
												    'gds_pnr'                 => $gdsPnr,     
												    'status'                  => $travellerdetails->status, 
    	                                            'pass_flight_no'          => $number, 
    	                                            'created_date'            => date('Y-m-d H:i:s')
    	                                        ]);                        

    	                                        }                   
    	                                }                               
    	                             return true;  
    	                         }
    	                         else{
    	                             return false;
    	                         }    

    	                
    	            } 
    	        }   
        // $insertdata = $this->db->insert('dmt_transfer_detail', $data);
        // return $insertdata;
    }

    // added by Lilawati karale on 21-02-2019
    public function getCities($city = "") {
        $queryStr = "select *"
                . " FROM flight_city"
                . " WHERE label LIKE '$city%' group by label limit 10";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        $nCount = count($result);

        if ($nCount > 0) {
            for ($i = 0; $i < $nCount; $i++) {
                $aResult[$i]['label'] = $result[$i]['label'];
                $aResult[$i]['name'] = $result[$i]['label'];
                $aResult[$i]['code'] = $result[$i]['value'];
                $aResult[$i]['id'] = $result[$i]['id'];                
            }
        } else {
            $aResult[0]['label'] = 'No result found';
            $aResult[0]['name'] = 'No result found';
            $aResult[0]['value'] = '';
            $aResult[0]['id'] = '';
        }
        //$aResult_json = json_encode($aResult);
        return $aResult;
    }

}