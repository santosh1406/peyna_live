<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Agent_config_model extends MY_Model {
    
	var $table  = 'agent_config';
    var $fields = array("id","agent_id","security_deposit_amt","top_up_limit","hhm_limit","etim_transfer_limit","locking_duration");
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
    
}
// End of Agent_model class