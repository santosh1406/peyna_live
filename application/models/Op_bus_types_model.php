<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Op_bus_types_model extends MY_Model {

    var $table  = 'op_bus_types';
    var $fields = array("id","bus_type_id","op_id","op_bus_type_name","op_bus_type_cd", "is_child_concession", "is_seat_layout", "is_seat_number", "created_date","created_by","updated_date","updated_by","record_status");
    
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

    function get_type_count()
    {
        $where = array(
                        'op_bus_type_name' => $this->op_bus_type_name,
                        'op_id' => $this->op_id 
                       );
        
        
        $this->db->where($where);
        return $this->db->count_all_results($this->table);
    }
    
            
    function get_detail_category($cat, $op_id)
    {
        $where_clause  = array(
                                'op_bus_type_cd' => $cat,
                                'op_id' => $op_id
                            );
        
        $this->db->where($where_clause);
        $data = $this->db->get($this->table);        
        return $data->row();
        
    }
    
    function get_detail_bus_type($input){
      
        $this->db->select("obt.bus_type_id,obt.bus_type_id,obt.op_id,obt.op_bus_type_name,obt.op_bus_type_cd");
        $this->db->from("op_bus_types obt");
        $this->db->join('op_master om', 'om.op_id=obt.op_id', "inner");
        
        $this->db->join('bus_type bt', 'bt.id=obt.bus_type_id', "inner");
        $this->db->where('obt.op_id', $input['op_id']);
        $query = $this->db->get();
        return $query->result_array();
    
    }
    function get_detail_all_bus_type($input){
      
        $this->db->select("obt.id,obt.bus_type_id,obt.bus_type_id,obt.op_id,obt.op_bus_type_name,obt.op_bus_type_cd");
        $this->db->from("op_bus_types obt");
        $this->db->join('op_master om', 'om.op_id=obt.op_id', "inner");
        
        $this->db->join('bus_type bt', 'bt.id=obt.bus_type_id', "inner");
        $this->db->where('obt.op_id', $input['op_id']);
          $this->db->where('obt.bus_type_id', $input['bus_type_id']);
        $query = $this->db->get();
        return $query->result_array();
    
    }
}

?>
