<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Machine_unregister_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_agentwise_waybill_detail($id) {

        $status_arr = array('FINISHED', 'CLOSED', 'CANCELED');
        $err_status_arr = array('OK', 'PROCESSED', 'STARTED', 'NEW', 'OPEN');

        $this->db->select("am.agent_code,wp.OP_ID,d.STATUS,wp.waybill_no,wp.agent_id,em.VERSION_DATE,em.SERIAL_NUMBER,wp.STAND_NAME,am.fname as agent,wp.collection_tm,");

        $this->db->join('rsrtc' . '.duty_allocation d', 'd.DUTY_ALLOCATION_ID=wp.DUTY_ALLOCATION_ID', 'left');
        $this->db->join('rsrtc' . '.etm_master em', 'em.agent_id=wp.AGENT_ID', 'left');
        $this->db->join('agent_master am', 'wp.agent_id=am.agent_id', 'left');

        $this->db->from('waybillprogramming wp');
        $this->db->where('wp.AGENT_ID', $id);
        //$this->db->where('wp.AGENT_ID','43');
        $this->db->where('em.USER_UNREGISTER', '0');
        $this->db->where_in('wp.WAYBILL_STATUS', $err_status_arr);
        $this->db->or_where_in('d.STATUS', $err_status_arr);
        $this->db->where('d.ETIM_NO', 'wp.ETIM_NO');

        $res = $this->db->get();
        $result = $res->result_array();

        if (isset($result) && count($result) > 0) {

            return $result = 'Collection required,So machine not applicable for unregister!!!';
        } elseif (count($result) > 0) {

            $this->db->select("am.agent_code,em.ADD_DATE,wp.OP_ID,bs.BUS_STOP_CD,em.VERSION_DATE,mv.version VERSION_ID,o.opening_bal,o.trans_amount,o.amount,d.STATUS,wp.waybill_no,wp.agent_id,em.SERIAL_NUMBER,wp.STAND_NAME,am.fname as agent,wp.DUTY_DT,wp.collection_tm,");

            $this->db->join('rsrtc' . '.duty_allocation d', 'd.DUTY_ALLOCATION_ID=wp.DUTY_ALLOCATION_ID', 'left');
            $this->db->join('rsrtc' . '.etm_master em', 'em.agent_id=wp.AGENT_ID', 'left');
            $this->db->join('ticket_data td', 'wp.WAYBILL_NO=td.waybill_no', 'left');
            $this->db->join('rsrtc' . '.machine_version mv', 'mv.version_id=em.VERSION_ID', 'left');
            $this->db->from('waybillprogramming wp');

            $this->db->join('agent_master am', 'wp.agent_id=am.agent_id', 'left');
            $this->datatables->join('op_wallet o', 'o.user_id=am.agent_id', 'left');
            $this->datatables->join('op_wallet_trans ow', 'ow.op_wallet_id=o.id', 'left');
            $this->datatables->join('rsrtc.bus_stops bs', 'bs.BUS_STOP_NM=wp.STAND_NAME', 'left');


            $this->db->where('em.USER_UNREGISTER', '0');
            $this->db->where_in('wp.WAYBILL_STATUS', $status_arr);
            //   $this->db->where('d.ETIM_NO','wp.ETIM_NO');

            $this->db->where_in('d.STATUS', $status_arr);
            $this->db->where('wp.AGENT_ID', $id);
            // $this->db->where('wp.AGENT_ID','43');
            $this->db->order_by('em.ETIM_NO', 'desc');
            $this->db->limit(1);
            $res = $this->db->get();
            $result = $res->result_array();
            if (isset($result) && count($result) > 0) {

                return $result;
            } else {
                return $result = 'Machine not applicable for unregister!!!';
            }
        } else {
            $this->db->select("am.agent_code,em.ADD_DATE,em.VERSION_DATE,mv.version VERSION_ID,em.SERIAL_NUMBER,am.fname as agent,wp.waybill_no,wp.agent_id,wp.STAND_NAME,wp.DUTY_DT,wp.collection_tm,wp.WAYBILL_STATUS as STATUS");

            $this->db->join('rsrtc' . '.machine_version mv', 'mv.version_id=em.VERSION_ID', 'left');
            $this->db->from('rsrtc' . '.etm_master em');

            $this->db->join('agent_master am', 'em.agent_id=am.agent_id', 'left');
            $this->db->join('waybillprogramming wp', 'em.agent_id=wp.AGENT_ID', 'left');

            $this->db->where('em.USER_UNREGISTER', '0');

            $this->db->order_by('em.ETIM_NO', 'desc');
            $this->db->limit(1);
            $res = $this->db->get();
            $result = $res->result_array();

            if (isset($result) && count($result) > 0) {

                return $result;
            } else {
                return $result = 'Machine not applicable for unregister!!!';
            }
        }

    }

    public function machine_unre_without_machine($id, $reason_id) {
        $status_unreg = 0;
        $status_arr = array('FINISHED', 'CLOSED', 'CANCELED');
        $err_status_arr = array('OK', 'PROCESSED', 'STARTED', 'NEW', 'OPEN');

        $this->db->select("am.agent_code,wp.OP_ID,d.STATUS,wp.waybill_no,wp.agent_id,em.VERSION_DATE,em.SERIAL_NUMBER,wp.STAND_NAME,am.fname as agent,wp.collection_tm,");

        $this->db->join('rsrtc' . '.duty_allocation d', 'd.DUTY_ALLOCATION_ID=wp.DUTY_ALLOCATION_ID', 'left');
        $this->db->join('rsrtc' . '.etm_master em', 'em.agent_id=wp.AGENT_ID', 'left');
        $this->db->join('agent_master am', 'wp.agent_id=am.agent_id', 'left');

        $this->db->from('waybillprogramming wp');
        $this->db->where('wp.AGENT_ID', $id);
        //$this->db->where('wp.AGENT_ID','43');
        $this->db->where('em.USER_UNREGISTER', '0');
        $this->db->where_in('wp.WAYBILL_STATUS', $err_status_arr);
        $this->db->or_where_in('d.STATUS', $err_status_arr);
        $this->db->where('d.ETIM_NO', 'wp.ETIM_NO');

        $res = $this->db->get();
        $result = $res->result_array();


        if (isset($result) && count($result) > 0) {

            return $result = 'Collection required,So machine not applicable for unregister!!!';
        } elseif (count($result) > 0) {

            $this->db->select("am.agent_code,em.ADD_DATE,wp.OP_ID,bs.BUS_STOP_CD,em.VERSION_DATE,mv.version VERSION_ID,o.opening_bal,o.trans_amount,o.amount,d.STATUS,wp.waybill_no,wp.agent_id,em.SERIAL_NUMBER,wp.STAND_NAME,am.fname as agent,wp.DUTY_DT,wp.collection_tm,");

            $this->db->join('rsrtc' . '.duty_allocation d', 'd.DUTY_ALLOCATION_ID=wp.DUTY_ALLOCATION_ID', 'left');
            $this->db->join('rsrtc' . '.etm_master em', 'em.agent_id=wp.AGENT_ID', 'left');
            $this->db->join('ticket_data td', 'wp.WAYBILL_NO=td.waybill_no', 'left');
            $this->db->join('rsrtc' . '.machine_version mv', 'mv.version_id=em.VERSION_ID', 'left');
            $this->db->from('waybillprogramming wp');

            $this->db->join('agent_master am', 'wp.agent_id=am.agent_id', 'left');
            $this->datatables->join('op_wallet o', 'o.user_id=am.agent_id', 'left');
            $this->datatables->join('op_wallet_trans ow', 'ow.op_wallet_id=o.id', 'left');
            $this->datatables->join('rsrtc.bus_stops bs', 'bs.BUS_STOP_NM=wp.STAND_NAME', 'left');


            $this->db->where('em.USER_UNREGISTER', '0');
            $this->db->where_in('wp.WAYBILL_STATUS', $status_arr);

            $this->db->where_in('d.STATUS', $status_arr);
            $this->db->where('wp.AGENT_ID', $id);

            $this->db->order_by('em.ETIM_NO', 'desc');
            $this->db->limit(1);
            $res = $this->db->get();
            $result = $res->result_array();

            if (isset($result) && count($result) > 0) {
                $status_unreg = 1;
            } else {
                return $result = 'Machine not applicable for unregister!!!';
            }
        } else {
            $this->db->select("am.agent_code,em.ADD_DATE,em.VERSION_DATE,mv.version VERSION_ID,em.SERIAL_NUMBER,am.fname as agent");

            $this->db->join('rsrtc' . '.machine_version mv', 'mv.version_id=em.VERSION_ID', 'left');
            $this->db->from('rsrtc' . '.etm_master em');

            $this->db->join('agent_master am', 'em.agent_id=am.agent_id', 'left');


            $this->db->where('em.USER_UNREGISTER', '0');

            $this->db->order_by('em.ETIM_NO', 'desc');
            $this->db->limit(1);
            $res = $this->db->get();
            $result = $res->result_array();

            if (isset($result) && count($result) > 0) {
                $status_unreg = 1;
            } else {
                return $result = 'Machine not applicable for unregister!!!';
            }
        }
        if ($status_unreg == 1) {


            $this->db->select("ETIM_NO,ETIM_SEQ_NO,DEPOT_CD,ETIM_STATUS,IS_WORKING,LAST_UPDATE_DATE,agent_id,USER_ID,"
                    . "ADD_DATE,VERSION_ID,VERSION_DATE,SERIAL_NUMBER,HANDLE_STATUS,"
                    . "REASON,SIM_IMSI,SIM_CCID,MACHINE_CATAGORY");


            $this->db->from('rsrtc' . '.etm_master');

            $this->db->where('agent_id', $id);
            $this->db->where('user_unregister', '0');


            $this->db->limit(1);
            $res = $this->db->get();
            $mnresult = $res->result_array();
            //show(count($mnresult),1);   
            if (isset($mnresult) && count($mnresult) > 0) {
                // show(count($mnresult), 1);
                $etim_no = $mnresult[0]['SERIAL_NUMBER'];

                /*

                  $this->db->select("count(ETIM_SEQ_NO) as etim_count,SERIAL_NUMBER");
                  $this->db->from('rsrtc' . '.etm_master');
                  $this->db->where('etim_no', $mnresult['0']['ETIM_NO']);
                  $this->db->where('serial_number', $mnresult['0']['SERIAL_NUMBER']);
                  $this->db->order_by('ADD_DATE');
                  $this->db->limit(1);
                  $res = $this->db->get();
                  $row_result = $res->result_array();

                  if (isset($row_result) && count($row_result) >= '0') {
                  // show(count($row_result), 1);
                  $handheldNumber = $row_result[0]['SERIAL_NUMBER']; */


                if ($etim_no && strpos($etim_no, '_')) {
                    $inc_val = explode('_', $etim_no);
                    $un_reg_txt = $inc_val[1] + 1;
                    $ser_no = $etim_no . 'UNREG_' . $un_reg_txt;
                } else {
                    $ser_no = $etim_no . 'UNREG_0';
                }//show($ser_no,1);

                $data = array("SERIAL_NUMBER" => $ser_no, "USER_UNREGISTER" => '1', 'ETIM_STATUS' => 'N');

                $this->db->where('agent_id', $id);

                $this->db->update('rsrtc.etm_master', $data);


                $insert_id = $this->db->query('insert into rsrtc.etm_master_log (ETIM_NO,ETIM_SEQ_NO,ETIM_STATUS,'
                        . 'IS_WORKING,LAST_UPDATE_DATE,agent_id,ADD_DATE ,VERSION_ID,'
                        . 'VERSION_DATE,SERIAL_NUMBER,REASON,INSERTED_DATE)'
                        . "VALUES ('" . $mnresult[0]['ETIM_NO'] . "','" . $mnresult[0]['ETIM_SEQ_NO'] . "',"
                        . " '" . $mnresult[0]['ETIM_STATUS'] . "','" . $mnresult[0]['IS_WORKING'] . "',"
                        . "'" . $mnresult[0]['LAST_UPDATE_DATE'] . "','" . $mnresult[0]['agent_id'] . "',"
                        . "'" . $mnresult[0]['ADD_DATE'] . "','" . $mnresult[0]['VERSION_ID'] . "',"
                        . "'" . $mnresult[0]['VERSION_DATE'] . "','" . $mnresult[0]['SERIAL_NUMBER'] . "','" . $reason_id . "',"
                        . "'" . date("Y-m-d H:i:s") . "') ");

                //$this->db->query('SET FOREIGN_KEY_CHECKS = 0,insert in to routes values ('.$routes.')');
                //print_r($query->result());
                $response["status"] = "success";
                $response["message"] = "This Machine is unregistered successfully.";
            } else {

                $response["status"] = "error";
                $response["message"] = "This Machine number is already unregistered.";
            }
        } else {

            $response["status"] = "error";
            $response["message"] = "This Machine number is already unregistered.";
        }



        return json_encode($response);
    }

    public function cancell_waybill($data) {//show($data,1);die($data);
        $status_unreg = 0; //show('ghghg',1);
        $status_arr = array('OPEN');
        $err_status_arr = array('PROCESSED', 'STARTED', 'NEW', 'OPEN');

        $this->db->select("am.agent_code,wp.OP_ID,d.STATUS,wp.waybill_no,wp.agent_id,wp.DUTY_ALLOCATION_ID");

        $this->db->join('rsrtc' . '.duty_allocation d', 'd.DUTY_ALLOCATION_ID=wp.DUTY_ALLOCATION_ID', 'left');
        $this->db->join('rsrtc' . '.etm_master em', 'em.agent_id=wp.AGENT_ID', 'left');
        $this->db->join('agent_master am', 'wp.agent_id=am.agent_id', 'left');

        $this->db->from('waybillprogramming wp');
        $this->db->where('wp.AGENT_ID', $data['agent_id']);
        $this->db->where('wp.WAYBILL_NO', $data['waybill_no']);
        $this->db->where('em.USER_UNREGISTER', '0');
        $this->db->where_in('wp.WAYBILL_STATUS', $err_status_arr);
        $this->db->or_where_in('d.STATUS', $err_status_arr);
        $this->db->where('d.ETIM_NO', 'wp.ETIM_NO');

        $res = $this->db->get();
        $result = $res->result_array(); //die('sdfg');
       // show($result,1);
        if (isset($result) && count($result) <= 0) {

            return $result = 'Not valid waybill for cancellation!!!';
        } else {
           // show($result[0]['DUTY_ALLOCATION_ID'], 1);
            $wp_data = array("WAYBILL_STATUS" => 'CANCELED');
            $this->db->where('AGENT_ID', $data['agent_id']);
            $this->db->where('WAYBILL_NO', $data['waybill_no']);
            $this->db->update('waybillprogramming', $wp_data);
          // show(last_query(), 1);
            $duty_data = array("status" => 'CANCELED');
            $this->db->where('AGENT_ID', $data['agent_id']);
            $this->db->where('DUTY_ALLOCATION_ID', $result[0]['DUTY_ALLOCATION_ID']);
            $this->db->update('rsrtc.duty_allocation', $duty_data);
            $insert_id = $this->db->query('insert into cancel_waybill_log (waybill_no,agent_id,duty_allocation_id,reason_id,created_date)'
                        . "VALUES ('". $data['waybill_no'] . "','" . $data['agent_id'] . "','" . $result[0]['DUTY_ALLOCATION_ID'] . "','" . $data['reason_id'] . "',"
                        . "'" . date("Y-m-d H:i:s") . "') ");
            $response["status"] = "success";
            $response["message"] = "This waybill is cancelled successfully.";
        }


        //show(last_query(), 1);
        return json_encode($response);
    }

}

?>