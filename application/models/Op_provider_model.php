<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Op_provider_model extends MY_Model {

    var $table  = 'op_provider';
    var $fields = array("id","op_id","op_service_id","is_status","is_deleted","inserted_by","inserted_date","updated_date");
    // var $fields = "id,w_id,user_id,amt,status,expire_at,comment,added_by,added_on";
    var $key    = 'id';


    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
      
}
?>