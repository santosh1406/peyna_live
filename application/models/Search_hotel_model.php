<?php

//Created by Sonali Kamble on 31-may-19
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Search_hotel_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    // added by shrasti bansal on 31-12-2018
    public function cityList($operator = "", $type = "") {

        $queryStr = "select *"
                . " FROM hotel_city_master";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        return $result;
    }
    
    public function saveBookingData($data = "") {
        if ($data) {
            $this->db->insert('hotel_booking', $data);
            $insert_id = $this->db->insert_id();
            return  $insert_id;
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'hotel_booking_tbl.log', $data, 'hotel_booking tbl');
            return true;
        }
        return false;
    }
    
    public function getCityDetails($id){
        $queryStr = "select *"
                . " FROM hotel_city_master"
                . " where city_id = '$id'";
        $query = $this->db->query($queryStr);
        $result = $query->result_array();
        
        return $result[0];
    }
    
    
    
    public function saveBookingTempData($data = "") {
        if ($data) {
            $this->db->insert('hotel_booking_temp', $data);
//            $insert_id = $this->db->insert_id();
//            return  $insert_id;
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'hotel_booking_temp_tbl.log', $data, 'hotel_booking temp tbl');
            return true;
        }
        return false;
    }
    
    public function batchInsertHotelGuestDetails($table, $data)
    {
       return $this->db->insert_batch($table, $data);
    }

}