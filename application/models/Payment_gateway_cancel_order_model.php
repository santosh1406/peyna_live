<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Payment_gateway_cancel_order_model extends MY_Model
{
    /**
     * Instanciar o CI
     */
var $table      = 'payment_gateway_cancel_order';
var $fields     = array("id","ticket_id","type","pg_tracking_id","request","response","created_at");
var $key        = 'id';

    protected $set_created = false;

    protected $set_modified = false;
    
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }
    
    
}
