<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Authentication_model extends MY_Model {

    protected $table    = 'cb_auth_data';
     // var $fields = array("id", "waybill_no", "agent_id", "depot_name", "duty_date", "trip_no", "psgr_count",
     //                     "total_amount", "dsa_ticket", "dsa_revenue", "depot_auth_created_by", "ho_auth_created_by",
     //                      "depo_auth_status", "ho_auth_status", "depo_auth_created_date", "ho_auth_created_date",
     //                       "created_by", "create_date", "is_status", "is_deleted" );
   
    public function __construct() {
        parent::__construct();
        $this->_init();      
    }

	public function get_Details($per_page, $offset)
	{
        $this->db->select('am.agent_id,am.depot_name,cb.depo_auth_status,cb.ho_auth_status,cb.dsa_ticket,cb.dsa_revenue,am.fname,td.ticket_id,td.waybill_no,td.update_dt,FORMAT(sum(td.total_amount),2) as total_amount,
                          count(td.ticket_id)  as pass_no,td.trip_no');
        $this->db->from("ticket_data td");
        $this->db->join('agent_master am', 'td.agent_id = am.agent_id', 'left');
        $this->db->join('cb_auth_data cb', 'td.waybill_no = cb.waybill_no', 'left');
        $this->db->where("td.waybill_no != ''");
        $this->db->group_by('td.waybill_no');
        $this->limit($per_page, $offset);
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;
	}

    public function get_count()
    {
        $this->db->select('1');
        $this->db->from("ticket_data td");
        $this->db->join('agent_master am', 'td.agent_id = am.agent_id', 'left');
        $this->db->where("td.waybill_no != ''");
        $this->db->group_by('td.waybill_no');
        $data = $this->db->get();
        $cnt = count($data->result_array());
        return $cnt;      
    }

    public function get_auth_Details($per_page, $offset)
    {
        $this->db->select('ad.id ,ad.waybill_no ,ad.agent_id,am.fname,ad.depot_name,ad.duty_date,ad.trip_no,ad.psgr_count,ad.total_amount,ad.dsa_ticket,ad.depot_auth_created_by,ad.ho_auth_created_by,ad.depo_auth_status,ad.ho_auth_status , ad.depo_auth_created_date,ad.ho_auth_created_date,ad.dsa_revenue');
        $this->db->from("cb_auth_data ad");
        $this->db->join('agent_master am', 'ad.agent_id = am.agent_id', 'left');    
        $this->db->where("ad.is_status = '1'");
        $this->db->where("ad.is_deleted = '0'");
        $this->limit($per_page, $offset);
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;
    }

    public function get_auth_count()
    {
        $this->db->select('count(id) as cnt');
        $this->db->from("cb_auth_data");    
        $this->db->where("is_status = '1'");
        $this->db->where("is_deleted = '0'");
        $data = $this->db->get();
        $result = $data->result_array();
        $cnt = $result[0]['cnt'];
        return $cnt;
    }

    public function chk_waybill_exist($waybill_no)
    {
        if($waybill_no != '')
        {    
            $this->db->select('id');
            $this->db->from("cb_auth_data");    
            $this->db->where("waybill_no ='".$waybill_no."'");
            $this->db->where("is_status = '1'");
            $this->db->where("is_deleted = '0'");
            $data = $this->db->get();
            $result = $data->result_array();
            $id = $result[0]['id'];
            return $id;
        }
    }
 
}
?>