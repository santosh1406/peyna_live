<?php

class Cca_payment_gateway
{
    var $ci;
    private $cca_working_key;
    private $cca_access_code;
    private $cca_production_url;
  
    public function __construct()
    {
        $this->ci  = &get_instance();

        $this->ci->cca_working_key = $this->ci->config->item("cca_working_key");
        $this->ci->cca_access_code = $this->ci->config->item("cca_access_code");
        $this->ci->cca_production_url = $this->ci->config->item("cca_production_url");
        $this->ci->cca_rsa_key_url = $this->ci->config->item("cca_rsa_key_url");

        $this->ci->load->model(array('payment_transaction_fss_model','payout_summary_details_model','tracking_request_response_model','payment_transaction_metadata_model', 'common_model', 'payment_confirmation_model', 'tickets_model', 'return_ticket_mapping_model','users_model','wallet_topup_model','wallet_model','wallet_trans_log_model','op_trans_details_model','op_wallet_model','payment_gateway_cancel_order_model'));
    }

    public function GetHandleResponse()
    {

        $pgres = $this->ci->input->post();
        // show($pgres,1,"pgres");
        
        /* 
        BELOW ARE LIST OF PARAMETERS THAT WILL BE RECEIVED BY MERCHANT FROM PAYMENT GATEWAY 
        */
        try
        {
            /*Variable Declaration*/
            /*=========================================================================================*/
            /*
                $merchant_param1 -> ticket_ref_no;
                $merchant_param2 -> ticket_id;
                $merchant_param3 -> user_id;
            */
            
            //Decrypt Response here using ALGORITHM SPECIFIED BY CCAVENUE
            $rcvdString =   decrypt($pgres["encResp"],$this->ci->config->item("cca_working_key"));     //Crypto Decryption used as per the specified working key.
            /*$decryptValues  =   explode('&', $rcvdString);*/

            log_data("pg/ccavenue_pg_".date("d-m-Y")."_log.log",$rcvdString, 'recived decrypted string');

            $cc_result = "";
            $order_status = "";

            parse_str($rcvdString);
            parse_str($rcvdString, $array_response);

            /*
             *          NOTICE  : DO NOT UNCOMMENT BELOW LINES
             */
            /*$array_response = "";
            foreach ($decryptValues as $key => $value) {
                $darray = array();
                $var_value = "";
                
                $darray = explode('=',$value);
                $variable = $darray[0];
                $var_value = $darray[1];
                ${$variable} = isset($var_value) ? $var_value : '';
                $array_response[$variable] = isset($var_value) ? $var_value : '';
            }*/

            //for log Response
            log_data("pg/ccavenue_pg_".date("d-m-Y")."_log.log",$array_response, 'Response From CCAvenue PG');

            //IMP data to pass to view
            $data["ticket_ref_no"] = $merchant_param1;
            $data["ticket_id"] = $merchant_param2;
            $data["payment_by"] = "ccavenue";

            /*GENERATING TRANSACTION LOG FILE START HERE*/
            /*=========================================================================================*/
            if($this->ci->session->userdata("user_id") > 0)
            {
                $user_id =  $this->ci->session->userdata("user_id");
            }
            else
            {
                /*IMP NOTE : When you need merchant_param4 for different purpose then remove user_id from merchant_param4,
                             and make here query to fecth user_id.
                */
                $user_id =  $merchant_param3;
            }

            $title = array("order_id","tracking_id","bank_ref_no","order_status","failure_message","payment_mode","card_name","status_code","status_message","currency","amount","billing_ name","billing_ address","billing_ city","billing_ state","billing_ zip","billing_ country","billing_ tel","billing_ email","delivery_ name","delivery_ address","delivery_ city","delivery_ state","delivery_ zip","delivery_ country","delivery_ tel","merchant_param1","merchant_param2","merchant_param3","merchant_param4","merchant_param5","vault","offer_type","offer_code","discount_value","IP Address","User Agent");
            $val = array($order_id,$tracking_id,$bank_ref_no,$order_status,$failure_message,$payment_mode,$card_name,$status_code,$status_message,$currency,$amount,$billing_name,$billing_address,$billing_city,$billing_state,$billing_zip,$billing_country,$billing_tel,$billing_email,$delivery_name,$delivery_address,$delivery_city,$delivery_state,$delivery_zip,$delivery_country,$delivery_tel,$merchant_param1,$merchant_param2,$merchant_param3,$merchant_param4,$merchant_param5,$vault,$offer_type,$offer_code,$discount_value,$this->ci->session->userdata("ip_address"),$this->ci->session->userdata("user_agent"));
            $pgfile = PG_LOG.date("d-m-Y")."_cca.csv";

            if(!file_exists($pgfile))
            {
                $file = fopen($pgfile,"a");
                fputcsv($file,$title);
            }

            $file = fopen($pgfile,"a");
            fputcsv($file,$val);

            fclose($file);
            /*GENERATING TRANSACTION LOG FILE END HERE*/
            /*/=================================================================================================  */

            if($order_id != '' && $failure_message == "")  
            { 

              /*
              IMPORTANT NOTE - MERCHANT SHOULD UPDATE 
              TRANACTION PAYMENT STATUS IN MERCHANT DATABASE AT THIS POSITION 
              AND THEN REDIRECT CUSTOMER ON RESULT PAGE
              */
                if ((strtolower($order_status) == "success"))
                {
                    /* NOTE - MERCHANT MUST LOG THE RESPONSE RECEIVED IN LOGS AS PER BEST PRACTICE */
                    /*
                    IMPORTANT NOTE - MERCHANT DOES RESPONSE HANDLING AND VALIDATIONS OF 
                    TRACK ID, AMOUNT AT THIS PLACE. THEN ONLY MERCHANT SHOULD UPDATE 
                    TRANACTION PAYMENT STATUS IN MERCHANT DATABASE AT THIS POSITION 
                    AND THEN REDIRECT CUSTOMER ON RESULT PAGE
                    */

                    /* !!IMPORTANT INFORMATION!!
                    During redirection, ME can pass the values as per ME requirement.
                    NOTE: NO PROCESSING should be done on the RESULT PAGE basis of values passed in the RESULT PAGE from this page. 
                    ME does all validations on the responseURL page and then redirects the customer to RESULT PAGE ONLY FOR RECEIPT PRESENTATION/TRANSACTION STATUS CONFIRMATION
                    For demonstration purpose the result and track id are passed to Result page
                    */
                    /* Hashing Response Successful  */

                    //update payment_transaction_fss table
                    $check_ticket_details = $this->ci->tickets_model->where("ticket_id",$data["ticket_id"])->find_all();
                    
                    if($check_ticket_details && $check_ticket_details[0]->transaction_status == "pfailed")
                    {
                        $array_where = array('track_id' => $order_id);
                        $fields_update = array('status' => strtolower($order_status),
                                               'pg_track_id' => $tracking_id,
                                               'bank_ref_no' => $bank_ref_no,  
                                               'response' => json_encode($array_response)
                                              );

                        
                        $pay_trans_true_false = $this->ci->payment_transaction_fss_model->update_where($array_where,'', $fields_update);
                       
                        if($pay_trans_true_false)
                        {
                            //Insert into payment_transaction_metadata
                           
                            $trans_metadata = array("payment_transaction_id"=>$pay_trans_true_false,
                                                    "order_id"=>$order_id,
                                                    "tracking_id"=>$tracking_id,
                                                    "bank_ref_no"=>$bank_ref_no,
                                                    "order_status"=>$order_status,
                                                    "failure_message"=>$failure_message,
                                                    "payment_mode"=>$payment_mode,
                                                    "card_name"=>$card_name,
                                                    "status_code"=>$status_code,
                                                    "status_message"=>$status_message,
                                                    "currency"=>$currency,
                                                    "amount"=>$amount,
                                                    "billing_name"=>$billing_name,
                                                    "billing_address"=>$billing_address,
                                                    "billing_city"=>$billing_city,
                                                    "billing_state"=>$billing_state,
                                                    "billing_zip"=>$billing_zip,
                                                    "billing_country"=>$billing_country,
                                                    "billing_tel"=>$billing_tel,
                                                    "billing_email"=>$billing_email,
                                                    "delivery_name"=>$delivery_name,
                                                    "delivery_address"=>$delivery_address,
                                                    "delivery_city"=>$delivery_city,
                                                    "delivery_state"=>$delivery_state,
                                                    "delivery_zip"=>$delivery_zip,
                                                    "delivery_country"=>$delivery_country,
                                                    "delivery_tel"=>$delivery_tel,
                                                    "merchant_param1"=>$merchant_param1,
                                                    "merchant_param2"=>$merchant_param2,
                                                    "merchant_param3"=>$merchant_param3,
                                                    "merchant_param4"=>$merchant_param4,
                                                    "merchant_param5"=>$merchant_param5,
                                                    "vault"=>$vault,
                                                    "offer_type"=>$offer_type,
                                                    "offer_code"=>$offer_code,
                                                    "discount_value" => $discount_value
                                                    );

                            $ptm_id = $this->ci->db->insert('payment_transaction_metadata', $trans_metadata); 
            
                            /*
                                $merchant_param1 -> ticket_ref_no;
                                $merchant_param2 -> ticket_id;
                                $merchant_param3 -> user_id;
                            */
                            $this->ci->db->where(array("ticket_id" => $merchant_param2,"transaction_status" => "pfailed"))->update("tickets",array("transaction_status" => "psuccess"));
                            $data["is_updated"] = true;
                            $cc_result = "CAPTURED";
                        }
                        else
                        {

                            $this->ci->db->where(array("ticket_id" => $merchant_param2))->update("tickets",array("transaction_status" => "psuccess","failed_reason" => "payment transaction table not updated"));
                            $data["is_updated"] = false;

                            $custom_error_mail = array(
                                                        "custom_error_subject" => "Transaction table not updated.",
                                                        "custom_error_message" => "payment success but Ticket Not Booked because Transaction table not updated for ticket_id - ".$merchant_param2
                                                      );

                            $this->ci->common_model->developer_custom_error_mail($custom_error_mail);
                        }

                        $data["pay_trans_true_false"] = $pay_trans_true_false;
                    }
                    else
                    {
                        $redirected_to = "home";
                        $redirect_to_home = true;
                        if($check_ticket_details[0]->transaction_status == "success" && $check_ticket_details[0]->pnr_no != "")
                        {
                            $redirected_to = "view ticket";
                            $redirect_to_home = false;
                        }
                        
                        $custom_error_mail = array(
                                                    "custom_error_subject" => "Double Hit (Page Refresh) & redirected to ".$redirected_to,
                                                    "custom_error_message" => "Page refreshed or Made double hit in the process after payment done. So we have redirected this user to the <b>".$redirected_to."</b> page. please find the use details for ticket id ".$data["ticket_id"]."<br/><br/>".json_encode($check_ticket_details)
                                                    );

                        $this->ci->common_model->developer_custom_error_mail($custom_error_mail);

                        if($redirect_to_home)
                        {
                            redirect(base_url());
                        }
                        else
                        {
                            redirect(base_url()."front/booking/view_ticket/".$check_ticket_details[0]->ticket_ref_no);
                        }
                    }
                }
                else 
                {
                    /************ Mail Start Here ********/
                    eval(ADMIN_EMAIL_ARRAY);
                    /*$mail_replacement_array = array(
                                                    '{{ticket_id}}' => $data["ticket_id"],
                                                    '{{transaction_status}}' => (isset($order_status)) ? strtolower($order_status) : "failed",
                                                    '{{site_url}}' => base_url()

                                                    );
                    $mailbody_array = array(
                                            "mail_title" => PG_TRANSACTION_FAILED_MAIL_TEMPLATE,
                                            "mail_client" => $admin_array,
                                            "mail_replacement_array" => $mail_replacement_array 
                                            );

                    $mail_result = setup_mail($mailbody_array);*/

                    $pg_failed_data = array(
                                            "ticket_id" => $data["ticket_id"],
                                            "transaction_status" => (isset($order_status)) ? strtolower($order_status) : "failed",
                                            "site_url" => base_url(),
                                            "sent_to" => $admin_array
                                        );

                    /*$this->ci->common_model->payment_gateway_failed_mail($pg_failed_data);*/
                    $this->ci->common_model->management_payment_gateway_failed_mail($pg_failed_data);

                    /************ Mail End Here ********/
                    $cca_status_message = "";

                    if(isset($status_message) && $status_message != "")
                    {
                        $cca_status_message = $status_message;
                    }
                    
                    $cc_update_tickets = array(
                                                "transaction_status" => strtolower($order_status), 
                                                "payment_by" => "ccavenue",
                                                "failed_reason" => $cca_status_message,
                                                "pg_tracking_id" => isset($tracking_id) ? $tracking_id : "",
                                                "payment_mode" => isset($payment_mode) ? $payment_mode : ""
                                              );

                    $this->ci->db->where(array("ticket_id" => $merchant_param2))->update("tickets", $cc_update_tickets);

                    $rpt_data = $this->ci->return_ticket_mapping_model->get_rpt_mapping($data["ticket_id"]);

                    if($rpt_data)
                    {
                        $this->ci->db->where(array("ticket_id" => $rpt_data[0]->ticket_id))->update("tickets", $cc_update_tickets);
                    }

                    /*$this->ci->db->where(array("ticket_id" => $merchant_param2))->update("tickets", array("transaction_status" => strtolower($order_status), "payment_by" => "ccavenue"));*/

                    $cc_update_payment_transaction_fss = array(
                                                "response" => json_encode($pgres),
                                                "status" => strtolower($order_status), 
                                                "payment_by" => "cca",
                                                "pg_track_id" => isset($tracking_id) ? $tracking_id : ""
                                              );

                    $this->ci->db->where(array("track_id" => $order_id))->update("payment_transaction_fss",$cc_update_payment_transaction_fss);
                   
                    // $this->ci->db->where(array("track_id" => $order_id))->update("payment_transaction_fss",array("response" => json_encode($pgres)));
                    // $this->ci->db->where(array("track_id" => $ResTrackID))->update("payment_transaction_fss",array("response" => json_encode($pgres)));
                    /* NOTE - MERCHANT MUST LOG THE RESPONSE RECEIVED IN LOGS AS PER BEST PRACTICE */
                    $data["is_updated"] = false;
                    $data["error_msg"] = "Transaction not successfull";
                    // $REDIRECT = 'http://www.merchantdemo.com/StatusTRAN.php?ResResult='.$ResResult.'&ResTrackId='.$ResTrackID.'&ResAmount='.$ResAmount.'&ResPaymentId='.$ResPaymentId.'&ResRef='.$ResRef.'&ResTranId='.$ResTranId.'&ResError='.$ResErrorText.'&ResAuthResCode='.$ResauthRespCode;   
                    // header("location:". $REDIRECT );
                }
            } 
            else 
            {
                /************ Mail Start Here ********/
                eval(ADMIN_EMAIL_ARRAY);
                // $mail_replacement_array = array('{{ticket_id}}' => $data["ticket_id"]);
                /*$mail_replacement_array = array(
                                                '{{ticket_id}}' => $data["ticket_id"],
                                                '{{transaction_status}}' => (isset($order_status)) ? strtolower($order_status) : "failed",
                                                '{{site_url}}' => base_url()

                                                );
                $mailbody_array = array(
                                        "mail_title" => PG_TRANSACTION_FAILED_MAIL_TEMPLATE,
                                        "mail_client" => $admin_array,
                                        "mail_replacement_array" => $mail_replacement_array 
                                        );

                $mail_result = setup_mail($mailbody_array);*/
                $pg_failed_data = array(
                                            "ticket_id" => $data["ticket_id"],
                                            "transaction_status" => (isset($order_status)) ? strtolower($order_status) : "failed",
                                            "site_url" => base_url(),
                                            "sent_to" => $admin_array
                                        );

                /*$this->ci->common_model->payment_gateway_failed_mail($pg_failed_data);*/
                $this->ci->common_model->management_payment_gateway_failed_mail($pg_failed_data);
                

                /************ Mail End Here ********/
              /*
              ERROR IN TRANSACTION PROCESSING
              IMPORTANT NOTE - MERCHANT SHOULD UPDATE 
              TRANACTION PAYMENT STATUS IN MERCHANT DATABASE AT THIS POSITION 
              AND THEN REDIRECT CUSTOMER ON RESULT PAGE
              */
                $this->ci->db->where(array("ticket_id" => $data["ticket_id"]))->update("tickets",array("transaction_status" => "pfailed", "payment_by" => "ccavenue", "failed_reason" => "error from ccavenue payment gateway. Failure Message - ".$failure_message));
                $data["is_updated"] = false;
                $data["error_msg"] = "Transaction not successfull";
            }
        }
        catch(Exception $e)
        {
          var_dump($e);
        }

        $data["pg_response"] = $array_response;
        $data["pg_response"]["result"] = $cc_result;
        $data["transaction_order_status"] = (isset($order_status)) ? strtolower($order_status) : "failed";
        
        return $data;
    }



   /*
    *Author : Suraj Rathod
    *Function : refundOrder()
    *Detail : To refund order
    */
    public function refundOrder($merchant_array_data)
    {
        log_data("refund/ccavenue_refund".date("d-m-Y")."_log.log",$merchant_array_data, 'Merchant Array Data');

        // Generate json data after call below method
        $merchant_data = json_encode($merchant_array_data);
         
        log_data("refund/ccavenue_refund".date("d-m-Y")."_log.log",$merchant_data, 'Merchant Json Data');

        // Encrypt merchant data with working key shared by ccavenue
        $encrypted_data = encrypt($merchant_data, $this->ci->config->item("cca_working_key"));
         
        //make final request string for the API call
        $final_data = array(
                            "request_type"  => "JSON",
                            "access_code"   => $this->ci->config->item("cca_access_code"),
                            "command"       => "refundOrder",
                            "response_type" => "JSON",
                            "enc_request"   => $encrypted_data
                            );

        log_data("refund/ccavenue_refund".date("d-m-Y")."_log.log",$final_data,'Final Refund Parameter Sent To CCavenue');

        $response = sendDataOverPost($this->ci->config->item("cca_production_refund_url"),$final_data);

        log_data("refund/ccavenue_refund".date("d-m-Y")."_log.log",$response,'Refund Response From CCavenue');

        parse_str($response, $response_encrypted);

        $response_decrypted = decrypt($response_encrypted["enc_response"], $this->ci->config->item("cca_working_key"));

        //this is done because json response not coming properly.
        $response_decrypted   =  substr($response_decrypted, 0, strrpos($response_decrypted,"}")+1);

        log_data("refund/ccavenue_refund".date("d-m-Y")."_log.log",$response_decrypted,'Refund Response From CCavenue');

        $response_decode = json_decode($response_decrypted);
        
        log_data("refund/ccavenue_refund".date("d-m-Y")."_log.log",$response_decode,'Refund From CCavenue');

        return $response_decode->Refund_Order_Result;
    }// End Of refundOrder
    
       /*
    *Author : Suraj Rathod
    *Function : cancelOrder()
    *Detail : To cancel order
    */
    public function cancelOrder($merchant_array_data, $extra_data = array())
    {
        log_data("cancelOrder/ccavenue_refund".date("d-m-Y")."_log.log",$merchant_array_data, 'Merchant Array Data');

        // Generate json data after call below method
        $merchant_data = json_encode($merchant_array_data);

        log_data("cancelOrder/ccavenue_refund".date("d-m-Y")."_log.log",$merchant_data, 'Merchant Json Data');

        // Encrypt merchant data with working key shared by ccavenue
        $encrypted_data = encrypt($merchant_data, $this->ci->config->item("cca_working_key"));
         
        //make final request string for the API call
        $final_data = array(
                            "request_type"  => "JSON",
                            "access_code"   => $this->ci->config->item("cca_access_code"),
                            "command"       => "cancelOrder",
                            "response_type" => "JSON",
                            "enc_request"   => $encrypted_data
                            );

        log_data("cancelOrder/ccavenue_refund".date("d-m-Y")."_log.log",$final_data,'Final Refund Parameter Sent To CCavenue');
        if($final_data)
        {
            $is_exist = $this->ci->payment_gateway_cancel_order_model->where(["ticket_id" => $extra_data['ticket_id']])->find_all();
            /*if(!$is_exist)
            {*/
                $data_to_insert = array ( "ticket_id" => $extra_data['ticket_id'],
                                          "pg_tracking_id" => $extra_data['pg_tracking_id'],
                                          "type" => $extra_data['transaction_status'],
                                          "request"         => $merchant_data
                                         );
                $insert_data = $this->ci->payment_gateway_cancel_order_model->insert($data_to_insert);
            /*}*/
        }
        $response = sendDataOverPost($this->ci->config->item("cca_production_refund_url"),$final_data);

        log_data("cancelOrder/ccavenue_refund".date("d-m-Y")."_log.log",$response,'Refund Response From CCavenue');

        parse_str($response, $response_encrypted);

        $response_decrypted = decrypt($response_encrypted["enc_response"], $this->ci->config->item("cca_working_key"));
        //this is done because json response not coming properly.
        $response_decrypted   =  substr($response_decrypted, 0, strrpos($response_decrypted,"}")+1);

        log_data("cancelOrder/ccavenue_refund".date("d-m-Y")."_log.log",$response_decrypted,'Refund Response From CCavenue');
        if($response_decrypted)
       {
            $to_update = array('response' => $response_decrypted);
            $this->ci->db->where('ticket_id', $extra_data['ticket_id']);
            $this->ci->db->update('payment_gateway_cancel_order', $to_update);
       }
        $response_decode = json_decode($response_decrypted);
        
        log_data("cancelOrder/ccavenue_refund".date("d-m-Y")."_log.log",$response_decode,'Refund From CCavenue');

        return $response_decode->Order_Result;
    }// End Of cancelOrder



    /*
    *Author : Suraj Rathod
    *Function : paymentConfirmation()
    *Detail : To refund order
    */
    public function paymentConfirmation($order_array_data,$extra_array = array())
    {
        $payment_confirmation_array = array();
        log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",$order_array_data, 'Order Array Data');

        $order_list = array("order_List" => $order_array_data);

        // Generate json data after call below method
        $order_data = json_encode($order_list);

        log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",$order_data, 'Order Json Data');

        // Encrypt merchant data with working key shared by ccavenue
        $encrypted_data = encrypt($order_data, $this->ci->config->item("cca_working_key"));
         
        //make final request string for the API call
        $final_data = array(
                            "enc_request"   => $encrypted_data,
                            "access_code"   => $this->ci->config->item("cca_access_code"),
                            "request_type"  => "JSON",
                            "response_type" => "JSON",
                            "command"       => "confirmOrder"
                            );

        log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",$final_data,'Final confirmation Parameter Sent To CCavenue');

        $response = sendDataOverPost($this->ci->config->item("cca_production_api_url"),$final_data);

        log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",$response,'Confirmation Response From CCavenue');

        parse_str($response, $response_encrypted);

        $response_decrypted = decrypt($response_encrypted["enc_response"], $this->ci->config->item("cca_working_key"));

        if($response_encrypted["status"] == 0)
        {
            $payment_confirmation_array["status"] = "success";
        }
        else
        {
            $payment_confirmation_array["status"] = "error";
        }
        //this is done because json response not coming properly.
        $response_decrypted   =  substr($response_decrypted, 0, strrpos($response_decrypted,"}")+1);

        log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",$response_decrypted,'Confirmation Response From CCavenue');

        $response_decode = json_decode($response_decrypted);
        
        $payment_confirmation_array["order_result"] = $response_decode->Order_Result;

        log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",$response_decode,'Confirmation From CCavenue');

        /*         VIMP NOTICE
         *Make this dynamic
         *Currently making only one entry
         */
        $pc_array = array("ticket_id"=>isset($extra_array["ticket_id"]) ? $extra_array["ticket_id"] : "",
                        "pg_tracking_id"=> $order_array_data[0]->reference_no,
                        "amount"=> $order_array_data[0]->amount,
                        "request_parameter"=>$order_data,
                        "response_encrypted"=>$response,
                        "response_decrypted"=>$response_decrypted,
                        );

        $this->ci->db->insert('payment_confirmation', $pc_array);

        return $payment_confirmation_array;
    }// End Of paymentConfirmation


   /*
    *Author : Suraj Rathod
    *Function : paymentConfirmation_cron()
    *Detail : To refund order
    */
    public function paymentConfirmation_cron($order_array_data,$extra_array = array())
    {
        $payment_confirmation_array = array();
        if(empty($extra_array)) {
            log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",$order_array_data, 'Order Array Data');
        }
        else {
            log_data("transaction_confirmation_wallet_topup/ccavenue_confirmation_".date("d-m-Y")."_log.log",$order_array_data, 'Order Array Data');
        }

        $order_list = array("order_List" => $order_array_data);

        // Generate json data after call below method
        $order_data = json_encode($order_list);
        if(empty($extra_array)) {
            log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",$order_data, 'Order Json Data');
        }
        else {
            log_data("transaction_confirmation_wallet_topup/ccavenue_confirmation_".date("d-m-Y")."_log.log",$order_data, 'Order Json Data');
        }
        // Encrypt merchant data with working key shared by ccavenue
        $encrypted_data = encrypt($order_data, $this->ci->config->item("cca_working_key"));
         
        //make final request string for the API call
        $final_data = array(
                            "enc_request"   => $encrypted_data,
                            "access_code"   => $this->ci->config->item("cca_access_code"),
                            "request_type"  => "JSON",
                            "response_type" => "JSON",
                            "command"       => "confirmOrder"
                            );
        if(empty($extra_array)) {
            log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",$final_data,'Final confirmation Parameter Sent To CCavenue');
        }
        else {
            log_data("transaction_confirmation_wallet_topup/ccavenue_confirmation_".date("d-m-Y")."_log.log",$final_data,'Final confirmation Parameter Sent To CCavenue');
        }
        $response = sendDataOverPost($this->ci->config->item("cca_production_api_url"),$final_data);
        if(empty($extra_array)) {
            log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",$response,'Confirmation Response From CCavenue');
        }
        else {
            log_data("transaction_confirmation_wallet_topup/ccavenue_confirmation_".date("d-m-Y")."_log.log",$response,'Confirmation Response From CCavenue');
        }
        parse_str($response, $response_encrypted);

        $response_decrypted = decrypt($response_encrypted["enc_response"], $this->ci->config->item("cca_working_key"));

        /*if($response_encrypted["status"] == 0)
        {
            $payment_confirmation_array["status"] = "success";
        }
        else
        {
            $payment_confirmation_array["status"] = "error";
        }*/

        $payment_confirmation_array["status"] = $response_encrypted["status"];
        
        //this is done because json response not coming properly.
        $response_decrypted   =  substr($response_decrypted, 0, strrpos($response_decrypted,"}")+1);
        if(empty($extra_array)) {
            log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",$response_decrypted,'Confirmation Response From CCavenue');
        }
        else {
            log_data("transaction_confirmation_wallet_topup/ccavenue_confirmation_".date("d-m-Y")."_log.log",$response_decrypted,'Confirmation Response From CCavenue');
        }
        $response_decode = json_decode($response_decrypted);
        
        $payment_confirmation_array["order_result"] = $response_decode->Order_Result;
        if(empty($extra_array)) {
            log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",$response_decode,'Confirmation From CCavenue');
        }
        else {
            log_data("transaction_confirmation_wallet_topup/ccavenue_confirmation_".date("d-m-Y")."_log.log",$response_decode,'Confirmation From CCavenue');
        }
        /*         VIMP NOTICE
         *Make this dynamic
         *Currently making only one entry
         */
        /*$pc_array = array("ticket_id"=>isset($extra_array["ticket_id"]) ? $extra_array["ticket_id"] : "",
                        "pg_tracking_id"=> $order_array_data[0]->reference_no,
                        "amount"=> $order_array_data[0]->amount,
                        "request_parameter"=>$order_data,
                        "response_encrypted"=>$response,
                        "response_decrypted"=>$response_decrypted,
                        );

        $this->ci->db->insert('payment_confirmation', $pc_array);*/

        return $payment_confirmation_array;
    }// End Of paymentConfirmation

    public function topup_paymentConfirmation_cron($order_array_data,$extra_array = array())
    {
        $payment_confirmation_array = array();
        if(empty($extra_array)) {
            log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",$order_array_data, 'Order Array Data');
        }
        else {
            log_data("transaction_confirmation_wallet_topup/ccavenue_confirmation_".date("d-m-Y")."_log.log",$order_array_data, 'Order Array Data');
        }

        $order_list = array("order_List" => $order_array_data);

        // Generate json data after call below method
        $order_data = json_encode($order_list);
        if(empty($extra_array)) {
            log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",$order_data, 'Order Json Data');
        }
        else {
            log_data("transaction_confirmation_wallet_topup/ccavenue_confirmation_".date("d-m-Y")."_log.log",$order_data, 'Order Json Data');
        }
        // Encrypt merchant data with working key shared by ccavenue
        $encrypted_data = encrypt($order_data, $this->ci->config->item("cca_working_key"));
         
        //make final request string for the API call
        $final_data = array(
                            "enc_request"   => $encrypted_data,
                            "access_code"   => $this->ci->config->item("cca_access_code"),
                            "request_type"  => "JSON",
                            "response_type" => "JSON",
                            "command"       => "confirmOrder"
                            );
        if(empty($extra_array)) {
            log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",$final_data,'Final confirmation Parameter Sent To CCavenue');
        }
        else {
            log_data("transaction_confirmation_wallet_topup/ccavenue_confirmation_".date("d-m-Y")."_log.log",$final_data,'Final confirmation Parameter Sent To CCavenue');
        }
        $response = sendDataOverPost($this->ci->config->item("cca_production_api_url"),$final_data);
        if(empty($extra_array)) {
            log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",$response,'Confirmation Response From CCavenue');
        }
        else {
            log_data("transaction_confirmation_wallet_topup/ccavenue_confirmation_".date("d-m-Y")."_log.log",$response,'Confirmation Response From CCavenue');
        }
        parse_str($response, $response_encrypted);

        $response_decrypted = decrypt($response_encrypted["enc_response"], $this->ci->config->item("cca_working_key"));

        /*if($response_encrypted["status"] == 0)
        {
            $payment_confirmation_array["status"] = "success";
        }
        else
        {
            $payment_confirmation_array["status"] = "error";
        }*/

        $payment_confirmation_array["status"] = $response_encrypted["status"];
        
        //this is done because json response not coming properly.
        $response_decrypted   =  substr($response_decrypted, 0, strrpos($response_decrypted,"}")+1);
        if(empty($extra_array)) {
            log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",$response_decrypted,'Confirmation Response From CCavenue');
        }
        else {
            log_data("transaction_confirmation_wallet_topup/ccavenue_confirmation_".date("d-m-Y")."_log.log",$response_decrypted,'Confirmation Response From CCavenue');
        }
        $response_decode = json_decode($response_decrypted);
        
        $payment_confirmation_array["order_result"] = $response_decode->Order_Result;
        if(empty($extra_array)) {
            log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",$response_decode,'Confirmation From CCavenue');
        }
        else {
            log_data("transaction_confirmation_wallet_topup/ccavenue_confirmation_".date("d-m-Y")."_log.log",$response_decode,'Confirmation From CCavenue');
        }
        /*         VIMP NOTICE
         *Make this dynamic
         *Currently making only one entry
         */
        /*$pc_array = array("ticket_id"=>isset($extra_array["ticket_id"]) ? $extra_array["ticket_id"] : "",
                        "pg_tracking_id"=> $order_array_data[0]->reference_no,
                        "amount"=> $order_array_data[0]->amount,
                        "request_parameter"=>$order_data,
                        "response_encrypted"=>$response,
                        "response_decrypted"=>$response_decrypted,
                        );

        $this->ci->db->insert('payment_confirmation', $pc_array);*/

        return $payment_confirmation_array;
    }// End Of paymentConfirmation

   /*
    *Author     : Suraj Rathod
    *Function   : getRsaKey_api()
    *Detail     : To refund RSA Key For Mobile APP
    */
    public function getRsaKey_api($input = array())
    {
        $parameter_array    =   [];
        $return_ticket_id   = "";
        $parameter_array["ticket_ref_no"]  =   $input["block_key"];
        if($input && !empty($input["block_key"]))
        {
            $ticket_data        =   $this->ci->tickets_model->where([
                                                                "ticket_ref_no" => $input["block_key"],
                                                                "inserted_date < " => date("Y-m-d H:i:s"),
                                                                "inserted_date > " => date("Y-m-d",strtotime('-440 hour'))
                                                                ])->find_all();
            if($ticket_data)
            {
                $php_uniqid = uniqid();
                $order_id = $php_uniqid.$ticket_data[0]->ticket_id;
                $response_data              =   ["block_key" => $input["block_key"],"ticket_id" => $ticket_data[0]->ticket_id,"order_id" => $order_id];
                $post_array                 =   ["access_code" => $this->ci->config->item("cca_access_code"), "order_id" => $order_id];

                logging_data("cca/rsa_key.log",$post_array,"Request");
                $rsa_key                    =   CcaSendDataOverPost($this->ci->cca_rsa_key_url,$post_array);
                logging_data("cca/rsa_key.log",$rsa_key,"Response");

                $response_data["rsa_key"]   =   $rsa_key;
                
                $parameter_array["ticket_id"]   = $ticket_data[0]->ticket_id;

                if($input && !empty($input["block_key_return"]))
                {
                    $return_ticket_data        =   $this->ci->tickets_model->where([
                                                                                    "ticket_ref_no" => $input["block_key_return"],
                                                                                    "inserted_date < " => date("Y-m-d H:i:s"),
                                                                                    "inserted_date > " => date("Y-m-d",strtotime('-440 hour'))
                                                                                    ])->find_all();
                    if($return_ticket_data)
                    {
                        $response_data["return_ticket_id"]   = $return_ticket_data[0]->ticket_id;
                        $response_data["block_key_return"]   = $input["block_key_return"];
                        $return_ticket_id = $return_ticket_data[0]->ticket_id;
                        $this->ci->db->insert('return_ticket_mapping', ["ticket_id" => $ticket_data[0]->ticket_id,
                                                                        "return_ticket_id" => $return_ticket_data[0]->ticket_id
                                                                        ]
                                              );

                        $this->ci->db->where(array("ticket_id" => $ticket_data[0]->ticket_id))->update("tickets",array("is_return_journey" => '1','return_ticket_id' => $return_ticket_data[0]->ticket_id));
                    }
                }



                $response_array                 = array('status' => 'success' ,'data' => $response_data);
                $parameter_array["order_id"]    = $order_id;
                $parameter_array["pg_name"]     = "ccavenue";
                $parameter_array["rsa_key_for"] = isset($input["device"]) ? $input["device"] : "android";
                $parameter_array["request"]     = json_encode($input);
                $parameter_array["response"]    = json_encode($response_array);
                $parameter_array["key"]         = $rsa_key; 
            }
            else
            {
                $response_array = array('status' => 'failed' ,'error' => "Please provide valid block key.");
            }

            $parameter_array["ip_address"] = $this->ci->input->ip_address();
            $parameter_array["user_agent"] = $this->ci->input->user_agent();

            if($parameter_array && !empty($input["block_key"]))
            {
                if($this->ci->input->server('PHP_AUTH_USER'))
                {
                    $userdata   =   $this->ci->users_model->where("username",$this->ci->input->server('PHP_AUTH_USER'))->find_all();
                    if($userdata)
                    {
                        $parameter_array["user_id"] = $userdata[0]->id;
                    }
                }
                $this->ci->db->insert('payment_gateway_key', $parameter_array);
                $update_to_ticket = ['transaction_status' =>'pfailed'];
                $this->ci->db->where(array("ticket_id" =>$ticket_data[0]->ticket_id))->update('tickets',$update_to_ticket);
            }

            /*****************Insert into payment transaction start****************************/
            $payment_transaction_data = [
                                            "track_id" => $order_id,
                                            "payment_by" => "cca",
                                            "user_id" => $userdata[0]->id,
                                            "ticket_id" => $ticket_data[0]->ticket_id,
                                            "return_ticket_id" => $return_ticket_id,
                                            "total_amount" => $ticket_data[0]->tot_fare_amt_with_tax,
                                            "ip_address" => $this->ci->input->ip_address(),
                                            "user_agent" => ""
                                        ];

            $this->ci->db->insert('payment_transaction_fss', $payment_transaction_data);
            /*****************Insert into payment transaction end*******************************/

            return $response_array;
        }
        else
        {
            return false;
        }
    }// End Of getRsaKey_api

    public function GetTopupResponse($redirect = false)
    {
        $pgres = $this->ci->input->post(); 
        try {
            $current_working_key = $this->ci->config->item("cca_working_key_agent");
            
            $rcvdString = decrypt($pgres["encResp"], $current_working_key);     //Crypto Decryption used as per the specified working key.
            $cc_result = "";
            $order_status = "";

            parse_str($rcvdString);
            parse_str($rcvdString, $array_response);

            logging_data("pg/agent_ccavenue_pg_".date("d-m-Y")."_log.log",$array_response, 'Response From CCAvenue PG');

            logging_data("topup/topup_request_log.log",$array_response, 'Response From CCAvenue PG');

            $data["payment_tran_fss_id"] = $merchant_param1;
            $data["wallet_topup_id"] = $merchant_param2;
            $data["payment_by"] = "ccavenue";
            if($this->ci->session->userdata("user_id") > 0) {
                $user_id = $this->ci->session->userdata("user_id");
            }

            $title = array("order_id", "tracking_id", "bank_ref_no", "order_status", "failure_message", "payment_mode", "card_name", "status_code", "status_message", "currency", "amount", "billing_ name", "billing_ address", "billing_ city", "billing_ state", "billing_ zip", "billing_ country", "billing_ tel", "billing_ email", "delivery_ name", "delivery_ address", "delivery_ city", "delivery_ state", "delivery_ zip", "delivery_ country", "delivery_ tel", "merchant_param1", "merchant_param2", "merchant_param3", "merchant_param4", "merchant_param5", "vault", "offer_type", "offer_code", "discount_value", "IP Address", "User Agent");
            $val = array($order_id, $tracking_id, $bank_ref_no, $order_status, $failure_message, $payment_mode, $card_name, $status_code, $status_message, $currency, $amount, $billing_name, $billing_address, $billing_city, $billing_state, $billing_zip, $billing_country, $billing_tel, $billing_email, $delivery_name, $delivery_address, $delivery_city, $delivery_state, $delivery_zip, $delivery_country, $delivery_tel, $merchant_param1, $merchant_param2, $merchant_param3, $merchant_param4, $merchant_param5, $vault, $offer_type, $offer_code, $discount_value, $this->ci->session->userdata("ip_address"), $this->ci->session->userdata("user_agent"));
            $pgfile = PG_LOG . date("d-m-Y") . "_cca.csv";

            if (!file_exists($pgfile)) {
                $file = fopen($pgfile, "a");
                fputcsv($file, $title);
            }

            $file = fopen($pgfile, "a");
            fputcsv($file, $val);

            fclose($file);
            if ($order_id != '' && $failure_message == "") {

                $array_where = array('track_id' => $order_id);

                $fields_update = array('status' => strtolower($order_status),
                                        'pg_track_id' => $tracking_id,
                                        'bank_ref_no' => $bank_ref_no,
                                        'response' => json_encode($array_response)
                                        );
                
                $pay_trans_id = $this->ci->payment_transaction_fss_model->update_where($array_where, '', $fields_update);
            
                $trans_metadata = array(
                    "payment_transaction_id" => $pay_trans_id,
                    "order_id" => $order_id,
                    "tracking_id" => $tracking_id,
                    "bank_ref_no" => $bank_ref_no,
                    "order_status" => $order_status,
                    "failure_message" => $failure_message,
                    "payment_mode" => $payment_mode,
                    "card_name" => $card_name,
                    "status_code" => $status_code,
                    "status_message" => $status_message,
                    "currency" => $currency,
                    "amount" => $amount,
                    "billing_name" => $billing_name,
                    "billing_address" => $billing_address,
                    "billing_city" => $billing_city,
                    "billing_state" => $billing_state,
                    "billing_zip" => $billing_zip,
                    "billing_country" => $billing_country,
                    "billing_tel" => $billing_tel,
                    "billing_email" => $billing_email,
                    "delivery_name" => $delivery_name,
                    "delivery_address" => $delivery_address,
                    "delivery_city" => $delivery_city,
                    "delivery_state" => $delivery_state,
                    "delivery_zip" => $delivery_zip,
                    "delivery_country" => $delivery_country,
                    "delivery_tel" => $delivery_tel,
                    "merchant_param1" => $merchant_param1,
                    "merchant_param2" => $merchant_param2,
                    "merchant_param3" => $merchant_param3,
                    "merchant_param4" => $merchant_param4,
                    "merchant_param5" => $merchant_param5,
                    "vault" => $vault,
                    "offer_type" => $offer_type,
                    "offer_code" => $offer_code,
                    "discount_value" => $discount_value
                );
                $ptm_id = $this->ci->db->insert('payment_transaction_metadata', $trans_metadata);
                $data["is_updated"] = true;
                   
                if(strtolower($order_status) == 'success') {
                    $check_wallet_topup = $this->ci->wallet_topup_model->where(["id" => $data["wallet_topup_id"]])->find_all();
                    if($check_wallet_topup) {
                        if(strtolower($check_wallet_topup[0]->transaction_status) == "pfailed") {
                            $pg_array_where = array('id' => $data["wallet_topup_id"]);
                            $fields_update = array(
                                                    'transaction_status' => "success",
                                                    'pg_tracking_id' => $tracking_id,
                                                    'is_status' => "Y",
                                                    'is_approvel' => "A",
                                                    'bank_name' => $card_name
                                                    );
                            $is_wallet_topup_updated = $this->ci->wallet_topup_model->update_where($pg_array_where, '', $fields_update);

                            if($is_wallet_topup_updated) {
                                $wallet_data = $this->ci->wallet_model->where('user_id',$this->ci->session->userdata('user_id'))->find_all();
                                $new_wallet_id = "";
                                if(!$wallet_data) {
                                    //wallet creation
                                    $account_no = substr(hexdec(uniqid()), 4, 12);
                                    $new_wallet = [
                                                    'user_id' => $this->ci->session->userdata('user_id'),
                                                    'amt' => $array_response['mer_amount'],
                                                    'status' => 'Y',
                                                    'comment' => "added_by_self",
                                                    'added_by'=> $this->ci->session->userdata('user_id'),
                                                    'account_no' => $account_no
                                                 ];

                                    $new_wallet_id =    $this->ci->wallet_model->insert($new_wallet);

                                    $current_wallet_amount = 0;
                                    $current_wallet_id = $new_wallet_id;
                                    $to_update_wallet = false;
                                    
                                }
                                else {
                                    $current_wallet_amount = $wallet_data[0]->amt;
                                    $current_wallet_id = $wallet_data[0]->id;
                                    $to_update_wallet = true;
                                }

                                $transaction_no = substr(hexdec(uniqid()), 4, 12);
                                $trns_data['transaction_type_id'] = '1';
                                $trns_data['w_id'] = $current_wallet_id;
                                $trns_data['amt'] = $array_response['mer_amount'];
                                $trns_data['wallet_type'] = 'actual_wallet';
                                $trns_data['comment'] = 'payment by ccavenue';
                                $trns_data['status'] = 'Amount Credited';
                                $trns_data['user_id'] = $this->ci->session->userdata('user_id');
                                $trns_data['added_by'] = $this->ci->session->userdata('user_id');
                                $trns_data['added_on'] = date("Y-m-d H:i:s", time());
                                $trns_data['topup_id'] = $data["wallet_topup_id"];
                                $trns_data['transaction_type'] = 'Credited';
                                $trns_data['is_status'] = 'Y'; 
                                $trns_data['amt_before_trans'] = $current_wallet_amount;
                                $trns_data['amt_after_trans'] = $current_wallet_amount + $array_response['mer_amount']; 
                                $trns_data['transaction_no'] =  $transaction_no;                             
                                $wallet_trans_id = $this->ci->common_model->insert('wallet_trans',$trns_data);
                                if($to_update_wallet) {
                                    
                                    $wallet_to_update = array(
                                                              'amt' => $array_response['mer_amount']+$current_wallet_amount,
                                                              'updated_by'=> $this->ci->session->userdata('user_id'),
                                                              'updated_date'=> date('Y-m-d h:m:s'),
                                                              'last_transction_id' => $wallet_trans_id
                                                              );

                                    $this->ci->db->where('user_id',$this->ci->session->userdata('user_id'));
                                    $this->ci->db->update('wallet', $wallet_to_update);

                                    $title = array('User Id','Amount','Amount Befor','Amount After','Topup Id','Ticket Id','Date','Detail');
                                    $val = array ($this->ci->session->userdata('user_id'),$array_response['mer_amount'],$wallet_data[0]->amt,$array_response['mer_amount']+$wallet_data[0]->amt,$data["wallet_topup_id"],'',date("Y-m-d H:i:s"),'Function Detail : users/get_cca_response, Detail : Add Amount into wallet from payment gateway');
                                
                                    $FileName = "wallet/user_wallet_detail/wallet_".$this->ci->session->userdata('user_id').".csv";

                                    make_csv($FileName,$title,$val);
                                    
                                }
                                
                                /* Debit Entry For Rokad In Wallet Trans Table*/
                                $RollID = COMPANY_ROLE_ID;
                                $transaction_no = substr(hexdec(uniqid()), 4, 12);
                                $RokadUserData = $this->ci->common_model->get_value('users','id','role_id='.$RollID); 
                                $RokadWalletData = $this->ci->common_model->get_value('wallet','id,amt','user_id='.$RokadUserData->id);
                                $trans_data['transaction_type_id'] = '14';
                                $trans_data['w_id'] = $RokadWalletData->id;
                                $trans_data['amt'] = $array_response['mer_amount'];
                                $trans_data['wallet_type'] = 'actual_wallet';
                                $trans_data['comment'] = 'payment by ccavenue';
                                $trans_data['status'] = 'Amount Debited';
                                $trans_data['user_id'] = $RokadUserData->id;
                                $trans_data['added_by'] = $this->ci->session->userdata('user_id');
                                $trans_data['added_on'] = date("Y-m-d H:i:s", time());
                                $trans_data['topup_id'] = $data["wallet_topup_id"];
                                $trans_data['transaction_type'] = 'Debited';
                                $trans_data['is_status'] = 'Y'; 
                                $trans_data['amt_before_trans'] = $RokadWalletData->amt;
                                $trans_data['amt_after_trans'] = $RokadWalletData->amt+$array_response['mer_amount'];                               
                                $trans_data['transaction_no'] = $transaction_no;
                                $rokad_wallet_trans_id = $this->ci->common_model->insert('wallet_trans',$trans_data);
                                    
                                if($rokad_wallet_trans_id) {
                                    
                                    $wallet_to_update = array(
                                                              'amt' =>$RokadWalletData->amt-$array_response['mer_amount'],
                                                              'updated_by'=> $this->ci->session->userdata('user_id'),
                                                              'updated_date'=> date('Y-m-d h:m:s'),
                                                              'last_transction_id' => $rokad_wallet_trans_id
                                                              );

                                    $this->ci->db->where('user_id',$RokadUserData->id);
                                    $this->ci->db->update('wallet', $wallet_to_update);
                                    $title = array('User Id','Amount','Amount Befor','Amount After','Topup Id','Ticket Id','Date','Detail');
                                    $val = array ($this->ci->session->userdata('user_id'),$array_response['mer_amount'],$RokadWalletData->amt,$RokadWalletData->amt-$array_response['mer_amount'],$data["wallet_topup_id"],'',date("Y-m-d H:i:s"),'Function Detail : users/get_cca_response, Detail : Add Amount into wallet from payment gateway');
                                
                                    $FileName = "wallet/user_wallet_detail/wallet_".$RokadUserData->id.".csv";

                                    make_csv($FileName,$title,$val);
                                    
                                }

                                /* send email and sms to admin and client*/
                                $firstname = $this->ci->session->userdata('first_name');
                                $lastname  = $this->ci->session->userdata('last_name');

                                $mail_replacement_array =   array (
                                                "{{topup_by}}"                         => $check_wallet_topup[0]->topup_by,
                                                "{{amount}}"                           => round($check_wallet_topup[0]->amount,2),
                                                "{{bank_name}}"                        => $check_wallet_topup[0]->bank_name,
                                                "{{bank_acc_no}}"                      => $check_wallet_topup[0]->bank_acc_no,
                                                "{{transaction_no}}"                   => $check_wallet_topup[0]->transaction_no,
                                                "{{transaction_remark}}"               => $check_wallet_topup[0]->transaction_remark,
                                                "{{username}}"                         => $firstname.' '.$lastname,
                                                "{{useremail}}"                        => $this->ci->session->userdata('email'),
                                                "{{transaction_date}}"                 => date('d-m-Y H:i:s',strtotime($check_wallet_topup[0]->created_date)),
                                                "{{is_approvel}}"                      => $check_wallet_topup[0]->is_approvel
                                            );

                                eval(ADMIN_EMAIL_ARRAY_FOR_AGENT);//for admin
                                $data['mail_content']   = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),WALLET_TOPUP_REQUEST);
                                $mailbody_array_admin   =   array("subject" => "TopUp Request #'".$order_id."' of Rs ".round($check_wallet_topup[0]->amount,2)." by ".$firstname.' '.$lastname."  is successfull ",
                                                                  "message" => $data['mail_content'],
                                                                  "to"      => $admin_array_for_agent
                                                            );
                                $mail_result_admin      = send_mail($mailbody_array_admin);  
                                
                                //for User
                                $UserEmail = $this->ci->session->userdata('email');
                                $UserMobile = $this->ci->session->userdata('mobile_no');
                                $mailbody_array_agnt    =   array("subject" => "TopUp Request #".$order_id." of Rs ".round($check_wallet_topup[0]->amount,2)." is successfull ",
                                                                  "message" => $data['mail_content'],
                                                                  "to"      => $UserEmail
                                                            );
                                $mail_result_agent      = send_mail($mailbody_array_agnt);  
                                
                                
                                if($mail_result_admin && $mail_result_agent)
                                {
                                   $data['flag']        = "@#success#@";
                                }
                                
                                //sms to agent
                                $mail_replacement_arraysms =   array(
                                                                    "{{username}}" => $firstname.' '.$lastname, 
                                                                    "{{wallet_id}}" => $wallet_data[0]->account_no,
                                                                    "{{transaction_amount}}" => $array_response['mer_amount'],
                                                                    "{{wallet_amount}}" => $array_response['mer_amount']+$wallet_data[0]->amt
                                                                    ); 
                                $flag     = true;
                                if($flag == true)
                                {
                                    $msg_to_sent = str_replace(array_keys($mail_replacement_arraysms),array_values($mail_replacement_arraysms),WALLET_TOPUP_FROM_PG);
                                    $is_send     = sendSMS($UserMobile, $msg_to_sent);
                                    if($is_send)
                                    {
                                       $data['sms']       = "@#success#@";
                                    }
                                }
                                $data["page_msg_type"]   = "success";
                                $data["page_msg"]        = "Amount added to wallet successfully.";
                            }
                        }
                        else
                        {
                            
                            $custom_error_mail = array(
                                                        "custom_error_subject" => "Important : Wallet Double Hit (Page Refresh)",
                                                        "custom_error_message" => "Page refreshed or Made double hit in the process after payment done in agent wallet. Raw data to track. ".json_encode($array_response)
                                                        );

                            $this->ci->common_model->developer_custom_error_mail($custom_error_mail);

                            
                            $data["page_msg_type"]   = "error";
                            $data["page_msg"]        = "Please contact customer support.";
                        }
                    }
                    else
                    {
                        //no data found mail
                        
                        $custom_error_mail = array(
                                                    "custom_error_subject" => "Important : Wallet Topup table data not found.",
                                                    "custom_error_message" => "Agent Wallet topup data not found. Please Track. Raw data to track. ".json_encode($array_response)
                                                    );

                        $this->ci->common_model->developer_custom_error_mail($custom_error_mail);

                        $data["page_msg_type"]   = "error";
                        $data["page_msg"]        = "Please contact customer support.";
                    }
                }
                else
                {
                    
                    //pfailed reason
                    $pg_array_where = array('id' => $data["wallet_topup_id"]);
                    $fields_update = array('transaction_status' => strtolower($order_status),'is_approvel' => strtolower($order_status));
                    $is_wallet_topup_updated = $this->ci->wallet_topup_model->update_where($pg_array_where, '', $fields_update);
                    
                    $custom_error_mail = array(
                                                "custom_error_subject" => "Wallet Topup ".strtolower($order_status),
                                                "custom_error_message" => "Reason : ".$failure_message." <br/> <br/>Agent Wallet topup failed. ".json_encode($array_response)
                                                );

                    $this->ci->common_model->developer_custom_error_mail($custom_error_mail);

                    $data["page_msg_type"]   = "error";
                    $data["page_msg"]        = "Payment not done. Please try again";
                }
            } 
            else
            {
                
                $pg_array_where = array('id' => $data["wallet_topup_id"]);
                $fields_update = array('transaction_status' => strtolower($order_status),'is_approvel' => strtolower($order_status));
                $is_wallet_topup_updated = $this->ci->wallet_topup_model->update_where($pg_array_where, '', $fields_update);

                $custom_error_mail = array(
                                            "custom_error_subject" => "Important : Order ID is missing.",
                                            "custom_error_message" => "Agent Wallet topup failed. Order ID is missing. ".json_encode($array_response)
                                            );

                $this->ci->common_model->developer_custom_error_mail($custom_error_mail);

                $data["page_msg_type"]   = "error";
                $data["page_msg"]        = "Payment not done. Please try again";
            }
        } 
        catch (Exception $e)
        {
            
            var_dump($e);
        }
        $RollID = $this->ci->session->userdata("role_id");
        if($RollID == MASTER_DISTRIBUTOR_ROLE_ID){
          $data["page_redirect"]   = base_url()."admin/fund_request";
        } else if($RollID == AREA_DISTRIBUTOR_ROLE_ID){
          $data["page_redirect"]   = base_url()."admin/fund_request";  
        } else if($RollID == DISTRIBUTOR){
          $data["page_redirect"]   = base_url()."admin/fund_request"; 
        } else {
            if($redirect) {
              $data["page_redirect"]   = base_url()."admin/users/get_profile";  
            }
            else {
                $data["page_redirect"]   = base_url()."admin/fund_request"; 
            }
          
        }
        return $data;
    }

    public function GetCcavenueData($ccavenue_array_data)
    {
        $ccavenue_data = json_encode($ccavenue_array_data);
     
        log_data("reconsillation_ccavenue/ccavenue_data" . date("d-m-Y") . "_log.log", $ccavenue_data, 'Merchant Json Data');

        $encrypted_data = encrypt($ccavenue_data, $this->ci->config->item("cca_working_key"));
        
        log_data("reconsillation_ccavenue/ccavenue_data" . date("d-m-Y") . "_log.log", $payout_summary_encrypted_data, 'payout_summary_encrypted_json Data');  
        
        $sending_data = array(
                            "request_type" => "JSON",
                            "access_code" => $this->ci->config->item("cca_access_code"),
                            "command" => "orderStatusTracker",
                            "response_type" => "JSON",
                            "reference_no" =>  $ccavenue_array_data['reference_no'],
                            "enc_request" => $encrypted_data,
                          );
        $sending_json_data = json_encode($sending_data);

        if($sending_data)
        {
            $is_exist = $this->ci->tracking_request_response_model->where(["pg_tracking_id" => $ccavenue_array_data['reference_no']])->find_all();

            if(!$is_exist)
            {
                $data = [];
                $data['pg_tracking_id']  = $ccavenue_array_data['reference_no'];
                $data['type'] = 'getccavenue_details';
                $data['ticket_id'] = $ccavenue_array_data['ticket_id'];
                $data['request'] = $sending_json_data;
                $insert_request = $this->ci->tracking_request_response_model->insert($data);
            }
        }

       log_data("reconsillation_ccavenue/ccavenue_data".date("d-m-Y")."_log.log",$sending_data,'Parameter Sent To CCavenue');
       $response = sendDataOverPost($this->ci->config->item("cca_production_api_url"),$sending_data);
       log_data("reconsillation_ccavenue/ccavenue_data".date("d-m-Y")."_log.log",$response,'Response From CCavenue');
       
       parse_str($response, $response_encrypted);

       $response_decrypted = decrypt($response_encrypted["enc_response"], $this->ci->config->item("cca_working_key"));

       log_data("reconsillation_ccavenue/ccavenue_data".date("d-m-Y")."_log.log",$response_decrypted,'Get Response From CCavenue');

       $response_decrypted   =  substr($response_decrypted, 0, strrpos($response_decrypted,"}")+1);

       $response_decode = json_decode($response_decrypted);      

       if($response_decode)
       {
            $to_update = array('response' => $response_decrypted);
            $this->ci->db->where('pg_tracking_id', $ccavenue_array_data['reference_no']);
            $this->ci->db->update('tracking_request_response', $to_update);
       }

        log_data("reconsillation_ccavenue/ccavenue_data".date("d-m-Y")."_log.log",$response_decode,'Json Response From CCavenue');

        $data['response']   = $response_decode->Order_Status_Result;
        $data['status']     = ($response_decode->Order_Status_Result->status == 0) ? "success" : "failed";
       
         return $data ;
    }

    public function GetpayoutData($payout_summary_details)
    {
       $payout_summary_json = json_encode($payout_summary_details);   
        
       log_data("reconsillation_ccavenue/ccavenue_data" . date("d-m-Y") . "_log.log", $payout_summary_json, 'payout_summary_json Data');  
       
       $payout_summary_encrypted_data = encrypt($payout_summary_json, $this->ci->config->item("cca_working_key"));
       
       $payout_summary_sending=array(
                                    "access_code" => $this->ci->config->item("cca_access_code"),
                                    "command" => 'payoutSummary',
                                    "request_type" => "JSON",
                                    "response_type" => "JSON",
                                    "settlement_date" =>  $payout_summary_details['settlement_date'],
                                    "enc_request" => $payout_summary_encrypted_data,
                                  );
       $payout_summary_sending_data = json_encode($payout_summary_sending);   
       
       log_data("reconsillation_ccavenue/ccavenue_data".date("d-m-Y")."_log.log",$payout_summary_sending_data,'Parameter Sent To CCavenue'); 

       $payout_response = sendDataOverPost($this->ci->config->item("cca_production_api_url"),$payout_summary_sending);
       
       parse_str($payout_response, $response_encrypted);

       $response_decrypted = decrypt($response_encrypted["enc_response"], $this->ci->config->item("cca_working_key"));
     
       log_data("reconsillation_ccavenue/ccavenue_data".date("d-m-Y")."_log.log",$response_decrypted,'Get Response From CCavenue');
       
       $payout = json_decode($response_decrypted); 

        $data['status'] = ($payout->Payout_Summary_Result->error_code == "") ? "success" : "failed";
        $data['response'] = $payout->Payout_Summary_Result;
        return $data ;
    }

    public function payIdDetails($payid_summary_details)
    {
        $payid_data = json_encode($payid_summary_details);

        log_data("reconsillation_ccavenue/ccavenue_data" . date("d-m-Y") . "_log.log", $payid_data, 'payid Json Data');

        $encrypted_payid_data = encrypt($payid_data, $this->ci->config->item("cca_working_key"));
        
        log_data("reconsillation_ccavenue/ccavenue_data" . date("d-m-Y") . "_log.log", $encrypted_payid_data, 'payid_encrypted_json Data');  
        
        $sending_payid_data = array(
                            "request_type" => "JSON",
                            "access_code" => $this->ci->config->item("cca_access_code"),
                            "command" => "payIdDetails",
                            "response_type" => "JSON",
                            "pay_id" =>  $payid_summary_details['pay_id'],
                            "enc_request" => $encrypted_payid_data,
                          );
       log_data("reconsillation_ccavenue/ccavenue_data".date("d-m-Y")."_log.log",$sending_payid_data,'Parameter Sent To CCavenue for payid');
        
       $response = sendDataOverPost($this->ci->config->item("cca_production_api_url"),$sending_payid_data);

       log_data("reconsillation_ccavenue/ccavenue_data".date("d-m-Y")."_log.log",$response,'Response From CCavenue for payid');
        
       parse_str($response, $response_encrypted);

       $response_decrypted = decrypt($response_encrypted["enc_response"], $this->ci->config->item("cca_working_key"));

       log_data("reconsillation_ccavenue/ccavenue_data".date("d-m-Y")."_log.log",$response_decrypted,'Get Response From CCavenue');

       $response_decrypted   =  substr($response_decrypted, 0, strrpos($response_decrypted,"}")+1);

       $payid_response_decode = json_decode($response_decrypted);      

        $data['response'] = $payid_response_decode->pay_id_details_Result;
        $data['status']   = ($payid_response_decode->pay_id_details_Result->error_code == "") ? "success" : "failed";;

        return $data;
    }
     public function GetOperatorTopupResponse($redirect = false)
    {
        $pgres = $this->ci->input->post(); 
        try {
            $current_working_key = $this->ci->config->item("cca_working_key_agent");
            
            $rcvdString = decrypt($pgres["encResp"], $current_working_key);     //Crypto Decryption used as per the specified working key.
            $cc_result = "";
            $order_status = "";

            parse_str($rcvdString);
            parse_str($rcvdString, $array_response);

            logging_data("pg/agent_ccavenue_pg_".date("d-m-Y")."_log.log",$array_response, 'Response From CCAvenue PG');

            logging_data("topup/topup_request_log.log",$array_response, 'Response From CCAvenue PG');

            $data["payment_tran_fss_id"] = $merchant_param1;
            $data["wallet_topup_id"] = $merchant_param2;
            $data["payment_by"] = "ccavenue";
            if($this->ci->session->userdata("user_id") > 0) {
                $user_id = $this->ci->session->userdata("user_id");
            }

            $title = array("order_id", "tracking_id", "bank_ref_no", "order_status", "failure_message", "payment_mode", "card_name", "status_code", "status_message", "currency", "amount", "billing_ name", "billing_ address", "billing_ city", "billing_ state", "billing_ zip", "billing_ country", "billing_ tel", "billing_ email", "delivery_ name", "delivery_ address", "delivery_ city", "delivery_ state", "delivery_ zip", "delivery_ country", "delivery_ tel", "merchant_param1", "merchant_param2", "merchant_param3", "merchant_param4", "merchant_param5", "vault", "offer_type", "offer_code", "discount_value", "IP Address", "User Agent");
            $val = array($order_id, $tracking_id, $bank_ref_no, $order_status, $failure_message, $payment_mode, $card_name, $status_code, $status_message, $currency, $amount, $billing_name, $billing_address, $billing_city, $billing_state, $billing_zip, $billing_country, $billing_tel, $billing_email, $delivery_name, $delivery_address, $delivery_city, $delivery_state, $delivery_zip, $delivery_country, $delivery_tel, $merchant_param1, $merchant_param2, $merchant_param3, $merchant_param4, $merchant_param5, $vault, $offer_type, $offer_code, $discount_value, $this->ci->session->userdata("ip_address"), $this->ci->session->userdata("user_agent"));
            $pgfile = PG_LOG . date("d-m-Y") . "_cca.csv";

            if (!file_exists($pgfile)) {
                $file = fopen($pgfile, "a");
                fputcsv($file, $title);
            }

            $file = fopen($pgfile, "a");
            fputcsv($file, $val);

            fclose($file);
            if ($order_id != '' && $failure_message == "") {

                $array_where = array('track_id' => $order_id);

                $fields_update = array('status' => strtolower($order_status),
                                        'pg_track_id' => $tracking_id,
                                        'bank_ref_no' => $bank_ref_no,
                                        'response' => json_encode($array_response)
                                        );
                
                $pay_trans_id = $this->ci->payment_transaction_fss_model->update_where($array_where, '', $fields_update);
            
                $trans_metadata = array(
                    "payment_transaction_id" => $pay_trans_id,
                    "order_id" => $order_id,
                    "tracking_id" => $tracking_id,
                    "bank_ref_no" => $bank_ref_no,
                    "order_status" => $order_status,
                    "failure_message" => $failure_message,
                    "payment_mode" => $payment_mode,
                    "card_name" => $card_name,
                    "status_code" => $status_code,
                    "status_message" => $status_message,
                    "currency" => $currency,
                    "amount" => $amount,
                    "billing_name" => $billing_name,
                    "billing_address" => $billing_address,
                    "billing_city" => $billing_city,
                    "billing_state" => $billing_state,
                    "billing_zip" => $billing_zip,
                    "billing_country" => $billing_country,
                    "billing_tel" => $billing_tel,
                    "billing_email" => $billing_email,
                    "delivery_name" => $delivery_name,
                    "delivery_address" => $delivery_address,
                    "delivery_city" => $delivery_city,
                    "delivery_state" => $delivery_state,
                    "delivery_zip" => $delivery_zip,
                    "delivery_country" => $delivery_country,
                    "delivery_tel" => $delivery_tel,
                    "merchant_param1" => $merchant_param1,
                    "merchant_param2" => $merchant_param2,
                    "merchant_param3" => $merchant_param3,
                    "merchant_param4" => $merchant_param4,
                    "merchant_param5" => $merchant_param5,
                    "vault" => $vault,
                    "offer_type" => $offer_type,
                    "offer_code" => $offer_code,
                    "discount_value" => $discount_value
                );
                $ptm_id = $this->ci->db->insert('payment_transaction_metadata', $trans_metadata);
                $data["is_updated"] = true;
                   
                if(strtolower($order_status) == 'success') {
                    $check_wallet_topup = $this->ci->op_trans_details_model->where(["op_trans_id" => $data["wallet_topup_id"]])->find_all();
                    if($check_wallet_topup) {
                        if(strtolower($check_wallet_topup[0]->transaction_status) == "pfailed") {
                            $pg_array_where = array('op_trans_id' => $data["wallet_topup_id"]);
                            $fields_update = array(
                                                    'transaction_status' => "success",
                                                    'pg_tracking_id' => $tracking_id,
                                                    'is_status' => '1'
                                                    
                                                    );
                            $is_wallet_topup_updated = $this->ci->op_trans_details_model->update_where($pg_array_where, '', $fields_update);

                            if($is_wallet_topup_updated) {
                                $wallet_data = $this->ci->op_wallet_model->where('user_id',$this->ci->session->userdata('user_id'))->find_all();
                                $new_wallet_id = "";
                                if(!$wallet_data) {
                                    $new_wallet = [
                                                    'user_id' => $this->ci->session->userdata('user_id'),
                                                    'amount' => $array_response['mer_amount'],
                                                    'opening_bal' => '0.00',
                                                    'comment' => "added_by_self",
                                                    'status'=> 'credit',
                                                    'created_by' => $this->session->userdata('user_id')
                                                 ];

                                    $new_wallet_id =    $this->ci->op_wallet_model->insert($new_wallet);

                                    $current_wallet_amount = 0;
                                    $current_wallet_id = $new_wallet_id;
                                    $to_update_wallet = false;
                                    
                                }
                                else {
                                    $current_wallet_amount = $wallet_data[0]->amt;
                                    $current_wallet_id = $wallet_data[0]->id;
                                    $to_update_wallet = true;
                                }

                                $TopUp_data['op_id'] = '0';
                                $TopUp_data['user_id'] = $this->session->userdata('user_id');
                                $TopUp_data['available_balance'] = $array_response['mer_amount'];
                                $TopUp_data['usable_balance'] = $array_response['mer_amount'];
                                $TopUp_data['provider_flag'] = 'o';
                                $TopUp_data['is_status'] = '0';
                                $TopUp_data['user_id'] = $this->ci->session->userdata('user_id');
                                $TopUp_data['created_by'] = $this->ci->session->userdata('user_id');
                                $TopUp_data['created_date'] = date("Y-m-d H:i:s", time());
                                                             
                                $last_topup_transction_id = $this->ci->common_model->insert('op_topup',$TopUp_data);

                                if($last_topup_transction_id)
                                {
                                   $op_trans_to_update = array(
                                                          'op_topup_id' => $last_topup_transction_id,
                                                          );
                                  $this->ci->db->where('op_trans_id',$data["wallet_topup_id"]);
                                  $this->ci->db->update('op_trans_details', $op_trans_to_update);
                                }

                                $trns_data['op_wallet_id'] = $current_wallet_id;
                                $trns_data['opening_bal'] = $array_response['mer_amount'];
                                $trns_data['closing_bal'] = $current_wallet_amount + $array_response['mer_amount'];
                                $trns_data['amount'] = $array_response['mer_amount'];
                                $trns_data['comment'] = 'Amount Credited';
                                $trns_data['user_id'] = $this->ci->session->userdata('user_id');
                                $trns_data['status'] = 'credit';
                                $trns_data['total_bal_amt_before_trans'] = $current_wallet_amount;
                                $trns_data['total_bal_amt_after_trans'] = $current_wallet_amount + $array_response['mer_amount'];
                                $trns_data['topup_id'] = $last_topup_transction_id;
                                $trns_data['wallet_type'] = 'actual_wallet';
                                                         
                                $wallet_trans_id = $this->ci->common_model->insert('op_wallet_trans',$trns_data);
                                if($to_update_wallet) {
                                    
                                    $wallet_to_update = array(
                                                              'amount' => $array_response['mer_amount']+$current_wallet_amount,
                                                              'opening_bal'=> $current_wallet_amount,
                                                              'trans_amount'=> $array_response['mer_amount'],
                                                              'updated_date'=> date('Y-m-d h:m:s')
                                                              );
                                $this->ci->db->where('user_id',$this->ci->session->userdata('user_id'));
                                $this->ci->db->update('op_wallet', $wallet_to_update);
                                }
                                
                                $data["page_msg_type"]   = "success";
                                $data["page_msg"]        = "Amount added to wallet successfully.";
                            }
                        }
                        else
                        {
                            
                            $custom_error_mail = array(
                                                        "custom_error_subject" => "Important : Wallet Double Hit (Page Refresh)",
                                                        "custom_error_message" => "Page refreshed or Made double hit in the process after payment done in agent wallet. Raw data to track. ".json_encode($array_response)
                                                        );

                            $this->ci->common_model->developer_custom_error_mail($custom_error_mail);

                            
                            $data["page_msg_type"]   = "error";
                            $data["page_msg"]        = "Please contact customer support.";
                        }
                    }
                    else
                    {
                        //no data found mail
                        
                        $custom_error_mail = array(
                                                    "custom_error_subject" => "Important : Wallet Topup table data not found.",
                                                    "custom_error_message" => "Agent Wallet topup data not found. Please Track. Raw data to track. ".json_encode($array_response)
                                                    );

                        $this->ci->common_model->developer_custom_error_mail($custom_error_mail);

                        $data["page_msg_type"]   = "error";
                        $data["page_msg"]        = "Please contact customer support.";
                    }
                }
                else
                {
                    
                    //pfailed reason
                    $pg_array_where = array('id' => $data["wallet_topup_id"]);
                    $fields_update = array('transaction_status' => strtolower($order_status),'is_approvel' => strtolower($order_status));
                    $is_wallet_topup_updated = $this->ci->wallet_topup_model->update_where($pg_array_where, '', $fields_update);
                    
                    $custom_error_mail = array(
                                                "custom_error_subject" => "Wallet Topup ".strtolower($order_status),
                                                "custom_error_message" => "Reason : ".$failure_message." <br/> <br/>Agent Wallet topup failed. ".json_encode($array_response)
                                                );

                    $this->ci->common_model->developer_custom_error_mail($custom_error_mail);

                    $data["page_msg_type"]   = "error";
                    $data["page_msg"]        = "Payment not done. Please try again";
                }
            } 
            else
            {
                
                $pg_array_where = array('id' => $data["wallet_topup_id"]);
                $fields_update = array('transaction_status' => strtolower($order_status),'is_approvel' => strtolower($order_status));
                $is_wallet_topup_updated = $this->ci->wallet_topup_model->update_where($pg_array_where, '', $fields_update);

                $custom_error_mail = array(
                                            "custom_error_subject" => "Important : Order ID is missing.",
                                            "custom_error_message" => "Agent Wallet topup failed. Order ID is missing. ".json_encode($array_response)
                                            );

                $this->ci->common_model->developer_custom_error_mail($custom_error_mail);

                $data["page_msg_type"]   = "error";
                $data["page_msg"]        = "Payment not done. Please try again";
            }
        } 
        catch (Exception $e)
        {
            
            var_dump($e);
        }
        $data["page_redirect"]   = base_url()."admin/op_topup";
    
        return $data;
    }


}//End of class 