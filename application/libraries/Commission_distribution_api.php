<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Commission_distribution_api extends CI_Model {

    var $provider_id;
    var $provider_type;

    public function __construct() {
        parent::__construct();

        $this->load->model(array("Utilities_model",'wallet_model', 'wallet_trans_model', 'wallet_topup_model', 'users_model', 'wallet_trans_log_model', 'common_model','service_master_model'));

        ini_set('max_execution_time', 100000);
        ini_set("memory_limit", "256M");
    }

    public function index() {
        
    }

   public function credit_vas_amount($serviceID = '', $params = "",$params2 = "", $trans_id = '') {
        //show('cron script',1);
        $this->load->model(array('wallet_trans_model', 'wallet_model', 'Utilities_model'));
        if (empty($serviceID)) {
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'credit_vas_comm.log', 'Provide service id to credit amount', 'credit vas');
            //die('Provide service id to credit amount');
        }
        $date1 = $date2 ='';
        if ($params == 'weekly') {
            $monday = strtotime("last monday");
            $monday = date('w', $monday) == date('w') ? $monday + 7 * 86400 : $monday;
            $sunday = strtotime(date("Y-m-d", $monday) . " +6 days");

            $date1 = date("Y-m-d", $monday);
            $date2 = date("Y-m-d", $sunday);
        } else if ($params == 'monthly') {
            $current_date = date('Y-m-d H:i:s');
            $date1 = date('Y-m-01', strtotime($current_date));
            $date2 = date('Y-m-t', strtotime($current_date));
        } else if ($params == 'daily') {
            $date1 = $date2 = date('Y-m-d');
        } else {
            $from_date = '';
            $to_date = '';
            if(!empty($params)){
                $date1 = !empty($params) ? date('Y-m-d', strtotime($params)).' 00:00:00': '';
                $date2 = !empty($params2) ?  date('Y-m-d', strtotime($params2)).' 23:59:59': date('Y-m-d', strtotime($params)).' 23:59:59';
            }
           
            //$date2 = date('Y-m-d H:i:s');
        }
           
        $this->db->select("*");
        $this->db->from("vas_commission_detail v");
        $this->db->where('v.comm_dist_status', 'N');
        $this->db->where('v.status', 'Y');
        $this->db->where('v.service_id', $serviceID);
        if (!empty($trans_id)) {
            $this->db->where('v.transaction_no', $trans_id);    
        }
        //$this->db->where_not_in('v.service_id', DMT_SERVICE_ID);
       
        if (!empty($date1)) {
           // $this->db->like('v.created_date', date('Y-m-d', strtotime($date1)));
            $this->db->where('v.created_date >=', $date1);
            $this->db->where('v.created_date <=', $date2);  
        }
        $result = $this->db->get();
//        show($this->db->last_query(),1);
        $vas_details = $result->result_array();
        $gst_amt = '1.18';
        if(!empty($vas_details)){
            
            foreach ($vas_details as $key => $vas_detail) {
                $trimax_amt = $rokad_trimax_amt = $company_amt = $rd_amt = $dd_amt = $ex_amt = $sa_amt = '';
                
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'credit_vas_comm.log', $vas_details, 'credit vas');
                $vas_id = $vas_detail['id'];
                $user_id = $vas_detail['user_id'];
                $service_id = $vas_detail['service_id'];
                $agent_code = $vas_detail['agent_code'];
                $service_name = $this->common_model->get_value('service_master', 'service_name', 'id="' . $service_id . '"');
                $serv_name = $service_name->service_name;
                /* Fetch commission amount */
                $trimax_amt = $vas_detail['trimax_final_amt'];
                $rokad_trimax_amt = round($vas_detail['rokad_trimax_amt']/$gst_amt,2);
                $company_amt = round($vas_detail['company_amt']/$gst_amt,2);
                $rd_amt = round($vas_detail['rd_amt']/$gst_amt,2);
                $dd_amt = round($vas_detail['dd_amt']/$gst_amt,2);
                $ex_amt = round($vas_detail['ex_amt']/$gst_amt,2);
                $sa_amt = round($vas_detail['sa_amt']/$gst_amt,2);
                /* Fetch commission amount */

                $getCommissionLevels = $this->getRetailerLevels($user_id);
                $trimax_id_level1 = $getCommissionLevels[0]['level_1'];
                $company_id_level2 = $getCommissionLevels[0]['level_2'];
                $rd_id_level3 = $getCommissionLevels[0]['level_3'];
                $dd_id_level4 = $getCommissionLevels[0]['level_4'];
                $ex_id_level5 = $getCommissionLevels[0]['level_5'];
                $sa_id_level6 = $user_id;
    //            echo '<pre>'; print_r($getCommissionLevels);  die;

                /* Fetch wallet amount of each level */
                $trimax_wallet_amt = !empty($trimax_id_level1) ? $this->Utilities_model->getUserWalletDetails($trimax_id_level1) : "";
                $company_wallet_amt = !empty($company_id_level2) ? $this->Utilities_model->getUserWalletDetails($company_id_level2) : "";
                $rd_wallet_amt = !empty($rd_id_level3) ? $this->Utilities_model->getUserWalletDetails($rd_id_level3) : "";
                $dd_wallet_amt = !empty($dd_id_level4) ? $this->Utilities_model->getUserWalletDetails($dd_id_level4) : "";
                $ex_wallet_amt = !empty($ex_id_level5) ? $this->Utilities_model->getUserWalletDetails($ex_id_level5) : "";
                $sa_wallet_amt = !empty($sa_id_level6) ? $this->Utilities_model->getUserWalletDetails($sa_id_level6) : "";
                /* Fetch wallet amount of each level */

                $this->db->trans_begin();

                if (!empty($ex_id_level5)) {
                    $agentwalletdata = $this->wallet_model->where('user_id', $sa_id_level6)->find_all();

                    if ($agentwalletdata) {
                        $agentWalletId = $agentwalletdata[0]->id;
                        $agentCurrentAmt = $agentwalletdata[0]->amt;
                    }

                    $agentAmtafterAdd = $agentCurrentAmt + $sa_amt;
                    $agTranNo = substr(hexdec(uniqid()), 4, 12);
                    $agent_wallet_trans_detail['transaction_no'] = $agTranNo;
                    $agent_wallet_trans_detail['w_id'] = $agentWalletId;
                    $agent_wallet_trans_detail['amt'] = $sa_amt;
                    $agent_wallet_trans_detail['wallet_type'] = 'actual_wallet';
                    $agent_wallet_trans_detail['comment'] = $serv_name . ' Vas commission amount added for agent ' . $sa_id_level6;
                    $agent_wallet_trans_detail['status'] = 'credited';
                    $agent_wallet_trans_detail['user_id'] = $sa_id_level6;
                    $agent_wallet_trans_detail['added_by'] = 'cron';
                    $agent_wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                    $agent_wallet_trans_detail['amt_before_trans'] = $agentCurrentAmt;
                    $agent_wallet_trans_detail['amt_after_trans'] = $agentAmtafterAdd;
                    $agent_wallet_trans_detail['transaction_type_id'] = '';
                    $agent_wallet_trans_detail['transaction_type'] = 'credited';
                    $agent_wallet_trans_detail['is_status'] = 'Y';
                    $agent_wallet_trans_detail['remark'] = 'Credit for vas_id ' . $vas_id;

                    log_data('vas/commission_cron/' . date('Y') . '/' . date('M') . '/' . date('d-m-Y') . '/commission_record' . '.log', $agent_wallet_trans_detail, 'distributor vas transaction commission data');
                    if ($sa_amt > 0) {
                        $agent_wallet_trans_id = $this->wallet_trans_model->insert($agent_wallet_trans_detail);
                    } else {
                        $agent_wallet_trans_id = true;
                    }

                    if ($agent_wallet_trans_id) {
                        $sa_data = array(
                            'amt' => $agentAmtafterAdd,
                            'updated_by' => 'cron',
                            'comment' => $serv_name . ' Vas commission amount added for agent ' . $sa_id_level6,
                            'updated_date' => date('Y-m-d H:i:s'),
                            'last_transction_id' => $agent_wallet_trans_id
                        );
                        $saUpdateWallet = $this->wallet_model->updateWalletData($sa_data, $sa_id_level6);

                        if ($saUpdateWallet) {
                            $Diswalletdata = $this->wallet_model->where('user_id', $ex_id_level5)->find_all();
                            // print_r($Diswalletdata); die;
                            if ($Diswalletdata) {
                                $disWalletId = $Diswalletdata[0]->id;
                                $disCurrentAmt = $Diswalletdata[0]->amt;
                            }

                            $disAmtafterAdd = $disCurrentAmt + $ex_amt;
                            $disTranNo = substr(hexdec(uniqid()), 4, 12);
                            $dis_wallet_trans_detail['transaction_no'] = $disTranNo;
                            $dis_wallet_trans_detail['w_id'] = $disWalletId;
                            $dis_wallet_trans_detail['amt'] = $ex_amt;
                            $dis_wallet_trans_detail['wallet_type'] = 'actual_wallet';
                            $dis_wallet_trans_detail['comment'] = $serv_name . ' Vas commission amount added for agent ' . $ex_id_level5;
                            $dis_wallet_trans_detail['status'] = 'credited';
                            $dis_wallet_trans_detail['user_id'] = $ex_id_level5;
                            $dis_wallet_trans_detail['added_by'] = 'cron';
                            $dis_wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                            $dis_wallet_trans_detail['amt_before_trans'] = $disCurrentAmt;
                            $dis_wallet_trans_detail['amt_after_trans'] = $disAmtafterAdd;
                            $dis_wallet_trans_detail['transaction_type_id'] = '';
                            $dis_wallet_trans_detail['transaction_type'] = 'credited';
                            $dis_wallet_trans_detail['is_status'] = 'Y';
                            $dis_wallet_trans_detail['remark'] = 'Credit for vas_id ' . $vas_id;

                            log_data('vas/commission_cron/' . date('Y') . '/' . date('M') . '/' . date('d-m-Y') . '/commission_record' . '.log', $dis_wallet_trans_detail, 'distributor vas transaction commission data');
                            if ($ex_amt > 0) {
                                $dis_wallet_trans_id = $this->wallet_trans_model->insert($dis_wallet_trans_detail);
                            } else {
                                $dis_wallet_trans_id = true;
                            }

                            if ($dis_wallet_trans_id) {
                                $d_data = array(
                                    'amt' => $disAmtafterAdd,
                                    'updated_by' => 'cron',
                                    'comment' => $serv_name . ' Vas commission amount added for agent ' . $ex_id_level5,
                                    'updated_date' => date('Y-m-d H:i:s'),
                                    'last_transction_id' => $dis_wallet_trans_id
                                );
                                $disUpdateWallet = $this->wallet_model->updateWalletData($d_data, $ex_id_level5);

                                if ($disUpdateWallet) {
                                    $adwalletdata = $this->wallet_model->where('user_id', $dd_id_level4)->find_all();
                                    if ($adwalletdata) {
                                        $adWalletId = $adwalletdata[0]->id;
                                        $adCurrentAmt = $adwalletdata[0]->amt;
                                    }

                                    $adAmtafterAdd = $adCurrentAmt + $dd_amt;
                                    $adTranNo = substr(hexdec(uniqid()), 4, 12);

                                    $ad_wallet_trans_detail['transaction_no'] = $adTranNo;
                                    $ad_wallet_trans_detail['w_id'] = $adWalletId;
                                    $ad_wallet_trans_detail['amt'] = $dd_amt;
                                    $ad_wallet_trans_detail['wallet_type'] = 'actual_wallet';
                                    $ad_wallet_trans_detail['comment'] = $serv_name . ' Vas commission amount added for agent ' . $dd_id_level4;
                                    $ad_wallet_trans_detail['status'] = 'credited';
                                    $ad_wallet_trans_detail['user_id'] = $dd_id_level4;
                                    $ad_wallet_trans_detail['added_by'] = 'cron';
                                    $ad_wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                                    $ad_wallet_trans_detail['amt_before_trans'] = $adCurrentAmt;
                                    $ad_wallet_trans_detail['amt_after_trans'] = $adAmtafterAdd;
                                    $ad_wallet_trans_detail['transaction_type_id'] = '';
                                    $ad_wallet_trans_detail['transaction_type'] = 'credited';
                                    $ad_wallet_trans_detail['is_status'] = 'Y';
                                    $ad_wallet_trans_detail['remark'] = 'Credit for vas_id ' . $vas_id;

                                    log_data('vas/commission_cron/' . date('Y') . '/' . date('M') . '/' . date('d-m-Y') . '/commission_record' . '.log', $ad_wallet_trans_detail, 'Area distributor wallet transaction commission data');

                                    if ($dd_amt > 0) {
                                        $ad_wallet_trans_id = $this->wallet_trans_model->insert($ad_wallet_trans_detail);
                                    } else {
                                        $ad_wallet_trans_id = true;
                                    }

                                    if ($ad_wallet_trans_id) {
                                        $ad_data = array(
                                            'amt' => $adAmtafterAdd,
                                            'updated_by' => 'cron',
                                            'comment' => $serv_name . ' Vas commission amount added for agent ' . $dd_id_level4,
                                            'updated_date' => date('Y-m-d H:i:s'),
                                            'last_transction_id' => $ad_wallet_trans_id
                                        );
                                        $adUpdateWallet = $this->wallet_model->updateWalletData($ad_data, $dd_id_level4);

                                        if ($adUpdateWallet) {
                                            $mdwalletdata = $this->wallet_model->where('user_id', $rd_id_level3)->find_all();
                                            if ($mdwalletdata) {
                                                $mdWalletId = $mdwalletdata[0]->id;
                                                $mdCurrentAmt = $mdwalletdata[0]->amt;
                                            }
                                            $mdAmtafterAdd = $mdCurrentAmt + $rd_amt;
                                            $mdTranNo = substr(hexdec(uniqid()), 4, 12);

                                            $md_wallet_trans_detail['transaction_no'] = $mdTranNo;
                                            $md_wallet_trans_detail['w_id'] = $mdWalletId;
                                            $md_wallet_trans_detail['amt'] = $rd_amt;
                                            $md_wallet_trans_detail['wallet_type'] = 'actual_wallet';
                                            $md_wallet_trans_detail['comment'] = $serv_name . ' Vas commission amount added for agent ' . $rd_id_level3;
                                            $md_wallet_trans_detail['status'] = 'credited';
                                            $md_wallet_trans_detail['user_id'] = $rd_id_level3;
                                            $md_wallet_trans_detail['added_by'] = 'cron';
                                            $md_wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                                            $md_wallet_trans_detail['amt_before_trans'] = $mdCurrentAmt;
                                            $md_wallet_trans_detail['amt_after_trans'] = $mdAmtafterAdd;
                                            $md_wallet_trans_detail['transaction_type_id'] = '';
                                            $md_wallet_trans_detail['transaction_type'] = 'credited';
                                            $md_wallet_trans_detail['is_status'] = 'Y';
                                            $md_wallet_trans_detail['remark'] = 'Credit for vas_id ' . $vas_id;

                                            log_data('vas/commission_cron/' . date('Y') . '/' . date('M') . '/' . date('d-m-Y') . '/commission_record' . '.log', $md_wallet_trans_detail, 'Master distributor wallet transaction commission data');

                                            if ($rd_amt > 0) {
                                                $md_wallet_trans_id = $this->wallet_trans_model->insert($md_wallet_trans_detail);
                                            } else {
                                                $md_wallet_trans_id = true;
                                            }

                                            if ($md_wallet_trans_id) {
                                                $md_data = array(
                                                    'amt' => $mdAmtafterAdd,
                                                    'updated_by' => 'cron',
                                                    'comment' => $serv_name . ' Vas commission amount added for agent ' . $rd_id_level3,
                                                    'updated_date' => date('Y-m-d H:i:s'),
                                                    'last_transction_id' => $md_wallet_trans_id
                                                );

                                                $mdUpdateWallet = $this->wallet_model->updateWalletData($md_data, $rd_id_level3);

                                                if ($mdUpdateWallet) {
                                                    $cmpwalletdata = $this->wallet_model->where('user_id', $company_id_level2)->find_all();
                                                    if ($cmpwalletdata) {
                                                        $cmpWalletId = $cmpwalletdata[0]->id;
                                                        $cmpCurrentAmt = $cmpwalletdata[0]->amt;
                                                    }
                                                    $cmpAmtafterAdd = $cmpCurrentAmt + $company_amt;
                                                    $cmTranNo = substr(hexdec(uniqid()), 4, 12);

                                                    $cmp_wallet_trans_detail['transaction_no'] = $cmTranNo;
                                                    $cmp_wallet_trans_detail['w_id'] = $cmpWalletId;
                                                    $cmp_wallet_trans_detail['amt'] = $company_amt;
                                                    $cmp_wallet_trans_detail['wallet_type'] = 'actual_wallet';
                                                    $cmp_wallet_trans_detail['comment'] = $serv_name . ' Vas commission amount added for agent ' . $company_id_level2;
                                                    $cmp_wallet_trans_detail['status'] = 'credited';
                                                    $cmp_wallet_trans_detail['user_id'] = $company_id_level2;
                                                    $cmp_wallet_trans_detail['added_by'] = 'cron';
                                                    $cmp_wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                                                    $cmp_wallet_trans_detail['amt_before_trans'] = $cmpCurrentAmt;
                                                    $cmp_wallet_trans_detail['amt_after_trans'] = $cmpAmtafterAdd;
                                                    $cmp_wallet_trans_detail['transaction_type_id'] = '';
                                                    $cmp_wallet_trans_detail['transaction_type'] = 'credited';
                                                    $cmp_wallet_trans_detail['is_status'] = 'Y';
                                                    $cmp_wallet_trans_detail['remark'] = 'Credit for vas_id ' . $vas_id;

                                                    log_data('vas/commission_cron/' . date('Y') . '/' . date('M') . '/' . date('d-m-Y') . '/commission_record' . '.log', $cmp_wallet_trans_detail, 'company wallet transaction commission data');

                                                    if ($company_amt > 0) {
                                                        $cmp_wallet_trans_id = $this->wallet_trans_model->insert($cmp_wallet_trans_detail);
                                                    } else {
                                                        $cmp_wallet_trans_id = true;
                                                    }

                                                    if ($cmp_wallet_trans_id) {
                                                        $cmp_data = array(
                                                            'amt' => $cmpAmtafterAdd,
                                                            'updated_by' => 'cron',
                                                            'comment' => $serv_name . ' Vas commission amount added for agent ' . $company_id_level2,
                                                            'updated_date' => date('Y-m-d H:i:s'),
                                                            'last_transction_id' => $cmp_wallet_trans_id
                                                        );
                                                        $cmpUpdateWallet = $this->wallet_model->updateWalletData($cmp_data, $company_id_level2);

                                                        if ($cmpWalletId) {
                                                            $trimaxwalletdata = $this->wallet_model->where('user_id', $trimax_id_level1)->find_all();
                                                            if ($trimaxwalletdata) {
                                                                $triWalletId = $trimaxwalletdata[0]->id;
                                                                $triCurrentAmt = $trimaxwalletdata[0]->amt;
                                                            }
                                                            $triAmtafterAdd = $triCurrentAmt + $rokad_trimax_amt;
                                                            $triTranNo = substr(hexdec(uniqid()), 4, 12);

                                                            $tri_wallet_trans_detail['transaction_no'] = $triTranNo;
                                                            $tri_wallet_trans_detail['w_id'] = $triWalletId;
                                                            $tri_wallet_trans_detail['amt'] = $rokad_trimax_amt;
                                                            $tri_wallet_trans_detail['wallet_type'] = 'actual_wallet';
                                                            $tri_wallet_trans_detail['comment'] = $serv_name . ' Vas commission amount added for agent ' . $trimax_id_level1;
                                                            $tri_wallet_trans_detail['status'] = 'credited';
                                                            $tri_wallet_trans_detail['user_id'] = $trimax_id_level1;
                                                            $tri_wallet_trans_detail['added_by'] = 'cron';
                                                            $tri_wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                                                            $tri_wallet_trans_detail['amt_before_trans'] = $triCurrentAmt;
                                                            $tri_wallet_trans_detail['amt_after_trans'] = $triAmtafterAdd;
                                                            $tri_wallet_trans_detail['transaction_type_id'] = '';
                                                            $tri_wallet_trans_detail['transaction_type'] = 'credited';
                                                            $tri_wallet_trans_detail['is_status'] = 'Y';
                                                            $tri_wallet_trans_detail['remark'] = 'Credit for vas_id ' . $vas_id;

                                                            log_data('vas/commission_cron/' . date('Y') . '/' . date('M') . '/' . date('d-m-Y') . '/commission_record' . '.log', $tri_wallet_trans_detail, 'trimax wallet transaction commission data');

                                                            if ($rokad_trimax_amt > 0) {
                                                                $trimax_wallet_trans_id = $this->wallet_trans_model->insert($tri_wallet_trans_detail);
                                                            } else {
                                                                $trimax_wallet_trans_id = true;
                                                            }
                                                            if ($trimax_wallet_trans_id) {
                                                                $tri_data = array(
                                                                    'amt' => $triAmtafterAdd,
                                                                    'updated_by' => 'cron',
                                                                    'comment' => $serv_name . ' Vas commission amount added for agent ' . $trimax_id_level1,
                                                                    'updated_date' => date('Y-m-d H:i:s'),
                                                                    'last_transction_id' => $cmp_wallet_trans_id
                                                                );

                                                                $triUpdateWallet = $this->wallet_model->updateWalletData($tri_data, $trimax_id_level1);
                                                                $status_updated = $this->Utilities_model->update_comm_status($vas_id);
                                                                if ($triUpdateWallet) {
                                                                    $this->db->trans_commit();
                                                                    log_data('vas/commission_cron/' . date('Y') . '/' . date('M') . '/' . date('d-m-Y') . '/commission_record' . '.log', 'Success commission updation against agent ' . $getCommissionLevels, 'success log');
                                                                } else {
                                                                    $this->db->trans_rollback();

                                                                    log_data('vas/commission_cron/' . date('Y') . '/' . date('M') . '/' . date('d-m-Y') . '/commission_record' . '.log', 'last update entry', 'success log');
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }else{
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'credit_vas_comm.log', 'Amount already credited', 'credit vas');
            //die('Amount already credited');
        }
        return 'success';
}

  function getRetailerLevels($user_id) {
        $this->db->select('u.id as user_id,u.agent_code,u.level_1,u.level_2,u.level_3,u.level_4,u.level_5,u.level_6');
        $this->db->from("users u");
        $this->db->where("u.id", $user_id);
        $user_result = $this->db->get();
        $user_result_array = $user_result->result_array();
        //show($this->db->last_query(),1);

        return $user_result_array;
    }
    
    //If BOS ticket is cancelled then debit the commission distributed
    //Get id from tickets table Search for id in remark column in wallet_trans
    //Get wallet id and user id and debit amount from resp user >> add entry in wallet_trans and update wallet
    //Change status to N in wallet_trans
     public function debit_vas_commission($id = '') {
        $this->load->model(array('wallet_trans_model', 'wallet_model', 'Utilities_model'));
        if (empty($id)) {
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'debit_vas_comm.log', 'Provide id to debit amount', 'credit vas');
            //die('Provide service id to credit amount');
        }

        $this->db->select("*");
        $this->db->from("wallet_trans w");
        $this->db->like('w.remark', 'Credit for vas_id ' . $id);
        $this->db->where('w.is_status', 'Y');
        $result = $this->db->get();
        //show($this->db->last_query(),1);

        $wallet_trans_details = $result->result_array();

        show($wallet_trans_details, 1);
        if (!empty($wallet_trans_details)) {
            foreach ($wallet_trans_details as $key => $wallet_trans_detail) {
                $trimax_amt = $rokad_trimax_amt = $company_amt = $rd_amt = $dd_amt = $ex_amt = $sa_amt = '';
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'debit_vas_comm.log', $wallet_trans_details, 'debit vas');

                $wallettrans_id = $wallet_trans_detail['id'];
                $transaction_id = $wallet_trans_detail['transaction_no'];
                $w_id = $wallet_trans_detail['w_id'];
                $amt = $wallet_trans_detail['amt'];
                $user_id = $wallet_trans_detail['user_id'];
               
                
                /* Fetch wallet amount of user */
                $wallet_amt = !empty($wallettrans_id) ? $this->Utilities_model->getUserWalletDetails($user_id) : "";
                /* Fetch wallet amount of user */

                if ($wallet_amt > 0) {
                    $walletdata = $this->wallet_model->where('user_id', $user_id)->find_all();

                    if ($walletdata) {
                        $WalletId = $walletdata[0]->id;
                        $walletCurrentAmt = $walletdata[0]->amt;
                    }
                    if ($WalletId == $w_id) {
                        $this->db->trans_begin();
                        $AmtafterAdd = $walletCurrentAmt - $amt;
                        $agTranNo = substr(hexdec(uniqid()), 4, 12);
                        $agent_wallet_trans_detail['transaction_no'] = $agTranNo;
                        $agent_wallet_trans_detail['w_id'] = $WalletId;
                        $agent_wallet_trans_detail['amt'] = $amt;
                        $agent_wallet_trans_detail['wallet_type'] = 'actual_wallet';
                        $agent_wallet_trans_detail['comment'] = ' Vas commission amount debited for  ' . $user_id;
                        $agent_wallet_trans_detail['status'] = 'debited';
                        $agent_wallet_trans_detail['user_id'] = $user_id;
                        $agent_wallet_trans_detail['added_by'] = 'cron';
                        $agent_wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                        $agent_wallet_trans_detail['amt_before_trans'] = $walletCurrentAmt;
                        $agent_wallet_trans_detail['amt_after_trans'] = $AmtafterAdd;
                        $agent_wallet_trans_detail['transaction_type_id'] = '';
                        $agent_wallet_trans_detail['transaction_type'] = 'debited';
                        $agent_wallet_trans_detail['is_status'] = 'Y';
                        $agent_wallet_trans_detail['remark'] = 'Debit for vas_id ' . $id;
                        //show($agent_wallet_trans_detail,1);
                        log_data('vas/commission_cron/' . date('Y') . '/' . date('M') . '/' . date('d-m-Y') . '/debit_vas_comm' . '.log', $agent_wallet_trans_detail, 'commission debited for ' . $id);
                        if ($amt > 0) {
                            $agent_wallet_trans_id = $this->wallet_trans_model->insert($agent_wallet_trans_detail);
                        } else {
                            $agent_wallet_trans_id = true;
                        }

                        if ($agent_wallet_trans_id) {
                            $sa_data = array(
                                'amt' => $AmtafterAdd,
                                'updated_by' => 'cron',
                                'comment' => ' Vas commission amount debited for ' . $user_id,
                                'updated_date' => date('Y-m-d H:i:s'),
                                'last_transction_id' => $agent_wallet_trans_id
                            );
                            $saUpdateWallet = $this->wallet_model->updateWalletData($sa_data, $user_id);
                            //=====update vas_commission comm_dist_status======
                            $update_data = array(
                                    'comm_dist_status' => 'Reverted'
                                ); 
                            $this->db->where('id', $id);
                            $this->db->update('vas_commission_detail', $update_data);
                           //=====update vas_commission comm_dist_status======
                            
                            //=====update old wallet_trans is_status======
                            $update_trans_data = array(
                                    'is_status' => 'N'
                                ); 
                            $this->db->where('id', $wallettrans_id);
                            $this->db->update('wallet_trans', $update_trans_data);
                           //=====update old wallet_trans is_status======
                            if ($saUpdateWallet) {
                                $this->db->trans_commit();
                                log_data('vas/commission_cron/' . date('Y') . '/' . date('M') . '/' . date('d-m-Y') . '/debit_vas_comm' . '.log', 'Success commission debit for vas ' . $id, 'success log');
                            } else {
                                $this->db->trans_rollback();

                                log_data('vas/commission_cron/' . date('Y') . '/' . date('M') . '/' . date('d-m-Y') . '/debit_vas_comm' . '.log','Failed commission debit for vas ' . $id, 'trans rollback log');
                            }
                        }
                    } else {
                        log_data('vas/commission_cron/' . date('Y') . '/' . date('M') . '/' . date('d-m-Y') . '/debit_vas_comm' . '.log', 'wallet id mismatch', 'trans rollback log');
                    }
                } else {
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'debit_vas_comm.log', 'Wallet amt less than 0 for ' . $wallettrans_id, 'debit vas');
                }
            }
            return 'success';
        }else{
          log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'debit_vas_comm.log', 'No data found for ' . $id, 'No data found debit vas');
        }
    }
}
