<?php //

defined('BASEPATH') OR exit('No direct script access allowed');

class Common_library {

     protected $CI;

    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->CI =& get_instance();
        //$this->load->helper('smartcard_helper');
        $this->apiCred = json_decode(SMART_CARD_CREDS, true);
        $header_value = $this->apiCred['X_API_KEY'];
        $this->header = array(
            'X-API-KEY:' . $header_value
        );

        $this->basicAuth = array(
            'username' => $this->apiCred['username'],
            'password' => $this->apiCred['password']
        );
    }

    public function total_fare_calc($id,$base_fare,$base_cc_arr = array(),$total_changes='',$pass_cat='', $cc_otc_data=array(),$param=array()) 
    {

		$amt = 0.00;
        $i = 0;
		$fare_data = array();

		if(!empty($base_cc_arr)) {
			$base_fare = $base_fare - $total_changes;
		}

		$base_fare = number_format($base_fare, 2, '.', '');

		$base_arr1[$i] = array('fee_type_value' => $base_fare,'fee_type_nm' => 'Base Fare', 'fee_type_cd' => 'BF', 'operand' => '+');
        $i++;

        //Transaction api for OTC card registration
        if($pass_cat == OTC_CARD || $pass_cat == REG_OTC_TRANS_TYPE) {
            $cc_trans_data_arr = $this->otc_reg_charges($param);
            //print_r($cc_trans_data_arr);

            $fare_data['data']['responseCode'] = $cc_trans_data_arr->data->responseCode;
            $fare_data['data']['responseMessage'] = $cc_trans_data_arr->data->responseMessage;
            $fare_data['data']['requestId'] = $cc_trans_data_arr->data->requestId;

            $cc_trans_data = $cc_trans_data_arr->data->txnRefNo;
			
            $amt = $amt + $cc_trans_data->TotalCharge;
            unset($cc_trans_data->TotalCharge);
            foreach ($cc_trans_data as $fee_name => $fee_value) {
                $base_arr1[$i] = array(
                    'fee_type_value' => number_format(round($fee_value,2), 2, '.', ''),
                    'fee_type_cd' => $fee_name,
                    'fee_type_nm' => $fee_name,
                    'operand' => "+", 
                );
                $i++;
            }
        }

        //Transaction api for reissue card
        if($pass_cat == REISSUE_OTC_TRANS_TYPE || $pass_cat == REISSUE_PPC_TRANS_TYPE) {
            $cc_trans_data_arr = $this->reissue_trans_charges($cc_otc_data);

            $fare_data['data']['responseCode'] = $cc_trans_data_arr['responseCode'];
            $fare_data['data']['responseMessage'] = $cc_trans_data_arr['responseMessage'];
            $fare_data['data']['requestId'] = $cc_trans_data_arr['requestId'];

            $cc_trans_data = $cc_trans_data_arr['ChargeList'];
            $fare_data['data']['cc_card_fee_details'] = $cc_trans_data;

            $amt = $amt + $cc_trans_data['TotalCharge'];
            unset($cc_trans_data['TotalCharge']);
            foreach ($cc_trans_data as $fee_name => $fee_value) {
                $base_arr1[$i] = array(
                    'fee_type_value' => number_format(round($fee_value,2), 2, '.', ''),
                    'fee_type_cd' => $fee_name,
                    'fee_type_nm' => $fee_name,
                    'operand' => "+",
                );
                $i++;
            }
        }
        $base_arr = array_merge($base_arr1,$base_cc_arr);

        //If card type fee is not set for card master then it will return the base fare only
		$card_fee_details = $this->CI->Customer_registration_model->get_card_fee_details($id);
		if(empty($card_fee_details)) {
			$fare_data['data']['card_fee_details'] = $base_arr;
			$fare_data['data']['total_fare_amt'] = $base_fare + $amt;
			$fare_data['msg'] = 'No extra amount charged';
			return $fare_data;
		}

        //It is reset to zero if card fee details is found then the further logic will calculate the total amount from the start
        $amt = 0.00;
		$result = array_merge($base_arr,$card_fee_details);
        
        $fee_type_cd_arr = array_column($result,'fee_type_cd');
        $scf_card_fee = 0.00;
        if(in_array(SMART_CARD_FEE_CD,$fee_type_cd_arr)) {
            $key = array_search (SMART_CARD_FEE_CD, $fee_type_cd_arr);
            $scf_card_fee = $result[$key]['fee_type_value'];
        }

		foreach ($result as $key => &$value) {
			switch ($value['operand']) {
				case '+':
					$amt = $amt + $value['fee_type_value'];
				break;

				case '-':
					$amt = $amt - $value['fee_type_value'];
				break;

				case '%+':
					$perc_amt1 = $scf_card_fee * $value['fee_type_value'] * 0.01;
					$value['fee_type_nm'] = $value['fee_type_nm'] . '(' .$value['fee_type_value'] . ')';
					$value['fee_type_value'] = number_format(round($perc_amt1,2), 2, '.', '');
					$amt = $amt + $perc_amt1;
				break;

				case '%-':
					$perc_amt2 = $scf_card_fee * $value['fee_type_value'] * 0.01;
					$value['fee_type_nm'] = $value['fee_type_nm'] . '(' .$value['fee_type_value'] . ')';
					$value['fee_type_value'] = number_format(round($perc_amt2,2), 2, '.', '');
					$amt = $amt - $perc_amt2;
				break;
			}
		}

		$fare_data['data']['card_fee_details'] = $result;
		$rounding_amt = m_round_calc($amt,ONE_ROUND_FACTOR);
		$fare_data['data']['total_fare_amt'] = number_format($rounding_amt, 2, '.', '');
		$fare_data['msg'] = 'Data with extra charges';
		return $fare_data;
	}

	public function otc_reg_charges($cc_otc_data) {
		$URL = $this->apiCred['url'] . '/CCAS_api/otc_reg_trans_charges';
		$request =$cc_otc_data ;//$this->post(); 
		$data = curlForPost($URL, $request, $this->basicAuth, $this->header);
		return $json = json_decode($data);
		//json_encode($this->response($json));
        }
    
    public function reissue_trans_charges($cc_otc_data) 
     {

        $vendor_id = $this->CI->Customer_registration_model->get_vendor_id($cc_otc_data['user_name'])['vendor_id'];
        switch ($vendor_id) {
            case '4':
                $initiatorType = ROKAD_INITIATOR_TYPE."";
                break;
            
            default:
                $initiatorType = INITIATOR_TYPE."";
                break;
        }

    	$url = CCAS_URL . "GetRISSUETransactionCharges/";
        $data = array(
            "requestId" => $cc_otc_data['requestId']."",
            "txnType" => "get_reissue_charges",
            "initiatorId" => $cc_otc_data['terminal_code']."",
            "initiatorType" => $initiatorType,
            "TxnAmount" => 0,
            "loginId" => $cc_otc_data['user_name'],
            "sessionId" => $cc_otc_data['session_id']
        );

        $header = getCcasApiHeader();
        $json_data = json_encode($data);
        $returnStr = sendDataOverPost($url, $header, $json_data, '','', $ccasFlag=1);

        $returnArr = json_decode($returnStr, 1);
        if (!empty($returnArr)) {
            if (isset($returnArr['responseCode'])) {
                if ($returnArr['responseCode'] == RESPONSE_CODE_SUCCESS_CC) {
                    $result['responseCode'] = $returnArr['responseCode'];
                    $result['responseMessage'] = $returnArr['responseMessage'];
                    $result['requestId'] = $returnArr['requestId'];
                    $result['ChargeList'] = $returnArr['ChargeList'];
                    return $result;
                } else  {
                    $result['responseCode'] = $returnArr['responseCode'];
                    $result['responseMessage'] = $returnArr['responseMessage'];
                    $result['requestId'] = $returnArr['requestId'];
                    $message = array('status' => 'failure', 'msg' => 'failure', 'data' => $result);
                    $this->CI->set_response($message, 200, FALSE);
                }
            } else {
                $message = array('status' => 'failure', 'msg' => 'Error in response code');
                $this->CI->set_response($message, 200, FALSE);
            }
        } else {
            $message = array('status' => 'failure', 'msg' => 'Error in response');
            $this->CI->set_response($message, 200, FALSE);
        }
    }
}
