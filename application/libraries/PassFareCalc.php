<?php //

defined('BASEPATH') OR exit('No direct script access allowed');

class PassFareCalc {

     protected $CI;

        // We'll use a constructor, as you can't directly call a function
        // from a property definition.
        public function __construct()
        {
                // Assign the CodeIgniter super-object
                $this->CI =& get_instance();
                $this->CI->load->model(array('Passfare_model'));
                
        }
        
       
    public function getBaseFare($input) {

        $url = MSRTC_FARE_URL;

        $returnStr = sendDataOverPost($url, '', $input);
        if (!empty($returnStr)) {
            $returnArr = explode('[|$$$|]', $returnStr);
            $data['countResponseArr'] = $returnArr[1];

            $statusStr = $returnArr[2];
            $data['statusArr'] = json_decode($statusStr, 1);

            if (isset($data['statusArr']['STATUS'])) {
                if ($data['statusArr']['STATUS'] == 1) {

                    $responseStr = $returnArr[4];
                    $responseArr = explode('[|##|]', $responseStr);
                    $fareChangeDateStr = $responseArr[0];
                    $dateWiseFareStr = $responseArr[1];

                    $data['fareChangeDateArr'] = json_decode($fareChangeDateStr, 1);
                    $data['dateWiseFareArr'] = json_decode($dateWiseFareStr, 1);
// show($data,1);
                    $routeArr = explode(',', $input['ROUTE_NO']);

                    $fareArr = array();
                    $k = 0;

                    foreach ($routeArr as $key => $val) {

                        if (isset($data['dateWiseFareArr'][$val])) {

                            foreach ($data['dateWiseFareArr'][$val] as $key2 => $val2) {
                                $busTypeKey = $key2;

                                foreach ($data['dateWiseFareArr'][$val][$busTypeKey] as $key3 => $val3) {
                                    $fareTypeKey = $key3;

                                    foreach ($data['dateWiseFareArr'][$val][$busTypeKey][$fareTypeKey] as $key6 => $val6) {

                                        foreach ($data['dateWiseFareArr'][$val][$busTypeKey][$fareTypeKey][$key6][$input['FROM_STOP_CD']][$input['TILL_STOP_CD']] as $key4 => $val4) {
                                            $fromStopKey = $key4;
                                        }

                                        foreach ($data['dateWiseFareArr'][$val][$busTypeKey][$fareTypeKey][$key6][$input['FROM_STOP_CD']][$input['TILL_STOP_CD']][$fromStopKey] as $key5 => $val5) {
                                            $tillStopKey = $key5;
                                        }
        
                                        if($input['age'] > AGE_LIMIT) {
                                            // child base fare
                                            $baseFareAmt = $data['dateWiseFareArr'][$val][$busTypeKey][$fareTypeKey][$key6][$input['FROM_STOP_CD']][$input['TILL_STOP_CD']][$fromStopKey][$tillStopKey]['NC']['TOTAL_ADULT_AMT'];

                                            $baseAsnAmount = $data['dateWiseFareArr'][$val][$busTypeKey][$fareTypeKey][$key6][$input['FROM_STOP_CD']][$input['TILL_STOP_CD']][$fromStopKey][$tillStopKey]['NC']['ADULT_ASN_AMT'];

                                        } else {
                                            // adult base fare
                                            $baseFareAmt = $data['dateWiseFareArr'][$val][$busTypeKey][$fareTypeKey][$key6][$input['FROM_STOP_CD']][$input['TILL_STOP_CD']][$fromStopKey][$tillStopKey]['NC']['TOTAL_CHILD_AMT'];

                                            $baseAsnAmount = $data['dateWiseFareArr'][$val][$busTypeKey][$fareTypeKey][$key6][$input['FROM_STOP_CD']][$input['TILL_STOP_CD']][$fromStopKey][$tillStopKey]['NC']['CHILD_ASN_AMT'];
                                        }

                                        //As per request from wai on 2 Nov 2018 by amrut sir
                                        $baseFareAmt = m_round_calc($baseFareAmt,FIVE_ROUND_FACTOR);

                                        $fare_data_arr = $this->calculateFinalFare($baseFareAmt, $input['span_id'], $input['concession_id'], $input['TICKET_BOOKING_DATE']);

                                        $fareArr[$k] = $this->calculateConcession($fare_data_arr,$baseAsnAmount,$input['concession_id'],$input['bus_type_id'],$input['TICKET_BOOKING_DATE'],$input['pass_category_id']);
                                        $fareArr[$k]['per_day_base_fare'] = $baseFareAmt;

                                        $fareArr[$k]['route_no'] = $val;
                                        $fareArr[$k]['route_name'] = '';
                                        $fareArr[$k]['bus_type_cd'] = $busTypeKey;
                                        $fareArr[$k]['fare_type_cd'] = $fareTypeKey;
                                        break;
                                    }
                                }
                                $k++;
                            }
                        }
                    }

                    $outputArr['status'] = 'success';
                    $outputArr['msg'] = 'Success';
                    $outputArr['data'] = $fareArr;
                } else {
                    $outputArr['status'] = 'failure';
                    $outputArr['msg'] = $data['statusArr']['STATUS_MESSAGE'];
                    $outputArr['data'] = '';
                }
            } else {
                $outputArr['status'] = 'failure';
                $outputArr['msg'] = 'Error';
                $outputArr['data'] = '';
            }
        } else {
            $outputArr['status'] = 'failure';
            $outputArr['msg'] = 'Error';
            $outputArr['data'] = '';
        }
        return $outputArr;
    }

    public function calculateConcession($basefare_arr,$asn_amt,$concession_id,$bus_type_id,$booking_date,$pass_category_id, $asn_status='') {

        $fare_data = array();

        $total_base_fare = $basefare_arr['fare_amt'];
        $total_asn_amt = $asn_amt * NUMBER_OF_TRIPS * $basefare_arr['chargeableDays'];
        if($asn_status == 'Y') {
            unset($total_asn_amt);
            $total_asn_amt = $asn_amt;
        }

        $concession_data = $this->CI->Passfare_model->getConcessionRate($concession_id,$bus_type_id, $booking_date);
        if(empty($concession_data)) {
            $message = array('status' => 'failure', 'msg' => 'Concession rate master not created for this concession and bus type');
            $this->CI->set_response($message, 200, FALSE);
        }

        $concession_rate_f = (100 - $concession_data[0]['concession_rate']);

        if($pass_category_id == STUDENT_CARD) {
            $net_fare_amount = $total_base_fare - $total_asn_amt;
        } else {
            $net_fare_amount = $total_base_fare;
        }

        $before_rounding_fare = (($concession_rate_f * $net_fare_amount) / 100);
        $pass_amt = m_round_calc($before_rounding_fare,FIVE_ROUND_FACTOR);

        if($concession_data[0]['is_reimbursable'] == 'Y') {
            $reimbursement_amt = $net_fare_amount - $pass_amt;
        } else {
            $reimbursement_amt = number_format(0, 2, '.', '');
        }
        
        $fare_data = array(
                            'total_base_fare' => $total_base_fare,
                            'net_fare_amount' => $net_fare_amount,
                            'total_asn_amount' => $total_asn_amt,
                            'fare_before_rounding' => $before_rounding_fare,
                            'total_amt' => number_format($pass_amt, 2, '.', ''),
                            'reimbursement_amount' => $reimbursement_amt
                        );
        return $fare_data;
    }

    public function calculateFinalFare($basefare, $span_id, $concession_id, $journey_from_date) {
        $chargeableDays = $this->CI->Passfare_model->getChargeableDays($span_id, $concession_id, $journey_from_date);

        $fare = $basefare * NUMBER_OF_TRIPS * $chargeableDays;
        $fare_amt_data = array('fare_amt' => $fare, 'chargeableDays' => $chargeableDays);
        return $fare_amt_data;
    }
   
}
