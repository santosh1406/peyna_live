<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . 'libraries/PHPExcel.php';

class Excel extends PHPExcel {
    public function __construct() {
        parent::__construct();
    }
    
    public function data_2_excel($filename, $data = null, $headers = true)
    {
     
        $this->setActiveSheetIndex(0);
        
        $col = 'A';
        
        $rowNumber = 1;
        if ($headers){
            foreach ($data[0] as $key => $val) {
                $this->getActiveSheet()->getCell($col . $rowNumber)->setValue(str_replace("_", " ", $key));
                $col++;
            }
            $rowNumber++;
        }
            
        foreach ($data as $row) {
            $col = 'A';
            if(isset($row) && count($row)>0){
                foreach ($row as $cell) {
                    $this->getActiveSheet()->setCellValueExplicit($col . $rowNumber, $cell,PHPExcel_Cell_DataType::TYPE_STRING);
                    $col++;
                 }
            }
            $rowNumber++;
        }

        $file = "export/$filename";
        
        if(!file_exists(dirname($file)))
        {
            mkdir(dirname($file), 0770, true);
        }
        
        header('Content-type: application/ms-excel');
        header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
        header("Cache-control: private");
        $objWriter = PHPExcel_IOFactory::createWriter($this, 'Excel2007');
        $objWriter->save("export/$filename");
        header("location: " . base_url() . $file);
        //unlink(FCPATH . "export/$filename");
                
    }

    public function data_save_in_excel($filename, $data = null)
    {
     
        $this->setActiveSheetIndex(0);
        
        $col = 'A';
        
        foreach ($data[0] as $key => $val) {
            $this->getActiveSheet()->getCell($col . '1')->setValue(str_replace("_", " ", $key));
            $col++;
        }
            
        $rowNumber = 2;
        foreach ($data as $row) {
            $col = 'A';
            foreach ($row as $cell) {
                $this->getActiveSheet()->setCellValueExplicit($col . $rowNumber, $cell);
                $col++;
            }
            $rowNumber++;
        }

        $file = "cron_ors_report/$filename";
        
        if(!file_exists(dirname($file)))
        {
            mkdir(dirname($file), 0770, true);
        }
        
      //  header('Content-type: application/ms-excel');
     //   header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
    //    header("Cache-control: private");
        $objWriter = PHPExcel_IOFactory::createWriter($this, 'Excel5');
        $objWriter->save("cron_ors_report/$filename");
      //  header("location: " . base_url() . $file);
      //  unlink(base_url() . "export/$filename");
                
    }

        

}


