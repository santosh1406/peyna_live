<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reconcilation_api extends CI_Model {

    var $provider_id;
    var $provider_type;

    public function __construct() {
        parent::__construct();

        $this->load->model(array("tso_model","Utilities_model",'wallet_model', 'wallet_trans_model', 'wallet_topup_model', 'users_model', 'wallet_trans_log_model', 'common_model','service_master_model'));
        $this->load->helper('tso_helper');
        
        ini_set('max_execution_time', 100000);
        ini_set("memory_limit", "256M");
    }

    public function index() {
        
    }
 
   public function vendorPassbook($userdata) {
        /*
          $userdata = array(
          'type' => 1->credit;2->debit,
          'amount' => 1,
          'vendor_id' => 1,
          'trans_ref_no' => 1,
          'vendor_service_id' => 1
          );
         */
        if (isset($userdata) && !empty($userdata)) {
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'vendor_passbook.log', $userdata, 'TSO Vendor passbook');
            $amount = $userdata['amount'];
            $type = $userdata['type']; // 1->add(credit);2->debit
            $vendor_id = $userdata['vendor_id'];
            $trans_ref_no = $userdata['trans_ref_no'];
            $vendor_service_id = $userdata['vendor_service_id'];
            $this->load->model(array('vendor_passbook_model'));
            // step 1 : check/find open balance
            $result = $this->vendor_passbook_model->getData('vendor_wallet', 'amount', array('vendor_id' => $vendor_id));
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'vendor_passbook.log', $result, 'TSO Vendor passbook');
            if (isset($result) && !empty($result)) {
                $opening_balance = $result[0]['amount'];
                // step 2 : find transaction_type is credit or debit
                $transaction_types_result = $this->vendor_passbook_model->getData('vendor_trans_types', 'type', array('id' => $type));

                $transaction_type = $transaction_types_result[0]['type'];

                if ($transaction_type == 'credit') {
                    $closing_amount = $opening_balance + $amount;
                } else {
                    $closing_amount = $opening_balance - $amount;
                }
                // step 3 : insert data into Vendor_trans
                $insertData = array(
                    'vendor_id' => $vendor_id,
                    'vendor_service_id' => $vendor_service_id,
                    'trans_type' => $type,
                    'opening_balance' => $opening_balance,
                    'trans_amt' => $amount,
                    'closing_balance' => $closing_amount,
                    'trans_ref_no' => $trans_ref_no,
                    'created_date' => date('Y-m-d H:i:s')
                );
                $insert_id = $this->insertVendorAmount($insertData);
                // step 4 : update wallet amount in Vendor_wallet
                if (isset($insert_id) && $insert_id > 0) {
                    $update_data = $this->updateClosingAmount('', $vendor_id);
                    $insertData = array(
                       'vendor_id' => $vendor_id,
                       'amount' => $closing_amount,
                       'status' => 'Y'
                    );
                    $insert_data = $this->insertVendorWalletAmount($insertData);
                    if ($insert_data) {
                        $data['status'] = 1;
                        $data['msg'] = "Successfully Insert Data";
                    } else {
                        $data['status'] = 0;
                        $data['msg'] = "Somethink is wrong please try after some time 2 ";
                    }
                } else {
                    $data['status'] = 0;
                    $data['msg'] = "Somethink is wrong please try after some time";
                }
            } else {
                $data['status'] = 0;
                $data['msg'] = "Open balance is not found";
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'vendor_passbook.log', $data, 'TSO Vendor passbook else');
            }
        } else {
            $data['status'] = 0;
            $data['msg'] = "Somethink is wrong please try after some time 1";
        }
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'vendor_passbook.log', $data, 'TSO Vendor passbook data end');
        return json_encode($data);
    }

//end vendorPassbook

   public  function updateClosingAmount($amount ='', $vendor_id) {
        /**
         * update wallet amount in vendor_wallet
         */
        $this->load->model(array('vendor_passbook_model'));
        if ($vendor_id > 0) {
            $update = array(
                'status' => 'N'
            );
            $where = array(
                'vendor_id' => $vendor_id
            );
            $update_data = $this->vendor_passbook_model->updateData('vendor_wallet', $update, $where);
            if ($update_data) {
                $status = true;
            } else {
                $status = false;
            }
        } else {
            $status = false;
        }
        return $status;
    }


  public function insertVendorAmount($insertData) {
        if (isset($insertData) && !empty($insertData)) {
            $insert_id = $this->vendor_passbook_model->insertData('vendor_trans', $insertData);
        } else {
            $insert_id = false;
        }
        return $insert_id;
    }

//end insertVendorWalletAmount
    
     public function insertVendorWalletAmount($insertData) {
        if (isset($insertData) && !empty($insertData)) {
            $insert_id = $this->vendor_passbook_model->insertData('vendor_wallet', $insertData);
        } else {
            $insert_id = false;
        }
        return $insert_id;
    }

//end insertVendorWalletAmount

   public function updateTopUp($userdata) {
        /*
          $userdata = array(
          'vendor_id' => 1,
          'amount' => 1000
          );
         */
        $vendor_id = $userdata['vendor_id'];
        $amount = $userdata['amount'];
        $this->load->model(array('vendor_passbook_model'));
        if (isset($amount) && $amount > 0) {
            //step 1: first check wallet amount
            $result = $this->vendor_passbook_model->getVendorData('vendor_wallet', 'amount', array('vendor_id' => $vendor_id));

            if (isset($result) && !empty($result)) {
                $opening_balance = $amount;//$result[0]['amount'];
                //$closing_amount = $opening_balance + $amount;
                $closing_amount = $amount;
                // update wallet amount and trasaction amount
                //$update_data = $this->updateClosingAmount($closing_amount, $vendor_id);
                 $insertData = array(
                   'vendor_id' => $vendor_id,
                   'amount' => $amount,
                   'status' => 'Y'
                );
                $insert_data = $this->insertVendorWalletAmount($insertData);
                
                if ($insert_data) {
                    $data['status'] = 1;
                    $data['msg'] = "Successfully Update Data";
                } else {
                    $data['status'] = 0;
                    $data['msg'] = "Somethink is wrong please try after some time 2 ";
                }
            } else {
                $opening_balance = 0;
                $closing_amount = $opening_balance + $amount;

                $insertData = array(
                    'vendor_id' => $vendor_id,
                    'amount' => $amount,
                    'status' => 'Y'
                );
                // insert wallet amount
                $insert_id = $this->vendor_passbook_model->insertData('vendor_wallet', $insertData);
                if ($insert_id) {
                    $data['status'] = 1;
                    $data['msg'] = "Successfully Insert Data";
                } else {
                    $data['status'] = 0;
                    $data['msg'] = "Somethink is wrong please try after some time";
                }
            }
            // vendor_service_id(21 is service_master id for TOP UP) & trans_type(5 is vendor_trans_types id for TOP UP)
           /* $insertData2 = array(
                'vendor_id' => $vendor_id,
                'vendor_service_id' => 21,
                'trans_type' => 5,
                'opening_balance' => $opening_balance,
                'trans_amt' => $amount,
                'closing_balance' => $closing_amount
            );
            $insert_id2 = $this->insertVendorAmount($insertData2);
            if ($insert_id2) {
                $data['status'] = 1;
                $data['msg'] = "Successfully Insert Data";
            } else {
                $data['status'] = 0;
                $data['msg'] = "Somethink is wrong please try after some time";
            }*/
        } else {
            $data['status'] = 0;
            $data['msg'] = "Somethink is wrong please try after some time";
        }
        return json_encode($data);
    }

//end updateTopUp
}
