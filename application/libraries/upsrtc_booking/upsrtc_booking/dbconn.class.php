<?php

include_once("config.php");

class dbconn {

    var $conn;

    /**
     * 	set mysql database server connection
     */
    function __construct() {
        $this->getConnect();
        //return $this->conn;
    }

    /**
     * 	set mysql database server connection
     * 	first define constant host, username, password
     *  db name in config.php file
     */
    function getConnect() {
       // echo DB_HOST, DB_USER, DB_PASSWORD;
        $this->conn = mysql_connect(UP_DB_HOST, UP_DB_USER, UP_DB_PASSWORD) ;
                //or die($this->error("Problem In Database Connection!", "There is some problem in database connection.\nPlease check your databse host name, username and password!"));
        if (is_resource($this->conn)) {
            mysql_select_db(UP_DB_NAME) ;
            //or die($this->error("Problem In Selecting Database!", "There is some problem in database selection.\nplease check database exists or database name is correct."));
        } else {
            echo "Sorry";
        }
        return $this->conn;
    }

    function __distruct() {
        mysql_close($this->conn);
    }

}

function error($message, $details="") {
    $no = mysql_errno();
    $msg = mysql_error();
    $err_msg = "<pre>";
    $err_msg .="<h1>$message</h1>";
    $err_msg .="$details<br><br>";
    $err_msg .="<b>Error Number \t:</b> $no<br>";
    $err_msg .="<b>Error Message     :</b> $msg<br>";
    $err_msg .="<b>File Name :</b> " . __FILE__ . "<br><br>";
    $err_msg .="<hr></pre>";
    return $err_msg;
}

function timer($finish = true) {
    static $start_frac_sec, $start_sec, $end_frac_sec, $end_sec;
    if ($finish) {
        list($end_frac_sec, $end_sec) = explode(" ", microtime());
        //echo '<p style="font-size: smaller">'.$_SERVER['SCRIPT_FILENAME'].' took about ' . round((($end_sec - $start_sec)+($end_frac_sec - $start_frac_sec)),4) . ' seconds to generate.</p>';
        mysql_query("INSERT INTO response_time(PAGE_NM,RESPONSE_TM,EXECUTION_DATE,depocode) VALUES('" . $_SERVER['SCRIPT_FILENAME'] . "','" . round((($end_sec - $start_sec) + ($end_frac_sec - $start_frac_sec)), 4) . "',CURRENT_TIMESTAMP,'" . $_SESSION['depotcd'] . "')");
    } else {
        list($start_frac_sec, $start_sec) = explode(" ", microtime());
    }
}

timer(false);
?>
