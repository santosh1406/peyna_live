<?php

include_once("dbop.class.php");

class Calfunction extends dbop {

    //echo "test here";
    /*     * DATE_TYPE : INT
     * Bus Service Number
     */

    private $busServiceNo;

    /* DATE_TYPE : DATETIME
     * Departure Time Of bus
     */
    public $depttm;
    public $CAL_METHOD;
    /*     * DATA_TYPE : STRING
     * Adult Or Child
     * $AdultChild == 'A' OR 'a' Adult Passenger
     * $AdultChild == 'C' OR 'c' Child Passenger
     */
    private $AdultChild;
    /*     * DATE_TYPE : STRING
     * Concession Code
     * If Normal Passenger : $conc = 'NC' or 'nc'
     * For Concessional Passenger other values Which is one of from Table : concessions
     */
    private $conc = "";
    /*     * DATA_TYPE : INTEGER
     * Fare Per KM According To Bus Service Type AND ROUTE TYPE
     */
    public $km;
    public $TTL_KM;
    public $KM_TOTAL;

    /** Minimum Fare For Adult.
     * Child Minimum Fare is to be calculated according to CONCESSION_RATE for Child with respect to Bus Service Type
     */
    public $route_type;
    /*     * DATA_TYPE : ENUM 0: FOR INTRA STATE ROUTES THAT IS ROUTES IN RAJASTHAN
     * 0: FOR INTRA STATE ROUTES THAT IS ROUTES IN RAJASTHAN
     * 1: FOR INTER STATE ROUTES
     */
    private $cc_rate = "0.00";

    /**
     * Parameters to be assigned for calculation
     */
    private $bus_type_cd;
    public $route_type_c;
    /* /
     * ROUTE NO
     * DATA TYPE: INT
     */
    private $routeNo;
    /*
     * BOARDING STOP
     * DATA TYPE: STRING
     */
    private $brdStop;
    /*
     * ALIGHTING STOP
     * DATA TYPE: STRING
     */
    private $altStop;
    /*
     * Fare Type:
     * N: Regular Calculation
     * F: Direct Fare
     */
    private $flatFlag;
    private $from_stop_seq;
    private $till_stop_seq;

    function __construct() {
        
        
    }

    function __destruct() {
        
    }

    /*     * Basic information for fare calculation ie From Stage No and till stage number along with any interstate parameter
     * if exists are passed for further calculation
     */

    function getJourneyDetails() {
        
    }

    /*     * Returns Chargable KM according to journey Parameters
     */

    function getKm() {
        // echo "abcd". $this->km."kkk";
        $chargable_km = ceil(($this->km) / 1) * 1;
        return $this->km = $chargable_km;
    }

    function getTotalKm() {
        $chargable_km = ceil(($this->KM_TOTAL) / 1) * 1;
        return $this->KM_TOTAL = $chargable_km;
    }

    /*
      Return Route Parameters along with charagable kilometers
     */

    function getRoutePara($paraArray) {
        if(isset($paraArray['BUS_SERVICE_NO']) && $paraArray['BUS_SERVICE_NO']!='0'){
            $this->busServiceNo = $paraArray['BUS_SERVICE_NO'];
            $sql = "SELECT ROUTE_NO,BUS_TYPE_CD,FLAG_FLAT_RT "
                    . " FROM bus_services "
                    . " WHERE BUS_SERVICE_NO = '" . $paraArray['BUS_SERVICE_NO'] . "'";
            $res = $this->select($sql);
            if (isset($res) && isset($res[0])) {
                $paraArray['ROUTE_NO'] = $res[0]['ROUTE_NO'];
                $paraArray['BUS_TYPE_CD'] = $res[0]["BUS_TYPE_CD"];
                $this->routeNo = $res[0]['ROUTE_NO'];
                $this->flatFlag = $res[0]['FLAG_FLAT_RT'];
            }
        }else{
            $this->flatFlag = 'N';
        }
        $sql = "SELECT ROUTE_TYPE from routes where route_no = '" . $paraArray['ROUTE_NO'] . "'";
        $rtDetails = $this->select($sql);
        $this->routeNo = $paraArray['ROUTE_NO'];
        $this->route_type = $rtDetails[0]['ROUTE_TYPE'];
        $this->bus_type_cd = $paraArray['BUS_TYPE_CD'];
        $sql = "select INTRA_STATE_DISTANCE,KM "
                . " from route_stops where route_no = '" . $paraArray['ROUTE_NO'] . "'
            AND BUS_STOP_CD IN('" . $paraArray['BRD_STOP'] . "','" . $paraArray['ALT_STOP'] . "') "
                . " and `STATUS`='Y'";
        $rtStopDetails = $this->select($sql);
        if (isset($rtStopDetails[0]) && count($rtStopDetails) > 0) {
            $this->km = abs($rtStopDetails[0]['INTRA_STATE_DISTANCE'] - $rtStopDetails[1]['INTRA_STATE_DISTANCE']);
            $this->TTL_KM = abs($rtStopDetails[0]['KM'] - $rtStopDetails[1]['KM']);
        }
    }

    /*     * Returns Total Fare
     */

    function getFare($paraArray) {
        error_reporting(1);
        $this->noofpax = $paraArray['NO_PAX']; //no of psgr
        $this->brdStop = $paraArray['BRD_STOP'];
        $this->altStop = $paraArray['ALT_STOP'];
        $this->conc = $paraArray['PAX_CONC']; //psgr concession not used
        $this->paxType($paraArray['PAX_AGE']); //not used
        $this->getRoutePara($paraArray);
        $this->depttm = $paraArray["DEPT_DT"]; //In dd/mm/yy  

        $this->CAL_METHOD = $paraArray['CAL_METHOD']; //to be ask to yo
//exit;  


        $tollDetails = $this->getTollAmt();
        $octroiDetails = $this->getOctroiAmt();
        $itCharge = $this->getITCharges();
        $fareArray = $this->getFarePerKm();
        //PRINT_R($fareArray);
        //EXIT;
        $discount_arr = $this->getDiscountRate();
        $discount_rate = $discount_arr[0]['SCHEME_DISCOUNT'];

        $disc_rate = 0;
        $disc_arr = $this->getDiscRate();
        if (isset($disc_arr) && $disc_arr > 0) {
            $disc_rate = $disc_arr[0]['DISC_RATE'];
        } else {
            $disc_rate = 0;
        }


        /*
         * Other Amount Include
         * Sleeper Charge Per Passenger
         */
        //print_r($fareArray);
        $otherAmt = $this->getOtherAmt();
        /*
         * Service Charge
         */
        $serCharge = $this->getServiceCharge($paraArray);
        $fareArray[0]['SERVICE_CHARGE'] = $serCharge;
        if (isset($tollDetails[0]) && isset($fareArray[0])) {
            $fareArray[0]['TOLL_TAX'] = ($tollDetails[0]["ADULT_AMT"] <> 'FNA' ? $tollDetails[0]["ADULT_AMT"] : $fareArray[0]['TOLL']);
        } else {
            $fareArray[0]['TOLL_TAX'] = 0;
        }
        if (isset($octroiDetails[0])) {
            $fareArray[0]['OCTROI_AMT'] = $octroiDetails[0]["ADULT_AMT"];
        } else {
            $fareArray[0]['OCTROI_AMT'] = 0;
        }if (isset($itCharge[0])) {
            $fareArray[0]['IT_CHARGES_TYPE'] = $itCharge[0]["IT_CHARGE_TYPE"];
        } else {
            $fareArray[0]['IT_CHARGES_TYPE'] = 0;
        }
        if (isset($itCharge[0])) {
            if (isset($itCharge[0]) && $itCharge[0]['IT_CHARGE_TYPE'] == 'S') {
                $fareArray[0]['IT_CHARGES'] = $itCharge[0]['IT_CHARGES'] * $paraArray['NO_PAX'];
            } else {
                $fareArray[0]['IT_CHARGES'] = $itCharge[0]['IT_CHARGES'] * $paraArray['NO_PAX'];
                ;
            }
        } else {
            $fareArray[0]['IT_CHARGES'] = 0;
        }
        $disc_adt_fare = 0;
        $disc_cld_fare = 0;
        $fareArray[0]['OTHER_CHARGES'] = '0';

       

        //Actual Fare in Paisa
        if ($this->flatFlag == 'N') {
            /*
             * Interstate Fare
             */
            $interstateFare = $this->getInterstateFare($paraArray);
            //echo "<pre>";
            // print_r($interstateFare);
            // exit;
//echo "----".$interstateFare['I_ADULT_AMT'];
            if ($interstateFare['I_ADULT_AMT'] == 'SNA') {
                $fareArray[0]['ADULT_FARE'] = 'SNA';
                $fareArray[0]['CHILD_FARE'] = 'SNA';
                $fareArray[0]['ERR_MSG'] = 'SNA';
                return $fareArray;
            }



            $total_basefare = 0;
            $total_acs = 0;
            $total_hr = 0;
            $total_it = 0;
            $total_otherCharges = 0;
            $total_adultminfare = 0;
//echo count($interstateFare);
            if (count($interstateFare) != count($interstateFare, COUNT_RECURSIVE)) {
                $index = 1;
                foreach ($interstateFare as $array) {
                    $total_basefare += $array['basefare'];
                    $total_basefare_child += ($array['basefare'] / 2);
                    $total_acs += $array['acs'];
                    $total_hr += $array['hr'];
                    $total_it += $array['it'];
                    $total_otherCharges += $array['otherCharge1'] + $array['otherCharge2'] + $array['otherCharge3'] + $array['otherCharge4'] + $array['otherCharge5'];
                    // $total_adultminfare += $array['adultminfare'];
                    // $total_childminfare += $array['childminfare'];
                    /*
                     * Interstate Amount
                     */
                    //echo "Index is ".$index;
                    //echo 'A_STATE_'.$index;
                    $fareArray[0]['A_STATE_' . $index] = $array['basefare'];
                    $fareArray[0]['C_STATE_' . $index] = $array['basefare'] / 2;
                    $index++;
                }
            } else {
                $total_basefare = $interstateFare['basefare'];
                $total_basefare_child = ($interstateFare['basefare'] / 2);
                $total_acs = $interstateFare['acs'];
                $total_hr = $interstateFare['hr'];
                $total_it = $interstateFare['it'];
                $total_otherCharges = $interstateFare['otherCharge1'] + $interstateFare['otherCharge2'] + $interstateFare['otherCharge3'] + $interstateFare['otherCharge4'] + $interstateFare['otherCharge5'];
                $fareArray[0]['A_STATE_1'] = $interstateFare['basefare'];
                $fareArray[0]['C_STATE_1'] = $interstateFare['basefare'] / 2;
            }
//        echo "sum_basefare-->". $total_basefare.'<br>';
//        echo "sum_acs-->". $total_acs.'<br>';
//        echo "sum_hr-->". $total_hr.'<br>';
//        echo "sum_it-->". $total_it.'<br>';
//        echo "total_otherCharges-->". $total_otherCharges.'<br>';
//        echo "total_adultminfare-->". $total_adultminfare.'<br>';
//        echo "total_childminfare-->". $total_childminfare.'<br>';
//        echo $total_int_fare = $total_basefare + $total_acs+ $total_hr+ $total_it;     
            //  exit;
//            if (empty($interstateFare) ) {
//                $fareArray[0]['ADULT_FARE'] = 'FNA';
//                $fareArray[0]['CHILD_FARE'] = 'FNA';
//                $fareArray[0]['ERR_MSG'] = 'FNA';
//                return $fareArray;
//            }
            /*
             * Check If Direct Fare Exists
             */
            $direct = $this->direct_fare($paraArray);
           if(isset($direct) && isset($direct[0])){
            if ($direct[0]['BASE_FARE'] == 0) {
                //echo "dsfdsf";
                // exit;
                $adultFare = $fareArray[0]['FARE_PER_KM'] * $fareArray[0]['CHARGABLE_KM'];
                if ($fareArray[0]['CHARGABLE_KM'] > 0) {
                    $adultFare = ($adultFare < $fareArray[0]['ADULT_MIN_FARE'] * 100) ? $fareArray[0]['ADULT_MIN_FARE'] * 100 : $adultFare;
                }
            } else {
                $adultFare = ($direct[0]['BASE_FARE'] - $fareArray[0]['ACC_COMP_SURCHARGE'] - $fareArray[0]['HR_SURCHARGE'] - $fareArray[0]['IT_CHARGES'] - $fareArray[0]['OCTROI_AMT'] - $fareArray[0]['TOLL_TAX']) * 100;
            
           }
           } /*
             * Actual Adult Fare in rupee
             */
            $fareArray[0]['ADULT_FARE'] = round($adultFare / 100, 2);
            /*
             * Actual Child Fare in Rupee
             */
            $fareArray[0]['CHILD_FARE'] = round(($fareArray[0]['ADULT_FARE'] / 2), 2);
            /*
             * Interstate Amount
             */
//            $fareArray[0]['A_STATE_1'] = $interstateFare['I_ADULT_AMT'];
//            $fareArray[0]['C_STATE_1'] = $interstateFare['I_CHILD_AMT'];

            /*
             * Sleeper Charge
             */
            $fareArray[0]['ADULT_SLEEPER_FARE'] = $otherAmt['ADULT_SLEEPER_FARE'];
            $fareArray[0]['CHILD_SLEEPER_FARE'] = $otherAmt['CHILD_SLEEPER_FARE'];
            /*
             * Total Base Fare
             */
            $fareArray[0]['ADULT_FARE'] += $total_basefare;
            $fareArray[0]['CHILD_FARE'] += $total_basefare_child;
            $fareArray[0]['ACC_COMP_SURCHARGE'] +=$total_acs;
            $fareArray[0]['HR_SURCHARGE']+=$total_hr;
            $fareArray[0]['IT_SURCHARGE']+=$total_it;
            $fareArray[0]['OTHER_CHARGES']+=$total_otherCharges;


            $fareArray[0]['IS_HOME_ONLY'] = 'N';

            $fareArray[0]['TOTAL_ADULT_FARE'] = $fareArray[0]['ADULT_FARE'] + $fareArray[0]['ACC_COMP_SURCHARGE'] +
                    $fareArray[0]['HR_SURCHARGE'] + $fareArray[0]['IT_SURCHARGE'] +
                    $fareArray[0]['TOLL'] + $fareArray[0]['OTHER_CHARGES']; /* + $fareArray[0]['IT_CHARGES'] +
              $fareArray[0]['OCTROI_AMT'] + $fareArray[0]['TOLL_TAX']; */

            $fareArray[0]['TOTAL_CHILD_FARE'] = $fareArray[0]['CHILD_FARE'] + $fareArray[0]['ACC_COMP_SURCHARGE'] +
                    $fareArray[0]['HR_SURCHARGE'] + $fareArray[0]['IT_SURCHARGE'] +
                    $fareArray[0]['TOLL'] + $fareArray[0]['OTHER_CHARGES']; /* + $fareArray[0]['IT_CHARGES'] +
              $fareArray[0]['OCTROI_AMT'] + $fareArray[0]['TOLL_TAX']; */
            if ($fareArray[0]['FARE_FACTOR'] == 0)
                $fareArray[0]['FARE_FACTOR'] = 1;
            // echo 'FACTOR_TYPE'.$fareArray[0]['FACTOR_TYPE'];
            // exit;
            switch ($fareArray[0]['FACTOR_TYPE']) {
                case 'R':
                    $adultFare = round($fareArray[0]['TOTAL_ADULT_FARE'] / $fareArray[0]['FARE_FACTOR']);
                    $adultFare = $adultFare * $fareArray[0]['FARE_FACTOR'];
                    $childFare = round($fareArray[0]['TOTAL_CHILD_FARE'] / $fareArray[0]['FARE_FACTOR']);
                    $childFare = $childFare * $fareArray[0]['FARE_FACTOR'];
                    $fareArray[0]['CHARGABLE_ADULT_FARE'] = $adultFare;
                    $fareArray[0]['CHARGABLE_CHILD_FARE'] = $childFare;
                    $fareArray[0]['ADULT_FARE'] = $adultFare - ($fareArray[0]['ACC_COMP_SURCHARGE'] +
                            $fareArray[0]['HR_SURCHARGE'] + $fareArray[0]['IT_SURCHARGE'] +
                            $fareArray[0]['TOLL'] + $fareArray[0]['OTHER_CHARGES']); /* +$fareArray[0]['IT_CHARGES'] +
                      $fareArray[0]['OCTROI_AMT'] + $fareArray[0]['TOLL_TAX']); */
                    $fareArray[0]['CHILD_FARE'] = $childFare - ($fareArray[0]['ACC_COMP_SURCHARGE'] +
                            $fareArray[0]['HR_SURCHARGE'] + $fareArray[0]['IT_SURCHARGE'] +
                            $fareArray[0]['TOLL'] + $fareArray[0]['OTHER_CHARGES']); /* + $fareArray[0]['IT_CHARGES'] +
                      $fareArray[0]['OCTROI_AMT'] + $fareArray[0]['TOLL_TAX']); */
                    break;
                case 'C':
                    $adultFare = ceil($fareArray[0]['TOTAL_ADULT_FARE'] / $fareArray[0]['FARE_FACTOR']);
                    $adultFare = $adultFare * $fareArray[0]['FARE_FACTOR'];
                    $childFare = ceil($fareArray[0]['TOTAL_CHILD_FARE'] / $fareArray[0]['FARE_FACTOR']);
                    $childFare = $childFare * $fareArray[0]['FARE_FACTOR'];
                    $fareArray[0]['CHARGABLE_ADULT_FARE'] = $adultFare;
                    $fareArray[0]['CHARGABLE_CHILD_FARE'] = $childFare;
                    $fareArray[0]['ADULT_FARE'] = $adultFare - ($fareArray[0]['ACC_COMP_SURCHARGE'] +
                            $fareArray[0]['HR_SURCHARGE'] + $fareArray[0]['IT_SURCHARGE'] +
                            $fareArray[0]['TOLL'] + $fareArray[0]['OTHER_CHARGES']); /* +$fareArray[0]['IT_CHARGES'] +
                      $fareArray[0]['OCTROI_AMT'] + $fareArray[0]['TOLL_TAX']); */
                    $fareArray[0]['CHILD_FARE'] = $childFare - ($fareArray[0]['ACC_COMP_SURCHARGE'] +
                            $fareArray[0]['HR_SURCHARGE'] + $fareArray[0]['IT_SURCHARGE'] +
                            $fareArray[0]['TOLL'] + $fareArray[0]['OTHER_CHARGES']); /* + $fareArray[0]['IT_CHARGES'] +
                      $fareArray[0]['OCTROI_AMT'] + $fareArray[0]['TOLL_TAX']); */
                    break;
                case 'F':
                    $adultFare = floor($fareArray[0]['TOTAL_ADULT_FARE'] / $fareArray[0]['FARE_FACTOR']);
                    $adultFare = $adultFare * $fareArray[0]['FARE_FACTOR'];
                    $childFare = floor($fareArray[0]['TOTAL_CHILD_FARE'] / $fareArray[0]['FARE_FACTOR']);
                    $childFare = $childFare * $fareArray[0]['FARE_FACTOR'];
                    $fareArray[0]['CHARGABLE_ADULT_FARE'] = $adultFare;
                    $fareArray[0]['CHARGABLE_CHILD_FARE'] = $childFare;
                    $fareArray[0]['ADULT_FARE'] = $adultFare - ($fareArray[0]['ACC_COMP_SURCHARGE'] +
                            $fareArray[0]['HR_SURCHARGE'] + $fareArray[0]['IT_SURCHARGE'] +
                            $fareArray[0]['TOLL'] + $fareArray[0]['OTHER_CHARGES']); /* +$fareArray[0]['IT_CHARGES'] +
                      $fareArray[0]['OCTROI_AMT'] + $fareArray[0]['TOLL_TAX']); */
                    $fareArray[0]['CHILD_FARE'] = $childFare - ($fareArray[0]['ACC_COMP_SURCHARGE'] +
                            $fareArray[0]['HR_SURCHARGE'] + $fareArray[0]['IT_SURCHARGE'] +
                            $fareArray[0]['TOLL'] + $fareArray[0]['OTHER_CHARGES']); /* + $fareArray[0]['IT_CHARGES'] +
                      $fareArray[0]['OCTROI_AMT'] + $fareArray[0]['TOLL_TAX']); */
                    break;
            }
            //echo "conce". $this->conc;
            //  exit;
            //Concessional Amount
            if ($this->conc <> "") {
                //echo "1";
                //exit;
                $concFare = $this->getConcessionAmt($fareArray);
                if ($concFare['ERR_MSG'] == ""):
                    switch ($this->AdultChild) {
                        case 'A':
                            $fareArray[0]['ERR_MSG'] = $concFare['ERR_MSG'];
                            if ($concFare["CONC_FARE"] . "" == "FNA") {
                                $concFare['CONC_FARE'] = $fareArray[0]['ADULT_FARE'];
                            }
                            $fareArray[0]['ADULT_FARE'] = ($this->conc == "") ? $fareArray[0]['ADULT_FARE'] : $concFare['CONC_FARE'];

                            $fareArray[0]['A_STATE_1'] = ($this->conc == "") ? $fareArray[0]['A_STATE_1'] : $concFare['A_STATE_1'];

                            break;
                        case 'C':
                            $fareArray[0]['ERR_MSG'] = ($concFare['CONC_FARE'] . "" <> "FNA") ? "" : "0";
                            $concFare['CONC_FARE'] = ($concFare['CONC_FARE'] . "" <> "FNA") ? $concFare['CONC_FARE'] : $fareArray[0]['CHILD_FARE'];
                            $fareArray[0]['CHILD_FARE'] = ($this->conc == "") ? $fareArray[0]['CHILD_FARE'] : $concFare['CONC_FARE'];

                            $fareArray[0]['A_STATE_1'] = ($this->conc == "") ? $fareArray[0]['A_STATE_1'] : $concFare['A_STATE_1'];

                            break;
                    }
                    $fareArray[0]['CONC_RATE'] = $concFare['CONC_RATE'];
                    $fareArray[0]['IS_HOME_ONLY'] = $concFare['IS_HOME_ONLY'];
                else:
                    $fareArray[0]['ERR_MSG'] = $concFare['ERR_MSG'];
                endif;
            }
        }
        if ($this->flatFlag == 'Y') {
            //echo "2";
            //exit;
            $flatFare = $this->flatAmount();

            //Actual Adult Fare in rupee
            if (isset($flatFare[0]) && $flatFare[0]['ADULT_AMT'] == "FNA") {
                $fareArray[0]['ADULT_FARE'] = "FNA";
                $fareArray[0]['CHILD_FARE'] = "FNA";
                return $fareArray;
            } else {
//                $fareArray[0]['ADULT_FARE'] = (int) $flatFare[0]['ADULT_AMT'];
//                //Actual Child Fare in Rupee
//                $fareArray[0]['CHILD_FARE'] = (int) $flatFare[0]['CHILD_AMT'];                

                if (isset($flatFare[0]) && isset($flatFare[0]['ADULT_AMT'])) {
                    $fareArray[0]['ADULT_FARE'] = $flatFare[0]['ADULT_AMT'];
                } else {
                    $fareArray[0]['ADULT_FARE'] = '0';
                }
                //Actual Child Fare in Rupee
                //$fareArray[0]['CHILD_FARE'] =$flatFare[0]['CHILD_AMT'];
                if (isset($flatFare[0]) && isset($flatFare[0]['CHILD_AMT'])) {
                    $fareArray[0]['CHILD_FARE'] = $flatFare[0]['CHILD_AMT'];
                } else {
                    $fareArray[0]['CHILD_FARE'] = '0';
                }
                /*
                 * Sleeper Charge
                 */
                //echo $fareArray[0]['ADULT_FARE']."".$fareArray[0]['CHILD_FARE'];
                $fareArray[0]['ADULT_SLEEPER_FARE'] = $otherAmt['ADULT_SLEEPER_FARE'];
                $fareArray[0]['CHILD_SLEEPER_FARE'] = $otherAmt['CHILD_SLEEPER_FARE'];

                if ($this->conc <> "") {
                    //echo "3";
                    //exit;
                    $concFare = $this->getConcessionAmt($fareArray);
                    if ($concFare['ERR_MSG'] == ""):
                        switch ($this->AdultChild) {
                            case 'A':
                                $fareArray[0]['ERR_MSG'] = $concFare['ERR_MSG'];
                                if ($concFare["CONC_FARE"] . "" == "FNA") {
                                    $concFare['CONC_FARE'] = $fareArray[0]['ADULT_FARE'];
                                }
                                $fareArray[0]['ADULT_FARE'] = ($this->conc == "") ? $fareArray[0]['ADULT_FARE'] : $concFare['CONC_FARE'];

                                $fareArray[0]['A_STATE_1'] = ($this->conc == "") ? $fareArray[0]['A_STATE_1'] : $concFare['A_STATE_1'];
                                break;
                            case 'C':
                                $fareArray[0]['ERR_MSG'] = ($concFare['CONC_FARE'] . "" <> "FNA") ? "" : "0";
                                $concFare['CONC_FARE'] = ($concFare['CONC_FARE'] . "" <> "FNA") ? $concFare['CONC_FARE'] : $fareArray[0]['CHILD_FARE'];
                                $fareArray[0]['CHILD_FARE'] = ($this->conc == "") ? $fareArray[0]['CHILD_FARE'] : $concFare['CONC_FARE'];

                                $fareArray[0]['C_STATE_1'] = ($this->conc == "") ? $fareArray[0]['C_STATE_1'] : $concFare['C_STATE_1'];
                                break;
                        }
                        $fareArray[0]['CONC_RATE'] = $concFare['CONC_RATE'];
                        $fareArray[0]['IS_HOME_ONLY'] = $concFare['IS_HOME_ONLY'];
                    else:
                        $fareArray[0]['ERR_MSG'] = $concFare['ERR_MSG'];
                    endif;
                }
            }
        }
        /*
         * CATEGORY:
         *      1: Other Charges are Zero...
         *      2: Other Charges will not have any concession..
         */
        $fareArray[0]['CATEGORY'] = $concFare['CATEGORY'];
        if ($concFare['CATEGORY'] == 1) {
            //echo "5";
            $fareArray[0]['ACC_COMP_SURCHARGE'] = 0;
            $fareArray[0]['HR_SURCHARGE'] = 0;
            $fareArray[0]['IT_SURCHARGE'] = 0;
            $fareArray[0]['TOLL'] = 0;
            $fareArray[0]['TOLL_TAX'] = 0;
            $fareArray[0]['RESERVATION_AMT'] = 0;
            $fareArray[0]['IT_CHARGES'] = 0;
            $fareArray[0]['ADULT_SLEEPER_FARE'] = 0;
            $fareArray[0]['CHILD_SLEEPER_FARE'] = 0;
            $fareArray[0]['OCTROI_AMT'] = 0;
            $fareArray[0]['OTHER_CHARGES'] = 0;
        }

        if ($paraArray['hid_ticket_type'] == 'G') {
            // echo "6";
            if ($this->noofpax > 3) {
                $discount_amt = ($fareArray[0]['ADULT_FARE'] * $this->noofpax) * $discount_rate / 100;
                $fareArray[0]['DISCOUNT_AMT'] = round($discount_amt);
            }
        }

        /* added on 29th nov 2013 -yn */
        $disc_adt_fare = 0;
        $disc_cld_fare = 0;
        if ($disc_rate > 0) {
            //echo "7";
            $disc_adt_fare = (($fareArray[0]['ADULT_FARE'] * $disc_rate) / 100);
            $disc_cld_fare = (($fareArray[0]['CHILD_FARE'] * $disc_rate) / 100);
            $fareArray[0]['DISC_ADT_FARE'] = $disc_adt_fare;
            $fareArray[0]['DISC_CLD_FARE'] = $disc_cld_fare;
            $fareArray[0]['DISC_RATE'] = $disc_rate;
            $fareArray[0]['ADULT_FARE'] = ($fareArray[0]['ADULT_FARE'] - $disc_adt_fare);
            $fareArray[0]['CHILD_FARE'] = ($fareArray[0]['CHILD_FARE'] - $disc_cld_fare);
        }

        /* added on 29th nov 2013 -yn */




//        echo $fareArray[0]['ADULT_FARE']."-".$fareArray[0]['ACC_COMP_SURCHARGE']."-".$fareArray[0]['HR_SURCHARGE']."-".$fareArray[0]['IT_SURCHARGE']."-".$fareArray[0]['TOLL']."-";
//        $fareArray[0]['TOTAL_ADULT_FARE'] = $fareArray[0]['ADULT_FARE'] + $fareArray[0]['ACC_COMP_SURCHARGE'] +
//                $fareArray[0]['HR_SURCHARGE']+ $fareArray[0]['IT_SURCHARGE'] +
//                $fareArray[0]['TOLL'] + $fareArray[0]['IT_CHARGES'] + $fareArray[0]['OCTROI_AMT'] + $fareArray[0]['TOLL_TAX'];
//        $fareArray[0]['TOTAL_CHILD_FARE'] = $fareArray[0]['CHILD_FARE'] + $fareArray[0]['ACC_COMP_SURCHARGE'] +
//                $fareArray[0]['HR_SURCHARGE'] + $fareArray[0]['IT_SURCHARGE'] +
//                $fareArray[0]['TOLL'] + $fareArray[0]['IT_CHARGES'] + $fareArray[0]['OCTROI_AMT'] + $fareArray[0]['TOLL_TAX'];
        $fareArray[0]['TOTAL_ADULT_FARE'] = $fareArray[0]['ADULT_FARE'] + $fareArray[0]['ACC_COMP_SURCHARGE'] +
                $fareArray[0]['HR_SURCHARGE'] + $fareArray[0]['IT_SURCHARGE'] +
                $fareArray[0]['TOLL'] + $fareArray[0]['OTHER_CHARGES'];

        $fareArray[0]['TOTAL_CHILD_FARE'] = $fareArray[0]['CHILD_FARE'] + $fareArray[0]['ACC_COMP_SURCHARGE'] +
                $fareArray[0]['HR_SURCHARGE'] + $fareArray[0]['IT_SURCHARGE'] +
                $fareArray[0]['TOLL'] + $fareArray[0]['OTHER_CHARGES'];

        $fareArray[0]['TOTAL_KM'] = $this->TTL_KM;

        /* added on 29th nov 2013 -yn */

        $adultFare = round($fareArray[0]['TOTAL_ADULT_FARE'] / $fareArray[0]['FARE_FACTOR']);
        $fareArray[0]['TOTAL_ADULT_FARE'] = $adultFare * $fareArray[0]['FARE_FACTOR'];

        $childFare = round($fareArray[0]['TOTAL_CHILD_FARE'] / $fareArray[0]['FARE_FACTOR']);
        $fareArray[0]['TOTAL_CHILD_FARE'] = $childFare * $fareArray[0]['FARE_FACTOR'];


        $fareArray[0]['ADULT_FARE'] = $fareArray[0]['TOTAL_ADULT_FARE'] - ( $fareArray[0]['ACC_COMP_SURCHARGE'] +
                $fareArray[0]['HR_SURCHARGE'] + $fareArray[0]['IT_SURCHARGE'] +
                $fareArray[0]['TOLL'] + $fareArray[0]['OTHER_CHARGES']);
        $fareArray[0]['CHILD_FARE'] = $fareArray[0]['TOTAL_CHILD_FARE'] - ( $fareArray[0]['ACC_COMP_SURCHARGE'] +
                $fareArray[0]['HR_SURCHARGE'] + $fareArray[0]['IT_SURCHARGE'] +
                $fareArray[0]['TOLL'] + $fareArray[0]['OTHER_CHARGES']);

        /* added on 29th nov 2013 -yn */





        return $fareArray;
    }

    /*     * This Will return Fare per KM according to bus service type and route type
     *
     */

    function getAcsAmt() {

        $sql = "SELECT * FROM flat_fare WHERE ROUTE_NO = '" . $this->routeNo . "' AND BUS_TYPE_CD = '" . $this->bus_type_cd . "'

            AND FROM_STOP_CD = '" . $this->brdStop . "' AND TILL_STOP_CD = '" . $this->altStop . "'

            AND STR_TO_DATE('" . $this->depttm . "','%d/%m/%y') BETWEEN EFFECTIVE_FROM AND EFFECTIVE_TILL AND TRANSACTION_TYPE = 'A'";

        $res = $this->select($sql);

        if (count($res) == 0) {

            $res[0]['ADULT_AMT'] = "FNA";

            $res[0]['CHILD_AMT'] = "FNA";
        }

        return $res;
    }

    function getHrsAmt() {

        $sql = "SELECT * FROM flat_fare WHERE ROUTE_NO = '" . $this->routeNo . "' AND BUS_TYPE_CD = '" . $this->bus_type_cd . "'

            AND FROM_STOP_CD = '" . $this->brdStop . "' AND TILL_STOP_CD = '" . $this->altStop . "'

            AND STR_TO_DATE('" . $this->depttm . "','%d/%m/%y') BETWEEN EFFECTIVE_FROM AND EFFECTIVE_TILL AND TRANSACTION_TYPE = 'P'";

        $res = $this->select($sql);

        if (count($res) == 0) {

            $res[0]['ADULT_AMT'] = "FNA";

            $res[0]['CHILD_AMT'] = "FNA";
        }

        return $res;
    }

    function getFarePerKm() {

        //Get Reservation Charges

        $resCharge = $this->getReservationCharge();



        $acs = $this->getAcsAmt();

        $hrs = $this->getHrsAmt();
        $toll = $this->getTollAmt();
        //newly added

        $itn = $this->getITAmt();

        
        $fareArray[0]['RESERVATION_AMT'] = ((isset($resCharge) && isset($resCharge[0]))?$resCharge[0]['RESERVATION_AMT']:'0');

        $sql = "SELECT FARE_ID,ADULT_MIN_FARE,CHILD_MIN_FARE FROM ticket_fares WHERE FOR_ROUTE_TYPE = '" . $this->route_type . "'

        AND BUS_TYPE_CD = '" . $this->bus_type_cd . "'

        AND str_to_date('" . $this->depttm . "','%d/%m/%y') BETWEEN EFFECTIVE_FROM

            AND IF(EFFECTIVE_TILL IS NULL, DATE_ADD(STR_TO_DATE('" . $this->depttm . "','%d/%m/%y'), interval " . $_SESSION['SYS_CONF']['RES_OPEN_DAYS'] . " day),EFFECTIVE_TILL)";



        $res = $this->select($sql);

        if (count($res) == 1) {

            $km = $this->getKm();
            //$km_total=$this->getTotalKm();
            //if($km_total!='0' && $km_total!='')
            // $km=$km_total;
            //echo "KM:".$km;	
            if (isset($res) && $res[0]) {
                $fare = "SELECT FARE_PER_KM, ACC_COMP_SURCHARGE, HR_SURCHARGE, FACTOR_TYPE, FARE_FACTOR,IT_SURCHARGE,TOLL

            FROM ticket_fare_details WHERE FARE_ID = '" . $res[0]['FARE_ID'] . "' AND FARE_STATUS = 'Y' AND

            '" . $km . "' between FROM_KM AND TILL_KM";
                $fareDetails = $this->select($fare);
            }

            //  print_r($fareDetails);die;
            if ($this->CAL_METHOD == 'A') {
                $_POST['sel_f_category'] = 'A';
            }

            if (count($fareDetails) == 1) {
                if ($_POST['sel_f_category'] == 'A') {
                    if (isset($fareDetails[0]) && isset($fareDetails[0]['FARE_PER_KM'])) {
                        $fareArray[0]['FARE_PER_KM'] = $fareDetails[0]['FARE_PER_KM'];

                        $fareArray[0]['ACC_COMP_SURCHARGE'] = $fareDetails[0]['ACC_COMP_SURCHARGE'];
                        $fareArray[0]['HR_SURCHARGE'] = $fareDetails[0]['HR_SURCHARGE'];
                        // $fareArray[0]['TOLL'] = ($toll[0]['ADULT_AMT']=="FNA")?$fareDetails[0]['TOLL']:$toll[0]['ADULT_AMT'];

                        $fareArray[0]['TOLL_TAX'] = 0;
                        $fareArray[0]['IT_SURCHARGE'] = $fareDetails[0]['IT_SURCHARGE'];
                        $fareArray[0]['FACTOR_TYPE'] = $fareDetails[0]['FACTOR_TYPE'];
                        $fareArray[0]['FARE_FACTOR'] = $fareDetails[0]['FARE_FACTOR'];
                        $fareArray[0]['ADULT_MIN_FARE'] = $res[0]['ADULT_MIN_FARE'];
                        $fareArray[0]['CHILD_MIN_FARE'] = $res[0]['CHILD_MIN_FARE'];
                        $fareArray[0]['CHARGABLE_KM'] = $km;
                        return $fareArray;
                    } else {
                        $fareArray[0]['FARE_PER_KM'] = '0';
                        return $fareArray;
                    }
                } else if ($this->route_type_c == 'C' && $_POST['sel_f_category'] == 'F') {
                    // echo "aaa";
                    $fareArray[0]['FARE_PER_KM'] = $fareDetails[0]['FARE_PER_KM'];
                    $fareArray[0]['ACC_COMP_SURCHARGE'] = ($acs[0]['ADULT_AMT'] == "FNA") ? 0 : $acs[0]['ADULT_AMT'];
                    $fareArray[0]['HR_SURCHARGE'] = ($hrs[0]['ADULT_AMT'] == "FNA") ? 0 : $hrs[0]['ADULT_AMT'];
                    $fareArray[0]['TOLL'] = ($toll[0]['ADULT_AMT'] == "FNA") ? 0 : $toll[0]['ADULT_AMT'];
                    $fareArray[0]['IT_SURCHARGE'] = ($itn[0]['ADULT_AMT'] == "FNA") ? 0 : $itn[0]['ADULT_AMT'];
                    $fareArray[0]['FACTOR_TYPE'] = $fareDetails[0]['FACTOR_TYPE'];
                    $fareArray[0]['FARE_FACTOR'] = $fareDetails[0]['FARE_FACTOR'];
                    $fareArray[0]['ADULT_MIN_FARE'] = $res[0]['ADULT_MIN_FARE'];
                    $fareArray[0]['CHILD_MIN_FARE'] = $res[0]['CHILD_MIN_FARE'];
                    $fareArray[0]['CHARGABLE_KM'] = $km;

                    return $fareArray;
                } else {
                    // echo "bb";

                    $fareArray[0]['FARE_PER_KM'] = $fareDetails[0]['FARE_PER_KM'];
                    $fareArray[0]['ACC_COMP_SURCHARGE'] = $fareDetails[0]['ACC_COMP_SURCHARGE'];
                    $fareArray[0]['ACC_COMP_SURCHARGE'] = ($acs[0]['ADULT_AMT'] == "FNA") ? $fareDetails[0]['ACC_COMP_SURCHARGE'] : $acs[0]['ADULT_AMT'];
                    $fareArray[0]['HR_SURCHARGE'] = ($hrs[0]['ADULT_AMT'] == "FNA") ? $fareDetails[0]['HR_SURCHARGE'] : $hrs[0]['ADULT_AMT'];
                    $fareArray[0]['TOLL'] = ($toll[0]['ADULT_AMT'] == "FNA") ? $fareDetails[0]['TOLL'] : $toll[0]['ADULT_AMT'];


                    //$fareArray[0]['IT_SURCHARGE'] = $fareDetails[0]['IT_SURCHARGE'];
                    $fareArray[0]['IT_SURCHARGE'] = ($itn[0]['ADULT_AMT'] == "FNA") ? $fareDetails[0]['IT_SURCHARGE'] : $itn[0]['ADULT_AMT'];



                    $fareArray[0]['FACTOR_TYPE'] = $fareDetails[0]['FACTOR_TYPE'];

                    $fareArray[0]['FARE_FACTOR'] = $fareDetails[0]['FARE_FACTOR'];

                    $fareArray[0]['ADULT_MIN_FARE'] = $res[0]['ADULT_MIN_FARE'];

                    $fareArray[0]['CHILD_MIN_FARE'] = $res[0]['CHILD_MIN_FARE'];

                    $fareArray[0]['CHARGABLE_KM'] = $km;

                    return $fareArray;
                }
            } else {

                $fareArray[0]['FARE_PER_KM'] = 0;

                $fareArray[0]['ACC_COMP_SURCHARGE'] = 0;

                $fareArray[0]['HR_SURCHARGE'] = 0;

                $fareArray[0]['IT_SURCHARGE'] = 0;

                //$fareArray[0]['TOLL'] = 0;
                if (isset($toll) && isset($toll[0])) {
                    $fareArray[0]['TOLL'] = ($toll[0]['ADULT_AMT'] == "FNA") ? $fareDetails[0]['TOLL'] : $toll[0]['ADULT_AMT'];
                }

                $fareArray[0]['FACTOR_TYPE'] = 0;

                $fareArray[0]['FARE_FACTOR'] = 0;

                $fareArray[0]['CHARGABLE_KM'] = 0;

                $fareArray[0]['ADULT_MIN_FARE'] = 0;

                $fareArray[0]['CHILD_MIN_FARE'] = 0;

                return $fareArray;
            }
        } else {

            $fareArray[0]['FARE_PER_KM'] = 0;

            $fareArray[0]['ACC_COMP_SURCHARGE'] = 0;

            $fareArray[0]['HR_SURCHARGE'] = 0;

            $fareArray[0]['IT_SURCHARGE'] = 0;

            $fareArray[0]['TOLL'] = 0;

            $fareArray[0]['FACTOR_TYPE'] = 0;

            $fareArray[0]['FARE_FACTOR'] = 0;

            $fareArray[0]['CHARGABLE_KM'] = 0;

            $fareArray[0]['ADULT_MIN_FARE'] = 0;

            $fareArray[0]['CHILD_MIN_FARE'] = 0;

            return $fareArray;
        }
    }

    /**
     * Calculate Flat Rate Amount
     */
    function flatAmount() {
        $sql = "select * from flat_fare WHERE route_no = '" . $this->routeNo . "' AND BUS_TYPE_CD = '" . $this->bus_type_cd . "'
           and FROM_STOP_CD = '" . $this->brdStop . "' and TILL_STOP_Cd = '" . $this->altStop . "'
           and STR_TO_DATE('" . $this->depttm . "','%d/%m/%y') BETWEEN EFFECTIVE_FROM AND EFFECTIVE_TILL AND TRANSACTION_TYPE = 'F'";
        $res = $this->select($sql);
        if (count($res) == 1) {
            return $res;
        } else {
            $res[0]['ADULT_AMT'] = "FNA";
            $res[0]['CHILD_AMT'] = "FNA";
            return $res;
        }
    }

    /*
     * Interstate Fare
     */

    function getInterstateFare($paraArray) {
     //   print_r($paraArray);die;
        $pieces = explode("/", $paraArray['DEPT_DT']);
        $onDate = date("Y-m-d", strtotime("20" . $pieces[2] . "-" . $pieces[1] . "-" . $pieces[0]));
        /* if($paraArray['PROC']=='CURRENT'){
          $inter_stop=$this->getInterstateStop($paraArray);
          $res[0]['first_interstate_stop']=$inter_stop['FIRST_IS_STOP'];
          $res[0]['last_interstate_stop']=$inter_stop['LAST_IS_STOP'];
          }
          else{
          $sql = "SELECT * FROM temp_select_bus_services WHERE FROM_STOP_CD = '".$this->brdStop."'
          AND TILL_STOP_CD = '".$this->altStop."' AND BUS_SERVICE_NO = '".$this->busServiceNo."'";
          $res = $this->select($sql);
          } */
        if ($paraArray['PROC'] == 'CURRENT') {
            /* $sql = "SELECT * FROM temp_select_bus_services WHERE FROM_STOP_CD = '".$this->brdStop."'
              AND TILL_STOP_CD = '".$this->altStop."' AND BUS_SERVICE_NO = '".$this->busServiceNo."'";
              $res = $this->select($sql);
              $paraArray['FROM_STOP_SEQ'] = $res[0]['from_stop_seq'];
              $paraArray['TILL_STOP_SEQ'] = $res[0]['till_stop_seq'];

              $inter_stop=$this->getInterstateStop($paraArray);
              $res[0]['first_interstate_stop']=$inter_stop['FIRST_IS_STOP'];
              print_r($inter_stop);echo "deepak";
              $res[0]['last_interstate_stop']=$inter_stop['LAST_IS_STOP'];
              $paraArray['FIRST_IS_STOP_SEQ'] = $inter_stop['FIRST_IS_STOP_SEQ'];
              $paraArray['LAST_IS_STOP_SEQ'] = $inter_stop['LAST_IS_STOP_SEQ']; */
            if($this->busServiceNo!='0' && isset($this->busServiceNo)){
                $sql = "SELECT * FROM temp_select_bus_services WHERE FROM_STOP_CD = '" . $this->brdStop . "'
                AND BUS_SERVICE_NO = '" . $this->busServiceNo . "'";
                $res = $this->select($sql);

                if (isset($res) && isset($res[0]) && isset($res[0]['from_stop_seq'])) {
                    $paraArray['FROM_STOP_SEQ'] = $res[0]['from_stop_seq'];
                } else {
                    $paraArray['FROM_STOP_SEQ'] = 1;
                }

                $sql = "SELECT rs.STOP_SEQ,bs.bus_type_cd FROM route_stops rs
                            inner join bus_services bs on bs.ROUTE_NO = rs.ROUTE_NO
                                AND rs.STATUS = 'Y' 
                                AND bs.BUS_SERVICE_NO = '" . $this->busServiceNo . "'
                                    AND BUS_STOP_CD = '" . $this->altStop . "' ";
                $res = $this->select($sql);
                $res[0]['bus_type_cd'] = $res[0]['bus_type_cd'];
                $paraArray['TILL_STOP_SEQ'] = $res[0]['STOP_SEQ'];
            }
            else{
                 $sql = "SELECT rs.STOP_SEQ FROM route_stops rs
                           
                    where rs.STATUS = 'Y'  AND rs.BUS_STOP_CD = '".$paraArray['ALT_STOP']."' and rs.ROUTE_NO='".$paraArray['ROUTE_NO']."'";
                     

                $res = $this->select($sql);
                $res[0]['bus_type_cd'] = $paraArray['BUS_TYPE_CD'];
                $paraArray['TILL_STOP_SEQ'] = $res[0]['STOP_SEQ'];

            }
                $inter_stop = $this->getInterstateStop($paraArray);
                $res[0]['first_interstate_stop'] = $inter_stop['FIRST_IS_STOP'];
                $res[0]['last_interstate_stop'] = $inter_stop['LAST_IS_STOP'];
                $paraArray['FIRST_IS_STOP_SEQ'] = $inter_stop['FIRST_IS_STOP_SEQ'];
                $paraArray['LAST_IS_STOP_SEQ'] = $inter_stop['LAST_IS_STOP_SEQ'];
            }
         else {
            $sql = "SELECT * FROM temp_select_bus_services WHERE FROM_STOP_CD = '" . $this->brdStop . "'
                AND TILL_STOP_CD = '" . $this->altStop . "' AND BUS_SERVICE_NO = '" . $this->busServiceNo . "'";
            $res = $this->select($sql);
            $paraArray['FROM_STOP_SEQ'] = $res[0]['from_stop_seq'];
            $paraArray['TILL_STOP_SEQ'] = $res[0]['till_stop_seq'];
            $inter_stop = $this->getInterstateStop($paraArray);
            $paraArray['FIRST_IS_STOP_SEQ'] = $inter_stop['FIRST_IS_STOP_SEQ'];
            $paraArray['LAST_IS_STOP_SEQ'] = $inter_stop['LAST_IS_STOP_SEQ'];
        }
        //print_r($res);
        if ($res[0]['first_interstate_stop'] <> "") {

            $cal_met = 2;
            if ($cal_met == 1) {
                if (count($res) == 1) {
                    $sql = "SELECT * FROM flat_fare where FROM_STOP_CD = '" . $res[0]['first_interstate_stop'] . "'
                    AND TILL_STOP_CD = '" . $res[0]['last_interstate_stop'] . "' AND ROUTE_NO = '" . $this->routeNo . "'
                        AND BUS_TYPE_CD = '" . $res[0]['bus_type_cd'] . "' AND STR_TO_DATE('" . $this->depttm . "','%d/%m/%y')
                            BETWEEN EFFECTIVE_FROM AND EFFECTIVE_TILL AND TRANSACTION_TYPE = 'I'";
                    $sql = "SELECT * FROM flat_fare where FROM_STOP_CD = '" . $res[0]['first_interstate_stop'] . "'
                            AND TILL_STOP_CD = '" . $res[0]['last_interstate_stop'] . "' AND ROUTE_NO = '" . $this->routeNo . "'
                                AND FROM_SEQ = '" . $paraArray['FIRST_IS_STOP_SEQ'] . "' AND TILL_SEQ = '" . $paraArray['LAST_IS_STOP_SEQ'] . "'
                                    AND BUS_TYPE_CD = '" . $res[0]['bus_type_cd'] . "' AND STR_TO_DATE('" . $this->depttm . "','%d/%m/%y')
                                        BETWEEN EFFECTIVE_FROM AND EFFECTIVE_TILL AND TRANSACTION_TYPE = 'I'";
                    $res = $this->select($sql);
                    if (count($res) == 1) {
                        $iFare['I_ADULT_AMT'] = $res[0]['ADULT_AMT'];
                        $iFare['I_CHILD_AMT'] = $res[0]['CHILD_AMT'];
                    } else {
                        $iFare['I_ADULT_AMT'] = 'FNA';
                        $iFare['I_CHILD_AMT'] = 'FNA';
                    }
                } else {
                    $iFare['I_ADULT_AMT'] = '0';
                    $iFare['I_CHILD_AMT'] = '0';
                }
            }
            if ($cal_met == 2) {

                if (count($res) == 1) {

                    /* include_once('../app_classes/automatic_fare.class.php');
                      $automaticfare = new Automatic_fare();
                      $iFare=$automaticfare->getAutomaticFare(PARENT_STATE_CD,$this->routeNo,$this->brdStop,$this->altStop,$paraArray['FROM_STOP_SEQ'],$paraArray['TILL_STOP_SEQ'],$onDate,$this->bus_type_cd);
                     */
                    // //  echo "<pre>";print_r($iFare);
                    // exit;
                    if (empty($iFare)) {

                        $iFare['I_ADULT_AMT'] = 'SNA';
                        $iFare['I_CHILD_AMT'] = 'SNA';
                        ?>

                        <?php

                    }
                }
            }
        } else {
            $iFare['I_ADULT_AMT'] = '0';
            $iFare['I_CHILD_AMT'] = '0';
        }

        return $iFare;
    }

    /*     * Get Concession Rate along with Concession scope
     */

    function getConcessionAmt($fareArray) {
        /*
         * Validate Concession
         * check if concession is applicable for Passenger or not
         */
        $concFlag = false;
        $sql = "SELECT CONCESSION_CD,CHILD_PERMITTED,ADULT_PERMITTED,IS_PARENTCONC,PARENT_CONC_CD,`CATEGORY`
            FROM concessions WHERE CONCESSION_CD = '" . $this->conc . "'";
        $res = $this->select($sql);
        if (count($res) == 0) {
            $fare['ERR_MSG'] = "0";
            $fare['CONC_FARE'] = "FNA";
            return $fare;
        }

        if (isset($res[0]) && isset($res[0]['CATEGORY'])) {
            $fare['CATEGORY'] = $res[0]['CATEGORY'];
        } else {
            $fare['CATEGORY'] = '1';
        }
        switch ($this->AdultChild) {
            case 'A':
                if (isset($res[0]) && $res[0]['ADULT_PERMITTED'] == 'Y') {
                    $concFlag = true;
                } else {
                    $fare['ERR_MSG'] = "1";
                    return $fare;
                }
                break;
            case 'C':
                if (isset($res[0]) && $res[0]['CHILD_PERMITTED'] == 'Y') {
                    $concFlag = true;
                } else {
                    $fare['ERR_MSG'] = "1";
                    return $fare;
                }
                break;
        }
        if ($concFlag) {
            switch ($this->flatFlag) {
                case 'N':
                    $sql = "SELECT CONCESSION_RATE,IS_HOME_STATE_ONLY FROM concession_rates WHERE BUS_TYPE_CD = '" . $this->bus_type_cd . "'
                    AND STR_TO_DATE('" . $this->depttm . "','%d/%m/%y') BETWEEN EFFECTIVE_FROM
                        AND if(EFFECTIVE_TILL IS NULL, DATE_ADD(NOW(),INTERVAL '" . $_SESSION['SYS_CONF']['RES_OPEN_DAYS'] . "' DAY),EFFECTIVE_TILL)
                        AND CONCESSION_CD = '" . $this->conc . "'";
                    $res = $this->select($sql);
                    if (count($res) == 1) {
                        $this->cc_rate = $res[0]['CONCESSION_RATE'];
                        $homeConc = $res[0]['IS_HOME_STATE_ONLY'];
                    } else {
                        $fare['ERR_MSG'] = "1";
                        return $fare;
                    }
                    break;
                case 'Y':
                    $sql = "SELECT CONCESSION_RATE,IS_HOME_STATE_ONLY FROM concession_rates WHERE BUS_TYPE_CD = '" . $this->bus_type_cd . "'
                    AND STR_TO_DATE('" . $this->depttm . "','%d/%m/%y') BETWEEN EFFECTIVE_FROM
                        AND if(EFFECTIVE_TILL IS NULL, DATE_ADD(STR_TO_DATE('" . $this->depttm . "','%d/%m/%y'),INTERVAL '" . $_SESSION['SYS_CONF']['RES_OPEN_DAYS'] . "' DAY),EFFECTIVE_TILL)
                        AND CONCESSION_CD = '" . $this->conc . "'";
                    $res = $this->select($sql);
                    if (count($res) == 1) {
                        $this->cc_rate = $res[0]['CONCESSION_RATE'];
                        $homeConc = $res[0]['IS_HOME_STATE_ONLY'];
                    } else {
                        $fare['ERR_MSG'] = "1";
                        return $fare;
                    }
                    break;
            }
            /*
             * Calculating Concessional Amount
             */
            if ($homeConc == 'N') {
                switch ($this->AdultChild) {
                    case 'A':
                        $fare['CONC_FARE'] = $fareArray[0]['ADULT_FARE'] * (100 - $this->cc_rate) / 100;
                        $fare['CONC_FARE'] = round($fare['CONC_FARE']);

                        $fare['A_STATE_1'] = $fareArray[0]['A_STATE_1'] * (100 - $this->cc_rate) / 100;
                        $fare['A_STATE_1'] = ROUND($fare['A_STATE_1']);
                        $fare['HOME_STATE_AMT'] = $fare['CONC_FARE'] - $fare['A_STATE_1'];

                        $fare['CONC_RATE'] = $this->cc_rate;
                        $fare['IS_HOME_ONLY'] = $homeConc;
                        return $fare;
                        break;
                    case 'C':
                        $fare['CONC_FARE'] = $fareArray[0]['CHILD_FARE'] * (100 - $this->cc_rate) / 100;
                        $fare['CONC_FARE'] = round($fare['CONC_FARE']);

                        $fare['C_STATE_1'] = $fareArray[0]['C_STATE_1'] * (100 - $this->cc_rate) / 100;
                        $fare['C_STATE_1'] = ROUND($fare['C_STATE_1']);
                        $fare['C_HOME_STATE_AMT'] = $fare['CONC_FARE'] - $fare['C_STATE_1'];

                        $fare['CONC_RATE'] = $this->cc_rate;
                        $fare['IS_HOME_ONLY'] = $homeConc;
                        return $fare;
                        break;
                }
            }
            if ($homeConc == 'Y') {
                //State Wise Break up..
                switch ($this->AdultChild) {
                    case 'A':
                        $fare['CONC_FARE'] = ($fareArray[0]['ADULT_FARE'] - $fareArray[0]['A_STATE_1']) * (100 - $this->cc_rate) / 100;
                        $fare['CONC_FARE'] = round($fare['CONC_FARE']) + $fareArray[0]['A_STATE_1'];

                        $fare['A_STATE_1'] = $fareArray[0]['A_STATE_1'];
                        $fare['HOME_STATE_AMT'] = $fare['CONC_FARE'] - $fare['A_STATE_1'];

                        $fare['CONC_RATE'] = $this->cc_rate;
                        $fare['IS_HOME_ONLY'] = $homeConc;
                        return $fare;
                        break;
                    case 'C':
                        $fare['CONC_FARE'] = ($fareArray[0]['CHILD_FARE'] - $fareArray[0]['C_STATE_1']) * (100 - $this->cc_rate) / 100;
                        $fare['CONC_FARE'] = round($fare['CONC_FARE']) + $fareArray[0]['C_STATE_1'];

                        $fare['C_STATE_1'] = $fareArray[0]['C_STATE_1'];
                        $fare['C_HOME_STATE_AMT'] = $fare['CONC_FARE'] - $fare['C_STATE_1'];

                        $fare['CONC_RATE'] = $this->cc_rate;
                        $fare['IS_HOME_ONLY'] = $homeConc;
                        return $fare;
                        break;
                }
            }
        } else {
            
        }
    }

    /*
     * Get Reservation Charge
     */

    function getReservationCharge() {
        $sql = "SELECT BUS_TYPE_CD, RESERVATION_AMT FROM bus_reservation_charges WHERE BUS_TYPE_CD = '" . $this->bus_type_cd . "'
            AND EFFECTIVE_TILL IS NULL";
        return $this->select($sql);
    }

    /*
     * Get IT Charge
     */

    function getITCharges() {
        /* $sql = "SELECT BUS_TYPE_CD, IT_CHARGES FROM bus_it_charges WHERE BUS_TYPE_CD = '".$this->bus_type_cd."'
          AND EFFECTIVE_TILL IS NULL";
          return $this->select($sql); */
        $res='';
        $_SESSION['SYS_CONF']['RES_OPEN_DAYS'] = 1;
        $sql = "SELECT KM,BUS_TYPE_CD,INTRA_STATE_DISTANCE FROM route_stops rs INNER JOIN bus_services bs on bs.ROUTE_NO = rs.ROUTE_NO
		WHERE rs.STATUS = 'Y' AND BUS_STOP_CD IN('" . $this->brdStop . "', '" . $this->altStop . "')
		AND bs.BUS_SERVICE_NO = '" . $this->busServiceNo . "' ";
        $res = $this->select($sql);
        if (isset($res) && isset($res[0]) && isset($res[0]['KM'])) {
            $km = abs($res[0]['KM'] - $res[1]['KM']);
        }
        if (isset($res) && isset($res[0]) && isset($res[0]['INTRA_STATE_DISTANCE'])) {

            $kmIntra = abs($res[0]['INTRA_STATE_DISTANCE'] - $res[1]['INTRA_STATE_DISTANCE']);
        }
        $sql = "SELECT BUS_TYPE_CD, IT_CHARGES,IT_CHARGE_TYPE,IS_HOME_STATE_ONLY, DISTANCE_CHARGEBLE 
            FROM bus_it_charges WHERE BUS_TYPE_CD = '" . $this->bus_type_cd . "'
            AND str_to_date('" . $this->depttm . "','%d/%m/%y') BETWEEN EFFECTIVE_FROM
            AND IF(EFFECTIVE_TILL IS NULL, DATE_ADD(STR_TO_DATE('" . $this->depttm . "','%d/%m/%y'), interval " . $_SESSION['SYS_CONF']['RES_OPEN_DAYS'] . " day),EFFECTIVE_TILL)";
        $res = $this->select($sql);
        if (count($res) == 0) {
            $res[0]['BUS_TYPE_CD'] = $this->bus_type_cd;
            $res[0]['IT_CHARGE_TYPE'] = 'S';
            $res[0]['IT_CHARGES'] = 0;
        } else {
            if (isset($res[0]) && isset($res[0]['IS_HOME_STATE_ONLY'])) {
                switch ($res[0]['IS_HOME_STATE_ONLY']) {
                    case 'Y':
                        if ($res[0]['DISTANCE_CHARGEBLE'] >= $kmIntra)
                            $res[0]['IT_CHARGES'] = 0;
                        break;
                    case 'N':
                        if ($res[0]['DISTANCE_CHARGEBLE'] >= $km)
                            $res[0]['IT_CHARGES'] = 0;
                        break;
                }
            }
        }
//        if ($res[0]['BUS_TYPE_CD'] == 'EXP') {
//            if ($km <= 50) {
//                $res[0]['IT_CHARGES'] = 0;
//            }
//        }

        return $res;
    }

    /*
     * Get Toll tax with in the specified Route
     */

    function getTollAmt() {
        $sql = "SELECT * FROM flat_fare WHERE ROUTE_NO = '" . $this->routeNo . "' AND BUS_TYPE_CD = '" . $this->bus_type_cd . "'
            AND FROM_STOP_CD = '" . $this->brdStop . "' AND TILL_STOP_CD = '" . $this->altStop . "'
            AND STR_TO_DATE('" . $this->depttm . "','%d/%m/%y') BETWEEN EFFECTIVE_FROM AND EFFECTIVE_TILL AND TRANSACTION_TYPE = 'T'";
        $res = $this->select($sql);
        if (count($res) == 0) {
            $res[0]['ADULT_AMT'] = "FNA";
            $res[0]['CHILD_AMT'] = "FNA";
        }

        return $res;
    }

    /*
     * Get Octroi with in the specified Route
     */

    function getOctroiAmt() {
        $sql = "SELECT * FROM flat_fare WHERE ROUTE_NO = '" . $this->routeNo . "' AND BUS_TYPE_CD = '" . $this->bus_type_cd . "'
            AND FROM_STOP_CD = '" . $this->brdStop . "' AND TILL_STOP_CD = '" . $this->altStop . "'
            AND STR_TO_DATE('" . $this->depttm . "','%d/%m/%y') BETWEEN EFFECTIVE_FROM AND EFFECTIVE_TILL AND TRANSACTION_TYPE = 'O'";
        $res = $this->select($sql);
        if (count($res) == 0) {
            $res[0]['ADULT_AMT'] = 0;
            $res[0]['CHILD_AMT'] = 0;
        }
        return $res;
    }

    /*
     * Deside Passenger Type
     * Adult Or Child
     */

    function paxType($paxAge) {
        $_SESSION['SYS_CONF']['CHILD_MAX_AGE'] = 'A';
        $this->AdultChild = ($paxAge > $_SESSION['SYS_CONF']['CHILD_MAX_AGE']) ? 'A' : 'C';
    }

    /*
     * Finding Other amount based on journey details and
     * seat type.
     */

    function getOtherAmt() {
        $sql = "SELECT bs.BUS_SERVICE_NO, bfs.SEAT_TYPE,bs.ROUTE_NO, bs.BUS_FORMAT_CD,bs.BUS_TYPE_CD FROM bus_services bs INNER JOIN bus_format_seats bfs
	ON bfs.bus_format_cd = bs.BUS_FORMAT_CD
	WHERE bs.BUS_SERVICE_NO = '" . $this->busServiceNo . "' AND SEAT_TYPE = 'S' GROUP BY SEAT_TYPE";
        $res = $this->select($sql);
        if (isset($res[0]) && count($res) == 1) {
            $sql = "select ADULT_SLEEPER_FARE, CHILD_SLEEPER_FARE FROM sleeper_fare WHERE BUS_TYPE_CD = '" . $res[0]['BUS_TYPE_CD'] . "'
                AND BUS_FORMAT_CD = '" . $res[0]['BUS_FORMAT_CD'] . "' AND BUS_STOP_CD = '" . $this->altStop . "'
                    AND str_to_date('" . $this->depttm . "','%d/%m/%y') BETWEEN EFFECTIVE_FROM AND IF(EFFECTIVE_TILL IS NULL, DATE_ADD(str_to_date('" . $this->depttm . "','%d/%m/%y'), INTERVAL '" . $_SESSION['SYS_CONF']['RES_OPEN_DAYS'] . "' DAY),EFFECTIVE_TILL)
                        AND ROUTE_NO = '" . $res[0]['ROUTE_NO'] . "'";
            $res = $this->select($sql);
            if (count($res) == 1) {
                $sFare['ADULT_SLEEPER_FARE'] = $res[0]['ADULT_SLEEPER_FARE'];
                $sFare['CHILD_SLEEPER_FARE'] = $res[0]['CHILD_SLEEPER_FARE'];
            } else {
                $sFare['ADULT_SLEEPER_FARE'] = "0";
                $sFare['CHILD_SLEEPER_FARE'] = "0";
            }
            return $sFare;
        }
        $sFare['ADULT_SLEEPER_FARE'] = "0";
        $sFare['CHILD_SLEEPER_FARE'] = "0";
        return $sFare;
    }

    /*
     * Service Charges
     */

    function getServiceCharge($paraArray) {
        /*
         * Service Charge On Transaction Type
         */$serTx = 0;
        $serAgent = 0;
        $_SESSION['SYS_CONF']['RES_OPEN_DAYS'] = 1;
        $_SESSION['agentType'] = 1;
        $sql = "SELECT SERVICE_CHARGE, SERVICE_CHARGE_TYPE FROM payment_mode WHERE AGENT_TYPE = '" . $_SESSION['agentType'] . "'
            AND PAY_ID = '" . $paraArray['PAY_MODE'] . "' AND DATE(NOW())
                BETWEEN EFFECTIVE_FROM AND IF(EFFECTIVE_TILL IS NULL, DATE_ADD(NOW(), INTERVAL " . $_SESSION['SYS_CONF']['RES_OPEN_DAYS'] . " DAY),EFFECTIVE_TILL)";
        $res = $this->select($sql);
        if (isset($res[0])) {
            switch ($res[0]['SERVICE_CHARGE_TYPE']) {
                case 'S':
                    $serTx = $paraArray['NO_PAX'] * $res[0]['SERVICE_CHARGE'];
                    break;
                case 'T':
                    $serTx = $res[0]['SERVICE_CHARGE'];
                    break;
            }
        }
        /*
         * Service Charge On Agent Type
         */
        switch ($_SESSION['agentType']) {
            case 'M':
                $serAgent = 0;
                break;
            case 'P':
                if ($paraArray['BUS_TYPE_CD'] <> "") {
                    $sql = "SELECT SERVICE_CHARGE, SERVICE_CHARGE_TYPE FROM pagent_service_charge WHERE BUS_TYPE_CD = '" . $paraArray['BUS_TYPE_CD'] . "'
                    AND DATE(NOW()) BETWEEN EFFECTIVE_FROM AND IF(EFFECTIVE_TILL IS NULL, DATE_ADD(NOW(), INTERVAL " . $_SESSION['SYS_CONF']['RES_OPEN_DAYS'] . " DAY),EFFECTIVE_TILL)";
                } else {
                    $sql = "SELECT pc.SERVICE_CHARGE, pc.SERVICE_CHARGE_TYPE FROM pagent_service_charge pc inner join bus_services bs on bs.bus_type_cd = pc.bus_type_cd
                         WHERE bs.BUS_SERVICE_NO = '" . $paraArray['BUS_SERVICE_NO'] . "'
                    AND DATE(NOW()) BETWEEN pc.EFFECTIVE_FROM AND IF(pc.EFFECTIVE_TILL IS NULL, DATE_ADD(NOW(), INTERVAL " . $_SESSION['SYS_CONF']['RES_OPEN_DAYS'] . " DAY),pc.EFFECTIVE_TILL)";
                }
                $res = $this->select($sql);
                switch ($res[0]['SERVICE_CHARGE_TYPE']) {
                    case 'S':
                        $serAgent = $paraArray['NO_PAX'] * $res[0]['SERVICE_CHARGE'];
                        break;
                    case 'T':
                        $serAgent = $res[0]['SERVICE_CHARGE'];
                        break;
                }
                break;
        }
        $serCharge = $serTx + $serAgent;
        return $serCharge;
    }

    function getBaseFare($paraArray, $all_post) {
        //echo '<pre>';print_r($_POST);
        $_POST = $all_post;
        $_SESSION['SYS_CONF']['RES_OPEN_DAYS'] = 30;
        $this->routeNo = $paraArray['ROUTE_NO'];
        $this->brdStop = $paraArray['BRD_STOP'];
        $this->altStop = $paraArray['ALT_STOP'];
        $this->km = $paraArray['KM'];
        $this->KM_TOTAL = $paraArray['KM_TOTAL'];
        $this->depttm = $paraArray['DEPARTURE_TM'];
        $this->bus_type_cd = $paraArray['BUS_TYPE_CD'];
        $this->route_type_c = $paraArray['ROUTE_TYPE'];
        $this->route_type = 'I';
        $km = $this->getKm();

        $fareDetails = $this->getFarePerKm();
        //echo '<pre>';print_r($fareDetails);exit;
        $fareArray[0]['FARE_PER_KM'] = $fareDetails[0]['FARE_PER_KM'];
        $fareArray[0]['ACC_COMP_SURCHARGE'] = $fareDetails[0]['ACC_COMP_SURCHARGE'];
        $fareArray[0]['HR_SURCHARGE'] = $fareDetails[0]['HR_SURCHARGE'];
        $fareArray[0]['IT_SURCHARGE'] = $fareDetails[0]['IT_SURCHARGE'];
        $fareArray[0]['TOLL'] = $fareDetails[0]['TOLL'];
        $fareArray[0]['RESERVATION_AMT'] = $fareDetails[0]['RESERVATION_AMT'];

        $fareArray[0]['FACTOR_TYPE'] = $fareDetails[0]['FACTOR_TYPE'];
        $fareArray[0]['FARE_FACTOR'] = $fareDetails[0]['FARE_FACTOR'];
        $fareArray[0]['CHARGABLE_KM'] = $km;
        $fareArray[0]['ADULT_MIN_FARE'] = $fareDetails[0]['ADULT_MIN_FARE'];
        $fareArray[0]['CHILD_MIN_FARE'] = $fareDetails[0]['CHILD_MIN_FARE'];

        //echo "C KM:".$km;

        switch ($fareArray[0]['FACTOR_TYPE']) {
            case 'R':
                $baseFare = round(($fareArray[0]['FARE_PER_KM'] * $km / 100), 2);
                $finalFare = $baseFare + $fareArray[0]['ACC_COMP_SURCHARGE'] + $fareArray[0]['HR_SURCHARGE'];
                $finalFare = round($finalFare / $fareArray[0]['FARE_FACTOR']) * $fareArray[0]['FARE_FACTOR'];
                //commented on  300113
                //$baseFare = $finalFare - ($fareArray[0]['ACC_COMP_SURCHARGE'] + $fareArray[0]['HR_SURCHARGE']);
                if ($km > 0) {
                    // $baseFare = ($baseFare < $fareArray[0]['ADULT_MIN_FARE']) ? $fareArray[0]['ADULT_MIN_FARE'] : $baseFare;
                }
                break;
            case 'C':
                $baseFare = round($fareArray[0]['FARE_PER_KM'] * $km / 100);
                $finalFare = $baseFare + $fareArray[0]['ACC_COMP_SURCHARGE'] + $fareArray[0]['HR_SURCHARGE'];
                $finalFare = Ceil($finalFare / $fareArray[0]['FARE_FACTOR']) * $fareArray[0]['FARE_FACTOR'];
                $baseFare = $finalFare - ($fareArray[0]['ACC_COMP_SURCHARGE'] + $fareArray[0]['HR_SURCHARGE']);
                if ($km > 0) {
                    $baseFare = ($baseFare < $fareArray[0]['ADULT_MIN_FARE']) ? $fareArray[0]['ADULT_MIN_FARE'] : $baseFare;
                }
                break;
            case 'F':
                $baseFare = round($fareArray[0]['FARE_PER_KM'] * $km / 100);
                $finalFare = $baseFare + $fareArray[0]['ACC_COMP_SURCHARGE'] + $fareArray[0]['HR_SURCHARGE'];
                $finalFare = floor($finalFare / $fareArray[0]['FARE_FACTOR']) * $fareArray[0]['FARE_FACTOR'];
                $baseFare = $finalFare - ($fareArray[0]['ACC_COMP_SURCHARGE'] + $fareArray[0]['HR_SURCHARGE']);
                if ($km > 0) {
                    $baseFare = ($baseFare < $fareArray[0]['ADULT_MIN_FARE']) ? $fareArray[0]['ADULT_MIN_FARE'] : $baseFare;
                }
                break;
        }
        $this->from_stop_seq = $paraArray['FROM_STOP_SEQ'];
        $this->till_stop_seq = $paraArray['TILL_STOP_SEQ'];

        $res = $this->printTollAmt();
        $flatAmt = $this->getFlatOctroiFare($_POST['sel_f_category']);
        //echo '<pre>';print_r($flatAmt);exit;

        $isStops = $this->getInterstateStop($paraArray);
        $paraArray['FIRST_IS_STOP'] = $isStops['FIRST_IS_STOP'];
        $paraArray['LAST_IS_STOP'] = $isStops['LAST_IS_STOP'];
        $paraArray['FIRST_IS_STOP_SEQ'] = $isStops['FIRST_IS_STOP_SEQ'];
        $paraArray['LAST_IS_STOP_SEQ'] = $isStops['LAST_IS_STOP_SEQ'];
        $inter = $this->getISFare($paraArray);

        if ($_POST['rd_fare'] == 'BF') {
            if ($_POST['sel_f_category'] == 'F') {
                return $flatAmt[0]['ADULT_AMT'];
            } else {
                return $baseFare;
            }
            return $baseFare;
        } else if ($_POST['rd_fare'] == 'ACS') {
            return $fareArray[0]['ACC_COMP_SURCHARGE'];
        } else if ($_POST['rd_fare'] == 'HRS') {
            return $fareArray[0]['HR_SURCHARGE'];
        } else if ($_POST['rd_fare'] == 'TT') {
            if ($_POST['sel_f_category'] == 'F') {
                return $fareArray[0]['TOLL'];
            } else {
                return $fareArray[0]['TOLL'];
            }
        } else if ($_POST['rd_fare'] == 'IR') {
            return $inter['I_ADULT_AMT'];
        } else if ($_POST['rd_fare'] == 'IT') {
            return $fareArray[0]['IT_SURCHARGE'];
        } else if ($_POST['rd_fare'] == 'FF') {
            if ($_POST['sel_f_category'] == 'F') {
                $totalFare = makeRound($flatAmt[0]['ADULT_AMT'] + $fareArray[0]['ACC_COMP_SURCHARGE'] + $fareArray[0]['HR_SURCHARGE'] + $fareArray[0]['TOLL'] + $fareArray[0]['IT_SURCHARGE'] + $fareArray[0]['RESERVATION_AMT']);

                return ($flatAmt[0]['ADULT_AMT'] . "<br>" . $fareArray[0]['ACC_COMP_SURCHARGE'] . "<br>" . $fareArray[0]['HR_SURCHARGE'] . "<br>" . $fareArray[0]['TOLL'] . "<br>" . $fareArray[0]['IT_SURCHARGE'] . "<br>" . $fareArray[0]['RESERVATION_AMT'] . "<br>" . $totalFare);
            } else {

                //echo $baseFare."-".$inter['I_ADULT_AMT']."<br>";

                if ($paraArray['FIRST_IS_STOP'] <> "") {
                    $baseFare = $baseFare + $inter['I_ADULT_AMT'];
                    $totalFare = makeRound($baseFare + $fareArray[0]['ACC_COMP_SURCHARGE'] + $fareArray[0]['HR_SURCHARGE'] + $fareArray[0]['TOLL'] + $fareArray[0]['IT_SURCHARGE'] + $fareArray[0]['RESERVATION_AMT']);
                    return ($baseFare . "<br>" . $fareArray[0]['ACC_COMP_SURCHARGE'] . "<br>" . $fareArray[0]['HR_SURCHARGE'] . "<br>" . $fareArray[0]['TOLL'] . "<br>" . $fareArray[0]['IT_SURCHARGE'] . "<br>" . $fareArray[0]['RESERVATION_AMT'] . "<br>" . $totalFare);
                } else {
                    $totalFare = makeRound($baseFare + $fareArray[0]['ACC_COMP_SURCHARGE'] + $fareArray[0]['HR_SURCHARGE'] + $fareArray[0]['TOLL'] + $fareArray[0]['IT_SURCHARGE'] + $fareArray[0]['RESERVATION_AMT']);
                    return ($baseFare . "<br>" . $fareArray[0]['ACC_COMP_SURCHARGE'] . "<br>" . $fareArray[0]['HR_SURCHARGE'] . "<br>" . $fareArray[0]['TOLL'] . "<br>" . $fareArray[0]['IT_SURCHARGE'] . "<br>" . $fareArray[0]['RESERVATION_AMT'] . "<br>" . $totalFare);
                }
            }
        }

        return $baseFare . " " . $fareArray[0]['ACC_COMP_SURCHARGE'] . " " . $fareArray[0]['HR_SURCHARGE'] . " " . $res[0]['ADULT_AMT'] . "-" . $paraArray['FROM_STOP_SEQ'] . "-" . $paraArray['TILL_STOP_SEQ'];
        //return
    }

    /*
     * Base Fare For Current Booking
     */

    function getBaseFare_auto($paraArray, $all_post) {
        // echo '<pre>';print_r($paraArray);
        // echo '<pre>';print_r($_POST);
        $_POST = $all_post;
        $_SESSION['SYS_CONF']['RES_OPEN_DAYS'] = 30;
        $this->routeNo = $paraArray['ROUTE_NO'];
        $this->brdStop = $paraArray['BRD_STOP'];
        $this->altStop = $paraArray['ALT_STOP'];
        $this->km = $paraArray['KM'];
        $this->KM_TOTAL = $paraArray['KM_TOTAL'];
        $this->depttm = $paraArray['DEPARTURE_TM'];
        $this->bus_type_cd = $paraArray['BUS_TYPE_CD'];
        $this->route_type_c = $paraArray['ROUTE_TYPE'];
        $this->route_type = 'I';
        $km = $this->getKm();
        //echo "Date". $this->depttm;
        //echo date("Y/m/d", strtotime($this->depttm));
        //echo "date = :".$_GET['cd'];

        $pieces = explode("/", $this->depttm);
        $onDate = date("Y-m-d", strtotime("20" . $pieces[2] . "-" . $pieces[1] . "-" . $pieces[0]));

        $this->from_stop_seq = $paraArray['FROM_STOP_SEQ'];
        $this->till_stop_seq = $paraArray['TILL_STOP_SEQ'];

        $res = $this->printTollAmt();
        $flatAmt = $this->getFlatOctroiFare($_POST['sel_f_category']);
        //echo '<pre>';print_r($flatAmt);exit;

        $isStops = $this->getInterstateStop($paraArray);
        $paraArray['FIRST_IS_STOP'] = $isStops['FIRST_IS_STOP'];
        $paraArray['LAST_IS_STOP'] = $isStops['LAST_IS_STOP'];
        $paraArray['FIRST_IS_STOP_SEQ'] = $isStops['FIRST_IS_STOP_SEQ'];
        $paraArray['LAST_IS_STOP_SEQ'] = $isStops['LAST_IS_STOP_SEQ'];
        $inter = $this->getISFare($paraArray);

        /* added on 13/01/14 */
        include_once('../app_classes/automatic_fare.class.php');
        $automaticfare = new Automatic_fare();


        if ($_POST['sel_f_category'] == 'A' && $this->route_type_c == 'I') {
            if ($paraArray['FIRST_IS_STOP'] <> "") {
                // echo "test here";
                /*
                 * Request Paramaters :
                  1. Parent State  :  UP
                  2. Route No.      :  15
                  3. From Stop Code : LKO
                  4. Till Stop Code : DLI
                  5. From Stop Sequence : 1
                  6. Till Stop Sequence.   : 30
                  7. Departure Date    : 2014-01-13
                  8. Bus Type Code  : ORD
                 */
                //echo PARENT_STATE_CD;
                $fareArr = $automaticfare->getAutomaticFare(PARENT_STATE_CD, $this->routeNo, $this->brdStop, $this->altStop, $this->from_stop_seq, $this->till_stop_seq, $onDate, $this->bus_type_cd);
                //echo "<pre>";print_r($fareArr);
                if (empty($fareArr)) {
                    ?>
                    <script>
                        document.getElementById('error').className = "showerror";
                    </script>
                <?php

                }
                $total_basefare = 0;
                $total_acs = 0;
                $total_hr = 0;
                $total_it = 0;
                $total_otherCharges = 0;

                if (count($fareArr) != count($fareArr, COUNT_RECURSIVE)) {
                    foreach ($fareArr as $array) {
                        $total_basefare += $array['basefare'];
                        $total_acs += $array['acs'];
                        $total_hr += $array['hr'];
                        $total_it += $array['it'];
                        $total_otherCharges += $array['otherCharge1'] + $array['otherCharge2'] + $array['otherCharge3'] + $array['otherCharge4'] + $array['otherCharge5'];
                    }
                } else {
                    $total_basefare = $fareArr['basefare'];
                    $total_acs = $fareArr['acs'];
                    $total_hr = $fareArr['hr'];
                    $total_it = $fareArr['it'];
                    $total_otherCharges = $fareArr['otherCharge1'] + $fareArr['otherCharge2'] + $fareArr['otherCharge3'] + $fareArr['otherCharge4'] + $fareArr['otherCharge5'];
                }
                //    echo "sum_basefare-->". $total_basefare.'<br>';
                //    echo "sum_acs-->". $total_acs.'<br>';
                //    echo "sum_hr-->". $total_hr.'<br>';
                //    echo "sum_it-->". $total_it.'<br>';
                //    echo "total_otherCharges-->". $total_otherCharges.'<br>';
            }
            $total_int_fare = $total_basefare + $total_acs + $total_hr + $total_it;
        }


        if ($_POST['sel_f_category'] == 'A' && $this->route_type_c == 'C') {

            $cityfareArr = $automaticfare->getCityFare($this->routeNo, $this->from_stop_seq, $this->till_stop_seq, $onDate, $this->bus_type_cd, 'C', 'C');
            // echo "<pre>";
            // print_r($cityfareArr);
            if (empty($cityfareArr)) {
                ?>
                <script>
                    document.getElementById('error').className = "showerror";
                </script>
            <?php

            }
            $total_citybasefare = 0;
            $total_cityacs = 0;
            $total_cityhr = 0;
            $total_cityit = 0;
            $total_citytoll = 0;
            $total_finalFare = 0;

            $total_citybasefare = round($cityfareArr['netFare'], 2);
            $total_cityacs = $cityfareArr['acs'];
            $total_cityhr = $cityfareArr['pas'];
            $total_citytoll = $cityfareArr['toll'];
            $total_cityit = $cityfareArr['it'];
            $total_finalFare = $cityfareArr['finalFare'];
            // $total_cityotherCharges = $fareArr['otherCharge1'] + $fareArr['otherCharge2']+ $fareArr['otherCharge3']+ $fareArr['otherCharge4']+ $fareArr['otherCharge5'];

            $total_city_fare = $total_citybasefare + $total_cityacs + $total_cityhr + $total_cityit + $total_citytoll;
        }

        /* added on 13/01/14 */



        $fareDetails = $this->getFarePerKm();
        // echo '<pre>';print_r($fareDetails);
        //exit;
        if ($km == 0) {
            $fareArray[0]['ACC_COMP_SURCHARGE'] = 0;
            $fareArray[0]['HR_SURCHARGE'] = 0;
            $fareArray[0]['IT_SURCHARGE'] = 0;
            $fareArray[0]['TOLL'] = $fareDetails[0]['TOLL'];
        } else {
            $fareArray[0]['ACC_COMP_SURCHARGE'] = $fareDetails[0]['ACC_COMP_SURCHARGE'];
            $fareArray[0]['HR_SURCHARGE'] = $fareDetails[0]['HR_SURCHARGE'];
            $fareArray[0]['IT_SURCHARGE'] = $fareDetails[0]['IT_SURCHARGE'];
            $fareArray[0]['TOLL'] = $fareDetails[0]['TOLL'];
        }
        $fareArray[0]['FARE_PER_KM'] = $fareDetails[0]['FARE_PER_KM'];


        $fareArray[0]['RESERVATION_AMT'] = $fareDetails[0]['RESERVATION_AMT'];

        $fareArray[0]['FACTOR_TYPE'] = $fareDetails[0]['FACTOR_TYPE'];
        $fareArray[0]['FARE_FACTOR'] = $fareDetails[0]['FARE_FACTOR'];
        $fareArray[0]['CHARGABLE_KM'] = $km;
        $fareArray[0]['ADULT_MIN_FARE'] = $fareDetails[0]['ADULT_MIN_FARE'];
        $fareArray[0]['CHILD_MIN_FARE'] = $fareDetails[0]['CHILD_MIN_FARE'];
        if ($total_otherCharges != '') {
            $fareArray[0]['otherCharges'] = $total_otherCharges;
        } else {
            $fareArray[0]['otherCharges'] = 0;
        }

        //echo "C KM:".$km;

        switch ($fareArray[0]['FACTOR_TYPE']) {
            case 'R':
                $baseFare = round(($fareArray[0]['FARE_PER_KM'] * $km / 100), 2);
                $finalFare = $baseFare + $fareArray[0]['ACC_COMP_SURCHARGE'] + $fareArray[0]['HR_SURCHARGE'];
                $finalFare = round($finalFare / $fareArray[0]['FARE_FACTOR']) * $fareArray[0]['FARE_FACTOR'];
                //commented on  300113
                //$baseFare = $finalFare - ($fareArray[0]['ACC_COMP_SURCHARGE'] + $fareArray[0]['HR_SURCHARGE']);
                if ($km > 0) {
                    // $baseFare = ($baseFare < $fareArray[0]['ADULT_MIN_FARE']) ? $fareArray[0]['ADULT_MIN_FARE'] : $baseFare;
                }
                break;
            case 'C':
                $baseFare = round($fareArray[0]['FARE_PER_KM'] * $km / 100);
                $finalFare = $baseFare + $fareArray[0]['ACC_COMP_SURCHARGE'] + $fareArray[0]['HR_SURCHARGE'];
                $finalFare = Ceil($finalFare / $fareArray[0]['FARE_FACTOR']) * $fareArray[0]['FARE_FACTOR'];
                $baseFare = $finalFare - ($fareArray[0]['ACC_COMP_SURCHARGE'] + $fareArray[0]['HR_SURCHARGE']);
                if ($km > 0) {
                    $baseFare = ($baseFare < $fareArray[0]['ADULT_MIN_FARE']) ? $fareArray[0]['ADULT_MIN_FARE'] : $baseFare;
                }
                break;
            case 'F':
                $baseFare = round($fareArray[0]['FARE_PER_KM'] * $km / 100);
                $finalFare = $baseFare + $fareArray[0]['ACC_COMP_SURCHARGE'] + $fareArray[0]['HR_SURCHARGE'];
                $finalFare = floor($finalFare / $fareArray[0]['FARE_FACTOR']) * $fareArray[0]['FARE_FACTOR'];
                $baseFare = $finalFare - ($fareArray[0]['ACC_COMP_SURCHARGE'] + $fareArray[0]['HR_SURCHARGE']);
                if ($km > 0) {
                    $baseFare = ($baseFare < $fareArray[0]['ADULT_MIN_FARE']) ? $fareArray[0]['ADULT_MIN_FARE'] : $baseFare;
                }
                break;
        }

        if ($_POST['rd_fare'] == 'BF') {
            if ($_POST['sel_f_category'] == 'F') {
                return $flatAmt[0]['ADULT_AMT'];
            } else {
                if ($paraArray['FIRST_IS_STOP'] <> "") {
                    $baseFare = $baseFare + $inter['I_ADULT_AMT'];
                }
                return $baseFare;
            }
            //return $baseFare;
        } else if ($_POST['rd_fare'] == 'ACS') {
            return $fareArray[0]['ACC_COMP_SURCHARGE'];
        } else if ($_POST['rd_fare'] == 'HRS') {
            return $fareArray[0]['HR_SURCHARGE'];
        } else if ($_POST['rd_fare'] == 'TT') {
            if ($_POST['sel_f_category'] == 'F') {
                return $fareArray[0]['TOLL'];
            } else {
                return $fareArray[0]['TOLL'];
            }
        } else if ($_POST['rd_fare'] == 'IR') {
            return $inter['I_ADULT_AMT'];
        } else if ($_POST['rd_fare'] == 'IT') {
            return $fareArray[0]['IT_SURCHARGE'];
        } else if ($_POST['rd_fare'] == 'FF') {
            if ($_POST['sel_f_category'] == 'F') {
                $totalFare = makeRound($flatAmt[0]['ADULT_AMT'] + $fareArray[0]['ACC_COMP_SURCHARGE'] + $fareArray[0]['HR_SURCHARGE'] + $fareArray[0]['TOLL'] + $fareArray[0]['IT_SURCHARGE'] + $fareArray[0]['RESERVATION_AMT'] + $fareArray[0]['otherCharges']);

                return ($flatAmt[0]['ADULT_AMT'] . "<br>" . $fareArray[0]['ACC_COMP_SURCHARGE'] . "<br>" . $fareArray[0]['HR_SURCHARGE'] . "<br>" . $fareArray[0]['TOLL'] . "<br>" . $fareArray[0]['IT_SURCHARGE'] . "<br>" . $fareArray[0]['RESERVATION_AMT'] . "<br>" . $fareArray[0]['otherCharges'] . "<br>" . $totalFare . "<br>" . sbiRound($totalFare - $fareArray[0]['RESERVATION_AMT']));
            } elseif ($_POST['sel_f_category'] == 'A' && $this->route_type_c == 'I') {
//$total_basefare + $total_acs+ $total_hr+ $total_it
                //echo $total_basefare."-".$total_acs."-".$total_hr."-".$total_it;


                $totalFare = makeRound($baseFare + $fareArray[0]['ACC_COMP_SURCHARGE'] + $fareArray[0]['HR_SURCHARGE'] + $fareArray[0]['TOLL'] + $fareArray[0]['IT_SURCHARGE'] + $fareArray[0]['RESERVATION_AMT'] + $fareArray[0]['otherCharges'] + $total_int_fare);
                // return ($baseFare ." - <span>".$total_basefare."</span><br>" .$fareArray[0]['ACC_COMP_SURCHARGE'] ."-<span>".$total_acs."</span><br>". $fareArray[0]['HR_SURCHARGE']."-<span>". $total_hr ."</span><br>". $fareArray[0]['TOLL']."<br>".$fareArray[0]['IT_SURCHARGE']."-<span>".$total_it."</span><br>".$fareArray[0]['RESERVATION_AMT']."<br>".$fareArray[0]['otherCharges']."<br>".$totalFare);
                return ("<table style='empty-cells:hide;' border='1' cellpadding='0' cellspacing='0'><tr><td class='fare_left'>" . $baseFare . "<td><td class='fare_right'>" . $total_basefare . "</td></tr><tr><td class='fare_left'>" . $fareArray[0]['ACC_COMP_SURCHARGE'] . "<td><td class='fare_right'>" . $total_acs . "</td></tr><tr><td class='fare_left'>" . $fareArray[0]['HR_SURCHARGE'] . "<td><td class='fare_right'>" . $total_hr . "</td></tr><tr><td class='fare_left'>" . $fareArray[0]['TOLL'] . "<td><td class='fare_right'>-</td></tr><tr><td class='fare_left'>" . $fareArray[0]['IT_SURCHARGE'] . "<td><td class='fare_right'>" . $total_it . "</td></tr><tr><td class='fare_left'>" . $fareArray[0]['RESERVATION_AMT'] . "<td><td class='fare_right'>-</td></tr><tr><td class='fare_left'>-<td><td class='fare_right'>" . $fareArray[0]['otherCharges'] . "</td></tr><tr><td colspan='3' align='right'>" . $totalFare . "<td></tr><tr><td colspan='3' align='right'>" . sbiRound($totalFare - $fareArray[0]['RESERVATION_AMT']) . "<td></tr></table>");
            } elseif ($_POST['sel_f_category'] == 'A' && $this->route_type_c == 'C') {
//$total_basefare + $total_acs+ $total_hr+ $total_it
                //echo $total_basefare."-".$total_acs."-".$total_hr."-".$total_it;


                $totalFare = makeRound($baseFare + $fareArray[0]['ACC_COMP_SURCHARGE'] + $fareArray[0]['HR_SURCHARGE'] + $fareArray[0]['TOLL'] + $fareArray[0]['IT_SURCHARGE'] + $fareArray[0]['RESERVATION_AMT'] + $fareArray[0]['otherCharges'] + $total_int_fare);
                // return ($baseFare ." - <span>".$total_basefare."</span><br>" .$fareArray[0]['ACC_COMP_SURCHARGE'] ."-<span>".$total_acs."</span><br>". $fareArray[0]['HR_SURCHARGE']."-<span>". $total_hr ."</span><br>". $fareArray[0]['TOLL']."<br>".$fareArray[0]['IT_SURCHARGE']."-<span>".$total_it."</span><br>".$fareArray[0]['RESERVATION_AMT']."<br>".$fareArray[0]['otherCharges']."<br>".$totalFare);
                return ("<table><tr><td class='city_fare'>" . $total_citybasefare . "</td></tr><tr><td class='city_fare'>" . $total_cityacs . "</td></tr><tr><td class='city_fare'>" . $total_cityhr . "</td></tr><tr><td class='city_fare'>" . $total_citytoll . "</td></tr><tr><td class='city_fare'>" . $total_cityit . "</td></tr><tr><td class='city_fare'>-</td></tr><tr><td class='city_fare'>-<td></tr><tr><td class='city_fare'>" . $cityfareArray[0]['finalFare'] . "<td></tr><tr><td class='city_fare'>" . $total_finalFare . "</td></tr><tr><td class='city_fare'>" . sbiRound($total_finalFare) . "</td></tr></table>");
            } else {

                //echo $baseFare."-".$inter['I_ADULT_AMT']."<br>";

                if ($paraArray['FIRST_IS_STOP'] <> "") {
                    $baseFare = $baseFare + $inter['I_ADULT_AMT'];
                    $totalFare = makeRound($baseFare + $fareArray[0]['ACC_COMP_SURCHARGE'] + $fareArray[0]['HR_SURCHARGE'] + $fareArray[0]['TOLL'] + $fareArray[0]['IT_SURCHARGE'] + $fareArray[0]['RESERVATION_AMT'] + $fareArray[0]['otherCharges']);
                    return ($baseFare . "<br>" . $fareArray[0]['ACC_COMP_SURCHARGE'] . "<br>" . $fareArray[0]['HR_SURCHARGE'] . "<br>" . $fareArray[0]['TOLL'] . "<br>" . $fareArray[0]['IT_SURCHARGE'] . "<br>" . $fareArray[0]['RESERVATION_AMT'] . "<br>" . $fareArray[0]['otherCharges'] . "<br>" . $totalFare . "<br>" . sbiRound($totalFare - $fareArray[0]['RESERVATION_AMT']));
                } else {
                    $totalFare = makeRound($baseFare + $fareArray[0]['ACC_COMP_SURCHARGE'] + $fareArray[0]['HR_SURCHARGE'] + $fareArray[0]['TOLL'] + $fareArray[0]['IT_SURCHARGE'] + $fareArray[0]['RESERVATION_AMT'] + $fareArray[0]['otherCharges']);
                    return ($baseFare . "<br>" . $fareArray[0]['ACC_COMP_SURCHARGE'] . "<br>" . $fareArray[0]['HR_SURCHARGE'] . "<br>" . $fareArray[0]['TOLL'] . "<br>" . $fareArray[0]['IT_SURCHARGE'] . "<br>" . $fareArray[0]['RESERVATION_AMT'] . "<br>" . $fareArray[0]['otherCharges'] . "<br>" . $totalFare . "<br>" . sbiRound($totalFare - $fareArray[0]['RESERVATION_AMT']));
                }
            }
        }

        return $baseFare . " " . $fareArray[0]['ACC_COMP_SURCHARGE'] . " " . $fareArray[0]['HR_SURCHARGE'] . " " . $res[0]['ADULT_AMT'] . "-" . $paraArray['FROM_STOP_SEQ'] . "-" . $paraArray['TILL_STOP_SEQ'];
        //return
    }

    function getInterstateStop($paraArray) {
        define('PARENT_STATE_CD', 'UP');
        if (isset($paraArray['FROM_STOP_SEQ']) && isset($paraArray['TILL_STOP_SEQ'])) {
            $sql = "SELECT rs.BUS_STOP_CD,bs.STATE_CD,rs.STOP_SEQ FROM route_stops rs INNER JOIN bus_stops bs
            ON bs.BUS_STOP_CD = rs.BUS_STOP_CD
            WHERE rs.STATUS = 'Y' AND rs.ROUTE_NO = '" . $this->routeNo . "'
                AND rs.STOP_SEQ BETWEEN '" . $paraArray['FROM_STOP_SEQ'] . "' AND '" . $paraArray['TILL_STOP_SEQ'] . "'
                    ORDER BY rs.STOP_SEQ";
            $res = $this->select($sql);
            $cnt = count($res);
            $firstIS = "";
            $lastIS = "";
            $paraArray['FIRST_IS_STOP'] = "";
            $paraArray['LAST_IS_STOP'] = "";
            for ($i = 0; $i < $cnt; $i++) {
                if ($firstIS == "") {
                    if ($res[$i]['STATE_CD'] <> PARENT_STATE_CD) {
                        $firstIS = $res[$i]['BUS_STOP_CD'];
                        $paraArray['FIRST_IS_STOP'] = $res[$i]['BUS_STOP_CD'];
                        $paraArray['LAST_IS_STOP'] = $res[$i]['BUS_STOP_CD'];
                        $paraArray['FIRST_IS_STOP_SEQ'] = $res[$i]['STOP_SEQ'];
                        $paraArray['LAST_IS_STOP_SEQ'] = $res[$i]['STOP_SEQ'];
                    }
                } else {
                    if ($res[$i]['STATE_CD'] <> PARENT_STATE_CD) {
                        $lastIS = $res[$i]['BUS_STOP_CD'];
                        $paraArray['LAST_IS_STOP'] = $res[$i]['BUS_STOP_CD'];
                        $paraArray['LAST_IS_STOP_SEQ'] = $res[$i]['STOP_SEQ'];
                    }
                }
            }
        }
        return $paraArray;
    }

    /*
     * Interstate Fare For Route Fare Calculation
     */

    function getISFare($paraArray) {
        if ($paraArray['FIRST_IS_STOP'] <> "") {

            $sql = "SELECT * FROM flat_fare where FROM_STOP_CD = '" . $paraArray['FIRST_IS_STOP'] . "'
                AND TILL_STOP_CD = '" . $paraArray['LAST_IS_STOP'] . "' AND ROUTE_NO = '" . $this->routeNo . "'
                    AND BUS_TYPE_CD = '" . $paraArray['BUS_TYPE_CD'] . "' AND STR_TO_DATE('" . $this->depttm . "','%d/%m/%y')                        
                        BETWEEN EFFECTIVE_FROM AND EFFECTIVE_TILL AND TRANSACTION_TYPE = 'I'
                            AND FROM_SEQ = '" . $paraArray['FIRST_IS_STOP_SEQ'] . "' AND TILL_SEQ = '" . $paraArray['LAST_IS_STOP_SEQ'] . "'";
            $res = $this->select($sql);
            if (count($res) == 1) {
                $iFare['I_ADULT_AMT'] = $res[0]['ADULT_AMT'];
                $iFare['I_CHILD_AMT'] = $res[0]['CHILD_AMT'];
            } else {
                $iFare['I_ADULT_AMT'] = 'FNA';
                $iFare['I_CHILD_AMT'] = 'FNA';
            }
        } else {
            $iFare['I_ADULT_AMT'] = '0';
            $iFare['I_CHILD_AMT'] = '0';
        }
        return $iFare;
    }

    /*
     * Direct Fare calculation
     */

    function getFlatOctroiFare($trsType) {

        $sql = "SELECT * FROM flat_fare WHERE ROUTE_NO = '" . $this->routeNo . "' AND BUS_TYPE_CD = '" . $this->bus_type_cd . "'
            AND FROM_STOP_CD = '" . $this->brdStop . "' AND TILL_STOP_CD = '" . $this->altStop . "'
            AND STR_TO_DATE('" . $this->depttm . "','%d/%m/%y') BETWEEN EFFECTIVE_FROM AND EFFECTIVE_TILL 
                AND TRANSACTION_TYPE = '" . $trsType . "'";
        $res = $this->select($sql);
        if (count($res) == 0) {
            $res[0]['ADULT_AMT'] = 0;
            $res[0]['CHILD_AMT'] = 0;
        }
        return $res;
    }

    function printTollAmt() {
        $sql = "SELECT * FROM flat_fare WHERE ROUTE_NO = '" . $this->routeNo . "' AND BUS_TYPE_CD = '" . $this->bus_type_cd . "'
            AND FROM_STOP_CD = '" . $this->brdStop . "' AND TILL_STOP_CD = '" . $this->altStop . "'
                AND FROM_SEQ = '" . $this->from_stop_seq . "' AND TILL_SEQ = '" . $this->till_stop_seq . "'
            AND STR_TO_DATE('" . $this->depttm . "','%d/%m/%y') BETWEEN EFFECTIVE_FROM AND EFFECTIVE_TILL AND TRANSACTION_TYPE = 'T'";
        $res = $this->select($sql);
        if (count($res) == 0) {
            $res[0]['ADULT_AMT'] = 0;
            $res[0]['CHILD_AMT'] = 0;
        }
        return $res;
    }

    function getDiscountRate() {
        $sql = "SELECT SCHEME_DISCOUNT FROM passanger_scheme WHERE SCHEME_CODE='gd' AND SCHEME_EFFECTILL IS NULL";
        $res = $this->select($sql);
        return $res;
    }

    function getDiscRate() {


        $sql = "select DISC_RATE from reservation_disc_master where BUS_TYPE_CD='" . $this->bus_type_cd . "' and str_to_date('" . $this->depttm . "','%d/%m/%Y') between EFF_FROM and EFF_TILL and datediff(str_to_date('" . $this->depttm . "','%d/%m/%Y'),CURDATE()) between FROM_DAYS and TILL_DAYS and now() >=ISSUE_DATE";
        $res = $this->select($sql);
        return $res;
    }

    function direct_fare($paraArray) {
        //print_r($paraArray);die; 
        if(isset($paraArray['BUS_SERVICE_NO']) && $paraArray['BUS_SERVICE_NO']!='0'){
            $sql = "SELECT BASE_FARE FROM direct_fare df 
                INNER JOIN bus_services bs ON(df.BUS_TYPE_CD = bs.BUS_TYPE_CD)
                WHERE FROM_STOP_CD = '" . $paraArray['BRD_STOP'] . "'
                AND TILL_STOP_CD = '" . $paraArray['ALT_STOP'] . "' AND bs.BUS_SERVICE_NO = '" . $paraArray['BUS_SERVICE_NO'] . "'
                    AND STR_TO_DATE('" . $paraArray['DEPT_DT'] . "','%d/%m/%y') BETWEEN df.EFFECTIVE_FROM AND IF(df.EFFECTIVE_TILL IS NULL,DATE_ADD(NOW(), INTERVAL 35 DAY), df.EFFECTIVE_TILL)";
        }else{
        $sql = "SELECT BASE_FARE FROM direct_fare df 
            WHERE FROM_STOP_CD = '" . $paraArray['BRD_STOP'] . "' 
            AND TILL_STOP_CD = '" . $paraArray['ALT_STOP'] . "' AND df.BUS_TYPE_CD = '" . $paraArray['BUS_TYPE_CD'] . "'
                AND STR_TO_DATE('" . $paraArray['DEPT_DT'] . "','%d/%m/%y') BETWEEN df.EFFECTIVE_FROM AND IF(df.EFFECTIVE_TILL IS NULL,DATE_ADD(NOW(), INTERVAL 35 DAY), df.EFFECTIVE_TILL)";
        
        }
        $res = $this->select($sql);
        return $res;
    }

    /*
     * Get IT tax with in the specified Route
     */

    function getITAmt() {
        $sql = "SELECT * FROM flat_fare WHERE ROUTE_NO = '" . $this->routeNo . "' AND BUS_TYPE_CD = '" . $this->bus_type_cd . "'
            AND FROM_STOP_CD = '" . $this->brdStop . "' AND TILL_STOP_CD = '" . $this->altStop . "'
            AND STR_TO_DATE('" . $this->depttm . "','%d/%m/%y') BETWEEN EFFECTIVE_FROM AND EFFECTIVE_TILL AND TRANSACTION_TYPE = 'S'";
        $res = $this->select($sql);
        if (count($res) == 0) {
            $res[0]['ADULT_AMT'] = "FNA";
            $res[0]['CHILD_AMT'] = "FNA";
        }

        return $res;
    }

}
?>
