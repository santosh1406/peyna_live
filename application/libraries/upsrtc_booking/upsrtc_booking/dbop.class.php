<?php
include_once("config.php");
include_once("dbconn.class.php");
include_once("dbop.class.php");
$dbconnect = new dbconn();
$conn = $dbconnect->getConnect();

	class dbop{

		/**
		 *	select query execution method is here
		 *	$sql	=	sql query to be executed
		 */
		 function select1($q)
		{
			//print($q);
			$rs=mysql_query($q) or die(mysql_error());
			$num=mysql_result($rs,0,0);			
			//print($num);
			return ($num);
			//return ($rs);
		}


		function select($sql,$iDim=0){	
                    
                    $data='';
			if (is_null($sql) || empty($sql)){
                            echo $sql;
                            echo $this->error("Problem In SQL Query!",$sql);
                            die();
                        }
                        if(!preg_match("/select/i", $sql)){
                            echo $this->error("Wrong SQL Query!", $sql);
                            die;
                        }            
                        $count = 0;
                        $result = mysql_query($sql) or die($this->error("Wrong SQL Query!", $sql));

                        if ($iDim == 1) {
                            while($row = mysql_fetch_array($result, MYSQL_NUM)){
                                $data[$row[0]] = $row[1];
                            }
                        } elseif ($iDim == 2) {
                            while($row = mysql_fetch_array($result, MYSQL_BOTH)){
                                //print_r($row);
                                $data[$row[0]][$row[1]] = $row;
                            }
                        } else {
                            while($row = mysql_fetch_array($result, MYSQL_ASSOC)){
                                $data[$count] = $row;
                                $count++;
                            }
                        }
                        mysql_free_result($result);
                        return $data;
		}

		/**
		 *	INSERT QUERY OPERATIONS WILL BE DONE BY insert($sql)
		 *	METHOD YOU HAVE TO PASS SQL QUERY AS ARGUMENT
		 *	$sql	=	sql query to be executed
		 */

		function insert($sql){
			if (is_null($sql) || empty($sql)){
				echo $this->error("Problem In SQL Query!",$sql);
				die();
			}
			if(!eregi("^insert", $sql)){
				echo $this->error("Wrong SQL Query!", $sql);
				die;
			}
			/*
			if(!is_resource($this->conn)){
				return false;
			}
			*/
			$result = mysql_query($sql) or die($this->error("Problem In Query Execution!", $sql));
			return mysql_insert_id();
		}

		/**		
		 *	UPDATE QUERY OPERATIONS WILL BE DONE BY update($sql)
		 *	METHOD YOU HAVE TO PASS SQL QUERY AS ARGUMENT		
		 *	$sql	=	sql query to be executed		
		 */

		function update($sql){
			if (is_null($sql) || empty($sql)){
				echo $this->error("Problem In SQL Query!",$sql);
				die();
			}
			if(!eregi("^update", $sql)){
				echo $this->error("Wrong SQL Query!", $sql);
				die;
			}
			$result = mysql_query($sql) or die($this->error("Problem In Query Execution!", $sql));
			return mysql_affected_rows();
		}

		/**
		 *	EXECUTE ALL SQL QUERIES/STATEMENTS FROM THIS FUNCTION
		 *	$sql	:	YOUR SQL QUERY TO BE EXECUTE
		 */

		function exec_sql($sql,$dbconn){
			if (is_null($sql) || empty($sql)){
				echo $this->error("Problem In SQL Query!",$sql);
				die();
			}
			$result = mysql_query($sql) or die($this->error("Problem In Query Execution!", $sql));
			if($result){
				$count = 0;
				while($row = mysql_fetch_array($result, MYSQL_ASSOC)){
					$data[$count] = $row;
					$count++;
				}
				mysql_free_result($result);
				return $data;
			}else{
				return false;
			}//if
		}

		function exec_sql_gen($sql,$dbconn){
			if (is_null($sql) || empty($sql)){
				echo $this->error("Problem In SQL Query!",$sql);
				die();
			}
			/*
			if(!is_resource($dbconn)){
				return false;
			}
			*/
			$result = mysql_query($sql,$dbconn) or die($this->error("Problem In Query Execution!", $sql));
			if($result){
				$count = 0;
				//mysql_free_result($result);
				return $data;
			}else{
				return false;
			}//if
		}

		function exec_sql_ins($sql,$dbconn){
			if (is_null($sql) || empty($sql)){
				echo $this->error("Problem In SQL Query!",$sql);
				die();
			}
			/*
			if(!is_resource($dbconn)){
				return false;
			}
			*/
			$result = mysql_query($sql,$dbconn) or die($this->error("Problem In Query Execution!", $sql));
			return mysql_insert_id();
		}

		function exec_sql_ph($sql,$dbconn){
			if (is_null($sql) || empty($sql)){
				echo $this->error("Problem In SQL Query!",$sql);
				die();
			}
			/*
			if(!is_resource($dbconn)){
				return false;
			}
			*/
			$result = mysql_query($sql,$dbconn) or die($this->error("Problem In Query Execution!", $sql));
			if(!eregi("^select", $sql)){
				return true;
			} else {
				$count = 0;
				$row = mysql_fetch_array($result, MYSQL_ASSOC);
				$data = $row['ph'];
				$count++;
				mysql_free_result($result);
				return $data;
			}
		}

		/**
		 *	EXECUTE ALL SQL QUERIES/STATEMENTS FROM THIS FUNCTION
		 *	$sql	:	YOUR SQL QUERY TO BE EXECUTE
		 */

		function exec_sql_num($sql,$dbconn){
			if (is_null($sql) || empty($sql)){
				echo $this->error("Problem In SQL Query!",$sql);
				die();
			}
			/*
			if(!is_resource($dbconn)){
				return false;
			}
			*/
			$result = mysql_query($sql,$dbconn) or die($this->error("Problem In Query Execution!", $sql));
			if(!eregi("^select", $sql)){
				return true;
			} else {
				$count  = 0;
				$data = mysql_num_rows($result);
				mysql_free_result($result);
				return $data;
			}
		}

		/**
		 *	EXECUTE ONLY SPECIFIC RECORD
		 *	$sql	:	YOUR SQL QUERY TO BE EXECUTE
		 */

		function exec_sql_srow($sql,$dbconn){
			if (is_null($sql) || empty($sql)){
				echo $this->error("Problem In SQL Query!",$sql);
				die();
			}
			/*
			if(!is_resource($dbconn)){
				return false;
			}
			*/
			$result = mysql_query($sql,$dbconn) or die($this->error("Problem In Query Execution!",$sql));
			if(!eregi("^select", $sql)){
				return true;
			} else {
				$count = 0;
				$data = mysql_fetch_row($result);
				mysql_free_result($result);
				return $data;
			}
		}

		/**
		 *	function getInsertSql returns sql query for insert a new
		 *	record in database table
		 *	$tbl		:	table name for operation
		 *	@fldArr	:	name of database fields
		 *	@valArr	:	value for each database field which you
		 *						defined in @fldArr
		 */

		function getInsertSql($tbl, $fldArr, $valArr){
			if (!is_array($fldArr) || !is_array($valArr)){
				echo "<pre>\n<h1>Problem In 'FIELD AND VALUE ARRAY'!</h1>Fields and Values must be pass as array.<hr></pre>";
				die();
			}
			if (sizeof($fldArr) !== sizeof($valArr)){
				echo "<pre>\n<h1>Problem In 'FIELD AND VALUE ARRAY'!</h1>Fields and Values arrays size must be same.<hr></pre>";
				die();
			}
			$sql = "INSERT INTO $tbl(";
			for ($i=0; $i<sizeof($fldArr); $i++){
				$sql .= "`".$fldArr[$i]."`, ";
			}
			$sql = $this->getSubString($sql, 0, strlen($sql)-2);
			$sql .= ") VALUES(";
			for ($i=0; $i<sizeof($valArr); $i++){
				$sql .= ($valArr[$i]!='now()') ? "'".$valArr[$i]."', " : $valArr[$i] . ", ";
			}
			$sql = $this->getSubString($sql, 0, strlen($sql)-2);
			$sql = $sql.")";
			
			return $sql;
		}

		/**
		 *	function getUpdateSql returns sql query for update
		 *	record(s) in database table
		 *	$tbl		:	table name for operation
		 *	@fldArr	:	name of database fields
		 *	@valArr	:	value for each database field which you
		 *						defined in @fldArr
		 */

		function getUpdateSql($tbl, $fldArr, $valArr, $fldName="", $value=""){
			if (!is_array($fldArr) || !is_array($valArr)){
				echo "<pre>\n<h1>Problem In 'FIELD AND VALUE ARRAY'!</h1>Fields and Values must be passed as array.<hr></pre>";
				die();
			}
			if (sizeof($fldArr) !== sizeof($valArr)){
				echo "<pre>\n<h1>Problem In 'FIELD AND VALUE ARRAY'!</h1>Fields and Values arrays size must be same.<hr></pre>";
				die();
			}
			$sql = "UPDATE $tbl SET ";
			for ($i=0; $i<sizeof($fldArr); $i++){
				$sql .= "".$fldArr[$i]." = ";
				$sql .= ($valArr[$i]!='now()') ? "'".$valArr[$i]."', " : $valArr[$i] . ", ";
			}
			$sql = $this->getSubString($sql, 0, strlen($sql)-2);
			if(!empty($fldName) && !empty($value)){
				$sql .= " WHERE $fldName = '$value'";
			}
			return $sql;
		}


		/**
		 *	function getSelectSql returns sql query for Getting
		 *	record(s) from database table
		 *	$tbl		:	table name for operation
		 *	@fldArr	:	name of database fields
		 *	@valArr	:	value for each database field which you
		 *						defined in @fldArr
		 */

		function getSelectSql($tbl, $fldArr, $fldName="", $value=""){
			$sql = "SELECT ";
			for ($i=0; $i<sizeof($fldArr); $i++){
				$sql .= "".$fldArr[$i]." , ";
			}
			$sql = $this->getSubString($sql, 0, strlen($sql)-2);
			$sql .= "from $tbl";
			if(!empty($fldName) && !empty($value)){
				$sql .= " WHERE $fldName = '$value'";
			}
			
			return $sql;
		}

		/**
		 *	error function shows database operation errors
		 *	$message	:	message to be show
		 *	$details	:	error message details
		 */

		function error($message, $sql){
			$no = mysql_errno();
			$msg = mysql_error();
                        $sql = strip_tags(addslashes($sql));
                        $msg = strip_tags(addslashes($msg));
                        mysql_query("ROLLBACK");
                       // $query = "INSERT INTO ORS_error_log (query,error,pagename,user_id,date)
                       //     values('".$sql."','".($no."-".$msg)."','".$_SERVER['SCRIPT_FILENAME']."','".$_SESSION['ucd']."',now())";
                       // $this->insert($query);
                        //$res = mysql_query($query) or die(mysql_error());
                        //echo mysql_insert_id();
			/*$err_msg = "<pre>";
			$err_msg .="<h1>$message</h1>";
			$err_msg .="$details<br><br>";
			$err_msg .="<b>Error Number \t:</b> $no<br>";
			$err_msg .="<b>Error Message     :</b> $msg<br>";
			$err_msg .="<b>File Name :</b> ".__FILE__."<br><br>";
			$err_msg .="<hr></pre>";
			return $err_msg;*/
                      // header("Location:../common/sesexpire.php");
                     //   die;
		}
		
		/*function error($message, $details="") {
		    $no = mysql_errno();
		}*/

		/**
		 *	destruct class for closing mysql
		 */


		/**
		*	returns a part of string from start position
		*	to end position
		*	$str		:	string to be proceed
		*	$start	:	start position of string where from you
		*						want to get starting of string
		*	$end		:	end position of string
		*/

		function getSubString($str, $start, $end){
			return substr($str, $start, $end);
		}

		/**
		 *	EXECUTE ALL SQL QUERIES/STATEMENTS FROM THIS FUNCTION
		 *	$sql	:	YOUR SQL QUERY TO BE EXECUTE ONLY FOR RESULTHOTEL.PHP
		 */

		function exec_sql_hotel($sql,$dbconn){
			if (is_null($sql) || empty($sql)){
				echo $this->error("Problem In SQL Query!","Your sql query seems wrong. Please check your SQL query.");
				die();
			}
			/*
			if(!is_resource($dbconn)){
				return false;
			}
			*/
			$result = mysql_query($sql,$dbconn) or die($this->error("Problem In Query Execution!", ""));
			if($result){
				$count  = 0;
				while($row = mysql_fetch_array($result, MYSQL_ASSOC)){
					$data[$count] = $row;
					$count++;
				}
				mysql_free_result($result);
				return $data;
			}else{
				return false;
			}//if
		}

		function exec_sql_hotelnum($sql,$dbconn){
			if (is_null($sql) || empty($sql)){
				echo $this->error("Problem In SQL Query!","Your sql query seems wrong. Please check your SQL query.");
				die();
			}
			/*
			if(!is_resource($dbconn)){
				return false;
			}
			*/
			$result = mysql_query($sql,$dbconn) or die($this->error("Problem In Query Execution!", ""));
			if($result){
				$count = 0;
				$data = mysql_num_rows($result);
				mysql_free_result($result);
				return $data;
			}else{
				return false;
			}//if
		}
	}//end of class
?>
