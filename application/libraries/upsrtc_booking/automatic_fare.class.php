<?php

class  Automatic_fare
{
	
	var $url;
	var $CI;

	public function __construct(){
           $this->ci  = & get_instance();
           $this->ci->load->library("Nusoap_lib");
           error_reporting(0);
           $this->ci->load->helper('soap_data');
           $this->ci->load->model(array('soap_client_model'));
           $this->xmlns = "http://etim.trimax.com/";
           //$this->url = "http://180.92.171.140:8080/SERVICE/FetchData?wsdl";
          // $this->url = "http://10.20.41.142:8080/StateFaresCalc/FaresCalc?wsdl";

            $this->url="http://10.20.41.242:8080/SERVICE/FetchData?wsdl";
        //    $this->url="http://180.92.171.94:8080/SERVICE/FetchData?wsdl";
	}

        
        
    function getAutomaticFare($parent_state,$route_no,$from_stop,$till_stop,$from_stop_seq,$till_stop_seq,$depart_date,$bus_type_cd) {
       
            $xml_str = <<<BOD1
  <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="{$this->xmlns}">
   <soapenv:Body>
    <ser:getInterStateFares>
        <parent_state>{$parent_state}</parent_state>
        <route_no>{$route_no}</route_no>
        <from_stop>{$from_stop}</from_stop>
        <till_stop>{$till_stop}</till_stop>
        <from_stop_seq>{$from_stop_seq}</from_stop_seq>
        <till_stop_seq>{$till_stop_seq}</till_stop_seq>
        <depart_date>{$depart_date}</depart_date>
        <bus_type_cd>{$bus_type_cd}</bus_type_cd>
     </ser:getInterStateFares>
   </soapenv:Body>
</soapenv:Envelope>
BOD1;
        $CI = & get_instance();    
        $CI->load->library("Nusoap_lib");

        $client = new nusoap_client($this->url,true);

        $func='getInterStateFares';
        $client->operation = $func;
       return $result = $client->send($xml_str,$func);
    
  
         
    }
     
    function getCityFare($route_no,$from_stop_seq,$till_stop_seq,$depart_date,$bus_type_cd,$fare_type,$route_type) {

             $soapaction = $url . "getFares"; 
           
            $mysoapmsg = <<<BOD1
  <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="{$this->xmlns}">
   <soapenv:Body>
    <ser:getFares>
        <route_no>{$route_no}</route_no>
        <fromStopSeq>{$from_stop_seq}</fromStopSeq>
        <tillStopSeq>{$till_stop_seq}</tillStopSeq>
        <depart_date>{$depart_date}</depart_date>
        <bus_type_cd>{$bus_type_cd}</bus_type_cd>
        <fare_type>{$fare_type}</fare_type>
        <route_type>{$route_type}</route_type>
        
     </ser:getFares>
   </soapenv:Body>
</soapenv:Envelope>
BOD1;
        return $data = get_soap_data($this->url, 'getFares', $mysoapmsg);
   
       
            
    }
}


?>