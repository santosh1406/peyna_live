<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Rediscache
{
    /**
     * Global container variables for chained argument results
     *
     */
    private $ci;
    public $is_suppported;
    

    public function __construct($config = array())
    {
        $this->ci =& get_instance();
        $this->ci->load->driver('cache');

        $this->init();
    }

    public function init(){
        $this->supported();
    }

    public function get($key)
    {
        $result = false;
        if($this->is_suppported)
        {
            $result = $this->ci->cache->redis->get($key);
            
        }
        return $result;
    }

    public function set($key, $data, $ttl = 0, $flag = true)
    {
        $result = false;

        // dont allow if resource

        if($this->is_suppported)
        {
            $result = $this->ci->cache->redis->save($key, $data, $ttl);
        }

        return $result;
    }

    public function delete($key)
    {
        return $this->ci->cache->redis->delete($key);
    }

    public function clean()
    {
        return $this->ci->cache->redis->clean();
    }

    public function info()
    {
        return $this->ci->cache->redis->cache_info();
    }

    public function supported()
    {
        $this->is_suppported = $this->ci->cache->redis->is_supported();
        return $this->is_suppported;
    }


}
