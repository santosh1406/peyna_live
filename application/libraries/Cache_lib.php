<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cache_lib
{
    /**
     * Global container variables for chained argument results
     *
     */
    private $ci;
    private $is_suppported;



	public function __construct()
    {
        $this->ci =& get_instance();

        $this->ci->load->driver('cache', array('adapter' => 'memcached'));
        $this->supported();
    }

    public function get($key)
    {
    	$result = false;
    	if($this->is_suppported)
    	{
	    	$result = $this->ci->cache->memcached->get($key);
	        
	    }
	    return $result;
    }

    public function set($key, $data, $ttl = 120, $flag = true)
    {
    	$result = false;
    	if($this->is_suppported)
    	{
    		$result = $this->ci->cache->memcached->save($key, $data, $ttl);
    	}

    	return $result;
    }

    public function delete($key)
    {
    	return $this->ci->cache->memcached->delete($key);
    }

    public function clean()
    {
    	return $this->ci->cache->memcached->clean();
    }

    public function info()
    {
    	return $this->ci->cache->memcached->cache_info();
    }

    public function supported()
    {
    	$this->is_suppported = $this->ci->cache->memcached->is_supported();
    	return $this->is_suppported;
    }


}
