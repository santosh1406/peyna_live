<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Rsrtc_api extends CI_Model {
	var $user_id;
	var $pass;
	var $url;
	var $user_type;
  var $op_id;
  var $dep_date;
  var $ci;

  public function __construct() {
    parent::__construct();

    ini_set('max_execution_time', 3000);
    ini_set("memory_limit",-1);


    $this->load->helper('soap_data');
    $this->load->model(array('soap_model/rsrtc_model'));
    
    // rsrtc_WSDL_URL  
    $this->user_id    = $this->config->item("rsrtc_wsdl_user");
    $this->pass       = $this->config->item("rsrtc_wsdl_password");
    $this->url        = $this->config->item("rsrtc_wsdl_url");
    $this->op_id      = $this->config->item("rsrtc_operator_id");
    $this->user_type  = 1;
          
    $this->dep_date       = $this->dep_date && $this->dep_date !=''? $this->dep_date : date('Y-m-d');
    $this->dep_date       = date('d/m/Y',strtotime($this->dep_date.' +1 days'));
    
    // show($this->dep_date,1);
  }

  public function index()
  {
    // echo 'class loaded';
  }

  public function seatAvailability($all_det){
    $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.rsrtc.trimax.com/">
                     <soapenv:Header/>
                     <soapenv:Body>
                        <ser:getSeatAvailability>
                           <!--Optional:-->
                           <SeatAvailabilityRequest>
                              <!--Optional:-->
                              <authentication>
                                 <!--Optional:-->
                                 <userName>'.$this->user_id.'</userName>
                                 <!--Optional:-->
                                 <password>'.$this->pass.'</password>
                                 <!--Optional:-->
                                 <userType>'.$all_det['user_type'].'</userType>
                              </authentication>
                              <serviceId>'.$all_det['service_id'].'</serviceId>
                              <!--Optional:-->
                              <fromStopId>'.$all_det['from_stop'].'</fromStopId>
                              <!--Optional:-->
                              <toStopId>'.$all_det['to_stop'].'</toStopId>
                              <!--Optional:-->
                              <dateOfJourney>'.$all_det['date'].'</dateOfJourney>
                           </SeatAvailabilityRequest>
                        </ser:getSeatAvailability>
                     </soapenv:Body>
                </soapenv:Envelope>';

    logging_data("ors/rsrtc/".date("F")."/ors_".date("d-m-Y")."_log.log",$xml_str, 'Seat availablity');
    $data = get_soap_data($this->url, 'getSeatAvailability', $xml_str);
    logging_data("ors/rsrtc/".date("F")."/ors_".date("d-m-Y")."_log.log",$data, 'Seat availablity');
    
    if (isset($data['SeatAvailabilityResponse']) && !isset($data['SeatAvailabilityResponse']['availableSeats'])) {
      $custom_error_mail = array(
                                  "custom_error_subject" => "RSRTC Seat layout Error from rsrtc.",
                                  "custom_error_message" => "Request - ".$xml_str."<br/></br/> Response - ".json_encode($data)
                                );
      $this->common_model->management_custom_error_mail($custom_error_mail,"rsrtc");
    }

    return $data;
  }

  public function temporary_booking($data = array()) { 
    $pass = $data['passenger'];
    $date = date('d/m/Y',strtotime($data['date']));

    $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.rsrtc.trimax.com/">
                        <soapenv:Header/>
                        <soapenv:Body>
                           <ser:doTemporaryBooking>
                              <!--Optional:-->
                              <TentativeBookingRequest>
                                 <!--Optional:-->
                                 <authentication>
                                    <!--Optional:-->
                                    <userName>'.$this->user_id.'</userName>
                                    <!--Optional:-->
                                    <password>'.$this->pass.'</password>
                                    <!--Optional:-->
                                    <userType>'.$this->user_type.'</userType>
                                 </authentication>
                                 <serviceId>'.$data['service_id'].'</serviceId>
                                 <!--Optional:-->
                                 <fromBusStopId>'.$data['from_stop_id'].'</fromBusStopId>
                                 <!--Optional:-->
                                 <toBusStopId>'.$data['to_stop_id'].'</toBusStopId>
                                 <!--Optional:-->
                                 <dateOfJourney>'.$date.'</dateOfJourney>
                                 <!--Optional:-->
                                 <passengerDetails>
                                 <!--Zero or more repetitions:-->
                                 ';

        foreach ($pass as $key => $value) {
        $xml_str .=                 '<passenger>
                                       <!--Optional:-->
                                       <passengerName>'.$value['name'].'</passengerName>
                                       <age>'.$value['age'].'</age>
                                       <!--Optional:-->
                                       <sex>'.$value['gender'].'</sex>
                                       <!--Optional:-->
                                       <contactNumber>'.$data['mobile_no'].'</contactNumber>
                                       <!--Optional:-->
                                       <email>'.$data['email'].'</email>
                                       <seatNo>'.$value['seat_no'].'</seatNo>
                                       <totalFare>'.$value['fare'].'</totalFare>
                                       <!--Optional:-->';
                                       if($value['gender'] == 'F')
                                       {
                                          $xml_str .= '<concession>MCT</concession>';
                                       }
                                       else
                                       {
                                          $xml_str .= '<concession>?</concession>';
                                       }
                                       
                    $xml_str .=       '<!--Optional:-->
                                       <concessionProof>?</concessionProof>
                                       <!--Optional:-->
                                       <seatType>?</seatType>
                                       <!--Optional:-->
                                       <fareDetail>
                                          <acc>?</acc>
                                          <adultFare>?</adultFare>
                                          <childFare>?</childFare>
                                          <!--Optional:-->
                                          <discount>
                                             <busServiceNo>?</busServiceNo>
                                             <concessionRate>?</concessionRate>
                                             <!--Optional:-->
                                             <discountType>?</discountType>
                                             <!--Optional:-->
                                             <fromStopCode>?</fromStopCode>
                                             <!--Optional:-->
                                             <isApplicable>?</isApplicable>
                                             <maxNumSeats>?</maxNumSeats>
                                             <!--Optional:-->
                                             <tillStopCode>?</tillStopCode>
                                          </discount>
                                          <hr>?</hr>
                                            <interstate>?</interstate>
                                            <it>?</it>
                                          <octroi>?</octroi>
                                          <other>?</other>
                                          <reservationCharge>?</reservationCharge>
                                          <sleeperCharge>?</sleeperCharge>
                                          <toll>?</toll>
                                       </fareDetail>
                                    </passenger>';
        }

    $xml_str .=                          '</passengerDetails>
                              </TentativeBookingRequest>
                           </ser:doTemporaryBooking>
                        </soapenv:Body>
                     </soapenv:Envelope>';

    logging_data("ors/rsrtc/".date("F")."/ors_".date("d-m-Y")."_log.log",$xml_str,'temp booking xml');
    $response = get_soap_data($this->url, 'doTemporaryBooking', $xml_str);
    logging_data("ors/rsrtc/".date("F")."/ors_".date("d-m-Y")."_log.log",$response, 'temp booking Response');

    $data_to_log = [
                    "ticket_id" => isset($data["ticket_id"]) ? $data["ticket_id"] : "",
                    "request" => $xml_str,
                    "response" => json_encode($response),
                    "type" => "temp_booking"
                   ];

    insert_request_response_log($data_to_log);

    if(isset($response['TentativeBookingResponse']) ) {
    	return $response['TentativeBookingResponse'];
    }
    else {
    	return $response['TentativeBookingResponse'];
    }
	}

	public function doconfirm_booking($data = array()) {	
		$xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.rsrtc.trimax.com/">
					   <soapenv:Header/>
					   <soapenv:Body>
					      <ser:doConfirmBooking>
					         <!--Optional:-->
					         <ConfirmBookingRequest>
					            <!--Optional:-->
					            <authentication>
					               <!--Optional:-->
					               <userName>'.$this->user_id.'</userName>
                                    <!--Optional:-->
                                    <password>'.$this->pass.'</password>
                                    <!--Optional:-->
                                    <userType>'.$this->user_type.'</userType>
					            </authentication>
					            <!--Optional:-->
					            <uniqueTransactionId>'.$data['ticket_id'].'</uniqueTransactionId>
					            <!--Optional:-->
					            <billDeskResponse>?</billDeskResponse>
					            <!--Optional:-->
					            <blockKey>'.$data['ticket_ref_no'].'</blockKey>
					            <seatCount>'.$data['seat_count'].'</seatCount>
					         </ConfirmBookingRequest>
					      </ser:doConfirmBooking>
					   </soapenv:Body>
					     </soapenv:Envelope>';

    logging_data("ors/rsrtc/".date("F")."/ors_".date("d-m-Y")."_log.log",$xml_str, 'confirm booking xml');
    $response = get_soap_data($this->url, 'doTemporaryBooking', $xml_str);
    logging_data("ors/rsrtc/".date("F")."/ors_".date("d-m-Y")."_log.log",$response, 'confirm booking response');

    $data_to_log = [
                    "ticket_id" => isset($data["ticket_id"]) ? $data["ticket_id"] : "",
                    "request" => $xml_str,
                    "type" => "confirm_booking",
                    "response" => json_encode($response),
                   ];

    insert_request_response_log($data_to_log);
    if(isset($response['ConfirmBookingResponse']) && $response['ConfirmBookingResponse']['status'] == 'SUCCESS') {
      return $response['ConfirmBookingResponse'];
    }
    else {
      return $response['ConfirmBookingResponse'];
    }
  }
    
  public function cancel_ticket($data) {
    $xml_str='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.rsrtc.trimax.com/">                        <soapenv:Header/>
            <soapenv:Body>
            <ser:cancelTicket>
                  <!--Optional:-->
                  <CancelRequest>
                     <!--Optional:-->
                     <authentication>
                        <!--Optional:-->
                       <userName>'.$this->user_id.'</userName>
                        <!--Optional:-->
                        <password>'.$this->pass.'</password>
                        <!--Optional:-->
                        <userType>'.$this->user_type.'</userType>
                     </authentication>
                     <!--Optional:-->
                     <pnr>'.$data['pnr_no'].'</pnr>
                     <!--Optional:-->
                     <seatNumbers></seatNumbers>
                  </CancelRequest>
               </ser:cancelTicket>
            </soapenv:Body>
              </soapenv:Envelope>';

    logging_data("ors/rsrtc/".date("F")."/cancel_".date("d-m-Y")."_log.log",$xml_str, 'Cancel Ticket Request');
    $response = get_soap_data($this->url, 'cancelTicket', $xml_str);
    logging_data("ors/rsrtc/".date("F")."/cancel_".date("d-m-Y")."_log.log",$response, 'Cancel Ticket Response');

    $data_to_log = [
                    "ticket_id" => isset($data['current_ticket_information']["ticket_id"]) ? $data['current_ticket_information']["ticket_id"] : "",
                    "request" => $xml_str,
                    "type" => "ticket_cancel",
                    "response" => json_encode($response),
                   ];

    insert_request_response_log($data_to_log);
    if(isset($response['CancelResponse']) && $response['CancelResponse']['status'] == 'SUCCESS') {
      return $response['CancelResponse'];
    }
    else {
      return $response['CancelResponse'];
    }
  }

  function getAvailableServices()
  {
    $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.rsrtc.trimax.com/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <ser:getAvailableServices>
                         <!--Optional:-->
                         <AvailableServiceRequest>
                            <!--Optional:-->
                            <authentication>
                               <!--Optional:-->
                               <userName>'.$this->user_id.'</userName>
                               <!--Optional:-->
                               <password>'.$this->pass.'</password>
                               <!--Optional:-->
                               <userType>'.$this->user_type.'</userType>
                            </authentication>
                            <!--Optional:-->
                            <dateOfJourney>'.$this->dep_date.'</dateOfJourney>
                         </AvailableServiceRequest>
                      </ser:getAvailableServices>
                   </soapenv:Body>
                </soapenv:Envelope>';

    $response = get_soap_data($this->url,  'getAvailableServices', $xml_str);
    $this->db->trans_begin();
    $data = $this->rsrtc_model->insert_available_services($response, $this->op_id,$this);
    
    if ($this->db->trans_status() === FALSE) {
        show(error_get_last());
        $this->db->trans_rollback();
    }
    else {
      $this->db->trans_commit();
    } 
    show($data,'', ' '.$this->dep_date. ' getAvailableServices');
  }

  function GetAllBusTypes() {
    $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.rsrtc.trimax.com/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <ser:GetAllBusTypes>
                         <!--Optional:-->
                         <AllBusTypeRequest>
                            <!--Optional:-->
                            <authentication>
                               <!--Optional:-->
                               <userName>'.$this->user_id.'</userName>
                               <!--Optional:-->
                               <password>'.$this->pass.'</password>
                               <!--Optional:-->
                               <userType>'.$this->user_type.'</userType>
                            </authentication>
                         </AllBusTypeRequest>
                      </ser:GetAllBusTypes>
                   </soapenv:Body>
                </soapenv:Envelope>';
    
    $response   = get_soap_data($this->url,  'GetAllBusTypes', $xml_str);
    $seat_layout_id = $this->rsrtc_model->insert_bus_type($response, $this->op_id);
    
    return $seat_layout_id;
    show($response, '', 'response');
    show($data,1, 'GetAllBusTypes');
  }

  function getAllBusFormats($bus_format_cd) {
    $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.rsrtc.trimax.com/">
                     <soapenv:Header/>
                     <soapenv:Body>
                        <ser:getAllBusFormats>
                           <!--Optional:-->
                           <AllBusFormatRequest>
                              <!--Optional:-->
                              <authentication>
                                 <!--Optional:-->
                                 <userName>'.$this->user_id.'</userName>
                                 <!--Optional:-->
                                 <password>'.$this->pass.'</password>
                                 <!--Optional:-->
                                 <userType>'.$this->user_type.'</userType>
                              </authentication>
                              <!--Optional:-->
                              <busFormatType>'.$bus_format_cd.'</busFormatType>
                           </AllBusFormatRequest>
                        </ser:getAllBusFormats>
                     </soapenv:Body>
                </soapenv:Envelope>';
    $response = get_soap_data($this->url,  'getAllBusFormats', $xml_str, $bus_format_cd);
    
    if(count($response)>0) {
        $seat_layout_id = $this->rsrtc_model->insert_bus_format($response, $this->op_id,$bus_format_cd);
    }
    else {
        show($response,'','No Response');
        return '';
    }
    return $seat_layout_id;
  }

  function getBoardingStops($data =array(), $ali_type) {
    $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.rsrtc.trimax.com/">
                     <soapenv:Header/>
                     <soapenv:Body>
                        <ser:getBoardingStops>
                           <!--Optional:-->
                           <GetBoardingStopsRequest>
                              <!--Optional:-->
                              <authentication>
                                 <!--Optional:-->
                                 <userName>'.$this->user_id.'</userName>
                                 <!--Optional:-->
                                 <password>'.$this->pass.'</password>
                                 <!--Optional:-->
                                 <userType>'.$this->user_type.'</userType>
                              </authentication>
                              <serviceId>'.$data['service_id'].'</serviceId>
                              <!--Optional:-->
                              <fromStopId>'.$data['from'].'</fromStopId>
                              <!--Optional:-->
                              <toStopId>'.$data['to'].'</toStopId>
                              <!--Optional:-->
                              <typeAliBrd>'.$ali_type.'</typeAliBrd>
                              <!--Optional:-->
                              <busTypeCode>'.$data['bus_type_code'].'</busTypeCode>
                           </GetBoardingStopsRequest>
                        </ser:getBoardingStops>
                     </soapenv:Body>
                  </soapenv:Envelope>';
    $response = get_soap_data($this->url,  'getBoardingStops', $xml_str);
    $stops = array();
    if( isset($response['GetBoardingStopsResponse'])  && count($response['GetBoardingStopsResponse']) >0 && ( isset($response['GetBoardingStopsResponse']['boardingStopDetails']) || isset($response['GetBoardingStopsResponse']['alightingStopDetails']))) {
      if($ali_type == 'BRD') {
        if(ENVIRONMENT == 'production') {
          $stop_code =  $data['from'];
          if( isset($response['GetBoardingStopsResponse']['boardingStopDetails']['boardingStopDetail']) && count($response['GetBoardingStopsResponse']['boardingStopDetails']) >0  &&  count($response['GetBoardingStopsResponse']['boardingStopDetails']['boardingStopDetail']) >0) {
            $stops = $response['GetBoardingStopsResponse']['boardingStopDetails']['boardingStopDetail'];
            
            if(isset($stops['busStopName'])) {
                $stops = array($stops);
            }
          }
        }
        else {
          $stop_code =  $data['to'];
          if( isset($response['GetBoardingStopsResponse']['alightingStopDetails']['alightingStopDetail']) && count($response['GetBoardingStopsResponse']['alightingStopDetails']) >0  &&  count($response['GetBoardingStopsResponse']['alightingStopDetails']['alightingStopDetail']) >0) {
            
            $stops = $response['GetBoardingStopsResponse']['alightingStopDetails']['alightingStopDetail'];
            
            if(isset($stops['busStopName'])) {
                $stops = array($stops);
            }
          }
        }
        if(count($stops)>0) {
          $this->rsrtc_model->insert_boarding_stops($stops, $ali_type, $stop_code, $this->op_id, $data);
        }
        $this->getBoardingStops($data, 'ALI');
      }
      else if($ali_type == 'ALI') {
        if(ENVIRONMENT == 'production') {
          $stop_code =  $data['to'];
          if(isset($response['GetBoardingStopsResponse']['alightingStopDetails']['alightingStopDetail']) && count($response['GetBoardingStopsResponse']['alightingStopDetails']) >0  &&  count($response['GetBoardingStopsResponse']['alightingStopDetails']['alightingStopDetail']) >0) {
            $stops = $response['GetBoardingStopsResponse']['alightingStopDetails']['alightingStopDetail'];

            if(isset($stops['busStopName'])) {
                $stops = array($stops);
            }
          }
        }
        else {
          $stop_code =  $data['from'];
          if(isset($response['GetBoardingStopsResponse']['boardingStopDetails']['boardingStopDetail']) && count($response['GetBoardingStopsResponse']['boardingStopDetails']) >0  &&  count($response['GetBoardingStopsResponse']['boardingStopDetails']['boardingStopDetail']) >0) {
            $stops = $response['GetBoardingStopsResponse']['boardingStopDetails']['boardingStopDetail'];

            if(isset($stops['busStopName'])) {
                $stops = array($stops);
            }
          }
        }
        if(count($stops)>0) {
          $this->rsrtc_model->insert_boarding_stops($stops, $ali_type, $stop_code, $this->op_id, $data);
        }
      }   
    }
    else {
      show($response,'','getBoardingStops----error');
    }
    return $response;
  }

  function getBusServiceStops($service_id, $trip_no = '', $op_bus_type = '', $service_data) {
    $xml_str  =  '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.rsrtc.trimax.com/">
                     <soapenv:Header/>
                     <soapenv:Body>
                        <ser:getBusServiceStops>
                           <!--Optional:-->
                           <BusServiceStopsRequest>
                              <!--Optional:-->
                              <authentication>
                                 <!--Optional:-->
                                 <userName>'.$this->user_id.'</userName>
                                 <!--Optional:-->
                                 <password>'.$this->pass.'</password>
                                 <!--Optional:-->
                                 <userType>'.$this->user_type.'</userType>
                              </authentication>
                              <serviceId>'.$service_id.'</serviceId>
                              <!--Optional:-->
                              <dateOfJourney>'.$this->dep_date.'</dateOfJourney>
                           </BusServiceStopsRequest>
                        </ser:getBusServiceStops>
                     </soapenv:Body>
                  </soapenv:Envelope>';
    
    $response   = get_soap_data($this->url,  'getBusServiceStops', $xml_str);

    $bstop                     = array();
    $bstop['service_id']       = $service_id;
    $bstop['trip_no']          = $trip_no;
    $bstop['op_bus_type']      = $op_bus_type;

    $this->db->trans_begin();
    $data       = $this->rsrtc_model->insert_bus_service_stops($response,$this->op_id, $this->dep_date, $this, $bstop, $service_data);
        
    if ($this->db->trans_status() === FALSE) {
        show(error_get_last());
        $this->db->trans_rollback();
    }
    else {
      $this->db->trans_commit();
    }
  } 

  /******************************* FOR BOOKING PROCESS START *****************************/
  public function get_seat_layout($input)
  {
    $seat_service_tax_amt = 0.00;
    $seat_service_tax_perc = 0.00;
    $op_service_charge_amt = 0.00;
    $op_service_charge_perc = 0.00;
    $current_seat_discount = 0.00;

    $response = array();
    $trip_data_array = array('from' => $input['from'],
                             'to' => $input['to'],
                             'date' => $input['date'],
                             'trip_no' => $input['trip_no']
                            );

    //We can easily reduce this get_search query here. Think it on later
    /*$trip_data      = $this->travel_destinations_model->get_search($trip_data_array);*/
    $trip_data      = $this->trip_master_model->where(["op_id" => $this->op_id,"trip_no" => $input['trip_no']])->find_all();

    if($trip_data) {
      $trip_data = $trip_data[0];
      $from_data   = $this->bus_stop_master_model->get_stops_detail($input['from'],$input['op_id']);
      $to_data   = $this->bus_stop_master_model->get_stops_detail($input['to'],$input['op_id']);

      if(count($from_data) > 0 && count($to_data) > 0) {
        $all_det['from_stop']           = $from_data[0]['op_stop_cd'];
        $all_det['bus_from_stop_id']    = $from_data[0]['bus_stop_id'];

        $all_det['bus_to_stop_id']      = $to_data[0]['bus_stop_id'];
        $all_det['to_stop']             = $to_data[0]['op_stop_cd'];
        $all_det['user_type']           = $this->user_type;
        $all_det['service_id']          = $trip_data->op_trip_no;
        $all_det['date']                = date('d/m/Y', strtotime($input['date']));
        $soap_name                      = strtolower($input['op_name'].'_api');
          
        $data = $this->seatAvailability($all_det);

        $data['error'] = array();
        if (isset($data['SeatAvailabilityResponse']) && isset($data['SeatAvailabilityResponse']['availableSeats']) && count($data['SeatAvailabilityResponse']['availableSeats']) > 0)  {
          if (isset($data['SeatAvailabilityResponse']['serviceId']) && count($data['SeatAvailabilityResponse']['serviceId']) > 0) {
            $op_trip_no = $data['SeatAvailabilityResponse']['serviceId'];
            $tot_fare = $data['SeatAvailabilityResponse']['fare'];
            $child_seat_fare_diffrance = $data['SeatAvailabilityResponse']['fare'] - $data['SeatAvailabilityResponse']['childFare'];

            $tot_seats = count($data['SeatAvailabilityResponse']['availableSeats']['availableSeat']);
            $update_travel_data = array();
            $update_travel_data['trip_no'] = $input['trip_no'];
            $update_travel_data['bus_from_stop_id'] = "";
            $update_travel_data['bus_to_stop_id'] = "";
            $update_travel_data['fare'] = $tot_fare;
            $update_travel_data['search_date'] = $input['date'];
            $update_travel_data['tot_seat_available'] = $tot_seats;
            $update_travel_data['tot_seats'] = $tot_seats;
            $update_travel_data['from'] = $input['from'];
            $update_travel_data['to'] = $input['to'];
            $update_travel_data['child_seat_fare'] =  isset($data['SeatAvailabilityResponse']['childFare']) ? $data['SeatAvailabilityResponse']['childFare'] : 0.00;
            $update_travel_data['senior_citizen_fare'] =  isset($data['SeatAvailabilityResponse']['seniorcitizenfare']) ? $data['SeatAvailabilityResponse']['seniorcitizenfare'] : 0.00;
            
            $update_travel_destn = $this->travel_destinations_model->update_fare_totseats($update_travel_data);
            $tot_data_seat_layout = $this->op_seat_layout_model->get_seat_layout($trip_data->seat_layout_id);
            $tot_rows = $tot_data_seat_layout[0]->no_rows;
            $tot_col = $tot_data_seat_layout[0]->no_cols;
            $double_tot_rows = $tot_data_seat_layout[0]->no_rows2;
            $double_tot_col = $tot_data_seat_layout[0]->no_cols2;
            $seat_type = $tot_data_seat_layout[0]->seat_type;
            $seat_layout = array();
            $available_seats_response = $data['SeatAvailabilityResponse']['availableSeats']['availableSeat'];
            if(!is_array($data['SeatAvailabilityResponse']['availableSeats']['availableSeat'])) {
                $available_seats_response = array($data['SeatAvailabilityResponse']['availableSeats']['availableSeat']);
            }
                  
            $seat_fare_detail = array();
            $total_available_seats = 0;
            $extra_array = [
                              "op_bus_type_cd" => $input['op_bus_type_cd'],
                        ];
            $fare_with_conveycharge = getConveyCharges($tot_fare, "rsrtc",$extra_array);

            $current_actual_seat_value = $fare_with_conveycharge;
            $current_actual_seat_value_with_tax = $fare_with_conveycharge;

            $basic_fare_array = array($fare_with_conveycharge);
            $fare_array = array($fare_with_conveycharge);

            $response['actual_fare'] = implode("/",array_filter($basic_fare_array));
            $response['actual_fare_array'] = implode("/",array_filter($fare_array));
            
            foreach ($tot_data_seat_layout as $key => $value) {
                      
              /******************* Layout Parameteres & Description Start******************/
              $current_seat_type = "";
              $horizontal_seat = false;
              $vertical_seat = false;

              if(strtolower($value->quota_type) == "ladies") {
                $current_seat_type = "ladies";
              }
              /******************* Layout Parameteres & Description End********************/

              $sleeperCharge = 0;
              $pos = $value->row_no . "X" . $value->col_no;
              $birth_no = $value->berth_no;
              if($value->seat_type == 'Sl') {
                $sleeperCharge = isset($data['SeatAvailabilityResponse']['sleeperFare']) ? $data['SeatAvailabilityResponse']['sleeperFare'] : 0.00;
                $horizontal_seat = true;
                $current_seat_type = "sleeper";
              }
              $seat_layout[$birth_no][$pos] = array(
                  'seat_no' => $value->seat_no,
                  'actual_seat_fare' => $current_actual_seat_value_with_tax + $sleeperCharge,
                  'actual_seat_basic_fare' => $current_actual_seat_value + $sleeperCharge,
                  'seat_fare' => $fare_with_conveycharge + $sleeperCharge,
                  'seat_basic_fare' => $fare_with_conveycharge + $sleeperCharge,
                  'seat_service_tax_amt' => $seat_service_tax_amt,
                  'seat_service_tax_perc' => $seat_service_tax_perc,
                  'op_service_charge_amt' => $op_service_charge_amt,
                  'op_service_charge_perc' => $op_service_charge_perc,
                  'row_no' => $value->row_no,
                  'col_no' => $value->col_no,
                  'seat_type' => $value->seat_type,
                  'berth_no' => $birth_no,
                  'quota' => strtolower($value->quota_type),
                  // 'quota' => "ladies",
                  'pos' => $pos,
                  'current_seat_type' => $current_seat_type,
                  'horizontal_seat' => $horizontal_seat,
                  'vertical_seat' => $vertical_seat,
                  'seat_discount' => $current_seat_discount,
                  'agent_commission' => 0,
                  'agent_comm_tds' =>  0,
                   'child_seat_fare' => $fare_with_conveycharge - $child_seat_fare_diffrance
              ); 

              if(!empty($available_seats_response) && in_array($value->seat_no, $available_seats_response) && in_array(strtolower($value->quota_type), array("general","ladies"))) {
                  $seat_layout[$birth_no][$pos]["available"] = 'Y';
                  $total_available_seats++;
              } 
              else {
                  $seat_layout[$birth_no][$pos]["available"] = 'N';
              }
                
              if($seat_layout[$birth_no][$pos]["available"] == 'N' && strtolower($value->quota_type) == "ladies") {
                  $seat_layout[$birth_no][$pos]["booked_by"] = 'F';
              }

              $seat_fare_detail[$birth_no][$value->seat_no] = $seat_layout[$birth_no][$pos];
            }
          }
              
          $response['fare'] = implode("/",array_filter($basic_fare_array));
          $response['fare_array'] = implode("/",array_filter($fare_array));
          $response['total_seats'] = $tot_seats;
          $response['total_available_seats'] = $total_available_seats;
          $response['available_seats'] = $data['SeatAvailabilityResponse']['availableSeats']['availableSeat'];
          $response['tot_rows'] = $tot_rows;
          $response['tot_cols'] = $tot_col;
          $response['tot_berth'] = 1; //Only One Berth IN msrtc. Please Discuss this.
          $response['seat_layout'] = $seat_layout;
          $response['seat_fare_detail'] = $seat_fare_detail;
          $response['update_travel_destn'] = $update_travel_destn;
          $response['flag'] = '@#success#@';
        }
        else {
          $number_of_seat_available = "";
          if(isset($data['SeatAvailabilityResponse']['serviceError']["errorReason"])) {
            if(stripos($data['SeatAvailabilityResponse']['serviceError']["errorReason"],"No seat available") !== false)
            {
              $number_of_seat_available = 0;
            }
          }
          $response['flag'] = '@#error#@';
          $response['error'] = isset($data['SeatAvailabilityResponse']['serviceError']) ? $data['SeatAvailabilityResponse']['serviceError'] : "";
          $response['total_available_seats'] =  $number_of_seat_available;
          $response['fare'] = 0;
        }
      }
      else{
           $response['fare'] = 0;    
      }
    }
    else {
      $custom_error_mail = array(
                                  "custom_error_subject" => "RSRTC Seat layout not fetched.",
                                  "custom_error_message" => "We have to fetch service again for following data .".json_encode($trip_data_array)
                                );

      $this->common_model->management_custom_error_mail($custom_error_mail,"management");

      $response['flag'] = '@#error#@';
      $response['error'] = "Seat Not available for given date.";
    }   
    $response['trip_no'] = $input['trip_no'];
    return $response;
  }

  /*
  *Author : Suraj Rathod
  *Function : getSeatAvailability() //seatAvailability() seatAvailabilityDetail() 
  *Detail : To get seat availability and other details like fare and total seats available
  */
  public function getSeatAvailability($input) {
    $data = array();
    $seats_details = array();
    $trip_data      = $this->travel_destinations_model->get_search($input);
    if($trip_data) {
      $trip_data = $trip_data[0];
      
      $from_data   = $this->bus_stop_master_model->get_stops_detail($input['from'], $this->op_id);
      $to_data   = $this->bus_stop_master_model->get_stops_detail($input['to'], $this->op_id);

      if(count($from_data) > 0 && count($to_data) > 0) {
        $all_det['from_stop']           = $from_data[0]['op_stop_cd'];
        $all_det['bus_from_stop_id']    = $from_data[0]['bus_stop_id'];
        $all_det['bus_to_stop_id']      = $to_data[0]['bus_stop_id'];
        $all_det['to_stop']             = $to_data[0]['op_stop_cd'];
        //dont delete below commented line. we will use it later.
        $all_det['user_type']           = $from_data[0]['op_id'];

        $all_det['service_id']          = $trip_data['op_trip_no'];
        $all_det['date'] = date('d/m/Y', strtotime($input['date']));

        $data = $this->seatAvailability($all_det);
        $data['error'] = array();
        if (isset($data['SeatAvailabilityResponse']) && isset($data['SeatAvailabilityResponse']['availableSeats']) && count($data['SeatAvailabilityResponse']['availableSeats']) > 0) {
          if (isset($data['SeatAvailabilityResponse']['serviceId']) && count($data['SeatAvailabilityResponse']['serviceId']) > 0) {
            $available_seats = $data['SeatAvailabilityResponse']['availableSeats']['availableSeat'];
            unset($data['SeatAvailabilityResponse']['availableSeats']['availableSeat']);

            /*
            *                  VERY IMP NOTICE
            *HERE We kept 1 as hardcode for berth. Discuss this issue.
            *We dont find double deck berth in msrtc BUSES so dont handled condition here.
            *
            */
            $berth_no = 1;
            $data['SeatAvailabilityResponse']['availableSeats']['availableSeat'][$berth_no] = (is_array($available_seats)) ? $available_seats : array($available_seats);
            $tot_data_seat_layout = $this->op_seat_layout_model->get_seat_layout($trip_data['seat_layout_id']);

            foreach ($tot_data_seat_layout as $tkey => $tvalue)
            {
              $is_ladies_seat = (strtolower($tvalue->quota_type) == "ladies") ? true : false;
              $seats_details[$berth_no][$tvalue->seat_no] = array('is_ladies_seat' => $is_ladies_seat);
            }

            $data["SeatAvailabilityResponse"]["seats_details"] =  $seats_details;

            $data['flag'] = 'success';
          }
          else {
            $data['flag'] = 'error';
            $data['error'] = isset($data['SeatAvailabilityResponse']['serviceError']) ? $data['SeatAvailabilityResponse']['serviceError'] : "";
          }
        }
        $data["trip_data"] = $trip_data;
        $data["from_data"] = $from_data; //useless return
        $data["to_data"] = $to_data;//useless return
      }
    }
    return $data;
  }

  public function do_temp_booking($data_to_process) {
      
    $data  = array();
    $input = $data_to_process["input"];
      
    if(!empty($input)) {
      $available_detail = $data_to_process['available_detail'];
      $email = $input['email_id'];
      $mobile_no = $input['mobile_no'];
      $agent_email = $input['agent_email_id'];
      $agent_mobile_no = $input['agent_mobile_no'];
      $user_id = $input['user_id'];     
      $service_id = $available_detail['service_id'];
      $from = $available_detail['from'];
      $to = $available_detail['to'];
      $date = $available_detail['date'];

      $tot_seat = 0;
      $seat = $input['seat'];
      foreach ($seat as $seatkey => $seatvalue) {
        $tot_seat += count($seatvalue);
      }

      if($tot_seat > 4) {
        redirect(base_url());
      }
      
      $fare = $available_detail['fare'];
      $child_seat_fare = $available_detail['child_seat_fare'];
          
      //IMP For MSRTC
      $trip_route_stop = $this->travel_destinations_model->getTripDetail_optripno($available_detail['op_id'], $service_id, $from, $to, $date);

      $tot_fare = 0;
      $passenger_id = 1;

      $api_data['from'] = $from;
      $api_data['to'] = $to;
      $api_data['date'] = $date;
      $api_data['service_id'] = $service_id;
      $api_data['from_stop_id'] = $available_detail['from_stop_id'];
      $api_data['to_stop_id'] = $available_detail['to_stop_id'];
      $api_data['user_type'] = $available_detail['user_type'];
      $api_data["email"] = $email;
      $api_data["mobile_no"] = $mobile_no;
      $dept_boarding_time = date("Y-m-d H:i:s",strtotime($api_data['date']." ".$available_detail['sch_dept_time']));
      $dept_time = date("Y-m-d H:i:s",strtotime($trip_route_stop["start_stop_sch_departure_date"]));

      // Insert into ticket table
      $this->tickets_model->ticket_type = 1; // For In future
      $this->tickets_model->request_type = 'B';
      $this->tickets_model->ticket_status = 'A';
      $this->tickets_model->bus_service_no = $api_data['service_id'];
      $this->tickets_model->dept_time = $dept_time;
      $this->tickets_model->boarding_time = $dept_boarding_time;
      $this->tickets_model->alighting_time = date("Y-m-d H:i:s",strtotime($trip_route_stop["end_stop_arrival_date"]));
      $this->tickets_model->droping_time = $available_detail['arrival_time'];
      $this->tickets_model->from_stop_cd = $trip_route_stop['from_stop_cd'];
      $this->tickets_model->till_stop_cd = $trip_route_stop['to_stop_cd'];
      $this->tickets_model->from_stop_name = $trip_route_stop['from_stop_name'];
      $this->tickets_model->till_stop_name = $trip_route_stop['to_stop_name'];
      $this->tickets_model->boarding_stop_name = $available_detail['boarding_stop_name'];
      $this->tickets_model->destination_stop_name = $available_detail['destination_stop_name'];
      $this->tickets_model->boarding_stop_cd = $available_detail['boarding_stop_cd'];
      $this->tickets_model->destination_stop_cd = $available_detail['destination_stop_cd'];
      $this->tickets_model->op_id = $available_detail['op_id'];
      $this->tickets_model->provider_id = $available_detail['provider_id'];
      $this->tickets_model->provider_type = $available_detail['provider_type'];
      $this->tickets_model->booked_from = "rsrtc";
      $this->tickets_model->num_passgr = $tot_seat;
      $this->tickets_model->cancellation_rate = 0; 
      $this->tickets_model->issue_time = date("Y-m-d H:i:s");
      $this->tickets_model->pay_id = 1;
      $this->tickets_model->session_no = 1;
      $this->tickets_model->op_name = $available_detail['op_name'];
      $this->tickets_model->user_email_id = $email;
      $this->tickets_model->mobile_no = $mobile_no;
      $this->tickets_model->agent_email_id = $agent_email;
      $this->tickets_model->agent_mobile_no = $agent_mobile_no;
      $this->tickets_model->inserted_by = $this->session->userdata("booking_user_id");
      $this->tickets_model->role_id = (!empty($this->session->userdata("role_id"))) ? $this->session->userdata("role_id") : 0;
      $this->tickets_model->booked_by = (!empty($this->session->userdata("role_name"))) ? strtolower($this->session->userdata("role_name")) : "guest_user";
      $this->tickets_model->bus_type = (isset($available_detail['bus_type']['op_bus_type_name'])) ? $available_detail['bus_type']['op_bus_type_name'] : "";
      $this->tickets_model->status = 'Y';
      $this->tickets_model->ip_address = $this->input->ip_address();
      $this->tickets_model->tds_per = $this->config->item("tds");
      $ticket_id = $this->tickets_model->save();

      $extra_charges_value = 0;
      $total_extra_charges_value = 0;
      $total_convey_charge = 0;
      $total_ticket_discount = 0;
      $seat_service_tax_amt = 0.00;
      $seat_service_tax_perc = 0.00;
      $op_service_charge_amt = 0.00;
      $op_service_charge_perc = 0.00;
      $total_service_tax_amount = 0;
      $total_op_service_charge_amount = 0;
      $total_basic_fare = 0;
      $current_seat_fare_with_conveycharges_for_discount = 0;
      $tickets_total_op_service_charge_amount = 0;
      $tickets_total_service_tax = 0;
      $tickets_total_basic_fare = 0;

      if($ticket_id > 0) {
        $psgr_no = 1;
        foreach ($seat as $berth_no => $seat_detail) {
          foreach ($seat_detail as $key => $value) {
            $value['seat_no'] = $key;
            $value['berth_no'] = $berth_no;
      
            /*
             *                  NOTICE : VERY IMP LOGIC FOR FARE
             *if is_child_concession in op_bus_types is Y then assign child seat fare
             *if is_child_concession in op_bus_types is N then assign adult seat fare 
             *FARE ASSIGNED IN VALUE FARE BECAUSE IT WILL DIRECTLY BE USED IN TEMP BOOKING API
             */
            if(isset($available_detail['is_child_concession']) && $available_detail['is_child_concession'] == "Y") {
              if($value['age'] < 3) {
                $value['fare'] = 0.00;
              }
              else if($value['age'] >= 3 && $value['age'] <= 12) {
                $value['fare'] = $child_seat_fare;
              }
              else {
                $value['fare'] = $fare;
              }
              //$value['fare'] = $fare;
            }
            else {
              /*        NOTICE
               * MSRTC not allowing Senior Citizen. 
               * Here condition handled. When allowed Change if condition.
               */
              if($value['age'] >= 65)
              {
                $value['fare'] = $fare;
              }
              else
              {
                $value['fare'] = $fare;
              }
            }

            //check seat type by trip no and seat no 
            $sleeperCharge = 0;

            $seat_type = $this->travel_destinations_model->get_seat_type_by_trip_no_seat_no($available_detail['trip_no'],$value['seat_no']);
            if(!empty($seat_type)) {
              if(isset($seat_type[0]['seat_type']) && $seat_type[0]['seat_type'] == 'Sl') {
                $sleeperCharge = isset($available_detail['sleeperFare']) ? $available_detail['sleeperFare'] : 0.00;
                $value['fare'] += $sleeperCharge;
              }
            }
            /*
            *NOTICE: MOST IMP
            *
            *DO NOT DELETE BELOW COMMENTED LINES. THESE ARE IMP.
            *BELOW WRITTEN LOGIC IS STATIC CONVEY CHARGES LOGIC.
            *THE LOGIC WRITTEN BELOW IS TEMP ALTERNATIVE FOR DYNAMIC LOGIC
            *CHANGE THIS AFTER DISCUSSION
            */
            /**************CONVEY CHARGE STATIC LOGIC START *****************/
            $three_percent_tot_fare = 0;
            $fifty_per_ticket = 0;
            $extra_charges_value = 0;
            $convey_charge_type = "";
            $convey_charge_value = "";

            // Check convey charges
            /**************CONVEY CHARGE STATIC LOGIC START *****************/
            if(isset($available_detail['bus_type']['op_bus_type_cd']))
            { 
              eval(RSRTC_BUS_TYPES_AC);
              if(in_array($available_detail['bus_type']['op_bus_type_cd'], $rsrtc_bus_types_ac_array))
              {
                  $extra_charges_value = RSRTC_PER_TICKET_CONVEY_CHARGE_FOR_AC/$tot_seat;
                  $convey_charge_type = "fix";
                  $convey_charge_value =  RSRTC_PER_TICKET_CONVEY_CHARGE_FOR_AC/$tot_seat;
              }
              else
              {
                  $extra_charges_value = RSRTC_PER_TICKET_CONVEY_CHARGE_FOR_NON_AC/$tot_seat;
                  $convey_charge_type = "fix";
                  $convey_charge_value = RSRTC_PER_TICKET_CONVEY_CHARGE_FOR_NON_AC/$tot_seat;
              }
            }

            $total_extra_charges_value += $extra_charges_value;
            /**************CONVEY CHARGE STATIC LOGIC END *****************/

            $current_seat_fare_with_conveycharges = $value['fare']+$extra_charges_value;
          
            // For Discount Calculation on Final Ticket Base fare
            $current_seat_fare_with_conveycharges_for_discount += $current_seat_fare_with_conveycharges;

            $tickets_total_op_service_charge_amount += $op_service_charge_amt;
            $tickets_total_service_tax += $seat_service_tax_amt;
            /**************DISCOUNT LOGIC END *****************/

            //calculate commission amount
            $op_commission_type = 'flat';
            $op_commission_per = '0';
            $op_commission = round(($extra_charges_value),5);
            $tds_on_commission = round($op_commission * ((float)($this->config->item('tds')/100)),5);
            $final_commission = $op_commission - $tds_on_commission;

            $tot_fare += $value['fare'];
            $total_basic_fare += $value['fare'];

            $api_data['passenger'][] = $value;
            $this->ticket_details_model->ticket_id = $ticket_id;
            $this->ticket_details_model->psgr_no = $psgr_no;
            $this->ticket_details_model->psgr_name = $value['name'];
            $this->ticket_details_model->psgr_age = $value['age'];
            $this->ticket_details_model->psgr_sex = $value['gender'];
            $this->ticket_details_model->psgr_type = ($value['age'] <= 12 && isset($available_detail['is_child_concession']) && $available_detail['is_child_concession'] == "Y") ? "C" : "A";
            $this->ticket_details_model->concession_cd = 1; // if exists /in case of rsrtc 60 pass ct 
            $this->ticket_details_model->concession_rate = 1; // from api /if exists /in case of rsrtc 60 pass ct 
            $this->ticket_details_model->discount = 1; // if available from api
            $this->ticket_details_model->is_home_state_only = 1; // Y or No //not required now
            $this->ticket_details_model->fare_amt = $value['fare'];
            $this->ticket_details_model->convey_name = "Convenience charge";
            $this->ticket_details_model->convey_type = $convey_charge_type;
            $this->ticket_details_model->convey_value = $convey_charge_value;
            $this->ticket_details_model->fare_convey_charge = $convey_charge_value;
            $this->ticket_details_model->seat_no = $value['seat_no'];
            $this->ticket_details_model->berth_no = $value['berth_no'];
            $this->ticket_details_model->seat_status = 'Y';
            $this->ticket_details_model->concession_proff = 1;  // if cct then required // voter id
            $this->ticket_details_model->proff_detail = 1; // if varified
            $this->ticket_details_model->status = 'Y';
            $this->ticket_details_model->type = $op_commission_type;
            $this->ticket_details_model->commission_percent = $op_commission_per;
            $this->ticket_details_model->op_commission =  $op_commission;
            $this->ticket_details_model->tds_on_commission =  $tds_on_commission;
            $this->ticket_details_model->final_commission =  $final_commission;
            $psgr_no++;

            $ticket_details_id = $this->ticket_details_model->save();
          }
        }
              
        $api_data["ticket_id"] = $ticket_id;
        $soap_name = strtolower($available_detail['op_name']) . '_api';
        $tb_data = $this->temporary_booking($api_data);
        $data['ticket_id'] = $ticket_id;

        $tickets_total_op_service_charge_amount = 0;
        $total_ticket_discount = 0 ;

        if(!empty($tb_data))
        {
          $temp_booking_status = $tb_data['status'];
          if ($temp_booking_status == 'FAILURE') 
          {
            $data['block_key'] = "";
            $data['status'] = $tb_data['status'];
            $data['error'] = $tb_data['serviceError']['errorReason'];
            $data['payment_confirm_booking_status'] = "error";
          } 
          else if($temp_booking_status == 'SUCCESS')
          {
            $total_convey_charge = 0;
            $insert_ttrans_array = array();
            $tickets_total_fare = $tot_fare; //$tickets_total_fare has decalred because in some case we had changed $tot_fare value
            $total_fare_without_discount = $tot_fare + $total_extra_charges_value;
                    
            $tax_charges = array();
                    
            $data['status'] = $temp_booking_status;
                        
            if(isset($tb_data['passengerDetails']['passenger']['fareDetail'])) {
              $current_seat_fare_with_conveycharges = $tb_data['passengerDetails']['passenger']['totalFare']+$extra_charges_value;
              /**************DISCOUNT LOGIC START *****************/
              $api_reponse_data = $tb_data['passengerDetails']['passenger']['fareDetail'];

              $amount_to_add = 0.00;
              if($tb_data['passengerDetails']['passenger']['age'] >= 3 && $tb_data['passengerDetails']['passenger']['age'] <= 12) {
                $amount_to_add = (isset($api_reponse_data['childFare'])) ? $api_reponse_data['childFare'] : 0.00;
                $res_op_service_charge_amt = (isset($tb_data['passengerDetails']['passenger']['serviceChrg'])) ? $tb_data['passengerDetails']['passenger']['serviceChrg'] : 0.00;
              }
              else {
                $amount_to_add = (isset($api_reponse_data['adultFare'])) ? $api_reponse_data['adultFare'] : 0.00;
                $res_op_service_charge_amt = (isset($tb_data['passengerDetails']['passenger']['serviceChrg'])) ? $tb_data['passengerDetails']['passenger']['serviceChrg'] : 0.00;   
              }

              $tickets_total_basic_fare += $amount_to_add;
              $tickets_total_op_service_charge_amount += $res_op_service_charge_amt;
              
            }
            else {
              $api_reponse_data = $tb_data['passengerDetails']['passenger'][0]['fareDetail'];
              foreach ($tb_data['passengerDetails']['passenger'] as $psgkey => $psgvalue) {
                $current_seat_fare_with_conveycharges = $psgvalue['totalFare']+$extra_charges_value;
                
                $amount_to_add = 0.00;
                if($psgvalue['age'] >= 3 && $psgvalue['age'] <= 12) {
                  $amount_to_add = (isset($psgvalue["fareDetail"]['childFare'])) ? $psgvalue["fareDetail"]['childFare'] : 0.00;
                  $res_op_service_charge_amt = (isset($psgvalue['serviceChrg'])) ? $psgvalue['serviceChrg'] : 0.00;
                }
                else {
                  $amount_to_add = (isset($psgvalue["fareDetail"]['adultFare'])) ? $psgvalue["fareDetail"]['adultFare'] : 0.00;
                  $res_op_service_charge_amt = (isset($psgvalue['serviceChrg'])) ? $psgvalue['serviceChrg'] : 0.00;
                }
                
                $tickets_total_basic_fare += $amount_to_add;
                $tickets_total_op_service_charge_amount += $res_op_service_charge_amt;
              }
            }

            $total_ac_service_tax = 0;
            $total_asn_fare = 0;
            $tot_fare = 0; 
            if(isset($tb_data['passengerDetails']['passenger']['fareDetail'])) {
              $total_ac_service_tax = (isset($api_reponse_data['acServiceTax'])) ? $api_reponse_data['acServiceTax'] : 0.00;
              $total_asn_fare = (isset($api_reponse_data['asnFare'])) ? $api_reponse_data['asnFare'] : 0.00;
              $seat_amount = $tb_data['passengerDetails']['passenger']['totalFare'] + $tb_data['passengerDetails']['passenger']['roundedAmt'] + $tb_data['passengerDetails']['passenger']['serviceChrg'] + $api_reponse_data['it'];
              $tot_fare += $seat_amount;
              $update_fare_details = array("adult_basic_fare" => (isset($api_reponse_data['adultFare'])) ? $api_reponse_data['adultFare'] : 0.00,
                                           "child_basic_fare" => (isset($api_reponse_data['childFare'])) ? $api_reponse_data['childFare'] : 0.00,
                                           "fare_acc" => (isset($api_reponse_data['acc'])) ? $api_reponse_data['acc'] : 0.00,
                                           "fare_hr" => (isset($api_reponse_data['hr'])) ? $api_reponse_data['hr'] : 0.00,
                                           "fare_it" => (isset($api_reponse_data['it'])) ? $api_reponse_data['it'] : 0.00,
                                           "fare_octroi" => (isset($api_reponse_data['octroi'])) ? $api_reponse_data['octroi'] : 0.00,
                                           "fare_other" => (isset($api_reponse_data['other'])) ? $api_reponse_data['other'] : 0.00,
                                           "fare_reservationCharge" => (isset($api_reponse_data['reservationCharge'])) ? $api_reponse_data['reservationCharge'] : 0.00,
                                           "fare_sleeperCharge" => (isset($api_reponse_data['sleeperCharge'])) ? $api_reponse_data['sleeperCharge'] : 0.00,
                                           "fare_toll" => (isset($api_reponse_data['toll'])) ? $api_reponse_data['toll'] : 0.00,
                                           "ac_service_tax" => $total_ac_service_tax,
                                           "asn_fare" => $total_asn_fare,
                                           "fare_amt" => $seat_amount,
                                           // "fare_convey_charge" => $extra_charges_value,
                                          );
              
              if($api_reponse_data['discount']['isApplicable'] == 'Y')
              {
                $update_fare_details['mahila_concession'] = '1';
                $update_fare_details['concession_cd'] =(isset($tb_data['passengerDetails']['passenger']['concession'])) ? $tb_data['passengerDetails']['passenger']['concession'] : '';
                $update_fare_details['concession_rate'] = (isset($api_reponse_data['discount']['concessionRate'])) ? $api_reponse_data['discount']['concessionRate'] : '';
              }

              $this->ticket_details_model->update_where(array("ticket_id" => $ticket_id),"",$update_fare_details);
            }
            else {
              foreach ($tb_data['passengerDetails']['passenger'] as $ppkey => $ppvalue) {
                $ac_asn = $ppvalue["fareDetail"];
                $total_ac_service_tax += (isset($ac_asn['acServiceTax'])) ? $ac_asn['acServiceTax'] : 0.00;
                $total_asn_fare += (isset($ac_asn['asnFare'])) ? $ac_asn['asnFare'] : 0.00;
                $seat_amount = $ppvalue['totalFare'] + $ppvalue['roundedAmt'] + $ppvalue['serviceChrg'] + $ac_asn['it'];
                $tot_fare += $seat_amount;
                $update_fare_details = array("adult_basic_fare" => (isset($ac_asn['adultFare'])) ? $ac_asn['adultFare'] : 0.00,
                                             "child_basic_fare" => (isset($ac_asn['childFare'])) ? $ac_asn['childFare'] : 0.00,
                                             "fare_acc" => (isset($ac_asn['acc'])) ? $ac_asn['acc'] : 0.00,
                                             "fare_hr" => (isset($ac_asn['hr'])) ? $ac_asn['hr'] : 0.00,
                                             "fare_it" => (isset($ac_asn['it'])) ? $ac_asn['it'] : 0.00,
                                             "fare_octroi" => (isset($ac_asn['octroi'])) ? $ac_asn['octroi'] : 0.00,
                                             "fare_other" => (isset($ac_asn['other'])) ? $ac_asn['other'] : 0.00,
                                             "fare_reservationCharge" => (isset($ac_asn['reservationCharge'])) ? $ac_asn['reservationCharge'] : 0.00,
                                             "fare_sleeperCharge" => (isset($ac_asn['sleeperCharge'])) ? $ac_asn['sleeperCharge'] : 0.00,
                                             "fare_toll" => (isset($ac_asn['toll'])) ? $ac_asn['toll'] : 0.00,
                                             "ac_service_tax" => (isset($ac_asn['acServiceTax'])) ? $ac_asn['acServiceTax'] : 0.00,
                                             "asn_fare" => (isset($ac_asn['asnFare'])) ? $ac_asn['asnFare'] : 0.00,
                                             // "fare_convey_charge" => $extra_charges_value,
                                             "fare_amt" => $seat_amount,
                                            );
                if($ac_asn['discount']['isApplicable'] == 'Y') {
                  $update_fare_details['mahila_concession'] = '1';
                  $update_fare_details['concession_cd'] =(isset($ppvalue['concession'])) ? $ppvalue['concession'] : '';
                  $update_fare_details['concession_rate'] = (isset($ac_asn['discount']['concessionRate'])) ? $ac_asn['discount']['concessionRate'] : '';
                }

                $this->ticket_details_model->update_where(array("ticket_id" => $ticket_id,"seat_no" => $ppvalue['seatNo']),"",$update_fare_details);
              }
            }
            /*******To Update ticket fare details in ticket details tables End ********/

             // Total Fare With out discount
            $total_fare_without_discount = $tot_fare + $total_extra_charges_value;

            $data['discount_value'] = $total_ticket_discount;
            $final_total_amount = $total_fare_without_discount-$data['discount_value'];

            /*******To Update Tickets Table Start*******/
            $ticket_fields_update = array("ticket_ref_no" => $tb_data['blockKey'],
                                         "total_basic_amount" => $tickets_total_basic_fare,
                                         "op_service_charge" => $tickets_total_op_service_charge_amount,
                                         "service_tax" => $tickets_total_service_tax,
                                         "tot_fare_amt" => $tot_fare,
                                         "tot_fare_amt_with_tax" => $final_total_amount,
                                         "total_fare_without_discount" => $total_fare_without_discount,
                                         "fare_convey_charge" => $total_extra_charges_value,
                                         "ac_service_tax" => $total_ac_service_tax,
                                         "asn_fare" => $total_asn_fare,
                                         "transaction_status" => "temp_booked");
            $this->tickets_model->update_where(array("ticket_id" => $ticket_id),"",$ticket_fields_update);
            /*******To Update Tickets Table End*******/
            $data['block_key'] = $tb_data['blockKey'];
            $currentTime = strtotime(date("Y-m-d H:i:s"));
            $timeBlock = $currentTime+(60*$tb_data['blockTime']);
            $timeToBlock = date("Y-m-d H:i:s", $timeBlock);

            $data['fare_charges'] = $api_reponse_data;
            $data['block_time_minutes'] = $tb_data['blockTime'];
            $data['block_current_time'] = $currentTime;
            $data['time_to_block'] = $timeToBlock;
            $data['ttb_year'] = date("Y", $timeBlock);
            $data['ttb_month'] = date("m", $timeBlock);
            $data['ttb_day'] = date("d", $timeBlock);
            $data['ttb_hours'] = date("H", $timeBlock);
            $data['ttb_minutes'] = date("i", $timeBlock);
            $data['ttb_seconds'] = date("s", $timeBlock);
            $data['total_fare'] = $final_total_amount;
            $data["total_fare_without_discount"] =  $total_fare_without_discount;
            $data["ticket_fare"] =  $tot_fare;
            $data["total_seats"] = $tot_seat;
            $data["tax_charges"] =  $tax_charges;
            $data['payment_confirm_booking_status'] = "success";
            $data['ticket_id'] = $ticket_id;
          }
        }
        else {
          $data['payment_confirm_booking_status'] = "error";
        }
      }
      else {
        $data['payment_confirm_booking_status'] = "error";
      }
    }
    return $data;
  }

  function final_booking($api_data, $data) {
    $final_response = array();
    $tbdata = $this->doconfirm_booking($api_data);
    /************* Actual ticket booking End Here ***********/ 
    if(trim($tbdata['status']) == 'SUCCESS') {   
      $onwards_ticket_ref_no = $data["ticket_ref_no"];

      $data["ticket_pnr"] = $tbdata["pnr"];
      $data["operator_ticket_no"] = $tbdata["pnr"];

      $transaction = array('transaction_status' => "success",
                            'payment_by' => $api_data["payment_by"],
                            'payment_mode' => $data["pg_response"]["payment_mode"],
                          );

      $this->tickets_model->update($data["ticket_id"],$transaction);
      //common model
      // $pg_success_response = $this->transaction_process($data);
      $pg_success_response = $this->transaction_process($data);
      $final_response["transaction_process_response"] = $pg_success_response;
      //make this function common afterwards. Because this is used 2 times (1 times in pg and 1 time in wallet)
      if(trim($pg_success_response["msg_type"]) == "success") {
        $rpt_data = $api_data["rpt_data"];
        if(count($rpt_data)) {
          $onward_ticket_id = $pg_success_response["ticket_id"];
          $rtransaction = array('is_return_journey' => "1",
                                'return_ticket_id' => $rpt_data[0]->ticket_id
                                );

          $this->tickets_model->update($onward_ticket_id,$rtransaction);
          /************* Actual ticket booking Start Here ***********/
          $rapi_data['ticket_id']      = str_pad($rpt_data[0]->ticket_id, 8, '0', STR_PAD_LEFT);
          $rapi_data['ticket_ref_no']  = $rpt_data[0]->ticket_ref_no;
          $rapi_data['seat_count']     = $rpt_data[0]->num_passgr;

          $soap_name = strtolower($rpt_data[0]->op_name) . '_api';
          $rtbdata = $this->doconfirm_booking($rapi_data);
          /************* Actual ticket booking End Here ***********/

          if($rtbdata['status'] == 'SUCCESS') {
            $data["ticket_ref_no"] = $rpt_data[0]->ticket_ref_no;
            // $data["ticket_pnr"] = $rtbdata["pnr"];
            $data["ticket_pnr"] = (isset($rtbdata["pnr"])) ? $rtbdata["pnr"] : "";
            $data["operator_ticket_no"] = (isset($rtbdata["pnr"])) ? $rtbdata["pnr"] : "";
            
            $data["ticket_id"] = $rpt_data[0]->ticket_id;
            
            $atransaction = array('transaction_status' => "success",
                                   'payment_by' => $api_data["payment_by"],
                                   'payment_mode' => $data["pg_response"]["payment_mode"],
                                  );

            $this->tickets_model->update($rpt_data[0]->ticket_id,$atransaction);

            $final_response["return_ticket_status"] = "success";

            $rpg_success_response = $this->transaction_process($data);
            $final_response["transaction_process_response_return"] = $rpg_success_response;
            if(trim($rpg_success_response["msg_type"]) == "success") {
              if(isset($data['is_back']) && $data['is_back']) {
                $url_redirect = base_url()."admin/reservation/view_ticket/".$onwards_ticket_ref_no."/".$rpt_data[0]->ticket_ref_no;  
              }
              else {
                $url_redirect = base_url()."front/booking/view_ticket/".$onwards_ticket_ref_no."/".$rpt_data[0]->ticket_ref_no;
              }

              $final_response["status"] = "success";
              $final_response["redirect_url"] = $url_redirect;
            }
            else if(trim($rpg_success_response["msg_type"]) == "error") {
              $transaction = array('failed_reason' => isset($rpg_success_response["msg"]) ? $rpg_success_response["msg"] : "some error occured in rsrtc library",
                                   'payment_by' => $api_data["payment_by"]
                                  );

              $id = $this->tickets_model->update($rpt_data[0]->ticket_id,$transaction);
              $final_response["status"] = "error";
              $final_response["return_transaction_process_api"] = "error";
              $final_response["data"] = $data;
            }
          }
          else
          {
            if($rpt_data[0]->transaction_status != "success") {
              $transaction = array('transaction_status' => "failed",
                                 'failed_reason' => "rsrtc return ticket api failed",
                                 'payment_by' => $api_data["payment_by"]
                                );

              $return_ticket_update_where = ["ticket_id" => $rpt_data[0]->ticket_id,"transaction_status != " => "success"];
              $id = $this->tickets_model->update($return_ticket_update_where,$transaction);
            }
            else {
              $this->common_model->developer_confirm_booking_error_mail("RSRTC (Return Journey)",$api_data['pt_data']->ticket_id);
            }

            $final_response["return_ticket_status"] = "error";
            $final_response["return_ticket_api_failed_reason"] = "api failed";

            $final_response["status"] = "success";
            $final_response["redirect_url"] = $pg_success_response["redirect_url"];
            // redirect($pg_success_response["redirect_url"]);
          }
        }
        else {
            $final_response["status"] = "success";
            $final_response["redirect_url"] = $pg_success_response["redirect_url"];
        }
      }
      else if(trim($pg_success_response["msg_type"]) == "error")
      {
        $transaction = array(/*'transaction_status' => "failed",*/
                           'failed_reason' => 'failed message - '.(isset($pg_success_response["msg"])) ? $pg_success_response["msg"] : "",
                           'payment_by' => $api_data["payment_by"]
                          );

        $id = $this->tickets_model->update($api_data['ticket_id'],$transaction);
        $final_response["status"] = "success";
        $final_response["redirect_url"] = $pg_success_response["redirect_url"];

        logging_data("condition_not_handled/".date("M")."/rsrtc_".date("d-m-Y")."_log.log",
                 "Ticket booked successfully but some error occured in transaction_process_api for ticket is ".$data["ticket_id"],
                  'Transaction Process Status');
      }
    }
    else {
      if($api_data['pt_data']->transaction_status != "success") {
        $transaction = array('transaction_status' => "failed",
                           'failed_reason' => 'rsrtc doconfirm_booking api failed',
                           'payment_by' => $api_data["payment_by"]
                          );

        $update_where = ["ticket_id" => $api_data['pt_data']->ticket_id, "transaction_status != " => "success"];
        // $id = $this->tickets_model->update($api_data['ticket_id'],$transaction);
        $id = $this->tickets_model->update($update_where,$transaction);

        $find_return_ticket = array();
        $find_return_ticket = $this->return_ticket_mapping_model->where("ticket_id",$api_data['pt_data']->ticket_id)->find_all();
        
        if(!empty($find_return_ticket) && $find_return_ticket[0]->return_ticket_id > 0) {
          $return_update_where = ["ticket_id" => $find_return_ticket[0]->return_ticket_id, "transaction_status != " => "success"];
          $rid = $this->tickets_model->update($return_update_where,$transaction);
        }
      }
      else {
        $this->common_model->developer_confirm_booking_error_mail("RSRTC",$api_data['pt_data']->ticket_id);
      }
        
        $final_response["status"] = "error";
        $final_response["api_failed_reason"] = "api failed";
        $final_response["data"] = $data;
    }

    return $final_response;
  }

 /*
  * @Author      : Suraj Rathod
  * @function    : transaction_process()
  * @param       : $response["pg_response"] -> Response from pg
                   $response["is_updated"] -> is payment_transaction_fss updated or not status
                   $response["pay_trans_fss_id"] -> payment_transaction_fss id
  * @detail      : After payment success and doconfirm booking success, book the ticket and make entry in ticket table.
  *                 
  */
  public function transaction_process($response)
  {
    $pg_success_response = array();
    $pg_result = isset($response['pg_response']['result']) ? $response['pg_response']['result'] : "";
    $wallet_result = (isset($response['is_wallet_success']) && $response['is_wallet_success']) ? true : false ;
    
    if($wallet_result || (isset($response["is_updated"]) && $response["is_updated"] && (($pg_result == "CAPTURED") || ($pg_result =="APPROVED")))) {
      $blockKey = $response["ticket_ref_no"];
      $ticket_id = $response["ticket_id"];
      //After Remove Below Query and pass data
      $pt_data = $this->tickets_model->where("ticket_ref_no",$blockKey)->find_all();
      
      // $pt_data = $response["ticket_data"];

      if($pt_data)
      {
        // Get Trip Detail from using bus_service_id (ticket table)
        $trip_detail = $this->trip_master_model->where("op_trip_no",$pt_data[0]->bus_service_no)->find_all();
        $dept_date = date("D, d M Y, h:i A",strtotime($pt_data[0]->dept_time));
        $dept_time = date("H:i:s",strtotime($pt_data[0]->dept_time));

        $data['ticket_data'] = $pt_data[0];

        $ptd_data = $this->ticket_details_model->where("ticket_id",$ticket_id)->find_all();
        
        //$payment_total_fare = $pt_data[0]->num_passgr * $pt_data[0]->tot_fare_amt;
        $payment_total_fare = $pt_data[0]->tot_fare_amt_with_tax;
        $ticket_total_fare_without_discount = $pt_data[0]->total_fare_without_discount;

        $data['passenger_ticket'] = $pt_data[0];
        $data['passenger_ticket_details'] = $ptd_data;
        $data['pnr'] = $response['ticket_pnr'];

        /****** To Update PNR, BOS REFERENCE NUMBER and SMS Count Start ***********/
        $bos_ref = getBossRefNo($ticket_id);
        // $this->tickets_model->boss_ref_no = $bos_ref["bos_ref_no"];
        // $bos_ref_exists = $this->tickets_model->select();
        $bos_ref_exists = $this->tickets_model->where("boss_ref_no",$bos_ref["bos_ref_no"])->find_all();
        
        if(!$bos_ref_exists) {
            $bos_ticket_digit = $bos_ref["bos_ticket_digit"];
            $bos_ticket_string = $bos_ref["bos_ticket_string"];
            $bos_ref_no = $bos_ref["bos_ref_no"];
        }

        /****** To Update PNR,BOS REFERENCE NUMBER and SMS Count End***********/

          

        /*********************************************************************/
        /*
        *This is for testing Purpose only.
        *After discussion change this.
        */
        //THIS SEAT NO QUERY CAN BE REDUCE LATER
        $seat_no = $this->tickets_model->get_seat_nos_by_tid($ticket_id);
        if(count($seat_no) > 1) {
            $seat_with_berth = "Berth 1: ".$seat_no[0]->seat_no." Berth 2: ".$seat_no[1]->seat_no;
            $seat_with_berth_ticket = "Berth 1: ".$seat_no[0]->seat_no." <br/> Berth 2: ".$seat_no[1]->seat_no;
        }
        else {
            $seat_with_berth = $seat_no[0]->seat_no;
            $seat_with_berth_ticket = $seat_no[0]->seat_no;
        }
        $ticket_display_fare = $payment_total_fare;
          
        $smsData = [];
        $smsData["data"] = $data;
        $smsData["data"]["passenger_ticket"]->pnr_no = $response["ticket_pnr"];
        $smsData["seat_with_berth"] = $seat_with_berth;
        //if ticket booked by ors_agent two sms send one for agent and second for pessanger //
        $mobile_array[] = $pt_data[0]->mobile_no;
        if($pt_data[0]->agent_mobile_no != "") {
            $mobile_array[] = $pt_data[0]->agent_mobile_no;
        }
        $is_send = sendTicketBookedSms($smsData, $mobile_array);
        $eticket_count = ($is_send) ? 1 : 0;
        $fields_update = array("transaction_status" => "success", 
                                "pnr_no" => $response["ticket_pnr"],
                                "bos_ticket_digit" => $bos_ticket_digit,
                                "bos_ticket_string" => $bos_ticket_string,
                                "boss_ref_no" => $bos_ref_no,
                                "eticket_count" => $eticket_count // IMP -> Placed 1 Hardcoded because. Here is the first time we are sending message.
                                );

        $this->tickets_model->update($ticket_id,$fields_update);
          
          
        /*
        * The Above SMS Code is temp and For testing purposeonly
        */

          
        //to check if user exists
        $email = $pt_data[0]->user_email_id;
        $data['email']=$email;
        $this->users_model->email = $email;
        $data['user_exists'] = $this->users_model->select();

        /*********************************************************************/

        if (count($ptd_data) > 0)
        {
          $psgr_no = 1;
          $passanger_detail = "";
          
          $adult_basic_fare = 0;
          $senior_basic_fare = 0;
          $child_basic_fare = 0;

          $total_reservation_charges = 0;
          $total_hr = 0;
          $total_acc = 0;
          $total_sl = 0;
          $total_it = 0;
          $total_toll_tax = 0;
          $total_octroi = 0;
          $total_other_amount = 0;
          foreach ($ptd_data as $key => $value) {
            $reservationCharge += $value->fare_reservationCharge;

            if($value->psgr_age > 12 && $value->psgr_type == "A") {
                $adult_child = "A";
            }
            else if($value->psgr_age <= 12 && $value->psgr_type == "C") {
                $adult_child = "C";
            }
            $consession = $value->mahila_concession == '1' ? $value->concession_cd : 'NA';
            $total_reservation_charges += $value->fare_reservationCharge;
            $total_hr += $value->fare_hr;
            $total_acc += $value->fare_acc;
            $total_sl += $value->fare_acc;
            $total_it += $value->fare_sleeperCharge;
            $total_toll_tax += $value->fare_toll;
            $total_octroi += $value->fare_octroi;
            $total_other_amount += $value->fare_other;

            $passanger_detail .= '
                                  <tr>
                                    <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;">
                                      '.ucwords($value->psgr_name).'
                                    </td>
                                    <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;">
                                     '.ucwords($value->psgr_age).'
                                    </td>
                                    <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;">
                                      '.$adult_child.'
                                    </td>
                                    <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;">
                                      '.ucwords($value->psgr_sex).'
                                    </td>
                                    <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;">
                                      '.$value->seat_no.'
                                    </td>
                                    <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"> 
                                      '.$consession.'
                                    </td>
                                    <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"> 
                                      General 
                                    </td>
                                  </tr>';
            $psgr_no++; 
          }
		      
          eval(FME_AGENT_ROLE_NAME);
          if(in_array($pt_data[0]->booked_by, $fme_agent_role_name)) {
            $ticket_fare_to_show = $pt_data[0]->tot_fare_amt_with_tax + $pt_data[0]->agent_commission_amt;
          }
          else if ($pt_data[0]->payment_by = 'csc') {
            $ticket_fare_to_show = $pt_data[0]->total_fare_without_discount; 
          }
          else {
            $ticket_fare_to_show = $pt_data[0]->tot_fare_amt_with_tax;
          }

          $mail_replacement_array = array(
                                         "{{bos_ref_no}}" => $bos_ref_no,
                                         "{{pnr_no}}" => $response["ticket_pnr"],
                                         "{{user_email}}" => $pt_data[0]->user_email_id,
                                         "{{mobile_number}}" => $pt_data[0]->mobile_no,
                                         "{{ticket_no}}" => $pt_data[0]->ticket_id,
                                         "{{service_start_place}}" => strtoupper($pt_data[0]->from_stop_name),
                                         "{{service_end_place}}" =>  strtoupper($pt_data[0]->till_stop_name),
                                         "{{boarding_point}}" =>  strtoupper($pt_data[0]->boarding_stop_name),
                                         "{{alightning_point}}" =>  strtoupper($pt_data[0]->destination_stop_name),
                                         "{{date_of_journey_origin_dmy}}" =>  date("d/m/Y",strtotime($pt_data[0]->dept_time)),
                                         "{{origin_departure_His}}" =>  date("H:i:s",strtotime($pt_data[0]->dept_time)),
                                         "{{boarding_time_dmyHis}}" =>  date("d/m/Y H:i:s",strtotime($pt_data[0]->boarding_time)),
                                         "{{dropping_time_dmyHis}}" =>  date("d/m/Y H:i:s",strtotime($pt_data[0]->droping_time)),
                                         "{{bus_type}}" => $pt_data[0]->bus_type,
                                         "{{num_passgr}}" => $pt_data[0]->num_passgr,
                                         "{{bus_types}}" => $pt_data[0]->bus_type,
                                         "{{total_basic_fare}}" => "Rs. ".$pt_data[0]->total_basic_amount,
                                         "{{total_reservation_charges}}" => "Rs. ".$total_reservation_charges,       
                                         "{{total_hr}}" => "Rs. ".$total_hr,
                                         "{{total_acc}}" => "Rs. ".$total_acc,
                                         "{{total_sl}}" => "Rs. ".$total_sl,
                                         "{{total_it}}" => "Rs. ".$total_it,
                                         "{{total_toll_tax}}" => "Rs. ".$total_toll_tax,
                                         "{{service_tax}}" => $pt_data[0]->fare_convey_charge,
                                         "{{pg_charges}}" => $pt_data[0]->pg_charges,
                                         "{{discount_amount}}" => $pt_data[0]->discount_value,
                                         "{{total_octroi}}" => "Rs. ".$total_octroi,
                                         "{{total_other_amount}}" => "Rs. ".$total_other_amount,
                                         "{{total_ticket_amount}}" => "Rs. ".$pt_data[0]->tot_fare_amt,
                                         "{{final_paid_amount}}" => "Rs. ".$ticket_fare_to_show,
                                         "{{transaction_date}}" => date("d/m/Y H:i:s",strtotime($pt_data[0]->inserted_date)),
                                         "{{passanger_details}}" => $passanger_detail,
                                         "{{depot_detail}}"   => ($trip_detail[0]->depot_name!='' ? ($trip_detail[0]->depot_email_id != '' ? $trip_detail[0]->depot_name."/".$trip_detail[0]->depot_email_id : $trip_detail[0]->depot_name) : ($trip_detail[0]->depot_email_id != '' ?  $trip_detail[0]->depot_email_id : "")),      
                                        );
                
          /************ Mail Start Here ********/
          $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),RSRTC_TICKET_TEMPLATE);

          $mailbody_array = array("subject" => "Rokad Ticket",
                                "message" => $data['mail_content'],
                                "to" =>  $email 
                                );
         
          $mail_result = send_mail($mailbody_array);
                      /************ Mail End Here ********/

          /************ Dump ticket Start Here ********/
          //dump ticket in table
          $this->ticket_dump_model->bos_ref_no = $bos_ref["bos_ref_no"];
          $this->ticket_dump_model->ticket_id = $ticket_id;
          $this->ticket_dump_model->ticket_ref_no = $blockKey;
          $this->ticket_dump_model->pnr_no = $response["ticket_pnr"];
          $this->ticket_dump_model->email = $email;
          $this->ticket_dump_model->mobile = $pt_data[0]->mobile_no;
          $this->ticket_dump_model->journey_date = $pt_data[0]->dept_time;
          $this->ticket_dump_model->ticket = $data['mail_content'];
          if ($this->session->userdata('booking_user_id') != "") {
             $this->ticket_dump_model->created_by = $this->session->userdata("booking_user_id");
          }
          $ticket_dump_id = $this->ticket_dump_model->save();
          /************ Dump ticket End Here ********/

          //insert into user action log
          $users_action_log = array("user_id" => $this->session->userdata("booking_user_id"),
                                    "name" => $this->session->userdata("display_name"),
                                    "url" => "bos_ref_no = ".$bos_ref["bos_ref_no"]." , ticket_ref_no = ".$blockKey." , ticket_id = ".$ticket_id." , pnr_no = ".$data['pnr'],
                                    "action" => "bus ticket booked",
                                    "message" => "Booked bus from <span class='navy_blue location'>".ucfirst($pt_data[0]->from_stop_name)."</span> to <span class='navy_blue location'>".ucfirst($pt_data[0]->till_stop_name)."</span> for the day <span class='navy_blue date'>".date("jS F Y", strtotime($pt_data[0]->dept_time))."</span>"
                                    );
          
          users_action_log($users_action_log);

          $pg_success_response["msg_type"] = "success";
          $pg_success_response["bos_ref_no"] = $bos_ref["bos_ref_no"];
          $pg_success_response["ticket_id"] = $ticket_id;
          if(isset($response['is_back']) && $response['is_back']==1){
              $pg_success_response["redirect_url"] = base_url()."admin/reservation/view_ticket/".$blockKey;
          }else{
              $pg_success_response["redirect_url"] = base_url()."front/booking/view_ticket/".$blockKey;
          } 
        }// end of if $ptd_data condition
        else {
            $pg_success_response["msg_type"] = "error";
            $pg_success_response["msg"] = "ticket detail data not available.";
            $pg_success_response["redirect_url"] = base_url();
        }
      }// end of if $pt_data condition
      else {
          $pg_success_response["msg_type"] = "error";
          $pg_success_response["msg"] = "ticket data not available.";
          $pg_success_response["redirect_url"] = base_url();
      }
    }// end if payment status $response["is_updated"] && $pg_result
    else {
        $pg_success_response["msg_type"] = "error";
        $pg_success_response["msg"] = "payment gateway error or transaction table not updated.";
        $pg_success_response["redirect_url"] = base_url();
    }
    return $pg_success_response;
  } // End of transaction_process
  /******************************* FOR BOOKING PROCESS END *****************************/
  
  public function getBalance() {
    $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.rsrtc.trimax.com/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <ser:getBalance>
                         <!--Optional:-->
                         <UserBalanceRequest>
                            <!--Optional:-->
                            <userName>'.$this->user_id.'</userName>
                            <!--Optional:-->
                            <password>'.$this->pass.'</password>
                            <!--Optional:-->
                            <userType>'.$this->user_type.'</userType>
                         </UserBalanceRequest>
                      </ser:getBalance>
                   </soapenv:Body>
                </soapenv:Envelope>';
    
    $data = get_soap_data($this->url, 'getBalance', $xml_str);
    
    return $data;
  }

  public function is_ticket_cancellable() {
    $input = $this->input->post();
    if(!empty($input) && isset($input["pnr_no"]) && $input["pnr_no"] != "")
    {
      $response  = [];
      $data = $this->isTicketCancelable($input);

      if(isset($data["IsTicketCancelableResponse"]) && strtolower($data["IsTicketCancelableResponse"]["status"]) == "success")
      {
        if($data["IsTicketCancelableResponse"]["cancellable"] )
        {
          $response["status"]                 = "success";
          $response["refundAmount"]           = $data["IsTicketCancelableResponse"]["refundAmount"];
          $response["cancellationCharge"]     = $data["IsTicketCancelableResponse"]["cancellationCharge"];
        }
        else
        {
          $response["status"]                 = "error";
          $response["error_message"]          = "Ticket is non-cancellable.";
        }
      }
      else
      {
        $response["status"]                 = "error";
        $response["error_message"]          = "This ticket cannot be cancelled. Please contact customer support.";
      }
      
      return $response;
    }
    else
    {
      return false;
    }
  }

  public function isTicketCancelable($ticket_pnr) {
    $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.rsrtc.trimax.com/">
                 <soapenv:Header/>
                 <soapenv:Body>
                    <ser:isTicketCancelable>
                       <!--Optional:-->
                       <IsTicketCancelableRequest>
                          <!--Optional:-->
                          <authentication>
                             <!--Optional:-->
                             <userName>'.$this->user_id.'</userName>
                             <!--Optional:-->
                             <password>'.$this->pass.'</password>
                             <!--Optional:-->
                             <userType>'.$this->user_type.'</userType>
                          </authentication>
                          <!--Optional:-->
                          <pnr>'.$ticket_pnr['pnr_no'].'</pnr>
                          <!--Optional:-->
                          <seatNumbers>?</seatNumbers>
                       </IsTicketCancelableRequest>
                    </ser:isTicketCancelable>
                 </soapenv:Body>
              </soapenv:Envelope>';

    logging_data("ors/check_cancellation/rsrtc/".date("F")."/check_cancellation_".date("d-m-Y")."_log.log",$xml_str, 'check_cancellation');
    $data = get_soap_data($this->url, 'isTicketCancelable', $xml_str);
    logging_data("ors/check_cancellation/rsrtc/".date("F")."/check_cancellation_".date("d-m-Y")."_log.log",$data, 'check_cancellation');
    
    $data_to_log = [
                    "ticket_id" => isset($ticket_pnr['current_ticket_information']["ticket_id"]) ? $ticket_pnr['current_ticket_information']["ticket_id"] : "",
                    "request" => $xml_str,
                    "type" => "is_ticket_cancelable",
                    "response" => json_encode($data),
                   ];

    insert_request_response_log($data_to_log);
    return $data;
  }

  // to send the cancel ticket start here//
  public function mail_cancel_ticket($tinfo,$data,$ptd_data)
  { 
    $trip_detail = $this->trip_master_model->where("op_trip_no",$tinfo['bus_service_no'])->find_all();
    foreach ($ptd_data as $key => $value) {
      if($value->psgr_age <= 12) {
        $adult_child = "C";
        $passanger_details_basic_fare += $value->child_basic_fare;
      }
      else {
        $adult_child = "A";
        $passanger_details_basic_fare += $value->adult_basic_fare;
      }
      $consession = $value->mahila_concession == '1' ? $value->concession_cd : 'NA';
      $total_reservation_charges += $value->fare_reservationCharge;
      $total_hr += $value->fare_hr;
      $total_acc += $value->fare_acc;
      $total_sl += $value->fare_acc;
      $total_it += $value->fare_sleeperCharge;
      $total_toll_tax += $value->fare_toll;
      $total_octroi += $value->fare_octroi;
      $total_other_amount += $value->fare_other;
      $passanger_detail .= '<tr>
                                  <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;">
                                    '.ucwords($value->psgr_name).'
                                  </td>
                                  <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;">
                                   '.ucwords($value->psgr_age).'
                                  </td>
                                  <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;">
                                    '.$adult_child.'
                                  </td>
                                  <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;">
                                    '.ucwords($value->psgr_sex).'
                                  </td>
                                  <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;">
                                    '.$value->seat_no.'
                                  </td>
                                  <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"> 
                                    '.$consession.'
                                  </td>
                                  <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"> 
                                    General 
                                  </td>
                            </tr>';
      $psgr_no++; 
    }
    if($tinfo != '' && $data != '') {
      if($tinfo['payment_by'] == 'csc') {
        $total_fare = $tinfo['total_fare_without_discount'];
        $refund_amt = $tinfo['total_fare_without_discount'] - $data['cancellation_cahrge'];
      } 
      else {
        $total_fare = $tinfo['tot_fare_amt_with_tax'];
        $refund_amt = $data['refund_amt'];
      }  
      $mail_replacement_array = array("{{boss_ref_no}}" => $tinfo['boss_ref_no'],
                                           "{{pnr_no}}" => $tinfo['pnr_no'],
                                           "{{user_email}}" => $tinfo['user_email_id'],
                                           "{{mobile_number}}" => $tinfo['mobile_no'],
                                           "{{ticket_no}}" => $tinfo['ticket_id'],
                                           "{{service_start_place}}" => strtoupper($tinfo['from_stop_name']),
                                           "{{service_end_place}}" =>  strtoupper($tinfo['till_stop_name']),
                                           "{{boarding_point}}" =>  strtoupper($tinfo['boarding_stop_name']),
                                           "{{alightning_point}}" =>  strtoupper($tinfo['destination_stop_name']),
                                           "{{date_of_journey_origin_dmy}}" =>  date("d/m/Y",strtotime($tinfo['dept_time'])),
                                           "{{origin_departure_His}}" =>  date("H:i:s",strtotime($tinfo['dept_time'])),
                                           "{{boarding_time_dmyHis}}" =>  date("d/m/Y H:i:s",strtotime($tinfo['boarding_time'])),
                                           "{{dropping_time_dmyHis}}" =>  date("d/m/Y H:i:s",strtotime($tinfo['droping_time'])),
                                           "{{bus_type}}" => $tinfo['bus_type'],
                                           "{{num_passgr}}" => $tinfo['num_passgr'],
                                           "{{bus_types}}" => $tinfo['bus_type'],
                                           "{{total_basic_fare}}" => "Rs. ".$tinfo['total_basic_amount'],
                                           "{{total_reservation_charges}}" => "Rs. ".$total_reservation_charges,       
                                           "{{total_hr}}" => "Rs. ".$total_hr,
                                           "{{total_acc}}" => "Rs. ".$total_acc,
                                           "{{total_sl}}" => "Rs. ".$total_sl,
                                           "{{total_it}}" => "Rs. ".$total_it,
                                           "{{total_toll_tax}}" => "Rs. ".$total_toll_tax,
                                           // "{{service_tax}}" => $tinfo['fare_convey_charge'],
                                           "{{refund_amount}}" => "Rs. ".$refund_amt,
                                           "{{pg_charges}}" =>  $tinfo['pg_charges'],
                                           "{{discount_amount}}" =>  $tinfo['discount_value'],
                                           "{{total_octroi}}" => "Rs. ".$total_octroi,
                                           "{{total_other_amount}}" => "Rs. ".$total_other_amount,
                                           "{{total_ticket_amount}}" => "Rs. ". $total_fare,
                                           "{{final_paid_amount}}" => "Rs. ".$ticket_fare_to_show,
                                           "{{transaction_date}}" => date("d/m/Y H:i:s",strtotime($tinfo['inserted_date'])),
                                           "{{passanger_details}}" => $passanger_detail,
                                           "{{depot_detail}}"   => ($trip_detail[0]->depot_name!='' ? ($trip_detail[0]->depot_email_id != '' ? $trip_detail[0]->depot_name."/".$trip_detail[0]->depot_email_id : $trip_detail[0]->depot_name) : ($trip_detail[0]->depot_email_id != '' ?  $trip_detail[0]->depot_email_id : "")),
                                           );
      /************ Mail Start Here **************/
      $email = $tinfo['user_email_id'];
      $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),RSRTC_CANCEL_TICKET_TEMPLATE);

      $mailbody_array = array("subject" => "Book On Spot Cancel Ticket",
                              "message" => $data['mail_content'],
                              "to" =>  $email
                              );
       
      $mail_result = send_mail($mailbody_array);
      /************ Mail End Here *********************/
      /************ Agent Mail Start Here **************/
      $email_agent = $tinfo['agent_email_id'];
      if($email_agent != "" && ($email_agent != $email)) {
          $agent_mailbody_array = array("subject" => "Book On Spot Cancel Ticket",
                                "message" => $data['mail_content'],
                                "to" =>  $email_agent
                                );
           $agent_mail_result = send_mail($agent_mailbody_array);
      }
      //************ Agent Mail End Start Here ***************//
    }
    else {
      redirect();
    }
    /************************ Update in Ticket_dump table start here *****************************/
      $update_in_ticket_dump = $this->ticket_dump_model->update_where(array('ticket_id' => $tinfo['ticket_id']),'',array('cancel_ticket' => $data['mail_content']));

    /************************ Update in Ticket_dump table end here *****************************/
  }// to send the cancel ticket end here

}
