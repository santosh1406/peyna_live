<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Upsrtc_api extends CI_Model {
 
  var $user_id;
  var $pass;
  var $url;
  var $user_type;
  var $op_id;
  var $dep_date;
  var $ci;

  public function __construct() {
    parent::__construct();

    ini_set('max_execution_time', 100000);
    ini_set("memory_limit","256M");

    $this->load->helper('soap_data');
    $this->load->model(array('soap_model/upsrtc_model','ticket_discount_description_model','cancellation_charges_model','ticket_details_model','base_commission_model'));
    $this->load->library(array('cca_payment_gateway'));

    $this->user_id 			= $this->config->item('upsrtc_wsdl_user');
    $this->pass    			= $this->config->item('upsrtc_wsdl_password');
    $this->url     			= $this->config->item("upsrtc_wsdl_url");
    $this->user_type		= 1;
    $this->op_id       	= $this->config->item('upsrtc_operator_id');

    $this->dep_date     = $this->dep_date && $this->dep_date !=''? $this->dep_date : date('Y-m-d');
    $this->dep_date     = date('d/m/Y',strtotime($this->dep_date.' +1 days'));
  }

  public function index() {
    // echo 'class loaded';
  }

  public function seatAvailability($all_det) {
    $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
                 <soapenv:Header/>
                 <soapenv:Body>
                    <ser:getSeatAvailability>
                       <!--Optional:-->
                       <SeatAvailabilityRequest>
                          <!--Optional:-->
                          <authentication>
                             <!--Optional:-->
                             <userName>'.$this->user_id.'</userName>
                             <!--Optional:-->
                             <password>'.$this->pass.'</password>
                             <!--Optional:-->
                             <userType>'.$this->user_type.'</userType>
                          </authentication>
                          <serviceId>'.$all_det['service_id'].'</serviceId>
                          <!--Optional:-->
                          <fromStopId>'.$all_det['from_stop'].'</fromStopId>
                          <!--Optional:-->
                          <toStopId>'.$all_det['to_stop'].'</toStopId>
                          <!--Optional:-->
                          <dateOfJourney>'.$all_det['date'].'</dateOfJourney>
                       </SeatAvailabilityRequest>
                    </ser:getSeatAvailability>
                 </soapenv:Body>
                </soapenv:Envelope>';

    logging_data("ors/upsrtc/".date("F")."/ors_".date("d-m-Y")."_log.log",$xml_str, 'Seat availablity');

    $data = get_soap_data($this->url, 'getSeatAvailability', $xml_str);

    logging_data("ors/upsrtc/".date("F")."/ors_".date("d-m-Y")."_log.log",$data, 'Seat availablity');
    
    if (!isset($data['SeatAvailabilityResponse']) && !isset($data['SeatAvailabilityResponse']['availableSeats']) && !(count($data['SeatAvailabilityResponse']['availableSeats']) > 0))
    {
      $custom_error_mail = array(
                                  "custom_error_subject" => "UPSRTC Seat layout Error from UPSRTC.",
                                  "custom_error_message" => "Request - ".$xml_str."<br/></br/> Response - ".json_encode($data)
                                );

      $this->common_model->management_custom_error_mail($custom_error_mail,"upsrtc");
    }

    return $data;
  }

  public function getCancelList($all_det) {

    $date = date('d/m/Y H:i:s',strtotime($all_det['date']));
    $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
                 <soapenv:Header/>
                 <soapenv:Body>
                    <ser:getCancelList>
                       <!--Optional:-->
                       <CancelListRequest>
                          <!--Optional:-->
                          <authentication>
                             <!--Optional:-->
                             <userName>'.$this->user_id.'</userName>
                             <!--Optional:-->
                             <password>'.$this->pass.'</password>
                             <!--Optional:-->
                             <userType>'.$this->user_type.'</userType>
                          </authentication>
                          <!--Optional:-->
                          <fromDate>'.$date.'</fromDate>
                       </CancelListRequest>
                    </ser:getCancelList>
                 </soapenv:Body>
                </soapenv:Envelope>';

    logging_data("ors/upsrtc/".date("F")."/ors_".date("d-m-Y")."_log.log",$xml_str,'temp booking xml');

    $response = get_soap_data($this->url, 'getCancelList', $xml_str);

    logging_data("ors/upsrtc/".date("F")."/ors_".date("d-m-Y")."_log.log",$response, 'temp booking Response');

    return $response;
  }

  public function temporary_booking($data = array())
  { 
    $pass = $data['passenger'];
    $date = date('d/m/Y',strtotime($data['date']));

    $xml_str = ' <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
                            <soapenv:Header/>
                            <soapenv:Body>
                               <ser:doTemporaryBooking>
                                  <!--Optional:-->
                                  <TentativeBookingRequest>
                                     <!--Optional:-->
                                     <authentication>
                                        <!--Optional:-->
                                        <userName>'.$this->user_id.'</userName>
                                        <!--Optional:-->
                                        <password>'.$this->pass.'</password>
                                        <!--Optional:-->
                                        <userType>'.$this->user_type.'</userType>
                                     </authentication>
                                     <serviceId>'.$data['service_id'].'</serviceId>
                                     <!--Optional:-->
                                     <fromBusStopId>'.$data['from_stop_id'].'</fromBusStopId>
                                     <!--Optional:-->
                                     <toBusStopId>'.$data['to_stop_id'].'</toBusStopId>
                                     <!--Optional:-->
                                     <dateOfJourney>'.$date.'</dateOfJourney>
                                     <!--Optional:-->
                                                          <passengerDetails>
                                                          <!--Zero or more repetitions:-->
                                 ';

        foreach ($pass as $key => $value) {
                                      
                                    
        $xml_str .=                 '  <passenger>
                                        <!--Optional:-->
                                        <passengerName>'.$value['name'].'</passengerName>
                                        <age>'.$value['age'].'</age>
                                        <!--Optional:-->
                                        <sex>'.$value['gender'].'</sex>
                                        <!--Optional:-->
                                        <contactNumber>'.$data["mobile_no"].'</contactNumber>
                                        <!--Optional:-->
                                        <email>'.$data["email"].'</email>
                                        <seatNo>'.$value['seat_no'].'</seatNo>
                                        <totalFare>'.$value['fare'].'</totalFare>
                                        <!--Optional:-->
                                        <seatType>?</seatType>
                                        <!--Optional:-->
                                        <fareDetail>
                                           <acc>?</acc>
                                           <adultFare>?</adultFare>
                                           <childFare>?</childFare>
                                           <!--Optional:-->
                                           <concode>?</concode>
                                           <hr>?</hr>
                                           <it>?</it>
                                           <octroi>?</octroi>
                                           <other>?</other>
                                           <reservationCharge>?</reservationCharge>
                                           <sleeperCharge>?</sleeperCharge>
                                           <state1>?</state1>
                                           <state2>?</state2>
                                           <state3>?</state3>
                                           <state4>?</state4>
                                           <state5>?</state5>
                                           <toll>?</toll>
                                        </fareDetail>
                                     </passenger>';
        }

        $xml_str .=            '</passengerDetails>
                                </TentativeBookingRequest>
                             </ser:doTemporaryBooking>
                          </soapenv:Body>
                       </soapenv:Envelope>
                       ';

    logging_data("ors/upsrtc/".date("F")."/ors_".date("d-m-Y")."_log.log",$xml_str,'temp booking xml');
    $response = get_soap_data($this->url, 'doTemporaryBooking', $xml_str);
    logging_data("ors/upsrtc/".date("F")."/ors_".date("d-m-Y")."_log.log",$response, 'temp booking Response');

    $data_to_log = [
                    "ticket_id" => isset($data["ticket_id"]) ? $data["ticket_id"] : "",
                    "request" => $xml_str,
                    "response" => json_encode($response),
                    "type" => "temp_booking"
                   ];

    insert_request_response_log($data_to_log);

    if(isset($response['TentativeBookingResponse'])) {
      return $response['TentativeBookingResponse'];
    }
    else {
      return $response['TentativeBookingResponse'];
    }
  }

  public function doconfirm_booking($data = array())
  { 
    $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
                            <soapenv:Header/>
                            <soapenv:Body>
                               <ser:doConfirmBooking>
                                  <!--Optional:-->
                                  <ConfirmBookingRequest>
                                     <!--Optional:-->
                                     <authentication>
                                        <!--Optional:-->
                                        <userName>'.$this->user_id.'</userName>
                                        <!--Optional:-->
                                        <password>'.$this->pass.'</password>
                                        <!--Optional:-->
                                        <userType>'.$this->user_type.'</userType>
                                     </authentication>
                                     <!--Optional:-->
                                     <uniqueTransactionId>'.$data['ticket_id'].'</uniqueTransactionId>
                                     <!--Optional:-->
                                     <billDeskResponse>?</billDeskResponse>
                                     <!--Optional:-->
                                     <blockKey>'.$data['ticket_ref_no'].'</blockKey>
                                     <seatCount>'.$data['seat_count'].'</seatCount>
                                  </ConfirmBookingRequest>
                               </ser:doConfirmBooking>
                            </soapenv:Body>
                 </soapenv:Envelope>';

    logging_data("ors/upsrtc/".date("F")."/ors_".date("d-m-Y")."_log.log",$xml_str, 'confirm booking xml');
    
    $response = get_soap_data($this->url, 'doTemporaryBooking', $xml_str);

    logging_data("ors/upsrtc/".date("F")."/ors_".date("d-m-Y")."_log.log",$response, 'confirm booking response');
    
    $data_to_log = [
                    "ticket_id" => isset($data["ticket_id"]) ? $data["ticket_id"] : "",
                    "request" => $xml_str,
                    "type" => "confirm_booking",
                    "response" => json_encode($response),
                   ];

    insert_request_response_log($data_to_log);
    
    if(isset($response['ConfirmBookingResponse']) && $response['ConfirmBookingResponse']['status'] == 'SUCCESS') {
      return $response['ConfirmBookingResponse'];
    }
    else {
      return $response['ConfirmBookingResponse'];
    }
  }

  function getAvailableServices() { 
    $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <ser:getAvailableServices>
                         <!--Optional:-->
                         <AvailableServiceRequest>
                            <!--Optional:-->
                            <authentication>
                               <!--Optional:-->
                               <userName>'.$this->user_id.'</userName>
                               <!--Optional:-->
                               <password>'.$this->pass.'</password>
                               <!--Optional:-->
                               <userType>'.$this->user_type.'</userType>
                            </authentication>
                            <!--Optional:-->
                            <dateOfJourney>'.$this->dep_date.'</dateOfJourney>
                         </AvailableServiceRequest>
                      </ser:getAvailableServices>
                   </soapenv:Body>
                </soapenv:Envelope>';
    $response = get_soap_data($this->url,  'getAvailableServices', $xml_str);
    $this->db->trans_begin();
      
    $data = $this->upsrtc_model->insert_available_services($response, $this->op_id,$this);
    if ($this->db->trans_status() === FALSE) {
      show(error_get_last());
      $this->db->trans_rollback();
    }
    else {
      $this->db->trans_commit();
    }
  }

  function GetAllBusTypes() {
    $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
                     <soapenv:Header/>
                     <soapenv:Body>
                        <ser:GetAllBusTypes>
                           <!--Optional:-->
                           <AllBusTypeRequest>
                              <!--Optional:-->
                              <authentication>
                                 <!--Optional:-->
                                 <userName>'.$this->user_id.'</userName>
                                 <!--Optional:-->
                                 <password>'.$this->pass.'</password>
                                 <!--Optional:-->
                                 <userType>'.$this->user_type.'</userType>
                              </authentication>
                           </AllBusTypeRequest>
                        </ser:GetAllBusTypes>
                     </soapenv:Body>
                  </soapenv:Envelope>';

    $response   = get_soap_data($this->url,  'GetAllBusTypes', $xml_str);
    
    $seat_layout_id = $this->upsrtc_model->insert_bus_type($response, $this->op_id);
    
    return $seat_layout_id;
  }

  function getAllBusFormats($bus_format_cd) {
    $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <ser:getAllBusFormats>
                         <!--Optional:-->
                         <AllBusFormatRequest>
                            <!--Optional:-->
                            <authentication>
                               <!--Optional:-->
                               <userName>'.$this->user_id.'</userName>
                               <!--Optional:-->
                               <password>'.$this->pass.'</password>
                               <!--Optional:-->
                               <userType>'.$this->user_type.'</userType>
                            </authentication>
                            <!--Optional:-->
                            <busFormatType>'.$bus_format_cd.'</busFormatType>
                         </AllBusFormatRequest>
                      </ser:getAllBusFormats>
                   </soapenv:Body>
                </soapenv:Envelope>';

    $response = get_soap_data($this->url,  'getAllBusFormats', $xml_str, $bus_format_cd);
    if(count($response)>0) {
      $seat_layout_id = $this->upsrtc_model->insert_bus_format($response, $this->op_id,$bus_format_cd);
    }
    else {
      show($response,'','No Response');
      return '';
    }
    return $seat_layout_id;
  }

  function getBoardingStops_api($data =array(), $ali_type) {
    $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.rsrtc.trimax.com/">
                  <soapenv:Header/>
                     <soapenv:Body>
                        <ser1:getBoardingStops xmlns:ser1="http://service.upsrtc.trimax.com/">
                           <!--Optional:-->
                           <GetBoardingStopsRequest>
                              <!--Optional:-->
                              <authentication>
                                 <!--Optional:-->
                                 <userName>'.$this->user_id.'</userName>
                                 <!--Optional:-->
                                 <password>'.$this->pass.'</password>
                                 <!--Optional:-->
                                 <userType>'.$this->user_type.'</userType>
                              </authentication>
                              <serviceId>'.$data['service_id'].'</serviceId>
                              <!--Optional:-->
                              <fromStopId>'.$data['from'].'</fromStopId>
                              <!--Optional:-->
                              <toStopId>'.$data['to'].'</toStopId>
                              <!--Optional:-->
                              <typeAliBrd>'.$ali_type.'</typeAliBrd>
                              <!--Optional:-->
                              <busTypeCode>'.$data['bus_type_code'].'</busTypeCode>
                           </GetBoardingStopsRequest>
                        </ser1:getBoardingStops>
                     </soapenv:Body>
                  </soapenv:Envelope>';

    $response = get_soap_data($this->url,  'getBoardingStops', $xml_str);

    return $response;
  }

  function getBoardingStops($data =array(), $ali_type) {
    $response = $this->getBoardingStops_api($data, $ali_type);
    $stops = array();

    if( isset($response['GetBoardingStopsResponse']) && count($response['GetBoardingStopsResponse']) >0 && ( isset($response['GetBoardingStopsResponse']['boardingStopDetails']) || isset($response['GetBoardingStopsResponse']['alightingStopDetails']))) {
      $data['service_id'] = $response['GetBoardingStopsResponse']['serviceId'];

      if($ali_type == 'BRD') {
        $stop_code =  $data['from'];
        if( isset($response['GetBoardingStopsResponse']['boardingStopDetails']['boardingStopDetail']) && count($response['GetBoardingStopsResponse']['boardingStopDetails']) >0  &&  count($response['GetBoardingStopsResponse']['boardingStopDetails']['boardingStopDetail']) >0) {

          $stops = $response['GetBoardingStopsResponse']['boardingStopDetails']['boardingStopDetail'];

          if(isset($stops['busStopName'])) {
              $stops = array($stops);
          }
        }
        $this->getBoardingStops($data, 'ALI');
      }
      else if($ali_type == 'ALI')
      {
        $stop_code =  $data['to'];
        if(isset($response['GetBoardingStopsResponse']['alightingStopDetails']['alightingStopDetail']) && count($response['GetBoardingStopsResponse']['alightingStopDetails']) >0  &&  count($response['GetBoardingStopsResponse']['alightingStopDetails']['alightingStopDetail']) >0) {
          $stops = $response['GetBoardingStopsResponse']['alightingStopDetails']['alightingStopDetail'];

          if(isset($stops['busStopName'])) {
              $stops = array($stops);
          }
        }
      }

      if(count($stops)>0) {
        $this->upsrtc_model->insert_boarding_stops($stops, $ali_type, $stop_code, $this->op_id, $data);
      }
    }
    else {
      show($response,'','getBoardingStops----error');
    }
    return $response;
  }

  function give_boarding_Stop_from_api($input) {
    $from = $this->bus_stop_master_model->get_detail_by_name($input['from'], $input['operator']);
    $to   = $this->bus_stop_master_model->get_detail_by_name($input['to'], $input['operator']);    
    $board_data = array(
                    'service_id' => $input['service_id'],
                    'from' => $from->op_stop_cd,
                    'to'  => $to->op_stop_cd,
                    'bus_type_code' => $input['bus_type_code'],
                );
    $data = array();

    $data = array('service_id' => $input['service_id'], );
    $boarding_data    = $this->getBoardingStops_api($board_data, 'BRD');

    if(!empty($boarding_data['GetBoardingStopsResponse']['boardingStopDetails']['boardingStopDetail'])){
      $data['boarding'] = $boarding_data['GetBoardingStopsResponse']['boardingStopDetails']['boardingStopDetail'];
    }
    
    $alighting_data   = $this->getBoardingStops_api($board_data, 'ALI');

    if(!empty($alighting_data['GetBoardingStopsResponse']['alightingStopDetails']['alightingStopDetail'])){
      $data['alighting'] = $alighting_data['GetBoardingStopsResponse']['alightingStopDetails']['alightingStopDetail'];
    }
    return $data;
  }

  function apiBoardingStop($input){

    $this->load->model(array('boarding_stop_model'));
    
    $from = $this->bus_stop_master_model->get_detail_by_name($input['from'], $input['operator']);
    $to   = $this->bus_stop_master_model->get_detail_by_name($input['to'], $input['operator']);

    $this->db->select('bs.bus_stop_name,bs.stop_code,bs.is_boarding,bs.is_alighting,bs.seq as sequence');
    $this->db->from('bord_ali_stop_master bs');
    // $this->db->where_in('bs.trip_no', $trip);
    $this->db->where('bs.op_trip_no', $input['service_id']);
    $this->db->where('bs.from', $from->op_stop_cd);
    $this->db->where('bs.bus_type', $input['bus_type_code']);
    $this->db->group_by("bs.trip_no , bs.bus_stop_name");
    $this->db->order_by("bs.seq", 'asc');
    $query = $this->db->get();

    $data = false;
    if($query->num_rows()) {
      $result  = $query->result();          

      if($result) {
        $data['service_id'] = $input['service_id'];
        foreach ($result as $key => $value) {
          if($value->is_boarding == 'Y') {
              $data['boarding'][] = $value;
          }
          elseif($value->is_alighting == 'Y') {
              $data['alighting'][] = $value;
          }
        }
      }
    }
    else {
      $data = $this->give_boarding_Stop_from_api($input);
    }

    if( empty($data['boarding']) || empty($data['alighting']) ) {  
      if(empty($data['boarding'])) {
          $data['boarding'] = array( array( 'bus_stop_name' => $from->bus_stop_name,'stop_code' => $from->op_stop_cd,'is_boarding' => 'Y','is_alighting' => 'N','sequence' => '1'));
      }
      if(empty($data['alighting'])) {
          $data['alighting'] = array(array('bus_stop_name' => $to->bus_stop_name,'stop_code' => $to->op_stop_cd,'is_boarding' => 'N','is_alighting' => 'Y','sequence' => '1'));
      }
    }
    return $data;
  }

  function getBusServiceStops($service_id, $trip_no = '', $op_bus_type = '') {

    $xml_str  =  '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
 						       <soapenv:Header/>
                     	<soapenv:Body>
                        <ser:getBusServiceStops>
                           <!--Optional:-->
                           <BusServiceStopsRequest>
                              <!--Optional:-->
                              <authentication>
                                 <!--Optional:-->
                                 <userName>'.$this->user_id.'</userName>
                                 <!--Optional:-->
                                 <password>'.$this->pass.'</password>
                                 <!--Optional:-->
                                 <userType>'.$this->user_type.'</userType>
                              </authentication>
                              <serviceId>'.$service_id.'</serviceId>
                              <!--Optional:-->
                              <dateOfJourney>'.$this->dep_date.'</dateOfJourney>
                           </BusServiceStopsRequest>
                        </ser:getBusServiceStops>
                     </soapenv:Body>
                  </soapenv:Envelope>';
      
    $response   = get_soap_data($this->url,  'getBusServiceStops', $xml_str);
    $bstop                     = array();
    $bstop['service_id']       = $service_id;
    $bstop['trip_no']          = $trip_no;
    $bstop['op_bus_type']      = $op_bus_type;

    $this->db->trans_begin();
      
    if($response && is_array($response) && count($response) >0 ) {
      $data       = $this->upsrtc_model->insert_bus_service_stops($response,$this->op_id, $this->dep_date, $this,$bstop);
      
      if ($this->db->trans_status() === FALSE) {
          show(error_get_last());
          $this->db->trans_rollback();
      }
      else {
          $this->db->trans_commit();
      }
    }
    else {
      show($xml_str,'','getBusServiceStops');
      show($response,'','getBusServiceStops');
    }
  }

  public function cancel_ticket($data) {
    $xml_str='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <ser:cancelTicket>
                         <!--Optional:-->
                         <CancelRequest>
                            <!--Optional:-->
                            <authentication>
                               <!--Optional:-->
                               <userName>'.$this->user_id.'</userName>
                               <!--Optional:-->
                               <password>'.$this->pass.'</password>
                               <!--Optional:-->
                               <userType>'.$this->user_type.'</userType>
                            </authentication>
                            <!--Optional:-->
                            <pnr>'.$data['pnr_no'].'</pnr>
                            <!--Optional:-->
                            <seatNumbers> </seatNumbers>
                         </CancelRequest>
                      </ser:cancelTicket>
                   </soapenv:Body>
              </soapenv:Envelope>';

    logging_data("ors/upsrtc/".date("F")."/cancel_".date("d-m-Y")."_log.log",$xml_str, 'Cancel Ticket Request');

    $response = get_soap_data($this->url, 'cancelTicket', $xml_str);

    logging_data("ors/upsrtc/".date("F")."/cancel_".date("d-m-Y")."_log.log",$response, 'Cancel Ticket Response');

    $data_to_log = [
                    "ticket_id" => isset($data['current_ticket_information']["ticket_id"]) ? $data['current_ticket_information']["ticket_id"] : "",
                    "request" => $xml_str,
                    "type" => "ticket_cancel",
                    "response" => json_encode($response),
                   ];

    insert_request_response_log($data_to_log);
    if(isset($response['CancelResponse']) && $response['CancelResponse']['status'] == 'SUCCESS') {
      return $response['CancelResponse'];
    }
    else {            
      return $response['CancelResponse'];
    }
  }

  /******************************* FOR BOOKING PROCESS START *****************************/
  public function get_seat_layout($input) {     
        
    $seat_service_tax_amt = 0.00;
    $seat_service_tax_perc = 0.00;
    $op_service_charge_amt = 0.00;
    $op_service_charge_perc = 0.00;
        
    $response = array();
    $trip_data_array = array('from' => $input['from'],
                             'to' => $input['to'],
                             'date' => $input['date'],
                             'trip_no' => $input['trip_no']
                            );

    $trip_data      = $this->travel_destinations_model->get_search($trip_data_array);
    if(count($trip_data) > 0) {
      $trip_data = $trip_data[0];
      $from_data   = $this->bus_stop_master_model->get_stops_detail($input['from'], $this->op_id,$trip_data['from_stop_code']);
      $to_data   = $this->bus_stop_master_model->get_stops_detail($input['to'], $this->op_id,$trip_data['to_stop_code']);

      if(count($from_data) > 0 && count($to_data) > 0) {

        $all_det['from_stop']           = $from_data[0]['op_stop_cd'];
        $all_det['bus_from_stop_id']    = $from_data[0]['bus_stop_id'];
        $all_det['bus_to_stop_id']      = $to_data[0]['bus_stop_id'];
        $all_det['to_stop']             = $to_data[0]['op_stop_cd'];
        $all_det['user_type']           = $from_data[0]['op_id'];
        $all_det['service_id']          = $trip_data['op_trip_no'];
        $all_det['date'] = date('d/m/Y', strtotime($input['date']));
        $soap_name = strtolower($input['op_name'].'_api');
      
        $data = $this->seatAvailability($all_det);

        $data['error'] = array();
        if (isset($data['SeatAvailabilityResponse']) && isset($data['SeatAvailabilityResponse']['availableSeats']) && count($data['SeatAvailabilityResponse']['availableSeats']) > 0) {
          if (isset($data['SeatAvailabilityResponse']['serviceId']) && count($data['SeatAvailabilityResponse']['serviceId']) > 0) {
            $op_trip_no = $data['SeatAvailabilityResponse']['serviceId'];
            $tot_fare = $data['SeatAvailabilityResponse']['fare'];

            // Need to do proper way 
            $child_seat_fare_diffrance = $data['SeatAvailabilityResponse']['fare'] - $data['SeatAvailabilityResponse']['childFare'];

            $tot_seats = count($data['SeatAvailabilityResponse']['availableSeats']['availableSeat']);
            $update_travel_data = array();
            $update_travel_data['trip_no'] = $input['trip_no'];
            $update_travel_data['bus_from_stop_id'] = $all_det['bus_from_stop_id'];
            $update_travel_data['bus_to_stop_id'] = $all_det['bus_to_stop_id'];
            $update_travel_data['fare'] = $tot_fare;
            $update_travel_data['search_date'] = $input['date'];
            $update_travel_data['tot_seats'] = $tot_seats;
            $update_travel_data['from'] = $input['from'];
            $update_travel_data['to'] = $input['to'];
            $update_travel_data['child_seat_fare'] = isset($data['SeatAvailabilityResponse']['childFare']) ? $data['SeatAvailabilityResponse']['childFare'] : 0.00;
            $update_travel_data['senior_citizen_fare'] = isset($data['SeatAvailabilityResponse']['seniorcitizenfare']) ? $data['SeatAvailabilityResponse']['seniorcitizenfare'] : 0.00;
            
            $update_travel_destn = $this->travel_destinations_model->update_fare_totseats($update_travel_data);

            $tot_data_seat_layout = $this->op_seat_layout_model->get_seat_layout($trip_data['seat_layout_id']);
                    
            $tot_rows = $tot_data_seat_layout[0]->no_rows;
            $tot_col = $tot_data_seat_layout[0]->no_cols;
            $double_tot_rows = $tot_data_seat_layout[0]->no_rows2;
            $double_tot_col = $tot_data_seat_layout[0]->no_cols2;
            $seat_type = $tot_data_seat_layout[0]->seat_type;
            $seat_layout = array();
            $available_seats_response = $data['SeatAvailabilityResponse']['availableSeats']['availableSeat'];
            if(!is_array($data['SeatAvailabilityResponse']['availableSeats']['availableSeat'])) {
                $available_seats_response = array($data['SeatAvailabilityResponse']['availableSeats']['availableSeat']);
            }
                    
            $seat_fare_detail = array();
            $total_available_seats = 0;
            $fare_with_conveycharge = getConveyCharges($tot_fare);

            $current_actual_seat_value = $fare_with_conveycharge;
            $current_actual_seat_value_with_tax = $fare_with_conveycharge;

            $basic_fare_array = array($fare_with_conveycharge);
            $fare_array = array($fare_with_conveycharge);

            $response['actual_fare'] = implode("/",array_filter($basic_fare_array));
            $response['actual_fare_array'] = implode("/",array_filter($fare_array));
            foreach ($tot_data_seat_layout as $key => $value) {
                       
              /******************* Layout Parameteres & Description Start******************/
              $current_seat_type = "";
              $horizontal_seat = false;
              $vertical_seat = false;

              if(strtolower($value->quota_type) == "ladies") {
                $current_seat_type = "ladies";
              }
              /******************* Layout Parameteres & Description End********************/

              $pos = $value->row_no . "X" . $value->col_no;
              $birth_no = $value->berth_no;

              $seat_layout[$birth_no][$pos] = array(
                  'seat_no' => $value->seat_no,
                  'actual_seat_fare' => $current_actual_seat_value_with_tax,
                  'actual_seat_basic_fare' => $current_actual_seat_value,
                  'seat_fare' => $fare_with_conveycharge,
                  'seat_basic_fare' => $fare_with_conveycharge,
                  'seat_service_tax_amt' => $seat_service_tax_amt,
                  'seat_service_tax_perc' => $seat_service_tax_perc,
                  'op_service_charge_amt' => $op_service_charge_amt,
                  'op_service_charge_perc' => $op_service_charge_perc,
                  'row_no' => $value->row_no,
                  'col_no' => $value->col_no,
                  'seat_type' => $value->seat_type,
                  'berth_no' => $birth_no,
                  'quota' => strtolower($value->quota_type),
                  'pos' => $pos,
                  'current_seat_type' => $current_seat_type,
                  'horizontal_seat' => $horizontal_seat,
                  'vertical_seat' => $vertical_seat,
                  'seat_discount' => $current_seat_discount,
                  'agent_commission' => 0,
                  'agent_comm_tds' =>  0,
                  'child_seat_fare' => $fare_with_conveycharge - $child_seat_fare_diffrance
              ); 

              if(!empty($available_seats_response) && in_array($value->seat_no, $available_seats_response) && in_array(strtolower($value->quota_type), array("general","ladies"))) {
                $seat_layout[$birth_no][$pos]["available"] = 'Y';
                $total_available_seats++;
              }
              else {
                $seat_layout[$birth_no][$pos]["available"] = 'N';
              }

              if($seat_layout[$birth_no][$pos]["available"] == 'N' && strtolower($value->quota_type) == "ladies") {
                  $seat_layout[$birth_no][$pos]["booked_by"] = 'F';
              }
              $seat_fare_detail[$birth_no][$value->seat_no] = $seat_layout[$birth_no][$pos];
            }      
          }     
                
          $response['stop_detail'] = $this->boarding_stop_model->getstops($input['trip_no'],$all_det['from_stop'],$trip_data['op_bus_type_cd']);
          
          $response['fare'] = implode("/",array_filter($basic_fare_array));
          $response['fare_array'] = implode("/",array_filter($fare_array));
          $response['total_seats'] = $tot_seats;
          $response['total_available_seats'] = $total_available_seats;
          $response['available_seats'] = $data['SeatAvailabilityResponse']['availableSeats']['availableSeat'];
          $response['tot_rows'] = $tot_rows;
          $response['tot_cols'] = $tot_col;
          $response['tot_berth'] = 1; //Only One Berth IN UPSRTC. Please Discuss this.
          $response['seat_layout'] = $seat_layout;
          $response['seat_fare_detail'] = $seat_fare_detail;
          $response['update_travel_destn'] = $update_travel_destn;
          $response['flag'] = '@#success#@';
        }
        else {
          $response['flag'] = '@#error#@';
          $response['error'] = isset($data['SeatAvailabilityResponse']['serviceError']) ? $data['SeatAvailabilityResponse']['serviceError'] : "";
          if(isset($response['error']['errorReason']) && $response['error']['errorReason'] != "" && $response['error']['errorReason'] == "No seat available !") {
            $response['total_available_seats'] = 0;
          }
          $response['fare'] = 0;
        }
      }
      else{
        $response['fare'] = 0;    
      }
    }
    else {
      $response['flag'] = '@#error#@';
      $response['error'] = "Seat Not available for given date.";
    }
        
    $response['trip_no'] = $input['trip_no'];
    return $response;
  }


  /*
  *Author : Suraj Rathod
  *Function : getSeatAvailability() //seatAvailability() seatAvailabilityDetail() 
  *Detail : To get seat availability and other details like fare and total seats available
  */
  public function getSeatAvailability($input)
  { 
    $data = array();
    $seats_details = array();
    $trip_data      = $this->travel_destinations_model->get_search($input);
    
    if(!empty($trip_data)) {
      $trip_data = $trip_data[0];

      $from_data   = $this->bus_stop_master_model->get_stops_detail($input['from'], $this->op_id,$trip_data['from_stop_code']);
      $to_data   = $this->bus_stop_master_model->get_stops_detail($input['to'], $this->op_id,$trip_data['to_stop_code']);
      
      if(count($from_data) > 0 && count($to_data) > 0){
        $all_det['from_stop']           = $from_data[0]['op_stop_cd'];
        $all_det['bus_from_stop_id']    = $from_data[0]['bus_stop_id'];
        $all_det['bus_to_stop_id']      = $to_data[0]['bus_stop_id'];
        $all_det['to_stop']             = $to_data[0]['op_stop_cd'];
        //dont delete below commented line. we will use it later.
        $all_det['user_type']           = $from_data[0]['op_id'];

        $all_det['service_id']          = $trip_data['op_trip_no'];
        $all_det['date'] = date('d/m/Y', strtotime($input['date']));

        $data = $this->seatAvailability($all_det);
        $data['error'] = array();
        if (isset($data['SeatAvailabilityResponse']) && isset($data['SeatAvailabilityResponse']['availableSeats']) && count($data['SeatAvailabilityResponse']['availableSeats']) > 0) {
          if (isset($data['SeatAvailabilityResponse']['serviceId']) && count($data['SeatAvailabilityResponse']['serviceId']) > 0) {
              
            $available_seats = $data['SeatAvailabilityResponse']['availableSeats']['availableSeat'];
            unset($data['SeatAvailabilityResponse']['availableSeats']['availableSeat']);

            /*
             *                  VERY IMP NOTICE
             *HERE We kept 1 as hardcode for berth. Discuss this issue.
             *We dont find double deck berth in UPSRTC BUSES so dont handled condition here.
             *
             */
            $berth_no = 1;
            $data['SeatAvailabilityResponse']['availableSeats']['availableSeat'][$berth_no] = (is_array($available_seats)) ? $available_seats : array($available_seats);
            
            $tot_data_seat_layout = $this->op_seat_layout_model->get_seat_layout($trip_data['seat_layout_id']);
            
            foreach ($tot_data_seat_layout as $tkey => $tvalue) {
              $is_ladies_seat = (strtolower($tvalue->quota_type) == "ladies") ? true : false;
              $seats_details[$berth_no][$tvalue->seat_no] = array('is_ladies_seat' => $is_ladies_seat);
            }

            $data["SeatAvailabilityResponse"]["seats_details"] =  $seats_details;
            $data['flag'] = 'success';
          }
          else {
            $data['flag'] = 'error';
            $data['error'] = isset($data['SeatAvailabilityResponse']['serviceError']) ? $data['SeatAvailabilityResponse']['serviceError'] : "";
          }
        }
      }
      else {
        $data['flag'] = 'error';
        $data['error'] = 'Ooopps.... Something went wrong. Please try again.';
      }

      $data["trip_data"] = $trip_data;
      $data["from_data"] = $from_data; //useless return
      $data["to_data"] = $to_data;//useless return
    } 
    return $data;
  }

  /**
   * [do_temp_booking description] : this function use for temp booking of upsrtc 
   * @param  [type] $data_to_process [description]
   * @return [type]                  [description]
   */
  public function do_temp_booking($data_to_process) {
    
    $data  = array();
    $input = $data_to_process["input"];
      
    if(!empty($input)) {
      $available_detail = $data_to_process['available_detail'];

      $email = $input['email_id'];
      $mobile_no = $input['mobile_no'];
      $agent_email = $input['agent_email_id'];
      $agent_mobile_no = $input['agent_mobile_no'];
      $user_id = $input['user_id'];
      $service_id = $available_detail['service_id'];
      $from = $available_detail['from'];
      $to = $available_detail['to'];
      $date = $available_detail['date'];

      $tot_seat = 0;
      $seat = $input['seat'];
      foreach ($seat as $berth => $seatvalue) {
        $tot_seat += count($seatvalue);
      }
      if($tot_seat > 6) {
        redirect(base_url());
      }
      
      $fare = $available_detail['fare'];
      $child_seat_fare = $available_detail['child_seat_fare'];
      $tot_fare = 0;
      $passenger_id = 1;

      $api_data['from'] = $from;
      $api_data['to'] = $to;
      $api_data['date'] = $date;
      $api_data['service_id'] = $service_id;
      $api_data['from_stop_id'] = $available_detail['from_stop_id'];
      $api_data['to_stop_id'] = $available_detail['to_stop_id'];
      $api_data['user_type'] = $available_detail['user_type'];
      $api_data["email"] = $email;
      $api_data["mobile_no"] = $mobile_no;
      $dept_boarding_time = date("Y-m-d H:i:s",strtotime($api_data['date']." ".$available_detail['sch_dept_time']));

      // Insert Into tickets table
      $this->tickets_model->ticket_type = 1; // For In future
      $this->tickets_model->request_type = 'B';
      $this->tickets_model->ticket_status = 'A';
      $this->tickets_model->bus_service_no = $api_data['service_id'];
      $this->tickets_model->dept_time = $dept_boarding_time;
      $this->tickets_model->boarding_time = $dept_boarding_time;
      $this->tickets_model->alighting_time = $available_detail['arrival_time'];
      $this->tickets_model->droping_time = $available_detail['arrival_time'];
      $this->tickets_model->from_stop_cd = $available_detail['from_stop_id'];
      $this->tickets_model->till_stop_cd = $available_detail['to_stop_id'];
      $this->tickets_model->from_stop_name = $available_detail['from'];
      $this->tickets_model->till_stop_name = $available_detail['to'];
      $this->tickets_model->boarding_stop_name = $available_detail['boarding_stop_name'];
      $this->tickets_model->destination_stop_name = $available_detail['destination_stop_name'];
      $this->tickets_model->boarding_stop_cd = $available_detail['boarding_stop_cd'];
      $this->tickets_model->destination_stop_cd = $available_detail['destination_stop_cd'];
      $this->tickets_model->op_id = $available_detail['op_id'];
      $this->tickets_model->provider_id = $available_detail['provider_id'];
      $this->tickets_model->provider_type = $available_detail['provider_type'];
      $this->tickets_model->booked_from = "upsrtc";
      $this->tickets_model->num_passgr = $tot_seat;
      $this->tickets_model->partial_cancellation = 0; // 0 indicate partial cancellation not allowed . 1 indicate partial cancellation allowed. 
      $this->tickets_model->cancellation_rate = 0; 
      $this->tickets_model->issue_time = date("Y-m-d H:i:s");
      $this->tickets_model->pay_id = 1;
      $this->tickets_model->session_no = 1;
      $this->tickets_model->op_name = $available_detail['op_name'];
      $this->tickets_model->user_email_id = $email;
      $this->tickets_model->mobile_no = $mobile_no;
      $this->tickets_model->agent_email_id = $agent_email;
      $this->tickets_model->agent_mobile_no = $agent_mobile_no;
      $this->tickets_model->inserted_by = $this->session->userdata("booking_user_id");
      $this->tickets_model->role_id = (!empty($this->session->userdata("role_id"))) ? $this->session->userdata("role_id") : 0;
  	  $this->tickets_model->booked_by = (!empty($this->session->userdata("role_name"))) ? strtolower($this->session->userdata("role_name")) : "guest_user";
      $this->tickets_model->bus_type = (isset($available_detail['bus_type']['op_bus_type_name'])) ? $available_detail['bus_type']['op_bus_type_name'] : "";
      $this->tickets_model->status = 'Y';
      $this->tickets_model->ip_address = $this->input->ip_address();
      $this->tickets_model->tds_per = $this->config->item('tds');
      $ticket_id = $this->tickets_model->save();

      $extra_charges_value = 0;
      $total_extra_charges_value = 0;
      $total_convey_charge = 0;
      $total_ticket_discount = 0;
      $seat_service_tax_amt = 0.00;
      $seat_service_tax_perc = 0.00;
      $op_service_charge_amt = 0.00;
      $op_service_charge_perc = 0.00;
      $total_service_tax_amount = 0;
      $total_op_service_charge_amount = 0;
      $total_basic_fare = 0;    
      $tickets_total_op_service_charge_amount = 0;
      $tickets_total_service_tax = 0;
      $tickets_total_basic_fare = 0;

      //get Base commission data from base commission table 
      $base_com_data = array(
                "op_id" => $available_detail['op_id'],
                "api_provider" => $available_detail['provider_id']
            );
      $base_com_response = $this->base_commission_model->base_commission_detail($base_com_data);
      if($ticket_id > 0) {
        $psgr_no = 1;
        foreach ($seat as $berth_no => $seat_detail) {
          foreach ($seat_detail as $key => $value) {
            $value['seat_no'] = $key;
            $value['berth_no'] = $berth_no;
      
            /*
             *                  NOTICE : VERY IMP LOGIC FOR FARE
             *if is_child_concession in op_bus_types is Y then assign child seat fare
             *if is_child_concession in op_bus_types is N then assign adult seat fare 
             */
            if(isset($available_detail['is_child_concession']) && $available_detail['is_child_concession'] == "Y") {
              if($value['age'] < 5) {
                $value['fare'] = 0.00;
              }
              else if($value['age'] >= 5 && $value['age'] <= 12) {
                $value['fare'] = $child_seat_fare;
              }
              else {
                $value['fare'] = $fare;
              }
            }
            else {
              $value['fare'] = $fare;
            }

            /*
            *NOTICE: MOST IMP
            *
            *DO NOT DELETE BELOW COMMENTED LINES. THESE ARE IMP.
            *BELOW WRITTEN LOGIC IS STATIC CONVEY CHARGES LOGIC.
            *THE LOGIC WRITTEN BELOW IS TEMP ALTERNATIVE FOR DYNAMIC LOGIC
            *CHANGE THIS AFTER DISCUSSION
            */
            /**************CONVEY CHARGE STATIC LOGIC START *****************/
            $three_percent_tot_fare = 0;
            $fifty_per_ticket = 0;
            $extra_charges_value = 0;
            $convey_charge_type = "";
            $convey_charge_value = "";

            $three_percent_tot_fare = ($value['fare'] * PERCENT_TICKET_CONVEY_CHARGE)/100 ;
            $fifty_per_ticket = (PER_TICKET_CONVEY_CHARGE);
            
            if($three_percent_tot_fare > $fifty_per_ticket) {
              $extra_charges_value = $three_percent_tot_fare;
              $convey_charge_type = "percentage";
              $convey_charge_value = PERCENT_TICKET_CONVEY_CHARGE;
            }
            else {
              $extra_charges_value = $fifty_per_ticket;
              $convey_charge_type = "fix";
              $convey_charge_value = PER_TICKET_CONVEY_CHARGE;
            }

            $total_extra_charges_value += $extra_charges_value;
            /**************CONVEY CHARGE STATIC LOGIC END *****************/

            $current_seat_fare_with_conveycharges = $value['fare']+$extra_charges_value;
	
  					// For Discount Calculation on Final Ticket Base fare
  					$current_seat_fare_with_conveycharges_for_discount += $current_seat_fare_with_conveycharges;
            
            $tickets_total_op_service_charge_amount += $op_service_charge_amt;
            $tickets_total_service_tax += $seat_service_tax_amt;
            
            $tot_fare += $value['fare'];
            $total_basic_fare += $value['fare'];
            $api_data['passenger'][] = $value;
            // Insert into ticket_details tables
            $this->ticket_details_model->ticket_id = $ticket_id;
            $this->ticket_details_model->psgr_no = $psgr_no;
            $this->ticket_details_model->psgr_name = $value['name'];
            $this->ticket_details_model->psgr_age = $value['age'];
            $this->ticket_details_model->psgr_sex = $value['gender'];
            $this->ticket_details_model->psgr_type = ($value['age'] <= 12 && isset($available_detail['is_child_concession']) && $available_detail['is_child_concession'] == "Y") ? "C" : "A";
            $this->ticket_details_model->concession_cd = 1; // if exists /in case of rsrtc 60 pass ct 
            $this->ticket_details_model->concession_rate = 1; // from api /if exists /in case of rsrtc 60 pass ct 
            $this->ticket_details_model->discount = 1; // if available from api
            $this->ticket_details_model->is_home_state_only = 1; // Y or No //not required now
            $this->ticket_details_model->fare_amt = $value['fare'];
            $this->ticket_details_model->convey_name = "Convenience charge";
            $this->ticket_details_model->convey_type = $convey_charge_type;
            $this->ticket_details_model->convey_value = $convey_charge_value;
            $this->ticket_details_model->fare_convey_charge = $extra_charges_value;                    
            $this->ticket_details_model->seat_no = $value['seat_no'];
            $this->ticket_details_model->berth_no = $value['berth_no'];
            $this->ticket_details_model->seat_status = 'Y';
            $this->ticket_details_model->concession_proff = 1;  // if cct then required // voter id
            $this->ticket_details_model->proff_detail = 1; // if varified
            $this->ticket_details_model->status = 'Y';
            $this->ticket_details_model->type = $convey_charge_type;
            $this->ticket_details_model->commission_percent = PERCENT_TICKET_CONVEY_CHARGE;
            $this->ticket_details_model->op_commission = $extra_charges_value;

            $tds_on_commission = round($extra_charges_value * ((float)($this->config->item('tds')/100)),5);
            $final_commission = $extra_charges_value - $tds_on_commission;

            $this->ticket_details_model->tds_on_commission =  $tds_on_commission;
            $this->ticket_details_model->final_commission =  $final_commission;
            $psgr_no++;

            $ticket_details_id = $this->ticket_details_model->save();
          }
        }
              
        $soap_name = strtolower($available_detail['op_name']) . '_api';

        $api_data["ticket_id"] = $ticket_id;
        $tb_data = $this->temporary_booking($api_data);
        $data['ticket_id'] = $ticket_id;
        if(!empty($tb_data)) {
          $temp_booking_status = $tb_data['status'];
          if ($temp_booking_status == 'FAILURE') {
            $data['block_key'] = "";
            $data['status'] = $tb_data['status'];
            $data['error'] = $tb_data['serviceError']['errorReason'];
            $data['payment_confirm_booking_status'] = "error";
          } 
          else if($temp_booking_status == 'SUCCESS') {
            $total_convey_charge = 0;
            $insert_ttrans_array = array();
            $tickets_total_fare = $tot_fare; //$tickets_total_fare has decalred because in some case we had changed $tot_fare value  
            $total_fare_without_discount = $tot_fare + $total_extra_charges_value; 
                  
            $data['discount_value'] = $total_ticket_discount;
            $final_total_amount = $total_fare_without_discount-$data['discount_value'];
            /*******temp discount logic end******/
            $tax_charges = array();
                    
            $data['status'] = $temp_booking_status;
            $this->tickets_model->ticket_id = $ticket_id;
            $this->tickets_model->ticket_ref_no = $tb_data['blockKey'];
              
            if(isset($tb_data['passengerDetails']['passenger']['fareDetail'])) {
              $api_reponse_data = $tb_data['passengerDetails']['passenger']['fareDetail'];
              $amount_to_add = 0.00;
              if($tb_data['passengerDetails']['passenger']['age'] >= 5 && $tb_data['passengerDetails']['passenger']['age'] <= 12) {
                $amount_to_add = (isset($api_reponse_data['childFare'])) ? $api_reponse_data['childFare'] : 0.00;
              }
              else {
                $amount_to_add = (isset($api_reponse_data['adultFare'])) ? $api_reponse_data['adultFare'] : 0.00;
              }
              $tickets_total_basic_fare += $amount_to_add;
            }
            else {
              $api_reponse_data = $tb_data['passengerDetails']['passenger'][0]['fareDetail'];       
              foreach ($tb_data['passengerDetails']['passenger'] as $psgkey => $psgvalue) {
                $amount_to_add = 0.00;
                if($psgvalue['age'] >= 5 && $psgvalue['age'] <= 12) {
                  $amount_to_add = (isset($psgvalue["fareDetail"]['childFare'])) ? $psgvalue["fareDetail"]['childFare'] : 0.00;
                }
                else {
                  $amount_to_add = (isset($psgvalue["fareDetail"]['adultFare'])) ? $psgvalue["fareDetail"]['adultFare'] : 0.00;
                }
                $tickets_total_basic_fare += $amount_to_add;
              }
            }
                     
            $this->tickets_model->total_basic_amount = $tickets_total_basic_fare;
            $this->tickets_model->op_service_charge = $tickets_total_op_service_charge_amount;
            $this->tickets_model->service_tax = $tickets_total_service_tax;
            $this->tickets_model->tot_fare_amt = $tot_fare;
            $this->tickets_model->tot_fare_amt_with_tax = $final_total_amount;
            $this->tickets_model->total_fare_without_discount = $total_fare_without_discount;
            $this->tickets_model->fare_convey_charge = $total_extra_charges_value;
            $this->tickets_model->transaction_status = "temp_booked";
            $this->tickets_model->save();
           
            /*******To Update ticket fare details in ticket details tables Start ********/ 
            $update_fare_details = array("adult_basic_fare" => (isset($api_reponse_data['adultFare'])) ? $api_reponse_data['adultFare'] : 0.00,
                                         "child_basic_fare" => (isset($api_reponse_data['childFare'])) ? $api_reponse_data['childFare'] : 0.00,
                                         "fare_acc" => (isset($api_reponse_data['acc'])) ? $api_reponse_data['acc'] : 0.00,
                                         "fare_hr" => (isset($api_reponse_data['hr'])) ? $api_reponse_data['hr'] : 0.00,
                                         "fare_it" => (isset($api_reponse_data['it'])) ? $api_reponse_data['it'] : 0.00,
                                         "fare_octroi" => (isset($api_reponse_data['octroi'])) ? $api_reponse_data['octroi'] : 0.00,
                                         "fare_other" => (isset($api_reponse_data['other'])) ? $api_reponse_data['other'] : 0.00,
                                         "fare_reservationCharge" => (isset($api_reponse_data['reservationCharge'])) ? $api_reponse_data['reservationCharge'] : 0.00,
                                         "fare_sleeperCharge" => (isset($api_reponse_data['sleeperCharge'])) ? $api_reponse_data['sleeperCharge'] : 0.00,
                                         "fare_toll" => (isset($api_reponse_data['toll'])) ? $api_reponse_data['toll'] : 0.00,
                                        );

            $this->ticket_details_model->update_where(array("ticket_id" => $ticket_id),"",$update_fare_details);
            /*******To Update ticket fare details in ticket details tables End ********/ 

            $data['block_key'] = $tb_data['blockKey'];
            $currentTime = strtotime(date("Y-m-d H:i:s"));
            $timeBlock = $currentTime+(60*$tb_data['blockTime']);
            $timeToBlock = date("Y-m-d H:i:s", $timeBlock);

            $data['fare_charges'] = $api_reponse_data;
            $data['block_time_minutes'] = $tb_data['blockTime'];
            $data['block_current_time'] = $currentTime;
            $data['time_to_block'] = $timeToBlock;
            $data['ttb_year'] = date("Y", $timeBlock);
            $data['ttb_month'] = date("m", $timeBlock);
            $data['ttb_day'] = date("d", $timeBlock);
            $data['ttb_hours'] = date("H", $timeBlock);
            $data['ttb_minutes'] = date("i", $timeBlock);
            $data['ttb_seconds'] = date("s", $timeBlock);
            $data['total_fare'] = $final_total_amount;
            $data["total_fare_without_discount"] =  $total_fare_without_discount;
            $data["ticket_fare"] =  $tot_fare;
            $data["total_seats"] = $tot_seat;
            $data["tax_charges"] =  $tax_charges;
            $data['payment_confirm_booking_status'] = "success";
            $data['ticket_id'] = $ticket_id;
          }     
        }
        else {
          $data['payment_confirm_booking_status'] = "error";
        }
      }
      else {
        $data['payment_confirm_booking_status'] = "error";
      }
    }
    return $data;
  }

  function final_booking($api_data, $data) {
    $final_response = array();
    $tbdata = $this->doconfirm_booking($api_data);
    /************* Actual ticket booking End Here ***********/

    if(trim($tbdata['status']) == 'SUCCESS') {   
      $onwards_ticket_ref_no = $data["ticket_ref_no"];
      
      $data["ticket_pnr"] = $tbdata["pnr"];
      $transaction = array('transaction_status' => "success",
                              'payment_by' => $api_data["payment_by"],
                              'payment_mode' => $data["pg_response"]["payment_mode"],
                            );
      $this->tickets_model->update($data["ticket_id"],$transaction);
      //common model
      $pg_success_response = $this->common_model->transaction_process($data);
      $final_response["transaction_process_response"] = $pg_success_response;

      //make this function common afterwards. Because this is used 2 times (1 times in pg and 1 time in wallet)
      if(trim($pg_success_response["msg_type"]) == "success")
      {
        $rpt_data = $api_data["rpt_data"];
        if(count($rpt_data)) {
          $onward_ticket_id = $pg_success_response["ticket_id"];

          $rtransaction = array('is_return_journey' => "1",
                                'return_ticket_id' => $rpt_data[0]->ticket_id
                                );

          $this->tickets_model->update($onward_ticket_id,$rtransaction);

          /************* Actual ticket booking Start Here ***********/
          $rapi_data['ticket_id']      = str_pad($rpt_data[0]->ticket_id, 8, '0', STR_PAD_LEFT);
          $rapi_data['ticket_ref_no']  = $rpt_data[0]->ticket_ref_no;
          $rapi_data['seat_count']     = $rpt_data[0]->num_passgr;

          $soap_name = strtolower($rpt_data[0]->op_name) . '_api';
          $rtbdata = $this->doconfirm_booking($rapi_data);
          /************* Actual ticket booking End Here ***********/

          if($rtbdata['status'] == 'SUCCESS') 
          {
              $data["ticket_ref_no"] = $rpt_data[0]->ticket_ref_no;
              $data["ticket_pnr"] = $rtbdata["pnr"];
              $data["ticket_id"] = $rpt_data[0]->ticket_id;
              
              $atransaction = array('transaction_status' => "success",
                                     'payment_by' => $api_data["payment_by"],
                                     'payment_mode' => $data["pg_response"]["payment_mode"],
                                    );

              $this->tickets_model->update($rpt_data[0]->ticket_id,$atransaction);

              $final_response["return_ticket_status"] = "success";

              $rpg_success_response = $this->common_model->transaction_process($data);
              $final_response["transaction_process_response"] = $rpg_success_response;
              
              if(trim($rpg_success_response["msg_type"]) == "success")
              {
                if(isset($data['is_back']) && $data['is_back'])
                {
                  $url_redirect = base_url()."admin/reservation/view_ticket/".$onwards_ticket_ref_no."/".$rpt_data[0]->ticket_ref_no;  
                }
                else
                {
                  $url_redirect = base_url()."front/booking/view_ticket/".$onwards_ticket_ref_no."/".$rpt_data[0]->ticket_ref_no;
                }

                $final_response["status"] = "success";
                $final_response["redirect_url"] = $url_redirect;
              }
              else if(trim($rpg_success_response["msg_type"]) == "error")
              {
                                     
                $transaction = array('payment_by' => $api_data["payment_by"],
                                     'failed_reason' => (isset($rpg_success_response["msg"])) ? $rpg_success_response["msg"] : "some error occured in msrtc library"
                                    );

                $id = $this->tickets_model->update($rpt_data[0]->ticket_id,$transaction);

                $final_response["status"] = "error";
                $final_response["return_transaction_process_api"] = "error";
                $final_response["data"] = $data;
              }
          }
          else
          {
            if($rpt_data[0]->transaction_status != "success")
            {
              $transaction = array('transaction_status' => "failed",
                                   'failed_reason' => "upsrtc return ticket api failed",
                                   'payment_by' => $api_data["payment_by"]
                                  );

              $return_ticket_update_where = ["ticket_id" => $rpt_data[0]->ticket_id,"transaction_status != " => "success"];
              $id = $this->tickets_model->update($return_ticket_update_where,$transaction);
            }
            else
            {
              $this->common_model->developer_confirm_booking_error_mail("UPSRTC (Return Journey)",$rpt_data[0]->ticket_id);
            }

            $final_response["return_ticket_status"] = "error";
            $final_response["return_ticket_api_failed_reason"] = "api failed";

            $final_response["status"] = "success";
            $final_response["redirect_url"] = $pg_success_response["redirect_url"];
          }
        }
        else {
            $final_response["status"] = "success";
            $final_response["redirect_url"] = $pg_success_response["redirect_url"];
        }
      }
      else if(trim($pg_success_response["msg_type"]) == "error")
      {
        $transaction = array('failed_reason' => 'failed message - '.(isset($pg_success_response["msg"])) ? $pg_success_response["msg"] : "",
                           'payment_by' => $api_data["payment_by"]
                          );

        $id = $this->tickets_model->update($api_data['ticket_id'],$transaction);
        $final_response["status"] = "success";
        $final_response["redirect_url"] = $pg_success_response["redirect_url"];

        logging_data("condition_not_handled/".date("M")."/upsrtc_".date("d-m-Y")."_log.log",
                   "Ticket booked successfully but some error occured in transaction_process_api for ticket is ".$data["ticket_id"],
                    'Transaction Process Status');
      }
    }
    else
    {
      if($api_data['pt_data']->transaction_status != "success") {
        $transaction = array('transaction_status' => "failed",
                             'failed_reason' => 'upsrtc doconfirm_booking api failed',
                             'payment_by' => $api_data["payment_by"]
                            );

        $update_where = ["ticket_id" => $api_data['pt_data']->ticket_id, "transaction_status != " => "success"];

        $id = $this->tickets_model->update($update_where,$transaction);

        $find_return_ticket = array();
        $find_return_ticket = $this->return_ticket_mapping_model->where("ticket_id",$api_data['ticket_id'])->find_all();
        
        if(!empty($find_return_ticket) && $find_return_ticket[0]->return_ticket_id > 0) {
          $return_update_where = ["ticket_id" => $find_return_ticket[0]->return_ticket_id, "transaction_status != " => "success"];
          $rid = $this->tickets_model->update($return_update_where,$transaction);
        }
      }
      else {
        $this->common_model->developer_confirm_booking_error_mail("UPSRTC",$api_data['pt_data']->ticket_id);
      }
      
      $final_response["status"] = "error";
      $final_response["api_failed_reason"] = "api failed";
      $final_response["data"] = $data;
    }

    return $final_response;
  }
  /******************************* FOR BOOKING PROCESS END *****************************/  
  
  public function getBalance() {
    $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
                  <soapenv:Header/>
                     <soapenv:Body>
                        <ser:getBalance>
                           <!--Optional:-->
                           <UserBalanceRequest>
                           <!--Optional:-->
                           <userName>'.$this->user_id.'</userName>
                           <!--Optional:-->
                           <password>'.$this->pass.'</password>
                           <!--Optional:-->
                           <userType>'.$this->user_type.'</userType>
                            </UserBalanceRequest>
                      </ser:getBalance>
                   </soapenv:Body>
                </soapenv:Envelope>';

    logging_data("ors/balance/upsrtc/".date("F")."/balance_".date("d-m-Y")."_log.log",$xml_str, 'Balance');
    
    $data = get_soap_data($this->url, 'getBalance', $xml_str);
    logging_data("ors/balance/upsrtc/".date("F")."/balance_".date("d-m-Y")."_log.log",$data, 'Balance');

    return $data;
  }

  public function isTicketCancelable($ticket_pnr) {
    $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <ser:isTicketCancelable>
                         <!--Optional:-->
                         <IsTicketCancelableRequest>
                            <!--Optional:-->
                            <authentication>
                               <!--Optional:-->
                               <userName>'.$this->user_id.'</userName>
                               <!--Optional:-->
                               <password>'.$this->pass.'</password>
                               <!--Optional:-->
                               <userType>'.$this->user_type.'</userType>
                            </authentication>
                            <!--Optional:-->
                            <pnr>'.$ticket_pnr['pnr_no'].'</pnr>
                            <!--Optional:-->
                            <seatNumbers></seatNumbers>
                         </IsTicketCancelableRequest>
                      </ser:isTicketCancelable>
                   </soapenv:Body>
                </soapenv:Envelope>';

    logging_data("ors/check_cancellation/upsrtc/".date("F")."/check_cancellation_".date("d-m-Y")."_log.log",$xml_str, 'check_cancellation');
    $data = get_soap_data($this->url, 'isTicketCancelable', $xml_str);
    logging_data("ors/check_cancellation/upsrtc/".date("F")."/check_cancellation_".date("d-m-Y")."_log.log",$data, 'check_cancellation');
    $data_to_log = [
                    "ticket_id" => isset($ticket_pnr['current_ticket_information']["ticket_id"]) ? $ticket_pnr['current_ticket_information']["ticket_id"] : "",
                    "request" => $xml_str,
                    "type" => "is_ticket_cancelable",
                    "response" => json_encode($data),
                   ];

    insert_request_response_log($data_to_log);

    return $data;
  }

  public function is_ticket_cancellable() {
    $input = $this->input->post();
    if(!empty($input) && isset($input["pnr_no"]) && $input["pnr_no"] != "")
    {
      $response  = [];
      $data = $this->isTicketCancelable($input);

      if(isset($data["IsTicketCancelableResponse"]) && strtolower($data["IsTicketCancelableResponse"]["status"]) == "success")
      {
        if($data["IsTicketCancelableResponse"]["cancellable"] )
        {
          $response["status"]                 = "success";
          $response["refundAmount"]           = $data["IsTicketCancelableResponse"]["refundAmount"];
          $response["cancellationCharge"]     = $data["IsTicketCancelableResponse"]["cancellationCharge"];
        }
        else
        {
          $response["status"]                 = "error";
          $response["error_message"]          = "Ticket is non-cancellable.";
        }
      }
      else
      {
        $response["status"]                 = "error";
        $response["error_message"]          = "This ticket cannot be cancelled. Please contact customer support.";
      }
      
      return $response;
    }
    else
    {
      return false;
    }
  }

  function getBusServiceStops_bus_service_wise($service_date, $service_id)
  { 
    logging_data("manually_bus_service_release/upsrtc/".date("Y-m-d").".log","Service Id - ".$service_id." and service date ".$service_date, "Manual Services");
    $all_services  = $this->getAvailableServices_bus_service_wise(date("d/m/Y",strtotime($service_date)));

    if(isset($all_services[$service_id])) {
      $current_bus_service  = $all_services[$service_id];
    }
    else {
      show("Service Id - ".$service_id." Not Found for the date given",1);
    }

    $xml_str  =  '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
                  <soapenv:Header/>
                      <soapenv:Body>
                        <ser:getBusServiceStops>
                           <!--Optional:-->
                           <BusServiceStopsRequest>
                              <!--Optional:-->
                              <authentication>
                                 <!--Optional:-->
                                 <userName>'.$this->user_id.'</userName>
                                 <!--Optional:-->
                                 <password>'.$this->pass.'</password>
                                 <!--Optional:-->
                                 <userType>'.$this->user_type.'</userType>
                              </authentication>
                              <serviceId>'.$service_id.'</serviceId>
                              <!--Optional:-->
                              <dateOfJourney>'.date("d/m/Y",strtotime($service_date)).'</dateOfJourney>
                           </BusServiceStopsRequest>
                        </ser:getBusServiceStops>
                     </soapenv:Body>
                 </soapenv:Envelope>';
      
    $response   = get_soap_data($this->url,  'getBusServiceStops', $xml_str);

    $this->db->trans_begin();
    
    if($response && is_array($response) && count($response) >0 )
    {
      $data       = $this->upsrtc_model->insert_bus_service_stops_bus_service_wise($response,$this->op_id, date("d/m/Y",strtotime($service_date)) , $service_id, $current_bus_service);
      
      if ($this->db->trans_status() === FALSE) {
          show(error_get_last());
          $this->db->trans_rollback();
      }
      else {
          $this->db->trans_commit();
      }
    }
  }

  function getAvailableServices_bus_service_wise($date)
  { 
    $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <ser:getAvailableServices>
                         <!--Optional:-->
                         <AvailableServiceRequest>
                            <!--Optional:-->
                            <authentication>
                               <!--Optional:-->
                               <userName>'.$this->user_id.'</userName>
                               <!--Optional:-->
                               <password>'.$this->pass.'</password>
                               <!--Optional:-->
                               <userType>'.$this->user_type.'</userType>
                            </authentication>
                            <!--Optional:-->
                            <dateOfJourney>'.$date.'</dateOfJourney>
                         </AvailableServiceRequest>
                      </ser:getAvailableServices>
                   </soapenv:Body>
                </soapenv:Envelope>';

    $data = get_soap_data($this->url,  'getAvailableServices', $xml_str);
  
    if(isset($data["AvailableServiceResponse"]["availableServices"]["service"]) && count($data["AvailableServiceResponse"]["availableServices"]["service"]) > 0)
    {
      $services   = $data["AvailableServiceResponse"]["availableServices"]["service"];

      foreach ($services as $key => $value)
      {
        $response[$value["serviceId"]]  = $value;
      }
    }
    return $response;

  }
  // to send the cancel ticket start here//
  public function mail_cancel_ticket($tinfo,$data,$ptd_data) { 
    foreach ($ptd_data as $key => $value) {
      $passanger_detail .= '<tr style="border:1px solid #000;">
                            <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">
                              '.ucwords($value->psgr_name).'
                            </td>
                            <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                              '.ucwords($value->psgr_age).'
                            </td>
                            <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                              '.$adult_child.'
                            </td>
                            <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                              '.ucwords($value->psgr_sex).'
                            </td>
                            <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                              '.$value->seat_no.'
                            </td>
                            <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                              NA
                            </td>
                          </tr>';
      $psgr_no++; 
    }
    $cancellation_policy_to_show = "";
    if(isset($pt_data[0]->cancellation_policy) && $pt_data[0]->cancellation_policy != "") {
      $cpolicy = $pt_data[0]->cancellation_policy != '' ? json_decode($pt_data[0]->cancellation_policy) : array();
      if(!empty($cpolicy)) {
        foreach ($cpolicy as $ckey => $cvalue) {
          if(!empty($cvalue)) {
                    $charges_in_per = "";
                    $charges_in_per = 100 - $cvalue->refundInPercentage;
                    $cancellation_policy_to_show .= '<tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;">
                                                      <td style="padding:3px;">Cancellation within '.$cvalue->cutoffTime.' hrs</td>
                                                      <td style="padding:3px;">'.$charges_in_per.'%</td>
                                                    </tr>';
          }
        }
      }
    }
    if($pt_data[0]->droping_time != "" && $pt_data[0]->droping_time != "0000-00-00 00:00:00") {
      $droping_time = date("h:i A",strtotime($pt_data[0]->droping_time));
    }
    else {
      $droping_time = date("h:i A",strtotime($pt_data[0]->alighting_time));
    }

    if($pt_data[0]->boarding_time != "" && $pt_data[0]->boarding_time != "0000-00-00 00:00:00") {
      $bus_boarding_time = date("h:i A",strtotime($pt_data[0]->boarding_time));
    }
    else {
      $bus_boarding_time = "NA";
    }
    $agent_details = "";
    if(!empty($this->session->userdata("user_id"))) {
      eval(AGENT_ID_ARRAY);
      if(in_array($this->session->userdata("role_id") , $agent_id_array)) {
        $user_agent_details  = $this->users_model
                                    ->where(["id" => $this->session->userdata("user_id")])
                                    ->find_all();
        if($user_agent_details) {
          $agent_firm_name = ($user_agent_details[0]->firm_name != "") ? $user_agent_details[0]->firm_name : $user_agent_details[0]->first_name." ".$user_agent_details[0]->last_name;
          $agent_details = "<tr>
                <td colspan=2' align='right' valign='top' style='color:#000;font-weight:bold; font-size:13px; text-align:centre; padding-top:7px;'>
                <span style='color:#ff940a;'>
                ".$agent_firm_name."</span><br />Agent Contact No. ".$user_agent_details[0]->mobile_no."</td>
              </tr>";
        }
      }
    }
    $mail_replacement_array = array('{{bank_transaction_no}}' => "NA",
                                    "{{pnr_no}}" => $tinfo['pnr_no'],
                                    "{{ticekt_no}}" => $tinfo['ticket_id'],
                                    "{{bos_ref_no}}" => $tinfo['boss_ref_no'],
                                    "{{service_end_place}}" => strtoupper($tinfo['till_stop_name']),
                                    "{{reservation_from}}" => strtoupper($tinfo['from_stop_name']),
                                    "{{res_frm_cd}}" => strtoupper($tinfo['boarding_stop_cd']),
                                    "{{reservation_to}}" => strtoupper($tinfo['till_stop_name']),
                                    "{{res_to_cd}}" => strtoupper($tinfo['till_stop_cd']),
                                    "{{boarding_pont}}" => $tinfo['boarding_stop_name'],
                                    "{{boarding_cd}}" => $tinfo['boarding_stop_cd'],
                                    "{{alighting_point}}" => $tinfo['destination_stop_name'],
                                    "{{alighting_cd}}" => $tinfo['destination_stop_cd'],
                                    "{{doj}}" =>date("D, d M Y, h:i A",strtotime($tinfo['dept_time'])),
                                    "{{dpt_tm}}" => date("h:i A",strtotime($tinfo['dept_time'])),
                                    "{{boarding_time}}" => date("h:i A",strtotime($tinfo['dept_time'])),
                                    "{{alighting_time}}" => date("h:i A",strtotime($tinfo['alighting_time'])),
                                    "{{bus_service_type}}" => $tinfo['bus_service_no'],
                                    "{{no_of_seats}}" =>$tinfo['num_passgr'],
                                    "{{psgr_email_id}}" => $tinfo['user_email_id'],
                                    "{{psgr_contact_no}}" => $tinfo['mobile_no'],
                                    "{{depot_name}}" => "NA",
                                    "{{total_chargeable_amt}}"  =>$tinfo['total_fare_without_discount'],
                                    "{{refund_amount}}"  =>$data['refund_amt'],
                                    "{{support_email_id}}" => 'online.support@upsrtc.com',
                                    "{{psgr_list}}" => $passanger_detail
                                );
    /************ Mail Start Here ********/
    $email = $tinfo['user_email_id'];
    $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),UPSRTC_CANCEL_TICKET_TEMPLATE);

    $mailbody_array = array("subject" => "Book On Spot Cancel Ticket",
                            "message" => $data['mail_content'],
                            "to" =>  $email
                            );
    $mail_result = send_mail($mailbody_array);

    //****if agent booked tkt send mail to both agent as well as customer****//
    $email_agent = $tinfo['agent_email_id'];
    if($email_agent != "" && ($email_agent != $email)) {
        $agent_mailbody_array = array("subject" => "Book On Spot Cancel Ticket",
                              "message" => $data['mail_content'],
                              "to" =>  $email_agent
                              );
         $agent_mail_result = send_mail($agent_mailbody_array);
    }
    //****************************** Mail End Here ******************//
    /************************ Update in Ticket_dump table start here *****************************/
    $update_in_ticket_dump = $this->ticket_dump_model->update_where(array('ticket_id' => $tinfo['ticket_id']),'',array('cancel_ticket' => $data['mail_content']));

    /************************ Update in Ticket_dump table end here *****************************/
  }// to send the cancel ticket end here
  //mail cancel Ticket End here //
  
  public function is_ticket_cancellable_fullpartial($request) {
    $input = $this->input->post();
    if(!empty($input["seat_no"]) && !is_array($input["seat_no"])) {
      $input["seat_no"] = explode(",", $input["seat_no"]);
    }

    if(!empty($input) && isset($input["pnr_no"]) && $input["pnr_no"] != "") {
      $response  = [];
      $data = $this->isPartialTicketCancelable($input);

      if(isset($data["PartialCancellationResponse"])) {
        if($data["PartialCancellationResponse"]["cancellable"] ) {
          $response["status"]                 = "success";
          $response["refundAmount"]           = $data["PartialCancellationResponse"]["refundAmount"];
          $response["cancellationCharge"]     = $data["PartialCancellationResponse"]["cancellationCharge"];
        }
        else {
          $response["status"]                 = "error";
          $response["error_message"]          = "Ticket is non-cancellable.";
        }
      }
      else {
        $response["status"]                 = "error";
        $response["error_message"]          = "This ticket cannot be cancelled. Please contact customer support.";
      }
      
      return $response;
    }
    else {
      return false;
    }
  }

  public function isPartialTicketCancelable($ticket_no) {
    $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
                  <soapenv:Header/>
                    <soapenv:Body>
                      <ser:ispartialTicketCancelable>
                        <!--Optional:-->
                          <PartialCancellationRequest>
                        <!--Optional:-->
                          <authentication>
                               <!--Optional:-->
                              <userName>'.$this->user_id.'</userName>
                               <!--Optional:-->
                               <password>'.$this->pass.'</password>
                               <!--Optional:-->
                               <userType>'.$this->user_type.'</userType>
                            </authentication>
                            <!--Optional:-->
                            <pnr>'.$ticket_no['pnr_no'].'</pnr>
                            <canceltypes>2</canceltypes>
                            <!--Optional:-->
                            <seatnoDetails>
                              <!--Zero or more repetitions:-->';
                              foreach ($ticket_no["seat_no"] as $key => $value)
                            {
                  $xml_str  .=  '<seatno>
                                  <seatNumber>'.$value.'</seatNumber>
                                </seatno>';
                            }
              $xml_str  .=  '</seatnoDetails>
                         </PartialCancellationRequest>
                        </ser:ispartialTicketCancelable>
                      </soapenv:Body>
                    </soapenv:Envelope>';

    logging_data("ors/check_cancellation/upsrtc/".date("F")."/check_cancellation_".date("d-m-Y")."_log.log",$xml_str, 'check_cancellation');
    $data = get_soap_data($this->url, 'ispartialTicketCancelable', $xml_str);
    logging_data("ors/check_cancellation/upsrtc/".date("F")."/check_cancellation_".date("d-m-Y")."_log.log",$data, 'check_cancellation');
    return $data;
  }

  
    function api_get_services($post) {
        $data = $this->travel_destinations_model->get_search_op_wise($post);

        $cancel_policy_data = $this->cancellation_charges_model->cancellation_charges($this->op_id);
        $cancel_policy_msrtc_data = $this->cancellation_charges_model->cancellation_charges(3);
        // $cancel_policy_data = to_array($cancel_policy);

        $cancel_policy = [];
        foreach ($cancel_policy_data as $key => $value) {
            $hour = round(($value->from / 3600), 2);

            $temp = new stdClass;

            $temp->cutoffTime = $hour;
            $temp->refundInPercentage = 100 - $value->cancel_rate;

            // $cancel_policy[] = 'Cancellation upto '.$hour.' hrs  Cancellation Charges is '.$value->cancel_rate.'%';  

            $cancel_policy[] = $temp;
        }

        $cancel_policy_msrtc = [];
        foreach ($cancel_policy_msrtc_data as $key => $value) {
            $hour = round(($value->from / 3600), 2);

            $temp = new stdClass;

            $temp->cutoffTime = $hour;
            $temp->refundInPercentage = 100 - $value->cancel_rate;

            // $cancel_policy[] = 'Cancellation upto '.$hour.' hrs  Cancellation Charges is '.$value->cancel_rate.'%';  

            $cancel_policy_msrtc[] = $temp;
        }
        foreach ($data as $key => $value) {
            $data[$key]['id_proof_required'] = false;
            $data[$key]['mticket_allowed'] = true;
            $data[$key]['routeScheduleId'] = 0;
            $data[$key]['inventory_type'] = 0;

            if (strtolower($value['op_name']) == "upsrtc") {
                $data[$key]['seat_fare_with_tax'] = getConveyCharges($value['seat_fare']);
                $data[$key]['operator'] = "upsrtc";
                $farearray = explode(",", getConveyCharges($value['seat_fare']));
            } else if (strtolower($value['op_name']) == "msrtc") {
                $data[$key]['seat_fare_with_tax'] = $value['seat_fare'];
                $data[$key]['operator'] = "msrtc";
                $farearray = explode(",", $value['seat_fare']);
            } else if (strtolower($value['op_name']) == "rsrtc") {
                $extra_value = ["op_bus_type_cd" => $value['op_bus_type_cd']];
                $data[$key]['seat_fare_with_tax'] = getConveyCharges($value['seat_fare'], strtolower($value['op_name']), $extra_value);
                $data[$key]['operator'] = "rsrtc";
                $farearray = explode(",", getConveyCharges($value['seat_fare'], strtolower($value['op_name']), $extra_value));
            }
            $discount_detail = array();
            foreach ($farearray as $k => $v) {
                $discount_detail[] = $this->discounts->discount_calculation($farearray[$k], $farearray[$k], $value['bus_type_id'], $value['op_id'], BOS_MOBILE_API_USER_ID);
            }
            if (!empty($discount_detail)) {
                $data[$key]['seat_fare_with_tax_with_discount'] = '';
                foreach ($discount_detail as $key1 => $value1) {
                    if (!empty($discount_detail[$key1]['seat_fare_with_tax_with_discount']))
                        $data[$key]['seat_fare_with_tax_with_discount'] .= $discount_detail[$key1]['seat_fare_with_tax_with_discount'] . ',';
                }
                if (!empty($data[$key]['seat_fare_with_tax_with_discount']))
                    $data[$key]['seat_fare_with_tax_with_discount'] = rtrim($data[$key]['seat_fare_with_tax_with_discount'], ',');
                else
                    $data[$key]['seat_fare_with_tax_with_discount'] = $data[$key]['seat_fare_with_tax'];
                if (!empty($discount_detail[0]['discount_type']))
                    $data[$key]['discount_type'] = $discount_detail[0]['discount_type'];
                else
                    $data[$key]['discount_type'] = 'NA';
                if (!empty($discount_detail[0]['discount_on']))
                    $data[$key]['discount_on'] = $discount_detail[0]['discount_on'];
                else
                    $data[$key]['discount_on'] = 'NA';
                if (!empty($discount_detail[0]['discount_rate']))
                    $data[$key]['discount_flat_rate'] = $discount_detail[0]['discount_rate'];
                else
                    $data[$key]['discount_flat_rate'] = 'NA';
            }
            // show($value,1);
            $param = array(
                'service_id' => $value['service_id'],
                'bus_type_code' => $value['op_bus_type_cd'],
                'operator' => $value['op_name'],
                'from' => $value['from_stop'],
                'to' => $value['to_stop']
            );

            $brs_ali = false;
            $brs_ali = $this->apiBoardingStop($param);

            // show($brs_ali);

            $data[$key]['boarding_stops'] = (empty($brs_ali)) ? array() : $brs_ali['boarding'];
            $data[$key]['dropping_stops'] = (empty($brs_ali)) ? array() : $brs_ali['alighting'];

            if (strtolower($value['op_name']) == "upsrtc") {
                $data[$key]['cancellation_policy'] = (!empty($cancel_policy)) ? $cancel_policy : [];
            } else if (strtolower($value['op_name']) == "msrtc") {
                $data[$key]['cancellation_policy'] = (!empty($cancel_policy_msrtc)) ? $cancel_policy_msrtc : [];
            }
        }
        return $data;
    }
   
}
