<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Msrtc_api extends CI_Model {
 
  var $user_id;
  var $pass;
  var $url;
  var $user_type;
  var $op_id;
  var $dep_date;
  var $ci;

  public function __construct() {
    parent::__construct();

    ini_set('max_execution_time', 100000);
    ini_set("memory_limit","1024M");

    $this->load->helper('soap_data');
    $this->load->model(array('soap_model/msrtc_model','ticket_details_model','common_model','trip_master_model','bus_service_stops_model','wallet_trans_model','base_commission_model','bus_type_model'));
    $this->load->library(array('cca_payment_gateway'));

    $this->user_id      = $this->config->item('msrtc_wsdl_user');
    $this->pass         = $this->config->item('msrtc_wsdl_password');
    $this->url          = $this->config->item("msrtc_wsdl_url");
    $this->user_type    = 1;
    $this->op_id        = $this->config->item('msrtc_operator_id');

    $this->dep_date     = $this->dep_date && $this->dep_date !=''? $this->dep_date : date('Y-m-d');
    $this->dep_date     = date('d/m/Y',strtotime($this->dep_date.' +1 days'));
  }

  public function index() {
    // echo 'class loaded';
  }

  public function seatAvailability($all_det) {
    $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
                  <soapenv:Header/>
                    <soapenv:Body>
                      <ser:getSeatAvailability>
                       <!--Optional:-->
                       <SeatAvailabilityRequest>
                          <!--Optional:-->
                          <authentication>
                             <!--Optional:-->
                             <userName>'.$this->user_id.'</userName>
                             <!--Optional:-->
                             <password>'.$this->pass.'</password>
                             <!--Optional:-->
                             <userType>'.$this->user_type.'</userType>
                          </authentication>
                          <serviceId>'.$all_det['service_id'].'</serviceId>
                          <!--Optional:-->
                          <fromStopId>'.$all_det['from_stop'].'</fromStopId>
                          <!--Optional:-->
                          <toStopId>'.$all_det['to_stop'].'</toStopId>
                          <!--Optional:-->
                          <dateOfJourney>'.$all_det['date'].'</dateOfJourney>
                       </SeatAvailabilityRequest>
                    </ser:getSeatAvailability>
                 </soapenv:Body>
              </soapenv:Envelope>';

    logging_data("ors/msrtc/".date("F")."/ors_".date("d-m-Y")."_log.log",$xml_str, 'Seat availablity');

    $data = get_soap_data($this->url, 'getSeatAvailability', $xml_str);

    logging_data("ors/msrtc/".date("F")."/ors_".date("d-m-Y")."_log.log",$data, 'Seat availablity');
    
    if (isset($data['SeatAvailabilityResponse']) && !isset($data['SeatAvailabilityResponse']['availableSeats'])) {
      $custom_error_mail = array(
                                  "custom_error_subject" => "MSRTC Seat layout Error from msrtc.",
                                  "custom_error_message" => "Request - ".$xml_str."<br/></br/> Response - ".json_encode($data)
                                );

      $this->common_model->management_custom_error_mail($custom_error_mail,"msrtc");
    }
    return $data;
  }

  public function getCancelList($all_det){

    $date = date('d/m/Y H:i:s',strtotime($all_det['date']));
    $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
                 <soapenv:Header/>
                 <soapenv:Body>
                    <ser:getCancelList>
                       <!--Optional:-->
                       <CancelListRequest>
                          <!--Optional:-->
                          <authentication>
                             <!--Optional:-->
                             <userName>'.$this->user_id.'</userName>
                             <!--Optional:-->
                             <password>'.$this->pass.'</password>
                             <!--Optional:-->
                             <userType>'.$this->user_type.'</userType>
                          </authentication>
                          <!--Optional:-->
                          <fromDate>'.$date.'</fromDate>
                       </CancelListRequest>
                    </ser:getCancelList>
                 </soapenv:Body>
                </soapenv:Envelope>';

    logging_data("ors/msrtc/".date("F")."/ors_".date("d-m-Y")."_log.log",$xml_str,'temp booking xml');

    $response = get_soap_data($this->url, 'getCancelList', $xml_str);

    logging_data("ors/msrtc/".date("F")."/ors_".date("d-m-Y")."_log.log",$response, 'temp booking Response');

    return $response;
  }

  public function temporary_booking($data = array()) { 
    $pass = $data['passenger'];
    $date = date('d/m/Y',strtotime($data['date']));

    $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <ser:doTemporaryBooking>
                         <!--Optional:-->
                         <TentativeBookingRequest>
                            <!--Optional:-->
                            <authentication>
                               <!--Optional:-->
                               <userName>'.$this->user_id.'</userName>
                               <!--Optional:-->
                               <password>'.$this->pass.'</password>
                               <!--Optional:-->
                               <userType>'.$this->user_type.'</userType>
                            </authentication>
                            <serviceId>'.$data['service_id'].'</serviceId>
                            <!--Optional:-->
                            <fromBusStopId>'.$data['from_stop_id'].'</fromBusStopId>
                            <!--Optional:-->
                            <toBusStopId>'.$data['to_stop_id'].'</toBusStopId>
                            <!--Optional:-->
                            <dateOfJourney>'.$date.'</dateOfJourney>
                            <!--Optional:-->
                            <passengerDetails>
                               <!--Zero or more repetitions:-->';
                    foreach ($pass as $key => $value) {
    $xml_str .=                 '<passenger>
                                  <!--Optional:-->
                                  <passengerName>'.$value['name'].'</passengerName>
                                  <age>'.$value['age'].'</age>
                                  <!--Optional:-->
                                  <concessioncode>?</concessioncode>
                                  <!--Optional:-->
                                  <concessionproof>?</concessionproof>
                                  <!--Optional:-->
                                  <sex>'.$value['gender'].'</sex>
                                  <!--Optional:-->
                                  <contactNumber>'.$data['mobile_no'].'</contactNumber>
                                  <!--Optional:-->
                                  <email>'.$data['email'].'</email>
                                  <seatNo>'.$value['seat_no'].'</seatNo>
                                  <totalFare>'.$value['fare'].'</totalFare>
                                  <!--Optional:-->
                                  <seatType>?</seatType>
                                  <!--Optional:-->
                                  <fareDetail>
                                     <!--Optional:-->
                                     <acc>?</acc>
                                     <adultFare>?</adultFare>
                                     <childFare>?</childFare>
                                     <reservationCharge>?</reservationCharge>
                                     <seniorfare>?</seniorfare>
                                  </fareDetail>
                               </passenger>';
              }
          $xml_str .= '</passengerDetails>
                         </TentativeBookingRequest>
                      </ser:doTemporaryBooking>
                   </soapenv:Body>
                </soapenv:Envelope>';

    logging_data("ors/msrtc/".date("F")."/ors_".date("d-m-Y")."_log.log",$xml_str,'temp booking xml');

    $response = get_soap_data($this->url, 'doTemporaryBooking', $xml_str);

    logging_data("ors/msrtc/".date("F")."/ors_".date("d-m-Y")."_log.log",$response, 'temp booking Response');

    $data_to_log = [
                    "ticket_id" => isset($data["ticket_id"]) ? $data["ticket_id"] : "",
                    "request" => $xml_str,
                    "response" => json_encode($response),
                    "type" => "temp_booking"
                   ];

    insert_request_response_log($data_to_log);

    if(isset($response['TentativeBookingResponse']) ) {
      return $response['TentativeBookingResponse'];
    }
    else {
      return $response['TentativeBookingResponse'];
    }
  }

  public function doconfirm_booking($data = array()) {    
    $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
                            <soapenv:Header/>
                            <soapenv:Body>
                               <ser:doConfirmBooking>
                                  <!--Optional:-->
                                  <ConfirmBookingRequest>
                                     <!--Optional:-->
                                     <authentication>
                                        <!--Optional:-->
                                        <userName>'.$this->user_id.'</userName>
                                        <!--Optional:-->
                                        <password>'.$this->pass.'</password>
                                        <!--Optional:-->
                                        <userType>'.$this->user_type.'</userType>
                                     </authentication>
                                     <!--Optional:-->
                                     <uniqueTransactionId>'.$data['ticket_id'].'</uniqueTransactionId>
                                     <!--Optional:-->
                                     <billDeskResponse>?</billDeskResponse>
                                     <!--Optional:-->
                                     <blockKey>'.$data['ticket_ref_no'].'</blockKey>
                                     <seatCount>'.$data['seat_count'].'</seatCount>
                                  </ConfirmBookingRequest>
                               </ser:doConfirmBooking>
                            </soapenv:Body>
                </soapenv:Envelope>';
                
                
                
    logging_data("ors/msrtc/".date("F")."/ors_".date("d-m-Y")."_log.log",$xml_str, 'confirm booking xml');
    $response = get_soap_data($this->url, 'doConfirmBooking', $xml_str);
    logging_data("ors/msrtc/".date("F")."/ors_".date("d-m-Y")."_log.log",$response, 'confirm booking response');
    
    $data_to_log = [
                    "ticket_id" => isset($data["ticket_id"]) ? $data["ticket_id"] : "",
                    "request" => $xml_str,
                    "type" => "confirm_booking",
                    "response" => json_encode($response),
                   ];

    insert_request_response_log($data_to_log);
    
    if(isset($response['ConfirmBookingResponse']) && $response['ConfirmBookingResponse']['status'] == 'SUCCESS') {
      return $response['ConfirmBookingResponse'];
    }
    else {
      return $response['ConfirmBookingResponse'];
    }
  }

  function getAvailableServices() {
    
      $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
                     <soapenv:Header/>
                     <soapenv:Body>
                        <ser:getAvailableServices>
                           <!--Optional:-->
                           <AvailableServiceRequest>
                              <!--Optional:-->
                              <authentication>
                                 <!--Optional:-->
                                 <userName>'.$this->user_id.'</userName>
                                 <!--Optional:-->
                                 <password>'.$this->pass.'</password>
                                 <!--Optional:-->
                                 <userType>'.$this->user_type.'</userType>
                              </authentication>
                              <!--Optional:-->
                              <dateOfJourney>'.$this->dep_date.'</dateOfJourney>
                           </AvailableServiceRequest>
                        </ser:getAvailableServices>
                     </soapenv:Body>
                  </soapenv:Envelope>';
      $response = get_soap_data($this->url,  'getAvailableServices', $xml_str);
      
      $this->db->trans_begin();
      
      $data = $this->msrtc_model->insert_available_services($response, $this->op_id,$this);
      
      if ($this->db->trans_status() === FALSE) {
          show(error_get_last());
          $this->db->trans_rollback();
      }
      else {
          $this->db->trans_commit();
      }
  }


  function latestGetAvailableServices() {
    
      $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
                     <soapenv:Header/>
                     <soapenv:Body>
                        <ser:LatestGetAvailableServices>
                           <!--Optional:-->
                           <LatestGetAvailableServices>
                              <!--Optional:-->
                              <authentication>
                                 <!--Optional:-->
                                 <userName>'.$this->user_id.'</userName>
                                 <!--Optional:-->
                                 <password>'.$this->pass.'</password>
                                 <!--Optional:-->
                                 <userType>'.$this->user_type.'</userType>
                              </authentication>
                              <!--Optional:-->
                              <dateOfJourney>'.$this->dep_date.'</dateOfJourney>
                              <status>N</status>
                           </LatestGetAvailableServices>
                        </ser:LatestGetAvailableServices>
                     </soapenv:Body>
                  </soapenv:Envelope>';
  
      $response = get_soap_data($this->url,  'LatestGetAvailableServices', $xml_str);

      $this->db->trans_begin();
      
      $data = $this->msrtc_model->insert_latest_available_services($response, $this->op_id,$this);
      
      if($this->db->trans_status() === FALSE) {
        show(error_get_last());
        $this->db->trans_rollback();
      }
      else {
          $this->db->trans_commit();
      } 
  }

  function GetAllBusTypes() {
    $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
                     <soapenv:Header/>
                     <soapenv:Body>
                        <ser:GetAllBusTypes>
                           <!--Optional:-->
                           <AllBusTypeRequest>
                              <!--Optional:-->
                              <authentication>
                                 <!--Optional:-->
                                 <userName>'.$this->user_id.'</userName>
                                 <!--Optional:-->
                                 <password>'.$this->pass.'</password>
                                 <!--Optional:-->
                                 <userType>'.$this->user_type.'</userType>
                              </authentication>
                           </AllBusTypeRequest>
                        </ser:GetAllBusTypes>
                     </soapenv:Body>
                  </soapenv:Envelope>';

    $response   = get_soap_data($this->url,  'GetAllBusTypes', $xml_str);
    $seat_layout_id = $this->msrtc_model->insert_bus_type($response, $this->op_id);
    
    return $seat_layout_id;
  }

  function getAllBusFormats($bus_format_cd) {
    $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
                       <soapenv:Header/>
                       <soapenv:Body>
                          <ser:getAllBusFormats>
                             <!--Optional:-->
                             <AllBusFormatRequest>
                                <!--Optional:-->
                                <authentication>
                                   <!--Optional:-->
                                   <userName>'.$this->user_id.'</userName>
                                   <!--Optional:-->
                                   <password>'.$this->pass.'</password>
                                   <!--Optional:-->
                                   <userType>'.$this->user_type.'</userType>
                                </authentication>
                                <!--Optional:-->
                                <busFormatType>'.$bus_format_cd.'</busFormatType>
                             </AllBusFormatRequest>
                          </ser:getAllBusFormats>
                       </soapenv:Body>
                </soapenv:Envelope>';
    
    $response = get_soap_data($this->url,  'getAllBusFormats', $xml_str, $bus_format_cd);
        
    if(count($response)>0) {
      $seat_layout_id = $this->msrtc_model->insert_bus_format($response, $this->op_id,$bus_format_cd);
    }
    else {
      show($response,'','No Response');
      return '';
    }
    return $seat_layout_id;  
  }

  function getBoardingStops_api($data =array(), $ali_type) {
    $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.rsrtc.trimax.com/">
                  <soapenv:Header/>
                     <soapenv:Body>
                        <ser1:getBoardingStops xmlns:ser1="http://service.msrtc.trimax.com/">
                           <!--Optional:-->
                           <GetBoardingStopsRequest>
                              <!--Optional:-->
                              <authentication>
                                 <!--Optional:-->
                                 <userName>'.$this->user_id.'</userName>
                                 <!--Optional:-->
                                 <password>'.$this->pass.'</password>
                                 <!--Optional:-->
                                 <userType>'.$this->user_type.'</userType>
                              </authentication>
                              <serviceId>'.$data['service_id'].'</serviceId>
                              <!--Optional:-->
                              <fromStopId>'.$data['from'].'</fromStopId>
                              <!--Optional:-->
                              <toStopId>'.$data['to'].'</toStopId>
                              <!--Optional:-->
                              <typeAliBrd>'.$ali_type.'</typeAliBrd>
                              <!--Optional:-->
                              <busTypeCode>'.$data['bus_type_code'].'</busTypeCode>
                           </GetBoardingStopsRequest>
                        </ser1:getBoardingStops>
                     </soapenv:Body>
                  </soapenv:Envelope>';

    $response = get_soap_data($this->url,  'getBoardingStops', $xml_str);
    return $response;
  }

  function getBoardingStops($data =array(), $ali_type) {
    $response = array();
    $stops = array();
    if( !empty($response) && isset($response['GetBoardingStopsResponse'])  && count($response['GetBoardingStopsResponse']) >0 && ( isset($response['GetBoardingStopsResponse']['boardingStopDetails']) || isset($response['GetBoardingStopsResponse']['alightingStopDetails']))) {
      $data['service_id'] = $response['GetBoardingStopsResponse']['serviceId'];
      if($ali_type == 'BRD') {
        $stop_code =  $data['from'];
        if( isset($response['GetBoardingStopsResponse']['boardingStopDetails']['boardingStopDetail']) && count($response['GetBoardingStopsResponse']['boardingStopDetails']) >0  &&  count($response['GetBoardingStopsResponse']['boardingStopDetails']['boardingStopDetail']) >0) {
          $stops = $response['GetBoardingStopsResponse']['boardingStopDetails']['boardingStopDetail'];
          if(isset($stops['busStopName'])) {
            $stops = array($stops);
          }
        }
        $this->getBoardingStops($data, 'ALI');
      }
      else if($ali_type == 'ALI') {
        $stop_code =  $data['to'];
        if(isset($response['GetBoardingStopsResponse']['alightingStopDetails']['alightingStopDetail']) && count($response['GetBoardingStopsResponse']['alightingStopDetails']) >0  &&  count($response['GetBoardingStopsResponse']['alightingStopDetails']['alightingStopDetail']) >0) {
          $stops = $response['GetBoardingStopsResponse']['alightingStopDetails']['alightingStopDetail'];
          if(isset($stops['busStopName'])) {
              $stops = array($stops);
          }
        }
      }

      if(count($stops)>0) {
        $this->msrtc_model->insert_boarding_stops($stops, $ali_type, $stop_code, $this->op_id, $data);
      }
    }
    else {
      show($response,'','getBoardingStops----error');
    }
    return $response;
  }

  function give_boarding_Stop_from_api($input)
  {
    $from = $this->bus_stop_master_model->get_detail_by_name($input['from_stop'], $input['operator']);
    $to   = $this->bus_stop_master_model->get_detail_by_name($input['to_stop'], $input['operator']);

    
    $board_data = array(
                    'service_id' => $input['service_id'],
                    'from' => $from->op_stop_cd,
                    'to'  => $to->op_stop_cd,
                    'bus_type_code' => $input['bus_type_code'],
                );


    $data = array();

    $data = array('service_id' => $input['service_id'], );
    $boarding_data    = $this->getBoardingStops_api($board_data, 'BRD');

    if(!empty($boarding_data['GetBoardingStopsResponse']['boardingStopDetails']['boardingStopDetail'])){
      $data['boarding'] = $boarding_data['GetBoardingStopsResponse']['boardingStopDetails']['boardingStopDetail'];
    }
    else
    {
        $data['boarding'] = array(
                                    'bus_stop_name' => $from->bus_stop_name,
                                    'stop_code' => $from->op_stop_cd,
                                    'is_boarding' => 'Y',
                                    'is_alighting' => 'N',
                                    'sequence' => '1',
                                  );
    }


    $alighting_data   = $this->getBoardingStops_api($board_data, 'ALI');

    if(!empty($alighting_data['GetBoardingStopsResponse']['alightingStopDetails']['alightingStopDetail'])){
      $data['alighting'] = $alighting_data['GetBoardingStopsResponse']['alightingStopDetails']['alightingStopDetail'];
    }
    else
    {
        $data['alighting'] = array(
                                    'bus_stop_name' => $to->bus_stop_name,
                                    'stop_code' => $to->op_stop_cd,
                                    'is_boarding' => 'N',
                                    'is_alighting' => 'Y',
                                    'sequence' => '1',
                                  );
    }

    return $data;
  }

  function getBusServiceStops($service_id, $trip_no = '', $op_bus_type = '', $service_data) {

     $xml_str  =  '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
            <soapenv:Header/>
                      <soapenv:Body>
                        <ser:getBusServiceStops>
                           <!--Optional:-->
                           <BusServiceStopsRequest>
                              <!--Optional:-->
                              <authentication>
                                 <!--Optional:-->
                                 <userName>'.$this->user_id.'</userName>
                                 <!--Optional:-->
                                 <password>'.$this->pass.'</password>
                                 <!--Optional:-->
                                 <userType>'.$this->user_type.'</userType>
                              </authentication>
                              <serviceId>'.$service_id.'</serviceId>
                              <!--Optional:-->
                              <dateOfJourney>'.$this->dep_date.'</dateOfJourney>
                           </BusServiceStopsRequest>
                        </ser:getBusServiceStops>
                     </soapenv:Body>
                  </soapenv:Envelope>';
      
      $response   = get_soap_data($this->url,  'getBusServiceStops', $xml_str);
      
      $bstop                     = array();
      $bstop['service_id']       = $service_id;
      $bstop['trip_no']          = $trip_no;
      $bstop['op_bus_type']      = $op_bus_type;

      $this->db->trans_begin();
      
      if($response && is_array($response) && count($response) >0 )
      {
        $data       = $this->msrtc_model->insert_bus_service_stops($response,$this->op_id, $this->dep_date, $this,$bstop,$service_data);
        
        if ($this->db->trans_status() === FALSE)
        {
            //show($this->db->queries,'', 'sucess database');
            show(error_get_last());
            $this->db->trans_rollback();
        }
        else
        {
            
            // $this->db->trans_rollback();
            $this->db->trans_commit();
        }
      }
      else
      {
        show($xml_str,'','getBusServiceStops');
        show($response,'','getBusServiceStops');
      }
      // $data       = $this->msrtc_model->insert_bus_service_stops($response,$this->op_id, $this->dep_date);
                     
      // show($data,1, 'getBusServiceStops');
  }
    
  public function cancel_ticket($data) {
    $xml_str='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <ser:cancelTicket>
                         <!--Optional:-->
                         <CancelRequest>
                            <!--Optional:-->
                            <authentication>
                               <!--Optional:-->
                               <userName>'.$this->user_id.'</userName>
                               <!--Optional:-->
                               <password>'.$this->pass.'</password>
                               <!--Optional:-->
                               <userType>'.$this->user_type.'</userType>
                            </authentication>
                            <!--Optional:-->
                            <ticketno>'.$data['pnr_no'].'</ticketno>
                            <!--Optional:-->
                            <seatNumbers> </seatNumbers>
                         </CancelRequest>
                      </ser:cancelTicket>
                   </soapenv:Body>
                </soapenv:Envelope>';

    logging_data("ors/msrtc/".date("F")."/cancel_".date("d-m-Y")."_log.log",$xml_str, 'Cancel Ticket Request');

    $response = get_soap_data($this->url, 'cancelTicket', $xml_str);

    logging_data("ors/msrtc/".date("F")."/cancel_".date("d-m-Y")."_log.log",$response, 'Cancel Ticket Response');

    $data_to_log = [
                    "ticket_id" => isset($data['current_ticket_information']["ticket_id"]) ? $data['current_ticket_information']["ticket_id"] : "",
                    "request" => $xml_str,
                    "type" => "ticket_cancel",
                    "response" => json_encode($response),
                   ];

    insert_request_response_log($data_to_log);
    if(isset($response['CancelResponse']) && $response['CancelResponse']['status'] == 'SUCCESS') {
      return $response['CancelResponse'];
    }
    else {            
      return $response['CancelResponse'];
    }
  }

  /******************************* FOR BOOKING PROCESS START *****************************/
  public function get_seat_layout($input) {
    $seat_service_tax_amt = 0.00;
    $seat_service_tax_perc = 0.00;
    $op_service_charge_amt = 0.00;
    $op_service_charge_perc = 0.00;
    $current_seat_discount = 0.00;

    $response = array();
    $trip_data_array = array('from' => $input['from'],
                             'to' => $input['to'],
                             'date' => $input['date'],
                             'trip_no' => $input['trip_no']
                            );

    //We can easily reduce this get_search query here. Think it on later
    /*$trip_data      = $this->travel_destinations_model->get_search($trip_data_array);*/
    $trip_data      = $this->trip_master_model->where(["op_id" => $this->op_id,"trip_no" => $input['trip_no']])->find_all();
    if($trip_data) {
      $trip_data = $trip_data[0];
      if($input && isset($input["msrtc_boarding"]) && isset($input["msrtc_alighting"])) {
        $all_det['from_stop']           = $input["msrtc_boarding"];
        $all_det['bus_from_stop_id']    = "";

        $all_det['bus_to_stop_id']      = "";
        $all_det['to_stop']             = $input["msrtc_alighting"];
        $all_det['user_type']           = $this->user_type;
        $all_det['service_id']          = $trip_data->op_trip_no;
        $all_det['date'] = date('d/m/Y', strtotime($input['date']));
        $soap_name = strtolower($input['op_name'].'_api');
        
        $data = $this->seatAvailability($all_det);

        $data['error'] = array();
        if (isset($data['SeatAvailabilityResponse']) && isset($data['SeatAvailabilityResponse']['availableSeats']) && count($data['SeatAvailabilityResponse']['availableSeats']) > 0) {
          if (isset($data['SeatAvailabilityResponse']['serviceId']) && count($data['SeatAvailabilityResponse']['serviceId']) > 0) {
            $op_trip_no = $data['SeatAvailabilityResponse']['serviceId'];
            $tot_fare = $data['SeatAvailabilityResponse']['fare'];

            // Need to do proper way 
            $child_seat_fare_diffrance = $data['SeatAvailabilityResponse']['fare'] - $data['SeatAvailabilityResponse']['childFare'];

            $tot_seats = count($data['SeatAvailabilityResponse']['availableSeats']['availableSeat']);
            $update_travel_data = array();
            $update_travel_data['trip_no'] = $input['trip_no'];
            $update_travel_data['bus_from_stop_id'] = "";
            $update_travel_data['bus_to_stop_id'] = "";
            $update_travel_data['fare'] = $tot_fare;
            $update_travel_data['search_date'] = $input['date'];
            $update_travel_data['tot_seat_available'] = $tot_seats;
            $update_travel_data['tot_seats'] = $tot_seats;
            $update_travel_data['from'] = $input['from'];
            $update_travel_data['to'] = $input['to'];
            $update_travel_data['child_seat_fare'] = $data['SeatAvailabilityResponse']['childFare'];
            $update_travel_data['senior_citizen_fare'] = $data['SeatAvailabilityResponse']['seniorcitizenfare'];
           
            $update_travel_destn = $this->travel_destinations_model->update_fare_totseats($update_travel_data);

            $tot_data_seat_layout = $this->op_seat_layout_model->get_seat_layout($trip_data->seat_layout_id);
                            
            $tot_rows = $tot_data_seat_layout[0]->no_rows;
            $tot_col = $tot_data_seat_layout[0]->no_cols;
            $double_tot_rows = $tot_data_seat_layout[0]->no_rows2;
            $double_tot_col = $tot_data_seat_layout[0]->no_cols2;
            $seat_type = $tot_data_seat_layout[0]->seat_type;
            $seat_layout = array();
            $available_seats_response = $data['SeatAvailabilityResponse']['availableSeats']['availableSeat'];
            if(!is_array($data['SeatAvailabilityResponse']['availableSeats']['availableSeat'])) {
                $available_seats_response = array($data['SeatAvailabilityResponse']['availableSeats']['availableSeat']);
            }
                            
            $seat_fare_detail = array();
            $total_available_seats = 0;
            $fare_with_conveycharge = getConveyCharges($tot_fare, "msrtc");

            $current_actual_seat_value = $fare_with_conveycharge;
            $current_actual_seat_value_with_tax = $fare_with_conveycharge;

            $basic_fare_array = array($fare_with_conveycharge);
            $fare_array = array($fare_with_conveycharge);

            $response['actual_fare'] = implode("/",array_filter($basic_fare_array));
            $response['actual_fare_array'] = implode("/",array_filter($fare_array));
                            
            foreach ($tot_data_seat_layout as $key => $value) {
                                
              /******************* Layout Parameteres & Description Start******************/
              $current_seat_type = "";
              $horizontal_seat = false;
              $vertical_seat = false;

              if(strtolower($value->quota_type) == "ladies") {
                $current_seat_type = "ladies";
              }
              /******************* Layout Parameteres & Description End********************/
              $pos = $value->row_no . "X" . $value->col_no;
              $birth_no = $value->berth_no;

              $seat_layout[$birth_no][$pos] = array(
                  'seat_no' => $value->seat_no,
                  'actual_seat_fare' => $current_actual_seat_value_with_tax,
                  'actual_seat_basic_fare' => $current_actual_seat_value,
                  'seat_fare' => $fare_with_conveycharge,
                  'seat_basic_fare' => $fare_with_conveycharge,
                  'seat_service_tax_amt' => $seat_service_tax_amt,
                  'seat_service_tax_perc' => $seat_service_tax_perc,
                  'op_service_charge_amt' => $op_service_charge_amt,
                  'op_service_charge_perc' => $op_service_charge_perc,
                  'row_no' => $value->row_no,
                  'col_no' => $value->col_no,
                  'seat_type' => $value->seat_type,
                  'berth_no' => $birth_no,
                  'quota' => strtolower($value->quota_type),
                  // 'quota' => "ladies",
                  'pos' => $pos,
                  'current_seat_type' => $current_seat_type,
                  'horizontal_seat' => $horizontal_seat,
                  'vertical_seat' => $vertical_seat,
                  'seat_discount' => $current_seat_discount,
                  'agent_commission' => 0,
                  'agent_comm_tds' =>  0,
                  'child_seat_fare' => $fare_with_conveycharge - $child_seat_fare_diffrance
              ); 

              if(!empty($available_seats_response) && in_array($value->seat_no, $available_seats_response) && in_array(strtolower($value->quota_type), array("general","ladies"))) {
                  $seat_layout[$birth_no][$pos]["available"] = 'Y';
                  $total_available_seats++;
              } else {
                  $seat_layout[$birth_no][$pos]["available"] = 'N';
              }

              if($seat_layout[$birth_no][$pos]["available"] == 'N' && strtolower($value->quota_type) == "ladies") {
                  $seat_layout[$birth_no][$pos]["booked_by"] = 'F';
              }
              $seat_fare_detail[$birth_no][$value->seat_no] = $seat_layout[$birth_no][$pos];
            }
          }
                        
          $response['fare'] = implode("/",array_filter($basic_fare_array));
          $response['fare_array'] = implode("/",array_filter($fare_array));
          $response['total_seats'] = $tot_seats;
          $response['total_available_seats'] = $total_available_seats;
          $response['available_seats'] = $data['SeatAvailabilityResponse']['availableSeats']['availableSeat'];
          $response['tot_rows'] = $tot_rows;
          $response['tot_cols'] = $tot_col;
          $response['tot_berth'] = 1; //Only One Berth IN msrtc. Please Discuss this.
          $response['seat_layout'] = $seat_layout;
          $response['seat_fare_detail'] = $seat_fare_detail;
          $response['update_travel_destn'] = $update_travel_destn;
          $response['flag'] = '@#success#@';
        }
        else {
            $number_of_seat_available = "";
            if(isset($data['SeatAvailabilityResponse']['serviceError']["errorReason"])) {
              if(stripos($data['SeatAvailabilityResponse']['serviceError']["errorReason"],"No seat available") !== false) {
                $number_of_seat_available = 0;
              }
            }
            $response['flag'] = '@#error#@';
            $response['error'] = isset($data['SeatAvailabilityResponse']['serviceError']) ? $data['SeatAvailabilityResponse']['serviceError'] : "";
            $response['total_available_seats'] =  $number_of_seat_available;
            $response['fare'] = 0;
        }
      }
      else {
           $response['fare'] = 0;    
      }
    }
    else {

        $custom_error_mail = array(
                                    "custom_error_subject" => "MSRTC Seat layout not fetched.",
                                    "custom_error_message" => "We have to fetch service again for following data .".json_encode($trip_data_array)
                                  );

        $this->common_model->management_custom_error_mail($custom_error_mail,"management");

        $response['flag'] = '@#error#@';
        $response['error'] = "Seat Not available for given date.";
    }
        
    $response['trip_no'] = $input['trip_no'];
    $response['stop_detail'] = array();
    return $response;
  }

  /*
  *Author : Suraj Rathod
  *Function : getSeatAvailability() //seatAvailability() seatAvailabilityDetail() 
  *Detail : To get seat availability and other details like fare and total seats available
  */
  public function getSeatAvailability($input) {
    $data = array();
    $seats_details = array();
    /*$trip_data      = $this->travel_destinations_model->get_search($input);*/
    $get_op_trip_no = $this->trip_master_model->get_trip_detail($input["trip_no"]);
    if($get_op_trip_no) {
      $trip_data      = $this->bus_service_stops_model->get_seat_availability_details($input["trip_no"],$get_op_trip_no["op_trip_no"],$input["op_id"]);
    
      if(!empty($trip_data)) {
        $trip_data                            = $trip_data[0];
        $trip_data["op_trip_no"]              = $get_op_trip_no["op_trip_no"];
        $trip_data["op_id"]                   = $this->op_id;
        $from_data                            = [];
        $to_data                              = [];
        if($input && isset($input["boarding_stop_cd"]) && isset($input["destination_stop_cd"])) {
          $msrtc_service_details        =  $this->bus_service_stops_model->get_service_stop_details($input["trip_no"],$trip_data['op_trip_no'],$input["boarding_stop_cd"],$input["destination_stop_cd"]);
          logging_data("msrtc_service_details_track_data/msrtc_service_details.log",$msrtc_service_details,"msrtc_service_details");
          if(!$msrtc_service_details) {
            redirect(base_url());
          }  

          $trip_data["sch_departure_tm"] = date("H:i:s",strtotime($msrtc_service_details[0]["departure_tm"]));
          
          $trip_departure_time =  date("H:i:s",strtotime($get_op_trip_no["departure_time"]));  
          if($trip_data["sch_departure_tm"] < $trip_departure_time) {
            $trip_data["dept_tm"] = date('Y-m-d H:i:s', strtotime($input["date"].' '.$trip_data["sch_departure_tm"]. '+1 day') );
            //$trip_data["dept_tm"] = date("Y-m-d",strtotime($input["date"]))." ".date("H:i:s",strtotime($msrtc_service_details[0]["departure_tm"]));
            $trip_data["sch_departure_date"] = date("Y-m-d",strtotime($input["date"].'+1 day'));

          }
          else {
            $trip_data["dept_tm"] = date("Y-m-d",strtotime($input["date"]))." ".date("H:i:s",strtotime($msrtc_service_details[0]["departure_tm"]));
            $trip_data["sch_departure_date"] = date("Y-m-d",strtotime($input["date"]));  
          } 
          $trip_data_day = ($trip_data['day'] > 0) ? ($trip_data['day'] -1 ) : $trip_data['day'];
          $trip_data_arrival_date_calculation = date('Y-m-d H:i:s', strtotime($input["date"].' '.date("H:i:s",strtotime($msrtc_service_details[0]["arrival_tm"])). '+'.$trip_data_day.'day') );
          $trip_data["arrival_date"] = $trip_data_arrival_date_calculation;
          $trip_data["arrival_tm"] = date("H:i:s",strtotime($trip_data_arrival_date_calculation));
          $trip_data["arrival_time"] = $trip_data_arrival_date_calculation;
          logging_data("msrtc_service_details_track_data/trip_data.log",$trip_data,"trip_data");
          $all_det['from_stop']           = $input['boarding_stop_cd'];
          $all_det['bus_from_stop_id']    = "";
          $all_det['bus_to_stop_id']      = "";
          $all_det['to_stop']             = $input['destination_stop_cd'];
          //dont delete below commented line. we will use it later.
          $all_det['user_type']           = $this->user_type;

          $all_det['service_id']          = $trip_data['op_trip_no'];
          $all_det['date'] = date('d/m/Y', strtotime($input['date']));

          $data = $this->seatAvailability($all_det);
            // $data = $this->{$soap_name}->seatAvailability($all_det);
          $data['error'] = array();
          if (isset($data['SeatAvailabilityResponse']) && isset($data['SeatAvailabilityResponse']['availableSeats']) && count($data['SeatAvailabilityResponse']['availableSeats']) > 0) {
            if (isset($data['SeatAvailabilityResponse']['serviceId']) && count($data['SeatAvailabilityResponse']['serviceId']) > 0) {
              $available_seats = $data['SeatAvailabilityResponse']['availableSeats']['availableSeat'];
              unset($data['SeatAvailabilityResponse']['availableSeats']['availableSeat']);
              /*
              *                  VERY IMP NOTICE
              *HERE We kept 1 as hardcode for berth. Discuss this issue.
              *We dont find double deck berth in msrtc BUSES so dont handled condition here.
              *
              */
              $berth_no = 1;
              $data['SeatAvailabilityResponse']['availableSeats']['availableSeat'][$berth_no] = (is_array($available_seats)) ? $available_seats : array($available_seats);
              $tot_data_seat_layout = $this->op_seat_layout_model->get_seat_layout($trip_data['seat_layout_id']);

              foreach ($tot_data_seat_layout as $tkey => $tvalue) {
                $is_ladies_seat = (strtolower($tvalue->quota_type) == "ladies") ? true : false;
                $seats_details[$berth_no][$tvalue->seat_no] = array('is_ladies_seat' => $is_ladies_seat);
              }

              $data["SeatAvailabilityResponse"]["seats_details"] =  $seats_details;

              $data['flag'] = 'success';
            }
            else {
              $data['flag'] = 'error';
              $data['error'] = isset($data['SeatAvailabilityResponse']['serviceError']) ? $data['SeatAvailabilityResponse']['serviceError'] : "";
            }
          }
        }
        else {
          $data['flag'] = 'error';
          $data['error'] = 'Ooopps.... Something went wrong. Please try again.';
        }

        $data["trip_data"] = $trip_data;
        $data["from_data"] = $from_data; //useless return
        $data["to_data"] = $to_data;//useless return
      }
    }
    return $data;
  }

  public function do_temp_booking($data_to_process)
  {   
    $data  = array();
    $input = $data_to_process["input"];
      
    if(!empty($input)) {
      $available_detail = $data_to_process['available_detail'];

      $email = $input['email_id'];
      $agent_email = $input['agent_email_id'];
      $mobile_no = $input['mobile_no'];
      $agent_mobile_no = $input['agent_mobile_no'];
      $user_id = $input['user_id'];
      $service_id = $available_detail['service_id'];
      $from = $available_detail['from'];
      $to = $available_detail['to'];
      $date = $available_detail['date'];

      $tot_seat = 0;
      $seat = $input['seat'];
      foreach ($seat as $seatkey => $seatvalue) {
        $tot_seat += count($seatvalue);
      }

      if($tot_seat > 6) {
        redirect(base_url());
      }

      $fare = $available_detail['fare'];
      $child_seat_fare = $available_detail['child_seat_fare'];
          
      //IMP For MSRTC
      $trip_route_stop = $this->travel_destinations_model->getTripDetail_optripno($available_detail['op_id'], $service_id, $from, $to, $date);

      $tot_fare = 0;
      $passenger_id = 1;

      $api_data['from'] = $from;
      $api_data['to'] = $to;
      $api_data['date'] = $date;
      $api_data['service_id'] = $service_id;
      $api_data['from_stop_id'] = $available_detail['from_stop_id'];
      $api_data['to_stop_id'] = $available_detail['to_stop_id'];
      $api_data['user_type'] = $available_detail['user_type'];
      $api_data["email"] = $email;
      $api_data["mobile_no"] = $mobile_no;
      if($available_detail['sch_dept_time'] < date("H:i:s",strtotime($trip_route_stop["start_stop_sch_departure_date"]))) {
        $dept_boarding_time = date("Y-m-d H:i:s",strtotime($api_data['date']." ".$available_detail['sch_dept_time']."+1 day"));
      }
      else {
        $dept_boarding_time = date("Y-m-d H:i:s",strtotime($api_data['date']." ".$available_detail['sch_dept_time']));
      }
      
      $dept_time = date("Y-m-d H:i:s",strtotime($trip_route_stop["start_stop_sch_departure_date"]));

      /************************Time Manipulation Start*******************************/
      $manipulated_boarding_time  = $dept_boarding_time;
      $manipulated_dept_time      = $dept_time;
      if(strtolower($trip_route_stop['from_stop_name']) == strtolower($available_detail['boarding_stop_name'])) {
        if(strtotime(date("H:i:s",strtotime($dept_time))) != strtotime(date("H:i:s",strtotime($dept_boarding_time)))) {
          if(date("H:i:s",strtotime($dept_time)) == "00:00:00" && date("H:i:s",strtotime($dept_boarding_time)) != "00:00:00") {
            $manipulated_boarding_time  = $dept_boarding_time;
            $manipulated_dept_time      = $dept_boarding_time;
          }
          else if(date("H:i:s",strtotime($dept_time)) != "00:00:00" && date("H:i:s",strtotime($dept_boarding_time)) == "00:00:00") {
            $manipulated_boarding_time  = $dept_time;
            $manipulated_dept_time      = $dept_time;
          }
        }
      }
      /************************Time Manipulation End*******************************/
      // Insert into tickets table
      $this->tickets_model->ticket_type = 1; // For In future
      $this->tickets_model->request_type = 'B';
      $this->tickets_model->ticket_status = 'A';
      $this->tickets_model->bus_service_no = $api_data['service_id'];
      $this->tickets_model->dept_time = $manipulated_dept_time;
      $this->tickets_model->boarding_time = $manipulated_boarding_time;
      $this->tickets_model->alighting_time = date("Y-m-d H:i:s",strtotime($trip_route_stop["end_stop_arrival_date"]));
      $this->tickets_model->droping_time = $available_detail['arrival_time'];
      $this->tickets_model->from_stop_cd = $trip_route_stop['from_stop_cd'];
      $this->tickets_model->till_stop_cd = $trip_route_stop['to_stop_cd'];
      $this->tickets_model->from_stop_name = $trip_route_stop['from_stop_name'];
      $this->tickets_model->till_stop_name = $trip_route_stop['to_stop_name'];
      $this->tickets_model->boarding_stop_name = $available_detail['boarding_stop_name'];
      $this->tickets_model->destination_stop_name = $available_detail['destination_stop_name'];
      $this->tickets_model->boarding_stop_cd = $available_detail['boarding_stop_cd'];
      $this->tickets_model->destination_stop_cd = $available_detail['destination_stop_cd'];
      $this->tickets_model->op_id = $available_detail['op_id'];
      $this->tickets_model->provider_id = $available_detail['provider_id'];
      $this->tickets_model->provider_type = $available_detail['provider_type'];
      $this->tickets_model->booked_from = "msrtc";
      $this->tickets_model->num_passgr = $tot_seat;
      $this->tickets_model->cancellation_rate = 0; 
      $this->tickets_model->partial_cancellation = 0; // 0 indicate partial cancellation not allowed . 1 indicate partial cancellation allowed. 
      $this->tickets_model->issue_time = date("Y-m-d H:i:s");
      $this->tickets_model->pay_id = 1;
      $this->tickets_model->session_no = 1;
      $this->tickets_model->op_name = $available_detail['op_name'];
      $this->tickets_model->user_email_id = $email;
      $this->tickets_model->mobile_no = $mobile_no;
      $this->tickets_model->agent_email_id = $agent_email;
      $this->tickets_model->agent_mobile_no = $agent_mobile_no;
      $this->tickets_model->inserted_by = $this->session->userdata("booking_user_id");
      $this->tickets_model->role_id = (!empty($this->session->userdata("role_id"))) ? $this->session->userdata("role_id") : 0;
      $this->tickets_model->booked_by = (!empty($this->session->userdata("role_name"))) ? strtolower($this->session->userdata("role_name")) : "guest_user";
      $this->tickets_model->bus_type = (isset($available_detail['bus_type']['op_bus_type_name'])) ? $available_detail['bus_type']['op_bus_type_name'] : "";
      $this->tickets_model->status = 'Y';
      $this->tickets_model->ip_address = $this->input->ip_address();
      $this->tickets_model->tds_per = $this->config->item("tds");
      $ticket_id = $this->tickets_model->save();

      $extra_charges_value = 0;
      $total_extra_charges_value = 0;
      $total_convey_charge = 0;
      $total_ticket_discount = 0;
      $seat_service_tax_amt = 0.00;
      $seat_service_tax_perc = 0.00;
      $op_service_charge_amt = 0.00;
      $op_service_charge_perc = 0.00;
      $total_service_tax_amount = 0;
      $total_op_service_charge_amount = 0;
      $total_basic_fare = 0;
      $current_seat_fare_with_conveycharges_for_discount = 0;
      $tickets_total_op_service_charge_amount = 0;
      $tickets_total_service_tax = 0;
      $tickets_total_basic_fare = 0;

      
      
      if($ticket_id > 0) {
        $psgr_no = 1;
        foreach ($seat as $berth_no => $seat_detail) {
          foreach ($seat_detail as $key => $value)  {
            $value['seat_no'] = $key;
            $value['berth_no'] = $berth_no;

            /*
             *                  NOTICE : VERY IMP LOGIC FOR FARE
             *if is_child_concession in op_bus_types is Y then assign child seat fare
             *if is_child_concession in op_bus_types is N then assign adult seat fare 
             *FARE ASSIGNED IN VALUE FARE BECAUSE IT WILL DIRECTLY BE USED IN TEMP BOOKING API
             */
            if(isset($available_detail['is_child_concession']) && $available_detail['is_child_concession'] == "Y") {
              if($value['age'] < 5) {
                $value['fare'] = 0.00;
              }
              else if($value['age'] >= 5 && $value['age'] <= MSRTC_CHILD_CONCESSION_AGE) {
                $value['fare'] = $child_seat_fare;
              }
              else {
                $value['fare'] = $fare;
              }
            }
            else {
              /*        NOTICE
               * MSRTC not allowing Senior Citizen. 
               * Here condition handled. When allowed Change if condition.
               */
              if($value['age'] >= 65) {
                $value['fare'] = $fare;
              }
              else {
                $value['fare'] = $fare;
              }
            }

            /*
            *NOTICE: MOST IMP
            *
            *DO NOT DELETE BELOW COMMENTED LINES. THESE ARE IMP.
            *BELOW WRITTEN LOGIC IS STATIC CONVEY CHARGES LOGIC.
            *THE LOGIC WRITTEN BELOW IS TEMP ALTERNATIVE FOR DYNAMIC LOGIC
            *CHANGE THIS AFTER DISCUSSION
            */
            /**************CONVEY CHARGE STATIC LOGIC START *****************/
            $three_percent_tot_fare = 0;
            $fifty_per_ticket = 0;
            $extra_charges_value = 0;
            $convey_charge_type = "";
            $convey_charge_value = "";

            $three_percent_tot_fare = ($value['fare'] * PERCENT_TICKET_CONVEY_CHARGE)/100 ;
            $fifty_per_ticket = (PER_TICKET_CONVEY_CHARGE);
            $total_extra_charges_value += $extra_charges_value;
            /**************CONVEY CHARGE STATIC LOGIC END *****************/

            $current_seat_fare_with_conveycharges = $value['fare']+$extra_charges_value;
          
            $tickets_total_op_service_charge_amount += $op_service_charge_amt;
            $tickets_total_service_tax += $seat_service_tax_amt;
            
            $tot_fare += $value['fare'];
            $total_basic_fare += $value['fare'];

            $api_data['passenger'][] = $value;
            // insert into ticket_deails table
            $this->ticket_details_model->ticket_id = $ticket_id;
            $this->ticket_details_model->psgr_no = $psgr_no;
            $this->ticket_details_model->psgr_name = $value['name'];
            $this->ticket_details_model->psgr_age = $value['age'];
            $this->ticket_details_model->psgr_sex = $value['gender'];
            $this->ticket_details_model->psgr_type = ($value['age'] <= MSRTC_CHILD_CONCESSION_AGE && isset($available_detail['is_child_concession']) && $available_detail['is_child_concession'] == "Y") ? "C" : "A";
            $this->ticket_details_model->concession_cd = 1; // if exists /in case of rsrtc 60 pass ct 
            $this->ticket_details_model->concession_rate = 1; // from api /if exists /in case of rsrtc 60 pass ct 
            $this->ticket_details_model->discount = 1; // if available from api
            $this->ticket_details_model->is_home_state_only = 1; // Y or No //not required now
            $this->ticket_details_model->fare_amt = $value['fare'];
            $this->ticket_details_model->convey_name = "Convenience charge";
            $this->ticket_details_model->convey_type = $convey_charge_type;
            $this->ticket_details_model->convey_value = $convey_charge_value;
            $this->ticket_details_model->fare_convey_charge = $convey_charge_value;
            $this->ticket_details_model->seat_no = $value['seat_no'];
            $this->ticket_details_model->berth_no = $value['berth_no'];
            $this->ticket_details_model->seat_status = 'Y';
            $this->ticket_details_model->concession_proff = 1;  // if cct then required // voter id
            $this->ticket_details_model->proff_detail = 1; // if varified
            $this->ticket_details_model->status = 'Y';
                  
            $psgr_no++;
                        
            $ticket_details_id = $this->ticket_details_model->save();
          }
        }
              
        $api_data["ticket_id"] = $ticket_id;
        $soap_name = strtolower($available_detail['op_name']) . '_api';
        $tb_data = $this->temporary_booking($api_data);
        $data['ticket_id'] = $ticket_id;
        if(!empty($tb_data)) {
          $temp_booking_status = $tb_data['status'];
          if ($temp_booking_status == 'FAILURE') {
              $data['block_key'] = "";
              $data['status'] = $tb_data['status'];
              $data['error'] = $tb_data['serviceError']['errorReason'];
              $data['payment_confirm_booking_status'] = "error";
          } 
          else if($temp_booking_status == 'SUCCESS') {
            $total_convey_charge = 0;
            $insert_ttrans_array = array();
            $tickets_total_fare = $tot_fare; //$tickets_total_fare has decalred because in some case we had changed $tot_fare value

            $total_fare_without_discount = $tot_fare + $total_extra_charges_value; 

            $data['discount_value'] = $total_ticket_discount;
            $final_total_amount = $total_fare_without_discount-$data['discount_value'];
            $tax_charges = array();

            /********to get convey charges based on bus type and operator id and save in ticket_tax_trans end********/

            $data['status'] = $temp_booking_status;
                        
            if(isset($tb_data['passengerDetails']['passenger']['fareDetail'])) {
              $api_reponse_data = $tb_data['passengerDetails']['passenger']['fareDetail'];

              $amount_to_add = 0.00;
              if($tb_data['passengerDetails']['passenger']['age'] >= 5 && $tb_data['passengerDetails']['passenger']['age'] <= MSRTC_CHILD_CONCESSION_AGE) {
                $amount_to_add = (isset($api_reponse_data['childFare'])) ? $api_reponse_data['childFare'] : 0.00;
              }
              else {
                $amount_to_add = (isset($api_reponse_data['adultFare'])) ? $api_reponse_data['adultFare'] : 0.00;
              }
              $tickets_total_basic_fare += $amount_to_add;
            }
            else {
              $api_reponse_data = $tb_data['passengerDetails']['passenger'][0]['fareDetail'];
              foreach ($tb_data['passengerDetails']['passenger'] as $psgkey => $psgvalue) {
                  $amount_to_add = 0.00;
                  if($psgvalue['age'] >= 5 && $psgvalue['age'] <= MSRTC_CHILD_CONCESSION_AGE) {
                    $amount_to_add = (isset($psgvalue["fareDetail"]['childFare'])) ? $psgvalue["fareDetail"]['childFare'] : 0.00;
                  }
                  else {
                    $amount_to_add = (isset($psgvalue["fareDetail"]['adultFare'])) ? $psgvalue["fareDetail"]['adultFare'] : 0.00;
                  }
                  $tickets_total_basic_fare += $amount_to_add;
              }
            }

            if($available_detail['bus_type_id'] != '') {
                $bus_type_id = $this->bus_type_model->column('bus_type_id')->where(array("id" => $available_detail['bus_type_id']))->as_array()->find_all();   
            }
            if(!empty($bus_type_id)) {
                $bus_type_id = $bus_type_id[0]['bus_type_id'];
            }
            //get Base commission data from base commission table 
            $base_com_data = array(
                "op_id" => $available_detail['op_id'],
                "api_provider" => $available_detail['provider_id'],
                "bus_type_id" => $bus_type_id
            );
            $base_com_response = $this->base_commission_model->base_commission_detail($base_com_data);

            $total_ac_service_tax = 0;
            $total_asn_fare = 0;
            $total_basic_with_ac = 0;
            if(isset($tb_data['passengerDetails']['passenger']['fareDetail'])) {
              $total_ac_service_tax = (isset($api_reponse_data['acServiceTax'])) ? $api_reponse_data['acServiceTax'] : 0.00;
              $total_asn_fare = (isset($api_reponse_data['asnFare'])) ? $api_reponse_data['asnFare'] : 0.00;
              $total_basic_with_ac = (isset($api_reponse_data['baseFare'])) ? $api_reponse_data['baseFare'] : 0.00;

              //save commission we are getting from operator 
              if($tb_data['passengerDetails']['passenger']['age'] >= 5 && $tb_data['passengerDetails']['passenger']['age'] <= MSRTC_CHILD_CONCESSION_AGE) {
                $fare_for_com_cal = (isset($api_reponse_data['childFare'])) ? $api_reponse_data['childFare'] : 0.00;
              }
              else {
                $fare_for_com_cal = (isset($api_reponse_data['adultFare'])) ? $api_reponse_data['adultFare'] : 0.00;
              }

              //calculate commission amount
              $op_commission_type = $base_com_response[0]->commission_type;
              $op_commission_per = $base_com_response[0]->percentage_value;
              $op_commission = round(($fare_for_com_cal * (float)($op_commission_per/100)),5);
              $tds_on_commission = round($op_commission * ((float)($this->config->item('tds')/100)),5);
              $final_commission = $op_commission - $tds_on_commission;
              
              $update_fare_details = array("adult_basic_fare" => (isset($api_reponse_data['adultFare'])) ? $api_reponse_data['adultFare'] : 0.00,
                                         "child_basic_fare" => (isset($api_reponse_data['childFare'])) ? $api_reponse_data['childFare'] : 0.00,
                                         "fare_acc" => (isset($api_reponse_data['acc'])) ? $api_reponse_data['acc'] : 0.00,
                                         "fare_hr" => (isset($api_reponse_data['hr'])) ? $api_reponse_data['hr'] : 0.00,
                                         "fare_it" => (isset($api_reponse_data['it'])) ? $api_reponse_data['it'] : 0.00,
                                         "fare_octroi" => (isset($api_reponse_data['octroi'])) ? $api_reponse_data['octroi'] : 0.00,
                                         "fare_other" => (isset($api_reponse_data['other'])) ? $api_reponse_data['other'] : 0.00,
                                         "fare_reservationCharge" => (isset($api_reponse_data['reservationCharge'])) ? $api_reponse_data['reservationCharge'] : 0.00,
                                         "fare_sleeperCharge" => (isset($api_reponse_data['sleeperCharge'])) ? $api_reponse_data['sleeperCharge'] : 0.00,
                                         "fare_toll" => (isset($api_reponse_data['toll'])) ? $api_reponse_data['toll'] : 0.00,
                                         "ac_service_tax" => $total_ac_service_tax,
                                         "asn_fare" => $total_asn_fare,
                                         "basic_with_ac" => (isset($api_reponse_data['baseFare'])) ? $api_reponse_data['baseFare'] : 0.00,
                                         "type" => $op_commission_type,
                                         "commission_percent" => $op_commission_per,
                                         "op_commission" => $op_commission,
                                         "tds_on_commission" => $tds_on_commission,
                                         "final_commission" => $final_commission
                                        );

              $this->ticket_details_model->update_where(array("ticket_id" => $ticket_id),"",$update_fare_details);
            }
            else {
              foreach ($tb_data['passengerDetails']['passenger'] as $ppkey => $ppvalue) {
                $ac_asn = $ppvalue["fareDetail"];
                $total_ac_service_tax += (isset($ac_asn['acServiceTax'])) ? $ac_asn['acServiceTax'] : 0.00;
                $total_asn_fare += (isset($ac_asn['asnFare'])) ? $ac_asn['asnFare'] : 0.00;
                $total_basic_with_ac += (isset($api_reponse_data['baseFare'])) ? $api_reponse_data['baseFare'] : 0.00;

                //save commission we are getting from operator 
                if($psgvalue['age'] >= 5 && $psgvalue <= MSRTC_CHILD_CONCESSION_AGE) {
                  $fare_for_com_cal = (isset($ac_asn['childFare'])) ? $ac_asn['childFare'] : 0.00;
                }
                else {
                  $fare_for_com_cal = (isset($ac_asn['adultFare'])) ? $ac_asn['adultFare'] : 0.00;
                }
                //calculate commission amount
                $op_commission_type = $base_com_response[0]->commission_type;
                $op_commission_per = $base_com_response[0]->percentage_value;
                $op_commission = round(($fare_for_com_cal * (float)($op_commission_per/100)),5);
                $tds_on_commission = round($op_commission * ((float)($this->config->item('tds')/100)),5);
                $final_commission = $op_commission - $tds_on_commission;
                
                $update_fare_details = array("adult_basic_fare" => (isset($ac_asn['adultFare'])) ? $ac_asn['adultFare'] : 0.00,
                                             "child_basic_fare" => (isset($ac_asn['childFare'])) ? $ac_asn['childFare'] : 0.00,
                                             "fare_acc" => (isset($ac_asn['acc'])) ? $ac_asn['acc'] : 0.00,
                                             "fare_hr" => (isset($ac_asn['hr'])) ? $ac_asn['hr'] : 0.00,
                                             "fare_it" => (isset($ac_asn['it'])) ? $ac_asn['it'] : 0.00,
                                             "fare_octroi" => (isset($ac_asn['octroi'])) ? $ac_asn['octroi'] : 0.00,
                                             "fare_other" => (isset($ac_asn['other'])) ? $ac_asn['other'] : 0.00,
                                             "fare_reservationCharge" => (isset($ac_asn['reservationCharge'])) ? $ac_asn['reservationCharge'] : 0.00,
                                             "fare_sleeperCharge" => (isset($ac_asn['sleeperCharge'])) ? $ac_asn['sleeperCharge'] : 0.00,
                                             "fare_toll" => (isset($ac_asn['toll'])) ? $ac_asn['toll'] : 0.00,
                                             "ac_service_tax" => (isset($ac_asn['acServiceTax'])) ? $ac_asn['acServiceTax'] : 0.00,
                                             "asn_fare" => (isset($ac_asn['asnFare'])) ? $ac_asn['asnFare'] : 0.00,
                                             "basic_with_ac" => (isset($api_reponse_data['baseFare'])) ? $api_reponse_data['baseFare'] : 0.00,
                                             "type" => $op_commission_type,
                                             "commission_percent" => $op_commission_per,
                                             "op_commission" => $op_commission,
                                             "tds_on_commission" => $tds_on_commission,
                                             "final_commission" => $final_commission
                                            );

                $this->ticket_details_model->update_where(array("ticket_id" => $ticket_id,"seat_no" => $ppvalue['seatNo']),"",$update_fare_details);
              }
            }
            /*******To Update ticket fare details in ticket details tables End ********/

            /*******To Update Tickets Table Start*******/
            $ticket_fields_update = array("ticket_ref_no" => $tb_data['blockKey'],
                                         "total_basic_amount" => $tickets_total_basic_fare,
                                         "op_service_charge" => $tickets_total_op_service_charge_amount,
                                         "service_tax" => $tickets_total_service_tax,
                                         "tot_fare_amt" => $tot_fare,
                                         "tot_fare_amt_with_tax" => $final_total_amount,
                                         "total_fare_without_discount" => $total_fare_without_discount,
                                         "fare_convey_charge" => $total_extra_charges_value,
                                         "ac_service_tax" => $total_ac_service_tax,
                                         "asn_fare" => $total_asn_fare,
                                         "total_basic_with_ac" => $total_basic_with_ac,
                                         "transaction_status" => "temp_booked");
                      
            $this->tickets_model->update_where(array("ticket_id" => $ticket_id),"",$ticket_fields_update);
            /*******To Update Tickets Table End*******/

            $data['block_key'] = $tb_data['blockKey'];
            $currentTime = strtotime(date("Y-m-d H:i:s"));
            $timeBlock = $currentTime+(60*$tb_data['blockTime']);
            $timeToBlock = date("Y-m-d H:i:s", $timeBlock);

            $data['fare_charges'] = $api_reponse_data;
            $data['block_time_minutes'] = $tb_data['blockTime'];
            $data['block_current_time'] = $currentTime;
            $data['time_to_block'] = $timeToBlock;
            $data['ttb_year'] = date("Y", $timeBlock);
            $data['ttb_month'] = date("m", $timeBlock);
            $data['ttb_day'] = date("d", $timeBlock);
            $data['ttb_hours'] = date("H", $timeBlock);
            $data['ttb_minutes'] = date("i", $timeBlock);
            $data['ttb_seconds'] = date("s", $timeBlock);
            $data['total_fare'] = $final_total_amount;
            $data["total_fare_without_discount"] =  $total_fare_without_discount;
            $data["ticket_fare"] =  $tot_fare;
            $data["total_seats"] = $tot_seat;
            $data["tax_charges"] =  $tax_charges;
            $data['payment_confirm_booking_status'] = "success";
            $data['ticket_id'] = $ticket_id;
          }
        }
        else {
          $data['payment_confirm_booking_status'] = "error";
        }
      }
      else {
        $data['payment_confirm_booking_status'] = "error";
      }
    }
    return $data;
  }


  function final_booking($api_data, $data)
  {
    $final_response = array();
    $tbdata = $this->doconfirm_booking($api_data);
    /************* Actual ticket booking End Here ***********/

    if(trim($tbdata['status']) == 'SUCCESS') 
    {   
        $onwards_ticket_ref_no = $data["ticket_ref_no"];

        $data["ticket_pnr"] = (isset($tbdata["pnr"])) ? $tbdata["pnr"] : (isset($tbdata["ticketno"])) ? $tbdata["ticketno"] : "";
        $data["operator_ticket_no"] = (isset($tbdata["pnr"])) ? $tbdata["pnr"] : (isset($tbdata["ticketno"])) ? $tbdata["ticketno"] : "";

        
        $transaction = array('transaction_status' => "success",
                              'payment_by' => $api_data["payment_by"],
                              'payment_mode' => $data["pg_response"]["payment_mode"],
                            );

        $this->tickets_model->update($data["ticket_id"],$transaction);
        //common model
        // $pg_success_response = $this->transaction_process($data);
        $pg_success_response = $this->transaction_process($data);
        $final_response["transaction_process_response"] = $pg_success_response;
        //make this function common afterwards. Because this is used 2 times (1 times in pg and 1 time in wallet)
        if(trim($pg_success_response["msg_type"]) == "success")
        {
          //$data["pg_response"]["udf2"] -> ticket_id 
          
          //To check if return journey
          // $rpt_data = $this->return_ticket_mapping_model->get_rpt_mapping($data["pg_response"]["udf2"]);
          $rpt_data = $api_data["rpt_data"];
          if(count($rpt_data))
          {
              $onward_ticket_id = $pg_success_response["ticket_id"];
              $rtransaction = array('is_return_journey' => "1",
                                    'return_ticket_id' => $rpt_data[0]->ticket_id
                                    );

              $this->tickets_model->update($onward_ticket_id,$rtransaction);
              /************* Actual ticket booking Start Here ***********/
              $rapi_data['ticket_id']      = str_pad($rpt_data[0]->ticket_id, 8, '0', STR_PAD_LEFT);
              $rapi_data['ticket_ref_no']  = $rpt_data[0]->ticket_ref_no;
              $rapi_data['seat_count']     = $rpt_data[0]->num_passgr;

              $soap_name = strtolower($rpt_data[0]->op_name) . '_api';
              $rtbdata = $this->doconfirm_booking($rapi_data);
              /************* Actual ticket booking End Here ***********/

              if($rtbdata['status'] == 'SUCCESS') 
              {
                  $data["ticket_ref_no"] = $rpt_data[0]->ticket_ref_no;
                  // $data["ticket_pnr"] = $rtbdata["pnr"];
                  $data["ticket_pnr"] = (isset($rtbdata["pnr"])) ? $rtbdata["pnr"] : (isset($rtbdata["ticketno"])) ? $rtbdata["ticketno"] : "";
                  $data["operator_ticket_no"] = (isset($rtbdata["pnr"])) ? $rtbdata["pnr"] : (isset($rtbdata["ticketno"])) ? $rtbdata["ticketno"] : "";
                  
                  $data["ticket_id"] = $rpt_data[0]->ticket_id;
                  
                  $atransaction = array('transaction_status' => "success",
                                         'payment_by' => $api_data["payment_by"],
                                         'payment_mode' => $data["pg_response"]["payment_mode"],
                                        );

                  $this->tickets_model->update($rpt_data[0]->ticket_id,$atransaction);

                  $final_response["return_ticket_status"] = "success";

                  $rpg_success_response = $this->transaction_process($data);
                  $final_response["transaction_process_response_return"] = $rpg_success_response;
                  if(trim($rpg_success_response["msg_type"]) == "success")
                  {
                    if(isset($data['is_back']) && $data['is_back'])
                    {
                      $url_redirect = base_url()."admin/reservation/view_ticket/".$onwards_ticket_ref_no."/".$rpt_data[0]->ticket_ref_no;  
                    }
                    else
                    {
                      $url_redirect = base_url()."front/booking/view_ticket/".$onwards_ticket_ref_no."/".$rpt_data[0]->ticket_ref_no;
                    }

                    $final_response["status"] = "success";
                    $final_response["redirect_url"] = $url_redirect;
                  }
                  else if(trim($rpg_success_response["msg_type"]) == "error")
                  {
                    $transaction = array('failed_reason' => isset($rpg_success_response["msg"]) ? $rpg_success_response["msg"] : "some error occured in msrtc library",
                                         'payment_by' => $api_data["payment_by"]
                                        );

                    $id = $this->tickets_model->update($rpt_data[0]->ticket_id,$transaction);
                

                    $final_response["status"] = "error";
                    $final_response["return_transaction_process_api"] = "error";
                    $final_response["data"] = $data;
                  }
              }
              else
              {
                if($rpt_data[0]->transaction_status != "success")
                {
                  $transaction = array('transaction_status' => "failed",
                                     'failed_reason' => "msrtc return ticket api failed",
                                     'payment_by' => $api_data["payment_by"]
                                    );

                  $return_ticket_update_where = ["ticket_id" => $rpt_data[0]->ticket_id,"transaction_status != " => "success"];
                  $id = $this->tickets_model->update($return_ticket_update_where,$transaction);
                }
                else
                {
                  $this->common_model->developer_confirm_booking_error_mail("MSRTC (Return Journey)",$api_data['pt_data']->ticket_id);
                }

                $final_response["return_ticket_status"] = "error";
                $final_response["return_ticket_api_failed_reason"] = "api failed";

                $final_response["status"] = "success";
                $final_response["redirect_url"] = $pg_success_response["redirect_url"];
                // redirect($pg_success_response["redirect_url"]);
              }
          }
          else
          {
              $final_response["status"] = "success";
              $final_response["redirect_url"] = $pg_success_response["redirect_url"];
          }
        }
        else if(trim($pg_success_response["msg_type"]) == "error")
        {
            $transaction = array(/*'transaction_status' => "failed",*/
                               'failed_reason' => 'failed message - '.(isset($pg_success_response["msg"])) ? $pg_success_response["msg"] : "",
                               'payment_by' => $api_data["payment_by"]
                              );

            $id = $this->tickets_model->update($api_data['ticket_id'],$transaction);
            $final_response["status"] = "success";
            $final_response["redirect_url"] = $pg_success_response["redirect_url"];

            logging_data("condition_not_handled/".date("M")."/msrtc_".date("d-m-Y")."_log.log",
                     "Ticket booked successfully but some error occured in transaction_process_api for ticket is ".$data["ticket_id"],
                      'Transaction Process Status');
        }
    }
    else
    {
        if($api_data['pt_data']->transaction_status != "success")
        {
          $transaction = array('transaction_status' => "failed",
                             'failed_reason' => 'msrtc doconfirm_booking api failed',
                             'payment_by' => $api_data["payment_by"]
                            );

          $update_where = ["ticket_id" => $api_data['pt_data']->ticket_id, "transaction_status != " => "success"];
          // $id = $this->tickets_model->update($api_data['ticket_id'],$transaction);
          $id = $this->tickets_model->update($update_where,$transaction);

          $find_return_ticket = array();
          $find_return_ticket = $this->return_ticket_mapping_model->where("ticket_id",$api_data['pt_data']->ticket_id)->find_all();
          
          if(!empty($find_return_ticket) && $find_return_ticket[0]->return_ticket_id > 0)
          {
            $return_update_where = ["ticket_id" => $find_return_ticket[0]->return_ticket_id, "transaction_status != " => "success"];
            $rid = $this->tickets_model->update($return_update_where,$transaction);
          }
        }
        else
        {
          $this->common_model->developer_confirm_booking_error_mail("MSRTC",$api_data['pt_data']->ticket_id);
        }
        
        $final_response["status"] = "error";
        $final_response["api_failed_reason"] = "api failed";
        $final_response["data"] = $data;
    }

    return $final_response;
  }
 /*
  * @Author      : Suraj Rathod
  * @function    : transaction_process()
  * @param       : $response["pg_response"] -> Response from pg
                   $response["is_updated"] -> is payment_transaction_fss updated or not status
                   $response["pay_trans_fss_id"] -> payment_transaction_fss id
  * @detail      : After payment success and doconfirm booking success, book the ticket and make entry in ticket table.
  *                 
  */
  public function transaction_process($response)
  {
      $pg_success_response = array();
      $pg_result = isset($response['pg_response']['result']) ? $response['pg_response']['result'] : "";
      $wallet_result = (isset($response['is_wallet_success']) && $response['is_wallet_success']) ? true : false ;
      
      if($wallet_result || (isset($response["is_updated"]) && $response["is_updated"] && (($pg_result == "CAPTURED") || ($pg_result =="APPROVED"))))
      {
          $blockKey = $response["ticket_ref_no"];
          $ticket_id = $response["ticket_id"];

          //After Remove Below Query and pass data
          $pt_data = $this->tickets_model->where("ticket_ref_no",$blockKey)->find_all();
          //here we fatching the wallet trans id for transaction id
          $wt_data = $this->wallet_trans_model->where('ticket_id',$pt_data[0]->ticket_id)->find_all();
          //if booked from wallet than wallet trans id else pg tracking id
          $pg_transaction_id = ($response['pg_response']['payment_mode'] == "wallet") ? $wt_data[0]->id : $pt_data[0]->pg_tracking_id ;
          // $pt_data = $response["ticket_data"];

          if($pt_data)
          {
              $dept_date = date("D, d M Y, h:i A",strtotime($pt_data[0]->dept_time));
              $dept_time = date("H:i:s",strtotime($pt_data[0]->dept_time));

              $data['ticket_data'] = $pt_data[0];

              //Ticket Detail Data
              // $this->ticket_details_model->ticket_id = $ticket_id;
              // $ptd_data = $this->ticket_details_model->select();
              $ptd_data = $this->ticket_details_model->where("ticket_id",$ticket_id)->find_all();
              
              //$payment_total_fare = $pt_data[0]->num_passgr * $pt_data[0]->tot_fare_amt;
              $payment_total_fare = $pt_data[0]->tot_fare_amt_with_tax;
              $ticket_total_fare_without_discount = $pt_data[0]->total_fare_without_discount;

              $data['passenger_ticket'] = $pt_data[0];
              $data['passenger_ticket_details'] = $ptd_data;
              $data['pnr'] = $response['ticket_pnr'];

              /****** To Update PNR, BOS REFERENCE NUMBER and SMS Count Start ***********/
              $bos_ref = getBossRefNo($ticket_id);
              // $this->tickets_model->boss_ref_no = $bos_ref["bos_ref_no"];
              // $bos_ref_exists = $this->tickets_model->select();
              $bos_ref_exists = $this->tickets_model->where("boss_ref_no",$bos_ref["bos_ref_no"])->find_all();
              
              if(!$bos_ref_exists)
              {
                  $bos_ticket_digit = $bos_ref["bos_ticket_digit"];
                  $bos_ticket_string = $bos_ref["bos_ticket_string"];
                  $bos_ref_no = $bos_ref["bos_ref_no"];
              }

              /*$fields_update = array("transaction_status" => "success", 
                                      "pnr_no" => $response["ticket_pnr"],
                                      "bos_ticket_digit" => $bos_ticket_digit,
                                      "bos_ticket_string" => $bos_ticket_string,
                                      "boss_ref_no" => $bos_ref_no,
                                      "eticket_count" => $eticket_count // IMP -> Placed 1 Hardcoded because. Here is the first time we are sending message.
                                      );

              $this->tickets_model->update($ticket_id,$fields_update);*/

              /****** To Update PNR,BOS REFERENCE NUMBER and SMS Count End***********/

              

              /*********************************************************************/
              /*
              *This is for testing Purpose only.
              *After discussion change this.
              */
              //THIS SEAT NO QUERY CAN BE REDUCE LATER
              $seat_no = $this->tickets_model->get_seat_nos_by_tid($ticket_id);
              if(count($seat_no) > 1)
              {
                  $seat_with_berth = "Berth 1: ".$seat_no[0]->seat_no." Berth 2: ".$seat_no[1]->seat_no;
                  $seat_with_berth_ticket = "Berth 1: ".$seat_no[0]->seat_no." <br/> Berth 2: ".$seat_no[1]->seat_no;
              }
              else
              {
                  $seat_with_berth = $seat_no[0]->seat_no;
                  $seat_with_berth_ticket = $seat_no[0]->seat_no;
              }

              $ticket_display_fare = $payment_total_fare;

              /*$ticket_sms_detail = array(
                                  "{{pnr_no}}" => $response['ticket_pnr'],
                                  "{{bos_ref_no}}" => $bos_ref_no,
                                  "{{amount_to_pay}}" => $pt_data[0]->total_fare_without_discount,
                                  "{{from_stop}}" => $pt_data[0]->from_stop_name,
                                  "{{to_stop}}" => $pt_data[0]->till_stop_name,
                                  "{{seat_no}}" => $seat_with_berth,
                                  "{{doj}}" => date("D, d M Y, h:i A",strtotime($pt_data[0]->boarding_time))
                                );

              log_data("sms/sms_".date("d-m-Y")."_log.log",$ticket_sms_detail, 'ticket_sms_detail array');

              $temp_sms_msg = getSMS("ticket_booking");
              $msg_to_sent = str_replace(array_keys($ticket_sms_detail),array_values($ticket_sms_detail),$temp_sms_msg[0]->sms_template_content);

              $is_send = sendSMS($pt_data[0]->mobile_no, $msg_to_sent, array('ticket_id' => $ticket_id));*/
              $smsData = [];
              $smsData["data"] = $data;
              $smsData["data"]["passenger_ticket"]->pnr_no = $response["ticket_pnr"];
              $smsData["seat_with_berth"] = $seat_with_berth;
              //if ticket booked by ors_agent two sms send one for agent and second for pessanger //
              $mobile_array[] = $pt_data[0]->mobile_no;
              if($pt_data[0]->agent_mobile_no != "")
              {
                  $mobile_array[] = $pt_data[0]->agent_mobile_no;
              }
              $is_send = sendTicketBookedSms($smsData, $mobile_array);
              $eticket_count = ($is_send) ? 1 : 0;


              $fields_update = array("transaction_status" => "success", 
                                      "pnr_no" => $response["ticket_pnr"],
                                      "bos_ticket_digit" => $bos_ticket_digit,
                                      "bos_ticket_string" => $bos_ticket_string,
                                      "boss_ref_no" => $bos_ref_no,
                                      "eticket_count" => $eticket_count // IMP -> Placed 1 Hardcoded because. Here is the first time we are sending message.
                                      );

              $this->tickets_model->update($ticket_id,$fields_update);
              
              
              /*
              * The Above SMS Code is temp and For testing purposeonly
              */

                
              //to check if user exists
              $email = $pt_data[0]->user_email_id;
              $data['email']=$email;
              $this->users_model->email = $email;
              $data['user_exists'] = $this->users_model->select();

              /*********************************************************************/

              if (count($ptd_data) > 0)
              {
                  //$basic_fare = $pt_data[0]->adult_basic_fare + $pt_data[0]->child_basic_fare;
                  $psgr_no = 1;
                  $reservationCharge = 0;
                  $passanger_detail = "";
                  
                  $adult_basic_fare = 0;
                  $senior_basic_fare = 0;
                  $child_basic_fare = 0;

                  foreach ($ptd_data as $key => $value)
                  {
                    $reservationCharge += $value->fare_reservationCharge;

                    if($value->psgr_age > MSRTC_CHILD_CONCESSION_AGE && $value->psgr_type == "A")
                    {
                        $senior_basic_fare += $value->adult_basic_fare;
                        $adult_child = "Adult";
                    }
                    else if($value->psgr_age <= MSRTC_CHILD_CONCESSION_AGE && $value->psgr_type == "C")
                    {
                        $child_basic_fare += $value->child_basic_fare;
                        $adult_child = "Children";
                    }

                    $total_calculate_basic_fare = $adult_basic_fare + $senior_basic_fare + $child_basic_fare;
                    // $adult_child = ($value->psgr_age > 12) ? "Adult" : "Children";


                    $passanger_detail .= '<tr style="border:1px solid #000;">
                                            <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">
                                              '.ucwords($value->psgr_name).'
                                            </td>
                                            <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                              '.ucwords($value->psgr_age).'
                                            </td>
                                            <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                              '.$adult_child.'
                                            </td>
                                            <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                              '.ucwords($value->psgr_sex).'
                                            </td>
                                            <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                              '.$value->seat_no.'
                                            </td>
                                            <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                              NA
                                            </td>
                                            <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                              NA
                                            </td>
                                            <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                              NA
                                            </td>
                                            <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                              NA
                                            </td>
                                          </tr>';
                      $psgr_no++; 
                  }

                  /*$service_detail = array();
                  $service_detail["bus_type"] = "NA";

                  if(isset($response["is_return_journey"]) && $response["is_return_journey"])
                  {
                    $available_detail = $this->session->userdata('available_detail_return');
                    $provider_available_bus = $this->session->userdata('provider_available_bus_return');
                  }
                  else
                  {
                    $available_detail = $this->session->userdata('available_detail');
                    $provider_available_bus = $this->session->userdata('provider_available_bus');
                  }

                  $service_detail = $provider_available_bus[$available_detail["service_id"]];
                  */
                  

                  $pickup_address = $pt_data[0]->boarding_stop_name;
                  $pickup_address .= ($pt_data[0]->pickup_address != "") ? " (".$pt_data[0]->pickup_address.")" : "";

                  $mail_replacement_array = array(
                                                 "{{bos_ref_no}}" => $bos_ref_no,
                                                 "{{pnr_no}}" => $response["ticket_pnr"],
                                                 "{{user_email}}" => $pt_data[0]->user_email_id,
                                                 "{{mobile_number}}" => $pt_data[0]->mobile_no,
                                                 "{{trip_no}}" => $pt_data[0]->bus_service_no,
                                                 // "{{ticket_no}}" => $pt_data[0]->ticket_id,
                                                 "{{ticket_no}}" => $response["ticket_pnr"],
                                                 "{{pg_transaction_id}}" => $pg_transaction_id,
                                                 "{{from_stop_name}}" => strtoupper($pt_data[0]->from_stop_name),
                                                 "{{till_stop_name}}" =>  strtoupper($pt_data[0]->till_stop_name),
                                                 "{{boarding_stop_name}}" =>  strtoupper($pt_data[0]->boarding_stop_name),
                                                 "{{destination_stop_name}}" =>  strtoupper($pt_data[0]->destination_stop_name),
                                                 "{{dept_time_date}}" =>  date("d/m/Y",strtotime($pt_data[0]->dept_time)),
                                                 "{{dept_time_his}}" =>  date("H:i",strtotime($pt_data[0]->dept_time))." Hrs",
                                                 "{{approx_boarding_time}}" =>  date("H:i",strtotime($pt_data[0]->boarding_time))." Hrs",
                                                 "{{bus_type}}" => $pt_data[0]->bus_type,
                                                 "{{num_passgr}}" => $pt_data[0]->num_passgr,
                                                 "{{bus_types}}" => $pt_data[0]->bus_type, //$service_detail["bus_type"],
                                                 "{{tot_fare_amt}}" => "Rs. ".$pt_data[0]->total_basic_amount,
                                                 "{{fare_reservationCharge}}" => "Rs. ".$reservationCharge,
                                                 "{{tot_fare_amt_with_tax}}" => "Rs. ".$pt_data[0]->total_fare_without_discount,
                                                 "{{asn_amount}}" => "Rs. ".$pt_data[0]->asn_fare,
                                                 "{{ac_service_charges}}" => "Rs. ".$pt_data[0]->ac_service_tax,
                                                 "{{passanger_details}}" => $passanger_detail
                                                 );
                        /*"{{header}}" => "<img src='http://www.bookonspot.com/app/assets/travelo/front/images/ticket_logo.png' width='300px;' />"*/
                      /************ Mail Start Here ********/
                      $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),MSRTC_TICKET_TEMPLATE);

                       $mailbody_array = array("subject" => "Rokad Ticket",
                                              "message" => $data['mail_content'],
                                              "to" =>  $email 
                                              );
                       
                       $mail_result = send_mail($mailbody_array);
                       //****if agent booked tkt send mail to both agent as well as customer****//
                      $email_agent = $pt_data[0]->agent_email_id;
                        if($email_agent != "" && ($email_agent != $email))
                        {
                            $agent_mailbody_array = array("subject" => "Rokad Ticket",
                                                  "message" => $data['mail_content'],
                                                  "to" =>  $email_agent
                                                  );
                             $agent_mail_result = send_mail($agent_mailbody_array);
                        }
                      /************ Mail End Here ********/

                      /************ Dump ticket Start Here ********/
                      //dump ticket in table
                      /*$this->mail_template_model->title = 'Upsrtc Ticket Template';
                      $mail_data = $this->mail_template_model->select();
                      $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),$mail_data[0]->body);
                      */

                      $this->ticket_dump_model->bos_ref_no = $bos_ref["bos_ref_no"];
                      $this->ticket_dump_model->ticket_id = $ticket_id;
                      $this->ticket_dump_model->ticket_ref_no = $blockKey;
                      $this->ticket_dump_model->pnr_no = $data['pnr'];
                      $this->ticket_dump_model->email = $email;
                      $this->ticket_dump_model->mobile = $pt_data[0]->mobile_no;
                      $this->ticket_dump_model->journey_date = $pt_data[0]->dept_time;
                      $this->ticket_dump_model->ticket = $data['mail_content'];
                      if ($this->session->userdata('booking_user_id') != "") {
                         $this->ticket_dump_model->created_by = $this->session->userdata("booking_user_id");
                      }

                      $ticket_dump_id = $this->ticket_dump_model->save();
                      /************ Dump ticket End Here ********/

                      //insert into user action log
                      $users_action_log = array("user_id" => $this->session->userdata("booking_user_id"),
                                                "name" => $this->session->userdata("display_name"),
                                                "url" => "bos_ref_no = ".$bos_ref["bos_ref_no"]." , ticket_ref_no = ".$blockKey." , ticket_id = ".$ticket_id." , pnr_no = ".$data['pnr'],
                                                "action" => "bus ticket booked",
                                                "message" => "Booked bus from <span class='navy_blue location'>".ucfirst($pt_data[0]->from_stop_name)."</span> to <span class='navy_blue location'>".ucfirst($pt_data[0]->till_stop_name)."</span> for the day <span class='navy_blue date'>".date("jS F Y", strtotime($pt_data[0]->dept_time))."</span>"
                                                );
                      
                      users_action_log($users_action_log);

                      /*if(!$this->session->userdata("is_user_registered") && $this->session->userdata("booking_user_id"))
                      { 
                        $activation_key = getRandomId(20);
                        $registration_mail_array = array("mail_title" => "bos_registration",
                                                         "mail_client" => $email,
                                                         "mail_replacement_array" => array('{{verification_mail_url}}' => base_url().'admin/login/activate_user_view/'.$activation_key,
                                                                                           '{{fullname}}' => "User"
                                                                                          )
                                                        );

                        $registration_mail_result = setup_mail($registration_mail_array);

                        $update_user_where  = array(
                                                    "id" => $this->session->userdata("booking_user_id"),
                                                    "email" => $email
                                                   );

                        $this->users_model->update($update_user_where,array('activate_key'  => $activation_key));
                      }*/

                      $pg_success_response["msg_type"] = "success";
                      $pg_success_response["bos_ref_no"] = $bos_ref["bos_ref_no"];
                      $pg_success_response["ticket_id"] = $ticket_id;
                      if(isset($response['is_back']) && $response['is_back']==1){
                          $pg_success_response["redirect_url"] = base_url()."admin/reservation/view_ticket/".$blockKey;
                      }else{
                          $pg_success_response["redirect_url"] = base_url()."front/booking/view_ticket/".$blockKey;
                      }
                      
                      // redirect(base_url()."front/booking/view_ticket/".$blockKey);
              }// end of if $ptd_data condition
              else
              {
                  $pg_success_response["msg_type"] = "error";
                  $pg_success_response["msg"] = "ticket detail data not available.";
                  $pg_success_response["redirect_url"] = base_url();
              }
       
          }// end of if $pt_data condition
          else
          {
              $pg_success_response["msg_type"] = "error";
              $pg_success_response["msg"] = "ticket data not available.";
              $pg_success_response["redirect_url"] = base_url();
          }
      }// end if payment status $response["is_updated"] && $pg_result
      else
      {
          $pg_success_response["msg_type"] = "error";
          $pg_success_response["msg"] = "payment gateway error or transaction table not updated.";
          $pg_success_response["redirect_url"] = base_url();
      }

      return $pg_success_response;
  } // End of transaction_process
  /******************************* FOR BOOKING PROCESS END *****************************/
      
  public function getBalance() {
    $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
                  <soapenv:Header/>
                   <soapenv:Body>
                     <ser:getBalance>
                        <!--Optional:-->
                        <UserBalanceRequest>
                           <!--Optional:-->
                           <userName>'.$this->user_id.'</userName>
                           <!--Optional:-->
                           <password>'.$this->pass.'</password>
                           <!--Optional:-->
                           <userType>'.$this->user_type.'</userType>
                        </UserBalanceRequest>
                     </ser:getBalance>
                  </soapenv:Body>
               </soapenv:Envelope>';

    logging_data("ors/balance/msrtc/".date("F")."/balance_".date("d-m-Y")."_log.log",$xml_str, 'Balance');

    $data = get_soap_data($this->url, 'getBalance', $xml_str);
  
    logging_data("ors/balance/msrtc/".date("F")."/balance_".date("d-m-Y")."_log.log",$data, 'Balance');

    return $data;  
  }

  public function isTicketCancelable($ticket_no)
  {

    $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <ser:isTicketCancelable>
                         <!--Optional:-->
                         <IsTicketCancelableRequest>
                            <!--Optional:-->
                            <authentication>
                               <!--Optional:-->
                               <userName>'.$this->user_id.'</userName>
                               <!--Optional:-->
                               <password>'.$this->pass.'</password>
                               <!--Optional:-->
                               <userType>'.$this->user_type.'</userType>
                            </authentication>
                            <!--Optional:-->
                            <ticketno>'.$ticket_no['pnr_no'].'</ticketno>
                            <!--Optional:-->
                            <seatNumbers></seatNumbers>
                         </IsTicketCancelableRequest>
                      </ser:isTicketCancelable>
                   </soapenv:Body>
                </soapenv:Envelope>';

    logging_data("ors/check_cancellation/msrtc/".date("F")."/check_cancellation_".date("d-m-Y")."_log.log",$xml_str, 'check_cancellation');
    $data = get_soap_data($this->url, 'isTicketCancelable', $xml_str);
    logging_data("ors/check_cancellation/msrtc/".date("F")."/check_cancellation_".date("d-m-Y")."_log.log",$data, 'check_cancellation');
    
    $data_to_log = [
                    "ticket_id" => isset($ticket_no['current_ticket_information']["ticket_id"]) ? $ticket_no['current_ticket_information']["ticket_id"] : "",
                    "request" => $xml_str,
                    "type" => "is_ticket_cancelable",
                    "response" => json_encode($data),
                   ];

    insert_request_response_log($data_to_log);
    return $data;
  }

  public function is_ticket_cancellable()
  {
    $input = $this->input->post();
    if(!empty($input) && isset($input["pnr_no"]) && $input["pnr_no"] != "")
    {
      $response  = [];
      $data = $this->isTicketCancelable($input);

      if(isset($data["IsTicketCancelableResponse"]) && strtolower($data["IsTicketCancelableResponse"]["status"]) == "success")
      {
        if($data["IsTicketCancelableResponse"]["cancellable"] )
        {
          $response["status"]                 = "success";
          $response["refundAmount"]           = $data["IsTicketCancelableResponse"]["refundAmount"];
          $response["cancellationCharge"]     = $data["IsTicketCancelableResponse"]["cancellationCharge"];
        }
        else
        {
          $response["status"]                 = "error";
          $response["error_message"]          = "Ticket is non-cancellable.";
        }
      }
      else
      {
        $response["status"]                 = "error";
        $response["error_message"]          = "This ticket cannot be cancelled. Please contact customer support.";
      }
      
      return $response;
    }
    else
    {
      return false;
    }
  }
  
  function getAvailableServices_bus_service_wise($date)
  {
      $response   = [];
      $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
                     <soapenv:Header/>
                     <soapenv:Body>
                        <ser:getAvailableServices>
                           <!--Optional:-->
                           <AvailableServiceRequest>
                              <!--Optional:-->
                              <authentication>
                                 <!--Optional:-->
                                 <userName>'.$this->user_id.'</userName>
                                 <!--Optional:-->
                                 <password>'.$this->pass.'</password>
                                 <!--Optional:-->
                                 <userType>'.$this->user_type.'</userType>
                              </authentication>
                              <!--Optional:-->
                              <dateOfJourney>'.$date.'</dateOfJourney>
                           </AvailableServiceRequest>
                        </ser:getAvailableServices>
                     </soapenv:Body>
                  </soapenv:Envelope>';
  
      $data = get_soap_data($this->url,  'getAvailableServices', $xml_str);

      if(isset($data["AvailableServiceResponse"]["availableServices"]["service"]) && count($data["AvailableServiceResponse"]["availableServices"]["service"]) > 0)
      {
        $services   = $data["AvailableServiceResponse"]["availableServices"]["service"];

        foreach ($services as $key => $value)
        {
          $response[$value["serviceId"]]  = $value;
        }
      }
      return $response;
  }

  function getBusServiceStops_bus_service_wise($service_date, $service_id)
  {
     logging_data("manually_bus_service_release/msrtc/".date("Y-m-d").".log","Service Id - ".$service_id." and service date ".$service_date, "Manual Services");

     $all_services  = $this->getAvailableServices_bus_service_wise(date("d/m/Y",strtotime($service_date)));
     if(isset($all_services[$service_id]))
     {
        $current_bus_service  = $all_services[$service_id];
     }
     else
     {
        show("Service Id - ".$service_id." Not Found for the date given",1);
     }
     $xml_str  =  '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
            <soapenv:Header/>
                      <soapenv:Body>
                        <ser:getBusServiceStops>
                           <!--Optional:-->
                           <BusServiceStopsRequest>
                              <!--Optional:-->
                              <authentication>
                                 <!--Optional:-->
                                 <userName>'.$this->user_id.'</userName>
                                 <!--Optional:-->
                                 <password>'.$this->pass.'</password>
                                 <!--Optional:-->
                                 <userType>'.$this->user_type.'</userType>
                              </authentication>
                              <serviceId>'.$service_id.'</serviceId>
                              <!--Optional:-->
                              <dateOfJourney>'.date("d/m/Y",strtotime($service_date)).'</dateOfJourney>
                           </BusServiceStopsRequest>
                        </ser:getBusServiceStops>
                     </soapenv:Body>
                  </soapenv:Envelope>';
      
      $response   = get_soap_data($this->url,  'getBusServiceStops', $xml_str);

      $this->db->trans_begin();
      
      if($response && is_array($response) && count($response) >0 )
      {
        $data       = $this->msrtc_model->insert_bus_service_stops_bus_service_wise($response,$this->op_id, date("d/m/Y",strtotime($service_date)) , $service_id, $current_bus_service);
        
        if ($this->db->trans_status() === FALSE)
        {
            //show($this->db->queries,'', 'sucess database');
            show(error_get_last());
            $this->db->trans_rollback();
        }
        else
        {
            // $this->db->trans_rollback();
            $this->db->trans_commit();
        }
      }
  }

  /******************************Partial Cancellatio Start**********************************/
  public function cancel_ticket_fullpartial($data)
  {
    $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
                 <soapenv:Header/>
                 <soapenv:Body>
                    <ser:cancelFullPartialTicket>
                       <!--Optional:-->
                       <CancelFullPartialRequest>
                          <!--Optional:-->
                          <authentication>
                             <!--Optional:-->
                             <userName>'.$this->user_id.'</userName>
                             <!--Optional:-->
                             <password>'.$this->pass.'</password>
                             <!--Optional:-->
                             <userType>'.$this->user_type.'</userType>
                          </authentication>
                          <!--Optional:-->
                          <ticketno>'.$data['pnr_no'].'</ticketno>
                          <cancelType>2</cancelType>
                          <!--Optional:-->
                          <seatNumbers><!--Zero or more repetitions:-->';
      foreach ($data["seat_no"] as $key => $value)
      {
        $xml_str  .=   '<seatNo>'.$value.'</seatNo>';
      } 

     $xml_str .= '</seatNumbers>
                       </CancelFullPartialRequest>
                    </ser:cancelFullPartialTicket>
                 </soapenv:Body>
              </soapenv:Envelope>';

    logging_data("ors/msrtc/".date("F")."/cancel_".date("d-m-Y")."_log.log",$xml_str, 'Cancel Ticket Request');

    $response = get_soap_data($this->url, 'cancelFullPartialTicket', $xml_str);

    logging_data("ors/msrtc/".date("F")."/cancel_".date("d-m-Y")."_log.log",$response, 'Cancel Ticket Response');

    if(isset($response['CancelResponse']) && $response['CancelResponse']['status'] == 'SUCCESS')
    {
      return $response['CancelResponse'];
    }
    else
    {            
      return $response['CancelResponse'];
    }
  }

  public function is_ticket_cancellable_fullpartial($request)
  {
    $input = $this->input->post();

    if(!empty($input["seat_no"]) && !is_array($input["seat_no"]))
    {
      $input["seat_no"] = explode(",", $input["seat_no"]);
    }

    if(!empty($input) && isset($input["pnr_no"]) && $input["pnr_no"] != "")
    {
      $response  = [];
      $data = $this->isTicketCancelable_fullpartial($input);

      if(isset($data["isTicketCancelableFullPartialResponse"]))
      {
        if($data["isTicketCancelableFullPartialResponse"]["cancellable"] )
        {
          $response["status"]                 = "success";
          $response["refundAmount"]           = $data["isTicketCancelableFullPartialResponse"]["refundAmount"];
          $response["cancellationCharge"]     = $data["isTicketCancelableFullPartialResponse"]["cancellationCharge"];
        }
        else
        {
          $response["status"]                 = "error";
          $response["error_message"]          = "Ticket is non-cancellable.";
        }
      }
      else
      {
        $response["status"]                 = "error";
        $response["error_message"]          = "This ticket cannot be cancelled. Please contact customer support.";
      }
      
      return $response;
    }
    else
    {
      return false;
    }
  }

  public function isTicketCancelable_fullpartial($ticket_no)
  {
    $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <ser:isTicketFullPartialCancelable>
                         <!--Optional:-->
                         <IsTicketCancelableFullPartialRequest>
                            <!--Optional:-->
                            <authentication>
                               <!--Optional:-->
                              <userName>'.$this->user_id.'</userName>
                               <!--Optional:-->
                               <password>'.$this->pass.'</password>
                               <!--Optional:-->
                               <userType>'.$this->user_type.'</userType>
                            </authentication>
                            <!--Optional:-->
                            <ticketno>'.$ticket_no['pnr_no'].'</ticketno>
                            <cancelType>2</cancelType>
                            <!--Optional:-->
                            <seatNumbers>
                               <!--Zero or more repetitions:-->';
                            foreach ($ticket_no["seat_no"] as $key => $value)
                            {
                $xml_str  .=   '<seatNo>'.$value.'</seatNo>';
                            }
                               
                $xml_str  .= '</seatNumbers>
                         </IsTicketCancelableFullPartialRequest>
                      </ser:isTicketFullPartialCancelable>
                   </soapenv:Body>
                </soapenv:Envelope>';

    logging_data("ors/check_cancellation/msrtc/".date("F")."/check_cancellation_".date("d-m-Y")."_log.log",$xml_str, 'check_cancellation');
    $data = get_soap_data($this->url, 'isTicketFullPartialCancelable', $xml_str);
    logging_data("ors/check_cancellation/msrtc/".date("F")."/check_cancellation_".date("d-m-Y")."_log.log",$data, 'check_cancellation');
    return $data;
  }


  public function regenerateTicket($ticket)
  {
    $return_data  = [];
    $pt_data = $this->tickets_model->where("ticket_id",$ticket["ticket_id"])->find_all();
     //here we fatching the wallet trans id for transaction id
    $wt_data = $this->wallet_trans_model->where('ticket_id',$pt_data[0]->ticket_id)->find_all();
    //if booked from wallet than wallet trans id else pg tracking id
    if($wt_data)
    {
       $pg_transaction_id = $wt_data[0]->id;
    }
    else
    {
        $pg_transaction_id = $pt_data[0]->pg_tracking_id;
    }

    $ticket_id  = $pt_data[0]->ticket_id; 
    if($pt_data)
    {
        $dept_date = date("D, d M Y, h:i A",strtotime($pt_data[0]->dept_time));
        $dept_time = date("H:i:s",strtotime($pt_data[0]->dept_time));

        $data['ticket_data'] = $pt_data[0];

        $ptd_data = $this->ticket_details_model->where("ticket_id",$ticket_id)->find_all();
        $active_ptd_data = $this->ticket_details_model->where(["ticket_id"=>$ticket_id,"status" => "Y"])->find_all();

        $payment_total_fare = $pt_data[0]->tot_fare_amt_with_tax;
        $ticket_total_fare_without_discount = $pt_data[0]->total_fare_without_discount;

        $data['passenger_ticket'] = $pt_data[0];
        $data['passenger_ticket_details'] = $active_ptd_data;
        $data['pnr'] = $pt_data[0]->pnr_no;
        /****** To Update PNR, BOS REFERENCE NUMBER and SMS Count Start ***********/
        $bos_ref = $pt_data[0]->boss_ref_no;
        /*********************************************************************/
        /*
        *This is for testing Purpose only.
        *After discussion change this.
        */
        //THIS SEAT NO QUERY CAN BE REDUCE LATER
        $seat_no = $this->tickets_model->get_seat_nos_by_tid($ticket_id);

        if(count($seat_no) > 1)
        {
            $seat_with_berth = "Berth 1: ".$seat_no[0]->seat_no." Berth 2: ".$seat_no[1]->seat_no;
            $seat_with_berth_ticket = "Berth 1: ".$seat_no[0]->seat_no." <br/> Berth 2: ".$seat_no[1]->seat_no;
        }
        else
        {
            $seat_with_berth = $seat_no[0]->seat_no;
            $seat_with_berth_ticket = $seat_no[0]->seat_no;
        }

        $ticket_display_fare = $payment_total_fare;

        if(!empty($seat_with_berth))
        {
          $smsData = [];
          $smsData["data"] = $data;
          $smsData["data"]["passenger_ticket"]->pnr_no = $pt_data[0]->pnr_no;
          $smsData["seat_with_berth"] = $seat_with_berth;

          $mobile_array[] = $pt_data[0]->mobile_no;
          if($pt_data[0]->agent_mobile_no != "")
          {
              $mobile_array[] = $pt_data[0]->agent_mobile_no;
          }
          $is_send = sendTicketBookedSms($smsData, $mobile_array);
        }

        
        /*
        * The Above SMS Code is temp and For testing purposeonly
        */
        //to check if user exists
        $email = $pt_data[0]->user_email_id;
        $data['email']=$email;
        $this->users_model->email = $email;
        $data['user_exists'] = $this->users_model->select();

        /*********************************************************************/

        if (!empty($seat_with_berth) && count($ptd_data) > 0)
        {
            //$basic_fare = $pt_data[0]->adult_basic_fare + $pt_data[0]->child_basic_fare;
            $psgr_no = 1;
            $reservationCharge = 0;
            $passanger_detail = "";
            
            $adult_basic_fare = 0;
            $senior_basic_fare = 0;
            $child_basic_fare = 0;
            $basic_amount_to_deduct = 0;
            $asn_fare_to_deduct = 0;
            $ac_service_tax_to_deduct = 0;
            $fare_amount_to_deduct = 0;

            foreach ($ptd_data as $key => $value)
            {
              if($value->status == "Y")
              {
                $reservationCharge += $value->fare_reservationCharge;

                if($value->psgr_age > MSRTC_CHILD_CONCESSION_AGE && $value->psgr_type == "A")
                {
                    $senior_basic_fare += $value->adult_basic_fare;
                    $adult_child = "Adult";
                }
                else if($value->psgr_age <= MSRTC_CHILD_CONCESSION_AGE && $value->psgr_type == "C")
                {
                    $child_basic_fare += $value->child_basic_fare;
                    $adult_child = "Children";
                }

                $total_calculate_basic_fare = $adult_basic_fare + $senior_basic_fare + $child_basic_fare;
                // $adult_child = ($value->psgr_age > 12) ? "Adult" : "Children";


                $passanger_detail .= '<tr style="border:1px solid #000;">
                                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">
                                          '.ucwords($value->psgr_name).'
                                        </td>
                                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                          '.ucwords($value->psgr_age).'
                                        </td>
                                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                          '.$adult_child.'
                                        </td>
                                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                          '.ucwords($value->psgr_sex).'
                                        </td>
                                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                          '.$value->seat_no.'
                                        </td>
                                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                          NA
                                        </td>
                                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                          NA
                                        </td>
                                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                          NA
                                        </td>
                                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                          NA
                                        </td>
                                      </tr>';
                  $psgr_no++; 
              }
              else if($value->status == "N")
              {
                $asn_fare_to_deduct += $value->asn_fare; 
                $ac_service_tax_to_deduct += $value->ac_service_tax;
                $fare_amount_to_deduct += $value->fare_amt;
                if($value->psgr_age > MSRTC_CHILD_CONCESSION_AGE && $value->psgr_type == "A")
                {
                    $basic_amount_to_deduct += $value->adult_basic_fare;
                }
                else if($value->psgr_age <= MSRTC_CHILD_CONCESSION_AGE && $value->psgr_type == "C")
                {
                    $basic_amount_to_deduct += $value->child_basic_fare;
                }
              }
            }

            $pickup_address = $pt_data[0]->boarding_stop_name;
            $pickup_address .= ($pt_data[0]->pickup_address != "") ? " (".$pt_data[0]->pickup_address.")" : "";

            $mail_replacement_array = array(
                                           "{{bos_ref_no}}" => $pt_data[0]->boss_ref_no,
                                           "{{pnr_no}}" => $pt_data[0]->pnr_no,
                                           "{{user_email}}" => $pt_data[0]->user_email_id,
                                           "{{mobile_number}}" => $pt_data[0]->mobile_no,
                                           "{{trip_no}}" => $pt_data[0]->bus_service_no,
                                           "{{ticket_no}}" => $pt_data[0]->pnr_no,
                                           "{{pg_transaction_id}}" => $pg_transaction_id,
                                           "{{from_stop_name}}" => strtoupper($pt_data[0]->from_stop_name),
                                           "{{till_stop_name}}" =>  strtoupper($pt_data[0]->till_stop_name),
                                           "{{boarding_stop_name}}" =>  strtoupper($pt_data[0]->boarding_stop_name),
                                           "{{destination_stop_name}}" =>  strtoupper($pt_data[0]->destination_stop_name),
                                           "{{dept_time_date}}" =>  date("d/m/Y",strtotime($pt_data[0]->dept_time)),
                                           "{{dept_time_his}}" =>  date("H:i",strtotime($pt_data[0]->dept_time))." Hrs",
                                           "{{approx_boarding_time}}" =>  date("H:i",strtotime($pt_data[0]->boarding_time))." Hrs",
                                           "{{bus_type}}" => $pt_data[0]->bus_type,
                                           "{{num_passgr}}" => $pt_data[0]->num_passgr,
                                           "{{bus_types}}" => $pt_data[0]->bus_type, //$service_detail["bus_type"],
                                           "{{tot_fare_amt}}" => "Rs. ".($pt_data[0]->total_basic_amount-$basic_amount_to_deduct),
                                           "{{fare_reservationCharge}}" => "Rs. ".$reservationCharge,
                                           "{{tot_fare_amt_with_tax}}" => "Rs. ".($pt_data[0]->total_fare_without_discount-$fare_amount_to_deduct),
                                           "{{asn_amount}}" => "Rs. ".($pt_data[0]->asn_fare-$asn_fare_to_deduct),
                                           "{{ac_service_charges}}" => "Rs. ".($pt_data[0]->ac_service_tax-$ac_service_tax_to_deduct),
                                           "{{passanger_details}}" => $passanger_detail
                                           );
                 
                /************ Mail Start Here ********/
                $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),MSRTC_TICKET_TEMPLATE);

                 $mailbody_array = array("subject" => "Rokad Ticket",
                                        "message" => $data['mail_content'],
                                        "to" =>  $email 
                                        );
                 
                 $mail_result = send_mail($mailbody_array);
                 /************Agent Mail Start Here ********/
                $email_agent = $pt_data[0]->agent_email_id;
                  if($email_agent != "" && ($email_agent != $email))
                  {
                      $agent_mailbody_array = array("subject" => "Rokad Ticket",
                                            "message" => $data['mail_content'],
                                            "to" =>  $email_agent
                                            );
                       $agent_mail_result = send_mail($agent_mailbody_array);
                  }
                /************ Mail End Here ********/
                $this->ticket_dump_model->update_where(array("ticket_id" => $ticket_id,"bos_ref_no" => $pt_data[0]->boss_ref_no),"",array("ticket" => $data['mail_content']));
                /************ Dump ticket End Here ********/

                //insert into user action log
                $users_action_log = array("user_id" => $this->session->userdata("booking_user_id"),
                                          "name" => $this->session->userdata("display_name"),
                                          "url" => "bos_ref_no = ".$pt_data[0]->boss_ref_no." , ticket_ref_no = ".$blockKey." , ticket_id = ".$ticket_id." , pnr_no = ".$data['pnr'],
                                          "action" => "bus ticket booked",
                                          "message" => "Partially cancelled bus from <span class='navy_blue location'>".ucfirst($pt_data[0]->from_stop_name)."</span> to <span class='navy_blue location'>".ucfirst($pt_data[0]->till_stop_name)."</span> for the day <span class='navy_blue date'>".date("jS F Y", strtotime($pt_data[0]->dept_time))."</span>"
                                          );
                
                users_action_log($users_action_log);

                $return_data["msg_type"] = "success";
                $return_data["bos_ref_no"] = $pt_data[0]->boss_ref_no;
                $return_data["ticket_id"] = $ticket_id;

        }// end of if $ptd_data condition
 
    }// end of if $pt_data condition

    return $return_data;
  }
  // to send the cancel ticket start here//
  public function mail_cancel_ticket($tinfo,$data,$ptd_data)
  {
    //here we fatching the wallet trans id for transaction id
        $wt_data = $this->wallet_trans_model->where('ticket_id',$pt_data[0]->ticket_id)->find_all();
        //if booked from wallet than wallet trans id else pg tracking id
        $pg_transaction_id = ($response['pg_response']['payment_mode'] == "wallet") ? $wt_data[0]->id : $pt_data[0]->pg_tracking_id ;
    foreach ($ptd_data as $key => $value)
        {
            $passanger_detail .= '<tr style="border:1px solid #000;">
                                  <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">
                                    '.ucwords($value->psgr_name).'
                                  </td>
                                  <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                    '.ucwords($value->psgr_age).'
                                  </td>
                                  <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                    '.$adult_child.'
                                  </td>
                                  <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                    '.ucwords($value->psgr_sex).'
                                  </td>
                                  <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                    '.$value->seat_no.'
                                  </td>
                                  <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                    NA
                                  </td>
                                  <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                    NA
                                  </td>
                                  <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                    NA
                                  </td>
                                  <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                                    NA
                                  </td>
                                </tr>';
            $psgr_no++; 
        }
    if($tinfo != '' && $data != '')
    {
       $mail_replacement_array = array(
                                       "{{bos_ref_no}}" => $tinfo['boss_ref_no'],
                                       "{{pnr_no}}" => $tinfo['pnr_no'],
                                       "{{user_email}}" => $tinfo['user_email_id'],
                                       "{{mobile_number}}" => $tinfo['mobile_no'],
                                       "{{trip_no}}" => $tinfo['bus_service_no'],
                                       "{{ticket_no}}" => $tinfo['pnr_no'],
                                       "{{pg_transaction_id}}" => $tinfo['pg_tracking_id'],
                                       "{{from_stop_name}}" => strtoupper($tinfo['from_stop_name']),
                                       "{{till_stop_name}}" =>  strtoupper($tinfo['till_stop_name']),
                                       "{{boarding_stop_name}}" =>  strtoupper($tinfo['boarding_stop_name']),
                                       "{{destination_stop_name}}" =>  strtoupper($tinfo['destination_stop_name']),
                                       "{{dept_time_date}}" =>  date("d/m/Y",strtotime($tinfo['dept_time'])),
                                       "{{dept_time_his}}" =>  date("H:i",strtotime($tinfo['dept_time']))." Hrs",
                                       "{{approx_boarding_time}}" =>  date("H:i",strtotime($tinfo['boarding_time']))." Hrs",
                                       "{{bus_type}}" => $tinfo['bus_type'],
                                       "{{num_passgr}}" => $tinfo['num_passgr'],
                                       "{{bus_types}}" => $tinfo['bus_type'],
                                       "{{tot_fare_amt}}" => "Rs. ".$tinfo['tot_fare_amt'],
                                       "{{cancellation_cahrge}}" => "Rs. ".$data['cancellation_cahrge'],
                                       "{{refund_amt}}" => "Rs. ".$data['refund_amt'],
                                       "{{ac_service_charges}}" => "Rs. ".$tinfo['ac_service_tax'],
                                       "{{passanger_details}}" => $passanger_detail
                                       );
            /************ Mail Start Here ********/
            $email = $tinfo['user_email_id'];
            $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),MSRTC_CANCEL_TICKET_TEMPLATE);

             $mailbody_array = array("subject" => "Book On Spot Cancel Ticket",
                                    "message" => $data['mail_content'],
                                    "to" =>  $email
                                    );
             
             $mail_result = send_mail($mailbody_array);

           //****if agent booked tkt send mail to both agent as well as customer****//
                  $email_agent = $tinfo['agent_email_id'];
                    if($email_agent != "" && ($email_agent != $email))
                    {
                        $agent_mailbody_array = array("subject" => "Book On Spot Cancel Ticket",
                                              "message" => $data['mail_content'],
                                              "to" =>  $email_agent
                                              );
                         $agent_mail_result = send_mail($agent_mailbody_array);
                    }
            //****************************** Mail End Here ******************//
    }
    else
    {
      redirect();
    }
    /************************ Update in Ticket_dump table start here *****************************/
      $update_in_ticket_dump = $this->ticket_dump_model->update_where(array('ticket_id' => $tinfo['ticket_id']),'',array('cancel_ticket' => $data['mail_content']));

    /************************ Update in Ticket_dump table end here *****************************/

  }// to send the cancel ticket end here

  /******************************Partial Cancellatio End**********************************/
  
}
