
<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Service_1 extends CI_Model {

	public function __construct() {
		parent::__construct();

		$this->load->model(array('agent_commission_template_log_model','package_master_model','package_detail_model','ticket_details_model'));
		$this->serviceId = 1;
	}

	public function calculate_commission($ticket_data,$update_data,$bus_type_id) {
		$data = array();
		// Get Master distributor id
        $master_distributor_id = $this->session->userdata('master_distributor_id');
        //get current package detail assign to md
        $pacakage_id_data = $this->agent_commission_template_log_model->column('agent_commission_template_log.commission_id')->join('package_master pm',"pm.id = agent_commission_template_log.commission_id","left")->where(array("user_id" => $master_distributor_id,"agent_commission_template_log.status" => '1',"pm.status" => '1'))->where('("'.date("Y-m-d").'" between `commission_from` and commission_to )')->order_by("agent_commission_template_log.id","desc")->limit('1')->find_all();
		
        if(!empty($pacakage_id_data[0]->commission_id) && $pacakage_id_data[0]->commission_id > 0 ) {

        	// get commission id for service according to package Id
        	$comm_id_data = $this->package_detail_model->column('commission_id')->where(array("package_id" => $pacakage_id_data[0]->commission_id))->find_all();
        	
        	if(!empty($comm_id_data[0]->commission_id) && $comm_id_data[0]->commission_id > 0 && (empty($ticket_data[0]->discount_id) || $ticket_data[0]->discount_id == 0 ) ) {
        		$ticket_detail = $this->ticket_details_model->column('id,final_commission')->where(array("ticket_id" => $ticket_data[0]->ticket_id))->as_array()->find_all(); 
	            logging_data("commission/calculate_commission.log",$ticket_detail,"Ticket Detail(Function: process_payment_confirm_booking),ticket id: ".$ticket_data[0]->ticket_id);
	            
	            $commission_cal_response    = calculate_commission_new($ticket_detail,$ticket_data[0]->provider_id, $ticket_data[0]->op_id,$ticket_data[0]->provider_type,$ticket_data[0]->ticket_id,$comm_id_data[0]->commission_id,$bus_type_id,$update_data);
	            
	            if(!empty($commission_cal_response)) {
	                $data["final_retailer_commission"]  = $commission_cal_response['final_retailer_commission'];
	                $data["retailer_commission"]        = $commission_cal_response["retailer_commission"];
	                $data["tds_on_retailer_commission"] = $commission_cal_response["tds_on_retailer_commission"];
	            }
        	}
        }
		return $data;
	}
}
