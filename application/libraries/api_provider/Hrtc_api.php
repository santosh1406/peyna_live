<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Hrtc_api extends CI_Model {
 
  var $user_id;
  var $pass;
  var $url;
  var $user_type;
  var $op_id;
  var $dep_date;
  var $ci;

  public function __construct() {
    parent::__construct();

    ini_set('max_execution_time', 100000);
    ini_set("memory_limit","1024M");

    $this->load->helper('soap_data');
    $this->load->model(array('soap_model/msrtc_model','common_model','users_model','ticket_dump_model','cancellation_charges_model','ticket_details_model','op_seat_layout_model','op_layout_details_model','api_bus_services_model','ticket_metadata_model','base_commission_model'));
    $this->load->library(array('cca_payment_gateway','rediscache'));

    $this->user_id                = $this->config->item('hrtc_wsdl_user');
    $this->pass                   = $this->config->item('hrtc_wsdl_password');
    $this->url                    = $this->config->item("hrtc_wsdl_url");
    $this->hrtc_api_secret_key    = $this->config->item("hrtc_api_secret_key");
    $this->op_id                  = 4;

    $this->dep_date     = $this->dep_date && $this->dep_date !=''? $this->dep_date : date('Y-m-d');
    $this->dep_date     = date('d/m/Y',strtotime($this->dep_date.' +1 days'));
    ini_set('display_errors', 0);
  }

  public function index()
  {
    // echo 'class loaded';
  }

  public function getBalance()
  {
    // mcrypt_encrypt_e($input["from_source_id"],$this->hrtc_api_secret_key)
    $action = "http://tempuri.org/GetAgentBalance";
    $request =  
              '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
                 <soapenv:Header/>
                 <soapenv:Body>
                    <tem:GetAgentBalance>
                       <!--Optional:-->
                       <tem:Agent>'.mcrypt_encrypt_e("bookonspot",$this->hrtc_api_secret_key).'</tem:Agent>
                    </tem:GetAgentBalance>
                 </soapenv:Body>
              </soapenv:Envelope>';
    logging_data("hrtc/".date("Y-m-d")."_GetAgentBalance.log",$request,"Request");
    $response = get_soap_data_hrtc($this->url, 'GetAgentBalance', $request, $action);
    logging_data("hrtc/".date("Y-m-d")."_GetAgentBalance.log",$response["GetAgentBalanceResult"],"Response");
    return $response["GetAgentBalanceResult"];
  }


  public function getAllTransactionDetails($input)
  {
    // mcrypt_encrypt_e($input["from_source_id"],$this->hrtc_api_secret_key)
    $action = "http://tempuri.org/GetAgentBalanceDetails";
    $request =  
              '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
                 <soapenv:Header/>
                 <soapenv:Body>
                    <tem:GetAgentBalanceDetails>
                       <!--Optional:-->
                       <tem:Agent>'.mcrypt_encrypt_e("bookonspot",$this->hrtc_api_secret_key).'</tem:Agent>
                       <!--Optional:-->
                       <tem:dateOfIssueFrom>'.mcrypt_encrypt_e(date("Y-m-d",strtotime($input["from_date"])),$this->hrtc_api_secret_key).'</tem:dateOfIssueFrom>
                       <!--Optional:-->
                       <tem:dateOfIssueTo>'.mcrypt_encrypt_e(date("Y-m-d",strtotime($input["to_date"])),$this->hrtc_api_secret_key).'</tem:dateOfIssueTo>
                    </tem:GetAgentBalanceDetails>
                 </soapenv:Body>
              </soapenv:Envelope>';
    logging_data("hrtc/".date("Y-m-d")."_GetAgentBalanceDetails.log",$request,"Request");
    $response = get_soap_data_hrtc($this->url, 'GetAgentBalanceDetails', $request, $action);
    logging_data("hrtc/".date("Y-m-d")."_GetAgentBalanceDetails.log",$response["GetAgentBalanceDetailsResult"],"Response");
    return $response["GetAgentBalanceDetailsResult"];
  }

  
  public function getAllBusTypes()
  {
    $action = "http://tempuri.org/GetAllBusTypes";
    $request =  
              '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
                 <soapenv:Header/>
                 <soapenv:Body>
                    <tem:GetAllBusTypes/>
                 </soapenv:Body>
              </soapenv:Envelope>';
    
    $encoded_response = get_soap_data_hrtc($this->url, 'GetAllBusTypes', $request, $action);

    $response = mcrypt_decrypt_e(trim($encoded_response["GetAllBusTypesResult"]),$this->hrtc_api_secret_key);
    return $response;
  }

  public function getAllCities()
  {
    $action = "http://tempuri.org/GetAllCities";
    $request =  
              '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <tem:GetAllCities/>
                   </soapenv:Body>
                </soapenv:Envelope>';

    // logging_data("hrtc",$request,"getAllCities Request");
    $encoded_response = get_soap_data_hrtc($this->url, 'GetAllBusTypes', $request, $action);
    $response = mcrypt_decrypt_e($encoded_response["GetAllCitiesResult"],$this->hrtc_api_secret_key);
    // logging_data("hrtc",$response,"getAllCities Response");
    return $response;
  }

  public function getAllSDPairs()
  {
    $action = "http://tempuri.org/GetAllSDPairs";
    $request =  
              '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <tem:GetAllSDPairs/>
                   </soapenv:Body>
                </soapenv:Envelope>';
    
    $encoded_response = get_soap_data_hrtc($this->url, 'GetAllSDPairs', $request, $action);

    $response = mcrypt_decrypt_e($encoded_response["GetAllSDPairsResult"],$this->hrtc_api_secret_key);
    return $response;
  }

  public function getSearchResults($input)
  {
    $response = [];
    $action = "http://tempuri.org/GetSearchResults";
    $request =  
              '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
                 <soapenv:Header/>
                 <soapenv:Body>
                    <tem:GetSearchResults>
                       <!--Optional:-->
                       <tem:SourceId>'.mcrypt_encrypt_e($input["from_source_id"],$this->hrtc_api_secret_key).'</tem:SourceId>
                       <!--Optional:-->
                       <tem:DestinationId>'.mcrypt_encrypt_e($input["to_source_id"],$this->hrtc_api_secret_key).'</tem:DestinationId>
                    </tem:GetSearchResults>
                 </soapenv:Body>
              </soapenv:Envelope>';
    
    logging_data("hrtc/".date("Y-m-d")."_GetSearchResults.log",$request,"Request");
    $encoded_response = get_soap_data_hrtc($this->url, 'GetSearchResults', $request, $action);

    if(is_array($encoded_response))
    {
      $response_string = mcrypt_decrypt_e($encoded_response["GetSearchResultsResult"],$this->hrtc_api_secret_key);
      $response_array = xml_to_array($response_string);
      logging_data("hrtc/".date("Y-m-d")."_GetSearchResults.log",$response_array,"response_array");
      if(isset($response_array["Table"]))
      {
        if(isset($response_array["Table"][0]))
        {
          $response = $response_array["Table"];
        }
        else
        {
          $response[] = $response_array["Table"];
        }
      }
    }

    return $response;
  }

  public function getAvailableBuses_for_single($input)
  {
      /*        VERY IMPORTANT NOTICE
       *Do not change trip_no format created in bus_data array
       *These parameter is used in seat layout
       */
      $hrtc_cacellation_charges = [];
      $data_to_store = [];
      $data = [];
      $data["bus_detail"] = array();
      $data["time_fare"]["bus_detail"] = array(
                                                "min_fare" => 0,
                                                "max_fare" => 0,
                                                "min_dep_tm" => 0,
                                                "max_dep_tm" => 0
                                             );
      $bus_data = [];
      $min_max_deptm = array();
      $min_max_fare = array();
      if(!empty($input["date"])){
        $current_journey_date = date("Y-m-d",strtotime($input["date"]));
      }
      if(!empty($input["date_of_jour"])){
        $current_journey_date = date("Y-m-d",strtotime($input["date_of_jour"]));
      }
      $total_seats_available = 0;

      $time_range = array(array('start' => '04:00:00', 'end' => '11:59:00', "shift" => "morning"),
                          array('start' => '12:00:00', 'end' => '15:59:00', "shift" => "afternoon"),
                          array('start' => '16:00:00', 'end' => '18:59:00', "shift" => "evening"),
                          array('start' => '19:00:00', 'end' => '23:59:00', "shift" => "night"),
                          array('start' => '00:00:00', 'end' => '03:59:00', "shift" => "mid_night")
                         );

      $hrtc_src_dest = $this->bus_source_destination_model->where(["source_name" => $input["from"], "destination_name" => $input["to"]])->find_all();
      if($hrtc_src_dest)
      {
        $hrtc_src_dest = $hrtc_src_dest[0];
      }
      else
      {
        return $data;
      }

      $bos_bustypes = $this->bus_type_model->find_all();
      if($bos_bustypes)
      {
        foreach ($bos_bustypes as $boskey => $bosvalue)
        {
          $bos_bus_types[trim($bosvalue->bus_type_name)]  = trim($bosvalue->id);
        }
      }

      $mapped_bus_types = array();
      $map_bus_type = $this->bus_type_model->get_operator_bus_types(HRTC_OPERATOR_ID);
      if($map_bus_type && isset($map_bus_type["mapped_type"]) && $map_bus_type["mapped_type"])
      {
        $mapped_bus_types = $map_bus_type["mapped_type"];
      }
      
      $response = $this->getSearchResults(["from_source_id" => $hrtc_src_dest->source_name_id,"to_source_id" => $hrtc_src_dest->destination_name_id]);
      if(!empty($response))
      {
          $hrtc_cacellation_charges   = $this->cancellation_charges_model->cancellation_charges(HRTC_OPERATOR_ID);
          foreach($response as $key => $value)
          {
              //If conditions for bus_type_id $bus_type_id
              if(array_key_exists(trim($value["Type"]), $mapped_bus_types))
              {
                $bus_type_id = $mapped_bus_types[trim($value["Type"])];
                if(!empty($bos_bus_types) && array_key_exists(trim($bus_type_id), $bos_bus_types))
                {
                  $bus_type_id = $bos_bus_types[trim($bus_type_id)];
                }
              }
              else
              {
                $bus_type_id = "Others";
              }

              $data_to_store[$value["RouteID"]] = $value;
              $actual_fare = $fare_with_conveycharge = getHrtcFareWithServiceCharge($value["Fare"]);

              /*******temp discount logic start******/
              $service_discount           = false;

              /*******temp discount logic end******/

              $total_seats_available += $value["SeatTo"]-$value["SeatFrom"];

              foreach($time_range as $i => $shift_time)
              {
                if(strtotime($time_range[$i]['start']) <= strtotime($value["DepartureTime"]) && strtotime($time_range[$i]['end']) >= strtotime($value["DepartureTime"]))
                {
                  $shift = $shift_time["shift"];
                }
              }

              $min_max_deptm[] = strtotime($current_journey_date." ".$value["DepartureTime"]);
              $min_max_fare[] = getHrtcFareWithServiceCharge($value["Fare"]);

              $bus_data[$key] = [
                                "op_user_id" => 0,
                                "from_stop_code" => trim($hrtc_src_dest->source_name),
                                "to_stop_code" => trim($hrtc_src_dest->destination_name),
                                "op_name" => "HRTC",
                                "trip_no" => trim($value["RouteID"])."_".trim($value["SourceID"])."_".trim($value["DestinationID"])."_".trim($value["SeatFrom"])."_".trim($value["SeatTo"]),
                                "op_id" => 4,
                                "route_no_name" => $value["RouteName"],
                                "bus_stop_name" => $hrtc_src_dest->destination_name,
                                "op_trip_no" => $value["RouteID"],
                                "service_id" => $value["RouteID"],
                                "seat_layout_id" => $value["TypeID"],
                                "layout_name" => $value["Type"],
                                "from_stop" => trim($hrtc_src_dest->source_name),
                                "to_stop" => trim($hrtc_src_dest->destination_name),
                                "dept_tm" => $current_journey_date." ".date("H:i:s",strtotime($value["DepartureTime"])),//change this
                                "sch_departure_date" => date("d-m-Y",strtotime($current_journey_date)),
                                "arrival_date" => "NA",
                                "arrival_tm" => "NA",
                                "day" => "NA",
                                "bus_type_id" => $bus_type_id,
                                "bus_type_name" => (array_key_exists(trim($value["Type"]), $mapped_bus_types)) ? $mapped_bus_types[trim($value["Type"])] : "Others",
                                "sch_departure_tm" => date("H:i:s",strtotime($value["DepartureTime"])),
                                "op_bus_type_name" => $value["Type"],
                                "op_bus_type_cd" => $value["TypeID"],
                                "is_child_concession" => ($value["TypeID"] == 1 || $value["TypeID"] == 2) ? "Y" : "N",
                                "tot_seat_available" => $value["SeatTo"]-$value["SeatFrom"],
                                "arrival_day" => "NA",
                                "arrival_time" => "NA",
                                "provider_id" => 1,
                                "provider_type" => "operator",
                                "barr" => [],
                                "str_dep_tm" => strtotime($value["DepartureTime"]),
                                "str_dep_tm_his" => $value["DepartureTime"],
                                "arrival_date_time" => "NA",
                                "str_dep_tm_hm" =>strtotime($current_journey_date." ".date("H:i:s",strtotime($value["DepartureTime"]))),
                                "service_discount" => $service_discount,
                                "actual_seat_fare" => $actual_fare,
                                "seat_fare" => $fare_with_conveycharge,
                                "mticket_allowed" => false,
                                "cancellation_policy" => $hrtc_cacellation_charges,
                                "inventory_type" => 0,
                                "travel_time" => "NA",
                                "short_travel_time" => "NA",
                                "bus_travel_duration" => "NA",
                                "travel_duration" => "NA",
                                "shift" => $shift
                              ];
          }

          if($bus_data)
          {
            $data["bus_detail"] = $bus_data;
            $data["time_fare"] = [
                                    "bus_detail" => [
                                                      "min_fare" => min($min_max_fare),
                                                      "max_fare" => max($min_max_fare),
                                                      "min_dep_tm" => min($min_max_deptm),
                                                      "max_dep_tm" => max($min_max_deptm)
                                                    ]
                                  ];
            $data["extra_detail"] = [
                                      "total_seats_available" => $total_seats_available,
                                      "stu_details" => [
                                                        "min_duration" => "NA",
                                                        "max_duration" => "NA",
                                                        "min_fare" => (float)min($min_max_fare),
                                                        "max_fare" => (float)max($min_max_fare),
                                                        "min_dep_tm" => (float)min($min_max_deptm),
                                                        "start_at" => date("h:i A",(float)min($min_max_deptm)),
                                                        "mticket_allowed" => false,
                                                        "max_dep_tm" => (float)max($min_max_deptm),
                                                        "stu_total_seats" => $total_seats_available,
                                                        "stu_total_buses" => count($data["bus_detail"]),
                                                        "op_name_array" => ["hrtc"]
                                                       ]
                                    ];
          }

          if($data_to_store)
          {
            $bus_service_where = array("from" => $input['from'],"op_name" => "hrtc", "to" => $input['to'], "date" => $current_journey_date);
            $is_service_exists = $this->api_bus_services_model->where($bus_service_where)->find_all();
            
            $data_to_store_encode = json_encode($data_to_store);

            if(!$is_service_exists)
            {
              $hrtc_save = array(
                                      'from' => $input['from'],
                                      'to' => $input['to'],
                                      'op_name' => "hrtc",
                                      'date' => $current_journey_date,
                                      'bus_service' => $data_to_store_encode,
                                    );

              $hrtc_bus_service_id = $this->api_bus_services_model->insert($hrtc_save);
            }
            else
            {
              $this->api_bus_services_model->update_where(array('from' => $input['from'],'to' => $input['to'],'op_name' => 'hrtc','date' => $current_journey_date),"",array("bus_service" => $data_to_store_encode));
            }
          }
      }

      return $data;
  }

  public function getAvailableBuses($input,$comm_id_data)
  {
      /*        VERY IMPORTANT NOTICE
       *Do not change trip_no format created in bus_data array
       *These parameter is used in seat layout
       */
      $hrtc_cacellation_charges = [];
      $data_to_store = [];
      $data = [];
      $data["bus_detail"] = array();
      $data["time_fare"]["bus_detail"] = array(
                                                "min_fare" => 0,
                                                "max_fare" => 0,
                                                "min_dep_tm" => 0,
                                                "max_dep_tm" => 0
                                             );
      $bus_data = [];
      $min_max_deptm = array();
      $min_max_fare = array();
      $current_journey_date = date("Y-m-d",strtotime($input["date_of_jour"]));
      $total_seats_available = 0;

      $time_range = array(array('start' => '04:00:00', 'end' => '11:59:00', "shift" => "morning"),
                          array('start' => '12:00:00', 'end' => '15:59:00', "shift" => "afternoon"),
                          array('start' => '16:00:00', 'end' => '18:59:00', "shift" => "evening"),
                          array('start' => '19:00:00', 'end' => '23:59:00', "shift" => "night"),
                          array('start' => '00:00:00', 'end' => '03:59:00', "shift" => "mid_night")
                         );

      /*$hrtc_src_dest = $this->bus_source_destination_model->where(["source_name" => $input["from"], "destination_name" => $input["to"]])->find_all();*/
      $all_hrtc_src_dest = $this->bus_source_destination_model->like("source_name",$input["from"],"both")->like("destination_name",$input["to"],"both")->find_all();
      if(!$all_hrtc_src_dest)
      {
        return $data;
      }

      /*if($hrtc_src_dest)
      {
        $hrtc_src_dest = $hrtc_src_dest[0];
      }
      else
      {
        return $data;
      }*/

      $bos_bustypes = $this->bus_type_model->find_all();
      if($bos_bustypes)
      {
        foreach ($bos_bustypes as $boskey => $bosvalue)
        {
          $bos_bus_types[trim($bosvalue->bus_type_name)]  = trim($bosvalue->id);
        }
      }

      $mapped_bus_types = array();
      $map_bus_type = $this->bus_type_model->get_operator_bus_types(HRTC_OPERATOR_ID);
      if($map_bus_type && isset($map_bus_type["mapped_type"]) && $map_bus_type["mapped_type"])
      {
        $mapped_bus_types = $map_bus_type["mapped_type"];
      }

      $hrtc_cacellation_charges   = $this->cancellation_charges_model->cancellation_charges(HRTC_OPERATOR_ID);

      foreach ($all_hrtc_src_dest as $outer_key => $hrtc_src_dest)
      {
        $response = $this->getSearchResults(["from_source_id" => $hrtc_src_dest->source_name_id,"to_source_id" => $hrtc_src_dest->destination_name_id]);

        if(!empty($response))
        {
          foreach($response as $key => $value)
          {
            if(date("Y-m-d H:i:s") <= $current_journey_date." ".date("H:i:s",strtotime($value["DepartureTime"]))) {

              //If conditions for bus_type_id $bus_type_id
              if(array_key_exists(trim($value["Type"]), $mapped_bus_types))
              {
                $bus_type_id = $mapped_bus_types[trim($value["Type"])];
                if(!empty($bos_bus_types) && array_key_exists(trim($bus_type_id), $bos_bus_types))
                {
                  $bus_type_id = $bos_bus_types[trim($bus_type_id)];
                }
              }
              else
              {
                $bus_type_id = "Others";
              }
              $data_to_store[$value["RouteID"]] = $value;
              $actual_fare = $fare_with_conveycharge = getHrtcFareWithServiceCharge($value["Fare"]);

              /*******temp discount logic start******/
              $service_discount           = false;
              $discount_id                = '0';
              /*******temp discount logic end******/

              $total_seats_available += $value["SeatTo"]-$value["SeatFrom"];

              foreach($time_range as $i => $shift_time) {
                if(strtotime($time_range[$i]['start']) <= strtotime($value["DepartureTime"]) && strtotime($time_range[$i]['end']) >= strtotime($value["DepartureTime"])) {
                  $shift = $shift_time["shift"];
                }
              }
              // Commission calculation (percentage)
              $commission_fare = array();
              $agent_commission = 0;
              eval(FME_AGENT_ROLE_ID);
              if(in_array($this->session->userdata("role_id"), $fme_agent_role_id) || ($this->session->userdata("role_id") == ORS_AGENT_USERS_ROLE_ID)) {
                  if(!empty($comm_id_data[0]->commission_id) && $comm_id_data[0]->commission_id > 0 )
                  {
                      $commission_fare[$i]['base_fare'] = $actual_fare;
                      $agent_commission = agent_ag_calculate_commission($commission_fare,1,$this->config->item("hrtc_operator_id"),$comm_id_data[0]->commission_id,$bus_type_id); 
                  }
              }
              if($agent_commission == "") {
                  $agent_commission = 0;
              }              
              $min_max_deptm[] = strtotime($current_journey_date." ".$value["DepartureTime"]);
              $min_max_fare[] = getHrtcFareWithServiceCharge($value["Fare"]);
              $bus_data[] = [
                                "bus_travel_id" => trim($value["RouteID"])."_".trim($value["SourceID"])."_".trim($value["DestinationID"])."_".trim($value["SeatFrom"])."_".trim($value["SeatTo"]),
                                "op_user_id" => 0,
                                "from_stop_code" => trim($hrtc_src_dest->source_name),
                                "to_stop_code" => trim($hrtc_src_dest->destination_name),
                                "op_name" => "HRTC",
                                "trip_no" => trim($value["RouteID"])."_".trim($value["SourceID"])."_".trim($value["DestinationID"])."_".trim($value["SeatFrom"])."_".trim($value["SeatTo"]),
                                "op_id" => 4,
                                "route_no_name" => $value["RouteName"],
                                "bus_stop_name" => $hrtc_src_dest->destination_name,
                                "op_trip_no" => $value["RouteID"],
                                "service_id" => $value["RouteID"],
                                "seat_layout_id" => $value["TypeID"],
                                "layout_name" => $value["Type"],
                                "from_stop" => trim($hrtc_src_dest->source_name),
                                "to_stop" => trim($hrtc_src_dest->destination_name),
                                "dept_tm" => $current_journey_date." ".date("H:i:s",strtotime($value["DepartureTime"])),//change this
                                "sch_departure_date" => date("d-m-Y",strtotime($current_journey_date)),
                                "arrival_date" => "NA",
                                "arrival_tm" => "NA",
                                "day" => "NA",
                                "bus_type_id" => $bus_type_id,
                                "bus_type_name" => (array_key_exists(trim($value["Type"]), $mapped_bus_types)) ? $mapped_bus_types[trim($value["Type"])] : "Others",
                                "sch_departure_tm" => date("H:i:s",strtotime($value["DepartureTime"])),
                                "op_bus_type_name" => $value["Type"],
                                "op_bus_type_cd" => $value["TypeID"],
                                "is_child_concession" => ($value["TypeID"] == 1 || $value["TypeID"] == 2) ? "Y" : "N",
                                "tot_seat_available" => $value["SeatTo"]-$value["SeatFrom"],
                                "arrival_day" => "NA",
                                "arrival_time" => "NA",
                                "provider_id" => 1,
                                "provider_type" => "operator",
                                "barr" => [],
                                "str_dep_tm" => strtotime($value["DepartureTime"]),
                                "str_dep_tm_his" => $value["DepartureTime"],
                                "arrival_date_time" => "NA",
                                "str_dep_tm_hm" =>strtotime($current_journey_date." ".date("H:i:s",strtotime($value["DepartureTime"]))),
                                "service_discount" => $service_discount,
                                "actual_seat_fare" => $actual_fare,
                                "seat_fare" => $fare_with_conveycharge,
                                "mticket_allowed" => false,
                                "cancellation_policy" => $hrtc_cacellation_charges,
                                "inventory_type" => 0,
                                "travel_time" => "NA",
                                "short_travel_time" => "NA",
                                "bus_travel_duration" => "NA",
                                "travel_duration" => "NA",
                                "shift" => $shift,
                                "agent_commission_percentage"=> $agent_commission
                              ];
            }
          }
        }
      }

      if($data_to_store)
      {
        $bus_service_where = array("from" => $input['from'],"op_name" => "hrtc", "to" => $input['to'], "date" => $current_journey_date);
        $is_service_exists = $this->api_bus_services_model->where($bus_service_where)->find_all();
        
        $data_to_store_encode = json_encode($data_to_store);

        if(!$is_service_exists)
        {
          $hrtc_save = array(
                                  'from' => $input['from'],
                                  'to' => $input['to'],
                                  'op_name' => "hrtc",
                                  'date' => $current_journey_date,
                                  'bus_service' => $data_to_store_encode,
                                );

          $hrtc_bus_service_id = $this->api_bus_services_model->insert($hrtc_save);
        }
        else
        {
          $this->api_bus_services_model->update_where(array('from' => $input['from'],'to' => $input['to'],'op_name' => 'hrtc','date' => $current_journey_date),"",array("bus_service" => $data_to_store_encode));
        }
      }
            
      if($bus_data)
      {
        $data["bus_detail"] = $bus_data;
        $data["time_fare"] = [
                                "bus_detail" => [
                                                  "min_fare" => min($min_max_fare),
                                                  "max_fare" => max($min_max_fare),
                                                  "min_dep_tm" => min($min_max_deptm),
                                                  "max_dep_tm" => max($min_max_deptm)
                                                ]
                              ];
        $data["extra_detail"] = [
                                  "total_seats_available" => $total_seats_available,
                                  "stu_details" => [
                                                    "min_duration" => "NA",
                                                    "max_duration" => "NA",
                                                    "min_fare" => (float)min($min_max_fare),
                                                    "max_fare" => (float)max($min_max_fare),
                                                    "min_dep_tm" => (float)min($min_max_deptm),
                                                    "start_at" => date("h:i A",(float)min($min_max_deptm)),
                                                    "mticket_allowed" => false,
                                                    "max_dep_tm" => (float)max($min_max_deptm),
                                                    "stu_total_seats" => $total_seats_available,
                                                    "stu_total_buses" => count($data["bus_detail"]),
                                                    "op_name_array" => ["hrtc"]
                                                   ]
                                ];
      }
      return $data;
  }
  

  public function get_seat_layout($input)
  {
    $seat_service_tax_amt = 0.00;
    $seat_service_tax_perc = 0.00;
    $op_service_charge_amt = 0.00;
    $op_service_charge_perc = 0.00;
    $route_source_destination_array = [];
    $response = array();

    $tot_data_seat_layout       =   [];
    $current_available_seats    =   [];
    $stop_detail                =   [];
    $bus_service_where = array("from" => $input["from"], "to" => $input["to"],"op_name" => "hrtc", "date" => date("Y-m-d",strtotime($input['date'])));
    $all_bus_services = $this->api_bus_services_model->where($bus_service_where)->find_all();
    if($all_bus_services)
    {
        $decoded_bus_services = json_decode($all_bus_services[0]->bus_service,true);
        $current_bus_services = $decoded_bus_services[$input["op_trip_no"]];
    }

    $seat_allowed_from = $current_bus_services["SeatFrom"];
    $seat_allowed_till = $current_bus_services["SeatTo"];
    if($current_bus_services)
    {
      // $booked_seats = $this->hrtc_api->getAvailabilityStatus(["route_id" => $current_bus_services["RouteID"], "journey_date" => date("Y-m-d",strtotime($input['date']))]);
      $booked_seats = $this->getAvailabilityStatus(["route_id" => $current_bus_services["RouteID"], "journey_date" => date("Y-m-d",strtotime($input['date']))]);
      
      if(!empty($booked_seats))
      {
        $hrtc_booked_seats          =   array_column($booked_seats, 'SeatNo');
        $hrtc_allowed_seats         =   range($seat_allowed_from,$seat_allowed_till);
        $current_available_seats    =   array_values(array_diff($hrtc_allowed_seats, $hrtc_booked_seats));
      }
      else
      {
        $current_available_seats    =   range($seat_allowed_from,$seat_allowed_till);
      }
      
      $tot_data_seat_layout         =   $this->op_seat_layout_model->get_seat_layout_for_operator(trim($current_bus_services["Type"]));
    }

    if($tot_data_seat_layout)
    {
        $current_bus_type_id = 0;
        $bus_type_detail = $this->bus_type_model->get_operator_bus_type_detail(["operator_bus_type_name" => trim($current_bus_services["Type"]),"operator_id" => HRTC_OPERATOR_ID]);
        if($bus_type_detail)
        {
            $current_bus_type_id = $bus_type_detail[0]["bus_type_id"];
        }

        $tot_fare   =   getHrtcFareWithServiceCharge($current_bus_services["Fare"]);
        $seat_fare_detail = array();
        $total_available_seats = 0;
        $fare_with_conveycharge = getConveyCharges($tot_fare,'hrtc');

        $current_actual_seat_value = $fare_with_conveycharge;
        $current_actual_seat_value_with_tax = $fare_with_conveycharge;

        $basic_fare_array = array($fare_with_conveycharge);
        $fare_array = array($fare_with_conveycharge);

        $response['actual_fare'] = implode("/",array_filter($basic_fare_array));
        $response['actual_fare_array'] = implode("/",array_filter($fare_array));
        /*******temp discount logic start******/
        $discount_array = array();
        
        foreach ($tot_data_seat_layout as $key => $value)
        {
            $current_seat_type = "";
            $horizontal_seat = false;
            $vertical_seat = false;

            if(strtolower($value->quota_type) == "ladies")
            {
              $current_seat_type = "ladies";
            }
            else if(strtolower($value->quota_type) == "driver")
            {
              $current_seat_type = "driver";
            }

            $pos = $value->row_no . "X" . $value->col_no;
            $birth_no = $value->berth_no;

            $seat_layout[$birth_no][$pos] = array(
                                                    'seat_no' => $value->seat_no,
                                                    'actual_seat_fare' => $current_actual_seat_value_with_tax,
                                                    'actual_seat_basic_fare' => $current_actual_seat_value,
                                                    'seat_fare' => $fare_with_conveycharge,
                                                    'seat_basic_fare' => $fare_with_conveycharge,
                                                    'seat_service_tax_amt' => $seat_service_tax_amt,
                                                    'seat_service_tax_perc' => $seat_service_tax_perc,
                                                    'op_service_charge_amt' => $op_service_charge_amt,
                                                    'op_service_charge_perc' => $op_service_charge_perc,
                                                    'row_no' => $value->row_no,
                                                    'col_no' => $value->col_no,
                                                    'seat_type' => $value->seat_type,
                                                    'berth_no' => $birth_no,
                                                    'quota' => strtolower($value->quota_type),
                                                    'pos' => $pos,
                                                    'current_seat_type' => $current_seat_type,
                                                    'horizontal_seat' => $horizontal_seat,
                                                    'vertical_seat' => $vertical_seat,
                                                    'seat_discount' => $current_seat_discount,
                                                    'agent_commission' => 0,
                                                    'agent_comm_tds' =>  0,
                                                ); 

            if(!empty($current_available_seats) && in_array($value->seat_no, $current_available_seats))
            {
              $seat_layout[$birth_no][$pos]["available"] = 'Y';
              $total_available_seats++;
            }
            else
            {
              $seat_layout[$birth_no][$pos]["available"] = 'N';
            }
            
            if($seat_layout[$birth_no][$pos]["available"] == 'N' && strtolower($value->quota_type) == "ladies")
            {
                $seat_layout[$birth_no][$pos]["booked_by"] = 'F';
            }

            $seat_fare_detail[$birth_no][$value->seat_no] = $seat_layout[$birth_no][$pos];
        }



        $response['fare'] = implode("/",array_filter($basic_fare_array));
        $response['fare_array'] = implode("/",array_filter($fare_array));
        $response['total_seats'] = $tot_data_seat_layout[0]->ttl_seats;
        $response['total_available_seats'] = $total_available_seats;
        $response['available_seats'] = $current_available_seats;
        $response['tot_rows'] = $tot_data_seat_layout[0]->no_rows;
        $response['tot_cols'] = $tot_data_seat_layout[0]->no_cols;
        $response['tot_berth'] = 1; //Only One Berth IN HRTC. Please Discuss this.
        $response['seat_layout'] = $seat_layout;
        $response['seat_fare_detail'] = $seat_fare_detail;
        $response['trip_no'] = $input['trip_no'];
        $response['stop_detail'] = $stop_detail;
        $response['flag'] = '@#success#@';
    }

    return $response;
  } 

  public function getAvailabilityStatus($input)
  {
    $response  = [];
    $action = "http://tempuri.org/GetAvailabilityStatus";
    $request =  
              '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
                 <soapenv:Header/>
                 <soapenv:Body>
                    <tem:GetAvailabilityStatus>
                       <!--Optional:-->
                       <tem:routeId>'.mcrypt_encrypt_e($input["route_id"],$this->hrtc_api_secret_key).'</tem:routeId>
                       <!--Optional:-->
                       <tem:dateOfJourney>'.mcrypt_encrypt_e(date("Y-m-d",strtotime($input["journey_date"])),$this->hrtc_api_secret_key).'</tem:dateOfJourney>
                    </tem:GetAvailabilityStatus>
                 </soapenv:Body>
              </soapenv:Envelope>';
    
    logging_data("hrtc/".date("Y-m-d")."_GetAvailabilityStatus.log",$request,"Request");
    $encoded_response = get_soap_data_hrtc($this->url, 'GetAvailabilityStatus', $request, $action);
    logging_data("hrtc/".date("Y-m-d")."_GetAvailabilityStatus.log",$encoded_response,"encoded_response");
    if(is_array($encoded_response))
    {
      $response_string = mcrypt_decrypt_e($encoded_response["GetAvailabilityStatusResult"],$this->hrtc_api_secret_key);
      $response_array = xml_to_array($response_string);
      logging_data("hrtc/".date("Y-m-d")."_GetAvailabilityStatus.log",$response_array,"response_array");
      if(isset($response_array["Table"]))
      {
        $response = $response_array["Table"];
      }
      /*else
      {
        $custom_error_mail = array(
                                    "custom_error_subject" => "HRTC Seat layout response not came from hrtc.",
                                    "custom_error_message" => "Journey Date - ".date("Y-m-d",strtotime($input["journey_date"]))." <br/></br>Route Id - ".$input["route_id"]."<br/></br>Request - ".$request." <br/></br> Response - Not came."
                                  );

        $this->common_model->developer_custom_error_mail($custom_error_mail,"management");
      }*/
    }
    else
    {
         $custom_error_mail = array(
                                    "custom_error_subject" => "HRTC Seat layout response not came from hrtc.",
                                    "custom_error_message" => "Journey Date - ".date("Y-m-d",strtotime($input["journey_date"]))." <br/></br>Route Id - ".$input["route_id"]."<br/></br>Request - ".$request." <br/></br> Response - Not came."
                                  );

        $this->common_model->developer_custom_error_mail($custom_error_mail,"management");
    }

    return $response;
  }



  public function getSeatAvailability($input)
  {
    $data = array();
    $booked_seats     = [];
    $bus_type_detail  = [];
    $SeatAvailabilityResponse = [];
    $trip_data = [];
    $rid_array = explode("_", $input["trip_no"]);
    $input["op_trip_no"] = $rid_array[0];
    
    $current_available_seats    =   [];
    $bus_service_where = array("from" => $input["boarding_stop_name"], "to" => $input["destination_stop_name"],"op_name" => "hrtc", "date" => date("Y-m-d",strtotime($input['date'])));
    $all_bus_services = $this->api_bus_services_model->where($bus_service_where)->find_all();
    if($all_bus_services)
    {
      $decoded_bus_services = json_decode($all_bus_services[0]->bus_service,true);
      $current_bus_services = $decoded_bus_services[$input["op_trip_no"]];
    }

    $seat_allowed_from = $current_bus_services["SeatFrom"];
    $seat_allowed_till = $current_bus_services["SeatTo"];
    if($current_bus_services)
    {
      // $booked_seats = $this->hrtc_api->getAvailabilityStatus(["route_id" => $current_bus_services["RouteID"], "journey_date" => date("Y-m-d",strtotime($input['date']))]);
      $booked_seats = $this->getAvailabilityStatus(["route_id" => $current_bus_services["RouteID"], "journey_date" => date("Y-m-d",strtotime($input['date']))]);
      if(!empty($booked_seats))
      {
        $hrtc_booked_seats          =   array_column($booked_seats, 'SeatNo');
        $hrtc_allowed_seats         =   range($seat_allowed_from,$seat_allowed_till);
        $current_available_seats    =   array_values(array_diff($hrtc_allowed_seats, $hrtc_booked_seats));
      }
      else
      {
        $current_available_seats    =   range($seat_allowed_from,$seat_allowed_till);
      }

      $current_bus_type_id = 0;
      $bus_type_detail = $this->bus_type_model->get_operator_bus_type_detail(["operator_bus_type_name" => trim($current_bus_services["Type"]),"operator_id" => HRTC_OPERATOR_ID]);
      if($bus_type_detail)
      {
        $current_bus_type_id = $bus_type_detail[0]["bus_type_id"];
      }

      $seat_format_details = [];
      $berth_no = 1;
      $bus_layout_details = $this->op_seat_layout_model->get_seat_layout_for_operator(trim($current_bus_services["Type"]));
      foreach ($bus_layout_details as $tkey => $tvalue)
      {
        $is_ladies_seat = (strtolower($tvalue->quota_type) == "ladies") ? true : false;
        $seat_format_details[$berth_no][$tvalue->seat_no] = array('is_ladies_seat' => $is_ladies_seat);
      }

      $is_child_concession = ($current_bus_services["TypeID"] == 1 || $current_bus_services["TypeID"] == 2) ? true : false; 
      $SeatAvailabilityResponse = [
                                    "serviceId" => $input["op_trip_no"],
                                    "fromStopId" => $current_bus_services["SourceID"],
                                    "toStopId" => $current_bus_services["DestinationID"],
                                    "dateOfJourney" => $input["date"],
                                    "fare" => getHrtcFareWithServiceCharge($current_bus_services["Fare"]),
                                    "sleeperFare" => "0.00",
                                    "childFare" => getHrtcChildFare($is_child_concession, $current_bus_services),
                                    "availableSeats" => 
                                              [
                                                  "availableSeat" => ["1" => $current_available_seats]
                                              ],
                                    "seats_details"  => $seat_format_details,
                                  ];

      $trip_data  = [
                      "sch_departure_tm" => date("H:i:s", strtotime($current_bus_services["DepartureTime"])),
                      /*"arrival_time" => date("Y-m-d",strtotime($input['date']))." ".date("h:i:s", strtotime($current_bus_services["DepartureTime"])),*/
                      "arrival_time" => "",
                      "is_child_concession" => $is_child_concession,
                      "user_type" => $input["op_id"],
                      "op_id" => $input["op_id"],
                      "bus_type_id" => $current_bus_type_id,
                      "service_id" => $input["op_trip_no"],
                      "op_trip_no" => $input["op_trip_no"],
                      "inventory_type" => "",
                      "id_proof_required" => true,
                      'provider_id' => $input["provider_id"],
                      'provider_type' => $input["provider_type"]
                    ];

      $from_data[] = [
                      "op_id" => HRTC_OPERATOR_ID,
                      "op_stop_name" => ucwords(strtolower($input["from"])),
                      "op_stop_cd" => "",
                      "bus_stop_name" => ucwords(strtolower($input["from"])) 
                      ];


      $to_data[] = [
                    "op_id" => HRTC_OPERATOR_ID,
                    "op_stop_name" => ucwords(strtolower($input["to"])),
                    "op_stop_cd" => "",
                    "bus_stop_name" => ucwords(strtolower($input["to"])) 
                    ];


      $data = [
                "SeatAvailabilityResponse" => $SeatAvailabilityResponse,
                "error" => [],
                "operator_bus_type_detail" => $bus_type_detail,
                "flag" => "success",
                "trip_data" => $trip_data,
                "from_data" => $from_data,
                "to_data" => $to_data
              ];
    }
    return $data;
  }



  public function do_temp_booking($data_to_process)
  {
    $current_bus_services = [];
    
    $data  = array();
    $input = $data_to_process["input"];
    
    if(!empty($input)) {
      $available_detail = $data_to_process['available_detail'];
      $input["op_trip_no"] = (isset($data_to_process['available_detail']) && isset($data_to_process['available_detail']['service_id']) && $data_to_process['available_detail']['service_id'] != "") ? $data_to_process['available_detail']['service_id'] : 0;
      $email = $input['email_id'];
      $mobile_no = $input['mobile_no'];
      $agent_email = $input['agent_email_id'];
      $agent_mobile_no = $input['agent_mobile_no'];
      $user_id = $input['user_id'];
        
      $service_id = $available_detail['service_id'];
      $from = $available_detail['from'];
      $to = $available_detail['to'];
      $date = $available_detail['date'];

      $tot_seat = 0;
      $seat = $input['seat'];
      foreach ($seat as $berth => $seatvalue) {
        $tot_seat += count($seatvalue);
      }

      if($tot_seat > 6) {
        redirect(base_url());
      }

      $bus_service_where = array("from" => $available_detail['boarding_stop_name'], "to" => $available_detail['destination_stop_name'],"op_name" => "hrtc", "date" => date("Y-m-d",strtotime($date)));
      $all_bus_services = $this->api_bus_services_model->where($bus_service_where)->find_all();

      if($all_bus_services) {
        $decoded_bus_services = json_decode($all_bus_services[0]->bus_service,true);
        $current_bus_services = $decoded_bus_services[$input["op_trip_no"]];
      }

      if(!$current_bus_services) {
        $data['payment_confirm_booking_status'] = "error";
        return $data;
      }

      $is_child_concession = ($current_bus_services["TypeID"] == 1 || $current_bus_services["TypeID"] == 2) ? true : false; 

      $fare = getHrtcFareWithServiceCharge($current_bus_services["Fare"]);
      $child_basic_seat_fare = 0;
      $child_seat_fare = ceil($child_basic_seat_fare + ($child_basic_seat_fare*HRTC_SERVICE_CHARGE_PERCENTAGE/100));
      $tot_fare = 0;
      $passenger_id = 1;

      $api_data['from'] = $from;
      $api_data['to'] = $to;
      $api_data['date'] = $date;
      $api_data['service_id'] = $service_id;
      $api_data['from_stop_id'] = $available_detail['from_stop_id'];
      $api_data['to_stop_id'] = $available_detail['to_stop_id'];
      $api_data['user_type'] = $available_detail['user_type'];
      $api_data["email"] = $email;
      $api_data["mobile_no"] = $mobile_no;
      $api_data["input"] = $input;
      $originate_boarding_time = date("Y-m-d H:i:s",strtotime($api_data['date']." ".$current_bus_services["OriginDepTime"]));
      $dept_boarding_time = date("Y-m-d H:i:s",strtotime($api_data['date']." ".$available_detail['sch_dept_time']));
      // Insert into tickets table
      $insert_ticket_array = [
                              "ticket_type" => 1,
                              "request_type" => 'B',
                              "ticket_status" => 'A',
                              "bus_service_no" => $api_data['service_id'],
                              "dept_time" => $originate_boarding_time,
                              "boarding_time" => $dept_boarding_time,
                              "alighting_time" => "0000-00-00 00:00:00",
                              "droping_time" => "0000-00-00 00:00:00",
                              "from_stop_cd" => $available_detail['from_stop_id'],
                              "till_stop_cd" => $available_detail['to_stop_id'],
                              "from_stop_name" => $available_detail['from'],
                              "till_stop_name" => $available_detail['to'],
                              "boarding_stop_name" => $available_detail['from'],
                              "destination_stop_name" => $available_detail['to'],
                              "boarding_stop_cd" => $available_detail['boarding_stop_name'],
                              "destination_stop_cd" => $available_detail['destination_stop_name'],
                              "op_id" => $available_detail['op_id'],
                              "provider_id" => $available_detail['provider_id'],
                              "provider_type" => $available_detail['provider_type'],
                              "booked_from" => "hrtc",
                              "num_passgr" =>  $tot_seat,
                              "cancellation_rate" =>  0,
                              "issue_time" => date("Y-m-d H:i:s"),
                              "pay_id" => 1,
                              "session_no" => $this->input->cookie('ci_session'),
                              "op_name" => $available_detail['op_name'],
                              "user_email_id" => $email,
                              "mobile_no" => $mobile_no,
                              "agent_email_id" => $agent_email,
                              "agent_mobile_no" => $agent_mobile_no,
                              "inserted_by" => $this->session->userdata("booking_user_id"),
                              "role_id" => (!empty($this->session->userdata("role_id"))) ? $this->session->userdata("role_id") : 0,
                              "booked_by" => (!empty($this->session->userdata("role_name"))) ? strtolower($this->session->userdata("role_name")) : "guest_user",
                              "bus_type" => (isset($available_detail['bus_type']['op_bus_type_name'])) ? $available_detail['bus_type']['op_bus_type_name'] : "",
                              "status" => "N",
                              "tds_per" => $this->config->item('tds'),
                              "ip_address" => $this->input->ip_address(),
                             ];

      $ticket_id = $this->tickets_model->insert($insert_ticket_array);

      $extra_charges_value = 0;
      $total_extra_charges_value = 0;
      $total_convey_charge = 0;
      $total_ticket_discount = 0;
      $seat_service_tax_amt = 0.00;
      $seat_service_tax_perc = 0.00;
      $op_service_charge_amt = 0.00;
      $op_service_charge_perc = 0.00;
      $total_service_tax_amount = 0;
      $total_op_service_charge_amount = 0;
      $total_basic_fare = 0;

      //get Base commission data from base commission table 
      $base_com_data = array(
                "op_id" => $available_detail['op_id'],
                "api_provider" => $available_detail['provider_id']
            );
      $base_com_response = $this->base_commission_model->base_commission_detail($base_com_data);
      if($ticket_id > 0) {
        $psgr_no = 1;
        foreach ($seat as $berth_no => $seat_detail) {
          foreach ($seat_detail as $key => $value)  {
            $value['seat_no'] = $key;
            $value['berth_no'] = $berth_no;
    
            /*
             *                  NOTICE : VERY IMP LOGIC FOR FARE
             *if is_child_concession in op_bus_types is Y then assign child seat fare
             *if is_child_concession in op_bus_types is N then assign adult seat fare 
             */
            /*if($is_child_concession)
            {
              $value['fare'] = ($value['age'] <= 12) ? $child_seat_fare : $fare;
            }
            else
            {
            $value['fare'] = $fare;
            }*/
            $value['fare'] = $fare;

            /*
            *NOTICE: MOST IMP
            *
            *DO NOT DELETE BELOW COMMENTED LINES. THESE ARE IMP.
            *BELOW WRITTEN LOGIC IS STATIC CONVEY CHARGES LOGIC.
            *THE LOGIC WRITTEN BELOW IS TEMP ALTERNATIVE FOR DYNAMIC LOGIC
            *CHANGE THIS AFTER DISCUSSION
            */
            /**************CONVEY CHARGE STATIC LOGIC START *****************/
            $three_percent_tot_fare = 0;
            $fifty_per_ticket = 0;
            $extra_charges_value = 0;
            $convey_charge_type = "";
            $convey_charge_value = "";

            $total_extra_charges_value += $extra_charges_value;
            /**************CONVEY CHARGE STATIC LOGIC END *****************/

            $current_seat_fare_with_conveycharges = $value['fare']+$extra_charges_value;

            /**************DISCOUNT LOGIC START *****************/
            
            /**************DISCOUNT LOGIC END *****************/

            $tot_fare += $value['fare'];
            $total_basic_fare += $current_bus_services["Fare"];

            $api_data['passenger'][] = $value;
            $this->ticket_details_model->ticket_id = $ticket_id;
            $this->ticket_details_model->psgr_no = $psgr_no;
            $this->ticket_details_model->psgr_name = $value['name'];
            $this->ticket_details_model->psgr_age = $value['age'];
            $this->ticket_details_model->psgr_sex = $value['gender'];
            $this->ticket_details_model->psgr_type = "A";
            $this->ticket_details_model->concession_cd = 1; // if exists /in case of rsrtc 60 pass ct 
            $this->ticket_details_model->concession_rate = 1; // from api /if exists /in case of rsrtc 60 pass ct 
            $this->ticket_details_model->discount = 1; // if available from api
            $this->ticket_details_model->is_home_state_only = 1; // Y or No //not required now
            $this->ticket_details_model->fare_amt = $value['fare'];
            $this->ticket_details_model->adult_basic_fare = $current_bus_services["Fare"];
            $this->ticket_details_model->child_basic_fare = $child_basic_seat_fare;
            $this->ticket_details_model->convey_name = "Convenience charge";
            $this->ticket_details_model->convey_type = $convey_charge_type;
            $this->ticket_details_model->convey_value = $convey_charge_value;
            $this->ticket_details_model->fare_convey_charge = $convey_charge_value;
            $this->ticket_details_model->seat_no = $value['seat_no'];
            $this->ticket_details_model->berth_no = $value['berth_no'];
            $this->ticket_details_model->service_tax_per = HRTC_SERVICE_CHARGE_PERCENTAGE;
            $this->ticket_details_model->service_tax = ($value['fare']*HRTC_SERVICE_CHARGE_PERCENTAGE/100);
            $this->ticket_details_model->seat_status = 'Y';
            $this->ticket_details_model->concession_proff = 1;  // if cct then required // voter id
            $this->ticket_details_model->proff_detail = 1; // if varified
            $this->ticket_details_model->status = 'Y';
            
            // Add commission which we are getting from operator 
            $op_commission_type = $base_com_response[0]->commission_type;
            $op_commission_per = $base_com_response[0]->percentage_value;
            $op_commission =  round( $current_bus_services["Fare"] * (float)($op_commission_per/100),5);
            $tds_on_commission = round($op_commission * ((float)($this->config->item('tds')/100)),5);
            $final_commission = $op_commission - $tds_on_commission;
            
            $this->ticket_details_model->type = $op_commission_type;
            $this->ticket_details_model->commission_percent = $op_commission_per;
            $this->ticket_details_model->op_commission =  $op_commission;
            $this->ticket_details_model->tds_on_commission =  $tds_on_commission;
            $this->ticket_details_model->final_commission =  $final_commission;
            
            $psgr_no++;

            $ticket_details_id = $this->ticket_details_model->save();

            
          }
        }   
        $soap_name = strtolower($available_detail['op_name']) . '_api';

        $api_data["ticket_id"] = $ticket_id;
        $tb_data = $this->temporary_booking($api_data);
        $this->ticket_metadata_model->insert(["ticket_id" => $ticket_id,"temp_booking_response" => json_encode($tb_data)]);
        $data['ticket_id'] = $ticket_id;
        if(!empty($tb_data)) {
          $temp_booking_status = $tb_data['status'];
          if ($temp_booking_status != 1 && strtolower($tb_data['message']) != "success") {
              $data['block_key'] = "";
              $data['status'] = $tb_data['status'];
              $data['error'] = $tb_data['serviceError']['errorReason'];
              $data['payment_confirm_booking_status'] = "error";
          } 
          else if($temp_booking_status == 1 && strtolower($tb_data['message']) == "success") {
            $tb_data['blockKey']  = $tb_data['TicketID'];
            $tb_data['blockTime'] = 30;
            $total_convey_charge = 0;
            $insert_ttrans_array = array();
            $tickets_total_fare = $tot_fare; //$tickets_total_fare has decalred because in some case we had changed $tot_fare value
           
            $total_fare_without_discount = $tot_fare + $total_extra_charges_value;
            
                
            $data['discount_value'] = $total_ticket_discount;
            $final_total_amount = $total_fare_without_discount-$data['discount_value'];
            $tax_charges = array();
            $data['status'] = $temp_booking_status;

            $update_ticket_array  = [
                                      "ticket_ref_no" => $tb_data['blockKey'],
                                      "total_basic_amount" => $total_basic_fare,
                                      "tot_fare_amt" => $tot_fare,
                                      "tot_fare_amt_with_tax" => $final_total_amount,
                                      "total_fare_without_discount" => $total_fare_without_discount,
                                      "fare_convey_charge" => $total_extra_charges_value,
                                      "service_tax" => $tb_data['ServiceTax'],
                                      "transaction_status" => "temp_booked",
                                    ];
      
            $this->tickets_model->update(["ticket_id" => $ticket_id], $update_ticket_array);
                
            $data['block_key'] = $tb_data['blockKey'];
            $currentTime = strtotime(date("Y-m-d H:i:s"));
            $timeBlock = $currentTime+(60*$tb_data['blockTime']);
            $timeToBlock = date("Y-m-d H:i:s", $timeBlock);

            $data['fare_charges'] = $api_reponse_data;
            $data['block_time_minutes'] = $tb_data['blockTime'];
            $data['block_current_time'] = $currentTime;
            $data['time_to_block'] = $timeToBlock;
            $data['ttb_year'] = date("Y", $timeBlock);
            $data['ttb_month'] = date("m", $timeBlock);
            $data['ttb_day'] = date("d", $timeBlock);
            $data['ttb_hours'] = date("H", $timeBlock);
            $data['ttb_minutes'] = date("i", $timeBlock);
            $data['ttb_seconds'] = date("s", $timeBlock);
            $data['total_fare'] = $final_total_amount;
            $data["total_fare_without_discount"] =  $total_fare_without_discount;
            $data["ticket_fare"] =  $tot_fare;
            $data["total_seats"] = $tot_seat;
            $data["tax_charges"] =  $tax_charges;
            $data['payment_confirm_booking_status'] = "success";
            $data['ticket_id'] = $ticket_id;
          }
        }
        else {
          $data['payment_confirm_booking_status'] = "error";
        }
      }
      else {
        $data['payment_confirm_booking_status'] = "error";
      }
    }
    return $data;
  }

  public function temporary_booking($data = array())
  {
    $response = [];
    $input            = $data["input"];
    $available_detail = $data["available_detail"];

    $date = date('Y-m-d',strtotime($data['date']));
    $pass =  $data['passenger'];
    if(empty($pass) && empty($available_detail))
    {
      return $response;
    }

    $action   = "http://tempuri.org/DoTentativeBooking";
   
    $sub_request = '<TentativeRequest>
                          <Address>INDIA</Address>
                          <BoardingPlace>'.$data["from"].'</BoardingPlace>
                          <CardID>11111111</CardID>
                          <CardNo>11111111</CardNo>
                          <DestinationId>'.$data["to_stop_id"].'</DestinationId>
                          <Email>'.$data["email"].'</Email>
                          <IdentityProof>'.$input["id_type"].'</IdentityProof>
                          <IdentityValue>'.$input["id_card_number"].'</IdentityValue>
                          <JourneyDate>'.$date.'</JourneyDate>
                          <Mobile>'.$data["mobile_no"].'</Mobile>';
                          foreach ($pass as $key => $value)
                          { 
                            $passanger_number = $key+1;
    $sub_request .=         '<pax'.$passanger_number.'>
                                <Age>'.$value['age'].'</Age>
                                <Name>'.$value['name'].'</Name>
                                <SeatNumber>'.$value['seat_no'].'</SeatNumber>
                                <Sex>'.$value['gender'].'</Sex>
                             </pax'.$passanger_number.'>';
                          }
                         
    $sub_request .= '<Phone>'.$input["mobile_no"].'</Phone>
                          <RouteCode>'.$data["service_id"].'</RouteCode>
                          <SessionID>'.uniqid().'</SessionID>
                          <SourceId>'.$data["from_stop_id"].'</SourceId>
                          <UserName>'.$this->user_id.'</UserName>
                        </TentativeRequest>';

    $request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
                 <soapenv:Header/>
                 <soapenv:Body>
                    <tem:DoTentativeBooking>
                       <!--Optional:-->
                       <tem:userName>'.mcrypt_encrypt_e($this->user_id,$this->hrtc_api_secret_key).'</tem:userName>
                       <!--Optional:-->
                       <tem:password>'.mcrypt_encrypt_e($this->pass,$this->hrtc_api_secret_key).'</tem:password>
                       <!--Optional:-->
                       <tem:tentativeReq>'.mcrypt_encrypt_e($sub_request,$this->hrtc_api_secret_key).'</tem:tentativeReq>
                    </tem:DoTentativeBooking>
                 </soapenv:Body>
              </soapenv:Envelope>';


    logging_data("hrtc/".date("Y-m-d")."_DoTentativeBooking.log",$request,"Request");
    $encoded_response = get_soap_data_hrtc($this->url, 'DoTentativeBooking', $request, $action);
    logging_data("hrtc/".date("Y-m-d")."_DoTentativeBooking.log",$encoded_response,"encoded_response");

    if(is_array($encoded_response))
    { 
      $response_string = mcrypt_decrypt_e($encoded_response["DoTentativeBookingResult"],$this->hrtc_api_secret_key);
       // logging_data("hrtc/".date("Y-m-d")."_DoTentativeBooking.log",$response_string,"response_string");
      $response_array = xml_to_array($response_string);

      logging_data("hrtc/".date("Y-m-d")."_DoTentativeBooking.log",$response_array,"response_array");
      
      $data_to_log = [
                    "ticket_id" => isset($data["ticket_id"]) ? $data["ticket_id"] : "",
                    "request" => $request,
                    "response" => json_encode($response_array),
                    "type" => "temp_booking"
                   ];

      insert_request_response_log($data_to_log);

      if(isset($response_array["Table"]))
      {
        $response = $response_array["Table"];
      }
    }
    return $response;
  }
  

  function final_booking($api_data, $data)
  {

    if(ENVIRONMENT != 'production')
    {
      $final_response["status"] = "error";
      $final_response["environment"] = "You are not authorized to book this ticket.";
      $final_response["data"] = "";
      return $final_response;
    }

    $final_response = array();
    $tbdata = $this->doconfirm_booking($api_data);
    /************* Actual ticket booking End Here ***********/

    if(trim($tbdata['status']) == 'SUCCESS') 
    {   
        $onwards_ticket_ref_no = $data["ticket_ref_no"];

        $data["ticket_pnr"] = $tbdata["pnr"];

        
        $transaction = array('transaction_status' => "success",
                              'operator_ticket_no' => $tbdata["pnr"],
                              'payment_by' => $api_data["payment_by"],
                              'payment_mode' => $data["pg_response"]["payment_mode"],
                            );

        $this->tickets_model->update($data["ticket_id"],$transaction);
        //common model
        $pg_success_response = $this->transaction_process($data);
        $final_response["transaction_process_response"] = $pg_success_response;

        //make this function common afterwards. Because this is used 2 times (1 times in pg and 1 time in wallet)
        if(trim($pg_success_response["msg_type"]) == "success")
        {
          //$data["pg_response"]["udf2"] -> ticket_id 
          
          //To check if return journey
          // $rpt_data = $this->return_ticket_mapping_model->get_rpt_mapping($data["pg_response"]["udf2"]);
          $rpt_data = $api_data["rpt_data"];
          if(count($rpt_data))
          {
              $onward_ticket_id = $pg_success_response["ticket_id"];

              $rtransaction = array('is_return_journey' => "1",
                                    'return_ticket_id' => $rpt_data[0]->ticket_id
                                    );

              $this->tickets_model->update($onward_ticket_id,$rtransaction);

              /************* Actual ticket booking Start Here ***********/
              $rapi_data['ticket_id']      = str_pad($rpt_data[0]->ticket_id, 8, '0', STR_PAD_LEFT);
              $rapi_data['ticket_ref_no']  = $rpt_data[0]->ticket_ref_no;
              $rapi_data['seat_count']     = $rpt_data[0]->num_passgr;

              $soap_name = strtolower($rpt_data[0]->op_name) . '_api';
              $rtbdata = $this->doconfirm_booking($rapi_data);
              /************* Actual ticket booking End Here ***********/

              if($rtbdata['status'] == 'SUCCESS') 
              {
                  $data["ticket_ref_no"] = $rpt_data[0]->ticket_ref_no;
                  $data["ticket_pnr"] = $rtbdata["pnr"];
                  $data["ticket_id"] = $rpt_data[0]->ticket_id;
                  
                  $atransaction = array('transaction_status' => "success",
                                         'payment_by' => $api_data["payment_by"],
                                         'operator_ticket_no' => $rtbdata["pnr"],
                                         'payment_mode' => $data["pg_response"]["payment_mode"],
                                        );

                  $this->tickets_model->update($rpt_data[0]->ticket_id,$atransaction);

                  $final_response["return_ticket_status"] = "success";

                  $rpg_success_response = $this->transaction_process($data);
                  $final_response["transaction_process_response"] = $rpg_success_response;
                  
                  if(trim($rpg_success_response["msg_type"]) == "success")
                  {
                    if(isset($data['is_back']) && $data['is_back'])
                    {
                      $url_redirect = base_url()."admin/reservation/view_ticket/".$onwards_ticket_ref_no."/".$rpt_data[0]->ticket_ref_no;  
                    }
                    else
                    {
                      $url_redirect = base_url()."front/booking/view_ticket/".$onwards_ticket_ref_no."/".$rpt_data[0]->ticket_ref_no;
                    }

                    $final_response["status"] = "success";
                    $final_response["redirect_url"] = $url_redirect;
                  }
                  else if(trim($rpg_success_response["msg_type"]) == "error")
                  {
                                         
                    $transaction = array('payment_by' => $api_data["payment_by"],
                                         'failed_reason' => (isset($rpg_success_response["msg"])) ? $rpg_success_response["msg"] : "some error occured in msrtc library"
                                        );

                    $id = $this->tickets_model->update($rpt_data[0]->ticket_id,$transaction);

                    $final_response["status"] = "error";
                    $final_response["return_transaction_process_api"] = "error";
                    $final_response["data"] = $data;
                  }
              }
              else
              {
                if($rpt_data[0]->transaction_status != "success")
                {
                  $transaction = array('transaction_status' => "failed",
                                       'failed_reason' => "hrtc return ticket api failed",
                                       'payment_by' => $api_data["payment_by"]
                                      );

                  $return_ticket_update_where = ["ticket_id" => $rpt_data[0]->ticket_id,"transaction_status != " => "success"];
                  $id = $this->tickets_model->update($return_ticket_update_where,$transaction);
                }
                else
                {
                  $this->common_model->developer_confirm_booking_error_mail("hrtc (Return Journey)",$rpt_data[0]->ticket_id);
                }

                $final_response["return_ticket_status"] = "error";
                $final_response["return_ticket_api_failed_reason"] = "api failed";

                $final_response["status"] = "success";
                $final_response["redirect_url"] = $pg_success_response["redirect_url"];
              }
          }
          else
          {
              $final_response["status"] = "success";
              $final_response["redirect_url"] = $pg_success_response["redirect_url"];
          }
        }
        else if(trim($pg_success_response["msg_type"]) == "error")
        {
          $transaction = array('failed_reason' => 'failed message - '.(isset($pg_success_response["msg"])) ? $pg_success_response["msg"] : "",
                             'payment_by' => $api_data["payment_by"]
                            );

          $id = $this->tickets_model->update($api_data['ticket_id'],$transaction);
          $final_response["status"] = "success";
          $final_response["redirect_url"] = $pg_success_response["redirect_url"];

          log_data("condition_not_handled/".date("M")."/hrtc_".date("d-m-Y")."_log.log",
                     "Ticket booked successfully but some error occured in transaction_process_api for ticket is ".$data["ticket_id"],
                      'Transaction Process Status');
        }
    }
    else
    {
        if($api_data['pt_data']->transaction_status != "success")
        {
          $transaction = array('transaction_status' => "failed",
                               'failed_reason' => 'hrtc doconfirm_booking api failed',
                               'payment_by' => $api_data["payment_by"]
                              );

          $update_where = ["ticket_id" => $api_data['pt_data']->ticket_id, "transaction_status != " => "success"];

          $id = $this->tickets_model->update($update_where,$transaction);

          $find_return_ticket = array();
          $find_return_ticket = $this->return_ticket_mapping_model->where("ticket_id",$api_data['ticket_id'])->find_all();
          
          if(!empty($find_return_ticket) && $find_return_ticket[0]->return_ticket_id > 0)
          {
            $return_update_where = ["ticket_id" => $find_return_ticket[0]->return_ticket_id, "transaction_status != " => "success"];
            $rid = $this->tickets_model->update($return_update_where,$transaction);
          }
        }
        else
        {
          $this->common_model->developer_confirm_booking_error_mail("hrtc",$api_data['pt_data']->ticket_id);
        }
        
        $final_response["status"] = "error";
        $final_response["api_failed_reason"] = "api failed";
        $final_response["data"] = $data;
    }
    
    return $final_response;
  }


  public function doconfirm_booking($block_key= array())
  {
    $response = [];

    if(empty($block_key) || !isset($block_key["ticket_ref_no"]) && $block_key["ticket_ref_no"] == "")
    {
      return $response;
    }

    $action = "http://tempuri.org/ConfirmBooking";
    $request =  
              '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
                 <soapenv:Header/>
                 <soapenv:Body>
                    <tem:ConfirmBooking>
                       <!--Optional:-->
                       <tem:userName>'.mcrypt_encrypt_e($this->user_id,$this->hrtc_api_secret_key).'</tem:userName>
                       <!--Optional:-->
                       <tem:password>'.mcrypt_encrypt_e($this->pass,$this->hrtc_api_secret_key).'</tem:password>
                       <!--Optional:-->
                       <tem:TicketId>'.mcrypt_encrypt_e($block_key["ticket_ref_no"],$this->hrtc_api_secret_key).'</tem:TicketId>
                    </tem:ConfirmBooking>
                 </soapenv:Body>
              </soapenv:Envelope>';
    
    logging_data("hrtc/".date("Y-m-d")."_ConfirmBooking.log",$request,"Request");
    $encoded_response = get_soap_data_hrtc($this->url, 'ConfirmBooking', $request, $action);
    logging_data("hrtc/".date("Y-m-d")."_ConfirmBooking.log",$encoded_response,"encoded_response");
    if(is_array($encoded_response))
    {
      $response_string = mcrypt_decrypt_e($encoded_response["ConfirmBookingResult"],$this->hrtc_api_secret_key);
      $response_array = xml_to_array($response_string);
      logging_data("hrtc/".date("Y-m-d")."_ConfirmBooking.log",$response_array,"response_array");

      $data_to_log = [
                    "ticket_id" => isset($block_key["ticket_id"]) ? $block_key["ticket_id"] : "",
                    "request" => $request,
                    "response" => json_encode($response_array),
                    "type" => "confirm_booking"
                   ];

      insert_request_response_log($data_to_log);
      if(isset($response_array["Table"]))
      {
        if(isset($response_array["Table"]["Status"]) && $response_array["Table"]["Status"] == 1 && isset($response_array["Table"]["Message"]) && strtolower($response_array["Table"]["Message"]) == "success")
        {
          $response["pnr"] = $block_key["ticket_ref_no"];
          $response["status"] = "SUCCESS";
        }
      }
      
    }
    return $response;
  }


  public function transaction_process($response)
  {
      $pg_success_response = array();
      $pg_result = isset($response['pg_response']['result']) ? $response['pg_response']['result'] : "";
      $wallet_result = (isset($response['is_wallet_success']) && $response['is_wallet_success']) ? true : false ;
      
      if($wallet_result || (isset($response["is_updated"]) && $response["is_updated"] && (($pg_result == "CAPTURED") || ($pg_result =="APPROVED"))))
      {
          $blockKey = $response["ticket_ref_no"];
          $ticket_id = $response["ticket_id"];

          //After Remove Below Query and pass data
          $pt_data = $this->tickets_model->where("ticket_ref_no",$blockKey)->find_all();
          
          // $pt_data = $response["ticket_data"];

          if($pt_data)
          {
              $dept_date = date("D, d M Y, h:i A",strtotime($pt_data[0]->dept_time));
              $dept_time = date("H:i:s",strtotime($pt_data[0]->dept_time));

              $data['ticket_data'] = $pt_data[0];

              //Ticket Detail Data
              // $this->ticket_details_model->ticket_id = $ticket_id;
              // $ptd_data = $this->ticket_details_model->select();
              $ptd_data = $this->ticket_details_model->where("ticket_id",$ticket_id)->find_all();
              
              //$payment_total_fare = $pt_data[0]->num_passgr * $pt_data[0]->tot_fare_amt;
              $payment_total_fare = $pt_data[0]->tot_fare_amt_with_tax;
              $ticket_total_fare_without_discount = $pt_data[0]->total_fare_without_discount;

              $data['passenger_ticket'] = $pt_data[0];
              $data['passenger_ticket_details'] = $ptd_data;
              $data['pnr'] = $response['ticket_pnr'];


              /****** To Update PNR, BOS REFERENCE NUMBER and SMS Count Start ***********/
              $bos_ref = getBossRefNo($ticket_id);
              // $this->tickets_model->boss_ref_no = $bos_ref["bos_ref_no"];
              // $bos_ref_exists = $this->tickets_model->select();
              $bos_ref_exists = $this->tickets_model->where("boss_ref_no",$bos_ref["bos_ref_no"])->find_all();
              
              if(!$bos_ref_exists)
              {
                  $bos_ticket_digit = $bos_ref["bos_ticket_digit"];
                  $bos_ticket_string = $bos_ref["bos_ticket_string"];
                  $bos_ref_no = $bos_ref["bos_ref_no"];
              }

              /*********************************************************************/
              /*
              *This is for testing Purpose only.
              *After discussion change this.
              */
              //THIS SEAT NO QUERY CAN BE REDUCE LATER
              $seat_no = $this->tickets_model->get_seat_nos_by_tid($ticket_id);
              if(count($seat_no) > 1)
              {
                  $seat_with_berth = "Berth 1: ".$seat_no[0]->seat_no." Berth 2: ".$seat_no[1]->seat_no;
                  $seat_with_berth_ticket = "Berth 1: ".$seat_no[0]->seat_no." <br/> Berth 2: ".$seat_no[1]->seat_no;
              }
              else
              {
                  $seat_with_berth = $seat_no[0]->seat_no;
                  $seat_with_berth_ticket = $seat_no[0]->seat_no;
              }

              $ticket_display_fare = $payment_total_fare;
              if($pt_data[0]->booked_by == "ors_agent")
              {
                $orsagent_ticket_data = $this->agent_tickets_model->where(array("ticket_id" => $ticket_id))->find_all();
                if($orsagent_ticket_data)
                {
                  $ticket_display_fare = $orsagent_ticket_data[0]->agent_total;
                }
              }
              

              /*$ticket_sms_detail = array(
                                  "{{pnr_no}}" => $response["ticket_pnr"],
                                  "{{bos_ref_no}}" => $bos_ref_no,
                                  "{{amount_to_pay}}" => $ticket_display_fare,
                                  "{{from_stop}}" => $pt_data[0]->from_stop_name,
                                  "{{to_stop}}" => $pt_data[0]->till_stop_name,
                                  "{{seat_no}}" => $seat_with_berth,
                                  "{{doj}}" => $dept_date
                                );

              log_data("sms/sms_".date("d-m-Y")."_log.log",$ticket_sms_detail, 'ticket_sms_detail array');
              $message_array[] = $pt_data[0]->mobile_no;
              if($pt_data[0]->agent_mobile_no != "")
              {
                  $message_array[] = $pt_data[0]->agent_mobile_no;
              }
              
              $temp_sms_msg = getSMS("ticket_booking");
              $msg_to_sent = str_replace(array_keys($ticket_sms_detail),array_values($ticket_sms_detail),$temp_sms_msg[0]->sms_template_content);

              $is_send = sendSMS($pt_data[0]->mobile_no, $msg_to_sent, array('ticket_id' => $ticket_id));*/
              $smsData = [];
              $smsData["data"] = $data;
              $smsData["data"]["passenger_ticket"]->pnr_no = $response["ticket_pnr"];
              $smsData["seat_with_berth"] = $seat_with_berth;
              //if ticket booked by ors_agent two sms send one for agent and second for pessanger //
              $mobile_array[] = $pt_data[0]->mobile_no;
                if($pt_data[0]->agent_mobile_no != "")
                {
                    $mobile_array[] = $pt_data[0]->agent_mobile_no;
                }
                $is_send = sendTicketBookedSms($smsData, $mobile_array);
              $eticket_count = ($is_send) ? 1 : 0;


              $fields_update = array("transaction_status" => "success", 
                                      "pnr_no" => $response["ticket_pnr"],
                                      "status" => "Y",
                                      "bos_ticket_digit" => $bos_ticket_digit,
                                      "bos_ticket_string" => $bos_ticket_string,
                                      "boss_ref_no" => $bos_ref_no,
                                      "eticket_count" => $eticket_count // IMP -> Placed 1 Hardcoded because. Here is the first time we are sending message.
                                      );

              $this->tickets_model->update($ticket_id,$fields_update);
              
              /*
              * The Above SMS Code is temp and For testing purposeonly
              */

                
              //to check if user exists
              $email = $pt_data[0]->user_email_id;
              $data['email']=$email;
              $this->users_model->email = $email;
              $data['user_exists'] = $this->users_model->select();

              /*********************************************************************/

              if(count($ptd_data) > 0)
              {
                  $number_of_adults = 0;
                  $number_of_childs = 0;
                  $psgr_no = 1;
                  $passanger_detail = "";
                  foreach ($ptd_data as $key => $value)
                  {
                    if($value->psgr_age <= 12)
                    {
                      $number_of_childs++;
                    }
                    else
                    {
                      $number_of_adults++;
                    }

                    $passenger_gender = "Male";
                    if(strtolower($value->psgr_sex) == "f")
                    {
                      $passenger_gender = "Female";
                    }
                    /*$passanger_detail .= '<tr>
                                            <td align="center">'.$value->seat_no.'</td>
                                            <td align="center">'.ucwords($value->psgr_name).'</td>
                                            <td align="center">'.$value->psgr_age.'</td>
                                            <td align="center">'.(strtolower($value->psgr_sex) == "f") ? "Female" : "Male".'</td>
                                          </tr>';*/
                      $passanger_detail .= "<tr>";
                      $passanger_detail .= "<td align='center'>".$value->seat_no."</td>";
                      $passanger_detail .= "<td align='center'>".ucwords($value->psgr_name)."</td>";
                      $passanger_detail .= "<td align='center'>".$value->psgr_age."</td>";
                      $passanger_detail .= "<td align='center'>".$passenger_gender."</td>";
                      $passanger_detail .= "</tr>";
                      $psgr_no++; 
                  }

                  if($pt_data[0]->boarding_time != "" && $pt_data[0]->boarding_time != "0000-00-00 00:00:00")
                  {
                    $bus_boarding_time = date("h:i A",strtotime($pt_data[0]->boarding_time));
                  }
                  else
                  {
                    $bus_boarding_time = "NA";
                  }

                  $mail_replacement_array = array('{{bus_service_no}}' => $pt_data[0]->bus_service_no,
                                                  '{{from}}' => $pt_data[0]->from_stop_name,
                                                  '{{to}}' => $pt_data[0]->till_stop_name,
                                                  '{{journy_time}}' => $bus_boarding_time,
                                                  '{{ticket_number}}' => $response["ticket_pnr"],
                                                  '{{bos_ref_no}}' => $bos_ref_no,
                                                  '{{email_id}}' => $pt_data[0]->user_email_id,
                                                  '{{journy_date}}' => date("d/m/Y",strtotime($pt_data[0]->boarding_time)),
                                                  '{{bus_type}}' => $pt_data[0]->bus_type,
                                                  '{{sub_total_fare}}' => $pt_data[0]->total_fare_without_discount - $pt_data[0]->service_tax,
                                                  '{{service_charge}}' => $pt_data[0]->service_tax,
                                                  '{{number_of_adults}}' => $number_of_adults,
                                                  '{{number_of_childs}}' => $number_of_childs,
                                                  '{{total_fare}}' => $pt_data[0]->total_fare_without_discount,
                                                  '{{passanger_details}}' => $passanger_detail,
                                                 );
                  
                       log_data("mail_replacement_array/1.log",$mail_replacement_array,"mail_replacement_array");
                      /************ Mail Start Here ********/
                      $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array), HRTC_TICKET_TEMPLATE);

                      $mailbody_array = array("subject" => "Rokad Ticket",
                                              "message" => $data['mail_content'],
                                              "to" =>  $email 
                                              );
                       
                       $mail_result = send_mail($mailbody_array);
                       //if agent booked tkt send mail to both agent as well as customer//
                     $agent_email_id = $pt_data[0]->agent_email_id;
                    if($agent_email_id != "" && ($agent_email_id != $email))
                    {
                        $agent_mailbody_array = array("subject" => "Rokad Ticket",
                                              "message" => $data['mail_content'],
                                              "to" =>  $agent_email_id
                                              );
                         $agent_mail_result = send_mail($agent_mailbody_array);
                    }
                      /************ Mail End Here ********/

                      /************ Dump ticket Start Here ********/
                      $this->ticket_dump_model->bos_ref_no = $bos_ref["bos_ref_no"];
                      $this->ticket_dump_model->ticket_id = $ticket_id;
                      $this->ticket_dump_model->ticket_ref_no = $blockKey;
                      $this->ticket_dump_model->pnr_no = $data['pnr'];
                      $this->ticket_dump_model->email = $email;
                      $this->ticket_dump_model->mobile = $pt_data[0]->mobile_no;
                      $this->ticket_dump_model->journey_date = $pt_data[0]->dept_time;
                      $this->ticket_dump_model->ticket = $data['mail_content'];
                      if($this->session->userdata('booking_user_id') != "")
                      {
                        $this->ticket_dump_model->created_by = $this->session->userdata("booking_user_id");
                      }

                      $ticket_dump_id = $this->ticket_dump_model->save();

                      /************ Dump ticket End Here ********/

                      //insert into user action log
                      $users_action_log = array("user_id" => $this->session->userdata("booking_user_id"),
                                                "name" => $this->session->userdata("display_name"),
                                                "url" => "bos_ref_no = ".$bos_ref["bos_ref_no"]." , ticket_ref_no = ".$blockKey." , ticket_id = ".$ticket_id." , pnr_no = ".$data['pnr'],
                                                "action" => "bus ticket booked",
                                                "message" => "Booked bus from <span class='navy_blue location'>".ucfirst($pt_data[0]->from_stop_name)."</span> to <span class='navy_blue location'>".ucfirst($pt_data[0]->till_stop_name)."</span> for the day <span class='navy_blue date'>".date("jS F Y", strtotime($pt_data[0]->dept_time))."</span>"
                                                );
                      
                      users_action_log($users_action_log);

                      $pg_success_response["msg_type"] = "success";
                      $pg_success_response["bos_ref_no"] = $bos_ref["bos_ref_no"];
                      $pg_success_response["ticket_id"] = $ticket_id;
                      if(isset($response['is_back']) && $response['is_back']==1){
                          $pg_success_response["redirect_url"] = base_url()."admin/reservation/view_ticket/".$blockKey;
                      }else{
                          $pg_success_response["redirect_url"] = base_url()."front/booking/view_ticket/".$blockKey;
                      }
              }// end of if $ptd_data condition
              else
              {
                  $custom_error_mail = array(
                                        "custom_error_subject" => "ticket data not available",
                                        "custom_error_message" => "ticket detail data not available .function name transaction_process_api"
                                      );

                  $this->common_model->developer_custom_error_mail($custom_error_mail);

                  $pg_success_response["msg_type"] = "error";
                  $pg_success_response["msg"] = "ticket detail data not available.";
                  $pg_success_response["redirect_url"] = base_url();
              }
       
          }// end of if $pt_data condition
          else
          {
              $custom_error_mail = array(
                                        "custom_error_subject" => "ticket data not available for ticket id - ".$ticket_id,
                                        "custom_error_message" => "Ticket Id : ".$ticket_id.". Function name transaction_process_api."
                                      );

              $this->common_model->developer_custom_error_mail($custom_error_mail);

              $pg_success_response["msg_type"] = "error";
              $pg_success_response["msg"] = "ticket data not available.";
              $pg_success_response["redirect_url"] = base_url();
          }
      }// end if payment status $response["is_updated"] && $pg_result
      else
      {
            $custom_error_mail = array(
                                        "custom_error_subject" => "payment gateway failed/error for ticket id - ".$response["ticket_id"],
                                        "custom_error_message" => "payment gateway failed/error or transaction table not updated for ticket id - ".$response["ticket_id"]."."
                                      );

            $this->common_model->developer_custom_error_mail($custom_error_mail);

          $pg_success_response["msg_type"] = "error";
          $pg_success_response["msg"] = "payment gateway failed/error or transaction table not updated.";
          $pg_success_response["redirect_url"] = base_url();
      }

      return $pg_success_response;
  } // End of transaction_process

  public function cancelSeats($input = array())
  {
      $response = [];

      $action = "http://tempuri.org/CancelSeats";
      $request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
                  <soapenv:Header/>
                    <soapenv:Body>
                      <tem:CancelSeats>
                        <!--Optional:-->
                        <tem:userName>'.mcrypt_encrypt_e($this->user_id,$this->hrtc_api_secret_key).'</tem:userName>
                        <!--Optional:-->
                        <tem:password>'.mcrypt_encrypt_e($this->pass,$this->hrtc_api_secret_key).'</tem:password>
                        <!--Optional:-->
                        <tem:TicketId>'.mcrypt_encrypt_e($input["TicketId"],$this->hrtc_api_secret_key).'</tem:TicketId>
                        <!--Optional:-->
                        <tem:DoCancel>'.mcrypt_encrypt_e(1,$this->hrtc_api_secret_key).'</tem:DoCancel>
                      </tem:CancelSeats>
                    </soapenv:Body>
                  </soapenv:Envelope>';

    logging_data("hrtc/".date("Y-m-d")."_cancelSeats.log",$request,"Request");

    $encoded_response = get_soap_data_hrtc($this->url, 'cancelSeats', $request, $action);

    logging_data("hrtc/".date("Y-m-d")."_cancelSeats.log",$encoded_response,"encoded_response");

    $response_string = mcrypt_decrypt_e($encoded_response["CancelSeatsResult"],$this->hrtc_api_secret_key);
    
    $response_array = xml_to_array($response_string);
    $data_to_log = [
                    "ticket_id" => isset($input["ticket_id"]) ? $input["ticket_id"] : "",
                    "request" => $request,
                    "type" => "ticket_cancel",
                    "response" => json_encode($response_array),
                   ];

    insert_request_response_log($data_to_log);

    logging_data("hrtc/".date("Y-m-d")."_cancelSeats.log",$response_array,"response_array");

    if(isset($response_array["Table"]))
    {
      $response_array["Table"]["status"] = $response_array["Table"]["Status"];
      $response_array["Table"]["message"] = strtolower($response_array["Table"]["Message"]);
      $response = $response_array["Table"];
    }

    return $response;
  }

  public function cancel_ticket($input = array())
  {
    $data   = [];
    if(!empty($input) && isset($input["pnr_no"]) && $input["pnr_no"] != "")
    {
      $current_ticket_information = $input["current_ticket_information"];
      $response = $this->cancelSeats(["TicketId" => $input["pnr_no"],"ticket_id" => $input['current_ticket_information']['ticket_id']]);

      if($response["status"] == 1 && strtolower($response['message']) == "success")
      {
        $charges_on_fare                = $current_ticket_information["tot_fare_amt_with_tax"] - $current_ticket_information["service_tax"];
        $data["status"]                 = "success";
        $data["refundAmount"]           = $response["RefundAmount"];
        $data["cancellationCharge"]     = (ceil(($charges_on_fare * $response["ChargePct"])/100) + $current_ticket_information["service_tax"]);
        $data["cancellationChargePer"]  = $response["ChargePct"];
        $data["raw_data"]               = $response;
      }
      else
      {
        $data["status"] = "error";
        $data["error_message"] = "Please contact customer support.";
      }
    }
    else
    {
      $data["status"] = "error";
      $data["error_message"] = "Insufficient data. Please refresh your page and try again.";
    }
    logging_data("hrtc/".date("Y-m-d")."_cancelSeats.log",$data,"cancel_ticket data");
    return $data;
  }
  
    function apiCancelTicket($ticket, $post)
  {
        $this->load->model(array(
                'tickets_model',
                'cancel_ticket_model',
            )
        );

        if(!isset($ticket->pnr_no) && $ticket->pnr_no == '')
        {
          return array( 'status' => 'failed', 'msg' => 'Mismatch data found, Contact the provider');   
        }
        $api_response = $this->cancelSeats(["TicketId" => $ticket->pnr_no]);

        if(strtolower($api_response["message"]) != 'success')
        {
          return array('status' => 'failed', 'msg' => 'Cancel Api failed', 'api_msg' => $api_response);
        }

        $this->tickets_model->update($ticket->ticket_id, array('status' => 'N','transaction_status' => 'cancel'));

        $this->ticket_details_model->update(array("ticket_id" => $ticket->ticket_id), array('status' => 'N','seat_status' => 'N'));

        $charges_on_fare = $ticket->tot_fare_amt_with_tax - $ticket->service_tax;
        $cancel_charge =  (ceil(($charges_on_fare * $api_response["ChargePct"])/100) + $ticket->service_tax);
        $cancel_ticket_arr = array(
            "ticket_id"        => $ticket->ticket_id,
            "pnr_no"        => $ticket->pnr_no,
            "new_pnr"        => '',
            "refund_amt"    => $api_response['RefundAmount'],
            "cancel_charge" => $cancel_charge, 
            "raw" =>json_encode($api_response)
         );

        $c_id = $this->cancel_ticket_model->insert($cancel_ticket_arr);

        if(!$c_id)
        {
          return array('status' => 'failed', 'msg' => 'cancel data not save', 'api_msg' => $api_response);
        }

        $paymentgateway_ref_no = "";
        $paymentgateway_ref_no = $ticket->pg_tracking_id;

        $acual_refund_paid = getRefundAmountToPay($ticket->tot_fare_amt_with_tax,$cancel_charge);

        $merchant_array_data  = array(
                                        "reference_no" => $paymentgateway_ref_no,
                                        "refund_amount" => $acual_refund_paid,
                                        "refund_ref_no" => uniqid()
                                      );

        $refund_status = $this->cca_payment_gateway->refundOrder($merchant_array_data);

        if($refund_status->refund_status)
        {
          return array('status' => 'failed', 'msg' => 'Error in processing request');
        }

        $update_refund_status = array('is_refund' => "1",
                                      "actual_refund_paid" => $acual_refund_paid,
                                      "pg_refund_amount" => $acual_refund_paid
                                  );

        $update_where_cancel = array("id" => $c_id);
        $refund_id = $this->cancel_ticket_model->update($update_where_cancel,$update_refund_status);

        if(!$refund_id)
        {
            log_data("refund/success_but_not_updated_ccavenue_refund_".date("d-m-Y")."_log.log","Refund is success but status is not updated for 'cancel_ticket_data table' for id ".$c_id,'cancel_ticket_data table id');
        }
        $response = array(                    
            'pnr'           => $ticket->pnr_no,
            'new_pnr'       => '',
            'refund_amt'    => $api_response['RefundAmount'],
            'cancel_charge' => $cancel_charge 
         );

        return array(
            'status' => 'success', 
            'msg' => 'Ticket Cancelled successfully.',
            'data' => $response
        );
  }
  /* 
  * @author    : Suraj
  * @function  : is_ticket_cancellable
  * @param     : 
  * @return    : 
  * @method    : HTTP POST
  * @Details   : This API is used check if ticket is cancelable
  */
  public function is_ticket_cancellable($fields)
  {
    $data   = array();

    $charges_in_percentage = "";
    $current_ticket_information = $fields["current_ticket_information"];

    if(!empty($current_ticket_information))
    {
      if(time() < strtotime($current_ticket_information["dept_time"]))
      {
        $current_time = time();
        $departure_time = strtotime($current_ticket_information["dept_time"]);
        $diff = $departure_time - $current_time;
        $hours = $diff / ( 60 * 60 );

        if($hours > 0)
        {
          $total_refund_amount = 0;
          $total_cancellation_charges = 0;

          if($hours >= 12)
          {
            $charges_in_percentage = 10;
          }
          else if($hours >= 4 && $hours < 12)
          {
            $charges_in_percentage = 25;
          }
          else
          {
            $charges_in_percentage = 100;
          }

          if($charges_in_percentage != "")
          {
            $charges_on_fare                = $current_ticket_information["tot_fare_amt_with_tax"] - $current_ticket_information["service_tax"];
            $total_cancellation_charges     = ceil(($charges_on_fare * $charges_in_percentage)/100) + $current_ticket_information["service_tax"];
            $total_refund_amount            = $charges_on_fare - $total_cancellation_charges ;
            $data["status"]                 = "success";
            $data["refundAmount"]           = $total_refund_amount;
            $data["cancellationCharge"]     = $total_cancellation_charges;
          }
          else
          {
            $data["status"] = "error";
            $data["error_message"] = "Please contact customer support.";
          }
        }
        else
        {
          $data["status"] = "error";
          $data["error_message"] = "Ticket is non-cancellable. Please contact customer support.";
        }
      }
      else
      {
        $data["status"] = "error";
        $data["error_message"] = "Ticket is non-cancellable. No refund will be made in case of e-tickets after departure of bus ( from the orginating station of the route ).";
      }
    }
    else
    {
      $data["status"] = "error";
      $data["error_message"] = "This ticket cannot be cancelled. Please contact customer support.";
    }
    
    return $data;
  }

  public function getAllPickDropPointForRouteId($route_id = "")
  {
    $response = [];

    if($route_id == "")
    {
      return $response;
    }

    $action = "http://tempuri.org/GetAllPickDropPointForRouteId";
    $request =  
              '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <tem:GetAllPickDropPointForRouteId>
                         <!--Optional:-->
                         <tem:routeId>'.mcrypt_encrypt_e($route_id,$this->hrtc_api_secret_key).'</tem:routeId>
                      </tem:GetAllPickDropPointForRouteId>
                   </soapenv:Body>
                </soapenv:Envelope>';
    
    logging_data("hrtc/".date("Y-m-d")."_getAllPickDropPointForRouteId.log",$request,"Request");
    $encoded_response = get_soap_data_hrtc($this->url, 'GetAllPickDropPointForRouteId', $request, $action);
    logging_data("hrtc/".date("Y-m-d")."_getAllPickDropPointForRouteId.log",$encoded_response,"encoded_response");
    if(is_array($encoded_response))
    {
      $response_string = mcrypt_decrypt_e($encoded_response["GetAllPickDropPointForRouteIdResult"],$this->hrtc_api_secret_key);
      $response_array = xml_to_array($response_string);
      logging_data("hrtc/".date("Y-m-d")."_getAllPickDropPointForRouteId.log",$response_array,"response_array");
      if(isset($response_array["Table"]))
      {
        $response = $response_array["Table"];
      }
    }
    return $response;
  }
  
  // to send the cancel ticket start here//
   public function mail_cancel_ticket($tinfo,$data,$ptd_data)
    { 
      foreach ($ptd_data as $key => $value)
        {
          $number_of_adults = 0;
          $number_of_childs = 0;
          $psgr_no = 1;
          $passanger_detail = "";
          foreach ($ptd_data as $key => $value)
          {
            if($value->psgr_age <= 12)
            {
              $number_of_childs++;
            }
            else
            {
              $number_of_adults++;
            }

          $passenger_gender = "Male";
          if(strtolower($value->psgr_sex) == "f")
          {
            $passenger_gender = "Female";
          }
            $passanger_detail .= "<tr>";
            $passanger_detail .= "<td align='center'>".$value->seat_no."</td>";
            $passanger_detail .= "<td align='center'>".ucwords($value->psgr_name)."</td>";
            $passanger_detail .= "<td align='center'>".$value->psgr_age."</td>";
            $passanger_detail .= "<td align='center'>".$passenger_gender."</td>";
            $passanger_detail .= "</tr>";
            $psgr_no++; 
        }

        if($tinfo['boarding_time'] != "" && $tinfo['boarding_time'] != "0000-00-00 00:00:00")
        {
          $bus_boarding_time = date("h:i A",strtotime($tinfo['boarding_time']));
        }
        else
        {
          $bus_boarding_time = "NA";
        }

        $mail_replacement_array = array('{{bus_service_no}}' => $tinfo['bus_service_no'],
                                        '{{from}}' => $tinfo['from_stop_name'],
                                        '{{to}}' => $tinfo['till_stop_name'],
                                        '{{journy_time}}' => $bus_boarding_time,
                                        '{{ticket_number}}' => $tinfo['pnr_no'],
                                        '{{bos_ref_no}}' => $tinfo['boss_ref_no'],
                                        '{{email_id}}' => $tinfo['user_email_id'],
                                        '{{journy_date}}' => date("d/m/Y",strtotime($tinfo['boarding_time'])),
                                        '{{bus_type}}' => $tinfo['user_email_id'],
                                        '{{sub_total_fare}}' => $tinfo['total_fare_without_discount'] - $tinfo['service_tax'],
                                        '{{service_charge}}' => $tinfo['service_tax'],
                                        '{{number_of_adults}}' => $number_of_adults,
                                        '{{number_of_childs}}' => $number_of_childs,
                                        '{{total_fare}}' => $tinfo['total_fare_without_discount'],
                                        '{{refund_amount}}' => $data['refund_amt'],
                                        '{{cancellation_cahrge}}' => $data['cancellation_cahrge'],
                                        '{{passanger_details}}' => $passanger_detail,
                                       );
         /************ Mail Start Here ********/
              $email = $tinfo['user_email_id'];
              $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),HRTC_CANCEL_TICKET_TEMPLATE);

               $mailbody_array = array("subject" => "Book On Spot Cancel Ticket",
                                      "message" => $data['mail_content'],
                                      "to" =>  $email
                                      );
               
               $mail_result = send_mail($mailbody_array);

             //****if agent booked tkt send mail to both agent as well as customer****//
                    $email_agent = $tinfo['agent_email_id'];
                      if($email_agent != "" && ($email_agent != $email))
                      {
                          $agent_mailbody_array = array("subject" => "Book On Spot Cancel Ticket",
                                                "message" => $data['mail_content'],
                                                "to" =>  $email_agent
                                                );
                           $agent_mail_result = send_mail($agent_mailbody_array);
                      }
              //****************************** Mail End Here ******************//
      }
            /************************ Update in Ticket_dump table start here *****************************/
              $update_in_ticket_dump = $this->ticket_dump_model->update_where(array('ticket_id' => $tinfo['ticket_id']),'',array('cancel_ticket' => $data['mail_content']));

            /************************ Update in Ticket_dump table end here *****************************/

  }// to send the cancel ticket end here
 

}