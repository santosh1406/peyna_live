<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Its_api extends CI_Model {

    var $user_id;
    var $pass;
    var $url;
    var $user_type;
    var $op_id;
    var $dep_date;
    var $ci;
    private $cancellationPolicy = array();

    public function __construct() {
        parent::__construct();

        ini_set('max_execution_time', 10000000);
        ini_set('memory_limit', '-1');

        $this->load->helper('soap_data');
        $this->load->model(array('soap_model/msrtc_model', 'bus_source_destination_model', 'common_model', 'users_model', 'ticket_dump_model', 'cancellation_charges_model', 'ticket_details_model', 'op_seat_layout_model', 'op_layout_details_model', 'api_bus_services_model', 'ticket_metadata_model', 'agent_commission_template_log_model', 'tickets_additional_data_model', 'temp_bus_types_model', 'bus_type_model', 'ticket_discount_description_model', 'ticket_details_model', 'Infinity_bus_service_model', 'ticket_id_proof_model'));
        $this->load->library(array('cca_payment_gateway', 'rediscache'));

        $this->url = $this->config->item("its_wsdl_user");
        $this->pass = $this->config->item('its_wsdl_password');
        $this->user_id = $this->config->item('its_wsdl_url');
        $this->its_api_transaction_key = $this->config->item("its_transaction_access_key");
        $this->its_api_inventry_key = $this->config->item("its_inventry_access_key");
    }

    public function index() {
        // echo 'class loaded';
    }

    public function getSource() {

        $action = "http://apibookonspot.itspl.net/GetSources";
        $request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:apib="http://apibookonspot.itspl.net/">
                 <soapenv:Header/>
                 <soapenv:Body>
                    <apib:GetSources>
                       <!--Optional:-->
                       <apib:VerifyCall>' . $this->config->item("its_transaction_access_key") . '</apib:VerifyCall>
                    </apib:GetSources>
                 </soapenv:Body>
              </soapenv:Envelope>';
        $response = get_soap_data_its($this->config->item("its_wsdl_url"), 'GetSources', $request, $action);
        if (isset($response["GetSourcesResult"]) && isset($response["GetSourcesResult"]["diffgram"]) && isset($response["GetSourcesResult"]["diffgram"]["DocumentElement"]) && $response["GetSourcesResult"]["diffgram"]["DocumentElement"]["ITSSources"]) {
            return $response["GetSourcesResult"]["diffgram"]["DocumentElement"]["ITSSources"];
        } else {
            return [];
        }
    }

    public function getDestinationsBasedOnSource($source_id) {
        if (empty($source_id)) {
            return [];
        }

        $action = "http://apibookonspot.itspl.net/GetDestinationsBasedOnSource";
        $request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:apib="http://apibookonspot.itspl.net/">
                     <soapenv:Header/>
                     <soapenv:Body>
                        <apib:GetDestinationsBasedOnSource>
                           <apib:SourceID>' . $source_id . '</apib:SourceID>
                           <!--Optional:-->
                           <apib:VerifyCall>' . $this->config->item("its_transaction_access_key") . '</apib:VerifyCall>
                        </apib:GetDestinationsBasedOnSource>
                     </soapenv:Body>
                  </soapenv:Envelope>';
        $response = get_soap_data_its($this->config->item("its_wsdl_url"), 'GetDestinationsBasedOnSource', $request, $action);

        if (isset($response["GetDestinationsBasedOnSourceResult"]) && isset($response["GetDestinationsBasedOnSourceResult"]["diffgram"]) && isset($response["GetDestinationsBasedOnSourceResult"]["diffgram"]["DocumentElement"]) && $response["GetDestinationsBasedOnSourceResult"]["diffgram"]["DocumentElement"]["ITSDestinations"]) {
            return $response["GetDestinationsBasedOnSourceResult"]["diffgram"]["DocumentElement"]["ITSDestinations"];
        }

        return [];
    }

    public function insertAllSDPairs() {
        $source = $this->getSource();
        $sd_pairs = [];
        $i = $j = 0;
        if ($source) {
            foreach ($source as $source_key => $source_value) {
                if (empty($source_value["CM_CityName"]) && empty($source_value["CM_CityID"])) {
                    echo "\n source Blank \n";
                    //pr($source);
                    echo "\n --------------------------X---------------------------------- \n";
                    continue;
                }
                $destination = $this->getDestinationsBasedOnSource(trim($source_value["CM_CityID"]));
                if (!empty($destination)) {
                    foreach ($destination as $destination_key => $destination_value) {
                        if (!empty($source_value["CM_CityName"]) && !empty($destination_value["CM_CityName"]) && !empty($source_value["CM_CityID"]) && !empty($destination_value["CM_CityID"])) {
                            $sd_pairs = ["api_provider_id" => (string) ITS_PROVIDER_ID,
                                "source_name_id" => trim($source_value["CM_CityID"]),
                                "source_name" => trim($source_value["CM_CityName"]),
                                "destination_name_id" => trim($destination_value["CM_CityID"]),
                                "destination_name" => trim($destination_value["CM_CityName"])
                            ];

                            $check_service = $this->bus_source_destination_model->where($sd_pairs)->find_all();
                            //pr($this->db->last_query());
                            $last_query = $this->db->last_query();
                            echo "\n Source : " . $destination_value["CM_CityName"];
                            echo " - Destination : " . $destination_value["CM_CityName"];
                            if (!$check_service) {
                                $this->bus_source_destination_model->insert($sd_pairs);
                                $i++;
                                echo " --- Ins";
                            } else {
                                $j++;
                                echo " --- No";
                            }
                            echo " \n ---------------------- \n";
                        } else {
                            echo "\n Destination Blank";
                            echo " \n ---------------------- \n";
                        }
                    }
                }
                echo "\n --------------------------X---------------------------------- \n";
            }
        }
        echo " \n Total Inserted : " . $i . " \n Total Failed : " . $j;
    }

    public function getAvailableRoutes($source_id, $destination_id, $date) {
        if (empty($source_id) || empty($destination_id) || empty($date)) {
            return [];
        }

        $date = date("d-m-Y", strtotime($date));

        $action = "http://apibookonspot.itspl.net/GetAvailableRoutes";
        $request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:apib="http://apibookonspot.itspl.net/">
                     <soapenv:Header/>
                     <soapenv:Body>
                        <apib:GetAvailableRoutes>
                           <apib:FromID>' . $source_id . '</apib:FromID>
                           <apib:ToID>' . $destination_id . '</apib:ToID>
                           <!--Optional:-->
                           <apib:JourneyDate>' . $date . '</apib:JourneyDate>
                           <!--Optional:-->
                           <apib:VerifyCall>' . $this->config->item("its_transaction_access_key") . '</apib:VerifyCall>
                        </apib:GetAvailableRoutes>
                     </soapenv:Body>
                  </soapenv:Envelope>';
        $response = get_soap_data_its($this->config->item("its_wsdl_url"), 'GetAvailableRoutes', $request, $action);

        if (isset($response["GetAvailableRoutesResult"]) && isset($response["GetAvailableRoutesResult"]["diffgram"]) && isset($response["GetAvailableRoutesResult"]["diffgram"]["DocumentElement"]) && $response["GetAvailableRoutesResult"]["diffgram"]["DocumentElement"]["AllRouteBusLists"]) {

            return $response["GetAvailableRoutesResult"]["diffgram"]["DocumentElement"]["AllRouteBusLists"];
        }

        return [];
    }

    public function getSeatArrangementDetails($reference_number) {
        if (empty($reference_number)) {
            return [];
        }

        $action = "http://apibookonspot.itspl.net/GetSeatArrangementDetails";
        $request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:apib="http://apibookonspot.itspl.net/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <apib:GetSeatArrangementDetails>
                         <!--Optional:-->
                         <apib:ReferenceNumber>' . $reference_number . '</apib:ReferenceNumber>
                         <!--Optional:-->
                         <apib:VerifyCall>' . $this->config->item("its_transaction_access_key") . '</apib:VerifyCall>
                      </apib:GetSeatArrangementDetails>
                   </soapenv:Body>
                </soapenv:Envelope>';
        $response = get_soap_data_its($this->config->item("its_wsdl_url"), 'GetSeatArrangementDetails', $request, $action);

        if (isset($response["GetSeatArrangementDetailsResult"]) && isset($response["GetSeatArrangementDetailsResult"]["diffgram"]) && isset($response["GetSeatArrangementDetailsResult"]["diffgram"]["DocumentElement"]) && $response["GetSeatArrangementDetailsResult"]["diffgram"]["DocumentElement"]["ITSSeatDetails"]) {
            return $response["GetSeatArrangementDetailsResult"]["diffgram"]["DocumentElement"]["ITSSeatDetails"];
        }

        return [];
    }

    public function getBoardingDropLocationsByCity($company_id, $city_id) {
        if (empty($company_id) || empty($city_id)) {
            return [];
        }

        $action = "http://apibookonspot.itspl.net/GetBoardingDropLocationsByCity";
        $request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:apib="http://apibookonspot.itspl.net/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <apib:GetBoardingDropLocationsByCity>
                         <apib:CompanyID>' . $company_id . '</apib:CompanyID>
                         <apib:CityID>' . $city_id . '</apib:CityID>
                         <!--Optional:-->
                         <apib:VerifyCall>' . $this->config->item("its_transaction_access_key") . '</apib:VerifyCall>
                      </apib:GetBoardingDropLocationsByCity>
                   </soapenv:Body>
                </soapenv:Envelope>';
        $response = get_soap_data_its($this->config->item("its_wsdl_url"), 'GetBoardingDropLocationsByCity', $request, $action);

        if (isset($response["GetBoardingDropLocationsByCityResult"]) && isset($response["GetBoardingDropLocationsByCityResult"]["diffgram"]) && isset($response["GetBoardingDropLocationsByCityResult"]["diffgram"]["DocumentElement"]) && $response["GetBoardingDropLocationsByCityResult"]["diffgram"]["DocumentElement"]["GetBoardingDropLocationsByCity"]) {
            return $response["GetBoardingDropLocationsByCityResult"]["diffgram"]["DocumentElement"]["GetBoardingDropLocationsByCity"];
        }

        return [];
    }

    public function blockSeatV2($data) {
        //Uncomment below line for testing [won't block actual ticket, will only block dummy ticket]
        //return array('BlockID'=>"888".rand(1111,9999),'Status'=>'1','Message'=>'Success');
        if (empty($data)) {
            return [];
        }

        $action = "http://apibookonspot.itspl.net/BlockSeatV2";
        $request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:apib="http://apibookonspot.itspl.net/">
                 <soapenv:Header/>
                 <soapenv:Body>
                    <apib:BlockSeatV2>
                       <!--Optional:-->
                       <apib:ReferenceNumber>' . $data["reference_number"] . '</apib:ReferenceNumber>
                       <!--Optional:-->
                       <apib:PassengerName>' . $data["passenger_name"] . '</apib:PassengerName>
                       <!--Optional:-->
                       <apib:SeatNames>' . $data["seat_number_gender"] . '</apib:SeatNames>
                       <!--Optional:-->
                       <apib:Email>' . $data["email"] . '</apib:Email>
                       <!--Optional:-->
                       <apib:Phone>' . $data["phone"] . '</apib:Phone>
                       <apib:PickupID>' . $data["pickup_id"] . '</apib:PickupID>
                       <apib:PayableAmount>' . $data["payable_amount"] . '</apib:PayableAmount>
                       <apib:TotalPassengers>' . $data["total_passengers"] . '</apib:TotalPassengers>
                       <!--Optional:-->
                       <apib:VerifyCall>' . $this->config->item("its_transaction_access_key") . '</apib:VerifyCall>
                    </apib:BlockSeatV2>
                 </soapenv:Body>
              </soapenv:Envelope>';
        $response = get_soap_data_its($this->config->item("its_wsdl_url"), 'BlockSeatV2', $request, $action);

        $data_to_log = [
                    "ticket_id" => isset($data["ticket_id"]) ? $data["ticket_id"] : "",
                    "request" => $request,
                    "response" => json_encode($response),
                    "type" => "temp_booking"
                   ];

        insert_request_response_log($data_to_log);
        if (isset($response["BlockSeatV2Result"]) && isset($response["BlockSeatV2Result"]["diffgram"]) && isset($response["BlockSeatV2Result"]["diffgram"]["DocumentElement"]) && $response["BlockSeatV2Result"]["diffgram"]["DocumentElement"]["ITSBlockSeatV2"]) {
            return $response["BlockSeatV2Result"]["diffgram"]["DocumentElement"]["ITSBlockSeatV2"];
        }

        return [];
    }

    public function bookSeat($data) {
        //Uncomment below line for testing [won't book actual ticket, will only book dummy ticket]
        //return array('Status'=>1,'PNRNO'=>  "9999".rand(1001, 9999));
        if (empty($data)) {
            return [];
        }

        $action = "http://apibookonspot.itspl.net/BookSeat";
        $request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:apib="http://apibookonspot.itspl.net/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <apib:BookSeat>
                         <!--Optional:-->
                         <apib:ReferenceNumber>' . $data["reference_number"] . '</apib:ReferenceNumber>
                         <!--Optional:-->
                         <apib:PassengerName>' . $data["passenger_name"] . '</apib:PassengerName>
                         <!--Optional:-->
                         <apib:SeatNames>' . $data["seat_number_gender"] . '</apib:SeatNames>
                         <!--Optional:-->
                         <apib:Email>' . $data["email"] . '</apib:Email>
                         <!--Optional:-->
                         <apib:Phone>' . $data["phone"] . '</apib:Phone>
                         <apib:PickupID>' . $data["pickup_id"] . '</apib:PickupID>
                         <apib:PayableAmount>' . $data["payable_amount"] . '</apib:PayableAmount>
                         <apib:TotalPassengers>' . $data["total_passengers"] . '</apib:TotalPassengers>
                         <!--Optional:-->
                         <apib:VerifyCall>' . $this->config->item("its_transaction_access_key") . '</apib:VerifyCall>
                      </apib:BookSeat>
                   </soapenv:Body>
                </soapenv:Envelope>';

        $response = get_soap_data_its($this->config->item("its_wsdl_url"), 'BookSeat', $request, $action);

        if (isset($response["BookSeatResult"]) && isset($response["BookSeatResult"]["diffgram"]) && isset($response["BookSeatResult"]["diffgram"]["DocumentElement"]) && $response["BookSeatResult"]["diffgram"]["DocumentElement"]["ITSBookSeat"]) {
            return $response["BookSeatResult"]["diffgram"]["DocumentElement"]["ITSBookSeat"];
        }

        return [];
    }

    public function cancelDetails($data) {

        if (empty($data)) {
            return [];
        }

        $action = "http://apibookonspot.itspl.net/CancelDetails";
        $request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:apib="http://apibookonspot.itspl.net/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <apib:CancelDetails>
                         <apib:PNRNo>' . $data["pnr_no"] . '</apib:PNRNo>
                         <!--Optional:-->
                         <apib:VerifyCall>' . $this->config->item("its_transaction_access_key") . '</apib:VerifyCall>
                      </apib:CancelDetails>
                   </soapenv:Body>
                </soapenv:Envelope>';

        $response = get_soap_data_its($this->config->item("its_wsdl_url"), 'CancelDetails', $request, $action);
        
        $data_to_log = [
                    "ticket_id" => isset($data["ticket_id"]) ? $data["ticket_id"] : "",
                    "request" => $request,
                    "type" => "is_ticket_cancelable",
                    "response" => json_encode($response),
                   ];

        insert_request_response_log($data_to_log);

        if (isset($response["CancelDetailsResult"]) && isset($response["CancelDetailsResult"]["diffgram"]) && isset($response["CancelDetailsResult"]["diffgram"]["DocumentElement"]) && $response["CancelDetailsResult"]["diffgram"]["DocumentElement"]["ITSTicketCancelDetails"]) {
            return $response["CancelDetailsResult"]["diffgram"]["DocumentElement"]["ITSTicketCancelDetails"];
        }

        return [];
    }

    public function confirmCancellation($data) {
        if (empty($data)) {
            return [];
        }

        $action = "http://apibookonspot.itspl.net/ConfirmCancellation";
        $request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:apib="http://apibookonspot.itspl.net/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <apib:ConfirmCancellation>
                         <apib:PNRNo>' . $data["pnr_no"] . '</apib:PNRNo>
                         <!--Optional:-->
                         <apib:VerifyCall>' . $this->config->item("its_transaction_access_key") . '</apib:VerifyCall>
                      </apib:ConfirmCancellation>
                   </soapenv:Body>
                </soapenv:Envelope>';

        $response = get_soap_data_its($this->config->item("its_wsdl_url"), 'ConfirmCancellation', $request, $action);

        $data_to_log = [
                    "ticket_id" => isset($data["ticket_id"]) ? $data["ticket_id"] : "",
                    "request" => $request,
                    "type" => "ticket_cancel",
                    "response" => json_encode($response),
                   ];

        insert_request_response_log($data_to_log);
        if (isset($response["ConfirmCancellationResult"]) && isset($response["ConfirmCancellationResult"]["diffgram"]) && isset($response["ConfirmCancellationResult"]["diffgram"]["DocumentElement"]) && $response["ConfirmCancellationResult"]["diffgram"]["DocumentElement"]["ConfirmCancellation"]) {
            return $response["ConfirmCancellationResult"]["diffgram"]["DocumentElement"]["ConfirmCancellation"];
        }

        return [];
    }

    public function bookSeatDetails($pnrNumber) {
        if (empty($pnrNumber)) {
            return [];
        }
        $action = "http://apibookonspot.itspl.net/BookSeatDetails";
        $request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:apib="http://apibookonspot.itspl.net/">
                 <soapenv:Header/>
                 <soapenv:Body>
                    <apib:BookSeatDetails>
                        <apib:PNRNo>' . $pnrNumber . '</apib:PNRNo>
                        <!--Optional:-->
                        <apib:VerifyCall>' . $this->config->item("its_transaction_access_key") . '</apib:VerifyCall>
                    </apib:BookSeatDetails>
                 </soapenv:Body>
              </soapenv:Envelope>';
        $response = get_soap_data_its($this->config->item("its_wsdl_url"), 'BookSeatDetails', $request, $action);
        if (isset($response["BookSeatDetailsResult"]["diffgram"]["DocumentElement"]["ITSBookSeatDetails"])) {
            return $response["BookSeatDetailsResult"]["diffgram"]["DocumentElement"]["ITSBookSeatDetails"];
        }
        return [];
    }

    public function ticketStatus($pnrNumber) {
        if (empty($pnrNumber)) {
            return [];
        }
        $action = "http://apibookonspot.itspl.net/TicketStatus";
        $request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:apib="http://apibookonspot.itspl.net/">
                 <soapenv:Header/>
                 <soapenv:Body>
                    <apib:TicketStatus>
                        <apib:PNRNo>' . $pnrNumber . '</apib:PNRNo>
                        <!--Optional:-->
                        <apib:VerifyCall>' . $this->config->item("its_transaction_access_key") . '</apib:VerifyCall>
                    </apib:TicketStatus>
                 </soapenv:Body>
              </soapenv:Envelope>';
        $response = get_soap_data_its($this->config->item("its_wsdl_url"), 'TicketStatus', $request, $action);

        if (isset($response["TicketStatusResult"]["diffgram"]["DocumentElement"]["ITSTicketStatus"])) {
            return $response["TicketStatusResult"]["diffgram"]["DocumentElement"]["ITSTicketStatus"];
        }
        return [];
    }

    public function getCompanyList() {
        $action = "http://apibookonspot.itspl.net/GetCompanyList";
        $request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:apib="http://apibookonspot.itspl.net/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <apib:GetCompanyList>
                         <!--Optional:-->
                         <apib:VerifyCall>' . $this->config->item("its_transaction_access_key") . '</apib:VerifyCall>
                      </apib:GetCompanyList>
                   </soapenv:Body>
                </soapenv:Envelope>';

        $response = get_soap_data_its($this->config->item("its_wsdl_url"), 'GetCompanyList', $request, $action);
        //return $response;
        if (isset($response["GetCompanyListResult"]) && isset($response["GetCompanyListResult"]["diffgram"]) && isset($response["GetCompanyListResult"]["diffgram"]["DocumentElement"]) && $response["GetCompanyListResult"]["diffgram"]["DocumentElement"]["ITSCompanyList"]) {
            return $response["GetCompanyListResult"]["diffgram"]["DocumentElement"]["ITSCompanyList"];
        }

        return [];
    }

    public function getCancellationPolicy($companyId = []) {
        if (empty($companyId)) {
            return [];
        }
        $action = "http://apibookonspot.itspl.net/GetCancellationPolicy";
        $request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:apib="http://apibookonspot.itspl.net/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <apib:GetCancellationPolicy>
                         <apib:CompanyID>' . $companyId . '</apib:CompanyID>
                         <!--Optional:-->
                         <apib:VerifyCall>' . $this->config->item("its_transaction_access_key") . '</apib:VerifyCall>
                      </apib:GetCancellationPolicy>
                   </soapenv:Body>
                </soapenv:Envelope>';

        $response = get_soap_data_its($this->config->item("its_wsdl_url"), 'GetCancellationPolicy', $request, $action);

        if (isset($response["GetCancellationPolicyResult"]["CancellationPolicy"])) {
            return $response["GetCancellationPolicyResult"]["CancellationPolicy"];
        }

        return [];
    }

    public function insertCancellationPolicies() {
        echo '\n Fetching company list \n ';
        $companies = $this->getCompanyList();
        echo '\n Fetching company list \n ';
        if (!empty($companies)) {
            if (!empty($companies['CompanyID'])) {
                echo '\n Fetching company policty for ' . $companies['CompanyID'] . '. ' . $companies['CompanyName'] . '\n ';
                $res = $this->getCancellationPolicy($companies['CompanyID']);
                //ITS_PROVIDER_ID
                if (!empty($res)) {
                    $insert[$companies['CompanyID']] = $res;
                } else {
                    echo "No cancellation policy found. \n";
                }
            } else {
                foreach ($companies as $company) {
                    if (!empty($company['CompanyID'])) {
                        $res = $this->getCancellationPolicy($company['CompanyID']);
                        if (!empty($res)) {
                            $insert[$company['CompanyID']] = $res;
                        } else {
                            echo "\n No cancellation policy found. \n";
                        }
                    }
                }
            }
            if (!empty($insert)) {
                echo "\n Inserting started. \n";
                $this->insertPolicyToDb($insert);
            }
            echo "Done please check your db";
        } else {
            echo "No companies found \n";
        }
    }

    private function insertPolicyToDb($data = array()) {
        if (!empty($data)) {
            foreach ($data as $companyId => $val) {
                $policy = [];
                if (!empty($val['NoCancelWithinMinutes'])) {
                    $policy[] = array('cutoffTime' => ("0-" . $val['NoCancelWithinMinutes'] / 60), 'RefundPercent' => 0);
                }
                if (!empty($val['Policy']['PolicyDetails'])) {
                    foreach ($val['Policy']['PolicyDetails'] as $slot) {
                        $time = (!empty($slot['ToMinutes']) ? (($slot['FromMinutes'] / 60) . "-" . ($slot['ToMinutes'] / 60)) : '');
                        $policy[] = array('cutoffTime' => $time, 'RefundPercent' => $slot['RefundPercent']);
                    }
                }
                $found = $this->getCancellationPolicyByCompanyId($companyId);
                if (empty($found)) {
                    $s = "INSERT INTO cancellation_policies (op_id,api_id,company_id,cancellation_policy,created_at) VALUES ('0','" . ITS_PROVIDER_ID . "','" . $companyId . "','" . json_encode($policy) . "',NOW())";
                    $this->db->query($s);
                    echo "\n Added Record \n";
                } else {
                    $s = "UPDATE cancellation_policies SET cancellation_policy = '" . json_encode($policy) . "' WHERE api_id='" . ITS_PROVIDER_ID . "' AND company_id = '" . $companyId . "'";
                    $this->db->query($s);
                    echo "\n Updated Record \n";
                }
            }
        }
    }

    public function getCancellationPolicyByCompanyId($company_id) {
        if (empty($company_id)) {
            return [];
        }
        $s = "SELECT * FROM cancellation_policies WHERE api_id= '" . ITS_PROVIDER_ID . "' AND company_id = '" . $company_id . "'";
        $q = $this->db->query($s);
        $r = $q->result_array();
        return $r;
    }

    public function getAvailableBuses($input,$comm_id_data, $to_update = true) {

        // *        VERY IMPORTANT NOTICE
        // * Do not change trip_no format created in bus_data array
        // * These parameter is used in seat layout
        $its_cacellation_charges = [];
        $data_to_store = [];
        $data = [];
        $data["bus_detail"] = array();
        $data["time_fare"]["bus_detail"] = array(
            "min_fare" => 0,
            "max_fare" => 0,
            "min_dep_tm" => 0,
            "max_dep_tm" => 0
        );
        $bus_data = [];
        $min_max_deptm = array();
        $min_max_fare = array();
        $current_journey_date = date("Y-m-d", strtotime($input["date_of_jour"]));
        $total_seats_available = 0;

        $session_store = array();
        $time_range = array(array('start' => '04:00:00', 'end' => '11:59:00', "shift" => "morning"),
            array('start' => '12:00:00', 'end' => '15:59:00', "shift" => "afternoon"),
            array('start' => '16:00:00', 'end' => '18:59:00', "shift" => "evening"),
            array('start' => '19:00:00', 'end' => '23:59:00', "shift" => "night"),
            array('start' => '00:00:00', 'end' => '03:59:00', "shift" => "mid_night")
        );

        if ($current_journey_date == "2016-10-30" || $current_journey_date == "2016-10-31") {
            return array("its_data" => $data);
        }

        $etsData = $input['etravels_data'];

        $all_its_src_dest = $input['source_destination_data'];

        $post_fields = array("sourceCity" => $input['from'],
            'sourceCityId' => $all_its_src_dest['source_name_id'],
            "destinationCity" => $input['to'],
            'destinationCityId' => $all_its_src_dest['destination_name_id'],
            "doj" => date("Y-m-d", strtotime($current_journey_date))
        );

        log_data("infinity/" . date("M") . "/api_service_" . date("d-m-Y") . "_log.log", $post_fields, 'Bus Service Request');
        $response = $this->getAvailableRoutes($all_its_src_dest['source_name_id'], $all_its_src_dest['destination_name_id'], $input['date_of_jour']);
        log_data("infinity/" . date("M") . "/api_service_" . date("d-m-Y") . "_log.log", $response, 'Bus Service Response ');
        //$response = $this->getAvailableRoutes('703', '243543253', '2016-11-30');
        if (!empty($response)) {
            if (empty($response[0])) {
                $response = array($response);
            }
        } else if ($response) {
            $data["status"] = "error";
            $data["bus_detail"] = array();
            $data["time_fare"]["bus_detail"] = array(
                "min_fare" => 0,
                "max_fare" => 1,
                "min_dep_tm" => 0,
                "max_dep_tm" => 0
            );
            return array("its_data" => $data);
        }

        $user_data['from'] = $input['from'];
        $user_data['to'] = $input['to'];
        $user_data['date_of_jour'] = $input['date_of_jour'];
        $user_data['date'] = date("Y-m-d", strtotime($input['date_of_jour']));

        $data['user_data'] = $user_data;

        $bos_bustypes = $this->bus_type_model->as_array()->find_all();
        if (!empty($bos_bustypes)) {
            foreach ($bos_bustypes as $bosvalue) {
                $bos_bus_types[trim($bosvalue['bus_type_name'])] = trim($bosvalue['id']);
            }
        }

        $mapped_bus_types = array();
        $map_bus_type = $this->bus_type_model->get_api_bus_type((string) ITS_PROVIDER_ID);
        if ($map_bus_type && !empty($map_bus_type["mapped_type"])) {
            $mapped_bus_types = $map_bus_type["mapped_type"];
        }

        $data['user_data']["from_stop_code"] = $input['from'];
        $data['user_data']["to_stop_code"] = $input['to'];

        $extra_array["mapped_bus_types"] = $mapped_bus_types;
        $bus_type = array();

        $min_fare = 0;
        $max_fare = 1;

        $min_dep_tm = 0;
        $max_dep_tm = 0;

        $min_max_fare = array();
        $min_max_deptm = array();

        $is_bus_type_filter_on = in_array($input['searchbustype'], ["ac", "non_ac"]) ? true : false;

        $setServicesToRedis = [];
        $j = 1;
        foreach ($response as $key => $value) {
            $value = array_map("utf8_encode",$value);
            $farearray  = array();
            $farearray_new = array();
            $to_proceed = true;
            $tempBusName = (!empty($value['BusTypeName'])) ? " " . $value['BusTypeName'] : "";
            $bustTypeIts = $extra_array["busType"] = trim($value['BusType'] == 0 ? "Ac" . $tempBusName : "Non Ac" . $tempBusName);

            if ($is_bus_type_filter_on) {
                $to_proceed = check_search_bus_type($input, $extra_array);
                //$to_proceed = true; //remove when live
            }

            if ($to_proceed) {
                if ((stripos(strtolower($value['CompanyName']), "Himachal Road Transport Corporation HRTC") === false && stripos(strtolower($value['CompanyName']), "Uttar Pradesh State Road Transport Corporation (UP") === false && stripos(strtolower($value['CompanyName']), "uttar pradesh state road transport corporation(upsrtc)") === false && stripos(strtolower($value['CompanyName']), "upsrtc") === false && stripos(strtolower($value['CompanyName']), "Uttar Pradesh State Transport") === false && stripos(strtolower($value['CompanyName']), "msrtc") === false)) {
                    //Compare ITS commission with etravelsmart
                    $keepInfinity = $this->selectFromEtsAndIts($value, $etsData);
                    if ($keepInfinity['status'] == true) {
                        //For boarding dropping stops (At the time of seat layout)
                        $sessKey = "its_board_drop_for_" . $value['RouteTimeID'];
                        $setDataSess = array('company_id' => $value['CompanyID'], 'from_city_id' => $value['FromCityId'], 'to_city_id' => $value['ToCityId']);
                        $this->session->set_userdata($sessKey, json_encode($setDataSess));
                        //Check with ets commission
                        if (isset($keepInfinity['key']) && $keepInfinity['key'] != '') {
                            if (!empty($input['api'])) {
                                $removeFromEtsRedis[] = $etsData['bus_detail'][$keepInfinity['key']]['routeScheduleId'];
                            }
                            unset($etsData['bus_detail'][$keepInfinity['key']]);
                        }
                        $departureTime = date("H:i:s", strtotime($value['RouteTime']));


                        //retrive trip wise  boarding stop detail
                        $dept_tm_his = date("H:i:s", strtotime($value['RouteTime']));

                        $session_store[$value['RouteTimeID']]['from_stop_id'] = $all_its_src_dest['source_name_id'];
                        $session_store[$value['RouteTimeID']]['to_stop_id'] = $all_its_src_dest['destination_name_id'];

                        // for boarding Points
                        $data["bus_detail"][$key]['barr'] = array();
                        if (!empty($value['BoardingPoints'])) {
                            $brdStops = explode("#", $value['BoardingPoints']);
                            if (!empty($brdStops)) {
                                foreach ($brdStops as $k => $val) {
                                    $tmp = explode("|", $val);
                                    $boardStopsDetail = array();
                                    $boardStopsDetail["stop_code"] = $boardStopsDetail["boarding_point_id"] = !empty($tmp[0]) ? $tmp[0] : '';
                                    '';
                                    $boardStopsDetail["bus_stop_name"] = $boardStopsDetail["boarding_stop_name"] = $boardStopsDetail["location"] = (!empty($tmp[1]) ? $tmp[1] : '');
                                    $boardStopsDetail["arrival_duration"] = !empty($tmp[2]) ? $tmp[2] : '';
                                    $boardStopsDetail["is_boarding"] = "Y"; //This is for boarding point
                                    $boardStopsDetail["is_alighting"] = "N"; //This is for dropping point
                                    $boardStopsDetail["seq"] = $k + 1;

                                    $data["bus_detail"][$key]['barr'][$boardStopsDetail["boarding_point_id"]] = $boardStopsDetail;

                                    // * For Session 
                                    if (!empty($tmp[0])) {
                                        if (!empty($input['api'])) {
                                            $input['from'] = strtolower($input['from']);
                                            $input['to'] = strtolower($input['to']);
                                            $this->rediscache->set($input['from'] . $input['to'] . $input['date_of_jour'], $boardStopsDetail, (60 * 60 * 60 * 24));
                                        }
                                        $session_store[$value['RouteTimeID']]["barr"][$tmp[0]] = $boardStopsDetail;
                                    }
                                }
                                unset($k, $val, $tmp, $drpStops, $boardStopsDetail);
                                $sessKey = "its_boarding_points_for_" . $value['RouteTimeID'];
                                $this->session->set_userdata($sessKey, $data["bus_detail"][$key]['barr']);
                            }
                        }
                        //Set Boarding points data into session 
                        // for dropping Points
                        $data["bus_detail"][$key]['darr'] = array();
                        if (!empty($value['DroppingPoints'])) {
                            $drpStops = explode("#", $value['DroppingPoints']);
                            if (!empty($drpStops)) {
                                foreach ($drpStops as $k => $val) {
                                    $tmp = explode("|", $val);
                                    $dropStopsDetail = array();
                                    $dropStopsDetail["stop_code"] = $dropStopsDetail["boarding_point_id"] = !empty($tmp[0]) ? $tmp[0] : '';
                                    $dropStopsDetail["bus_stop_name"] = $dropStopsDetail["boarding_stop_name"] = $dropStopsDetail["location"] = !empty($tmp[1]) ? $tmp[1] : '';
                                    $dropStopsDetail["arrival_duration"] = !empty($tmp[2]) ? $tmp[2] : '';
                                    $dropStopsDetail["is_boarding"] = "N"; //This is for boarding point
                                    $dropStopsDetail["is_alighting"] = "Y"; //This is for dropping point
                                    $dropStopsDetail["seq"] = $k + 1;

                                    $data["bus_detail"][$key]['barr'][] = $dropStopsDetail;

                                    // * For Session 
                                    if (!empty($tmp[0])) {
                                        $session_store[$value['RouteTimeID']]["darr"][$tmp[0]] = $dropStopsDetail;
                                    }
                                }
                                unset($k, $val, $tmp, $drpStops, $dropStopsDetail);
                                $sessKey = "its_dropping_points_for_" . $value['RouteTimeID'];
                                $this->session->set_userdata($sessKey, $data["bus_detail"][$key]['darr']);
                            }
                        }

                        //If conditions for bus_type_id $bus_type_id
                        if (array_key_exists($bustTypeIts, $mapped_bus_types)) {
                            $bus_type_id = $mapped_bus_types[$bustTypeIts];
                            if (!empty($bos_bus_types) && array_key_exists(trim($bus_type_id), $bos_bus_types)) {
                                $bus_type_id = $bos_bus_types[trim($bus_type_id)];
                            }
                        } else {
                            $bus_type_id = "Others";
                        }
                        //log_data("data/itsdata.log",date("Y-m-d H:i:s"),"bus_type_id");
                        //Required Details
                        $data["bus_detail"][$key]["bus_travel_id"] = $value['RouteTimeID'];
                        $data["bus_detail"][$key]["from_stop_code"] = $input['from'];
                        $data["bus_detail"][$key]["to_stop_code"] = $input['to'];
                        $data["bus_detail"][$key]['op_name'] = ucwords(str_replace("'", "", $value['CompanyName']));
                        $data["bus_detail"][$key]['bus_stop_name'] = $input['to'];

                        $data["bus_detail"][$key]['mticket_allowed'] = false; //Blind check it
                        $data["bus_detail"][$key]['from_stop'] = ucwords($input['from']);
                        $data["bus_detail"][$key]['to_stop'] = ucwords($input['to']);
                        $data["bus_detail"][$key]['dept_tm'] = date("Y-m-d", strtotime($input['date_of_jour'])) . " " . $dept_tm_his;
                        $data["bus_detail"][$key]['sch_departure_date'] = date("d-m-Y", strtotime($input['date_of_jour']));

                        $data["bus_detail"][$key]['arrival_tm'] = $value['ArrivalTime'];

                        $data["bus_detail"][$key]['api_bus_type'] = true;
                        $data["bus_detail"][$key]['bus_type_name'] = (array_key_exists(trim($data["bus_detail"][$key]['bus_type_name']), $mapped_bus_types)) ? $mapped_bus_types[trim($data["bus_detail"][$key]['bus_type_name'])] : "Others";
                        $data["bus_detail"][$key]['bus_type_id'] = $bus_type_id;
                        $data["bus_detail"][$key]['op_bus_type_name'] = trim($value['BusTypeName']);
                        $data["bus_detail"][$key]['tot_seat_available'] = $value['EmptySeats'];

                        $temp_bus_types[] = $data["bus_detail"][$key]['bus_type_name'];
                        //log_data("data/itsdata.log",$data["bus_detail"][$key],"after bus_type_id".date("Y-m-d H:i:s"));
                        //sorted fare in order. Used/benefit while sorting.
                        if (!empty($value['AcSeatRate'])) {
                            $farearray[] = $value['AcSeatRate'];
                        }
                        if (!empty($value['AcSleeperRate'])) {
                            $farearray[] = $value['AcSleeperRate'];
                        }
                        if (!empty($value['AcSlumberRate'])) {
                            $farearray[] = $value['AcSlumberRate'];
                        }
                        if (!empty($value['NonAcSeatRate'])) {
                            $farearray[] = $value['NonAcSeatRate'];
                        }
                        if (!empty($value['NonAcSleeperRate'])) {
                            $farearray[] = $value['NonAcSleeperRate'];
                        }
                        if (!empty($value['NonAcSlumberRate'])) {
                            $farearray[] = $value['NonAcSlumberRate'];
                        }
                        $min_max_fare = $farearray;
                        asort($farearray);
                        $data["bus_detail"][$key]['actual_seat_fare'] = implode(",", $farearray);
                        $data["bus_detail"][$key]['service_discount'] = false;
                        //Discount Calculation 
                        $discFlag = false;
                        
                        //log_data("data/itsdata.log",date("Y-m-d H:i:s"),"discount");
                        // -----Cancellation policy 
                        //check whethere available in class variable else fetch it from table
                        $cancellation_policy = array();
                        if (isset($this->cancellationPolicy[$value['CompanyID']])) {
                            $cancellation_policy = $this->cancellationPolicy[$value['CompanyID']];
                        } else {
                            //Fetch Data from table
                            $cpolicy = $this->getCancellationPolicyByCompanyId($value['CompanyID']);

                            if (!empty($cpolicy[0]['cancellation_policy'])) {
                                $policy = json_decode($cpolicy[0]['cancellation_policy'], 1);
                                if (!empty($policy)) {
                                    foreach ($policy as $k => $val) {
                                        $temp = new stdClass;
                                        $temp->cutoffTime = $val['cutoffTime'];
                                        $temp->refundInPercentage = $val['RefundPercent'];
                                        $cancellation_policy[] = $temp;
                                    }
                                    $this->cancellationPolicy[$value['CompanyID']] = $cancellation_policy;
                                }
                            }
                        }
                        //log_data("data/itsdata.log",date("Y-m-d H:i:s"),"cancellation_policy");
                        $data["bus_detail"][$key]['service_discount'] = $discFlag;
                        $farearray = !empty($farearray_new) ? $farearray_new : $farearray;
                        $data["bus_detail"][$key]['seat_fare'] = implode(",", array_unique($farearray));
                        $data["bus_detail"][$key]['cancellation_policy'] = $cancellation_policy;
                        $data["bus_detail"][$key]['inventory_type'] = 0; //Not provided
                        $data["bus_detail"][$key]['route_schedule_id'] = $value['RouteTimeID'];
                        $data["bus_detail"][$key]['id_proof_required'] = 0; //Not provided
                        $data["bus_detail"][$key]['partial_cancellation_allowed'] = 0; //Not provided
                        $data["bus_detail"][$key]['commission_percentage'] = ITS_COMMISSION;
                        $data["bus_detail"][$key]['str_dep_tm'] = strtotime($value['RouteTime']);
                        $data["bus_detail"][$key]['str_dep_tm_his'] = $value['RouteTime'];
                        $data["bus_detail"][$key]['str_dep_tm_hm'] = strtotime($user_data['date'] . " " . $dept_tm_his);
                        $data["bus_detail"][$key]['arrival_time'] = $value['ArrivalTime'];
                        $data["bus_detail"][$key]['arrival_date'] = $value['ArrivalTime'];
                        $data["bus_detail"][$key]['arrival_date_time'] = $value['ArrivalTime'];
                        $data["bus_detail"][$key]['op_trip_no'] = base64_encode($value['ReferenceNumber']);
                        $data["bus_detail"][$key]['trip_no'] = $value['RouteTimeID'];
                        $data["bus_detail"][$key]['service_id'] = $value['RouteTimeID'];
                        $data["bus_detail"][$key]['provider_id'] = ((string) ITS_PROVIDER_ID);
                        $data["bus_detail"][$key]['provider_type'] = "api_provider";
                        $data["bus_detail"][$key]['reference_number'] = $value['ReferenceNumber'];
                        $data["bus_detail"][$key]['op_id'] = 0;
                        //log_data("data/itsdata.log",$data["bus_detail"][$key],"bus detail");
                        $commission_fare = array();
                        $agent_commission = 0;
                        eval(FME_AGENT_ROLE_ID);
                        if(in_array($this->session->userdata("role_id"), $fme_agent_role_id) || ($this->session->userdata("role_id") == ORS_AGENT_USERS_ROLE_ID)) {
                            if(!empty($comm_id_data[0]->commission_id) && $comm_id_data[0]->commission_id > 0 ) {
                                $i = 0;
                                foreach($farearray_new1 as $vfare) {
                                    $commission_fare[$i]['base_fare'] = $vfare;
                                    $i++;
                                }
                                $agent_commission = agent_ag_calculate_commission($commission_fare,$this->config->item('its_api_id'),0,$comm_id_data[0]->commission_id,$data["bus_detail"][$key]['bus_type_id'],ITS_COMMISSION);
                            }
                        }
                        if($agent_commission == "") {
                            $agent_commission = 0;
                        }
                        
                        $data["bus_detail"][$key]['agent_commission_percentage']        = $agent_commission;
                        
                        // * ******* Total time required to travel calculation start*********
                        //This is incorrect. Because API does not provide us exact date of arrival time
                        // * ----------------------- 
                        $departure_his_str = strtotime(date("h:i A", strtotime($value['RouteTime'])));
                        $arrival_his_str = strtotime(date("h:i A", strtotime($value['ArrivalTime'])));

                        if ($departure_his_str < $arrival_his_str) {
                            //Same arrival day
                            $assumed_arrival_date = date("Y-m-d", strtotime($input['date_of_jour'])) . " " . date("H:i:s", strtotime($value['ArrivalTime']));
                        } else if (($departure_his_str > $arrival_his_str) || ($departure_his_str == $arrival_his_str)) {
                            //Add one day to arrival date 
                            $assumed_arrival_date = date("Y-m-d", strtotime($input['date_of_jour'] . ' +1 day')) . " " . date("H:i:s", strtotime($value['ArrivalTime']));
                        }
                        //for calculatiom
                        $assumed_departure_date = $input['date_of_jour'] . " " . date("H:i:s", $departure_his_str);
                        // ---------------------- //

                        $tmdiff = calculate_date_diff($assumed_departure_date, $assumed_arrival_date);
                        $days = $tmdiff->format('%d');
                        $hours = $tmdiff->format('%h');
                        $minutes = $tmdiff->format('%i');

                        $travel_time = (($days > 0) ? $days . " day " : "") . (($hours > 0) ? $hours . " hours " : "") . (($minutes > 0) ? $minutes . " minutes" : "");
                        $short_travel_time = (($days > 0) ? $days . " D " : "") . (($hours > 0) ? $hours . " H " : "") . (($minutes > 0) ? $minutes . " M" : "");
                        $new_time_to_show = calculate_travel_duration($tmdiff);

                        $data["bus_detail"][$key]["travel_time"] = $new_time_to_show;
                        $data["bus_detail"][$key]["short_travel_time"] = $new_time_to_show;
                        // * ******* Total time required to travel calculation end ********* 
                        // * *********** For Travel Duration Filter Start************* 
                        $travel_duration = "";
                        // $travel_duration = ($hours <= 6) ? "six" : ($hours > 6 && $hours <= 12) ? "twelve" : ($hours > 12 && $hours <= 24) ? "twentyfour" : ($hours > 24) ? "more" : "";
                        $travel_duration = "";
                        if ($days == 0 && $hours <= 6) {
                            $travel_duration = "six";
                        } else if ($days == 0 && $hours > 6 && $hours <= 12) {
                            $travel_duration = "twelve";
                        } else if ($days == 0 && $hours > 12 && $hours <= 24) {
                            $travel_duration = "twentyfour";
                        } else if ($days > 0) {
                            $travel_duration = "more";
                        }

                        $data["bus_detail"][$key]["travel_duration"] = $travel_duration;
                        // * *********** For Travel Duration Filter End***************

                        $min_max_deptm[] = $data["bus_detail"][$key]['str_dep_tm_hm'];


                        // * ***** to claculate shift ****** 
                        foreach ($time_range as $i => $shift_time) {
                            if (strtotime($time_range[$i]['start']) <= strtotime($dept_tm_his) && strtotime($time_range[$i]['end']) >= strtotime($dept_tm_his)) {
                                $shift = $shift_time["shift"];
                            }
                        }

                        $data["bus_detail"][$key]['shift'] = $shift;


                        // *                        NOTICE IMP
                        // * In the temp booking(controller -> booking.php function->temp_booking) 
                        // * We are not getting all the required detail in seat availabilty.
                        // * To AVOID API hit we are storing session here and destroying session in temp booking function.
                        // * Discuss this and find better alternate solution.

                        $session_store[$value['RouteTimeID']]["mticket_allowed"] = 0; //Not provided
                        $session_store[$value['RouteTimeID']]["partial_cancellation_allowed"] = ''; //Not provided
                        $session_store[$value['RouteTimeID']]["sch_departure_tm"] = $dept_tm_his;
                        $session_store[$value['RouteTimeID']]["arrival_time"] = $value['ArrivalTime'];
                        $session_store[$value['RouteTimeID']]["inventory_type"] = 0; //Not provided
                        $session_store[$value['RouteTimeID']]["bus_type"] = $data["bus_detail"][$key]['bus_type_name'];
                        $session_store[$value['RouteTimeID']]["commision_percentage"] = ITS_COMMISSION; //Not provided
                        $session_store[$value['RouteTimeID']]["id_proof_required"] = 0; //Not provided
                        $session_store[$value['RouteTimeID']]["departure_datetime"] = $data["bus_detail"][$key]['dept_tm'];
                        $session_store[$value['RouteTimeID']]["reference_number"] = $value['ReferenceNumber'];
                        //session set to used on next pages(Till booking end);
                        // * ********* end ****************** 
                        $setServicesToRedis[$value['RouteTimeID']] = $data["bus_detail"][$key];
                        //log_data("data/itsdata.log",date("Y-m-d H:i:s"),"SESSION STORE");                        
                    }
                }
            }
        }

        //Set Services to redis which will be useful at temp_booking
        if (!empty($input['api']) && !empty($setServicesToRedis)) {
            $input['from'] = strtolower($input['from']);
            $input['to'] = strtolower($input['to']);
            $this->rediscache->set($input['from'] . $input['to'] . $input['date_of_jour'], $setServicesToRedis, (60 * 60 * 60 * 24));
        }

        //Reset Etravels data into redis after commission comparision
        if (!empty($input['api']) && !empty($removeFromEtsRedis)) {
            $redisEtravelData = $this->rediscache->get($input['from'] . $input['to'] . $input['date_of_jour']);
            if (!empty($redisEtravelData)) {
                foreach ($removeFromEtsRedis as $val) {
                    if (!empty($redisEtravelData[$val])) {
                        unset($redisEtravelData[$val]);
                    }
                }
                $input['from'] = strtolower($input['from']);
                $input['to'] = strtolower($input['to']);
                $this->rediscache->set($input['from'] . $input['to'] . $input['date_of_jour'], $redisEtravelData, (60 * 60 * 60 * 24));
            }
        }

        if (!empty($data['bus_detail'])) {
            if (!empty($min_max_fare)) {
                $data["time_fare"]["bus_detail"]['min_fare'] = min($min_max_fare);
                $data["time_fare"]["bus_detail"]['max_fare'] = max($min_max_fare);
            }

            if (!empty($min_max_deptm)) {
                $data["time_fare"]["bus_detail"]['min_dep_tm'] = min($min_max_deptm);
                $data["time_fare"]["bus_detail"]['max_dep_tm'] = max($min_max_deptm);
            }

            // *          NOTICE
            // * Session is converted and fetch via table
            // * Save bus services in infinity_bus_service table
            // *
            $journey_date = date("Y-m-d", strtotime($input['date_of_jour']));

            $bus_service_where = array("from" => $input['from'], "to" => $input['to'], "date" => $journey_date);

            $is_service_exists = $this->getItsService($bus_service_where);

            $session_store_encode = json_encode($session_store);
            if (empty($is_service_exists)) {
                $infinity_save = array(
                    'from' => $input['from'],
                    'to' => $input['to'],
                    'date' => $journey_date,
                    'bus_service' => $session_store_encode,
                );
                $this->db->insert('infinity_bus_service', $infinity_save);
                $insert_id = $this->db->insert_id();
                $data["status"] = ($insert_id) ? "success" : "error";
            } else if ($to_update) {
                $this->db->update('infinity_bus_service', array("bus_service" => $session_store_encode), array('from' => $input['from'], 'to' => $input['to'], 'date' => $journey_date));
                $is_true = false;
            } else {
                $is_true = false;
                $data["status"] = "success";
            }
            $data["status"] = "success";
            // * *********************************************** 
            // *              VERY IMPORTANT NOTICE
            // * Below code is for to make entry of bus types in temp_bus_types table.
            // * We are purposely making seprate entry for each search to make more accurate bus types mapping.
            // * So that each and every bus types will be covered.
            // * After some days move this code to if else condition that will make entry only once for particular date and update
            // * same logic implemented for entering bus services
            // * we can move this conditon in above bus service  if else condition.
            // * From temp_bus_types after mapping delete data.
            $temp_bus_types = array_unique($temp_bus_types);
            $temp_bus_types_save = array(
                'date' => $journey_date,
                'provider_id' => ITS_PROVIDER_ID,
                'bus_types' => json_encode($temp_bus_types),
            );

            $temp_bus_types_id = $this->temp_bus_types_model->insert($temp_bus_types_save);


            $return['its_data'] = $data;

            if (!empty($etsData['bus_detail'])) {
                unset($etsData['compare_commission_data']);
                $return['ets_data'] = $etsData;
            } else {
                $return['ets_data'] = array();
            }
            return $return;
        } else {
            $data["status"] = "error";
            $data["bus_detail"] = array();
            $data["time_fare"]["bus_detail"] = array(
                "min_fare" => 0,
                "max_fare" => 1,
                "min_dep_tm" => 0,
                "max_dep_tm" => 0
            );
            return array("its_data" => $data);
        }
    }

    private function selectFromEtsAndIts($itsCurData = array(), $etsData = array()) {
        if (!empty($etsData) && !empty($etsData['compare_commission_data'])) {
            foreach ($etsData['compare_commission_data'] as $key => $val) {
                if ($val['operator_name'] == $itsCurData['CompanyID'] && $val['departure_time'] == $itsCurData['RouteTime'] && $val['arrival_time'] == $itsCurData['ArrivalTime']) {
                    if (ITS_COMMISSION > $val['commission_percentage']) {
                        return array("status" => true, "key" => $key, "msg" => "ets has lower commission.");
                    } else {
                        return array("status" => false, "msg" => "Ets has higher commission.");
                    }
                } else {
                    return array("status" => true, "msg" => "No match found in etravelsmart.");
                }
            }
        } else {
            return array("status" => true, "msg" => "No Ets Data found");
        }
    }

    private function getItsService($bus_service_where, $returnFields = "") {
        if ($returnFields != '' && $returnFields == 'all') {
            $fields = '*';
        } else {
            $fields = 'id';
        }
        if (!empty($bus_service_where['from']) && !empty($bus_service_where['to']) && !empty($bus_service_where['date'])) {
            $s = "SELECT " . $fields . " FROM infinity_bus_service WHERE `from` = '" . $bus_service_where['from'] . "' AND `to` = '" . $bus_service_where['to'] . "' AND `date` = '" . $bus_service_where['date'] . "'";
            $q = $this->db->query($s);
            $r = $q->result_array();
            return $r;
        } else {
            return array();
        }
    }

    public function get_seat_layout($fields) {
        $response = array();

        // *                    NOTICE
        // * We can fetch inventoryType from session instead of passing
        // * Discuss this later. Do not remove below comment of $fields['inventory_type']

        $post_fields = array(
            "reference_number" => base64_decode($fields['op_trip_no'])
        );

        log_data("infinity/" . date("M") . "/seat_layout_" . date("d-m-Y") . "_log.log", $post_fields, 'Seat Layout Request');
        $api_response = $this->getSeatArrangementDetails($post_fields['reference_number']);
        log_data("infinity/" . date("M") . "/seat_layout_" . date("d-m-Y") . "_log.log", $api_response, 'Seat Layout Response');

        if (!empty($api_response)) {

            $seat_fare_detail = array();
            $total_available_seats = 0;
            $column_array = array();
            $row_array = array();
            $fare_array = array();
            $basic_fare_array = array();

            foreach ($api_response as $key => $value) {
                //To fit this api with etravel smart made below changes
                $berthType = (isset($value['UpLowBerth']) && $value['UpLowBerth'] == 'UB' ) ? '1' : '0';

                /* $length = (isset($value['ColumnSpan']) && $value['ColumnSpan'] == '2') ? '2' : '1';
                  $width = (isset($value['RowSpan']) && $value['RowSpan'] == '2') ? '2' : '1'; */

                /* $length = (isset($value['RowSpan']) && $value['RowSpan'] == '2') ? '1' : '2';
                  $width = (isset($value['ColumnSpan']) && $value['ColumnSpan'] == '2') ? '1' : '2'; */

                if ($value['RowSpan'] == 0 && $value['ColumnSpan'] == 0) {
                    $length = $width = 1;
                }
                if ($value['RowSpan'] == 2 && $value['ColumnSpan'] == 0) {
                    $length = 1;
                    $width = 2;
                }

                if ($value['RowSpan'] == 0 && $value['ColumnSpan'] == 2) {
                    $length = 2;
                    $width = 1;
                }


                $tmp = isset($value['SeatCategory']) ? ((string) $value['SeatCategory']) : '1';
                $ladiesFlag = ((!empty($value['IsLadiesSeat']) && $value['IsLadiesSeat'] == 'Y') ? true : false);
                $acFlag = ($acFlag[0] == 2 ? true : false );
                $sleeperFlag = (isset($value['SeatType']) && $value['SeatType'] == 1) ? true : false;
                switch ($value['SeatCategory']) {
                    case '10' :
                        $seat_type = 'Seater';
                        break;
                    case '11' :
                        $seat_type = 'Sleeper,';
                        break;
                    case '12' :
                        $seat_type = 'Semi Sleeper(Slumber)';
                        break;
                    case '20' :
                        $seat_type = 'Ac,Seater';
                        break;
                    case '21' :
                        $seat_type = 'Ac,Sleeper';
                        break;
                    case '22' :
                        $seat_type = 'Ac,Semi Sleeper(Slumber)';
                        break;
                    default :
                        $seat_type = 'Sleeper';
                        break;
                }
                //To fit this api with etravel smart made above changes
                $horizontal_seat = false;
                $vertical_seat = false;

                // *           VERY IMP NOTICE
                // * POSITION OF COLUMN AND ROWS CHANGED HERE
                // * Insert row and column number in array to find total number of columns and rows.
                // * We can find this by this method also $seat_layout_var[count($seat_layout_var)-1]->row.
                // * But this method fails when response come in unorder manner. Thats why calculated manually.

                /* if (!in_array($value['Row'] + 1, $row_array)) {
                  $row_array[] = $value['Row'] + 1;
                  } */
                //$row_array[] = $value['Row'];


                /* if (!in_array($value['Column'] + 1, $column_array)) {
                  $column_array[] = $value['Column'] + 1;
                  } */
                //$column_array[] = $value['Column'];

                if (!in_array($value['Row'], $row_array)) {
                    $row_array[] = $value['Row'];
                }
                if (!in_array($value['Column'], $column_array)) {
                    $column_array[] = $value['Column'];
                }

                $coll[] = $value['Column'];
                $rowl[] = $value['Row'];


                if (!in_array($value['SeatRate'], $fare_array)) {
                    $fare_array[] = $value['SeatRate'];
                }

                if (!in_array($value['BaseFare'], $basic_fare_array)) {
                    $basic_fare_array[] = $value['BaseFare'];
                }

                //$pos = ($value['Column'] + 1) . "X" . ($value['Row'] + 1);
                //$pos = ($value['Column']) . "X" . ($value['Row']);
                $pos = ($value['Row']) . "X" . ($value['Column']);
                $berth_no = $berthType + 1;


                // $current_seat_no = trim(str_ireplace(array("(w)","(w"), "", $value->id));
                $current_seat_no = $value['SeatNo'];

                $current_seat_discount = 0;
                $current_actual_seat_value = $value['BaseFare'];
                $current_actual_seat_value_with_tax = $value['SeatRate'];

                $current_seat_value = $value['BaseFare'];
                $current_seat_value_with_tax = $value['SeatRate'];
                $current_seat_service_tax = $value['ServiceTax'];
                $current_seat_operator_service_charge = 0; //Not provided
                // * *****temp discount logic start***** 
                $discount_array = array();

                
                //echo "curr seat with tax".$current_seat_value;exit;
                // * *****temp discount logic end***** 
                // * ***************** Layout Parameteres & Description Start***************** 
                
                /*******temp agent commission logic start******/
          
            $agent_commission = 0;
            
          
          /*******temp agent commission logic end******/
                $current_seat_type = "";

                //Normal,seater,semi-sleeper
                if ($berthType == 0 && $length == 1 && $width == 1) {
                    if ($ladiesFlag) {
                        $current_seat_type = "ladies";
                    }
                }

                //vertical sleeper
                if ($length == 1 && $width == 2) {
                    if ($ladiesFlag) {
                        $current_seat_type = "vladies_sleeper";
                    } else {
                        $current_seat_type = "vsleeper";
                    }

                    $vertical_seat = true;
                }

                //horizontal sleeper
                if ($length == 2 && $width == 1) {
                    if ($ladiesFlag) {
                        $current_seat_type = "ladies_sleeper";
                    } else {
                        $current_seat_type = "sleeper";
                    }
                    $horizontal_seat = true;
                }

                // * ************* Layout Parameteres & Description End***************

                $seat_layout[$berth_no][$pos] = array(
                    'seat_no' => $current_seat_no,
                    'actual_seat_fare' => $current_actual_seat_value_with_tax,
                    'actual_seat_basic_fare' => $current_actual_seat_value,
                    'seat_fare' => $current_seat_value_with_tax,
                    'seat_basic_fare' => $current_seat_value,
                    'seat_service_tax_amt' => $current_seat_service_tax,
                    'seat_service_tax_perc' => $value['ServiceTax'],
                    // 'op_service_charge_amt' => $value->operatorServiceChargeAbsolute,
                    'op_service_charge_amt' => $current_seat_operator_service_charge,
                    'op_service_charge_perc' => 0, //Not Provided
                    //'row_no' => $value['Row'] + 1,
                    'row_no' => $value['Row'],
                    //'col_no' => $value['Column'] + 1,
                    'col_no' => $value['Column'],
                    'seat_type' => $seat_type,
                    'berth_no' => $berth_no,
                    'pos' => $pos,
                    'zindex' => $berthType,
                    'length' => $length,
                    'width' => $width,
                    'is_ac' => $acFlag,
                    'is_sleeper' => $sleeperFlag,
                    'is_ladies' => $ladiesFlag,
                    'current_seat_type' => $current_seat_type,
                    'horizontal_seat' => $horizontal_seat,
                    'vertical_seat' => $vertical_seat,
                    'seat_discount' => $current_seat_discount,
                    'agent_comm_tds' => $agent_commission['tds_value_on_commission_amount'],
                    'agent_commission' => ($agent_commission['agent_commission_amount'])
                );

                if ($value['Available'] == 'Y' && $value["BlockType"] == 0) {
                    $seat_layout[$berth_no][$pos]["available"] = 'Y';
                    $total_available_seats++;
                } else {
                    $seat_layout[$berth_no][$pos]["available"] = 'N';
                }
                
                $seatAvailable[] = $value['Available'];
                $seat_fare_detail[$berth_no][$current_seat_no] = $seat_layout[$berth_no][$pos];
            }

            $sessKey = 'its_board_drop_for_' . $fields['trip_no'];
            if ($this->session->has_userdata($sessKey)) {
                $params = json_decode($this->session->userdata($sessKey), 1);
                $res = $this->getBoardingDropLocationsByCity($params['company_id'], $params['from_city_id']);
            } else {
                $session_seatlayout_boarding_points = array();
            }


            $seatlayout_boarding_points = array();

            $sessBoardingKey = 'its_boarding_points_for_' . $fields['trip_no'];

            if ($this->session->has_userdata($sessBoardingKey)) {
                $session_seatlayout_boarding_points = $this->session->userdata($sessBoardingKey);
            } else {
                $session_seatlayout_boarding_points = array();
            }

            if (!empty($session_seatlayout_boarding_points)) {
                foreach ($session_seatlayout_boarding_points as $bkey => $bvalue) {
                    $boarding_detail_array = array(
                        "bus_stop_name" => $bvalue['boarding_stop_name'],
                        "boarding_stop_name" => $bvalue['boarding_stop_name'],
                        "arrival_duration" => $bvalue['arrival_duration'],
                        "location" => $bvalue['boarding_stop_name'],
                        "boarding_point_id" => $bvalue['boarding_point_id'],
                        "is_boarding" => "Y", //This is for boarding point
                        "is_alighting" => "N", //This is for dropping point
                        "seq" => $bvalue['seq'],
                    );

                    $seatlayout_boarding_points[] = $boarding_detail_array;
                    $session_seatlayout_boarding_points[$fields['trip_no']][$bvalue['boarding_point_id']] = $boarding_detail_array;
                }

                $this->session->set_userdata('seatlayout_boarding_points', $session_seatlayout_boarding_points);
            } else {
                log_data("infinity/" . date("M") . "/service_" . date("d-m-Y") . "_log.log", "boarding point empty", 'boarding point empty');
            }




            $seatlayout_dropping_points = array();

            $sessDroppingKey = 'its_dropping_points_for_' . $fields['trip_no'];

            if ($this->session->has_userdata($sessDroppingKey)) {
                $session_seatlayout_dropping_points = $this->session->userdata($sessDroppingKey);
            } else {
                $session_seatlayout_dropping_points = array();
            }

            if (!empty($session_seatlayout_boarding_points)) {
                foreach ($session_seatlayout_boarding_points as $bkey => $bvalue) {
                    $dropping_detail_array = array(
                        "bus_stop_name" => $bvalue['bus_stop_name'],
                        "dropping_stop_name" => $bvalue['bus_stop_name'],
                        "arrival_duration" => $bvalue['arrival_duration'],
                        "location" => $bvalue['bus_stop_name'],
                        "dropping_point_id" => $bvalue['dropping_point_id'],
                        "is_boarding" => "N", //This is for boarding point
                        "is_alighting" => "Y", //This is for dropping point
                        "seq" => $bvalue['seq'],
                    );

                    $seatlayout_dropping_points[] = $dropping_detail_array;
                    $session_seatlayout_dropping_points[$fields['trip_no']][$bvalue['dropping_point_id']] = $boarding_detail_array;
                }

                $this->session->set_userdata('seatlayout_dropping_points', $session_seatlayout_boarding_points);
            } else {
                log_data("infinity/" . date("M") . "/service_" . date("d-m-Y") . "_log.log", "boarding point empty", 'boarding point empty');
            }


            // *                      Notice
            // * ITS Provide Seat wise fare so thats why this below code is kept here

            $response['actual_fare'] = implode("/", array_filter($basic_fare_array)); //remove
            $response['actual_fare_array'] = implode("/", array_filter($fare_array)); //remove

            array_walk($fare_array, 'etravelsmarfarediscount_new', $current_seat_discount);
            array_walk($basic_fare_array, 'etravelsmarfarediscount_new', $current_seat_discount);
            $response['fare'] = implode("/", array_filter($basic_fare_array));
            $response['fare_array'] = implode("/", array_filter($fare_array));
            $response['total_seats'] = count($api_response);
            $response['total_available_seats'] = $total_available_seats;
//            $response['tot_rows'] = max($row_array);
//            $response['tot_cols'] = max($column_array);
            $response['tot_rows'] = count($row_array);
            $response['tot_cols'] = count($column_array);
            $response['row_array'] = $ret_row_array;
            $response['column_array'] = $ret_column_array;
            $response['seat_layout'] = $seat_layout;
            $response['seat_fare_detail'] = $seat_fare_detail;
            $response['boarding_points'] = $seatlayout_boarding_points;
            $response['inventory_type'] = 0; //No
            $response['update_travel_destn'] = "";
            $response['stop_detail'] = "";
            $response['its'] = true;
            $response['trip_no'] = $fields['trip_no'];
            $response['flag'] = '@#success#@';
        } else {
            log_data("infinity/" . date("M") . "/service_" . date("d-m-Y") . "_log.log", "Response not received from API i.e. Infinity", 'Seat Layout Response');
            log_data("infinity/" . date("M") . "/service_" . date("d-m-Y") . "_log.log", $api_response, 'Seat Layout Response (Response not received from API i.e. Infinity)');
            $response['flag'] = '@#error#@';
            $response['error'] = "Please contact customer support1.";
            $response['fare'] = 0;
        }

        return $response;
    }

    public function getSeatAvailability($fields) {

        $response = array();
        $its_data = array();

        // *              NOTICE
        // * BELOW MANUPULATION STARTED.
        // * PREVIOUSLY WE ARE STARTED USING DATA FROM SESSION. 
        // * HERE WE ARE FETCHING FROM DATABASE
        $fetched_its_data = array();
        $bus_service_where = array("from" => $fields["from"],
            "to" => $fields["to"],
            "date" => date("Y-m-d", strtotime($fields["date"]))
        );

        $its_data = $this->getItsService($bus_service_where, 'all');

        if (!empty($its_data)) {
            $its_data = json_decode($its_data[0]['bus_service'], 1);

            $fetched_its_data[$fields["trip_no"]] = (array_key_exists($fields["trip_no"], $its_data)) ? $its_data[$fields["trip_no"]] : "";

            if (!empty($fields["is_return_journey"])) {
                $this->session->set_userdata('provider_available_bus_return', $fetched_its_data);
            } else {
                $this->session->set_userdata('provider_available_bus', $fetched_its_data);
            }

            if (!empty($fields['its_reference_number']) && !empty($its_data[$fields['trip_no']]['reference_number']) && !empty($fields['its_reference_number']) && base64_decode($fields['its_reference_number']) == $its_data[$fields["trip_no"]]['reference_number']) {

                $fields['inventory_type'] = $its_data[$fields['trip_no']]["inventory_type"];

                $is_true = false;
                $available_seats = array();
                $available_seat_fare = array();
                $seats_details = array();
                $post_fields = array(
                    "reference_number" => $its_data[$fields["trip_no"]]['reference_number']
                );

                log_data("infinity/" . date("M") . "/seat_layout_" . date("d-m-Y") . "_log.log", $post_fields, 'Seat Layout Request');
                $api_response = $this->getSeatArrangementDetails($post_fields['reference_number']);
                log_data("infinity/" . date("M") . "/seat_layout_" . date("d-m-Y") . "_log.log", $api_response, 'Seat Layout Response');
                if (!empty($api_response)) {
                    $seat_layout_var = $response_decode->seats;
                    $seat_fare_detail = array();
                    $berth_array = array();
                    $fare_array = array();
                    foreach ($api_response as $key => $value) {
                        $current_seat_no = $value['SeatNo'];
                        $ladiesFlag = ((!empty($value['IsLadiesSeat']) && $value['IsLadiesSeat'] == 'Y') ? true : false);
                        $acFlag = ($acFlag[0] == 2 ? true : false );
                        $sleeperFlag = (isset($value['SeatType']) && $value['SeatType'] == 1) ? true : false;
                        $berth_no = (isset($value['UpLowBerth']) && $value['UpLowBerth'] == 'UB' ) ? '2' : '1';
                        if (!in_array($berth_no, $berth_array)) {
                            $berth_array[] = $berth_no;
                        }

                        if (!in_array($value['SeatRate'], $fare_array)) {
                            $fare_array[] = $value['SeatRate'];
                        }

                        if ($value['Available']) {
                            $available_seats[$berth_no][] = $current_seat_no;
                        }

                        //Seat Fare Details
                        $available_seat_fare[$berth_no][$current_seat_no]["total_fare_with_tax"] = $value['SeatRate'];
                        $available_seat_fare[$berth_no][$current_seat_no]["basic_fare"] = $value['BaseFare'];
                        $available_seat_fare[$berth_no][$current_seat_no]["service_tax_per"] = 0; //Not provided
                        $available_seat_fare[$berth_no][$current_seat_no]["service_tax_amount"] = $value['ServiceTax']; //Not provided
                        $available_seat_fare[$berth_no][$current_seat_no]["op_service_charge_per"] = 0; //Not provided
                        $available_seat_fare[$berth_no][$current_seat_no]["op_service_charge_amount"] = 0; //Not provided

                        $seats_details[$berth_no][$current_seat_no] = array(
                            "is_ladies_seat" => $ladiesFlag,
                            "is_ac" => $acFlag,
                            "is_sleeper" => $sleeperFlag
                        );
                    }

                    $response['total_berth'] = max($berth_array);
                    $response['trip_no'] = $fields['trip_no'];

                    // Data required for temp booking 
                    $response["SeatAvailabilityResponse"]["service_id"] = $fields["trip_no"];
                    $response["SeatAvailabilityResponse"]["inventory_type"] = $fields["inventory_type"];
                    $response["SeatAvailabilityResponse"]["reference_number"] = $its_data[$fields["trip_no"]]['reference_number'];

                    $response["SeatAvailabilityResponse"]["fromStopId"] = $its_data[$fields["trip_no"]]["from_stop_id"];
                    $response["SeatAvailabilityResponse"]["toStopId"] = $its_data[$fields["trip_no"]]["to_stop_id"];

                    $response["SeatAvailabilityResponse"]["dateOfJourney"] = str_replace("-", "/", $fields["date"]);
                    $response["SeatAvailabilityResponse"]["fare"] = $fare_array;
                    $response["SeatAvailabilityResponse"]["childFare"] = $fare_array;
                    $response["SeatAvailabilityResponse"]["sleeperFare"] = 0.00;
                    $response["SeatAvailabilityResponse"]['availableSeats']['availableSeat'] = $available_seats;
                    $response["SeatAvailabilityResponse"]['available_seat_fare'] = $available_seat_fare;
                    $response["SeatAvailabilityResponse"]["seats_details"] = $seats_details;

                    $response["error"] = array();


                    $response["trip_data"]["op_id"] = "";
                    $response["trip_data"]["user_type"] = "";
                    $response["trip_data"]["op_trip_no"] = $fields['trip_no'];
                    $response["trip_data"]["inventory_type"] = $fields["inventory_type"];
                    $response["trip_data"]["reference_number"] = $its_data[$fields["trip_no"]]['reference_number'];
                    $response["trip_data"]["is_child_concession"] = ""; //Not provided
                    $response["trip_data"]["id_proof_required"] = ""; //Not provided

                    $response["trip_data"]["sch_departure_tm"] = $its_data[$fields['trip_no']]["sch_departure_tm"];
                    //Not getting arrival date. So considered current date only

                    $departure_his_str = strtotime(date("h:i A", strtotime($its_data[$fields['trip_no']]["sch_departure_tm"])));
                    $arrival_his_str = strtotime(date("h:i A", strtotime($its_data[$fields['trip_no']]["arrival_time"])));

                    if ($departure_his_str < $arrival_his_str) {
                        //Same arrival day
                        $response["trip_data"]["arrival_time"] = date("Y-m-d", strtotime($fields['date'])) . " " . date("H:i:s", strtotime($its_data[$fields['trip_no']]["arrival_time"]));
                    } else if (($departure_his_str > $arrival_his_str) || ($departure_his_str == $arrival_his_str)) {
                        //Add one day to arrival date 
                        $response["trip_data"]["arrival_time"] = date("Y-m-d", strtotime($fields['date'] . ' +1 day')) . " " . date("H:i:s", strtotime($its_data[$fields['trip_no']]["arrival_time"]));
                    }


                    $seat_already_booked = array();

                    if (isset($response["total_berth"]) && $response["total_berth"] != "") {
                        for ($i = 1; $i <= $response["total_berth"]; $i++) {
                            if (!empty($fields['seats'][$i]) && !empty($available_seats[$i])) {
                                if (count(array_intersect($fields['seats'][$i], $available_seats[$i])) === count($fields['seats'][$i])) {
                                    $is_true = true;
                                } else {
                                    $is_true = false;
                                    $seat_already_booked = array_diff($fields['seats'][$i], $available_seats[$i]);
                                    break;
                                }
                            }
                        }
                    }

                    if ($is_true) {
                        $response['flag'] = 'success';
                    } else {
                        $response['flag'] = 'error';
                        $response['seat_already_booked'] = $seat_already_booked;
                        load_front_view(SEAT_UNAVAILABLE, $response);
                    }
                } else {
                    $response['flag'] = 'error';
                    $response['error'] = "Please contact customer support.";
                }
            } else {
                $response['flag'] = 'error';
                $response['error'] = "Please contact customer support.";
            }
        } else {

            $get_service_again = array(
                "from" => $fields["from"],
                "to" => $fields["to"],
                "date" => date("Y-m-d", strtotime($fields["date"])),
                "date_of_jour" => date("Y-m-d", strtotime($fields["date"])),
                "getSeatAvailability_fields" => $fields
            );

            $busdata = $this->getAvailableBuses($get_service_again, true);

            $response['flag'] = 'error';
            $response['error'] = "Please contact customer support.";
        }

        return $response;
    }

    public function do_temp_booking($fields) {
        

        $input = $fields["input"];
        $seats = $fields["input"]["seat"];
        $email = $input["email_id"];
        $mobile_no = $input["mobile_no"];
        $agent_email = $input["agent_email_id"];
        $agent_mobile_no = $input["agent_mobile_no"];
        $available_detail = $fields["available_detail"];
        $available_seat_fare = $fields["available_detail"]["available_seat_fare"];
        $seatlayout_boarding_points = $this->session->userdata('seatlayout_boarding_points');

        if (isset($input["is_return_journey"]) && $input["is_return_journey"]) {
            $its_available_bus = $this->session->userdata('provider_available_bus_return');
        } else {
            $its_available_bus = $this->session->userdata('provider_available_bus');
        }
        $service_detail = $its_available_bus[$available_detail["service_id"]];


        $tot_seat = 0;
        foreach ($seats as $berth => $seatvalue) {
            $tot_seat += count($seatvalue);

            //below code is to check if discount applicable
            
        }

        $dept_boarding_time = $available_detail["date"] . " " . $available_detail["sch_dept_time"];
        $dept_boarding_time = date("Y-m-d H:i:s", strtotime($dept_boarding_time));

        log_data("infinity_trash/" . date("M") . "/service_" . date("d-m-Y") . "_log.log", $available_detail, 'Temp Booking available_detail');
        log_data("infinity_trash/" . date("M") . "/service_" . date("d-m-Y") . "_log.log", $service_detail, 'Temp Booking service_detail');

        if (isset($available_detail["inventory_type"])) {
            if ($available_detail["inventory_type"] == 0 || $available_detail["inventory_type"] == 1) {
                $boarding_stop_time = date("Y-m-d", strtotime($available_detail["date"])) . " " . date("H:i:s", strtotime($service_detail["barr"][$available_detail["bsd"]]["arrival_duration"]));
            } else if ($available_detail["inventory_type"] > 1) {
                $boarding_point_detail = $seatlayout_boarding_points[$available_detail["service_id"]][$available_detail["bsd"]];
                $boarding_stop_time = date("Y-m-d", strtotime($available_detail["date"])) . " " . date("H:i:s", strtotime($boarding_point_detail["arrival_duration"]));
            }

            // * ************** Assumption For Dropping Stop Time Start **********************
            $dropping_stop_time = "";

            if (isset($available_detail["dsd"]) && $available_detail["dsd"] != "") {
                $departure_his_str = strtotime($service_detail["sch_departure_tm"]);
                $arrival_his_str = strtotime($service_detail["darr"][$available_detail["dsd"]]["arrival_duration"]);
                if ($departure_his_str < $arrival_his_str) {
                    //Same arrival day
                    $dropping_stop_time = date("Y-m-d", strtotime($service_detail['departure_datetime'])) . " " . date("H:i:s", $arrival_his_str);
                } else if (($departure_his_str > $arrival_his_str) || ($departure_his_str == $arrival_his_str)) {
                    //Add one day to arrival date 
                    $dropping_stop_time = date("Y-m-d", strtotime($service_detail['departure_datetime'] . ' +1 day')) . " " . date("H:i:s", $arrival_his_str);
                }
                // * **************  Assumption For Dropping Stop Time End ********************

                if ($dropping_stop_time == "") {
                    $dropping_stop_time = $available_detail['arrival_time'];
                }
            } else {
                $dropping_stop_time = $available_detail['arrival_time'];
            }
            //$this->db->trans_begin();
            $this->tickets_model->ticket_type = 1; // For In future
            $this->tickets_model->request_type = 'B';
            $this->tickets_model->ticket_status = 'A';
            $this->tickets_model->bus_service_no = "";
            $this->tickets_model->dept_time = $dept_boarding_time;
            $this->tickets_model->boarding_time = $boarding_stop_time;
            $this->tickets_model->alighting_time = $available_detail['arrival_time'];
            $this->tickets_model->droping_time = $dropping_stop_time;
            $this->tickets_model->from_stop_cd = "";
            $this->tickets_model->till_stop_cd = "";
            $this->tickets_model->from_stop_name = $available_detail['from'];
            $this->tickets_model->till_stop_name = $available_detail['to'];

            $this->tickets_model->boarding_stop_name = $available_detail['boarding_stop_name'];
            $this->tickets_model->destination_stop_name = $available_detail['destination_stop_name'];
            $this->tickets_model->boarding_stop_cd = $available_detail['boarding_stop_cd'];
            $this->tickets_model->destination_stop_cd = $available_detail['destination_stop_cd'];

            $this->tickets_model->op_id = $available_detail['op_id'];
            $this->tickets_model->provider_id = $available_detail['provider_id'];
            $this->tickets_model->provider_type = $available_detail['provider_type'];
            $this->tickets_model->booked_from = "infinity";
            $this->tickets_model->inventory_type = $service_detail['inventory_type'];
            $this->tickets_model->route_schedule_id = $available_detail['service_id'];
            $this->tickets_model->commision_percentage = $service_detail['commision_percentage'];
            $this->tickets_model->partial_cancellation = ($service_detail['partial_cancellation_allowed']) ? 1 : 0;
            $this->tickets_model->mticket = ($service_detail['mticket_allowed']) ? 1 : 0;

            $this->tickets_model->num_passgr = $tot_seat;
            $this->tickets_model->cancellation_rate = 0;
            $this->tickets_model->issue_time = date("Y-m-d H:i:s");
            $this->tickets_model->pay_id = 1;
            $this->tickets_model->session_no = 1;
            $this->tickets_model->op_name = $available_detail['op_name'];
            $this->tickets_model->user_email_id = $email;
            $this->tickets_model->mobile_no = $mobile_no;
            $this->tickets_model->agent_email_id = $agent_email;
            $this->tickets_model->agent_mobile_no = $agent_mobile_no;

            $this->tickets_model->id_proof_required = $available_detail['id_proof_required'];

            $this->tickets_model->inserted_by = $this->session->userdata("booking_user_id");
            $this->tickets_model->role_id = (!empty($this->session->userdata("role_id"))) ? $this->session->userdata("role_id") : 0;
            $this->tickets_model->booked_by = (!empty($this->session->userdata("role_name"))) ? strtolower($this->session->userdata("role_name")) : "guest_user";

            // $this->tickets_model->bus_type = $available_detail['bus_type']['op_bus_type_name'];
            $this->tickets_model->bus_type = (isset($available_detail['bus_type']['op_bus_type_name'])) ? $available_detail['bus_type']['op_bus_type_name'] : "";
            $this->tickets_model->status = 'Y';
            $this->tickets_model->ip_address = $this->input->ip_address();
            $this->tickets_model->tds_per = $this->config->item('tds');
            $ticket_id = $this->tickets_model->save();
            $total_ticket_discount = 0;
            $total_service_tax_amount = 0;
            $total_basic_fare = 0;
            $total_op_service_charge_amount = 0;
            $tickets_total_op_service_charge_amount = 0;
            $tickets_total_service_tax = 0;
            $tot_fare = 0;
            if ($ticket_id > 0) {
                $psgr_no = 1;
                foreach ($seats as $berth_no => $seat_detail) {
                    foreach ($seat_detail as $cseat_no => $value) {
                        $seatfare = 0;
                        $fare_detail_tax = $available_seat_fare[$berth_no][$cseat_no];
                        $seatfare = $fare_detail_tax["total_fare_with_tax"];

                        $discount_array = array();


                        

                        $tickets_total_op_service_charge_amount += $fare_detail_tax["op_service_charge_amount"];
                        $tickets_total_service_tax += $fare_detail_tax["service_tax_amount"];


                        // *                  NOTICE : VERY IMP LOGIC FOR FARE
                        // * if is_child_concession in op_bus_types is Y then assign child seat fare
                        // * if is_child_concession in op_bus_types is N then assign adult seat fare 

                        /* if(isset($available_detail['is_child_concession']) && $available_detail['is_child_concession'] == "Y") {
                          $seatfare = ($value['age'] <= 12) ? $child_seat_fare : $seatfare;
                          }
                          else {
                          $seatfare = $fare;
                          } */

                        //calculate commission amount
                        $op_commission_type = 'percentage';
                        $op_commission_per = $service_detail['commision_percentage'];
                        $op_commission = round(($fare_detail_tax["basic_fare"] * (float)($service_detail['commision_percentage']/100)),5);
                        $tds_on_commission = round($op_commission * ((float)($this->config->item('tds')/100)),5);
                        $final_commission = $op_commission - $tds_on_commission;

                        $tot_fare += $seatfare;
                        $total_basic_fare += $fare_detail_tax["basic_fare"];

                        $this->ticket_details_model->ticket_id = $ticket_id;
                        $this->ticket_details_model->psgr_no = $psgr_no;
                        $this->ticket_details_model->psgr_name = $value['name'];
                        $this->ticket_details_model->psgr_age = $value['age'];
                        $this->ticket_details_model->psgr_sex = $value['gender'];

                        // $this->ticket_details_model->psgr_type = ($value['age'] <= 12) ? "C" : "A";
                        $this->ticket_details_model->psgr_type = "A";
                        $this->ticket_details_model->concession_cd = 1; // if exists /in case of rsrtc 60 pass ct 
                        $this->ticket_details_model->concession_rate = 1; // from api /if exists /in case of rsrtc 60 pass ct 
                        $this->ticket_details_model->discount = 1; // if available from api
                        $this->ticket_details_model->is_home_state_only = 1; // Y or No //not required now

                        $this->ticket_details_model->adult_basic_fare = $fare_detail_tax["basic_fare"];
                        $this->ticket_details_model->child_basic_fare = 0.00;
                        $this->ticket_details_model->fare_acc = 0.00;
                        $this->ticket_details_model->fare_hr = 0.00;
                        $this->ticket_details_model->fare_it = 0.00;
                        $this->ticket_details_model->fare_octroi = 0.00;
                        $this->ticket_details_model->fare_other = 0.00;
                        $this->ticket_details_model->fare_reservationCharge = 0.00;
                        $this->ticket_details_model->fare_sleeperCharge = 0.00;
                        $this->ticket_details_model->fare_toll = 0.00;
                        $this->ticket_details_model->fare_convey_charge = 0.00;


                        $this->ticket_details_model->service_tax_per = $fare_detail_tax["service_tax_per"];
                        $this->ticket_details_model->service_tax = $fare_detail_tax["service_tax_amount"];
                        $this->ticket_details_model->op_service_charge_per = $fare_detail_tax["op_service_charge_per"];
                        $this->ticket_details_model->op_service_charge = $fare_detail_tax["op_service_charge_amount"];


                        $this->ticket_details_model->fare_amt = $seatfare;
                        $this->ticket_details_model->seat_no = $cseat_no;
                        $this->ticket_details_model->berth_no = $berth_no;
                        $this->ticket_details_model->seat_status = 'Y';
                        $this->ticket_details_model->concession_proff = 1;  // if cct then required // voter id
                        $this->ticket_details_model->proff_detail = 1; // if varified
                        //$this->ticket_details_model->identify_proof = 1; // not required
                        $this->ticket_details_model->status = 'Y';
                        $this->ticket_details_model->type = $op_commission_type;
                        $this->ticket_details_model->commission_percent = $op_commission_per;
                        $this->ticket_details_model->op_commission =  $op_commission;
                        $this->ticket_details_model->tds_on_commission =  $tds_on_commission;
                        $this->ticket_details_model->final_commission =  $final_commission;
                        $psgr_no++;

                        $ticket_details_id = $this->ticket_details_model->save();
                        
                    }
                }
                //if id proof required
                if ($input['id_type'] != "" && $input['id_card_number'] != "" && $input['name_on_id'] != "") {
                    $idproof_save = array(
                        'ticket_id' => $ticket_id,
                        'id_type' => $input['id_type'],
                        'id_number' => $input['id_card_number'],
                        'name' => $input['name_on_id']
                    );
                    $ticket_idproof_id = $this->ticket_id_proof_model->insert($idproof_save);
                }
                $fields["ticket_id"] = $ticket_id;
                $data['ticket_id'] = $ticket_id;
                $tb_data = $this->blockTicket($fields);
                // show($tb_data,1,"tb_data");
                if (!empty($tb_data)) {
                    $temp_booking_status = $tb_data['status'];
                    $is_success = $tb_data['is_success'];

                    if (!$is_success && $temp_booking_status == 'error') {
                        $data['block_key'] = "";
                        $data['status'] = $tb_data['status'];
                        $data['error'] = $tb_data['error_message'];
                        $data['payment_confirm_booking_status'] = "error";
                    } else if ($is_success && $temp_booking_status == 'success') {
                        $total_convey_charge = 0;
                        $extra_charges_value = 0;
                        $convey_charge_name = "";
                        $convey_charge_value = 0;
                        $total_fare_without_discount = $tot_fare + $extra_charges_value;

                        $data['discount_value'] = $total_ticket_discount;
                        $final_total_amount = $total_fare_without_discount - $total_ticket_discount;
                        // * *****temp discount logic end*****

                        $tax_charges = array();

                        $data['status'] = $temp_booking_status;
                        $this->tickets_model->ticket_id = $ticket_id;
                        $this->tickets_model->ticket_ref_no = $tb_data['blockKey'];
                        $this->tickets_model->pickup_address = $tb_data['boarding_point']['location'];

                        $this->tickets_model->total_basic_amount = $total_basic_fare;
                        $this->tickets_model->op_service_charge = $tickets_total_op_service_charge_amount;
                        $this->tickets_model->service_tax = $tickets_total_service_tax;

                        $this->tickets_model->tot_fare_amt = $tot_fare;
                        $this->tickets_model->tot_fare_amt_with_tax = $final_total_amount;
                        $this->tickets_model->total_fare_without_discount = $total_fare_without_discount;

                        $this->tickets_model->transaction_status = "temp_booked";
                        $this->tickets_model->save();
                        
                        /*
                         * NOTICE: MOST IMP
                         *
                         * DISCUSS THIS POINT.
                         * API IS NOT PROVIDING BLOCK TIME AFTER SEAT BLOCK.
                         * AFTER DISCUSSION REMOVE HARDCODED NUMBER 5.
                         */
                        $tb_data['blockTime'] = 2;

                        $currentTime = strtotime(date("Y-m-d H:i:s"));
                        $timeBlock = $currentTime + (60 * $tb_data['blockTime']);
                        $timeToBlock = date("Y-m-d H:i:s", $timeBlock);

                        $data['block_key'] = $tb_data['blockKey'];
                        $data['block_time_minutes'] = $tb_data['blockTime'];
                        $data['block_current_time'] = $currentTime;
                        $data['time_to_block'] = $timeToBlock;
                        $data['ttb_year'] = date("Y", $timeBlock);
                        $data['ttb_month'] = date("m", $timeBlock);
                        $data['ttb_day'] = date("d", $timeBlock);
                        $data['ttb_hours'] = date("H", $timeBlock);
                        $data['ttb_minutes'] = date("i", $timeBlock);
                        $data['ttb_seconds'] = date("s", $timeBlock);
                        $data['total_fare'] = $final_total_amount;
                        $data["total_fare_without_discount"] = $total_fare_without_discount;
                        $data["ticket_fare"] = $tot_fare;
                        $data["total_seats"] = $tot_seat;
                        $data["tax_charges"] = $tax_charges;
                        $data['payment_confirm_booking_status'] = "success";
                        $data['ticket_id'] = $ticket_id;
                    }
                } else {
                    $data['payment_confirm_booking_status'] = "error";
                }
            } else {
                $data['payment_confirm_booking_status'] = "error";
            }
        } else {
            log_data("its_trash/" . date("M") . "/service_" . date("d-m-Y") . "_log.log", "Inventory type error", 'Temp Booking inventory type');
            $data['payment_confirm_booking_status'] = "error";
        }
        return $data;
    }

    public function blockTicket($fields) {
        $data = array();
        $boardingPoint = array();
        $available_detail = $fields["available_detail"];
        $seats_details = $available_detail["seats_details"];
        $available_seat_fare = $fields["available_detail"]["available_seat_fare"];
        $binput = $fields["input"];
        $lowerBerth = 1;
        $upperBerth = 2;
        $ticket_id = $fields['ticket_id'];

        if (isset($binput["is_return_journey"]) && $binput["is_return_journey"]) {
            $its_available_bus = $this->session->userdata('provider_available_bus_return');
        } else {
            $its_available_bus = $this->session->userdata('provider_available_bus');
        }

        if (!empty($this->rediscache->get($available_detail['from'] . $available_detail['to'] . $available_detail['date']))) {
            $its_available_bus = $this->rediscache->get($available_detail['from'] . $available_detail['to'] . $available_detail['date']);
        }

        if (!empty($available_detail["service_id"]) && !empty($its_available_bus[$available_detail["service_id"]])) {
            $service_detail = $its_available_bus[$available_detail["service_id"]];
            if (!empty($service_detail["barr"][$available_detail["bsd"]])) {
                $boarding_point_detail = $service_detail["barr"][$available_detail["bsd"]];

                $boardingPoint = array(
                    "id" => $boarding_point_detail["boarding_point_id"],
                    "location" => $boarding_point_detail["location"],
                    "time" => $boarding_point_detail["arrival_duration"]
                );
            } else {
                $boardingPoint = array(
                    "id" => "",
                    "location" => "",
                    "time" => ""
                );
            }
        } else {
            $boardingPoint = array(
                "id" => "",
                "location" => "",
                "time" => ""
            );
        }


        //Check boarding 
        if ($available_detail['bsd'] !== '' && !empty($fields['input']['email_id']) && !empty($fields['input']['mobile_no'])) {
            $email = $fields['input']['email_id'];
            $phone = $fields['input']['mobile_no'];
            $pickupId = $available_detail['bsd'];
            //check seats information 
            if ((!empty($fields['input']['seat'][$lowerBerth]) || !empty($fields['input']['seat'][$upperBerth]))) {
                $referenceNumber = !empty($available_detail["its_reference_number"] && !empty(base64_decode($available_detail["its_reference_number"]))) ? base64_decode($available_detail["its_reference_number"]) : '';
                //Check Reference Number
                if ($referenceNumber !== '') {
                    $is_primary = true;
                    $totalFare = 0;
                    $totalPassengers = 0;
                    $blockSeats = '';
                    if (!empty($fields['input']['seat'][$lowerBerth])) {
                        foreach ($fields['input']['seat'][$lowerBerth] as $seatNo => $seat_detail) {
                            $total_fare += $available_seat_fare[$lowerBerth][$seatNo]["total_fare_with_tax"];
                            $totalPassengers++;
                            if ($blockSeats !== '') {
                                $blockSeats .= "|";
                            }
                            $blockSeats .= $seatNo . "," . $seat_detail["gender"];
                            $passengerName = $seat_detail["name"];
                        }
                    }

                    if (!empty($fields['input']['seat'][$upperBerth])) {
                        foreach ($fields['input']['seat'][$upperBerth] as $seatNo => $seat_detail) {
                            $total_fare += $available_seat_fare[$upperBerth][$seatNo]["total_fare_with_tax"];
                            $totalPassengers++;
                            if ($blockSeats !== '') {
                                $blockSeats .= "|";
                            }
                            $blockSeats .= $seatNo . "," . $seat_detail["gender"];
                            $passengerName = $seat_detail["name"];
                        }
                    }
                    //  echo $blockSeats;exit;
                    //Check params to block seat
                    if ($blockSeats !== '') {
                        $result = array();
                        $params['reference_number'] = $referenceNumber;
                        $params['passenger_name'] = $passengerName;
                        $params['seat_number_gender'] = $blockSeats;
                        $params['email'] = $email;
                        $params['phone'] = $phone;
                        $params['pickup_id'] = $pickupId;
                        $params['payable_amount'] = $total_fare;
                        $params['total_passengers'] = $totalPassengers;
                        $params['ticket_id'] = $ticket_id;

                        $this->session->set_userdata('its_confirm_booking_params', $params);
                        $lastId = $this->saveUpdateTicketMetadata(array('ticket_id' => $ticket_id, 'temp_booking_request' => json_encode($params)), 'add');
                        log_data("infinity/" . date("M") . "/service_" . date("d-m-Y") . "_log.log", $params, 'Block Seat Params');
                        $result = $this->blockSeatV2($params);
                        log_data("infinity/" . date("M") . "/service_" . date("d-m-Y") . "_log.log", $result, 'Block Seat Result');
                        if (isset($result)) {
                            if ($result['Status'] == 1 && !empty($result['BlockID'])) {
                                $data["is_success"] = true;
                                $data["status"] = 'success';
                                $data["blockKey"] = $result['BlockID'];
                            } else {
                                $data["is_success"] = false;
                                $data["status"] = "error";
                                $data["error_message"] = "Error code :" . $result['Status'] . " Message :" . $result['Message'];
                                log_data("infinity/" . date("M") . "/service_" . date("d-m-Y") . "_log.log", "Response from Infinity : " . $data["error_message"], 'Error log');
                            }
                        } else {
                            log_data("infinity/" . date("M") . "/service_" . date("d-m-Y") . "_log.log", "Response not found from Infinity", 'Error log');
                            $data["is_success"] = false;
                            $data["status"] = "error";
                            $data["error_message"] = "Response not found from Infinity";
                        }
                        if (!empty($lastId)) {
                            $this->saveUpdateTicketMetadata(array('id' => $lastId, 'response' => json_encode((!empty($result) ? $result : array()))));
                        }
                    } else {
                        log_data("infinity/" . date("M") . "/service_" . date("d-m-Y") . "_log.log", "Essential Data missing", 'Error log');
                        $data["is_success"] = false;
                        $data["status"] = "error";
                        $data["error_message"] = "Essential Data missing";
                    }
                } else {
                    log_data("infinity/" . date("M") . "/service_" . date("d-m-Y") . "_log.log", "Empty reference number", 'Error log');
                    $data["is_success"] = false;
                    $data["status"] = "error";
                    $data["error_message"] = "Empty reference number";
                }
            } else {
                log_data("infinity/" . date("M") . "/service_" . date("d-m-Y") . "_log.log", "Empty seat", 'Error log');
                $data["is_success"] = false;
                $data["status"] = "error";
                $data["error_message"] = "Empty seat";
            }
        } else {
            log_data("infinity/" . date("M") . "/service_" . date("d-m-Y") . "_log.log", "Empty boarding sotp Id or Mobile or eamil", 'Error');
            $data["is_success"] = false;
            $data["status"] = "error";
            $data["error_message"] = "Empty boarding stop Id or Mobile or eamil";
        }


        $data["boarding_point"] = $boardingPoint;
        //remove comment
        //log_data("infinity/" . date("M") . "/service_" . date("d-m-Y") . "_log.log", $boardingPoint, 'Reference');
        return $data;
    }

    function final_booking($api_data, $data) {
        $final_response = array();
        $tbdata = $this->doconfirm_booking(array('ticket_id' => $api_data['pt_data']->ticket_id));
        // * *********** Actual ticket booking End Here1 **********
        if (trim(strtolower($tbdata['status'])) == 'success') {
            $onwards_ticket_ref_no = $data["ticket_ref_no"];
            $data["ticket_pnr"] = $tbdata["pnr"];
            // * This is to update ticket table
            $transaction = array('transaction_status' => "success",
                'payment_by' => $api_data["payment_by"],
                'payment_mode' => $data["pg_response"]["payment_mode"],
                'commision_percentage' => (isset($tbdata['commision_percentage'])) ? $tbdata['commision_percentage'] : "",
                'api_ticket_no' => (isset($tbdata['api_ticket_no'])) ? $tbdata['api_ticket_no'] : "",
                'cancellation_policy' => (isset($tbdata['cancellation_policy'])) ? $tbdata['cancellation_policy'] : ""
            );
            $this->tickets_model->update($data["ticket_id"], $transaction);
            $pg_success_response = $this->common_model->transaction_process_api($data);
            // * For Reconcellation process 
            $this->saveReconcellations($data["ticket_pnr"], $data["ticket_id"], "success");

            $final_response["transaction_process_response"] = $pg_success_response;
            //make this function common afterwards. Because this is used 2 times (1 times in pg and 1 time in wallet)
            if (trim($pg_success_response["msg_type"]) == "success") {
                //To check if return journey
                // $rpt_data = $this->return_ticket_mapping_model->get_rpt_mapping($data["pg_response"]["udf2"]);
                $rpt_data = $api_data["rpt_data"];
                if (count($rpt_data)) {
                    $onward_ticket_id = $pg_success_response["ticket_id"];

                    //to track return journey.
                    $rtransaction = array('is_return_journey' => "1",
                        'return_ticket_id' => $rpt_data[0]->ticket_id
                    );

                    $this->tickets_model->update($onward_ticket_id, $rtransaction);

                    // * *********** Actual ticket booking Start Here **********

                    $rtbdata = $this->doconfirm_booking(array('ticket_id' => $rpt_data[0]->ticket_id));

                    // * *********** Actual ticket booking End Here ********** */

                    if (trim($rtbdata['status']) == 'success') {
                        $data["ticket_ref_no"] = $rpt_data[0]->ticket_ref_no;
                        $data["ticket_pnr"] = $rtbdata["pnr"];
                        $data["ticket_id"] = $rpt_data[0]->ticket_id;
                        $data["is_return_journey"] = true;


                        // * This is to update ticket table

                        $atransaction = array('transaction_status' => "success",
                            'payment_by' => $api_data["payment_by"],
                            'payment_mode' => $data["pg_response"]["payment_mode"],
                            'commision_percentage' => (isset($rtbdata['commision_percentage'])) ? $rtbdata['commision_percentage'] : "",
                            'api_ticket_no' => (isset($rtbdata['api_ticket_no'])) ? $rtbdata['api_ticket_no'] : "",
                            'cancellation_policy' => (isset($rtbdata['cancellation_policy'])) ? $tbdata['cancellation_policy'] : ""
                        );

                        $this->tickets_model->update($data["ticket_id"], $atransaction);

                        $final_response["return_ticket_status"] = "success";

                        $rpg_success_response = $this->common_model->transaction_process_api($data);

                        // * For Reconcellation process 
                        $this->saveReconcellations($data["ticket_pnr"], $data["ticket_id"], "success");

                        $final_response["transaction_process_response_return"] = $rpg_success_response;

                        if (trim($rpg_success_response["msg_type"]) == "success") {
                            if (isset($data['is_back']) && $data['is_back']) {
                                $url_redirect = base_url() . "admin/reservation/view_ticket/" . $onwards_ticket_ref_no . "/" . $rpt_data[0]->ticket_ref_no;
                            } else {
                                $url_redirect = base_url() . "front/booking/view_ticket/" . $onwards_ticket_ref_no . "/" . $rpt_data[0]->ticket_ref_no;
                            }

                            $final_response["status"] = "success";
                            $final_response["redirect_url"] = $url_redirect;
                        } else if (trim($rpg_success_response["msg_type"]) == "error") {
                            $transaction = array(
                                'failed_reason' => isset($rpg_success_response["msg"]) ? $rpg_success_response["msg"] : "some error occured in ets library",
                                'payment_by' => $api_data["payment_by"]
                            );

                            $id = $this->tickets_model->update($rpt_data[0]->ticket_id, $transaction);

                            $final_response["status"] = "error";
                            $final_response["return_transaction_process_api"] = "error";
                            $final_response["data"] = $data;
                        }
                    } else {
                        if ($rpt_data[0]->transaction_status != "success") {
                            $api_failed_reason = (isset($rtbdata["failed_reason"])) ? $rtbdata["failed_reason"] : "Its return ticket api failed";
                            $transaction = array('transaction_status' => "failed",
                                'failed_reason' => $api_failed_reason,
                                'payment_by' => $api_data["payment_by"]
                            );

                            $return_ticket_update_where = ["ticket_id" => $rpt_data[0]->ticket_id, "transaction_status != " => "success"];
                            $id = $this->tickets_model->update($return_ticket_update_where, $transaction);
                        } else {
                            $this->common_model->developer_confirm_booking_error_mail("ETS (Return Journey)", $rpt_data[0]->ticket_id);
                        }

                        $final_response["return_ticket_status"] = "error";
                        $final_response["return_ticket_api_failed_reason"] = $api_failed_reason;

                        $final_response["status"] = "success";
                        $final_response["redirect_url"] = $pg_success_response["redirect_url"];
                    }
                } else {
                    $final_response["status"] = "success";
                    $final_response["redirect_url"] = $pg_success_response["redirect_url"];
                }
            } else if (trim($pg_success_response["msg_type"]) == "error") {
                $transaction = array(/* 'transaction_status' => "failed", */
                    'failed_reason' => 'failed message - ' . (isset($pg_success_response["msg"])) ? $pg_success_response["msg"] : "",
                    'payment_by' => $api_data["payment_by"]
                );

                $id = $this->tickets_model->update($api_data['ticket_id'], $transaction);
                $final_response["status"] = "success";
                $final_response["redirect_url"] = $pg_success_response["redirect_url"];

                log_data("condition_not_handled/" . date("M") . "/ets_" . date("d-m-Y") . "_log.log", "Ticket booked successfully but some error occured in transaction_process_api for ticket is " . $data["ticket_id"], 'Transaction Process Status');
            }
        } else {
            if ($api_data['pt_data']->transaction_status != "success") {
                $api_failed_reason = (isset($tbdata["failed_reason"])) ? $tbdata["failed_reason"] : "Its api failed";
                $transaction = array('transaction_status' => "failed",
                    'failed_reason' => $api_failed_reason,
                    'payment_by' => $api_data["payment_by"]
                );

                $update_where = ["ticket_id" => $api_data['pt_data']->ticket_id, "transaction_status != " => "success"];
                $id = $this->tickets_model->update($update_where, $transaction);

                $find_return_ticket = $this->return_ticket_mapping_model->where("ticket_id", $api_data['pt_data']->ticket_id)->find_all();
                if ($find_return_ticket) {
                    $return_update_where = ["ticket_id" => $find_return_ticket[0]->return_ticket_id, "transaction_status != " => "success"];
                    $rid = $this->tickets_model->update($return_update_where, $transaction);
                }
            } else {
                $this->common_model->developer_confirm_booking_error_mail("Its", $api_data['pt_data']->ticket_id);
            }

            $final_response["status"] = "error";
            $final_response["api_failed_reason"] = $api_failed_reason;
            $final_response["data"] = $data;
        }

        log_data("infinity/" . date("M") . "/service_" . date("d-m-Y") . "_log.log", $final_response, 'Final Booking response');
        return $final_response;
    }

    public function doconfirm_booking($fields) {
        $data = array();
        //Get api parameter from ticket_metadata
        $meta_array = array('fields' => array('temp_booking_request'), 'ticket_id' => $fields['ticket_id']);
        $tempData = $this->getTicketMetaData($meta_array);
        $params = array();
        if (!empty($tempData[0]['temp_booking_request'])) {
            $params = json_decode($tempData[0]['temp_booking_request'], 1);

            log_data("infinity/" . date("M") . "/service_" . date("d-m-Y") . "_log.log", $params, 'Do Confirm booking Params or Request');

            $response = $this->bookSeat($params);
            log_data("infinity/" . date("M") . "/service_" . date("d-m-Y") . "_log.log", $response, 'Do Confirm booking Response');

            $data_to_log = [
                    "ticket_id" => isset($fields["ticket_id"]) ? $fields["ticket_id"] : "",
                    "request" => json_encode($params),
                    "response" => json_encode($response),
                    "type" => "confirm_booking"
                   ];

            insert_request_response_log($data_to_log);
            if (!empty($response) && !empty($response['Status'])) {

                if ($response['Status'] == '1') {
                    $data["status"] = 'success';
                    $data["pnr"] = $response['PNRNO'];
                    $data["api_ticket_no"] = $response['PNRNO'];
                    $data["commision_percentage"] = ITS_COMMISSION;
                    $data["cancellation_policy"] = '';
                } else {
                    $data["status"] = 'Error';
                    $data["failed_reason"] = 'Error code : ' . $response['Status'];
                }
            } else {
                $data["status"] = 'Error';
                $data["failed_reason"] = 'No Response from api';
            }
        } else {
            $data["status"] = 'Error';
            $data["failed_reason"] = 'No Request params found in metadata table';

            log_data("infinity/" . date("M") . "/service_" . date("d-m-Y") . "_log.log", array('function_input' => $fields, 'meta_query_response' => $tempData), 'No record found in ticket_metadata');
        }
        return $data;
    }

    private function saveUpdateTicketMetadata($data, $flag = '') {
        if ($flag == 'add') {
            $infinity_request_save = array(
                'ticket_id' => $data['ticket_id'],
                'temp_booking_request' => $data['temp_booking_request'],
            );
            $this->db->insert('ticket_metadata', $infinity_request_save);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        } else {
            $this->db->update('ticket_metadata', array("temp_booking_response" => $data['response']), array('id' => $data['id']));
            return true;
        }
    }

    private function getTicketMetaData($data) {
        $fields = "*";
        $where = "";
        if (!empty($data['fields'])) {
            $fields = implode(',', $data['fields']);
        }
        if (!empty($data['ticket_id'])) {
            $where = " WHERE ticket_id = '" . $data['ticket_id'] . "'";
        }

        $s = "SELECT " . $fields . " FROM  ticket_metadata " . $where;
        $q = $this->db->query($s);
        $r = $q->result_array();
        return $r;
    }

    /* public function getTicketByPnrNumber($pnrNo = '') {
      log_data("infinity/".date("M")."/reconciliation_".date("d-m-Y")."_log.log",$fields, 'Bus Reconciliation Request - getTicketByPnrNumber');


      $response = $this->bookSeatDetails($pnrNo);
      log_data("infinity/" . date("M") . "/reconciliation_" . date("d-m-Y") . "_log.log", $response, 'Bus Reconciliation Response - getTicketByPnrNumber');


      if (!empty($response['Status']) && $response['Status'] == "Booked") {
      return $response_decode;
      }
      else {
      return "";
      }
      } */

    public function saveReconcellations($pnrNo = "", $ticket_id = "", $action = "success") {
        if ($pnrNo != "" && $ticket_id != "") {
            $response = $this->bookSeatDetails($pnrNo);

            $ticket_status = "travelled";
            if ($action == "cancel") {
                $ticket_status = "cancelled";
            }

            $where_condition = ["ticket_number" => $pnrNo, "ticket_status" => $ticket_status];

            $check_if_exists = $this->tickets_additional_data_model->where($where_condition)->find_all();
            if (!$check_if_exists) {
                if ($response) {
                    /* $array_to_insert = [
                      "ticket_id" => $ticket_id,
                      "ticket_number" => $ets_ticket_number,
                      "actual_amount" => $response->actualAmount,
                      "total_amount" => $response->totalFareWithTaxes,
                      "paid_amount" => $response->paidAmount,
                      "tds_charges" => $response->tdsCharge,
                      "total_discount_amount" => $response->totalDiscountAmount,
                      "commission_percentage" => $response->commPCT,
                      "refund_amount" => $response->refundAmount,
                      "ticket_status" => strtolower($response->ticketStatus),
                      "insurance_amount" => $response->insuranceAmount,
                      "traveller_contact" => $response->serviceProviderContact,
                      "response" => json_encode($response)
                      ];
                      $this->tickets_additional_data_model->insert($array_to_insert); */
                }
            }
        }
    }

    public function is_ticket_cancellable($fields) {
        //API - This is cancelTicketConfirmation Method (First method)
        $data = array();

        if (is_array($fields['seat_no']) && !empty($fields['seat_no'])) {
            $seat_array = $fields['seat_no'];
        } else if ($fields['seat_no'] != "") {
            $seat_array = explode(",", $fields['seat_no']);
        }

        if (!empty($seat_array)) {

            $params['pnr_no'] = $fields['pnr_no'];
            $params['ticket_id'] = $fields['current_ticket_information']["ticket_id"];

            log_data("infinity/" . date("M") . "/cancel_ticket_" . date("d-m-Y") . "_log.log", $params, 'first hit cancelDetails params');

            $response = $this->cancelDetails($params);

            log_data("infinity/" . date("M") . "/cancel_ticket_" . date("d-m-Y") . "_log.log", json_encode($response), 'first hit cancelDetails Response json encoded');

            if (!empty($response)) {
                $data["status"] = "success";
                $data["refundAmount"] = $response['RefundAmount'];
                $data["cancellationCharge"] = $response['TotalFare'] - $response['RefundAmount'];
            } else {
                $data["status"] = "error";
                $data["error_message"] = "There might be a network issue. Please contact customer support.";
            }
        } else {
            $data["status"] = "error";
            $data["error_message"] = "Please provide seat number.";
        }

        return $data;
    }

    public function cancel_ticket($fields) {
        //API - This is cancelTicketConfirmation Method (First method)
        $data = array();
        if (is_array($fields['seat_no']) && !empty($fields['seat_no'])) {
            $seat_array = $fields['seat_no'];
        } else if ($fields['seat_no'] != "") {
            $seat_array = explode(",", $fields['seat_no']);
        }
        if (!empty($seat_array)) {

            $params['pnr_no'] = $fields['pnr_no'];
            $params['ticket_id'] = $fields['current_ticket_information']["ticket_id"];

            log_data("infinity/" . date("M") . "/cancel_ticket_" . date("d-m-Y") . "_log.log", $params, 'cancelTicketConfirmation params');

            $response = $this->cancelDetails($params);

            log_data("infinity/" . date("M") . "/cancel_ticket_" . date("d-m-Y") . "_log.log", json_encode($response), 'cancelTicketConfirmation Response json encoded');


            if (!empty($response)) {
                $final_cancel = $this->confirmCancellation($params);
                if (!empty($final_cancel) && $final_cancel["Status"] == '1') {
                    // * For Reconcellation process 
                    $this->saveReconcellations($fields['pnr_no'], $fields["current_ticket_information"]["ticket_id"], "success");

                    $data["status"] = "success";
                    $data["refundAmount"] = $final_cancel["RefundAmount"];
                    $data["cancellationCharge"] = $final_cancel["TotalFare"] - $final_cancel["RefundAmount"];
                    $data["cancellationChargePer"] = 0;
                    $data["raw_data"] = $final_cancel;
                } else {
                    $data["status"] = "error";
                    $data["error_message"] = 'Error code : ' . $final_cancel["Status"];
                }
            } else {
                $data["status"] = "error";
                $data["error_message"] = "Something unexpected happened. Please Try again.";
            }
        } else {
            $data["status"] = "error";
            $data["error_message"] = "Please provide seat number.";
        }

        return $data;
    }

    /*     * *******************MOBILE API STARTS HERE *********** */

    function api_get_seat_layout($post) {

        $config = array(
            array(
                'field' => 'from',
                'label' => 'From stop',
                'rules' => 'trim|required',
                'errors' => array('required' => 'from stop required')
            ),
            array(
                'field' => 'to',
                'label' => 'To stop',
                'rules' => 'trim|required',
                'errors' => array('required' => 'to stop required')
            ),
            array(
                'field' => 'date',
                'label' => 'Journey Date',
                'rules' => 'trim|required',
                'errors' => array('required' => 'Date required')
            ),
            array(
                'field' => 'inventory_type',
                'label' => 'inventory_type',
                'rules' => 'trim|required',
                'errors' => array('required' => 'inventory_type required')
            )
        );

        // $this->form_validation->set_data($data);

        if (form_validate_rules($config, $post) == FALSE) {
            $error = $this->form_validation->error_array();
            return array('status' => 'failed', 'msg' => $error);
        }

        $fields = array(
            'from' => $post['from'],
            'to' => $post['to'],
            'date' => $post['date'],
            'inventory_type' => $post['inventory_type'],
            'op_trip_no' => $post['service_id'], // routeScheduleId
        );
        $data = $this->its_api->get_seat_layout($fields);
        if (!$data) {
            return array('status' => 'failed', 'msg' => 'Opps some thing went wrong from operator end, Please select another bus!');
        }

        if ($data['flag'] != "@#success#@") {
            return array(
                'status' => 'failed',
                'msg' => 'Please contact customer support.',
                'apiMsg' => 'Please contact customer support.'
            );
        }

        foreach ($data as $key => $value) {
            if ($key == seat_fare_detail) {
                foreach ($value as $k => $v) {
                    foreach ($v as $final_value) {
                        $orientation = "";
                        if ($final_value['horizontal_seat'] == "1") {
                            $orientation = "horizontal";
                        } else {
                            if ($final_value['vertical_seat'] == "1")
                                $orientation = "vertical";
                            else
                                $orientation = "";
                        }

                        $seat = array(
                            'seat_no' => $final_value['seat_no'],
                            'seat_fare' => $final_value['seat_fare'],
                            'seat_basic_fare' => $final_value['seat_basic_fare'],
                            'seat_service_tax_amt' => $final_value['actual_seat_fare'], //confirm
                            'seat_service_tax_perc' => $final_value['seat_service_tax_perc'],
                            'row_no' => $final_value['row_no'],
                            'col_no' => $final_value['col_no'],
                            'seat_type' => $final_value['seat_type'],
                            'berth_no' => $final_value['berth_no'],
                            'pos' => $final_value['pos'],
                            'available' => $final_value['available'],
                            'width' => $final_value['width'],
                            'length' => $final_value['length'],
                            'service_tax_amount' => 0,
                            'service_tax_per' => 0,
                            'commission' => null,
                            'operator_service_charge_absolute' => $final_value['op_service_charge_amt'],
                            'operator_service_charge_percent' => $final_value['op_service_charge_perc'],
                            'ladies_seat' => $final_value['is_ladies'],
                            'booked_by' => null,
                            "ac" => isset($final_value['is_ac']) ? $final_value["is_ac"] : false, //confirm
                            "sleeper" => isset($final_value["is_sleeper"]) ? $final_value["is_sleeper"] : false, //confirm
                            'horizontal_seat' => $final_value['horizontal_seat'],
                            'vertical_seat' => $final_value['vertical_seat'],
                            'seat_orientation' => $orientation, //confirm this
                            'seat_fare_with_tax_with_discount' =>  $final_value['seat_fare']-$final_value['seat_discount'],
                        );
                        $seats[] = $seat;
                        if ($final_value['available'] == "Y") {
                            $available_seats = $final_value['seat_no'];
                            $available_seat_fare = array(
                                'total_fare_with_tax' => $final_value['seat_fare'],
                                'basic_fare' => $final_value['seat_basic_fare'],
                                'service_tax_per' => $final_value['seat_service_tax_perc'],
                                'service_tax_amount' => $final_value['actual_seat_fare'],
                                'op_service_charge_per' => $final_value['op_service_charge_perc'],
                                'op_service_charge_amount' => $final_value['op_service_charge_amt']
                            );
                            $seats_details = array(
                                'is_ladies_seat' => isset($final_value['is_ladies']) ? $final_value["is_ladies"] : 0,
                                'is_ac' => isset($final_value['is_ac']) ? $final_value["is_ac"] : 0,
                                'is_sleeper' => isset($final_value["is_sleeper"]) ? $final_value["is_sleeper"] : 0
                            );
                        }
                        $avail_seats[] = $available_seats;
                        $avail_seat_fare[$final_value['seat_no']] = $available_seat_fare;
                        $avail_seat_details[$final_value['seat_no']] = $seats_details;
                    }
                }
            }
        }
        $response['available_seat'] = $avail_seats;
        $response['available_seat_fare'] = $avail_seat_fare;
        $response['seats_details'] = $avail_seat_details;
        $response['seats'] = $seats;
        $response['boarding_stops'] = array(
            array(
                'bus_stop_name' => trim($post['from']),
                'stop_code' => trim($post['from']),
                'is_boarding' => 'Y', //confirm
                'is_alighting' => 'N', //confirm
                'sequence' => '1', //confirm
            )
        );
        $response['dropping_stops'] = array(
            array(
                'bus_stop_name' => trim($post['to']),
                'stop_code' => trim($post['to']),
                'is_boarding' => 'Y', //confirm
                'is_alighting' => 'N', //confirm
                'sequence' => '1', //confirm
            )
        );
        $response['inventory_type'] = 0;
        $response['route_schedule_id'] = $post['route_schedule_id'];
        $response['trip_no'] = $post['trip_no'];
        $response['no_of_berth'] = 1; //confirm
        $response['layout_orientation'] = "vertical"; //confirm
        $setSeatdetailsToRedis = [];
        $setSeatdetailsToRedis[$post['service_id']] = $response;
        //Set Services to redis which will be useful at temp_booking
        if (!empty($setSeatdetailsToRedis)) {
            $post['from'] = strtolower($post['from']);
            $post['to'] = strtolower($post['to']);
            $this->rediscache->set($post['from'] . $post['to'] . $post['date'], $setSeatdetailsToRedis, (60 * 60 * 60 * 24));
        }
        return array(
            'status' => 'success',
            'msg' => '',
            'data' => $response
        );
    }

    function apiTempBooking($post) {

        $config = array(
            array(
                'field' => 'from',
                'label' => 'From stop',
                'rules' => 'trim|required',
                'errors' => array('required' => 'from stop required')
            ),
            array(
                'field' => 'to',
                'label' => 'To stop',
                'rules' => 'trim|required',
                'errors' => array('required' => 'to stop required')
            ),
            array(
                'field' => 'date',
                'label' => 'Journey Date',
                'rules' => 'trim|required',
                'errors' => array('required' => 'Date required')
            ),
            array(
                'field' => 'service_id',
                'label' => 'Service Id',
                'rules' => 'trim|required',
                'errors' => array('required' => 'service_id required')
            ),
            array(
                'field' => 'route_schedule_id',
                'label' => 'route_schedule_id',
                'rules' => 'trim|required',
                'errors' => array('required' => 'route_schedule_id required')
            ),
            array(
                'field' => 'inventory_type',
                'label' => 'inventory_type',
                'rules' => 'trim|required',
                'errors' => array('required' => 'inventory_type required')
            )
        );
        // show($post,1); 
        if (form_validate_rules($config, $post) == FALSE) {
            $error = $this->form_validation->error_array();
            return array('status' => 'failed', 'msg' => $error);
        }

        $route_schedule_id = $post['route_schedule_id'];
        $services = $this->api_get_seat_layout($post);
        $seats = $this->get_formated_seat_layout($post);
        $date_of_journey = date("Y-m-d", strtotime($post['date']));

        $trip_condition = array(
            'from' => $post['from'],
            'to' => $post['to'],
            'date' => $date_of_journey
        );
        $trip_details = $this->Infinity_bus_service_model->as_array()->where($trip_condition)->find_all();
        $temp_arr = json_decode($trip_details[0]['bus_service']);
        $trip_data = $temp_arr->$post['route_schedule_id'];
        if (empty($services['data']['route_schedule_id']) || empty($seats)) {
            return array('status' => 'failed', 'msg' => 'Seat sold out. try another bus');
        }

        $trip = $services[$route_schedule_id];
        //------------ parameter for sending the data
        $passenger = array();
        $ac = false;
        $sleeper = false;
        $boarding_point = json_decode($post['boarding_point']);
        $passenger_array = json_decode($post['passenger']);
        $total_fare = 0;
        foreach ($passenger_array as $key => $vv) {

            $total_fare += $value->total_fare_with_taxes;
            foreach ($passenger_array as $k => $value) {
                $passenger[$value->seat_no] = array(
                    'name' => $value->name,
                    'lastName' => $value->last_name,
                    'gender' => $value->sex,
                    'age' => $value->age,
                    'seatNbr' => $value->seat_no,
                    'fare' => $value->fare,
                    'totalFareWithTaxes' => $value->total_fare_with_taxes,
                    'ladiesSeat' => $value->ladies_seat,
                    'mobile' => $value->mobile_no,
                    'title' => $value->title,
                    'email' => $value->email,
                    'idType' => $value->id_type,
                    'idNumber' => $value->id_number,
                    'nameOnId' => $value->name_on_id,
                    'primary' => $value->primary,
                    'ac' => $value->ac,
                    'sleeper' => $value->sleeper,
                );
            }
        }

        $fields = array(
            'input' => array(
                'seat' => array(1 => $passenger),
                'agent_mobile_no' => '',
                'agent_email_id' => '',
                'email_id' => $post['email'],
                'mobile_no' => $post['mobile_no'],
                'user_id' => $post['user_id'],
                'id_type' => $post['id_type'],
                'name_on_id' => $post['name_on_id'],
                'id_card_number' => $post['id_card_number'],
            ),
            'available_detail' => array(
                'trip_no' => $post['route_schedule_id'],
                'from_stop_id' => $trip_data->from_stop_id,
                'to_stop_id' => $trip_data->to_stop_id,
                'sch_dept_time' => $trip_data->sch_departure_tm,
                'arrival_time' => $trip_data->arrival_time,
                'from' => $post['from'],
                'to' => $post['to'],
                'boarding_stop_name' => $post['boarding_stop'],
                'destination_stop_name' => $post['destination_stop'],
                'boarding_stop_cd' => $post['boarding_stop_cd'],
                'destination_stop_cd' => $post['destination_stop_cd'],
                'date' => $post['date'],
                'fare' => array(
                ),
                'senior_citizen_fare' => array(),
                'is_child_concession' => array(),
                'child_seat_fare' => array(),
                'op_name' => $post['operator'],
                'library_name' => 'its_api',
                'bsd' => $post['boarding_stop_cd'],
                'dsd' => "",
                'user_type' => "",
                'op_id' => $post['op_id'],
                'bus_type_id' => "",
                'service_id' => $post['service_id'],
                'inventory_type' => $post['inventory_type'],
                'seats_details' => array(1 => $services['data']['seats_details']),
                'id_proof_required' => "",
                'bus_type' => array('op_bus_type_name' => 'Others'),
                'provider_id' => $post['provider_id'],
                'provider_type' => $post['provider_type'],
                'available_seat' => array(1 => $services['data']['available_seat']),
                'available_seat_fare' => array(1 => $services['data']['available_seat_fare']),
                'its_reference_number' => $post['service_id']
            ),
        );
        $data = $this->do_temp_booking($fields);

        if (!$data) {
            $ret = array('status' => 'failed', 'msg' => 'Api error Ivalid response from api. please try again later');
        } else {
            $ret = array(
                'status' => 'success',
                'msg' => '',
                'data' => $data
            );
        }
        return $ret;
    }

    /*     * ****DO CONFIRM BOOKING FOR API STARTS HERE ********* */

    public function apiDoconfirmBooking($data = array(), $ticket) {
        $ticket_id = $ticket['ticket_id'];
        $pt_data = $this->ticket_details_model
                ->where('ticket_id', $ticket['ticket_id'])
                ->find_all();
        $rpt_data = $this->return_ticket_mapping_model->get_rpt_mapping($ticket_id);
        $fb_event_value += $pt_data[0]->service_tax;
        $notify_seats .= $pt_data[0]->psgr_no;

        $api_data['ticket_id'] = str_pad($pt_data[0]->ticket_id, 8, '0', STR_PAD_LEFT);
        $api_data['ticket_ref_no'] = $data["block_key"];
        $api_data['seat_count'] = $pt_data[0]->psgr_no;
        $api_data['payment_by'] = "";
        $api_data['rpt_data'] = $rpt_data;
        $api_data['pt_data'] = $pt_data[0];
        $api_data['user_id'] = $data['user_id'];
        $api_data['unique_id'] = $data['unique_id'];
        if (empty($pt_data)) {
            return array("status" => "failed", "msg" => "not tickets found");
        }
        $api_request = array($api_data);
        $tbdata = $this->doconfirm_booking(array('ticket_id' => $api_data['ticket_id'],));
        $rpg_success_response = $this->process_confirm_booking($tbdata, $api_data);
        /*         * ******ACTUAL BOOKING STARTS HERE ******** */
        if (trim($rpg_success_response["msg_type"]) == "success") {
            //To check if return journey
            // $rpt_data = $this->return_ticket_mapping_model->get_rpt_mapping($data["pg_response"]["udf2"]);
            $rpt_data = $api_data["rpt_data"];
            if (count($rpt_data)) {
                $onward_ticket_id = $ticket_id;

                //to track return journey.
                $rtransaction = array('is_return_journey' => "1",
                    'return_ticket_id' => $rpt_data[0]->ticket_id
                );
                $this->tickets_model->update($onward_ticket_id, $rtransaction);
                // * *********** Actual ticket booking Start Here **********

                $rtbdata = $this->doconfirm_booking(array('ticket_id' => $rpt_data[0]->ticket_id));
                // * *********** Actual ticket booking End Here ********** */

                if (trim($rtbdata['status']) == 'success') {
                    $data["ticket_ref_no"] = $rpt_data[0]->ticket_ref_no;
                    $data["ticket_pnr"] = $rtbdata["pnr"];
                    $data["ticket_id"] = $rpt_data[0]->ticket_id;
                    $data["is_return_journey"] = true;


                    // * This is to update ticket table

                    $atransaction = array('transaction_status' => "success",
                        'payment_by' => $api_data["payment_by"],
                        'payment_mode' => $data["pg_response"]["payment_mode"],
                        'commision_percentage' => (isset($rtbdata['commision_percentage'])) ? $rtbdata['commision_percentage'] : "",
                        'api_ticket_no' => (isset($rtbdata['api_ticket_no'])) ? $rtbdata['api_ticket_no'] : "",
                        'cancellation_policy' => (isset($rtbdata['cancellation_policy'])) ? $tbdata['cancellation_policy'] : ""
                    );

                    $this->tickets_model->update($data["ticket_id"], $atransaction);

                    $final_response["return_ticket_status"] = "success";

                    $rpg_success_response = $this->process_confirm_booking($rtbdata, $data);

                    // * For Reconcellation process 
                    $this->saveReconcellations($data["ticket_pnr"], $data["ticket_id"], "success");

                    $final_response["transaction_process_response_return"] = $rpg_success_response;

                    if (trim($rpg_success_response["msg_type"]) == "success") {
                        $final_response["status"] = "success";
                    } else if (trim($rpg_success_response["msg_type"]) == "error") {
                        $transaction = array(
                            'failed_reason' => isset($rpg_success_response["msg"]) ? $rpg_success_response["msg"] : "some error occured in ets library",
                            'payment_by' => $api_data["payment_by"]
                        );

                        $id = $this->tickets_model->update($rpt_data[0]->ticket_id, $transaction);

                        $final_response["status"] = "error";
                        $final_response["return_transaction_process_api"] = "error";
                        $final_response["data"] = $data;
                    }
                } else {
                    if ($rpt_data[0]->transaction_status != "success") {
                        $api_failed_reason = (isset($rtbdata["failed_reason"])) ? $rtbdata["failed_reason"] : "Its return ticket api failed";
                        $transaction = array('transaction_status' => "failed",
                            'failed_reason' => $api_failed_reason,
                            'payment_by' => $api_data["payment_by"]
                        );

                        $return_ticket_update_where = ["ticket_id" => $rpt_data[0]->ticket_id, "transaction_status != " => "success"];
                        $id = $this->tickets_model->update($return_ticket_update_where, $transaction);
                    } else {
                        $this->common_model->developer_confirm_booking_error_mail("ETS (Return Journey)", $rpt_data[0]->ticket_id);
                    }

                    $final_response["return_ticket_status"] = "error";
                    $final_response["return_ticket_api_failed_reason"] = $api_failed_reason;

                    $final_response["status"] = "success";
                }
            } else {
                $final_response["status"] = "success";
            }
            $res_data['msg'] = "success";
            $res_data['bos_ref_no'] = $rpg_success_response["bos_ref_no"];
            $res_data['pnr'] = $rpg_success_response['pnr'];
        } else if (trim($rpg_success_response["msg_type"]) == "error") {
            $transaction = array(/* 'transaction_status' => "failed", */
                'failed_reason' => 'failed message - ' . (isset($pg_success_response["msg"])) ? $pg_success_response["msg"] : "",
                'payment_by' => $api_data["payment_by"]
            );

            $id = $this->tickets_model->update($api_data['ticket_id'], $transaction);
            $final_response["status"] = "success";
            $res_data['msg'] = "error";

            log_data("condition_not_handled/" . date("M") . "/ets_" . date("d-m-Y") . "_log.log", "Ticket booked successfully but some error occured in transaction_process_api for ticket is " . $data["ticket_id"], 'Transaction Process Status');
        }
        /*         * ******ACTUAL BOOKING ENDS HERE ********** */
        if ($res_data['msg'] == "error") {
            $response = array(
                'status' => 'failed',
                'msg' => 'Api failed'
            );
        } elseif ($res_data['msg'] == "success") {
            $response = array(
                'status' => 'success',
                'bos_key' => $res_data['bos_ref_no'],
                'pnr' => $res_data['pnr'],
                'ticket_no' => $ticket_id
            );
        }

        return $response;
    }

    function get_formated_seat_layout($post) {

        $data = $this->api_get_seat_layout($post);

        $tseats = array();
        if (!empty($data['data']['seats'])) {
            foreach ($data['data']['seats'] as $value) {
                $a = json_encode($value);
                $tseats[$value['seat_no']] = json_decode($a);
            }
        } else {
            return false;
        }

        $temp = new stdClass;
        $temp->seats = $tseats;

        return $temp;
    }

    function api_formated_services($from, $to, $date) {
        $services = $this->rediscache->get($from . $to . $date);
        if (empty($services)) {
            $fields = array(
                'from' => $from,
                'to' => $to,
                'date' => $date,
                'api' => 'api',
            );

            $data = $this->getAvailableBuses($fields);
            $services = array();
            foreach ($data as $key => $value) {
                $services[$value['routeScheduleId']] = $value;
            }
        }
        return $services;
    }

    function apiIsTicketCancelable($ticket, $post) {
        // $seatsDetail = $this->ticket_details_model->select('group_concat(seat_no) as seat')->find_by(['ticket_id' => $ticket->ticket_id]);
        $seatsDetail = "";
        $seatsDetail_a = $this->ticket_details_model->where('ticket_id', $ticket->ticket_id)->as_array()->find_all();
        if ($seatsDetail_a) {
            $seatsDetail_a = array_column($seatsDetail_a, 'seat_no');
            $seatsDetail = implode(",", $seatsDetail_a);
        }
        if (!$seatsDetail) {
            return ['status' => 'failed', 'msg' => 'seat error', 'data' => []];
        }


        $ticket_no = $ticket->pnr_no;
        $seats = $seatsDetail;
        $cancel_fields = ["seat_no" => $seats, "pnr_no" => $ticket_no];
        $data = $this->cancelDetails($cancel_fields);

        $response = ['TotalFare' => $data['TotalFare'],
            'refundAmount' => $data["RefundAmount"],
            'seats' => $data['SeatNames']
        ];

        return $response;
    }

    public function process_confirm_booking($tbdata, $api_data) {
        if (trim(strtolower($tbdata['status'])) == 'success') {
            $onwards_ticket_ref_no = $api_data["ticket_ref_no"];
            $data["ticket_pnr"] = $tbdata["pnr"];
            // * This is to update ticket table
            $transaction = array('transaction_status' => "success",
                'payment_by' => $api_data["payment_by"],
                'payment_mode' => $api_data["pg_response"]["payment_mode"],
                'commision_percentage' => (isset($tbdata['commision_percentage'])) ? $tbdata['commision_percentage'] : "",
                'api_ticket_no' => (isset($tbdata['api_ticket_no'])) ? $tbdata['api_ticket_no'] : "",
                'cancellation_policy' => (isset($tbdata['cancellation_policy'])) ? $tbdata['cancellation_policy'] : ""
            );

            $this->tickets_model->update($api_data['ticket_id'], $transaction);
            // $pg_success_response = $this->common_model->transaction_process_api($data);
            $blockKey = $api_data['ticket_ref_no'];
            $ticket_id = $api_data['ticket_id'];

            //After Remove Below Query and pass data
            $pt_data = $this->tickets_model->where("ticket_ref_no", $blockKey)->find_all();
            if ($pt_data) {
                $dept_date = date("D, d M Y, h:i A", strtotime($pt_data[0]->dept_time));
                $dept_time = date("H:i:s", strtotime($pt_data[0]->dept_time));

                $data['ticket_data'] = $pt_data[0];

                //Ticket Detail Data
                // $this->ticket_details_model->ticket_id = $ticket_id;
                // $ptd_data = $this->ticket_details_model->select();
                $ptd_data = $this->ticket_details_model->where("ticket_id", $ticket_id)->find_all();

                //$payment_total_fare = $pt_data[0]->num_passgr * $pt_data[0]->tot_fare_amt;
                $payment_total_fare = $pt_data[0]->tot_fare_amt_with_tax;
                $ticket_total_fare_without_discount = $pt_data[0]->total_fare_without_discount;

                $data['passenger_ticket'] = $pt_data[0];
                $data['passenger_ticket_details'] = $ptd_data;
                $data['pnr'] = $api_data['ticket_ref_no'];
                /*                 * **** To Update PNR, BOS REFERENCE NUMBER and SMS Count Start ********** */
                $bos_ref = getBossRefNo($ticket_id);
                // $this->tickets_model->boss_ref_no = $bos_ref["bos_ref_no"];
                // $bos_ref_exists = $this->tickets_model->select();
                $bos_ref_exists = $this->tickets_model->where("boss_ref_no", $bos_ref["bos_ref_no"])->find_all();
                if (!$bos_ref_exists) {
                    $bos_ticket_digit = $bos_ref["bos_ticket_digit"];
                    $bos_ticket_string = $bos_ref["bos_ticket_string"];
                    $bos_ref_no = $bos_ref["bos_ref_no"];
                }
                //THIS SEAT NO QUERY CAN BE REDUCE LATER
                $seat_no = $this->tickets_model->get_seat_nos_by_tid($ticket_id);
                if (count($seat_no) > 1) {
                    $seat_with_berth = "Berth 1: " . $seat_no[0]->seat_no . " Berth 2: " . $seat_no[1]->seat_no;
                } else {
                    $seat_with_berth = $seat_no[0]->seat_no;
                }

                $ticket_display_fare = $payment_total_fare;

                $smsData = [];
                $smsData["data"] = $data;
                $smsData["data"]["passenger_ticket"]->pnr_no = $data["ticket_pnr"];
                $smsData["seat_with_berth"] = $seat_with_berth;
//                $is_send = sendTicketBookedSms($smsData, $pt_data[0]->mobile_no);
                //if ticket booked by ors_agent two sms send one for agent and second for pessanger //
                $mobile_array[] = $pt_data[0]->mobile_no;
                if ($pt_data[0]->agent_mobile_no != "") {
                    $mobile_array[] = $pt_data[0]->agent_mobile_no;
                }
                $is_send = sendTicketBookedSms($smsData, $mobile_array);


                $eticket_count = ($is_send) ? 1 : 0;


                $fields_update = array("transaction_status" => "success",
                    "pnr_no" => $data["ticket_pnr"],
                    "bos_ticket_digit" => $bos_ticket_digit,
                    "bos_ticket_string" => $bos_ticket_string,
                    "boss_ref_no" => $bos_ref_no,
                    "eticket_count" => $eticket_count // IMP -> Placed 1 Hardcoded because. Here is the first time we are sending message.
                );

                $this->tickets_model->update($ticket_id, $fields_update);

                /*
                 * The Above SMS Code is temp and For testing purposeonly
                 */


                //to check if user exists
                $email = $pt_data[0]->user_email_id;
                $data['email'] = $email;
                $this->users_model->email = $email;
                $data['user_exists'] = $this->users_model->select();

                /*                 * ****************************************************************** */
                $available_detail = $this->session->userdata('available_detail');
                if (count($ptd_data) > 0) {
                    $passanger_details_basic_fare = 0;
                    //$basic_fare = $pt_data[0]->adult_basic_fare + $pt_data[0]->child_basic_fare;
                    $psgr_no = 1;
                    $passanger_detail = "";
                    foreach ($ptd_data as $key => $value) {

                        // $adult_child = ($value->psgr_age <= 12) ? "C" : "A";
                        /* if($value->psgr_age <= 12 && isset($available_detail['is_child_concession']) && $available_detail['is_child_concession'] == "Y") */
                        if ($value->psgr_age <= 12 && $value->psgr_type == "C") {
                            $adult_child = "C";
                            $passanger_details_basic_fare += $value->child_basic_fare;
                        } else {
                            $adult_child = "A";
                            $passanger_details_basic_fare += $value->adult_basic_fare;
                        }
                    }
                    //discuss master_id_data if condition later 
                    $tr_hide_show_class = ($pt_data[0]->discount_value > 0) ? " " : "hide";
                    //$basic_fare = $pt_data[0]->adult_basic_fare + $pt_data[0]->child_basic_fare;
                    $mail_replacement_array = array('{{bank_transaction_no}}' => "NA",
                        "{{pnr_no}}" => $data['pnr'],
                        "{{ticekt_no}}" => $ticket_id,
                        "{{bos_ref_no}}" => $bos_ref["bos_ref_no"],
                        "{{service_end_place}}" => $pt_data[0]->till_stop_name,
                        "{{reservation_from}}" => $pt_data[0]->from_stop_name,
                        "{{res_frm_cd}}" => $pt_data[0]->boarding_stop_cd,
                        "{{reservation_to}}" => $pt_data[0]->till_stop_name,
                        "{{res_to_cd}}" => $pt_data[0]->till_stop_cd,
                        "{{boarding_pont}}" => $pt_data[0]->boarding_stop_name,
                        "{{boarding_cd}}" => $pt_data[0]->boarding_stop_cd,
                        "{{alighting_point}}" => $pt_data[0]->destination_stop_name,
                        "{{alighting_cd}}" => $pt_data[0]->destination_stop_cd,
                        "{{doj}}" => $dept_date,
                        "{{dpt_tm}}" => date("h:i A", strtotime($dept_time)),
                        "{{boarding_time}}" => date("h:i A", strtotime($dept_time)),
                        "{{alighting_time}}" => date("h:i A", strtotime($pt_data[0]->alighting_time)),
                        "{{bus_service_type}}" => $pt_data[0]->bus_service_no,
                        "{{no_of_seats}}" => $pt_data[0]->num_passgr,
                        "{{psgr_email_id}}" => $pt_data[0]->user_email_id,
                        "{{psgr_contact_no}}" => $pt_data[0]->mobile_no,
                        "{{depot_name}}" => "NA",
                        // This must be come from op_config table. Email field is not there in table so added hardcore value.
                        "{{support_email_id}}" => 'online.support@upsrtc.com',
                        /* "{{tr_hide_show_class}}" => $tr_hide_show_class,
                          "{{discount_on_ticket}}" => $pt_data[0]->discount_value, */

                        // '{{basic_fare}}'  => $ptd_data[0]->adult_basic_fare, //this is temp. ask logic in meeting

                        /*               IMP NOTICE
                         * After Getting time insert detailed fare in tickets table
                         */
                        '{{basic_fare}}' => $passanger_details_basic_fare, //this is temp. ask logic in meeting
                        '{{res_charges}}' => $ptd_data[0]->fare_reservationCharge * $pt_data[0]->num_passgr,
                        '{{pas}}' => '0.00',
                        '{{acc}}' => $ptd_data[0]->fare_acc * $pt_data[0]->num_passgr,
                        '{{si}}' => $ptd_data[0]->fare_sleeperCharge * $pt_data[0]->num_passgr,
                        '{{it}}' => $ptd_data[0]->fare_it * $pt_data[0]->num_passgr,
                        '{{tt}}' => $ptd_data[0]->fare_toll * $pt_data[0]->num_passgr,
                        '{{octroi}}' => $ptd_data[0]->fare_octroi * $pt_data[0]->num_passgr,
                        '{{fare_hr}}' => $ptd_data[0]->fare_hr * $pt_data[0]->num_passgr,
                        '{{convey_charges}}' => $pt_data[0]->fare_convey_charge,
                        '{{other_amt}}' => $ptd_data[0]->fare_other * $pt_data[0]->num_passgr,
                        // '{{total_chargeable_amt}}' => $pt_data[0]->tot_fare_amt_with_tax,
                        '{{total_chargeable_amt}}' => $pt_data[0]->total_fare_without_discount,
                        "{{psgr_list}}" => $passanger_detail
                        );


                    /*                     * ********** Mail Start Here ******* */
                    $data['mail_content'] = str_replace(array_keys($mail_replacement_array), array_values($mail_replacement_array), UPSRTC_TICKET_TEMPLATE);

                    $mailbody_array = array("subject" => "Rokad Ticket",
                        "message" => $data['mail_content'],
                        "to" => $pt_data[0]->user_email_id
                    );

                    $mail_result = send_mail($mailbody_array);
                    //if agent booked tkt send mail to both agent as well as customer//
                    $email_agent = $pt_data[0]->agent_email_id;
                    if ($email_agent != "" && ($email_agent != $email)) {
                        $agent_mailbody_array = array("subject" => "Rokad Ticket",
                            "message" => $data['mail_content'],
                            "to" => $pt_data[0]->agent_email_id
                        );
                        $agent_mail_result = send_mail($agent_mailbody_array);
                    }
                    /*                     * ********** Mail End Here ******* */
                    /*                     * ********** Dump ticket Start Here ******* */
                    //dump ticket in table
                    /* $this->mail_template_model->title = 'Upsrtc Ticket Template';
                      $mail_data = $this->mail_template_model->select();
                      $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),$mail_data[0]->body); */


                    $this->ticket_dump_model->bos_ref_no = $bos_ref["bos_ref_no"];
                    $this->ticket_dump_model->ticket_id = $ticket_id;
                    $this->ticket_dump_model->ticket_ref_no = $blockKey;
                    $this->ticket_dump_model->pnr_no = $data['pnr'];
                    $this->ticket_dump_model->email = $email;
                    $this->ticket_dump_model->mobile = $pt_data[0]->mobile_no;
                    $this->ticket_dump_model->journey_date = $pt_data[0]->dept_time;
                    $this->ticket_dump_model->ticket = $data['mail_content'];
                    $this->ticket_dump_model->created_by = $api_data['user_id'];
                    $ticket_dump_id = $this->ticket_dump_model->save();
                    /*                     * ********** Dump ticket End Here ******* */

                    //insert into user action log
                    $users_action_log = array("user_id" => $api_data['user_id'],
                        "name" => $api_data['unique_id'],
                        "url" => "bos_ref_no = " . $bos_ref["bos_ref_no"] . " , ticket_ref_no = " . $blockKey . " , ticket_id = " . $ticket_id . " , pnr_no = " . $data['pnr'],
                        "action" => "bus ticket booked",
                        "message" => "Booked bus from <span class='navy_blue location'>" . ucfirst($pt_data[0]->from_stop_name) . "</span> to <span class='navy_blue location'>" . ucfirst($pt_data[0]->till_stop_name) . "</span> for the day <span class='navy_blue date'>" . date("jS F Y", strtotime($pt_data[0]->dept_time)) . "</span>"
                    );

                    users_action_log($users_action_log);
                    $pg_success_response["msg_type"] = "success";
                    $pg_success_response["bos_ref_no"] = $bos_ref["bos_ref_no"];
                    $pg_success_response['pnr'] = $data['pnr'];
                    $pg_success_response["ticket_id"] = $ticket_id;
                }// end of if $ptd_data condition
                else {
                    $custom_error_mail = array(
                        "custom_error_subject" => "ticket data not available",
                        "custom_error_message" => "ticket detail data not available .function name transaction_process_api"
                    );

                    $this->developer_custom_error_mail($custom_error_mail);

                    $pg_success_response["msg_type"] = "error";
                    $pg_success_response["msg"] = "ticket detail data not available.";
                }
            }
            // * For Reconcellation process 
            $this->saveReconcellations($data["ticket_pnr"], $data["ticket_id"], "success");
            $final_response = [];
            $final_response = $pg_success_response;
            //make this function common afterwards. Because this is used 2 times (1 times in pg and 1 time in wallet)
            return $final_response;
        }
    }

    public function apigetAvailableBuses($its_input_data) {
        $its_input_data['api'] = "api";
        $bus_services_result = $this->getAvailableBuses($its_input_data);
        //show($bus_services_result,1);
        $boardingPoint = $boardingPoint_array = [];
        $services_result = [];
        foreach ($bus_services_result['its_data']['bus_detail'] as $key => $value) {
            $day = (strtotime($value['str_dep_tm_his']) < strtotime($value['arrival_time'])) ? 0 : 1;
            if (!empty($value["barr"])) {
                $boarding_point_detail = $value["barr"];
                foreach ($boarding_point_detail as $k => $v) {
                    $boardingPoint_array = array(
                        "time" => $v["arrival_duration"],
                        "location" => $v["location"] . ",",
                        "id" => $v["boarding_point_id"]
                    );
                    $boardingPoint[] = $boardingPoint_array;
                }
            } else {
                $boardingPoint = array(
                    "time" => "",
                    "location" => "",
                    "id" => ""
                );
            }

            $dept_date = $value['dept_tm'];
            $travel_time = substr($value['travel_time'], 0, 2);
            $arrival_date = date("Y-m-d H:i:s", strtotime($dept_date . "+" . $travel_time . "hours"));
            $services_result = array(
                // 'op_user_id'            => '0',
                'from_stop_code' => $value['from_stop_code'],
                'to_stop_code' => $value['to_stop_code'],
                'from_stop' => $value['from_stop'],
                'to_stop' => $value['to_stop'],
                'trip_no' => $value['trip_no'],
                'route_no_name' => $value['from_stop'] . ' to ' . $value['to_stop'],
                'bus_stop_name' => $value['bus_stop_name'],
                'op_trip_no' => $value['op_trip_no'],
                'service_id' => $value['op_trip_no'],
                'seat_layout_id' => '0',
                'layout_name' => 'NA',
                'dept_tm' => $value['dept_tm'],
                'sch_departure_date' => date("d-m-Y", strtotime($value['sch_departure_date'])),
                'sch_departure_tm' => date("H:i:s", strtotime($value['dept_tm'])),
                'arrival_date' => $arrival_date,
                'arrival_tm' => $value['arrival_time'],
                'arrival_time' => $value['str_dep_tm_his'],
                'day' => $day,
                'bus_type_id' => $value['bus_type_id'],
                'bus_type_name' => $value['bus_type_name'],
                'op_bus_type_name' => $value['op_bus_type_name'],
                'op_bus_type_cd' => "",
                'is_child_concession' => false,
                'tot_seat_available' => $value['tot_seat_available'],
                'seat_fare' => $value['seat_fare'],
                'seat_fare_with_tax' => $value['actual_seat_fare'],
                'arrival_day' => $arrival_date,
                'boarding_stops' => $boardingPoint,
                'dropping_stops' => [],
                // custom
                'id_proof_required' => $value['id_proof_required'],
                'mticket_allowed' => $value['mticket_allowed'],
                'commission_percentage' => $value['commission_percentage'],
                'routeScheduleId' => $value['route_schedule_id'],
                'inventory_type' => $value['inventory_type'],
                'cancellation_policy' => $value['cancellation_policy'],
                'op_id' => $value['op_id'],
                'op_name' => $value['op_name'],
                'provider_id' => $value['provider_id'],
                'provider_type' => $value['provider_type'],
                'operator' => "",
                'seat_fare_with_tax_with_discount' => $value['seat_fare'],
                'discount_type' => "NA",
                'discount_on' => "NA",
                'discount_flat_rate' => "NA"
            );
            $services[] = $services_result;
        }
        $bus_services_result['its_data'] = $services;
        //show($bus_services_result,1);
        return $bus_services_result;
    }

    function apiCancelTicket($ticket, $post) {
        $this->load->model(array('ticket_details_model', 'cancel_ticket_model'));
        $this->load->library(array('cca_payment_gateway'));

        $seatsDetail = "";
        $seatsDetail_a = $this->ticket_details_model->where('ticket_id', $ticket->ticket_id)->as_array()->find_all();
        if ($seatsDetail_a) {
            $seatsDetail_a = array_column($seatsDetail_a, 'seat_no');
            $seatsDetail = implode(",", $seatsDetail_a);
        }
        if (!$seatsDetail) {
            return ['status' => 'failed', 'msg' => 'seat error', 'data' => []];
        }


        $ticket_no = $ticket->pnr_no;
        $seats = $seatsDetail;
        $api_data = ["seat_no" => $seats, "pnr_no" => $ticket_no];

        $data = $this->confirmCancellation($api_data);
        if ($data['Status'] != 1) {
            return array('status' => 'failed', 'msg' => 'Ticket is not cancellable', 'api_msg' => $data["error_message"]);
        }

        $this->tickets_model->update($ticket->ticket_id, array('status' => 'N', 'transaction_status' => 'cancel'));

        $this->ticket_details_model->update(array("ticket_id" => $ticket->ticket_id), array('status' => 'N', 'seat_status' => 'N'));

        $cancel_ticket_arr = array(
            "ticket_id" => $ticket->ticket_id,
            "pnr_no" => $ticket->pnr_no,
            "new_pnr" => '',
            "refund_amt" => $data['RefundAmount'],
            "raw" => json_encode($data)
        );

        $c_id = $this->cancel_ticket_model->insert($cancel_ticket_arr);

        if (!$c_id) {
            return array('status' => 'failed', 'msg' => 'cancel data not save', 'api_msg' => $data);
        }

        $paymentgateway_ref_no = "";
        $paymentgateway_ref_no = $ticket->pg_tracking_id;

        $acual_refund_paid = $data['RefundAmount'];

        $merchant_array_data = array(
            "reference_no" => $paymentgateway_ref_no,
            "refund_amount" => $acual_refund_paid,
            "refund_ref_no" => uniqid()
        );

        $refund_status = $this->cca_payment_gateway->refundOrder($merchant_array_data);
        /*
          if($refund_status->refund_status)
          {
          return array('status' => 'failed', 'msg' => 'Error in processing request');
          }
         */
        $update_refund_status = array('is_refund' => "1",
            "actual_refund_paid" => $acual_refund_paid,
            "pg_refund_amount" => $acual_refund_paid
        );

        $update_where_cancel = array("id" => $c_id);
        $refund_id = $this->cancel_ticket_model->update($update_where_cancel, $update_refund_status);

        if (!$refund_id) {
            log_data("refund/success_but_not_updated_ccavenue_refund_" . date("d-m-Y") . "_log.log", "Refund is success but status is not updated for 'cancel_ticket_data table' for id " . $c_id, 'cancel_ticket_data table id');
        }

        $response = array(
            'pnr' => $ticket->pnr_no,
            'new_pnr' => '',
            'refund_amt' => $acual_refund_paid
        );

        return array(
            'status' => 'success',
            'msg' => 'Ticket Cancelled successfully.',
            'data' => $response
        );
    }

    /*     * *******MAIL CANCEL TICKET STARTS HERE ********************** */

    public function mail_cancel_ticket($tinfo, $data, $ptd_data) {
        $tkt_cancellation_date = $this->cancel_ticket_model->column('created_at')->where('ticket_id', $tinfo['ticket_id'])->find_all();
        if (count($ptd_data) > 0) {
            $psgr_no = 1;
            $passanger_detail = "";

            foreach ($ptd_data as $key => $value) {
                $passanger_detail .= '<tr style="padding:7px 0;text-align: center">
                          <td style="padding:7px;">
                            ' . ucwords($value->psgr_name) . '
                          </td>
                          <td style="padding:7px 0;text-align: center">
                            <span style="line-height:27px;">
                            ' . ucwords($value->psgr_age) . '
                            </span>
                          </td>
                          <td style="padding:7px 0;text-align: center">
                            ' . ucwords($value->psgr_sex) . '
                          </td>
                          <td style="padding:7px 0;text-align: center">
                            ' . $value->seat_no . '
                          </td>
                        </tr>';
                $psgr_no++;
            }

            $cancellation_policy_to_show = "";
            if (isset($pt_data[0]->cancellation_policy) && $pt_data[0]->cancellation_policy != "") {
                $cpolicy = $pt_data[0]->cancellation_policy != '' ? json_decode($pt_data[0]->cancellation_policy) : array();
                if (!empty($cpolicy)) {
                    foreach ($cpolicy as $ckey => $cvalue) {
                        if (!empty($cvalue)) {
                            $charges_in_per = "";
                            $charges_in_per = 100 - $cvalue->refundInPercentage;
                            $cancellation_policy_to_show .= '<tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;">
                                                              <td style="padding:3px;">Cancellation within ' . $cvalue->cutoffTime . ' hrs</td>
                                                              <td style="padding:3px;">' . $charges_in_per . '%</td>
                                                            </tr>';
                        }
                    }
                }
            }
            $pickup_address = $pt_data[0]->boarding_stop_name;
            $pickup_address .= ($pt_data[0]->pickup_address != "") ? " (" . $pt_data[0]->pickup_address . ")" : "";

            if ($pt_data[0]->droping_time != "" && $pt_data[0]->droping_time != "0000-00-00 00:00:00") {
                $droping_time = date("h:i A", strtotime($pt_data[0]->droping_time));
            } else {
                $droping_time = date("h:i A", strtotime($pt_data[0]->alighting_time));
            }

            if ($pt_data[0]->boarding_time != "" && $pt_data[0]->boarding_time != "0000-00-00 00:00:00") {
                $bus_boarding_time = date("h:i A", strtotime($pt_data[0]->boarding_time));
            } else {
                $bus_boarding_time = "NA";
            }

            $agent_details = "";

            if (!empty($this->session->userdata("user_id"))) {
                eval(AGENT_ID_ARRAY);
                if (in_array($this->session->userdata("role_id"), $agent_id_array)) {

                    $user_agent_details = $this->users_model
                            ->where(["id" => $this->session->userdata("user_id")])
                            ->find_all();
                    if ($user_agent_details) {
                        $agent_firm_name = ($user_agent_details[0]->firm_name != "") ? $user_agent_details[0]->firm_name : $user_agent_details[0]->first_name . " " . $user_agent_details[0]->last_name;
                        $agent_details = "<tr>
                                <td colspan=2' align='right' valign='top' style='color:#000;font-weight:bold; font-size:13px; text-align:centre; padding-top:7px;'>
                                <span style='color:#ff940a;'>
                                " . $agent_firm_name . "</span><br />Agent Contact No. " . $user_agent_details[0]->mobile_no . "</td>
                              </tr>";
                    }
                }
            }


            $mail_replacement_array = array('{{cancellation_date}}' => date("jS F Y", strtotime($tkt_cancellation_date[0]->created_at)),
                "{{pnr_number}}" => $tinfo['pnr_no'],
                "{{from_to_journey}}" => $tinfo['from_stop_name'] . " to " . $tinfo['till_stop_name'],
                "{{journey_date}}" => date("l \- jS F Y", strtotime($tinfo['dept_time'])),
                "{{op_name}}" => $tinfo['op_name'],
                "{{seat_no}}" => $tinfo['num_passgr'],
                "{{bus_types}}" => $tinfo['bus_type'],
                "{{total_fare}}" => "Rs. " . $tinfo['tot_fare_amt_with_tax'],
                "{{bos_ref_no}}" => $tinfo['boss_ref_no'],
                "{{boarding_point}}" => $tinfo['boarding_stop_name'],
                "{{dropping_point}}" => $tinfo['destination_stop_name'],
                "{{departure_time}}" => $bus_boarding_time,
                "{{alighting_time}}" => date("h:i A", strtotime($tinfo['alighting_time'])),
                "{{email}}" => $tinfo['user_email_id'],
                "{{mobile_no}}" => $tinfo['mobile_no'],
                "{{cancellation_charge}}" => "Rs. " . $data['cancellation_cahrge'],
                "{{refund_amount}}" => "Rs. " . $data['refund_amt'],
                "{{passanger_details}}" => $passanger_detail,
                "{{agent_details}}" => $agent_details,
                "{{dynamic_cancellation_policy}}" => $cancellation_policy_to_show
            );

            /*             * ********** Mail Start Here ******* */
            $data['mail_content'] = str_replace(array_keys($mail_replacement_array), array_values($mail_replacement_array), BOS_CANCEL_TICKET_TEMPLATE);
            $mail = $tinfo['user_email_id'];
            $mailbody_array = array("subject" => "Bookonspot cancel Ticket",
                "message" => $data['mail_content'],
                "to" => $mail
            );

            $mail_result = send_mail($mailbody_array);

            //****if agent booked tkt send mail to both agent as well as customer****//
            $email_agent = $tinfo['agent_email_id'];
            if ($email_agent != "" && ($email_agent != $email)) {
                $agent_mailbody_array = array("subject" => "Rokad Ticket",
                    "message" => $data['mail_content'],
                    "to" => $email_agent
                );
                $agent_mail_result = send_mail($agent_mailbody_array);
            }
            /*             * ********** Mail End Here ******* */
            /*             * ********************** Update in Ticket_dump table start here **************************** */
            $update_in_ticket_dump = $this->ticket_dump_model->update_where(array('ticket_id' => $tinfo['ticket_id']), '', array('cancel_ticket' => $data['mail_content']));

            /*             * ********************** Update in Ticket_dump table end here **************************** */
        }
    }
     public function GetCurrentAccountBalance() {

        $action = "http://apibookonspot.itspl.net/GetCurrentAccountBalance";
        $request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:apib="http://apibookonspot.itspl.net/">
                 <soapenv:Header/>
                 <soapenv:Body>
                    <apib:GetCurrentAccountBalance>
                       <!--Optional:-->
                       <apib:VerifyCall>' . $this->config->item("its_transaction_access_key") . '</apib:VerifyCall>
                    </apib:GetCurrentAccountBalance>
                 </soapenv:Body>
              </soapenv:Envelope>';
        $response = get_soap_data_its($this->config->item("its_wsdl_url"), 'GetCurrentAccountBalance', $request, $action);
        //return $response;
       //show($response,1);
       
       if (isset($response["GetCurrentAccountBalanceResult"]['diffgram'])) {
            return $response["GetCurrentAccountBalanceResult"]["diffgram"]["DocumentElement"]["ITSCurrentAccountBAL"];
        } else {
            return [];
        }
    }
//Mail Cancel ticket end here......
}

?>
