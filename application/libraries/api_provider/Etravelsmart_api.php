<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Etravelsmart_api extends CI_Model {

  var $provider_id;
  var $provider_type;
 
  public function __construct()
  {
    parent::__construct();

    $this->load->library(array('rediscache','discounts'));

    $this->load->model(array("agent_commission_template_log_model","tickets_additional_data_model","etravels_bus_service_model","temp_bus_types_model","bus_type_model","ticket_discount_description_model",'ticket_details_model','discount_model'));

    ini_set('max_execution_time', 100000);
    ini_set("memory_limit","256M");

    $this->provider_id    = $this->config->item("etravelsmart_api_id");

    $this->provider_type  = "api_provider";
    $this->try            = 3;
  }

  public function index()
  {
    
  }

  //  function to fetch api data
  function getAvailableBusesApi($from, $to, $date)
  {    
    $fields   = array(
                      "sourceCity" => urlencode($from),
                      "destinationCity" => urlencode($to),
                      "doj" => date("Y-m-d", strtotime($date))
                    );

    log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$fields, 'Bus Service Request - getAvailableBusesApi');

    // $response = getEtravelsmartApiData("getAvailableBuses",$fields);

    for ($i=0; $i < $this->try; $i++) { 
      $response = getEtravelsmartApiData("getAvailableBuses",$fields);

      if(is_JSON($response)) break;
    }

    log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$response, 'Bus Service Response - getAvailableBusesApi');
    

    if(!is_JSON($response))
    {
      $response = false;
    }
    return $response;
  }

  function getBusLayoutApi($fields)
  {
    $post_fields = array(
                          "sourceCity"      => urlencode($fields['from']),
                          "destinationCity" => urlencode($fields['to']),
                          "doj"             => date("Y-m-d", strtotime($fields['date'])),
                          "inventoryType"   => $fields['inventory_type'],
                          "routeScheduleId" => $fields['op_trip_no']
                        );

    log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$post_fields, 'Bus Layout Request - getBusLayoutApi');

    // $response = getEtravelsmartApiData("getBusLayout",$post_fields);

    for ($i=0; $i < $this->try; $i++) { 
      $response = getEtravelsmartApiData("getBusLayout",$post_fields);

      if(is_JSON($response)) break;
    }

    log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$response, 'Bus Layout Response - getBusLayoutApi');

    if(!is_JSON($response))
    {
      $response = false;
    }

    return $response;
  }

  function blockTicketApi($post_fields)
  {

    log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$post_fields, 'Block Ticket Array Request');

    $post_fields = json_encode($post_fields);

    log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$post_fields, 'Block Ticket Json encode Request');

    
    for ($i=0; $i < $this->try; $i++) { 
      $response = getEtravelsmartApiData("blockTicket",$post_fields ,"POST");

      if(is_JSON($response)) break;
    }

    log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$response, 'Block Ticket Json encode response');

    if(!is_JSON($response))
    {
      $response = false;
    }

    return $response;
  }

  function seatBookingApi($fields)
  {
    $post_fields = array(
                        "blockTicketKey" => $fields['ticket_ref_no']
                      );

    log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$post_fields, 'Do Confirm Booking Request');

    // $response = getEtravelsmartApiData("seatBooking",$post_fields);

    // for ($i=0; $i < $this->try; $i++) { 
      $response = getEtravelsmartApiData("seatBooking",$post_fields);

      //if(is_JSON($response)) break;
    // }

    log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$response, 'Do Confirm Booking response');

    if(!is_JSON($response))
    {
      $response = false;
    }

    return $response;
  }

  function cancelTicketApi($post_fields)
  {
    
    $post_fields = json_encode($post_fields);
      
    log_data("etravelsmart/".date("M")."/cancel_ticket_".date("d-m-Y")."_log.log",$post_fields, 'cancelTicket Request parameter');
      
    // $response = getEtravelsmartApiData("cancelTicket",$post_fields,"POST");

    for ($i=0; $i < $this->try; $i++) { 
      $response = getEtravelsmartApiData("cancelTicket",$post_fields,"POST");

      if(is_JSON($response)) break;
    }

    log_data("etravelsmart/".date("M")."/cancel_ticket_".date("d-m-Y")."_log.log",$response, 'cancelTicket response');

    if(!is_JSON($response))
    {
      $response = false;
    }

    return $response;
      
  }

  function cancelTicketConfirmationApi($post_fields)
  {
    $post_fields = json_encode($post_fields);
    log_data("etravelsmart/".date("M")."/cancel_ticket_".date("d-m-Y")."_log.log",$post_fields, 'cancelTicketConfirmationApi Request');

    // $response = getEtravelsmartApiData("cancelTicket",$post_fields,"POST");
    
    for ($i=0; $i < $this->try; $i++) { 
      $response = getEtravelsmartApiData("cancelTicketConfirmation",$post_fields,"POST");

      if(is_JSON($response)) break;
    }


    log_data("etravelsmart/".date("M")."/cancel_ticket_".date("d-m-Y")."_log.log",$response, 'cancelTicketConfirmationApi response');

    if(!is_JSON($response))
    {
      $response = false;
    }

    return $response;
  }


  //  function to fetch api data ends 



 /* 
  * @author    : Suraj Rathod
  * @function  : getStations
  * @param     : 
  * @return    : List of Stations. It contains the following member variables apiStatus, list of apiStation.
  * @method    : HTTP GET
  * @detail    : This API provides the list of all available stations.
  */
  public function getStations()
  {
    $response = getEtravelsmartApiData("getStations");
    $response_decode = json_decode($response);
    $api_status = $response_decode->apiStatus;

    if($api_status->success && strtolower($api_status->message) == "success")
    {
      return $response_decode->stationList;
    }
    else
    {
      return "";
    }
  }


  /* 
  * @author    : Suraj
  * @function  : getAvailableBuses
  * @param     : 
  * @return    : Contains the status and message. It contains following member variables message, isSuccess.
  * @method    : HTTP GET
  * @detail    : This API provides the list of available routes between the given source, destination stations and journey-date
  */
  public function getAvailableBuses($fields, $comm_id_data,$to_update = true)
  {
    $data = array();
    $temp_bus_types = array();
    $bos_bus_types = array();
    $data["bus_detail"] = array();
    $data["time_fare"]["bus_detail"] = array("min_fare" => 0,
                                              "max_fare" => 1,
                                              "min_dep_tm" => 0,
                                              "max_dep_tm" =>0
                                            );
    $is_true = true;
    $session_store = $compareCommissionData = array();
    $time_range = array(array('start' => '04:00:00', 'end' => '11:59:00', "shift" => "morning"),
                        array('start' => '12:00:00', 'end' => '15:59:00', "shift" => "afternoon"),
                        array('start' => '16:00:00', 'end' => '18:59:00', "shift" => "evening"),
                        array('start' => '19:00:00', 'end' => '23:59:00', "shift" => "night"),
                        array('start' => '00:00:00', 'end' => '03:59:00', "shift" => "mid_night")
                       );

    $post_fields = array("sourceCity" => $fields['from'],
                        "destinationCity" => $fields['to'],
                        "doj" => date("Y-m-d", strtotime($fields['date_of_jour']))
                        );


    // $response = getEtravelsmartApiData("getAvailableBuses",$post_fields);
    $response = $this->getAvailableBusesApi($fields['from'], $fields['to'], $fields['date_of_jour']);

    $response_decode = json_decode($response);

    if(is_object($response_decode))
    {
      $api_status = $response_decode->apiStatus;

      $user_data['from'] = $fields['from'];
      $user_data['to'] = $fields['to'];
      $user_data['date_of_jour'] = $fields['date_of_jour'];
      $user_data['date'] = date("Y-m-d", strtotime($fields['date_of_jour']));

      $data['user_data'] = $user_data;

      if($api_status->success && strtolower($api_status->message) == "success")
      {
        //Get All Bus Types from bus_types table
        $bos_bustypes = $this->bus_type_model->find_all();

        if($bos_bustypes)
        {
          foreach ($bos_bustypes as $boskey => $bosvalue)
          {
            $bos_bus_types[trim($bosvalue->bus_type_name)]  = trim($bosvalue->id);
          }
        }

        $mapped_bus_types = array();
        $map_bus_type = $this->bus_type_model->get_api_bus_type($this->config->item("etravelsmart_api_id"));


        if($map_bus_type && isset($map_bus_type["mapped_type"]) && $map_bus_type["mapped_type"])
        {
          $mapped_bus_types = $map_bus_type["mapped_type"];
        }

        $data['user_data']["from_stop_code"] = $fields['from'];
        $data['user_data']["to_stop_code"] = $fields['to'];

        $extra_array["mapped_bus_types"] = $mapped_bus_types;
        
        /**************************************************/
        $travel = array();
        $bus_type = array();
        $dept_tm = array();

        $min_fare = 0;
        $max_fare = 1;

        $min_dep_tm = 0;
        $max_dep_tm = 0;
        
        $min_max_fare = array();
        $min_max_deptm = array();

        $is_bus_type_filter_on = in_array($fields['searchbustype'],["ac","non_ac"]) ? true : false;
        foreach ($response_decode->apiAvailableBuses as $key => $value) 
        {
          if($value->availableSeats > 0) {
            $compareCommissionData[$key]["commission_percentage"] = $value->commPCT;
            $compareCommissionData[$key]["operator_name"] = $value->operatorName;
            $compareCommissionData[$key]["bus_type"] = $value->busType;
            $compareCommissionData[$key]["arrival_time"] = $value->arrivalTime;
            $compareCommissionData[$key]["arrival_time"] = $value->arrivalTime;
            $compareCommissionData[$key]["departure_time"] = $value->departureTime;  
            $to_proceed = true;
            if($is_bus_type_filter_on) {
              $extra_array["busType"] = trim($value->busType);
              $to_proceed = check_search_bus_type($fields, $extra_array);
            }
            if($to_proceed)
            {
              if(stripos(strtolower($value->operatorName),"Himachal Road Transport Corporation HRTC") === false && stripos(strtolower($value->operatorName),"Uttar Pradesh State Road Transport Corporation (UP") === false && stripos(strtolower($value->operatorName),"uttar pradesh state road transport corporation(upsrtc)") === false && stripos(strtolower($value->operatorName),"upsrtc") === false && stripos(strtolower($value->operatorName),"Uttar Pradesh State Transport") === false && stripos(strtolower($value->operatorName),"msrtc") === false)
              {
                if (!in_array($value->operatorName, $travel)) {
                    $travel[] = $value->operatorName;
                }
                $departureTime = date("H:i:s",strtotime($value->departureTime));

                if (!in_array($departureTime, $dept_tm)) {
                    $dept_tm[] = $departureTime;
                }

                //retrive trip wise  boarding stop detail
                $dept_tm_his = date("H:i:s",strtotime($value->departureTime));
                // $bord_data = $this->Boarding_stop_model->getstops($value['trip_no']);
                // $data["bus_detail"][$key] = $value;
                $data["bus_detail"][$key]['barr'] = array();
                // for boarding Points
                // $data["bus_detail"][$key]['barr'] = $bord_data;

                  if(!empty($value->boardingPoints))
                  {
                    foreach($value->boardingPoints as $bkey => $bvalue)
                    {
                      $bstop_name = explode(",", $bvalue->location);

                      $boarding_detail_array = array(
                                              "bus_stop_name" => $bstop_name[0],
                                              "boarding_stop_name" => $bstop_name[0],

                                              "arrival_duration" => $bvalue->time,
                                              "location" => $bvalue->location,
                                              "boarding_point_id" => $bvalue->id,

                                              "is_boarding" => "Y",//This is for boarding point
                                              "is_alighting" => "N",//This is for dropping point
                                              "seq" => $bkey+1,
                                            );

                      $data["bus_detail"][$key]['barr'][] = $boarding_detail_array;

                      /*For Session*/
                      $session_store[$value->routeScheduleId]["barr"][$bvalue->id] = $boarding_detail_array;;
                    }
                  }

                  if(!empty($value->droppingPoints))
                  {
                    foreach($value->droppingPoints as $dkey => $dvalue)
                    {
                      $dstop_name = explode(",", $dvalue->location);
                      $dropping_detail_array = array(
                                                      "bus_stop_name" => $dstop_name[0],
                                                      "boarding_stop_name" => $dstop_name[0],

                                                      "arrival_duration" => $dvalue->time,
                                                      "location" => $dvalue->location,
                                                      "boarding_point_id" => $dvalue->id,

                                                      "is_boarding" => "N",//This is for boarding point
                                                      "is_alighting" => "Y",//This is for dropping point
                                                      "seq" => $dkey+1,
                                                    );
                      
                      $data["bus_detail"][$key]['barr'][] = $dropping_detail_array;
                      /*For Session*/
                      $session_store[$value->routeScheduleId]["darr"][$dvalue->id] = $dropping_detail_array;
                    }
                  }

                  //If conditions for bus_type_id $bus_type_id
                  if(array_key_exists(trim($value->busType), $mapped_bus_types))
                  {
                    $bus_type_id = $mapped_bus_types[trim($value->busType)];
                    if(!empty($bos_bus_types) && array_key_exists(trim($bus_type_id), $bos_bus_types))
                    {
                      $bus_type_id = $bos_bus_types[trim($bus_type_id)];
                    }
                  }
                  else
                  {
                    $bus_type_id = "Others";
                  }

                  //Required Details
                  $data["bus_detail"][$key]["bus_travel_id"]      = $value->routeScheduleId;
                  $data["bus_detail"][$key]["from_stop_code"]     = $fields['from'];
                  $data["bus_detail"][$key]["to_stop_code"]       = $fields['to'];
                  $data["bus_detail"][$key]['op_name']            = ucwords(str_replace("'", "", $value->operatorName));
                  $data["bus_detail"][$key]['bus_stop_name']      = $fields['to'];

                  $data["bus_detail"][$key]['mticket_allowed']    = $value->mTicketAllowed;
                  $data["bus_detail"][$key]['from_stop']          = ucwords($fields['from']);
                  $data["bus_detail"][$key]['to_stop']            = ucwords($fields['to']);
                  $data["bus_detail"][$key]['dept_tm']            = date("Y-m-d", strtotime($fields['date_of_jour']))." ".$dept_tm_his;
                  $data["bus_detail"][$key]['sch_departure_date'] = date("d-m-Y", strtotime($fields['date_of_jour']));
                  
                  $data["bus_detail"][$key]['arrival_tm']         = $value->arrivalTime;

                  // $data["bus_detail"][$key]['bus_type_name']                = trim($value->busType);
                  // $data["bus_detail"][$key]['bus_type_id']               = $value->busType;
                  // $data["bus_detail"][$key]['op_bus_type_name']          = $value->busType;

                  // $data["bus_detail"][$key]['op_bus_type_name']             = (array_key_exists(trim($value->busType), $mapped_bus_types)) ? $mapped_bus_types[trim($value->busType)] : "Others";
                  // $data["bus_detail"][$key]['bus_type_id']                  = (array_key_exists(trim($value->busType), $mapped_bus_types)) ? $mapped_bus_types[trim($value->busType)] : "Others";
                  $data["bus_detail"][$key]['api_bus_type']                 = true;
                  $data["bus_detail"][$key]['bus_type_name']                = (array_key_exists(trim($value->busType), $mapped_bus_types)) ? $mapped_bus_types[trim($value->busType)] : "Others";
                  $data["bus_detail"][$key]['bus_type_id']                  = $bus_type_id;
                  $data["bus_detail"][$key]['op_bus_type_name']             = trim($value->busType);
                  $data["bus_detail"][$key]['tot_seat_available']           = $value->availableSeats;

                  $temp_bus_types[]                                         = $value->busType;
                  //sorted fare in order. Used/benefit while sorting.
                  $farearray = explode(",",$value->fare);
                  $farearray = array_unique($farearray);
                  asort($farearray);
                  $data["bus_detail"][$key]['actual_seat_fare']                   = implode(",",$farearray);
                  $compareCommissionData[$key]["actual_seat_fare"] = $data["bus_detail"][$key]['actual_seat_fare'];
                  $data["bus_detail"][$key]['service_discount']           = false;
  
                  $data["bus_detail"][$key]['seat_fare']                    = implode(",",$farearray);
                  $compareCommissionData[$key]["seat_fare"] = $data["bus_detail"][$key]['seat_fare'];      
                  $cancellation_policy = array();

                  $cpolicy = $value->cancellationPolicy != '' ? json_decode($value->cancellationPolicy) : array();

                  if(!empty($cpolicy))
                  {
                    foreach ($cpolicy as $ckey => $cvalue)
                    {

                      if(!empty($cvalue))
                      {
                        $temp = new stdClass;

                        $temp->cutoffTime = $cvalue->cutoffTime;
                        $temp->refundInPercentage = 100 - $cvalue->refundInPercentage;
                        $cancellation_policy[] = $temp;
                      }
                    }
                  }
                // $data["bus_detail"][$key]['cancellation_policy']          = json_decode($value->cancellationPolicy);
                $data["bus_detail"][$key]['cancellation_policy']          = $cancellation_policy;
                $data["bus_detail"][$key]['inventory_type']               = $value->inventoryType;
                $data["bus_detail"][$key]['route_schedule_id']            = $value->routeScheduleId;
                $data["bus_detail"][$key]['id_proof_required']            = $value->idProofRequired;
                $data["bus_detail"][$key]['partial_cancellation_allowed'] = $value->partialCancellationAllowed;
                //calculate commission 
                $agent_commission = 0;
             
                $data["bus_detail"][$key]['commission_percentage']              = $value->commPCT;
                $data["bus_detail"][$key]['agent_commission_percentage']        = $agent_commission;
                $data["bus_detail"][$key]['str_dep_tm'] = strtotime($value->departureTime);
                // $data["bus_detail"][$key]['str_dep_tm'] = strtotime(date("Y-m-d", strtotime($fields['date_of_jour']))." ".$dept_tm_his);
                $data["bus_detail"][$key]['str_dep_tm_his'] = $value->departureTime;
                // $data["bus_detail"][$key]['str_dep_tm_hm'] = trim(hoursToMinutes($value->departureTime));
                $data["bus_detail"][$key]['str_dep_tm_hm'] = strtotime($user_data['date']." ".$dept_tm_his);

                $data["bus_detail"][$key]['arrival_time']   = $value->arrivalTime;
                $data["bus_detail"][$key]['arrival_date']   = $value->arrivalTime;
                $data["bus_detail"][$key]['arrival_date_time'] = $value->arrivalTime;
                $data["bus_detail"][$key]['op_trip_no'] = $value->routeScheduleId;
                $data["bus_detail"][$key]['trip_no'] = $value->routeScheduleId;

                $data["bus_detail"][$key]['provider_id'] = $this->config->item("etravelsmart_api_id");
                $data["bus_detail"][$key]['provider_type'] = "api_provider";
                $data["bus_detail"][$key]['op_id'] = 0;

                /*$data["bus_detail"][$key]['api_type'] = "api";
                $data["bus_detail"][$key]['api_name'] = ETRAVELSMART_API_NAME;*/
                // $data["bus_detail"][$key]['test'] = (strtotime($value->arrivalTime) > strtotime($value->departureTime)) ? "arrival greater" : "departure greater";

                /************* total time required to travel calculation start**************/

                /*
                 * This is incorrect. Because API does not provide us exact date of arrival time
                 */

                /*-----------------------*/
                $departure_his_str = strtotime(date("h:i A", strtotime($value->departureTime)));
                $arrival_his_str = strtotime(date("h:i A", strtotime($value->arrivalTime)));


                if($departure_his_str < $arrival_his_str)
                {
                  //Same arrival day
                  $assumed_arrival_date = date("Y-m-d", strtotime($fields['date_of_jour']))." ".date("H:i:s", strtotime($value->arrivalTime));
                }
                else if(($departure_his_str > $arrival_his_str) || ($departure_his_str == $arrival_his_str) )
                {
                  //Add one day to arrival date 
                  $assumed_arrival_date = date("Y-m-d", strtotime($fields['date_of_jour'] . ' +1 day'))." ".date("H:i:s", strtotime($value->arrivalTime));
                }
                //for calculatiom
                $assumed_departure_date = $fields['date_of_jour']." ".date("H:i:s",$departure_his_str);
                /*----------------------*/

                // $tmdiff = calculate_date_diff($value->departureTime,$value->arrivalTime);
                $tmdiff = calculate_date_diff($assumed_departure_date,$assumed_arrival_date);
                $days    = $tmdiff->format('%d');
                $hours   = $tmdiff->format('%h');
                $minutes = $tmdiff->format('%i');

                $travel_time = (($days > 0) ? $days." day " : "").(($hours > 0) ? $hours." hours " : "").(($minutes > 0) ? $minutes." minutes" : "");
                $short_travel_time = (($days > 0) ? $days." D " : "").(($hours > 0) ? $hours." H " : "").(($minutes > 0) ? $minutes." M" : "");
                /*$data["bus_detail"][$key]["travel_time"] = $travel_time;
                $data["bus_detail"][$key]["short_travel_time"] = $short_travel_time;*/
                $new_time_to_show = calculate_travel_duration($tmdiff);

                $data["bus_detail"][$key]["travel_time"] = $new_time_to_show;
                $data["bus_detail"][$key]["short_travel_time"] = $new_time_to_show;
                /************* total time required to travel calculation end**************/
               
                /************* For Travel Duration Filter Start**************/
                $travel_duration = "";
                // $travel_duration = ($hours <= 6) ? "six" : ($hours > 6 && $hours <= 12) ? "twelve" : ($hours > 12 && $hours <= 24) ? "twentyfour" : ($hours > 24) ? "more" : "";
                $travel_duration = "";
                if($days == 0 && $hours <= 6)
                {
                    $travel_duration = "six";
                }
                else if($days == 0 && $hours > 6 && $hours <= 12)
                {
                    $travel_duration = "twelve";
                }
                else if($days == 0 && $hours > 12 && $hours <= 24)
                {
                    $travel_duration = "twentyfour";
                }
                else if($days > 0)
                {
                    $travel_duration = "more";
                }

                $data["bus_detail"][$key]["travel_duration"] = $travel_duration;
                /************* For Travel Duration Filter End****************/

                if($value->fare > 0)
                {
                    if(strpos($value->fare, ","))
                    {
                        $fare_value_array = explode(",", $value->fare);

                        foreach ($fare_value_array as $fkey => $fvalue)
                        {
                          $min_max_fare[] = $fvalue;
                          // ($value->fare < $min_fare) ? $min_fare = $fvalue : ($fvalue > $max_fare) ? $max_fare = $fvalue : "";
                        }
                    }
                    else
                    {
                      $min_max_fare[] = $value->fare;
                      /*if($value->fare < $min_fare)
                      {
                        $min_fare = $value->fare;
                      }
                      else if($value->fare > $max_fare)
                      {
                        $max_fare = $value->fare;
                      }*/
                    }
                }

                /*if($data["bus_detail"][$key]['str_dep_tm_hm'] < $min_dep_tm)
                {
                    $min_dep_tm = $data["bus_detail"][$key]['str_dep_tm_hm'];
                }
                else if($data["bus_detail"][$key]['str_dep_tm_hm'] > $max_dep_tm)
                {
                    $max_dep_tm = $data["bus_detail"][$key]['str_dep_tm_hm'];
                }*/
                $min_max_deptm[] = $data["bus_detail"][$key]['str_dep_tm_hm'];


                /******* to claculate shift *******/
                foreach($time_range as $i => $shift_time){
                    if(strtotime($time_range[$i]['start']) <= strtotime($dept_tm_his) && strtotime($time_range[$i]['end']) >= strtotime($dept_tm_his)){
                        $shift = $shift_time["shift"];
                    }
                }

                /*$data["time_fare"]["bus_detail"]['min_fare'] = $min_fare;
                $data["time_fare"]["bus_detail"]['max_fare'] = $max_fare;

                $data["time_fare"]["bus_detail"]['min_dep_tm'] = $min_dep_tm;
                $data["time_fare"]["bus_detail"]['max_dep_tm'] = $max_dep_tm;*/

                $data["bus_detail"][$key]['shift'] = $shift;

                /*
                 *                        NOTICE IMP
                 *In the temp booking(controller -> booking.php function->temp_booking) 
                 *We are not getting all the required detail in seat availabilty.
                 *To AVOID API hit we are storing session here and destroying session in temp booking function.
                 *Discuss this and find better alternate solution.
                 */
                $session_store[$value->routeScheduleId]["mticket_allowed"] = $value->mTicketAllowed;;
                $session_store[$value->routeScheduleId]["partial_cancellation_allowed"] = $value->partialCancellationAllowed;
                $session_store[$value->routeScheduleId]["sch_departure_tm"] = $dept_tm_his;
                $session_store[$value->routeScheduleId]["arrival_time"] = $value->arrivalTime;
                $session_store[$value->routeScheduleId]["inventory_type"] = $value->inventoryType;
                $session_store[$value->routeScheduleId]["bus_type"] = $value->busType;
                $session_store[$value->routeScheduleId]["commision_percentage"] = $value->commPCT;
                $session_store[$value->routeScheduleId]["id_proof_required"] = $value->idProofRequired;
                $session_store[$value->routeScheduleId]["departure_datetime"] = $data["bus_detail"][$key]['dept_tm'];
              
                //session set to used on next pages(Till booking end);
                /********** end *******************/
              }
            }
          }
        }
        //end of foreach $response_decode->apiAvailableBuses

        if(!empty($min_max_fare))
        {
          $data["time_fare"]["bus_detail"]['min_fare'] = min($min_max_fare);
          $data["time_fare"]["bus_detail"]['max_fare'] = max($min_max_fare);
        }

        if(!empty($min_max_deptm))
        {
          $data["time_fare"]["bus_detail"]['min_dep_tm'] = min($min_max_deptm);
          $data["time_fare"]["bus_detail"]['max_dep_tm'] = max($min_max_deptm);
        }
        

        /*if(!isset($fields["is_return_journey"]))
        {
          $this->session->set_userdata('provider_available_bus', $session_store);
        }
        else if(isset($fields["is_return_journey"]) && $fields["is_return_journey"])
        {
          $this->session->set_userdata('provider_available_bus_return', $session_store);
        }*/
        
        /*
         *          NOTICE
         *Session is converted and fetch via table
         *Save bus services in etravels_bus_service table
         */
        $journey_date = date("Y-m-d", strtotime($fields['date_of_jour']));

        $bus_service_where = array("from" => $fields['from'], "to" => $fields['to'], "date" => $journey_date);
        $is_service_exists = $this->etravels_bus_service_model->where($bus_service_where)->find_all();
        
        $session_store_encode = json_encode($session_store);

        if(!$is_service_exists)
        {
          $etravels_save = array(
                                  'from' => $fields['from'],
                                  'to' => $fields['to'],
                                  'date' => $journey_date,
                                  'bus_service' => $session_store_encode,
                                );

          $etravels_bus_service_id = $this->etravels_bus_service_model->insert($etravels_save);

          $data["status"] = ($etravels_bus_service_id) ? "success" : "error";
        }
        else if($to_update)
        {

          $is_updated = $this->etravels_bus_service_model->update_where(array('from' => $fields['from'],'to' => $fields['to'],'date' => $journey_date),"",array("bus_service" => $session_store_encode));
          $is_true = false;
          /*$data["status"] = ($is_updated) ? "success" : "error";
          return $data;*/
        }
        else
        {
          $is_true = false;
          $data["status"] = "success";
        }
     
        /**************************************************/


        /*              VERY IMPORTANT NOTICE
         * Below code is for to make entry of bus types in temp_bus_types table.
         * We are purposely making seprate entry for each search to make more accurate bus types mapping.
         * So that each and every bus types will be covered.
         * After some days move this code to if else condition that will make entry only once for particular date and update
         * same logic implemented for entering bus services
         * we can move this conditon in above bus service  if else condition.
         * From temp_bus_types after mapping delete data.
         */
          $temp_bus_types = array_unique($temp_bus_types);
          $temp_bus_types_save = array(
                                    'date' => $journey_date,
                                    'provider_id' => 2,
                                    'bus_types' => json_encode($temp_bus_types),
                                  );

          $temp_bus_types_id = $this->temp_bus_types_model->insert($temp_bus_types_save);
      }
      else
      {
        $data["status"] = "error";
        $data["bus_detail"] = array();
        $data["time_fare"]["bus_detail"] = array("min_fare" => 0,
                                                  "max_fare" => 1,
                                                  "min_dep_tm" => 0,
                                                  "max_dep_tm" =>0
                                                 );
      }
    }
    else
    {
      $data["status"] = "error";
      $data["bus_detail"] = array();
      $data["time_fare"]["bus_detail"] = array("min_fare" => 0,
                                                  "max_fare" => 1,
                                                  "min_dep_tm" => 0,
                                                  "max_dep_tm" =>0
                                                 );
    }

    if($is_true)
    {
      log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$post_fields, 'Bus Service Request');
      log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$response, 'Bus Service Response Encoded');
      log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$response_decode, 'Bus Service Response Decoded');
    }
    $data['compare_commission_data'] = $compareCommissionData;
    return $data;
  }


  /* 
  * @author    : Suraj
  * @function  : get_seat_layout
  * @param     : Url, $fields, $auth;
  * @return    : returns provides the list of all available stations.
  * @method    : HTTP GET
  * @detail    : This API provides the list of available routes between the given source, destination stations and journey-date
  */
  public function get_seat_layout($fields)
  {
    $response = array();
    
    /*
     *                    NOTICE
     * We can fetch inventoryType from session instead of passing
     * Discuss this later. Do not remove below comment of $fields['inventory_type']
     */

    /*$etravels_data = $this->session->userdata();
    $fields['inventory_type'] = $etravels_data["provider_available_bus"][$fields['op_trip_no']]["inventory_type"];*/
    
    $post_fields = array("sourceCity" => $fields['from'],
                        "destinationCity" => $fields['to'],
                        "doj" => date("Y-m-d", strtotime($fields['date'])),
                        "inventoryType" => $fields['inventory_type'],
                        "routeScheduleId" => $fields['op_trip_no']
                        );

    // log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$post_fields, 'Seat Layout Request');

    // $api_response = getEtravelsmartApiData("getBusLayout",$post_fields);
    $api_response = $this->getBusLayoutApi($fields);

    log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$api_response, 'Seat Layout Response encoded');

    $response_decode = json_decode($api_response);
    
    log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$response_decode, 'Seat Layout Response');
    
    if(is_object($response_decode))
    {
      $api_status = $response_decode->apiStatus;

      if($api_status->success && strtolower($api_status->message) == "success")
      {
        $seat_layout_var  = $response_decode->seats;

        $seat_fare_detail = array();
        $total_available_seats = 0;
        $column_array = array();
        $row_array = array();
        $fare_array = array();
        $basic_fare_array = array();
        
        // return $response_decode->seats;
        foreach ($seat_layout_var as $key => $value)
        {
          $horizontal_seat  = false;
          $vertical_seat  = false;
          /*
           *                              VERY IMP NOTICE
           * POSITION OF COLUMN AND ROWS CHANGED HERE
           *Insert row and column number in array to find total number of columns and rows.
           *We can find this by this method also $seat_layout_var[count($seat_layout_var)-1]->row.
           *But this method fails when response come in unorder manner. Thats why calculated manually.
           */

          if(!in_array($value->row+1, $row_array))
          {
              $row_array[] = $value->row+1;
          }

          if(!in_array($value->column+1, $column_array))
          {
              $column_array[] = $value->column+1;
          }

          if(!in_array($value->totalFareWithTaxes, $fare_array) && $value->totalFareWithTaxes > 0)
          {
              $fare_array[] = $value->totalFareWithTaxes;
          }

          if(!in_array($value->fare, $basic_fare_array) && $value->fare > 0)
          {
              $basic_fare_array[] = $value->fare;
          }

          $pos = ($value->column+1) . "X" . ($value->row+1);
          $berth_no = $value->zIndex+1;
          $seat_type = ($value->ac) ? "AC" : "";
          $seat_type = ($seat_type != "" && $value->sleeper) ? $seat_type.",Sleeper" : ($value->sleeper) ? "Sleeper" : "";

          // $current_seat_no = trim(str_ireplace(array("(w)","(w"), "", $value->id));
          $current_seat_no = $value->id;

          $current_seat_discount = 0;
          $current_actual_seat_value = $value->fare;
          $current_actual_seat_value_with_tax = $value->totalFareWithTaxes;

          $current_seat_value = $value->fare;
          $current_seat_value_with_tax = $value->totalFareWithTaxes;
          $current_seat_service_tax = $value->serviceTaxAmount;
          $current_seat_operator_service_charge = $value->operatorServiceChargeAbsolute;

          /*******temp discount logic start******/
          $discount_array = array();
          
          /*******temp discount logic end******/
          
          /*******temp agent commission logic start******/
          
          $agent_commission = 0;
          
          
          /*******temp agent commission logic end******/
          /******************* Layout Parameteres & Description Start******************/
          $current_seat_type = "";

          //Normal,seater,semi-sleeper
          if($value->zIndex == 0 && $value->length == 1 && $value->width == 1)
          {
            if($value->ladiesSeat)
            {
              $current_seat_type = "ladies";
            }
          } 

          //vertical sleeper
          if($value->length == 1 && $value->width == 2)
          {
            if($value->ladiesSeat)
            {
              $current_seat_type = "vladies_sleeper";
            }
            else
            {
              $current_seat_type = "vsleeper";
            }

            $vertical_seat = true;
            // if(!$horizontal_seat)
            // {
            //   $horizontal_seat = true;
            // }
          }

          //horizontal sleeper
          if($value->length == 2 && $value->width == 1)
          {
            if($value->ladiesSeat)
            {
              $current_seat_type = "ladies_sleeper";
            }
            else
            {
              $current_seat_type = "sleeper";
            }

            $horizontal_seat = true;
            // if(!$horizontal_seat)
            // {
            //   $horizontal_seat = true;
            // }
          }
          
          /******************* Layout Parameteres & Description End********************/

          $seat_layout[$berth_no][$pos] = array(
                                                'seat_no' => $current_seat_no,
                                                'actual_seat_fare' => $current_actual_seat_value_with_tax,
                                                'actual_seat_basic_fare' => $current_actual_seat_value,
                                                'seat_fare' => $current_seat_value_with_tax,
                                                'seat_basic_fare' => $current_seat_value,
                                                'seat_service_tax_amt' => $current_seat_service_tax,
                                                'seat_service_tax_perc' => $value->serviceTaxPer,
                                                // 'op_service_charge_amt' => $value->operatorServiceChargeAbsolute,
                                                'op_service_charge_amt' => $current_seat_operator_service_charge,
                                                'op_service_charge_perc' => $value->operatorServiceChargePercent,
                                                'row_no' => $value->row+1,
                                                'col_no' => $value->column+1,
                                                'seat_type' => $seat_type,
                                                'berth_no' => $berth_no,
                                                'pos' => $pos,
                                                'zindex' => $value->zIndex,
                                                'length' => $value->length,
                                                'width' => $value->width,
                                                'is_ac' => $value->ac,
                                                'is_sleeper' => $value->sleeper,
                                                'is_ladies' => $value->ladiesSeat,
                                                'current_seat_type' => $current_seat_type,
                                                'horizontal_seat' => $horizontal_seat,
                                                'vertical_seat' => $vertical_seat,
                                                'seat_discount' => $current_seat_discount,
                                                'agent_comm_tds' => $agent_commission['tds_value_on_commission_amount'],
                                                'agent_commission' => ($agent_commission['agent_commission_amount']),
                                                'booked_by' => $value->bookedBy
                                                ); 

          if($value->available)
          {
            $seat_layout[$berth_no][$pos]["available"] = 'Y';
            $total_available_seats++;
          } 
          else 
          {
            $seat_layout[$berth_no][$pos]["available"] = 'N';
          }

          $seat_fare_detail[$berth_no][$current_seat_no] = $seat_layout[$berth_no][$pos];
        }


        $seatlayout_boarding_points = array();

        if($this->session->userdata('seatlayout_boarding_points'))
        {
          $session_seatlayout_boarding_points = $this->session->userdata('seatlayout_boarding_points');
        }
        else
        {
          $session_seatlayout_boarding_points = array();
        }

        if(!empty($response_decode->boardingPoints))
        {
          foreach($response_decode->boardingPoints as $bkey => $bvalue)
          {
            $bstop_name = explode(",", $bvalue->location);

            $boarding_detail_array = array(
                                    "bus_stop_name" => $bstop_name[0],
                                    "boarding_stop_name" => $bstop_name[0],

                                    "arrival_duration" => $bvalue->time,
                                    "location" => $bvalue->location,
                                    "boarding_point_id" => $bvalue->id,

                                    "is_boarding" => "Y",//This is for boarding point
                                    "is_alighting" => "N",//This is for dropping point
                                    "seq" => $bkey+1,
                                  );

            $seatlayout_boarding_points[] = $boarding_detail_array;
            $session_seatlayout_boarding_points[$fields['op_trip_no']][$bvalue->id] = $boarding_detail_array;
          }
          
          $this->session->set_userdata('seatlayout_boarding_points', $session_seatlayout_boarding_points);
        }
        else
        {
          log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log","boarding point empty", 'boarding point empty');
        }



        // $response['fare'] = max($fare_array);
       /* $basic_fare_array = asort($basic_fare_array);
        $fare_array       = asort($fare_array);*/

        /*                      Notice
         * ETS Provide Seat wise fare so thats why this below code is kept here
         */

        $response['actual_fare'] = implode("/",array_filter($basic_fare_array));//remove
        $response['actual_fare_array'] = implode("/",array_filter($fare_array));//remove
		
    
        array_walk($fare_array, 'etravelsmarfarediscount_new', $current_seat_discount );
        array_walk($basic_fare_array, 'etravelsmarfarediscount_new', $current_seat_discount );

        $response['fare'] = implode("/",array_filter($basic_fare_array));
        $response['fare_array'] = implode("/",array_filter($fare_array));
        $response['total_seats'] = count($seat_layout_var);
        $response['total_available_seats'] = $total_available_seats;
        $response['tot_rows'] = max($column_array);
        $response['tot_cols'] = max($row_array);
        $response['seat_layout'] = $seat_layout;
        $response['seat_fare_detail'] = $seat_fare_detail;
        $response['boarding_points'] = $seatlayout_boarding_points;
        $response['inventory_type'] = $response_decode->inventoryType;
        $response['update_travel_destn'] = "";
        $response['stop_detail']  = "";

        // $response['horizontal_seat']  = $horizontal_seat;
        // $response['vertical_seat']  = $vertical_seat;

        $response['trip_no']  = $fields['op_trip_no'];
        $response['flag'] = '@#success#@';
      }
      else
      {
        $response['flag'] = '@#error#@';
        $response['error'] = "Please contact customer support.";
        $response['fare'] = 0;
      }
    }
    else
    {
      log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log","Response not came from API i.e. Etravelsmart", 'Seat Layout Response');
      log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$api_response, 'Seat Layout Response (Response not came from API i.e. Etravelsmart)');
      $response['flag'] = '@#error#@';
      $response['error'] = "Please contact customer support1.";
      $response['fare'] = 0;
    }

    // log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$response, 'Seat Layout');
    return $response;
  }



  /* 
  * @author    : Suraj
  * @function  : getSeatAvailability
  * @param     : 
  * @return    : returns seat details.
  * @method    : HTTP GET
  * @detail    : 
  */
  public function getSeatAvailability($fields)
  {
    $response = array();
    $etravels_data = array();

    /*              NOTICE
     *BELOW MANUPULATION STARTED.
     *PREVIOUSLY WE ARE STARTED USING DATA FROM SESSION. 
     *HERE WE ARE FETCHING FROM DATABASE
     *
     */
    $fetched_etravels_data = array();
    $etravel_input = array("from" => $fields["from"],
                           "to" => $fields["to"],
                           "date" => date("Y-m-d",strtotime($fields["date"]))
                         );

    $e_data =  $this->etravels_bus_service_model->where($etravel_input)->find_all();
    $etravels_data = json_decode($e_data[0]->bus_service,true);

    $fetched_etravels_data[$fields["trip_no"]] = (array_key_exists($fields["trip_no"], $etravels_data)) ? $etravels_data[$fields["trip_no"]] : "";

    /*              NOTICE
     *MANUPULATION ENDED.
     */

    if(!empty($fetched_etravels_data[$fields["trip_no"]]))
    {
      if(isset($fields["is_return_journey"]) && $fields["is_return_journey"])
      {
        $this->session->set_userdata('provider_available_bus_return', $fetched_etravels_data);
        // $etravels_data = $this->session->userdata("provider_available_bus_return");
      }
      else
      {
        $this->session->set_userdata('provider_available_bus', $fetched_etravels_data);
        // $etravels_data = $this->session->userdata("etravels_available_bus");
      }
      
      if(!empty($etravels_data) && isset($etravels_data[$fields['trip_no']]["inventory_type"]))
      {

        $fields['inventory_type'] = $etravels_data[$fields['trip_no']]["inventory_type"];
        
        $is_true = false;
        $available_seats = array(); 
        $available_seat_fare = array(); 
        $seats_details = array(); 
        $post_fields = array("sourceCity" => urlencode($fields['from']),
                            "destinationCity" => urlencode($fields['to']),
                            "doj" => date("Y-m-d", strtotime($fields['date'])),
                            "inventoryType" => $fields['inventory_type'],
                            "routeScheduleId" => $fields['trip_no']
                            );

        $api_response = getEtravelsmartApiData("getBusLayout",$post_fields);

        // log_data("seat_availability",$api_response,"Etravelsmart api response");
        $response_decode = json_decode($api_response);
        // log_data("seat_availability",$response_decode,"Etravelsmart api response Decoded");
        
        if(is_object($response_decode))
        {
          $api_status = $response_decode->apiStatus;
        
          if($api_status->success && strtolower($api_status->message) == "success")
          {

            $seat_layout_var  = $response_decode->seats;
            $seat_fare_detail = array();
            $berth_array = array();
            $fare_array = array();

            foreach ($seat_layout_var as $key => $value)
            {
              // $current_seat_no = trim(str_ireplace(array("(w)","(w"), "", $value->id));
              $current_seat_no = $value->id;
              $berth_no = $value->zIndex+1;
              if(!in_array($berth_no, $berth_array))
              {
                  $berth_array[] = $berth_no;
              }

              if(!in_array($value->totalFareWithTaxes, $fare_array))
              {
                  $fare_array[] = $value->totalFareWithTaxes;
              }

              if($value->available)
              {
                $available_seats[$berth_no][] = $current_seat_no;
              }

              //Seat Fare Details
              $available_seat_fare[$berth_no][$current_seat_no]["total_fare_with_tax"] = $value->totalFareWithTaxes;
              $available_seat_fare[$berth_no][$current_seat_no]["basic_fare"] = $value->fare;
              $available_seat_fare[$berth_no][$current_seat_no]["service_tax_per"] = $value->serviceTaxPer;
              $available_seat_fare[$berth_no][$current_seat_no]["service_tax_amount"] = $value->serviceTaxAmount;
              $available_seat_fare[$berth_no][$current_seat_no]["op_service_charge_per"] = $value->operatorServiceChargePercent;
              $available_seat_fare[$berth_no][$current_seat_no]["op_service_charge_amount"] = $value->operatorServiceChargeAbsolute;


              $seats_details[$berth_no][$current_seat_no] = array(
                                                                  "is_ladies_seat" => $value->ladiesSeat,
                                                                  "is_ac" => $value->ac,
                                                                  "is_sleeper" => $value->sleeper
                                                                );
            }
            //end of for each

            $response['total_berth'] = max($berth_array);
            $response['trip_no']  = $fields['trip_no'];

            /*Data required for temp booking*/
            $response["SeatAvailabilityResponse"]["service_id"] = $fields["trip_no"];
            $response["SeatAvailabilityResponse"]["inventory_type"] = $fields["inventory_type"];

            // $response["SeatAvailabilityResponse"]["fromStopId"] = $fields["from"];
            // $response["SeatAvailabilityResponse"]["toStopId"] = $fields["to"];
            $response["SeatAvailabilityResponse"]["dateOfJourney"] = str_replace("-","/",$fields["date"]);
            $response["SeatAvailabilityResponse"]["fare"] = $fare_array;
            $response["SeatAvailabilityResponse"]["childFare"] = $fare_array;
            $response["SeatAvailabilityResponse"]["sleeperFare"] = 0.00;
            $response["SeatAvailabilityResponse"]['availableSeats']['availableSeat'] =  $available_seats;
            $response["SeatAvailabilityResponse"]['available_seat_fare'] =  $available_seat_fare;
            $response["SeatAvailabilityResponse"]["seats_details"] =  $seats_details;

            $response["error"] = array();

            $response["trip_data"]["op_id"] = "";
            $response["trip_data"]["user_type"] = "";
            $response["trip_data"]["op_trip_no"] = $fields['trip_no'];
            $response["trip_data"]["inventory_type"] = $fields["inventory_type"];
            $response["trip_data"]["is_child_concession"] = "";
            $response["trip_data"]["id_proof_required"] = $fetched_etravels_data[$fields['trip_no']]['id_proof_required'];

            $response["trip_data"]["sch_departure_tm"] = $etravels_data[$fields['trip_no']]["sch_departure_tm"];
            //Not getting arrival date. So considered current date only

            $departure_his_str = strtotime(date("h:i A", strtotime($etravels_data[$fields['trip_no']]["sch_departure_tm"])));
            $arrival_his_str = strtotime(date("h:i A", strtotime($etravels_data[$fields['trip_no']]["arrival_time"])));

            
            if($departure_his_str < $arrival_his_str)
            {
              //Same arrival day
              $response["trip_data"]["arrival_time"] = date("Y-m-d", strtotime($fields['date']))." ".date("H:i:s", strtotime($etravels_data[$fields['trip_no']]["arrival_time"]));
            }
            else if(($departure_his_str > $arrival_his_str) || ($departure_his_str == $arrival_his_str) )
            {
              //Add one day to arrival date 
              $response["trip_data"]["arrival_time"] = date("Y-m-d", strtotime($fields['date'] . ' +1 day'))." ".date("H:i:s", strtotime($etravels_data[$fields['trip_no']]["arrival_time"]));
            }

            

            $seat_already_booked = array();
            if (isset($response["total_berth"]) && $response["total_berth"] != "")
            {
                for ($i=1; $i <= $response["total_berth"]; $i++)
                {
                    if(isset($fields['seats'][$i]) && !empty($fields['seats'][$i]) && isset($available_seats[$i]) && !empty($available_seats[$i]) )
                    {
                        if(count(array_intersect($fields['seats'][$i], $available_seats[$i])) === count($fields['seats'][$i]))
                        {
                            $is_true = true;
                        }
                        else
                        {
                            $is_true = false;
                            $seat_already_booked = array_diff($fields['seats'][$i],$available_seats[$i]);
                            break;
                        }
                    }
                }
            }

            if($is_true)
            {
                $response['flag'] = 'success';
            }
            else
            {
                $response['flag'] = 'error';
                $response['seat_already_booked'] = $seat_already_booked;
                load_front_view(SEAT_UNAVAILABLE, $response);
            }

          }
          else
          {
            $response['flag'] = 'error';
            $response['error'] = "Please contact customer support.";
          }
        }
        else
        {
          $response['flag'] = 'error';
          $response['error'] = "Please contact customer support.";
        }
      }
      else
      {
        $response['flag'] = 'error';
        $response['error'] = "Please contact customer support.";
      }
    }
    else
    {

      $get_service_again = array("from" => $fields["from"],
                                 "to" => $fields["to"],
                                 "date" => date("Y-m-d",strtotime($fields["date"])),
                                 "date_of_jour" => date("Y-m-d",strtotime($fields["date"])),
                                 "getSeatAvailability_fields" => $fields
                                 );

      $busdata = $this->getAvailableBuses($get_service_again, true);

      $response['flag'] = 'error';
      $response['error'] = "Please contact customer support.";
    }
    
    return $response;
  }



  /* 
  * @author    : Suraj
  * @function  : blockTicket
  * @param     : 
  * @return    : apiStatus, inventoryType, blockTicketKey
  * @method    : HTTP POST
  * @Details   : This API is used to block the ticket for a period of 10 minutes.
  */
  public function blockTicket($fields)
  {

    $data = array();
    $boardingPoint = array();
    $available_detail = $fields["available_detail"];
    $seats_details = $available_detail["seats_details"];
    $available_seat_fare = $fields["available_detail"]["available_seat_fare"];
    $binput = $fields["input"];

    if(isset($binput["is_return_journey"]) && $binput["is_return_journey"])
    {
      $etravels_available_bus = $this->session->userdata('provider_available_bus_return');
    }
    else
    {
      $etravels_available_bus = $this->session->userdata('provider_available_bus');
    }

    $input = $fields["input"];
    $seats = $fields["input"]["seat"];
    $service_detail = $etravels_available_bus[$available_detail["service_id"]];


    log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$available_detail["inventory_type"], 'Inventory Type Block Ticket');

    // MANAGE BOARDING POINTS ACCORDING TO INVENTORY_TYPE
    if($available_detail["inventory_type"] == 0 || $available_detail["inventory_type"] == 1)
    {
      if(isset($service_detail["barr"][$available_detail["bsd"]]))
      {
        $boarding_point_detail = $service_detail["barr"][$available_detail["bsd"]];

        $boardingPoint = array("id" => $boarding_point_detail["boarding_point_id"],
                               "location" => $boarding_point_detail["location"],
                               "time" => $boarding_point_detail["arrival_duration"]
                              );

        log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$boarding_point_detail, 'Boarding Point Detail Found (Inventory Type 0 or 1 Found)');

      }
      else
      {
        $boardingPoint = array("id" => "",
                               "location" => $available_detail["from"],
                               "time" => ""
                              );

        log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$boardingPoint, 'Boarding Point Detail Not Found(Inventory Type 0 or 1 Found)');
      }
    }
    else
    {
      $seatlayout_boarding_points = $this->session->userdata('seatlayout_boarding_points');

      log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$seatlayout_boarding_points, 'session seatlayout_boarding_points');

      if(isset($seatlayout_boarding_points[$available_detail["service_id"]][$available_detail["bsd"]]))
      {
        $boarding_point_detail = $seatlayout_boarding_points[$available_detail["service_id"]][$available_detail["bsd"]];

        $boardingPoint = array("id" => $boarding_point_detail["boarding_point_id"],
                               "location" => $boarding_point_detail["location"],
                               "time" => $boarding_point_detail["arrival_duration"]
                              );

        
        log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$boarding_point_detail, 'Boarding Point Detail Found (Inventory Type More Than 1)');

      }
      else
      {
        $boardingPoint = array("id" => "",
                               "location" => $available_detail["from"],
                               "time" => ""
                              );

        log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$boardingPoint, 'Boarding Point Detail Not Found (Inventory Type More Than 1)');

      }
    }
    //END OF MANAGE BOARDING POINTS ACCORDING TO INVENTORY_TYPE
    
    

    $passanger_details = array();
    $fname_lname = array();
    $is_primary = true;
    $total_fare = 0;
    foreach ($seats as $berth_no => $seatdetails)
    {
      foreach ($seatdetails as $seat_no => $seat_detail)
      {
        $total_fare += $available_seat_fare[$berth_no][$seat_no]["total_fare_with_tax"];
        $fname_lname = explode(" ", $seat_detail["name"]);
        $passanger_details[] = array("age" => $seat_detail["age"],
                                     "name" => $fname_lname[0],
                                     "seatNbr" => $seat_no,
                                     "sex" => $seat_detail["gender"],
                                     "fare" => $available_seat_fare[$berth_no][$seat_no]["basic_fare"],
                                     "totalFareWithTaxes" => $available_seat_fare[$berth_no][$seat_no]["total_fare_with_tax"], //This is per seatwise fare with tax
                                     "ladiesSeat" => isset($seats_details[$berth_no][$seat_no]["is_ladies_seat"]) ? $seats_details[$berth_no][$seat_no]["is_ladies_seat"] : false,
                                     "lastName" => $fname_lname[count($fname_lname)-1], //discuss this
                                     "mobile" => $input["mobile_no"], //discuss this
                                     "title" => (strtolower($seat_detail["gender"]) == "m") ? "Mr" : "Mrs",
                                     "email" => $input["email_id"], //discuss this
                                     "idType" => $input['id_type'],
                                     "idNumber" => $input['id_card_number'],
                                     "nameOnId" => $input['name_on_id'],
                                     "primary" => $is_primary,
                                     "ac" => isset($seats_details[$berth_no][$seat_no]["is_ac"]) ? $seats_details[$berth_no][$seat_no]["is_ac"] : false,
                                     "sleeper" => isset($seats_details[$berth_no][$seat_no]["is_sleeper"]) ? $seats_details[$berth_no][$seat_no]["is_sleeper"] : false
                                    );

        $is_primary = false; //DO NOT UNCOMMENT. DICUSS THIS.
      }  
    }

    /*
     *NOTICE : TO get first name of upper or lower deck
     */
    if(isset($seats[1]))
    {
      $get_pname = $seats[1];
    }
    else if(isset($seats[2]))
    {
      $get_pname = $seats[2];
    }
    else
    {
      $get_pname = $seats[1];
    }

    $firstname_lastname = explode(" ", $get_pname[array_keys($get_pname)[0]]["name"]);
    
    // $firstname_lastname = explode(" ", $seats[1][array_keys($seats[1])[0]]["name"]);

    $post_fields  = array("sourceCity" => $available_detail["from"],
                          "destinationCity" => $available_detail["to"],
                          "doj" => date("Y-m-d",strtotime($available_detail["date"])),
                          "routeScheduleId" => $available_detail["service_id"],
                          "boardingPoint" => $boardingPoint,
                          "customerName" => $firstname_lastname[0],
                          "customerLastName" => $firstname_lastname[count($firstname_lastname)-1],
                          "customerEmail" => $input["email_id"],
                          "customerPhone" => $input["mobile_no"],
                          "emergencyPhNumber" => $input["mobile_no"],
                          "customerAddress" => "NA",
                          "blockSeatPaxDetails" => $passanger_details,
                          "inventoryType" => $service_detail["inventory_type"]
                         );

    // log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$post_fields, 'Block Ticket Array Request');

    // $post_fields = json_encode($post_fields);

    // log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$post_fields, 'Block Ticket Json encode Request');

    // $response = getEtravelsmartApiData("blockTicket",$post_fields ,"POST");
    // 
    $response = $this->blockTicketApi($post_fields);
    $response_decode = json_decode($response);

    $data_to_log = [
                    "ticket_id" => isset($fields["ticket_id"]) ? $fields["ticket_id"] : "",
                    "request" => json_encode($post_fields),
                    "response" => $response,
                    "type" => "temp_booking"
                   ];

    insert_request_response_log($data_to_log);

    log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$response_decode, 'Block Ticket Json encode Response');
    
    if(is_object($response_decode))
    {
      $api_status = $response_decode->apiStatus;

      if($api_status->success && strtolower($api_status->message) == "success")
      {
        $data["is_success"] = $api_status->success;
        $data["status"] = strtolower($api_status->message);
        $data["blockKey"] = $response_decode->blockTicketKey;
        // return $response_decode;
      }
      else
      {
        $data["is_success"] = false;
        $data["status"] = "error";
        $data["error_message"] = $api_status->message;
      }
    }
    else
    {
      log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log","Response not came from API i.e. Etravelsmart", 'Block Ticket Response');
      log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$response, 'Etravelsmart Message (Response not came from API i.e. Etravelsmart)');
      $data["is_success"] = false;
      $data["status"] = "error";
      $data["error_message"] = $api_status->message;
    }

    // log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$data, 'Block Ticket');
    $data["boarding_point"] = $boardingPoint;
    return $data;
  }


 /* 
  * @author    : Suraj
  * @function  : do_`ing
  * @param     : 
  * @return    : apiStatus, inventoryType, blockTicketKey
  * @method    : HTTP POST
  * @Details   : This API is used to block the ticket for a period of 10 minutes.
  */
  public function do_temp_booking($fields)
  {
	      
    $input = $fields["input"];
    $seats = $fields["input"]["seat"];
    $email = $input["email_id"];
    $mobile_no = $input["mobile_no"];
    $agent_email = $input["agent_email_id"];
    $agent_mobile_no = $input["agent_mobile_no"];
    $available_detail = $fields["available_detail"];
    $available_seat_fare = $fields["available_detail"]["available_seat_fare"];
    $seatlayout_boarding_points = $this->session->userdata('seatlayout_boarding_points');

    if(isset($input["is_return_journey"]) && $input["is_return_journey"]) {
      $etravels_available_bus = $this->session->userdata('provider_available_bus_return');
    }
    else {
      $etravels_available_bus = $this->session->userdata('provider_available_bus');
    }
    
    $service_detail = $etravels_available_bus[$available_detail["service_id"]];

    $tot_seat = 0;
    foreach ($seats as $berth => $seatvalue) {
      $tot_seat += count($seatvalue);
    }

    if($tot_seat > 6) {
      redirect(base_url());
    }
          
    $dept_boarding_time = $available_detail["date"]." ".$available_detail["sch_dept_time"];
    $dept_boarding_time = date("Y-m-d H:i:s", strtotime($dept_boarding_time));

    log_data("etravelsmart_trash/".date("M")."/service_".date("d-m-Y")."_log.log",$available_detail, 'Temp Booking available_detail');
    log_data("etravelsmart_trash/".date("M")."/service_".date("d-m-Y")."_log.log",$service_detail, 'Temp Booking service_detail');

    if(isset($available_detail["inventory_type"])) {
      if($available_detail["inventory_type"] == 0 || $available_detail["inventory_type"] == 1) {
        $boarding_stop_time = date("Y-m-d", strtotime($available_detail["date"]))." ".date("H:i:s",strtotime($service_detail["barr"][$available_detail["bsd"]]["arrival_duration"]));
      }
      else if($available_detail["inventory_type"] > 1) {
        $boarding_point_detail = $seatlayout_boarding_points[$available_detail["service_id"]][$available_detail["bsd"]];
        $boarding_stop_time = date("Y-m-d", strtotime($available_detail["date"]))." ".date("H:i:s",strtotime($boarding_point_detail["arrival_duration"]));
      }

      /**************** Assumption For Dropping Stop Time Start *************************/
      $dropping_stop_time = "";

      if(isset($available_detail["dsd"]) && $available_detail["dsd"] != "") {
        $departure_his_str  = strtotime($service_detail["sch_departure_tm"]);
        $arrival_his_str    = strtotime($service_detail["darr"][$available_detail["dsd"]]["arrival_duration"]);
        if($departure_his_str < $arrival_his_str) {
          //Same arrival day
          $dropping_stop_time = date("Y-m-d", strtotime($service_detail['departure_datetime']))." ".date("H:i:s", $arrival_his_str);
        }
        else if(($departure_his_str > $arrival_his_str) || ($departure_his_str == $arrival_his_str) ) {
          //Add one day to arrival date 
          $dropping_stop_time = date("Y-m-d", strtotime($service_detail['departure_datetime'] . ' +1 day'))." ".date("H:i:s", $arrival_his_str);
        }
        /**************** Assumption For Dropping Stop Time End *************************/
        
        if($dropping_stop_time == "") {
          $dropping_stop_time = $available_detail['arrival_time'];
        }
      }
      else {
        $dropping_stop_time = $available_detail['arrival_time'];
      }
      
      // Add into tickets table
      $this->tickets_model->ticket_type = 1; // For In future
      $this->tickets_model->request_type = 'B';
      $this->tickets_model->ticket_status = 'A';
      $this->tickets_model->bus_service_no = "";
      $this->tickets_model->dept_time = $dept_boarding_time;
      $this->tickets_model->boarding_time = $boarding_stop_time;
      $this->tickets_model->alighting_time = $available_detail['arrival_time'];
      $this->tickets_model->droping_time = $dropping_stop_time;
      $this->tickets_model->from_stop_cd = "";
      $this->tickets_model->till_stop_cd = "";
      $this->tickets_model->from_stop_name = $available_detail['from'];
      $this->tickets_model->till_stop_name = $available_detail['to'];
      $this->tickets_model->boarding_stop_name = $available_detail['boarding_stop_name'];
      $this->tickets_model->destination_stop_name = $available_detail['destination_stop_name'];
      $this->tickets_model->boarding_stop_cd = $available_detail['boarding_stop_cd'];
      $this->tickets_model->destination_stop_cd = $available_detail['destination_stop_cd'];
      $this->tickets_model->op_id = $available_detail['op_id'];
      $this->tickets_model->provider_id = $available_detail['provider_id'];
      $this->tickets_model->provider_type = $available_detail['provider_type'];
      $this->tickets_model->booked_from = "etravelsmart";
      $this->tickets_model->inventory_type = $service_detail['inventory_type'];
      $this->tickets_model->route_schedule_id = $available_detail['service_id'];
      $this->tickets_model->commision_percentage = $service_detail['commision_percentage'];
      $this->tickets_model->partial_cancellation = ($service_detail['partial_cancellation_allowed']) ? 1 : 0;
      $this->tickets_model->mticket = ($service_detail['mticket_allowed']) ? 1 : 0;
      $this->tickets_model->num_passgr = $tot_seat;
      $this->tickets_model->cancellation_rate = 0; 
      $this->tickets_model->issue_time = date("Y-m-d H:i:s");
      $this->tickets_model->pay_id = 1;
      $this->tickets_model->session_no = 1;
      $this->tickets_model->op_name = $available_detail['op_name'];
      $this->tickets_model->user_email_id = $email;
      $this->tickets_model->mobile_no = $mobile_no;
      $this->tickets_model->agent_email_id = $agent_email;
      $this->tickets_model->agent_mobile_no = $agent_mobile_no;
      $this->tickets_model->id_proof_required = $available_detail['id_proof_required'];
      $this->tickets_model->inserted_by = $this->session->userdata("booking_user_id");
      $this->tickets_model->role_id = (!empty($this->session->userdata("role_id"))) ? $this->session->userdata("role_id") : 0;
      $this->tickets_model->booked_by = (!empty($this->session->userdata("role_name"))) ? strtolower($this->session->userdata("role_name")) : "guest_user";
      $this->tickets_model->bus_type = (isset($available_detail['bus_type']['op_bus_type_name'])) ? $available_detail['bus_type']['op_bus_type_name'] : "";
      $this->tickets_model->status = 'Y';
      $this->tickets_model->ip_address = $this->input->ip_address();
      $this->tickets_model->tds_per = $this->config->item('tds');
      $ticket_id = $this->tickets_model->save();
      
      $total_ticket_discount = 0;
      $total_service_tax_amount = 0;
      $total_basic_fare = 0;
      $total_op_service_charge_amount = 0;
      $tickets_total_op_service_charge_amount = 0;
      $tickets_total_service_tax = 0;
      $tot_fare = 0;
      if($ticket_id > 0) {
          $psgr_no = 1;
          foreach ($seats as $berth_no => $seat_detail) {
            foreach ($seat_detail as $cseat_no => $value) {
              $seatfare = 0;
              $fare_detail_tax = $available_seat_fare[$berth_no][$cseat_no];
              $seatfare = $fare_detail_tax["total_fare_with_tax"];

              $discount_array = array();
              

              $tickets_total_op_service_charge_amount += $fare_detail_tax["op_service_charge_amount"];
              $tickets_total_service_tax += $fare_detail_tax["service_tax_amount"];
                
              /*
               *                  NOTICE : VERY IMP LOGIC FOR FARE
               *if is_child_concession in op_bus_types is Y then assign child seat fare
               *if is_child_concession in op_bus_types is N then assign adult seat fare 
               */
              /*if(isset($available_detail['is_child_concession']) && $available_detail['is_child_concession'] == "Y")
              {
                $seatfare = ($value['age'] <= 12) ? $child_seat_fare : $seatfare;
              }
              else
              {
                $seatfare = $fare;
              }*/

              //calculate commission amount
              $op_commission_type = 'percentage';
              $op_commission_per = $service_detail['commision_percentage'];
              $op_commission = round(($fare_detail_tax["basic_fare"] * (float)($service_detail['commision_percentage']/100)),5);
              $tds_on_commission = round($op_commission * ((float)($this->config->item('tds')/100)),5);
              $final_commission = $op_commission - $tds_on_commission;

              $tot_fare += $seatfare;
              $total_basic_fare += $fare_detail_tax["basic_fare"];
              // Add into ticket_details table
              $this->ticket_details_model->ticket_id = $ticket_id;
              $this->ticket_details_model->psgr_no = $psgr_no;
              $this->ticket_details_model->psgr_name = $value['name'];
              $this->ticket_details_model->psgr_age = $value['age'];
              $this->ticket_details_model->psgr_sex = $value['gender'];
              $this->ticket_details_model->psgr_type = "A";
              $this->ticket_details_model->concession_cd = 1; // if exists /in case of rsrtc 60 pass ct 
              $this->ticket_details_model->concession_rate = 1; // from api /if exists /in case of rsrtc 60 pass ct 
              $this->ticket_details_model->discount = 1; // if available from api
              $this->ticket_details_model->is_home_state_only = 1; // Y or No //not required now 
              $this->ticket_details_model->adult_basic_fare = $fare_detail_tax["basic_fare"];
              $this->ticket_details_model->child_basic_fare =  0.00;
              $this->ticket_details_model->fare_acc =  0.00;
              $this->ticket_details_model->fare_hr =  0.00;
              $this->ticket_details_model->fare_it =  0.00;
              $this->ticket_details_model->fare_octroi =  0.00;
              $this->ticket_details_model->fare_other = 0.00;
              $this->ticket_details_model->fare_reservationCharge = 0.00;
              $this->ticket_details_model->fare_sleeperCharge = 0.00;
              $this->ticket_details_model->fare_toll = 0.00;
              $this->ticket_details_model->fare_convey_charge = 0.00;
              $this->ticket_details_model->service_tax_per = $fare_detail_tax["service_tax_per"];
              $this->ticket_details_model->service_tax = $fare_detail_tax["service_tax_amount"];
              $this->ticket_details_model->op_service_charge_per = $fare_detail_tax["op_service_charge_per"];
              $this->ticket_details_model->op_service_charge = $fare_detail_tax["op_service_charge_amount"];
              $this->ticket_details_model->fare_amt = $seatfare;
              $this->ticket_details_model->seat_no = $cseat_no;
              $this->ticket_details_model->berth_no = $berth_no;
              $this->ticket_details_model->seat_status = 'Y';
              $this->ticket_details_model->concession_proff = 1;  // if cct then required // voter id
              $this->ticket_details_model->proff_detail = 1; // if varified
              $this->ticket_details_model->status = 'Y';
              $this->ticket_details_model->type = $op_commission_type;
              $this->ticket_details_model->commission_percent = $op_commission_per;
              $this->ticket_details_model->op_commission =  $op_commission;
              $this->ticket_details_model->tds_on_commission =  $tds_on_commission;
              $this->ticket_details_model->final_commission =  $final_commission;
              
              $psgr_no++;
              
              $ticket_details_id = $this->ticket_details_model->save();

            }
          }

          //if id proof required
          if($input['id_type'] != "" && $input['id_card_number'] != "" && $input['name_on_id'] != "") {
            $idproof_save = array(
                                'ticket_id' => $ticket_id,
                                'id_type' => $input['id_type'],
                                'id_number' => $input['id_card_number'],
                                'name' => $input['name_on_id']
                               );
            $ticket_idproof_id = $this->ticket_id_proof_model->insert($idproof_save);
          }
          
          $fields["ticket_id"] = $ticket_id;
          $data['ticket_id'] = $ticket_id;
          $tb_data = $this->blockTicket($fields);
          if(!empty($tb_data)) {
              $temp_booking_status = $tb_data['status'];
              $is_success = $tb_data['is_success'];

              if (!$is_success && $temp_booking_status == 'error') {
                  $data['block_key'] = "";
                  $data['status'] = $tb_data['status'];
                  $data['error'] = $tb_data['error_message'];
                  $data['payment_confirm_booking_status'] = "error";
              } 
              else if($is_success && $temp_booking_status == 'success') {
                $total_convey_charge = 0;
                $extra_charges_value = 0;
                $convey_charge_name = "";
                $convey_charge_value = 0;
                $total_fare_without_discount = $tot_fare+$extra_charges_value;
				

                $data['discount_value'] = $total_ticket_discount;
                $final_total_amount = $total_fare_without_discount-$total_ticket_discount;
                /*******temp discount logic end******/

                $tax_charges = array();

                $data['status'] = $temp_booking_status;
                $this->tickets_model->ticket_id = $ticket_id;
                $this->tickets_model->ticket_ref_no = $tb_data['blockKey'];
                $this->tickets_model->pickup_address = $tb_data['boarding_point']['location'];

              
                $this->tickets_model->total_basic_amount = $total_basic_fare;
                $this->tickets_model->op_service_charge = $tickets_total_op_service_charge_amount;
                $this->tickets_model->service_tax = $tickets_total_service_tax;
                $this->tickets_model->tot_fare_amt = $tot_fare;
                $this->tickets_model->tot_fare_amt_with_tax = $final_total_amount;
                $this->tickets_model->total_fare_without_discount = $total_fare_without_discount;
                $this->tickets_model->transaction_status = "temp_booked";
                $this->tickets_model->save();

                /*
                *NOTICE: MOST IMP
                *
                *DISCUSS THIS POINT.
                *API IS NOT PROVIDING BLOCK TIME AFTER SEAT BLOCK.
                *AFTER DISCUSSION REMOVE HARDCODED NUMBER 5.
                */
                $tb_data['blockTime'] = 5;

                $currentTime = strtotime(date("Y-m-d H:i:s"));
                $timeBlock = $currentTime+(60*$tb_data['blockTime']);
                $timeToBlock = date("Y-m-d H:i:s", $timeBlock);

                $data['block_key'] = $tb_data['blockKey'];
                $data['block_time_minutes'] = $tb_data['blockTime'];
                $data['block_current_time'] = $currentTime;
                $data['time_to_block'] = $timeToBlock;
                $data['ttb_year'] = date("Y", $timeBlock);
                $data['ttb_month'] = date("m", $timeBlock);
                $data['ttb_day'] = date("d", $timeBlock);
                $data['ttb_hours'] = date("H", $timeBlock);
                $data['ttb_minutes'] = date("i", $timeBlock);
                $data['ttb_seconds'] = date("s", $timeBlock);
                $data['total_fare'] = $final_total_amount;
                $data["total_fare_without_discount"] =  $total_fare_without_discount;
                $data["ticket_fare"] =  $tot_fare;
                $data["total_seats"] = $tot_seat;
                $data["tax_charges"] =  $tax_charges;
                $data['payment_confirm_booking_status'] = "success";
                $data['ticket_id'] = $ticket_id;
              }
          }
          else {
            $data['payment_confirm_booking_status'] = "error";
          }
      }
      else {
        $data['payment_confirm_booking_status'] = "error";
      }
    }
    else {
      log_data("etravelsmart_trash/".date("M")."/service_".date("d-m-Y")."_log.log","Inventory type error", 'Temp Booking inventory type');
      $data['payment_confirm_booking_status'] = "error";
    }
    return $data;
  }//END OF Temp Booking



 /* 
  * @author    : Suraj
  * @function  : final_booking
  * @param     : 
  * @return    : 
  * @method    : 
  * @detail    : 
  */
  function final_booking($api_data, $data)
  {
    $final_response = array();
    $tbdata = $this->doconfirm_booking($api_data);
    /************* Actual ticket booking End Here1 ***********/

    if(trim(strtolower($tbdata['status'])) == 'success') 
    {   
        $onwards_ticket_ref_no = $data["ticket_ref_no"];
        
        $data["ticket_pnr"] = $tbdata["pnr"];

        /*
         *This is to update ticket table
         */
          $transaction = array('transaction_status' => "success",
                                'payment_by' => $api_data["payment_by"],
                                'payment_mode' => $data["pg_response"]["payment_mode"],
                                'commision_percentage' => (isset($tbdata['commision_percentage'])) ? $tbdata['commision_percentage'] : "", 
                                'api_ticket_no' => (isset($tbdata['api_ticket_no'])) ? $tbdata['api_ticket_no'] : "", 
                                'cancellation_policy' => (isset($tbdata['cancellation_policy'])) ? $tbdata['cancellation_policy'] : "" 
                              );

          $this->tickets_model->update($data["ticket_id"],$transaction);

        //common model
        $pg_success_response = $this->common_model->transaction_process_api($data);

        /*For Reconcellation process*/
        $this->saveReconcellations($tbdata['api_ticket_no'], $data["ticket_id"], "success");
        $final_response["transaction_process_response"] = $pg_success_response;
        //make this function common afterwards. Because this is used 2 times (1 times in pg and 1 time in wallet)
        if(trim($pg_success_response["msg_type"]) == "success")
        {
          //To check if return journey
          // $rpt_data = $this->return_ticket_mapping_model->get_rpt_mapping($data["pg_response"]["udf2"]);
          $rpt_data = $api_data["rpt_data"];
          if(count($rpt_data))
          {
              $onward_ticket_id = $pg_success_response["ticket_id"];

              //to track return journey.
              $rtransaction = array('is_return_journey' => "1",
                                    'return_ticket_id' => $rpt_data[0]->ticket_id
                                  );

              $this->tickets_model->update($onward_ticket_id,$rtransaction);

              /************* Actual ticket booking Start Here ***********/
              $rapi_data['ticket_id']      = str_pad($rpt_data[0]->ticket_id, 8, '0', STR_PAD_LEFT);
              $rapi_data['ticket_ref_no']  = $rpt_data[0]->ticket_ref_no;
              $rapi_data['seat_count']     = $rpt_data[0]->num_passgr;

              $soap_name = strtolower($rpt_data[0]->op_name) . '_api';
              $rtbdata = $this->doconfirm_booking($rapi_data);
              /************* Actual ticket booking End Here ***********/

              if(trim($rtbdata['status']) == 'success') 
              {
                  $data["ticket_ref_no"] = $rpt_data[0]->ticket_ref_no;
                  $data["ticket_pnr"] = $rtbdata["pnr"];
                  $data["ticket_id"] = $rpt_data[0]->ticket_id;
                  $data["is_return_journey"] = true;
                  
                  /*
                   *This is to update ticket table
                   */
                  $atransaction = array('transaction_status' => "success",
                                        'payment_by' => $api_data["payment_by"],
                                        'payment_mode' => $data["pg_response"]["payment_mode"],
                                        'commision_percentage' => (isset($rtbdata['commision_percentage'])) ? $rtbdata['commision_percentage'] : "", 
                                        'api_ticket_no' => (isset($rtbdata['api_ticket_no'])) ? $rtbdata['api_ticket_no'] : "",
                                        'cancellation_policy' => (isset($rtbdata['cancellation_policy'])) ? $tbdata['cancellation_policy'] : "" 
                                      );

                  $this->tickets_model->update($data["ticket_id"],$atransaction);

                  $final_response["return_ticket_status"] = "success";
                  
                  $rpg_success_response = $this->common_model->transaction_process_api($data);

                  /*For Reconcellation process*/
                  $this->saveReconcellations($rtbdata['api_ticket_no'], $data["ticket_id"], "success");

                  $final_response["transaction_process_response_return"] = $rpg_success_response;

                  if(trim($rpg_success_response["msg_type"]) == "success")
                  {
                    if(isset($data['is_back']) && $data['is_back'])
                    {
                      $url_redirect = base_url()."admin/reservation/view_ticket/".$onwards_ticket_ref_no."/".$rpt_data[0]->ticket_ref_no;  
                    }
                    else
                    {
                      $url_redirect = base_url()."front/booking/view_ticket/".$onwards_ticket_ref_no."/".$rpt_data[0]->ticket_ref_no;
                    }

                    $final_response["status"] = "success";
                    $final_response["redirect_url"] = $url_redirect;
                  }
                  else if(trim($rpg_success_response["msg_type"]) == "error")
                  {
                    $transaction = array(
                                         'failed_reason' => isset($rpg_success_response["msg"]) ? $rpg_success_response["msg"] : "some error occured in ets library",
                                         'payment_by' => $api_data["payment_by"]
                                        );

                    $id = $this->tickets_model->update($rpt_data[0]->ticket_id,$transaction);

                    $final_response["status"] = "error";
                    $final_response["return_transaction_process_api"] = "error";
                    $final_response["data"] = $data;
                  }
              }
              else
              {
                if($rpt_data[0]->transaction_status != "success")
                {
                  $api_failed_reason = (isset($rtbdata["failed_reason"])) ? $rtbdata["failed_reason"] : "Etravelsmart return ticket api failed";
                  $transaction = array('transaction_status' => "failed",
                                       'failed_reason' => $api_failed_reason,
                                       'payment_by' => $api_data["payment_by"]
                                      );

                  $return_ticket_update_where = ["ticket_id" => $rpt_data[0]->ticket_id,"transaction_status != " => "success"];
                  $id = $this->tickets_model->update($return_ticket_update_where,$transaction);
                }
                else
                {
                  $this->common_model->developer_confirm_booking_error_mail("ETS (Return Journey)",$rpt_data[0]->ticket_id);
                }
                
                $final_response["return_ticket_status"] = "error";
                $final_response["return_ticket_api_failed_reason"] = $api_failed_reason;

                $final_response["status"] = "success";
                $final_response["redirect_url"] = $pg_success_response["redirect_url"];
              }
          }
          else
          {
              $final_response["status"] = "success";
              $final_response["redirect_url"] = $pg_success_response["redirect_url"];
          }
        }
        else if(trim($pg_success_response["msg_type"]) == "error")
        {
            $transaction = array(/*'transaction_status' => "failed",*/
                               'failed_reason' => 'failed message - '.(isset($pg_success_response["msg"])) ? $pg_success_response["msg"] : "",
                               'payment_by' => $api_data["payment_by"]
                              );

            $id = $this->tickets_model->update($api_data['ticket_id'],$transaction);
            $final_response["status"] = "success";
            $final_response["redirect_url"] = $pg_success_response["redirect_url"];

            log_data("condition_not_handled/".date("M")."/ets_".date("d-m-Y")."_log.log",
                     "Ticket booked successfully but some error occured in transaction_process_api for ticket is ".$data["ticket_id"],
                      'Transaction Process Status');
        }
    }
    else
    {
      if($api_data['pt_data']->transaction_status != "success")
      {
        $api_failed_reason = (isset($tbdata["failed_reason"])) ? $tbdata["failed_reason"] : "Etravelsmart api failed";
        $transaction = array('transaction_status' => "failed",
                             'failed_reason' => $api_failed_reason,
                             'payment_by' => $api_data["payment_by"]
                            );

        $update_where = ["ticket_id" => $api_data['pt_data']->ticket_id, "transaction_status != " => "success"];
        $id = $this->tickets_model->update($update_where,$transaction);

        $find_return_ticket = $this->return_ticket_mapping_model->where("ticket_id",$api_data['pt_data']->ticket_id)->find_all();
        if($find_return_ticket)
        {
          $return_update_where = ["ticket_id" => $find_return_ticket[0]->return_ticket_id, "transaction_status != " => "success"];
          $rid = $this->tickets_model->update($return_update_where,$transaction);
        }
      }
      else
      {
        $this->common_model->developer_confirm_booking_error_mail("Etravelsmart",$api_data['pt_data']->ticket_id);
      }
        
      $final_response["status"] = "error";
      $final_response["api_failed_reason"] = $api_failed_reason;
      $final_response["data"] = $data;
    }

    log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$final_response, 'Final Booking');

    return $final_response;
  }

 /* 
  * @author    : Suraj
  * @function  : doconfirm_booking
  * @param     : blockTicketKey -> value of the Block ticket
  * @return    : Contains apiStatus, inventoryType, etstnumber, opPNR, commPCT, totalFare, cancellationPolicy
  * @method    : HTTP GET
  * @detail    : This API is used to book the blocked ticket from BlockTicket API.
  */
  public function doconfirm_booking($fields)
  {
    $data   = array();
    // $post_fields = array("blockTicketKey" => $fields['ticket_ref_no']);

    // log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$post_fields, 'Do Confirm Booking Request');

    // $response = getEtravelsmartApiData("seatBooking",$post_fields);
    $response = $this->seatBookingApi($fields);

    $response_decode = json_decode($response);

    log_data("etravelsmart/".date("M")."/service_".date("d-m-Y")."_log.log",$response_decode, 'Do Confirm booking Response');

    $data_to_log = [
                    "ticket_id" => isset($fields["ticket_id"]) ? $fields["ticket_id"] : "",
                    "request" => json_encode($fields),
                    "response" => $response,
                    "type" => "confirm_booking"
                   ];

    insert_request_response_log($data_to_log);
    
    if(is_object($response_decode))
    {
      $api_status = $response_decode->apiStatus;

      if($api_status->success && strtolower($api_status->message) == "success")
      {
        $data["status"] = strtolower($api_status->message);
        $data["pnr"] = $response_decode->opPNR;
        $data["api_ticket_no"] = $response_decode->etstnumber;
        $data["commision_percentage"] = $response_decode->commPCT;
        $data["cancellation_policy"] = $response_decode->cancellationPolicy;
      }
      else
      {
        $data["status"] = "error";
        $data["failed_reason"] = $api_status->message;
      }
    }
    else
    {
      $data["status"] = "error";
      $data["failed_reason"] = "Response not came from API i.e. etravelsmart";
    }


    return $data;
  }



  /* 
  * @author    : Suraj
  * @function  : cancel_ticket
  * @param     : etsTicketNo      -> ETS Ticket Number
                 seatNbrsToCancel -> List seat numbers to cancel
  * @return    : apiStatues, cancellable, partiallyCancellable, totalTicketFare, totalRefundAmount, cancelChargesPercentage, cancellationCharges
  * @method    : HTTP POST
  * @Details   : This API is used cancel the ticket after confirmation from cancelTicketConfirmation API.
  */
  public function cancel_ticket($fields)
  {
    //API - This is cancelTicketConfirmation Method (First method)
    $data   = array();

    if(is_array($fields['seat_no']) && !empty($fields['seat_no']))
    {
      $seat_array = $fields['seat_no'];
    }
    else if($fields['seat_no'] != "")
    {
      $seat_array = explode(",",$fields['seat_no']);
    }
    
    if(!empty($seat_array))
    {

      $post_fields = array("etsTicketNo" => $fields['api_ticket_no'], "seatNbrsToCancel" => $seat_array);
      // $post_fields = json_encode($post_fields);
      // log_data("etravelsmart/".date("M")."/cancel_ticket_".date("d-m-Y")."_log.log",$post_fields, 'cancelTicketConfirmation Request parameter');
      
      // $response = getEtravelsmartApiData("cancelTicketConfirmation",$post_fields,"POST");
      $response = $this->cancelTicketConfirmationApi($post_fields);

      log_data("etravelsmart/".date("M")."/cancel_ticket_".date("d-m-Y")."_log.log",$response, 'cancelTicketConfirmation Response json encoded');
      
      $response_decode = json_decode($response);
      log_data("etravelsmart/".date("M")."/cancel_ticket_".date("d-m-Y")."_log.log",$response_decode, 'cancelTicketConfirmation Response json decoded');

      $data_to_log = [
                    "ticket_id" => isset($fields['current_ticket_information']["ticket_id"]) ? $fields['current_ticket_information']["ticket_id"] : "",
                    "request" => json_encode($post_fields),
                    "type" => "ticket_cancel",
                    "response" => $response
                   ];

      insert_request_response_log($data_to_log);

      if(is_object($response_decode))
      {
        $api_status = $response_decode->apiStatus;

        if($api_status->success && strtolower($api_status->message) == "success")
        {
          if($response_decode->cancellable)
          {
            $final_cancel = $this->cancel_ticket_confirmation($fields);

            if($final_cancel["status"] == "success")
            {
              /*For Reconcellation process*/
              $this->saveReconcellations($fields['api_ticket_no'], $fields["current_ticket_information"]["ticket_id"], "success");

              $data["status"] = "success";
              $data["refundAmount"]           = $final_cancel["refundAmount"];
              $data["cancellationCharge"]     = $final_cancel["cancellationCharge"];
              $data["cancellationChargePer"]  = $final_cancel["cancellationChargePer"];
              $data["raw_data"]               = $final_cancel["raw_data"];
            }
            else
            {
              $data["status"] = "error";
              $data["error_message"] = $final_cancel["error_message"];
            }
          }
          else
          {
            $data["status"] = "error";
            $data["error_message"] = "Ticket is not cancellable.";
          }
          
        }
        else
        {
          $data["status"] = "error";
          $data["error_message"] = $response_decode->message;
        }
      }
      else
      {
        $data["status"] = "error";
        $data["error_message"] = "Something unexpected happened. Please Try again.";
      }
    }
    else
    {
      $data["status"] = "error";
      $data["error_message"] = "Please provide seat number.";
    }
    
    return $data;
  }


   /* 
  * @author    : Suraj
  * @function  : cancel_ticket_confirmation
  * @param     : etsTicketNo      -> ETS Ticket Number
                 seatNbrsToCancel -> List seat numbers to cancel
  * @return    : apiStatus, cancellable, partiallyCancellable, totalTicketFare, totalRefundAmount, cancelChargesPercentage, cancellationCharges
  * @method    : HTTP POST
  * @Details   : This API is used to confirm the cancel ticket details before actually cancelling the ticket.
  */
  public function cancel_ticket_confirmation($fields)
  {
    //API - This is cancelTicket Method (second i.e. final method)
    $data   = array();

    if(is_array($fields['seat_no']) && !empty($fields['seat_no']))
    {
      $seat_array = $fields['seat_no'];
    }
    else if($fields['seat_no'] != "")
    {
      $seat_array = explode(",",$fields['seat_no']);
    }
    
    if(!empty($seat_array))
    {

      $post_fields = array("etsTicketNo" => $fields['api_ticket_no'], "seatNbrsToCancel" => $seat_array);
      // $post_fields = json_encode($post_fields);
      // log_data("etravelsmart/".date("M")."/cancel_ticket_".date("d-m-Y")."_log.log",$post_fields, 'cancelTicket Request parameter');

      // $response = getEtravelsmartApiData("cancelTicket",$post_fields,"POST");
      $response = $this->cancelTicketApi($post_fields);
      log_data("etravelsmart/".date("M")."/cancel_ticket_".date("d-m-Y")."_log.log",$response, 'cancelTicket Response json encoded');

      $response_decode = json_decode($response);
      log_data("etravelsmart/".date("M")."/cancel_ticket_".date("d-m-Y")."_log.log",$response_decode, 'cancelTicket Response json decoded');

      if(is_object($response_decode))
      {
        $api_status = $response_decode->apiStatus;
        if($api_status->success && strtolower($api_status->message) == "success")
        {
          $data["status"] = "success";
          $data["refundAmount"]           = $response_decode->totalRefundAmount;
          $data["cancellationCharge"]     = $response_decode->cancellationCharges;
          $data["cancellationChargePer"]  = $response_decode->cancelChargesPercentage;
          $data["raw_data"]                = $response_decode;
        }
        else
        {
          $data["status"] = "error";
          $data["error_message"] = "Something unexpected happened. Please Try again.";
        }
      }
      else
      {
        $data["status"] = "error";
        $data["error_message"] = "Something unexpected happened. Please Try again.";
      }
    }
    else
    {
      $data["status"] = "error";
      $data["error_message"] = "Please provide seat number.";
    }
    
    return $data;
  }




/* 
  * @author    : Suraj
  * @function  : is_ticket_cancellable
  * @param     : etsTicketNo      -> ETS Ticket Number
                 seatNbrsToCancel -> List seat numbers to cancel
  * @return    : apiStatues, cancellable, partiallyCancellable, totalTicketFare, totalRefundAmount, cancelChargesPercentage, cancellationCharges
  * @method    : HTTP POST
  * @Details   : This API is used check if ticket is cancelable
  */
  public function is_ticket_cancellable($fields)
  {
    //API - This is cancelTicketConfirmation Method (First method)
    $data   = array();

    if(is_array($fields['seat_no']) && !empty($fields['seat_no']))
    {
      $seat_array = $fields['seat_no'];
    }
    else if($fields['seat_no'] != "")
    {
      $seat_array = explode(",",$fields['seat_no']);
    }
    
    if(!empty($seat_array))
    {

      $post_fields = array("etsTicketNo" => $fields['api_ticket_no'], "seatNbrsToCancel" => $seat_array);

      $response = $this->cancelTicketConfirmationApi($post_fields);

      $data_to_log = [
                    "ticket_id" => isset($fields['current_ticket_information']["ticket_id"]) ? $fields['current_ticket_information']["ticket_id"] : "",
                    "request" => json_encode($post_fields),
                    "type" => "is_ticket_cancelable",
                    "response" => $response,
                   ];

      insert_request_response_log($data_to_log);

      log_data("etravelsmart/".date("M")."/cancel_ticket_".date("d-m-Y")."_log.log",$response, 'first hit cancelTicketConfirmation Response json encoded');
      
      $response_decode = json_decode($response);
      log_data("etravelsmart/".date("M")."/cancel_ticket_".date("d-m-Y")."_log.log",$response_decode, 'first hit  cancelTicketConfirmation Response json decoded');


      if(is_object($response_decode))
      {
        $api_status = $response_decode->apiStatus;

        if($api_status->success && strtolower($api_status->message) == "success")
        {
          if($response_decode->cancellable)
          {
            $data["status"]                 = "success";
            $data["refundAmount"]           = $response_decode->totalRefundAmount;
            $data["cancellationCharge"]     = $response_decode->cancellationCharges;
          }
          else
          {
            $data["status"] = "error";
            $data["error_message"] = "Ticket is non-cancellable.";
          }
        }
        else
        {
          $data["status"] = "error";
          $data["error_message"] = "This ticket cannot be cancelled. Please contact customer support.";
        }
      }
      else
      {
        $data["status"] = "error";
        $data["error_message"] = "There might be a network issue. Please Try again.";
      }
    }
    else
    {
      $data["status"] = "error";
      $data["error_message"] = "Please provide seat number.";
    }
    
    return $data;
  }

 /* 
  * @author    : Suraj Rathod
  * @function  : EtGetStations
  * @param     : 
  * @return    : returns provides the list of all available stations.
  */
  public function getSourceDestination()
  {
      $response = $this->etravelsmart_api->getStations();
      $cities = array();

      if($response)
      {        
        foreach ($response as $key => $value)
        {
            $cities[$key]["value"] = ucwords($value->stationName);
            $cities[$key]["label"] = ucwords($value->stationName);
        }
      }

      array_multisort($cities,SORT_ASC);
      // $data_json = json_encode($cities);

      return $cities;
  }

 /* 
  * @author    : Suraj
  * @function  : getTicketByETSTNumber
  * @param     : ETS Ticket number generated after successful booking.
  * @return    : Contains the message and isSuccess
  * @method    : HTTP GET
  * @Details   : This is used to book the blocked ticket
  */
  public function getTicketByETSTNumber($fields)
  {
    log_data("etravelsmart/".date("M")."/reconcellation_".date("d-m-Y")."_log.log",$fields, 'Bus Reconcellation Request - getTicketByETSTNumber');
    $response = getEtravelsmartApiData("getTicketByETSTNumber",$fields);
    log_data("etravelsmart/".date("M")."/reconcellation_".date("d-m-Y")."_log.log",$response, 'Bus Reconcellation Response - getTicketByETSTNumber');
    
    $response_decode = json_decode($response);
    $api_status = $response_decode->apiStatus;
    
    if($api_status->success && strtolower($api_status->message) == "success")
    {
      return $response_decode;
    }
    else
    {
      return "";
    }
  }



  /* 
  * @author    : Suraj
  * @function  : getMyPlanAndBalance
  * @param     : 
  * @return    : returns Balance and plan
  */
  public function getMyPlanAndBalance()
  {
    $response = getEtravelsmartApiData("getMyPlanAndBalance");
    
    $response_decode = json_decode($response);
    $api_status = $response_decode->apiStatus;
    
    if($api_status->success && strtolower($api_status->message) == "success")
    {
      return $response_decode;
    }
    else
    {
      return "";
    }
  }

  public function saveReconcellations($ets_ticket_number = "", $ticket_id = "", $action = "success")
  {
    if($ets_ticket_number != "" && $ticket_id != "")
    {
      $fields     =   array("ETSTNumber" => $ets_ticket_number);
      $response   =   $this->getTicketByETSTNumber($fields);

      $ticket_status  =   "travelled";
      if($action == "cancel")
      {
        $ticket_status  =   "cancelled";
      }

      $where_condition = ["ticket_number" => $ets_ticket_number, "ticket_status" => $ticket_status];
      
      $check_if_exists  = $this->tickets_additional_data_model->where($where_condition)->find_all();
      if(!$check_if_exists)
      {
        if($response)
        {
          $array_to_insert  = [
                                "ticket_id" => $ticket_id,
                                "ticket_number" => $ets_ticket_number,
                                "actual_amount" => $response->actualAmount,
                                "total_amount" => $response->totalFareWithTaxes,
                                "paid_amount" => $response->paidAmount,
                                "tds_charges" => $response->tdsCharge,
                                "total_discount_amount" => $response->totalDiscountAmount,
                                "commission_percentage" => $response->commPCT,
                                "service_tax_amount" => $response->serviceTaxAmount,
                                "refund_amount" => $response->refundAmount,
                                "ticket_status" => strtolower($response->ticketStatus),
                                "insurance_amount" => $response->insuranceAmount,
                                "traveller_contact" => $response->serviceProviderContact,
                                "response" => json_encode($response)
                              ];
          $this->tickets_additional_data_model->insert($array_to_insert);
        }
      }
    }
  }
  // to send the cancel ticket start here//
   public function mail_cancel_ticket($tinfo,$data,$ptd_data) {
    $tkt_cancellation_date = $this->cancel_ticket_model->column('created_at')->where('ticket_id', $tinfo['ticket_id'])->find_all();
    if (count($ptd_data) > 0) {
        $psgr_no = 1;
        $passanger_detail = "";
       
        foreach ($ptd_data as $key => $value)
        {
          $passanger_detail .= '<tr style="padding:7px 0;text-align: center">
                          <td style="padding:7px;">
                            '.ucwords($value->psgr_name).'
                          </td>
                          <td style="padding:7px 0;text-align: center">
                            <span style="line-height:27px;">
                            '.ucwords($value->psgr_age).'
                            </span>
                          </td>
                          <td style="padding:7px 0;text-align: center">
                            '.ucwords($value->psgr_sex).'
                          </td>
                          <td style="padding:7px 0;text-align: center">
                            '.$value->seat_no.'
                          </td>
                        </tr>';
              $psgr_no++; 
        }

                    $cancellation_policy_to_show = "";
                    if(isset($pt_data[0]->cancellation_policy) && $pt_data[0]->cancellation_policy != "")
                    {
                      $cpolicy = $pt_data[0]->cancellation_policy != '' ? json_decode($pt_data[0]->cancellation_policy) : array();
                      if(!empty($cpolicy))
                      {
                        foreach ($cpolicy as $ckey => $cvalue)
                        {
                          if(!empty($cvalue))
                          {
                            $charges_in_per = "";
                            $charges_in_per = 100 - $cvalue->refundInPercentage;
                            $cancellation_policy_to_show .= '<tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;">
                                                              <td style="padding:3px;">Cancellation within '.$cvalue->cutoffTime.' hrs</td>
                                                              <td style="padding:3px;">'.$charges_in_per.'%</td>
                                                            </tr>';
                          }
                        }
                      }
                    }
                    $pickup_address = $pt_data[0]->boarding_stop_name;
                    $pickup_address .= ($pt_data[0]->pickup_address != "") ? " (".$pt_data[0]->pickup_address.")" : "";
                    
                    if($tinfo['droping_time'] != "" && $tinfo['droping_time'] != "0000-00-00 00:00:00")
                    {
                      $droping_time = date("h:i A",strtotime($tinfo['droping_time']));
                    }
                    else
                    {
                      $droping_time = "NA";
                    }

                    if($tinfo['boarding_time'] != "" && $tinfo['boarding_time'] != "0000-00-00 00:00:00")
                    {
                      $bus_boarding_time = date("h:i A",strtotime($tinfo['boarding_time']));
                    }
                    else
                    {
                      $bus_boarding_time = "NA";
                    }

                    $agent_details = "";
                    
                    if(!empty($this->session->userdata("user_id")))
                    {
                      eval(AGENT_ID_ARRAY);
                      if(in_array($this->session->userdata("role_id") , $agent_id_array))
                      {

                        $user_agent_details  = $this->users_model
                                                    ->where(["id" => $this->session->userdata("user_id")])
                                                    ->find_all();
                        if($user_agent_details)
                        {
                          $agent_firm_name = ($user_agent_details[0]->firm_name != "") ? $user_agent_details[0]->firm_name : $user_agent_details[0]->first_name." ".$user_agent_details[0]->last_name;
                          $agent_details = "<tr>
                                <td colspan=2' align='right' valign='top' style='color:#000;font-weight:bold; font-size:13px; text-align:centre; padding-top:7px;'>
                                <span style='color:#ff940a;'>
                                ".$agent_firm_name."</span><br />Agent Contact No. ".$user_agent_details[0]->mobile_no."</td>
                              </tr>";
                        }
                      }

                    }

                    if($tinfo['payment_by'] == 'csc') {
                      $total_fare = $tinfo['total_fare_without_discount'];
                      $refund_amt = $tinfo['total_fare_without_discount'] - $data['cancellation_cahrge'];
                    } 
                    else {
                      $total_fare = $tinfo['tot_fare_amt_with_tax'];
                      $refund_amt = $data['refund_amt'];
                    }                                       
                    $mail_replacement_array = array('{{cancellation_date}}' => date("jS F Y",strtotime($tkt_cancellation_date[0]->created_at)),
                                                   "{{pnr_number}}" => $tinfo['pnr_no'],
                                                   "{{from_to_journey}}" => $tinfo['from_stop_name']." to ".$tinfo['till_stop_name'],
                                                   "{{journey_date}}" =>  date("l \- jS F Y",strtotime($tinfo['dept_time'])),
                                                   "{{op_name}}" => $tinfo['op_name'],
                                                   "{{seat_no}}" => $tinfo['num_passgr'],
                                                   "{{bus_types}}" => $tinfo['bus_type'], //$service_detail["bus_type"],
                                                   "{{total_fare}}" => "Rs. ".$total_fare,
                                                   // "{{total_fare}}" => "Rs. ".$pt_data[0]->total_fare_without_discount,
                                                   "{{bos_ref_no}}" => $tinfo['boss_ref_no'],
                                                   "{{boarding_point}}" => $tinfo['boarding_stop_name'],
                                                   "{{dropping_point}}" => $tinfo['destination_stop_name'],
                                                   "{{departure_time}}" => $bus_boarding_time,
                                                   // "{{alighting_time}}" => date("h:i A",strtotime($pt_data[0]->alighting_time)),
                                                   "{{alighting_time}}" => date("h:i A",strtotime($tinfo['alighting_time'])),
                                                   "{{email}}" => $tinfo['user_email_id'],
                                                   "{{mobile_no}}" => $tinfo['mobile_no'],
                                                   "{{cancellation_charge}}" =>"Rs. ".$data['cancellation_cahrge'],
                                                   "{{refund_amount}}" => "Rs. ".$refund_amt,
                                                   "{{passanger_details}}" => $passanger_detail,
                                                   "{{agent_details}}" => $agent_details,
                                                   "{{dynamic_cancellation_policy}}" => $cancellation_policy_to_show
                                                   );
                        /************ Mail Start Here ********/
                        $data['mail_content'] = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),BOS_CANCEL_TICKET_TEMPLATE);
                        $mail = $tinfo['user_email_id'];
                        $mailbody_array = array("subject" => "Rokad cancel Ticket",
                                                "message" => $data['mail_content'],
                                                "to" =>   $mail
                                                );
                         
                         $mail_result = send_mail($mailbody_array);
                         
                      //****if agent booked tkt send mail to both agent as well as customer****//
                     $email_agent = $tinfo['agent_email_id'];
                        if($email_agent != "" && ($email_agent != $email))
                        {
                            $agent_mailbody_array = array("subject" => "Rokad Ticket",
                                                  "message" => $data['mail_content'],
                                                  "to" =>  $email_agent
                                                  );
                             $agent_mail_result = send_mail($agent_mailbody_array);
                        }
                        /************ Mail End Here ********/
     /************************ Update in Ticket_dump table start here *****************************/
        $update_in_ticket_dump = $this->ticket_dump_model->update_where(array('ticket_id' => $tinfo['ticket_id']),'',array('cancel_ticket' => $data['mail_content']));

      /************************ Update in Ticket_dump table end here *****************************/

  }
}

    function apiGetAvailabeBus($post,$serviceCall = false) {
    
        $data = $this->etravelsmart_api->getAvailableBusesApi($post['from'], $post['to'], $post['date']);
        if (!is_json($data)) {
            return false;
        }

        $data = json_decode($data);


        if (empty($data->apiStatus->success)) {
            return false;
        }

        $tdata = array();
        $rdata = array();
        $compareCommissionData = array();
        $data = $data->apiAvailableBuses;
        
        foreach ($data as $key => $value) {

            if (stripos(strtolower($value->operatorName), "Himachal Road Transport Corporation HRTC") === false && stripos(strtolower($value->operatorName), "Uttar Pradesh State Road Transport Corporation (UP") === false && stripos(strtolower($value->operatorName), "uttar pradesh state road transport corporation(upsrtc)") === false && stripos(strtolower($value->operatorName), "upsrtc") === false && stripos(strtolower($value->operatorName), "Uttar Pradesh State Transport") === false && stripos(strtolower($value->operatorName), "msrtc") === false) {
                $cancellation_policy = array();

                $cpolicy = $value->cancellationPolicy != '' ? json_decode($value->cancellationPolicy) : array();
                
                if (!empty($cpolicy)) {
                    foreach ($cpolicy as $ckey => $cvalue) {

                        if (!empty($cvalue)) {
                            $temp = new stdClass;

                            $temp->cutoffTime = $cvalue->cutoffTime;
                            $temp->refundInPercentage = 100 - $cvalue->refundInPercentage;
                            // $cancellation_policy[] = $temp;
                            // $cancellation_policy[] = 'Cancellation within '.$cvalue->cutoffTime.' hrs cancellation charges '. (100 - $cvalue->refundInPercentage) .'%';
                            $cancellation_policy[] = $temp;
                        }
                    }
                }
                
                // $cancel_policy = to_array(json_decode($value->cancellationPolicy));
                /**************ETRAVELS CHANGES FOR ITS COMMISSION COMPARISON ******************/
                $compareCommissionData[$key]["commission_percentage"] = $value->commPCT;
                $compareCommissionData[$key]["operator_name"] = $value->operatorName;
                $compareCommissionData[$key]["bus_type"] = $value->busType;
                $compareCommissionData[$key]["arrival_time"] = $value->arrivalTime;
                $compareCommissionData[$key]["arrival_time"] = $value->arrivalTime;
                $compareCommissionData[$key]["departure_time"] = $value->departureTime;
                
                $dept_tm_his = date("H:i:s", strtotime($value->departureTime));

                $arr_time = date("H:i:s", strtotime($value->arrivalTime));

                $day = (strtotime($dept_tm_his) < strtotime($arr_time)) ? 0 : 1;

                $arr_date = date('Y-m-d H:i:s', strtotime($post['date'] . ' ' . $arr_time . '+' . $day . ' days'));
                $this->load->model(['bus_type_model']);
                $bus_types = $this->bus_type_model->get_bus_type_by_api_bus_type($value->busType);
                if (empty($bus_types)) {
                    $bus_types['id'] = $value->busType;
                    $bus_types['bus_type_name'] = $value->busType;
                }
                $services = array(
                    // 'op_user_id'            => '0',
                    'from_stop_code' => $post['from'],
                    'to_stop_code' => $post['to'],
                    'from_stop' => $post['from'],
                    'to_stop' => $post['to'],
                    'trip_no' => $value->routeScheduleId,
                    'route_no_name' => $post['from'] . ' to ' . $post['to'],
                    'bus_stop_name' => $post['to'],
                    'op_trip_no' => $value->routeScheduleId,
                    'service_id' => $value->serviceId,
                    'seat_layout_id' => '0',
                    'layout_name' => 'NA',
                    'dept_tm' => date("Y-m-d", strtotime($post['date'])) . " " . $dept_tm_his,
                    'sch_departure_date' => date("d-m-Y", strtotime($post['date'])),
                    'sch_departure_tm' => $dept_tm_his,
                    'arrival_date' => $arr_date,
                    'arrival_tm' => $arr_time,
                    'arrival_time' => $arr_date,
                    'day' => $day,
                    'bus_type_id' => $bus_types['id'],
                    'bus_type_name' => $bus_types['bus_type_name'],
                    'op_bus_type_name' => $value->busType,
                    'op_bus_type_cd' => $value->busType,
                    'is_child_concession' => false,
                    'tot_seat_available' => $value->availableSeats,
                    'seat_fare' => $value->fare,
                    'seat_fare_with_tax' => $value->fare,
                    'arrival_day' => '',
                    'boarding_stops' => $value->boardingPoints,
                    'dropping_stops' => [],
                    // custom
                    'id_proof_required' => $value->idProofRequired,
                    'mticket_allowed' => $value->mTicketAllowed,
                    'commission_percentage' => $value->commPCT,
                    'routeScheduleId' => $value->routeScheduleId,
                    'inventory_type' => $value->inventoryType,
                    'cancellation_policy' => $cancellation_policy,
                    'op_id' => $value->operatorId,
                    'op_name' => $value->operatorName,
                    'provider_id' => $this->provider_id,
                    'provider_type' => $this->provider_type,
                    'operator' => $value->operatorName,
                );
                
                $farearray = explode(",", $value->fare);
                

                $discount_detail = array();
               
                foreach ($farearray as $key => $val) {
                    $discount_detail[] = $this->discounts->discount_calculation($farearray[$key], $farearray[$key], $value->busType, $value->operatorId, BOS_MOBILE_API_USER_ID);
                }
                
                if (!empty($discount_detail)) {
                    $services['seat_fare_with_tax_with_discount'] = '';
                    foreach ($discount_detail as $key1 => $value1) {
                        if (!empty($discount_detail[$key1]['seat_fare_with_tax_with_discount']))
                            $services['seat_fare_with_tax_with_discount'] .= $discount_detail[$key1]['seat_fare_with_tax_with_discount'] . ',';
                    }
                    if (!empty($services['seat_fare_with_tax_with_discount']))
                        $services['seat_fare_with_tax_with_discount'] = rtrim($services['seat_fare_with_tax_with_discount'], ',');
                    else {
                        $services['seat_fare_with_tax_with_discount'] = $value->fare;
                    }
                    if (!empty($discount_detail[0]['discount_type']))
                        $services['discount_type'] = $discount_detail[0]['discount_type'];
                    else
                        $services['discount_type'] = 'NA';

                    if (!empty($discount_detail[0]['discount_on']))
                        $services['discount_on'] = $discount_detail[0]['discount_on'];
                    else
                        $services['discount_on'] = 'NA';

                    if (!empty($discount_detail[0]['discount_rate']))
                        $services['discount_flat_rate'] = $discount_detail[0]['discount_rate'];
                    else
                        $services['discount_flat_rate'] = 'NA';
                }
                
                //Kindly check
                //$compareCommissionData[$key]["actual_seat_fare"] = $data["bus_detail"][$key]['actual_seat_fare'];
                //$compareCommissionData[$key]["seat_fare"] = $data["bus_detail"][$key]['seat_fare']; 
                /**************ETRAVELS CHANGES FOR ITS COMMISSION COMPARISON ******************/
                $services['compare_commission_data'] = $compareCommissionData;
                /**************ETRAVELS CHANGES FOR ITS COMMISSION COMPARISON ******************/
                $tdata[] = $services;
                $rdata[$value->routeScheduleId] = $services;
            }
           
        } // end of foreach
        /**************ETRAVELS CHANGES FOR ITS COMMISSION COMPARISON ******************/
        $this->rediscache->set($post['from'] . $post['to'] . $post['date'], $rdata, (60 * 60 * 60 * 24));
        return $tdata;
        /**************ETRAVELS CHANGES FOR ITS COMMISSION COMPARISON ******************/
    }

      function api_get_seat_layout($post){    

    $config = array(
                  array(
                          'field' => 'from', 
                          'label' => 'From stop', 
                          'rules' => 'trim|required', 
                          'errors' => array('required' => 'from stop required' )
                      ),
                  array(
                          'field' => 'to', 
                          'label' => 'To stop', 
                          'rules' => 'trim|required', 
                          'errors' => array('required' => 'to stop required' )
                      ),
                  array(
                          'field' => 'date', 
                          'label' => 'Journey Date', 
                          'rules' => 'trim|required', 
                          'errors' => array('required' => 'Date required' )
                      ),

                  array(
                          'field' => 'route_schedule_id', 
                          'label' => 'route_schedule_id', 
                          'rules' => 'trim|required', 
                          'errors' => array('required' => 'route_schedule_id required' )
                      ),
                  array(
                          'field' => 'inventory_type', 
                          'label' => 'inventory_type', 
                          'rules' => 'trim|required', 
                          'errors' => array('required' => 'inventory_type required' )
                      ),

                );
    
    // $this->form_validation->set_data($data);

    if (form_validate_rules($config, $post) == FALSE) 
    {
        $error = $this->form_validation->error_array();
        return array( 'status' => 'failed' ,'msg' => $error);
    }

    
    $fields = array(
                            'from'              => $post['from'],
                            'to'                => $post['to'],
                            'date'              => $post['date'],
                            'inventory_type'    => $post['inventory_type'],
                            'op_trip_no'        => $post['route_schedule_id'],         // routeScheduleId
                        );

    $data = $this->etravelsmart_api->getBusLayoutApi($fields);
 
    if(!$data){
      return array('status' => 'failed', 'msg' => 'Opps some thing went wrong from operator end, Please select another bus!');
    }

    $data = json_decode($data);

    if(empty($data->apiStatus->success))
    {
      return array(
                    'status' => 'failed', 
                    'msg' => $data->apiStatus->message,
                    'apiMsg' => $res_data->apiStatus->message
                  );
    }

    $response = $tdata = array();


    /*$response['boarding_stops']       = $data->boardingPoints;*/
    $response['serviceTaxApplicable'] = $data->serviceTaxApplicable;
    $response['inventory_type']       = $data->inventoryType;

    $stored_services = $this->api_formated_services($post['from'], $post['to'], $post['date'] );

    $stored_trip = $stored_services[$post['route_schedule_id']];
    if($data->inventoryType > 1)
    {
      $response['boarding_stops']       = $data->boardingPoints;
    }
    else
    {
      $response['boarding_stops']       = $stored_trip["boarding_stops"];
    }
    
    $data         = $data->seats;
    $seats = array();
    $fseat = array();
    $berth_array = array();
    foreach ($data as $key => $value) {

      $berth_array[] = $value->zIndex;
      $is_seat_sleeper = false;
      $seat_orientation = "horizontal";

      if($value->length == 1 && $value->width == 2)
      {
        $seat_orientation = "vertical";
        $is_seat_sleeper = true;
      }

      if($value->length == 2 && $value->width == 1)
      {
        $seat_orientation = "horizontal";
        $is_seat_sleeper = true;
      }

      if($value->sleeper)
      {
        $is_seat_sleeper = true;
      }
      
      $seat = array(
                      'seat_no'                           => $value->id,
                      'seat_fare'                         => $value->fare,
                      'seat_basic_fare'                   => $value->fare,
                      'seat_service_tax_amt'              => $value->totalFareWithTaxes,
                      'seat_service_tax_perc'             => 0,
                      'row_no'                            => $value->row,
                      'col_no'                            => $value->column,
                      'seat_type'                         => '',
                      'berth_no'                          => $value->zIndex,
                      'pos'                               => $value->row.' X '.$value->column,
                      'available'                         => ($value->available)? 'Y' : 'N',
                      'width'                             => $value->width,
                      'length'                            => $value->length,
                      'service_tax_amount'                => $value->serviceTaxAmount,
                      'service_tax_per'                   => $value->serviceTaxPer,
                      'commission'                        => $value->commission,
                      'operator_service_charge_absolute'  => $value->operatorServiceChargeAbsolute,
                      'operator_service_charge_percent'   => $value->operatorServiceChargePercent,
                      'ladies_seat'                       => $value->ladiesSeat,
                      'booked_by'                         => $value->bookedBy,
                      'ac'                                => $value->ac,
                      'sleeper'                           => $is_seat_sleeper,
                      'seat_orientation'                  => $seat_orientation,
                  );      
                  $seat_fare_with_tax_with_discount = $this->discounts->discount_calculation($value->fare,$value->totalFareWithTaxes,$stored_services[$value->busType],$stored_services[$value->operatorId],BOS_MOBILE_API_USER_ID);
       
                  if(!empty($seat_fare_with_tax_with_discount['seat_fare_with_tax_with_discount'])){
                    $seat['seat_fare_with_tax_with_discount'] = $seat_fare_with_tax_with_discount['seat_fare_with_tax_with_discount'] ; 
                  }
                  else{
                    $seat['seat_fare_with_tax_with_discount'] = $value->totalFareWithTaxes;
                 }
      $seats[] = $seat;
    }

    $response['seats']                    = $seats;
    $response['route_schedule_id']        = $post['route_schedule_id'];
    $response['trip_no']                  = $post['route_schedule_id'];
    $response['no_of_berth']              = count(array_unique($berth_array));
    $response['layout_orientation']       = "horizontal";

    return array(
                  'status'  => 'success', 
                  'msg'     => '', 
                  'data'    => $response
              );
  }

  function api_formated_services($from, $to, $date) {
        
        $services = $this->rediscache->get($from.$to.$date);
      
        if(empty($services)) {
            $fields = array(
                'from'  => $from,
                'to'    => $to,
                'date'  => $date
            );

            $data = $this->apiGetAvailabeBus($fields);
            $services = array();
            foreach ($data as $key => $value) {
                $services[$value['routeScheduleId']]   = $value;
            }
        }
         else
        { 
          $services = $this->rediscache->get($from.$to.$date);
        }
         
        return $services;
  } 


}
