<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Bookonspot extends CI_Model {
    var $provider_id;
    var $provider_type;
    
    public function __construct()
    {
      parent::__construct();

   //   $this->load->library(array('rediscache'));

      $this->load->model(array(''));

      ini_set('max_execution_time', 100000);
      ini_set("memory_limit","256M");

    }

  public function index()
  {
    
  }
  
  public function commission_redirect()
  {
      $data["redirect"]=  base_url()."admin/commission";
      data2json($data);
  }
 
}