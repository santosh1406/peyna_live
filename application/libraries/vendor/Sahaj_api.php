<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class  Sahaj_api extends CI_Model {
  
    var $url;  

    public function __construct() {
      parent::__construct();

      ini_set('max_execution_time', 3000);
      ini_set("memory_limit",-1);
      $this->load->helper(array('soap_data_helper'));

      // $this->url  = 'http://120.63.246.195:7777/Trimax/TrimaxServices?wsdl';
      // $this->url  = 'http://10.21.41.39:8080/Trimax/TrimaxServices?wsdl';
      // $this->url  = 'http://180.92.175.105:8080/Trimax/TrimaxServices?wsdl';

      // -----  sahaj server 
//      $this->url  = 'http://59.181.158.111:7777/Trimax/TrimaxServices?wsdl';
      $this->url  = $this->config->item('sahaj_api_url');

      log_data('vendor/agent_'.date('d-m-Y').'.log',$this->url,'URL');

    }

    function mappingBosDetailsUsingRetailer($data = array())
    {    
      // RetailerID, BOS_id, cardNo, HHM Serial number
      $data['hhm_sr_no'] = isset($data['hhm_sr_no']) ?  $data['hhm_sr_no'] : '';

      $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.trimax.apps.com/">
                     <soapenv:Header/>
                     <soapenv:Body>
                        <ws:mappingBosDetailsUsingRetailer>
                           <!--Optional:-->
                           <arg0>'.$data['bos_agent_code'].'</arg0>
                           <!--Optional:-->
                           <arg1>'.$data['rfid_code'].'</arg1>
                           <!--Optional:-->
                           <arg2>'.$data['hhm_sr_no'].'</arg2>
                           <!--Optional:-->
                           <arg3>'.$data['v_agent_code'].'</arg3>
                        </ws:mappingBosDetailsUsingRetailer>
                     </soapenv:Body>
                  </soapenv:Envelope>';

      log_data('vendor/agent_'.date('d-m-Y').'.log', $xml_str);

      $data = get_soap_data($this->url, 'mappingBosDetailsUsingRetailer', $xml_str);

      log_data('vendor/agent_'.date('d-m-Y').'.log',$data,'response');
      return $data;
    }

    function hhmTransactionUpdate($data)
    {
//        /show($data,1,'sahaj');
        if(is_array($data))
        {
            $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.trimax.apps.com/">
                             <soapenv:Header/>
                             <soapenv:Body>
                                <ws:hmmTransactionUpdate>
                                   <!--Optional:-->
                                   <arg0>'.$data['agent_code'].'</arg0>
                                   <!--Optional:-->
                                   <arg1>'.$data['way_bill_id'].'</arg1>
                                   <!--Optional:-->
                                   <arg2>'.$data['allocation_date'].'</arg2>
                                   <!--Optional:-->
                                   <arg3>'.$data['opening_balance'].'</arg3>
                                   <!--Optional:-->
                                   <arg4>'.$data['collection_date'].'</arg4>
                                   <!--Optional:-->
                                   <arg5>'.$data['no_of_tickets'].'</arg5>
                                   <arg6>'.$data['consumed_amount'].'</arg6>
                                   <!--Optional:-->
                                   <arg7>'.$data['remaining_balance'].'</arg7>
                                </ws:hmmTransactionUpdate>
                             </soapenv:Body>
                          </soapenv:Envelope>';

            log_data('vendor/agent_hhmtransupdate_'.date('d-m-Y').'.log', $xml_str);

            $data = get_soap_data($this->url, 'hmmTransactionUpdate', $xml_str);

            log_data('vendor/agent_hhmtransupdate_'.date('d-m-Y').'.log',$data,'response');
            return $data;
        }
    }
    
    function addAgentSellerPoint($data) {
        show($data,1,'Agents seller point');
        if(is_array($data))
        {
            $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.trimax.apps.com/">
                        <soapenv:Header/>
                        <soapenv:Body>
                           <ws:addAgentSellerPoint>
                              <!--Optional:-->
                              <arg0>'.$data[''].'</arg0>
                              <!--Optional:-->
                              <arg1>'.$data[''].'</arg1>
                              <!--Optional:-->
                              <arg2>'.$data[''].'</arg2>
                              <arg3>'.$data[''].'</arg3>
                              <!--Optional:-->
                              <arg4>'.$data[''].'</arg4>
                           </ws:addAgentSellerPoint>
                        </soapenv:Body>
                     </soapenv:Envelope>';

            log_data('vendor/agents_seller_point_'.date('d-m-Y').'.log', $xml_str);

            $data = get_soap_data($this->url, 'addAgentSellerPoint', $xml_str);

            log_data('vendor/agents_seller_point_'.date('d-m-Y').'.log',$data,'response');
            return $data;
        }
    }
    
    function addAgentWayBillMaster($data) {
        show($data,1);
        if(is_array($data))
        {
            $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.trimax.apps.com/">
                        <soapenv:Header/>
                        <soapenv:Body>
                           <ws:addAgentWayBillMaster>
                              <!--Optional:-->
                              <arg0>'.$data[''].'</arg0>
                              <!--Optional:-->
                              <arg1>'.$data[''].'</arg1>
                              <!--Optional:-->
                              <arg2>'.$data[''].'</arg2>
                              <!--Optional:-->
                              <arg3>'.$data[''].'</arg3>
                              <!--Optional:-->
                              <arg4>'.$data[''].'</arg4>
                              <!--Optional:-->
                              <arg5>'.$data[''].'</arg5>
                              <arg6>'.$data[''].'</arg6>
                              <!--Optional:-->
                              <arg7>'.$data[''].'</arg7>
                              <!--Optional:-->
                              <arg8>'.$data[''].'</arg8>
                           </ws:addAgentWayBillMaster>
                        </soapenv:Body>
                     </soapenv:Envelope>';

            log_data('vendor/agents_waybill_master'.date('d-m-Y').'.log', $xml_str);

            $data = get_soap_data($this->url, 'addAgentWayBillMaster', $xml_str);

            log_data('vendor/agents_waybill_master'.date('d-m-Y').'.log',$data,'response');
            return $data;
        }
    }
    
    function callBalanceTransferApprove($data) {
        show($data,1,'Balance Transfer');
        if(is_array($data))
        {
            $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.trimax.apps.com/">
                        <soapenv:Header/>
                        <soapenv:Body>
                           <ws:callBalanceTransferApprove>
                              <!--Optional:-->
                                <arg0>'.$data[''].'</arg0>
                           </ws:callBalanceTransferApprove>
                        </soapenv:Body>
                     </soapenv:Envelope>';

            log_data('vendor/balance_transfer_approval'.date('d-m-Y').'.log', $xml_str);

            $data = get_soap_data($this->url, 'callBalanceTransferApprove', $xml_str);

            log_data('vendor/balance_transfer_approval'.date('d-m-Y').'.log',$data,'response');
            return $data;
        }
    }
    
    function callTopupBalance($data) {
        // show($data,1,'Topup Balance');
        if(is_array($data))
        {
            $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.trimax.apps.com/">
                        <soapenv:Header/>
                        <soapenv:Body>
                           <ws:callTopupBalance>
                              <!--Optional:-->
                              <arg0>'.$data['agent_code'].'</arg0>
                              <!--Optional:-->
                              <arg1>'.$data['way_bill_no'].'</arg1>
                              <arg2>'.$data['balance'].'</arg2>
                           </ws:callTopupBalance>
                        </soapenv:Body>
                     </soapenv:Envelope>';

            log_data('vendor/topup_balance'.date('d-m-Y').'.log', $xml_str);

            $data = get_soap_data($this->url, 'callTopupBalance', $xml_str);

            log_data('vendor/topup_balance'.date('d-m-Y').'.log',$data,'response');
            return $data;
        }
    }
    
    
    function createRetailer($data) {
        show($data,1,'create Retailer');
        if(is_array($data))
        {
            $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.trimax.apps.com/">
                        <soapenv:Header/>
                        <soapenv:Body>
                           <ws:createRetailer>
                              <!--Optional:-->
                              <arg0>'.$data[''].'</arg0>
                              <!--Optional:-->
                              <arg1>'.$data[''].'</arg1>
                           </ws:createRetailer>
                        </soapenv:Body>
                     </soapenv:Envelope>';

            log_data('vendor/create_retailer'.date('d-m-Y').'.log', $xml_str);

            $data = get_soap_data($this->url, 'createRetailer', $xml_str);

            log_data('vendor/create_retailer'.date('d-m-Y').'.log',$data,'response');
            return $data;
        }
    }
    
    
    function deleteAgentSellerPoint($data) {
        show($data,1,'Delete Agent seller Point');
        if(is_array($data))
        {
            $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.trimax.apps.com/">
                        <soapenv:Header/>
                        <soapenv:Body>
                           <ws:deleteAgentSellerPoint>
                              <!--Optional:-->
                              <arg0>'.$data[''].'</arg0>
                           </ws:deleteAgentSellerPoint>
                        </soapenv:Body>
                     </soapenv:Envelope>';

            log_data('vendor/delete_agent_seller_point'.date('d-m-Y').'.log', $xml_str);

            $data = get_soap_data($this->url, 'deleteAgentSellerPoint', $xml_str);

            log_data('vendor/delete_agent_seller_point'.date('d-m-Y').'.log',$data,'response');
            return $data;
        }
    }
    
    
    function deleteAgentWayBillMaster($data) {
        show($data,1,'Delete Agent waybill Master');
        if(is_array($data))
        {
            $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.trimax.apps.com/">
                        <soapenv:Header/>
                        <soapenv:Body>
                           <ws:deleteAgentWayBillMaster>
                              <!--Optional:-->
                              <arg0>'.$data[''].'</arg0>
                           </ws:deleteAgentWayBillMaster>
                        </soapenv:Body>
                     </soapenv:Envelope>';

            log_data('vendor/delete_agent_waybill_master'.date('d-m-Y').'.log', $xml_str);

            $data = get_soap_data($this->url, 'deleteAgentWayBillMaster', $xml_str);

            log_data('vendor/delete_agent_waybill_master'.date('d-m-Y').'.log',$data,'response');
            return $data;
        }
    }
    
    
    function editUsingBos($data) {
        show($data,1,'Edit using Bos');
        if(is_array($data))
        {
            $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.trimax.apps.com/">
                        <soapenv:Header/>
                        <soapenv:Body>
                           <ws:editUsingBos>
                              <!--Optional:-->
                              <arg0>'.$data[''].'</arg0>
                              <!--Optional:-->
                              <arg1>'.$data[''].'</arg1>
                              <!--Optional:-->
                              <arg2>'.$data[''].'</arg2>
                              <arg3>'.$data[''].'</arg3>
                           </ws:editUsingBos>
                        </soapenv:Body>
                     </soapenv:Envelope>';

            log_data('vendor/edit_using_bos'.date('d-m-Y').'.log', $xml_str);

            $data = get_soap_data($this->url, 'editUsingBos', $xml_str);

            log_data('vendor/edit_using_bos'.date('d-m-Y').'.log',$data,'response');
            return $data;
        }
    }
    
    
    function getNewlyAddedAgent($data) {
        show($data,1,'getNewlyAddedAgent');
        if(is_array($data))
        {
            $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.trimax.apps.com/">
                        <soapenv:Header/>
                        <soapenv:Body>
                           <ws:getNewlyAddedAgent/>
                        </soapenv:Body>
                     </soapenv:Envelope>';

            log_data('vendor/get_newly_added_agent'.date('d-m-Y').'.log', $xml_str);

            $data = get_soap_data($this->url, 'getNewlyAddedAgent', $xml_str);

            log_data('vendor/get_newly_added_agent'.date('d-m-Y').'.log',$data,'response');
            return $data;
        }
    }
    
    
    function updateAgentSellerPoint($data) {
        show($data,1,'updateAgentSellerPoint');
        if(is_array($data))
        {
            $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.trimax.apps.com/">
                        <soapenv:Header/>
                        <soapenv:Body>
                           <ws:updateAgentSellerPoint>
                              <!--Optional:-->
                              <arg0>'.$data[''].'</arg0>
                              <!--Optional:-->
                              <arg1>'.$data[''].'</arg1>
                              <!--Optional:-->
                              <arg2>'.$data[''].'</arg2>
                              <arg3>'.$data[''].'</arg3>
                              <!--Optional:-->
                              <arg4>'.$data[''].'</arg4>
                           </ws:updateAgentSellerPoint>
                        </soapenv:Body>
                     </soapenv:Envelope>';

            log_data('vendor/Update_agent_seller_point'.date('d-m-Y').'.log', $xml_str);

            $data = get_soap_data($this->url, 'updateAgentSellerPoint', $xml_str);

            log_data('vendor/Update_agent_seller_point'.date('d-m-Y').'.log',$data,'response');
            return $data;
        }
    }
    
    
    function updateAgentWayBillMaster($data) {
        show($data,1,'updateAgentWayBillMaster');
        if(is_array($data))
        {
            $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.trimax.apps.com/">
                        <soapenv:Header/>
                        <soapenv:Body>
                           <ws:updateAgentWayBillMaster>
                              <!--Optional:-->
                              <arg0>'.$data[''].'</arg0>
                              <!--Optional:-->
                              <arg1>'.$data[''].'</arg1>
                              <!--Optional:-->
                              <arg2>'.$data[''].'</arg2>
                              <!--Optional:-->
                              <arg3>'.$data[''].'</arg3>
                              <!--Optional:-->
                              <arg4>'.$data[''].'</arg4>
                              <!--Optional:-->
                              <arg5>'.$data[''].'</arg5>
                              <arg6>'.$data[''].'</arg6>
                              <!--Optional:-->
                              <arg7>'.$data[''].'</arg7>
                              <!--Optional:-->
                              <arg8>'.$data[''].'</arg8>
                           </ws:updateAgentWayBillMaster>
                        </soapenv:Body>
                     </soapenv:Envelope>';

            log_data('vendor/Update_agent_waybill_master'.date('d-m-Y').'.log', $xml_str);

            $data = get_soap_data($this->url, 'updateAgentWayBillMaster', $xml_str);

            log_data('vendor/Update_agent_waybill_master'.date('d-m-Y').'.log',$data,'response');
            return $data;
        }
    }
    
    
    function updateTopuplimit($data) {
        show($data,1,'updateTopuplimit');
        if(is_array($data))
        {
            $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.trimax.apps.com/">
                        <soapenv:Header/>
                        <soapenv:Body>
                           <ws:updateTopuplimit>
                              <!--Optional:-->
                              <arg0>'.$data[''].'</arg0>
                              <!--Optional:-->
                              <arg1>'.$data[''].'</arg1>
                              <!--Optional:-->
                              <arg2>'.$data[''].'</arg2>
                              <!--Optional:-->
                              <arg3>'.$data[''].'</arg3>
                           </ws:updateTopuplimit>
                        </soapenv:Body>
                     </soapenv:Envelope>';

            log_data('vendor/update_topup_limit'.date('d-m-Y').'.log', $xml_str);

            $data = get_soap_data($this->url, 'updateTopuplimit', $xml_str);

            log_data('vendor/update_topup_limit'.date('d-m-Y').'.log',$data,'response');
            return $data;
        }
    }

}