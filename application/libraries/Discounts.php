<?php

class Discounts
{
    var $ci;
   
    public function __construct()
    {
        $this->ci  = &get_instance();
		$this->ci->load->model(array('discount_model','coupon_applyed_to_ticket','tickets_model','coupon_log_model'));
		$this->ci->load->library('rediscache');
		
    }

    public function discount_calculation_old($current_seat_value,$current_seat_value_with_tax,$current_seat_service_tax,$serviceTaxPer,$current_seat_operator_service_charge,$operatorServiceChargePercent)
    {
        $data = array();
        if((float)$current_seat_value >= $this->ci->config->item('discount_above'))
        {
            if($this->ci->config->item('enable_discount') && strtotime(date('Y-m-d')) <= strtotime($this->ci->config->item('discount_till')) )
            {
                if($current_seat_value >= 0 && $current_seat_value_with_tax >= 0 && $current_seat_service_tax >= 0 && $serviceTaxPer >= 0 && $current_seat_operator_service_charge >= 0 && $operatorServiceChargePercent >= 0)
                {
                    if($this->ci->config->item('discount_type') == "percentage")
                    {
                        if($this->ci->config->item('discount_on') == "seat_fare")
                        {
                            $discount_percentage = $this->ci->config->item('discount_percent_rate');
                            $current_seat_discount = ($current_seat_value_with_tax * $discount_percentage)/100;
							if($current_seat_discount > $this->ci->config->item('max_dis_availed'))
							{
								$current_seat_discount = $this->ci->config->item('max_dis_availed');
							}
                            $current_seat_value_with_tax = $current_seat_value_with_tax - $current_seat_discount;
                        }
                        else if($this->ci->config->item('discount_on') == "base_fare")
                        {
                            $discount_percentage = $this->ci->config->item('discount_percent_rate');
                            $current_seat_discount = ($current_seat_value * $discount_percentage)/100;
                            $current_seat_value = $current_seat_value - $current_seat_discount;
                            $current_seat_service_tax = ($current_seat_value * $serviceTaxPer)/100;
                            $current_seat_operator_service_charge = ($current_seat_value * $operatorServiceChargePercent)/100;
                        }
                    }
                    else if($this->ci->config->item('discount_type') == "flat")
                    {
                        if($this->ci->config->item('discount_on') == "seat_fare")
                        {
                            $discount_flat = $this->ci->config->item('discount_flat_rate');
                            $current_seat_discount = $discount_flat;
//							if($current_seat_discount > $this->ci->config->item('max_dis_availed'))
//							{
//								$current_seat_discount = $this->ci->config->item('max_dis_availed');
//							}
                            $current_seat_value_with_tax = $current_seat_value_with_tax - $current_seat_discount;
                        }
                        else if($this->ci->config->item('discount_on') == "base_fare")
                        {
                            $discount_flat = $this->ci->config->item('discount_flat_rate');
                            $current_seat_discount = $discount_flat;
                            $current_seat_value = $current_seat_value - $current_seat_discount;
                            $current_seat_service_tax = ($current_seat_value * $serviceTaxPer)/100;
                            $current_seat_operator_service_charge = ($current_seat_value * $operatorServiceChargePercent)/100;
                        }
                    }

                    $data["current_seat_discount"] = $current_seat_discount;
                    $data["current_seat_value"] = $current_seat_value;
                    $data["current_seat_value_with_tax"] = $current_seat_value_with_tax;
                    $data["current_seat_service_tax"] = $current_seat_service_tax;
                    $data["current_seat_operator_service_charge"] = $current_seat_operator_service_charge;
                    
                }
            }
        }

        return $data;
    }
	
	
    public function discount_calculation_new($current_seat_value,$current_seat_value_with_tax,$current_seat_service_tax,$serviceTaxPer,$current_seat_operator_service_charge,$operatorServiceChargePercent,$opratorId,$busTypeId)
    {
		
		$data = array();
        if((float)$current_seat_value >= $this->ci->config->item('discount_above'))
        {
           
			if($this->ci->config->item('enable_discount') && strtotime(date('Y-m-d')) <= strtotime($this->ci->config->item('discount_till')) )
            {
                if($current_seat_value >= 0 && $current_seat_value_with_tax >= 0 && $current_seat_service_tax >= 0 && $serviceTaxPer >= 0 && $current_seat_operator_service_charge >= 0 && $operatorServiceChargePercent >= 0)
                {
					if($this->ci->config->item('is_operator_based') == "1" && $this->ci->config->item('op_id') == $opratorId)
					{
						if($this->ci->config->item('discount_type') == "percentage")
						{
							if($this->ci->config->item('discount_on') == "seat_fare")
							{
								$discount_percentage = $this->ci->config->item('discount_percent_rate');
								$current_seat_discount = ($current_seat_value_with_tax * $discount_percentage)/100;
								if($current_seat_discount > $this->ci->config->item('max_dis_availed'))
								{
									$current_seat_discount = $this->ci->config->item('max_dis_availed');
								}
								$current_seat_value_with_tax = $current_seat_value_with_tax - $current_seat_discount;
							}
							
						}
						else if($this->ci->config->item('discount_type') == "flat")
						{
							if($this->ci->config->item('discount_on') == "seat_fare")
							{
								$discount_flat = $this->ci->config->item('discount_flat_rate');
								$current_seat_discount = $discount_flat;
								if($current_seat_discount > $this->ci->config->item('max_dis_availed'))
								{
									$current_seat_discount = $this->ci->config->item('max_dis_availed');
								}
								$current_seat_value_with_tax = $current_seat_value_with_tax - $current_seat_discount;
							}
						}
					}
					
					else if($this->ci->config->item('is_bus_type_based') == "1" && $this->ci->config->item('bus_type') == $busTypeId)
					{
						if($this->ci->config->item('discount_type') == "percentage")
						{
							if($this->ci->config->item('discount_on') == "seat_fare")
							{
								$discount_percentage = $this->ci->config->item('discount_percent_rate');
								$current_seat_discount = ($current_seat_value_with_tax * $discount_percentage)/100;
								if($current_seat_discount > $this->ci->config->item('max_dis_availed'))
								{
									$current_seat_discount = $this->ci->config->item('max_dis_availed');
								}
								$current_seat_value_with_tax = $current_seat_value_with_tax - $current_seat_discount;
							}
							
						}
						else if($this->ci->config->item('discount_type') == "flat")
						{
							if($this->ci->config->item('discount_on') == "seat_fare")
							{
								$discount_flat = $this->ci->config->item('discount_flat_rate');
								$current_seat_discount = $discount_flat;
								if($current_seat_discount > $this->ci->config->item('max_dis_availed'))
								{
									$current_seat_discount = $this->ci->config->item('max_dis_availed');
								}
								$current_seat_value_with_tax = $current_seat_value_with_tax - $current_seat_discount;
							}
						}
					}
					else if($this->ci->config->item('is_bus_type_based') == "0" && $this->ci->config->item('is_operator_based') == "0" && $this->ci->config->item('is_transaction_based') == "0" && $this->ci->config->item('is_ticket_count_based') == "0")
					{
						if($this->ci->config->item('discount_type') == "percentage")
						{
							if($this->ci->config->item('discount_on') == "seat_fare")
							{
								$discount_percentage = $this->ci->config->item('discount_percent_rate');
								$current_seat_discount = ($current_seat_value_with_tax * $discount_percentage)/100;
								if($current_seat_discount > $this->ci->config->item('max_dis_availed'))
								{
									$current_seat_discount = $this->ci->config->item('max_dis_availed');
								}
								$current_seat_value_with_tax = $current_seat_value_with_tax - $current_seat_discount;
							}
							
						}
						else if($this->ci->config->item('discount_type') == "flat")
						{
							if($this->ci->config->item('discount_on') == "seat_fare")
							{
								$discount_flat = $this->ci->config->item('discount_flat_rate');
								$current_seat_discount = $discount_flat;
								if($current_seat_discount > $this->ci->config->item('max_dis_availed'))
								{
									$current_seat_discount = $this->ci->config->item('max_dis_availed');
								}
								$current_seat_value_with_tax = $current_seat_value_with_tax - $current_seat_discount;
							}
						}
					}
					
                    

                    $data["current_seat_discount"] = $current_seat_discount;
                    $data["current_seat_value"] = $current_seat_value;
                    $data["current_seat_value_with_tax"] = $current_seat_value_with_tax;
                    $data["current_seat_service_tax"] = $current_seat_service_tax;
                    $data["current_seat_operator_service_charge"] = $current_seat_operator_service_charge;
                }
            }
        }

        return $data;
    }
	
	
    public function calculate_generic_coupon_discount($totalFare,$couponCode,$total_psng,$inserted_by ='')
	{ 
		$date = date('Y-m-d');
		$couponId = '';
		if(isset($this->ci->session->userdata()["role_id"]) && $this->ci->session->userdata()["role_id"] == ORS_AGENT_ROLE_ID && isset($this->ci->session->userdata()["role_name"]) && strtolower($this->ci->session->userdata()["role_name"]) == ORS_AGENT_ROLE_NAME)
		{
			$couponDetail = $this->ci->discount_model->where('"'.$date.'" between effective_from and effective_to')->where('generic_coupon_code',$couponCode,TRUE)->like('type_of_user',ORS_AGENT_ROLE_ID)->get_all()->result();
		}
		else
		{
			$couponDetail = $this->ci->discount_model->where('"'.$date.'" between effective_from and effective_to')->where('generic_coupon_code',$couponCode,TRUE)->like('type_of_user',USER_ROLE_ID)->get_all()->result();
		}
		if($couponDetail)
		{
			$couponId = $couponDetail[0]->id;
			if($totalFare >= $couponDetail[0]->min_ticket_value)
			{
				if($couponDetail[0]->is_freq_user)
				{
					$userId = $this->ci->session->userdata('booking_user_id');
					
					if($couponDetail[0]->period == 'monthly')
					{
						$userTicketCount = $this->ci->coupon_applyed_to_ticket->where(array("user_id" => $userId,"MONTH(added_on)" => date('m'),"YEAR(added_on)" => date('Y'),"coupon_code" => $couponCode))->count_all();
					}
					else if ($couponDetail[0]->period == 'yearly')
					{
						$userTicketCount = $this->ci->coupon_applyed_to_ticket->where(array("user_id" => $userId,"YEAR(added_on)" => date('Y'),"coupon_code" => $couponCode))->count_all();
					}
					
					if($userTicketCount < $couponDetail[0]->no_of_transaction)
					{
						if($couponDetail[0]->type_discount == "flat")
						{
							$total_ticket_discount = $couponDetail[0]->disc_value;
							if($total_ticket_discount > $couponDetail[0]->max_dis_availed)
							{
								$total_ticket_discount = $couponDetail[0]->max_dis_availed;
							}
						}
						else if ($couponDetail[0]->type_discount == "percentage")
						{
							$total_ticket_discount = ($totalFare * $couponDetail[0]->disc_value)/100;
							if($total_ticket_discount > $couponDetail[0]->max_dis_availed)
							{
								$total_ticket_discount = $couponDetail[0]->max_dis_availed;
							}
							
						}
						$return['flag'] = "@#success#@";
						$return['coupon_id'] = $couponDetail[0]->id;
						$return['total_ticket_discount'] = $total_ticket_discount;
						$return['total_price'] = $totalFare - $total_ticket_discount;
					}
					else
					{
						$return['flag'] = "error";
						$return['error_msg'] = "coupon has reached the limit of usage per a customer";
					}
				}
				else
				{
					if($couponDetail[0]->type_discount == "flat")
					{
						$total_ticket_discount = $couponDetail[0]->disc_value;
						if($total_ticket_discount > $couponDetail[0]->max_dis_availed)
						{
							$total_ticket_discount = $couponDetail[0]->max_dis_availed;
						}
					}
					else if ($couponDetail[0]->type_discount == "percentage")
					{
						$total_ticket_discount = ($totalFare * $couponDetail[0]->disc_value)/100;
						if($total_ticket_discount > $couponDetail[0]->max_dis_availed)
						{
							$total_ticket_discount = $couponDetail[0]->max_dis_availed;
						}
						
					}
					$return['flag'] = "@#success#@";
					$return['coupon_id'] = $couponDetail[0]->id;
					$return['total_ticket_discount'] = $total_ticket_discount * $total_psng;
					$return['per_seat_ticket_discount'] = $total_ticket_discount;
					$return['total_price'] = $totalFare - ($total_ticket_discount * $total_psng);
					$return['discount_on'] = $couponDetail[0]->discount_on;
					$return['discount_above'] = $couponDetail[0]->min_ticket_value;
					
				}
			}
			else
			{
					$return['flag'] = "error";
					$return['error_msg'] = "Coupon code can not apply";
			}
		}
		else
		{
			$return['flag'] = "error";
			$return['error_msg'] = "Coupon code Invaild";
		}
                if(empty($inserted_by)){
                    $inserted_by = $this->ci->session->userdata('booking_user_id');
                }
		$coupon_log_array = array(
								"user_id" => $inserted_by,
								"coupon_code" => $couponCode,
								"coupon_id" => $couponId, 
								"added_by" => $inserted_by,
								"added_on" => date('Y-m-d H:i:s'),
								"status" => 'Y'
							);
		$this->ci->coupon_log_model->insert($coupon_log_array);
		
		return $return;
	}
        
        
	
	public function booking_time_discount_calculation($total_basic_fare,$total_fare_without_discount,$opId,$busTypeId,$tot_seat)
	{
		$total_ticket_discount = 0;
		if($this->ci->config->item('discount_on') == 'base_fare')
		{
			if($total_basic_fare >= $this->ci->config->item('discount_above'))
			{
				if($this->ci->config->item('is_operator_based')== '1' &&  $opId == $this->ci->config->item('op_id'))
				{
					if($this->ci->config->item('discount_type') == "flat")
					{
						$total_ticket_discount = $this->ci->config->item('discount_flat_rate');
						if($total_ticket_discount > $this->ci->config->item('max_dis_availed'))
						{
							$total_ticket_discount = $this->ci->config->item('max_dis_availed');
						}
					}
					else if ($this->ci->config->item('discount_type') == "percentage")
					{
						$total_ticket_discount = ($total_basic_fare * $this->ci->config->item('discount_percent_rate'))/100;
						if($total_ticket_discount > $this->ci->config->item('max_dis_availed'))
						{
							$total_ticket_discount = $this->ci->config->item('max_dis_availed');
						}
					}
				}
				else if ($this->ci->config->item('is_bus_type_based')== '1' &&  $busTypeId == $this->ci->config->item('bus_type'))
				{
					if($this->ci->config->item('discount_type') == "flat")
					{
						$total_ticket_discount = $this->ci->config->item('discount_flat_rate');
						if($total_ticket_discount > $this->ci->config->item('max_dis_availed'))
						{
							$total_ticket_discount = $this->ci->config->item('max_dis_availed');
						}
					}
					else if ($this->ci->config->item('discount_type') == "percentage")
					{
						$total_ticket_discount = ($total_basic_fare * $this->ci->config->item('discount_percent_rate'))/100;
						if($total_ticket_discount > $this->ci->config->item('max_dis_availed'))
						{
							$total_ticket_discount = $this->ci->config->item('max_dis_availed');
						}
						
					}
				}
				else if ($this->ci->config->item('is_operator_based')== '0' && $this->ci->config->item('is_bus_type_based') == '0' && $this->ci->config->item('is_transaction_based') == '0' && $this->ci->config->item('is_ticket_count_based') == '0')
				{
					if($this->ci->config->item('discount_type') == "flat")
					{
						$total_ticket_discount = $this->ci->config->item('discount_flat_rate');
						if($total_ticket_discount > $this->ci->config->item('max_dis_availed'))
						{
							$total_ticket_discount = $this->ci->config->item('max_dis_availed');
						}
					}
					else if ($this->ci->config->item('discount_type') == "percentage")
					{
						$total_ticket_discount = ($total_basic_fare * $this->ci->config->item('discount_percent_rate'))/100;
						if($total_ticket_discount > $this->ci->config->item('max_dis_availed'))
						{
							$total_ticket_discount = $this->ci->config->item('max_dis_availed');
						}
					}
				}
			}
		}
		else if ($this->ci->config->item('discount_on') == 'total_fare')
		{
			if($total_fare_without_discount >= $this->ci->config->item('discount_above'))
			{
				if($this->ci->config->item('is_operator_based')== '1' &&  $opId == $this->ci->config->item('op_id'))
				{
					if($this->ci->config->item('discount_type') == "flat")
					{
						$total_ticket_discount = $this->ci->config->item('discount_flat_rate');
						if($total_ticket_discount > $this->ci->config->item('max_dis_availed'))
						{
							$total_ticket_discount = $this->ci->config->item('max_dis_availed');
						}
					}
					else if ($this->ci->config->item('discount_type') == "percentage")
					{
						$total_ticket_discount = ($total_fare_without_discount * $this->ci->config->item('discount_percent_rate'))/100;
						if($total_ticket_discount > $this->ci->config->item('max_dis_availed'))
						{
							$total_ticket_discount = $this->ci->config->item('max_dis_availed');
						}
					}
				}
				else if ($this->ci->config->item('is_bus_type_based')== '1' &&  $busTypeId == $this->ci->config->item('bus_type'))
				{
					if($this->ci->config->item('discount_type') == "flat")
					{
						$total_ticket_discount = $this->ci->config->item('discount_flat_rate');
						if($total_ticket_discount > $this->ci->config->item('max_dis_availed'))
						{
							$total_ticket_discount = $this->ci->config->item('max_dis_availed');
						}
					}
					else if ($this->ci->config->item('discount_type') == "percentage")
					{
						$total_ticket_discount = ($total_fare_without_discount * $this->ci->config->item('discount_percent_rate'))/100;
						if($total_ticket_discount > $this->ci->config->item('max_dis_availed'))
						{
							$total_ticket_discount = $this->ci->config->item('max_dis_availed');
						}
					}
				}
				else if ($this->ci->config->item('is_ticket_count_based')== '1' &&  $tot_seat >= $this->ci->config->item('no_of_tickets'))
				{
					
					if($this->ci->config->item('discount_type') == "flat")
					{
						$total_ticket_discount = $this->ci->config->item('discount_flat_rate');
						if($total_ticket_discount > $this->ci->config->item('max_dis_availed'))
						{
							$total_ticket_discount = $this->ci->config->item('max_dis_availed');
						}
					}
					else if ($this->ci->config->item('discount_type') == "percentage")
					{
						$total_ticket_discount = ($total_fare_without_discount * $this->ci->config->item('discount_percent_rate'))/100;
						if($total_ticket_discount > $this->ci->config->item('max_dis_availed'))
						{
							$total_ticket_discount = $this->ci->config->item('max_dis_availed');
						}
						
					}
				}
				else if ($this->ci->session->userdata('user_id') !='' && $this->ci->config->item('is_transaction_based')== '1')
				{
					$userTicketCount = $this->ci->tickets_model->where(array("inserted_by" => $this->ci->session->userdata('user_id'),"transaction_status" => 'success'))->count_all();
					
					if($this->ci->config->item('transaction_no') == $userTicketCount+1)
					{
						if($this->ci->config->item('discount_type') == "flat")
						{
							$total_ticket_discount = $this->ci->config->item('discount_flat_rate');
							if($total_ticket_discount > $this->ci->config->item('max_dis_availed'))
							{
								$total_ticket_discount = $this->ci->config->item('max_dis_availed');
							}
						}
						else if ($this->ci->config->item('discount_type') == "percentage")
						{
							$total_ticket_discount = ($total_fare_without_discount * $this->ci->config->item('discount_percent_rate'))/100;
							if($total_ticket_discount > $this->ci->config->item('max_dis_availed'))
							{
								$total_ticket_discount = $this->ci->config->item('max_dis_availed');
							}
							
						}
					}
				}
				else if ($this->ci->config->item('is_operator_based')== '0' && $this->ci->config->item('is_bus_type_based') == '0' && $this->ci->config->item('is_transaction_based') == '0' && $this->ci->config->item('is_ticket_count_based') == '0')
				{
					if($this->ci->config->item('discount_type') == "flat")
					{
						$total_ticket_discount = $this->ci->config->item('discount_flat_rate');
						if($total_ticket_discount > $this->ci->config->item('max_dis_availed'))
						{
							$total_ticket_discount = $this->ci->config->item('max_dis_availed');
						}
					}
					else if ($this->ci->config->item('discount_type') == "percentage")
					{
						$total_ticket_discount = ($total_fare_without_discount * $this->ci->config->item('discount_percent_rate'))/100;
						if($total_ticket_discount > $this->ci->config->item('max_dis_availed'))
						{
							$total_ticket_discount = $this->ci->config->item('max_dis_availed');
						}
					}
				}
			}
		}
		
		return $total_ticket_discount;
	}

    public function discount_detail($role_id)
	{ 
		$date = date('Y-m-d'); 
		$discount_config = $this->ci->discount_model->where('"'.$date.'" between effective_from and effective_to')->where(array("is_generic_coupon_based" => '0',"status" => '0'))->like('type_of_user', $role_id)->order_by('id','desc')->find_all();
		
		$discountArray = array();
		$discountArray['enable_discount'] = false;
		if($discount_config)
		{
			/* New function to save discount data in to redis cache ----- */
			$array = json_decode(json_encode($discount_config), True);
		
			$discountArray['enable_discount'] = true;
			$discountArray['bus_type_based'] = FindValueFromArray($array,1,'is_bus_type_based');
			
			$discountArray['bus_type_based']["seat_fare"] = FindValueFromArray($array,1,'is_bus_type_based','seat_fare','discount_on');
			
			$discountArray['bus_type_based']["base_fare"] = FindValueFromArray($array,1,'is_bus_type_based','base_fare','discount_on');
			
			$discountArray['bus_type_based']["total_fare"] = FindValueFromArray($array,1,'is_bus_type_based','total_fare','discount_on');
			
			$discountArray['is_operator_based'] = FindValueFromArray($array,1,'is_operator_based');
			
			$discountArray['is_operator_based']["seat_fare"] = FindValueFromArray($array,1,'is_operator_based','seat_fare','discount_on');
			
			$discountArray['is_operator_based']["base_fare"] = FindValueFromArray($array,1,'is_operator_based','base_fare','discount_on');
			
			$discountArray['is_operator_based']["total_fare"] = FindValueFromArray($array,1,'is_operator_based','total_fare','discount_on');
			
			$discountArray['transaction_based'] = FindValueFromArray($array,1,'is_transaction_based');
			
			//$transaction_based["seat_fare"] = $this->findNameFromID($array,1,'is_transaction_based','seat_fare','discount_on');
			
			//$transaction_based["base_fare"] = $this->findNameFromID($array,1,'is_transaction_based','base_fare','discount_on');
			
			//$transaction_based["total_fare"] = $this->findNameFromID($array,1,'is_transaction_based','total_fare','discount_on');
			
			$discountArray['ticket_count_based'] = FindValueFromArray($array,1,'is_ticket_count_based');
			
			//$ticket_count_based["seat_fare"] = $this->findNameFromID($array,1,'is_ticket_count_based','seat_fare','discount_on');
			
			//$ticket_count_based["base_fare"] = $this->findNameFromID($array,1,'is_ticket_count_based','base_fare','discount_on');
			
			//$ticket_count_based["total_fare"] = $this->findNameFromID($array,1,'is_ticket_count_based','total_fare','discount_on');
			
			$discountArray['no_condition'] = FindValueFromArray($array,0);
			
			$discountArray['no_condition']["seat_fare"] = FindValueFromArray($array,0,'','seat_fare','discount_on');
			
			$discountArray['no_condition']["base_fare"] = FindValueFromArray($array,0,'','base_fare','discount_on');
			
			$discountArray['no_condition']["total_fare"] = FindValueFromArray($array,0,'','total_fare','discount_on');
                        
			/* end Function */
		}
		if($role_id == ORS_AGENT_ROLE_ID)
		{
			$this->ci->rediscache->set('discount_data_agent',$discountArray);
                        $this->ci->session->set_userdata('discount_check',true);
		}else if($role_id == BOS_MOBILE_API_USER_ID){
                        return $discountArray;
		}else
		{
                        $this->ci->session->set_userdata('discount_check',true);
			$this->ci->rediscache->set('discount_data_user',$discountArray);
		}
		
		
	}

    public function searching_time_discount_calculation($actual_fare,$fare_with_conveycharge,$busTypeId,$opId,$providerId = '0')
	{
		$discount_detail = array();
		$userids        = $this->ci->session->userdata('user_id');
        $userData       = $this->ci->users_model->where("id",$userids)->find_all();

        eval(FME_AGENT_ROLE_ID);
        if(!in_array($this->ci->session->userdata('role_id'), $fme_agent_role_id) && $this->ci->session->userdata('role_id') != ORS_AGENT_USERS_ROLE_ID) {
	        if((empty($userData[0]->commission_id) || $userData[0]->commission_id == 0 ))
	        {
				if($this->ci->session->userdata("role_id") == ORS_AGENT_ROLE_ID)
				{
					$DiscountData = $this->ci->rediscache->get('discount_data_agent');
				}
				else
				{
					$DiscountData = $this->ci->rediscache->get('discount_data_user');
	                                //print_r($DiscountData); exit;
				}
				
				
				$discount_value = 0;
				$discountId = 0;
				
				if(!empty($DiscountData['bus_type_based']["seat_fare"]) || !empty($DiscountData['is_operator_based']["seat_fare"]))
				{
					
					if(!empty($DiscountData['bus_type_based']["seat_fare"]))
					{
						foreach($DiscountData['bus_type_based']["seat_fare"] as $discountDetail)
						{
							if($actual_fare >= $discountDetail['min_ticket_value'] && strtotime(date('Y-m-d')) <= strtotime($discountDetail['effective_to']) && $discountDetail['bus_type'] == $busTypeId && $actual_fare <= $discountDetail['max_ticket_value'])
							{
								if($discountDetail['type_discount'] == "percentage")
								{
									$discount_percentage = $discountDetail['disc_value'];
									$new_discount_value = ($fare_with_conveycharge * $discount_percentage)/100;
									if($new_discount_value > $discountDetail['max_dis_availed'])
									{
										$new_discount_value = $discountDetail['max_dis_availed'];
									}
									
									if($new_discount_value > $discount_value)
									{
										$discount_value = $new_discount_value;
										$discountId = $discountDetail['id'];
									}
									
								}
								else if($discountDetail['type_discount'] == "flat")
								{
									$new_discount_value = $discountDetail['disc_value'];
									if($new_discount_value > $discountDetail['max_dis_availed'])
									{
										$new_discount_value = $discountDetail['max_dis_availed'];
									}
									
									if($new_discount_value > $discount_value)
									{
										$discount_value = $new_discount_value;
										$discountId = $discountDetail['id'];
									}
									
								}
							} 
						} 
					} 
					if (!empty($DiscountData['is_operator_based']["seat_fare"]))
					{
						
						foreach($DiscountData['is_operator_based']["seat_fare"] as $discountDetail)
						{
							if($actual_fare >= $discountDetail['min_ticket_value'] && strtotime(date('Y-m-d')) <= strtotime($discountDetail['effective_to']) && $discountDetail['op_id'] == $opId && $actual_fare <= $discountDetail['max_ticket_value'] && $discountDetail['provider_id'] == $providerId)
							{
								if($discountDetail['type_discount'] == "percentage")
								{
									$discount_percentage = $discountDetail['disc_value'];
									$new_discount_value = ($fare_with_conveycharge * $discount_percentage)/100;
									if($new_discount_value > $discountDetail['max_dis_availed'])
									{
										$new_discount_value = $discountDetail['max_dis_availed'];
									}
									
									if($new_discount_value > $discount_value)
									{
										$discount_value = $new_discount_value;
										$discountId = $discountDetail['id'];
									}
									
								}
								else if($discountDetail['type_discount'] == "flat")
								{
									$new_discount_value = $discountDetail['disc_value'];
									if($new_discount_value > $discountDetail['max_dis_availed'])
									{
										$new_discount_value = $discountDetail['max_dis_availed'];
									}
									
									if($new_discount_value > $discount_value)
									{
										$discount_value = $new_discount_value;
										$discountId = $discountDetail['id'];
									}
									
								}
							}
						}
						
					}
				}
				
				// For no Condition 
				if(!empty($DiscountData['no_condition']['seat_fare']))
				{
					foreach($DiscountData['no_condition']['seat_fare'] as $discountDetail)
					{
						if($actual_fare >= $discountDetail['min_ticket_value'] && strtotime(date('Y-m-d')) <= strtotime($discountDetail['effective_to']) && $actual_fare <= $discountDetail['max_ticket_value'])
						{
							if($discountDetail['type_discount'] == "percentage")
							{
								$discount_percentage = $discountDetail['disc_value'];
								$new_discount_value = ($fare_with_conveycharge * $discount_percentage)/100;
								if($new_discount_value > $discountDetail['max_dis_availed'])
								{
									$new_discount_value = $discountDetail['max_dis_availed'];
								}
								
								if($new_discount_value > $discount_value)
								{
									$discount_value = $new_discount_value;
									$discountId = $discountDetail['id'];
								}
								
							}
							else if($discountDetail['type_discount'] == "flat")
							{
								$new_discount_value = $discountDetail['disc_value'];
								if($new_discount_value > $discountDetail['max_dis_availed'])
								{
									$new_discount_value = $discountDetail['max_dis_availed'];
								}
								
								if($new_discount_value > $discount_value)
								{
									$discount_value = $new_discount_value;
									$discountId = $discountDetail['id'];
								}
								
							}
						}
					}
				}
				
				
				$discount_detail['discount_value'] = $discount_value;
				$discount_detail['discountId'] = $discountId;
			}
		}
		
		return $discount_detail;
	}
        
    public function discount_calculation($actual_fare, $fare_with_conveycharge, $busTypeId, $opId, $role_id){ 
            $discount_value = 0;
            $discountId = 0;
		
            $date = date('Y-m-d'); 
            $discount_config = $this->ci->discount_model->where('"'.$date.'" between effective_from and effective_to')->where(array("is_generic_coupon_based" => '0',"status" => '0'))->like('type_of_user', $role_id)->order_by('id','desc')->find_all();
            

            $discountArray = array();
            $discountArray['enable_discount'] = false;
            if($discount_config)
            {
                    /* New function to save discount data in to redis cache ----- */
                    $array = json_decode(json_encode($discount_config), True);

                    $discountArray['enable_discount'] = true;
                    $discountArray['bus_type_based'] = FindValueFromArray($array,1,'is_bus_type_based');

                    $discountArray['bus_type_based']["seat_fare"] = FindValueFromArray($array,1,'is_bus_type_based','seat_fare','discount_on');

                    $discountArray['bus_type_based']["base_fare"] = FindValueFromArray($array,1,'is_bus_type_based','base_fare','discount_on');

                    $discountArray['bus_type_based']["total_fare"] = FindValueFromArray($array,1,'is_bus_type_based','total_fare','discount_on');

                    $discountArray['is_operator_based'] = FindValueFromArray($array,1,'is_operator_based');

                    $discountArray['is_operator_based']["seat_fare"] = FindValueFromArray($array,1,'is_operator_based','seat_fare','discount_on');

                    $discountArray['is_operator_based']["base_fare"] = FindValueFromArray($array,1,'is_operator_based','base_fare','discount_on');

                    $discountArray['is_operator_based']["total_fare"] = FindValueFromArray($array,1,'is_operator_based','total_fare','discount_on');

                    $discountArray['transaction_based'] = FindValueFromArray($array,1,'is_transaction_based');

                    $discountArray['ticket_count_based'] = FindValueFromArray($array,1,'is_ticket_count_based');

                    $discountArray['no_condition'] = FindValueFromArray($array,0);

                    $discountArray['no_condition']["seat_fare"] = FindValueFromArray($array,0,'','seat_fare','discount_on');

                    $discountArray['no_condition']["base_fare"] = FindValueFromArray($array,0,'','base_fare','discount_on');

                    $discountArray['no_condition']["total_fare"] = FindValueFromArray($array,0,'','total_fare','discount_on');
            }	
            if(!empty($discountArray['bus_type_based']["seat_fare"]) || !empty($discountArray['is_operator_based']["seat_fare"]))
            {

                    if(!empty($discountArray['bus_type_based']["seat_fare"]))
                    {
                            foreach($discountArray['bus_type_based']["seat_fare"] as $discountDetail)
                            {
                                    if($actual_fare >= $discountDetail['min_ticket_value'] && strtotime(date('Y-m-d')) <= strtotime($discountDetail['effective_to']) && $discountDetail['bus_type'] == $busTypeId && $actual_fare <= $discountDetail['max_ticket_value'])
                                    {
                                            if($discountDetail['type_discount'] == "percentage")
                                            {
                                                    $discount_percentage = $discountDetail['disc_value'];
                                                    $new_discount_value = ($fare_with_conveycharge * $discount_percentage)/100;
                                                    if($new_discount_value > $discountDetail['max_dis_availed'])
                                                    {
                                                            $new_discount_value = $discountDetail['max_dis_availed'];
                                                    }

                                                    if($new_discount_value > $discount_value)
                                                    {
                                                            $discount_value = $new_discount_value;
                                                            $discountId = $discountDetail['id']; 
                                                            $discount_type = $discountDetail['type_discount'];
                                                            $discount_on = 'seat_fare';
                                                            $description = $discountDetail['description']; 
                                                    }

                                            }
                                            else if($discountDetail['type_discount'] == "flat")
                                            {
                                                    $new_discount_value = $discountDetail['disc_value'];
                                                    if($new_discount_value > $discountDetail['max_dis_availed'])
                                                    {
                                                            $new_discount_value = $discountDetail['max_dis_availed'];
                                                    }

                                                    if($new_discount_value > $discount_value)
                                                    {
                                                            $discount_value = $new_discount_value;
                                                            $discountId = $discountDetail['id'];
                                                            $discount_type = $discountDetail['type_discount'];
                                                            $discount_on = 'seat_fare';
                                                            $description = $discountDetail['description']; 
                                                    }

                                            }
                                    } 
                            } 
                    } 
                    if (!empty($discountArray['is_operator_based']["seat_fare"]))
                    {

                            foreach($discountArray['is_operator_based']["seat_fare"] as $discountDetail)
                            {
                                    if($actual_fare >= $discountDetail['min_ticket_value'] && strtotime(date('Y-m-d')) <= strtotime($discountDetail['effective_to']) && $discountDetail['op_id'] == $opId && $actual_fare <= $discountDetail['max_ticket_value'])
                                    {
                                            if($discountDetail['type_discount'] == "percentage")
                                            {
                                                    $discount_percentage = $discountDetail['disc_value'];
                                                    $new_discount_value = ($fare_with_conveycharge * $discount_percentage)/100;
                                                    if($new_discount_value > $discountDetail['max_dis_availed'])
                                                    {
                                                            $new_discount_value = $discountDetail['max_dis_availed'];
                                                    }

                                                    if($new_discount_value > $discount_value)
                                                    {
                                                            $discount_value = $new_discount_value;
                                                            $discountId = $discountDetail['id']; 
                                                            $discount_type = $discountDetail['type_discount'];
                                                            $discount_on = 'seat_fare';
                                                            $description = $discountDetail['description']; 
                                                    }

                                            }
                                            else if($discountDetail['type_discount'] == "flat")
                                            {
                                                    $new_discount_value = $discountDetail['disc_value'];
                                                    if($new_discount_value > $discountDetail['max_dis_availed'])
                                                    {
                                                            $new_discount_value = $discountDetail['max_dis_availed'];
                                                    }

                                                    if($new_discount_value > $discount_value)
                                                    {
                                                            $discount_value = $new_discount_value;
                                                            $discountId = $discountDetail['id'];
                                                            $discount_type = $discountDetail['type_discount'];
                                                            $discount_on = 'seat_fare';
                                                            $description = $discountDetail['description']; 
                                                    }

                                            }
                                    }
                            }
                    }
            }
            // For no Condition 
            if(!empty($discountArray['no_condition']['seat_fare']))
            { 
                    foreach($discountArray['no_condition']['seat_fare'] as $discountDetail)
                    {
                            if($actual_fare >= $discountDetail['min_ticket_value'] && strtotime(date('Y-m-d')) <= strtotime($discountDetail['effective_to']) && $actual_fare <= $discountDetail['max_ticket_value'])
                            { 
                                    if($discountDetail['type_discount'] == "percentage")
                                    {
                                            $discount_percentage = $discountDetail['disc_value'];
                                            $new_discount_value = ($fare_with_conveycharge * $discount_percentage)/100;                                            
                                            if($new_discount_value > $discountDetail['max_dis_availed'])
                                            {
                                                    $new_discount_value = $discountDetail['max_dis_availed'];
                                            }
                                            if($new_discount_value > $discount_value)
                                            {
                                                    $discount_value = $new_discount_value;
                                                    $discountId = $discountDetail['id'];
                                                    $discount_type = $discountDetail['type_discount'];
                                                    $discount_on = 'seat_fare';
                                                    $description = $discountDetail['description']; 
                                            }
                                            
                                    }
                                    else if($discountDetail['type_discount'] == "flat")
                                    {
                                            $new_discount_value = $discountDetail['disc_value'];
                                            if($new_discount_value > $discountDetail['max_dis_availed'])
                                            {
                                                    $new_discount_value = $discountDetail['max_dis_availed'];
                                            }

                                            if($new_discount_value > $discount_value)
                                            {
                                                    $discount_value = $new_discount_value;
                                                    $discountId = $discountDetail['id'];
                                                    $discount_type = $discountDetail['type_discount'];
                                                    $discount_on = 'seat_fare';
                                                    $description = $discountDetail['description']; 
                                            }
                                    }
                            } 
                    }
            }
            if(!empty($discountId))
                $discount_detail['discountId'] = $discountId;
             if(!empty($description))
                $discount_detail['description'] = $description;
            if(!empty($actual_fare))
                $discount_detail['seat_fare'] = $actual_fare;
            if(!empty($fare_with_conveycharge))
                $discount_detail['seat_fare_with_tax'] = $fare_with_conveycharge;    
            if(!empty($fare_with_conveycharge) && !empty($discount_value))
                $discount_detail['seat_fare_with_tax_with_discount'] = $fare_with_conveycharge - $discount_value;
            if(!empty($discount_type))
                $discount_detail['discount_type'] = $discount_type;
            if(!empty($discount_value))
                 $discount_detail['discount_value'] = $discount_value;
            if(!empty($discount_on))
                $discount_detail['discount_on'] = $discount_on;
            if(!empty($discount_detail['discount_type']) && $discount_detail['discount_type'] == "percentage")
            {
                $discount_detail['discount_rate'] = isset($discount_percentage)?$discount_percentage:'0';
            }
            if(!empty($discount_detail['discount_type']) && $discount_detail['discount_type'] == "flat"){
                $discount_detail['discount_rate'] = isset($discount_value)?$discount_value:'0';
            }
            return $discount_detail;
        }
        
    }//End of class