<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//require_once('phpass-0.1/PasswordHash.php');
//
//define('STATUS_ACTIVATED', '1');
//define('STATUS_NOT_ACTIVATED', '0');

/**
 * auth
 *
 * Authentication library for Code Igniter.
 *
 * @package		auth
 * @author		Ilya Konyukhov (http://konyukhov.com/soft/)
 * @version		1.0.9
 * @based on	DX Auth by Dexcell (http://dexcell.shinsengumiteam.com/dx_auth)
 * @license		MIT License Copyright (c) 2008 Erick Hartanto
 */
class Auth
{
	private $error = array();

	function __construct()
	{
		$this->ci =& get_instance();

        // $this->ci->config->load('auth', TRUE);
        $this->ci->load->library('session');
		$this->ci->load->database();
        $this->ci->load->model(array('users_model', 'role_model','common_model', 'user_login_count_model', 'wallet_model'));
        $this->pos_encryption_key='QWRTEfnfdys635';
	}

	/**
	 * Login user on the site. Return TRUE if login is successful
	 * (user exists and activated, password is correct), otherwise FALSE.
	 *
	 * @param	string	(username or email or both depending on settings in config file)
	 * @param	string
	 * @param	bool
	 * @return	bool
	 */
	function login($username, $pass) {
		
		$msg_type =  '';
		$msg =  '';		
        $udata = $this->ci->users_model->where(array("username" => $username,"is_deleted" => "N"))->find_all();
		$users_action_log = array();
		if (!empty($udata)) 
		{
            //check if user active
            $user_array = $udata[0];
            if ($user_array->status == "Y") 
			{				
				if($this->get_login_attempts($username) >= $this->ci->config->item('login_max_attempts'))
				{ // disable account and redirect to home page
					
					$this->ci->users_model->id = $user_array->id;
					$this->ci->users_model->status = 'N';					
					$this->ci->users_model->save();
			
					$users_action_log = array("user_id" => $user_array->id,
                                      "name" => $username,
                                      "url" => "",
                                      "action" => "login",
                                      "message" => "account disabled as user exceeded maximum limit of invalid login attempts.",
                                      "ip_address" => $this->ci->input->ip_address()
                                      );
									  
					users_action_log($users_action_log);			
					$this->ci->session->set_flashdata('msg_type', 'error');
					$this->ci->session->set_flashdata('msg', 'Your account is disabled as you have exceeded maximum limit of invalid login attempts. Please contact support.');
					redirect(base_url()."admin/login/login_view");								  
				}			
								
                $salt = $user_array->salt;
                $user_password = $user_array->password;
                if ($pass != "")
				{
                  
                    $password = md5($salt . $pass);
                  
                    if ($password == $user_password)
					{                        
                        $roledata = $this->ci->role_model->find($user_array->role_id);
                        $DocData = $this->ci->users_model->getProfileImg($user_array->id,3);
						$DocProfile = $DocData->large_img_link;
						$path = base_url().'uploads/agent_docs/'.$user_array->id.'/photo/large/';
						
                        if($user_array->profile_image != ""){                            
                            $profile_image = base_url().PROFILE_IMAGE.$user_array->profile_image;
						}elseif(@getimagesize($path.$DocProfile)){   //check image is present or not
							 $profile_image = $path.$DocProfile;
                        }elseif($user_array->gender =='M'){
                             $profile_image = base_url().'user_profile/male.jpeg';
                        }elseif($user_array->gender =='F'){
                             $profile_image = base_url().'user_profile/female.jpeg';
                        }
                        if($user_array->level == MASTER_DISTRIBUTOR_LEVEL) {
                        	$md_id = $user_array->id;
                        }
                        else {
                        	$md_id = $user_array->level_2;;
                        }
						
						$session_id = session_id();
						
                         $user_data = array(
                            "id" => $user_array->id,
                            "user_id" => $user_array->id,
                            "role_id" => $user_array->role_id,
                            "role_name" => $roledata->role_name,
                            "username" => rokad_decrypt($user_array->username,$this->ci->config->item('pos_encryption_key')),
                            "email" => rokad_decrypt($user_array->email,$this->ci->config->item('pos_encryption_key')),
                            "display_name" => $user_array->display_name,
                            "first_name" => $user_array->first_name,
                            "last_name" => $user_array->last_name,
                            "mobile_no" => rokad_decrypt($user_array->mobile_no,$this->ci->config->item('pos_encryption_key')),
                            "profile_image" => $profile_image,
                            "level" => $user_array->level,
                            "master_distributor_id" => $md_id,
                            "t_pin" => $user_array->tpin,
                            "kyc_verify_status" => $user_array->kyc_verify_status,
                            "session_id" => $session_id
                        );
                        /****************** check if remember is on start ********************/
                        if(isset($login_post["remember"]) && $login_post["remember"] == "on") {
                            $remember_value = getRandomId(16);
                            $this->ci->users_model->id = $user_array->id;
                            $this->ci->users_model->remember_me = $remember_value;
                            $remem_id = $this->ci->users_model->save();

                            if($remem_id > 0)
                            {
                                $this->input->set_cookie("remember_me",$remember_value,7776000);    
                            }
                        }
                        /***************** check if remember is on end ********************/
                        $this->ci->session->set_userdata($user_data);
                        $this->ci->session->set_flashdata('msg_type', 'success');
                        $this->ci->session->set_flashdata('msg', 'Welcome To Rokad.');
						
						$UserUpdate = array("session_id" => $session_id);
						$UpdateId = $this->ci->common_model->update('users','id',$user_array->id,$UserUpdate);
                        
                        $users_action_log = array("user_id" => $user_array->id,
                                                  "name" => $user_array->first_name." ".$user_array->last_name,
                                                  "url" => "user_id = ".$user_array->id,
                                                  "action" => "logged in",
                                                  "message" => "logged in",
                                                  "RMN" => rokad_decrypt($user_array->mobile_no,$this->ci->config->item('pos_encryption_key'))
                                                  );
                        users_action_log($users_action_log);
						$this->clear_login_attempts($username);
                        
                        $this->ci->output->clear_all_cache();
					 	if($roledata->front_back_access == 'both') {
                            if($roledata->home_page == 'dashboard') {

                                redirect(base_url() . "admin/dashboard");
                            }
                            else if ($roledata->home_page == 'home') {
                                redirect(base_url()."front/home/search_new");
                            }
                            else {
                                redirect(base_url()."home");
                            }
                        }
                        else if($roledata->front_back_access == 'back') {
                        	redirect(base_url() . "admin/dashboard");
                        }
                        else if ($roledata->front_back_access == 'front'){
                            redirect(base_url()."front/home/search_new");
                        }
                        else {
                            redirect(base_url()."home");
                        }
                    } 
                    else 
					{                        
						//insert into user action log
                        $users_action_log = array("user_id" => $user_array->id,
                                                  "name" => $username,
                                                  "url" => "user_id = ".$user_array->id,
                                                  "action" => "logged in",
                                                  "message" => "Username or Password is incorrect"
                                                  );						
						
						users_action_log($users_action_log);
						
						$msg = 'Username or Password is incorrect.';						
						$this->increase_login_attempt($username);						
						if($this->get_login_attempts($username) == ($this->ci->config->item('login_max_attempts')))
						{ 
							$msg .= 'Last attempt left before your account get locked';
						}						
						$this->ci->session->set_flashdata('msg_type', 'error');
						$this->ci->session->set_flashdata('msg', $msg);						
						redirect(base_url()."admin/login/login_view");						
                    }
                } 
                else
				{					
                    //insert into user action log
                    $users_action_log = array("user_id" => $user_array->id,
                                              "name" => $user_array->first_name." ".$user_array->last_name,
                                              "url" => "user_id = ".$user_array->id,
                                              "action" => "logged in",
                                              "message" => "tried logged with password empty"
                                              );			
					
					users_action_log($users_action_log);
					
					$msg = 'Username or Password is incorrect.';						
					$this->increase_login_attempt($username);						
					if($this->get_login_attempts($username) == ($this->ci->config->item('login_max_attempts')))
					{ 
						$msg .= 'Last attempt left before your account get locked';
					}						
					$this->ci->session->set_flashdata('msg_type', 'error');
					$this->ci->session->set_flashdata('msg', $msg);						
					redirect(base_url()."admin/login/login_view");
				}
            } 
            else 
			{			
                //insert into user action log
                $users_action_log = array("user_id" => $user_array->id,
                                          "name" => $user_array->first_name." ".$user_array->last_name,
                                          "url" => "user_id = ".$user_array->id,
                                          "action" => "logged in",
                                          "message" => "tried logged with disabled account"
                                          );			
				
				users_action_log($users_action_log);			
				$this->ci->session->set_flashdata('msg_type', 'error');
				$this->ci->session->set_flashdata('msg', 'Your account is disabled.');		
				redirect(base_url()."admin/login/login_view");
			}				
        } 
        else
		{
			$this->increase_login_attempt($username);				
            //insert into user action log
            $users_action_log = array("user_id" => "",
                                      "name" => $username,
                                      "url" => "",
                                      "action" => "logged in",
                                      "message" => "tried logged in",
                                      "ip_address" => $this->ci->input->ip_address()
                                      );
									  
			users_action_log($users_action_log);			
			$this->ci->session->set_flashdata('msg_type', 'error');
			$this->ci->session->set_flashdata('msg', 'Username or Password is incorrect.');		
			if($this->get_login_attempts($username) >= $this->ci->config->item('login_max_attempts'))
			{ // redirect to home page			
				redirect(base_url());
			}
			else
			{
				redirect(base_url()."admin/login/login_view");
			}
		}	
	}

	/**
	 * Logout user from the site
	 *
	 * @return	void
	 */
	function logout()
	{
		$this->delete_autologin();

		// See http://codeigniter.com/forums/viewreply/662369/ as the reason for the next line
		$this->ci->session->set_userdata(array('user_id' => '', 'username' => '', 'status' => ''));

		$this->ci->session->sess_destroy();
	}

	/**
	 * Check if user logged in. Also test if user is activated or not.
	 *
	 * @param	bool
	 * @return	bool
	 */
	function is_logged_in($activated = TRUE)
	{
		return $this->ci->session->userdata('status') === ($activated ? STATUS_ACTIVATED : STATUS_NOT_ACTIVATED);
	}

	/**
	 * Get user_id
	 *
	 * @return	string
	 */
	function get_user_id()
	{
		return $this->ci->session->userdata('user_id');
	}

	public function user_id()
	{
		return $this->ci->session->userdata('user_id');
	}

	/**
	 * Get username
	 *
	 * @return	string
	 */
	function get_username()
	{
		return $this->ci->session->userdata('username');
	}

	/**
	 * Create new user on the site and return some data about it:
	 * user_id, username, password, email, new_email_key (if any).
	 *
	 * @param	string
	 * @param	string
	 * @param	string
	 * @param	bool
	 * @return	array
	 */
	function create_user($username, $email, $password, $email_activation)
	{
		if ((strlen($username) > 0) AND !$this->ci->users->is_username_available($username)) {
			$this->error = array('username' => 'auth_username_in_use');

		} elseif (!$this->ci->users->is_email_available($email)) {
			$this->error = array('email' => 'auth_email_in_use');

		} else {
			// Hash password using phpass
			$hasher = new PasswordHash(
					$this->ci->config->item('phpass_hash_strength', 'auth'),
					$this->ci->config->item('phpass_hash_portable', 'auth'));
			$hashed_password = $hasher->HashPassword($password);

			$data = array(
				'username'	=> $username,
				'password'	=> $hashed_password,
				'email'		=> $email,
				'last_ip'	=> $this->ci->input->ip_address(),
			);

			if ($email_activation) {
				$data['new_email_key'] = md5(rand().microtime());
			}
			if (!is_null($res = $this->ci->users->create_user($data, !$email_activation))) {
				$data['user_id'] = $res['user_id'];
				$data['password'] = $password;
				unset($data['last_ip']);
				return $data;
			}
		}
		return NULL;
	}

	/**
	 * Check if username available for registering.
	 * Can be called for instant form validation.
	 *
	 * @param	string
	 * @return	bool
	 */
	function is_username_available($username)
	{
		return ((strlen($username) > 0) AND $this->ci->users->is_username_available($username));
	}

	/**
	 * Check if email available for registering.
	 * Can be called for instant form validation.
	 *
	 * @param	string
	 * @return	bool
	 */
	function is_email_available($email)
	{
		return ((strlen($email) > 0) AND $this->ci->users->is_email_available($email));
	}

	/**
	 * Change email for activation and return some data about user:
	 * user_id, username, email, new_email_key.
	 * Can be called for not activated users only.
	 *
	 * @param	string
	 * @return	array
	 */
	function change_email($email)
	{
		$user_id = $this->ci->session->userdata('user_id');

		if (!is_null($user = $this->ci->users->get_user_by_id($user_id, FALSE))) {

			$data = array(
				'user_id'	=> $user_id,
				'username'	=> $user->username,
				'email'		=> $email,
			);
			if (strtolower($user->email) == strtolower($email)) {		// leave activation key as is
				$data['new_email_key'] = $user->new_email_key;
				return $data;

			} elseif ($this->ci->users->is_email_available($email)) {
				$data['new_email_key'] = md5(rand().microtime());
				$this->ci->users->set_new_email($user_id, $email, $data['new_email_key'], FALSE);
				return $data;

			} else {
				$this->error = array('email' => 'auth_email_in_use');
			}
		}
		return NULL;
	}

	/**
	 * Activate user using given key
	 *
	 * @param	string
	 * @param	string
	 * @param	bool
	 * @return	bool
	 */
	function activate_user($user_id, $activation_key, $activate_by_email = TRUE)
	{
		$this->ci->users->purge_na($this->ci->config->item('email_activation_expire', 'auth'));

		if ((strlen($user_id) > 0) AND (strlen($activation_key) > 0)) {
			return $this->ci->users->activate_user($user_id, $activation_key, $activate_by_email);
		}
		return FALSE;
	}

	/**
	 * Set new password key for user and return some data about user:
	 * user_id, username, email, new_pass_key.
	 * The password key can be used to verify user when resetting his/her password.
	 *
	 * @param	string
	 * @return	array
	 */
	function forgot_password($login)
	{
		if (strlen($login) > 0) {
			if (!is_null($user = $this->ci->users->get_user_by_login($login))) {

				$data = array(
					'user_id'		=> $user->id,
					'username'		=> $user->username,
					'email'			=> $user->email,
					'new_pass_key'	=> md5(rand().microtime()),
				);

				$this->ci->users->set_password_key($user->id, $data['new_pass_key']);
				return $data;

			} else {
				$this->error = array('login' => 'auth_incorrect_email_or_username');
			}
		}
		return NULL;
	}

	/**
	 * Check if given password key is valid and user is authenticated.
	 *
	 * @param	string
	 * @param	string
	 * @return	bool
	 */
	function can_reset_password($user_id, $new_pass_key)
	{
		if ((strlen($user_id) > 0) AND (strlen($new_pass_key) > 0)) {
			return $this->ci->users->can_reset_password(
				$user_id,
				$new_pass_key,
				$this->ci->config->item('forgot_password_expire', 'auth'));
		}
		return FALSE;
	}

	/**
	 * Replace user password (forgotten) with a new one (set by user)
	 * and return some data about it: user_id, username, new_password, email.
	 *
	 * @param	string
	 * @param	string
	 * @return	bool
	 */
	function reset_password($user_id, $new_pass_key, $new_password)
	{
		if ((strlen($user_id) > 0) AND (strlen($new_pass_key) > 0) AND (strlen($new_password) > 0)) {

			if (!is_null($user = $this->ci->users->get_user_by_id($user_id, TRUE))) {

				// Hash password using phpass
				$hasher = new PasswordHash(
						$this->ci->config->item('phpass_hash_strength', 'auth'),
						$this->ci->config->item('phpass_hash_portable', 'auth'));
				$hashed_password = $hasher->HashPassword($new_password);

				if ($this->ci->users->reset_password(
						$user_id,
						$hashed_password,
						$new_pass_key,
						$this->ci->config->item('forgot_password_expire', 'auth'))) {	// success

					// Clear all user's autologins
					$this->ci->load->model('auth/user_autologin');
					$this->ci->user_autologin->clear($user->id);

					return array(
						'user_id'		=> $user_id,
						'username'		=> $user->username,
						'email'			=> $user->email,
						'new_password'	=> $new_password,
					);
				}
			}
		}
		return NULL;
	}

	/**
	 * Change user password (only when user is logged in)
	 *
	 * @param	string
	 * @param	string
	 * @return	bool
	 */
	function change_password($old_pass, $new_pass)
	{
		$user_id = $this->ci->session->userdata('user_id');

		if (!is_null($user = $this->ci->users->get_user_by_id($user_id, TRUE))) {

			// Check if old password correct
			$hasher = new PasswordHash(
					$this->ci->config->item('phpass_hash_strength', 'auth'),
					$this->ci->config->item('phpass_hash_portable', 'auth'));
			if ($hasher->CheckPassword($old_pass, $user->password)) {			// success

				// Hash new password using phpass
				$hashed_password = $hasher->HashPassword($new_pass);

				// Replace old password with new one
				$this->ci->users->change_password($user_id, $hashed_password);
				return TRUE;

			} else {															// fail
				$this->error = array('old_password' => 'auth_incorrect_password');
			}
		}
		return FALSE;
	}

	/**
	 * Change user email (only when user is logged in) and return some data about user:
	 * user_id, username, new_email, new_email_key.
	 * The new email cannot be used for login or notification before it is activated.
	 *
	 * @param	string
	 * @param	string
	 * @return	array
	 */
	function set_new_email($new_email, $password)
	{
		$user_id = $this->ci->session->userdata('user_id');

		if (!is_null($user = $this->ci->users->get_user_by_id($user_id, TRUE))) {

			// Check if password correct
			$hasher = new PasswordHash(
					$this->ci->config->item('phpass_hash_strength', 'auth'),
					$this->ci->config->item('phpass_hash_portable', 'auth'));
			if ($hasher->CheckPassword($password, $user->password)) {			// success

				$data = array(
					'user_id'	=> $user_id,
					'username'	=> $user->username,
					'new_email'	=> $new_email,
				);

				if ($user->email == $new_email) {
					$this->error = array('email' => 'auth_current_email');

				} elseif ($user->new_email == $new_email) {		// leave email key as is
					$data['new_email_key'] = $user->new_email_key;
					return $data;

				} elseif ($this->ci->users->is_email_available($new_email)) {
					$data['new_email_key'] = md5(rand().microtime());
					$this->ci->users->set_new_email($user_id, $new_email, $data['new_email_key'], TRUE);
					return $data;

				} else {
					$this->error = array('email' => 'auth_email_in_use');
				}
			} else {															// fail
				$this->error = array('password' => 'auth_incorrect_password');
			}
		}
		return NULL;
	}

	/**
	 * Activate new email, if email activation key is valid.
	 *
	 * @param	string
	 * @param	string
	 * @return	bool
	 */
	function activate_new_email($user_id, $new_email_key)
	{
		if ((strlen($user_id) > 0) AND (strlen($new_email_key) > 0)) {
			return $this->ci->users->activate_new_email(
					$user_id,
					$new_email_key);
		}
		return FALSE;
	}

	/**
	 * Delete user from the site (only when user is logged in)
	 *
	 * @param	string
	 * @return	bool
	 */
	function delete_user($password)
	{
		$user_id = $this->ci->session->userdata('user_id');

		if (!is_null($user = $this->ci->users->get_user_by_id($user_id, TRUE))) {

			// Check if password correct
			$hasher = new PasswordHash(
					$this->ci->config->item('phpass_hash_strength', 'auth'),
					$this->ci->config->item('phpass_hash_portable', 'auth'));
			if ($hasher->CheckPassword($password, $user->password)) {			// success

				$this->ci->users->delete_user($user_id);
				$this->logout();
				return TRUE;

			} else {															// fail
				$this->error = array('password' => 'auth_incorrect_password');
			}
		}
		return FALSE;
	}

	/**
	 * Get error message.
	 * Can be invoked after any failed operation such as login or register.
	 *
	 * @return	string
	 */
	function get_error_message()
	{
		return $this->error;
	}

	/**
	 * Save data for user's autologin
	 *
	 * @param	int
	 * @return	bool
	 */
	private function create_autologin($user_id)
	{
		$this->ci->load->helper('cookie');
		$key = substr(md5(uniqid(rand().get_cookie($this->ci->config->item('sess_cookie_name')))), 0, 16);

		$this->ci->load->model('auth/user_autologin');
		$this->ci->user_autologin->purge($user_id);

		if ($this->ci->user_autologin->set($user_id, md5($key))) {
			set_cookie(array(
					'name' 		=> $this->ci->config->item('autologin_cookie_name', 'auth'),
					'value'		=> serialize(array('user_id' => $user_id, 'key' => $key)),
					'expire'	=> $this->ci->config->item('autologin_cookie_life', 'auth'),
			));
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Clear user's autologin data
	 *
	 * @return	void
	 */
	private function delete_autologin()
	{
		$this->ci->load->helper('cookie');
		if ($cookie = get_cookie($this->ci->config->item('autologin_cookie_name', 'auth'), TRUE)) {

			$data = unserialize($cookie);

			$this->ci->load->model('auth/user_autologin');
			$this->ci->user_autologin->delete($data['user_id'], md5($data['key']));

			delete_cookie($this->ci->config->item('autologin_cookie_name', 'auth'));
		}
	}

	/**
	 * Login user automatically if he/she provides correct autologin verification
	 *
	 * @return	void
	 */
	private function autologin()
	{
		if (!$this->is_logged_in() AND !$this->is_logged_in(FALSE)) {			// not logged in (as any user)

			$this->ci->load->helper('cookie');
			if ($cookie = get_cookie($this->ci->config->item('autologin_cookie_name', 'auth'), TRUE)) {

				$data = unserialize($cookie);

				if (isset($data['key']) AND isset($data['user_id'])) {

					$this->ci->load->model('auth/user_autologin');
					if (!is_null($user = $this->ci->user_autologin->get($data['user_id'], md5($data['key'])))) {

						// Login user
						$this->ci->session->set_userdata(array(
								'user_id'	=> $user->id,
								'username'	=> $user->username,
								'status'	=> STATUS_ACTIVATED,
						));

						// Renew users cookie to prevent it from expiring
						set_cookie(array(
								'name' 		=> $this->ci->config->item('autologin_cookie_name', 'auth'),
								'value'		=> $cookie,
								'expire'	=> $this->ci->config->item('autologin_cookie_life', 'auth'),
						));

						$this->ci->users->update_login_info(
								$user->id,
								$this->ci->config->item('login_record_ip', 'auth'),
								$this->ci->config->item('login_record_time', 'auth'));
						return TRUE;
					}
				}
			}
		}
		return FALSE;
	}

	/**
	 * Check if login attempts exceeded max login attempts (specified in config)
	 *
	 * @param	string
	 * @return	bool
	 */
	function get_login_attempts($login)
	{
		$this->ci->config->load('auth');
		if ($this->ci->config->item('login_count_attempts')) {		
			$this->ci->load->model('auth/login_attempts');
			return $this->ci->login_attempts->get_attempts_num($this->ci->input->ip_address(), $login);			
		}
		return FALSE;
	}

	/**
	 * Increase number of attempts for given IP-address and login
	 * (if attempts to login is being counted)
	 *
	 * @param	string
	 * @return	void
	 */
	function increase_login_attempt($login)
	{
		$this->ci->config->load('auth');	
		if ($this->ci->config->item('login_count_attempts')) {		
			if($this->get_login_attempts($login) < $this->ci->config->item('login_max_attempts')) {			
				$this->ci->load->model('auth/login_attempts');
				$this->ci->login_attempts->increase_attempt($this->ci->input->ip_address(), $login);
			}
		}	
	}

	/**
	 * Clear all attempt records for given IP-address and login
	 * (if attempts to login is being counted)
	 *
	 * @param	string
	 * @return	void
	 */
	function clear_login_attempts($login)
	{
		$this->ci->config->load('auth');
		if ($this->ci->config->item('login_count_attempts')) {
			$this->ci->load->model('auth/login_attempts');
			$this->ci->login_attempts->clear_attempts(
					$this->ci->input->ip_address(),
					$login,
					$this->ci->config->item('login_attempt_expire'));
		}
	}

	/* function used for api */

	public function api_auth($user, $pass)
	{
		$this->ci->load->model(array('users_model'));
		$flag = false;

		if($user != '' && $pass != '')
		{
                        $user=rokad_encrypt($user, $this->pos_encryption_key);
                       
			$user_details	=  $this->ci->users_model
							->where(array('username' => $user))
							->find_all();
                       
			if($user_details && $user_details[0]->password == md5($user_details[0]->salt . trim($pass)))
			{
				$flag = true;
				// $this->session->set_userdata('user', $user[0]);
			}
		}
		return $flag;
	}

	function get_api_user($username, $password)
	{
		$this->ci->load->model(array('users_model', 'vendor_model'));
		$flag = false;

		$token 	= $this->ci->input->get_request_header('_token');



		// if($user != '' && $password != '' && !empty($token))
		if($username != '' && $password != '')
		{
			$user	=  $this->ci->users_model
							->column('users.*, role.role_name')
							->where(array('username' => $username))
							->or_where(array('email' => $username))
							->join('user_roles role', 'users.role_id = role.id')
							->find_all();


			if($user && $user[0]->password == md5($user[0]->salt . $password))
			{
				$user[0]->vendor = 0;
				if(strtolower($user[0]->role_name) == 'vendor')
		        {

		            $user[0]->vendor = $this->ci->vendor_model->find_by('user_id', $user[0]->id);
		        }

				$flag = $user[0];
				// $this->session->set_userdata('user', $user[0]);
			}
		}
		return $flag;
	}

	function get_user_id_bykey($key)
	{
		// $this->ci->db->from('api_key')->where()
	}
        
        function mlogin($username, $pass)
	{
            $udata = $this->ci->users_model->where('username', $username)->or_where('email', $username)->find_all();
            
            if (!empty($udata)) {
                
                //check if user active
                $user_array = $udata[0];

                if($user_array->agent_type == "b2b_api_partner")
                {
					$this->ci->session->set_flashdata('msg_type', 'error');
					$this->ci->session->set_flashdata('msg', 'Your account is still disabled. Please contact customer support.');
					redirect(base_url()."admin/mlogin");
                }

                if ($user_array->status == "Y") {
                    $salt = $user_array->salt;
                    
                    $user_password = $user_array->password;
                    if ($pass != "") {
                        $password = md5($salt . $pass);
                       
                        if ($password == $user_password) {
							
                            // to generate menu,permission files
                            generate_menu_file($user_array->id, $user_array->role_id);

                            if (check_usermenu_files($user_array->id)) {
                                // // to set session
                                // $this->ci->role_model->id = $user_array->role_id;
                                
                                $roledata = $this->ci->role_model->find($user_array->role_id);

                                if($user_array->profile_image != "")
                                    $profile_image = $user_array->profile_image;
                                else
                                     $profile_image = USER_PROFILE_IMAGE;

                                $user_data = array(
                                    "id" => $user_array->id,
                                    "user_id" => $user_array->id,
                                    // "agent_id" => $user_array->a_id,
                                    //"op_id" => $user_array->op_id,
                                    "role_id" => $user_array->role_id,
                                    "role_name" => $roledata->role_name,
                                    "username" => $user_array->username,
                                    "email" => $user_array->email,
                                    "display_name" => $user_array->display_name,
                                    "first_name" => $user_array->first_name,
                                    "last_name" => $user_array->last_name,
                                    "mobile_no" => $user_array->mobile_no,
                                    "profile_image" => $profile_image
                                );
                                
                                $depot_mapping = $this->depot_lookup($user_array->id);
                                if(!empty($depot_mapping)) {
                                    $user_data['depot_user'] = TRUE;
                                    $user_data['depot_cd'] = $depot_mapping['depot_cd'];
                                    $user_data['project'] = $depot_mapping['project'];
                                }
                                /******************check if logged in user is agent *******************/
                                $agent_mapping  = $this->check_agent($user_array->id);

                                if(!empty($agent_mapping))
                                {
                                	$user_data['is_Agent'] = TRUE;
                                	$user_data['agent_id'] = $agent_mapping['agent_id'];
                                	$user_data['agent_code'] = $agent_mapping['agent_code'];
                                	$user_data['fname'] 	 = $agent_mapping['fname'];
                                	$user_data['DIVISION_CD']= $agent_mapping['zone_cd'];
                                	$user_data['DIVISION_NM']= $agent_mapping['zone_name'];
                                	$user_data['DEPOT_CD']= $agent_mapping['depot_cd'];
                                	$user_data['DEPOT_NM']= $agent_mapping['depot_name'];
                                }
                                

                                /****************** check if remember is on start ********************/
                                if(isset($login_post["remember"]) && $login_post["remember"] == "on")
                                {
                                    $remember_value = getRandomId(16);
                                    $this->ci->users_model->id = $user_array->id;
                                    $this->ci->users_model->remember_me = $remember_value;
                                    $remem_id = $this->ci->users_model->save();

                                    if($remem_id > 0)
                                    {
                                        $this->input->set_cookie("remember_me",$remember_value,7776000);    
                                    }
                                }
                                /***************** check if remember is on end ********************/
                                $this->ci->session->set_userdata($user_data);
                                $this->set_extra_detail();
                                 $role_id=$user_array->role_id;

                                if ($role_id != '19' && $role_id !='20' && $role_id!= '21')
                                {
                                    $this->ci->session->set_flashdata('msg_type', 'success');
                                    $this->ci->session->set_flashdata('msg', 'Welcome To Rokad.');
                                 }
                                //redirect(base_url()."home");

                                //insert into user action log
                                $users_action_log = array("user_id" => $user_array->id,
                                                          "name" => $user_array->first_name." ".$user_array->last_name,
                                                          "url" => "user_id = ".$user_array->id,
                                                          "action" => "logged in",
                                                          "message" => "logged in"
                                                          );
                                users_action_log($users_action_log);
                                // /*
                                // ********************* visitor first time login check start ************* */
                                // $user_visit = $this->ci->user_login_count_model->check_first_time_visit();
                                // // print_r($user_visit);

                                // $boarding_stop_id = $user_visit['boarding_stop_id'];
                                // if ($user_visit['visit'] == 1) {
                                //     $this->ci->session->set_userdata('user_visit', 1);
                                //     $this->ci->session->set_userdata('boarding_stop_id', $user_visit['boarding_stop_id']);
                                // } else {
                                //     $this->ci->session->set_userdata('user_visit', 0);
                                // }

                                // /*                             * ******************* visitor first time login check end ******* */
                                $this->ci->output->clear_all_cache();

                              
                              
								//if(in_array(strtolower($roledata->role_name), array(ORS_AGENT_ROLE_NAME,B2B_API_PARTNER_ROLE_NAME,USER_ROLE_NAME,SUPPORT_ROLE_NAME)))
								eval(FME_AGENT_ROLE_NAME);
								if(in_array(strtolower($roledata->role_name), array(B2B_API_PARTNER_ROLE_NAME,USER_ROLE_NAME,SUPPORT_ROLE_NAME))||in_array(strtolower($roledata->role_name),$fme_agent_role_name))
                                {
                                    redirect(base_url()."home");
                                }
                                else
                                {
                                   // redirect(base_url()."admin/users/profile/" . $user_array->id);
                                    return $user_data;
                                }


                                //redirect("acl/acl/aclmenu");
                            } else {

                                $this->ci->session->set_flashdata('msg_type', 'error');
                                $this->ci->session->set_flashdata('msg', 'Login error. Please try again later.');
                                redirect(base_url()."admin/mlogin");
                            }
                        } else {
                            $this->ci->session->set_flashdata('msg_type', 'error');
                            $this->ci->session->set_flashdata('msg', 'Password is incorrect.');
                            //insert into user action log
                            $users_action_log = array("user_id" => $user_array->id,
                                                      "name" => $username,
                                                      "url" => "user_id = ".$user_array->id,
                                                      "action" => "logged in",
                                                      "message" => "Username or Password is incorrect"
                                                      );
                            users_action_log($users_action_log);
                              $role_id=$user_array->role_id;
                             
                            if ($role_id != '19' && $role_id !='20' && $role_id!= '21')
                                {
                                    redirect(base_url()."admin/mlogin");
                                }else{
                                    redirect(base_url()."admin/mlogin");
                                }
                        }
                    } else {
                        $this->ci->session->set_flashdata('msg_type', 'error');
                        $this->ci->session->set_flashdata('msg', 'Your id is not register with us please login with google or facebook.');
                        redirect(base_url()."admin/mlogin");
                    }
                } else {
                    $this->ci->session->set_flashdata('msg_type', 'error');
                    $this->ci->session->set_flashdata('msg', 'Your account is disabled.');
                    //insert into user action log
                    $users_action_log = array("user_id" => $user_array->id,
                                              "name" => $user_array->first_name." ".$user_array->last_name,
                                              "url" => "user_id = ".$user_array->id,
                                              "action" => "logged in",
                                              "message" => "tried logged with diabled account"
                                              );
                    users_action_log($users_action_log);

                    redirect(base_url()."admin/mlogin/");
                }
            } else {
                $this->ci->session->set_flashdata('msg_type', 'error');
                $this->ci->session->set_flashdata('msg', 'Username or Password is incorrect.');
                //insert into user action log
                $users_action_log = array("user_id" => "",
                                          "name" => $username,
                                          "url" => "",
                                          "action" => "logged in",
                                          "message" => "tried logged in"
                                          );
                
                users_action_log($users_action_log);
                  $role_id=$user_array->role_id;
                  redirect(base_url()."admin/mlogin/");
               
            }
		
	}
    
    public function depot_lookup($user_id) {
        $this->ci->load->model(array('user_depot_model'));
        if(!empty($user_id)) {
            $udata = $this->ci->user_depot_model->where('user_id', $user_id)->as_array()->find_all();
            if(!empty($udata[0])) {
                return $udata[0];
            }
            else {
                return array();
            }
        }
        else {
            return array();
        }

    }

    public function check_agent($user_id)
    {
    	 $this->ci->load->model(array('agent_model'));

    	 if(!empty($user_id))
    	 {
    	 	 $udata = $this->ci->agent_model->get_by_user_id($user_id);
    	 	 if(!empty($udata))
    	 	 {
    	 	 	$udata = get_object_vars($udata);
                return $udata;
            }
            else {
                return array();
            }
    	 }
    	 else
    	 {
    	 	return array();
    	 }
    	
    }
}

/* End of file auth.php */
/* Location: ./application/libraries/auth.php */
