<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Buses_api
{
	public $ci;
	
	function __construct()
	{
		$this->ci =& get_instance();

		$this->ci->load->config('api_holder');

		// show(config_item('api_holder'),1);
	}

	function index(){

	}

	public function __get($name)
    {
    	$provider = config_item('api_holder');
    	$path = config_item('api_holder_path');

    	if(in_array($name, $provider))
    	{
    		if($this->ci->load->is_loaded($path[$name])){
    			return $this->ci->{$name};
    		}

    		$this->ci->load->library($path[$name]);
    		return $this->ci->{$name};
    	}
	}
}