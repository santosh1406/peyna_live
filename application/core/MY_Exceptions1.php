<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Exceptions extends CI_Exceptions {
	protected $ci;
	
	public function __construct()
	{
		parent::__construct();
		$this->ci = & get_instance();

		$this->ob_level = ob_get_level();
		// Note:  Do not log messages from this constructor.
	}

	// --------------------------------------------------------------------

	/**
	 * Exception Logger
	 *
	 * This function logs PHP generated error messages
	 *
	 * @access	private
	 * @param	string	the error severity
	 * @param	string	the error string
	 * @param	string	the error filepath
	 * @param	string	the error line number
	 * @return	string
	 */
	function log_exception($severity, $message, $filepath, $line)
	{
		$severity = ( ! isset($this->levels[$severity])) ? $severity : $this->levels[$severity];

		log_message('error', 'Severity: '.$severity.'  --> '.$message. ' '.$filepath.' '.$line, TRUE);
	}

	// --------------------------------------------------------------------

	/**
	 * 404 Page Not Found Handler
	 *
	 * @access	private
	 * @param	string	the page
	 * @param 	bool	log error yes/no
	 * @return	string
	 */
	function show_404($page = '', $log_error = TRUE)
	{
		$heading = "404 Page Not Found";
		$message = "The page you requested was not found.";

		// By default we log this, but allow a dev to skip it
		if ($log_error)
		{
			log_message('error', '404 Page Not Found --> '.$page);
		}

		echo $this->show_error($heading, $message, 'error_404', 404);
		exit;
	}

	// --------------------------------------------------------------------

	/**
	 * General Error Page
	 *
	 * This function takes an error message as input
	 * (either as a string or an array) and displays
	 * it using the specified template.
	 *
	 * @access	private
	 * @param	string	the heading
	 * @param	string	the message
	 * @param	string	the template name
	 * @param 	int		the status code
	 * @return	string
	 */
	function show_error($heading, $message, $template = 'error_general', $status_code = 500)
	{
		set_status_header($status_code);

		$message = '<p>'.implode('</p><p>', ( ! is_array($message)) ? array($message) : $message).'</p>';

		if (ob_get_level() > $this->ob_level + 1)
		{
			ob_end_flush();
		}
		ob_start();
		// include(APPPATH.'errors/'.$template.'.php');
		$buffer = ob_get_contents();
		ob_end_clean();
		return $buffer;
	}

	// --------------------------------------------------------------------

	/**
	 * Native PHP error handler
	 *
	 * @access	private
	 * @param	string	the error severity
	 * @param	string	the error string
	 * @param	string	the error filepath
	 * @param	string	the error line number
	 * @return	string
	 */
	function show_php_error($severity, $message, $filepath, $line)
	{
		$severity = ( ! isset($this->levels[$severity])) ? $severity : $this->levels[$severity];

		$filepath = str_replace("\\", "/", $filepath);

		// For safety reasons we do not show the full file path
		if (FALSE !== strpos($filepath, '/'))
		{
			$x = explode('/', $filepath);
			$filepath = $x[count($x)-2].'/'.end($x);
		}

		if (ob_get_level() > $this->ob_level + 1)
		{
			ob_end_flush();
		}

		ob_start();
			// include(APPPATH.'errors/error_php.php');
			// include(FCPATH.'errors/error_php.php');
			$buffer = ob_get_contents();

		ob_end_clean();

		$replace_array = array(
							"{{severity}}"    	=> $severity,
							"{{msg}}"    		=> $message,
							"{{filepath}}"    	=> $filepath,
							"{{line}}"    		=> $line,
							"{{current_url}}"   => current_url(),
							"{{time}}"   => date("d-m-Y h:i A"),
						);

		$mail_template =  str_replace(array_keys($replace_array), array_values($replace_array), ERROR_MAIL_TEMPLATE);

		$error_msg = 'Severity: '.$severity.'  --> '.$message. ' '.$filepath.' '.$line.' <br> Url Accessed :- '. current_url();
		
		$msg = $mail_template;
	    
	    log_data('error/app_error_'.date('d-m-Y').'.log',$error_msg);
	    if($this->ci->config->item('enable_error_mail'))
	    {
	    	$this->send_mailer($msg);
	    }
	    else
	    {	   
	    	return $msg;
	    }

		// echo $buffer;
	}

	function send_mailer($msg)
	{
		if(!$this->ci->config->item('enable_error_mail'))
		return false;

		eval(ADMIN_EMAIL_ARRAY);

		$Subjects = "Error: Please solve it soon.";
		$emails = $admin_array;

		// foreach ($emails as $key => $value) {
		// 	mail($value, $Subjects, $msg, $from);	
		// }

		$from 			= 'From: mukesh@bookonspot.com';
		$mail_array 	= array();
        $client_email 	= 'rajendras@trimax.in';
        
        $replacements[$client_email] 	= array();        
        $mail_template 					= $msg;
        
        $mail_array['subject']  = 'Serious error occured';
        $mail_array['from']     = array(COMMON_MAIL_ADDRESS => COMMON_MAIL_LABEL);
        $mail_array['to']       = $client_email ;
        $mail_array['message']  = html_entity_decode($mail_template);
        $mail_array['cc']  		= $admin_array;

        $result = send_mail($mail_array, $replacements);
	}


}
// END Exceptions Class

/* End of file Exceptions.php */
/* Location: ./system/core/Exceptions.php */