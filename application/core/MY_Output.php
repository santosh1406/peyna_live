<?php

class  MY_Output extends CI_Output
{

    public function _display($output = '')
    {
        // Note:  We use load_class() because we can't use $CI =& get_instance()
        // since this function is sometimes called by the caching mechanism,
        // which happens before the CI super object is available.
        $BM =& load_class('Benchmark', 'core');
        $CFG =& load_class('Config', 'core');

        // Grab the super object if we can.
        if (class_exists('CI_Controller', FALSE)) {
            $CI =& get_instance();
        }

        // --------------------------------------------------------------------

        // Set the output data
        if ($output === '') {
            $output =& $this->final_output;
        }

        // --------------------------------------------------------------------

        // Do we need to write a cache file? Only if the controller does not have its
        // own _output() method and we are not dealing with a cache file, which we
        // can determine by the existence of the $CI object above
        if ($this->cache_expiration > 0 && isset($CI) && !method_exists($CI, '_output')) {
            $this->_write_cache($output);
        }

        // --------------------------------------------------------------------

        // Parse out the elapsed time and memory usage,
        // then swap the pseudo-variables with the data

        $elapsed = $BM->elapsed_time('total_execution_time_start', 'total_execution_time_end');

        if ($this->parse_exec_vars === TRUE) {
            $memory = round(memory_get_usage() / 1024 / 1024, 2) . 'MB';
            $output = str_replace(array('{elapsed_time}', '{memory_usage}'), array($elapsed, $memory), $output);
        }

        // --------------------------------------------------------------------

        // Is compression requested?
        if (isset($CI) // This means that we're not serving a cache file, if we were, it would already be compressed
            && $this->_compress_output === TRUE
            && isset($_SERVER['HTTP_ACCEPT_ENCODING']) && strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== FALSE
        ) {
            if ($CI->input->is_ajax_request() != false) {
                ob_flush();
            }
            ob_start('ob_gzhandler');
        }

        // --------------------------------------------------------------------

        // Are there any server headers to send?
        if (count($this->headers) > 0) {
            foreach ($this->headers as $header) {
                @header($header[0], $header[1]);
            }
        }

        // --------------------------------------------------------------------

        // Does the $CI object exist?
        // If not we know we are dealing with a cache file so we'll
        // simply echo out the data and exit.
        if (!isset($CI)) {
            if ($this->_compress_output === TRUE) {
                if (isset($_SERVER['HTTP_ACCEPT_ENCODING']) && strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== FALSE) {
                    header('Content-Encoding: gzip');
                    header('Content-Length: ' . strlen($output));
                } else {
                    // User agent doesn't support gzip compression,
                    // so we'll have to decompress our cache
                    $output = gzinflate(substr($output, 10, -8));
                }
            }

            echo $output;
            log_message('info', 'Final output sent to browser');
            log_message('debug', 'Total execution time: ' . $elapsed);
            return;
        }

        // --------------------------------------------------------------------

        // Do we need to generate profile data?
        // If so, load the Profile class and run it.
        if ($this->enable_profiler === TRUE) {
            $CI->load->library('profiler');
            if (!empty($this->_profiler_sections)) {
                $CI->profiler->set_sections($this->_profiler_sections);
            }

            // If the output data contains closing </body> and </html> tags
            // we will remove them and add them back after we insert the profile data
            $output = preg_replace('|</body>.*?</html>|is', '', $output, -1, $count) . $CI->profiler->run();
            if ($count > 0) {
                $output .= '</body></html>';
            }
        }

        // Does the controller contain a function named _output()?
        // If so send the output there.  Otherwise, echo it.
        if (method_exists($CI, '_output')) {
            $CI->_output($output);
        } else {
            echo $output; // Send it to the browser!
        }

        log_message('info', 'Final output sent to browser');
        log_message('debug', 'Total execution time: ' . $elapsed);
    }


    /**
     * Clears the cache for the specified path
     * @param string $uri The URI path
     * @return boolean TRUE if successful, FALSE if not
     */
    public function clear_path_cache($uri)
    {
        $CI =& get_instance();
        $path = $CI->config->item('cache_path');
        
        $cache_path = ($path == '') ? APPPATH.'cache/' : $path;
        
        $uri =  $CI->config->item('base_url').
        $CI->config->item('index_page').
        $uri;

        $cache_path .= md5($uri);
        
        return @unlink($cache_path);
    }
    
    /**
     * Clears all cache from the cache directory
     */
    public function clear_all_cache()
    {
        $CI =& get_instance();
        $path = $CI->config->item('cache_path');
        
        $cache_path = ($path == '') ? APPPATH.'cache/' : $path;
        
        $handle = opendir($cache_path);

        while (($file = readdir($handle))!== FALSE) 
        {
            //Leave the directory protection alone
            if ($file != '.htaccess' && $file != 'index.html')
            {
               @unlink($cache_path.'/'.$file);
            }
        }

        closedir($handle);
    }
    
    /**
     * Checks to see if a cache file exists for the specified path
     * @param string $uri The URI path to check
     * @return boolean TRUE if it is, FALSE if not
     */
    public function path_cached($uri)
    {
        $CI =& get_instance();
        $path = $CI->config->item('cache_path');
        
        $cache_path = ($path == '') ? APPPATH.'cache/' : $path;
        
        $uri =  $CI->config->item('base_url').
        $CI->config->item('index_page').
        $uri;

        $cache_path .= md5($uri);
        
        return file_exists($cache_path);
    }
    
    /**
     * Returns the cache expiration timestamp for the specified path
     * @param string $uri The URI patch to check
     * @return int|boolean The expiration Unix timestamp or FALSE if there is no cache
     */
    public function get_path_cache_expiration($uri)
    {
        $CI =& get_instance();
        $path = $CI->config->item('cache_path');
        
        $cache_path = ($path == '') ? APPPATH.'cache/' : $path;
        
        $uri =  $CI->config->item('base_url').
        $CI->config->item('index_page').
        $uri;

        $cache_path .= md5($uri);
        
        if (!$fp = @fopen($cache_path, FOPEN_READ))
        {
            return FALSE;
        }
        
        flock($fp, LOCK_SH);

        $cache = '';
        if (filesize($cache_path) > 0)
        {
            $cache = fread($fp, filesize($cache_path));
        }

        flock($fp, LOCK_UN);
        fclose($fp);

        //Strip out the embedded timestamp
        if (!preg_match("/(\d+TS--->)/", $cache, $match))
        {
            return FALSE;
        }
        
        //Return the timestamp
        return (int)trim(str_replace('TS--->', '', $match['1']));
    }
}