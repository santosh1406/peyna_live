<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Exceptions extends CI_Exceptions {
	protected $ci;
	
	public function __construct()
	{
		parent::__construct();

		$this->ci = & get_instance();

		$this->ci->config->load('email_php_errors');


		// Note:  Do not log messages from this constructor.
	}
	

	// --------------------------------------------------------------------

	/**
	 * Exception Logger
	 *
	 * This function logs PHP generated error messages
	 *
	 * @access	private
	 * @param	string	the error severity
	 * @param	string	the error string
	 * @param	string	the error filepath
	 * @param	string	the error line number
	 * @return	string
	 */
	function log_exception($severity, $message, $filepath, $line)
    {
    	$this->ci->config->load('email_php_errors');
    	// if it's enabled
        $this->send_mailer($severity, $message, $filepath, $line);
         
        // do the rest of the codeigniter stuff
        parent::log_exception($severity, $message, $filepath, $line);
    }

 //    public function show_php_error($severity, $message, $filepath, $line)
 //    {
 //    	parent::show_php_error($severity, $message, $filepath, $line);
 //    	$this->send_mailer($severity, $message, $filepath, $line);
	// }
	

	function send_mailer($severity, $message, $filepath, $line)
	{
    $this->ci->load->model('php_error_mail_model');

		if (!config_item('email_php_errors') && ENVIRONMENT != "production"){
			return false;
		}

		// eval(ADMIN_EMAIL_ARRAY);

    $admin_array    = config_item('php_error_to_cc');
    $from 			= 'From: '.config_item('php_error_from');
    $mail_array 	= array();
    $client_email 	= config_item('php_error_from');

    $replacements[$client_email] 	= array(); 


    $subject = config_item('php_error_subject');
    $subject = $this->_replace_short_tags($subject, $severity, $message, $filepath, $line);
    $mail_array['subject']  = $subject;
    $mail_array['from']     = config_item('php_error_from');
    $mail_array['to']       = config_item('php_error_to') ;

    $content = config_item('php_error_content');
    $content = $this->_replace_short_tags($content, $severity, $message, $filepath, $line);
    $current_url = current_url();

    $current_date = date('Y-m-d h:i:s');
    $newdate = strtotime("-30 min" , strtotime($current_date));
    $thirty_min_Ago = date("Y-m-d h:i:s" , $newdate);;
    $where    = ['message' => $message,'Url_accessed'=> $current_url];
    $msg_exist = $this->ci->php_error_mail_model->where($where)->where('Occured_on between "'.$thirty_min_Ago.'" and "'.$current_date.'"')->as_array()->find_all(); 
    if(empty($msg_exist))
    {
      $mail_array['message']  = html_entity_decode($content);
      $mail_array['cc']     = $admin_array;
      $result = send_mail($mail_array, $replacements);

      if($result)
      {
      $insert_error_mail = $this->ci->db->insert('php_error_mail', 
      array( "error_type"    => $severity,
      "message"       => $message,
      "filepath"      => $filepath,
      "Url_accessed"  => current_url(),
      "line"          => $line,
      "Occured_on"    => $current_date,
      "ip_address"    => $this->ci->input->ip_address()));     
      }
    }

  $error_msg = 'Severity: '.$severity.'  --> '.$message. ' '.$filepath.' '.$line.' <br> Url Accessed :- '. current_url();
  log_data('error/app_error_'.date('d-m-Y').'.log',$error_msg);

  return true;
        
	}

	/**
     * replace short tags with values.
     *
     * @access private
     * @param string $content
     * @param string $severity
     * @param string $message
     * @param string $filepath
     * @param int $line
     * @return string
     */
    private function _replace_short_tags($content, $severity, $message, $filepath, $line)
    {
        $replace_array = array(
                            "{{severity}}"      => $severity,
                            "{{msg}}"           => $message,
                            "{{filepath}}"      => $filepath,
                            "{{line}}"          => $line,
                            "{{current_url}}"   => current_url(),
                            "{{ip}}"			=> $this->ci->input->ip_address(),
                            "{{time}}"          => date("d-m-Y h:i A"),
                        );

        $content = str_replace(array_keys($replace_array), array_values($replace_array), $content);
        return $content;
    }
}
// END Exceptions Class

/* End of file Exceptions.php */
/* Location: ./system/core/Exceptions.php */
