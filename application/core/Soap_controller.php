<?php

class Soap_controller extends CI_Controller{

	public $ns;

	public function __construct()
	{
		parent::__construct();

		$this->ns = site_url().'/upsrtc/';
        $this->load->library("Nusoap_lib"); // load nusoap toolkit library in controller
        $this->nusoap_server = new soap_server(); // create soap server object
        $this->nusoap_server->configureWSDL("SOAP Server Using NuSOAP in CodeIgniter", $this->ns); // wsdl cinfiguration
        $this->nusoap_server->wsdl->schemaTargetNamespace = $this->ns; // server namespace

  //       $input_array = array ('a' => "xsd:string", 'b' => "xsd:string"); // "addnumbers" method parameters
		// $return_array = array ("return" => "xsd:string");
		// $this->nusoap_server->register('addnumbers', $input_array, $return_array, "urn:SOAPServerWSDL", "urn:".$ns."/addnumbers", "rpc", "encoded", "Addition Of Two Numbers");
	}


 	
} // end of file
