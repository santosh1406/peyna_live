<?php

class MY_Controller extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	
		if($this->config->item('maintenance_mode') == TRUE)
		{
        	load_front_view(MAINTENANCE_VIEW);
		}
        is_logged_in();		
		
		if ($this->input->is_ajax_request()) {		
		}		
		else{
			$this->check_url_permission();
		}
	}
	
    private function check_url_permission() {
    
        $first = $this->uri->segment(1);
        $second = $this->uri->segment(2);
        $third  = $this->uri->segment(3);

        $url = $first .'/'. $second;
        // echo $url;
        $role_id = $this->session->userdata('role_id');
        $role_name = $this->session->userdata('role_name');

        //echo $url; die();


        if(!empty($role_id)){

            // if($role_name != 'Super Admin'){
                $this->load->model(array('permission_model', 'menu_model','user_role_model'));
                
                $actionUrl = ($third != '') ? $url.'/'. $third : $url;
                
                $menu = $this->menu_model->column('id, display_name, type')->where('link', $actionUrl)->as_array()->find_all();

                if ($menu == false){
                   $menu = $this->menu_model->column('id, display_name, type')->where('link', $url)->as_array()->find_all();
                }

                // echo $this->db->last_query();die;
                // echo "<pre>"; var_dump($menu); // die;
                
                $userRole = $this->user_role_model->column('home_page,  front_back_access')->where('id', $role_id)->as_array()->find_all();

        	    $permissions = $this->permission_model->column('action_id, action_name')->where('menu_id',$menu[0]['id'])->where('group_id', $role_id)->as_array()->find_all();
				
				 //echo $this->db->last_query(); die;
				 // echo "<pre>"; var_dump($permissions);die;                  
                $permissions =true;
                if(empty($permissions)){
                    $this->session->set_flashdata('msg_type', 'success');
        			$this->session->set_flashdata('msg', 'No Access to this Module');
                    redirect(base_url().$userRole[0]['home_page']);
                }else{
                    /*
                    $perms = explode(',', $permissions[0]['action_name']);
                     //echo "<pre>"; print_r($perms);die();
                    
                    switch ($third) {
                        case '':
                            if (!in_array('view', $perms))
                                die('No Access to this Module');
                            break;
                        case 'add':
                            if (!in_array('create', $perms))
                                die('No Access to this Module');
                            break;
                        case 'edit':
                            if (!in_array('edit', $perms))
                                die('No Access to this Module');break;
                        case 'delete':
                            if (!in_array('delete', $perms))
                                die('No Access to this Module');
                            break;
                    } */
                //}
            
            }
        }
            
    }

 	
} // end of file
