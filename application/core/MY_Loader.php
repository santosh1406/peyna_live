<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class MY_Loader extends CI_Loader{
    public function __construct(){
        parent::__construct();
    }
    public function soap_api ($file_path){
        $CI = & get_instance();
     
        $file_path_arr = explode('/', $file_path);
        $file_name = end($file_path_arr);
        
        $file_path = APPPATH.'controllers/'.$file_path.'.php';

        //show($file_path,1);

        $object_name = $file_name;
        $class_name = ucfirst($file_name);


        //$file_path = '/opt/lampp/htdocs/mukesh/project/internal_app/application/controllers/soap_api/Upsrtc.php';

        if(file_exists($file_path)){
            require $file_path;
         
            $CI->{strtolower($object_name)} = new $class_name();
        }
        else
        {
            show_error("Unable to load the requested controller class: ".$class_name);
        }
    }

    /**
     * Returns true if the model with the given name is loaded; false otherwise.
     *
     * @param   string  name for the model
     * @return  bool
     */
    public function is_model_loaded($name) 
    {
        return in_array($name, $this->_ci_models, TRUE);
    }
}