<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of post controller
 * post controller is written to find user menu access.
 * If user dont have permission then user will be redirected to homepage
 * 
 * 
 * BuildMenu function are recursive function. This function is only used to arrange menu in format.
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
class post_controller {

    var $CI;

    public function __construct() {

        $this->CI = &get_instance();
    }

    public function check_is_remembered() {

        remember_login_check();
    }

    public function is_maintenance_mode()
    {
        if(file_exists(APPPATH.'config/config.php'))
        {
            include(APPPATH.'config/config.php');

            if(isset($config['maintenance_mode']) && $config['maintenance_mode']===TRUE)
            {
                $this->show_site_offline();
                exit;
            }
        }
    }

    private function show_site_offline()
    {
         echo "<center>
                    <img src='".base_url().FRONT_ASSETS."images/logo.png' alt='Online Ticket Booking System' />
                    <h1 class='page-title'>System Under Maintenance</h1>
                    <h4 class=''>System is in maintenance. Please try again after sometime.</h4>
                </center>";
    }

    public function check_all_query()
    {
        //enable_profiler();
    }
    /*function initialize() {
        
        $site_lang = $ci->session->userdata('site_lang');
        if ($site_lang) {
            $ci->lang->load('message',$ci->session->userdata('site_lang'));
        } else {
            $ci->lang->load('message','english');
        }
    }*/

    public function load_view()
    {
        //enable_profiler();
        if(!isset($this->CI->session->userdata()["mobile_mode"]))
        {
            include_once(APPPATH.'config/website_constants.php');
            $this->CI->session->set_userdata('mobile_mode',true);
            
        }
        else
        {
            include_once(APPPATH.'config/website_constants.php');
            
        }
    }

}
