<?php
define('BOS_ORS_TICKET_BOOKED_SMS_TEMPLATE', '{{departure_stop}}-{{alightning_stop}}, {{operator_bus_type}} PNR/Tkt No: {{pnr_no}} Travels: {{operator_name}} DOJ {{date_of_journey}} {{boarding_point}}, {{boarding_point_time}} - {{destination_stop}} Seats: {{seat_no}}, {{primary_passanger_name}} | {{primary_passanger_gender}} Contact: {{operator_contact_number}}');

define('BOS_REGISTRATION', 'Dear {{username}},Thank you for registering with us. We have sent activation link to your email {{email}}.');

define('BOS_CANCEL_TICKET', 'Your ticket with Rokad Ref Number: {{bos_ref_no}} is cancelled. Amount: Rs.{{amount_to_pay}} will be refunded in your account.');

define('INTERNAL_TICKET_NOTIFICATION', 'Rokad Ref:{{bos_ref_no}}, TKT Amt:{{total_amount}}, No of Passengers:{{total_seats}}, Cust Name:{{cust_name}}, Mobile:{{mobile_no}}, TOT DIS TKT CNT:{{discount_tkt_count}}, TOT DIS PASNGR CNT:{{discount_psgr_count}}');

define('WALLET_TOPUP_REQUEST_REJECTION_SMS','Your Rokad Top-up request is rejected. Please check your email for details or call Rokad Customer Care No. 02266383930.');

define('WALLET_TOPUP_REQUEST_SMS_FOR_AGENT','Dear {{request_to_username}}, Please accept request from {{request_by_role_name}} {{request_by_username}} of INR {{amount}}');

define('SMS_AGENT_BALANCE_ALERT','Current balance of your Rokad Wallet is {{amount}}. Please recharge your wallet to continue booking tickets.');

define('REGISTRATION_OTP', 'Dear {{username}}, Please enter OTP {{otp}} to complete your registration process.');

define('REGISTRATION_TPIN', 'Welcome {{username}}, to Rokad! Your Transaction Pin is: {{tpin}}.');

define('RESET_USERNAME_OTP', 'Dear {{username}}, Your OTP is {{otp}}. This OTP is valid for 5 minutes. Do not share this OTP with anyone.');

define('WALLET_TOPUP_FROM_PG' , 'Dear {{username}}, {{wallet_id}} is credited by INR {{transaction_amount}} and current Wallet balance is INR {{wallet_amount}}');

define('WALLET_TOPUP_REQUEST_TO','Dear {{request_to_username}}, Please accept request from {{request_by_role_name}} {{request_by_username}} of INR {{amount}}');

define('WALLET_TOPUP_APPROVAL_TO','Dear {{username}} , Your balance request is {{accept_reject}} and your current Wallet balance is INR {{wallet_balance}}');

define('WALLET_TOPUP_APPROVAL_BY','Dear {{username}} , Your Wallet is debited by INR {{transaction_amount}} against the reference No {{transaction_no}}	and current Wallet balance is INR {{wallet_balance}}');

define('FUND_TRANSFER_TO','Dear {{transfer_to_username}} , Your wallet is credited by {{transfer_by_username}} for amount INR {{transaction_amount}} and your current Wallet balance is INR {{wallet_amount}}');

define('ACCOUNT_DISABLED', 'Dear {{username}}, Your Rokad account is disabled please contact support.');