<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = 'show404';

$route['translate_uri_dashes'] = FALSE;


/* routes for front end */
$route['render/(.*)']                       = "render_file/get_scripts/$1/$2/$3/$4/$5/$6";


/* routes for admin  */
$route['soap/(.*)']                         = "admin/soap_client/$1/$2/$3";
$route['login/(.*)']                        = "admin/login/$1";

/* front controller Routes */
$route['profile']                        	= "admin/users/get_profile";
$route['contact_us']                        = "front/page/contact_us";
$route['services']                         	= "front/page/services";
$route['about_us']                         	= "front/page/about_us";

$route['home']                         		= "front/home_new";
$route['print_sms_ticket']                  = "front/booking/print_sms_ticket";
$route['cancel_booking']                  	= "front/cancel_booking";
$route['refund']                            = "front/refund"; 
$route['cancel']                  			= "front/cancel_booking/index";
$route['cancel_tickets']                  	= "front/cancel_booking/cancel_tickets1";
$route['is_ticket_cancellable']             = "front/cancel_booking/is_ticket_cancellable";
$route['faq']                  				= "front/page/faq";
$route['static/faq']                  				= "front/page/faq_static";
$route['feedback']                         	= "front/page/feedback";
$route['terms_and_condition']               = "front/page/terms_and_condition";
$route['static/terms_and_condition']               = "front/page/terms_and_condition_static";
$route['testimonials']               		= "front/page/testimonials";
$route['privacy_policy']               		= "front/page/privacy_policy";
$route['static/privacy_policy']               		= "front/page/privacy_policy_static";
$route['booking_history']               	= "front/booking/booking_history";
$route['failed_transaction']               	= "front/transaction/failed_transaction";
$route['agent_registration']           		= "front/agent_registration/agent_registration_view";

/* routes for soap_server*/
$route['server/upsrtc']						= "soap_server/upsrtc/index";

/* routes for back end */
$route['admin/agent/commission']				= "admin/agent/commission";
$route['admin/agent/commission/add']				= "admin/agent/commission_add";
$route['admin/agent/commission/edit/(:any)']			= "admin/agent/commission_edit/$1";
$route['admin/agent/commission/delete/(:any)']			= "admin/agent/commission_delete/$1";
$route['admin/agent/commission/save']				= "admin/agent/commission_save";
$route['admin/agent/commission/get_list']			= "admin/agent/get_commission_list";
$route['admin/agent/commission/check_agent_commission']		= "admin/agent/check_agent_commission";

$route['admin/agent/pattern/ajax_get_pattern/(:any)']		= "admin/agent/ajax_get_pattern/$1";
$route['admin/agent/commission/pattern']					= "admin/agent/view_pattern";
$route['admin/agent/commission/pattern/add']				= "admin/agent/add_pattern_comm/";
$route['admin/agent/commission/pattern/edit/(:any)']				= "admin/agent/edit_pattern_comm/$1";
$route['admin/agent/commission/pattern/save']				= "admin/agent/save_pattern_comm/";


$route['admin/agent/']										= "admin/agent/";
$route['admin/agent/edit/(:any)']							= "admin/agent/agent_edit/$1";


/*************  operator commission   *************/ 
$route['admin/operator/commission']							= "admin/operator/commission";
$route['admin/operator/commission/add']						= "admin/operator/commission_add";
$route['admin/operator/commission/edit']					= "admin/operator/commission_edit/$1";
$route['admin/operator/commission/save']					= "admin/operator/commission_save";
$route['admin/operator/commission/delete']					= "admin/operator/commission_delete/$1";

	

/************************  Rest api section  ************************/
// $route['rest_server/current_booking/getServices']					= "rest_server/bus/getcb_services";

/************************  Rest api section  ************************/

/* End of file routes.php */
/* Location: ./application/config/routes.php */