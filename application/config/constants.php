<?php

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rightsADMIN_RESERVATION_POPUP
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');


/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0755);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
  |--------------------------------------------------------------------------
  | Display Debug backtrace
  |--------------------------------------------------------------------------
  |
  | If set to TRUE, a backtrace will be displayed along with php errors. If
  | error_reporting is disabled, the backtrace will not display, regardless
  | of this setting
  |
 */
define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
  |--------------------------------------------------------------------------
  | Exit Status Codes
  |--------------------------------------------------------------------------
  |
  | Used to indicate the conditions under which the script is exit()ing.
  | While there is no universal standard for error codes, there are some
  | broad conventions.  Three such conventions are mentioned below, for
  | those who wish to make use of them.  The CodeIgniter defaults were
  | chosen for the least overlap with these conventions, while still
  | leaving room for others to be defined in future versions and user
  | applications.
  |
  | The three main conventions used for determining exit status codes
  | are as follows:
  |
  |    Standard C/C++ Library (stdlibc):
  |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
  |       (This link also contains other GNU-specific conventions)
  |    BSD sysexits.h:
  |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
  |    Bash scripting:
  |       http://tldp.org/LDP/abs/html/exitcodes.html
  |
 */
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
define('SMS_LIMIT', 3); // Message Limit

define('EXT', '.php');
// require_once ('admin_config.php');
require_once ('constants/view_constant.php');


//===============================  for mail setting  ================================
// define('COMMON_SMTP_SERVER', 'localhost');
// define('COMMON_SMTP_USERNAME', 'bosadmin');
// define('COMMON_SMTP_PASSWORD', 'TrimaxBos@321');
// define('MAIL_PORT_NO', '25');

define('COMMON_MAIL_ADDRESS', 'noreply@peyna.in');
define('COMMON_MAIL_BOUNCE_EMAIL', 'info@peyna.in');
define('COMMON_MAIL_LABEL', 'Rokad');
define('BOS_SUPPORT_MAIL', 'info@peyna.in');
//===============================  for mail setting  ================================


define('CURRENT_DATE', date("Y-m-d"));

define('TRANSACTION_HANDLING_CHARGES_ON_REFUND_FLAG', TRUE);
define('TRANSACTION_HANDLING_CHARGES_ON_REFUND_VALUE', 10);

define("ADMIN_EMAIL_ARRAY", '$admin_array = array(
                                                "santoshg.bos@gmail.com",
                                                "santoshg.bos@gmail.com"
                                              );');
define("ADMIN_EMAIL_ARRAY_FOR_AGENT", '$admin_array_for_agent = array(
                                                "santoshg.bos@gmail.com",
                                                "santoshg.bos@gmail.com",
                                                "santoshg.bos@gmail.com"
                                              );');

define("SYSTEM_USERS_ARRAY", '$system_users_array = array(
                                                          "super admin",
                                                          "admin",
							  "company",
                                                          "support"
                                                        );');
define("ADMIN_EMAIL_ARRAY_FOR_AGENT_TICKET_TOPUP", '$admin_array_for_agent_ticket_topup = array(
                                                "lilawati.bookonspot@gmail.com",
                                                "surekha.trimax@gmail.com" ,
                                                "anushaponnam25@gmail.com",
                                                "ganeshj@bookonspot.com",
                                                "apurva.gharde@gmail.com",
                                                "ponnamanitha1994@gmail.com"
                                              );');

/* * **************************************
 * * Inter office use user roll array, they will redirect to admin side with limited access 
  /*************************************** */
define("OFFICE_USERS_ROLL_ARRAY", '$office_users_roll_array = array(
                                                          "b2c report",
                                                          "account",
                                                          "b2b report",
                                                          "support admin",
                                                          "call center",
                                                          "csc report"
                                                        );');

define("SYSTEM_ADMIN_ARRAY", '$system_admin_array = array(
														  "super admin",
														  "admin"
														);');

define("CURRENT_BOOKING_SYSTEM_USERS_ARRAY", '$cb_system_users_array = array(
                                                          "rsrtc user",
                                                          "current booking report",
                                                          "current booking admin"
                                                        );');
define("SYSTEM_USERS_ID_ARRAY", '$system_users_id_array = array(
                                                              1,
                                                              2,
                                                              9,
                                                              12
                                                            );');
define("CB_SYSTEM_USERS_ID_ARRAY", '$cb_system_users_id_array = array(
                                                             19,20,21
                                                            );');


define("REGISTERED_USERS_ID_ARRAY", '$registered_users_id_array = array(
                                                                        7,
                                                                        16,
                                                                        17,
                                                                        18
                                                                      );');

define("CUSTOM_ERROR_EMAIL_ARRAY", '$custom_error_email_array = array(
                                                                    "santoshg.bos@gmail.com",
                                                                    "santoshg.bos@gmail.com"
                                                                  );');


define("MSRTC_ERROR_EMAIL_ARRAY", '$msrtc_error_email_array = array(
                                                                    "santoshg.bos@gmail.com"
                                                                  );');


define("UPSRTC_ERROR_EMAIL_ARRAY", '$upsrtc_error_email_array = array(
                                                                    "santoshg.bos@gmail.com"
                                                                  );');


define("MANAGEMENT_ERROR_EMAIL_ARRAY", '$management_error_email_array = array(
                                                                              "santoshg.bos@gmail.com"
                                                                            );');


define("PAYMENT_GATEWAY_ERROR_EMAIL_ARRAY", '$payment_gateway_error_email_array = array(
                                                                                        "santoshg.bos@gmail.com"
                                                                                      );');


define("CONTACT_US_EMAIL", '$contactus_email_array = array(                                                            
                                                            "support@peyna.in"
                                                          );');


define("BOS_BUS_TYPES_AC", '$bos_bus_types_ac_array = array(
                                                              "AC Seater",
                                                              "AC Sleeper",
                                                              "AC Semi Sleeper"
                                                            );');

define("RSRTC_BUS_TYPES_AC", '$rsrtc_bus_types_ac_array = array(
                                                                "GRL",
                                                                "ACS",
                                                                "GND",
                                                                "AC",
                                                                "CAC",
                                                                "VOL",
                                                                "VLF",
                                                                "VLC",
                                                                "VPT"
                                                              );');

define("UPSRTC_BUS_TYPES_NON_AC", '$upsrtc_bus_types_non_ac_array = array(
                                                                "CIT",
                                                                "EXP",
                                                                "GLD",
                                                                "GRM",
                                                                "JNT",
                                                                "ORD",
                                                                "PGL",
                                                                "RPL",
                                                                "SHN",
                                                                "SHT",
                                                                "SLP"
                                                              );');


define("HRTC_BUS_TYPE_ID_ARRAY", '$hrtc_bus_type_id_array = array(
                                                                  "himsuta (volvo)" => 1,
                                                                  "himgaurav (tata-ac)" => 2,
                                                                  "him mani (deluxe)" => 3,
                                                                  "semi deluxe" => 4,
                                                                  "ordinary" => 5,
                                                                  "super fast(ordinary)" => 6
                                                                );');

define("SYSTEM_AGENT_ARRAY", '$system_agent_array = array(
                                                          "ors_agent"
                                                        );');


define("AGENT_ID_ARRAY", '$agent_id_array = array(
                                                    16,
                                                    27
                                                  );');


define('ERROR_MAIL_TEMPLATE', "<table border='1' cellpadding='5' cellspacing='5'>
                                    <tr>
                                      <td colspan='2'>Serious issue</td>
                                    </tr>                                    
                                    <tr>
                                      <td>Error Type</td>
                                      <td>{{severity}}</td>
                                    </tr>
                                    <tr>
                                      <td>message</td>
                                      <td>{{msg}}</td>
                                    </tr>
                                    <tr>
                                      <td>filepath</td>
                                      <td>{{filepath}}</td>
                                    </tr>
                                    <tr>
                                      <td>line</td>
                                      <td>{{line}}</td>
                                    </tr>
                                    <tr>
                                      <td>Url Accessed</td>
                                      <td>{{current_url}}</td>
                                    </tr>
                                    <tr>
                                      <td>Occured on</td>
                                      <td>{{time}}</td>
                                    </tr>
                                  </table>");



define("PAGINATION_ARRAY", '$pagination_array = array("10"=>"10 / page","20"=>"20 / page","30"=>"30 / page","40"=>"40 / page","50"=>"50 / page");');
define('DEFAULT_START_INDEX', '0');
define('DEFAULT_PER_PAGE', '5');
define('DEFAULT_FILTERING_FILED', 'id');
// define('DEFAULT_FILTERING_ORDER',                 'ASC');
define('DEFAULT_FILTERING_ORDER', 'DESC');


//--------      RENDER_CONTROLLERS_CLIENTS_SCRIPTS

define('RENDER_CONTROLLERS', 'render_file/');
define('RENDER_CONTROLLERS_GET_SCRIPTS', RENDER_CONTROLLERS . 'get_scripts/');
define('RENDER_CONTROLLERS_GET_SCRIPTS_NEW', RENDER_CONTROLLERS . 'get_scripts_new/');


define('ADMIN_ENTITY', 'admin');
define('CLIENT_ENTITY', 'client');


/**/

define('CONTACT_CONFIG_ENTITY', "contact");

define('BACK_THEME', 'adminlte');
define('FRONT_THEME', 'travelo');
define('FRONT_THEME_CB', 'travelo_cb');
define('ASSETS', 'assets/');
define('TRAVELO_ASSETS', ASSETS . 'travelo_assets/');
define('BUS_ANGULAR', ASSETS . 'travelo/front/angular/bus/');

define('PROFILE_IMAGE', ASSETS . 'profile_image/');

define('BACK_ASSETS', ASSETS . BACK_THEME . '/' . 'back/');
define('BACK_LAYOUTS', BACK_THEME . '/_layouts/back/');
define('BACK_VIEW', BACK_THEME . '/back/');

define('TMP_FOLDER', FCPATH . 'tmp');
define('LOG_FOLDER', FCPATH . 'log/');
define('CAPTCHA_FOLDER', FCPATH . 'captcha/');
define('UPLOAD_FOLDER', FCPATH . 'uploads/');
define('AGENT_DOCS_FOLDER', UPLOAD_FOLDER . 'agent_docs/');
define('AGENT_API_FOLDER', UPLOAD_FOLDER . 'agent_api_files/');
//===============================  ADMINLTE BACK IMAGE constant  ================================
define('BACK_ASSETS_IMG', BACK_ASSETS . 'img/');

//===============================  BACK VIEW constant  ================================
//===============================  OTHER MODULE constant  ================================

define('OTHER_VIEW_FOLDER', BACK_VIEW . 'other/');

define('DATATABEL_FOLDER', OTHER_VIEW_FOLDER . 'datatable/');

define('DATATABEL_VIEW', DATATABEL_FOLDER . 'datatable');
define('DATATABEL_AJAX', DATATABEL_FOLDER . 'datatable_ajax');
define('DATATABEL_POPUP', DATATABEL_FOLDER . 'datatable_popup');


//===============================  FRONT VIEW constant  ================================
//=============================== CSC ================================
/* define('CSC_PROCESSING_PAGE',                   FRONT_VIEW.'csc_payment_gateway_processing'); */
define('DS', DIRECTORY_SEPARATOR);


//===============================  BACK VIEW constant  ================================
//===============================  User Roles constant  ================================
define('USER_ROLE_NAME', 'users');
define('SUPPORT_ROLE_NAME', 'support');
define('USER_ROLE_ID', '7');

define('CRON_USER_ID', '3001');

define('AGENT_ROLE_NAME', 'agent');
define('FME_ROLE_NAME', 'fme');
define('FME_AGENT_NAME', 'fme_agent');
//define('AGENT_ROLE_ID',     '5');

define('ORS_AGENT_ROLE_NAME', 'ors_agent');
define('FME_AGENT_ROLE_NAME', '$fme_agent_role_name = array("ors_agent","fme_agent");');
define('WALLET_FAILURE_TRANS_EMAILS', '$wallet_failure_trans_emails = array(
                                                            "santoshg.bos@gmail.com",
                                                            "santoshg.bos@gmail.com",
                                                            "santoshg.bos@gmail.com"
                                                          );');
//define('FME_ADMIN_ROLE_ID', "25");
define('FME_ROLE_ID', "26");
define('ORS_AGENT_ROLE_ID', '16');
define('FME_AGENT_ID', "27");
define('FME_AGENT_ROLE_ID', '$fme_agent_role_id = array("16","27");');
define('ORS_AGENT_MARKUP_AMOUNT', 100);
define('ORS_AGENT_MARKUP_TYPE', "per_seat");


define('CORPORATE_ROLE_NAME', 'corporate');
define('CORPORATE_ROLE_ID', '18');

define('ADMIN_ROLE_NAME', 'admin');
define('SUPER_ADMIN_ROLE_NAME', 'Super Admin');
define('API_PROVIDER_ROLE', 'API Provider');
define('SUBAGENT_ROLE_NAME', 'sub agent');
define('DIRECT_AGENT_ROLE', 'Direct Agent');
define('PRINCIPAL_AGENT_ROLE', 'Principal Agent');
define('TERMINAL_AGENT_ROLE', '	terminal Agent');
define('RETAILER_AGENT_ROLE', 'Retailer');


define("ROLE_ARRAY", '$role_array = array("10"=>"10 / page","20"=>"20 / page","30"=>"30 / page","40"=>"40 / page","50"=>"50 / page");');

define('ETRAVELSMART_PROVIDER_ID', '2');
define('HRTC_OPERATOR_ID', 4);
define('HRTC_SERVICE_CHARGE_PERCENTAGE', 1.50);

define('ITS_PROVIDER_ID', 3);
define('ITS_COMMISSION', 8);

define('AGENT_SITE_ARRAY', '$agent_site_array = array("16","27","30","31","32");');



define('ADMIN_VIEW_FOLDER', BACK_VIEW . 'admin/');
define('USER_FOLDER', ADMIN_VIEW_FOLDER . 'users/');


define('USER_PROFILE', USER_FOLDER . 'profile');
define('EDIT_USER_PROFILE', USER_FOLDER . 'edit_profile');
define('EDIT_USER_PROFILE_VIEW', USER_FOLDER . 'edit_profile_view');
/* define('FRONT_USER_PROFILE', FRONT_VIEW . 'userprofile');
  define('THANK_YOU', FRONT_VIEW . 'thank_you');
  define('EMAIL_VERIFY', FRONT_VIEW . 'email_verify'); */

define('API_FOLDER', ADMIN_VIEW_FOLDER . 'api/');

define('API_VIEW', API_FOLDER . 'api');


define('ROLE_FOLDER', ADMIN_VIEW_FOLDER . 'role/');

define('ROLE_VIEW', ROLE_FOLDER . 'role');
define('ROLE_AJAX', ROLE_FOLDER . 'role_ajax');
define('ROLE_POPUP', ROLE_FOLDER . 'role_popup');

define('MENU_FOLDER', ADMIN_VIEW_FOLDER . 'menu/');

define('MENU_VIEW', MENU_FOLDER . 'menu');
define('MENU_AJAX', MENU_FOLDER . 'menu_ajax');
define('MENU_POPUP', MENU_FOLDER . 'menu_popup');

define('USERS_FOLDER', ADMIN_VIEW_FOLDER . 'users/');
define('ORS_USERS_FOLDER', ADMIN_VIEW_FOLDER . 'ors_users/');
define('FME_FOLDER', ADMIN_VIEW_FOLDER . 'fme_master/');

define('REGISTRATION_FOLDER', ADMIN_VIEW_FOLDER . 'registration_folder/');
define('MD_REGISTRATION_VIEW', REGISTRATION_FOLDER . '/md_registration_view');
define('ALL_ROKAD_USERS', REGISTRATION_FOLDER . '/all_rokad_users');
define('ALL_ROKAD_USERS_AJAX', REGISTRATION_FOLDER . '/all_rokad_users_ajax');
define('EDIT_ROKAD_USERS', REGISTRATION_FOLDER . '/edit_rokad_users');
define('VIEW_ROKAD_USERS', REGISTRATION_FOLDER . '/view_details');
define('ADMIN_REGISTRATION_VIEW', REGISTRATION_FOLDER . '/admin_registration_view');
define('TECHNICAL_USERS_LIST', REGISTRATION_FOLDER . '/technical_users_list');

define('USERS_VIEW', USERS_FOLDER . 'users');
define('USERS_AJAX', USERS_FOLDER . 'users_ajax');
define('USERS_POPUP', USERS_FOLDER . 'users_popup');
define('USERS_PASSWORD_POPUP', USERS_FOLDER . 'generate_password_popup');
define('USERS_KYC_POPUP', USERS_FOLDER . 'kyc_popup');
define('NOT_AUTHORIZED_USERS', USERS_FOLDER . 'users_not_authorized_view');

define('ORS_USERS_VIEW', ORS_USERS_FOLDER . 'users');
define('ORS_USERS_AJAX', ORS_USERS_FOLDER . 'users_ajax');
define('ORS_USERS_POPUP', ORS_USERS_FOLDER . 'users_popup');
define('ORS_USER_EDIT_PROFILE', ORS_USERS_FOLDER . 'edit_profile_view');

define('FMES_VIEW', FME_FOLDER . 'fmes');
define('FMES_AJAX', FME_FOLDER . 'fmes_ajax');
define('FMES_POPUP', FME_FOLDER . 'fmes_registration');
define('EDIT_FME_PROFILE_VIEW', FME_FOLDER . 'edit_profile_view');
define('FMES_AGENT_TRANSACTION_REPORT_VIEW', FME_FOLDER . 'fme_agent_transaction_report_view');
define('EMPLOYEEWISE_INCENTIVE_REPORT_VIEW', FME_FOLDER . 'employeewise_incentive_earned_report');
define('FMES_AGENT_LIST_VIEW', FME_FOLDER . 'fme_agent_list_view');


define('ORS_SUB_AGENT', ADMIN_VIEW_FOLDER . 'ors_sub_agent/');
define('OFFICE_MASTER_VIEW', ORS_SUB_AGENT . 'ors_office_master_view/');
define('OFFICE_MASTER_HOME', OFFICE_MASTER_VIEW . 'office_master_home');
define('ADD_OFFICE', OFFICE_MASTER_VIEW . 'add_office');
define('EDIT_OFFICE', OFFICE_MASTER_VIEW . 'edit_office');
define('NOT_AUTHORIZED', OFFICE_MASTER_VIEW . 'office_not_authorized_view');

define('MAIL_TEMPLATE_FOLDER', ADMIN_VIEW_FOLDER . 'mail_template/');

define('MAIL_TEMPLATE_VIEW', MAIL_TEMPLATE_FOLDER . 'mail_template');

/* * *****refund module --> ********** */
define('REFUND_MODULE_FOLDER', ADMIN_VIEW_FOLDER . 'refund_module/');
define('REF_USERS_VIEW', REFUND_MODULE_FOLDER . 'refund');
define('REFUND_USERS_AJAX', REFUND_MODULE_FOLDER . 'refund_ajax');
define('REFUND_USERS_POPUP', REFUND_MODULE_FOLDER . 'refund_popup');
define('REFUND_PROFILE', REFUND_MODULE_FOLDER . 'profile');
define('REFUND_EDIT_PROFILE_VIEW', REFUND_MODULE_FOLDER . 'edit_profile_view');
/* * *****  refund module********** */
define('MAIL_TEMPLATE_AJAX', MAIL_TEMPLATE_FOLDER . 'mail_template_ajax');
define('MAIL_TEMPLATE_POPUP', MAIL_TEMPLATE_FOLDER . 'mail_template_popup');

define('AVG_FARE_FOLDER', ADMIN_VIEW_FOLDER . 'avg_fare/');
define('FARE_VIEW', AVG_FARE_FOLDER . 'fare');

define('WALLET_FOLDER', ADMIN_VIEW_FOLDER . 'wallet/');

define('WALLET_VIEW', WALLET_FOLDER . 'wallet');
define('WALLET_AJAX', WALLET_FOLDER . 'wallet_ajax');
define('WALLET_POPUP', WALLET_FOLDER . 'wallet_popup');
define('WALLET_ADD_AMOUNT', WALLET_FOLDER . 'add_amount_wallet');
define('OPERATOR_NOTIFICATION_FOLDER', ADMIN_VIEW_FOLDER . 'Operator_notification/');
define('OPERATOR_NOTIFICATION_VIEW', OPERATOR_NOTIFICATION_FOLDER . 'operator_notification');

define('ROLE_MENU_REL_FOLDER', ADMIN_VIEW_FOLDER . 'role_menu_rel/');
define('ROLE_MENU_REL_VIEW', ROLE_MENU_REL_FOLDER . 'role_menu_rel');

define('MENU_SEQUENCE', ADMIN_VIEW_FOLDER . 'menu_sequence/');
define('MENU_SEQUENCE_VIEW', MENU_SEQUENCE . 'menu_sequence');
//
define('USER_WALLET_FOLDER', ADMIN_VIEW_FOLDER . 'user_wallet/');
define('USER_WALLET', USER_WALLET_FOLDER . 'user_wallet');
define('USER_WALLET_VIEW', USER_WALLET_FOLDER . 'user_wallet_view');
define('USER_TOPUP', USER_WALLET_FOLDER . 'user_topup');
define('USER_WALLET_TRANS_VIEW', USER_WALLET_FOLDER . 'wallet_transactions');
define('USER_TOPUP_HISTORY', USER_WALLET_FOLDER . 'user_topup_history');
define('USER_TOPUP_TRANS_VIEW', USER_WALLET_FOLDER . 'user_topup_trans_view');
define('TOPUP_REVERSAL_VIEW', USER_WALLET_FOLDER . 'topup_reversal_trans_view');
define('ALL_USER_WALLET_TRANS_VIEW', USER_WALLET_FOLDER . 'users_wallet_transactions');

define('USER_COMMISSION_FOLDER', ADMIN_VIEW_FOLDER . 'commission/');
define('USER_COMMISSION', USER_COMMISSION_FOLDER . 'commission');
define('AGENT_COMMISSION_TEMPLATE', USER_COMMISSION_FOLDER . 'agent_commission_template');
define('COMMISSION_ASSIGN_TEMPLATE', USER_COMMISSION_FOLDER . 'commission_assign_template');
define('MY_COMMISSION', USER_COMMISSION_FOLDER . 'my_commission');

//added by harshada kulkarni on 24-07-2018
define('UTILITIES_FOLDER', ADMIN_VIEW_FOLDER . 'utilities/');
define('PREPAID_RECHARGE_VIEW', UTILITIES_FOLDER . 'prepaid_recharge_view');
define('TSO_PREPAID_RECHARGE_VIEW', UTILITIES_FOLDER . 'tso_prepaid_recharge_view');
define('DATACARD_RECHARGE_VIEW', UTILITIES_FOLDER . 'datacard_recharge_view');
//added by sachin chavan on 22-08-2018
define('LANDLINE_VIEW', UTILITIES_FOLDER . 'landline_view');
define('ELECTRICITY_VIEW', UTILITIES_FOLDER . 'electricity_view');
define('GAS_VIEW', UTILITIES_FOLDER . 'gas_view');
define('WATER_VIEW', UTILITIES_FOLDER . 'water_view');
define('BROADBAND_VIEW', UTILITIES_FOLDER . 'broadband_view');
define('GIFTCARD_VIEW', UTILITIES_FOLDER . 'giftcard_view');

//added by pooja kambali on 29 sep 2018
define('DMT_FOLDER', ADMIN_VIEW_FOLDER . 'dmt/');
define('DMT_VIEW', DMT_FOLDER . 'dmt_view_new');
define('TRANSACTION_LIST', DMT_FOLDER . 'transaction_list_view');
define('TRANSACTION_STATUS', DMT_FOLDER . 'transaction_status_view');
define('DMT_URL','http://10.20.107.140/rokad/rest_server/Dmt/');
define('Dmt','4');
//define('fINO_URL','http://10.15.20.131:8001/Uiservice.svc/FinoMoneyTransactionApi/');


//added by harshada kulkarni on 25-07-2018
define('DTH_VIEW', UTILITIES_FOLDER . 'dth_view');
define('TSO_DTH_VIEW', UTILITIES_FOLDER . 'tso_dth_view');
//added by pooja kambali on 1st Aug 2018
define('CERT', '8CEB189D0C44964AE0C8C55D54D9BD04610D88FD');  //our testing key cyberplat
define('CP_PASSWORD', 'Cyberplat@111'); //new pwd given for staging cyberplat
//added by harshada kulkarni on 30-07-2018
define('CP_SD', 347669);
define('CP_AP', 347670);
define('CP_OP', 347671);

//Wallet Balance added by Vijay
define('WALLET_BALANCE_FOLDER', ADMIN_VIEW_FOLDER.'wallet_balance/');
define('WALLET_BALANCE_VIEW', WALLET_BALANCE_FOLDER.'wallet_balance');
define('JRI_WALLET_BALANCE_VIEW', WALLET_BALANCE_FOLDER.'jri_balance');

//added by harshada kulkarni on 23-08-2018
define('X_API_KEY', 'abcdefghijklmn');
define('SERVICE_COMMISSION', '0');
define('CYBERPLAT_TRIMAX_COMMISSION', '20');
//added by harshada kulkarni on 23-08-2018
define('ADD_COMMISSION_OPTIONS', USER_COMMISSION_FOLDER . 'add_commission_options');
//added by harshada kulkarni on 23-08-2018
define('COMMISSION_OPTIONS', USER_COMMISSION_FOLDER . 'commission_options');
//added by harshada kulkarni on 04-09-2018
define('EDIT_COMMISSION_OPTIONS', USER_COMMISSION_FOLDER . 'edit_commission_options');
//added by harshada kulkarni on 06-09-2018
define('SERVICES_COMMISSIONS', USER_COMMISSION_FOLDER . 'services_commissions');
//added by harshada kulkarni on 06-09-2018
define('LOAD_SERVICES_COMMISSIONS', USER_COMMISSION_FOLDER . 'load_services_commissions');
//added by harshada kulkarni on 07-09-2018
define('ADD_SERVICES_COMMISSIONS', USER_COMMISSION_FOLDER . 'add_services_commissions');
//added by harshada kulkarni on 14-09-2018
define('EDIT_SERVICES_COMMISSIONS', USER_COMMISSION_FOLDER . 'edit_services_commissions');
//added by harshada kulkarni on 17-09-2018
define('BOS_SERVICE_ID', '1');
define('UTILITIES_SERVICE_ID', '3');

//added by sachin on 22-08-2018
define('AGENTID', 'BD01BD02MBB000000001');
define('CHANNEL', 'INT');
define('IP_ADDR', '180.92.175.107');
define('MAC_ADDR', 'B8:AE:ED:B3:70:66');
define('TERMINAL_ID', '123456');
define('RECEIVER_MOBILE_NO', '9000000000'); // for giftcard
define('RECEIVER_FIRST_NAME', 'ROKAD'); // for giftcard
define('RECEIVER_LAST_NAME', 'ROKAD'); // for giftcard
define('RECEIVER_MAIL_NAME', 'santoshg.bos@gmail.com'); // for giftcard
define('PRODUCT_TYPE', 'Digital'); // for giftcard

//===============================  MASTER MODULE  ================================
define('MASTER_VIEW_FOLDER', BACK_VIEW . 'master/');

define('BUS_SERVICE_FOLDER', BACK_VIEW . 'bus_service/');

define('SEAT_LAYOUT_FOLDER', MASTER_VIEW_FOLDER . 'seat_layout/');

define('SEAT_VIEW', SEAT_LAYOUT_FOLDER . 'seat');
define('ADD_SEAT_LAYOUT', SEAT_LAYOUT_FOLDER . 'add_seat_layout');
define('EDIT_SEAT_LAYOUT', SEAT_LAYOUT_FOLDER . 'edit_seat_layout');
define('SEAT_AJAX', SEAT_LAYOUT_FOLDER . 'seat_ajax');


define('FARE_CHART_FOLDER', MASTER_VIEW_FOLDER . 'create_fare/');
define('FARE_CHART_VIEW', FARE_CHART_FOLDER . 'create_fare_chart');

define('ASSIGN_FARE_CHART_VIEW', FARE_CHART_FOLDER . 'assign_fare_chart');

define('BUS_SERVICE_VIEW', BUS_SERVICE_FOLDER . 'bus_service');
define('NEW_BUS_SERVICE_VIEW', BUS_SERVICE_FOLDER . 'create_new_bus_service');
define('BUS_SERVICE_STOPS_VIEW', BUS_SERVICE_FOLDER . 'bus_service_stops');
define('UPDATE_BUS_SERVICE_DETAILS_VIEW', BUS_SERVICE_FOLDER . 'update_bus_service_details');
define('BUS_SERVICE_DETAILS_VIEW', BUS_SERVICE_FOLDER . 'bus_service_details');
define('COPY_BUS_SERVICE_VIEW', BUS_SERVICE_FOLDER . 'copy_new_bus_service');


define('DASHBOARD_FOLDER', ADMIN_VIEW_FOLDER . 'dashboard/');
define('ASSIGN_PROVIDERS_TO_SERVICES', DASHBOARD_FOLDER . 'assign_providers_to_services');
define('DASHBOARD_VIEW', DASHBOARD_FOLDER . 'dashboard');
define('AGENT_USER_DASHBOARD', DASHBOARD_FOLDER . 'dashboard_agent_users');
define('DISTRIBUTOR_USER_DASHBOARD', DASHBOARD_FOLDER . 'dashboard_distributer_users');
define('DISTRIBUTOR_SUBAGENT_DASHBOARD', DASHBOARD_FOLDER . 'dashboard_distributor_subagents');

/* ROUTE_FORMATION */
define('ADMIN_ROUTE_FORMATION_FOLDER', ADMIN_VIEW_FOLDER . 'route_formation/');

define('ADMIN_ROUTE_FORMATION_VIEW', ADMIN_ROUTE_FORMATION_FOLDER . 'route_formation');
define('ADMIN_ROUTE_FORMATION_AJAX', ADMIN_ROUTE_FORMATION_FOLDER . 'route_formation_ajax');
define('ADMIN_ROUTE_FORMATION_POPUP', ADMIN_ROUTE_FORMATION_FOLDER . 'route_formation_popup');
define('ADMIN_ROUTE_FORMATION_POPUP_EDIT', ADMIN_ROUTE_FORMATION_FOLDER . 'route_formation_popup_edit');

define('ADMIN_APPROVED_ROUTE_FORMATION_FOLDER', ADMIN_VIEW_FOLDER . 'approved_route_formation/');

define('ADMIN_APPROVED_ROUTE_FORMATION_VIEW', ADMIN_APPROVED_ROUTE_FORMATION_FOLDER . 'approved_route_formation');
define('ADMIN_APPROVED_ROUTE_FORMATION_AJAX', ADMIN_APPROVED_ROUTE_FORMATION_FOLDER . 'approved_route_formation_ajax');
define('ADMIN_APPROVED_ROUTE_FORMATION_POPUP', ADMIN_APPROVED_ROUTE_FORMATION_FOLDER . 'approved_route_formation_popup');
define('ADMIN_APPROVED_ROUTE_FORMATION_POPUP_EDIT', ADMIN_APPROVED_ROUTE_FORMATION_FOLDER . 'approved_route_formation_popup_edit');
/* ROUTE_FORMATION */


/* OPERATOR_CONFIG */
define('ADMIN_OPERATOR_CONFIG_FOLDER', ADMIN_VIEW_FOLDER . 'op_config/');


define('ADMIN_OPERATOR_CONFIG_VIEW', ADMIN_OPERATOR_CONFIG_FOLDER . 'op_config');
define('ADMIN_OPERATOR_CONFIG_AJAX', ADMIN_OPERATOR_CONFIG_FOLDER . 'op_config_ajax');
define('ADMIN_OPERATOR_CONFIG_POPUP', ADMIN_OPERATOR_CONFIG_FOLDER . 'op_config_popup');

/* OPRATORE_CONFIG */
/* SMS_TEMPLATE */
define('ADMIN_SMS_TEMPLATE_FOLDER', ADMIN_VIEW_FOLDER . 'sms_template/');

define('ADMIN_SMS_TEMPLATE_VIEW', ADMIN_SMS_TEMPLATE_FOLDER . 'sms_template');
define('ADMIN_SMS_TEMPLATE_AJAX', ADMIN_SMS_TEMPLATE_FOLDER . 'sms_template_ajax');
define('ADMIN_SMS_TEMPLATE_POPUP', ADMIN_SMS_TEMPLATE_FOLDER . 'sms_template_popup');

/* SMS_TEMPLATE */

/* OP_COMMISSION_MASTER */
define('ADMIN_OP_COMMISSION_MASTER_FOLDER', ADMIN_VIEW_FOLDER . 'op_commission_master/');


define('ADMIN_OP_COMMISSION_MASTER_VIEW', ADMIN_OP_COMMISSION_MASTER_FOLDER . 'op_commission_master');
define('ADMIN_OP_COMMISSION_MASTER_AJAX', ADMIN_OP_COMMISSION_MASTER_FOLDER . 'op_commission_master_ajax');
define('ADMIN_OP_COMMISSION_MASTER_POPUP', ADMIN_OP_COMMISSION_MASTER_FOLDER . 'op_commission_master_popup');

/* OP_COMMISSION_MASTER */

define('MSRTC_CHILD_CONCESSION_AGE', 11);

//===============================  DUMP MODULE  ================================
define('DUMP_VIEW_FOLDER', BACK_VIEW . 'dump/');
define('BUS_LAYOUT_FOLDER', DUMP_VIEW_FOLDER . 'bus/');

define('DUMP_BUS_VIEW', BUS_LAYOUT_FOLDER . 'bus');


define('OPERATOR_FOLDER', ADMIN_VIEW_FOLDER . 'op_master_details/');
define('OPERATOR_VIEW', OPERATOR_FOLDER . 'bus_operator_view');
define('OPERATOR_AJAX', OPERATOR_FOLDER . 'bus_operator_ajax');
define('OPERATOR_POPUP', OPERATOR_FOLDER . 'bus_operator_pop');

//Operator Wallet View

define('OPERATOR_WALLET_FOLDER', ADMIN_VIEW_FOLDER . 'operator/');
define('OPERATOR_WALLET_VIEW', OPERATOR_WALLET_FOLDER . 'wallet');
define('OPERATOR_WALLET_TRANS_VIEW', OPERATOR_WALLET_FOLDER . 'wallet_trans');
define('WAYBILL_TRANS_VIEW', OPERATOR_WALLET_FOLDER . 'waybill_trans');
define('CHECK_WAYBILL_VIEW', OPERATOR_WALLET_FOLDER . 'check_waybill');
define('OPERATOR_TRANS_REPORT_VIEW', OPERATOR_WALLET_FOLDER . 'wallet_trans_report');
//BUS STOP MASTER
define('BUSSTOP_FOLDER', ADMIN_VIEW_FOLDER . 'bus_stops/');
define('BUS_STOPS_VIEW', BUSSTOP_FOLDER . 'bus_stop_view');
define('BUS_STOPS_AJAX', BUSSTOP_FOLDER . 'bus_stop_ajax');
define('BUS_STOPS_POPUP', BUSSTOP_FOLDER . 'bus_stop_pop');



//commision_master
define('COMMISION_MASTER', ADMIN_VIEW_FOLDER . 'commision_master/');
define('COMMISON_VIEW', COMMISION_MASTER . 'commision_view');
define('COMMISON_AJAX', COMMISION_MASTER . 'commision_ajax');
define('COMMISON_POPUP', COMMISION_MASTER . 'commision_pop');


//agent master
define('AGENT_MASTER', ADMIN_VIEW_FOLDER . 'agent_master/');
define('AGENT_VIEW', AGENT_MASTER . 'agent_view');
define('AGENT_LIST_VIEW', AGENT_MASTER . 'agent_list_view');




//boading_ali
define('BOARD_MASTER', ADMIN_VIEW_FOLDER . 'Board_detail/');
define('BOARD_MASTER_VIEW', BOARD_MASTER . 'bording_stop');
define('BOARD_MASTER_AJAX', BOARD_MASTER . 'boardali_ajax');
define('BOARD_MASTER_POPUP', BOARD_MASTER . 'boarding_pop');

define('USER_PROFILE_IMAGE', 'default_user.png');
define('BOOKING_AGENT_FOLDER', ADMIN_VIEW_FOLDER . 'booking_agent/');


define('BACK_AGENT_FOLDER', ADMIN_VIEW_FOLDER . 'agent/');

define('BACK_AGENT_VIEW', BACK_AGENT_FOLDER . 'agent_view');
define('BACK_AGENT_EDIT', BACK_AGENT_FOLDER . 'agent_edit');
define('CREATE_NEW_AGENT', BACK_AGENT_FOLDER . 'create_agent');
define('AGENTWISE_MACHINE_UNREGISTER', BACK_AGENT_FOLDER . 'machine_unregister');
define('CONFIG_AGENT_VIEW', BACK_AGENT_FOLDER . 'config');
define('BACK_AGENT_DOCUEMNT', BACK_AGENT_FOLDER . 'agent_document');
define('BACK_AGENT_IMAGES_SHOW', BACK_AGENT_FOLDER . 'agent_show_images');

//agent docs master
define('BACK_AGENT_DOCS_ADD', BACK_AGENT_FOLDER . 'new_agent_docs_add');
define('BACK_AGENT_NAME_VIEW', BACK_AGENT_FOLDER . 'new_agent_name_view');
define('BACK_AGENT_DOC_VIEW', BACK_AGENT_FOLDER . 'new_agent_docs_view');

define('BACK_AGENT_COMMISSION', BACK_AGENT_FOLDER . 'commission');
define('BACK_PATTERN_COMMISION_LIST', BACK_AGENT_FOLDER . 'patter_commission_list');
define('BACK_AGENT_COMMISSION_ADD', BACK_AGENT_FOLDER . 'commission_add');
define('BACK_AGENT_COMMISSION_EDIT', BACK_AGENT_FOLDER . 'commission_edit');

define('BACK_AGENT_COMMISSION_PATTER_VIEW', BACK_AGENT_FOLDER . 'pattern');
define('BACK_AGENT_COMMISSION_PATTER_ADD', BACK_AGENT_FOLDER . 'pattern_add');
define('BACK_AGENT_COMMISSION_PATTER_EDIT', BACK_AGENT_FOLDER . 'pattern_edit');

define('AGENT_CONFIG_VIEW', BOOKING_AGENT_FOLDER . 'agent_config_view');
define('AGENT_CONFIG_AJAX', BOOKING_AGENT_FOLDER . 'agent_config_ajax');
define('AGENT_CONFIG_ADD', BOOKING_AGENT_FOLDER . 'agent_config_add');
define('AGENT_CONFIG_EDIT', BOOKING_AGENT_FOLDER . 'agent_config_edit');



//agent docs master
define('AGENT_DOCS_ADD', BOOKING_AGENT_FOLDER . 'agent_docs_add');
define('AGENT_NAME_VIEW', BOOKING_AGENT_FOLDER . 'agent_name_view');
define('AGENT_DOC_VIEW', BOOKING_AGENT_FOLDER . 'agent_docs_view');

define('AGENT_DOC_IMAGE_HEIGHT', '90');
define('AGENT_DOC_IMAGE_WIDTH', '130');
define('AGENT_DOC_IMAGE_UPLOAD_LIMIT', '10');
define('AGENT_LEVEL_LIMIT', '5');


define('OPERATOR_BANKDETAIL_VIEW', OPERATOR_FOLDER . 'op_bank_detail_view');
define('OPERATOR_BANKDETAIL_AJAX', OPERATOR_FOLDER . 'op_bank_detail_ajax');
define('OPERATOR_BANKDETAIL_ADD', OPERATOR_FOLDER . 'op_bank_add');
define('OPERATOR_BANKDETAIL_EDIT', OPERATOR_FOLDER . 'op_bank_detail_edit');
define('OP_BANKDETAIL_SUCCESS', OPERATOR_FOLDER . 'op_bank_detail_success');

define('OPERATOR_APIDETAIL_VIEW', OPERATOR_FOLDER . 'op_api_detail_view');
define('OPERATOR_APIDETAIL_AJAX', OPERATOR_FOLDER . 'op_api_detail_ajax');
define('OPERATOR_APIDETAIL_ADD', OPERATOR_FOLDER . 'op_api_detail_add');
define('OPERATOR_APIDETAIL_EDIT', OPERATOR_FOLDER . 'op_api_detail_edit');
define('OP_API_SUCCESS', OPERATOR_FOLDER . 'op_api_detail_success');

define('OPERATOR_FARECOLL_VIEW', OPERATOR_FOLDER . 'op_fare_coll_view');
define('OPERATOR_FARECOLL_AJAX', OPERATOR_FOLDER . 'op_fare_coll_ajax');
define('OPERATOR_FARECOLL_ADD', OPERATOR_FOLDER . 'op_fare_coll_add');
define('OPERATOR_FARECOLL_EDIT', OPERATOR_FOLDER . 'op_fare_coll_edit');
define('OP_FARECOLL_SUCCESS', OPERATOR_FOLDER . 'op_fare_coll_success');


//cancel ticket folder
define('CANCEL_TICKET_FOLDER', ADMIN_VIEW_FOLDER . 'cancel_ticket/');
define('CANCEL_TICKET_VIEW', CANCEL_TICKET_FOLDER . 'bos_cancel_ticket');
define('REFUND_REQUEST_VIEW', CANCEL_TICKET_FOLDER . 'refund_request_view');

//*** Unsuccessfull booked ticket against successful payment ***//
define('UNSUCCESSFUL_BOOKED_TKT_FOLDER', ADMIN_VIEW_FOLDER . 'unsuccessful_booked_tkt/');
define('UNSUCCESSFUL_BOOKED_TKT_VIEW', UNSUCCESSFUL_BOOKED_TKT_FOLDER . 'unsuccessful_booked_tkt_view');
/* favourite_list */
define('ADMIN_FAVOURITE_LIST_FOLDER', ADMIN_VIEW_FOLDER . 'favourite_list/');
define('CSC_CANCEL_TICKET_VIEW', CANCEL_TICKET_FOLDER . 'csc_bos_cancel_ticket');


define('REPORT_FOLDER', BACK_VIEW . 'report/');
define('RESERVATION_REPORT_VIEW', REPORT_FOLDER . 'reservation_report');

define('RESERVATION_REFUND_VIEW', REPORT_FOLDER . 'reservation_refund_report');
define('TICKET_COUNT_REPORT_VIEW', REPORT_FOLDER . 'ticket_count_report');
define('TOP_DEFAULTER_REPORT_VIEW', REPORT_FOLDER . 'top_defaulter_report');
define('BUS_SERVICE_REPORT_VIEW', REPORT_FOLDER . 'bus_service_report');

define('OPERATOR_REPORT_FOLDER', BACK_VIEW . 'operator_report/');
define('OPERATOR_REPORT_VIEW', REPORT_FOLDER . 'operator_report');
define('BOOKED_TICKETS_REPORT_VIEW', REPORT_FOLDER . 'booked_tickets_report');
define('STAR_SELLER_REPORT_VIEW', REPORT_FOLDER . 'star_seller_waybill_sign_on_off_report');
define('HHM_ON_REPORT_VIEW', REPORT_FOLDER . 'hhm_on_report');
define('HHM_OFF_REPORT_VIEW', REPORT_FOLDER . 'hhm_off_report');
define('WAYBILL_AMOUNT_DATA', REPORT_FOLDER . 'waybill_amount_data');
define('DAILY_TKT_SALES', REPORT_FOLDER . 'daily_tkt_sales');
define('WAYBILL_TICKET_SALES', REPORT_FOLDER . 'waybill_wise_ticket_report');
define('STARSELLERPOINT_TICKET_SALES', REPORT_FOLDER . 'starsellerpoint_wise_ticket_report');
define('DAILY_ADVANCE_TICKET_SALES', REPORT_FOLDER . 'daily_advance_tkt_report');
define('USER_FEEDBACK_VIEW', REPORT_FOLDER . 'user_feedback');
define('DAILY_ADVANCE_API_OP_WISE', REPORT_FOLDER . 'daily_tkt_op_api_wise');
/* user report list */
define('USER_REPORT_VIEW', REPORT_FOLDER . 'user_report');
define('PENDING_USER_REPORT_VIEW', REPORT_FOLDER . 'pending_user');
define('PENDINGKYC_REPORT_VIEW', REPORT_FOLDER . 'pendingkyc_report');
define('MSRTC_REPORT_VIEW', REPORT_FOLDER . 'msrtc_report');
define('TICKET_REPORT_VIEW', REPORT_FOLDER . 'ticket_report');
/* user report list */
define('FUND_TRANSFER_REPORT', REPORT_FOLDER . 'fund_transfer_report');
define('BALANCE_REQUEST_REPORT', REPORT_FOLDER . 'balance_request_report');
define('DETAIL_COMMISSION_REPORT', REPORT_FOLDER . 'detail_commission_report');
define('MONTHLY_COMMISSION_REPORT', REPORT_FOLDER . 'monthly_commission_report');
define('SMS_LOG_REPORT', REPORT_FOLDER . 'sms_log_report');

// Added by santosh 29/07/19
define('AGENT_STATUS_REPORT', REPORT_FOLDER.'agent_status_report');
define('AGENT_PAYMENT_REPORT', REPORT_FOLDER.'agent_payment_report');

define('AGENT_REPORT_VIEW', REPORT_FOLDER . 'agent_report');
define('RECONSILLATION_REPORT_VIEW', REPORT_FOLDER . 'reconsillation_report');
define('RECONSILLATION_CANCEL_REPORT_VIEW', REPORT_FOLDER . 'reconsillation_cancel_report');
define('TICKET_TRANSACTION_REPORT_VIEW', REPORT_FOLDER . 'ticket_transaction_report');
define('BOS_TICKET_DETAILS_VIEW', REPORT_FOLDER . 'bos_ticket_details_report');
define('BOS_TICKET_DETAILS_COUNT_VIEW', REPORT_FOLDER . 'bos_ticket_details_count_report');
define('REVENUE_REPORT', REPORT_FOLDER . 'revenue_report');
define('BOS_TICKET_DETAILS_COUNT_VIEW_SUMMARY', REPORT_FOLDER . 'ticket_details_count_report_summary');
define('BOS_COMMISSION_OFFER_VIEW', REPORT_FOLDER . 'bos_commission_offer_report');
define('BOS_TICKET_DATA_VIEW', REPORT_FOLDER . 'bos_ticket_details');
define('BOS_AGENT_USERS_TICKET_DATA_VIEW', REPORT_FOLDER . 'bos_agent_users_ticket_details');
define('BOS_ADMIN_TICKET_DATA_VIEW', REPORT_FOLDER . 'bos_admin_ticket_details');
define('BOS_INCOMPLETE_TRANSACTION_REPORT', REPORT_FOLDER . 'incomplete_transactions_report');
define('SUPPORT_SUMMARY_REPORT_VIEW', REPORT_FOLDER . 'support_summary_report');
define('USER_TYPE_WISE_REPORT_VIEW', REPORT_FOLDER . 'user_type_wise_report_view');
define('RSRTC_RECONSILIATION_REPORT', REPORT_FOLDER . 'rsrtc_reconciliation_statement_report');
define('DISTRIBUTOR_TICKET_DETAILS', REPORT_FOLDER . 'distributor_ticket_details');

define('AUTHORIZED_AGENT_REPORT_VIEW', REPORT_FOLDER . 'authorized_booking_agent_report');
define('AUTHORIZED_AGENT_PRINT_VIEW', REPORT_FOLDER . 'authorized_booking_agent_print_report');
define('DATEWISE_ORS_TICKET_BOOKING_REPORT_VIEW', REPORT_FOLDER . 'datewise_ors_ticket_booking_report');
define('DATEWISE_CURRENT_BOOKING_TICKET_REPORT_VIEW', REPORT_FOLDER . 'datewise_current_booking_ticket_report');
define('WALLET_TRANSACTION_REPORT', REPORT_FOLDER . 'wallet_transaction_report');
define('WALLET_TRANSACTION_RETAILER_REPORT', REPORT_FOLDER . 'wallet_transaction_retailer_report');
define('STOPWISE_TICKET_COUNT_REPORT_VIEW', REPORT_FOLDER . 'stop_wise_ticket_count');
define('REFERRER_WISE_SUMMARY_REPORT_VIEW', REPORT_FOLDER . 'referrer_wise_summary_report_view');
define('CROSS_SALE_REPORT_VIEW', REPORT_FOLDER . 'cross_sale_report_view');
define('AGENTWISE_REPORT_VIEW', REPORT_FOLDER . 'agentwise_report');

//ors agent reports
define('AGENT_REPORT_FOLDER', BACK_VIEW . 'ors_agent_report/');
define('ORS_AGENT_TICKET_SUMMARY_REPORT', AGENT_REPORT_FOLDER . 'ors_agent_ticket_summary_report');
define('ORS_AGENT_TICKET_SUMMARY_REPORT_VIEW', AGENT_REPORT_FOLDER . 'ors_agent_ticket_summary_report_view');
define('ORS_AGENT_USERS_TICKET_SUMMARY_REPORT_VIEW', AGENT_REPORT_FOLDER . 'ors_agent_users_ticket_summary_report_view');
define('ORS_AGENT_ADMIN_TICKET_SUMMARY_REPORT_VIEW', AGENT_REPORT_FOLDER . 'ors_agent_admin_ticket_summary_report_view');
define('ORS_AGENT_WALLET_TRANSACTION_REPORT_VIEW', AGENT_REPORT_FOLDER . 'ors_agent_wallet_transaction_report_view');
define('ORS_AGENT_WALLET_TRANSACTION_REPORT_ADMIN', AGENT_REPORT_FOLDER . 'ors_agent_wallet_transaction_report_admin');

define('RSRTC_REPORT_FOLDER', BACK_VIEW . 'rsrtc_report/');
define('AGENTS_DETAILS_RSRTC', RSRTC_REPORT_FOLDER . 'agents_details_rsrtc');

define('REMITTANCE', RSRTC_REPORT_FOLDER . 'remittance');

define('TICKET_SUMMARY_REPORT_RSRTC', RSRTC_REPORT_FOLDER . 'ticket_summary_report_rsrtc');
define('AGENT_WISE_WAY_BILL_REPORT', RSRTC_REPORT_FOLDER . 'agent_wise_way_bill_report');
define('AGENT_WISE_DSA_REPORT', RSRTC_REPORT_FOLDER . 'agent_wise_dsa_report');
define('CONCESSIONAL_REPORT', RSRTC_REPORT_FOLDER . 'concessional_report');
define('OP_TOPUP_REPORT', RSRTC_REPORT_FOLDER . 'op_topup_report');

define('CONVEY_CHARGE_MASTER_FOLDER', ADMIN_VIEW_FOLDER . 'convey_charge_master/');
define('CONVEY_CHARGE_MASTER', CONVEY_CHARGE_MASTER_FOLDER . 'create_convey_charge_master');
define('CONVEY_CHARGE_LIST', CONVEY_CHARGE_MASTER_FOLDER . 'convey_charge_list');
define('UPDATE_CONVEY_CHARGE_LIST', CONVEY_CHARGE_MASTER_FOLDER . 'update_convey_charge_master');


//Operator Master
define('OPERATOR_DETAILS_FOLDER', ADMIN_VIEW_FOLDER . 'op_master_details/');
define('OPERATOR_MASTER_VIEW', OPERATOR_DETAILS_FOLDER . 'op_master_list');
define('OPERATOR_MASTER_AJAX', OPERATOR_DETAILS_FOLDER . 'op_master_ajax');
define('OPERATOR_MASTER_ADD', OPERATOR_DETAILS_FOLDER . 'op_master_add');
define('OPERATOR_MASTER_EDIT', OPERATOR_DETAILS_FOLDER . 'op_master_edit');
define('OPERATOR_MASTER_SUCCESS', OPERATOR_DETAILS_FOLDER . 'op_add_success');
define('OPERATOR_MASTER_REPORT', OPERATOR_REPORT_VIEW . 'op_report_view');

//Operator Topup 
define('OPERATOR_TOPUP_FOLDER', ADMIN_VIEW_FOLDER . 'op_topup/');
define('OPERATOR_TOPUP_VIEW', OPERATOR_TOPUP_FOLDER . 'op_topup_list');
define('OPERATOR_TOPUP_AJAX', OPERATOR_TOPUP_FOLDER . 'op_topup_ajax');
define('OPERATOR_TOPUP_ADD', OPERATOR_TOPUP_FOLDER . 'op_topup_add');
define('OPERATOR_TOPUP_EDIT', OPERATOR_TOPUP_FOLDER . 'op_topup_edit');
define('OP_PGPROCESSING_VIEW', OPERATOR_TOPUP_FOLDER . 'op_pgprocessing');

/* * ***************  Api and cron Constant  **************** */
define('CRON_VIEW_FOLDER', BACK_VIEW . 'cron/');

define('CRON_BUS_FOLDER', CRON_VIEW_FOLDER . 'bus/');
define('CRON_BUS', CRON_BUS_FOLDER . 'bus');
define('RELEASE_BUS_SERVICE_WISE', CRON_BUS_FOLDER . 'release_bus_service_wise');



define('FAILED_TRANSACTION', FRONT_VIEW . 'failed_transaction');

//Operator API PROVIDER
define('OPERATOR_API_PROVIDER_FOLDER', ADMIN_VIEW_FOLDER . 'op_api_provider/');
define('OPERATOR_API_PROVIDER_VIEW', OPERATOR_API_PROVIDER_FOLDER . 'op_provider_list');
define('OPERATOR_API_PROVIDER_AJAX', OPERATOR_API_PROVIDER_FOLDER . 'op_provider_ajax');
define('OPERATOR_API_PROVIDER_ADD', OPERATOR_API_PROVIDER_FOLDER . 'op_provider_add');
define('OPERATOR_API_PROVIDER_EDIT', OPERATOR_API_PROVIDER_FOLDER . 'op_provider_edit');

//Operator API API_CONFIG
define('OPERATOR_API_CONFIG_FOLDER', ADMIN_VIEW_FOLDER . 'op_api_config/');
define('OPERATOR_API_CONFIG_VIEW', OPERATOR_API_CONFIG_FOLDER . 'op_api_config_list');
define('OPERATOR_API_CONFIG_AJAX', OPERATOR_API_CONFIG_FOLDER . 'op_api_config_ajax');
define('OPERATOR_API_CONFIG_ADD', OPERATOR_API_CONFIG_FOLDER . 'op_api_config_add');
define('OPERATOR_API_CONFIG_EDIT', OPERATOR_API_CONFIG_FOLDER . 'op_api_config_edit');


//Operator API API_CONFIG
define('OPERATOR_API_DETAIL_FOLDER', ADMIN_VIEW_FOLDER . 'op_api_detail/');
define('OPERATOR_API_DETAIL_VIEW', OPERATOR_API_DETAIL_FOLDER . 'op_api_detail_list');
define('OPERATOR_API_DETAIL_AJAX', OPERATOR_API_DETAIL_FOLDER . 'op_api_detail_ajax');
define('OPERATOR_API_DETAIL_ADD', OPERATOR_API_DETAIL_FOLDER . 'op_api_detail_add');
define('OPERATOR_API_DETAIL_EDIT', OPERATOR_API_DETAIL_FOLDER . 'op_api_detail_edit');


//API Pagination constant
define('API_PAGINATION_LIMIT', '1000');


//PER TICKET CONVEY CHARGE LIMIT
define('PER_TICKET_CONVEY_CHARGE', 40);
define('PERCENT_TICKET_CONVEY_CHARGE', 3);
//===============================  FRONT THEME COLOR  ================================
define('THEME_CALENDER', 'orange');
define('THEME_BTN', 'btnorange');
define('RSRTC_PER_TICKET_CONVEY_CHARGE_FOR_AC', 10);
define('RSRTC_PER_TICKET_CONVEY_CHARGE_FOR_NON_AC', 5);

//GEO LOCATION
define('GEO_LOCATION', 'http://ip-api.com/php/');



define('AGENT_WALLET_FOLDER', ADMIN_VIEW_FOLDER . 'agent/');
define('AGENT_WALLET_VIEW', AGENT_WALLET_FOLDER . 'agent_wallet');
define('AGENT_WALLET_TRANS_VIEW', AGENT_WALLET_FOLDER . 'agent_wallet_trans');


define('IS_MARKUP', true);
define('GUEST_USER', '0');
define('BOS_MOBILE_API_USER_ID', '22');
define('BOS_MOBILE_API_USER_ROLE', 'bos_mobile_api_user');

/*
  |--------------------------------------------------------------------------
  | Global Auth Constants
  |--------------------------------------------------------------------------
  |
 */

define('PG_LOG', FCPATH . 'log/pg_log/');
define('TMP_PATH', FCPATH . 'tmp/');
define('MANUAL_HIT', 'GIfYR1EhoDMznqbm8Ygd');
/* define('PAYMENT_TRANSACTION_FAILED',    FRONT_VIEW.'payment_transaction_failed'); */

define('CCAVENUE_CACERT_FILE_PATH', APPPATH . 'libraries/payment_gateway/cacert.pem');

//define('BOS_TICKET_COUNT_EMAILS',    'rajendra.trimax@gmail.com,jaymalak@trimax.in');//correct one

define('BOS_TICKET_COUNT_EMAILS', 'jay.bookonspot@gmail.com');
define('WALLET_CALCULATION_EMAILS', '$wallet_calculation_emails = array(
                                                            "santoshg.bos@gmail.com",
                                                            "santoshg.bos@gmail.com"
                                                          );');
define('WALLET_FAILURE_TRANS_EMAILS', '$wallet_failure_trans_emails = array(
                                                            "santoshg.bos@gmail.com",
                                                            "santoshg.bos@gmail.com",
                                                            "santoshg.bos@gmail.com"
                                                          );');

/* * ******COMMISSION PERCENTAGE START**** */
define('MSRTC_COMMISSION_PERCENTAGE', '4');
define('HRTC_COMMISSION_PERCENTAGE', '4');
/* * ******COMMISSION PERCENTAGE END**** */

require_once ('ticket_template_constants.php');
require_once ('email_template_constants.php');
require_once ('sms_template_constants.php');
define('AGENT_ID_ANGULAR', '$agent_id_angular = array("7039","3339");');

define('AUTHENTICATION_VIEW', REPORT_FOLDER . 'authentication_view');

define('AUTHENTICATION_VIEW', REPORT_FOLDER . 'authentication_view');

define('AUTHENTICATION_VIEW', REPORT_FOLDER . 'authentication_view');
define('AUTH_POPUP', REPORT_FOLDER . 'auth_popup');
define('DEPOT_USER', 37);
define('HO_AUTHENTICATION_VIEW', REPORT_FOLDER . 'ho_auth_view');


define('WAYBILL_CLOSE_REPORT_VIEW', OPERATOR_WALLET_FOLDER . 'waybill_close_view');
define('WAYBILL_CLOSE_REPORT_VIEW_QUERY', OPERATOR_WALLET_FOLDER . 'waybill_close_view_query');

define('CANCELLED_WAYBILLS_VIEW', OPERATOR_WALLET_FOLDER . 'cancelled_waybill_list');


define('MSRTC_DEDUCTED_AMT_DASHBOARD', '50000');
define('UPSRTC_DEDUCTED_AMT_DASHBOARD', '190000');


define('FUND_TRANSFER_FOLDER', ADMIN_VIEW_FOLDER . 'fund_transfer/');
define('PROCEED_VIEW', FUND_TRANSFER_FOLDER . 'proceed');
define('FUND_TRANSFER_VIEW', FUND_TRANSFER_FOLDER . 'fund_transfer_view');
define('TRANSFER_SUCCESS_VIEW', FUND_TRANSFER_FOLDER . 'success_processiong');



/* * ********payonspot constants  role id********* */
define('SUPERADMIN_ROLE_ID', "1");
define('TRIMAX_ROLE_ID', "2");
define('COMPANY_ROLE_ID', "3");
define('MASTER_DISTRIBUTOR_ROLE_ID', "4");
define('AREA_DISTRIBUTOR_ROLE_ID', '5');
define('DISTRIBUTOR', "6");
define('RETAILER_ROLE_ID', "7");
define('SUPPORT_ROLE_ID', "8");
define('ADMIN_ROLE_ID', "9");

define("BANK_REQUEST_ROLE_ARRAY",  json_encode(array(TRIMAX_ROLE_ID, COMPANY_ROLE_ID, MASTER_DISTRIBUTOR_ROLE_ID, AREA_DISTRIBUTOR_ROLE_ID, DISTRIBUTOR, RETAILER_ROLE_ID))
      );
/* * ********payonspot constants  level id********* */
define('TRIMAX_LEVEL', "1");
define('COMPANY_LEVEL', "2");
define('MASTER_DISTRIBUTOR_LEVEL', "3");
define('AREA_DISTRIBUTOR_LEVEL', '4');
define('POS_DISTRIBUTOR_LEVEL', "5");
define('RETAILOR_LEVEL', "6");
/* * ********COMPANY_ID CONSTANT FOR LOCAL AND STAGING USED IN REGISTRATION BY RAJ********* */
define('TRIMAX_USER_ID', "1");
define('COUNTRY_ID', "1");
/* * ********Rokad Chain Constants********* */
define("LEVEL_CHAIN_CONSTANTS", '$level_chain_constants = array(1,2,3,4,5,6);');
// Role Menu
define('ROLE_GROUP_FOLDER', ADMIN_VIEW_FOLDER . 'role_group/');

define('ROLE_GROUP_VIEW', ROLE_GROUP_FOLDER . 'role_group_view');
define('CREATE_ROLE__VIEW', ROLE_GROUP_FOLDER . 'create_role');
define('SHOW_ROLE_VIEW', ROLE_GROUP_FOLDER . 'show_role_view');
define('EDIT_ROLE_VIEW', ROLE_GROUP_FOLDER . 'edit_role_view');


// User Role Menu
define('USER_ROLES_FOLDER', ADMIN_VIEW_FOLDER . 'user_roles/');

define('USER_ROLES_VIEW', USER_ROLES_FOLDER . 'role_group_view');
define('CREATE_USER_ROLE__VIEW', USER_ROLES_FOLDER . 'create_role');
define('SHOW_USER_ROLE_VIEW', USER_ROLES_FOLDER . 'show_role_view');
define('EDIT_USER_ROLE_VIEW', USER_ROLES_FOLDER . 'edit_role_view');

define('PERMISSION_MENU_FOLDER', ADMIN_VIEW_FOLDER . 'permission_menu/');

define('INDEX_VIEW', PERMISSION_MENU_FOLDER . 'index');
define('EDIT_MENU_VIEW', PERMISSION_MENU_FOLDER . 'edit');
define('SHOW_ROLE_VIW', ROLE_GROUP_FOLDER . 'show_role_view');


//FUNDS MANAGEMENT
define('BACK_FUND_FOLDER', ADMIN_VIEW_FOLDER . 'fund_request/');
define('BACK_FUND_REQUEST_LIST', BACK_FUND_FOLDER . 'fund_request_list');
define('BACK_FUND_REQUEST_CREATE', BACK_FUND_FOLDER . 'fund_request_create');
define('BACK_SELF_FUND_REQUEST', BACK_FUND_FOLDER . 'self_fund_request');
define('BACK_SELF_REQUEST_LIST', BACK_FUND_FOLDER . 'self_topup_request_approval_list');
define('BACK_WALLET_ADD_MONEY', BACK_FUND_FOLDER . 'add_money_wallet');
define('BACK_FUND_REQUEST_VIEW', BACK_FUND_FOLDER . 'fund_request_view');
define('BACK_FUND_REQUEST_APPROVAL_LIST', BACK_FUND_FOLDER . 'fund_request_approval_list');
define('BACK_FUND_REQUEST_APPROVAL', BACK_FUND_FOLDER . 'fund_request_approval');
define('BACK_FUND_REQUEST_APPROVAL_VIEW', BACK_FUND_FOLDER . 'fund_request_approval_view');
define('BACK_FUND_SUPPORT_REQUEST_LIST', BACK_FUND_FOLDER . 'fund_support_request_list');
define('BACK_FUND_SUPPORT_REQUEST_CREATE', BACK_FUND_FOLDER . 'fund_support_request_create');
define('BACK_FUND_SUPPORT_REQUEST_VIEW', BACK_FUND_FOLDER . 'fund_support_request_view');

//ROKAD TOPUP
define('BACK_ROKAD_TOPUP_FOLDER', ADMIN_VIEW_FOLDER . 'rokad_topup/');
define('BACK_ROKAD_TOPUP_REQUEST_LIST', BACK_ROKAD_TOPUP_FOLDER . 'rokad_topup_request_list');
define('BACK_ROKAD_TOPUP_REQUEST_CREATE', BACK_ROKAD_TOPUP_FOLDER . 'rokad_topup_request_create');
define('BACK_ROKAD_TOPUP_REQUEST_VIEW', BACK_ROKAD_TOPUP_FOLDER . 'rokad_topup_request_view');
define('BACK_ROKAD_TOPUP_REQUEST_APPROVAL_LIST', BACK_ROKAD_TOPUP_FOLDER . 'rokad_topup_request_approval_list');
define('BACK_ROKAD_TOPUP_REQUEST_APPROVAL', BACK_ROKAD_TOPUP_FOLDER . 'rokad_topup_request_approval');
define('BACK_ROKAD_TOPUP_REQUEST_APPROVAL_VIEW', BACK_ROKAD_TOPUP_FOLDER . 'rokad_topup_request_approval_view');


// reset username
define('RESET_USERNAME_FOLDER', ADMIN_VIEW_FOLDER . 'reset_username/');
define('RESET_USERNAME', RESET_USERNAME_FOLDER . 'reset_username');
define('RESET_USERNAME_STEP2', RESET_USERNAME_FOLDER . 'reset_step2');
define('RESET_USERNAME_STEP3', RESET_USERNAME_FOLDER . 'reset_step3');


// merchants
define('MERCHANT_FOLDER', ADMIN_VIEW_FOLDER . 'merchant/');
define('MERCHANT_VIEW', MERCHANT_FOLDER . 'index');
define('MERCHANT_CREATE_VIEW', MERCHANT_FOLDER . 'create_merchant');
define('MERCHANT_EDIT_VIEW', MERCHANT_FOLDER . 'edit_merchant');

//Machne Register
define('CB_MACHINE_ALLOCATION_FOLDER', ADMIN_VIEW_FOLDER . 'current_booking_machine_allocation/');
define('MACHINE_REGISTRATION_VIEW', CB_MACHINE_ALLOCATION_FOLDER . 'machine_registration_view');
define('MACHINE_UNREGISTRATION_VIEW', CB_MACHINE_ALLOCATION_FOLDER . 'machine_unregistration_view');
define('MACHINE_ALLOCATION_VIEW', CB_MACHINE_ALLOCATION_FOLDER . 'machine_allocation_view');
define('MACHINE_COLLECTION_VIEW', CB_MACHINE_ALLOCATION_FOLDER . 'machine_collection_view');
define('MACHINE_REG_ALLOCATION_VIEW', CB_MACHINE_ALLOCATION_FOLDER . 'machine_reg_allocation_view');
define('MACHINE_COLLECTION_ALLOCATION_VIEW', CB_MACHINE_ALLOCATION_FOLDER . 'machine_collection_allocation');
define('MACHINE_ASSIGN_VIEW', CB_MACHINE_ALLOCATION_FOLDER . 'machine_assign_view');
define('MACHINE_LOST_COLLECTION_VIEW', CB_MACHINE_ALLOCATION_FOLDER . 'machine_lost_collection');

//Wallet Limit
define('WALLET_LIMIT', 9999999999); // Message Limit
//define('WALLET_LIMIT', 100000); // Message Limit
define('WALLET_BAL_LIMIT', 100000);
define('MSRTC_WALLET_LIMIT', 9999999999);
//company role name
define('COMPANY_ROLE_NAME', 'company');

//company role name
define('DOMAIN_NAME_VAL', 'http://www.peyna.in');

define('ROKAD_SUPPORT_MAIL', 'support@peyna.in');
//start msrtc commision 
define('OP_COMMISSION_RATE', '0.00');
define('OP_TDS_RATE', '0');
define('OP_TDS_TYPE', 'percentage');
//end msrtc commission
define('MSRTC_USER_ROLE_ID', '9');
define('EncryptKey', 'fdac7124c6a2b3f6');


//service
define('SERVICE_FOLDER', ADMIN_VIEW_FOLDER . 'service/');
define('SERVICE_VIEW', SERVICE_FOLDER . 'select_service_view');
define('CREATE_SERVICE_VIEW', SERVICE_FOLDER . 'create_service');

//Subservice
define('SERVICE_FOLDER', ADMIN_VIEW_FOLDER . 'service/');
define('SUBSERVICE_VIEW', SERVICE_FOLDER . 'sub_service_view');
define('CREATE_SUBSERVICE_VIEW', SERVICE_FOLDER . 'create_subservice');

//commission
define('SELECT_SERVICE_VIEW', USER_COMMISSION_FOLDER . 'select_service_view');

//package
define('COMMISSION_PACKAGE_FOLDER', ADMIN_VIEW_FOLDER . 'package/');
define('USER_PACKAGE', COMMISSION_PACKAGE_FOLDER . 'package');
define('PACKAGE_VIEW', COMMISSION_PACKAGE_FOLDER . 'view');
define('ASSIGN_PACKAGE_VIEW', COMMISSION_PACKAGE_FOLDER . 'assign_package');

//lock
define('LOCK_FOLDER', ADMIN_VIEW_FOLDER . 'lock/');
define('LOCK_VIEW', LOCK_FOLDER . 'lock_view');

//Ticket window topup
define('TICKET_WINDOW_FOLDER', ADMIN_VIEW_FOLDER . 'ticket_window_topup/');
define('TICKET_WINDOW_TOPUP_VIEW', TICKET_WINDOW_FOLDER . 'ticket_window_topup_view');

define('MSRTC_SERVICE_ID', '2');
define('PACKAGE_CREATE', COMMISSION_PACKAGE_FOLDER . 'create');
define('CB_SERVICE_ID', '2');
define('MSRTC_REPORT_URL', 'http://180.92.175.148/msrtc_cb/index.php/admin/cb_report_md');
define('MSRTC_APPLICATION', 'http://10.21.41.103/msrtc_cb/');
define('TDS_APPLICABLE_MONTHLY_COMMISSION', '5');
define('GST_APPLICABLE_MONTHLY_COMMISSION', '18');
define('MSRTC_DB_NAME', 'msrtc_bos_dev_testing');
define('MSRTC_REPLICATION', 'msrtc_replication');

define('ACCOUNT_ROLE_ID', "10");

define('BACK_ACC_FUND_REQUEST_APPROVAL_LIST', BACK_FUND_FOLDER . 'acc_fund_request_approval_list');

define('BACK_ACC_FUND_REQUEST_APPROVAL', BACK_FUND_FOLDER . 'acc_fund_request_approval');

define('BACK_ACC_FUND_REQUEST_APPROVAL_VIEW', BACK_FUND_FOLDER . 'fund_acc_request_approval_view');


define('CB_MACHINE_MASTER_FOLDER', ADMIN_VIEW_FOLDER . 'cb_machine_master/');

define('CB_MASTER_VIEW', CB_MACHINE_MASTER_FOLDER . 'machine_master_view');
define('CREATE_MACHINE_MASTER_VIEW', CB_MACHINE_MASTER_FOLDER . 'create_machine');
define('EDIT_MACHINE_MASTER_VIEW', CB_MACHINE_MASTER_FOLDER . 'edit_machine_master_view');

define('ASSIGN_DEVICE_VIEW', CB_MACHINE_MASTER_FOLDER . 'assign_device');
define('LIST_ASSIGN_DEVICE_VIEW', CB_MACHINE_MASTER_FOLDER . 'assign_machine_master_view');
define('LIST_AGENT_ASSIGN_DEVICE_VIEW', CB_MACHINE_MASTER_FOLDER . 'list_assign_machine_master_view');
define('MY_COMPANY_COMMISSION', USER_COMMISSION_FOLDER . 'my_company_commission');

/* * ****************seller amount************************ */
define('SELLER_AMOUNT_FOLDER', ADMIN_VIEW_FOLDER . 'seller_amount/');
define('SELLER_AMOUNT_VIEW', SELLER_AMOUNT_FOLDER . '/seller_amount_view');
define('SELLER_AMOUNT_ADD', SELLER_AMOUNT_FOLDER . '/add_seller_amount');
define('SELLER_AMOUNT_EDIT', SELLER_AMOUNT_FOLDER . '/edit_seller_amount');

define('TICKET_TOPUP_EMAIL', 'ponnamanitha1994@gmail.com');
define('TICKET_TOPUP_MOBILE_NO', '7738191267');
define("CONTACT_US_NO", '+91 7738343137');

/* * ****************zip route stop*********************** */
define('ZIP_Folder', ADMIN_VIEW_FOLDER . 'zip_folder/');
define('ZIP_ROUTE_STOP', ZIP_Folder . '/zip_route_stop');
define('ZIP_FARE_STOP', ZIP_Folder . '/zip_fare_stop');

/* * ***********************DOWNLOAD******************* */
define('DOWNLOAD_FOLDER', ADMIN_VIEW_FOLDER . 'download/');
define('DOWNLOAD_CSV_VIEW', DOWNLOAD_FOLDER . 'download_csv');

define('ZIP_FARE_STOP', ZIP_Folder . '/zip_fare_stop');

/* * **************Change SA seller point*************** */
define('EDIT_SALE_AGENT_SELLER_POINT', REGISTRATION_FOLDER . '/edit_sale_agent_seller_point');


//Added by Vijay Ghadi     @23/08/2018
define('ALL_FRONTEND_REGISTERED_USERS', REGISTRATION_FOLDER . '/all_frontend_registered_users');
define('EDIT_FRONTEND_USERS', REGISTRATION_FOLDER . '/edit_frontend_users');

//Added by Vijay Ghadi
define('WALLET_BALANCE_FOLDER', ADMIN_VIEW_FOLDER.'wallet_balance/');
define('WALLET_BALANCE_VIEW', WALLET_BALANCE_FOLDER.'wallet_balance');



////added by Sonali Kamble on 20-august-2018
//define('ADVANCED_BOOKING_FOLDER', ADMIN_VIEW_FOLDER . 'advanced_booking/');
//define('SEARCH_BUS_VIEW', ADVANCED_BOOKING_FOLDER . 'search_bus_view');
////define('SERVICES_VIEW',ADVANCED_BOOKING_FOLDER.'services_view');
////define('BUS_SERVICES',ADVANCED_BOOKING_FOLDER.'bus_services_view');
//define('PRINT_TICKET',ADVANCED_BOOKING_FOLDER.'print_ticket_view');
//define('TEMP_BOOKING',ADVANCED_BOOKING_FOLDER.'temp_booking_view');
//
//define('CANCEL_MODULE',ADVANCED_BOOKING_FOLDER.'cancel_booking_view');
//define('BOS_WALLET_BALANCE',WALLET_BALANCE_FOLDER.'bos_wallet_balance');
//define('BOS_WALLET_BALANCE',WALLET_BALANCE_FOLDER.'bos_wallet_balance');



define('SERVICES_FOLDER', ADMIN_VIEW_FOLDER . 'services/');
define('SERVICES_VIEW', SERVICES_FOLDER . 'edit_services_view');





define('development_url','http://180.92.175.107/app/bos_api/api_v1/bus/');
define('api_username','ketang@bookonspot.com');
define('api_pwd','ketan164');
define('X_API_KEY','abcdefghijklmn');
define('token','2132165465');


//Added by Vijay Ghadi
define('MACHINE_SYNC_VIEW', CB_MACHINE_ALLOCATION_FOLDER . 'machine_sync_view');
define('MSRTC_CB','msrtc_bos_dev');
//Added by Vijay Ghadi for Master Sync Data
define('MSRTC_CB','msrtc_bos_dev');

//  Merge Bus Depots -- Gopal
define('MERGE_BD_FOLDER', ADMIN_VIEW_FOLDER . 'merge_bus_depots/');
define('MERGE_BD_VIEW', MERGE_BD_FOLDER . '/index_merge_bus_depots');
define('MERGE_BD_ADD', MERGE_BD_FOLDER . '/add_merge_bus_depots');
define('MERGE_BD_EDIT', MERGE_BD_FOLDER . '/edit_merge_bus_depots');

// For JRI Api

define('MOBILE_PREPAID', 1);
define('MOBILE_POSTPAID', 2);
define('DTH', 3);
define('LANDLINE', 4);
define('ELECTRICITY', 5);
define('GAS', 6);
define('BROADBAND', 7);
define('WATER', 8);
define('DATACARD', 9);
define('RECHARGE', 10);

define('JRI_MOBILE_OPERATORS', json_encode(array(
          'AIRTEL' => 'AIRTEL',
          'BSNL' => 'BSNL',
          'Idea' => 'Idea',
          'Jio' => 'Jio',
          'MTNL' => 'MTNL',
          'Tata Indicom' => 'Tata Indicom',
          'DOCOMO' => 'Tata DOCOMO',
          'T24' => 'T24',
          'Vodafone' => 'Vodafone'
)
)
);

define('JRI_DTH_OPERATORS', json_encode(array(
          'Airtel TV DTH' => 'Airtel TV DTH',
          'DISH TV DTH' => 'DISH TV DTH',
          'RELIANCE BIG TV' => 'RELIANCE BIG TV',
          'SUN DIRECT' => 'SUN DIRECT',
          'TATA SKY' => 'TATA SKY',
          'VIDEOCON D2h' => 'VIDEOCON D2h'
))
);

define('TSO_DTH_OPERATORS', json_encode(array(
    'AD' => 'Airtel DTH',
    'BD' => 'Big TV',
    'DT' => 'Dish TV',
    'TS' => 'Tata Sky',
    'VD' => 'Videocon DTH',
    'ST' => 'Sun TV'
)
)
);

define('JRI_DATACARD_OPERATORS', json_encode(array(
          'Airtel DataCard' => 'Airtel Datacard',
          'BSNL DataCard' => 'BSNL Datacard',
          'Idea DataCard' => 'IDEA Datacard',
          'MTNL DataCard' => 'MTNL DataCard',
          'Vodafone DataCard' => 'Vodafone Datacard'
)
)
);


define('LOCATIONS', json_encode(array(
  'AP' => 'Andhra Pradesh', 
  'ASM' => 'Assam' ,
  'BIH' => 'Bihar & Jharkhand' ,
  'CHE' => 'Chennai',
  'DEL' => 'Delhi',
  'GUJ' => 'Gujrat', 
  'HAR' => 'Haryana',
  'HP' => 'Himachal Pradesh',
  'JK' => 'J&K',
  'KK' => 'Karnataka', 
  'KER' => 'Kerala', 
  'KOL' => 'Kolkata',
  'MP' => 'Madhya Pradesh', 
  'MAH' => 'Maharashtra', 
  'MUM' => 'Mumbai', 
  'NE' => 'NorthEast', 
  'ORI' => 'Orissa', 
  'PUN' => 'Pune',
  'Punjab' => 'Punjab',
  'RAJ' => 'Rajasthan', 
  'TN' => 'Tamilnadu', 
  'UPE' => 'Uttar Pradesh (East)',
  'UPW' => 'Uttar Pradesh (West)', 
  'UPW' => 'Uttaranchal', 
  'WB' => 'WestBengal & AN Island'
  ))
);

define('JRI_CRED', json_encode(array(
    'SecurityKey' => 'S834344',
    'EmailId' => 'santoshg.bos@gmail.com',
    'Password' => '123321',
    'md5key' => 'Tr!m13'
)));

define('JRI_CRED_TEST', json_encode(array(
    'SecurityKey' => 'S911205',
    'EmailId' => 'santoshg.bos@gmail.com',
    'Password' => '123147',
    'md5key' => 'Rok15$'
)));

define('JRI_TRIMAX_COMMISSION', '15');

define('JRI', 5);
define('MIN_WALLET_AMT',10000);
define('JRI_TRANSACTION_REPORT',REPORT_FOLDER.'jri_transaction_report');
define('JRI_STATASTICS_REPORT',REPORT_FOLDER.'jri_statatics');

//Added by Santosh
define('TSO_PRE_MOBILE_OPERATORS', json_encode(array(
          'AR' => 'AIRTEL',
          'BS' => 'BSNL',
          'ID' => 'Idea',
          'VF' => 'Vodafone',
          'RJ' => 'JIO',
          'TI' => 'TataIndicom',
          'TD' => 'Tata DOCOMO',
          'AI' => 'Aircel',
          'TE' => 'Telenor',
          'VR' => 'Vergin GSM',
          'VC' => 'Virgin CDMA',
          'MTS' => 'MTS'
)
)
);


define('TSO_POST_MOBILE_OPERATORS', json_encode(array(
          'AP' => 'AIRTEL BILL',
          'BB' => 'BSNL BILL',
          'IB' => 'Idea BILL',
          'VP' => 'Vodafone BILL',
          'RB' => 'Reliance BILL',
          'TB' => 'Tata BILL'
)
)
);

// define('TSO_FOLDER', ADMIN_VIEW_FOLDER . 'tso/');
// define('TSO_PREPAID_RECHARGE_VIEW', TSO_FOLDER . 'prepaid_recharge_view');

define('TSO_CREDS', json_encode(array(
    'soapUrl' => "https://www.remitindia.in/NTDAPI/RechargeService.asmx",
    'soapUser' => '9679544706', //API User ID
    'soapPassword' => '9679544706', //API Password
    'mobileno' => 9679544706, //Registered Mobile No
    'soapAPIKey' => 'rZc89GNqvGUMx08TqhOk8Yc5bDYn74P582y',
    'smspin' => 4532, //SMS PIN
    'stv' => 0
)));

/** added by shrasti bansal**/
define('SMART_CARD_CREDS', json_encode(array(
    'url' => "https://porstesting.msrtcors.com/MSRTC_RFIDPASS/api",
    'username' => 'poweruser', //API User ID
    'password' => 'trimax#123', //API Password
    'X_API_KEY' => 'abcd', //SMS PIN
)));
/** added by prabhat **/
define('CARD_ISSUANCE', 'ps_card_issuance_details');
define('CARD_TRANS', 'ps_card_transaction');
define('CARD_ISSUANCE_S2S', 'ps_s2s_card_issuance_details');
define('SW_WALLET_TOPUP', 'ps_card_topup_request');
define('CUSTOMER_CARD_MASTER', 'ps_customer_card_master');
define('MSRTC_DB_RE_NAME', 'msrtc_replication');
define('CARD_TRANS_DETAILS', 'ps_card_transaction_details');
define('AMOUNT_TRANS', 'ps_amount_transaction');
define('AMOUNT_TRANS_DETAILS', 'ps_amount_transaction_details');
// For command master
define('REG_COMMAND', 'REGISTRATION');
define('RENEW_COMMAND', 'RENEW');
//Card Types
define('CARDTYPE_U18_CC', 'U18');
define('CARDTYPE_OTC_CC', 'OTC');
define('CARDTYPE_AOP_CC', 'AOP');
define('CARDTYPE_PPC_CC', 'PPC');

define('TOPUP_SPAN', '6');
// wallet status
define('W_STATUS_NA', 'NA');
define('W_STATUS_CREATED', 'C');
define('W_STATUS_NOT_CREATED', 'NC');
define('W_STATUS_NOT_INPROGRESS', 'P');

//Message ID for OTP
define('OTP_MSG_ID_REG', '4');
define('OTP_MSG_ID_REISSUANCE', '5');
define('OTP_MSG_ID_BLOCK', '7');
define('OTP_MSG_ID_LOAD', '8');
define('OTP_MSG_ID_SURRENDER', '10');
define('BASEFARE_KEY', 'TopupAmount');
define('BONUS_KEY', 'BonusAmt');

define('TOTALCHARGES_KEY', 'TotalCharge');
define('AGE_LIMIT', 12);
define('RESPONSE_CODE_SUCCESS_CC', 200);
define('RESPONSE_CODE_FAIL_CC', 400);
define('AGE_LIMIT_CARDTYPE_CC', 18);
//CCAS parameter set
define('INITIATOR_TYPE', 'DPR');


//card status
define('DISPATCH_STATUS', 'D');
define('BLOCK_STATUS', 'B');
define('SURRENDER_STATUS', 'S');


//Dates
define('TODAY_DATE', date('Y-m-d'));
//Pass Category Id
define('REGULAR_CARD', '1');
define('STUDENT_CARD', '2');
define('CONC_CARD', '3');
define('TRAVEL_AS_U_LIKE_CARD', '4');
define('OTC_CARD', '5');
define('REG_OTC_TRANS_TYPE', 'RGOT');
define('REGISTRATION_TRANS_TYPE', 'RG');

//Table name for registration of user
//define('CUSTOMER_MASTER', 'ps_customer_master');
///define('CUSTOMER_STUDENT_MASTER', 'ps_customer_student_master');

///define('IMAGE_DETAILS', 'ps_image_details');
//define('CARD_TRANS', 'ps_card_transaction');

//define('MASTER_CARD', 'ps_master_card');
//define('INVENTORY', 'inventory');
define('CCAS_URL','http://52.25.13.115:8069/citycash/');


/** added by sonali**/
define('CUSTOMER_MASTER', 'ps_customer_master');
define('CUSTOMER_STUDENT_MASTER', 'ps_customer_student_master');
define('IMAGE_DETAILS', 'ps_image_details');
//define('CARD_TRANS_DETAILS', 'ps_card_transaction_details');
//define('AMOUNT_TRANS', 'ps_amount_transaction');
//define('AMOUNT_TRANS_DETAILS', 'ps_amount_transaction_details');
define('MASTER_CARD', 'ps_master_card');
define('INVENTORY', 'inventory');

//Pass Category Id
//define('STUDENT_CARD', '2');

//Image Folder Details
define('IMAGE_FOLDER_NAME', 'uploads');

//Dates
//define('TODAY_DATE', date('Y-m-d'));

// transaction type code
//define('REGISTRATION_TRANS_TYPE', 'RG');

define('NUMBER_OF_TRIPS', 2); 

//m round factor
define('FIVE_ROUND_FACTOR', 5);
define('ONE_ROUND_FACTOR', 1);

//Parameters for t_id generation
define('T_ID_ISSUE_NM', 'TRIMAX_ID');
//depot code for otc card whitlisting
define('OTC_DEPOT_CD', '999');
define('itms_user_id', '33');

define('ITMSUSER', 'rokad1234');
define('ITMSPASSWORD', 'trimax123');

define('REG_PPC_TRANS_TYPE_CODE', 'RG');
define('REG_OTC_TRANS_TYPE_CODE', 'RGOT');

define('REG_PPC_TRANS_TYPE_ID', '1');
define('SW_LOAD_TRANS_TYPE_ID', '4');
define('TW_LOAD_CASH_TRANS_TYPE_ID', '5');
define('TW_LOAD_SW_TRANS_TYPE_ID', '6');
define('REG_OTC_TRANS_TYPE_ID', '11');
define('REISSUE_OTC_TRANS_ID', '12');
define('REISSUE_PPC_TRANS_ID', '2');


define('SURRENDER_PPC_TRANS_TYPE_ID', '9');
define('SURRENDER_OTC_TRANS_TYPE_ID', '14');
define('PASS_RENEW_SW_TRANS_TYPE_ID', '16');
define('PASS_RENEW_CASH_TRANS_TYPE_ID', '3');
define('BLOCK_TRANS_TYPE_ID', '8');
define('REACTIVATION_TRANS_TYPE_ID', '10');




//For Proof
define('APPLICATION_TYPE_FINO', 'FINO');
define('APPLICATION_TYPE_MSRTC', 'MSRTC');

define('WRITING_STATUS', 'A');
define('INITIATED_STATUS', 'I');


define('TAL_STUDENT_STATE', '40');
define('TAL_STUDENT_INTER_STATE', '48');
define('REGULAR_STUDENT', '39');
define('REISSUE_OTC_TRANS_TYPE', 'RIOT');
define('REISSUE_PPC_TRANS_TYPE', 'RI');


define('IMAGE_VIEW',ADVANCED_BOOKING_FOLDER.'image_view');
define('STUDENT_OCCUPATION', '3');
define('SMART_CARD_FEE_CD','SCF');

//define('CRON_URL','https://10.20.52.156/MSRTC_RFIDPASS/api/');

define('CRON_URL','https://uatcardapi.msrtcors.com/api/'); //added by sonali

define('MIN_DMT_AMT','100');

// added by sonali on 24-01-2019 @Microatm service
define('MATM_FOLDER', ADMIN_VIEW_FOLDER . 'matm/');
define('CASH_WITHDRAW_VIEW', MATM_FOLDER . 'cash_withdraw_view');
define('BALANCE_ENQUIRY_VIEW', MATM_FOLDER . 'balance_enquiry_view');
define('MATM_TRANSACTION_STATUS_VIEW', MATM_FOLDER . 'matm_transaction_status_view');
define('MATM_TRANSACTION_VIEW', MATM_FOLDER . 'transaction_details_view');
define('MATM_TRANSACTION_LIST_REPORT', REPORT_FOLDER . 'matm_transaction_list_view');

define('MATM_CASH_WITHDRAW','156');
define('MATM_BALANCE_ENQUIRY','157');
define('MATM_TRANSACTION_STATUS','158');

define('MATM_HEADER_KEY','982b0d01-b262-4ece-a2a2-45be82212ba1');
define('MATM_BODY_KEY','8329fffc-b192-4c9b-80cf-ccbbdf48bd67');

define('MATM_MERCHANT_ID','8446808997');
define('MATM_VERSION_NO','1000');
define('MATM_CLIENT_ID','40');
define('MATM_AUTH_KEY','bff9a593-8aae-4d82-9f07-c6513fd8cff7');

define('MICRO_ATM_SERVICE_ID','17');

define('MICRO_ATM_COMMISSION_NAME','MicroatmMicro_atm');
define('MATM_CREDIT_AMOUNT', MATM_FOLDER . 'amount_transfer_view');
define('MATM_AGENT_SETTLEMENT_REPORT', MATM_FOLDER . 'agent_settlement_report_view');
define('MATM_CHECK_AMOUNT_SETTLEMENT', MATM_FOLDER . 'account_check_amount_settlement_view');


//added by Sonali Kamble on 31-may-19
define('AIR_AND_HOTEL_BOOKING_FOLDER', ADMIN_VIEW_FOLDER . 'hotel_booking/');
define('FLIGHT_BOOKING_VIEW', AIR_AND_HOTEL_BOOKING_FOLDER . 'flight_booking_view');

define('FLIGHT_SEARCH_VIEW', AIR_AND_HOTEL_BOOKING_FOLDER . 'flight_search_view');
define('FLIGHT_SERVICES_VIEW', AIR_AND_HOTEL_BOOKING_FOLDER . 'flight_services_view');
define('FLIGHT_BOOKING_FORM_VIEW', AIR_AND_HOTEL_BOOKING_FOLDER . 'flight_booking_form_view');
define('FLIGHT_REVIEW_VIEW', AIR_AND_HOTEL_BOOKING_FOLDER . 'flight_review_view');

//added by Sonali Kamble on 31-may-19
define('HOTEL_BOOKING_FOLDER', ADMIN_VIEW_FOLDER . 'hotel_booking/');
define('SEARCH_HOTEL_VIEW', HOTEL_BOOKING_FOLDER . 'search_hotel_view');
define('HOTEL_SERVICES_VIEW', HOTEL_BOOKING_FOLDER . 'hotel_services_view');
define('ADVANCE_FLIGHT_SEARCH_VIEW', HOTEL_BOOKING_FOLDER . 'advance_flight_search_view');
define('SEARCH_HOTEL_VIEW1', HOTEL_BOOKING_FOLDER . 'search_hotel_view_majhar');
define('SEARCH_HOTEL_VIEW2', HOTEL_BOOKING_FOLDER . 'search_hotel_view_rupali');
define('PRINT_TICKET',HOTEL_BOOKING_FOLDER.'print_hotel_ticket_view');

define('ADVANCE_SEARCH_HOTEL_VIEW', HOTEL_BOOKING_FOLDER . 'advance_search_hotel_view');
define('HOTEL_BOOKING_VIEW', HOTEL_BOOKING_FOLDER . 'hotel_booking_view');











//added by shrasti bansal on 31-dec-2018
/*define('BOS_HOTEL_CREDS', json_encode(array(
    'url' => "http://180.92.175.107/app/bos_api/api_v1",
    'username' => 'trimax@via.com', //API User ID
    'password' => 'trimax123', //API Password
    'viaAccessToken' => '424ee901-9977-6648-8e90-38ee8a80a5d5', //SMS PIN
    'viaUserToken' => 'b19f06ba3a9c4dc61cd94272fa84c8ef8710c3c6c37a517f5c600f2e4f0fec3fe1138bef47c9f8e295d3b6352e7d6de13bbab95d47d80dba0d1fed6d686d6896b94a8e7f6f42b585527c75cf207c5b140057767e3cb6185ee0cc7939d02b879b062ccbb532cc324ed12b29579ca96ddd',
    'deviceId' => '28d92fdb-0069-4f0e-bd38-71bfaeb4dbd5',
)));*/

//added by shrasti bansal on 31-dec-2018
// BOS flight credentials
define('BOS_HOTEL_CREDS', json_encode(array(
    'url' => "http://180.92.175.107/app/bos_api/api_v1/",
    'username' => 'tanvi.bookonspot@gmail.com', //API User ID
    'password' => 'tanvi', //API Password
    'viaAccessToken' => '424ee901-9977-6648-8e90-38ee8a80a5d5', //SMS PIN
    'viaUserToken' => 'b19f06ba3a9c4dc61cd94272fa84c8ef8710c3c6c37a517f5c600f2e4f0fec3fe1138bef47c9f8e295d3b6352e7d6de13bbab95d47d80dba0d1fed6d686d6896b94a8e7f6f42b585527c75cf207c5b140057767e3cb6185ee0cc7939d02b879b062ccbb532cc324ed12b29579ca96ddd',
    'deviceId' => '28d92fdb-0069-4f0e-bd38-71bfaeb4dbd5',
)));

//added by shrasti bansal on 25-jan-2019

define('ROKAD_HOTEL_CREDS', json_encode(array(
    'username' => "9000000000", //API User ID
    'password' => 'BoS@S123', //API Password
    'xApiKey' => 'abcdefghijklmn',
)));

define('CITYCASH_PENDING_STATUS', 'X');
define('REISSUE_STATUS',"2");

// added by sonali on 20-feb-2019
define('SERVICE_PASS_REGISTRATION_ISSUANCE_VIEW', REPORT_FOLDER . 'service_pass_registration_issuance_view');
define('CONCESSION_PASS_REPORT', REPORT_FOLDER . 'concession_pass_report');
define('SW_TOPUP_REPORT', REPORT_FOLDER. 'sw_wallet_topup_report_view');
define('TW_TOPUP_REPORT', REPORT_FOLDER. 'tw_wallet_topup_report_view');






//Function name for enquiry api
define('WALLET_CREATION_FUNC',"CCAS_api/wallet_creation/");
define('TW_TOPUP_FUNC',"CCAS_api/tw_wallet_topup/");
define('SW_TOPUP_FUNC',"CCAS_api/sw_wallet_topup/");
define('REQUEST_DONE_STATUS',"Y");
define('REQUEST_PENDING_STATUS',"P");
define('REQUEST_NOT_DONE_STATUS',"N"); 
// added by sonali
//Fino Amount code
define('BANK_FEE', 'LoadingFee');
define('APPLICATION_FEE', 'AF');
define('GST_FEE', 'GST');

// added by sonali on 27-01-2019 @AEPS service
define('AEPS_CASH_WITHDRAW','151');
define('AEPS_BALANCE_ENQUIRY','152');
define('AEPS_TRANSACTION_STATUS','154');


define('AEPS_FOLDER', ADMIN_VIEW_FOLDER . 'aeps/');
define('AEPS_CASH_WITHDRAW_VIEW', AEPS_FOLDER . 'aeps_cash_withdraw_view');
define('AEPS_BALANCE_ENQUIRY_VIEW', AEPS_FOLDER . 'aeps_balance_enquiry_view');
define('AEPS_TRANSACTION_STATUS_VIEW', AEPS_FOLDER . 'aeps_transaction_status_view');
define('AEPS_TRANSACTION_VIEW', AEPS_FOLDER . 'aeps_transaction_details_view');
define('AEPS_TRANSACTION_REPORT', REPORT_FOLDER . 'aeps_transaction_report');

define('AEPS_SERVICE_ID','20');
define('AEPS_COMMISSION_NAME','aepsAEPS');

define('AEPS_HEADER_KEY','982b0d01-b262-4ece-a2a2-45be82212ba1');
define('AEPS_BODY_KEY','8329fffc-b192-4c9b-80cf-ccbbdf48bd67');

define('AEPS_MERCHANT_ID','8446808997');
define('AEPS_VERSION_NO','1000');
define('AEPS_CLIENT_ID','40');
define('AEPS_AUTH_KEY','bff9a593-8aae-4d82-9f07-c6513fd8cff7');

define('PASS_RENEW_SW_TRANS_TYPE', 'BPSW');
define('PASS_RENEW_CASH_TRANS_TYPE', 'TPBP');
define('BLOCK_TRANS_TYPE', 'RL');
define('REACTIVATION_TRANS_TYPE', 'RA');
define('REQUEST_FROM_WEB',"web");
define('REQUEST_FROM_DESKTOP',"desktop");
define('RENEWAL_STATUS',"3");
define('REGISTRATION_STATUS',"4");

define('LOGIN_EXPIRE_TIME','+5 minute');
define('CITYCASH_EMAILS',json_encode(array('santoshg.bos@gmail.com')));

define('SMART_CARD_SERVICE','11');


/* otc cart inventory 
 * added by sonali
 */
define('PER_CARD_AMOUNT','2');
define('SMART_CARD_FOLDER', ADMIN_VIEW_FOLDER . 'smart_card/');
define('OTC_CARD_INVENTORY_VIEW', SMART_CARD_FOLDER . 'otc_card_inventory_view');
define('CARD_ASSIGNDATA_VIEW', SMART_CARD_FOLDER . 'card_assigndata_view');

define('BC_POOL_DEBIT_VIA_CASH_FUNC',"CCAS_api/bc_pool_debit_via_cash/");
define('BC_POOL_DEBIT_VIA_SW_FUNC',"CCAS_api/bc_pool_debit_via_sw/");
define('VIA_CASH', 'CASH');
define('VIA_SW', 'SW');

define('BCCHARGES', 'bccharges');
define('REPOSTING_ERROR_CODE', '4241');

///Constant for reports start here /
define('TW_LOAD_CASH_TRANS_TYPE_VALUE', 'TPTC');
define('TW_LOAD_SW_TRANS_TYPE_VALUE', 'TPTS');
define('REG_PPC_TRANS_TYPE_VALUE', 'RG');

///Constant for reports end here /
define('DCC_REPORT_CRON',json_encode(array('santoshg.bos@gmail.com')));


//added by prabhat
define('ADVANCED_BOOKING_FOLDER', ADMIN_VIEW_FOLDER . 'advanced_booking/');
define('SEARCH_BUS_VIEW', ADVANCED_BOOKING_FOLDER . 'search_bus_view');
define('PRINT_TICKET',ADVANCED_BOOKING_FOLDER.'print_ticket_view');
define('PRINT_TICKET2',ADVANCED_BOOKING_FOLDER.'print_ticket_view2');
define('TEMP_BOOKING',ADVANCED_BOOKING_FOLDER.'temp_booking_view');
define('CANCEL_MODULE',ADVANCED_BOOKING_FOLDER.'cancel_ticket_view');
define('HOME_SEARCH', ADVANCED_BOOKING_FOLDER . 'home_search_view');
define('ADVANCE_SEARCH', ADVANCED_BOOKING_FOLDER . 'advance_search_view');
define('PASSENGER_DETAILS', ADVANCED_BOOKING_FOLDER . 'passenger_details');
define('BOOK_PAYMENT', ADVANCED_BOOKING_FOLDER . 'book_payment_view');
define('CANCEL_MODULE_LIST',ADVANCED_BOOKING_FOLDER.'cancel_ticket_list');
define('FLIGHT_BOOKING_FOLDER', ADMIN_VIEW_FOLDER . 'flight_booking/');
define('FLIGHT_SEARCH', FLIGHT_BOOKING_FOLDER . 'home_search_view');
define('FLIGHT_LIST_SEARCH', FLIGHT_BOOKING_FOLDER . 'advance_search_view');


define('BUS_PASS_RENEWAL_CONFIGURE_TIME', "+3 hour");

define('HIGH_LEVEL_COMMISSION_REPORT', REPORT_FOLDER . 'high_level_commission_report');

define('CONSOLIDATE_REPORT_VIEW', REPORT_FOLDER . 'consolidate_report_view');

define('TRANSACTION_LIST1', REPORT_FOLDER . 'transaction_list_view');

define('PREPAID_SERVICE_ID', '5');
define('POSTPAID_SERVICE_ID', '6');
define('DATACARD_SERVICE_ID', '12');
define('EPAY_SERVICE_ID', '15');

//added by vaidehi on 18-12-18
define('BBPS_UTILITIES_FOLDER', ADMIN_VIEW_FOLDER . 'utilities_bbps/');

define('BBPS_GAS_VIEW', BBPS_UTILITIES_FOLDER . 'bbps_gas_view');
define('BBPS_ELECTRICITY_VIEW', BBPS_UTILITIES_FOLDER . 'bbps_electricity_view');
define('BBPS_WATER_VIEW', BBPS_UTILITIES_FOLDER . 'bbps_water_view');
define('BBPS_DTH_VIEW', BBPS_UTILITIES_FOLDER . 'bbps_dth_view');
define('BBPS_BROADBAND_POSTPAID_VIEW', BBPS_UTILITIES_FOLDER . 'bbps_broadbandpostpaid_view');
define('BBPS_MOBILE_POSTPAID_VIEW', BBPS_UTILITIES_FOLDER . 'bbps_mobilepostpaid_view');
define('BBPS_LANDLINE_POSTPAID_VIEW', BBPS_UTILITIES_FOLDER . 'bbps_landlinepostpaid_view');
define('ELECTRICITY_RECEIPT_PAGE', BBPS_UTILITIES_FOLDER . 'receipt_page_view');
define('ELECTRICITY_TRANSACTION_REPORT',REPORT_FOLDER . 'electricity_report_view');

define('BILLERLIST','https://myaccount.itzcash.com/easy/servlet/BbpouApi?requestType=BILLERLISTBYCAT&category=');
define('BASEURL','https://myaccount.itzcash.com/easy/servlet/BbpouApi?requestType=FETCHBILL&FETCHREQUEST=');
define('ITZCASHURL','https://myaccount.itzcash.com/easy/servlet/BbpouApi');

define('SERVICE_COMMISSION', '0');
define('BBPS_TRIMAX_COMMISSION', '20');
define('BBPS_UTILITIES_SERVICE_ID', '12');
define('SOURCEID','BOS');
define('BBPS_EMAIL_ID', 'santoshg.bos@gmail.com');
define('BBPS_MOBILE_NO', '9892650971');
define('BBPS_DEALER_CODE','D000096821');
define('BBPS_USERNAME','ITZDS100258');
define('BBPS_AGENT_ID','IT01IT02AGT000024599');
define('BBPS_PASSWORD','Totaram@1976');





/*new constant added 22/04/2019 start here */
define('LOSS_STATUS', 'L');
define('FAULTY_STATUS', 'F');
/*new constant added 22/04/2019 end here */

/*new constant added 23/04/2019 start here */

define('BASE_FARE_NAME', 'Pass Amount');
define('TOPUP_FARE_NAME', 'Topup Amount');
define('BASE_FARE_CODE', 'BF');
define('RESPONSE_CODE_TIMEOUT_CC', 4999);

/*new constant added 22/04/2019 end here */
define('SURRENDER_PPC_TRANS_TYPE_VALUE', 'SD');
define('SURRENDER_OTC_TRANS_TYPE_VALUE', 'SDOT');



define('SET_SERVICE_COMMISSION',USER_COMMISSION_FOLDER.'set_service_commission');



//smartcard commission constant - sonali

define('SMARTCARD_COMMISSION_NAME','SmartcardSmartcard');
define('FINO_COMMISSION','20');
define('REMAINING_COMMISSION', '80');
define('LESS_MARKETING_COST', '10.06');
define('CITY_CASH', '0.94');
define('EFFECTIVE_COMMISSION_FOR_BOS', '7.69');

define('TICKET_DETAILS_LIST', ADVANCED_BOOKING_FOLDER . 'ticket_deatils_list');

define('SMARTCARD_TOTAL_COMMISSION','12.00');

//added by Lilawati Karale on 07-02-2019
define('PREPAID_FOLDER', ADMIN_VIEW_FOLDER . 'prepaid/');
define('JRI_PREPAID_RECHARGE_VIEW', PREPAID_FOLDER . 'jri_prepaid_recharge_view');
define('TSO_PREPAID_RECHARGE_NEW_VIEW', PREPAID_FOLDER . 'tso_prepaid_recharge_new_view');
define('PREPAID_RECEIPT_PAGE', PREPAID_FOLDER . 'receipt_page_view');

define('POSTPAID_FOLDER', ADMIN_VIEW_FOLDER . 'postpaid/');
define('JRI_POSTPAID_RECHARGE_VIEW', POSTPAID_FOLDER . 'jri_postpaid_recharge_view');
define('TSO_POSTPAID_RECHARGE_VIEW', POSTPAID_FOLDER . 'tso_postpaid_recharge_view');

define('POSTPAID_RECEIPT_PAGE', POSTPAID_FOLDER . 'receipt_page_view');

define('CONSOLIDATE_REPORT_EMAILS',json_encode(array('santoshg.bos@gmail.com')));

define('MIN_CARD_AMOUNT', '20');

define('OTC_CARD_REPORT',REPORT_FOLDER.'otc_card_report_view');
define('OTP_MASTER', 'rokad_otp');
define('OTP_TIME_OUT', "+15 minutes");

define('GST_FEE', 'GST');
define('SMART_CARD_FEE', 'SCF');
define('BASE_FARE', 'BF');
define('APPLICATION_FEE', 'AF');

define('ROLE_ID_ARRAY', json_encode(array('3','4','5','6','7','11','12','13','14','15','16')));

define('START_DATE','2019-03-01');
define('END_DATE','2019-07-12');