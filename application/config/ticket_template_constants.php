<?php

define('BOS_TICKET_TEMPLATE', '<table style="font-size:13px;font-family: \'Arial\',sans-serif; 	table-layout: fixed; line-height: 20px;color:#000;border-collapse: collapse;" align="center" width="800px" bordercolor="#cccccc" border="1px" cellpadding="2" cellspacing="2">
									<tr valign="top">
										<td valign="top" colspan="4">
											<table class="respond" style="font-size:14px;font-family: \'Arial\',sans-serif; color:#000;" align="center" width="100%" cellpadding="0" cellspacing="0">
												<tr>
													<td colspan="2" rowspan="2" align="left" valign="top"><a href="https://www.rokad.in" target="_blank"><img src="https://www.rokad.in/assets/travelo/front/images/logo_bos.png" width="165" height="90" /></a></td>
													<td colspan="2" align="right" valign="top" style="color:#000;font-weight:bold; font-size:13px; text-align:centre;">Booking</br>Date: {{booked_date}}</td>
												</tr>
												{{agent_details}}
											</table>
										</td>
									</tr>
									<tr valign="middle" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 20px;">
										<td valign="middle"><span style="color:#ff940a;font-weight:bold; font-size:13px;">Journey:</span> </td>
										<td valign="middle">{{from_to_journey}}</td>
										<td valign="middle"><span style="color:#ff940a;font-weight:bold; font-size:13px;">Date of Journey: </span> </td>
										<td> {{journey_date}} </td>
									</tr>
									<tr valign="top">
										<td  style="padding:7px 0;" colspan="4" bgcolor="#efefef"><strong>Trip Information:</strong> </td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Travels:</span> </td>
										<td>{{op_name}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Bus Type:</span> </td>
										<td>{{bus_types}}</td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height:25px;">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">PNR No.:</span> </td>
										<td>{{pnr_number}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Total Seat(s):</span> </td>
										<td>{{seat_no}}</td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Rokad Ref No.:</span> </td>
										<td>{{bos_ref_no}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Total Fare:</span> </td>
										<td>{{total_fare}}</td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										<td style="padding:7px 0;" colspan="4" bgcolor="#efefef"><strong>Boarding Information:</strong> </td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Boarding Point:</span> </td>
										<td>{{boarding_point}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Dropping Point:</span> </td>
										<td>{{dropping_point}}</td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Departure Time:</span> </td>
										<td>{{departure_time}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Alighting Time:</span> </td>
										<td>{{alighting_time}}</td>
									</tr>
									<tr valign="top">
										<td style="padding:7px 0;" colspan="4" bgcolor="#efefef"><strong>Passenger Information:</strong> </td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 40px">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Email Id:</span> </td>
										<td>{{email}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Contact No.:</span> </td>
										<td>{{mobile_no}}</td>
									</tr>
									<tr valign="top"></tr>
									<tr valign="top">
										<td colspan="4"><table class="respond" style="font-size:14px;font-family: \'Arial\',sans-serif; color:#000;" align="center" width="100%" bordercolor="#cccccc" border="1px" cellpadding="0" cellspacing="0">
												<tr >
													<th style="padding:7px;text-align:center;" > <span style="color:#000;font-weight:bold; font-size:13px;">Passenger Name</span> </th>
													<th style="padding:7px 0;text-align:center;"> <span style="color:#000;font-weight:bold; font-size:13px;">Age</span> </th>
													<th style="padding:7px 0;text-align:center;"> <span style="color:#000;font-weight:bold; font-size:13px;">Gender</span> </th>
													<th style="padding:7px 0;text-align:center;"> <span style="color:#000;font-weight:bold; font-size:13px;">Seat No.</span> </th>
												</tr>
												{{passanger_details}}
											</table>
											</td>
									</tr>
								</table>
								<table class="respond" style="font-size:13px;font-family: \'Arial\',sans-serif; text-align:justify; line-height: 20px;color:#000;border-collapse: collapse;" align="center" width="800px" bordercolor="#cccccc" border="0px" cellpadding="0" cellspacing="0">
									<tr>
										<td><strong></br>
											Terms & Conditions: </strong> </td>
									</tr>
									<tr>
										<td> Rokad acts as an “Intermediary” solely to assist customers in gathering travel information, determining the availability of 
											travel-related products and services, making legitimate reservations or otherwise transacting business with travel operators, and 
											for facilitating travel requirements. You acknowledge that Rokad merely provides intermediary services in order to facilitate these services. </td>
									</tr>
									<tr>
										<td><strong>Communication Policy :</strong> </td>
									</tr>
									<tr>
										<td> Upon transacting on the Rokad website, you will receive an e-mail from Rokad informing the status of your transaction on your registered email ID 
											or the ID provided at the time of booking. You will also receive a SMS on your registered phone number or the mobile number provided at the time of 
											booking. The responsibility of entering your accurate contact details like your name, email ID and mobile number is solely yours. </br>
											Rokad is not responsible to provide information on any change in bus schedules, cancellation, status of bus operator, etc. </td>
									</tr>
									<tr>
										<td><strong>Refunds:</strong> </td>
									</tr>
									<tr>
										<td> In case of cancellation, refunds will be affected according to the bus operator\'s cancellation policy charges along with our service charge 
											if any as mentioned above . After bus departure or in case of No Show, no Cancellation / Refund requests will be accepted. </br>
											Refund process may take duration of 2-3 working days due to banking procedures. In case of Net Banking, it may take 7-8 working days. 
											Processing times for cancellation and refund requests may vary from operator to operator </br>
											In case of unavailability of services, you will receive full refund except the transaction charges . </td>
									</tr>
									<tr>
										<td><strong>Grievance Policy:</strong> </td>
									</tr>
									<tr>
										<td> You can file a grievance / share feedback if you are disappointed with the services rendered by Rokad or any other issues. You can give your grievance /
											feedback through email by emailing us at <a href="mailto:support@rokad.in">support@rokad.in</a> or by calling us on <strong>022-66813970, +917738343137</strong> within 10 days of your travel date. </td>
									</tr>
									<tr>
										<td><strong>Other Points:</strong> </td>
									</tr>
									<tr>
										<td> Rokad shall not be responsible or held liable for the behavior of operational staff in bus. Rokad shall not be responsible or held liable if the 
											bus does not stop at a pick up or drop point. Rokad shall not be responsible or held liable if passenger reaches boarding point after the stipulated time. 
											If there are any on board charges against anything by bus operator, then Rokad is not responsible. Rokad shall not be responsible or held liable for any 
											bus delay due to breakdown or operational issues. Rokad shall not be held liable for accident claims. Rokad shall not be responsible or held liable for 
											criminal/civil offense by the operator. Refund against cancellation shall be done as per policy of operator. </td>
									</tr>
									<tr>
										<td><strong>Cancellation policy:</strong> </td>
									</tr>
									<tr>
										<td> In case of cancellation, you can cancel your tickets online by logging in to<a href ="https://www.rokad.in" target="blank"> www.rokad.in</a> Cancellation charges of the tickets may 
											vary as per the policies the operators. Cancellations are allowed till advance booking duration of the service. </br>
											No refund will be made in case of e-tickets after departure of bus (from the originating bus stop of the route). </td>
									</tr>
									<tr>
										<td> For all queries call us on <strong>022-66813970, +917738343137 </strong> or write to us </td>
									</tr>
									<tr>
									
									<table class="respond" style="font-size:13px;font-family: \'Arial\,sans-serif; 	  line-height: 20px;color:#000;border-collapse: collapse;" align="center" width="800px" bordercolor="#cccccc" border="0px" cellpadding="10" cellspacing="10">
										<tr>
											<td colspan="2" style="padding:5px; font-weight:bold;">Cancellation Policy</td>
										</tr>
										<tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;">
											<td style="padding:3px;">Cancellation Time (Befor Departure)</td>
											<td style="padding:3px;">Charges</td>
										</tr>
										{{dynamic_cancellation_policy}}
										<tr>
											<td> For all queries call us on <strong>022-66813970, +91-7738343137 </strong> or email us at <a href="mailto:support@rokad.in">support@rokad.in</a> </td>
										</tr>
									</table>
									</tr>
								</table>'
    					);

//*****************Cancel Ticket Template ***************//////////

define('BOS_CANCEL_TICKET_TEMPLATE', '<table style="font-size:13px;font-family: \'Arial\',sans-serif; 	table-layout: fixed; line-height: 20px;color:#000;border-collapse: collapse;" align="center" width="800px" bordercolor="#cccccc" border="1px" cellpadding="2" cellspacing="2">
									<tr valign="top">
										<td valign="top" colspan="4">
											<table style="font-size:14px;font-family: \'Arial\',sans-serif; color:#000;" align="center" width="100%" cellpadding="0" cellspacing="0">
												<tr>
													<td colspan="2" rowspan="2" align="left" valign="top"><a href="https://www.rokad.in" target="_blank"><img src="https://www.rokad.in/assets/travelo/front/images/logo_bos.png" width="165" height="90" /></a></td>
													<td align="center" valign="top" style="vertical-align:middle; font-size:15px;font-family:Calibri (Body); font-weight:bold;">E-Cancellation Ticket</td>
													<td colspan="2" align="right" valign="top" style="color:#000;font-weight:bold; font-size:13px; text-align:centre;">Cancellation</br>Date: {{cancellation_date}}</td>
												</tr>
												{{agent_details}}
											</table>
										</td>
									</tr>
									<tr valign="middle" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 20px;">
										<td valign="middle"><span style="color:#ff940a;font-weight:bold; font-size:13px;">Journey:</span> </td>
										<td valign="middle">{{from_to_journey}}</td>
										<td valign="middle"><span style="color:#ff940a;font-weight:bold; font-size:13px;">Date of Journey: </span> </td>
										<td> {{journey_date}} </td>
									</tr>
									<tr valign="top">
										<td  style="padding:7px 0;" colspan="4" bgcolor="#efefef"><strong>Trip Information:</strong> </td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Travels:</span> </td>
										<td>{{op_name}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Bus Type:</span> </td>
										<td>{{bus_types}}</td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height:25px;">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">PNR No.:</span> </td>
										<td>{{pnr_number}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Total Seat(s):</span> </td>
										<td>{{seat_no}}</td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Rokad Ref No.:</span> </td>
										<td>{{bos_ref_no}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Total Fare:</span> </td>
										<td>{{total_fare}}</td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Cancellation Charge:</span> </td>
										<td>{{cancellation_charge}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Refund Amount:</span> </td>
										<td>{{refund_amount}}</td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										<td style="padding:7px 0;" colspan="4" bgcolor="#efefef"><strong>Boarding Information:</strong> </td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Boarding Point:</span> </td>
										<td>{{boarding_point}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Dropping Point:</span> </td>
										<td>{{dropping_point}}</td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Departure Time:</span> </td>
										<td>{{departure_time}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Alighting Time:</span> </td>
										<td>{{alighting_time}}</td>
									</tr>
									<tr valign="top">
										<td style="padding:7px 0;" colspan="4" bgcolor="#efefef"><strong>Passenger Information:</strong> </td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 40px">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Email Id:</span> </td>
										<td>{{email}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Contact No.:</span> </td>
										<td>{{mobile_no}}</td>
									</tr>
									<tr valign="top"></tr>
									<tr valign="top">
										<td colspan="4"><table style="font-size:14px;font-family: \'Arial\',sans-serif; color:#000;" align="center" width="100%" bordercolor="#cccccc" border="1px" cellpadding="0" cellspacing="0">
												<tr >
													<th style="padding:7px;text-align:center;" > <span style="color:#000;font-weight:bold; font-size:13px;">Passenger Name</span> </th>
													<th style="padding:7px 0;text-align:center;"> <span style="color:#000;font-weight:bold; font-size:13px;">Age</span> </th>
													<th style="padding:7px 0;text-align:center;"> <span style="color:#000;font-weight:bold; font-size:13px;">Gender</span> </th>
													<th style="padding:7px 0;text-align:center;"> <span style="color:#000;font-weight:bold; font-size:13px;">Seat No.</span> </th>
												</tr>
												{{passanger_details}}
											</table>
											</td>
									</tr>
								</table>
								<table style="font-size:13px;font-family: \'Arial\',sans-serif; text-align:justify; line-height: 20px;color:#000;border-collapse: collapse;" align="center" width="800px" bordercolor="#cccccc" border="0px" cellpadding="0" cellspacing="0">
									<tr>
										<td><strong></br>
											Terms & Conditions: </strong> </td>
									</tr>
									<tr>
										<td> Rokad acts as an “Intermediary” solely to assist customers in gathering travel information, determining the availability of 
											travel-related products and services, making legitimate reservations or otherwise transacting business with travel operators, and 
											for facilitating travel requirements. You acknowledge that Rokad merely provides intermediary services in order to facilitate these services. </td>
									</tr>
									<tr>
										<td><strong>Communication Policy :</strong> </td>
									</tr>
									<tr>
										<td> Upon transacting on the Rokad website, you will receive an e-mail from Rokad informing the status of your transaction on your registered email ID 
											or the ID provided at the time of booking. You will also receive a SMS on your registered phone number or the mobile number provided at the time of 
											booking. The responsibility of entering your accurate contact details like your name, email ID and mobile number is solely yours. </br>
											Rokad is not responsible to provide information on any change in bus schedules, cancellation, status of bus operator, etc. </td>
									</tr>
									<tr>
										<td><strong>Refunds:</strong> </td>
									</tr>
									<tr>
										<td> In case of cancellation, refunds will be affected according to the bus operator\'s cancellation policy charges along with our service charge 
											if any as mentioned above . After bus departure or in case of No Show, no Cancellation / Refund requests will be accepted. </br>
											Refund process may take duration of 2-3 working days due to banking procedures. In case of Net Banking, it may take 7-8 working days. 
											Processing times for cancellation and refund requests may vary from operator to operator </br>
											In case of unavailability of services, you will receive full refund except the transaction charges . </td>
									</tr>
									<tr>
										<td><strong>Grievance Policy:</strong> </td>
									</tr>
									<tr>
										<td> You can file a grievance / share feedback if you are disappointed with the services rendered by Rokad or any other issues. You can give your grievance /
											feedback through email by emailing us at <a href="mailto:support@rokad.in">support@rokad.in</a> or by calling us on <strong>022-66813970, +917738343137</strong> within 10 days of your travel date. </td>
									</tr>
									<tr>
										<td><strong>Other Points:</strong> </td>
									</tr>
									<tr>
										<td> Rokad shall not be responsible or held liable for the behavior of operational staff in bus. Rokad shall not be responsible or held liable if the 
											bus does not stop at a pick up or drop point. Rokad shall not be responsible or held liable if passenger reaches boarding point after the stipulated time. 
											If there are any on board charges against anything by bus operator, then Rokad is not responsible. Rokad shall not be responsible or held liable for any 
											bus delay due to breakdown or operational issues. Rokad shall not be held liable for accident claims. Rokad shall not be responsible or held liable for 
											criminal/civil offense by the operator. Refund against cancellation shall be done as per policy of operator. </td>
									</tr>
									<tr>
										<td><strong>Cancellation policy:</strong> </td>
									</tr>
									<tr>
										<td> In case of cancellation, you can cancel your tickets online by logging in to<a href ="https://www.rokad.in" target="blank"> www.rokad.in</a> Cancellation charges of the tickets may 
											vary as per the policies the operators. Cancellations are allowed till advance booking duration of the service. </br>
											No refund will be made in case of e-tickets after departure of bus (from the originating bus stop of the route). </td>
									</tr>
									<tr>
										<td> For all queries call us on <strong>022-66813970, +917738343137 </strong> or write to us </td>
									</tr>
									<tr>
									
									<table style="font-size:13px;font-family: \'Arial\,sans-serif; 	  line-height: 20px;color:#000;border-collapse: collapse;" align="center" width="800px" bordercolor="#cccccc" border="0px" cellpadding="10" cellspacing="10">
										<tr>
											<td colspan="2" style="padding:5px; font-weight:bold;">Cancellation Policy</td>
										</tr>
										<tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;">
											<td style="padding:3px;">Cancellation Time (Befor Departure)</td>
											<td style="padding:3px;">Charges</td>
										</tr>
										{{dynamic_cancellation_policy}}
										<tr>
											<td> For all queries call us on <strong>022-66813970, +917738343137 </strong> or email us at <a href="mailto:support@rokad.in">support@rokad.in</a> </td>
										</tr>
									</table>
									</tr>
								</table>'
    					);



define('MSRTC_TICKET_TEMPLATE', '<table width="100%" border="1" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; border-collapse:collapse; margin:0 auto;">
								  	<tr>
									    <td align="left" valign="top">
								          <table width="100%" border="0">
								              <tr>
								                <td align="right" valign="top">
								                	<img src="https://www.rokad.in/assets/travelo/front/images/ticket-logo/msrtc-logo.jpg" />
								                </td>
								                <td align="center" valign="top" style="vertical-align:middle; font-size:15px; font-weight:bold; line-height:24px;"> 
								                	Maharashtra State Road Transport Corporation <br/> E-Reservation Ticket
								                </td>
								                <td align="left" valign="top" style="vertical-align:middle;">
								                	<img src="https://www.rokad.in/assets/travelo/front/images/logo_bos.png"  />
								                </td>
								              </tr>
								            </table>
								        </td>
							  		</tr>
								  	<tr style="line-height:20px; padding:0 10px;">
									    <td align="left" valign="top">
									        <table class="respond" width="98%" border="0" style="border:1px solid #000; border-collapse:collapse; margin:6px !important;">
										      <tr style="border:1px solid #000;">
										        <td colspan="4" align="left" valign="top"  style="border:1px solid #000; padding-left:7px;">
										        	<strong> Trip Information</strong>
										        </td>
									          </tr>
										      <tr style="border:1px solid #000;">
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Trip Number:</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
										        	{{trip_no}}
										        </td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Approx. Boarding Time:</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{approx_boarding_time}}</td>
									          </tr>
										      <tr style="border:1px solid #000;">
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Ticket Number:</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
										        	{{ticket_no}}
										        </td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Transaction Id:</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
										        	{{pg_transaction_id}}
										        </td>
									          </tr>
										      <tr style="border:1px solid #000;">
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Service Start Place:</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{from_stop_name}}</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Service End Place:</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{till_stop_name}}</td>
									          </tr>
										      <tr style="border:1px solid #000;">
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Passenger Boarding Point :</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{boarding_stop_name}}</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Passenger Alighting Point:</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{destination_stop_name}}</td>
									          </tr>
										      <tr style="border:1px solid #000;">
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Date of Journey :</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
										        	<span tabindex="0" data-term="goog_1215566089">
										        		{{dept_time_date}}
										        	</span>
										        </td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Departure time from Starting Place:</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
										        	<span tabindex="0" data-term="goog_1215566090">
										        		{{dept_time_his}}
										        	</span>
										        </td>
									          </tr>
										      <tr style="border:1px solid #000;">
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Bus Service Type :</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
										        	{{bus_type}}
										        </td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">No. of Seats :</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
										        	{{num_passgr}}
										        </td>
									          </tr>
									        </table>
								    	</td>
							  		</tr>
								  	<tr>
								    	<td align="left" valign="top" style="line-height:15px; padding-left:7px;"><strong> Passengers Information</strong></td>
							  		</tr>
								  	<tr style="line-height:20px; padding:0 10px;">
								    	<td align="left" valign="top">
							        		<table class="respond" width="98%" border="0" style="border-collapse:collapse; margin:6px !important;">
										      <tr style="border:1px solid #000;">
										        <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Name</strong></td>
										        <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Age</strong></td>
										        <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Adult/Child</strong></td>
										        <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Gender</strong></td>
										        <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Seat No</strong></td>
										        <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Quota</strong></td>
										        <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Conc. Code</strong></td>
										        <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Conc. Proof</strong></td>
										        <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Conc. Proof Details</strong></td>
									          </tr>
										      {{passanger_details}}
							        		</table>
							        	</td>
							  		</tr>
							  		<tr>
										<td valign="top" align="left">
											<table class="respond" width="98%" border="1" style="border-collapse:collapse; padding:10px; line-height:20px; background:#e0e0e0; margin:6px;">
												<tbody>
													<tr>
														<td colspan="2"><strong>Total Fare Details </strong></td>
													</tr>
													<tr>
														<td align="right">Basic Fare :</td>
														<td align="right">{{tot_fare_amt}}</td>
													</tr>
													<tr>
														<td align="right">Reservation Charge :</td>
														<td align="right">{{fare_reservationCharge}}</td>
													</tr>
													<tr>
														<td align="right">ASN Amount :</td>
														<td align="right">{{asn_amount}}</td>
													</tr>
													<tr>
														<td align="right">Ac Service Charges :</td>
														<td align="right">{{ac_service_charges}}</td>
													</tr>
													<tr>
														<td align="right">
															Total Chargeable Amount :<br/>
															Including Service Tax
														</td>
														<td align="right">{{tot_fare_amt_with_tax}} + (Convenience charges will be 0.75% of total ticket amount + Government Service Tax)</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								  	<tr style="line-height:20px; padding:0 10px;">
									    <td align="left" valign="top">
									        <table class="respond" width="98%" border="0" style="border:1px solid #000; border-collapse:collapse; margin:6px !important;">
										      <tr style="border:1px solid #000;">
										        <td colspan="4" align="left" valign="top"  style="border:1px solid #000; padding-left:7px;"><strong> Rokad eTicket Information</strong></td>
									          </tr>
										      <tr style="border:1px solid #000;">
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">BOS Ref. No.:</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{bos_ref_no}}</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Email:</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{user_email}}</td>
									          </tr>
									        </table>
								    	</td>
							  		</tr>
									<tr>
										<td align="left" valign="top" style="padding:7px; text-align:justify;">
											<strong>Important</strong>
											<ul style="list-style-type: disc; margin-left: 10px;font-size:9px;">
												<li>This e-ticket is not transferable.</li>
												<li>This e-ticket is valid only for the details specified above.</li>
												<li>One of the passenger on this e-ticket is required to carry the print out of this e-ticket or to show the soft copy of e-ticket or the authenticate e-ticket SMS sent by M/S Rokad on his mobilecell, tab, laptop etc. along with Photo ID Card in Original like Voter ID Card, Aadhar Card, Driving Licence, Passport. Certificate issued by Tahsildar, ID issued by Depot Manager, Pan Card, however the senior citizen must carry his original photo ID as mentioned in e-ticket or SMS during travel.</li>
												<li>If passenger having concessional ticket found without valid ID the ticket will be treated as Invalid Ticket and amount tendered towards e-ticket will be forfeited in default. In addition the passenger would be required to purchase a fresh ticket as a normal passenger.</li>
												<li>Photo ID issued by appropriate authority should be enough elaborative and legible to establish a proof of age of 65 years or above and resident of State of Maharashtra.</li>
												<li>Corporation reserves the rights to change/cancel the bus service mentioned in the e-ticket.</li>
												<li>Passenger will have to pay the fare difference if any in case of Change of Service or in case of Revision of fare.</li>
												<li>e-ticket cancellation is allowed before 4 hours of scheduled departure time from origin OR before preparation of Reservation Chart, whichever is earlier.</li>
												<li>In case of e-tickets, cancellation of ticket will be done exclusively by the concern booking agency only.</li>
												<li>In exceptional case of cancellation of ticket due to cancellation of bus service will be done by depot manager and refund will be given by cash.</li>
											</ul>
											<p style="text-align:center;"><strong>Wish You Happy Journey</strong></p>
										</td>
								  	</tr>
							</table>
							'
    );

// For Cancel Ticket Start Here //
define('MSRTC_CANCEL_TICKET_TEMPLATE', '<table width="100%" border="1" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; border-collapse:collapse; margin:0 auto;">
								  	<tr>
									    <td align="left" valign="top">
								          <table width="100%" border="0">
								              <tr>
								                <td align="right" valign="top">
								                	<img src="https://www.rokad.in/assets/travelo/front/images/ticket-logo/msrtc-logo.jpg" />
								                </td>
								                <td align="center" valign="top" style="vertical-align:middle; font-size:15px; font-weight:bold; line-height:24px;"> 
								                	Maharashtra State Road Transport Corporation <br/> E-Cancellation Ticket
								                </td>
								                <td align="left" valign="top" style="vertical-align:middle;">
								                	<img src="https://www.rokad.in/assets/travelo/front/images/logo_bos.png"  />
								                </td>
								              </tr>
								            </table>
								        </td>
							  		</tr>
								  	<tr style="line-height:20px; padding:0 10px;">
									    <td align="left" valign="top">
									        <table width="98%" border="0" style="border:1px solid #000; border-collapse:collapse; margin:6px !important;">
										      <tr style="border:1px solid #000;">
										        <td colspan="4" align="left" valign="top"  style="border:1px solid #000; padding-left:7px;">
										        	<strong> Trip Information</strong>
										        </td>
									          </tr>
										      <tr style="border:1px solid #000;">
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Trip Number:</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
										        	{{trip_no}}
										        </td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Approx. Boarding Time:</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{approx_boarding_time}}</td>
									          </tr>
										      <tr style="border:1px solid #000;">
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Ticket Number:</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
										        	{{ticket_no}}
										        </td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Transaction Id:</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
										        	{{pg_transaction_id}}
										        </td>
									          </tr>
										      <tr style="border:1px solid #000;">
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Service Start Place:</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{from_stop_name}}</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Service End Place:</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{till_stop_name}}</td>
									          </tr>
										      <tr style="border:1px solid #000;">
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Passenger Boarding Point :</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{boarding_stop_name}}</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Passenger Alighting Point:</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{destination_stop_name}}</td>
									          </tr>
										      <tr style="border:1px solid #000;">
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Date of Journey :</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
										        	<span tabindex="0" data-term="goog_1215566089">
										        		{{dept_time_date}}
										        	</span>
										        </td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Departure time from Starting Place:</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
										        	<span tabindex="0" data-term="goog_1215566090">
										        		{{dept_time_his}}
										        	</span>
										        </td>
									          </tr>
										      <tr style="border:1px solid #000;">
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Bus Service Type :</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
										        	{{bus_type}}
										        </td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">No. of Seats :</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
										        	{{num_passgr}}
										        </td>
									          </tr>
									        </table>
								    	</td>
							  		</tr>
								  	<tr>
								    	<td align="left" valign="top" style="line-height:15px; padding-left:7px;"><strong> Passengers Information</strong></td>
							  		</tr>
								  	<tr style="line-height:20px; padding:0 10px;">
								    	<td align="left" valign="top">
							        		<table width="98%" border="0" style="border-collapse:collapse; margin:6px !important;">
										      <tr style="border:1px solid #000;">
										        <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Name</strong></td>
										        <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Age</strong></td>
										        <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Adult/Child</strong></td>
										        <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Gender</strong></td>
										        <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Seat No</strong></td>
										        <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Quota</strong></td>
										        <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Conc. Code</strong></td>
										        <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Conc. Proof</strong></td>
										        <td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Conc. Proof Details</strong></td>
									          </tr>
										      {{passanger_details}}
							        		</table>
							        	</td>
							  		</tr>
							  		<tr>
										<td valign="top" align="left">
											<table width="98%" border="1" style="border-collapse:collapse; padding:10px; line-height:20px; background:#e0e0e0; margin:6px;">
												<tbody>
													<tr>
														<td colspan="2"><strong>Total Fare Details </strong></td>
													</tr>
													<tr>
														<td align="right">Total Fare :</td>
														<td align="right">{{tot_fare_amt}}</td>
													</tr>
													<tr>
														<td align="right">Cancellation Charge :</td>
														<td align="right">{{cancellation_cahrge}}</td>
													</tr>
													<tr>
														<td align="right">Refund Amount :</td>
														<td align="right">{{refund_amt}}</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								  	<tr style="line-height:20px; padding:0 10px;">
									    <td align="left" valign="top">
									        <table width="98%" border="0" style="border:1px solid #000; border-collapse:collapse; margin:6px !important;">
										      <tr style="border:1px solid #000;">
										        <td colspan="4" align="left" valign="top"  style="border:1px solid #000; padding-left:7px;"><strong> BOS eTicket Information</strong></td>
									          </tr>
										      <tr style="border:1px solid #000;">
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">BOS Ref. No.:</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{bos_ref_no}}</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Email:</td>
										        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{user_email}}</td>
									          </tr>
									        </table>
								    	</td>
							  		</tr>
									<tr>
										<td align="left" valign="top" style="padding:7px; text-align:justify;">
											<strong>Important</strong>
											<ul style="list-style-type: disc; margin-left: 10px;font-size:9px;">
												<li>This e-ticket is not transferable.</li>
												<li>This e-ticket is valid only for the details specified above.</li>
												<li>One of the passenger on this e-ticket is required to carry the print out of this e-ticket or to show the soft copy of e-ticket or the authenticate e-ticket SMS sent by M/S Rokad on his mobilecell, tab, laptop etc. along with Photo ID Card in Original like Voter ID Card, Aadhar Card, Driving Licence, Passport. Certificate issued by Tahsildar, ID issued by Depot Manager, Pan Card, however the senior citizen must carry his original photo ID as mentioned in e-ticket or SMS during travel.</li>
												<li>If passenger having concessional ticket found without valid ID the ticket will be treated as Invalid Ticket and amount tendered towards e-ticket will be forfeited in default. In addition the passenger would be required to purchase a fresh ticket as a normal passenger.</li>
												<li>Photo ID issued by appropriate authority should be enough elaborative and legible to establish a proof of age of 65 years or above and resident of State of Maharashtra.</li>
												<li>Corporation reserves the rights to change/cancel the bus service mentioned in the e-ticket.</li>
												<li>Passenger will have to pay the fare difference if any in case of Change of Service or in case of Revision of fare.</li>
												<li>e-ticket cancellation is allowed before 4 hours of scheduled departure time from origin OR before preparation of Reservation Chart, whichever is earlier.</li>
												<li>In case of e-tickets, cancellation of ticket will be done exclusively by the concern booking agency only.</li>
												<li>In exceptional case of cancellation of ticket due to cancellation of bus service will be done by depot manager and refund will be given by cash.</li>
											</ul>
											<p style="text-align:center;"><strong>Wish You Happy Journey</strong></p>
										</td>
								  	</tr>
							</table>
							'
    );
// For Cancel Ticket End Here//


define('HRTC_TICKET_TEMPLATE', '<table style="font-size:12px;font-family: \'Arial\',sans-serif; color:#000;" align="center" width="100%" bordercolor="#cccccc" border="1px" cellpadding="5" cellspacing="0">
								    <tr>
								        <td>
								        	<img src="https://www.rokad.in/assets/travelo/front/images/ticket-logo/hrtc_logo.jpg" alt="Himachal Road Tranport Corporation" >
								        	<p>www.rokad.in</p>
								        </td>
								        <td align="center" colspan="2">
								        	 <table style="font-size:12px;font-family: \'Arial\',sans-serif; color:#000;" align="center" width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td style="border-right:2px solid #cccccc;">
											            <h1 style="font-size:21px;font-family: \'Tangerine\', serif; font-weight: 600;  line-height: 21px;color:#000;">
											                हिमाचल पथ परिवहन निगम </br>
											                HIMACHAL ROAD TRANPORT CORPORATION JOURNEY CUM RESERVATION TICKET
											            </h1>
											        </td>
													<td>
														<img alt="Rokad Banner" src="https://www.rokad.in/assets/travelo/front/images/logo_bos.png" >
													</td>
												</tr>
											</table>
								        </td>
								    </tr>
								    <tr>
								        <td align="center" colspan="3">
								            <h3 style="font-size:14px;font-family: \'Arial\',sans-serif; font-weight: 600;  line-height: 20px;color:#000;">
								                Route ID : {{bus_service_no}} : {{from}} - {{to}} :{{journy_time}}
								            </h3>
								        </td>
								    </tr>
								    <tr>
								        <td>यात्रा पत्र TICKET NO.</td>
								        <td>प्रस्थान समय DEP.TIME FROM SOURCE</td>
								        <td>यात्रा तिथि JOURNEY DATE</td>
								    </tr>
								    <tr>
								        <td><strong>{{ticket_number}}</strong></td>
								        <td><strong>{{journy_time}}</strong></td>
								        <td><strong>{{journy_date}}</strong></td>
								    </tr>
								    <tr>
								        <td>सेवा श्रेणी SERVICE CLASS</td>
								        <td>उदगम स्थान SOURCE</td>
								        <td>गंतव्य स्थान DESTINATION</td>
								    </tr>
								    <tr>
								        <td><strong>{{bus_type}}</strong></td>
								        <td><strong>{{from}}</strong></td>
								        <td><strong>{{to}}</strong></td>
								    </tr>
								    <tr>
								        <td colspan="3">बस मैं चढ़ने स्थान का BOARDING PLACE {{from}}</td>
								    </tr>
								    <tr>
								        <td valign="top" colspan="2" rowspan="6">
								            <table class="respond" style="font-size:12px;font-family: \'Arial\',sans-serif; color:#000;" align="center" width="100%" bordercolor="#a0a0a0" border="1px" cellpadding="5" cellspacing="0">
								                <tr>
								                    <th bgcolor="#eaeaea"><strong>Seat No.<strong></th>
								                    <th bgcolor="#eaeaea"><strong>Name<strong></th>
								                    <th bgcolor="#eaeaea"><strong>Age<strong></th>
								                    <th bgcolor="#eaeaea"><strong>Sex<strong></th>
								                </tr>
								                {{passanger_details}}
								                <tr>
								                    <td colspan="2">Fare:<strong>{{sub_total_fare}}</strong></td>
								                    <td colspan="2">Service Charges: <strong>{{service_charge}}</strong></td>
								                </tr>
								                <tr>
								                    <td colspan="2">DiscountType:<strong>No Discount</strong></td>
								                    <td colspan="2">Discount:<strong>0</strong></td>
								                </tr>
								            </table>
								        </td>
								        <td>वयस्क ADULT</td>
								    </tr>
								    <tr>
								        <td><strong>{{number_of_adults}}</strong></td>
								    </tr>
								    <tr>
								        <td>बच्चे CHILD</td>
								    </tr>
								    <tr>
								        <td><strong>{{number_of_childs}}</strong></td>
								    </tr>
								    <tr>
								        <td>NET AMT</td>
								    </tr>
								    <tr>
								        <td><strong>{{total_fare}}</strong></td>
								    </tr>
								    <tr>
										<td colspan="3">
										
										<table class="respond" style="font-size:12px;font-family: \'Arial\',sans-serif; color:#000;" align="center" width="100%" bordercolor="#cccccc" border="1px" cellpadding="5" cellspacing="0">
											<tr style="border:1px solid #000;">
										        <td colspan="4" align="left" valign="top"= ><strong> BOS eTicket Information</strong></td>
									          </tr>
										      <tr style="border:1px solid #000;">
										        <td align="left" valign="top" >BOS Ref. No.:</td>
										        <td align="left" valign="top" >{{bos_ref_no}}</td>
										        <td align="left" valign="top" >Email:</td>
										        <td align="left" valign="top" >{{email_id}}</td>
									          </tr>
									        </table>
										</td>
									</tr>
								    <tr>
								        <td colspan="3">*Passenger who has to carry the Identity Card in original.<br>
								        <strong>Note :</strong> You have to pay discount given, if you failed to produce Smart Cards on demand during journey.</td>
								    </tr>
								</table>
								<table class="respond" style="font-size:11px;font-family: \'Arial\',sans-serif; color:#000;" align="center" border="0px" cellpadding="5" cellspacing="0" width="100%">
									<tbody>
										<tr>
											<td colspan="3">&nbsp;</td>
										</tr>
										<tr>
											<td colspan="2">
												<strong>Terms & Conditions: </strong>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												Rokad acts as an “Intermediary” solely to assist customers in gathering travel information,
												determining the availability of travel-related products and services, making legitimate reservations or 
												otherwise transacting business with travel operators, and for facilitating travel requirements. 
												You acknowledge that Rokad merely provides intermediary services in order to facilitate these services. 
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<strong>Communication Policy :</strong>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												Upon transacting on the Rokad website, you will receive an e-mail from Rokad informing the status of your transaction on your registered 
												email ID or the ID provided at the time of booking. You will also receive a SMS on your registered phone number or the mobile number 
												provided at the time of booking. The responsibility of entering your accurate contact details like your name, email ID and mobile number 
												is solely yours.  
												</br>
												Rokad is not responsible to provide information on any change in bus schedules, cancellation, status of bus operator, etc.
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<strong>Refunds:</strong>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												In case of cancellation, refunds will be affected according to the bus operator\'s cancellation policy charges along with our service charge if any as mentioned above . After bus departure or in case of No Show, no Cancellation / Refund requests will be accepted. 
												</br>
												Refund process may take duration of 2-3 working days due to banking procedures. 
												</br>	
												In case of Net Banking, it may take 7-8 working days. Processing times for cancellation and refund requests may vary from operator to operator.
												In case of unavailability of services, you will receive full refund except the transaction charges .
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<strong>Grievance Policy:</strong>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												You can file a grievance / share feedback if you are disappointed with the services rendered by Rokad or any other
												issues. You can give your grievance / feedback through email by emailing us at
												<a href="mailto:support@rokad.in">support@rokad.in</a>  or by 
												calling us on <strong>022-66813970, +917738343137</strong> within 10 days of your travel date.
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<strong>Other Points:</strong>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												Rokad shall not be responsible or held liable for the behavior of operational staff in bus. Rokad shall not be responsible 
												or held liable if the bus does not stop at a pick up or drop point. Rokad shall not be responsible or held liable 
												if passenger reaches boarding point after the stipulated time. If there are any on board charges against anything 
												by bus operator, then Rokad is not responsible. Rokad shall not be responsible or held liable for any bus delay due to 
												breakdown or operational issues. Rokad shall not be held liable for accident claims. Rokad shall not be responsible or 
												held liable for criminal/civil offense by the operator. Refund against cancellation shall be done as per policy of 
												operator.
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<strong>Cancellation policy:</strong>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												In case of cancellation, you can cancel your tickets online by logging in to www.rokad.in. Cancellation charges 
												of the tickets may vary as per the policies the operators. Cancellations are allowed till advance booking duration of 
												the service.
												</br>
												An amount of 10% of fare will be deducted in case the cancellation of e-tickets is up to 12 hours prior to the 
												departure time of the bus (from originating bus stop of the route).
												</br>
												An amount of 25% of fare will be deducted in case the cancellation of e-tickets is within 4 to 12 hours prior to 
												the departure time of the bus (from originating bus stop of the route). After this no cancellation is allowed.
												</br>
												The e-Tickets can only be cancelled by the User himself upto 4 hours prior to the Departure Time of the bus 
												(from the originating bus stop of the route) through www.rokad.in. 
											</td>
										</tr>
										<tr>
											<td colspan="2">
												The service charges  are non-refundable in case of e-Tickets cancellation.
											</td>
										</tr>
										<tr>
											<td colspan="2">
												No refund will be made in case of e-tickets after departure of bus (from the originating bus stop of the route).
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<strong>For all queries call us on 022-66813970, +917738343137 or write to us  </strong>
											</td>
										</tr>
										<tr>
											<table class="respond" style="font-size:12px;font-family: \'Arial\',sans-serif; margin:10px 0; color:#373636 ;" align="center" width="100%" bordercolor="#cccccc" border="0px" cellpadding="5" cellspacing="5">
	                                          <tr>
	                                            <td colspan="2" style="padding:5px; font-weight:bold;">Cancellation Policy</td>
	                                          </tr>
	                                          <tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;">
	                                            <td style="padding:3px;">Cancellation Time (Befor Departure)</td>
	                                            <td style="padding:3px;">Charges</td>
	                                          </tr>
	                                          <tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;">
                                              	<td style="padding:3px;">Cancellation within 12 hrs</td>
                                              	<td style="padding:3px;">10%</td>
                                              </tr>
                                              <tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;">
                                              	<td style="padding:3px;">Cancellation within 4-12 hrs</td>
                                              	<td style="padding:3px;">25%</td>
                                              </tr>
	                                        </table>
										</tr>
										<tr>
											<td colspan="2">
												<strong>
													For all queries call us on 022-66813970, +917738343137 or email us at
													<a href="mailto:support@rokad.in">support@rokad.in</a>
												</strong>  
											</td>
										</tr>
									</tbody>
								</table>'
							);

// HSRTC Cancel Ticket Template Start Here //
define('HRTC_CANCEL_TICKET_TEMPLATE', '<table style="font-size:12px;font-family: \'Arial\',sans-serif; color:#000;" align="center" width="100%" bordercolor="#cccccc" border="1px" cellpadding="5" cellspacing="0">
								    <tr>
								        <td>
								        	<img src="https://www.rokad.in/assets/travelo/front/images/ticket-logo/hrtc_logo.jpg" alt="Himachal Road Tranport Corporation" >
								        	<p>www.rokad.in</p>
								        </td>
								        <td align="center" colspan="2">
								        	 <table style="font-size:12px;font-family: \'Arial\',sans-serif; color:#000;" align="center" width="100%" border="0" cellpadding="0" cellspacing="0">
												<tr>
													<td style="border-right:2px solid #cccccc;">
											            <h1 style="font-size:21px;font-family: \'Tangerine\', serif; font-weight: 600;  line-height: 21px;color:#000;">
											                हिमाचल पथ परिवहन निगम </br>
											                HIMACHAL ROAD TRANPORT CORPORATION JOURNEY CUM RESERVATION TICKET
											            </h1>
											        </td>
													<td>
														<img alt="Rokad Banner" src="https://www.rokad.in/assets/travelo/front/images/ticket-logo/BoS_Sq_logo_small.png">
													</td>
												</tr>
											</table>
								        </td>
								    </tr>
								    <tr>
								        <td align="center" colspan="3">
								            <h3 style="font-size:14px;font-family: \'Arial\',sans-serif; font-weight: 600;  line-height: 20px;color:#000;">
								                Route ID : {{bus_service_no}} : {{from}} - {{to}} :{{journy_time}}
								            </h3>
								        </td>
								    </tr>
								    <tr>
								        <td>यात्रा पत्र TICKET NO.</td>
								        <td>प्रस्थान समय DEP.TIME FROM SOURCE</td>
								        <td>यात्रा तिथि JOURNEY DATE</td>
								    </tr>
								    <tr>
								        <td><strong>{{ticket_number}}</strong></td>
								        <td><strong>{{journy_time}}</strong></td>
								        <td><strong>{{journy_date}}</strong></td>
								    </tr>
								    <tr>
								        <td>सेवा श्रेणी SERVICE CLASS</td>
								        <td>उदगम स्थान SOURCE</td>
								        <td>गंतव्य स्थान DESTINATION</td>
								    </tr>
								    <tr>
								        <td><strong>{{bus_type}}</strong></td>
								        <td><strong>{{from}}</strong></td>
								        <td><strong>{{to}}</strong></td>
								    </tr>
								    <tr>
								        <td colspan="3">बस मैं चढ़ने स्थान का BOARDING PLACE {{from}}</td>
								    </tr>
								    <tr>
								        <td valign="top" colspan="2" rowspan="10">
								            <table style="font-size:12px;font-family: \'Arial\',sans-serif; color:#000;" align="center" width="100%" bordercolor="#a0a0a0" border="1px" cellpadding="5" cellspacing="0">
								                <tr>
								                    <th bgcolor="#eaeaea"><strong>Seat No.<strong></th>
								                    <th bgcolor="#eaeaea"><strong>Name<strong></th>
								                    <th bgcolor="#eaeaea"><strong>Age<strong></th>
								                    <th bgcolor="#eaeaea"><strong>Sex<strong></th>
								                </tr>
								                {{passanger_details}}
								                <tr>
								                    <td colspan="2">Fare:<strong>{{sub_total_fare}}</strong></td>
								                    <td colspan="2">Service Charges: <strong>{{service_charge}}</strong></td>
								                </tr>
								                <tr>
								                    <td colspan="2">DiscountType:<strong>No Discount</strong></td>
								                    <td colspan="2">Discount:<strong>0</strong></td>
								                </tr>
								            </table>
								        </td>
								        <td>वयस्क ADULT</td>
								    </tr>
								    <tr>
								        <td><strong>{{number_of_adults}}</strong></td>
								    </tr>
								    <tr>
								        <td>बच्चे CHILD</td>
								    </tr>
								    <tr>
								        <td><strong>{{number_of_childs}}</strong></td>
								    </tr>
								    <tr>
								        <td>NET AMT</td>
								    </tr>
								    <tr>
								        <td><strong>{{total_fare}}</strong></td>
								    </tr>
								    <tr>
								        <td>Refund Amount</td>
								    </tr>
								    <tr>
								        <td><strong>{{refund_amount}}</strong></td>
								    </tr>
								    <tr>
								        <td>Cancellation Charge</td>
								    </tr>
								    <tr>
								        <td><strong>{{cancellation_cahrge}}</strong></td>
								    </tr>
								    <tr>
										<td colspan="3">
										
										<table style="font-size:12px;font-family: \'Arial\',sans-serif; color:#000;" align="center" width="100%" bordercolor="#cccccc" border="1px" cellpadding="5" cellspacing="0">
											<tr style="border:1px solid #000;">
										        <td colspan="4" align="left" valign="top"= ><strong> BOS eTicket Information</strong></td>
									          </tr>
										      <tr style="border:1px solid #000;">
										        <td align="left" valign="top" >BOS Ref. No.:</td>
										        <td align="left" valign="top" >{{bos_ref_no}}</td>
										        <td align="left" valign="top" >Email:</td>
										        <td align="left" valign="top" >{{email_id}}</td>
									          </tr>
									        </table>
										</td>
									</tr>
								    <tr>
								        <td colspan="3">*Passenger who has to carry the Identity Card in original.<br>
								        <strong>Note :</strong> You have to pay discount given, if you failed to produce Smart Cards on demand during journey.</td>
								    </tr>
								</table>
								<table style="font-size:11px;font-family: \'Arial\',sans-serif; color:#000;" align="center" border="0px" cellpadding="5" cellspacing="0" width="100%">
									<tbody>
										<tr>
											<td colspan="3">&nbsp;</td>
										</tr>
										<tr>
											<td colspan="2">
												<strong>Terms & Conditions: </strong>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												Rokad acts as an “Intermediary” solely to assist customers in gathering travel information,
												determining the availability of travel-related products and services, making legitimate reservations or 
												otherwise transacting business with travel operators, and for facilitating travel requirements. 
												You acknowledge that Rokad merely provides intermediary services in order to facilitate these services. 
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<strong>Communication Policy :</strong>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												Upon transacting on the Rokad website, you will receive an e-mail from Rokad informing the status of your transaction on your registered 
												email ID or the ID provided at the time of booking. You will also receive a SMS on your registered phone number or the mobile number 
												provided at the time of booking. The responsibility of entering your accurate contact details like your name, email ID and mobile number 
												is solely yours.  
												</br>
												Rokad is not responsible to provide information on any change in bus schedules, cancellation, status of bus operator, etc.
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<strong>Refunds:</strong>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												In case of cancellation, refunds will be affected according to the bus operator\'s cancellation policy charges along with our service charge if any as mentioned above . After bus departure or in case of No Show, no Cancellation / Refund requests will be accepted. 
												</br>
												Refund process may take duration of 2-3 working days due to banking procedures. 
												</br>	
												In case of Net Banking, it may take 7-8 working days. Processing times for cancellation and refund requests may vary from operator to operator.
												In case of unavailability of services, you will receive full refund except the transaction charges .
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<strong>Grievance Policy:</strong>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												You can file a grievance / share feedback if you are disappointed with the services rendered by Rokad or any other
												issues. You can give your grievance / feedback through email by emailing us at
												<a href="mailto:support@rokad.in">support@rokad.in</a>  or by 
												calling us on <strong>022-66813970, +917738343137</strong> within 10 days of your travel date.
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<strong>Other Points:</strong>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												Rokad shall not be responsible or held liable for the behavior of operational staff in bus. Rokad shall not be responsible 
												or held liable if the bus does not stop at a pick up or drop point. Rokad shall not be responsible or held liable 
												if passenger reaches boarding point after the stipulated time. If there are any on board charges against anything 
												by bus operator, then Rokad is not responsible. Rokad shall not be responsible or held liable for any bus delay due to 
												breakdown or operational issues. Rokad shall not be held liable for accident claims. Rokad shall not be responsible or 
												held liable for criminal/civil offense by the operator. Refund against cancellation shall be done as per policy of 
												operator.
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<strong>Cancellation policy:</strong>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												In case of cancellation, you can cancel your tickets online by logging in to www.rokad.in. Cancellation charges 
												of the tickets may vary as per the policies the operators. Cancellations are allowed till advance booking duration of 
												the service.
												</br>
												An amount of 10% of fare will be deducted in case the cancellation of e-tickets is up to 12 hours prior to the 
												departure time of the bus (from originating bus stop of the route).
												</br>
												An amount of 25% of fare will be deducted in case the cancellation of e-tickets is within 4 to 12 hours prior to 
												the departure time of the bus (from originating bus stop of the route). After this no cancellation is allowed.
												</br>
												The e-Tickets can only be cancelled by the User himself upto 4 hours prior to the Departure Time of the bus 
												(from the originating bus stop of the route) through www.rokad.in. 
											</td>
										</tr>
										<tr>
											<td colspan="2">
												The service charges  are non-refundable in case of e-Tickets cancellation.
											</td>
										</tr>
										<tr>
											<td colspan="2">
												No refund will be made in case of e-tickets after departure of bus (from the originating bus stop of the route).
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<strong>For all queries call us on 022-66813970, +917738343137 or write to us  </strong>
											</td>
										</tr>
										<tr>
											<table style="font-size:12px;font-family: \'Arial\',sans-serif; margin:10px 0; color:#373636 ;" align="center" width="100%" bordercolor="#cccccc" border="0px" cellpadding="5" cellspacing="5">
	                                          <tr>
	                                            <td colspan="2" style="padding:5px; font-weight:bold;">Cancellation Policy</td>
	                                          </tr>
	                                          <tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;">
	                                            <td style="padding:3px;">Cancellation Time (Befor Departure)</td>
	                                            <td style="padding:3px;">Charges</td>
	                                          </tr>
	                                          <tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;">
                                              	<td style="padding:3px;">Cancellation within 12 hrs</td>
                                              	<td style="padding:3px;">10%</td>
                                              </tr>
                                              <tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;">
                                              	<td style="padding:3px;">Cancellation within 4-12 hrs</td>
                                              	<td style="padding:3px;">25%</td>
                                              </tr>
	                                        </table>
										</tr>
										<tr>
											<td colspan="2">
												<strong>
													For all queries call us on 022-66813970, +917738343137 or email us at
													<a href="mailto:support@rokad.in">support@rokad.in</a>
												</strong>  
											</td>
										</tr>
									</tbody>
								</table>'
							);
// HSRTC Cancel Ticket Template End Here//

//DELETE BELOW TEMP_TICKET_NOTIFICATION
define('TEMP_TICKET_NOTIFICATION', 'Ticket Booked Successfully.PNR:{{pnr_no}},Rokad Ref:{{bos_ref_no}},Seat No:{{seat_no}},Amt:Rs.{{amount_to_pay}},From:{{from_stop}},To:{{to_stop}},DOJ:{{doj}} www.rokad.in');

// define('CANCEL_REFUND_TICKET_SMS', 'Your ticket is cancelled, BOS Ref Number: {{bos_ref_no}}, Rs.{{amount_to_pay}} will be refunded to you. Please check your A/c after {{day}} days.');

define('RSRTC_TICKET_TEMPLATE', '<table width="800" border="1" cellpadding="0" style="font-family:Calibri (Body); border-collapse:collapse; margin:0 auto;">
									<tbody>
										<tr>
											<td align="left" valign="top">
												<table width="100%" border="0" style="font-family:Calibri (Body);font-size:11px;">
													<tbody>
														<tr>
															<td align="left" valign="top"><img src="https://www.rokad.in/assets/travelo/front/images/ticket-logo/RSRTC_logo.jpg" width="120" height="105" /></td>
															<td align="center" valign="middle">
																<table width="100%" border="0">
																	<tr>
																		<td align="center" valign="top" style="vertical-align:middle; font-size:14px; font-weight:bold; line-height:24px;">Rajasthan State Road Transport Corporation</td>
																	</tr>
																	<tr>
																		<td align="center" valign="top" style="vertical-align:middle; font-size:11px;font-family:Calibri (Body); font-weight:bold;">E-Ticket</td>
																	</tr>
																</table>
															</td>
															<td align="right" valign="top" style="vertical-align:middle;"><a href="https://www.rokad.in" target="_blank"><img src="https://www.rokad.in/assets/travelo/front/images/logo_bos.png"  /></a>
																<br/>
																<span style="margin-right:30px;">www.rokad.in</span>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
										<tr style="line-height:20px; padding:0 10px;">
											<td align="left" valign="top">
												<table class="respond" width="98%" border="0" style="border:1px solid #000; border-collapse:collapse; margin:6px !important;font-size:11px;font-family:Calibri (Body);">
													<tbody>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Rokad Ref No.</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{bos_ref_no}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">PNR No.</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{pnr_no}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Reservation From</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{from_stop_name}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Reservation To</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{till_stop_name}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Boarding Point</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{boarding_stop_name}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Alighting Point</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{destination_stop_name}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Boarding Time(Approximate)</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{dept_time_his}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Alighting Time(Approximate)</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{alighting_time}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Journey Date</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{dept_time_date}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Transaction Date & Time</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{dept_time_date}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Depot name/email id</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{boarding_point_contact_no}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Bus Service Type</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{bus_type}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Name of Website Service Offered</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">www.rokad.in</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Website Support Email Id</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">support@rokad.in</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Passenger email id</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{user_email}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;"></td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;"></td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
										<tr>
											<td align="left" valign="top">&nbsp;</td>
										</tr>
										<tr>
											<td align="left" valign="top" style="font-family:Calibri (Body);font-size:11px;line-height:25px; padding-left:7px;"><strong>Passenger Detail</strong></td>
										</tr>
										<tr style="line-height:20px; padding:0 10px;">
											<td align="left" valign="top">
												<table class="respond" width="98%" border="0" style="border-collapse:collapse; margin:6px !important;font-size:11px;font-family:Calibri (Body);">
													<tbody>
														<tr style="border:1px solid #000;">
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Name</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Age</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Adult/Child</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Gender</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Seat No. </strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Concession</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Quota</strong></td>
														</tr>
														{{passanger_details}}
													</tbody>
												</table>
											</td>
										</tr>
										<tr>
											<td valign="top" align="left">&nbsp;</td>
										</tr>
										<tr>
											<td style="font-family:Calibri (Body);font-size:11px;line-height:25px; padding-left:7px;" valign="top" align="left"><strong>Fare Detail</strong></td>
										</tr>
										<tr>
											<td valign="top" align="right">
												<table class="respond" width="98%" border="0" style="border-collapse:collapse; margin:6px !important;font-size:11px;font-family:Calibri (Body);">
													<tbody>
														<tr>
															<td width="30%" align="right" valign="top" style="border:1px solid #000; padding-left:7px; ">Fare :</td>
															<td width="20%" align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{tot_fare_amt}}</td>
															<td></td>
														</tr>
														<tr>
															<td align="right" valign="top" style="border:1px solid #000; padding-left:7px; ">Service Charges :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{other_charges}}</td>
															<td></td>
														</tr>
														<tr>
															<td align="right" valign="top" style="border:1px solid #000; padding-left:7px; ">Payment Gateway  Charges :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">0</td>
															<td></td>
														</tr>
														<tr>
															<td align="right" valign="top" style="border:1px solid #000; padding-left:7px; "><strong>Discount, if any :</strong></td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>0</strong></td>
															<td></td>
														</tr>
														<tr>
															<td align="right" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Total Fare :</strong></td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>{{tot_fare_amt_with_tax}}</strong></td>
															<td></td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
										<tr>
											<td align="left" valign="top">
												<table class="respond" width="98%" border="0" style="font-size:11px;font-family:Calibri (Body); ">
													<tbody>
														<tr>
															<td colspan="5"><strong>Important Information:</strong></td>
														</tr>
														<tr>
															<td colspan="4">
																<ul>
																	<li>This E-Ticket is not Transferable and valid only for the details specified above.</li>
																	<li>Passenger is required to carry either printout of the e-tickets or SMS while travelling.</li>
																	<li>The On-Duty Conductor/Checking staff person authorized by RSRTC will verify the Photo Identity Card of the passenger regarding e-Ticket details on Print out/SMS & Reservation Chart. If the passenger fails to produce the Print Out/SMS of e-Ticket and the specified Photo Identity Proof in original during journey, the e-ticket will be treated as INVALID and the passenger will be treated as "Traveling without Ticket". Photocopy of Identity Proof is not allowed.</li>
																	<li>RSRTC reserves the rights to change/cancel the bus service mentioned in the e-ticket. </li>
																	<li>In case of change of service or revision of fare or due to technical error, passenger will have to pay the fare difference in cash.</li>
																	<li>Cancellation of E-Ticket is permitted under the terms and conditions agreed by the passenger while booking the e-ticket.</li>
																	<li>For Debit/Credit Cards additional surcharge is as per Payment Gateway policy of service offering agency.</li>
																	<li>If there is any query related with refund. Please send your mail to service offering website. E-Mail id :support@rokad.in</li>
																</ul>
																<center><strong>Wish You A Happy Journey</strong>
																<br clear="all"/>
																For more details visit us www.rokad.in and helpdesk email/Phone No.
																Send your support e-mail to: rsrtchelpdesk@gmail.com.</center>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
								<br />
								<table class="respond" width="800" border="1" cellpadding="0" style="font-family:Calibri (Body); border-collapse:collapse; margin:0 auto; font-size:11px;">
									<tr>
										<td align="left" valign="top" style="line-height:35px; padding-left:7px;"><strong>Cancellation Policies</strong></td>
									</tr>
									<tr>
										<td align="left" valign="top">
											<table class="respond" width="98%" border="0" style="border-collapse:collapse; margin:6px !important;font-size:11px;">
												<tr>
													<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;"><strong>Cancellation Time</strong></td>
													<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; "><strong>Cancellation Charges</strong></td>
												</tr>
												<tr>
													<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Cancellation upto 24 hrs</td>
													<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; ">5.00 %</td>
												</tr>
												<tr>
													<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Cancellation upto 1 hrs</td>
													<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; ">20.00 %</td>
												</tr>
												<tr>
													<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Cancellation upto 30 mins</td>
													<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; ">50.00 %</td>
												</tr>
												<tr>
													<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;" colspan="2">* <em>Reservation charges and government service tax charge (if any) are non refundable.</em></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>');

define('RSRTC_CANCEL_TICKET_TEMPLATE', '<table width="800" border="1" cellpadding="0" style="font-family:Calibri (Body); border-collapse:collapse; margin:0 auto;">
									<tbody>
										<tr>
											<td align="left" valign="top">
												<table width="100%" border="0" style="font-family:Calibri (Body);font-size:11px;">
													<tbody>
														<tr>
															<td align="left" valign="top"><img src="https://www.rokad.in/assets/travelo/front/images/ticket-logo/RSRTC_logo.jpg" width="120" height="105" /></td>
															<td align="center" valign="middle">
																<table width="100%" border="0">
																	<tr>
																		<td align="center" valign="top" style="vertical-align:middle; font-size:14px; font-weight:bold; line-height:24px;">Rajasthan State Road Transport Corporation</td>
																	</tr>
																	<tr>
																		<td align="center" valign="top" style="vertical-align:middle; font-size:11px;font-family:Calibri (Body); font-weight:bold;">E-Cancellation Ticket</td>
																	</tr>
																</table>
															</td>
															<td align="right" valign="top" style="vertical-align:middle;"><a href="https://www.rokad.in" target="_blank"><img src="https://www.rokad.in/assets/travelo/front/images/ticket-logo/BoS_Sq_logo_small.png" width="165" height="95" /></a>
																<br/>
																<span style="margin-right:30px;">www.rokad.in</span>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
										<tr style="line-height:20px; padding:0 10px;">
											<td align="left" valign="top">
												<table width="98%" border="0" style="border:1px solid #000; border-collapse:collapse; margin:6px !important;font-size:11px;font-family:Calibri (Body);">
													<tbody>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Cancellation Ticket No </td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{ticket_no}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;"></td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;"></td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Rokad Ref No.</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{boss_ref_no}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">PNR No.</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{pnr_no}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Reservation From</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{boarding_point}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Reservation To</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{alightning_point}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Boarding Point</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{boarding_point}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Alighting Point</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{alightning_point}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Boarding Time(Approximate)</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{boarding_time_dmyHis}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Alighting Time(Approximate)</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{dropping_time_dmyHis}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Journey Date</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{date_of_journey_origin_dmy}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Transaction Date & Time</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{transaction_date}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Depot name/email id</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{depot_detail}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Bus Service Type</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{bus_type}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Name of Website Service Offered</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">www.rokad.in</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Website Support Email Id</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">support@rokad.in</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Passenger email id</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{user_email}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;"></td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;"></td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
										<tr>
											<td align="left" valign="top">&nbsp;</td>
										</tr>
										<tr>
											<td align="left" valign="top" style="font-family:Calibri (Body);font-size:11px;line-height:25px; padding-left:7px;"><strong>Passenger Detail</strong></td>
										</tr>
										<tr style="line-height:20px; padding:0 10px;">
											<td align="left" valign="top">
												<table width="98%" border="0" style="border-collapse:collapse; margin:6px !important;font-size:11px;font-family:Calibri (Body);">
													<tbody>
														<tr style="border:1px solid #000;">
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Name</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Age</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Adult/Child</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Gender</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Seat No. </strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Concession</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Quota</strong></td>
														</tr>
														{{passanger_details}}
													</tbody>
												</table>
											</td>
										</tr>
										<tr>
											<td valign="top" align="left">&nbsp;</td>
										</tr>
										<tr>
											<td style="font-family:Calibri (Body);font-size:11px;line-height:25px; padding-left:7px;" valign="top" align="left"><strong>Fare Detail</strong></td>
										</tr>
										<tr>
											<td valign="top" align="right">
												<table width="98%" border="0" style="border-collapse:collapse; margin:6px !important;font-size:11px;font-family:Calibri (Body);">
													<tbody>
														<tr>
															<td width="30%" align="right" valign="top" style="border:1px solid #000; padding-left:7px; ">Fare :</td>
															<td width="20%" align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{total_ticket_amount}}</td>
															<td></td>
														</tr>
														<tr>
															<td align="right" valign="top" style="border:1px solid #000; padding-left:7px; ">Total Refund Amount :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{refund_amount}}</td>
															<td></td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
										<tr>
											<td align="left" valign="top">
												<table width="98%" border="0" style="font-size:11px;font-family:Calibri (Body); ">
													<tbody>
														<tr>
															<td colspan="5"><strong>Important Information:</strong></td>
														</tr>
														<tr>
															<td colspan="4">
																<ul>
																	<li>This E-Ticket is not Transferable and valid only for the details specified above.</li>
																	<li>Passenger is required to carry either printout of the e-tickets or SMS while travelling.</li>
																	<li>The On-Duty Conductor/Checking staff person authorized by RSRTC will verify the Photo Identity Card of the passenger regarding e-Ticket details on Print out/SMS & Reservation Chart. If the passenger fails to produce the Print Out/SMS of e-Ticket and the specified Photo Identity Proof in original during journey, the e-ticket will be treated as INVALID and the passenger will be treated as "Traveling without Ticket". Photocopy of Identity Proof is not allowed.</li>
																	<li>RSRTC reserves the rights to change/cancel the bus service mentioned in the e-ticket. </li>
																	<li>In case of change of service or revision of fare or due to technical error, passenger will have to pay the fare difference in cash.</li>
																	<li>Cancellation of E-Ticket is permitted under the terms and conditions agreed by the passenger while booking the e-ticket.</li>
																	<li>For Debit/Credit Cards additional surcharge is as per Payment Gateway policy of service offering agency.</li>
																	<li>If there is any query related with refund. Please send your mail to service offering website. E-Mail id :support@rokad.in</li>
																</ul>
																<center><strong>Wish You A Happy Journey</strong>
																<br clear="all"/>
																For more details visit us www.rokad.in and helpdesk email/Phone No.
																Send your support e-mail to: rsrtchelpdesk@gmail.com.</center>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
								<br />
								<table width="800" border="1" cellpadding="0" style="font-family:Calibri (Body); border-collapse:collapse; margin:0 auto; font-size:11px;">
									<tr>
										<td align="left" valign="top" style="line-height:35px; padding-left:7px;"><strong>Cancellation Policies</strong></td>
									</tr>
									<tr>
										<td align="left" valign="top">
											<table width="98%" border="0" style="border-collapse:collapse; margin:6px !important;font-size:11px;">
												<tr>
													<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;"><strong>Cancellation Time</strong></td>
													<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; "><strong>Cancellation Charges</strong></td>
												</tr>
												<tr>
													<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Cancellation upto 24 hrs</td>
													<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; ">5.00 %</td>
												</tr>
												<tr>
													<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Cancellation upto 1 hrs</td>
													<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; ">20.00 %</td>
												</tr>
												<tr>
													<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Cancellation upto 30 mins</td>
													<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; ">50.00 %</td>
												</tr>
												<tr>
													<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;" colspan="2">* <em>Reservation charges and government service tax charge (if any) are non refundable.</em></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>');



define('UPSRTC_TICKET_TEMPLATE_BC', '<table width="800" border="1" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; border-collapse:collapse; margin:0 auto;">
									<tbody>
										<tr>
											<td align="left" valign="top"><table width="100%" border="0">
													<tbody>
														<tr>
															<td align="right" valign="top"><img alt="UPSRTC Banner" src="https://www.upsrtconline.co.in/images/UPSRTC_logo.gif"></td>
															<td align="center" valign="top" style="vertical-align:middle; font-size:17px; font-weight:bold; line-height:24px;">Uttar Pradesh State Road Transport Corporation E-Ticket</td>
															<td align="right" valign="top" style="vertical-align:middle;"><a href="https://www.rokad.in" target="_blank"><img src="https://www.rokad.in/assets/travelo/front/images/logo_bos.png"  /></a></td>
														</tr>
													</tbody>
												</table></td>
										</tr>
										<tr style="line-height:20px; padding:0 10px;">
											<td align="left" valign="top"><table class="respond" width="98%" border="0" style="border:1px solid #000; border-collapse:collapse; margin:6px !important;">
													<tbody>
														<tr style="border:1px solid #000;">
															<td colspan="2" align="left" valign="top"  style="border:1px solid #000; padding-left:7px;">Up Trip Information</td>
															<td align="left" valign="top"  style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Bank Transaction No :</td>
															<td>{{bank_transaction_no}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Ticket No :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{ticekt_no}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">PNR No.</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{pnr_no}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Rokad Ref No:</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{bos_ref_no}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Service End Place :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{service_end_place}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Reservation From:</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{reservation_from}}({{res_frm_cd}})</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Reservation To:</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{reservation_to}}({{res_to_cd}})</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Boarding Point :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{boarding_pont}}({{boarding_cd}})</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Alighting Point :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{alighting_point}}({{alighting_cd}})</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Date of Journey(Origin) :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{doj}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Departure Time(Origin) :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{dpt_tm}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Boarding Time(Approx)</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{boarding_time}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Alighting Time(Approx)</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{alighting_time}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Bus Service Type :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{bus_service_type}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">No. of Seats :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{no_of_seats}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Email Id :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{psgr_email_id}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Contact No :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{psgr_contact_no}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Depot Name :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{depot_name}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Support Email Id :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">online.support@upsrtc.com</td>
														</tr>
													</tbody>
												</table></td>
										</tr>
										<tr>
											<td align="left" valign="top" style="line-height:25px; padding-left:7px;"><strong>Up Passengers Information</strong></td>
										</tr>
										<tr style="line-height:20px; padding:0 10px;">
											<td align="left" valign="top"><table class="respond" width="98%" border="0" style="border-collapse:collapse; margin:6px !important;">
													<tbody>
														<tr style="border:1px solid #000;">
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Name</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Age</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Adult/Child</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Gender</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Seat</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Quota</strong></td>
														</tr>
														{{psgr_list}}
													</tbody>
												</table>
											</td>
										</tr>
										<tr>
											<td valign="top" align="right"><table class="respond" align="right" width="40%" border="1" style="border-collapse:collapse; padding:10px; line-height:20px; margin:6px;">
													<tbody>
														<tr>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;" colspan="2"><strong>Total Fare Details </strong></td>
														</tr>
														<tr>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Basic Fare :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{basic_fare}}</td>
														</tr>
														<tr>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Reservation Charge :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{res_charges}}</td>
														</tr>
														<tr>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Pass. Amenities Sur :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{pas}}</td>
														</tr>
														<tr>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">ACC Sur :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{acc}}</td>
														</tr>
														<tr>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Sl.Charge :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{si}}</td>
														</tr>
														<tr>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">IT.Charge :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{it}}</td>
														</tr>
														<tr>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Toll Tax :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{tt}}</td>
														</tr>
														<tr>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Octroi :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{octroi}}</td>
														</tr>
														<tr>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">HR Charges :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{fare_hr}}</td>
														</tr>
														<tr>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Other Amt :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{other_amt}}</td>
														</tr>
														<tr>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Convey Charges :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{convey_charges}}</td>
														</tr>
														<tr>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Total Chargeable Amount :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{total_chargeable_amt}}</td>
														</tr>
													</tbody>
												</table></td>
										</tr>
																			
										<tr>
											<td align="left" valign="top"><table class="respond" width="98%" border="0">
													<tbody>
														<tr>
															<td colspan="5"><strong>Important</strong></td>
														</tr>
														<tr>
															<td colspan="4"><ul>
																	<li>This e-ticket is not transferable and valid only for the details specified above.</li>
																</ul></td>
														</tr>
													</tbody>
												</table></td>
										</tr>
									</tbody>
								</table>
								<br />
								<table class="respond" width="800" border="1" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; border-collapse:collapse; margin:0 auto;">
								<tr>
									<td align="left" valign="top" style="line-height:35px; padding-left:7px;"><strong>Cancellation Policies</strong></td>
								</tr>
								<tr>
									<td align="left" valign="top">
										<table class="respond" width="98%" border="0" style="border-collapse:collapse; margin:6px !important;">
										<tr>
											<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;"><strong>Cancellation Time</strong></td>
									 		<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; "><strong>Cancellation Charges</strong></td>
										</tr>
										<tr>
											<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Cancellation upto 24 hrs</td>
									 		<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; ">0.00 %</td>
										</tr>
										<tr>
											<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Cancellation upto 2 hrs</td>
									 		<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; ">10.00 %</td>
										</tr>
										<tr>
											<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Cancellation upto 1 hrs</td>
									 		<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; ">15.00 %</td>
										</tr>
										</table>
									</td>
								</tr>
								</table>');
define('UPSRTC_CANCEL_TICKET_TEMPLATE', '<table width="800" border="1" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; border-collapse:collapse; margin:0 auto;">
									<tbody>
										<tr>
											<td align="left" valign="top"><table width="100%" border="0">
													<tbody>
														<tr>
															<td align="right" valign="top"><img alt="UPSRTC Banner" src="https://www.upsrtconline.co.in/images/UPSRTC_logo.gif"></td>
															<td align="center" valign="top" style="vertical-align:middle; font-size:17px; font-weight:bold; line-height:24px;">Uttar Pradesh State Road Transport Corporation <br>E-Cancellation Ticket</br></td>
															<td align="right" valign="top" style="vertical-align:middle;"><a href="https://www.rokad.in" target="_blank"><img src="https://www.rokad.in/assets/travelo/front/images/logo_bos.png"  /></a></td>
														</tr>
													</tbody>
												</table></td>
										</tr>
										<tr style="line-height:20px; padding:0 10px;">
											<td align="left" valign="top"><table width="98%" border="0" style="border:1px solid #000; border-collapse:collapse; margin:6px !important;">
													<tbody>
														<tr style="border:1px solid #000;">
															<td colspan="2" align="left" valign="top"  style="border:1px solid #000; padding-left:7px;">Up Trip Information</td>
															<td align="left" valign="top"  style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Bank Transaction No :</td>
															<td>{{bank_transaction_no}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Ticket No :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{ticekt_no}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">PNR No.</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{pnr_no}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Rokad Ref No:</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{bos_ref_no}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Service End Place :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{service_end_place}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Reservation From:</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{reservation_from}}({{res_frm_cd}})</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Reservation To:</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{reservation_to}}({{res_to_cd}})</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Boarding Point :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{boarding_pont}}({{boarding_cd}})</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Alighting Point :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{alighting_point}}({{alighting_cd}})</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Date of Journey(Origin) :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{doj}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Departure Time(Origin) :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{dpt_tm}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Boarding Time(Approx)</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{boarding_time}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Alighting Time(Approx)</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{alighting_time}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Bus Service Type :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{bus_service_type}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">No. of Seats :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{no_of_seats}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Email Id :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{psgr_email_id}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Contact No :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{psgr_contact_no}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Depot Name :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{depot_name}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Support Email Id :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">online.support@upsrtc.com</td>
														</tr>
													</tbody>
												</table></td>
										</tr>
										<tr>
											<td align="left" valign="top" style="line-height:25px; padding-left:7px;"><strong>Up Passengers Information</strong></td>
										</tr>
										<tr style="line-height:20px; padding:0 10px;">
											<td align="left" valign="top"><table width="98%" border="0" style="border-collapse:collapse; margin:6px !important;">
													<tbody>
														<tr style="border:1px solid #000;">
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Name</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Age</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Adult/Child</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Gender</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Seat</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Quota</strong></td>
														</tr>
														{{psgr_list}}
													</tbody>
												</table>
											</td>
										</tr>
										<tr>
											<td valign="top" align="left">&nbsp;</td>
										</tr>
										<tr>
											<td style="font-family:Calibri (Body);font-size:11px;line-height:25px; padding-left:7px;" valign="top" align="left"><strong>Fare Detail</strong></td>
										</tr>
										<tr>
											<td valign="top" align="right">
												<table width="98%" border="0" style="border-collapse:collapse; margin:6px !important;font-size:11px;font-family:Calibri (Body);">
													<tbody>
														<tr>
															<td width="30%" align="right" valign="top" style="border:1px solid #000; padding-left:7px; ">Fare :</td>
															<td width="20%" align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{total_chargeable_amt}}</td>
															<td></td>
														</tr>
														<tr>
															<td align="right" valign="top" style="border:1px solid #000; padding-left:7px; ">Total Refund Amount :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{refund_amount}}</td>
															<td></td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
										
									</tbody>
								</table>
								<br />
								<table width="800" border="1" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; border-collapse:collapse; margin:0 auto;">
								<tr>
									<td align="left" valign="top" style="line-height:35px; padding-left:7px;"><strong>Cancellation Policies</strong></td>
								</tr>
								<tr>
									<td align="left" valign="top">
										<table width="98%" border="0" style="border-collapse:collapse; margin:6px !important;">
										<tr>
											<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;"><strong>Cancellation Time</strong></td>
									 		<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; "><strong>Cancellation Charges</strong></td>
										</tr>
										<tr>
											<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Cancellation upto 24 hrs</td>
									 		<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; ">0.00 %</td>
										</tr>
										<tr>
											<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Cancellation upto 2 hrs</td>
									 		<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; ">10.00 %</td>
										</tr>
										<tr>
											<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Cancellation upto 1 hrs</td>
									 		<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; ">15.00 %</td>
										</tr>
										</table>
									</td>
								</tr>
								</table>');

define('UPSRTC_TICKET_TEMPLATE', '<table width="800" border="1" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; border-collapse:collapse; margin:0 auto;">
									<tbody>
										<tr>
											<td align="left" valign="top"><table width="100%" border="0">
													<tbody>
														<tr>
															<td align="right" valign="top"><img alt="UPSRTC Banner" src="https://www.upsrtconline.co.in/images/UPSRTC_logo.gif"></td>
															<td align="center" valign="top" style="vertical-align:middle; font-size:17px; font-weight:bold; line-height:24px;">Uttar Pradesh State Road Transport Corporation E-Ticket</td>
															<td align="right" valign="top" style="vertical-align:middle;"><a href="https://www.rokad.in" target="_blank"><img src="https://www.rokad.in/assets/travelo/front/images/logo_bos.png"  /></a></td>
														</tr>
													</tbody>
												</table></td>
										</tr>
										<tr style="line-height:20px; padding:0 10px;">
											<td align="left" valign="top"><table width="98%" border="0" style="border:1px solid #000; border-collapse:collapse; margin:6px !important;">
													<tbody>
														<tr style="border:1px solid #000;">
															<td colspan="2" align="left" valign="top"  style="border:1px solid #000; padding-left:7px;">Up Trip Information</td>
															<td align="left" valign="top"  style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Bank Transaction No :</td>
															<td>{{bank_transaction_no}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Ticket No :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{ticekt_no}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">PNR No.</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{pnr_no}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">BOS Ref No:</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{bos_ref_no}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Service End Place :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{till_stop_name}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Reservation From:</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{boarding_stop_name}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Reservation To:</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{destination_stop_name}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Boarding Point :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{boarding_stop_name}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Alighting Point :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{destination_stop_name}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Date of Journey(Origin) :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{dept_time_date}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Departure Time(Origin) :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{dept_time_his}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Boarding Time(Approx)</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{dept_time_date}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Alighting Time(Approx)</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{dept_time_date}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Bus Service Type :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{bus_type}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">No. of Seats :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{no_of_seats}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Email Id :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{user_email}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Contact No :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{mobile_number}}</td>
														</tr>
														<tr style="border:1px solid #000;">
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Depot Name :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{boarding_stop_name}}</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Support Email Id :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">online.support@upsrtc.com</td>
														</tr>
													</tbody>
												</table></td>
										</tr>
										<tr>
											<td align="left" valign="top" style="line-height:25px; padding-left:7px;"><strong>Up Passengers Information</strong></td>
										</tr>
										<tr style="line-height:20px; padding:0 10px;">
											<td align="left" valign="top"><table width="98%" border="0" style="border-collapse:collapse; margin:6px !important;">
													<tbody>
														<tr style="border:1px solid #000;">
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Name</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Age</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Adult/Child</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Gender</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Seat</strong></td>
															<td align="center" valign="top" style="border:1px solid #000; padding-left:7px;"><strong>Quota</strong></td>
														</tr>
														{{passanger_details}}
													</tbody>
												</table>
											</td>
										</tr>
										<tr>
											<td valign="top" align="right"><table align="right" width="40%" border="1" style="border-collapse:collapse; padding:10px; line-height:20px; margin:6px;">
													<tbody>
														<tr>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;" colspan="2"><strong>Total Fare Details </strong></td>
														</tr>
														<tr>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Basic Fare :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{tot_fare_amt}}</td>
														</tr>
														
														
														<tr>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Gst Amount :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{gst_charges}}</td>
														</tr>
														<tr>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Other Charges :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{other_charges}}</td>
														</tr>
													
														
														<tr>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Total Chargeable Amount :</td>
															<td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">{{tot_fare_amt_with_tax}}</td>
														</tr>
													</tbody>
												</table></td>
										</tr>
										<tr>
											<td align="left" valign="top"><table width="98%" border="0">
													<tbody>
														<tr>
															<td colspan="5"><strong>Important</strong></td>
														</tr>
														<tr>
															<td colspan="4"><ul>
																	<li>This e-ticket is not transferable and valid only for the details specified above.</li>
																</ul></td>
														</tr>
													</tbody>
												</table></td>
										</tr>
									</tbody>
								</table>
								<br />
								<table width="800" border="1" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif; border-collapse:collapse; margin:0 auto;">
								<tr>
									<td align="left" valign="top" style="line-height:35px; padding-left:7px;"><strong>Cancellation Policies</strong></td>
								</tr>
								<tr>
									<td align="left" valign="top">
										<table width="98%" border="0" style="border-collapse:collapse; margin:6px !important;">
										<tr>
											<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;"><strong>Cancellation Time</strong></td>
									 		<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; "><strong>Cancellation Charges</strong></td>
										</tr>
										<tr>
											<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Cancellation upto 24 hrs</td>
									 		<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; ">0.00 %</td>
										</tr>
										<tr>
											<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Cancellation upto 2 hrs</td>
									 		<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; ">10.00 %</td>
										</tr>
										<tr>
											<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">Cancellation upto 1 hrs</td>
									 		<td align="left" valign="top" style="border:1px solid #000; padding-left:7px; ">15.00 %</td>
										</tr>
										</table>
									</td>
								</tr>
								</table>');

define('TRAVELYAARI_BOS_TICKET_TEMPLATE', '<table style="font-size:13px;font-family: \'Arial\',sans-serif; 	table-layout: fixed; line-height: 20px;color:#000;border-collapse: collapse;" align="center" width="800px" bordercolor="#cccccc" border="1px" cellpadding="2" cellspacing="2">
									<tr valign="top">
										<td valign="top" colspan="4">
											<table class="respond" style="font-size:14px;font-family: \'Arial\',sans-serif; color:#000;" align="center" width="100%" cellpadding="0" cellspacing="0">
												<tr>
													<td style="widht:33%" colspan="2" rowspan="2" align="left" valign="top"><a href="https://bookonspot.com" target="_blank">
								                	<img src="https://www.rokad.in/assets/travelo/front/images/logo_bos.png" width="165" height="90" /> <br>support@bookonspot.com
								                </td></a></td>
												
													<td colspan="2" align="right" valign="top" style="color:#000;font-weight:bold; font-size:13px; text-align:centre;">Booking</br>Date: {{booked_date}}</td>
												</tr>
												{{agent_details}}
											</table>
										</td>
									</tr>
									<tr valign="middle" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 20px;">
										<td valign="middle"><span style="color:#ff940a;font-weight:bold; font-size:13px;">Journey:</span> </td>
										<td valign="middle">{{from_to_journey}}</td>
										<td valign="middle"><span style="color:#ff940a;font-weight:bold; font-size:13px;">Date of Journey: </span> </td>
										<td> {{journey_date}} </td>
									</tr>
									<tr valign="top">
										<td  style="padding:7px 0;" colspan="4" bgcolor="#efefef"><strong>Trip Information:</strong> </td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Travels:</span> </td>
										<td>{{op_name}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Bus Type:</span> </td>
										<td>{{bus_types}}</td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height:25px;">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">PNR No.:</span> </td>
										<td>{{pnr_no}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Total Seat(s):</span> </td>
										<td>{{seat_no}}</td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">BOS Ref No.:</span> </td>
										<td>{{bos_ref_no}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Total Fare:</span> </td>
										<td>{{tot_fare_amt_with_tax}}</td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										<td style="padding:7px 0;" colspan="4" bgcolor="#efefef"><strong>Boarding Information:</strong> </td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Boarding Point:</span> </td>
										<td>{{from_stop_name}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Dropping Point:</span> </td>
										<td>{{till_stop_name}}</td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Departure Time:</span> </td>
										<td>{{dept_time_date}} {{dept_time_his}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Alighting Time:</span> </td>
										<td>{{alighting_time}}</td>
									</tr>



									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Boarding Point Contact Number:</span> </td>
										<td>{{boarding_point_contact_no}}</td>

										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Boarding Point Address:</span> </td>
										<td>{{boarding_point_address}}</td>

									</tr>

									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Boarding Point Landmark:</span> </td>
										<td>{{boarding_point_landmark}}</td>


									</tr>



									<tr valign="top">
										<td style="padding:7px 0;" colspan="4" bgcolor="#efefef"><strong>Passenger Information:</strong> </td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 40px">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Email Id:</span> </td>
										<td>{{user_email}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Contact No.:</span> </td>
										<td>{{mobile_number}}</td>
									</tr>
									<tr valign="top"></tr>
									<tr valign="top">
										<td colspan="4"><table class="respond" style="font-size:14px;font-family: \'Arial\',sans-serif; color:#000;" align="center" width="100%" bordercolor="#cccccc" border="1px" cellpadding="0" cellspacing="0">
												<tr >
													<th style="padding:7px;text-align:center;" > <span style="color:#000;font-weight:bold; font-size:13px;">Passenger Name</span> </th>
													<th style="padding:7px 0;text-align:center;"> <span style="color:#000;font-weight:bold; font-size:13px;">Age</span> </th>
													<th style="padding:7px 0;text-align:center;"> <span style="color:#000;font-weight:bold; font-size:13px;">Gender</span> </th>
													<th style="padding:7px 0;text-align:center;"> <span style="color:#000;font-weight:bold; font-size:13px;">Seat No.</span> </th>
												</tr>
												{{passanger_details}}
											</table>
											</td>
									</tr>
								</table>
								<table class="respond" style="font-size:13px;font-family: \'Arial\',sans-serif; text-align:justify; line-height: 20px;color:#000;border-collapse: collapse;" align="center" width="800px" bordercolor="#cccccc" border="0px" cellpadding="0" cellspacing="0">
									<tr>
										<td><strong></br>
											Terms & Conditions: </strong> </td>
									</tr>
									<tr>
										<td> Rokad acts as an “Intermediary” solely to assist customers in gathering travel information, determining the availability of travel related products and services, making legitimate reservations or otherwise transacting business with travel operators, and for facilitating travel requirements. You acknowledge that BoS merely provides intermediary services in order to facilitate these services. </td>
									</tr>
									<tr>
										<td><strong>Communication Policy :</strong> </td>
									</tr>
									<tr>
										<td>Upon transacting on the BoS website, you will receive an e-mail from BoS informing the status of your transaction on your registered email ID or the email ID provided at the time of booking. You will also receive a SMS on your registered mobile number provided at the time of booking. The responsibility of entering your accurate contact details like your name, email ID and mobile number is solely yours. BoS is not responsible for providing any information to you regarding any change in bus schedules, cancellation of service, status of bus service, etc.</td>
									</tr>
									<tr>
										<td><strong>Refunds:</strong> </td>
									</tr>
									<tr>
										<td> In case of cancellation, due refund amount will be paid according to the bus operator\'s cancellation policy. After scheduled departure of bus service cancellation/ Refund request cannot be accepted. Refund process may take duration of 7-8 working days for online payments/net banking etc. In case of service cancellation from operator, you will receive full refund except the payment gateway charges and any other non refundable charges. Basic fare is refundable as per cancellation policy and it varies from operator to operator. Service tax/Reservation charges or any other operational charges are non refundable. </td>
									</tr>
									<tr>
										<td><strong>Grievance Policy:</strong> </td>
									</tr>
									<tr>
										<td> You can file a grievance/share feedback if you are disappointed with the services rendered by BoS or any other issues. You can give your grievance/feedback through email by emailing us at <a href="mailto:support@bookonspot.com">support@bookonspot.com</a> or by calling us on <strong>022-66813930, +917710805805</strong> </td>
									</tr>
									<tr>
										<td><strong>Other Points:</strong> </td>
									</tr>
									<tr>
										<td> BoS shall not be responsible or held liable for the behavior of operational staff in bus. BoS shall not be responsible or held liable if the bus does not stop at a pick up or drop point. BoS shall not be responsible or held liable if passenger reaches boarding point after the Scheduled departure time. If there are any on board charges from bus operator, then BoS is not responsible for it. BoS shall not be responsible or held liable for any bus delay due to breakdown or operational issues/road blockage/traffic jam/agitation etc. BoS shall not be held liable for accident claims. BoS shall not be responsible or held liable for criminal/civil offense by the operator.</td>
									</tr>
									<tr>
										<td><strong>Cancellation Process:</strong> </td>
									</tr>
									<tr>
										<td> You can cancel your ticket online by logging in to <a href ="https://bookonspot.com" target="blank"> https://bookonspot.com</a> Cancellation charges of the tickets may vary as per the policies of the operators. Cancellations are allowed till scheduled departure of service and as per policy of operator.</td>
									</tr>
									<tr>
										<td><strong>Cancellation policy:</strong> </td>
									</tr>													
									<tr>									
									<table class="respond" style="font-size:13px;font-family: \'Arial\,sans-serif; 	  line-height: 20px;color:#000;border-collapse: collapse;" align="center" width="800px" bordercolor="#cccccc" border="0px" cellpadding="10" cellspacing="10">					
									 <tr> <table class="respond" style="font-size:13px;font-family: "Arial\,sans-serif; line-height: 20px;color:#000;border-collapse: collapse;" align="center" width="800px" bordercolor="#cccccc" border="0px" cellpadding="10" cellspacing="10"> <tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;"> <td style="padding:3px;"><u>Cancellation Time(Before Scheduled Departure)</u></td> <td style="padding:3px;"><u>Cancellation Charges(In %)</u></td> </tr> <tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;"> <td style="padding:3px;">Cancellation within 0 hrs</td> <td style="padding:3px;">100%</td> </tr><tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;"> <td style="padding:3px;">Cancellation within 10 hrs</td> <td style="padding:3px;">40%</td> </tr><tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;"> <td style="padding:3px;">Cancellation within 15 hrs</td> <td style="padding:3px;">20%</td> </tr><tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;"> <td style="padding:3px;">Cancellation within 24 hrs</td> <td style="padding:3px;">10%</td> </tr>
										<tr>
											<td> For all queries call us on <strong>022-66813930, +917710805805 </strong> or email us at <a href="mailto:support@bookonspot.com">support@bookonspot.com</a> </td>
										</tr>
									</table>
									</tr>
								</table>'
    					);


define('ETS_BOS_TICKET_TEMPLATE', '<table style="font-size:13px;font-family: \'Arial\',sans-serif; 	table-layout: fixed; line-height: 20px;color:#000;border-collapse: collapse;" align="center" width="800px" bordercolor="#cccccc" border="1px" cellpadding="2" cellspacing="2">
									<tr valign="top">
										<td valign="top" colspan="4">
											<table class="respond" style="font-size:14px;font-family: \'Arial\',sans-serif; color:#000;" align="center" width="100%" cellpadding="0" cellspacing="0">
												<tr>
													<td style="widht:33%" colspan="2" rowspan="2" align="left" valign="top"><a href="https://bookonspot.com" target="_blank">
								                	<img src="https://www.rokad.in/assets/travelo/front/images/logo_bos.png"  /> <br>https://bookonspot.com
								                </td></a></td>
												
													<td colspan="2" align="right" valign="top" style="color:#000;font-weight:bold; font-size:13px; text-align:centre;">Booking</br>Date: {{booked_date}}</td>
												</tr>
												{{agent_details}}
											</table>
										</td>
									</tr>
									<tr valign="middle" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 20px;">
										<td valign="middle"><span style="color:#ff940a;font-weight:bold; font-size:13px;">Journey:</span> </td>
										<td valign="middle">{{from_to_journey}}</td>
										<td valign="middle"><span style="color:#ff940a;font-weight:bold; font-size:13px;">Date of Journey: </span> </td>
										<td> {{journey_date}} </td>
									</tr>
									<tr valign="top">
										<td  style="padding:7px 0;" colspan="4" bgcolor="#efefef"><strong>Trip Information:</strong> </td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Travels:</span> </td>
										<td>{{op_name}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Bus Type:</span> </td>
										<td>{{bus_types}}</td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height:25px;">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">PNR No.:</span> </td>
										<td>{{pnr_no}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Total Seat(s):</span> </td>
										<td>{{seat_no}}</td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">BOS Ref No.:</span> </td>
										<td>{{bos_ref_no}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Total Fare:</span> </td>
										<td>{{tot_fare_amt_with_tax}}</td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										<td style="padding:7px 0;" colspan="4" bgcolor="#efefef"><strong>Boarding Information:</strong> </td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Boarding Point:</span> </td>
										<td>{{from_stop_name}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Dropping Point:</span> </td>
										<td>{{till_stop_name}}</td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Departure Time:</span> </td>
										<td>{{dept_time_date}} {{dept_time_his}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Alighting Time:</span> </td>
										<td>{{alighting_time}}</td>
									</tr>



									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Boarding Point Contact Number:</span> </td>
										<td>{{boarding_point_contact_no}}</td>

										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Boarding Point Address:</span> </td>
										<td>{{boarding_point_address}}</td>

									</tr>

									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 25px;">
										
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Boarding Point Landmark:</span> </td>
										<td>{{boarding_point_landmark}}</td>


									</tr>



									<tr valign="top">
										<td style="padding:7px 0;" colspan="4" bgcolor="#efefef"><strong>Passenger Information:</strong> </td>
									</tr>
									<tr valign="top" style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;line-height: 40px">
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;">Email Id:</span> </td>
										<td>{{user_email}}</td>
										<td><span style="color:#ff940a;font-weight:bold; font-size:13px;"> Contact No.:</span> </td>
										<td>{{mobile_number}}</td>
									</tr>
									<tr valign="top"></tr>
									<tr valign="top">
										<td colspan="4"><table class="respond" style="font-size:14px;font-family: \'Arial\',sans-serif; color:#000;" align="center" width="100%" bordercolor="#cccccc" border="1px" cellpadding="0" cellspacing="0">
												<tr >
													<th style="padding:7px;text-align:center;" > <span style="color:#000;font-weight:bold; font-size:13px;">Passenger Name</span> </th>
													<th style="padding:7px 0;text-align:center;"> <span style="color:#000;font-weight:bold; font-size:13px;">Age</span> </th>
													<th style="padding:7px 0;text-align:center;"> <span style="color:#000;font-weight:bold; font-size:13px;">Gender</span> </th>
													<th style="padding:7px 0;text-align:center;"> <span style="color:#000;font-weight:bold; font-size:13px;">Seat No.</span> </th>
												</tr>
												{{passanger_details}}
											</table>
											</td>
									</tr>
								</table>
								<table class="respond" style="font-size:13px;font-family: \'Arial\',sans-serif; text-align:justify; line-height: 20px;color:#000;border-collapse: collapse;" align="center" width="800px" bordercolor="#cccccc" border="0px" cellpadding="0" cellspacing="0">
									<tr>
										<td><strong></br>
											Terms & Conditions: </strong> </td>
									</tr>
									<tr>
										<td> Rokad acts as an “Intermediary” solely to assist customers in gathering travel information, determining the availability of travel related products and services, making legitimate reservations or otherwise transacting business with travel operators, and for facilitating travel requirements. You acknowledge that BoS merely provides intermediary services in order to facilitate these services. </td>
									</tr>
									<tr>
										<td><strong>Communication Policy :</strong> </td>
									</tr>
									<tr>
										<td>Upon transacting on the BoS website, you will receive an e-mail from BoS informing the status of your transaction on your registered email ID or the email ID provided at the time of booking. You will also receive a SMS on your registered mobile number provided at the time of booking. The responsibility of entering your accurate contact details like your name, email ID and mobile number is solely yours. BoS is not responsible for providing any information to you regarding any change in bus schedules, cancellation of service, status of bus service, etc.</td>
									</tr>
									<tr>
										<td><strong>Refunds:</strong> </td>
									</tr>
									<tr>
										<td> In case of cancellation, due refund amount will be paid according to the bus operator\'s cancellation policy. After scheduled departure of bus service cancellation/ Refund request cannot be accepted. Refund process may take duration of 7-8 working days for online payments/net banking etc. In case of service cancellation from operator, you will receive full refund except the payment gateway charges and any other non refundable charges. Basic fare is refundable as per cancellation policy and it varies from operator to operator. Service tax/Reservation charges or any other operational charges are non refundable. </td>
									</tr>
									<tr>
										<td><strong>Grievance Policy:</strong> </td>
									</tr>
									<tr>
										<td> You can file a grievance/share feedback if you are disappointed with the services rendered by BoS or any other issues. You can give your grievance/feedback through email by emailing us at <a href="mailto:support@bookonspot.com">support@bookonspot.com</a> or by calling us on <strong>022-66813930, +917710805805</strong> </td>
									</tr>
									<tr>
										<td><strong>Other Points:</strong> </td>
									</tr>
									<tr>
										<td> BoS shall not be responsible or held liable for the behavior of operational staff in bus. BoS shall not be responsible or held liable if the bus does not stop at a pick up or drop point. BoS shall not be responsible or held liable if passenger reaches boarding point after the Scheduled departure time. If there are any on board charges from bus operator, then BoS is not responsible for it. BoS shall not be responsible or held liable for any bus delay due to breakdown or operational issues/road blockage/traffic jam/agitation etc. BoS shall not be held liable for accident claims. BoS shall not be responsible or held liable for criminal/civil offense by the operator.</td>
									</tr>
									<tr>
										<td><strong>Cancellation Process:</strong> </td>
									</tr>
									<tr>
										<td> You can cancel your ticket online by logging in to <a href ="https://bookonspot.com" target="blank"> https://bookonspot.com</a> Cancellation charges of the tickets may vary as per the policies of the operators. Cancellations are allowed till scheduled departure of service and as per policy of operator.</td>
									</tr>
									<tr>
										<td><strong>Cancellation policy:</strong> </td>
									</tr>													
									<tr>									
									<table class="respond" style="font-size:13px;font-family: \'Arial\,sans-serif; 	  line-height: 20px;color:#000;border-collapse: collapse;" align="center" width="800px" bordercolor="#cccccc" border="0px" cellpadding="10" cellspacing="10">					
									 <tr> <table class="respond" style="font-size:13px;font-family: "Arial\,sans-serif; line-height: 20px;color:#000;border-collapse: collapse;" align="center" width="800px" bordercolor="#cccccc" border="0px" cellpadding="10" cellspacing="10"> <tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;"> <td style="padding:3px;"><u>Cancellation Time(Before Scheduled Departure)</u></td> <td style="padding:3px;"><u>Cancellation Charges(In %)</u></td> </tr> <tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;"> <td style="padding:3px;">Cancellation within 0 hrs</td> <td style="padding:3px;">100%</td> </tr><tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;"> <td style="padding:3px;">Cancellation within 10 hrs</td> <td style="padding:3px;">40%</td> </tr><tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;"> <td style="padding:3px;">Cancellation within 15 hrs</td> <td style="padding:3px;">20%</td> </tr><tr style="border-bottom:1pt solid #d8d8d8; border-top:1px solid #d8d8d8;"> <td style="padding:3px;">Cancellation within 24 hrs</td> <td style="padding:3px;">10%</td> </tr>
										<tr>
											<td> For all queries call us on <strong>022-66813930, +917710805805 </strong> or email us at <a href="mailto:support@bookonspot.com">support@bookonspot.com</a> </td>
										</tr>
									</table>
									</tr>
								</table>'
    					);
