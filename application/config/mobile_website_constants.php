<?php

define('FRONT_LAYOUTS', FRONT_THEME . '/_layouts/mobile_front/');
define('FRONT_VIEW', FRONT_THEME . '/mobile_front/');
define('COMMON_ASSETS', ASSETS . 'common/');
define('FRONT_ASSETS', ASSETS . FRONT_THEME . '/' . 'mobile_front/');

define('FRONT_IMAGES', FRONT_ASSETS . 'images/');
define('HOMEPAGE_IMAGES', FRONT_IMAGES . 'homepage/');

//===============================  FRONT VIEW constant  ================================

define('COMING_SOON',           FRONT_VIEW . 'coming_soon');
define('CONTACT_US',            FRONT_VIEW . 'contact_us');
define('SERVICES',              FRONT_VIEW . 'services');
define('ABOUT_US',              FRONT_VIEW . 'about_us');
define('FEEDBACK',              FRONT_VIEW . 'feedback');
define('PGPROCESSING_VIEW', FRONT_VIEW . 'pgprocessing');
define('AGENT_PGPROCESSING_VIEW', FRONT_VIEW . 'agent_pgprocessing');
define('FAQ',                   FRONT_VIEW . 'faq');
define('FAQ_STATIC',                   FRONT_VIEW . 'faq_static');
define('TESTIMONIALS',          FRONT_VIEW . 'testimonials');
define('PRIVACY_POLICY',        FRONT_VIEW . 'privacy_policy');
define('PRIVACY_POLICY_STATIC',        FRONT_VIEW . 'privacy_policy_static');
define('TERMS_AND_CONDITION',   FRONT_VIEW . 'terms_and_condition');
define('TERMS_AND_CONDITION_STATIC',   FRONT_VIEW . 'terms_and_condition_static');

define('HOME_VIEW', FRONT_VIEW . 'home_view');
define('HOME_VIEW1', FRONT_VIEW . 'home_view1');
define('LOGIN_VIEW', FRONT_VIEW . '/login');
define('SIGNUP_VIEW', FRONT_VIEW . '/signup');

define('AGENT_REGISTRATION_VIEW', FRONT_VIEW. '/agent_registration_view');
define('USER_REGISTRATION', FRONT_VIEW . '/user_registration');
define('MAINTENANCE_VIEW', FRONT_VIEW . '/maintenance_view');
define('CUSTOM_ERROR', FRONT_VIEW . '/custom_error');
define('FORGOT_PASSWORD_VIEW', FRONT_VIEW . '/forgot_password_view');
define('AGENT_SIGNUP_VIEW', FRONT_VIEW . '/agent_signup');

define('TEST_LOAD_MODAL',       FRONT_VIEW . 'test_load_modal');



define('REGISTER_VIEW', FRONT_VIEW . 'register');
define('BOOKING_VIEW', FRONT_VIEW . 'booking_view');

define('CONFIRM_BOOKING_VIEW', FRONT_VIEW . 'confirm_booking');

define('PASSENGER_INFORMATION_VIEW', FRONT_VIEW . 'passenger_information_view');
define('SEAT_UNAVAILABLE', FRONT_VIEW . 'seat_unavailable');
define('UNEXPECTED_ERROR', FRONT_VIEW . 'unexpected_error');
define('INVALID_KEY', FRONT_VIEW . 'invalid_key');
define('PAYEMENT_CONFIRM_BOOKING_VIEW', FRONT_VIEW . 'payment_confirm_booking');//old
define('PAYEMENT_GATEWAY_CONFIRM_BOOKING_VIEW', FRONT_VIEW . 'payement_gateway_confirm_booking');
define('FINAL_BOOKING_VIEW', FRONT_VIEW . 'final_booking');
define('BOOKING_HISTORY_VIEW', FRONT_VIEW . 'booking_history_view');
define('CANCEL_BOOKING_VIEW', FRONT_VIEW . 'cancel_booking');
define('REFUND_VIEW', FRONT_VIEW . 'refund_view'); 
define('INCENTIVE_EARNED', FRONT_VIEW . 'emp_incentive_earn');
define('CANCEL_BOOKING_AJAX', FRONT_VIEW . 'cancel_booking_ajax');
define('PASSENGER_BOOKING_INFO', FRONT_VIEW . 'pas_detail_view');
define('UNAVAILABLE', FRONT_VIEW . 'unavailable');
define('EMAIL_VERIFY_SUCCESS', FRONT_VIEW . 'email_verify_success');
define('PRINT_SMS_TICKET', FRONT_VIEW . 'print_sms_ticket');
define('PAYMENT_FAILED', FRONT_VIEW . 'payment_failed');

define('HOME_SEARCH_VIEW', FRONT_VIEW . 'home_search');
define('HOME_SEARCH_ANGULAR_VIEW', FRONT_VIEW . 'home_search_angular');
define('HOME_SEARCH_VIEW_NEW', FRONT_VIEW . 'home_search_new');
define('HS_BUS_DETAIL_AJAX', FRONT_VIEW . 'hs_bus_detail_ajax');
define('HS_BUS_SEAT_DETAIL_AJAX', FRONT_VIEW . 'hs_bus_seat_detail_ajax');
define('HOME_SEARCH_TPL', FRONT_VIEW . 'home_search_tpl');
define('HOME_SEARCH_NEW_TPL', FRONT_VIEW . 'home_search_new_tpl');
define('AJ_VIEW', FRONT_VIEW . 'aj/');
define('AJ_INDEX_VIEW', AJ_VIEW . 'index');
define('AJ_HOME_VIEW', AJ_VIEW . 'home');
define('AJ_BUS_SERVICES', AJ_VIEW . 'bus_services');
define('AJ_BUS_DETAIL_AJAX', AJ_VIEW . 'bus_detail_ajax');
define('EMITRA_PAYEMENT_GATEWAY_CONFIRM_BOOKING_VIEW', FRONT_VIEW . 'emitra_payement_gateway_confirm_booking');

define('FRONT_USER_PROFILE', FRONT_VIEW . 'userprofile');
define('THANK_YOU', FRONT_VIEW . 'thank_you');
define('EMAIL_VERIFY', FRONT_VIEW . 'email_verify');

//CCA PG
define('CCA_PROCESSING_PAGE',                   FRONT_VIEW.'payment_gateway_processing');
define('PAYMENT_SUCCESS_PAGE',                   FRONT_VIEW.'payment_success_processing');
define('PAYMENT_TRANSACTION_FAILED',    FRONT_VIEW.'payment_transaction_failed');
define('CSC_PROCESSING_PAGE',                   FRONT_VIEW.'csc_payment_gateway_processing');

define('PG_TRANSACTION_FAILED_MAIL_TEMPLATE',   'pg_failed');

define('CSC_PROCESSING_PAGE',                   FRONT_VIEW.'csc_payment_gateway_processing');
define('AGENT_SEARCH_PAGE', FRONT_VIEW . 'agent_search_page_new');
?>