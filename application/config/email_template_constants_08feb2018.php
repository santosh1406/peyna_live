<?php

define('PG_FAILED_TEMPLATE', "<p>Transaction <b>{{pg_failed_reason}}</b> for <b>ticket id {{ticket_id}}</b>.</p>
							  <p>The failure could be due to payment gateway failure response,api failure etc. In order to prevent transaction failure, please check log file generated as soon as possible.</p>
							  <p>This mail is from {{site_url}}.</p>

								<p>Regards,</p>
								<p>Team Rokad</p>");


define('PG_SUCCESS_TICKET_FAILED_TEMPLATE', "<p>Payment done successfully but ticket booking failed.</b>.</p>
											 <p><b>Transaction Details are as below:</b></p>
											 <p>Ticket No:{{ticket_id}}</p>
											 <p>Ticket Amount:{{ticket_amount}}</p>
											 <p>CCAvenue Charges:{{ccavenue_charges}}</p>
											 <p>Total Amount:{{total_ticket_amount}}</p>
											 <p>CCAvenue Reference Number:{{pg_tracking_id}}</p>
											 <br/>
											 <p><b>Customer Details:</b></p>
											 <p>Name:{{customer_name}}</p>
											 <p>Email:{{customer_email}}</p>
											 <p>Phone:{{customer_mobile_number}}</p>
											 <p>Journey From:{{journey_from_to}}</p>
											 <p>Journey Date:{{journey_date}}</p>
											 <p>Booking Date:{{booking_date}}</p>
											 
											 <p><b>Booking Failed Reason:</b>{{failed_reason}}</p>

											 <p>Please check details if ticket is not booked then process to refund <b>Rs.{{ticket_refund_amount}}.</b></p>

											 <p>This mail is from {{site_url}}.</p>
											 
											 <p><b>NOTE:{{extra_note}}</b></p>

											 <p>Regards,</p>
											 <p>Team Rokad</p>");


define('UNSUCCESSFUL_TICKET_BOOKED_REFUND', "<p>Payment done successfully but ticket booking failed.</b>.</p>
											 <p><b>Transaction Details are as below:</b></p>
											 <p>Ticket No:{{ticket_id}}</p>
											 <p>Ticket Amount:{{ticket_amount}}</p>
                       <p>PG Reference Number (If booked from PG):{{pg_tracking_id}}</p>
											 <p>Wallet Id (If booked from Wallet):{{wallet_id}}</p>
											 <p>Journey From:{{journey_from_to}}</p>
											 <p>Journey Date:{{journey_date}}</p>
											 <p>Booking Date:{{booking_date}}</p>
											 <p><b>Ticket Booked Status:</b>{{transaction_status}}</p>

											 <p>This mail is from {{site_url}}.</p>
											 
											 <p>Regards,</p>
											 <p>Team Rokad</p>");


define('DEVELOPER_CUSTOM_ERROR_MESSAGE', "<p>Very important notice. Please track this.</b></p>
										  <p>{{custom_error_message}}</p>

										 <p>Regards,</p>
										 <p>Team Rokad</p>");

define('WALLET_TOPUP_REQUEST','<table width="100%" border="1" style="border-collapse: collapse; border:1px solid #000; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:30px;">
                                <tr>
                                <td>
                                <table width="95%" border="0" style="margin:0 auto;">
                                <tr>
                                <td><img alt="Rokad" src="https://www.rokad.in/assets/travelo/front/images/logo.png" style="width:104px;"></td>
                                </tr>
                                <tr></tr>
                                <tr style="line-height:40px;">
                                <td style="text-align:center;">
                                <span style="color:#ff940a; font-weight:bold; font-size:12px;"></span>
                                <span style="font-weight:bold; font-size:12px;">Wallet Topup Request<br></span>
                                </td>
                                </tr>
                                </table>
                                </td>
                                </tr>
                                <tr>
                                <td><table width="95%" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:20px; margin:0 auto;"><tr>
                                <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">TopUp By</span><br />{{topup_by}}<br />
                                <br /><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Amount</span><br />{{amount}}</td>
                                <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">User Email</span><br />{{useremail}}<br />
                                <br /><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Name</span><br />{{username}}</td>  
                                </tr>
                                <tr></tr>
                                <tr style="border-top:1px solid #000;">
                                <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Bank Name</span><br />{{bank_name}}<br /><br />
                                <span style="color:#ff940a; text-decoration:underline;line-height:27px;">Bank Account No</span><br />{{bank_acc_no}}
                                </td>
                                <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Transaction No</span><br />{{transaction_no}}<br />
                                <br /><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Transaction Remark</span><br />{{transaction_remark}}</td>
                                </tr>
                                <tr></tr>
                                <tr><td valign="top" style="padding-top:10px;"><p><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Transaction Date</span><br />
                                {{transaction_date}}</p></td>
                                <td valign="top">
                                <p><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Approval</span><br />{{is_approvel}}</p></td>
                                </tr>
                                </table>
                                </td>
                                </tr>
                                </table><br /><br />
                                <p>Regards,</p><p>Team Rokad</p>');

define('WALLET_TOPUP_REQUEST_REJECTED','<table width="100%" border="1" style="border-collapse: collapse; border:1px solid #000; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:30px;">
                                        <tr><td><table width="95%" border="0" style="margin:0 auto;">
                                        <tr><td><img alt="Rokad" src="https://www.rokad.in/assets/travelo/front/images/logo.png" style="width:104px;"></td>
                                        </tr><tr></tr>
                                        <tr style="line-height:40px;"><td style="text-align:center;">
                                        <span style="color:#ff940a; font-weight:bold; font-size:12px;"></span>
                                        <span style="font-weight:bold; font-size:12px;">Wallet Topup Request<br></span>
                                        </td></tr></table></td></tr><tr>
                                        <td><table width="95%" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:20px; margin:0 auto;"><tr>
                                        <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">TopUp By</span><br />{{topup_by}}<br />
                                        <br /><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Amount</span><br />{{amount}}</td>
                                        <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">User Email</span><br />{{useremail}}<br />
                                        <br /><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Name</span><br />{{username}}</td>  
                                        </tr>
                                        <tr></tr>
                                        <tr style="border-top:1px solid #000;">
                                        <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Bank Name</span><br />{{bank_name}}<br /><br />
                                        <span style="color:#ff940a; text-decoration:underline;line-height:27px;">Bank Account No</span><br />{{bank_acc_no}}
                                        </td>
                                        <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Transaction No</span><br />{{transaction_no}}<br />
                                        <br /><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Transaction Remark</span><br />{{transaction_remark}}</td>
                                        </tr>
                                        <tr></tr>
                                        <tr><td valign="top" style="padding-top:10px;"><p><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Transaction Date</span><br />
                                        {{transaction_date}}</p></td>
                                        <td valign="top">
                                        <p><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Approval</span><br />{{is_approvel}}</p></td>
                                        </tr>
                                        <tr><td valign="top" style="padding-top:10px;"><p><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Rejection Reason</span><br />
                                        {{reject_reason}}</p></td>
                                        </tr>
                                        </table>
                                        </td>
                                        </tr>
                                        </table><br /><br />
                                        <p>Regards,</p><p>Team Rokad</p>');

define('AGENT_BALANCE_ALERT','<p>Dear <b>{{username}}</b>,</p>
							  <p>Your Wallet Balance is Below Rs 2000.</p>
                              <p>Your Current Balance is Rs {{amount}}.</p>
                              <p>Kindly Recharge your account to book tickets without any difficulties.</p>
                              









                              <p>Regards,</p>
							  <p>Team Rokad</p>');

define('ADMIN_AGENT_BALANCE_LOW','<p>Dear <b>Admin</b>,</p>
							      <p>Below is the list of Agent whose Balance is less than Rs 2000.</p>
                              
                                <table width="100%" border="1" style="border-collapse: collapse; border:1px solid #000; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:30px;">
                                <tr>
                                   <th>Name</th>
                                   <th>Email</th>
                                   <th>Current Balance</th>
                                </tr>
                                <tr>
                                   <td>{{username}}</td>
                                   <td>{{useremail}}</td>
                                   <td>{{amount}}</td>
                                 </tr>
                                </table><br><br><br><br>
                                







                              <p>Regards,</p>
							  <p>Team Rokad</p>');

define('OPERATOR_LOW_BALANCE_ALERT','<p>Dear <b>Team</b>,</p>
                                  <p>Please find operator wise balance details as below , </p>
                              
                                <table width="100%" border="1" style="border:1px solid #000; font-family:Arial, Helvetica, sans-serif; font-size:13px; line-height:30px;">
                                <tr>
                                   <th>Sr.no</th>
                                   <th>Operator Name</th>
                                   <th>Minimum Balance</th>
                                   <th>Current Balance</th>
                                </tr>
                                <tr>
                                   <td>1</th>
                                   <td>Msrtc</td>
                                   <td>{{msrtc_min}}</td>
                                   {{msrtc_current}}
                                 </tr>
                                 <tr>
                                   <td>2</th>
                                   <td>Upsrtc</td>
                                   <td>{{upsrtc_min}}</td>
                                   {{upsrtc_current}}
                                 </tr>
                                 <tr>
                                   <td>3</th>
                                   <td>Rsrtc</td>
                                   <td>{{rsrtc_min}}</td>
                                   {{rsrtc_current}}
                                </tr> 
                                 <tr>
                                   <td>4</th>
                                   <td>Hrtc</td>
                                   <td>{{hrtc_min}}</td>
                                   {{hrtc_current}}
                                 </tr>
                                 <tr>
                                   <td>5</th>
                                   <td>Etravelsmart</td>
                                   <td>{{ets_min}}</td>
                                   {{ets_current}}
                                 </tr>
                                </table><br><br><br><br>
                              <p>Regards,</p>
                              <p>Team Rokad</p>');

define('CB_OP_WALLET_TOPUP_REQUEST','<table width="100%" border="1" style="border-collapse: collapse; border:1px solid #000; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:30px;">
                                <tr>
                                <td>
                                <table width="95%" border="0" style="margin:0 auto;">
                                <tr></tr>
                                <tr style="line-height:40px;">
                                <td style="text-align:center;">
                                <span style="color:#ff940a; font-weight:bold; font-size:12px;"></span>
                                <span style="font-weight:bold; font-size:12px;">Wallet Topup Request<br></span>
                                </td>
                                </tr>
                                </table>
                                </td>
                                </tr>
                                <tr>
                                <td><table width="95%" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:20px; margin:0 auto;"><tr>
                                <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">TopUp By</span><br />{{topup_by}}<br />
                                <br /><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Amount</span><br />{{amount}}</td>
                                <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Email</span><br />{{useremail}}<br />
                                <br /><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Name</span><br />{{username}}</td>  
                                </tr>
                                <tr></tr>
                                <tr></tr>
                                <tr><td valign="top" style="padding-top:10px;"><p><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Transaction Date</span><br />
                                {{transaction_date}}</p></td>
                                <td valign="top">
                                <p><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Approval</span><br />{{is_approvel}}</p></td>
                                </tr>
                                </table>
                                </td>
                                </tr>
                                </table>');
define('CB_OPERATOR_BALANCE_ALERT','<p>Dear <b>{{username}}</b>,</p>
                <p>Your Wallet Balance is Below Rs 1,00,000.</p>
                              <p>Your Current Balance is Rs {{wallet_amount}}.</p>
                              <p>Kindly Recharge your account to book tickets without any difficulties.</p>
                              
                              <br><br>








                              <p>Regards,</p>
                <p>Trimax</p>');
				
define('ROKAD_BALANCE_ALERT','<p>Dear <b>{{username}}</b>,</p>
							  <p>Your Current Wallet Balance is Rs {{amount}}.</p>
							  <p>Your Wallet has reached or cross the minimum threshold limit Rs {{threshold_amount}}.</p>
                              <p>Please refill your wallet to avail services.</p>
                              









                              <p>Regards,</p>
							  <p>Team Rokad</p>');				