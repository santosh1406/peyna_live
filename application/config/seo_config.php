<?php

$config['home_metadata'] = array(
									"title" => "Peyna - Digital E-wallet", 
									"description" => "Peyna - Digital E-wallet", 
									"keywords" => "Peyna - Digital E-wallet" 
								);

$config['about_metadata'] = array(
									"title" => "Peyna - Digital E-wallet", 
									"description" => "Peyna - Digital E-wallet", 
									"keywords" => "Peyna - Digital E-wallet" 
								);

$config['faq_metadata'] = array(
									"title" => "Peyna - Digital E-wallet", 
									"description" => "Peyna - Digital E-wallet", 
									"keywords" => "Peyna - Digital E-wallet" 
								);


$config['tc_metadata'] = array(
									"title" => "Peyna - Digital E-wallet", 
									"description" => "Peyna - Digital E-wallet", 
									"keywords" => "Peyna - Digital E-wallet" 
								);


$config['login_metadata'] = array(
									"title" => "Peyna - Digital E-wallet", 
									"description" => "Peyna - Digital E-wallet", 
									"keywords" => "Peyna - Digital E-wallet" 
								);


$config['signup_metadata'] = array(
									"title" => "Peyna - Digital E-wallet", 
									"description" => "Peyna - Digital E-wallet", 
									"keywords" => "Peyna - Digital E-wallet" 
								);

$config['print_sms_ticket_metadata'] = array(
												"title" => "Peyna - Digital E-wallet", 
												"description" => "Peyna - Digital E-wallet", 
												"keywords" => "Peyna - Digital E-wallet" 
											);

$config['cancel_booking_metadata'] = array(
											"title" => "Peyna - Digital E-wallet", 
											"description" => "Peyna - Digital E-wallet", 
											"keywords" => "Peyna - Digital E-wallet" 
										  );

$config['privacy_policy_metadata'] = array(
											"title" => "Peyna - Digital E-wallet", 
											"description" => "Peyna - Digital E-wallet", 
											"keywords" => "Peyna - Digital E-wallet" 
										  );

$config['contact_us_metadata'] = array(
											"title" => "Peyna - Digital E-wallet", 
											"description" => "Peyna - Digital E-wallet", 
											"keywords" => "Peyna - Digital E-wallet" 
										  );


$config['agent_registration_metadata'] = array(
											"title" => "Peyna - Digital E-wallet", 
											"description" => "Peyna - Digital E-wallet", 
											"keywords" => "Peyna - Digital E-wallet" 
										  );

$config['dynamic_homesearch_metadata'] = array(
						                        "banglore_hyderabad"  	=>[
						                                                    "metadata_title" => "Bangalore Hyderebad Bus Tickets | Hyderabad Bangalore Bus Booking",
						                                                    "metadata_description" => "Get Lowest Prices On Bangalore To Hyderabad Online Bus Tickets. Best Offers On Online Bus Booking Of All Hyderabad Bangalore Buses",
						                                                    "metadata_keywords" => ""
						                                                  ],

						                        "charbagh_gorakhpur"  	=>[
						                                                    "metadata_title" => "Charbagh Gorakhpur Bus Tickets | Gorakhpur Charbagh Online Bus Booking",
						                                                    "metadata_description" => "Get Lowest Prices On Charbagh To Gorakhpur Online Bus Tickets. Best Offers On Online Bus Booking Of Gorakhpur Charbagh Buses",
						                                                    "metadata_keywords" => ""
						                                                  ],
						                        "chennai_coimbatore"  	=>[
						                                                    "metadata_title" => "Chennai Coimbatore Bus Tickets | Coimbatore Chennai Online Bus Booking",
						                                                    "metadata_description" => "Get Lowest Prices On Chennai To Coimbatore Online Bus Tickets. Best Offers On Online Bus Booking Of Coimbatore Chennai Buses",
						                                                    "metadata_keywords" => ""
						                                                  ],
						                        "chennai_madurai"     	=>[
						                                                    "metadata_title" => "Chennai Madurai Bus Tickets | Madurai Chennai Online Bus Booking",
						                                                    "metadata_description" => "Get Lowest Prices On Chennai To Madurai Online Bus Tickets. Best Offers On Online Bus Booking Of Madurai Chennai Buses",
						                                                    "metadata_keywords" => ""
						                                                  ],
						                        "chennai_trichy"      	=>[
						                                                    "metadata_title" => "Chennai Trichy Bus Tickets | Trichy Chennai Online Bus Booking",
						                                                    "metadata_description" => "Get Lowest Prices On Chennai To Trichy Online Bus Tickets. Best Offers On Online Bus Booking Of Trichy Chennai Buses",
						                                                    "metadata_keywords" => ""
						                                                  ],
						                        "delhi_jaipur"        	=>[
						                                                    "metadata_title" => "Delhi Jaipur Bus Tickets | Jaipur Delhi Online Bus Booking",
						                                                    "metadata_description" => "Get Lowest Prices On Delhi To Jaipur Online Bus Tickets. Best Offers On Online Bus Booking Of All Jaipur Delhi Buses",
						                                                    "metadata_keywords" => ""
						                                                  ],
						                        "delhi_manali"        	=>[
						                                                    "metadata_title" => "Delhi Manali Bus Tickets | Manali Delhi Online Bus Booking",
						                                                    "metadata_description" => "Get Lowest Prices On Delhi To Manali Online Bus Tickets. Best Offers On Online Bus Booking Of All Manali Delhi Buses",
						                                                    "metadata_keywords" => ""
						                                                  ],
						                        "hyderabad_chennai"   	=>[
						                                                    "metadata_title" => "Hyderebad Chennai Bus Tickets | Chennai Hyderabad Online Bus Booking",
						                                                    "metadata_description" => "Get Lowest Prices On Hyderabad To Chennal Online Bus Tickets. Best Offers On Online Bus Booking Of All Chennai Hyderabad Buses",
						                                                    "metadata_keywords" => ""
						                                                  ],
						                        "hyderabad_vijaywada" 	=>[
						                                                    "metadata_title" => "Hyderabad Vijayawada Bus | Vijayawada Hyderabad Online Bus Booking",
						                                                    "metadata_description" => "Get Lowest Prices On Hyderabad To Vijayawada Online Bus Tickets. Best Offers On Online Bus Booking Of Vijayawada Hyderabad Buses",
						                                                    "metadata_keywords" => ""
						                                                  ],
						                        "lucknow_delhi"       	=>[
						                                                    "metadata_title" => "Lucknow Delhi Bus Tickets | Delhi Lucknow Online Bus Booking",
						                                                    "metadata_description" => "Get Lowest Prices On Lucknow To Delhi Online Bus Tickets. Best Offers On Online Bus Booking Of All Delhi Lucknow Buses",
						                                                    "metadata_keywords" => ""
						                                                  ],
						                        "mumbai_ahmedabad"    	=>[
						                                                    "metadata_title" => "Mumbai Ahmedabad Bus Tickets | Ahmedabad Mumbai Online Bus Booking",
						                                                    "metadata_description" => "Get Lowest Prices On Mumbai To Ahmedabad Online Bus Tickets. Best Offers On Online Bus Booking Of All Ahmedabad Mumbai Buses",
						                                                    "metadata_keywords" => ""
						                                                  ],
						                        "mumbai_pune"         	=>[
						                                                    "metadata_title" => "Mumbai Pune Bus Booking | Pune Mumbai Bus Tickets | Online Bus Booking",
						                                                    "metadata_description" => "Get Lowest Prices On Mumbai to Pune Bus Tickets. Best Offers On Online Bus Booking Of Shivneri, Neeta Travels and All Pune Mumbai Buses",
						                                                    "metadata_keywords" => ""
						                                                  ],
						                        "mumbai_shirdi"         =>[
						                                                    "metadata_title" => "Mumbai Shirdi Bus Tickets | Shirdi Mumbai Online Bus Booking",
						                                                    "metadata_description" => "Get Lowest Prices On Mumbai To Shirdi Online Bus Tickets. Best Offers On Online Bus Booking Of All Shirdi Mumbai Buses",
						                                                    "metadata_keywords" => ""
						                                                  ],
						                        "mumbai_surat"         	=>[
						                                                    "metadata_title" => "Mumbai Surat Bus Tickets | Surat Mumbai Online Bus Booking",
						                                                    "metadata_description" => "Get Lowest Prices On Mumbai To Surat Online Bus Tickets. Best Offers On Online Bus Booking Of All Surat Mumbai Buses",
						                                                    "metadata_keywords" => ""
						                                                  ],
						                        "mumbai_vadodara"      	=>[
						                                                    "metadata_title" => "Mumbai Vadodara Bus Tickets | Vadodara Mumbai Online Bus Booking",
						                                                    "metadata_description" => "Get Lowest Prices On Mumbai To Vadodara Online Bus Tickets. Best Offers On Online Bus Booking Of All Vadodara Mumbai Buses",
						                                                    "metadata_keywords" => ""
						                                                  ],
						                        "mumbai_banglore"      	=>[
						                                                    "metadata_title" => "Pune Bangalore Bus Tickets | Bangalore Pune Online Bus Booking",
						                                                    "metadata_description" => "Get Lowest Prices On Pune To Bangalore Online Bus Tickets. Best Offers On Online Bus Booking Of All Bangalore Pune Buses",
						                                                    "metadata_keywords" => ""
						                                                  ],
					                     );