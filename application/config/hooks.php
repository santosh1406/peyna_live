<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/

/*
 *@Author 		: Suraj Rathod
 */
$hook['post_controller_constructor'][] = array(
				                                'class'    => 'post_controller',
				                                'function' => 'check_is_remembered',
				                                'filename' => 'post_controller_constructor.php',
				                                'filepath' => 'hooks'
				                                );

$hook['post_controller_constructor'][] = array(
				                                'class'    => 'post_controller',
				                                'function' => 'is_maintenance_mode',
				                                'filename' => 'post_controller_constructor.php',
				                                'filepath' => 'hooks'
				                                );

$hook['post_controller_constructor'][] = array(
				                                'class'    => 'post_controller',
				                                'function' => 'check_all_query',
				                                'filename' => 'post_controller_constructor.php',
				                                'filepath' => 'hooks'
				                                );

$hook['post_controller_constructor'][] = array(
				                                'class'    => 'post_controller',
				                                'function' => 'load_view',
				                                'filename' => 'post_controller_constructor.php',
				                                'filepath' => 'hooks'
				                                );

// 'post_controller' indicated execution of hooks after controller is finished
$hook['post_controller'][] 			   = array(
											    'class' => 'db_log',             // Name of Class
											    'function' => 'logQueries',     // Name of function to be executed in from Class
											    'filename' => 'db_log.php',    // Name of the Hook file
											    'filepath' => 'hooks'         // Name of folder where Hook file is stored
												);
/* End of file hooks.php */
/* Location: ./application/config/hooks.php */