<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * where to send php errors to and from
 *
 * @file email_php_errors.php
 */

// to enable, set this to true

$config['email_php_errors']    = false;
if( in_array(ENVIRONMENT, array('production', 'testing', 'development'))  )
{
  $config['email_php_errors']    = true;
}



$config['php_error_from'] 	   = 'bookonspot_errors@bos.com';
$config['php_error_to'] 	     = 'mihirs@bookonspot.com';
$config['php_error_to_cc']   	 = array(
                                        "mihirs@bookonspot.com"
                                      );



// available shortcodes are {{severity}}, {{message}}, {{filepath}}, {{line}}
$config['php_error_subject'] = 'Error: ('.ENVIRONMENT.') Rokad '.ENVIRONMENT.' ';

$config['php_error_content'] = '<table border="1" cellpadding="5" cellspacing="5">
                                    <tr>
                                      <td colspan="2">Serious issue</td>
                                    </tr>                                    
                                    <tr>
                                      <td>Error Type ( severity)</td>
                                      <td>{{severity}}</td>
                                    </tr>
                                    <tr>
                                      <td>message</td>
                                      <td>{{msg}}</td>
                                    </tr>
                                    <tr>
                                      <td>filepath</td>
                                      <td>{{filepath}}</td>
                                    </tr>
                                    <tr>
                                      <td>line</td>
                                      <td>{{line}}</td>
                                    </tr>
                                    <tr>
                                      <td>Url Accessed</td>
                                      <td>{{current_url}}</td>
                                    </tr>
                                    <tr>
                                      <td>Occured on</td>
                                      <td>{{time}}</td>
                                    </tr>
                                    <tr>
                                      <td>ip_address</td>
                                      <td>{{ip}}</td>
                                    </tr>
                                  </table>';

/* End of file email_php_errors.php */
/* Location: ./application/config/email_php_errors.php */