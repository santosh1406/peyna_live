<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$config['api_holder'] 	= array(
								'upsrtc_api',
                                'rsrtc_api',
								'msrtc_api',
							// 	'etravelsmart_api',
								'hrtc_api',
								'its_api'
							);

$config['api_holder_path'] 	= array(
								'upsrtc_api' => 'soap_api/upsrtc_api',
								'msrtc_api' => 'soap_api/msrtc_api',
								'rsrtc_api' => 'soap_api/rsrtc_api',
								// 'etravelsmart_api' => 'api_provider/etravelsmart_api',
                                'hrtc_api' => 'api_provider/hrtc_api',
								'its_api' =>  'api_provider/its_api'
							);
