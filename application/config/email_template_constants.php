<?php

define('PG_FAILED_TEMPLATE', "<p>Transaction <b>{{pg_failed_reason}}</b> for <b>ticket id {{ticket_id}}</b>.</p>
                <p>The failure could be due to payment gateway failure response,api failure etc. In order to prevent transaction failure, please check log file generated as soon as possible.</p>
                <p>This mail is from {{site_url}}.</p>

                <p>Regards,</p>
                <p>Team Peyna</p>");


define('PG_SUCCESS_TICKET_FAILED_TEMPLATE', "<p>Payment done successfully but ticket booking failed.</b>.</p>
                       <p><b>Transaction Details are as below:</b></p>
                       <p>Ticket No:{{ticket_id}}</p>
                       <p>Ticket Amount:{{ticket_amount}}</p>
                       <p>CCAvenue Charges:{{ccavenue_charges}}</p>
                       <p>Total Amount:{{total_ticket_amount}}</p>
                       <p>CCAvenue Reference Number:{{pg_tracking_id}}</p>
                       <br/>
                       <p><b>Customer Details:</b></p>
                       <p>Name:{{customer_name}}</p>
                       <p>Email:{{customer_email}}</p>
                       <p>Phone:{{customer_mobile_number}}</p>
                       <p>Journey From:{{journey_from_to}}</p>
                       <p>Journey Date:{{journey_date}}</p>
                       <p>Booking Date:{{booking_date}}</p>
                       
                       <p><b>Booking Failed Reason:</b>{{failed_reason}}</p>

                       <p>Please check details if ticket is not booked then process to refund <b>Rs.{{ticket_refund_amount}}.</b></p>

                       <p>This mail is from {{site_url}}.</p>
                       
                       <p><b>NOTE:{{extra_note}}</b></p>

                       <p>Regards,</p>
                       <p>Team Peyna</p>");


define('UNSUCCESSFUL_TICKET_BOOKED_REFUND', "<p>Payment done successfully but ticket booking failed.</b>.</p>
                       <p><b>Transaction Details are as below:</b></p>
                       <p>Ticket No:{{ticket_id}}</p>
                       <p>Ticket Amount:{{ticket_amount}}</p>
                       <p>PG Reference Number (If booked from PG):{{pg_tracking_id}}</p>
                       <p>Wallet Id (If booked from Wallet):{{wallet_id}}</p>
                       <p>Journey From:{{journey_from_to}}</p>
                       <p>Journey Date:{{journey_date}}</p>
                       <p>Booking Date:{{booking_date}}</p>
                       <p><b>Ticket Booked Status:</b>{{transaction_status}}</p>

                       <p>This mail is from {{site_url}}.</p>
                       
                       <p>Regards,</p>
                       <p>Team Peyna</p>");


define('DEVELOPER_CUSTOM_ERROR_MESSAGE', "<p>Very important notice. Please track this.</b></p>
                      <p>{{custom_error_message}}</p>

                     <p>Regards,</p>
                     <p>Team Peyna</p>");

define('WALLET_TOPUP_REQUEST','<table width="100%" border="1" style="border-collapse: collapse; border:1px solid #000; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:30px;">
                                <tr>
                                <td>
                                <table width="95%" border="0" style="margin:0 auto;">
                                <tr>
                                <td><img alt="Peyna" src="http://peyna.in/assets/travelo/front/images/logo.png" style="width:104px;"></td>
                                </tr>
                                <tr></tr>
                                <tr style="line-height:40px;">
                                <td style="text-align:center;">
                                <span style="color:#ff940a; font-weight:bold; font-size:12px;"></span>
                                <span style="font-weight:bold; font-size:12px;">Wallet Topup Request<br></span>
                                </td>
                                </tr>
                                </table>
                                </td>
                                </tr>
                                <tr>
                                <td><table width="95%" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:20px; margin:0 auto;"><tr>
                                <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">TopUp By</span><br />{{topup_by}}<br />
                                <br /><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Amount</span><br />{{amount}}</td>
                                <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">User Email</span><br />{{useremail}}<br />
                                <br /><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Name</span><br />{{username}}</td>  
                                </tr>
                                <tr></tr>
                                <tr style="border-top:1px solid #000;">
                                <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Bank Name</span><br />{{bank_name}}<br /><br />
                                <span style="color:#ff940a; text-decoration:underline;line-height:27px;">Bank Account No</span><br />{{bank_acc_no}}
                                </td>
                                <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Transaction No</span><br />{{transaction_no}}<br />
                                <br /><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Transaction Remark</span><br />{{transaction_remark}}</td>
                                </tr>
                                <tr></tr>
                                <tr><td valign="top" style="padding-top:10px;"><p><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Transaction Date</span><br />
                                {{transaction_date}}</p></td>
                                <td valign="top">
                                <p><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Approval</span><br />{{is_approvel}}</p></td>
                                </tr>
                                </table>
                                </td>
                                </tr>
                                </table><br /><br />
                                <p>Regards,</p><p>Team Peyna</p>');

define('WALLET_TOPUP_REQUEST_REJECTED','<table width="100%" border="1" style="border-collapse: collapse; border:1px solid #000; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:30px;">
                                        <tr><td><table width="95%" border="0" style="margin:0 auto;">
                                        <tr><td><img alt="Peyna" src="http://peyna.in/assets/travelo/front/images/logo.png" style="width:104px;"></td>
                                        </tr><tr></tr>
                                        <tr style="line-height:40px;"><td style="text-align:center;">
                                        <span style="color:#ff940a; font-weight:bold; font-size:12px;"></span>
                                        <span style="font-weight:bold; font-size:12px;">Wallet Topup Request<br></span>
                                        </td></tr></table></td></tr><tr>
                                        <td><table width="95%" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:20px; margin:0 auto;"><tr>
                                        <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">TopUp By</span><br />{{topup_by}}<br />
                                        <br /><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Amount</span><br />{{amount}}</td>
                                        <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">User Email</span><br />{{useremail}}<br />
                                        <br /><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Name</span><br />{{username}}</td>  
                                        </tr>
                                        <tr></tr>
                                        <tr style="border-top:1px solid #000;">
                                        <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Bank Name</span><br />{{bank_name}}<br /><br />
                                        <span style="color:#ff940a; text-decoration:underline;line-height:27px;">Bank Account No</span><br />{{bank_acc_no}}
                                        </td>
                                        <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Transaction No</span><br />{{transaction_no}}<br />
                                        <br /><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Transaction Remark</span><br />{{transaction_remark}}</td>
                                        </tr>
                                        <tr></tr>
                                        <tr><td valign="top" style="padding-top:10px;"><p><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Transaction Date</span><br />
                                        {{transaction_date}}</p></td>
                                        <td valign="top">
                                        <p><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Approval</span><br />{{is_approvel}}</p></td>
                                        </tr>
                                        <tr><td valign="top" style="padding-top:10px;"><p><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Rejection Reason</span><br />
                                        {{reject_reason}}</p></td>
                                        </tr>
                                        </table>
                                        </td>
                                        </tr>
                                        </table><br /><br />
                                        <p>Regards,</p><p>Team Peyna</p>');

define('AGENT_BALANCE_ALERT','<p>Dear <b>{{username}}</b>,</p>
                <p>Your Wallet Balance is Below Rs 2000.</p>
                              <p>Your Current Balance is Rs {{amount}}.</p>
                              <p>Kindly Recharge your account to book tickets without any difficulties.</p>
                              









                              <p>Regards,</p>
                <p>Team Peyna</p>');

define('ADMIN_AGENT_BALANCE_LOW','<p>Dear <b>Admin</b>,</p>
                    <p>Below is the list of Agent whose Balance is less than Rs 2000.</p>
                              
                                <table width="100%" border="1" style="border-collapse: collapse; border:1px solid #000; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:30px;">
                                <tr>
                                   <th>Name</th>
                                   <th>Email</th>
                                   <th>Current Balance</th>
                                </tr>
                                <tr>
                                   <td>{{username}}</td>
                                   <td>{{useremail}}</td>
                                   <td>{{amount}}</td>
                                 </tr>
                                </table><br><br><br><br>
                                







                              <p>Regards,</p>
                <p>Team Peyna</p>');

define('OPERATOR_LOW_BALANCE_ALERT','<p>Dear <b>Team</b>,</p>
                                  <p>Please find operator wise balance details as below , </p>
                              
                                <table width="100%" border="1" style="border:1px solid #000; font-family:Arial, Helvetica, sans-serif; font-size:13px; line-height:30px;">
                                <tr>
                                   <th>Sr.no</th>
                                   <th>Operator Name</th>
                                   <th>Minimum Balance</th>
                                   <th>Current Balance</th>
                                </tr>
                                <tr>
                                   <td>1</th>
                                   <td>Msrtc</td>
                                   <td>{{msrtc_min}}</td>
                                   {{msrtc_current}}
                                 </tr>
                                 <tr>
                                   <td>2</th>
                                   <td>Upsrtc</td>
                                   <td>{{upsrtc_min}}</td>
                                   {{upsrtc_current}}
                                 </tr>
                                 <tr>
                                   <td>3</th>
                                   <td>Rsrtc</td>
                                   <td>{{rsrtc_min}}</td>
                                   {{rsrtc_current}}
                                </tr> 
                                 <tr>
                                   <td>4</th>
                                   <td>Hrtc</td>
                                   <td>{{hrtc_min}}</td>
                                   {{hrtc_current}}
                                 </tr>
                                 <tr>
                                   <td>5</th>
                                   <td>Etravelsmart</td>
                                   <td>{{ets_min}}</td>
                                   {{ets_current}}
                                 </tr>
                                </table><br><br><br><br>
                              <p>Regards,</p>
                              <p>Team Peyna</p>');

define('CB_OP_WALLET_TOPUP_REQUEST','<table width="100%" border="1" style="border-collapse: collapse; border:1px solid #000; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:30px;">
                                <tr>
                                <td>
                                <table width="95%" border="0" style="margin:0 auto;">
                                <tr></tr>
                                <tr style="line-height:40px;">
                                <td style="text-align:center;">
                                <span style="color:#ff940a; font-weight:bold; font-size:12px;"></span>
                                <span style="font-weight:bold; font-size:12px;">Wallet Topup Request<br></span>
                                </td>
                                </tr>
                                </table>
                                </td>
                                </tr>
                                <tr>
                                <td><table width="95%" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:20px; margin:0 auto;"><tr>
                                <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">TopUp By</span><br />{{topup_by}}<br />
                                <br /><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Amount</span><br />{{amount}}</td>
                                <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Email</span><br />{{useremail}}<br />
                                <br /><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Name</span><br />{{username}}</td>  
                                </tr>
                                <tr></tr>
                                <tr></tr>
                                <tr><td valign="top" style="padding-top:10px;"><p><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Transaction Date</span><br />
                                {{transaction_date}}</p></td>
                                <td valign="top">
                                <p><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Approval</span><br />{{is_approvel}}</p></td>
                                </tr>
                                </table>
                                </td>
                                </tr>
                                </table>');
define('CB_OPERATOR_BALANCE_ALERT','<p>Dear <b>{{username}}</b>,</p>
                <p>Your Wallet Balance is Below Rs 1,00,000.</p>
                              <p>Your Current Balance is Rs {{wallet_amount}}.</p>
                              <p>Kindly Recharge your account to book tickets without any difficulties.</p>
                              
                              <br><br>








                              <p>Regards,</p>
                <p>Trimax</p>');
        
define('Peyna_BALANCE_ALERT','<p>Dear <b>{{username}}</b>,</p>
                <p>Your Current Wallet Balance is Rs {{amount}}.</p>
                <p>Your Wallet has reached or cross the minimum threshold limit Rs {{threshold_amount}}.</p>
                              <p>Please refill your wallet to avail services.</p>
                              









                              <p>Regards,</p>
                <p>Team Peyna</p>');

define('TICKET_WINDOW_TOPUP_REQUEST','<table width="100%" border="1" style="border-collapse: collapse; border:1px solid #000; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:30px;">
                                <tr>
                                <td>
                                <table width="95%" border="0" style="margin:0 auto;">
                                <tr>
                                <td><img alt="Peyna" src="http://peyna.in/assets/travelo/front/images/logo.png" style="width:104px;"></td>
                                </tr>
                                <tr></tr>
                                <tr style="line-height:40px;">
                                <td style="text-align:center;">
                                <span style="color:#ff940a; font-weight:bold; font-size:12px;"></span>
                                <span style="font-weight:bold; font-size:12px;">Ticket Window Topup Request<br></span>
                                </td>
                                </tr>
                                </table>
                                </td>
                                </tr>
                                <tr>
                                <td><table width="95%" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:20px; margin:0 auto;"><tr>
                                <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">TopUp By</span><br />{{topup_by}}<br />
                                <br /><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Amount</span><br />{{amount}}</td>
                                <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">User Email</span><br />{{useremail}}<br />
                                <br /><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Name</span><br />{{username}}</td>  
                                </tr>
                                <tr></tr>
                                <tr style="border-top:1px solid #000;">
                                <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Bank Name</span><br />{{bank_name}}<br /><br />
                                <span style="color:#ff940a; text-decoration:underline;line-height:27px;">Bank Account No</span><br />{{bank_acc_no}}
                                </td>
                                <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Transaction No</span><br />{{transaction_no}}<br />
                                <br /><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Transaction Remark</span><br />{{transaction_remark}}</td>
                                </tr>
                                <tr></tr>                                
                                </table>
                                </td>
                                </tr>
                                </table><br /><br />
                                <p>Regards,</p><p>Team Peyna</p>'); 
define('AGENT_TERMINAL_CODE','<p>Dear <b>{{username}}</b>,</p>               
                              <p>Your Terminal Code is  {{terminal_code}}.</p>
                              <p>Regards,</p>
        <p>Team Peyna</p>');

define('CITYCASH_REQUEST','<table width="100%" border="1" style="border-collapse: collapse; border:1px solid #000; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:30px;">
                                <tr>
                                <td>
                                <table width="95%" border="0" style="margin:0 auto;">
                                <tr></tr>
                                <tr style="line-height:40px;">
                                <td style="text-align:center;">
                                <span style="color:#ff940a; font-weight:bold; font-size:12px;"></span>
                                <span style="font-weight:bold; font-size:12px;">Terminal Code For Whitelist<br></span>
                                </td>
                                </tr>
                                </table>
                                </td>
                                </tr>
                                <tr>
                                <td><table width="95%" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:20px; margin:0 auto;"><tr>
                                <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Agent Code </span>{{agent_code}}</td>
                                <td><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Agent Name </span>{{agent_name}}</td>
                                <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Terminal Code </span>{{terminal_code}}<br />
                               </td>  
                                </tr>
                                <tr></tr>
                                <tr></tr>
                                
                                </table>
                                </td>
                                </tr>
                                </table>');

define('DMT_RECEIPT','<table align="center" border="1" width="550" style="border-collapse: collapse; margin-top: 5px;">
  
  <tr>
    <td align="left" valign="top" style="padding: 18px;">
      <table align="center" border="0"  cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td valign="middle" align="left"><img src="https://Peyna.in/assets/travelo/front/images/logo.png" class="img-responsive" width="168px" height="40px"></td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="right"></td>
        </tr>
        <tr>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="bottom" align="right"><strong>Date: </strong>'.date('d-m-Y').'&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding: 15px;">
      <table align="center" border="0"  width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td valign="middle" align="left" width="25%" style="font-weight: bold; height: 25px;">Service Name <b> : </b></td>        
          <td valign="middle" align="left">{{service_name}}</td>
           <td valign="middle" align="left" style="font-weight: bold;">Type <b> : </b></td>         
          <td valign="middle" align="left">{{transfer_type}}</td>         
        </tr>
        <tr>
          <td valign="middle" align="left" width="25%" style="font-weight: bold; height: 25px;">Agent Name <b> : </b></td>        
          <td valign="middle" align="left">{{agent_name}}</td>
          <td valign="middle" align="left" style="font-weight: bold;">Agent Code <b> : </b></td>
          <td valign="middle" align="left">{{agent_code}}</td>
        </tr>
        <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 25px;">Mobile No  <b> : </b></td>         
          <td valign="middle" align="left">{{customer_mobile_no}}</td>
          <td valign="middle" align="left" style="font-weight: bold;">Customer Name <b> : </b></td>
          <td valign="middle" align="left">{{customer_name}}</td>
        </tr>
        <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 25px;">Beneficiary IFSC Code <b> : </b></td>
          <td valign="middle" align="left">{{ifsc_code}}</td>
          <td valign="middle" align="left" style="font-weight: bold;">Beneficiary Account Number <b> : </b></td>         
          <td valign="middle" align="left">{{account_no}}</td>
        </tr>
         <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 25px;">Amount <b> : </b></td>
          <td valign="middle" align="left">{{amount}}</td>
          <td valign="middle" align="left" style="font-weight: bold;">Service Fee <b> : </b></td>
         

          <td valign="middle" align="left">{{customer_charge}}</td>
        </tr>
        <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 25px;">Transaction No <b> : </b></td>          
          <td valign="middle" align="left">{{trans_no}}</td>
          <td valign="middle" align="left" style="font-weight: bold;">Paid By  <b> : </b></td>         
          <td valign="middle" align="left">Wallet</td>
        </tr>        
        <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 1px;">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
        </tr>    
        <tr>
          <td valign="middle" align="left" colspan="4">
            <table align="center" border="1" width="100%" cellpadding="2" cellspacing="1">
              <tr>
                <td valign="middle" align="left" width="20%" style="padding: 5px;"><b>Sr. No.</b></td>
                <td valign="middle" align="left" width="77%" style="padding: 5px;"><b>Charge Name</b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>Paid Amount</b></td>
              </tr>
              <tr>
                <td valign="middle" align="left" style="padding: 5px;">1</td>
                <td valign="middle" align="left" style="padding: 5px;">Domestic Money Transfer (DMT) </td>
                <td valign="middle" align="right" style="padding: 5px;">{{amount}}</td>
              </tr>
              <tr>
                <td valign="middle" align="left" style="padding: 5px;">2</td>
                <td valign="middle" align="left" style="padding: 5px;">{{service_fee}}</td>
                <td valign="middle" align="right" style="padding: 5px;">{{customer_charge}}</td>
              </tr>
              <tr>
                <td valign="middle" align="right" colspan="2" style="padding: 5px;"><b>Sub Total: </b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>{{final_amt}}</b></td>
              </tr>
              <tr>
                <td valign="middle" align="right" colspan="2" style="padding: 5px;"><b>Total Paid Amount : </b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>{{final_amt}}</b></td>
              </tr>
            </table>
            <br/>
            <b>Total Paid Amount(In Words) : </b>{{amount_in_words}} Only
            <br/>
            <br/>
            <b>Note: This is computer generated receipt signature not required.</b>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding: 50px 15px 15px 15px;">
    <br>
      <table align="center" border="0"  cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td valign="middle" align="right">'."Receiver's Signature".'</td>
        </tr>
      </table>
    </td>
  </tr>
</table>');

define('MOBILE_PREPAID_RECEIPT','<table align="center" border="1" width="500" style="border-collapse: collapse; margin-top: 5px;">
  <tr>
    <td align="left" valign="top" style="padding: 16px;">
      <table align="center" border="0"  cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td valign="middle" align="left"><img src="http://peyna.in/assets/travelo/front/images/logo.png" class="img-responsive" width="168px" height="40px"></td>
          <td valign="middle" align="left">&nbsp;</td>
        </tr>
        <tr>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="bottom" align="right"><strong>Date:</strong>'.date('d-m-Y').'&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding: 18px;">
      <table align="center" border="0"  width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td valign="middle" align="left" width="25%" style="font-weight: bold; height: 25px;">Service Name <b> : </b></td>        
          <td valign="middle" align="left">{{service_name}}</td>
           <td valign="middle" align="left" style="font-weight: bold;">Customer Mobile No<b> : </b></td>         
          <td valign="middle" align="left">{{customer_mobile_no}}</td>         
        </tr>
        <tr>
          <td valign="middle" align="left" width="25%" style="font-weight: bold; height: 25px;">Agent Name <b> : </b></td>        
          <td valign="middle" align="left">{{agent_name}}</td>
          <td valign="middle" align="left" style="font-weight: bold;">Agent Code <b> : </b></td>
          <td valign="middle" align="left">{{agent_code}}</td>
        </tr>        
        <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 25px;">Recharge Amount <b> : </b></td>
          <td valign="middle" align="left">{{recharge_amount}}</td>
          <td valign="middle" align="left" style="font-weight: bold;">Paid Mode <b> : </b></td>         
          <td valign="middle" align="left">Cash</td>
        </tr>
        <tr>          
          <td valign="middle" align="left" style="font-weight: bold;">Provider Name <b> : </b></td>
          <td valign="middle" align="left">{{provider}}</td>
          <td valign="middle" align="left" style="font-weight: bold; height: 25px;">Transaction No <b> : </b></td>          
          <td valign="middle" align="left">{{trans_no}}</td>
        </tr>
        <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 1px;">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
        </tr>    
        <tr>
          <td valign="middle" align="left" colspan="4">
            <table align="center" border="1" width="100%" cellpadding="2" cellspacing="1">
              <tr>
                <td valign="middle" align="left" width="20%" style="padding: 5px;"><b>Sr. No.</b></td>
                <td valign="middle" align="left" width="77%" style="padding: 5px;"><b>Charge Name</b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>Paid Amount</b></td>
              </tr>
              <tr>
                <td valign="middle" align="left" style="padding: 5px;">1</td>
                <td valign="middle" align="left" style="padding: 5px;">Mobile Prepaid Recharge </td>
                <td valign="middle" align="right" style="padding: 5px;">{{recharge_amount}}</td>
              </tr>             
              <tr>
                <td valign="middle" align="right" colspan="2" style="padding: 5px;"><b>Sub Total: </b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>{{recharge_amount}}</b></td>
              </tr>
              <tr>
                <td valign="middle" align="right" colspan="2" style="padding: 5px;"><b>Total Paid Amount : </b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>{{recharge_amount}}</b></td>
              </tr>
            </table>
            <br/>
            <b>Total Paid Amount(In Words) : </b>{{amount_in_words}} Only
            <br/>
            <br/>
            <b>Note: This is computer generated receipt signature not required.</b>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding: 50px 15px 15px 15px;">
    <br>
      <table align="center" border="0"  cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td valign="middle" align="right">'."Receiver's Signature".'</td>
        </tr>
      </table>
    </td>
  </tr>
</table>');

define('MOBILE_POSTPAID_RECEIPT','<table align="center" border="1" width="500" style="border-collapse: collapse; margin-top: 5px;">
  <tr>
    <td align="left" valign="top" style="padding: 18px;">
      <table align="center" border="0"  cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td valign="middle" align="left"><img src="https://Peyna.in/assets/travelo/front/images/logo.png" class="img-responsive" width="168px" height="40px"></td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="right"></td>
        </tr>
        <tr>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="bottom" align="right"><strong>Date:</strong>'.date('d-m-Y').'&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding: 15px;">
      <table align="center" border="0"  width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td valign="middle" align="left" width="25%" style="font-weight: bold; height: 25px;">Service Name <b> : </b></td>        
          <td valign="middle" align="left">{{service_name}}</td>
           <td valign="middle" align="left" style="font-weight: bold;">Customer Mobile No<b> : </b></td>         
          <td valign="middle" align="left">{{customer_mobile_no}}</td>         
        </tr>
        <tr>
          <td valign="middle" align="left" width="25%" style="font-weight: bold; height: 25px;">Agent Name <b> : </b></td>        
          <td valign="middle" align="left">{{agent_name}}</td>
          <td valign="middle" align="left" style="font-weight: bold;">Agent Code <b> : </b></td>
          <td valign="middle" align="left">{{agent_code}}</td>
        </tr>        
        <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 25px;">Recharge Amount <b> : </b></td>
          <td valign="middle" align="left">{{recharge_amount}}</td>
          <td valign="middle" align="left" style="font-weight: bold;">Paid Mode <b> : </b></td>         
          <td valign="middle" align="left">Cash</td>
        </tr>
        <tr>          
          <td valign="middle" align="left" style="font-weight: bold;">Provider Name <b> : </b></td>
          <td valign="middle" align="left">{{provider}}</td>
          <td valign="middle" align="left" style="font-weight: bold; height: 25px;">Transaction No <b> : </b></td>          
          <td valign="middle" align="left">{{trans_no}}</td>
        </tr>
        <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 1px;">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
        </tr>    
        <tr>
          <td valign="middle" align="left" colspan="4">
            <table align="center" border="1" width="100%" cellpadding="2" cellspacing="1">
              <tr>
                <td valign="middle" align="left" width="20%" style="padding: 5px;"><b>Sr. No.</b></td>
                <td valign="middle" align="left" width="77%" style="padding: 5px;"><b>Charge Name</b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>Paid Amount</b></td>
              </tr>
              <tr>
                <td valign="middle" align="left" style="padding: 5px;">1</td>
                <td valign="middle" align="left" style="padding: 5px;">Mobile Postpaid Recharge </td>
                <td valign="middle" align="right" style="padding: 5px;">{{recharge_amount}}</td>
              </tr>             
              <tr>
                <td valign="middle" align="right" colspan="2" style="padding: 5px;"><b>Sub Total: </b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>{{recharge_amount}}</b></td>
              </tr>
              <tr>
                <td valign="middle" align="right" colspan="2" style="padding: 5px;"><b>Total Paid Amount : </b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>{{recharge_amount}}</b></td>
              </tr>
            </table>
            <br/>
            <b>Total Paid Amount(In Words) : </b>{{amount_in_words}} Only
            <br/>
            <br/>
            <b>Note: This is computer generated receipt signature not required.</b>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding: 50px 15px 15px 15px;">
    <br>
      <table align="center" border="0"  cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td valign="middle" align="right">'."Receiver's Signature".'</td>
        </tr>
      </table>
    </td>
  </tr>
</table>');
define('DTH_RECHARGE_RECEIPT','<table align="center" border="1" width="500" style="border-collapse: collapse; margin-top: 5px;">
  <tr>
    <td align="left" valign="top" style="padding: 18px;">
      <table align="center" border="0"  cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td valign="middle" align="left"><img src="https://Peyna.in/assets/travelo/front/images/logo.png" class="img-responsive" width="168px" height="40px"></td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="right"></td>
        </tr>
        <tr>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="bottom" align="right"><strong>Date:</strong>'.date('d-m-Y').'&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding: 15px;">
      <table align="center" border="0"  width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td valign="middle" align="left" width="25%" style="font-weight: bold; height: 25px;">Service Name <b> : </b></td>        
          <td valign="middle" align="left">{{service_name}}</td>
           <td valign="middle" align="left" style="font-weight: bold;">Customer Mobile No<b> : </b></td>         
          <td valign="middle" align="left">{{customer_mobile_no}}</td>         
        </tr>
        <tr>
          <td valign="middle" align="left" width="25%" style="font-weight: bold; height: 25px;">Agent Name <b> : </b></td>        
          <td valign="middle" align="left">{{agent_name}}</td>
          <td valign="middle" align="left" style="font-weight: bold;">Agent Code <b> : </b></td>
          <td valign="middle" align="left">{{agent_code}}</td>
        </tr>        
        <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 25px;">Recharge Amount <b> : </b></td>
          <td valign="middle" align="left">{{recharge_amount}}</td>
          <td valign="middle" align="left" style="font-weight: bold;">Paid Mode <b> : </b></td>         
          <td valign="middle" align="left">Wallet</td>
        </tr>
        <tr>          
          <td valign="middle" align="left" style="font-weight: bold;">Provider Name <b> : </b></td>
          <td valign="middle" align="left">{{provider}}</td>
          <td valign="middle" align="left" style="font-weight: bold; height: 25px;">Transaction No <b> : </b></td>          
          <td valign="middle" align="left">{{trans_no}}</td>
        </tr>
        <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 1px;">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
        </tr>    
        <tr>
          <td valign="middle" align="left" colspan="4">
            <table align="center" border="1" width="100%" cellpadding="2" cellspacing="1">
              <tr>
                <td valign="middle" align="left" width="20%" style="padding: 5px;"><b>Sr. No.</b></td>
                <td valign="middle" align="left" width="77%" style="padding: 5px;"><b>Charge Name</b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>Paid Amount</b></td>
              </tr>
              <tr>
                <td valign="middle" align="left" style="padding: 5px;">1</td>
                <td valign="middle" align="left" style="padding: 5px;">DTH Recharge </td>
                <td valign="middle" align="right" style="padding: 5px;">{{recharge_amount}}</td>
              </tr>             
              <tr>
                <td valign="middle" align="right" colspan="2" style="padding: 5px;"><b>Sub Total: </b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>{{recharge_amount}}</b></td>
              </tr>
              <tr>
                <td valign="middle" align="right" colspan="2" style="padding: 5px;"><b>Total Paid Amount : </b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>{{recharge_amount}}</b></td>
              </tr>
            </table>
            <br/>
            <b>Total Paid Amount(In Words) : </b>{{amount_in_words}} Only
            <br/>
            <br/>
            <b>Note: This is computer generated receipt signature not required.</b>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding: 50px 15px 15px 15px;">
    <br>
      <table align="center" border="0"  cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td valign="middle" align="right">'."Receiver's Signature".'</td>
        </tr>
      </table>
    </td>
  </tr>
</table>');

define('DATACARD_RECHARGE_RECEIPT','<table align="center" border="1" width="500" style="border-collapse: collapse; margin-top: 5px;">
  <tr>
    <td align="left" valign="top" style="padding: 15px;">
      <table align="center" border="0"  cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td valign="middle" align="left"><img src="https://Peyna.in/assets/travelo/front/images/logo.png" class="img-responsive" width="168px" height="40px"></td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="right"><img src="#" class="img-responsive" width="120px" height="40px"></td>
        </tr>
        <tr>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="bottom" align="right"><strong>Date:</strong>'.date('d-m-Y').'&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding: 15px;">
      <table align="center" border="0"  width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td valign="middle" align="left" width="25%" style="font-weight: bold; height: 25px;">Service Name <b> : </b></td>        
          <td valign="middle" align="left">{{service_name}}</td>
           <td valign="middle" align="left" style="font-weight: bold;">Customer Mobile No<b> : </b></td>         
          <td valign="middle" align="left">{{customer_mobile_no}}</td>         
        </tr>
        <tr>
          <td valign="middle" align="left" width="25%" style="font-weight: bold; height: 25px;">Sale Agent Name <b> : </b></td>        
          <td valign="middle" align="left">{{agent_name}}</td>
          <td valign="middle" align="left" style="font-weight: bold;">Sale Agent Code <b> : </b></td>
          <td valign="middle" align="left">{{agent_code}}</td>
        </tr>        
        <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 25px;">Recharge Type <b> : </b></td>
          <td valign="middle" align="left">{{type}}</td>
          <td valign="middle" align="left" style="font-weight: bold; height: 25px;">Recharge Amount <b> : </b></td>
          <td valign="middle" align="left">{{recharge_amount}}</td>
        </tr>
        <tr>          
          <td valign="middle" align="left" style="font-weight: bold;">Provider Name <b> : </b></td>
          <td valign="middle" align="left">{{provider}}</td>
          <td valign="middle" align="left" style="font-weight: bold; height: 25px;">Transaction No <b> : </b></td>          
          <td valign="middle" align="left">{{trans_no}}</td>
        </tr> 
        <tr>
           <td valign="middle" align="left" style="font-weight: bold;">Paid Mode <b> : </b></td>         
          <td valign="middle" align="left">Cash</td>
        </tr>  
        <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 1px;">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
        </tr>     
        <tr>
          <td valign="middle" align="left" colspan="4">
            <table align="center" border="1" width="75%" cellpadding="2" cellspacing="1">
              <tr>
                <td valign="middle" align="left" width="20%" style="padding: 5px;"><b>Sr. No.</b></td>
                <td valign="middle" align="left" width="77%" style="padding: 5px;"><b>Charge Name</b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>Paid Amount</b></td>
              </tr>
              <tr>
                <td valign="middle" align="left" style="padding: 5px;">1</td>
                <td valign="middle" align="left" style="padding: 5px;">Datacard Recharge </td>
                <td valign="middle" align="right" style="padding: 5px;">{{recharge_amount}}</td>
              </tr>             
              <tr>
                <td valign="middle" align="right" colspan="2" style="padding: 5px;"><b>Sub Total: </b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>{{recharge_amount}}</b></td>
              </tr>
              <tr>
                <td valign="middle" align="right" colspan="2" style="padding: 5px;"><b>Total Paid Amount : </b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>{{recharge_amount}}</b></td>
              </tr>
            </table>
            <br/>
            <b>Total Paid Amount(In Words) : </b>{{amount_in_words}} Only
            <br/>
            <br/>
            <b>Note: This is computer generated receipt signature not required.</b>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding: 50px 15px 15px 15px;">
    <br>
      <table align="center" border="0"  cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td valign="middle" align="right">'."Receiver's Signature".'</td>
        </tr>
      </table>
    </td>

  </tr>
</table>');

define('EPAYLATER_RECHARGE_RECEIPT','<table align="center" border="1" width="500" style="border-collapse: collapse; margin-top: 5px;">
  <tr>
    <td align="left" valign="top" style="padding: 15px;">
      <table align="center" border="0"  cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td valign="middle" align="left"><img src="https://Peyna.in/assets/travelo/front/images/logo.png" class="img-responsive" width="168px" height="40px"></td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="right"><img src="#" class="img-responsive" width="120px" height="40px"></td>
        </tr>
        <tr>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="bottom" align="right"><strong>Date:</strong>'.date('d-m-Y').'&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding: 18px;">
      <table align="center" border="0"  width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td valign="middle" align="left" width="25%" style="font-weight: bold; height: 25px;">Service Name <b> : </b></td>        
          <td valign="middle" align="left">{{service_name}}</td>
           <td valign="middle" align="left" style="font-weight: bold;">Customer Mobile No<b> : </b></td>         
          <td valign="middle" align="left">{{customer_mobile_no}}</td>         
        </tr>
        <tr>
          <td valign="middle" align="left" width="25%" style="font-weight: bold; height: 25px;">Sale Agent Name <b> : </b></td>        
          <td valign="middle" align="left">{{agent_name}}</td>
          <td valign="middle" align="left" style="font-weight: bold;">Sale Agent Code <b> : </b></td>
          <td valign="middle" align="left">{{agent_code}}</td>
        </tr>        
        <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 25px;">Recharge Type <b> : </b></td>
          <td valign="middle" align="left">{{type}}</td>
          <td valign="middle" align="left" style="font-weight: bold; height: 25px;">Recharge Amount <b> : </b></td>
          <td valign="middle" align="left">{{recharge_amount}}</td>
        </tr>
        <tr>          
          <td valign="middle" align="left" style="font-weight: bold;">Provider Name <b> : </b></td>
          <td valign="middle" align="left">{{provider}}</td>
          <td valign="middle" align="left" style="font-weight: bold; height: 25px;">Transaction No <b> : </b></td>          
          <td valign="middle" align="left">{{trans_no}}</td>
        </tr> 
        <tr>
           <td valign="middle" align="left" style="font-weight: bold;">Paid Mode <b> : </b></td>         
          <td valign="middle" align="left">Epaylater</td>
          <td valign="middle" align="left" style="font-weight: bold;">Epaylater Transaction No <b> : </b></td>
          <td valign="middle" align="left">{{epay_trans_no}}</td>
        </tr> 
        <tr>
           <td valign="middle" align="left" style="font-weight: bold;">Paid Mode <b> : </b></td>         
          <td valign="middle" align="left">Epaylater</td>
          <td valign="middle" align="left" style="font-weight: bold;">Epaylater Transaction No <b> : </b></td>
          <td valign="middle" align="left">{{epay_trans_no}}</td>
        </tr>  
        <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 1px;">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
        </tr>     
        <tr>
          <td valign="middle" align="left" colspan="4">
            <table align="center" border="1" width="100%" cellpadding="2" cellspacing="1">
              <tr>
                <td valign="middle" align="left" width="20%" style="padding: 5px;"><b>Sr. No.</b></td>
                <td valign="middle" align="left" width="77%" style="padding: 5px;"><b>Charge Name</b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>Paid Amount</b></td>
              </tr>
              <tr>
                <td valign="middle" align="left" style="padding: 5px;">1</td>
                <td valign="middle" align="left" style="padding: 5px;">{{service_name}} Recharge</td>
                <td valign="middle" align="right" style="padding: 5px;">{{recharge_amount}}</td>
              </tr>             
              <tr>
                <td valign="middle" align="right" colspan="2" style="padding: 5px;"><b>Sub Total: </b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>{{recharge_amount}}</b></td>
              </tr>
              <tr>
                <td valign="middle" align="right" colspan="2" style="padding: 5px;"><b>Total Paid Amount : </b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>{{recharge_amount}}</b></td>
              </tr>
            </table>
            <br/>
            <b>Total Paid Amount(In Words) : </b>{{amount_in_words}} Only
            <br/>
            <br/>
            <b>Note: This is computer generated receipt signature not required.</b>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding: 50px 15px 15px 15px;">
    <br>
      <table align="center" border="0"  cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td valign="middle" align="right">'."Receiver's Signature".'</td>
        </tr>
      </table>
    </td>
  </tr>
</table>');

define('MATM_RECEIPT','<table align="center" border="1" width="500" style="border-collapse: collapse; margin-top: 5px;">
  <tr>
    <td align="left" valign="top" style="padding: 18px;">
      <table align="center" border="0"  cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td valign="middle" align="left"><img src="https://Peyna.in/assets/travelo/front/images/logo.png" class="img-responsive" width="168px" height="40px"></td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="right"><img src="#" class="img-responsive" width="120px" height="40px"></td>
        </tr>
        <tr>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="bottom" align="right"><strong>Date:</strong>'.date('d-m-Y').'&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding: 15px;">
      <table align="center" border="0"  width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td valign="middle" align="left" width="25%" style="font-weight: bold; height: 25px;">Service Name </td>
          <td valign="middle" align="left" width="1%" height="1%"></td>
          <td valign="middle" align="left"><b> : </b>{{service_name}}</td>
          <td valign="middle" align="left" style="font-weight: bold;">Sell Agent Name</td>
          <td valign="middle" align="left" width="1%"></td>
          <td valign="middle" align="left"><b> : </b>{{agent_name}}</td>
        </tr>
        <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 25px;">Mobile No</td>
          <td valign="middle" align="left" width="1%"></td>
          <td valign="middle" align="left"><b> : </b>{{mobile_no}}</td>
          <td valign="middle" align="left" style="font-weight: bold;">Paid By</td>
          <td valign="middle" align="left" width="1%"></td>
          <td valign="middle" align="left"><b> : </b>Wallet</td>
        </tr>
        <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 25px;">Peyna Trans No</td>
          <td valign="middle" align="left" width="1%"></td>
          <td valign="middle" align="left"><b> : </b>{{Peyna_trans_no}}</td>
          <td valign="middle" align="left" style="font-weight: bold;">Trans Ref No</td>
          <td valign="middle" align="left" width="1%"></td>
          <td valign="middle" align="left"><b> : </b>{{trans_ref_no}}</td>
        </tr>
        
        <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 2px;">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
        </tr>
        <tr>
          <td valign="middle" align="left" colspan="6">
            <table align="center" border="1" width="100%" cellpadding="2" cellspacing="1">
              <tr>
                <td valign="middle" align="left" width="20%" style="padding: 5px;"><b>Sr. No.</b></td>
                <td valign="middle" align="left" width="77%" style="padding: 5px;"><b>Charge Name</b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>Paid Amount</b></td>
              </tr>
              <tr>
                <td valign="middle" align="left" style="padding: 5px;">1</td>
                <td valign="middle" align="left" style="padding: 5px;">Micro ATM Cash Withdraw</td>
                <td valign="middle" align="right" style="padding: 5px;">{{amount}}</td>
              </tr>
              
              <tr>
                <td valign="middle" align="right" colspan="2" style="padding: 5px;"><b>Sub Total: </b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>{{amount}}</b></td>
              </tr>
              <tr>
                <td valign="middle" align="right" colspan="2" style="padding: 5px;"><b>Total Paid Amount: </b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>{{amount}}</b></td>
              </tr>

            </table>
            <br/>
            <b>Total Paid Amount(In Words):</b>{{amount_in_words}} Only

            <br/>
            <br/>
            <b>Note: This is computer generated receipt signature not required.</b>

          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding: 50px 15px 15px 15px;">
      <table align="center" border="0"  cellpadding="2" cellspacing="2" width="100%" height="15px" style="margin-top:15px">
        <tr>
          <td valign="middle" align="right">'."Receiver's Signature".'</td>
        </tr>
      </table>
    </td>
  </tr>
</table>');

define('AEPS_RECEIPT','<table align="center" border="1" width="500" style="border-collapse: collapse; margin-top: 5px;">
  <tr>
    <td align="left" valign="top" style="padding: 18px;">
      <table align="center" border="0"  cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td valign="middle" align="left"><img src="https://Peyna.in/assets/travelo/front/images/logo.png" class="img-responsive" width="168px" height="40px"></td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="right"><img src="#" class="img-responsive" width="120px" height="40px"></td>
        </tr>
        <tr>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="bottom" align="right"><strong>Date:</strong>'.date('d-m-Y').'&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding: 15px;">
      <table align="center" border="0"  width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td valign="middle" align="left" width="25%" style="font-weight: bold; height: 25px;">Service Name </td>
          <td valign="middle" align="left" width="1%" height="1%"></td>
          <td valign="middle" align="left"><b> : </b>{{service_name}}</td>
          <td valign="middle" align="left" style="font-weight: bold;">Sell Agent Name</td>
          <td valign="middle" align="left" width="1%"></td>
          <td valign="middle" align="left"><b> : </b>{{agent_name}}</td>
        </tr>
        <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 25px;">Mobile No</td>
          <td valign="middle" align="left" width="1%"></td>
          <td valign="middle" align="left"><b> : </b>{{mobile_no}}</td>
          <td valign="middle" align="left" style="font-weight: bold;">Paid By</td>
          <td valign="middle" align="left" width="1%"></td>
          <td valign="middle" align="left"><b> : </b>Wallet</td>
        </tr>
        <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 25px;">Peyna Trans No</td>
          <td valign="middle" align="left" width="1%"></td>
          <td valign="middle" align="left"><b> : </b>{{Peyna_trans_no}}</td>
          <td valign="middle" align="left" style="font-weight: bold;">Trans Ref No</td>
          <td valign="middle" align="left" width="1%"></td>
          <td valign="middle" align="left"><b> : </b>{{trans_ref_no}}</td>
        </tr>
        
        <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 2px;">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
        </tr>
        <tr>
          <td valign="middle" align="left" colspan="6">
            <table align="center" border="1" width="100%" cellpadding="2" cellspacing="1">
              <tr>
                <td valign="middle" align="left" width="20%" style="padding: 5px;"><b>Sr. No.</b></td>
                <td valign="middle" align="left" width="77%" style="padding: 5px;"><b>Charge Name</b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>Paid Amount</b></td>
              </tr>
              <tr>
                <td valign="middle" align="left" style="padding: 5px;">1</td>
                <td valign="middle" align="left" style="padding: 5px;">AEPS Cash Withdraw</td>
                <td valign="middle" align="right" style="padding: 5px;">{{amount}}</td>
              </tr>
              
              <tr>
                <td valign="middle" align="right" colspan="2" style="padding: 5px;"><b>Sub Total: </b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>{{amount}}</b></td>
              </tr>
              <tr>
                <td valign="middle" align="right" colspan="2" style="padding: 5px;"><b>Total Paid Amount: </b></td>
                <td valign="middle" align="right" style="padding: 5px;"><b>{{amount}}</b></td>
              </tr>

            </table>
            <br/>
            <b>Total Paid Amount(In Words):</b>{{amount_in_words}} Only

            <br/>
            <br/>
            <b>Note: This is computer generated receipt signature not required.</b>

          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding: 50px 15px 15px 15px;">
      <table align="center" border="0"  cellpadding="2" cellspacing="2" width="100%" height="15px" style="margin-top:15px">
        <tr>
          <td valign="middle" align="right">'."Receiver's Signature".'</td>
        </tr>
      </table>
    </td>
  </tr>
</table>');
$rupSign = "Rs.";
define('BBPS_ELECTRICITY_RECEIPT','<table align="center" border="1" width="560" style="border-collapse: collapse; margin-top: 5px;">
  <tr>
    <td align="left" valign="top" style="padding: 15px;">
      <table align="center" border="0"  cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td valign="middle" align="left"><img src="https://Peyna.in/assets/travelo/front/images/logo.png" class="img-responsive" width="168px" height="40px"></td>
          <td valign="middle" align="left">&nbsp;</td>
          
        </tr>
        <tr>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="bottom" align="right"><strong>Date:</strong>'.date('d-m-Y').'&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding: 15px;">
      <table align="center" border="0"  cellpadding="2" cellspacing="2" width="100%">
        <tr>
          <td valign="middle" align="left" width="25%" style="font-weight: bold; height: 25px;">Service Name </td>
          <td valign="middle" align="left"><b> : </b>{{service_name}}</td>
                                        <td valign="middle" align="left" style="font-weight: bold;">Customer Name</td>
          <td valign="middle" align="left" nowrap><b> : </b>{{customer_name}}</td>
        </tr>
                                <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 25px;">Mobile No</td>
          <td valign="middle" align="left"><b> : </b>{{mobile_no}}</td>
          <td valign="middle" align="left" style="font-weight: bold;">Paid By</td>
          <td valign="middle" align="left"><b> : </b>Cash</td>
        </tr>
        <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 25px;">Peyna Trans No</td>
          <td valign="middle" align="left"><b> : </b>{{Peyna_trans_no}}</td>
          <td valign="middle" align="left" style="font-weight: bold;">Trans Ref No</td>
          <td valign="middle" align="left"><b> : </b>{{trans_ref_no}}</td>
        </tr>
        
        <tr>
          <td valign="middle" align="left" style="font-weight: bold; height: 2px;">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
          <td valign="middle" align="left">&nbsp;</td>
        </tr>
                        </table>
                </td>
        </tr>
        <tr>
    <td align="left" valign="top" style="padding: 15px;">
      <table align="center" border="1" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;">
                            <tr>
                                    <td valign="middle" align="left" style="padding: 5px;"><b>Sr. No.</b></td>
                                    <td valign="middle" align="left" style="padding: 5px;"><b>Charge Name</b></td>
                                    <td valign="middle" align="right" style="padding: 5px;"><b>Paid Amount ('.$rupSign.')</b></td>
                            </tr>
                            <tr>
                                    <td valign="middle" align="left" style="padding: 5px;">1</td>
                                    <td valign="middle" align="left" style="padding: 5px;">Bill Amount</td>
                                    <td valign="middle" align="right" style="padding: 5px;">{{amount}}</td>
                            </tr>
                            <tr>
                                    <td valign="middle" align="left" style="padding: 5px;">2</td>
                                    <td valign="middle" align="left" style="padding: 5px;">Customer Charge</td>
                                    <td valign="middle" align="right" style="padding: 5px;">{{customer_charge}}</td>
                            </tr>

                            <tr>
                                    <td valign="middle" align="right" colspan="2" style="padding: 5px;"><b>Sub Total: </b></td>
                                    <td valign="middle" align="right" style="padding: 5px;"><b>{{total_amount}}</b></td>
                            </tr>
                            <tr>
                                    <td valign="middle" align="right" colspan="2" style="padding: 5px;"><b>Total Paid Amount: </b></td>
                                    <td valign="middle" align="right" style="padding: 5px;"><b>{{total_amount}}</b></td>
                            </tr>
                            <tr>
                                    <td valign="middle" align="left" colspan="3" style="padding: 5px;">
                                        <br/>
                                        <b>Total Paid Amount(In Words):</b>{{amount_in_words}} Only

                                        <br/>
                                        <br/>
                                        <b>Note: This is computer generated receipt signature not required.</b>
                                    </td>
                            </tr>
                        </table>
                        
                </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding: 50px 15px 15px 15px;">
      <table align="center" border="0"  cellpadding="2" cellspacing="2" width="100%" height="15px" style="margin-top:15px">
        <tr>
          <td valign="middle" align="right">'."Receiver's Signature".'</td>
        </tr>
      </table>
    </td>
  </tr>
</table>');