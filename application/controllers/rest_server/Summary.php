<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Summary extends REST_Controller {

	public function __construct() { 
	    parent::__construct();
	    $this->load->helper(array('smartcard_helper','common_helper'));
            $this->apiCred = json_decode(SMART_CARD_CREDS,true);
            $header_value = $this->apiCred['X_API_KEY'];
            $this->header = array(
                'X-API-KEY:'.$header_value   
            );
           $this->load->model(array('Mainmodel','Summary_model'));


            $this->basicAuth = array(
            'username' => $this->apiCred['username'],
            'password' => $this->apiCred['password']
            );
            
            $request = $this->post();
            $user_id = $request['apploginuser'];
            $session_id= $this->Mainmodel->get_session_id($this->input->post('session_id'),$user_id);
	    $this->session_id = $session_id['itms_session_id'];
	    $this->user_id =itms_user_id;
       /*log code added start here */
        
        $log_data = array();
        $log_data['url'] = current_url();
        $log_data['Request_type'] = $_SERVER['REQUEST_METHOD'];
        $log_data['get'] = $this->input->get();
        $log_data['post'] = $this->post();
        $log_data['header'] = getallheaders();
        $log_data['user'] = $this->input->server('PHP_AUTH_USER');
        $log_data['ip'] = $this->input->ip_address();
            // show($log_data);
        log_data('rest/smartcard/smart_card_summary_api_' . date('d-m-Y') . '.log', $log_data, 'api');
        $insert_data = array('username' => $_POST['apploginuser'], 'ip' => $_SERVER['REMOTE_ADDR'], 'method' => $_SERVER['REQUEST_URI'], 'type' => 'get', 'version_id' => $_POST['version_id']);
        $this->requestId = $this->Mainmodel->insert_audit_data($insert_data)."";
        /*log code added end here */
        
        /*   added code disallow multiple login of same user code start here */
        $startTime = date("Y-m-d H:i:s");
        $cenvertedTime = date('Y-m-d H:i:s',strtotime(LOGIN_EXPIRE_TIME,strtotime($startTime))) ;
        $this->Mainmodel->upDateLastActivityTime($this->input->post('session_id'),$cenvertedTime);
        /*   added code disallow multiple login of same user code end here */ 
        
        
	}

	
        public function load_summary_post() 
        {

        	$dataInsert = array();
	        $URL= $this->apiCred['url'].'/Summary/load_summary';
	        $request = $this->post();
	        $itms_request = $this->post();
		$itms_request['apploginuser'] = $this->user_id ;
		$itms_request['session_id'] = $this->session_id ;
	        
	        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
		$json = json_decode($data);
	        log_data('rest/smartcard/smart_card_summary_api_' . date('d-m-Y') . '.log', $json, 'api');
	        if(strtolower($json->status)=="success")
	        {

	            $dataInsert = array(
	                    'apploginuser' => $this->input->post('apploginuser'),
	                    'summary_date' => $this->input->post('summary_date'),
	                    'session_id' => $this->input->post('session_id'),
	                    'depot_cd' => $this->input->post('depot_cd'),
	                    'terminal_id' => $this->input->post('terminal_id'),
	                    'summary_status' => 'P'
	                );
	          
	        
        	 $this->Summary_model->getSummaryData($dataInsert);
        	
	           	
	     }
		    $this->response_save($data);

             json_response($json);
	          
        
        }
        
         public function get_summary_post() {
	    
	    $URL= $this->apiCred['url'].'/Summary/get_summary';
	    $request = $this->post();
	    $itms_request = $this->post();
		$itms_request['apploginuser'] = $this->user_id ;
		$itms_request['session_id'] = $this->session_id ;
	    $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
	    $json = json_decode($data);
	    log_data('rest/smartcard/smart_card_summary_api_' . date('d-m-Y') . '.log', $json, 'api');
		// $this->output
		// 	->set_status_header(200)
		// 	->set_content_type('application/json', 'utf-8')
		// 	->set_output(json_encode($json));
		    $this->response_save($data);

		json_response($json);
	    
	}
        
         public function generate_summary_post() {
	    
	    $URL= $this->apiCred['url'].'/Summary/generate_summary';
	    $request = $this->post();
            $itms_request = $this->post();
            $itms_request['apploginuser'] = $this->user_id ;
	    $itms_request['session_id'] = $this->session_id ;
            
	    $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
	    $json = json_decode($data);
            log_data('rest/smartcard/smart_card_summary_api_' . date('d-m-Y') . '.log', $json, 'api');
            
            if(strtolower($json->status)=="success"){              
               $data = array(
                'apploginuser' => $request['apploginuser'],
                'session_id' => $request['session_id'],
                'summary_date' => $request['summary_date'],
                'summary_no' => $request['summary_no'],
                'summary_status' => 'P',
                'terminal_id' => '1',
            );
            
                $created_by = $request['apploginuser'];
                $summary_date = $request['summary_date'];
                $summary_status = 'P';  

                $updateId = $this->Summary_model->updateSummaryStatusInSummary($created_by, $summary_date, $summary_status);

                $updateId = $this->Summary_model->updateSummaryData($data);
                
                $summaryArr = $this->Summary_model->getSummaryData($data);
                

               // $message = array('status' => 'success', 'msg' => 'success', 'data' => $summaryArr);
               // json_encode($this->response_save($message,200));
            }
            		    $this->response_save($data);

            json_response($json);
//	    $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
	    
	}
        
       public function close_summary_post() {
	    
	    $URL= $this->apiCred['url'].'/Summary/close_summary';
	    $request = $this->post();
            
            $itms_request = $this->post();
            $itms_request['apploginuser'] = $this->user_id ;
	    $itms_request['session_id'] = $this->session_id ;            
	    $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);            
            
	    $json = json_decode($data);
            log_data('rest/smartcard/smart_card_summary_api_' . date('d-m-Y') . '.log', $json, 'api');
            if(strtolower($json->status)=="success"){

                $data = array(
                    'apploginuser' => $request['apploginuser'],
                    'session_id' => $request['session_id'],
                    'summary_date' => $request['summary_date'],
                    'summary_no' => $request['summary_no'],
                    'depot_cd' => $request['depot_cd'],
                    'terminal_id' => $request['terminal_id'],
                    'summary_status' => 'C'
                );

                $created_by = $request['apploginuser'];
                $summary_date = $request['summary_date'];
                $summary_no = $request['summary_no'];
                $summary_status = 'C';   

                $this->Summary_model->updateSummaryStatusInSummary($created_by, $summary_date, $summary_status);

                $this->Summary_model->updateSummaryData($data);

                $summaryArr = $this->Summary_model->getSummary($created_by,$summary_date,$summary_status,$summary_no);

//                $message = array('status' => 'success', 'msg' => 'success', 'data' => $summaryArr);
//                json_encode($this->response_save($message,200));
        } 
//	    $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        		    $this->response_save($data);

        json_response($json);
	    
	}
       

       

}