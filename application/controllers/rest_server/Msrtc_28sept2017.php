<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Msrtc extends REST_Controller {

    var $db2;

    function __construct() {
        parent::__construct();

        ini_set('memory_limit', '1024M');

        //$this->db2 = $this->load->database('local_up_db', TRUE);
        $this->db2 = $this->load->database('default', TRUE);
        $model_array = array('wallet_model', 'users_model','wallet_trans_model');

        $this->load->model($model_array);

        $data = array();
        $data['url'] = current_url();
        $data['Request_type'] = $_SERVER['REQUEST_METHOD'];
        $data['get'] = $this->input->get();
        $data['post'] = $this->post();
        $data['api_post'] = $this->post();
        $data['header'] = getallheaders();
        $data['user'] = $this->input->server('PHP_AUTH_USER');
        $data['ip'] = $this->input->ip_address();
        $this->pos_encryption_key = 'QWRTEfnfdys635';
        log_data('rest/msrtc_new/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $data, 'api');
    }

    /* @Author      : Maunica Shah
     * @function    : getRetailerBalance
     * @param       : $agent_code
     * @detail      : To return wallet balance.
     */

    function getRetailerBalance_post() {
        $input = $this->post();
        $config = array(
            array(
                'field' => 'agent_code',
                'label' => 'agent_code',
                'rules' => 'trim|required',
                'errors' => array('required' => 'Agent code required')
            ),
        );

        if (form_validate_rules($config) == FALSE) {
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
        } else {
            $find_user_id = $this->users_model->where('agent_code', $input['agent_code'])->find_all();
            if (empty($find_user_id)) {
                $this->response(array('status' => 'failed', 'msg' => 'please provide valid Agent code'), 200);
            } else {
                $data = $this->wallet_model->where('user_id', $find_user_id[0]->id)->column('user_id,amt as balance')->find_all();
                log_data('rest/getRetailerBalance/' . date('M') . '/' . date('d-m-Y') . '/response' . '.log', $data, 'api');
                if (!empty($data)) {
                    $this->response(array('status' => 'success', 'data' => $data));
                } else {
                    $this->response(array('status' => 'failed', 'data' => 'No details found'));
                }
            }
        }
    }

    function getRetailerDetails_post() {
        $input = $this->post();
        $agent_code = trim($input['agent_code']);
        $where_condition = array('agent_code' => $input['agent_code'], 'role_id' => RETAILER_ROLE_ID);
        $find_user_id = $this->users_model->where($where_condition)->find_all();

        $config = array(
            array(
                'field' => 'agent_code',
                'label' => 'agent_code',
                'rules' => 'trim|required',
                'errors' => array('required' => 'Agent code required')
            ),
        );
        if (form_validate_rules($config) == FALSE) {
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
        } else {
            if (isset($find_user_id) && $find_user_id != '') {
                if (isset($agent_code) && $agent_code != '') {
                    $this->db->select("id,first_name,last_name,email,sec_email,gender,address1,address2,pincode,pancard,adharcard,mobile_no,dob,depot_name,depot_cd,agent_code");
                    $this->db->from("users");
                    $this->db->where("agent_code", $agent_code);
                    $query = $this->db->get();
                    $data = $query->result_array();
                    log_data('rest/getAllRetailers/' . date('M') . '/' . date('d-m-Y') . '/response' . '.log', $data, 'api');
                    $data[0]['email'] = rokad_decrypt($data[0]['email'], $this->pos_encryption_key);
                    $data[0]['pincode'] = rokad_decrypt($data[0]['pincode'], $this->pos_encryption_key);
                    $data[0]['mobile_no'] = rokad_decrypt($data[0]['mobile_no'], $this->pos_encryption_key);
                    $data[0]['dob'] = rokad_decrypt($data[0]['dob'], $this->pos_encryption_key);
                    $data[0]['pancard'] = rokad_decrypt($data[0]['pancard'], $this->pos_encryption_key);
                    $data[0]['adharcard'] = rokad_decrypt($data[0]['adharcard'], $this->pos_encryption_key);
                    if ($data) {
                        $this->response(array('status' => 'Success', 'data' => $data));
                    } else {
                        $this->response(array('status' => 'Failed', 'error' => 'No details found for this Agent code'));
                    }
                } else {
                    $this->response(array('status' => 'Failed', 'error' => 'Please provide Agent Code'));
                }
            } else {
                $this->response(array('status' => 'Failed', 'error' => 'Please enter Agent code for retailer only.'));
            }
        }
    }


    function getTransactionID_post()
    {
            $input = $this->post();

            $config = array(
                array(
                    'field' => 'agent_code',
                    'label' => 'agent code',
                    'rules' => 'trim|required',
                    'errors' => array('required' => 'Agent code required')
                ),
                array(
                    'field' => 'amount',
                    'label' => 'Amount',
                    'rules' => 'trim|required',
                'errors' => array('required' => 'Amount is required')
                ),
               /* array(
                    'field' => 'waybill_no',
                    'label' => 'Waybill no',
                    'rules' => 'trim|required',
                    'errors' => array('required' => 'Waybill no is mandatory')
                )*/
            );

            if (form_validate_rules($config) == FALSE) {
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {
                $aUser = $this->users_model->where('agent_code', $input['agent_code'])->find_all();
                if (empty($aUser)) {
                    $this->response(array('status' => 'failed', 'msg' => 'please provide valid Agent code'), 200);
                } else {
                    $data = $this->wallet_model->where('user_id', $aUser[0]->id)->find_all();

                    log_data('rest/getTransactionID/' . date('M') . '/' . date('d-m-Y') . '/response' . '.log', $data, 'api');

                    if (!empty($data)) {
                        
                        $transaction_no  = substr(hexdec(uniqid()), 4, 12);
                        $wallet_amt      = $data[0]->amt;
                        $amount_to_deduct = $input['amount'];

                        if($amount_to_deduct > $wallet_amt)
                        {
                            $this->response(array('status' => 'failed', 'msg' => 'Insufficient wallet balance'), 200);
                        }
                        else
                        {
                            $balance_to_update = $wallet_amt - $amount_to_deduct;

                            $wallet_trans_detail['transaction_no'] = $transaction_no;
                            $wallet_trans_detail['w_id'] = $data[0]->id;
                            $wallet_trans_detail['amt'] =  $input['amount'];
                            $wallet_trans_detail['wallet_type'] =  'actual_wallet';
                            $wallet_trans_detail['comment'] = 'MSRTC waybill deduction';
                            $wallet_trans_detail['status'] = 'debited';
                            $wallet_trans_detail['user_id'] = $aUser[0]->id;
                            $wallet_trans_detail['added_by'] = $aUser[0]->id;
                            $wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                            $wallet_trans_detail['amt_before_trans'] = $wallet_amt;
                            $wallet_trans_detail['amt_after_trans'] = $balance_to_update;
                            $wallet_trans_detail['amt_transfer_from'] = $aUser[0]->id;
                            $wallet_trans_detail['transaction_type_id'] = '11';
                            $wallet_trans_detail['transaction_type'] = 'Debited';
                            //$wallet_trans_detail['waybill_no'] = $input['waybill_no'];
                            $wallet_trans_detail['is_status'] = 'Y';


                             $wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail);

                             if($wallet_trans_id) 
                             {  
                                $agent_code = rokad_encrypt($input['agent_code'],$this->pos_encryption_key);
                                $transaction_no = rokad_encrypt($transaction_no,$this->pos_encryption_key);

                                $update_data = array(
                                'amt'      => $balance_to_update,
                                'last_transction_id'=>$wallet_trans_id,
                                'comment'  => "Updated wallet balance",
                                'updated_by' => $aUser[0]->id,
                                'updated_date' => date("Y-m-d H:i:s")
                                );
                                $this->db->where('id',$data[0]->id);
                                $this->db->update('wallet',$update_data);

                                $response_arr =  array('agent_code'=>$agent_code,'transaction_no'=>$transaction_no);
                                $this->response(array('status' => 'success', 'data' => $response_arr), 200);            
                             }
                             else
                             {
                                $this->response(array('status' => 'failed', 'msg' => "Some error occured !!"), 200);   
                             }
                        }
                        

                        $this->response(array('status' => 'success', 'data' => $data));
                    } else {
                        $this->response(array('status' => 'failed', 'data' => 'No details found'));
                    }
                }
            }
    }

    function saveWaybill_post()
    {
            $input = $this->post();

            $config = array(
                array(
                    'field' => 'transaction_no',
                    'label' => 'Transaction Number',
                    'rules' => 'trim|required',
                    'errors' => array('required' => 'Transaction number is required')
                ),
               array(
                    'field' => 'waybill_no',
                    'label' => 'Waybill no',
                    'rules' => 'trim|required',
                    'errors' => array('required' => 'Waybill no is mandatory')
                )       
            );

            if (form_validate_rules($config) == FALSE) {
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {
                $wallet_info = $this->wallet_trans_model->where('transaction_no', $input['transaction_no'])->find_all();
                if(!empty($wallet_info))
                {
                    $wallet_trans_id = $wallet_info[0]->id;
                    $wallet_data = array(
                    'waybill_no' => $input['waybill_no'],
                    'comment'  => "Updated waybill number"
                    );
                    $this->db->where('id',$wallet_trans_id);
                    $this->db->where('transaction_no',$input['transaction_no']);
                    if($this->db->update('wallet_trans',$wallet_data))
                    {
                        $this->response(array('status' => 'success', 'msg' => 'Waybill number updated successfully'));       
                    }
                    else
                    {
                        $this->response(array('status' => 'failed', 'msg' => 'Something went wrong !!'));          
                    }

                }
                else
                {
                    $this->response(array('status' => 'failed', 'msg' => 'Invalid transaction number'));
                }
                
            }
    }
    
    function cancelWaybill_post()
    {
            $input = $this->post();
            $config = array(
                array(
                    'field' => 'agent_code',
                    'label' => 'agent code',
                    'rules' => 'trim|required',
                    'errors' => array('required' => 'Agent code required')
                ),
                array(
                    'field' => 'amount',
                    'label' => 'Amount',
                    'rules' => 'trim|required',
                    'errors' => array('required' => 'Amount is required')
                ),
                array(
                    'field' => 'waybill_no',
                    'label' => 'Waybill no',
                    'rules' => 'trim|required',
                    'errors' => array('required' => 'Waybill no is mandatory')
                )
            );

            if (form_validate_rules($config) == FALSE) {
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {
                $aUser = $this->users_model->where('agent_code', $input['agent_code'])->find_all();
                if (empty($aUser)) {
                    $this->response(array('status' => 'failed', 'msg' => 'please provide valid Agent code'), 200);
                } else {
                    $data = $this->wallet_model->where('user_id', $aUser[0]->id)->find_all();

                    log_data('rest/cancelWaybill_post/' . date('M') . '/' . date('d-m-Y') . '/response' . '.log', $data, 'api');

                    if (!empty($data)) {
                        
                        $transaction_no  = substr(hexdec(uniqid()), 4, 12);
                        $wallet_amt      = $data[0]->amt;
                        $amount_to_add = $input['amount'];

                        if($amount_to_add > 0)
                        {
                        $balance_to_update = $wallet_amt + $amount_to_add;

                        $wallet_trans_detail['transaction_no'] = $transaction_no;
                        $wallet_trans_detail['w_id'] = $data[0]->id;
                        $wallet_trans_detail['amt'] =  $input['amount'];
                        $wallet_trans_detail['wallet_type'] =  'actual_wallet';
                        $wallet_trans_detail['comment'] = 'MSRTC cancel waybill addition';
                        $wallet_trans_detail['status'] = 'credited';
                        $wallet_trans_detail['user_id'] = $aUser[0]->id;
                        $wallet_trans_detail['added_by'] = $aUser[0]->id;
                        $wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                        $wallet_trans_detail['amt_before_trans'] = $wallet_amt;
                        $wallet_trans_detail['amt_after_trans'] = $balance_to_update;
                        $wallet_trans_detail['amt_transfer_from'] = $aUser[0]->id;
                        $wallet_trans_detail['transaction_type_id'] = '3';
                        $wallet_trans_detail['transaction_type'] = 'Credited';
                        $wallet_trans_detail['waybill_no'] = $input['waybill_no'];
                        $wallet_trans_detail['is_status'] = 'Y';


                         $wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail);

                         if($wallet_trans_id) 
                         {  
                            $agent_code = rokad_encrypt($input['agent_code'],$this->pos_encryption_key);
                            $transaction_no = rokad_encrypt($transaction_no,$this->pos_encryption_key);

                            $update_data = array(
                            'amt'      => $balance_to_update,
                            'last_transction_id'=>$wallet_trans_id,
                            'comment'  => "Updated wallet balance",
                            'updated_by' => $aUser[0]->id,
                            'updated_date' => date("Y-m-d H:i:s")
                            );
                            $this->db->where('id',$data[0]->id);
                            $this->db->update('wallet',$update_data);

                            $response_arr =  array('agent_code'=>$agent_code,'transaction_no'=>$transaction_no);
                            $this->response(array('status' => 'success', 'data' => $response_arr), 200);            
                         }
                         else
                         {
                            $this->response(array('status' => 'failed', 'msg' => "Some error occured !!"), 200);   
                         }
                        
                        
                    }
                        $this->response(array('status' => 'success', 'data' => $data));
                    } else {
                        $this->response(array('status' => 'failed', 'data' => 'No details found'));
                    }
                }
            }
    }
}
