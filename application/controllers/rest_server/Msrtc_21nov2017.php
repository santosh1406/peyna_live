<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Msrtc extends REST_Controller {

    var $db2;

    function __construct() {
        parent::__construct();

        ini_set('memory_limit', '1024M');

        //$this->db2 = $this->load->database('local_up_db', TRUE);
        $this->db2 = $this->load->database('default', TRUE);
        $model_array = array('wallet_model', 'users_model','wallet_trans_model','cb_msrtc_agent_transaction_model','cb_msrtc_missed_transaction_model');

        $this->load->model($model_array);

        $data = array();
        $data['url'] = current_url();
        $data['Request_type'] = $_SERVER['REQUEST_METHOD'];
        $data['get'] = $this->input->get();
        $data['post'] = $this->post();
        $data['api_post'] = $this->post();
        $data['header'] = getallheaders();
        $data['user'] = $this->input->server('PHP_AUTH_USER');
        $data['ip'] = $this->input->ip_address();
        $this->pos_encryption_key = 'QWRTEfnfdys635';
        log_data('rest/msrtc_new/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $data, 'api');
    }

    /* @Author      : Maunica Shah
     * @function    : getRetailerBalance
     * @param       : $agent_code
     * @detail      : To return wallet balance.
     */

    function getRetailerBalance_post() {
        $input = $this->post();
        $config = array(
            array(
                'field' => 'agent_code',
                'label' => 'agent_code',
                'rules' => 'trim|required',
                'errors' => array('required' => 'Agent code required')
            ),
        );

        if (form_validate_rules($config) == FALSE) {
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
        } else {
            $find_user_id = $this->users_model->where('agent_code', $input['agent_code'])->find_all();
            if (empty($find_user_id)) {
                $this->response(array('status' => 'failed', 'msg' => 'please provide valid Agent code'), 200);
            } else {
                $data = $this->wallet_model->where('user_id', $find_user_id[0]->id)->column('user_id,amt as balance')->find_all();
                log_data('rest/getRetailerBalance/' . date('M') . '/' . date('d-m-Y') . '/response' . '.log', $data, 'api');
                if (!empty($data)) {
                    $this->response(array('status' => 'success', 'data' => $data));
                } else {
                    $this->response(array('status' => 'failed', 'data' => 'No details found'));
                }
            }
        }
    }

    function getRetailerDetails_post() {
        $input = $this->post();
        $agent_code = trim($input['agent_code']);
        $where_condition = array('agent_code' => $input['agent_code'], 'role_id' => RETAILER_ROLE_ID);
        $find_user_id = $this->users_model->where($where_condition)->find_all();

        $config = array(
            array(
                'field' => 'agent_code',
                'label' => 'agent_code',
                'rules' => 'trim|required',
                'errors' => array('required' => 'Agent code required')
            ),
        );
        if (form_validate_rules($config) == FALSE) {
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
        } else {
            if (isset($find_user_id) && $find_user_id != '') {
                if (isset($agent_code) && $agent_code != '') {
                    $this->db->select("id,first_name,last_name,email,sec_email,gender,address1,address2,pincode,pancard,adharcard,mobile_no,dob,depot_name,depot_cd,agent_code");
                    $this->db->from("users");
                    $this->db->where("agent_code", $agent_code);
                    $query = $this->db->get();
                    $data = $query->result_array();
                    log_data('rest/getAllRetailers/' . date('M') . '/' . date('d-m-Y') . '/response' . '.log', $data, 'api');
                    $data[0]['email'] = rokad_decrypt($data[0]['email'], $this->pos_encryption_key);
                    $data[0]['pincode'] = rokad_decrypt($data[0]['pincode'], $this->pos_encryption_key);
                    $data[0]['mobile_no'] = rokad_decrypt($data[0]['mobile_no'], $this->pos_encryption_key);
                    $data[0]['dob'] = rokad_decrypt($data[0]['dob'], $this->pos_encryption_key);
                    $data[0]['pancard'] = rokad_decrypt($data[0]['pancard'], $this->pos_encryption_key);
                    $data[0]['adharcard'] = rokad_decrypt($data[0]['adharcard'], $this->pos_encryption_key);
                    if ($data) {
                        $this->response(array('status' => 'Success', 'data' => $data));
                    } else {
                        $this->response(array('status' => 'Failed', 'error' => 'No details found for this Agent code'));
                    }
                } else {
                    $this->response(array('status' => 'Failed', 'error' => 'Please provide Agent Code'));
                }
            } else {
                $this->response(array('status' => 'Failed', 'error' => 'Please enter Agent code for retailer only.'));
            }
        }
    }


    function getTransactionID_post()
    {
            $input = $this->post();
            $config = array(
                array(
                    'field' => 'agent_code',
                    'label' => 'agent code',
                    'rules' => 'trim|required',
                    'errors' => array('required' => 'Agent code required')
                ),
                array(
                    'field' => 'amount',
                    'label' => 'Amount',
                    'rules' => 'trim|required',
                'errors' => array('required' => 'Amount is required')
                ),
               /* array(
                    'field' => 'waybill_no',
                    'label' => 'Waybill no',
                    'rules' => 'trim|required',
                    'errors' => array('required' => 'Waybill no is mandatory')
                )*/
            );

            if (form_validate_rules($config) == FALSE) {
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {
                $aUser = $this->users_model->where('agent_code', $input['agent_code'])->find_all();
                if (empty($aUser)) {
                    $this->response(array('status' => 'failed', 'msg' => 'please provide valid Agent code'), 200);
                } else {
                    $data = $this->wallet_model->where('user_id', $aUser[0]->id)->find_all();

                    log_data('rest/getTransactionID/' . date('M') . '/' . date('d-m-Y') . '/response' . '.log', $data, 'api');

                    if (!empty($data)) {
                        
                        $transaction_no  = substr(hexdec(uniqid()), 4, 12);
                        $wallet_amt      = $data[0]->amt;
                        $amount_to_deduct = $input['amount'];

                        if($amount_to_deduct > $wallet_amt)
                        {
                            $this->response(array('status' => 'failed', 'msg' => 'Insufficient wallet balance'), 200);
                        }
                        else
                        {
                            $balance_to_update = $wallet_amt - $amount_to_deduct;

                            $wallet_trans_detail['transaction_no'] = $transaction_no;
                            $wallet_trans_detail['w_id'] = $data[0]->id;
                            $wallet_trans_detail['amt'] =  $input['amount'];
                            $wallet_trans_detail['wallet_type'] =  'actual_wallet';
                            $wallet_trans_detail['comment'] = 'MSRTC waybill deduction';
                            $wallet_trans_detail['status'] = 'debited';
                            $wallet_trans_detail['user_id'] = $aUser[0]->id;
                            $wallet_trans_detail['added_by'] = $aUser[0]->id;
                            $wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                            $wallet_trans_detail['amt_before_trans'] = $wallet_amt;
                            $wallet_trans_detail['amt_after_trans'] = $balance_to_update;
                            $wallet_trans_detail['amt_transfer_from'] = $aUser[0]->id;
                            $wallet_trans_detail['transaction_type_id'] = '11';
                            $wallet_trans_detail['transaction_type'] = 'Debited';
                            //$wallet_trans_detail['waybill_no'] = $input['waybill_no'];
                            $wallet_trans_detail['is_status'] = 'Y';


                             $wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail);

                             if($wallet_trans_id) 
                             {  
                                $agent_code = rokad_encrypt($input['agent_code'],$this->pos_encryption_key);
                                $transaction_no = rokad_encrypt($transaction_no,$this->pos_encryption_key);

                                $update_data = array(
                                'amt'      => $balance_to_update,
                                'last_transction_id'=>$wallet_trans_id,
                                'comment'  => "Updated wallet balance",
                                'updated_by' => $aUser[0]->id,
                                'updated_date' => date("Y-m-d H:i:s")
                                );
                                $this->db->where('id',$data[0]->id);
                                $this->db->update('wallet',$update_data);
	
                                $response_arr =  array('agent_code'=>$agent_code,'transaction_no'=>$transaction_no);
log_data('rest/getTransactionID/' . date('M') . '/' . date('d-m-Y') . '/response' . '.log', $response_arr, 'api');
                                $this->response(array('status' => 'success', 'data' => $response_arr), 200);            
                             }
                             else
                             {
                                $this->response(array('status' => 'failed', 'msg' => "Some error occured !!"), 200);   
                             }
                        }
                        

                        $this->response(array('status' => 'success', 'data' => $data));
                    } else {
                        $this->response(array('status' => 'failed', 'data' => 'No details found'));
                    }
                }
            }
    }

    function saveWaybill_post()
    {
            $input = $this->post();

            $config = array(
                array(
                    'field' => 'transaction_no',
                    'label' => 'Transaction Number',
                    'rules' => 'trim|required',
                    'errors' => array('required' => 'Transaction number is required')
                ),
               array(
                    'field' => 'waybill_no',
                    'label' => 'Waybill no',
                    'rules' => 'trim|required',
                    'errors' => array('required' => 'Waybill no is required')
                ) ,
               array(
                    'field' => 'agent_code',
                    'label' => 'Agent code',
                    'rules' => 'trim|required',
                    'errors' => array('required' => 'Agent code is required')
                ),    
                array(
                    'field' => 'topup_amt',
                    'label' => 'Topup Amount',
                    'rules' => 'trim|required|numeric',
                    'errors' => array('required' => 'Topup amount is required',
                                      'numeric' => 'Topup amount should be in numeric format')
                ),

                array(
                    'field' => 'msrtc_op_wallet_trans_id',
                    'label' => 'msrtc op_wallet trans_id',
                    'rules' => 'trim|required',
                    'errors' => array('required' => 'Msrtc op_wallet_trans_id number is required')
                )        
            );

            if (form_validate_rules($config) == FALSE) {
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {  
                $wallet_info = $this->wallet_trans_model->where('transaction_no', $input['transaction_no'])->find_all();
                if(!empty($wallet_info))
                {
                    $wallet_trans_id = $wallet_info[0]->id;
                    $wallet_data = array(
                    'waybill_no' => $input['waybill_no'],
                    'comment'  => "Updated waybill number"
                    );
                    $this->db->where('id',$wallet_trans_id);
                    $this->db->where('transaction_no',$input['transaction_no']);
                    if($this->db->update('wallet_trans',$wallet_data))
                    {
                        $user_info = $this->users_model->where('agent_code',$input['agent_code'])->find_all();
                        if(!empty($user_info)) 
                        {
                            $agent_id = $user_info[0]->id;
                            $transaction_data = array (
                                    'agent_id' => $agent_id,
                                    'agent_code' => $input['agent_code'],
                                    'waybill_no' => $input['waybill_no'],
                                    'waybill_topup_amt' => $input['topup_amt'],
                                    'waybill_process_date' => date('Y-m-d H:i:s'),
                                    'waybill_process_status' => 'Y','msrtc_op_wallet_trans_id'=>$input['msrtc_op_wallet_trans_id'],
                                    'wallet_transaction_no'=>$input['transaction_no']
                                );
                            $msrtc_agent_trans_id = $this->cb_msrtc_agent_transaction_model->insert($transaction_data);
                            if($msrtc_agent_trans_id)
                            {
                                $this->response(array('status' => 'success', 'msg' => 'Agent transaction details saved successfully'),200);    
                            } 
                            else {
                                $this->response(array('status'=> 'failed', 'msg' => 'Something went wrong !!'),200);
                            }
                        }
                        else {
                            $this->response(array('status' => 'failed', 'msg' => 'please provide valid Agent code'), 200);
                        }
                        $this->response(array('status' => 'success', 'msg' => 'Waybill number updated successfully'), 200);       
                    }
                    else
                    {
                        $this->response(array('status' => 'failed', 'msg' => 'Something went wrong !!'));          
                    }
                }
                else
                {
                    $this->response(array('status' => 'failed', 'msg' => 'Invalid transaction number'));
                }
                
            }
    }
    
    function updateAgentTransaction_post()
    {
        $input = $this->post();
        $config = array(
                array(
                    'field' => 'waybill_no',
                    'label' => 'Waybill no',
                    'rules' => 'trim|required',
                    'errors' => array('required' => 'Waybill no is required')
                    ),
                array(
                    'field' => 'agent_code',
                    'label'=> 'Agent code',
                    'rules' => 'trim|required',
                    'errors' => array('required' => 'Agent code is required')
                ),
                array(
                    'field' => 'collection_date',
                    'label' => 'Collection date',
                    'rules' => 'trim|required',
                    'errors' => array('required' => 'Collection date is required')
                ),
                 array(
                    'field' => 'total_trips',
                    'label' => 'Total trips',
                    'rules' => 'trim|required|numeric',
                    'errors' => array('required' => 'Total trips is required',
                                      'numeric' => 'Total trips should be in numeric format')
                    ),
                array(
                    'field' => 'consumed_amt',
                    'label' => 'Consumed amount',
                    'rules' => 'trim|required|numeric',
                    'errors' => array('required' => 'Consumed amount is required',
                                      'numeric' => 'Consumed amount should be in numeric format')
                   ),
                 array(
                    'field' => 'remaining_bal',
                    'label' => 'Remaining balance',
                    'rules' => 'trim|required|numeric',
                    'errors' => array('required' => 'Remaining balance is required',
                                      'numeric' => 'Remaining balance should be in numeric format')
                    )
            );
        if(form_validate_rules($config) == FALSE) {
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed' , 'error' => $error), 200);
        }
        else {

                $where_cond = array(
                    'waybill_no' => $input['waybill_no'],
                    'agent_code' => $input['agent_code']
                );
                $agent_trans_info = $this->cb_msrtc_agent_transaction_model->where($where_cond)->find_all();
                $trans_data = array(
                                    'collection_date' => $input['collection_date'],
                                    'total_trips' => $input['total_trips'],
                                    'consumed_amt' => $input['consumed_amt'],
                                    'remaining_bal' => $input['remaining_bal'],
                                    'collection_status' => 'Y'
                                ) ;

                if(!empty($agent_trans_info)) {
                   // if exists , update existing transaction details                    
                    $is_updated = $this->cb_msrtc_agent_transaction_model->update($where_cond, $trans_data);
                    if($is_updated) {
                        $this->response(array('status' => 'success', 'msg' => 'Agent transaction details updated succesfully'), 200);
                    } else {
                        $this->response(array('status' => 'failed', 'msg' => 'Something went wrong !!'), 200);
                    }
                } else {
                    // insert new transaction details  
                    $trans_data['waybill_no'] = $input['waybill_no'];
                    $trans_data['agent_code'] = $input['agent_code'];
                    $user_info = $this->users_model->where('agent_code',$input['agent_code'])->find_all();
                    if(!empty($user_info)) 
                    {
                        $trans_data['agent_id'] = $user_info[0]->id;
                        $missed_trans_id = $this->cb_msrtc_missed_transaction_model->insert($trans_data);
                        if($missed_trans_id){
                            $this->response(array('status' => 'success', 'msg' => 'Agent missed transaction details saved successfully'), 200);
                        } else {
                            $this->response(array('status' => 'failed', 'msg' => 'Something went wrong !!'), 200);
                        }
                    } else {
                        $this->response(array('status' => 'failed', 'msg' => 'please provide valid Agent code'), 200);
                    }

                }    
            } 
    }

    function cancelWaybill_post()
    {
            $input = $this->post();
            $config = array(
                array(
                    'field' => 'agent_code',
                    'label' => 'agent code',
                    'rules' => 'trim|required',
                    'errors' => array('required' => 'Agent code required')
                ),
                array(
                    'field' => 'amount',
                    'label' => 'Amount',
                    'rules' => 'trim|required',
                    'errors' => array('required' => 'Amount is required')
                ),
                array(
                    'field' => 'waybill_no',
                    'label' => 'Waybill no',
                    'rules' => 'trim|required',
                    'errors' => array('required' => 'Waybill no is mandatory')
                )
            );

            if (form_validate_rules($config) == FALSE) {
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {
                $aUser = $this->users_model->where('agent_code', $input['agent_code'])->find_all();
                if (empty($aUser)) {
                    $this->response(array('status' => 'failed', 'msg' => 'please provide valid Agent code'), 200);
                } else {
                    $data = $this->wallet_model->where('user_id', $aUser[0]->id)->find_all();

                    log_data('rest/cancelWaybill_post/' . date('M') . '/' . date('d-m-Y') . '/response' . '.log', $data, 'api');

                    if (!empty($data)) {
                        
                        $transaction_no  = substr(hexdec(uniqid()), 4, 12);
                        $wallet_amt      = $data[0]->amt;
                        $amount_to_add = $input['amount'];

                        if($amount_to_add > 0)
                        {
                        $balance_to_update = $wallet_amt + $amount_to_add;

                        $wallet_trans_detail['transaction_no'] = $transaction_no;
                        $wallet_trans_detail['w_id'] = $data[0]->id;
                        $wallet_trans_detail['amt'] =  $input['amount'];
                        $wallet_trans_detail['wallet_type'] =  'actual_wallet';
                        $wallet_trans_detail['comment'] = 'MSRTC cancel waybill addition';
                        $wallet_trans_detail['status'] = 'credited';
                        $wallet_trans_detail['user_id'] = $aUser[0]->id;
                        $wallet_trans_detail['added_by'] = $aUser[0]->id;
                        $wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                        $wallet_trans_detail['amt_before_trans'] = $wallet_amt;
                        $wallet_trans_detail['amt_after_trans'] = $balance_to_update;
                        $wallet_trans_detail['amt_transfer_from'] = $aUser[0]->id;
                        $wallet_trans_detail['transaction_type_id'] = '3';
                        $wallet_trans_detail['transaction_type'] = 'Credited';
                        $wallet_trans_detail['waybill_no'] = $input['waybill_no'];
                        $wallet_trans_detail['is_status'] = 'Y';


                         $wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail);

                         if($wallet_trans_id) 
                         {  
                            $agent_code = rokad_encrypt($input['agent_code'],$this->pos_encryption_key);
                            $transaction_no = rokad_encrypt($transaction_no,$this->pos_encryption_key);

                            $update_data = array(
                            'amt'      => $balance_to_update,
                            'last_transction_id'=>$wallet_trans_id,
                            'comment'  => "Updated wallet balance",
                            'updated_by' => $aUser[0]->id,
                            'updated_date' => date("Y-m-d H:i:s")
                            );
                            $this->db->where('id',$data[0]->id);
                            $this->db->update('wallet',$update_data);

                            $response_arr =  array('agent_code'=>$agent_code,'transaction_no'=>$transaction_no);
                            $this->response(array('status' => 'success', 'data' => $response_arr), 200);            
                         }
                         else
                         {
                            $this->response(array('status' => 'failed', 'msg' => "Some error occured !!"), 200);   
                         }
                        
                        
                    }
                        $this->response(array('status' => 'success', 'data' => $data));
                    } else {
                        $this->response(array('status' => 'failed', 'data' => 'No details found'));
                    }
                }
            }
    }


function login_post() 
    {
          
              $input = $this->post();
    
              $config = array(
              array(
                    'field' => 'mobile_no',
                    'label' => 'Mobile Number',
                    'rules' => 'trim|required',
                    'errors' => array('required' => 'Please enter your registered mobile number')
                    ),
              array(
                    'field' => 'password',
                    'label' => 'Password',
                    'rules' => 'trim|required',
                    'errors' => array('required' => 'Password is mandatory')
                    )
              );
              
              if (form_validate_rules($config) == FALSE)
              {
                  $error = $this->form_validation->error_array();
                  $this->response(array('status' => 'failed', 'error' => $error), 200);
              }
              else
              {
                  $mobile_no = trim($input['mobile_no']);
                  $password  = trim($input['password']);
                  $date = date('Y-m-d');
                  $encrypt_mobile = rokad_encrypt($mobile_no, $this->pos_encryption_key);
                  $where_condition = array('username' => $encrypt_mobile);
                  $aUser = $this->users_model->where($where_condition)->find_all(); 
                  $today=date("d-m-Y");
                  if(!empty($aUser) && $aUser[0]->status == 'Y')
                  {
                     $aUser_password = $aUser[0]->password;
                     $aUser_salt = $aUser[0]->salt;
                     $entered_pwd = md5($aUser_salt.$password);
                    
                     if($entered_pwd == $aUser_password)
                     {
                        $aData = array();
                        $user_id =  $aUser[0]->id;
                        $role_id = $aUser[0]->role_id;
                        $roleArray = $this->user_role_model->getRolename($role_id);
                        $country_name = $this->Country_master_model->get_country_by_id($aUser[0]->country);
                        $state_name   = $this->state_master_model->get_state_by_id($aUser[0]->state);
                        $city_name    = $this->city_master_model->get_city_by_id($aUser[0]->city);
                        
                        $aData[0]['id']                     = rokad_encrypt($user_id,$this->pos_encryption_key);
                         $aData[0]['first_name']             = rokad_encrypt($aUser[0]->first_name,$this->pos_encryption_key);
                         $aData[0]['last_name']              = rokad_encrypt($aUser[0]->last_name,$this->pos_encryption_key);
                         $aData[0]['username']               = $aUser[0]->username;
                         $aData[0]['gender']                 = rokad_encrypt($aUser[0]->gender,$this->pos_encryption_key);
                         $aData[0]['email']                  = $aUser[0]->email;
                         $aData[0]['address1']               = rokad_encrypt($aUser[0]->address1,$this->pos_encryption_key);
                         $aData[0]['address2']               = rokad_encrypt($aUser[0]->address2,$this->pos_encryption_key);
                         $aData[0]['dob']                    = $aUser[0]->dob;
                         $aData[0]['pincode']                = $aUser[0]->pincode;
                         $aData[0]['country']                = rokad_encrypt($country_name,$this->pos_encryption_key);
                         $aData[0]['state']                  = rokad_encrypt($state_name,$this->pos_encryption_key);
                         $aData[0]['city']                   = rokad_encrypt($city_name,$this->pos_encryption_key);
                         $aData[0]['role_id']                = rokad_encrypt($aUser[0]->role_id,$this->pos_encryption_key);
                         $aData[0]['role_name']              = rokad_encrypt($role_name,$this->pos_encryption_key);
                         $aData[0]['phone_no']  = $aUser[0]->mobile_no;
                         $aData[0]['agent_code']     = rokad_encrypt($aUser[0]->agent_code,$this->pos_encryption_key);
                         $aData[0]['current_date']     = rokad_encrypt($today,$this->pos_encryption_key);
                         
                         $where_clause = array(
                              'agent_id' => $aUser[0]->id,
                              'DATE(date_added)' => date('Y-m-d')
                          );

                        $login_exists = $this->login_count_model->where($where_clause)->count_all();
                        if($login_exists)
                        { 
                            $is_login_updated = $this->db->where($where_clause)->set('login_count','login_count+1',FALSE)->update('login_count');  

                            if(!$is_login_updated)
                            {
                                $this->response( array( 'status' => 'failed', 'msg' => 'Some error!'), 200);
                            }                   
                        } 
                        else 
                        {
                            $insert_data =  array( 
                                'agent_id'=> $aUser[0]->id,
                                'agent_code'=>$aUser[0]->agent_code,
                                'login_count' => 1 ,
                                'date_added' => date('Y-m-d H:i:s')
                              );
                            $insert_id = $this->login_count_model->insert($insert_data);
                            
                            if(!$insert_id)
                             {
                                $this->response( array( 'status' => 'failed', 'msg' => 'Some error!'), 200);
                            }
                        }

                        $login_count_detail = $this->login_count_model->where($where_clause)->find_all();

                        if(!empty($login_count_detail)) 
                        {
                            $aData[0]['login_attempts']  = rokad_encrypt($login_count_detail[0]->login_count,$this->pos_encryption_key);
                        }

                        $this->response(array('status' => 'success', 'data' => $aData), 200);
                     }
                     else
                     {
                        $this->response(array('status' => 'failed', 'data' => 'Invalid passwod'));
                     }
                     
                  }
                  else
                  {
                        $this->response(array('status' => 'failed', 'data' => 'User details not found'));
                  }

              }
    }
}
