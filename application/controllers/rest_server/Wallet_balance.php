<?php

/* @Author      : Vijay Ghadi
 * @function    : cyberplate
 * @param       : NA
 * @detail      : Wallet Balance Checking
 */

// Srl.        Date        Modified By            Why & What
// 01    29-10-2018        <Sonali Kamble>       1. To Show BOS Wallet Balance using API  

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Wallet_balance extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('wallet_balance_helper');
    }

    public function cyberplate_balance_get() {
        $check_return_array = cyberplate();
        if ($check_return_array['ERROR'] == 0) {
            $this->response(array('status' => 'success', 'data' => $check_return_array['REST'], 'msg' => 'Cyberplate Balance is :' . $check_return_array['REST']), 200);
        } else {
            $this->response(array('status' => 'failed', 'data' => $check_return_array['ERROR'], 'msg' => 'Error'), 200);
        }
    }

    public function bos_post() {
        
        $data = array();
        $input = $this->post();             

        if ($input['email'] == "" ) {
            $this->response(array('status' => 'Failed', 'data' => "", 'msg' => 'Please provide proper inputs.'), 200);
        } else { 
            
            $curl_url ="getBalance";
            $curlResult = curlForPostData_BOSRest($curl_url,$input);            
                   
            $this->response(array('status' => 'Success', 'data' => $curlResult), 200);
        
        }
    }

    public function jri_balance_get() {
        $check_return_array = jri_balance();
        
        // if ($check_return_array['ERROR'] == 0) {
            $this->response(array('status' => 'success', 'data' => $check_return_array, 'msg' => 'JRI Balance is :' . $check_return_array['Balance']), 200);
        // } else {
            // $this->response(array('status' => 'failed', 'data' => $check_return_array['ERROR'], 'msg' => 'Error'), 200);
        // }

    }

}

?>