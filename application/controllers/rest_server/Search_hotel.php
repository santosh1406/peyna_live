<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Search_hotel extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('hotel_helper'));        
        $this->apiCred = json_decode(BOS_HOTEL_CREDS, true);
    }

    public function hotel_services_post() {
        $curl_url = $this->apiCred['url'] . '/Hotel/search';
        $input = $this->post();   
        
        if ($input['checkInDate'] == "" || $input['checkOutDate'] == "" || $input['regionIds'] == "" || $input['rooms'] == "") {
             $this->response(array('status' => 'Failed', 'data' => "", 'msg' => 'Please provide proper inputs.'), 200);
         } else { 
        
        $curlResult = curlForPostData_BOS_Hotel($curl_url, $input,$this->apiCred); 
        //show($curlResult,1);
        if(strtolower($curlResult)== 'failed to connect to 180.92.175.107 port 80: timed out'){
            $response = $curlResult;
        }else{
            $response = json_decode($curlResult);
        }
        	
        //show($response,1);
        log_data('rest/hotel/'.date('d-m-Y').'/hotel_api_' . date('d-m-Y') . 'hotel_booking_post.log', $response, 'hotel_booking_post');            
	$this->response(array('status' => 'Success', 'data' => $response), 200);
         }

    }
    
    public function get_rooms_post() {
        $curl_url = $this->apiCred['url'] . '/Hotel/get_rooms';
        $input = $this->post();   
        
        if ($input['searchId'] == "" || $input['hotelIds'] == "") {
             $this->response(array('status' => 'Failed', 'data' => "", 'msg' => 'Please provide proper inputs.'), 200);
         } else { 
        
        $curlResult = curlForPostData_BOS_Hotel($curl_url, $input,$this->apiCred); 
        //show($curlResult,1);
        if(strtolower($curlResult)== 'failed to connect to 180.92.175.107 port 80: timed out'){
            $response = $curlResult;
        }else{
            $response = json_decode($curlResult);
        }
        	
        //show($response,1);
        log_data('rest/hotel/'.date('d-m-Y').'/hotel_api_' . date('d-m-Y') . 'hotel_booking_post.log', $response, 'get_rooms_post');            
	$this->response(array('status' => 'Success', 'data' => $response), 200);
         }

    }
    
    public function reprice_post(){
        $curl_url = $this->apiCred['url'] . '/Hotel/reprice';
        $input = $this->post();   
        
        if ($input['itineraryKey'] == "") {
             $this->response(array('status' => 'Failed', 'data' => "", 'msg' => 'Please provide proper inputs.'), 200);
        } else { 
        
            $curlResult = curlForPostData_BOS_Hotel($curl_url, $input,$this->apiCred); 
            //show($curlResult,1);
            if(strtolower($curlResult)== 'failed to connect to 180.92.175.107 port 80: timed out'){
                $response = $curlResult;
            }else{
                $response = json_decode($curlResult);
            }

            //show($response,1);
            log_data('rest/hotel/'.date('d-m-Y').'/hotel_api_' . date('d-m-Y') . 'hotel_booking_post.log', $response, 'reprise_post');            
            $this->response(array('status' => 'Success', 'data' => $response), 200);
        }
    }
    
    public function book_post(){
        $curl_url = $this->apiCred['url'] . '/Hotel/book';
        $input = $this->post();   
        
        if ($input['bookingKey'] == "" || $input['deliveryData'] == "" || $input['roomPaxDetails'] == "") {
             $this->response(array('status' => 'Failed', 'data' => "", 'msg' => 'Please provide proper inputs.'), 200);
        } else { 
            
            $curlResult = curlForPostData_BOS_Hotel($curl_url, $input,$this->apiCred); 
            //show($curlResult,1);
            if(strtolower($curlResult)== 'failed to connect to 180.92.175.107 port 80: timed out'){
                $response = $curlResult;
            }else{
                $response = json_decode($curlResult);
            }

            //show($response,1);
            log_data('rest/hotel/'.date('d-m-Y').'/hotel_api_' . date('d-m-Y') . 'hotel_booking_post.log', $response, 'book_post');            
            $this->response(array('status' => 'Success', 'data' => $response), 200);
        }
    }
    
    public function booking_retrieve_post(){
        $curl_url = $this->apiCred['url'] . '/Hotel/booking_retrieve';
        $input = $this->post();   
        
        if ($input['referenceNo'] == "" || $input['emailOrMobile'] == "" ) {
             $this->response(array('status' => 'Failed', 'data' => "", 'msg' => 'Please provide proper inputs.'), 200);
        } else { 
            
            $curlResult = curlForPostData_BOS_Hotel($curl_url, $input,$this->apiCred); 
            //show($curlResult,1);
            if(strtolower($curlResult)== 'failed to connect to 180.92.175.107 port 80: timed out'){
                $response = $curlResult;
            }else{
                $response = json_decode($curlResult);
            }

            //show($response,1);
            log_data('rest/hotel/'.date('d-m-Y').'/hotel_api_' . date('d-m-Y') . 'hotel_booking_retrieve_post.log', $response, 'booking_retrieve_post');            
            $this->response(array('status' => 'Success', 'data' => $response), 200);
        }
    }
    
    

}