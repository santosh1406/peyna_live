<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Card_management extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('smartcard_helper', 'common_helper'));

        $this->apiCred = json_decode(SMART_CARD_CREDS, true);

        $header_value = $this->apiCred['X_API_KEY'];
        $this->header = array(
            'X-API-KEY:' . $header_value
        );

        $this->basicAuth = array(
            'username' => $this->apiCred['username'],
            'password' => $this->apiCred['password']
        );

        $this->load->model(array('Mainmodel', 'Customer_registration_model', 'Passfare_model'));

        $request = $this->post();
        $user_id = $request['apploginuser'];
        $session_id = $this->Mainmodel->get_session_id($this->input->post('session_id'), $user_id);
        $this->session_id = $session_id['itms_session_id'];
        $this->user_id = itms_user_id;

        /* log code added start here */

        $log_data = array();
        $log_data['url'] = current_url();
        $log_data['Request_type'] = $_SERVER['REQUEST_METHOD'];
        $log_data['get'] = $this->input->get();
        $log_data['post'] = $this->post();
        $log_data['header'] = getallheaders();
        $log_data['user'] = $this->input->server('PHP_AUTH_USER');
        $log_data['ip'] = $this->input->ip_address();
        // show($log_data);
        log_data('rest/smartcard/smart_card_management_api_' . date('d-m-Y') . '.log', $log_data, 'api');
        $insert_data = array('username' => $_POST['apploginuser'], 'ip' => $_SERVER['REMOTE_ADDR'], 'method' => $_SERVER['REQUEST_URI'], 'type' => 'get', 'version_id' => $_POST['version_id']);
        $this->requestId = $this->Mainmodel->insert_audit_data($insert_data) . "";
        /* log code added end here */
        
        /*   added code disallow multiple login of same user code start here */
        $startTime = date("Y-m-d H:i:s");
        $cenvertedTime = date('Y-m-d H:i:s',strtotime(LOGIN_EXPIRE_TIME,strtotime($startTime))) ;
        $this->Mainmodel->upDateLastActivityTime($this->input->post('session_id'),$cenvertedTime);
        /*   added code disallow multiple login of same user code end here */
    }

    /** Add $nsert_data array for insert. * */
    public function card_issuance_post() {

        $URL = $this->apiCred['url'] . '/Card_management/card_issuance';

        $request = $this->post();

        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id;
        $itms_request['session_id'] = $this->session_id;
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);

        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_management_api_' . date('d-m-Y') . '.log', $data, 'api');

        if (strtolower($json->status) == 'success') {
            $insert_data = array(
                'cc_txn_id' => $request['cc_txn_id'],
                'flag' => $request['flag'],
                'sw_expiry' => (isset($request['sw_expiry']) ? $request['sw_expiry'] : ''),
                'tw_expiry' => $request['tw_expiry'],
                'cust_card_id' => $request['cust_card_id'],
                'sw_bal' => (isset($request['sw_bal']) ? $request['sw_bal'] : ''),
                'tw_bal' => $request['tw_bal'],
                'customer_id' => $request['customer_id'],
                'trimax_id' => $request['trimax_card_id'],
                'created_by' => date('Y-m-d H:i:s'),
            );
            $this->Mainmodel->insert_inssuance_data($insert_data, CARD_ISSUANCE);

            $card_trans_data = $this->Mainmodel->get_card_trans_id($request, 'asc');
            $card_trans_update['activation_date'] = $request['activation_date'];
            $card_trans_update['expiry_date'] = $request['expiry_date'];
            $card_trans_update['modified_by'] = $request['apploginuser'];
            $update_card_trans_res = $this->Mainmodel->update_card_trans($card_trans_data['id'], $card_trans_update);

            $cust_card_date = $this->Customer_registration_model->get_card_val_exp_date($card_trans_data['pass_concession_id']);

            if ($cust_card_date['expiry_type'] == 'C') {
                $cust_card_date['valid_from'] = $card_trans_update['activation_date'];
                $cust_card_date['valid_to'] = date($card_trans_update['activation_date'], strtotime("+" . $cust_card_date['valid_days'] . " days"));
            }

            $master_card_where = array('trimax_card_id' => $request['trimax_card_id'], 'is_active' => '1');
            $master_card_update = array('card_status' => 'D', 'cust_card_id' => $request['cust_card_id'], 'customer_id' => $request['customer_id'], 'modified_by' => $request['apploginuser']);
            $update_master_card_res = $this->Mainmodel->update_master_card($master_card_where, $master_card_update);



            $where = array('trimax_card_id' => $request['trimax_card_id'], 'card_status' => 'A', 'is_active' => '1');
            $update_data = array('card_validity_date' => $cust_card_date['valid_from'], 'card_expiry_date' => $cust_card_date['valid_to'], 'cust_card_id' => $request['cust_card_id'], 'dispatch_date' => TODAY_DATE, 'card_status' => 'D', 'modified_by' => $request['apploginuser']);

            $update_cust_card_res = $this->Mainmodel->update_cust_card($where, $update_data);

            $data = $this->Mainmodel->get_cust_info($request);
//			$this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        }
        //json_encode($this->response_save($json));
        json_response($json);
    }

    public function s2s_card_issuance_post() {

        $URL = $this->apiCred['url'] . '/Card_management/s2s_card_issuance';
        $request = $this->post();

        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id;
        $itms_request['session_id'] = $this->session_id;
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);

        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_management_api_' . date('d-m-Y') . '.log', $data, 'api');

        if (strtolower($json->status) == 'success') {
            $insert_data = array(
                'cc_txn_id' => $request['cc_txn_id'],
                'flag' => $request['flag'],
                'sw_expiry' => (isset($request['sw_expiry']) ? $request['sw_expiry'] : ''),
                'tw_expiry' => $request['tw_expiry'],
                'cust_card_id' => $request['cust_card_id'],
                'sw_bal' => (isset($request['sw_bal']) ? $request['sw_bal'] : ''),
                'tw_bal' => $request['tw_bal'],
                'trimax_id' => $request['t_id'],
                'created_date' => date('Y-m-d H:i:s'),
            );

            $this->Mainmodel->insert_inssuance_data($insert_data, CARD_ISSUANCE_S2S);
        }
//        $this->output
//                ->set_status_header(200)
//                ->set_content_type('application/json', 'utf-8')
//                ->set_output(json_encode($json));
        json_response($json);
    }

    public function card_surrender_post() {
        $URL = $this->apiCred['url'] . '/Card_management/card_surrender';
        $request = $this->post();

        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id;
        $itms_request['session_id'] = $this->session_id;
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);

        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_management_api_' . date('d-m-Y') . '.log', $data, 'api');

        if ($json->status == "success") {
            $cust_card_id = $this->Passfare_model->getCustCardIdByTrimaxID($request['trimax_card_id']);
            $insert_data = array(
                'cust_card_id' => $cust_card_id,
                'trimax_card_id' => $request['trimax_card_id'],
                'surrender_date' => date('Y-m-d H:i:s'),
                'amount' => $request['amount'],
                'surrender_method' => $request['surrenderMethod'],
                'created_by' => $request['apploginuser']
            );
            $this->Mainmodel->update_card_status($request, SURRENDER_STATUS, $insert_data, 'ps_report_card_surrender');
        }

//         $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
    }

    public function pre_card_issuance_post() {
        $URL = $this->apiCred['url'] . '/Card_management/pre_card_issuance';
        $request = $this->post();
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id;
        $itms_request['session_id'] = $this->session_id;
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_management_api_' . date('d-m-Y') . '.log', $data, 'api');

        if ($json->status == "success") 
         {
            $post_value = $this->input->post();
            $user_name = $this->input->post('apploginuser');
            $card_trans_data = $this->Mainmodel->get_card_trans_id($post_value, 'asc');
            if ($post_value['strCardReissued'] == 'false') {
                
                $data['card_activation_date'] = $json->data->card_activation_date;
        	$data['card_expiry_date'] = $json->data->card_expiry_date;
        	$update_cust_card_data = array('card_validity_date' => $cust_card_date['card_validity_date'], 'card_expiry_date' => $cust_card_date['card_expiry_date'], 'modified_by' => $user_name);
                $where = array('trimax_card_id' => $post_value['trimax_card_id'], 'card_status' => 'A');
                $this->Mainmodel->update_cust_card($where, $update_cust_card_data);
                $card_trans_update['activation_date'] = $json->data->pass_activation_date;;
                $card_trans_update['expiry_date'] = $json->data->pass_expiry_date;;
                $card_trans_update['modified_by'] = $user_name;
                $this->Mainmodel->update_card_trans($card_trans_data['id'], $card_trans_update);
            }
        }
         $this->response_save($data);
        json_response($json);
    }

    public function post_card_issuance_post() {
        $URL = $this->apiCred['url'] . '/Card_management/post_card_issuance';
        $request = $this->post();
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id;
        $itms_request['session_id'] = $this->session_id;
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
       
        log_data('rest/smartcard/smart_card_management_api_' . date('d-m-Y') . '.log', $data, 'api');

        if ($json->status == "success") {
            
            $user_name = $this->input->post('apploginuser');
            $post_value = $this->input->post();
            $lt_pos_req = json_decode($post_value['chargeList'],1);
            $posTxnNo = isset($post_value['cc_txn_id']) ? $post_value['cc_txn_id'] : '';
	    $posReqId = isset($lt_pos_req['posReqId']) ? $lt_pos_req['posReqId'] : '';
            
            $insert_data = array(
                'cc_txn_id' => $post_value['cc_txn_id'],
                'flag' => $post_value['flag'],
                'sw_expiry' => (isset($post_value['sw_expiry']) ? $post_value['sw_expiry'] : ''),
                'tw_expiry' => $post_value['tw_expiry'],
                'cust_card_id' => $post_value['cust_card_id'],
                'sw_bal' => (isset($post_value['sw_bal']) ? $post_value['sw_bal'] : ''),
                'tw_bal' => $post_value['tw_bal'],
                'customer_id' => $post_value['customer_id'],
                'trimax_id' => $post_value['trimax_card_id'],
                'terminal_id' => (isset($post_value['terminal_id']) ? $post_value['terminal_id'] : ''),
                'pos_status_code' => $post_value['pos_status_code'],
                'pos_request_id' => $posReqId,
                'created_by' => $post_value['apploginuser']
            );
            $this->Mainmodel->insert_inssuance_data($insert_data, CARD_ISSUANCE);
            $card_trans_data = $this->Mainmodel->get_card_trans_id($post_value, 'asc');
            
            $update_data = array('pos_request_id' => $posReqId, 'pos_txn_ref_no' => $posTxnNo);
            $this->Mainmodel->update_card_trans($card_trans_data['id'],$update_data);
            
            
            $where = array('trimax_card_id' => $post_value['trimax_card_id'], 'card_status' => 'A', 'is_active' => '1');
            $master_card_update = array('card_status' => DISPATCH_STATUS, 'cust_card_id' => $post_value['cust_card_id'], 'customer_id' => $post_value['customer_id'], 'modified_by' => $user_name);
            $update_master_card_res = $this->Mainmodel->update_master_card($where, $master_card_update);

            $update_cust_card_data['cust_card_id'] = $post_value['cust_card_id'];
            $update_cust_card_data['dispatch_date'] = TODAY_DATE;
            $update_cust_card_data['card_status'] = DISPATCH_STATUS;
            $update_cust_card_data['modified_by'] = $user_name;
            $update_cust_card_res = $this->Mainmodel->update_cust_card($where, $update_cust_card_data);
        }
       $this->response_save($data);
        json_response($json);
    }

}
