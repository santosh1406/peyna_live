<?php

/* Created by Shrasti Bansal*/
if (!defined('BASEPATH')) exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Login extends REST_Controller {
    var $db2;

    public function __construct() { 
        parent::__construct();
	    $this->load->helper('smartcard_helper');
          //  $this->load->helper('smartcard_helper');
            $this->apiCred = json_decode(SMART_CARD_CREDS,true);
            $header_value = $this->apiCred['X_API_KEY'];
            $this->header = array(
                'X-API-KEY:'.$header_value   
            );

            $this->basicAuth = array(
            'username' => $this->apiCred['username'],
            'password' => $this->apiCred['password']
            );
  	    $this->load->model('Users_model');
        $this->load->model('Mainmodel');
        

    }
  
    
    public function loginvalidate_post() 
    {
        $request = $this->post();

        
        $username=trim($request['username']);
        $pass = trim($request['password']);
        $username=rokad_encrypt($username, $this->config->item('pos_encryption_key'));

        $result =  $this->Mainmodel->get_loginDetails($username);
	//print_r($result);exit;
        $salt = $result['salt'];
         $password = md5($salt . $pass);

       if($result['username'] == $username && $result['password'] == $password )
       {
            $response_data = array();
            $terminal_id = $this->Mainmodel->get_terminal_id($this->input->post('terminal'),$username);
		   
            $check_login_rs = $this->Mainmodel->check_if_already_login($result['id']);
             $date1 = $check_login_rs['end_tm'] ; 
            if(empty($check_login_rs['session_id']))
             {
			 // do nothing
	     }
             else if(strtotime($date1) > strtotime(date("Y-m-d H:i:s")) ) 
             {
                    $message = array('status' => 'failure', 'msg' => 'User is already login');
                    $this->set_response($message, 200, FALSE);
                    return ;
             }
            $terminal_id= $terminal_id['terminal_id'];
            if(empty($terminal_id)) 
            {
                $message = array('status' => 'failure', 'msg' => 'Invalid terminal code');
                $this->set_response($message, 200, FALSE);
                return ;
            }  
                /*create session start*/
        // MSRTC lOGIN CODE START CALL 					 

					$URL= $this->apiCred['url'].'/Login/loginvalidate';
					$requestforItms = array('username'=>ITMSUSER
								 ,'password'=>md5(ITMSPASSWORD),
								  'terminal'=>$this->input->post('terminal')
					,'version_id'=>'1.2.9','ismd5'=>'true',    
                                            ); 
					// 'MSMCT00000010001'
					$dataMsrct = curlForPost($URL,$requestforItms,$this->basicAuth, $this->header);
					$MSRTC_json = json_decode($dataMsrct);
  		  // MSRTC lOGIN CODE END CALL 	
         if($MSRTC_json->status!='success')
          {
	    json_response($MSRTC_json);
          }else{	                               
                      
           $startTime = date("Y-m-d H:i:s");
           $cenvertedTime = date('Y-m-d H:i:s',strtotime(LOGIN_EXPIRE_TIME,strtotime($startTime))) ;
 
           //DATE_ADD(now(), INTERVAL 5 MINUTE)
            $data = array(
            /*'user_type_id' => $this->input->post('user_type'),*/
                                'itms_session_id' => $MSRTC_json->data->session_id,
            'user_id' => $result['id'],
            'app_type' => 'CITYAPI',
            'terminal_id' => $terminal_id,
            'last_activity' => '',
            'ip_addr' => $this->input->ip_address(),
            'end_tm'  => $cenvertedTime
        );
            
          log_data('rest/smart_card_login_api_' . date('d-m-Y') . '.log', $data, 'api');
         $return = $this->Mainmodel->insertSessiondata($data);

         if($return > 0) 
          {
                        $response_data = array(
                        'id' => $result['id'],
                        'user_code' => '',
                        'agent_code' => $result['agent_code'],
                        'user_name' => $result['username'],
                        'division_name' => $result['division_name'],
                        'division_code' => $result['division_code'],
                        'depo_name' =>$result['depo_name'], 
                        'depo_code' =>$result['depo_code'],
                        'terminal_id' => $terminal_id,
                        'role_id' => $result['role_id'],
                        'session_id' => $return,
                        'server_system_date' => TODAY_DATE,
                        'ip_address' => $this->input->ip_address(),
			'first_name' => $result['first_name'],
                        'last_name' => $result['first_name'],
                    );
           }
              
           /*create session end*/
			 $message = array('status' => 'success', 'msg' => 'Login Success', 'data' => $response_data);
			 json_encode($this->response($message));

            } 
       
         }
        else {
                         $message = array('status' => 'failure', 'msg' => 'Invalid credentials');
                               json_encode($this->response($message));

           }

    
        
    }
       
   
  function cryptoJsAesDecrypt($passphrase, $jsonString) {
        $jsondata = json_decode($jsonString, true);
        try {
            $salt = hex2bin($jsondata["s"]);
            $iv = hex2bin($jsondata["iv"]);
        } catch (Exception $e) {
            return null;
        }
        $ct = base64_decode($jsondata["ct"]);
        $concatedPassphrase = $passphrase . $salt;
        $md5 = array();
        $md5[0] = md5($concatedPassphrase, true);
        $result = $md5[0];
        for ($i = 1; $i < 3; $i++) {
            $md5[$i] = md5($md5[$i - 1] . $concatedPassphrase, true);
            $result .= $md5[$i];
        }
        $key = substr($result, 0, 32);
        $data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
        return json_decode($data, true);
    }

    function get_loginDetails($username) 
    {
       $this->db->select('*');
       $this->db->from('users au');
       $this->db->where('au.username',$username);
       $query = $this->db->get();
       $result = $query->row_array();
        return $result;
      
    }
    
    public function get_terminal_id($username) 
    {
        $this->db->select('retailer_service_mapping.terminal_id');
        $this->db->from('users');
        $this->db->join('retailer_service_mapping','retailer_service_mapping.agent_id = users.id');
        $this->db->where('au.user_name',$username);
        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }
    
    public function logout_post(){
 
    	$this->form_validation->set_rules('session_id','Terminal','trim|required|xss_clean');
		$this->form_validation->set_rules('apploginuser','User Name','trim|required|xss_clean');
		$this->form_validation->set_rules('version_id','Version Id','trim|required|xss_clean');

		if($this->form_validation->run()){

                $session_id = $this->input->post('session_id');
                $apploginuser = $this->input->post('apploginuser');
                
                $cenvertedTime = date("Y-m-d H:i:s");
                $updateSession = $this->Mainmodel->upDateLastActivityTime($session_id,$cenvertedTime);
			
    		if($updateSession){
			
		$session_id  = $this->Mainmodel->get_session_id($this->input->post('session_id'),$apploginuser);
                $session_id = $session_id['itms_session_id'];

               $URL= $this->apiCred['url'].'/Login/logout';
			   $requestforItms = array('session_id'=>$session_id
						    ,'apploginuser'=>itms_user_id
						    ,'version_id'=>'1.2.9'); 
			   $data = curlForPost($URL,$requestforItms,$this->basicAuth, $this->header);
			   $json = json_decode($data);
               json_response($json);

			} else {
				$message = array('status' => 'success', 'msg' => 'Logout Failed');
				$this->set_response($message, 200);
			}
			

		}else{
			$error = $this->form_validation->error_array();
            $message = array('status' => 'failure', 'msg' => 'Validation Error', 'data' => $error);
            $this->set_response($message, 200);
		}	
    }
    
     public function email_otp_post() 
      {
          
        $user_details= $this->Mainmodel->get_userdetailsByterminal_code($this->input->post('terminal_code'));
        if(empty($user_details))  {  
          $message = array('status' => 'failure', 'msg' => 'Invalid Termincal code');
          json_encode($this->response($message));
        }  
        $otp=$this->get_otp();
        
        $otp_data=array(
            'agent_id'=> $user_details['id'],
             'otp'=> $otp,
         );
         $otp_id = $this->Mainmodel->insert_otp_data($otp_data, OTP_MASTER);
         $email = rokad_decrypt($user_details['email'], $this->config->item('pos_encryption_key'));
         $mobile_no = rokad_decrypt($user_details['mobile_no'], $this->config->item('pos_encryption_key'));
         
         $response_data= array(
             'otp_request_id'=>$otp_id,
             'otp'=>$otp,
             'terminal_code'=>$this->input->post('terminal_code'),
             'agent_id'=>$user_details['id'],
             'agent_name'=>$user_details['first_name'].' '.$user_details['last_name'],
             'agent_email'=>$email,
             'agent_contact'=>$mobile_no,
         );
         
        if(!empty($otp_id))
        {  
          $message = array('status' => 'success', 'msg' => 'OTP generated successully', 'data' => $response_data);
          json_encode($this->response($message));
        }else{
          $message = array('status' => 'failure', 'msg' => 'Could not complete your request');
          json_encode($this->response($message));
         }
    }
    
    public function email_otp_resend_post() 
      {
       $otp_request_id=$this->input->post('otp_request_id') ;
       
       $otp_data = $this->Mainmodel->get_otp_data($otp_request_id,OTP_MASTER);
       
        if(empty($otp_data))  {  
          $message = array('status' => 'failure', 'msg' => 'Invalid otp request id');
          json_encode($this->response($message));
        } 
        $created_at = strtotime ( OTP_TIME_OUT , strtotime ( $otp_data['created_at'] ) ) ;
        
        $configurable_time =strtotime( date("Y-m-d H:i:s" ));
        if($created_at < $configurable_time)
        {
           $otp_data=array('is_expired'=>'Y');
           $this->Mainmodel->update_otp_data($otp_request_id,$otp_data, OTP_MASTER);
           $message = array('status' => 'failure', 'msg' => 'OTP is expired');
           json_encode($this->response($message));
         }
        
         $user_details= $this->Mainmodel->get_userdetailsByterminal_code($this->input->post('terminal_code'));
        if(empty($user_details))  {  
          $message = array('status' => 'failure', 'msg' => 'Invalid Termincal code');
          json_encode($this->response($message));
        }  
        $otp=$this->get_otp();
        
        $otp_data=array(
            'agent_id'=> $user_details['id'],
             'otp'=> $otp,
         );
         $otp_id = $this->Mainmodel->insert_otp_data($otp_data, OTP_MASTER);
         
         $otp_data=array('is_expired'=>'Y');
         $this->Mainmodel->update_otp_data($otp_request_id,$otp_data, OTP_MASTER);
         
         $email = rokad_decrypt($user_details['email'], $this->config->item('pos_encryption_key'));
         $mobile_no = rokad_decrypt($user_details['mobile_no'], $this->config->item('pos_encryption_key'));
         $response_data= array(
             'otp_request_id'=>$otp_id,
             'otp'=>$otp,
             'terminal_code'=>$this->input->post('terminal_code'),
             'agent_id'=>$user_details['id'],
             'agent_name'=>$user_details['first_name'].' '.$user_details['last_name'],
             'agent_email'=>$email,
             'agent_contact'=>$mobile_no,
         );
         
        if(!empty($otp_id))
        {  
          $message = array('status' => 'success', 'msg' => 'OTP generated successully', 'data' => $response_data);
          json_encode($this->response($message));
        }else{
          $message = array('status' => 'failure', 'msg' => 'Could not complete your request');
          json_encode($this->response($message));
         }
    }
    
     public function email_otp_validate_post() 
      {
        $user_details= $this->Mainmodel->get_userdetailsByterminal_code($this->input->post('terminal_code'));
        if(empty($user_details))  {  
          $message = array('status' => 'failure', 'msg' => 'Invalid Termincal code');
          json_encode($this->response($message));
        }  
       $otp=$this->input->post('otp') ;
       
       $otp_data = $this->Mainmodel->get_otp_data_by_otp($otp,$user_details,OTP_MASTER);
       
        if(empty($otp_data))  {  
          $message = array('status' => 'failure', 'msg' => 'Invalid otp, Please try again');
          json_encode($this->response($message));
        } 
        $created_at = strtotime ( OTP_TIME_OUT , strtotime ( $otp_data['created_at'] ) ) ;
        $configurable_time =strtotime( date("Y-m-d H:i:s" ));
        
        if($created_at < $configurable_time)
        {
           $otp_request_id=$otp_data['id'];
           $otp_data=array('is_expired'=>'Y');
           $this->Mainmodel->update_otp_data($otp_request_id,$otp_data, OTP_MASTER);
           $message = array('status' => 'failure', 'msg' => 'OTP is expired');
           json_encode($this->response($message));
         }
        
        $otp_request_id =  $otp_data['id'] ;
        $otp_data=array('is_expired'=>'Y');
        $this->Mainmodel->update_otp_data($otp_request_id,$otp_data, OTP_MASTER);
    
         $email = rokad_decrypt($user_details['email'], $this->config->item('pos_encryption_key'));
         $mobile_no = rokad_decrypt($user_details['mobile_no'], $this->config->item('pos_encryption_key'));
         
         $response_data= array(
             'terminal_code'=>$this->input->post('terminal_code'),
             'agent_id'=>$user_details['id'],
             'agent_name'=>$user_details['first_name'].' '.$user_details['last_name'],
             'agent_email'=>$email,
             'agent_contact'=>$mobile_no,
         );
         
          $message = array('status' => 'success', 'msg' => 'OTP is matched', 'data' => $response_data);
          json_encode($this->response($message));
    }
    
    function get_otp()
    {
          return $six_digit_random_number = mt_rand(100000, 999999);
    }
    
     public function get_updated_app_version_post(){ 
         
        $today =date('Y-m-d') ;
        $user_name  = $this->input->post('apploginuser');
        $version_id = $this->input->post('version_id');
        $session_id = $this->input->post('session_id');
        $depot_cd   = $this->input->post('depot_cd');
        $agent_code = $this->input->post('agent_id');
        $macaddress = $this->input->post('macaddress');
        $os         = $this->input->post('os'); 
        $currDeskAppVer = $this->input->post('currDeskAppVer');
        $username = $this->input->post('username');
        $terminal = $this->input->post('terminal');
        

        
            //$this->form_validation->set_rules('apploginuser','App Login User Id','trim|required|xss_clean');
            $this->form_validation->set_rules('version_id','Version Id','trim|required|xss_clean');
            //$this->form_validation->set_rules('session_id','Session Id','trim|required|xss_clean');
            
         //   $this->form_validation->set_rules('depot_cd','depot cd User Id','trim|required|xss_clean');
           // $this->form_validation->set_rules('agent_code','Agent Code','trim|required|xss_clean');
            $this->form_validation->set_rules('macaddress','Macaddress','trim|required|xss_clean');
            $this->form_validation->set_rules('os','os','trim|required|xss_clean');
            $this->form_validation->set_rules('currDeskAppVer','CurrDeskAppVer','trim|required|xss_clean');            
            $this->form_validation->set_rules('terminal','Terminal','trim|required|xss_clean');            
            $this->form_validation->set_rules('username','Username','trim|required|xss_clean');
            
	if($this->form_validation->run()){
            
            $user_data = $this->Mainmodel->CheckUserSession($this->input->post());
            //show($user_data,1);
            $is_valdation_failed= false ;
            
                if(empty($user_data)){

                   $is_valdation_failed= true ;
                   $responseData['responseMessage'] = "wrong session and user id";
                       $message = array('status' => 'failure', 'msg' => 'wrong session and user id', 'data' => $responseData);
                       $json = json_encode($message);
                       $json = json_decode($json);
                       json_response($json);
                }
                
                if(!empty($user_data)){

                    $start_tm =$user_data['start_tm'] ;
                    $start_tm_arr =explode(' ',$start_tm);
                    
                    if($today!=$start_tm_arr[0]) {
                        $is_valdation_failed= true ;
                        $responseData['responseMessage'] = "wrong session data.";
                        $message = array('status' => 'failure', 'msg' => 'wrong session data', 'data' => $responseData);
                        $json = json_encode($message);
                        $json = json_decode($json);
                        json_response($json);
                    }
                }
				  
                if($is_valdation_failed==false){

                                $data =array(
                                        'depot_cd'   =>  $depot_cd ? $depot_cd : '',
                                        'agent_code' =>  $agent_code ? $agent_code: '',
                                        'session_id' =>  $session_id ? $session_id : '',
                                        'version_id' =>  $version_id,
                                        'macaddress' =>  $macaddress,
                                        'os'         =>  $os,
                                        'created_by' =>  $user_name
                                );
                        $this->Mainmodel->insertData($data,'updated_app_version_downloaded_history');
                        $version_data = $this->Mainmodel->getCurrentVersion();
                        //show($version_data,1);
                        $windowsCommand = json_decode(json_encode('["cmd /c start cmd.exe /K ", "\" cd " , " && " ," && exit \""]'));
                        $version_data1= array('windowsCommand'=> $windowsCommand);
                        
                        $t = array_merge($version_data,$version_data1);
                        //show($t,1);
                        $message = array('status' => 'success', 'msg' => 'success', "data" => $t);
                        $json = json_encode($message);
                        $json = json_decode($json);
                        json_response($json);
            }
        } else {
                $error = $this->form_validation->error_array();
                $message = array('status' => 'failure', 'msg' => 'Error in given fields', 'data' => $error);
                $json = json_encode($message);
                $json = json_decode($json);
                json_response($json);
          }
       }
	 
     public function dowonloadLtposLib_post() 
       { 
           $version_data = $this->Mainmodel->getCurrentVersion();
           $message = array('status' => 'success', 'msg' => 'success', "data" => $version_data);
           $json = json_encode($message);
           $json = json_decode($json);
	    json_response($json);
       }
       
    // get terminal code - sonali on 24june19   
    public function getTerminalCd_post(){        
                
        $this->input->post('depot_cd');     
        $this->input->post('agent_id');
        $this->input->post('apploginuser');
        $this->input->post('session_id');
        $this->input->post('version_id');
        $username=trim($this->input->post('username'));
        
        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        
        if ($this->form_validation->run()) {
         
            $username = rokad_encrypt($username, $this->config->item('pos_encryption_key'));        
            $terminal_code = $this->Mainmodel->getTerminalCode($username); 
            if(!empty($terminal_code)){
                
                $message = array('status' => 'success', 'msg' => 'Get terminal code successfully','errorCode'=> 200, 'data' => ['terminal_code'=>$terminal_code['terminal_code']]);
                json_encode($this->response($message, 200));
            }else{
                $message = array('status' => 'failure', 'msg' => 'No terminal code for given user id','errorCode'=> 404, 'data' => '');
                json_encode($this->response($message, 200));
            }
            
        
        } else {
            $error = $this->form_validation->error_array();        
            $message = array('status' => 'failure', 'msg' => 'Error', 'data' => $error);
            $this->set_response($message, 200);
        }


    }
    
    


}

?>