<?php

/*
 * @Author       Vijay Ghadi
 * @created on   24-09-2018
 * @description  Api for Registration
 * 
 */

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Signup extends REST_Controller {

    protected $data;

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $model_array = array('users_model', 'agent_bank_detail_model', 'agent_model', 'retailer_service_mapping_model', 'buisness_type_model');
        $this->load->model($model_array);
        log_data('rest/signup/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $data, 'Signup_api');
    }

    /*
     * @function    : register_post
     * @param       : first_name, last_name, firm_name etc....
     * @detail      : To register user
     */

    function register_post() {
        $input = $this->post();

        $firm_name = isset($input ['firm_name']) ? trim($input ['firm_name']) : "";
        $first_name = isset($input ['first_name']) ? trim($input ['first_name']) : "";
        $last_name = isset($input ['last_name']) ? trim($input ['last_name']) : "";
        $display_name = $first_name . " " . $last_name;
        $email = isset($input ['email']) ? trim($input ['email']) : "";
        $company_gst_no = isset($input ['company_gst_no']) ? trim($input ['company_gst_no']) : "";
        $mobile_no = isset($input ['mobile_no']) ? trim($input ['mobile_no']) : "";
        $gender = isset($input ['gender']) ? trim($input ['gender']) : "";
        $dob = isset($input ['dob']) ? date('Y-m-d', strtotime($input ['dob'])) : "";
        $address1 = isset($input ['address1']) ? trim($input ['address1']) : "";
        $address2 = isset($input ['address2']) ? trim($input ['address2']) : "";
        $agent_state = isset($input ['agent_state']) ? trim($input ['agent_state']) : "";
        $agent_city = isset($input ['agent_city']) ? trim($input ['agent_city']) : "";
        $pincode = isset($input ['pincode']) ? trim($input ['pincode']) : "";
        $pancard = isset($input ['pancard']) ? trim($input ['pancard']) : "";
        $adharcard = isset($input ['adharcard']) ? trim($input ['adharcard']) : "";
		$country_id = COUNTRY_ID;
        /* pallavi new changes for addition feilds
          date 16-01-2019
         */

        $address2 = isset($input ['address2']) ? trim($input ['address2']) : "";
        $depot_name = isset($input ['depot_name']) ? trim($input ['depot_name']) : "";
        $stand_name = isset($input ['stand_name']) ? trim($input ['stand_name']) : "";
        $shop_location = isset($input ['shop_location']) ? trim($input ['shop_location']) : "";
        $agent_state1 = isset($input ['state1']) ? trim($input ['state1']) : "";
        $agent_city1 = isset($input ['city1']) ? trim($input ['city1']) : "";
        $pincode1 = isset($input ['pincode1']) ? trim($input ['pincode1']) : "";
        $refempname = isset($input ['emp_name']) ? trim($input ['emp_name']) : "";
        $refempmob = isset($input ['emp_mob']) ? trim($input ['emp_mob']) : "";
        $ref_emp_designation = isset($input ['ref_emp_designation']) ? trim($input ['ref_emp_designation']) : "";
        $current_business = isset($input ['current_business']) ? trim($input ['current_business']) : "";
        
        /*sonali code start */
        $payment_mode = isset($input ['payment_mode']) ? trim($input ['payment_mode']) : "";
        $cheque_no = isset($input ['cheque_no']) ? trim($input ['cheque_no']) : "";
        $cheque_date = isset($input ['cheque_date']) ? trim($input ['cheque_date']) : "";
        $branch = isset($input ['branch']) ? trim($input ['branch']) : "";
        $bank_name = isset($input ['bank_name']) ? trim($input ['bank_name']) : "";
        $amount = isset($input ['amount']) ? trim($input ['amount']) : "";
        
        
        /*sonali code end */

        // encryption done here
        $new_pancard = rokad_encrypt($pancard, $this->config->item("pos_encryption_key"));
        $new_adharcard = rokad_encrypt($adharcard, $this->config->item("pos_encryption_key"));
        $new_mobile = rokad_encrypt($mobile_no, $this->config->item("pos_encryption_key"));
        $new_pincode = rokad_encrypt($pincode, $this->config->item("pos_encryption_key"));
        $new_email = rokad_encrypt($email, $this->config->item("pos_encryption_key"));
        $new_dob = rokad_encrypt($dob, $this->config->item("pos_encryption_key"));

        //by pallavi
        $new_pincode1 = rokad_encrypt($pincode1, $this->config->item("pos_encryption_key"));
        $new_mobile1 = rokad_encrypt($refempmob, $this->config->item("pos_encryption_key"));

        $ac_no = trim($input['accno']);
        $bank_nm = trim($input['bname']);
        $bran_nm = trim($input['brname']);
        $ifsc_code = trim($input['ifsc']);
        $accname = trim($input['accname']);

        $config = array(
            array(
                'field' => 'firm_name',
                'label' => 'Name of Firm/Company',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please enter name of firm/company',
                    'xss_clean' => 'CSRF is not allowed'
                )
            ),
            array(
                'field' => 'first_name',
                'label' => 'First Name',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please enter first name'
                )
            ),
            array(
                'field' => 'last_name',
                'label' => 'Last Name',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please enter last nanme'
                )
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|xss_clean|required|valid_email|callback_email_check',
                'errors' => array(
                    'required' => 'Please enter valid email',
                    'valid_email' => 'Please enter the valid email',
                    'is_unique' => 'Email id already exsist with us'
                )
            ),
            array(
                'field' => 'mobile_no',
                'label' => 'Mobile Numebr',
                'rules' => 'trim|xss_clean|required|min_length[10]|max_length[10]|callback_mobileno_check'
            ),
            array(
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'trim|xss_clean'
            ),
            array(
                'field' => 'dob',
                'label' => 'Date of birth',
                'rules' => 'trim|xss_clean|required|callback_validate_age',
                'errors' => array(
                    'required' => 'Please select your age'
                )
            ),
            array(
                'field' => 'address1',
                'label' => 'Address1',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please enter the address'
                )
            ),
            array(
                'field' => 'address2',
                'label' => 'Address2',
                'rules' => 'trim|xss_clean'
            ),
            array(
                'field' => 'agent_state',
                'label' => 'Agent State',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please select the state'
                )
            ),
            array(
                'field' => 'agent_city',
                'label' => 'Agent City',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please select the city'
                )
            ),
            array(
                'field' => 'pincode',
                'label' => 'Pincode',
                'rules' => 'trim|xss_clean|required|numeric|min_length[6]|max_length[6]',
                'errors' => array(
                    'required' => 'Please enter the pin code'
                )
            ),
            array(
                'field' => 'company_gst_no',
                'label' => 'company_gst_no',
                'rules' => 'trim|xss_clean|required|min_length[15]|max_length[15]',
                'errors' => array(
                    'required' => 'Please enter valid Company GST No'
                )
            ),
            array(
                'field' => 'pancard',
                'label' => 'Pancard',
                'rules' => 'trim|xss_clean|required|min_length[10]|max_length[10]',
                'errors' => array(
                    'required' => 'Please enter valid Pancard Number'
                )
            ),
            array(
                'field' => 'adharcard',
                'label' => 'Aadhaar Card',
                'rules' => 'trim|xss_clean|required|min_length[12]|max_length[12]',
                'errors' => array(
                    'required' => 'Please enter valid Aadhaar Card Number'
                )
            ),
            array(
                'field' => 'current_business',
                'label' => 'Current Business',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please enter Current Business'
                )
            ),
            array(
                'field' => 'current_business',
                'label' => 'Current Business',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please enter Current Business'
                )
            ),
            array(
                'field' => 'address2',
                'label' => 'Shop Address',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please enter the address'
                )
            ),
            array(
                'field' => 'depot_name',
                'label' => 'Depot Name',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please enter the Depot Name'
                )
            ),
            array(
                'field' => 'stand_name',
                'label' => 'Stand Name',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please enter the stand Name'
                )
            ),
            array(
                'field' => 'shop_location',
                'label' => 'Shop Location',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please enter the Shop Location'
                )
            ),
            array(
                'field' => 'shop_location',
                'label' => 'Shop Location',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please enter the Shop Location'
                )
            ),
            array(
                'field' => 'agent_state1',
                'label' => 'Shop State',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please select the state'
                )
            ),
            array(
                'field' => 'agent_city1',
                'label' => 'Shop City',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please select the city'
                )
            ),
            array(
                'field' => 'pincode1',
                'label' => 'Pincode',
                'rules' => 'trim|xss_clean|required|numeric|min_length[6]|max_length[6]',
                'errors' => array(
                    'required' => 'Please enter the pin code'
                )
            ),
            array(
                'field' => 'refempname',
                'label' => 'Reference Name',
                'rules' => 'trim|xss_clean'
            ),
            array(
                'field' => 'refempmob',
                'label' => 'Ref. Mobile Numebr',
                'rules' => 'trim|xss_clean|min_length[10]|max_length[10]|callback_mobileno_check'
            ),
            array(
                'field' => 'ref_emp_designation',
                'label' => 'Designation ',
                'rules' => 'trim|xss_clean'
            )
        );

        if (form_validate_rules($config) == FALSE) {
            $data['error'] = $this->form_validation->error_array();

            log_data('rest/signup_api_' . date('d-m-Y') . '.log', $data, 'signup_api error msg');
            $this->response(array('status' => 'failed', 'data' => $data), 200);
        } else {
            //show($input, 1);
            $selected_files = array();
            $selected_files = $_FILES;

            log_data('rest/signup_api_' . date('d-m-Y') . '.log', $input, 'signup_api log start');
            log_data('rest/signup_api_' . date('d-m-Y') . '.log', $selected_files, 'selected_files');

            if (sizeof($selected_files) == 3) {

                $this->users_model->username = $new_mobile;
                $this->users_model->firm_name = $firm_name;
                $this->users_model->first_name = $first_name;
                $this->users_model->last_name = $last_name;
                $this->users_model->gender = $gender;
                $this->users_model->email = $new_email;
                $this->users_model->company_gst_no = $company_gst_no;
                $this->users_model->address1 = $address1;
                $this->users_model->address2 = $address2;
                $this->users_model->pincode = $new_pincode;
                $this->users_model->pancard = $new_pancard;
                $this->users_model->adharcard = $new_adharcard;
                $this->users_model->mobile_no = $new_mobile;
                $this->users_model->country = $country_id;
                $this->users_model->state = $agent_state;
                $this->users_model->city = $agent_city;
                $this->users_model->dob = $new_dob;
                $this->users_model->display_name = $display_name;
                $this->users_model->kyc_verify_status = "N";
                $this->users_model->email_verify_status = "N";
                $this->users_model->verify_status = "N";
                $this->users_model->status = "N";
                $this->users_model->is_deleted = "N";
                $this->users_model->created_date = date("Y-m-d h:i:s:a");
                $this->users_model->ip_address = $this->input->ip_address();
                $this->users_model->user_agent = $this->input->user_agent();
                $this->users_model->reg_from = "F";
                $this->users_model->reg_status = "N";

                /* pallavi changes */
                $this->users_model->address2 = $address2;
                $this->users_model->depot_name = $depot_name;
                $this->users_model->stand_name = $stand_name;
                $this->users_model->shop_location = $shop_location;
                $this->users_model->state1 = $agent_state1;
                $this->users_model->city1 = $agent_city1;
                $this->users_model->pincode1 = $pincode1;
                $this->users_model->refempname = $refempname;
                $this->users_model->refempmob = $refempmob;
                $this->users_model->ref_emp_designation = $ref_emp_designation;
                $this->users_model->current_business = $current_business;

                /*sonali changes start*/
                
                $this->users_model->payment_mode  =  $payment_mode;
                $this->users_model->cheque_no = $cheque_no;
                $this->users_model->cheque_date = $cheque_date;
                $this->users_model->branch = $branch;
                $this->users_model->bank_name = $bank_name;
                $this->users_model->amount = $amount;
                
                        
                       
                
                /*sonali changes end*/


                //$upload_id = $this->upload_docs($input,1);
                $last_insert_id_users = $this->users_model->save();




                if (!empty($last_insert_id_users)) {


                    //by pallavi on 17-01-2019*/

                    $arr = explode(',', $input['services']);
                    if (!empty($arr)) {
                        if (in_array('10', $arr)) {
                            array_push($arr, '5');
                            array_push($arr, '6');
                        }

                        foreach ($arr as $service_id) {
                            $data = array(
                                'agent_id' => $last_insert_id_users,
                                'service_id' => $service_id,
                                'created_by' => $last_insert_id_users,
                                'updated_by' => $last_insert_id_users,
                                'updated_at' => date('Y-m-d H:i:s'),
                            );
                            $insert_service = $this->retailer_service_mapping_model->saveServiceDetails($data);
                        }

                        $sQuery = "UPDATE retailer_service_mapping_log SET status='N' WHERE agent_id='" . $last_insert_id_users . "'";
                        $query = $this->db->query($sQuery);

                        //added by harshada kulkarni on 13-07-2018 for CR 335 -start
                        $serviceArrStr = $input['services'];
                        $logData = array(
                            'agent_id' => $last_insert_id_users,
                            'service_id' => $serviceArrStr,
                            'status' => "Y",
                            'created_by' => $last_insert_id_users,
                            'created_at' => date('Y-m-d H:i:s'),
                        );
                        $insert_service_log = $this->retailer_service_mapping_model->saveServiceDetailsLog($logData);
                        //added by harshada kulkarni on 13-07-2018 for CR 335 -end
                    }


                    //end pallavi code for 
                    //File Uploading functionality
                    $ud = $this->upload_docs($input, $last_insert_id_users);
                    if ($ud) {
                        $this->agent_bank_detail_model->agent_id = $last_insert_id_users;
                        $this->agent_bank_detail_model->bank_acc_name = $accname;
                        $this->agent_bank_detail_model->bank_acc_no = $ac_no;
                        $this->agent_bank_detail_model->bank_name = $bank_nm;
                        $this->agent_bank_detail_model->bank_branch = $bran_nm;
                        $this->agent_bank_detail_model->bank_ifsc_code = $ifsc_code;
                        $this->agent_bank_detail_model->is_status = 1;
                        $this->agent_bank_detail_model->inserted_date = date('Y-m-d H:i:s');
                        $this->agent_bank_detail_model->is_deleted = 0;

                        $this->agent_bank_detail_model->save();

                        log_data('rest/signup_api_' . date('d-m-Y') . '.log', 'New User id -' . $last_insert_id_users, 'signup_api log end');
                        $this->response(array("status" => 'success', "msg" => 'User Registered Successfully'), 200);
                    } else {
                        if ($this->users_model->delete_user($last_insert_id_users)) {
                            log_data('rest/signup_api_' . date('d-m-Y') . '.log', 'failed to upload files', 'signup_api log end');
                            $this->response(array("status" => 'error', "msg" => 'Error while file uploading'), 200);
                        } else {
                            log_data('rest/signup_api_' . date('d-m-Y') . '.log', 'failed to upload files', 'signup_api log end');
                            $this->response(array("status" => 'error', "msg" => 'User created but Error while file uploading'), 200);
                        }
                    }
                } else {
                    log_data('rest/signup_api_' . date('d-m-Y') . '.log', 'failed to inset data into users table', 'signup_api log end');
                    $this->response(array("status" => 'failed', "msg" => 'Error while inserting data into users table'), 200);
                }
            } else {
                log_data('rest/signup_api_' . date('d-m-Y') . '.log', 'select all files for upload', 'signup_api log end');
                $this->response(array("status" => 'success', "msg" => 'Please select all files for upload'), 200);
            }
        }
    }

    public function mobileno_check($mobileno) {
        $new_mobileno = rokad_encrypt($mobileno, $this->config->item("pos_encryption_key"));
        $chk_mob = $this->users_model->where(array(
                    'mobile_no' => $new_mobileno,
                    "is_deleted" => 'N'
                ))->find_all();
        if ($chk_mob) {
            $this->form_validation->set_message('mobileno_check', 'The mobile no allready exsits with us.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function email_check($email) {
        $new_mail = rokad_encrypt($email, $this->config->item("pos_encryption_key"));
        $chk_email = $this->users_model->where(array(
                    'email' => $new_mail,
                    "is_deleted" => 'N'
                ))->find_all();
        //$this->response(array("status" => 'check_mail',"msg" => $chk_email));
        if ($chk_email) {
            $this->form_validation->set_message('email_check', 'The email id allready exsits with us.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function validate_age($age) {
        $dob = new DateTime($age);
        $now = new DateTime ();
        if ($now->diff($dob)->y >= 18) {
            return TRUE;
        } else {
            $this->form_validation->set_message('validate_age', 'Entered Age DOB should be above 18 years');
            return FALSE;
        }
    }

    function upload_docs($input, $id) {
        //show($input,1);
        $this->load->library('upload');
        $doccat = isset($input['doccat']) ? $input['doccat'] : "";
        $pi_doc = isset($input['docname1']) ? $input['docname1'] : " ";
        $pa_doc = isset($input['docname2']) ? $input['docname2'] : " ";
        $ph_doc = isset($input['docname3']) ? $input['docname3'] : " ";

        $doccat = explode(',', $doccat);

        //show($_FILES,1);
        $i = 0;
        $doc_nm[$i] = $pi_doc;
        $doc_nm[++$i] = $pa_doc;
        $doc_nm[++$i] = $ph_doc;


        $docname = $this->agent_model->selected_doc_name($doccat, $doc_nm);
        //show($docname,1);
        $send_data = array();

        if (!file_exists(AGENT_DOCS_FOLDER . '/' . $id)) {
            $old = umask(0);
            mkdir(AGENT_DOCS_FOLDER . '/' . $id, 0777, true);
            umask($old);
        }
        $subfolder = array("id_proof", 'add_proof', 'photo');
        for ($c = 0; $c < count($subfolder); $c++) {
            if (!file_exists(AGENT_DOCS_FOLDER . '/' . $id . '/' . $subfolder[$c] . '/large')) {
                mkdir(AGENT_DOCS_FOLDER . '/' . $id . '/' . $subfolder[$c] . '/large', 0777, true);
            }
            if (!file_exists(AGENT_DOCS_FOLDER . '/' . $id . '/' . $subfolder[$c] . '/thumb')) {
                mkdir(AGENT_DOCS_FOLDER . '/' . $id . '/' . $subfolder[$c] . '/thumb', 0777, true);
            }
        }

        //$files = $_FILES;
        //show($files,1); die;
        $i = 0;
        $files['img_doc']['name'][0] = $_FILES['img_doc1']['name'];
        $files['img_doc']['type'][0] = $_FILES['img_doc1']['type'];
        $files['img_doc']['tmp_name'][0] = $_FILES['img_doc1']['tmp_name'];
        $files['img_doc']['error'][0] = $_FILES['img_doc1']['error'];
        $files['img_doc']['size'][0] = $_FILES['img_doc1']['size'];

        $files['img_doc']['name'][1] = $_FILES['img_doc2']['name'];
        $files['img_doc']['type'][1] = $_FILES['img_doc2']['type'];
        $files['img_doc']['tmp_name'][1] = $_FILES['img_doc2']['tmp_name'];
        $files['img_doc']['error'][1] = $_FILES['img_doc2']['error'];
        $files['img_doc']['size'][1] = $_FILES['img_doc2']['size'];

        $files['img_doc']['name'][2] = $_FILES['img_doc3']['name'];
        $files['img_doc']['type'][2] = $_FILES['img_doc3']['type'];
        $files['img_doc']['tmp_name'][2] = $_FILES['img_doc3']['tmp_name'];
        $files['img_doc']['error'][2] = $_FILES['img_doc3']['error'];
        $files['img_doc']['size'][2] = $_FILES['img_doc3']['size'];

        //show($files,1); die;
        $img1_wid = AGENT_DOC_IMAGE_WIDTH;
        $img1_hei = AGENT_DOC_IMAGE_HEIGHT;
        $medium = 'thumb';
        $data = array();
        $i = 0;
        $cpt = count($files['img_doc']['name']);

        if ($_FILES && $_FILES['img_doc']['name'] !== "") {

            for ($i = 0; $i < $cpt; $i++) {

                $_FILES['img_doc']['name'] = $files['img_doc']['name'][$i];
                $_FILES['img_doc']['type'] = $files['img_doc']['type'][$i];
                $_FILES['img_doc']['tmp_name'] = $files['img_doc']['tmp_name'][$i];
                $_FILES['img_doc']['error'] = $files['img_doc']['error'][$i];
                $_FILES['img_doc']['size'] = $files['img_doc']['size'][$i];
                $info = explode('.', $files['img_doc']['name'][$i]);

                $file_ext = $info[1];
                $date = date_create();
                $time_stamp = date_timestamp_get($date);

                $file_name = str_replace(" ", "_", strtolower($docname[$i])) . "." . strtolower($file_ext);
                $pathname[$i] = $file_name;
                //show($input[$i]);

                if ($doccat[$i] != "" && $doccat[$i] == 1) {
                    $fold[$i] = 'id_proof';
                }
                if ($doccat[$i] != "" && $doccat[$i] == 2) {
                    $fold[$i] = 'add_proof';
                }
                if ($doccat[$i] != "" && $doccat[$i] == 3) {
                    $fold[$i] = 'photo';
                }
                $this->upload->initialize($this->set_upload_options($pathname[$i], $fold[$i], $id));

                if ($this->upload->do_upload('img_doc')) {
                    $img_data = $this->upload->data();

                    $old_path = $img_data['full_path'];
                    $new_path = AGENT_DOCS_FOLDER . $id . '/' . $fold[$i] . '/thumb/' . $file_name;

                    $this->resize($old_path, $new_path, AGENT_DOC_IMAGE_WIDTH, AGENT_DOC_IMAGE_HEIGHT);


                    $send_data[] = array("agent_id" => $id,
                        "docs_cat" => $doccat[$i],
                        "docs_name" => strtolower($doc_nm[$i]),
                        "large_img_link" => $pathname[$i],
                        "thum_img_link" => $pathname[$i],
                        "is_status" => 1,
                        "is_deleted" => 0,
                        "created_by" => $this->session->userdata('user_id'),
                    );
                } else {
                    $data['upload_errors'][$i] = $this->upload->display_errors();
                    return false;
                    //$this->response(array("status" => 'error', "msg" => 'Error while file uploading'), 200);
                }
            }
            $this->agent_model->save_agent_docs_detail_batch($send_data);
            return true;
        } else {
            //$this->response(array("status" => 'error', "msg" => 'Error while file uploading'), 200);
            return false;
        }
    }  

    private function set_upload_options($pathname, $folder, $uid) {
        $config = array();


        $config['upload_path'] = AGENT_DOCS_FOLDER . $uid . '/' . $folder . '/large';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['overwrite'] = FALSE;
        $config['file_name'] = $pathname;
        return $config;
    }

    public function resize($org_img, $new_img, $width, $height) {
        $this->load->library('image_lib');

        $config['image_library'] = 'gd2';
        $config['source_image'] = $org_img;
        $config['maintain_ratio'] = TRUE;
        $config['quality'] = "100";
        $config['new_image'] = $new_img;
        $config['width'] = $width;
        $this->image_lib->initialize($config);


        if ($this->image_lib->resize()) {
            return array('success' => true);
        } else {
            return $this->image_lib->display_errors();
        }
    }

 
}
