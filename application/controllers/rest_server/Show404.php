<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Show404 extends CI_Controller {

	public function index()
	{
		//redirect(base_url().'home');		
		load_front_view('show_404');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/Welcome.php */