<?php

/* Created by Shrasti Bansal*/
if (!defined('BASEPATH')) exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class OtpManager extends REST_Controller {
    var $db2;

    public function __construct() { 
        parent::__construct();
	    $this->load->helper('smartcard_helper');
          //  $this->load->helper('smartcard_helper');
            $this->apiCred = json_decode(SMART_CARD_CREDS,true);
            $header_value = $this->apiCred['X_API_KEY'];
            $this->header = array(
                'X-API-KEY:'.$header_value   
            );

            $this->basicAuth = array(
            'username' => $this->apiCred['username'],
            'password' => $this->apiCred['password']
            );
  	    $this->load->model('Users_model');
        $this->load->model('Mainmodel');
		   $log_data = array();
            $log_data['url'] = current_url();
            $log_data['Request_type'] = $_SERVER['REQUEST_METHOD'];
            $log_data['get'] = $this->input->get();
            $log_data['post'] = $this->post();
            $log_data['header'] = getallheaders();
            $log_data['user'] = $this->input->server('PHP_AUTH_USER');
            $log_data['ip'] = $this->input->ip_address();
        log_data('rest/smartcard/email_otp_api_' . date('d-m-Y') . '.log', $log_data, 'api');


    }
   
    
     public function email_otp_post() 
      {
	 $terminal_code= $this->input->post('terminal_code') ;
	     if( $terminal_code=='' )  {  
          $message = array('status' => 'failure', 'msg' => 'blank Terminal code');
		   log_data('rest/smartcard/email_otp_api_' . date('d-m-Y') . '.log',  $message, 'api');

          json_encode($this->response($message));
		  exit;
        }  
          
        $user_details= $this->Mainmodel->get_userdetailsByterminal_code($this->input->post('terminal_code'));
		
        if(empty($user_details) )  {  
          $message = array('status' => 'failure', 'msg' => 'Invalid Terminal code');
		   log_data('rest/smartcard/email_otp_api_' . date('d-m-Y') . '.log',  $message, 'api');

          json_encode($this->response($message));
		  exit;
        }  
        
        $otp_data = $this->Mainmodel->get_otp_data_by_user_id($user_details,OTP_MASTER);
        //print_r($otp_data);
       
        if(!empty($otp_data))  {  
          $message = array('status' => 'success', 'msg' => 'OTP was already generated, Please find otp_request_id','data'=>array('otp_request_id'=>$otp_data['id']));
		  	log_data('rest/smartcard/email_otp_api_' . date('d-m-Y') . '.log',  $message, 'api');

          json_encode($this->response($message));
		  exit;
        } 
        
        
        
        $otp=$this->get_otp();
        
        $otp_data=array(
            'agent_id'=> $user_details['id'],
             'otp'=> $otp,
         );
         $otp_id = $this->Mainmodel->insert_otp_data($otp_data, OTP_MASTER);
         $email = rokad_decrypt($user_details['email'], $this->config->item('pos_encryption_key'));
         $mobile_no = rokad_decrypt($user_details['mobile_no'], $this->config->item('pos_encryption_key'));
         
         $response_data= array(
             'otp_request_id'=>$otp_id,
             'otp'=>$otp,
             'terminal_code'=>$this->input->post('terminal_code'),
             'agent_id'=>$user_details['id'],
             'agent_name'=>$user_details['first_name'].' '.$user_details['last_name'],
             'agent_email'=>$email,
             'agent_contact'=>$mobile_no,
         );
         
        if(!empty($otp_id))
        {  
		  /*email code start here*/  
          $email = json_encode(array($email)) ;

          $replacements = array();
          $subject = 'Email OTP '; 
       //   $msg = "Email OTP ".$otp;
          $msg = $this->emailformat($otp);
          $mail_array = array("subject" => $subject,
                "message" => $msg,
                "to" => json_decode($email), 
	      );
          send_mail($mail_array, $replacements);
          /*email code end here*/    
			log_data('rest/smartcard/email_otp_api_' . date('d-m-Y') . '.log',  $response_data, 'api');

           $message = array('status' => 'success', 'msg' => 'OTP generated successfully', 'data' => $response_data);
           json_encode($this->response($message));
		   exit;
        }else{
				  log_data('rest/smartcard/email_otp_api_' . date('d-m-Y') . '.log',  $message, 'api');

          $message = array('status' => 'failure', 'msg' => 'Could not complete your request');
          json_encode($this->response($message));
		   exit;
         }
               
    }
    
    public function email_otp_resend_post() 
      {
       $otp_request_id=$this->input->post('otp_request_id') ;
       
       $otp_data = $this->Mainmodel->get_otp_data($otp_request_id,OTP_MASTER);
       
        if(empty($otp_data))  {  
          $message = array('status' => 'failure', 'msg' => 'Invalid otp request id');
		  				  log_data('rest/smartcard/email_otp_api_' . date('d-m-Y') . '.log',  $message, 'api');

          json_encode($this->response($message));
		   exit;
        } 
		
		$user_details= $this->Mainmodel->get_userdetailsByterminal_code($this->input->post('terminal_code'));
        if(empty($user_details))  {  
          $message = array('status' => 'failure', 'msg' => 'Invalid Terminal code');
		  				  log_data('rest/smartcard/email_otp_api_' . date('d-m-Y') . '.log',  $message, 'api');

          json_encode($this->response($message));
		   exit;
        } 
		 
        $created_at = strtotime ( OTP_TIME_OUT , strtotime ( $otp_data['created_at'] ) ) ;
        
        $configurable_time =strtotime( date("Y-m-d H:i:s" ));
        if($created_at < $configurable_time)
        {
           $otp_data=array('is_expired'=>'Y');
           $this->Mainmodel->update_otp_data($otp_request_id,$otp_data, OTP_MASTER);
           $message = array('status' => 'failure', 'msg' => 'OTP is expired');
		   				  log_data('rest/smartcard/email_otp_api_' . date('d-m-Y') . '.log',  $message, 'api');

           json_encode($this->response($message));
		    exit;
         }
        
        
        $otp=$this->get_otp();
        
        $otp_data=array(
            'agent_id'=> $user_details['id'],
             'otp'=> $otp,
         );
         $otp_id = $this->Mainmodel->insert_otp_data($otp_data, OTP_MASTER);
         
         $otp_data=array('is_expired'=>'Y');
         $this->Mainmodel->update_otp_data($otp_request_id,$otp_data, OTP_MASTER);
         
         $email = rokad_decrypt($user_details['email'], $this->config->item('pos_encryption_key'));
         $mobile_no = rokad_decrypt($user_details['mobile_no'], $this->config->item('pos_encryption_key'));
         $response_data= array(
             'otp_request_id'=>$otp_id,
             'otp'=>$otp,
             'terminal_code'=>$this->input->post('terminal_code'),
             'agent_id'=>$user_details['id'],
             'agent_name'=>$user_details['first_name'].' '.$user_details['last_name'],
             'agent_email'=>$email,
             'agent_contact'=>$mobile_no,
         );
         
        if(!empty($otp_id))
        {  
		  /*email code start here*/  
	      $email = json_encode(array($email)) ;
	      $replacements = array();
          $subject = 'Email OTP '; 
         // $msg = "Email OTP ".$otp;
          
		  $msg = $this->emailformat($otp);
          $mail_array = array("subject" => $subject,
                "message" => $msg,
                "to" => json_decode($email), 
	      );
          send_mail($mail_array, $replacements);
          /*email code end here*/   
          $message = array('status' => 'success', 'msg' => 'OTP generated successfully', 'data' => $response_data);
		  				  log_data('rest/smartcard/email_otp_api_' . date('d-m-Y') . '.log',  $response_data, 'api');

          json_encode($this->response($message));
		   exit;
        }else{
          $message = array('status' => 'failure', 'msg' => 'Could not complete your request');
		  				  log_data('rest/smartcard/email_otp_api_' . date('d-m-Y') . '.log',  $message, 'api');

          json_encode($this->response($message));
		   exit;
         }

    }
    
     public function email_otp_validate_post() 
      {
        $user_details= $this->Mainmodel->get_userdetailsByterminal_code($this->input->post('terminal_code'));
        if(empty($user_details))  {  
          $message = array('status' => 'failure', 'msg' => 'Invalid Terminal code');
		  				  log_data('rest/smartcard/email_otp_api_' . date('d-m-Y') . '.log',  $message, 'api');

          json_encode($this->response($message));
		   exit;
        }  
       $otp=$this->input->post('otp') ;
       
       $otp_data = $this->Mainmodel->get_otp_data_by_otp($otp,$user_details,OTP_MASTER);
       
        if(empty($otp_data))  {  
          $message = array('status' => 'failure', 'msg' => 'Invalid otp, Please try again');
		  				  log_data('rest/smartcard/email_otp_api_' . date('d-m-Y') . '.log',  $message, 'api');

          json_encode($this->response($message));
		   exit;
        } 
        $created_at = strtotime ( OTP_TIME_OUT , strtotime ( $otp_data['created_at'] ) ) ;
        $configurable_time =strtotime( date("Y-m-d H:i:s" ));
        
        if($created_at < $configurable_time)
        {
           $otp_request_id=$otp_data['id'];
           $otp_data=array('is_expired'=>'Y');
           $this->Mainmodel->update_otp_data($otp_request_id,$otp_data, OTP_MASTER);
           $message = array('status' => 'failure', 'msg' => 'OTP is expired');
		   				  log_data('rest/smartcard/email_otp_api_' . date('d-m-Y') . '.log',  $message, 'api');

           json_encode($this->response($message));
		    exit;
         }
        
        $otp_request_id =  $otp_data['id'] ;
        $otp_data=array('is_expired'=>'Y');
        $this->Mainmodel->update_otp_data($otp_request_id,$otp_data, OTP_MASTER);
    
         $email = rokad_decrypt($user_details['email'], $this->config->item('pos_encryption_key'));
         $mobile_no = rokad_decrypt($user_details['mobile_no'], $this->config->item('pos_encryption_key'));
         
         $response_data= array(
             'terminal_code'=>$this->input->post('terminal_code'),
             'agent_id'=>$user_details['id'],
             'agent_name'=>$user_details['first_name'].' '.$user_details['last_name'],
             'agent_email'=>$email,
             'agent_contact'=>$mobile_no,
         );
         
          $message = array('status' => 'success', 'msg' => 'OTP is matched', 'data' => $response_data);
		  				  log_data('rest/smartcard/email_otp_api_' . date('d-m-Y') . '.log',  $message, 'api');
		    log_data('rest/smartcard/email_otp_api_' . date('d-m-Y') . '.log',  $message, 'api');

          json_encode($this->response($message));
		  exit;
		  

    }
    
    function get_otp()
    {
          return $six_digit_random_number = mt_rand(100000, 999999);
    }
	
	function emailformat($otp)
	{
	   
	 return $text="<table border='1' cellpadding='5' cellspacing='5'>
                                    <tr>
                                      <td colspan='2'>Please find below otp.</td>
                                    </tr>  

                                   <tr>
                                      <td colspan='2'>OTP:".$otp."</td>
                                    </tr> 									
                                  </table>";
								  
								  
	
	}

}

?>