<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Curl_controller extends CI_Controller
{
    
    protected $api_url;
    protected $etim_api_url;
    protected $date;
    
    public function __construct() {
        parent::__construct();
        
        $check = $this->input->get('env') ? $this->input->get('env') : 'local';

        switch ($check) {           

            case 'live':
                $url = 'https://www.bookonspot.com/rest_server/';
                $this->api_url = $url . 'bus/';
                $this->agent_api_url = $url . 'agent/';
                $this->user_url = $url . 'user/';
                $this->master_data_url = $url . 'master_data/';
                // code...
                break;
            
            case 'staging':
                $url = 'https://www.bookonspot.com/app/bosstaging/rest_server/';
                $this->api_url = $url . 'bus/';
                $this->agent_api_url = $url . 'agent/';
                $this->user_url = $url . 'user/';
                $this->master_data_url = $url . 'master_data/';
                // code...
                break;

            case 'testing':
                $url = 'http://10.21.41.243/' . 'rest_server/';
                $this->api_url = $url . 'bus/';
                $this->agent_api_url = $url . 'agent/';
                $this->user_url = $url . 'user/';
                $this->master_data_url = $url . 'master_data/';
                break;

            default:
                $url = base_url() . 'rest_server/';
                $this->api_url = $url . 'bus/';
                $this->agent_api_url = $url . 'agent/';
                $this->user_url = $url . 'user/';
                $this->master_data_url = $url . 'master_data/';
                
                break;
        }
        
        $this->date = date('d-m-Y', strtotime(date('Y-m-d') . '+2 day'));
        
        $this->date = date('12-12-2015');
        $this->date = date('12-12-2015');

        $this->from = 'Hyderabad';
        $this->to   = 'Bangalore';

        // $this->from = 'Lucknow';
        // $this->to   = 'Gorakhpur';
        
        
    }
    
    public function index()
    {
        echo 'call the function';
    }

    //---------------------- upsrtc
    
    public function get_all_bus_type() {
        
        // $url = $this->api_url.'all_bus_type/format/json';
        $url = $this->api_url . 'getAllBusType/format/php';
        
        $fields = array('operator' => 'UPSRTC');

        show($url,'','url');
        show($fields,'','fields');
        
        $data = sendDataOverPost($url, $fields, 1);
        
        show($data, 1, 'response');
        show(json_decode($data), 1, 'response');
    }

    public function get_all_bus_stop() {
        
        // $url = $this->api_url.'all_bus_type/format/json';
        $url = $this->api_url . 'getAllstops/format/php';
        
        $fields = array('operator' => 'UPSRTC');

        show($url,'','url');
        show($fields,'','fields');
        
        $data = sendDataOverPost($url, $fields, 1);
        
        show($data, 1, 'response');
        show(json_decode($data), 1, 'response');
    }
    
    function up_get_Services()
    {
        $url = $this->api_url . 'getAvailableServices/format/json';
               
        $fields = array(   
                        'date'  => '04-02-2016',
                        // 'return_date' => '13-12-2015', 
                        'from'  => 'Hyderabad', 
                        'to'    => 'Bangalore'
                    );

        show($url,'','url');
        show($fields,'','fields');
        
        $data = sendDataOverPost($url, $fields, 1,12000);

        // show(var_dump($data));
        show($data, 0, 'response');
        show(json_decode($data), 1, 'response');
    }


    function up_get_seatAvailability() {
        $url = $this->api_url . 'getSeatAvailability/format/json';

        $fields = array(
                        'date'          => '12-12-2015',
                        'from'          => 'Kaisarbagh', 
                        'to'            => 'Gorakhpur', 
                        'trip_no'       => '158',
                        'service_id'    => '2670',

                        'inventory_type'    => '2',
                        'route_schedule_id' => '2670',

                        'provider_id'   => '1',
                        'provider_type' => 'operator',
                        'op_id'         => '2',
                        'operator'      => 'upsrtc',
                    );
        
        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        
        show($data, '', 'response');
        show(json_decode($data), 1, 'response');
    }

    function up_tempBooking1() {
        
        $passenger = array();

        $ac             = false;
        $sleeper        = false;

        $passenger[] = array(
                            'name'                  => 'Suraj',
                            'last_name'             => 'Rathod',
                            'age'                   => '25',
                            'sex'                   => 'M',
                            'birth_no'              => '0',
                            'seat_no'               => '8',
                            'fare'                  => '267',
                            'total_fare_with_taxes' => '317',
                            'ladies_seat'           => 'false',
                            'mobile_no'             => '9867493006',
                            'title'                 => 'Mr',
                            'email'                 => 'surajra@trimax.in',
                            'id_type'               => 'pancard',
                            'id_number'             => '5555555555',
                            'name_on_id'            => 'Suraj Rathod',
                            'primary'               => 'true',
                            'ac'                    => $ac,
                            'sleeper'               => $sleeper,
                        );

        $boarding_point                 = array();
        $boarding_point['id']           = 6;
        $boarding_point['location']     = 'Ameerpet,|Jeans Corner:  040-50607090';
        $boarding_point['time']         = '05:45PM';

        $fields = array(
                        'service_id'            => '2670',
                        'route_schedule_id'     => '2670',
                        'inventory_type'        => '0',
                        'from'                  => 'Kaisarbagh',
                        'to'                    => 'Gorakhpur',
                        'date'                  => '12-12-2015',
        
                        // 'from'               => 'KSB', 
                        // 'to'                 => 'DLI',

                        'boarding_stop'         => 'Kaisarbagh',
                        'destination_stop'      => 'Gorakhpur',
                        'boarding_stop_cd'      => 'KSB',
                        'destination_stop_cd'   => 'GKP',

                        'boarding_stop'         => 'Kaisarbagh',
                        'destination_stop'      => 'Gorakhpur',
                        'boarding_stop_cd'      => 'KSB',
                        'destination_stop_cd'   => 'GKP',

                        'boarding_point'        => json_encode($boarding_point),
                        'name'                  => 'Suraj',
                        'last_name'             => 'Rathod',
                        'address'               => 'Thane',
                        'email'                 => 'surajra@trimax.in',
                        'mobile_no'             => '9867493006',
                        'emergency_phone_no'    => '9867493006',
                        'provider_id'           => '1',
                        'provider_type'         => 'operator',
                        'op_id'                 => '2',
                        'operator'              => 'upsrtc',
                        'user_id'               => '',

        );
    
              

        // $fields['passenger'] = '[{"sex":"M","age":"35","name":"Rahul","fare":"267","seat_no":"6","birth_no":"1"}]';
        $fields['passenger'] = json_encode($passenger);
        
        $url = $this->api_url . 'tempBooking/format/php';

        show($url,'','url');
        show($fields,'','fields');

        
        $data = sendDataOverPost($url, $fields, 1, 600000);
            
        show($data, 1, 'response');
    }

    function up_confirm_booking1() {
        $url = $this->api_url . 'doConfirmBooking';
        // $fields = array('block_key' => '3409139184697', 'unique_id' => uniqid(), 'seat_count' => 1, 'operator' => 'UPSRTc');
        
        $fields = array(
                        'block_key'     => '4415759109687', 
                        'unique_id'     => uniqid(), 
                        'seat_count'    => 1,  
                        // 'provider_id'   => '1',
                        // 'provider_type' => 'operator',
                        // 'op_id'         => '2',
                        // 'operator'      => 'UPSRTC',
                        'user_id'       => '',
                    );
        
        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        
        show($data, 1, 'response');
    }

    function up_cancel_ticket() {
        $url = $this->api_url . 'cancelTicket';
        
        $fields = array(
                        'pnr'           => 'XGKP3412041500001',  
                        // 'provider_id'   => '1',
                        // 'provider_type' => 'operator',
                        // 'op_id'         => '2',
                        // 'operator'      => 'UPSRTC',
                    );
        
        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        
        show($data, 1, 'response');
    }

    //---------------------- upsrtc
    
    /*function get_bus_service_stops() {
        
        $url = $this->api_url . 'getBusServiceStops/format/php';
        $fields = array(
                            'operator' => 'UPSRTC', 
                            'seets_cancel_ticketrvice_id' => '2917',
                            'date' => $this->date, 
                        );

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1, 'response');
    }*/
    
    /*function getSeatLayout() {
        $url = $this->api_url . 'getSeatLayout/format/json';

        $fields = array(
                            'operator' => 'UPSRTC',
                            'layout_name' => 'ORD52'
                        );

        show($url,'','url');
        show($fields,'','fields');
        
        $data = sendDataOverPost($url, $fields, 1);
        
        show($data);
        show(json_decode($data), 1, 'response');
    }*/
        
    function ets_get_Services() {
        
        $url = $this->api_url . 'getAvailableServices/format/json';
               
        $fields = array(   
                        'date'=> $this->date,
                        // 'date'=> '12-12-2015',
                        // 'return_date' => '13-12-2015', 
                        'from' => $this->from, 
                        'to' => $this->to
                    );

        show($url,'','url');
        show($fields,'','fields');
        
        $data = sendDataOverPost($url, $fields, 1,12000);

        // show(var_dump($data));
        show($data, 0, 'response');
        show(json_decode($data), 1, 'response');
    }

    function ets_get_seatAvailability() {
        
        $url = $this->api_url . 'getSeatAvailability/format/json';

        $fields = array(
                        'date'          => $this->date,
                        'from'          => $this->from, 
                        'to'            => $this->to, 
                        'trip_no'       => '6471',
                        'service_id'    => '6471',

                        'inventory_type'    => '0',
                        'route_schedule_id' => '6471',

                        'provider_id'   => '2',
                        'provider_type' => 'api_provider',
                        'op_id'         => '2',
                        'operator'      => 'upsrtc',
                    );

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        
        show($data, '', 'response');
        show(json_decode($data), 1, 'response');
    }
    
    

    function ets_tempBooking1() {
        
        $passenger = array();

        $ac             = false;
        $sleeper        = false;

        $passenger[] = array(
                            'name'            => 'Suraj',
                            'last_name'             => 'Rathod',
                            'age'                   => '25',
                            'sex'                   => 'M',
                            'birth_no'              => '0',
                            'seat_no'               => 'B3',
                            'fare'                  => '960',
                            'total_fare_with_taxes' => '960',
                            'ladies_seat'           => 'false',
                            'mobile_no'             => '9867493006',
                            'title'                 => 'Mr',
                            'email'                 => 'surajra@trimax.in',
                            'id_type'               => 'pancard',
                            'id_number'             => '5555555555',
                            'name_on_id'            => 'Suraj Rathod',
                            'primary'               => 'true',
                            'ac'                    => $ac,
                            'sleeper'               => $sleeper,
                        );

        $boarding_point                 = array();
        $boarding_point['id']           = 2889;
        $boarding_point['location']     = 'Mettuguda,Opp. Mettuguda Church';
        $boarding_point['time']         = '04:50PM';

        $fields = array(
                        'service_id'            => '6471',
                        'route_schedule_id'     => '6471',
                        'inventory_type'        => '0',

                        'from'                  => 'Hyderabad',
                        'to'                    => 'Bangalore',
                        'date'                  => $this->date,
        
                        // 'from'               => 'KSB', 
                        // 'to'                 => 'DLI',

                        'boarding_stop'         => 'Kaisarbagh',
                        'destination_stop'      => 'Gorakhpur',
                        'boarding_stop_cd'      => 'KSB',
                        'destination_stop_cd'   => 'GKP',

                        'boarding_stop'         => '',
                        'destination_stop'      => '',
                        'boarding_stop_cd'      => '',
                        'destination_stop_cd'   => '',

                        'boarding_point'        => json_encode($boarding_point),
                        'name'                  => 'Suraj',
                        'last_name'             => 'Rathod',
                        'address'               => 'Thane',
                        'email'                 => 'surajra@trimax.in',
                        'mobile_no'             => '9867493006',
                        'emergency_phone_no'    => '9867493006',
                        'provider_id'           => '2',
                        'provider_type'         => 'api_provider',
                        'op_id'                 => '2',
                        'operator'              => 'Diwakar Travels',
                        'user_id'               => '',

        );        

        // $fields['passenger'] = '[{"sex":"M","age":"35","name":"Rahul","fare":"267","seat_no":"6","birth_no":"1"}]';
        $fields['passenger'] = json_encode($passenger);
        
        $url = $this->api_url . 'tempBooking/format/php';

        show($url,'','url');
        show($fields,'','fields');

        
        $data = sendDataOverPost($url, $fields, 1, 600000);
            
        show($data, 1, 'response');
    }

    
    function ets_confirm_booking1() {
        $url = $this->api_url . 'doConfirmBooking';
        // $fields = array('block_key' => '3409139184697', 'unique_id' => uniqid(), 'seat_count' => 1, 'operator' => 'UPSRTc');
        
        $fields = array(
                        'block_key'     => 'ETS0S64715963', 
                        'unique_id'     => uniqid(),
                        'seat_count'    => 1,
                        'user_id'       => '',
                    );
        
        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        
        show($data, 1, 'response');
    }
    
    function boarding_stops() {
        $url = $this->api_url . 'getBoardingStops/format/json';
        
        
        $fields = array(
                        'service_id'    => '2917', 
                        'from'          => 'Kaisarbagh', 
                        'to'            => 'Delhi', 
                        'bus_type_code' => 'VPL',  
                        'provider_id'   => '1',
                        'provider_type' => 'operator',
                        'op_id'         => '2',
                        'operator'      => 'UPSRTC',
                    );

        show($url,'','url');
        show($fields,'','fields');
        
        $data = sendDataOverPost($url, $fields, 1);
            
        var_dump($data);

        show($data, 1, 'response');
    }
    
    function ets_cancel_ticket() {
        $url = $this->api_url . 'cancelTicket';
        
        $fields = array(
                        'pnr'              => 'ABRS973632',  
                        // 'provider_id'   => '2',
                        // 'provider_type' => 'api_provider',
                        // 'op_id'         => '0',
                        // 'operator'      => 'UPSRTC',
                    );
        
        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        
        show($data, 1, 'response');
    }

    //  API to show list of transactions between dates (on the basis of user id)
    //  API to show List of transactions on which cancellations can be performed (on the basis of user 

    function ticket_trans()
    {
        $url = $this->api_url . 'ticketTransaction/format/php';
        
        $fields = array(
                        'email'     => 'riyaponnam@gmail.com',  
                        // 'mobile'    => '9867493006',
                        'from_date'     => '01-01-2016',
                        'till_date'     => '24-01-2016',
                    );
        
        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        
        show($data, 1, 'response');   
    }

    function cancel_ticket_trans(){

        $url = $this->api_url . 'cancellableTicketTransaction/format/php';
        
        $fields = array(
                        'email'     => 'trimaxs123@gmail.com',
                        // 'mobile'    => '9867493006',
                    );
        
        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        
        show($data, 1, 'response');      
    }

    function sms_ticket(){
        $url = $this->api_url . 'smsTicket/format/php';
        
        $fields = array(
                        // 'email'     => 'probhakar.here@gmail.com',
                        'pnr'  => 'XCHB3410031500003',
                        // 'mobile'    => '9867493006',
                    );
        
        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        
        show($data, 1, 'response');         
    }

    function email_ticket(){
        $url = $this->api_url . 'emailTicket/format/php';
        
        $fields = array(
                        // 'email'     => 'probhakar.here@gmail.com',
                        'pnr'  => 'ABRS973613',
                        // 'mobile'    => '9867493006',
                    );
        
        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        
        show($data, 1, 'response');
    }

    


    
    /***************CURRENT BOOKING*********************************************/
    
    /*
     * bus_stops
     * author:pankaj
     *
    */
    
    function getCbBusStops() {
        $url = $this->api_url . 'getCbBusStops/format/php';
        $fields = array("bn" => 'aa');
        
        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*  function    : getCbRoutes
     *   author      : pankaj
    */
    function getCbRoutes() {
        $url = $this->api_url . 'getCbRoutes/format/php';
        $fields = array();

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*route stops
     * author:pankaj
    */
    function getCbRouteStops() {
        $url = $this->api_url . 'getCbRouteStops/format/php';
        $fields = array("route_no" => '42');

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*route stops
     * author:pankaj
    */
    function getCbServiceStops() {
        $url = $this->api_url . 'getCbServiceStops/format/php';

        $fields = array("route_no" => 3762, "bus_type" => 'ORD');

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*get bus types
     * author:pankaj
    */
    function getAllCbBusTypes() {
        $url = $this->api_url . 'getAllBusTypes/format/php';
        $fields = array("bn" => 'aa');

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*get bus services
     * author:pankaj
    */
    
    function getCbBusServices() {
        $url = $this->api_url . 'getAllCbBusServices/format/php';

        $fields = array('page' => 10,);

        show($url,'','url');
        show($fields,'','fields');
        
        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*get current booking services
     * author:pankaj
    */
    
    function getCbServices() {
        $url = $this->api_url . 'getCbServices/format/php';
        
        // $url = base_url().'rest_server/upsrtc/'.'current_booking/getServices/format/php';
        $today = '29-06-2015';
        $fields = array('date' => $today);

        show($url,'','url');
        show($fields,'','fields');
        
        $data = sendDataOverPost($url, $fields, 1);
        
        show($data, 1);
    }
    
    /***************CURRENT BOOKING END*********************************************/
    
    /*
     * Get Boarding stop name
    */
    
    function getBoardingStops() {
        $url = $this->api_url . 'getBoardingStops/format/php';
        $fields = array(
                        'serviceId' => '2',
                        'fromStopCode' => 'CHB',
                        'toStopCode' => 'ALD',
                        'brd' => 'ALI',
                        'busTypeCD' => 'VPL'
                    );

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        
        show($data, 1, 'response');
    }
    
    function getAgentDetail() {
        
        $url = $this->agent_api_url . 'getAgentDetail/format/json';
        $fields = array('vAgentCode' => 'SD2015071096', 'agent_code' => 'bos00018');
        
        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*
     * Add Agent Details
    */
    
    function addAgentDetails() {
        
        $url = $this->agent_api_url . 'addAgentDetails/format/json';
        // show($url,1);
        
        $fields = array(
                        'fname' => 'ANIL COMPUTER CENTER', 
                        'mname' => 'ANIL COMPUTER CENTER', 
                        'lname' => 'ANIL COMPUTER CENTER', 
                        'v_agent_code' => 'RT0000002016012188',
                        'address'       => 'FAIZABAD, UP',
                        'country_name' => 'india', 
                        'state_name' => '22', 
                        'city_name' => '898',        
                        // 'gender'        => '',
                        // 'dob'           => '',
                        'email' => 'vpvermalko@gmail', 
                        'contact_no' => '9453966781', 
                        'pincode' => '421301', 
                        'level' => '5',
                        'level3' => 'BOS00014', 
                        'level4' => 'BOS00062', 
                        'level5' => '', 
                        'kyc' => 'N',
                    );

        show($url,'','url');
        show($fields,'','fields');
        
        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    function save_agent_kyc() {
        
        //$agent_detail = array('agent_code' => 'BOS00160', 'v_agent_code' => 'DT002015072716', "doc" => array(array('doc_cat' => 1, 'doc_name' => 2, 'doc_path' => 'http://bollywood.celebden.com/wp-content/uploads/2013/02/Hrithik-Roshan2.jpg'), array('doc_cat' => 1, 'doc_name' => 1, 'doc_path' => 'http://media1.santabanta.com/full1/Indian%20%20Celebrities%28M%29/Hrithik%20Roshan/hrithik-roshan-70a.jpg'), array('doc_cat' => 2, 'doc_name' => 6, 'doc_path' => 'http://media1.santabanta.com/full1/Indian%20%20Celebrities%28M%29/Hrithik%20Roshan/hrithik-roshan-70a.jpg'), array('doc_cat' => 2, 'doc_name' => 7, 'doc_path' => 'http://media1.santabanta.com/full1/Indian%20%20Celebrities%28M%29/Hrithik%20Roshan/hrithik-roshan-70a.jpg')));
        
        $fields = array(
                            'agent_code' => 'BOS00076',
                            'v_agent_code' => 'RT0000002016011665',
                            "doc" => '[{"doc_comment":"99999999999999","doc_name":"9","doc_path":"http:\/\/180.92.175.105:8080\/BosImages\/RT0000002016011665\/5perc1.jpg","doc_cat":"1"},{"doc_comment":"pancard","doc_name":"10","doc_path":"http:\/\/180.92.175.105:8080\/BosImages\/RT0000002016011665\/5perc1.jpg","doc_cat":"2"}]',
                            );
        
        // show($agent_detail,1);
        
        
        
        $url = $this->agent_api_url . 'uploadKycDetail/format/php';
        
        show($url,'','url');
        show($fields,'','fields');
        
        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1, 'response');
    }
    
    function update_agent_detail() {
        
        $url = $this->agent_api_url . 'updateAgentDetail/format/php';

        $fields = array('fname' => 'NITIN', 'agent_code' => 'bos00037', 'v_agent_code' => 'DT002015071479', 'mname' => 'YASHWANT', 'lname' => 'PATEL', 'address' => 'PANVEL', 'city_id' => '', 'state_id' => '', 'country_id' => '', 'gender' => '', 'dob' => '30-02-2014', 'contact_no' => '9773925577', 'pincode' => '', 'updated_by' => '1500', 'updated_date' => date('Y-m-d H:i:s', time()));

        show($url,'','url');
        show($fields,'','fields');
        
        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    function agent_star_seller_point() {
        $fields = array("agent_code" => 'bos000112', "bus_stop_name" => 'Ma1455');
        
        $url = $this->agent_api_url . 'save_agent_star_seller_point/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*
     * Transfer amount to etim
    */
    function addEtimAmount() {
        $url = $this->etim_api_url . 'addEtimAmount/format/php';
        $fields = array('agent_code' => 'bos00001', 'etim_seriale_no' => '12345678', 'amt' => '100.00');
        
        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*
     * Approve Kyc
    */
    
    function mapAgentToVendor() {
        $url = $this->agent_api_url . 'mapAgentToVendor/format/php';
        $fields = array('agent_code' => 'bos00001');

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*
     * Agents wallet Balance
    */
    
    function agentWalletBalance() {
        $url = $this->agent_api_url . 'walletBalance/format/php';
        $fields = array('agent_code' => 'bos00001');

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*
     * Agents ETIM Balance
    */
    
    function agentEitmBalance() {
        $url = $this->agent_api_url . 'eitmBalance/format/php';
        $fields = array('agent_code' => 'bos00001', 'etim_seriale_no' => '12345678');

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*function:agent_top_up
     * this function updates agent balance
    */
    
    function agent_top_up() {
        $url = $this->agent_api_url . 'agent_top_up/format/php';
        
        $fields = array("agent_code" => 'bos00004', 'v_agent_code' => 'DT002015070964', 'top_up_amt' => 1000);
        
        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    function agent_hhm_trans_update() {
        $input = $this->input->post();
        $fields = array(
                            "agent_code" => 'BOS00049', 
                            "wayBillId" => '0000000015', 
                            "wayBillAllocationDate" => '2015-10-07 18:17:52', 
                            "openingBalance" => '2500', 
                            "wayBillCollectionDate" => '2015-10-07 18:22:41',
                            "noOfTickets" => '4', 
                            "consumedamount" => '68.00', 
                            "remainingBalance" => '2432',
                        );
        
        $url = $this->agent_api_url . 'hhmTransactionUpdate/format/php';
        
        show($url,'','url');
        show($fields,'','fields');
        
        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    function add_agent_seller_points() {
        $fields = array('agent_code' => 'bos00007');
        
        $url = $this->agent_api_url . 'add_agent_seller_points/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*
     * Add Agents Waybill API
    */
    function add_agent_way_bill_master() {
        $fields = array('agent_code' => 'bos00007');
        
        $url = $this->agent_api_url . 'add_agent_way_bill_master/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*
     * Balance transfer Aprroval APIfne
    */
    
    function call_balance_transfer_approve() {
        $fields = array('agent_code' => 'bos00007');
        
        $url = $this->agent_api_url . 'call_balance_transfer_approve/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*
     * Topup Balance
    */
    function call_topup_balance() {
        $fields = array(
                            'agent_code' => 'BOS00049', 
                            'way_bill_no' => '0000000015', 
                            'balance' => '10',
                            'remaining_balance' => 0, 
                        );
        
        // show($fields,1);
        $url = $this->agent_api_url . 'call_topup_balance/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*
     * Create Retailer API
    */
    function create_retailer() {
        $fields = array('agent_code' => 'bos00007');
        
        $url = $this->agent_api_url . 'create_retailer/format/php';
        
        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*
     * Delete Agents Seller Point
    */
    function delete_agent_seller_point() {
        $fields = array('agent_code' => 'bos00007');
        
        $url = $this->agent_api_url . 'delete_agent_seller_point/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*
     * Delete Agents Waybill Master
    */
    function delete_agent_waybill_master() {
        $fields = array('agent_code' => 'bos00007');
        
        $url = $this->agent_api_url . 'delete_agent_waybill_master/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*
     * Edit Using Bos
    */
    function edit_using_bos() {
        $fields = array('agent_code' => 'bos00007');
        
        $url = $this->agent_api_url . 'edit_using_bos/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*
     * Get Newly added agent
    */
    function get_newly_added_agent() {
        $fields = array('agent_code' => 'bos00007');
        
        $url = $this->agent_api_url . 'get_newly_added_agent/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*
     * Update Agent Seller Pount
    */
    function update_agent_seller_point() {
        $fields = array('agent_code' => 'bos00007');
        
        $url = $this->agent_api_url . 'update_agent_seller_point/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*
     * Update Agent Waybill Master
    */
    function update_agent_waybill_master() {
        $fields = array('agent_code' => 'bos00007');
        
        $url = $this->agent_api_url . 'update_agent_waybill_master/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*
     * Update Topup Limit
    */
    function update_topup_limit() {
        $fields = array('agent_code' => 'bos00007');
        
        $url = $this->agent_api_url . 'update_topup_limit/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*
     * Function for Encryption
    */
    function encrypt_string() {
        
        //$input = $this->input->post();\
        // $fields = array('plain_text' => 'key=ajslkasdjsdfk###agent_code=BOS00003###op_code=12345###bus_stop=ALM###balance=30000');
        $fields = array('plain_text' => 'key=ajslkasdjsdfk###balance=100###bus_stop=ALM###agent_code=BOS00314###op_code=UPSRTC');
        $fields = array('plain_text' => 'agent_code=BOS00043###op_code=UPSRTC###page=Machine_registration###key=ajslkasdjsdfk');
        
        $url = $this->agent_api_url . 'fnEncrypt/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*
     * Function for Decryption
    */
    function decrypt_string() {
        
        //$input = $this->input->post();
        $fields = array('encrypted_text' => '99b975251522f994320bbed624b060c16b7d7f05358393d5fb7985e0251b51313721cada32ba4ee7fda0486084848634d789ee6c231a3c9959a339720cd1a3e232b7c24828e9747fbdd009ec1524b462250a620b15a3e9346a940c150fd4d5db');
        
        $url = $this->agent_api_url . 'fnDecrypt/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*
     * function to validate the agent
    */
    function validate_agent_login() {
        $input = $this->input->post();
        $fields = array('agent_username' => $input['agent_username'], 'agent_password' => $input['agent_password']);
        $url = $this->agent_api_url . 'validate_agent_login/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    /*
     * Get Agent details using agent code
    */
    
    function get_agent_details() {
        $input = $this->input->post();
        $fields = array('agent_code' => $input['agent_code']);
        $url = $this->agent_api_url . 'get_agent_details/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    
    //------------- User Module
    

    function login() {
        
        $fields = array(
                        'email' => 'help26oct@bos.com', 
                        'password' => 'password'
                    );
        
        $url = $this->user_url . 'login/format/json';
        
        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        
        show($data);
        show(json_decode($data), 1);
    }

    function resend_activation_link() {
        
        $fields = array(
                        'email' => 'rahul@gmail.com'
                    );
        
        $url = $this->user_url . 'resend_activation_link/format/json';

        show($url,'','url');
        show($fields,'','fields');
        
        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }

    function get_user_detail() {
        
        $fields = array(
                        'email' => 'trimaxs123@gmail.com'
                    );
        
        $url = $this->user_url . 'detail/format/php';

        show($url,'','url');
        show($fields,'','fields');
        
        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
     
    function user_register() {
        $input = $this->input->post();
        $fields = array(
                        'name' => 'help26oct',
                        'email' => 'help26oct@bos.com',
                        // 'username' => 'help', 
                        'password' => 'password',
                        // 'confirm_password' => 'help@123', 
                        'mob_no' => '9999992815',
                        'dob' => '12-12-1987',
                        'address' => '4, rohan ',
                        'type' => 'user'
        
        // user | agent
        );
        $url = $this->user_url . 'register/format/php';

        show($url);
        show($fields);


        $data = sendDataOverPost($url, $fields, 1);
        
        show($data, 1);
    }
    
    //edit data
    
    function edit_user() {
        $input = $this->input->post();
        $fields = array(
                            'name' => 'help26oct test', 
                            'email' => 'help26oct@bos.com',  
                            'mob_no' => '9999992815',
                            'dob' => '12-12-1987',
                            'address' => '4, rohan',
                            'gender' => 'M', 
                            'city' => 'panvel', 
                            'state' => '', 
                            'country' => 'India', 
                            'pincode' => '410206', 
                            'userfile' => 'http://static.dnaindia.com/sites/default/files/2015/06/11/345509-hrithik-hi-res-2.jpg'
                        );

        $url = $this->user_url . 'edit/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    function change_email() {
        
        $fields = array('user_id' => 16, 'email' => 'appsperts@bos.com', 'sec_email' => 'pankaj.durve1990@gmail.com',);
        
        $url = $this->user_url . 'change_email/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    function change_password() {
        
        $fields = array(
                            'email' => 'trimaxs123@gmail.com', 
                            'old_password' => 'Susma@1234', 
                            'new_password' => 'Susma@1234',
                        );

        $url = $this->user_url . 'change_password/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }

    function forgot_password(){
        // RxSNm345
        // f3544adafef9a008c157fcf9d66708bd
        $fields = array('email' => 'rajendras@trimax.in');
        
        $url = $this->user_url . 'forgot_password/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }

    //------------- User Module
    
    function get_country_master() {
        
        $fields = '';
        $url = $this->master_data_url . 'country_master/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    function get_state_master() {
        
        $fields = '';
        $url = $this->master_data_url . 'state_master/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    function get_city_master() {
        
        $fields = '';
        $url = $this->master_data_url . 'city_master/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    
    function get_docs_cat() {
        $fields = '';
        $url = $this->master_data_url . 'docs_cat_master/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
    function get_docs_name() {
        $fields = '';
        $url = $this->master_data_url . 'docs_name_master/format/php';

        show($url,'','url');
        show($fields,'','fields');

        $data = sendDataOverPost($url, $fields, 1);
        show($data, 1);
    }
}

/* End of file Curl_controller.php */

/* Location: ./application/controllers/rest_server/Curl_controller.php */
