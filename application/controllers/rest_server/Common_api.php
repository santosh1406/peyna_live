<?php

/* Created by Shrasti Bansal*/
if (!defined('BASEPATH')) exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';
ini_set('memory_limit','500M');

class Common_api extends REST_Controller {
    var $db2;

    public function __construct() { 
        parent::__construct();
		
	$this->db2 = $this->load->database('default', TRUE);
        $this->load->model(array('Mainmodel','Passfare_model','Common_api_model','Otc_card_inventory_model'));
        $this->pos_encryption_key = 'QWRTEfnfdys635';
		
       $this->load->helper('smartcard_helper'); 
       $this->load->helper('common_helper'); 
        $this->apiCred = json_decode(SMART_CARD_CREDS,true);
        $header_value = $this->apiCred['X_API_KEY'];
        $this->header = array(
            'X-API-KEY:'.$header_value   
        );

        $this->basicAuth = array(
        'username' => $this->apiCred['username'],
        'password' => $this->apiCred['password']
        );
         $insert_data = array('username' => $this->input->post('apploginuser'), 'ip' => $_SERVER['REMOTE_ADDR'], 'method' => $_SERVER['REQUEST_URI'], 'type' => 'get', 'version_id' => $this->input->post('version_id'));
        $this->requestId = $this->Mainmodel->insert_audit_data($insert_data)."";
        
		
        $log_data = array();
        $log_data['url'] = current_url();
        $log_data['Request_type'] = $_SERVER['REQUEST_METHOD'];
        $log_data['get'] = $this->input->get();
        $log_data['post'] = $this->post();
        $log_data['header'] = getallheaders();
        $log_data['user'] = $this->input->server('PHP_AUTH_USER');
        $log_data['ip'] = $this->input->ip_address();
        // show($log_data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $log_data, 'api');
        $request = $this->post();
        $user_id = $request['apploginuser'];
        $session_id= $this->Mainmodel->get_session_id($this->input->post('session_id'),$user_id);
        $this->session_id = $session_id['itms_session_id'];
        $this->user_id =itms_user_id;
		
        /*   added code disallow multiple login of same user code start here */
        $startTime = date("Y-m-d H:i:s");
        $cenvertedTime = date('Y-m-d H:i:s',strtotime(LOGIN_EXPIRE_TIME,strtotime($startTime))) ;
        $this->Mainmodel->upDateLastActivityTime($this->input->post('session_id'),$cenvertedTime);
        /*   added code disallow multiple login of same user code end here */
    }
  

    public function getOccupationmaster_post() {
      
        $URL= $this->apiCred['url'].'/Common_api/getOccupationmaster';

        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
                
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
        
    }
      public function get_pass_conc_post() {
      
        $URL= $this->apiCred['url'].'/Common_api/get_pass_conc';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
         //        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
        
    }
    public function base_fare_calc_post() {
        
        $URL= $this->apiCred['url'].'/Farecalculation/base_fare_calc';
       
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
        
    }


    public function get_card_category_post() {
      
        $URL= $this->apiCred['url'].'/Common_api/get_card_category';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
        
    }

    public function get_state_data_post() {
        $URL= $this->apiCred['url'].'/Common_api/get_state_data';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
        //        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
        
    }

        public function get_city_data_post() {
        $URL= $this->apiCred['url'].'/Common_api/get_city_data';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
        
    }
    public function get_district_post() {
        $URL= $this->apiCred['url'].'/Common_api/get_district';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
       //        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
        
    }
    public function get_taluka_post() {
        
        $URL= $this->apiCred['url'].'/Common_api/get_taluka';
       
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
        
    }
    public function passtype_post() {
       
        $URL= $this->apiCred['url'].'/Common_api/passtype';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
         //        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
        
    }
    public function busstoplist_post() {
        $URL= $this->apiCred['url'].'/Common_api/busstoplist';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
		//print_R($data);  exit;;
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
       //        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
        
    }
    public function busstoplist_code_post() {
        $URL= $this->apiCred['url'].'/Common_api/busstoplist_code';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
        
    }
    public function passValidity_post() {
        
        $URL= $this->apiCred['url'].'/Common_api/passValidity';
        
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
         //        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
        
    }
    public function bustype_post() {

        $URL= $this->apiCred['url'].'/Common_api/bustype';
        

        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
        
    }
    public function regions_post() {
      
        $URL= $this->apiCred['url'].'/Common_api/regions';
       
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
       //        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
        
    }
    public function divisions_post() {
        $URL= $this->apiCred['url'].'/Common_api/divisions';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
         //        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
        
    }
    public function passcategory_post_old() {
        
        $URL= $this->apiCred['url'].'/Common_api/passcategory';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
        

    }
	
    public function passcategory_post() {
        
        $user_name = $this->input->post('apploginuser');
        $version_id = $this->input->post('version_id');
        $session_id = $this->input->post('session_id');

        $this->form_validation->set_rules('version_id','Version Id','trim|required|xss_clean');
        $this->form_validation->set_rules('apploginuser','App Login User Id','trim|required|xss_clean');
        $this->form_validation->set_rules('session_id', 'Session Id', 'trim|required|xss_clean');
        
        if($this->form_validation->run()) {
            $passtypeData = $this->Common_api_model->get_passcategory();
            if($passtypeData) {
                $message = array('status' => 'success', 'msg' => 'Success','data' => $passtypeData);
				json_response($message);
            } else {
                $message = array('status' => 'failure', 'msg' => 'No pass category found');
                json_response($message);
            }
        } else {
            $error = $this->form_validation->error_array();
            $message = array('status' => 'failure', 'msg' => 'Error', 'data' => $error);
            json_response($message);

        }
    }
    
    public function getproofdetail_post() {
        $URL= $this->apiCred['url'].'/Common_api/getproofdetail';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
        
        
    }
    public function checkmobileno_post() {
        $URL= $this->apiCred['url'].'/Common_api/checkmobileno';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
    }

    public function get_agent_list_post(){
        $URL = "https://10.20.52.156/MSRTC_RFIDPASS/api/Common_api/genTerminalForRokad";
        $itms_request = array();
        $itms_request['version_id'] = 1.23;

        $this->db->select("id");
        $this->db->from("service_master");
        $this->db->where("service_name","Smart Card");
        $query = $this->db->get();
        $service_id =  $query->row()->id;

        $this->db->select("u.id,u.agent_code,rs.depot_code");
        $this->db->from("users u");
        $this->db->join("retailer_service_mapping rs","rs.agent_id = u.id");
        $this->db->where(["service_id" => $service_id,"terminal_code"=> NULL,"terminal_id" =>NULL]);
        $this->db->or_where('terminal_code =', ''); 
        $this->db->or_where('terminal_id =', ''); 


        $rsquery = $this->db->get();
        $data =  $rsquery->result_array();

       // var_dump($data);die;
        
        foreach ($data as $key => $value) {
            $itms_request['rokad_user_id'] = $data[$key]["id"];
            $itms_request['depot_cd'] = $data[$key]["depot_code"];
            $response = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
            $terminal_res = json_decode($response,true);
            if ($terminal_res['status'] == "success") {
                $terminal_data = array(
                    "terminal_id"=>$terminal_res['data']['terminal_id'],
                    "terminal_code"=>$terminal_res['data']['terminal_code']

                    );
                 $this->db->where('agent_id', $data[$key]["id"]);
                 $this->db->update('retailer_service_mapping', $terminal_data);
            }
        }

          // $data[$key]['dob'] = rokad_decrypt($data[$key]['dob'], $this->pos_encryption_key);
       
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $data, 'api');
        if ($data){
            $this->response( array('status' => 'success', 'msg' => 'Success', 'data' => $data ), 200);
        }
        else
        {
            $this->response( array('status' => 'success', 'msg' => 'No records found' ), 200);
        }
    }
    public function checkemailaddres_post() {
        $URL= $this->apiCred['url'].'/Common_api/checkemailaddres';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
       //        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
    }
    public function checkuidnumber_post() {
        $URL= $this->apiCred['url'].'/Common_api/checkuidnumber';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
    }
    public function checkrefno_post() {
        $URL= $this->apiCred['url'].'/Common_api/checkrefno';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
    }
        public function compare_city_state_post() {
        $URL= $this->apiCred['url'].'/Common_api/compare_city_state';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json)); 
        json_response($json);
    }
    public function getprefixmaster_post() {
        $URL= $this->apiCred['url'].'/Common_api/getprefixmaster';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
         //        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
    }
    public function bustype_desk_post() {
        $URL= $this->apiCred['url'].'/Common_api/bustype_desk';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
    }
    public function getproofdetail_desk_post() {
        $URL= $this->apiCred['url'].'/Common_api/getproofdetail_desk';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
    }
    public function getproofdetail_wallet_post() {
        $URL= $this->apiCred['url'].'/Common_api/getproofdetail_wallet';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
    }
    public function generic_customer_old_search_post() {
        $URL= $this->apiCred['url'].'/Common_api/generic_customer_search';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
	
		 //        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
    }
	
	 public function generic_customer_search_old29042019_post() {
        $URL= $this->apiCred['url'].'/Common_api/generic_customer_search';
        $itms_request = $this->post();
		$request = $this->post();

        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;
$custCard_id = $this->Passfare_model->getCustCardIdData($this->input->post('search_value'));
if(!empty($custCard_id)){
        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        
        $json = json_decode($data);
log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
	
		if (strtolower($json->status) == 'success') 
		{   
		  if(!empty($json->data))
		  {
			  $trimaxId=$json->data[0]->trimax_card_id;
			  $card_data = $this->Passfare_model->getCardDataByTid($trimaxId);
			
			  if(!empty($card_data[0])   && $card_data[0]['is_whitelisted'] != '1') 
		      {
			   if(!empty($json->data[0]->cust_card_id))
			   {
				 $custCardId=$json->data[0]->cust_card_id;
				 $master_card_where = array('trimax_card_id' => $trimaxId, 'is_active' => '1', 'card_status' => INITIATED_STATUS);
			     $master_card_update = array('cust_card_id' => $custCardId, 'card_status' => WRITING_STATUS, 'is_whitelisted' => '1', 'modified_by' => $request['apploginuser']);
			  
			     $this->Mainmodel->update_master_card($master_card_where,$master_card_update);
			   
				$cust_card_master_update = array('cust_card_id' => $custCardId, 'card_status' => 'A', 'modified_by' => $request['apploginuser']);
			    $this->Mainmodel->update_cust_card($master_card_where,$cust_card_master_update);
				}
			 }
		  }
		}

}else{
   $data = '{"status": "failure","msg": "failure","data": {
        "responseMessage": "Customer id does not exist."
    }}';
$json = json_decode($data);
}
        
        json_response($json); 
    }



    public function generic_customer_search_post() {
        $URL= $this->apiCred['url'].'/Common_api/generic_customer_search';
        $itms_request = $this->post();
	$request = $this->post();
        $search_value = $this->input->post('search_value');

            $sql_query = "SELECT p_cm.id AS customer_id FROM ps_customer_master p_cm "
                    . "INNER JOIN ps_customer_card_master p_ccm ON p_ccm.customer_id = p_cm.id "
                    . "INNER JOIN ps_card_transaction p_ct ON p_ct.trimax_card_id = p_ccm.trimax_card_id "
                    . "INNER JOIN ps_pass_category p_pct ON p_pct.id = p_ct.pass_category_id "
                    . "INNER JOIN ps_master_card p_mc on p_mc.id = p_ccm.master_card_id "
                    . "LEFT OUTER JOIN ps_image_details p_imgd on p_imgd.customer_master_id = p_cm.id AND p_imgd.is_active = '1' "
                   ;

            //this switch case is used for column name
            switch ($this->input->post('search_key_name')) {
                case 'name':
                    $sql_query .= " WHERE (p_cm.first_name LIKE '%". $search_value ."%' OR p_cm.middle_name LIKE '%".$search_value."%' OR p_cm.last_name LIKE '%".$search_value."%' OR p_ccm.name_on_card LIKE '%".$search_value."%') AND p_cm.is_active = '1' AND p_ccm.is_active = '1' AND p_ct.is_active = '1' AND p_mc.is_active = '1'";
                    break;

                case 'receipt_no':
                    $sql_query .= " WHERE p_ct.receipt_number = '".$search_value."' AND p_cm.is_active = '1' AND p_ccm.is_active = '1' AND p_ct.is_active = '1' AND p_mc.is_active = '1'";
                    break;

                case 'card_id':
                    $sql_query .= " WHERE p_ccm.cust_card_id = '".$search_value."' AND p_cm.is_active = '1' AND p_ccm.is_active = '1' AND p_ct.is_active = '1' AND p_mc.is_active = '1'";
                    break;

                case 'mobile_no':
                    $sql_query .= " WHERE p_cm.mobile_number = ".$search_value." AND p_cm.is_active = '1' AND p_ccm.is_active = '1' AND p_ct.is_active = '1' AND p_mc.is_active = '1'";
                    break;

                default:
                    $message = array('status' => 'failure', 'msg' => 'Search key name is not valid');
                    $this->set_response($message, 200, FALSE);
                    break;
            }

            //this switch case is used for type of process i.e. registration, renewal
            switch ($this->input->post('process_type')) {
                case 'issuance':
                    $sql_query .= " ORDER BY p_ccm.id desc";
                    $error_msg = 'Card is not available at dispatch location';
                    break;

                case 'customer_detail':
             
                    $error_msg = 'Customer detail not found for card id '.$search_value;
                    break;

                case 'card_status':
                    $error_msg = 'Card Details not found for Receipt Number'.$search_value;
                    break;    
                
                case 'pass_image':
                    $sql_query .= " AND p_ct.write_flag = '1'";
                    $error_msg = 'Customer not found';
                    break;

                default:
                    $message = array('status' => 'failure', 'msg' => 'Process type is not valid');
                    $json = json_encode($message);
	            $json = json_decode($json);
                    break;
            }

            $response_data = $this->Common_api_model->get_query_result($sql_query);
log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $sql_query, 'api');
            if(empty($response_data)) {
                $message = array('status' => 'failure', 'msg' => $error_msg);
                $json = json_encode($message);
	        $json = json_decode($json);
            }else{
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
		
		if (strtolower($json->status) == 'success') 
		{   
		  if(!empty($json->data))
		  {
			  $trimaxId=$json->data[0]->trimax_card_id;
			  $card_data = $this->Passfare_model->getCardDataByTid($trimaxId);
			
			  if(!empty($card_data[0])   && $card_data[0]['is_whitelisted'] != '1') 
		      {
			   if(!empty($json->data[0]->cust_card_id))
			   {
				 $custCardId=$json->data[0]->cust_card_id;
				 $master_card_where = array('trimax_card_id' => $trimaxId, 'is_active' => '1', 'card_status' => INITIATED_STATUS);
			     $master_card_update = array('cust_card_id' => $custCardId, 'card_status' => WRITING_STATUS, 'is_whitelisted' => '1', 'modified_by' => $request['apploginuser']);
			  
			     $this->Mainmodel->update_master_card($master_card_where,$master_card_update);
			   
				$cust_card_master_update = array('cust_card_id' => $custCardId, 'card_status' => 'A', 'modified_by' => $request['apploginuser']);
			    $this->Mainmodel->update_cust_card($master_card_where,$cust_card_master_update);
				}
			 }
		  }
		}
        }         
        json_response($json); 
    }
 
	
    public function get_expiry_date_post() {
        $URL= $this->apiCred['url'].'/Common_api/get_expiry_date';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
    }
    public function get_student_standard_categories_post() {
        $URL= $this->apiCred['url'].'/Common_api/get_student_standard_categories';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json)); 
        json_response($json);
    }
    public function get_student_standards_post() {
        $URL= $this->apiCred['url'].'/Common_api/get_student_standards';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
    }
    public function get_dashboard_details_post_old() {
        $URL= $this->apiCred['url'].'/Common_api/get_dashboard_details';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);

    }
    
     public function get_dashboard_details_post() {
        $user_name = $this->input->post('apploginuser');
        $version_id = $this->input->post('version_id');
        $session_id = $this->input->post('session_id');

        $this->form_validation->set_rules('apploginuser', 'App Login User Id', 'trim|required|xss_clean');
        $this->form_validation->set_rules('version_id', 'Version Id', 'trim|required|xss_clean');
        $this->form_validation->set_rules('session_id', 'Session Id', 'trim|required|xss_clean');

        if ($this->form_validation->run()) {
            $user_name = trim($user_name);
            $returnArr = $this->Common_api_model->getDashboardDetails($user_name);
         
            if (empty($returnArr)) {
                $message = array('status' => 'failure', 'msg' => 'No details found.');
                json_response($message);
               }
              $message = array('status' => 'success', 'msg' => 'success', 'data' => $returnArr);
              json_response($message);
        } else {
            $error = $this->form_validation->error_array();
            $message = array('status' => 'failure', 'msg' => 'fail', 'data' => $error);
            json_response($message);;
        }
    }
    
    public function get_saral_data_post() {
        $URL= $this->apiCred['url'].'/Common_api/get_saral_data';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
    }
    public function getstudproof_desk_post() {
        $URL= $this->apiCred['url'].'/Common_api/getstudproof_desk';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
       json_response($json);
   }
    public function get_last_trans_details_post() {
        $URL= $this->apiCred['url'].'/Common_api/get_last_trans_details';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
         //        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json)); 
        json_response($json);
    }
    public function get_route_details_post() {
        $URL= $this->apiCred['url'].'/Common_api/get_route_details';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
         //        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        json_response($json);
    }
    

    //edited by sonali on 1-april-19
     public function get_wallet_amount_post()
     {

	

        $request = $this->post();  
        $amount = getWalletAmount($request);
        $total_cards = $this->Otc_card_inventory_model->getTotalCards($request);
        //show($total_cards,1);
        if (!empty($amount) && ($total_cards>=0)) {
            $response_data = array(
                            'amount' => $amount,
                            'total_otc_card' => $total_cards);
          
            $json = array('status' => 'success',"msg" => "successfully loaded details",'data' => $response_data);    
        }else{
            $json = array('status' => 'failure',"msg" => "error occured");
        }
         json_response($json);
      }
    
      public function getTwCreatedStatus_post() {
        $URL= $this->apiCred['url'].'/Common_api/getTwCreatedStatus';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
        json_response($json);
    }
    
     public function remittanceSlip_post() {
        $URL= $this->apiCred['url'].'/Common_api/remittanceSlip';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
        json_response($json);
    }
    
	 public function remittance_old_post() {
        $URL= $this->apiCred['url'].'/Common_api/remittance';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
        json_response($json);
    }
    
        public function remittance_post(){ 
        $this->form_validation->set_rules('apploginuser','App Login User Id','trim|required|xss_clean');
        $this->form_validation->set_rules('version_id','Version Id','trim|required|xss_clean');
        $this->form_validation->set_rules('session_id','Session Id','trim|required|xss_clean');
        $this->form_validation->set_rules('date','date','trim|required|xss_clean');

        if($this->form_validation->run()){
            $input = $this->input->post();
            $result = $this->Common_api_model->get_remittance($input);
            if(!empty($result)){
           		
			  $message = array('status' => 'success', 'msg' => 'success', 'data' => array('getRemittaceSummaryReport'=>$result));
              json_response($message);
				
				
            }else{
                $message = array('status'=>'failure','msg' =>'failure','data'=>'No Data Found');
                json_response($message);
            }

        }else{
            $error = $this->form_validation->error_array();
         	 $message = array('status' => 'failure', 'msg' => 'fail', 'data' => $error);
            json_response($message);;
        }
    }
    

	
      public function getRequestIdForLTPOS_post() {
        $URL= $this->apiCred['url'].'/Common_api/getRequestIdForLTPOS';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
		//print_R($data);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
        if (strtolower($json->status) == 'success') 
	{
          //$success = $this->Common_api_model->insertData($data,'api_request_response');
         }
        json_response($json);
    }
      
    public function updateResponseForLTPOS_post() {
        $URL= $this->apiCred['url'].'/Common_api/updateResponseForLTPOS';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
       // $success = $this->Common_api_model->updateLTPOSResponse($input);

        json_response($json);
    }
    
     public function get_receiptList_post() {
        $URL= $this->apiCred['url'].'/Common_api/get_receiptList';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');


        json_response($json);
    }
         public function re_printReceipt_post() {
        $URL= $this->apiCred['url'].'/Common_api/re_printReceipt';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        /*code added to change order of GST code start here   */
        $temp_arr=[] ;
        foreach($json->data[0]->card_fee_details as $key => $value) {
                 if($value->fee_type_cd=='BF'  )
                 {
                      $temp_arr[]= $value ;
                 }
         }
        foreach($json->data[0]->card_fee_details as $key => $value) {
                 if($value->fee_type_cd=='GST' )
                 {
                      $temp_arr[]= $value ;
                 }
         } 

        foreach($json->data[0]->card_fee_details as $key => $value) {
               if($value->fee_type_cd=='BF' || $value->fee_type_cd=='GST' )
                 {
                 }else{
                   $temp_arr[]= $value ;
                 }
         } 
                    $json->data[0]->card_fee_details=     $temp_arr ;
                /*code added to change order of GST code end here   */
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');

        json_response($json);
    }
      public function googleTranslation_post() {
        $URL= $this->apiCred['url'].'/Common_api/googleTranslation';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');

        json_response($json);
    }
        
	public function get_registration_Data_post() 
       {
			 $URL = $this->apiCred['url'] . '/Common_api/get_registration_Data';
			$request = $this->post();
			
			$itms_request = $this->post();
			$itms_request['apploginuser'] = $this->user_id ;
			$itms_request['session_id'] = $this->session_id ;
			
			$data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
			$json = json_decode($data);
			//json_encode($this->response($json));
			log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
		   
			 json_response($json);
}

 public function transliterate_post() {
        $URL= $this->apiCred['url'].'/Common_api/transliterate';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;

        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_common_api_' . date('d-m-Y') . '.log', $json, 'api');
       // $this->response($data);

        json_response($json);
    }
	
public function flash_msg_post() {
        $message = array('status' => 'success', 'msg' => 'welcome');
        //$this->set_response($message, 200);
        json_encode($this->response($message));
   }
        
}

?>