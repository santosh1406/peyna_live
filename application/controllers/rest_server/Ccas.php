<?php

//added by harshada kulkarni on 26-09-2018
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';

class Ccas extends REST_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model("Ccas_model");
        $this->header = array(
            "Content-Type:application/json",
            "Authorization:token 67b3889e211c607d39b8c3edb3c66c70adfe3d46"
        );
        $this->sessionData["user_id"] = "123";
        $this->sessionData["username"] = "9595251197";
        $this->sessionData["password"] = "Trimax@123";
    }

    public function customerSearch_post() {

        $url = "http://52.25.13.115/citycash/customerSearch/";
        $data = array(
            "txnType" => "customer_search",
            "cardType" => "OTC",
            "custIdentifier" => array(
                "mobileNumber" => "8509898933",
                "custCardId" => ""
            ),
            "requestId" => "as4d6a5s4d56a4d",
            "initiatorId" => "desktop",
            "initiatorType" => "DP"
        );
        $json_data = json_encode($data);
        $curlResult = curlForPostData($url, $json_data, $this->sessionData, $this->header);
        $returnArr = json_decode($curlResult, 1);

        if (!empty($curlResult)) {
            if (isset($returnArr['responseCode'])) {
                if ($returnArr['responseCode'] == RESPONSE_CODE_SUCCESS_CC) {
                    $result['responseCode'] = $returnArr['responseCode'];
                    $result['responseMessage'] = $returnArr['responseMessage'];
                    $result['requestId'] = $returnArr['requestId'];
                    $message = array('status' => 'success', 'msg' => 'Success', 'data' => $result);
                    $this->response(array('status' => 'success', 'data' => $result, 'msg' => 'Success'), 200);
                } else if ($returnArr['responseCode'] == RESPONSE_CODE_FAIL_CC) {
                    $result['responseCode'] = $returnArr['responseCode'];
                    $result['responseMessage'] = $returnArr['responseMessage'];
                    $result['requestId'] = $returnArr['requestId'];
                    $this->response(array('status' => 'failure', 'data' => $result, 'msg' => 'Failure'), 200);
                } else {
                    $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
                }
            } else {
                $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
            }
        } else {
            $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
        }
    }

    public function swCheckLimit_post() {

        $url = "http://52.25.13.115/citycash/CheckLimit/";
        $data = array(
            "txnType" => "sw_limit",
            "custIdentifier" => array(
                "mobileNumber" => "7977366429",
                "custCardId" => "123456789"
            ),
            "requestId" => "as4d6a5s4d56a4d",
            "initiatorId" => "23352155",
            "initiatorType" => "LTP",
            "amount" => "100"
        );

        $json_data = json_encode($data);
        $curlResult = curlForPostData($url, $json_data, $this->sessionData, $this->header);
        $returnArr = json_decode($curlResult, 1);

        if (!empty($curlResult)) {
            if (isset($returnArr['responseCode'])) {
                if ($returnArr['responseCode'] == RESPONSE_CODE_SUCCESS_CC) {
                    $result['responseCode'] = $returnArr['responseCode'];
                    $result['responseMessage'] = $returnArr['responseMessage'];
                    $result['requestId'] = $returnArr['requestId'];
                    $message = array('status' => 'success', 'msg' => 'Success', 'data' => $result);
                    $this->response(array('status' => 'success', 'data' => $result, 'msg' => 'Success'), 200);
                } else if ($returnArr['responseCode'] == RESPONSE_CODE_FAIL_CC) {
                    $result['responseCode'] = $returnArr['responseCode'];
                    $result['responseMessage'] = $returnArr['responseMessage'];
                    $result['requestId'] = $returnArr['requestId'];
                    $this->response(array('status' => 'failure', 'data' => $result, 'msg' => 'Failure'), 200);
                } else {
                    $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
                }
            } else {
                $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
            }
        } else {
            $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
        }
    }

    public function swTopup_post() {

        $url = "http://52.25.13.115/citycash/Topup/";
        $data = array(
            "txnType" => "sw_topup",
            "paymentMode" => "CASH",
            "custIdentifier" => array(
                "mobileNumber" => "7977366429",
                "custCardId" => "123456789"
            ),
            "amount" => "1",
            "requestId" => "as4d6a5s4d56a4d",
            "initiatorId" => "23352155",
            "initiatorType" => "LTP"
        );

        $json_data = json_encode($data);
        $curlResult = curlForPostData($url, $json_data, $this->sessionData, $this->header);
        $returnArr = json_decode($curlResult, 1);

        if (!empty($curlResult)) {
            if (isset($returnArr['responseCode'])) {
                if ($returnArr['responseCode'] == RESPONSE_CODE_SUCCESS_CC) {
                    $result['responseCode'] = $returnArr['responseCode'];
                    $result['responseMessage'] = $returnArr['responseMessage'];
                    $result['requestId'] = $returnArr['requestId'];
                    $message = array('status' => 'success', 'msg' => 'Success', 'data' => $result);
                    $this->response(array('status' => 'success', 'data' => $result, 'msg' => 'Success'), 200);
                } else if ($returnArr['responseCode'] == RESPONSE_CODE_FAIL_CC) {
                    $result['responseCode'] = $returnArr['responseCode'];
                    $result['responseMessage'] = $returnArr['responseMessage'];
                    $result['requestId'] = $returnArr['requestId'];
                    $this->response(array('status' => 'failure', 'data' => $result, 'msg' => 'Failure'), 200);
                } else {
                    $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
                }
            } else {
                $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
            }
        } else {
            $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
        }
    }

    public function twCheckLimit_post() {

        $url = "http://52.25.13.115/citycash/CheckLimit/";
        $data = array(
            "txnType" => "tw_limit",
            "custIdentifier" => array(
                "mobileNumber" => "7977366429",
                "custCardId" => "123456789"
            ),
            "requestId" => "as4d6a5s4d56a4d",
            "initiatorId" => "23352155",
            "initiatorType" => "LTP",
            "amount" => "100"
        );

        $json_data = json_encode($data);
        $curlResult = curlForPostData($url, $json_data, $this->sessionData, $this->header);
        $returnArr = json_decode($curlResult, 1);

        if (!empty($curlResult)) {
            if (isset($returnArr['responseCode'])) {
                if ($returnArr['responseCode'] == RESPONSE_CODE_SUCCESS_CC) {
                    $result['responseCode'] = $returnArr['responseCode'];
                    $result['responseMessage'] = $returnArr['responseMessage'];
                    $result['requestId'] = $returnArr['requestId'];
                    $message = array('status' => 'success', 'msg' => 'Success', 'data' => $result);
                    $this->response(array('status' => 'success', 'data' => $result, 'msg' => 'Success'), 200);
                } else if ($returnArr['responseCode'] == RESPONSE_CODE_FAIL_CC) {
                    $result['responseCode'] = $returnArr['responseCode'];
                    $result['responseMessage'] = $returnArr['responseMessage'];
                    $result['requestId'] = $returnArr['requestId'];
                    $this->response(array('status' => 'failure', 'data' => $result, 'msg' => 'Failure'), 200);
                } else {
                    $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
                }
            } else {
                $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
            }
        } else {
            $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
        }
    }

    public function twCardList_post() {

        $url = "http://52.25.13.115/citycash/GetCardList/";
        $data = array(
            "txnType" => "tw_topup",
            "custIdentifier" => array(
                "mobileNumber" => "7977366429"
            ),
            "requestId" => "as4d6a5s4d56a4d",
            "initiatorId" => "23352155",
            "initiatorType" => "LTP",
            "amount" => "100"
        );

        $json_data = json_encode($data);
        $curlResult = curlForPostData($url, $json_data, $this->sessionData, $this->header);
        $returnArr = json_decode($curlResult, 1);

        if (!empty($curlResult)) {
            if (isset($returnArr['responseCode'])) {
                if ($returnArr['responseCode'] == RESPONSE_CODE_SUCCESS_CC) {
                    $result['responseCode'] = $returnArr['responseCode'];
                    $result['responseMessage'] = $returnArr['responseMessage'];
                    $result['requestId'] = $returnArr['requestId'];
                    $message = array('status' => 'success', 'msg' => 'Success', 'data' => $result);
                    $this->response(array('status' => 'success', 'data' => $result, 'msg' => 'Success'), 200);
                } else if ($returnArr['responseCode'] == RESPONSE_CODE_FAIL_CC) {
                    $result['responseCode'] = $returnArr['responseCode'];
                    $result['responseMessage'] = $returnArr['responseMessage'];
                    $result['requestId'] = $returnArr['requestId'];
                    $this->response(array('status' => 'failure', 'data' => $result, 'msg' => 'Failure'), 200);
                } else {
                    $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
                }
            } else {
                $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
            }
        } else {
            $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
        }
    }

    public function twTopup_post() {

        $url = "http://52.25.13.115/citycash/Topup/";
        $data = array(
            "txnType" => "tw_topup",
            "paymentMode" => "CASH",
            "custIdentifier" => array(
                "mobileNumber" => "7977366429",
                "custCardId" => "123456789"
            ),
            "amount" => "100",
            "requestId" => "as4d6a5s4d56a4d",
            "initiatorId" => "23352155",
            "initiatorType" => "LTP"
        );

        $json_data = json_encode($data);
        $curlResult = curlForPostData($url, $json_data, $this->sessionData, $this->header);
        $returnArr = json_decode($curlResult, 1);

        if (!empty($curlResult)) {
            if (isset($returnArr['responseCode'])) {
                if ($returnArr['responseCode'] == RESPONSE_CODE_SUCCESS_CC) {
                    $result['responseCode'] = $returnArr['responseCode'];
                    $result['responseMessage'] = $returnArr['responseMessage'];
                    $result['requestId'] = $returnArr['requestId'];
                    $message = array('status' => 'success', 'msg' => 'Success', 'data' => $result);
                    $this->response(array('status' => 'success', 'data' => $result, 'msg' => 'Success'), 200);
                } else if ($returnArr['responseCode'] == RESPONSE_CODE_FAIL_CC) {
                    $result['responseCode'] = $returnArr['responseCode'];
                    $result['responseMessage'] = $returnArr['responseMessage'];
                    $result['requestId'] = $returnArr['requestId'];
                    $this->response(array('status' => 'failure', 'data' => $result, 'msg' => 'Failure'), 200);
                } else {
                    $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
                }
            } else {
                $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
            }
        } else {
            $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
        }
    }

    public function ccasTopup_post() {

        $data = $result = array();
        $input = $this->post();
        $getRequestId = getRandomId(15);

        if (!empty($input)) {

            $config = array(
                array('field' => 'txnType', 'label' => 'Transaction Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter transaction type.')),
                array('field' => 'mobileNumber', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.')),
                array('field' => 'custCardId', 'label' => 'Card Number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter card number.')),
                array('field' => 'initiatorId', 'label' => 'Initiator Id', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter initiator id.')),
                array('field' => 'initiatorType', 'label' => 'Initiator Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter initiator type.')),
                array('field' => 'amount', 'label' => 'Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter amount.')),
                array('field' => 'paymentmode', 'label' => 'Payment Mode', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter payment mode.')),
            );

            if (form_validate_rules($config) == FALSE) {
                $error = $this->form_validation->error_array();
                //log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ccasTopup.log', $error, 'input-error-Response');
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {
                $input['requestId'] = $getRequestId;
                $input_json = json_encode($input);
                //log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ccasTopup.log', $input, 'desktop-input-request');   

                $transaction_data = array(
                    "request_id" => $input['requestId'],
                    "mobile_number" => $input['mobileNumber'],
                    "request" => $input_json,
                    "created_by" => $this->sessionData["user_id"],
                    "created_at" => date("Y-m-d H:i:s", time()),
                );
                $last_instered_id = $this->Ccas_model->saveTransactionData($transaction_data);

                $customer_search = $this->customerSearch($input);
                $result["customer_search"] = $customer_search;
                $reponse_json = json_encode($result);
                //log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ccasTopup.log', $customer_search, 'customer-search-result');                
                //temporary success response for customer search
                $customer_search['responseCode'] = "200";
                $customer_search['responseMessage'] = "Customer available";
                //temporary success response for customer search
                if ($customer_search['responseCode'] == RESPONSE_CODE_SUCCESS_CC && $customer_search['responseMessage'] == "Customer available") {
                    $update_transaction_data = $this->Ccas_model->updateTransactionData($last_instered_id, $reponse_json, "success");
                    if ($input['txnType'] == 'sw_topup') {
                        $sw_check_limit = $this->swCheckLimit($input);
                        $result["sw_check_limit"] = $sw_check_limit;
                        $reponse_json = json_encode($result);
                        //log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ccasTopup.log', $sw_check_limit, 'sw-check-limit');
                        //temporary success response for sw check limit                        
                        $sw_check_limit['responseCode'] = "200";
                        $sw_check_limit['responseMessage'] = "Success";
                        //temporary success response for sw check limit
                        if ($sw_check_limit['responseCode'] == RESPONSE_CODE_SUCCESS_CC && $sw_check_limit['responseMessage'] == "Success") {
                            $update_transaction_data = $this->Ccas_model->updateTransactionData($last_instered_id, $reponse_json, "success");
                            $sw_top_up = $this->topUp($input);
                            $result["sw_top_up"] = $sw_top_up;
                            $reponse_json = json_encode($result);
                            //log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ccasTopup.log', $sw_top_up, 'sw-top-up');
                            //temporary success response for sw topup
                            $sw_top_up['responseCode'] = "200";
                            $sw_top_up['responseMessage'] = "Success";
                            //temporary success response for sw topup
                            if ($sw_top_up['responseCode'] == RESPONSE_CODE_SUCCESS_CC && $sw_top_up['responseMessage'] == "Success") {
                                $update_transaction_data = $this->Ccas_model->updateTransactionData($last_instered_id, $reponse_json, "success");
                                $this->response(array('status' => 'success', 'data' => $sw_top_up), 200);
                            } else if ($sw_top_up['responseCode'] == RESPONSE_CODE_FAIL_CC) {
                                $update_transaction_data = $this->Ccas_model->updateTransactionData($last_instered_id, $reponse_json, "failure");
                                $this->response(array('status' => 'failure', 'data' => $sw_top_up, 'msg' => $sw_top_up['responseMessage']), 200);
                            } else {
                                $update_transaction_data = $this->Ccas_model->updateTransactionData($last_instered_id, $reponse_json, "failure");
                                $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
                            }
                        } else if ($sw_check_limit['responseCode'] == RESPONSE_CODE_FAIL_CC) {
                            $update_transaction_data = $this->Ccas_model->updateTransactionData($last_instered_id, $reponse_json, "failure");
                            $this->response(array('status' => 'failure', 'data' => $sw_check_limit, 'msg' => $sw_check_limit['responseMessage']), 200);
                        } else {
                            $update_transaction_data = $this->Ccas_model->updateTransactionData($last_instered_id, $reponse_json, "failure");
                            $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
                        }
                    }
                    if ($input['txnType'] == 'tw_topup') {
                        $tw_check_limit = $this->twCheckLimit($input);
                        $result["tw_check_limit"] = $tw_check_limit;
                        $reponse_json = json_encode($result);
                        //log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ccasTopup.log', $tw_check_limit, 'tw-check-limit');
                        //temporary success response for tw check limit                        
                        $tw_check_limit['responseCode'] = "200";
                        $tw_check_limit['responseMessage'] = "Success";
                        //temporary success response for tw check limit
                        if ($tw_check_limit['responseCode'] == RESPONSE_CODE_SUCCESS_CC && $tw_check_limit['responseMessage'] == "Success") {
                            $update_transaction_data = $this->Ccas_model->updateTransactionData($last_instered_id, $reponse_json, "success");
                            $tw_top_up = $this->topUp($input);
                            $result["tw_top_up"] = $tw_top_up;
                            $reponse_json = json_encode($result);
                            //log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ccasTopup.log', $tw_top_up, 'tw-top-up');
                            //temporary success response for tw topup
                            $tw_top_up['responseCode'] = "200";
                            $tw_top_up['responseMessage'] = "Success";
                            //temporary success response for tw topup
                            if ($tw_top_up['responseCode'] == RESPONSE_CODE_SUCCESS_CC && $tw_top_up['responseMessage'] == "Success") {
                                $update_transaction_data = $this->Ccas_model->updateTransactionData($last_instered_id, $reponse_json, "success");
                                $this->response(array('status' => 'success', 'data' => $tw_top_up), 200);
                            } else if ($tw_top_up['responseCode'] == RESPONSE_CODE_FAIL_CC) {
                                $update_transaction_data = $this->Ccas_model->updateTransactionData($last_instered_id, $reponse_json, "failure");
                                $this->response(array('status' => 'failure', 'data' => $tw_top_up, 'msg' => $tw_top_up['responseMessage']), 200);
                            } else {
                                $update_transaction_data = $this->Ccas_model->updateTransactionData($last_instered_id, $reponse_json, "failure");
                                $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
                            }
                        } else if ($tw_check_limit['responseCode'] == RESPONSE_CODE_FAIL_CC) {
                            $update_transaction_data = $this->Ccas_model->updateTransactionData($last_instered_id, $reponse_json, "failure");
                            $this->response(array('status' => 'failure', 'data' => $tw_check_limit, 'msg' => $tw_check_limit['responseMessage']), 200);
                        } else {
                            $update_transaction_data = $this->Ccas_model->updateTransactionData($last_instered_id, $reponse_json, "failure");
                            $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
                        }
                    }
                } else if ($customer_search['responseCode'] == RESPONSE_CODE_FAIL_CC) {
                    $update_transaction_data = $this->Ccas_model->updateTransactionData($last_instered_id, $reponse_json, "failure");
                    $this->response(array('status' => 'failure', 'data' => $customer_search, 'msg' => $customer_search['responseMessage']), 200);
                } else {
                    $update_transaction_data = $this->Ccas_model->updateTransactionData($last_instered_id, $reponse_json, "failure");
                    $this->response(array('status' => 'failure', 'msg' => 'No data found'), 200);
                }
            }
        } else {
            $this->response(array('status' => 'failed', 'data' => '', 'msg' => 'Please enter input fields'), 200);
        }
    }

    public function customerSearch($input = "") {

        $url = "http://52.25.13.115/citycash/customerSearch/";
        $data = array(
            "txnType" => "customer_search",
            "cardType" => "OTC",
            "custIdentifier" => array(
                "mobileNumber" => $input['mobileNumber'],
                "custCardId" => ""
            ),
            "requestId" => $input['requestId'],
            "initiatorId" => "desktop",
            "initiatorType" => "DP"
        );
        $json_data = json_encode($data);
        $curlResult = curlForPostData($url, $json_data, $this->sessionData, $this->header);
        $returnArr = json_decode($curlResult, 1);
        return $returnArr;
    }

    public function swCheckLimit($input = "") {

        $url = "http://52.25.13.115/citycash/CheckLimit/";
        $data = array(
            "txnType" => "sw_limit",
            "custIdentifier" => array(
                "mobileNumber" => $input['mobileNumber'],
                "custCardId" => $input['custCardId']
            ),
            "requestId" => $input['requestId'],
            "initiatorId" => $input['initiatorId'],
            "initiatorType" => $input['initiatorType'],
            "amount" => $input['amount']
        );
        $json_data = json_encode($data);
        $curlResult = curlForPostData($url, $json_data, $this->sessionData, $this->header);
        $returnArr = json_decode($curlResult, 1);
        return $returnArr;
    }

    public function twCheckLimit($input = "") {

        $url = "http://52.25.13.115/citycash/CheckLimit/";
        $data = array(
            "txnType" => "tw_limit",
            "custIdentifier" => array(
                "mobileNumber" => $input['mobileNumber'],
                "custCardId" => $input['custCardId']
            ),
            "requestId" => $input['requestId'],
            "initiatorId" => $input['initiatorId'],
            "initiatorType" => $input['initiatorType'],
            "amount" => $input['amount']
        );

        $json_data = json_encode($data);
        $curlResult = curlForPostData($url, $json_data, $this->sessionData, $this->header);
        $returnArr = json_decode($curlResult, 1);
        return $returnArr;
    }

    public function topUp($input = "") {

        $url = "http://52.25.13.115/citycash/Topup/";
        $data = array(
            "txnType" => $input['txnType'],
            "paymentMode" => $input['paymentmode'],
            "custIdentifier" => array(
                "mobileNumber" => $input['mobileNumber'],
                "custCardId" => $input['custCardId']
            ),
            "amount" => $input['amount'],
            "requestId" => $input['requestId'],
            "initiatorId" => $input['initiatorId'],
            "initiatorType" => $input['initiatorType']
        );
        $json_data = json_encode($data);
        $curlResult = curlForPostData($url, $json_data, $this->sessionData, $this->header);
        $returnArr = json_decode($curlResult, 1);
        return $returnArr;
    }

}

// End of Class
