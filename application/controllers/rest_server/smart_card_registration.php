<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Smart_card_registration extends REST_Controller {

    public function __construct() { 
        parent::__construct();
        
        //load user model
        $this->load->helper('smartcard_helper');
    }

    public function get_occupation_list_post() {
        $apiCred = json_decode(SMART_CARD_CREDS,true);

        $header_value = $apiCred['X_API_KEY'];
        $URL= $apiCred['url'].'/Common_api/getOccupationmaster';
        $header = array(
            'X-API-KEY:'.$header_value   
        );

        $basicAuth = array(
        'username' => $apiCred['username'],
        'password' => $apiCred['password']
        );

        $request = $this->post();
        $data = curlForPost($URL,$request,$basicAuth, $header);
        $json = json_decode($data);
        json_encode($this->response($json));
        
    }

    public function customer_search_post() {
        $apiCred = json_decode(SMART_CARD_CREDS,true);

        $header_value = $apiCred['X_API_KEY'];
        $URL= $apiCred['url'].'/CCAS_api/customer_search';
        $header = array(
            'X-API-KEY:'.$header_value   
        );

        $basicAuth = array(
        'username' => $apiCred['username'],
        'password' => $apiCred['password']
        );

        $request = $this->post();
        $data = curlForPost($URL,$request,$basicAuth, $header);
        $json = json_decode($data);
        json_encode($this->response($json));
        
    }

        
}

?>