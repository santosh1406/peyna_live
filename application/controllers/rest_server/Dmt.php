<?php

//Created by pooja kambali on 27-09-2018

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Dmt extends REST_Controller {


    function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('Dmt_model');
    }

    public function neft_post() {
        header('Content-Type: application/json');
        $data = array();
        $input = $this->post();
        if (!empty($input)) {
            $curl_url = DMT_URL . "neft";
            // show($curl_url, 1);
          //$curl_url = "http://10.20.107.140/rokad/rest_server/Dmt/neft";

            $currdate = date('Y-m-d H:m:s');
            $data['created_on'] = $currdate;
            $input['rfu2'] = '';
            $input['rfu3'] = '';
            $data['recharge_from'] = $input['recharge_from'];
            $wallet_tran_id = substr(hexdec(uniqid()), 4, 12);

            $config = array(
                array('field' => 'CustomerMobileNo', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|xss_clean|min_length[10]', 'errors' => array('required' => 'Please enter mobile number.')),
                array('field' => 'BeneIFSCCode', 'label' => 'IFSC Code', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter ifsc code.')),
                array('field' => 'BeneAccountNo', 'label' => 'Beneficiary Account Number.', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter Beneficiary Account number.')),
                array('field' => 'BeneName', 'label' => 'Beneficiary Name.', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Beneficiary Name.')),
                array('field' => 'Amount', 'label' => 'Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter Amount.')),
                array('field' => 'CustomerName', 'label' => 'Customer Name.', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Customer Name.')),
                array('field' => 'rfu1', 'label' => 'Remarks', 'rules' => 'trim|xss_clean', 'errors' => array('required' => 'Please enter Remark'))
            );

            $client_id = rand();
            // echo 'test';exit;

            if (form_validate_rules($config) == False) {
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {
                $data['user_id'] = $input['user_id'];
                $data['type'] = $input['type'];
                $data['ClientUniqueID'] = $client_id;
                $data['CustomerMobileNo'] = $input['CustomerMobileNo'];
                $data['BeneIFSCCode'] = $input['BeneIFSCCode'];
                $data['BeneAccountNo'] = $input['BeneAccountNo'];
                $data['BeneName'] = $input['BeneName'];
                $data['Amount'] = $input['Amount'];
                $data['customer_charge'] = $input['customer_charge'];
                $data['CustomerName'] = $input['CustomerName'];
                $data['rfu1'] = $input['rfu1'];
                $data['rfu2'] = $input['rfu2'];
                $data['rfu3'] = $input['rfu3'];
                $data['transaction_no']=$wallet_tran_id;
                
                $user_id = $data['user_id'];
                $check_user_wallet = $this->Dmt_model->getWalletDetails($user_id);
                // /echo "a: ".$check_user_wallet[0]['amt'];
                if ($check_user_wallet[0]['amt'] > ($data['Amount'] + $input['customer_charge'])) {

                   // $transfer = $this->Dmt_model->neft($data);
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dmt.log', $data, 'Input-Operator');
                    // show($curl_url, 1);
                    $curlResult = curlForPostData_dmt1($curl_url, $data);
                    // echo $curlResult;exit;
                    $respArr = json_decode($curlResult, 1);

                    if ($respArr['MessageString'] == 'Transaction Successful') {
                        $transfer = $this->Dmt_model->neft($data);
                        $commissionResult = $this->serviceRetailerCommissionDistribution($input, $wallet_tran_id);
                        $updated_wallet_amt = $check_user_wallet[0]['amt'] - ($data['Amount'] + $input['customer_charge']);
                        $update_wallet = $this->Dmt_model->updateWallet($user_id, $updated_wallet_amt);
                       
                        $transaction_data = array(
                                'transaction_no' =>$wallet_tran_id,
                                'w_id' => $check_user_wallet[0]['id'],
                                'amt' => $data['Amount'] + $input['customer_charge'],
                                'comment' => 'Fino Transaction',
                                'amt_before_trans' => $check_user_wallet[0]['amt'],
                                'amt_after_trans' => $updated_wallet_amt,
                                'status' =>'debited',
                                'user_id' => $user_id
                            );
                        $up_wall_amt = $this->Dmt_model->insertTransWallet($transaction_data);


                    }
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dmt_response.log', $respArr, 'Input-Operator');
                    $up = $this->Dmt_model->neftupdate($respArr);
                    $this->response(array('status' => 'Success', 'data' => $curlResult), 200);
                   //echo  $curlResult;
                    //exit;
                } else {

                    $this->response(array('status' => 'Failed', 'msg' => 'Insufficient balance'), 200);
                }
            }
        }
    }
    
    
    

    public function serviceRetailerCommissionDistribution($input = "", $trans_ref_no = "") 
    {
        $user_id = $input['user_id'];
        $amount = $input['Amount'];

        $service_id = DMT_SERVICE_ID;
        $getAgentServicesCommission = $this->Dmt_model->getAgentServicesCommission($user_id, $service_id, $amount);
        if ($getAgentServicesCommission) {

            $agent_id = $getAgentServicesCommission[0]['id'];
            $agent_code = $getAgentServicesCommission[0]['agent_code'];
            $distributor_id = $getAgentServicesCommission[0]['level_5'];
            $Area_dis_id = $getAgentServicesCommission[0]['level_4'];
            $master_dis_id = $getAgentServicesCommission[0]['level_3'];
            $company_id = $getAgentServicesCommission[0]['level_2'];
            $trimax_id = $getAgentServicesCommission[0]['level_1'];
            $service_id = $getAgentServicesCommission[0]['service_id'];
            $commission_id = $getAgentServicesCommission[0]['commission_id'];
            $slab_for_calculation = $getAgentServicesCommission[0]['upper_slab'];
            $total_commission = $getAgentServicesCommission[0]['total_commission'];

            $values = json_decode($getAgentServicesCommission[0]['values']);

            $trimax_commission = '0';
            $company_commission = '0';
            $md_commission = '0';
            $ad_commission = '0';
            $dist_commission = '0';
            $retailer_commission = '0';
            $with_gst = $without_gst = 0;
            if (!empty($values)) {
                $trimax_commission = round($values->Trimax, 2);
                $company_commission = round($values->Rokad, 2);
                $md_commission = round($values->MD, 2);
                $ad_commission = round($values->AD, 2);
                $dist_commission = round($values->Distributor, 2);
                $retailer_commission = round($values->Retailer, 2);

                $recharge_amount = $slab_for_calculation;
                // $amount = $input['Amount'] - $getAgentServicesCommission[0]['customer_charge']; //transaction amount
                // $commission_percent = DMT_TRIMAX_COMMISSION; //trimax_comm_percent                                                       
                $commission_percent = !empty($total_commission) ? $total_commission : '0';
                $trimax_cal = calculateGstTdsDMT($recharge_amount, $commission_percent);
                $earnings = $trimax_cal['earnings']; //trimax_earnings
                $gst = $trimax_cal['gst']; //trimax_gst                            
                $tds = $trimax_cal['tds']; //trimax_tds
                $final_amount = $trimax_cal['final_amount']; //trimax_final_amt                                                        

                $rokad_trimax_cal = calculateGstTdsDMT($earnings, $trimax_commission);
                $rokad_trimax_amt = $rokad_trimax_cal['earnings']; //rokad_trimax_amt
                $with_gst = $with_gst + $rokad_trimax_cal['earnings'];
                $rokad_trimax_cal_with_tds_gst = calculateGstTdsDMT($final_amount, $trimax_commission);
                $rokad_trimax_amt_with_tds_gst = $rokad_trimax_cal_with_tds_gst['earnings']; //rokad_trimax_amt_with_tds_gst
                $without_gst = $without_gst + $rokad_trimax_amt_with_tds_gst;        

                $company_cal = calculateGstTdsDMT($earnings, $company_commission);
                $company_amt = $company_cal['earnings'];//company gst
                $with_gst = $with_gst + $company_cal['earnings'];
                $company_cal_with_tds_gst = calculateGstTdsDMT($final_amount, $company_commission);
                $company_amt_with_tds_gst = $company_cal_with_tds_gst['earnings']; //company_amt_with_tds_gst                    
                $without_gst = $without_gst + $company_amt_with_tds_gst;    

                $rd_cal = calculateGstTdsDMT($earnings, $md_commission);
                $rd_amt = $rd_cal['earnings']; //rd_amt            
                $with_gst = $with_gst + $rd_cal['earnings'];                 
                $rd_cal_with_tds_gst = calculateGstTdsDMT($final_amount, $md_commission);
                $rd_amt_with_tds_gst = $rd_cal_with_tds_gst['earnings']; //rd_amt_with_tds_gst
                $without_gst = $without_gst + $rd_amt_with_tds_gst;

                $dd_cal = calculateGstTdsDMT($earnings, $ad_commission);
                $dd_amt = $dd_cal['earnings']; //dd_amt   
                $with_gst = $with_gst + $dd_cal['earnings'];                                 
                $dd_cal_with_tds_gst = calculateGstTdsDMT($final_amount, $ad_commission);
                $dd_amt_with_tds_gst = $dd_cal_with_tds_gst['earnings']; //dd_amt_with_tds_gst
                $without_gst = $without_gst + $dd_amt_with_tds_gst;

                $ex_cal = calculateGstTdsDMT($earnings, $dist_commission);
                $ex_amt = $ex_cal['earnings']; //ex_amt      
                $with_gst = $with_gst + $ex_cal['earnings'];              
                $ex_cal_with_tds_gst = calculateGstTdsDMT($final_amount, $dist_commission);
                $ex_amt_with_tds_gst = $ex_cal_with_tds_gst['earnings']; //ex_amt_with_tds_gst
                $without_gst = $without_gst + $ex_amt_with_tds_gst;


                $sa_cal = calculateGstTdsDMT($earnings, $retailer_commission);
                $sa_amt = $sa_cal['earnings']; //sa_amt     
                $with_gst = $with_gst + $sa_cal['earnings'];              
                $sa_cal_with_tds_gst = calculateGstTdsDMT($final_amount, $retailer_commission);
                $sa_amt_with_tds_gst = $sa_cal_with_tds_gst['earnings']; //sa_amt_with_tds_gst 
                $without_gst = $without_gst + $sa_amt_with_tds_gst;

                $remaining_commission = round($with_gst, 2) - round($earnings, 2);

                /*For commission adjustment - Gopal */
                // $array_without_empty = array_filter((array)$values[0]);
                // $keys = array_keys($array_without_empty);
                // switch ($keys[0]) {
                //     case 'Trimax':
                //         $rokad_trimax_amt = $earnings - ($with_gst - $rokad_trimax_amt);
                //         $rokad_trimax_amt_with_tds_gst = $final_amount - ($without_gst - $rokad_trimax_amt_with_tds_gst); 
                //         break;
                //     case 'Rokad':
                //         $company_amt = $earnings - ($with_gst - $company_amt);
                //         $company_amt_with_tds_gst = $final_amount - ($without_gst - $company_amt_with_tds_gst);
                //         break;
                //     case 'MD':
                //         $rd_amt = $earnings - ($with_gst - $rd_amt);
                //         $rd_amt_with_tds_gst = $final_amount - ($without_gst - $rd_amt_with_tds_gst); 
                //         break;
                //     case 'AD':
                //         $dd_amt = $earnings - ($with_gst - $dd_amt);
                //         $dd_amt_with_tds_gst = $final_amount - ($without_gst - $dd_amt_with_tds_gst); 
                //         break;
                //     case 'Distributor':
                //         $ex_amt = $earnings - ($with_gst - $ex_amt);
                //         $ex_amt_with_tds_gst = $final_amount - ($without_gst - $ex_amt_with_tds_gst); 
                //         break;
                //     case 'Retailer':
                //         $sa_amt = $earnings - ($with_gst - $sa_amt);
                //         $sa_amt_with_tds_gst = $final_amount - ($without_gst - $sa_amt_with_tds_gst);
                //         break;    
                // }



                /*for commission adjustment  - Gopal */
            }

            $comm_distribution_array['user_id'] = $user_id;
            $comm_distribution_array['service_id'] = $service_id;
            $comm_distribution_array['commission_id'] = $commission_id;
            $comm_distribution_array['transaction_no'] = $trans_ref_no;
            $comm_distribution_array['transaction_amt'] = $amount;
            $comm_distribution_array['customer_charge'] = $input['customer_charge'];
            $comm_distribution_array['service_comm_percent'] = SERVICE_COMMISSION;
            $comm_distribution_array['trimax_comm_percent'] = DMT_TRIMAX_COMMISSION;
            $comm_distribution_array['gst_percentage'] = GST_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['tds_percentage'] = TDS_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['trimax_earnings'] = $earnings;
            $comm_distribution_array['trimax_gst'] = $gst;
            $comm_distribution_array['trimax_tds'] = $tds;
            $comm_distribution_array['trimax_final_amt'] = $final_amount;
            $comm_distribution_array['rokad_trimax_per'] = $values->Trimax;
            $comm_distribution_array['rokad_trimax_amt'] = $rokad_trimax_amt;
            $comm_distribution_array['rokad_trimax_amt_with_tds_gst'] = $rokad_trimax_amt_with_tds_gst;
            $comm_distribution_array['company_per'] = $values > Rokad;
            $comm_distribution_array['company_amt'] = $company_amt;
            $comm_distribution_array['company_amt_with_tds_gst'] = $company_amt_with_tds_gst;
            $comm_distribution_array['rd_per'] = $values->MD;
            $comm_distribution_array['rd_amt'] = $rd_amt;
            $comm_distribution_array['rd_amt_with_tds_gst'] = $rd_amt_with_tds_gst;
            $comm_distribution_array['dd_per'] = $values->AD;
            $comm_distribution_array['dd_amt'] = $dd_amt;
            $comm_distribution_array['dd_amt_with_tds_gst'] = $dd_amt_with_tds_gst;
            $comm_distribution_array['ex_per'] = $values->Distributor;
            $comm_distribution_array['ex_amt'] = $ex_amt;
            $comm_distribution_array['ex_amt_with_tds_gst'] = $ex_amt_with_tds_gst;
            $comm_distribution_array['sa_per'] = $values->Retailer;
            $comm_distribution_array['sa_amt'] = $sa_amt;
            $comm_distribution_array['sa_amt_with_tds_gst'] = $sa_amt_with_tds_gst;
            $comm_distribution_array['status'] = 'Y';
            $comm_distribution_array['created_by'] = $user_id;
            $comm_distribution_array['commission_difference'] = $remaining_commission;
            $comm_distribution_array['created_date'] = date('Y-m-d H:i:s');

            $id = $this->Dmt_model->saveCommissionData($comm_distribution_array);
            $comm_distribution_array['id'] = $id;
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dmt.log', $comm_distribution_array, 'Commission Distribution Response');

            return $comm_distribution_array;
        }
    }

     public function transaction_status_post() {
     header('Content-Type: application/json');
                 
        $data = array();
        $input = $this->post();
        if (!empty($input)) {
           $curl_url = DMT_URL . "transaction_status";

              //$curl_url = "http://10.20.107.140/rokad/rest_server/Dmt/transaction_status";
             $config = array(
                array('field' => 'ClientUniqueID', 'label' => 'Transaction', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter Transaction number.'))
                   );
       if (form_validate_rules($config) == False) {
               // echo "add";
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {
                
               $data['ClientUniqueID'] = $input['ClientUniqueID'];
             //  print_r($transaction);exit;
              // $check_user_wallet = $this->Dmt_model->getWalletDetails($user_id);
                //if ($check_user_wallet[0]['amt'] > $data['Amount']) {

                  //  $transfer = $this->Dmt_model->neft($data);
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'status.log', $data, 'Input-Operator');
                    $curlResult = curlForPostData_dmt1($curl_url, $data);

                    $respArr = json_decode($curlResult, 1);
                   /*if ($respArr['MessageString'] == 'Transaction Successful') {
                        $commissionResult = $this->serviceRetailerCommissionDistribution($input, $data['ClientUniqueID']);
                        $updated_wallet_amt = $check_user_wallet[0]['amt'] - $data['Amount'];
                       $update_wallet = $this->Dmt_model->updateWallet($user_id, $updated_wallet_amt);
                    }*/
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'status.log', $respArr, 'Input-Operator');
                  //  $up = $this->Dmt_model->neftupdate($respArr);

                    echo $curlResult;
                    exit;
                //} else {

                    //$this->response(array('status' => 'Failed', 'msg' => 'Insufficient balance'), 200);
                }
            }
        }
    }

    
//}
?>

