<?php

/* Created by Shrasti Bansal*/
if (!defined('BASEPATH')) exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Farecalculation extends REST_Controller {
    var $db2;

    public function __construct() { 
        parent::__construct();
            $this->load->helper(array('smartcard_helper','common_helper'));
            $this->load->model(array('Mainmodel'));
            $this->apiCred = json_decode(SMART_CARD_CREDS,true);
            $header_value = $this->apiCred['X_API_KEY'];
            $this->header = array(
                'X-API-KEY:'.$header_value   
            );

            $this->basicAuth = array(
            'username' => $this->apiCred['username'],
            'password' => $this->apiCred['password']
            );
            $log_data = array();
            $log_data['url'] = current_url();
            $log_data['Request_type'] = $_SERVER['REQUEST_METHOD'];
            $log_data['get'] = $this->input->get();
            $log_data['post'] = $this->post();
            $log_data['header'] = getallheaders();
            $log_data['user'] = $this->input->server('PHP_AUTH_USER');
            $log_data['ip'] = $this->input->ip_address();
            // show($log_data);
            log_data('rest/smart_card_fare_cal_api_' . date('d-m-Y') . '.log', $log_data, 'api');
            
            $insert_data = array('username' => $this->input->post('apploginuser'), 'ip' => $_SERVER['REMOTE_ADDR'], 'method' => $_SERVER['REQUEST_URI'], 'type' => 'get', 'version_id' => $this->input->post('version_id'));
        $this->requestId = $this->Mainmodel->insert_audit_data($insert_data)."";

            $request = $this->post();
            $user_id = $request['apploginuser'];
            $session_id= $this->Mainmodel->get_session_id($this->input->post('session_id'),$user_id);
           
            $this->session_id = $session_id['itms_session_id'];
            $this->user_id =itms_user_id;
            
             /*   added code disallow multiple login of same user code start here */
        $startTime = date("Y-m-d H:i:s");
        $cenvertedTime = date('Y-m-d H:i:s',strtotime(LOGIN_EXPIRE_TIME,strtotime($startTime))) ;
        $this->Mainmodel->upDateLastActivityTime($this->input->post('session_id'),$cenvertedTime);
        /*   added code disallow multiple login of same user code end here */

    }
  

  
    public function base_fare_calc_post() {
        
        $URL= $this->apiCred['url'].'/Farecalculation/base_fare_calc';
       
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
	$itms_request['session_id'] = $this->session_id ;
        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smart_card_fare_cal_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        		    $this->response($json);

        json_response($json);
        
    }
    
    public function base_fare_tal_cal_post() {
        
        $URL= $this->apiCred['url'].'/Farecalculation/base_fare_tal_cal';       
        
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
	$itms_request['session_id'] = $this->session_id ;
        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header);
        $json = json_decode($data);       
        log_data('rest/smart_card_fare_cal_api_' . date('d-m-Y') . '.log', $json, 'api');
//        $this->output
//			->set_status_header(200)
//			->set_content_type('application/json', 'utf-8')
//			->set_output(json_encode($json));
        		    $this->response($json);

        json_response($json);
        
    }
        
}

?>