<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Pass_renewal extends REST_Controller {

    public function __construct() {
        parent::__construct();
	   	$this->load->helper(array('smartcard_helper','common_helper','various_helper'));
        $this->apiCred = json_decode(SMART_CARD_CREDS, true);
        $header_value = $this->apiCred['X_API_KEY'];
        $this->header = array(
            'X-API-KEY:' . $header_value
        );

        $this->basicAuth = array(
            'username' => $this->apiCred['username'],
            'password' => $this->apiCred['password']
        );

        $this->load->model(array('Mainmodel','Customer_registration_model','Utility_model','Utilities_model'));
        $this->load->library('PassFareCalc', 'common_library');
		$request = $this->post();

		$user_id = $request['apploginuser'];
        $session_id= $this->Mainmodel->get_session_id($this->input->post('session_id'),$user_id);
        $this->session_id = $session_id['itms_session_id'];
        $this->user_id =itms_user_id;
        
        /*log code added start here */
        
        $log_data = array();
        $log_data['url'] = current_url();
        $log_data['Request_type'] = $_SERVER['REQUEST_METHOD'];
        $log_data['get'] = $this->input->get();
        $log_data['post'] = $this->post();
        $log_data['header'] = getallheaders();
        $log_data['user'] = $this->input->server('PHP_AUTH_USER');
        $log_data['ip'] = $this->input->ip_address();
            // show($log_data);
        log_data('rest/smartcard/smart_card_pass_renewal_api_' . date('d-m-Y') . '.log', $log_data, 'api');
        $insert_data = array('username' => $_POST['apploginuser'], 'ip' => $_SERVER['REMOTE_ADDR'], 'method' => $_SERVER['REQUEST_URI'], 'type' => 'get', 'version_id' => $_POST['version_id']);
        $this->requestId = $this->Mainmodel->insert_audit_data($insert_data)."";
        /*log code added end here */
        
             /*   added code disallow multiple login of same user code start here */
        $startTime = date("Y-m-d H:i:s");
        $cenvertedTime = date('Y-m-d H:i:s',strtotime(LOGIN_EXPIRE_TIME,strtotime($startTime))) ;
        $this->Mainmodel->upDateLastActivityTime($this->input->post('session_id'),$cenvertedTime);
        /*   added code disallow multiple login of same user code end here */ 
        
    }

    public function get_pass_data_desk_post() {

        $URL = $this->apiCred['url'] . '/Pass_renewal/get_pass_data_desk';
        // $request = $this->post();
        // $data = curlForPost($URL, $request, $this->basicAuth, $this->header);

        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;
        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header); 

        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_pass_renewal_api_' . date('d-m-Y') . '.log', $json, 'api');

   //      $this->output
			// ->set_status_header(200)
			// ->set_content_type('application/json', 'utf-8')
			// ->set_output(json_encode($json));
      $this->response_save($data);
        json_response($json);
    }

   public function pass_renew_desk_post(){

        $request = $this->post();
        $itms_request = $this->post();
        
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;
        
        /*check wallet balance added by sonali code start here  */
           
        $bccharges= 0 ; 
        $EPurseFees= 0 ; 
        $check_amount=$request['total_fare_amt'] ; 
        if($request['trans_type']==PASS_RENEW_SW_TRANS_TYPE || $request['trans_type']==PASS_RENEW_CASH_TRANS_TYPE )
        {
           if(!empty($request['Card_fee_details'])) 
           {
	        $Card_fee_details = json_decode($request['Card_fee_details'],TRUE);
                
                foreach ($Card_fee_details as $key => $value) {
                    
                    if(strtolower($value['fee_type_nm']) == BCCHARGES){
                      $bccharges = $value['fee_type_value'];  
                     }
                     if(strtolower($value['fee_type_nm']) == 'EPurseFees'){
                      $EPurseFees = $value['fee_type_value'];  
                     }
                    
                }
               
           }
            $check_amount=$bccharges+$EPurseFees ;
        }
        
          $request['amount']= $check_amount ;
         $result = checkWalletTxn_Smartcard($request, $comments = "check wallet balance");
         $response = json_decode($result,200);
         $responseMessage = ['responseCode' =>$response['responseCode'],"responseMessage"=>$response['msg']];
         
         if ($response) {
             $this->response_save(array('status' => $response['status'], 'msg' => $response['status'], 'data'=>$responseMessage ), 200);
         }
       /*check wallet balance added by sonali code end here */  
         
         $URL = $this->apiCred['url'] . '/Pass_renewal/pass_renew_desk';
         $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
                 
	 //print_r($data);
         $this->response_save($data);

         $json = json_decode($data);
         log_data('rest/smartcard/smart_card_pass_renewal_api_' . date('d-m-Y') . '.log', $json, 'api');
       
       
        if ((strtolower($json->status)) == 'success'  ) {
             $postVal = $this->input->post();
             $user_name = $this->input->post('apploginuser');
             $get_card_trans_data = $this->Mainmodel->get_card_trans_id($request, 'desc');

            $is_active = '1';
            $write_flag = '1';
            
            if($postVal['trans_type'] == PASS_RENEW_CASH_TRANS_TYPE || $postVal['trans_type'] == PASS_RENEW_SW_TRANS_TYPE) {
                /* Check if commission available code start- sonali*/           
                
                $trimax_card_id = $this->input->post('trimax_card_id');
                $trans_type=$this->input->post('trans_type') ;
                
                $getPassRenewalId = $this->Mainmodel->getPassRenewalId($trimax_card_id,$trans_type,$itms_request['passRenewalRequestId'],RENEWAL_STATUS);
                
                $d1 = $getPassRenewalId['created_date'];
                $d2 = date("Y-m-d H:i:s");
                $tmdiff = calculate_date_diff($d1, $d2);
                $new_time_to_show = calculate_travel_duration($tmdiff);
                 
                $hours = str_replace(' ','',$new_time_to_show);
                
                $input['apploginuser'] = $request['apploginuser'];
                $check_level = $this->Utilities_model->get_commission_level();
                $commissionResult = $this->Utilities_model->commission_setby_level($request['apploginuser'],$check_level->level);
                 //show($commissionResult,1);
                $created_by = $commissionResult[0]['level'];
                $input['created_by'] = $created_by;      
                
                
                if($postVal['trans_type'] == PASS_RENEW_SW_TRANS_TYPE){	
                    if ($hours > '03:00Hrs') {
                        $input['card_type'] = 'SmartcardSmartcard';
                        $check_if_commission_available = $this->checkCommission($input);

                        log_data('rest/smartcard/smart_card_ccas_api_check_commssion_available_' . date('d-m-Y') . '.log', $check_if_commission_available, 'check_commssion_available');
                        //show($check_if_commission_available);
                        
                        if (count($check_if_commission_available) == 0) {            
                            $this->response_save(array('status' => 'Failed', 'msg' => 'Commission not set. Please contact your administrator.', 'data'=>$check_if_commission_available ), 200);
                        }
                    }
                }  else {
                        $input['card_type'] = 'SmartcardSmartcard';
                        $check_if_commission_available = $this->checkCommission($input);

                        log_data('rest/smartcard/smart_card_ccas_api_check_commssion_available_' . date('d-m-Y') . '.log', $check_if_commission_available, 'check_commssion_available');
                        //show($check_if_commission_available);
                        if (count($check_if_commission_available) == 0) {            
                            $this->response_save(array('status' => 'Failed', 'msg' => 'Commission not set. Please contact your administrator.', 'data'=>$check_if_commission_available ), 200);
                        }
                }
            /* Check if commission available code end - sonali*/
            }
            
            if($postVal['trans_type'] == PASS_RENEW_SW_TRANS_TYPE || (isset($postVal) && $postVal['request_from'] == REQUEST_FROM_WEB && $postVal['trans_type'] == PASS_RENEW_CASH_TRANS_TYPE)) {
                $is_active = RENEWAL_STATUS;
                $write_flag = '0';
            }
             
            $request['pass_concession_id'] = $get_card_trans_data['pass_concession_id'];
            $request['pass_category_id'] = $get_card_trans_data['pass_category_id'];
            $request['display_card_id'] = $request['trimax_card_id'];

            $curr_pass_exp_date = strtotime($get_card_trans_data['expiry_date']);

            //$span_days = $this->Mainmodel->get_span_period($request);
           // $total_days = $span_days['span_period_days'] - 1;

            $activation_date = date('Y-m-d', strtotime($get_card_trans_data['expiry_date'] . '+1 days'));
            if (strtotime(TODAY_DATE) > $curr_pass_exp_date) {
                $activation_date = TODAY_DATE;
            } elseif (isset($request['activation_date']) && !empty($request['activation_date'])) {
                $activation_date = $request['activation_date'];
            }
          //  $expiry_date = date('Y-m-d', strtotime($activation_date . "+" . $total_days . " days"));
           // $card_fee_master_id['ctrans_type_id'] = 23;
            //$card_fee_master_id['id'] = 2;

            $vendor_id = $this->Customer_registration_model->get_vendor_id($request['apploginuser'])['vendor_id'];
            
            $receipt_number = $json->data->receipt_number; 


            $total_base_fare = isset($request['base_fare_1']) ? $request['base_fare_1'] : '';
            $bf = 1;
            if(isset($request['base_fare_2'])) {
                $bf = 2;
                $total_base_fare = $request['base_fare_1'] + $request['base_fare_2'];
            }

            $card_trans_data = array(
                    'trimax_card_id' => $request['trimax_card_id'],
                    'pass_type_id' => '1',
                    'pass_concession_id' => $request['pass_concession_id'],
                    'pass_category_id' => $request['pass_category_id'],
                    'activation_date' => $activation_date,
                    'expiry_date' => isset($request['expiry_date']) ? $request['expiry_date'] : '', 
                  //   'transaction_type_id' => $card_fee_master_id['ctrans_type_id'],
                    'write_flag' => $write_flag,
                    'transaction_type_code' => isset($request['trans_type']) ? $request['trans_type'] : '', 
                    'depot_cd' => $request['depot_cd'],
                    'terminal_id' => $request['terminal_id'],
                    'receipt_number' => $receipt_number,
		     'request_id' => $json->data->passRenewalRequestId,
                    'pass_renewal_request_id' => $json->data->passRenewalRequestId,
                  //  'span_id' => isset($request['span_id']) ? $request['span_id'] : '',
                    'span_name' => isset($request['span_name']) ? $request['span_name'] : '',
                    'bus_type_cd' => isset($request['bus_type_cd']) ? $request['bus_type_cd'] : '',
                   // 'conc_service_rel_id' => isset($conc_service_rel_id) ? $conc_service_rel_id : '',
                   // 'card_fee_master_id' => $card_fee_master_id['id'],
                    'amount' => $total_base_fare,
                    'session_id' => $request['session_id'],
                    'vendor_id' => $vendor_id,
                    'created_by' => $request['apploginuser'],
                    'is_active' => $is_active
                );
            $card_trans_id = $this->Customer_registration_model->insert_customer_reg_data($card_trans_data,CARD_TRANS);


            //Update card_transaction write flag as 0 and current to 1
            if($write_flag == '1') {
              $update_data = array('write_flag' => '0', 'modified_by' => $user_name);
              $this->Mainmodel->update_card_tarns_write_flag($card_trans_id,$request['trimax_card_id'],$update_data);
            }
             //card transaction details start here

            $card_trans_details_id = $fare_data_details = $fare_data_details = $fare_data = array();

            for ($i=1; $i <= $bf; $i++) {
                $card_trans_detail_data = array(
                    'card_transaction_id' => $card_trans_id,
                    'trimax_card_id' => $request['trimax_card_id'],
                   // 'card_fee_master_id' => $card_fee_master_id['id'],
                   // 'transaction_type_code' => isset($request['trans_type']) ? $request['trans_type'] : '', 
                    'route_no' => isset($request['route_no_'.$i]) ? $request['route_no_'.$i] : '',
                    'route_type' => isset($request['route_type_'.$i]) ? $request['route_type_'.$i] : '',
                    'fare_type_cd' => isset($request['fare_type_cd_'.$i]) ? $request['fare_type_cd_'.$i] : '',
                    'service_number' => '',
                    'from_stop_code' => isset($request['from_stop_code_'.$i]) ? $request['from_stop_code_'.$i] : '',
                    'till_stop_code' => isset($request['till_stop_code_'.$i]) ? $request['till_stop_code_'.$i] : '',
                    'via_stop_code' => isset($request['via_stop_cd_'.$i]) ? $request['via_stop_cd_'.$i] : '',
                    'amount' => isset($request['base_fare_'.$i]) ? $request['base_fare_'.$i] : '',
                    'session_id' => $request['session_id'],
                    'created_by' => $request['apploginuser'],
                    'is_active' => $is_active
                );

       
			if(isset($request['base_fare_'.$i]) && !empty($request['base_fare_'.$i]) && $request['base_fare_'.$i] > 0 && !in_array($request['pass_category_id'], array(OTC_CARD,TRAVEL_AS_U_LIKE_CARD))) {

                    $fare_data_details = isset($request['fare_data_'.$i]) ? $request['fare_data_'.$i] : '';

                    $fare_data_details_arr = json_decode($fare_data_details,TRUE);
                    $per_day_base_fare = $fare_data_details_arr['per_day_base_fare'];
                    $card_trans_detail_data['per_day_base_fare'] = $per_day_base_fare;
                    $card_trans_detail_data['total_base_fare'] = $fare_data_details_arr['total_base_fare'];
                    $card_trans_detail_data['total_asn_amount'] = $fare_data_details_arr['total_asn_amount'];
                    $card_trans_detail_data['net_fare_amount'] = $fare_data_details_arr['net_fare_amount'];
                    $card_trans_detail_data['fare_before_rounding'] = $fare_data_details_arr['fare_before_rounding'];
                    $card_trans_detail_data['fare_rounding_difference'] = $fare_data_details_arr['fare_before_rounding'] - $fare_data_details_arr['total_amt'];
                    $card_trans_detail_data['reimbursement_amount'] = $fare_data_details_arr['reimbursement_amount'];
                }

                $card_trans_details_id[$i] = $this->Customer_registration_model->insert_customer_reg_data($card_trans_detail_data,CARD_TRANS_DETAILS);

            }
            
            // calculation of total fare and its details
            // Need uncomment 
         //   $total_fare_data['data']['total_fare_amt'] = 0;
           
           $total_fare_amt = $request['total_fare_amt'];
            //amount transaction start here
            $amt_trans_data = array(
                    'card_transaction_id' => $card_trans_id,
                    'trimax_card_id' => $request['trimax_card_id'],
                    'total_amount' => $total_fare_amt,
                  //  'transaction_type_id' => $card_fee_master_id['ctrans_type_id'],
                    'transaction_type_code' => isset($request['trans_type']) ? $request['trans_type'] : '', 
                    'depot_cd' => $request['depot_cd'],
                    'terminal_id' => $request['terminal_id'],
                    'session_id' => $request['session_id'],
                    'vendor_id' => $vendor_id,
                    'created_by' => $request['apploginuser'],
                    'is_active' => $is_active
                );
            $amt_trans_id = $this->Customer_registration_model->insert_customer_reg_data($amt_trans_data,AMOUNT_TRANS);
          
           if(!empty($request['Card_fee_details'])) 
           {
		       $Card_fee_details = json_decode($request['Card_fee_details'],TRUE);
                foreach ($Card_fee_details as $key => $value) {
                    $amt_trans_details_data[] = array(
                            'amount_transaction_id' => $amt_trans_id,
                            'amount_code' => $value['fee_type_cd'],
                            'amount' => $value['fee_type_value'],
                            'operand' => $value['operand'],
                            'created_by' => $request['apploginuser'],
                            'is_active' => $is_active
                        );
                }
                $amt_trans_details_id = $this->Customer_registration_model->insert_customer_reg_bulk_data($amt_trans_details_data,AMOUNT_TRANS_DETAILS);
           }
           
             if($is_active == '1') {
                //$this->Utility_model->save_command_data($request['trimax_card_id'],RENEW_COMMAND);
                $update_req_txn['request_id'] = $json->data->passRenewalRequestId;
                $whereData['id'] = $card_trans_id;
                $whereData['is_active'] = '1';
                //$this->Mainmodel->updateReqTxnData($update_req_txn,$whereData);
            }
           //array('trimax_card_id' => $request['trimax_card_id'], 'receipt_number' => $receipt_number);
            
            // Daubt in below line.
          // $request['amount'] = $request['total_fare_amt']-$bccharges;
           //updateWalletTxn($request);
           
            if($request['trans_type']==PASS_RENEW_SW_TRANS_TYPE || $request['trans_type']==PASS_RENEW_CASH_TRANS_TYPE)
            {
				 
                if($bccharges!='0'  || $EPurseFees!='0'  || $request['trans_type']==PASS_RENEW_CASH_TRANS_TYPE)
                {
                    $URL = $this->apiCred['url'] . '/CCAS_api/bcPoolDebit';
					$itms_request['trimaxId'] =$itms_request['trimax_card_id'] ;
	             $itms_request['mode_type'] = ($request['trans_type']==PASS_RENEW_SW_TRANS_TYPE) ? 'SW' : 'CASH';
                    $itms_request['passRenewalRequestId'] =$json->data->passRenewalRequestId  ;
                    $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
					
                    $json_bcPoolDebit = json_decode($data);
                    log_data('rest/smartcard/smart_card_pass_renewal_api_' . date('d-m-Y') . '.log', $json_bcPoolDebit, 'api');
                    
                if(!empty($json_bcPoolDebit->status) && strtolower($json_bcPoolDebit->status) == 'success' )
		{ 
					
		    $json_bcPoolDebit->data->bcpool_RequestId =$json_bcPoolDebit->data->requestId  ;
		    unset($json_bcPoolDebit->data->requestId);
		    $json_bcPoolDebit->data->receipt_number = $receipt_number ;
                    $json_bcPoolDebit->data->trimax_card_id = $itms_request['trimaxId'] ;
                    $json_bcPoolDebit->data->passRenewalRequestId  = $json->data->passRenewalRequestId ;
					
		   if ($request['trans_type']==PASS_RENEW_CASH_TRANS_TYPE) 
                     {
                        $itms_request['passRenewalRequestId'] =$json->data->passRenewalRequestId  ;
                        $itms_request['txnRefNo'] =$json_bcPoolDebit->data->txnRefNo  ;
	                 $itms_request['requestId'] =$json_bcPoolDebit->data->bcpool_RequestId  ;
                        $this->update_pass_renewal_status($itms_request);
                     }   
				   
		if($request['trans_type']==PASS_RENEW_CASH_TRANS_TYPE)
                    {
                      $request['amount'] =$request['total_fare_amt'];  
                    }else{
                      $request['amount'] =($bccharges+$EPurseFees);
                     }    
                     
                    //start commission calculation code - by sonali  
                    $input['service'] = 'smartcard';
                    $input['apploginuser'] = $request['apploginuser'];                    
                    $input['created_by'] = $created_by; 
                    $wallet_trans_no = substr(hexdec(uniqid()), 4, 12);
                    $input['card_type'] = SMARTCARD_COMMISSION_NAME;
                    $input['recharge_amount'] = $request['amount'];
                    
                    if($postVal['trans_type'] == PASS_RENEW_SW_TRANS_TYPE){	
                        if ($hours > '03:00Hrs') {
                            $commissionResult = $this->serviceRetailerCommissionDistributionForRecharges($input, $wallet_trans_no,$created_by);
                            log_data('rest/smartcard/smart_card_ccas_api_check_commssion_result_' . date('d-m-Y') . '.log', $commissionResult, 'Commission result');

                        }
                    }else{
                            $commissionResult = $this->serviceRetailerCommissionDistributionForRecharges($input, $wallet_trans_no,$created_by);
                            log_data('rest/smartcard/smart_card_ccas_api_check_commssion_result_' . date('d-m-Y') . '.log', $commissionResult, 'Commission result');

                    }
                     
                    //end commission calculation code - by sonali
                     $wallet_txn_Arr= updateWalletTxn($request);
                      if(!empty($card_trans_id))
                      {
                        $updateWalletTransNo = ['wallet_trans_no' => $wallet_txn_Arr['transaction_no']];                         
                        $this->db->where('id',$card_trans_id);
			$this->db->update("ps_card_transaction", $updateWalletTransNo);
                      }  
                   
                    json_response($json_bcPoolDebit);

                   }else if(!empty($json_bcPoolDebit->status) ){
                       $json_bcPoolDebit->data->passRenewalRequestId = $json->data->passRenewalRequestId  ;
                      
                       json_response($json_bcPoolDebit);
                   }else if(empty($json_bcPoolDebit) )
                   {
                       $responseData['responseCode'] = RESPONSE_CODE_TIMEOUT_CC;
                        $responseData['responseMessage'] = "Connection Timed Out. Please perform Transaction Enquiry to confirm status.";                      $responseData['passRenewalRequestId'] = $json->data->passRenewalRequestId  ;
                        $message = array('status' => 'failure', 'msg' => 'failure', "data" => $responseData);
                        $json = json_encode($message);
                        $json = json_decode($json); 
                      
                        json_response($json);
                    }     

                }else{
				
		   if($request['trans_type']==PASS_RENEW_CASH_TRANS_TYPE) 
                     {
                        $itms_request['passRenewalRequestId'] =$json->data->passRenewalRequestId  ;
                       //$itms_request['txn_ref_no'] =$json->data->request_id  ;
                        $this->update_pass_renewal_status($itms_request);
                     } 
                  
	           json_response($json);   
		 }

            }else{
		
               json_response($json);   
            } 
           
        }else
        { 
           json_response($json);    
        }
        
		//sleep(120);
    } 

    
     public function update_pass_renewal_status_post()  
     {
        $URL = $this->apiCred['url'] . '/Pass_renewal/update_pass_renewal_status';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;
        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header); 
        $json = json_decode($data);
		
		//print_R($data); 
        log_data('rest/smartcard/smart_card_pass_renewal_api_' . date('d-m-Y') . '.log', $json, 'api');
      
        
           if (strtolower($json->status) == 'success' ) 
           {
                $user_name = $this->input->post('apploginuser');
                $trimax_card_id = $this->input->post('trimax_card_id');

                $trans_type=$this->input->post('trans_type') ;
               $chk_renewal_data = $this->Mainmodel->checkPassRenewalData($trimax_card_id,$trans_type,$itms_request['passRenewalRequestId'],RENEWAL_STATUS);
               $card_trans_id = $chk_renewal_data['card_trans_id'];
               $card_trans_details_id = $chk_renewal_data['card_trans_details_id'];
               $amt_trans_id = $chk_renewal_data['amt_trans_id'];
               $trans_update_data = array('write_flag' => '0', 'modified_by' => $user_name);
               
               $this->Mainmodel->update_card_tarns_write_flag($card_trans_id,$trimax_card_id,$trans_update_data);
               
               $trans_update_data['write_flag'] = '1';
               
               $this->Mainmodel->update_card_trans($card_trans_id,$trans_update_data);
               
               $update_data = array('is_active' => '1', 'updated_by' => $user_name);
              $master_id_arr = array('card_trans_id' => $card_trans_id, 'card_trans_details_id_1' => $card_trans_details_id, 'amt_trans_id' => $amt_trans_id);
              
              
              $this->Mainmodel->updateActiveStatus($master_id_arr,$update_data);
              
              
              $card_update_data = array('is_active' => '1', 'modified_by' => $user_name);
              
              $this->Mainmodel->update_card_trans_detail($card_trans_id,$trimax_card_id,$card_update_data);
              
              //$update_req_txn['pos_request_id'] = $this->input->post('pos_request_id');
              //$update_req_txn['txn_ref_no'] = $this->input->post('txn_ref_no');
              
               $update_req_txn = array();
               
               $postVal = $this->post();
               
               if(isset($postVal['requestId']) && !empty($postVal['requestId'])) {
                    $update_req_txn['request_id'] = $postVal['requestId'];
                }

                if(isset($postVal['txnRefNo']) && !empty($postVal['txnRefNo'])) {
                    $update_req_txn['txn_ref_no'] = $postVal['txnRefNo'];
                }

                if(isset($postVal['posRequestId']) && !empty($postVal['posRequestId'])) {
                    $update_req_txn['pos_request_id'] = $postVal['posRequestId'];
                }

                if(isset($postVal['posTxnRefNo']) && !empty($postVal['posTxnRefNo'])) {
                    $update_req_txn['pos_txn_ref_no'] = $postVal['posTxnRefNo'];
                }

              $whereData['is_active'] = '1';
              $whereData['id'] =$card_trans_id;
              
              $this->Mainmodel->updateReqTxnData($update_req_txn,$whereData);
           } 
           json_response($data);
           json_response($json);
    }
	
	// call from pass_renew_desk if it is case of cash
    public function update_pass_renewal_status($_post) 
     {
            $URL = $this->apiCred['url'] . '/Pass_renewal/update_pass_renewal_status';
            $itms_request = $_post;
            $itms_request['apploginuser'] = $this->user_id ;
            $itms_request['session_id'] = $this->session_id ;
            $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header); 
            $json = json_decode($data);
	    log_data('rest/smartcard/smart_card_pass_renewal_api_' . date('d-m-Y') . '.log', $data, 'api');
	    log_data('rest/smartcard/smart_card_pass_renewal_api_' . date('d-m-Y') . '.log', $json, 'api');
            if (strtolower($json->status) == 'success') 
            {
                $user_name = $_post['apploginuser'];
                $trimax_card_id =$_post['trimax_card_id']; //$this->input->post('trimax_card_id');
                $trans_type=$_post['trans_type']; // $this->input->post('trans_type') ;
               
               $chk_renewal_data = $this->Mainmodel->checkPassRenewalData($trimax_card_id,$trans_type,$itms_request['passRenewalRequestId'],RENEWAL_STATUS);

               $card_trans_id = $chk_renewal_data['card_trans_id'];
               $card_trans_details_id = $chk_renewal_data['card_trans_details_id'];
               $amt_trans_id = $chk_renewal_data['amt_trans_id'];
               $trans_update_data = array('write_flag' => '0', 'modified_by' => $user_name);
               
               $this->Mainmodel->update_card_tarns_write_flag($card_trans_id,$trimax_card_id,$trans_update_data);
               
               $trans_update_data['write_flag'] = '1';
               
               $this->Mainmodel->update_card_trans($card_trans_id,$trans_update_data);
               
               $update_data = array('is_active' => '1', 'updated_by' => $user_name);
              $master_id_arr = array('card_trans_id' => $card_trans_id, 'card_trans_details_id_1' => $card_trans_details_id, 'amt_trans_id' => $amt_trans_id);
              
              $this->Mainmodel->updateActiveStatus($master_id_arr,$update_data);
              $card_update_data = array('is_active' => '1', 'modified_by' => $user_name);
              $this->Mainmodel->update_card_trans_detail($card_trans_id,$trimax_card_id,$card_update_data);
              $update_req_txn = array();
              /*new code added start here  by rashid */
                $postVal =  $_post ;
                if(isset($postVal['requestId']) && !empty($postVal['requestId'])) {
                    $update_req_txn['request_id'] = $postVal['requestId'];
                }

                if(isset($postVal['txnRefNo']) && !empty($postVal['txnRefNo'])) {
                    $update_req_txn['txn_ref_no'] = $postVal['txnRefNo'];
                }

                if(isset($postVal['posRequestId']) && !empty($postVal['posRequestId'])) {
                    $update_req_txn['pos_request_id'] = $postVal['posRequestId'];
                }

                if(isset($postVal['posTxnRefNo']) && !empty($postVal['posTxnRefNo'])) {
                    $update_req_txn['pos_txn_ref_no'] = $postVal['posTxnRefNo'];
                }

                if(!empty($update_req_txn)) {
                    $whereData['is_active'] = '1';
                    $whereData['id'] = $card_trans_id;
                    $this->Mainmodel->updateReqTxnData($update_req_txn,$whereData);
                }
          /*new code added end here  by rashid */
     
         }
		    
    }
    
    public function checkCommission($input) {
        if ($input['card_type'] == SMARTCARD_COMMISSION_NAME) {            
            $code = strtolower($input['card_type']);
            $service_id = SMART_CARD_SERVICE;
}

        $LcSqlStr = "SELECT * from services_commissions 
        where status = 'Y'";
        $LcSqlStr .= " AND `values` NOT LIKE '{\"Trimax\":\"0\",\"Rokad\":\"0\",\"MD\":\"0\",\"AD\":\"0\",\"Distributor\":\"0\",\"Retailer\":\"0\"}'";
        $LcSqlStr .= " AND `commission_name` LIKE '%" . str_replace(" ", "", $code) . "%'";
        $LcSqlStr .= " AND `created_by` = '" . $input['created_by'] . "'";
        $LcSqlStr .= " AND service_id = '" . $service_id . "'";
        ;
        //log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/micro_atm/' . 'micro_atm_commission_query.log', $LcSqlStr, 'Commission query');
        log_data('rest/smartcard/smart_card_ccas_api_commission_query_' . date('d-m-Y') . '.log', $LcSqlStr, 'Commission query');
        $query = $this->db->query($LcSqlStr);
        return $query->result();
    }
    
    public function serviceRetailerCommissionDistributionForRecharges($input = "", $trans_ref_no = "",$created_by =""){

        $user_id = $input['apploginuser'];
        if(in_array($input['service'], ['smartcard']) ){
            if ($input['card_type'] == SMARTCARD_COMMISSION_NAME) {            
                $code = strtolower($input['card_type']);
                $service_id = SMART_CARD_SERVICE;
            }

            $LcSqlStr = "SELECT * from services_commissions 
            where status = 'Y'";
            $LcSqlStr .= " AND `values` NOT LIKE '{\"Trimax\":\"0\",\"Rokad\":\"0\",\"MD\":\"0\",\"AD\":\"0\",\"Distributor\":\"0\",\"Retailer\":\"0\"}'";
            $LcSqlStr .= " AND `commission_name` LIKE '%" . str_replace(" ", "", $code) . "%'";
            $LcSqlStr .= " AND service_id = '" . $service_id . "'"; 
            ;

            // show($LcSqlStr, 1);

            $query = $this->db->query($LcSqlStr);
            $new = $query->result();
            if($new){
                $row = $new[0];
                $getRetailerServicesCommission = $this->Utilities_model->getRetailerServicesCommissionForRecharges($user_id, $row->service_id, $row->sub_service_id,$created_by);
                //show($getRetailerServicesCommission,1);
            }
        }else{
            $service_id = SMART_CARD_SERVICE;
            $getRetailerServicesCommission = $this->Utilities_model->getRetailerServicesCommission($user_id, $service_id,$created_by);
        }

        if ($getRetailerServicesCommission) {
            
            $agent_id = $getRetailerServicesCommission[0]['user_id'];
            $agent_code = $getRetailerServicesCommission[0]['agent_code'];
            $distributor_id = $getRetailerServicesCommission[0]['level_5'];
            $Area_dis_id = $getRetailerServicesCommission[0]['level_4'];
            $master_dis_id = $getRetailerServicesCommission[0]['level_3'];
            $company_id = $getRetailerServicesCommission[0]['level_2'];
            $trimax_id = $getRetailerServicesCommission[0]['level_1'];
            $service_id = $getRetailerServicesCommission[0]['service_id'];
            $commission_id = $getRetailerServicesCommission[0]['commission_id'];
            $total_commission = $getRetailerServicesCommission[0]['total_commission'];

            $values[0] = json_decode($getRetailerServicesCommission[0]['values']);
//            show($values[0],1);
//            die('hhj');
            $trimax_commission = '0';
            $company_commission = '0';
            $md_commission = '0';
            $ad_commission = '0';
            $dist_commission = '0';
            $retailer_commission = '0';
            if (!empty($values)) {

                $trimax_commission = isset($values[0]->Trimax) ? round($values[0]->Trimax, 2) : '0.00';
                $company_commission = isset($values[0]->Rokad) ? round($values[0]->Rokad, 2) : '0.00';
                $md_commission = isset($values[0]->MD) ? round($values[0]->MD, 2) : '0.00';
                $ad_commission = isset($values[0]->AD) ? round($values[0]->AD, 2) : '0.00';
                $dist_commission = isset($values[0]->Distributor) ? round($values[0]->Distributor, 2) : '0.00';
                $retailer_commission = isset($values[0]->Retailer) ? round($values[0]->Retailer, 2) : '0.00';

                $recharge_amount = $input['recharge_amount']; //transaction_amt
                                   
                
                $commission_percent = !empty($total_commission) ? $total_commission : '0';
              
                $trimax_cal = calculateGstTds($recharge_amount, $commission_percent);
                
                $earnings = $trimax_cal['earnings']; //trimax_earnings
                $gst = $trimax_cal['gst']; //trimax_gst                            
                $tds = $trimax_cal['tds']; //trimax_tds
                $final_amount = $trimax_cal['final_amount']; //trimax_final_amt                                                        

                $rokad_trimax_cal = calculateGstTds($earnings, $trimax_commission);
                $rokad_trimax_amt = $rokad_trimax_cal['earnings']; //rokad_trimax_amt                             
                $rokad_trimax_cal_with_tds_gst = calculateGstTds($final_amount, $trimax_commission);
                $rokad_trimax_amt_with_tds_gst = $rokad_trimax_cal_with_tds_gst['earnings']; //rokad_trimax_amt_with_tds_gst                            

                $company_cal = calculateGstTds($earnings, $company_commission);
                $company_amt = $company_cal['earnings']; //company_amt                             
                $company_cal_with_tds_gst = calculateGstTds($final_amount, $company_commission);
                $company_amt_with_tds_gst = $company_cal_with_tds_gst['earnings']; //company_amt_with_tds_gst                        

                $rd_cal = calculateGstTds($earnings, $md_commission);
                $rd_amt = $rd_cal['earnings']; //rd_amt                             
                $rd_cal_with_tds_gst = calculateGstTds($final_amount, $md_commission);
                $rd_amt_with_tds_gst = $rd_cal_with_tds_gst['earnings']; //rd_amt_with_tds_gst

                $dd_cal = calculateGstTds($earnings, $ad_commission);
                $dd_amt = $dd_cal['earnings']; //dd_amt                             
                $dd_cal_with_tds_gst = calculateGstTds($final_amount, $ad_commission);
                $dd_amt_with_tds_gst = $dd_cal_with_tds_gst['earnings']; //dd_amt_with_tds_gst

                $ex_cal = calculateGstTds($earnings, $dist_commission);
                $ex_amt = $ex_cal['earnings']; //ex_amt                             
                $ex_cal_with_tds_gst = calculateGstTds($final_amount, $dist_commission);
                $ex_amt_with_tds_gst = $ex_cal_with_tds_gst['earnings']; //ex_amt_with_tds_gst

                $sa_cal = calculateGstTds($earnings, $retailer_commission);
                $sa_amt = $sa_cal['earnings']; //sa_amt                             
                $sa_cal_with_tds_gst = calculateGstTds($final_amount, $retailer_commission);
                $sa_amt_with_tds_gst = $sa_cal_with_tds_gst['earnings']; //sa_amt_with_tds_gst              
            }

            $comm_distribution_array['user_id'] = $agent_id;
            $comm_distribution_array['service_id'] = $service_id;
            $comm_distribution_array['commission_id'] = $commission_id;
            $comm_distribution_array['transaction_no'] = $trans_ref_no;
            $comm_distribution_array['transaction_amt'] = $recharge_amount;
            $comm_distribution_array['service_comm_percent'] = SERVICE_COMMISSION;
            $comm_distribution_array['trimax_comm_percent'] = $total_commission;
            $comm_distribution_array['gst_percentage'] = GST_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['tds_percentage'] = TDS_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['trimax_earnings'] = $earnings;
            $comm_distribution_array['trimax_gst'] = $gst;
            $comm_distribution_array['trimax_tds'] = $tds;
            $comm_distribution_array['trimax_final_amt'] = $final_amount;
            $comm_distribution_array['rokad_trimax_per'] = $trimax_commission;
            $comm_distribution_array['rokad_trimax_amt'] = $rokad_trimax_amt;
            $comm_distribution_array['rokad_trimax_amt_with_tds_gst'] = $rokad_trimax_amt_with_tds_gst;
            $comm_distribution_array['company_per'] = $company_commission;
            $comm_distribution_array['company_amt'] = $company_amt;
            $comm_distribution_array['company_amt_with_tds_gst'] = $company_amt_with_tds_gst;
            $comm_distribution_array['rd_per'] = $md_commission;
            $comm_distribution_array['rd_amt'] = $rd_amt;
            $comm_distribution_array['rd_amt_with_tds_gst'] = $rd_amt_with_tds_gst;
            $comm_distribution_array['dd_per'] = $ad_commission;
            $comm_distribution_array['dd_amt'] = $dd_amt;
            $comm_distribution_array['dd_amt_with_tds_gst'] = $dd_amt_with_tds_gst;
            $comm_distribution_array['ex_per'] = $dist_commission;
            $comm_distribution_array['ex_amt'] = $ex_amt;
            $comm_distribution_array['ex_amt_with_tds_gst'] = $ex_amt_with_tds_gst;
            $comm_distribution_array['sa_per'] = $retailer_commission;
            $comm_distribution_array['sa_amt'] = $sa_amt;
            $comm_distribution_array['sa_amt_with_tds_gst'] = $sa_amt_with_tds_gst;
            $comm_distribution_array['status'] = 'Y';
            $comm_distribution_array['created_by'] = $user_id;
            $comm_distribution_array['created_date'] = date('Y-m-d H:i:s');

            $id = $this->Utilities_model->saveVasCommissionData($comm_distribution_array);
            //show($id,1);
            // $comm_distribution_array['id'] = $id;
           
            log_data('rest/smartcard/smart_card_ccas_api_commission_distribution_response_' . date('d-m-Y') . '.log', $comm_distribution_array, 'Commission Distribution Response');
           // return $comm_distribution_array; // edited by sonali on 6june19
            
            
            //smartcard commission distribution           
            $total_commission_const = round(SMARTCARD_TOTAL_COMMISSION);
            
            $fino_cal = calculateGstTds($total_commission_const, FINO_COMMISSION);
            $fino_amt = $fino_cal['earnings']; //fino_amt  
            
            $remaining_cal = calculateGstTds($total_commission_const, REMAINING_COMMISSION);
            $remaining_amt = $remaining_cal['earnings']; //fino_amt 
            
            $less_marketing_cost_cal = calculateGstTds($remaining_amt, LESS_MARKETING_COST);
            $less_marketing_cost_amt = $less_marketing_cost_cal['earnings']; //fino_amt 
            
            $city_cash_amt = CITY_CASH;
            
            $effective_commission_for_bos = EFFECTIVE_COMMISSION_FOR_BOS;
            
            
            $comm_distribution_amt['fino'] = $fino_amt;  
            $comm_distribution_amt['less_marketing_cost'] = $less_marketing_cost_amt;
            $comm_distribution_amt['city_cash'] = $city_cash_amt;
            $comm_distribution_amt['effective_commission_for_bos'] = $effective_commission_for_bos;
            $comm_distribution_amt['status'] = 'Y';
            $comm_distribution_amt['created_date'] = date('Y-m-d H:i:s');
            
            $this->Utilities_model->saveSmartcardCommissionData($comm_distribution_amt);
            
            log_data('rest/smartcard/smart_card_ccas_api_smartcard_commission_distribution_response_' . date('d-m-Y') . '.log', $comm_distribution_amt, 'Smartcard_Commission Distribution Response');
           
            
        }
        
       
    }
    
     
}
