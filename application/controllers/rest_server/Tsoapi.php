<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Tsoapi extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('tso_model', 'wallet_trans_model', 'Utilities_model'));
        $this->load->helper('tso_helper');
        $this->load->library('Reconcilation_api');
    }

    public function recharge_post() {
        $data = array();
        $input = $this->post();
//        echo 'rec'; 
//        show($input, 1);
                            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'TSO_recharge_input.log', $input, 'TSO Input');
                

//                         $input = array(
//     ['recharge_from'] => 'web',
//     ['plan_type'] => 'Prepaid Mobile',
//     ['type'] => 'TSO',
//     ['service'] => 'TSO',
//     ['pre_operator'] => 'A'
//     ['mobile_number'] => '7798727137',
//     ['recharge_type'] => 0,
//     ['recharge_amount'] => 10,
//     ['payment_type'] => cash,
//     ['submit_recharge'] => '',
//     ['user_id'] => 338,
//     ['rec_type'] => 'Prepaid Mobile',
//     ['mobile_operator'] => 'AIRTEL'
// );    
        if (!empty($input)) {

            $config = array(
                array('field' => 'plan_type', 'label' => 'Recharge Plan Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select recharge plan type.')),
                array('field' => ($input['plan_type'] == 'Prepaid Mobile') ? 'pre_operator' : 'post_operator', 'label' => 'Mobile Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select mobile operator.')),
                array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.')),
                array('field' => 'recharge_amount', 'label' => 'Recharge Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter recharge amount.')),
                array('field' => ($input['plan_type'] == 'Prepaid Mobile') ? 'recharge_type' : '', 'label' => 'Recharge Type', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please select recharge type.'))
            );
            if (form_validate_rules($config) == FALSE) {
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {
                /* Check TSO Wallet Funds */
                $balance = $this->getTSOBal();
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'TSO_recharge_balance.log', $balance, 'TSO Balance - API response');
                

                if ($balance['STATUSCODE'] != 0) {
                    $this->response(array('status' => 'Failed', 'data' => [], 'msg' => 'Insufficient Wallet balance', 'api_message' => $balance['STATUSMSG']), 200);
                }
                
                
                $wallet_trans_no = substr(hexdec(uniqid()), 4, 12);
                $user_id = $input['user_id'];
                $recharge_from = $input['recharge_from'];
                $operator = $input['plan_type'] == 'Prepaid Mobile' ? $input['pre_operator'] : $input['post_operator'];
                $plan_type = $input['plan_type'];
                $recharge_type = $recharge_type = $input['recharge_type'] == 0 ? 'TOPUP' : 'SCHEME';
                $mobile_number = $input['mobile_number'];
                $recharge_amount = $input['recharge_amount'];
                $comment = $input['comment'];

                /* Get data from wallet table */
                $check_user_wallet = $this->tso_model->getUserWalletDetails($user_id);
                //show($check_user_wallet,1);
               // log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'TSO_recharge_wallet.log', $check_user_wallet, 'TSO Balance - API response');
                
                if ($check_user_wallet[0]['amt'] >= $recharge_amount) {

                    /* Check if commission available - Gopal*/
                    $check_level = $this->Utilities_model->get_commission_level();
                    $commissionResult = $this->Utilities_model->commission_setby_level($input['user_id'],$check_level->level);
                    $created_by = $commissionResult[0]['level'];
                    
//                     $commissionResult = $this->Utilities_model->commission_from($input['user_id']);
//                    $created_by = $commissionResult[0]['level_3'];
                    // $check_if_commission_available = $this->checkCommission($input,$created_by);
                    // if(count($check_if_commission_available) == 0){
                    //     $this->response(array('status' => 'Failed', 'data' => $check_if_commission_available, 'msg' => 'Commission not set. Please contact your administrator.'), 200);
                    // }
                    /* Check if commission available - Gopal*/

                    /* Call To TSO API */
                    $recharge_from_TSO = recharge_with_TSO($input);

                   //  show($recharge_from_TSO,1);
                   //   log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'TSO_recharge_wallet.log', $check_user_wallet, 'TSO Balance - API response');
              
                    $recharge_from_TSO = $recharge_from_TSO['NTDRESP'];
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'TSO_recharge_final.log', $recharge_from_TSO, 'TSO Recharge - API response');
                    $check_errors = $this->checkErrorsInTSOResponse($recharge_from_TSO, 'M');
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'TSO_recharge.log', $check_errors, 'TSO Recharge - TSO Errors Check');
                    if ($check_errors['type'] == 'error') {
                        $this->response(array('status' => 'Failed', 'data' => $recharge_from_TSO, 'msg' => $check_errors['msg']), 200);
                    } elseif ($check_errors['type'] == 'success') {

                        $recharge_tran_id = $recharge_from_TSO['TRNID'];
                        

                        /* Call To TSO API */
                        if($plan_type == 'Postpaid Mobile'){
                            //For TSO Postpaid fixed commission capping
                            //Vaidehi 23rd may 19
                      //   $commissionResult = $this->serviceRetailerCommissionDistributionForPostpaid($input, $wallet_trans_no,$created_by);   
                        }else{
                       //  $commissionResult = $this->serviceRetailerCommissionDistributionForRecharges($input, $wallet_trans_no,$created_by);   
                        }

                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'TSO_recharge.log', $commissionResult, 'TSO Recharge - Commission result');

                        $updated_wallet_amt = $check_user_wallet[0]['amt'] - $recharge_amount;

                        /* Update wallet table */
                        $update_wallet = $this->tso_model->updateUserWallet($user_id, $updated_wallet_amt);

                        $wallet_trans_detail['w_id'] = $update_wallet[0]['id'];
                        $wallet_trans_detail['amt'] = $recharge_amount;
                        $wallet_trans_detail['comment'] = 'TSO Mobile Recharge';
                        $wallet_trans_detail['status'] = 'Debited';
                        $wallet_trans_detail['user_id'] = $user_id;
                        $wallet_trans_detail['added_by'] = $user_id;
                        $wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                        $wallet_trans_detail['amt_before_trans'] = $check_user_wallet[0]['amt'];
                        $wallet_trans_detail['amt_after_trans'] = $updated_wallet_amt;
                        $wallet_trans_detail['ticket_id'] = '';
                        // $wallet_trans_detail['transaction_type_id'] = '3';
                        $wallet_trans_detail['wallet_type'] = 'actual_wallet';
                        $wallet_trans_detail['transaction_type'] = 'Debited';
                        $wallet_trans_detail['is_status'] = 'Y';

                        $wallet_trans_detail['transaction_no'] = $wallet_trans_no;

                        /* Update wallet_trans table */
                        $wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail);

                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'TSO_recharge.log', $update_wallet, 'TSO Recharge Response START');
                        // show($update_wallet, 1);
                        if(isset($input['payment_type']) && !empty($input['payment_type']) && $input['payment_type'] == 'epaylater'){
                            $trans_type = 'epay';
                        }else{
                            $trans_type = 'cash';
                        }
                        $transaction_data = array(
                            'type' => $plan_type,
                            'operator' => $operator,
                            'recharge_on' => $mobile_number,
                            'recharge_from' => $recharge_from,
                            'last_transaction_amount' => $recharge_amount,
                            'transaction_id' => $recharge_tran_id,
                            'wallet_tran_id' => $wallet_trans_no,
                            'created_by' => $input['user_id'],
                            'transaction_type' => $trans_type
                        );

                        /* Insert into recharge_transaction table */
                        $this->tso_model->saveTransactionData($transaction_data);

                        if ($recharge_from_TSO['STATUSCODE'] == 0) {
                            $status = "Success";
                        } elseif ($recharge_from_TSO['STATUSCODE'] == 6) {
                            $status = "Request Accepted";
                        } elseif ($recharge_from_TSO['STATUSCODE'] == 1 || $recharge_from_TSO['STATUSCODE'] == 9) {
                            $status = "Request Unsuccessfull";
                        }

                        if ($recharge_from_TSO['TRNSTATUS'] == 0) {
                            $trans_status = "Pending";
                        } elseif ($recharge_from_TSO['TRNSTATUS'] == 1) {
                            $trans_status = "Success";
                        } elseif ($recharge_from_TSO['TRNSTATUS'] == 4 || $recharge_from_TSO['TRNSTATUS'] == 6) {
                            $trans_status = "Hold | Underque | Request Accepted";
                        } elseif ($recharge_from_TSO['TRNSTATUS'] == 2 || $recharge_from_TSO['TRNSTATUS'] == 3) {
                            $trans_status = "Failed";
                        } elseif ($recharge_from_TSO['TRNSTATUS'] == 5) {
                            $trans_status = "Refunded";
                        }

                        $recharge_response_TSO = array(
                            "customer_number" => $mobile_number,
                            "amount" => $recharge_amount,
                            "operator" => $operator,
                            "request_type" => "Mobile Recharge",
                            "plan_type" => $plan_type,
                            "recharge_type" => $recharge_type,
                            "transaction_id" => $recharge_tran_id,
                            "status" => $status,
                            "trans_status" => $trans_status,
                            "status_text" => $recharge_from_TSO['STATUSMSG'],
                            "trans_reference" => $recharge_from_TSO['TRNREFNO'],
                            "wallet_trans_no" => $wallet_trans_no,
                            "created_at" => date('Y-m-d H:i:s'),
                            "created_by" => $input['user_id'],
                            "tso_topup_bal" => $balance['STATUSMSG'],//$recharge_from_TSO['BAL'] - $recharge_amount,
                            // ,"updated_at" => date('Y-m-d H:i:s')
                        );

                        $recharge_from_TSO['wallet_tran_id'] = (string) $wallet_trans_no;
                        $recharge_from_TSO['transaction_id'] = (string) $recharge_tran_id;

                        /* Insert into tso_transaction_details table */
                        $this->tso_model->saveTSOData($recharge_response_TSO);
                        //Update vendor wallet topup
                        $user_data = array('vendor_id'=> '3','amount' => $recharge_from_TSO['BAL']);
                      //  $res = $this->reconcilation_api->updateTopUp($user_data);
                
                        //Vendor Master for reconsilation tables
                         $user_data = array('type' => 'debit','amount' => $recharge_amount,'vendor_id' => '3','trans_ref_no' => $wallet_trans_no,'vendor_service_id' => '18');
                       //  $result = $this->reconcilation_api->vendorPassbook($user_data);
                         log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'TSO_recharge.log', $recharge_from_TSO, 'TSO Recharge - End');

                        $this->response(array('status' => 'Success', 'data' => $recharge_from_TSO, 'msg' => $check_errors['msg']), 200);
                    }
                } else {
                    $this->response(array('status' => 'Failed', 'data' => $recharge_from_TSO, 'msg' => 'Insufficient balance'), 200);
                }
            }
        }
    }

    private function checkErrorsInTSOResponse($response, $type) {

        $response_error = [];

        if ($response['STATUSCODE'] == 0) {
            $response_error['type'] = 'success';
            // $response_error['msg'] = $response['STATUSMSG'];
            $response_error['msg'] = 'Recharge Successful';
        } elseif ($response['STATUSCODE'] == 6) {
            $response_error['type'] = 'success';
            // $response_error['msg'] = $response['STATUSMSG'];
            $response_error['msg'] = 'Recharge Successful';
        } elseif ($response['STATUSCODE'] == 1) {
            $response_error['type'] = 'error';
            $response_error['msg'] = $response['STATUSMSG'];
        } elseif ($response['STATUSCODE'] == 9) {
            $response_error['type'] = 'error';
            $response_error['msg'] = $response['STATUSMSG'];
        }

        return $response_error;
    }

    public function serviceRetailerCommissionDistribution($input = "", $trans_ref_no = "") {

        $user_id = $input['user_id'];
        $service_id = UTILITIES_SERVICE_ID;
        $getRetailerServicesCommission = $this->tso_model->getRetailerServicesCommission($user_id, $service_id);
        // show($getRetailerServicesCommission, 1);

        if ($getRetailerServicesCommission) {

            $agent_id = $getRetailerServicesCommission[0]['user_id'];
            $agent_code = $getRetailerServicesCommission[0]['agent_code'];
            $distributor_id = $getRetailerServicesCommission[0]['level_5'];
            $Area_dis_id = $getRetailerServicesCommission[0]['level_4'];
            $master_dis_id = $getRetailerServicesCommission[0]['level_3'];
            $company_id = $getRetailerServicesCommission[0]['level_2'];
            $trimax_id = $getRetailerServicesCommission[0]['level_1'];
            $service_id = $getRetailerServicesCommission[0]['service_id'];
            $commission_id = $getRetailerServicesCommission[0]['commission_id'];
            $total_commission = $getRetailerServicesCommission[0]['total_commission'];

            $values[0] = json_decode($getRetailerServicesCommission[0]['values']);

            $trimax_commission = '0';
            $company_commission = '0';
            $md_commission = '0';
            $ad_commission = '0';
            $dist_commission = '0';
            $retailer_commission = '0';
            $with_gst = $without_gst = 0;
            $with_gst = $without_gst = 0;
            if (!empty($values)) {

                $trimax_commission = round($values[0]->Trimax, 2);
                $company_commission = round($values[0]->Rokad, 2);
                $md_commission = round($values[0]->MD, 2);
                $ad_commission = round($values[0]->AD, 2);
                $dist_commission = round($values[0]->Distributor, 2);
                $retailer_commission = round($values[0]->Retailer, 2);

                $recharge_amount = $input['recharge_amount']; 
                $commission_percent = !empty($total_commission) ? $total_commission : '0';                        
                $trimax_cal = calculateGstTds($recharge_amount, $commission_percent);
                $earnings = $trimax_cal['earnings']; //trimax_earnings
                $gst = $trimax_cal['gst']; //trimax_gst                            
                $tds = $trimax_cal['tds']; //trimax_tds
                $final_amount = $trimax_cal['final_amount']; //trimax_final_amt                                                        

                $rokad_trimax_cal = calculateGstTds($earnings, $trimax_commission);
                $rokad_trimax_amt = $rokad_trimax_cal['earnings']; //rokad_trimax_amt
                $with_gst = $with_gst + $rokad_trimax_cal['earnings'];
                $rokad_trimax_cal_with_tds_gst = calculateGstTds($final_amount, $trimax_commission);
                $rokad_trimax_amt_with_tds_gst = $rokad_trimax_cal_with_tds_gst['earnings']; //rokad_trimax_amt_with_tds_gst
                $without_gst = $without_gst + $rokad_trimax_amt_with_tds_gst;        

                $company_cal = calculateGstTds($earnings, $company_commission);
                $company_amt = $company_cal['earnings'];//company gst
                $with_gst = $with_gst + $company_cal['earnings'];
                $company_cal_with_tds_gst = calculateGstTds($final_amount, $company_commission);
                $company_amt_with_tds_gst = $company_cal_with_tds_gst['earnings']; //company_amt_with_tds_gst                    
                $without_gst = $without_gst + $company_amt_with_tds_gst;    

                $rd_cal = calculateGstTds($earnings, $md_commission);
                $rd_amt = $rd_cal['earnings']; //rd_amt            
                $with_gst = $with_gst + $rd_cal['earnings'];                 
                $rd_cal_with_tds_gst = calculateGstTds($final_amount, $md_commission);
                $rd_amt_with_tds_gst = $rd_cal_with_tds_gst['earnings']; //rd_amt_with_tds_gst
                $without_gst = $without_gst + $rd_amt_with_tds_gst;

                $dd_cal = calculateGstTds($earnings, $ad_commission);
                $dd_amt = $dd_cal['earnings']; //dd_amt   
                $with_gst = $with_gst + $dd_cal['earnings'];                                 
                $dd_cal_with_tds_gst = calculateGstTds($final_amount, $ad_commission);
                $dd_amt_with_tds_gst = $dd_cal_with_tds_gst['earnings']; //dd_amt_with_tds_gst
                $without_gst = $without_gst + $dd_amt_with_tds_gst;

                $ex_cal = calculateGstTds($earnings, $dist_commission);
                $ex_amt = $ex_cal['earnings']; //ex_amt      
                $with_gst = $with_gst + $ex_cal['earnings'];              
                $ex_cal_with_tds_gst = calculateGstTds($final_amount, $dist_commission);
                $ex_amt_with_tds_gst = $ex_cal_with_tds_gst['earnings']; //ex_amt_with_tds_gst
                $without_gst = $without_gst + $ex_amt_with_tds_gst;


                $sa_cal = calculateGstTds($earnings, $retailer_commission);
                $sa_amt = $sa_cal['earnings']; //sa_amt     
                $with_gst = $with_gst + $sa_cal['earnings'];              
                $sa_cal_with_tds_gst = calculateGstTds($final_amount, $retailer_commission);
                $sa_amt_with_tds_gst = $sa_cal_with_tds_gst['earnings']; //sa_amt_with_tds_gst 
                $without_gst = $without_gst + $sa_amt_with_tds_gst;

                $remaining_commission = round($with_gst, 2) - round($earnings, 2);

                /*For commission adjustment - Gopal */
                // $array_without_empty = array_filter((array)$values[0]);
                // $keys = array_keys($array_without_empty);
                // switch ($keys[0]) {
                //     case 'Trimax':
                //         $rokad_trimax_amt = $earnings - ($with_gst - $rokad_trimax_amt);
                //         $rokad_trimax_amt_with_tds_gst = $final_amount - ($without_gst - $rokad_trimax_amt_with_tds_gst); 
                //         break;
                //     case 'Rokad':
                //         $company_amt = $earnings - ($with_gst - $company_amt);
                //         $company_amt_with_tds_gst = $final_amount - ($without_gst - $company_amt_with_tds_gst);
                //         break;
                //     case 'MD':
                //         $rd_amt = $earnings - ($with_gst - $rd_amt);
                //         $rd_amt_with_tds_gst = $final_amount - ($without_gst - $rd_amt_with_tds_gst); 
                //         break;
                //     case 'AD':
                //         $dd_amt = $earnings - ($with_gst - $dd_amt);
                //         $dd_amt_with_tds_gst = $final_amount - ($without_gst - $dd_amt_with_tds_gst); 
                //         break;
                //     case 'Distributor':
                //         $ex_amt = $earnings - ($with_gst - $ex_amt);
                //         $ex_amt_with_tds_gst = $final_amount - ($without_gst - $ex_amt_with_tds_gst); 
                //         break;
                //     case 'Retailer':
                //         $sa_amt = $earnings - ($with_gst - $sa_amt);
                //         $sa_amt_with_tds_gst = $final_amount - ($without_gst - $sa_amt_with_tds_gst); 
                //         break;    
                // }
                /*for commission adjustment  - Gopal */             
            }

            $comm_distribution_array['user_id'] = $agent_id;
            $comm_distribution_array['service_id'] = $service_id;
            $comm_distribution_array['commission_id'] = $commission_id;
            $comm_distribution_array['transaction_no'] = $trans_ref_no;
            $comm_distribution_array['transaction_amt'] = $recharge_amount;
            $comm_distribution_array['service_comm_percent'] = SERVICE_COMMISSION;
            $comm_distribution_array['trimax_comm_percent'] = $total_commission;
            ;
            $comm_distribution_array['gst_percentage'] = GST_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['tds_percentage'] = TDS_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['trimax_earnings'] = $earnings;
            $comm_distribution_array['trimax_gst'] = $gst;
            $comm_distribution_array['trimax_tds'] = $tds;
            $comm_distribution_array['trimax_final_amt'] = $final_amount;
            $comm_distribution_array['rokad_trimax_per'] = $values[0]->Trimax;
            $comm_distribution_array['rokad_trimax_amt'] = $rokad_trimax_amt;
            $comm_distribution_array['rokad_trimax_amt_with_tds_gst'] = $rokad_trimax_amt_with_tds_gst;
            $comm_distribution_array['company_per'] = $values[0]->Rokad;
            $comm_distribution_array['company_amt'] = $company_amt;
            $comm_distribution_array['company_amt_with_tds_gst'] = $company_amt_with_tds_gst;
            $comm_distribution_array['rd_per'] = $values[0]->MD;
            $comm_distribution_array['rd_amt'] = $rd_amt;
            $comm_distribution_array['rd_amt_with_tds_gst'] = $rd_amt_with_tds_gst;
            $comm_distribution_array['dd_per'] = $values[0]->AD;
            $comm_distribution_array['dd_amt'] = $dd_amt;
            $comm_distribution_array['dd_amt_with_tds_gst'] = $dd_amt_with_tds_gst;
            $comm_distribution_array['ex_per'] = $values[0]->Distributor;
            $comm_distribution_array['ex_amt'] = $ex_amt;
            $comm_distribution_array['ex_amt_with_tds_gst'] = $ex_amt_with_tds_gst;
            $comm_distribution_array['sa_per'] = $values[0]->Retailer;
            $comm_distribution_array['sa_amt'] = $sa_amt;
            $comm_distribution_array['sa_amt_with_tds_gst'] = $sa_amt_with_tds_gst;
            $comm_distribution_array['status'] = 'Y';
            $comm_distribution_array['created_by'] = $user_id;
            $comm_distribution_array['commission_difference'] = $remaining_commission;
            $comm_distribution_array['created_date'] = date('Y-m-d H:i:s');

            $id = $this->Utilities_model->saveVasCommissionData($comm_distribution_array);
            // $comm_distribution_array['id'] = $id;
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'tata_docomo_special.log', $comm_distribution_array, 'Commission Distribution Response');

            return $comm_distribution_array;
        }
    }


     public function serviceRetailerCommissionDistributionForRecharges($input = "", $trans_ref_no = "",$created_by =""){

        $user_id = $input['user_id'];
        if(in_array($input['service'], ['jri', 'JRI', 'tso', 'TSO']) ){
            if($input['rec_type'] == 'Prepaid Mobile'){
                $code = strtolower($input['service'].$input['mobile_operator'].'Prepaid');
                $service_id = PREPAID_SERVICE_ID;
            }elseif($input['rec_type'] == 'Postpaid Mobile'){
                $code = strtolower($input['service'].$input['mobile_operator'].'Postpaid');
                $service_id = POSTPAID_SERVICE_ID;
            }elseif($input['recharge_type'] == 'DTH'){
                $code = strtolower($input['service'].$input['dth_operator']);
                $service_id = DTH_SERVICE_ID;
            }

            $LcSqlStr = "SELECT * from services_commissions 
            where status = 'Y'";
            $LcSqlStr .= " AND `values` NOT LIKE '{\"Trimax\":\"0\",\"Rokad\":\"0\",\"MD\":\"0\",\"AD\":\"0\",\"Distributor\":\"0\",\"Retailer\":\"0\"}'";
            $LcSqlStr .= " AND `commission_name` LIKE '%" . str_replace(" ", "", $code) . "%'";
            $LcSqlStr .= " AND service_id = '" . $service_id . "'"; 
            $LcSqlStr .= " AND created_by = '" . $created_by . "'"; 
            ;

            // show($LcSqlStr, 1);
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'tsoapi.log', $LcSqlStr, 'Commission Distribution Response');
            $query = $this->db->query($LcSqlStr);
            $new = $query->result();
            if($new){
                $row = $new[0];
                $getRetailerServicesCommission = $this->Utilities_model->getRetailerServicesCommissionForRecharges($user_id, $row->service_id, $row->sub_service_id,$created_by);
            }
        }else{
            $service_id = UTILITIES_SERVICE_ID;
            $getRetailerServicesCommission = $this->Utilities_model->getRetailerServicesCommission($user_id, $service_id,$created_by);
        }
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'tsoapi.log', $getRetailerServicesCommission, 'Commission Distribution Response');
        if ($getRetailerServicesCommission) {

            $agent_id = $getRetailerServicesCommission[0]['user_id'];
            $agent_code = $getRetailerServicesCommission[0]['agent_code'];
            $distributor_id = $getRetailerServicesCommission[0]['level_5'];
            $Area_dis_id = $getRetailerServicesCommission[0]['level_4'];
            $master_dis_id = $getRetailerServicesCommission[0]['level_3'];
            $company_id = $getRetailerServicesCommission[0]['level_2'];
            $trimax_id = $getRetailerServicesCommission[0]['level_1'];
            $service_id = $getRetailerServicesCommission[0]['service_id'];
            $commission_id = $getRetailerServicesCommission[0]['commission_id'];
            $total_commission = $getRetailerServicesCommission[0]['total_commission'];

            $values[0] = json_decode($getRetailerServicesCommission[0]['values']);

            $trimax_commission = '0';
            $company_commission = '0';
            $md_commission = '0';
            $ad_commission = '0';
            $dist_commission = '0';
            $retailer_commission = '0';
            $with_gst = $without_gst = 0;
            
            if (!empty($values)) {

                $trimax_commission = isset($values[0]->Trimax) ? round($values[0]->Trimax, 2) : '0.00';
                $company_commission = isset($values[0]->Rokad) ? round($values[0]->Rokad, 2) : '0.00';
                $md_commission = isset($values[0]->MD) ? round($values[0]->MD, 2) : '0.00';
                $ad_commission = isset($values[0]->AD) ? round($values[0]->AD, 2) : '0.00';
                $dist_commission = isset($values[0]->Distributor) ? round($values[0]->Distributor, 2) : '0.00';
                $retailer_commission = isset($values[0]->Retailer) ? round($values[0]->Retailer, 2) : '0.00';

                $recharge_amount = $input['recharge_amount']; //transaction_amt
                // $commission_percent = $input['type'] == 'JRI' ? JRI_TRIMAX_COMMISSION : CYBERPLAT_TRIMAX_COMMISSION; //trimax_comm_percent                     
                
                $commission_percent = !empty($total_commission) ? $total_commission : '0';

                $trimax_cal = calculateGstTds($recharge_amount, $commission_percent);
                $earnings = $trimax_cal['earnings']; //trimax_earnings
                $gst = $trimax_cal['gst']; //trimax_gst                            
                $tds = $trimax_cal['tds']; //trimax_tds
                $final_amount = $trimax_cal['final_amount']; //trimax_final_amt                                                        

                $rokad_trimax_cal = calculateGstTds($earnings, $trimax_commission);
                $rokad_trimax_amt = $rokad_trimax_cal['earnings']; //rokad_trimax_amt                             
                $rokad_trimax_cal_with_tds_gst = calculateGstTds($final_amount, $trimax_commission);
                $rokad_trimax_amt_with_tds_gst = $rokad_trimax_cal_with_tds_gst['earnings']; //rokad_trimax_amt_with_tds_gst                            

                $company_cal = calculateGstTds($earnings, $company_commission);
                $company_amt = $company_cal['earnings']; //company_amt                             
                $company_cal_with_tds_gst = calculateGstTds($final_amount, $company_commission);
                $company_amt_with_tds_gst = $company_cal_with_tds_gst['earnings']; //company_amt_with_tds_gst                        

                $rd_cal = calculateGstTds($earnings, $md_commission);
                $rd_amt = $rd_cal['earnings']; //rd_amt                             
                $rd_cal_with_tds_gst = calculateGstTds($final_amount, $md_commission);
                $rd_amt_with_tds_gst = $rd_cal_with_tds_gst['earnings']; //rd_amt_with_tds_gst
                
                $dd_cal = calculateGstTds($earnings, $ad_commission);
                $dd_amt = $dd_cal['earnings']; //dd_amt                             
                $dd_cal_with_tds_gst = calculateGstTds($final_amount, $ad_commission);
                $dd_amt_with_tds_gst = $dd_cal_with_tds_gst['earnings']; //dd_amt_with_tds_gst

                $ex_cal = calculateGstTds($earnings, $dist_commission);
                $ex_amt = $ex_cal['earnings']; //ex_amt                             
                $ex_cal_with_tds_gst = calculateGstTds($final_amount, $dist_commission);
                $ex_amt_with_tds_gst = $ex_cal_with_tds_gst['earnings']; //ex_amt_with_tds_gst

                $sa_cal = calculateGstTds($earnings, $retailer_commission);
                $sa_amt = $sa_cal['earnings']; //sa_amt                             
                $sa_cal_with_tds_gst = calculateGstTds($final_amount, $retailer_commission);
                $sa_amt_with_tds_gst = $sa_cal_with_tds_gst['earnings']; //sa_amt_with_tds_gst              
            }

             $comm_distribution_array['user_id'] = $agent_id;
            $comm_distribution_array['service_id'] = $service_id;
            $comm_distribution_array['commission_id'] = $commission_id;
            $comm_distribution_array['transaction_no'] = $trans_ref_no;
            $comm_distribution_array['transaction_amt'] = $recharge_amount;
            $comm_distribution_array['service_comm_percent'] = SERVICE_COMMISSION;
            $comm_distribution_array['trimax_comm_percent'] = $total_commission;
            $comm_distribution_array['gst_percentage'] = GST_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['tds_percentage'] = TDS_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['trimax_earnings'] = $earnings;
            $comm_distribution_array['trimax_gst'] = $gst;
            $comm_distribution_array['trimax_tds'] = $tds;
            $comm_distribution_array['trimax_final_amt'] = $final_amount;
            $comm_distribution_array['rokad_trimax_per'] = $trimax_commission;
            $comm_distribution_array['rokad_trimax_amt'] = $rokad_trimax_amt;
            $comm_distribution_array['rokad_trimax_amt_with_tds_gst'] = $rokad_trimax_amt_with_tds_gst;
            $comm_distribution_array['company_per'] = $company_commission;
            $comm_distribution_array['company_amt'] = $company_amt;
            $comm_distribution_array['company_amt_with_tds_gst'] = $company_amt_with_tds_gst;
            $comm_distribution_array['rd_per'] = $md_commission;
            $comm_distribution_array['rd_amt'] = $rd_amt;
            $comm_distribution_array['rd_amt_with_tds_gst'] = $rd_amt_with_tds_gst;
            $comm_distribution_array['dd_per'] = $ad_commission;
            $comm_distribution_array['dd_amt'] = $dd_amt;
            $comm_distribution_array['dd_amt_with_tds_gst'] = $dd_amt_with_tds_gst;
            $comm_distribution_array['ex_per'] = $dist_commission;
            $comm_distribution_array['ex_amt'] = $ex_amt;
            $comm_distribution_array['ex_amt_with_tds_gst'] = $ex_amt_with_tds_gst;
            $comm_distribution_array['sa_per'] = $retailer_commission;
            $comm_distribution_array['sa_amt'] = $sa_amt;
            $comm_distribution_array['sa_amt_with_tds_gst'] = $sa_amt_with_tds_gst;
            $comm_distribution_array['status'] = 'Y';
            $comm_distribution_array['created_by'] = $user_id;
            $comm_distribution_array['created_date'] = date('Y-m-d H:i:s');

            $id = $this->Utilities_model->saveVasCommissionData($comm_distribution_array);
            // $comm_distribution_array['id'] = $id;
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'TSO_recharge.log', $comm_distribution_array, '$comm_distribution_array');

            return $comm_distribution_array;
        }
    }
    
    public function getTsoBal() {
        return authTSO();
    }

    public function transStatus_post() {
        $data = array();
        $input = $this->post();
        //show($input,1);
        if (!empty($input)) {

            $config = array(
                array('field' => 'trans_no', 'label' => 'Transaction Id', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Transaction Id is required')),
                array('field' => 'mobile_no', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.'))
            );
            if (form_validate_rules($config) == FALSE) {
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {
                /* Call To TSO API */
                $transStatus = TransStatus($input);
                $this->response(array('status' => 'success', 'data' => $transStatus), 200);
            }
        }
    }

    public function checkCommission($input,$created_by){
        if($input['plan_type'] == 'Prepaid Mobile'){
            $code = strtolower($input['service'].$input['mobile_operator'].'Prepaid');
            $service_id = PREPAID_SERVICE_ID;
        }elseif($input['plan_type'] == 'Postpaid Mobile'){
            $code = strtolower($input['service'].$input['mobile_operator'].'Postpaid');
            $service_id = POSTPAID_SERVICE_ID;
        }elseif($input['recharge_type'] == 'DTH'){
            $code = strtolower($input['service'].$input['dth_operator']);
            $service_id = DTH_SERVICE_ID;
        }

        $LcSqlStr = "SELECT * from services_commissions 
        where status = 'Y'";
        $LcSqlStr .= " AND `values` NOT LIKE '{\"Trimax\":\"0\",\"Rokad\":\"0\",\"MD\":\"0\",\"AD\":\"0\",\"Distributor\":\"0\",\"Retailer\":\"0\"}'";
        $LcSqlStr .= " AND `commission_name` LIKE '%" . str_replace(" ", "", $code) . "%'";
        $LcSqlStr .= " AND `created_by` = '". $created_by . "'";
        $LcSqlStr .= " AND service_id = '" . $service_id . "'"; 
        ;
	 log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'TSO_recharge.log', $LcSqlStr, 'Commission query');
        $query = $this->db->query($LcSqlStr);
        return $query->result();

    }
    
    public function serviceRetailerCommissionDistributionForPostpaid($input = "", $trans_ref_no = "",$created_by =""){
       $user_id = $input['user_id'];
        if(in_array($input['service'], ['jri', 'JRI', 'tso', 'TSO']) ){
            if($input['rec_type'] == 'Prepaid Mobile'){
                $code = strtolower($input['service'].$input['mobile_operator'].'Prepaid');
                $service_id = PREPAID_SERVICE_ID;
            }elseif($input['rec_type'] == 'Postpaid Mobile'){
                $code = strtolower($input['service'].$input['mobile_operator'].'Postpaid');
                $service_id = POSTPAID_SERVICE_ID;
            }elseif($input['recharge_type'] == 'DTH'){
                $code = strtolower($input['service'].$input['dth_operator']);
                $service_id = DTH_SERVICE_ID;
            }

            $LcSqlStr = "SELECT * from services_commissions 
            where status = 'Y'";
            $LcSqlStr .= " AND `values` NOT LIKE '{\"Trimax\":\"0\",\"Rokad\":\"0\",\"MD\":\"0\",\"AD\":\"0\",\"Distributor\":\"0\",\"Retailer\":\"0\"}'";
            $LcSqlStr .= " AND `commission_name` LIKE '%" . str_replace(" ", "", $code) . "%'";
            $LcSqlStr .= " AND service_id = '" . $service_id . "'"; 
            $LcSqlStr .= " AND created_by = '" . $created_by . "'"; 
            ;

            // show($LcSqlStr, 1);
            $query = $this->db->query($LcSqlStr);
            $new = $query->result();
            if($new){
                $row = $new[0];
                $getRetailerServicesCommission = $this->Utilities_model->getRetailerServicesCommissionForRecharges($user_id, $row->service_id, $row->sub_service_id,$created_by);
                $sql = "select bos_caped_rupees,agent_caped_rupees from sub_services where  service_id = '" . $row->service_id . "' AND id = '" . $row->sub_service_id . "'";
                $squery = $this->db->query($sql);
                $cap_value = $squery->result();
                $agent_capped_value = $cap_value[0];
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'TSO_recharge.log', $agent_capped_value, '$agent_capped_value1');
            }
        }else{
            $service_id = UTILITIES_SERVICE_ID;
            $getRetailerServicesCommission = $this->Utilities_model->getRetailerServicesCommission($user_id, $service_id,$created_by);
        }
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'TSO_recharge.log', $getRetailerServicesCommission, '$getRetailerServicesCommission');
        if ($getRetailerServicesCommission) {

            $agent_id = $getRetailerServicesCommission[0]['user_id'];
            $agent_code = $getRetailerServicesCommission[0]['agent_code'];
            $distributor_id = $getRetailerServicesCommission[0]['level_5'];
            $Area_dis_id = $getRetailerServicesCommission[0]['level_4'];
            $master_dis_id = $getRetailerServicesCommission[0]['level_3'];
            $company_id = $getRetailerServicesCommission[0]['level_2'];
            $trimax_id = $getRetailerServicesCommission[0]['level_1'];
            $service_id = $getRetailerServicesCommission[0]['service_id'];
            $commission_id = $getRetailerServicesCommission[0]['commission_id'];
            $total_commission = $getRetailerServicesCommission[0]['total_commission'];

            $values[0] = json_decode($getRetailerServicesCommission[0]['values']);

            $trimax_commission = '0';
            $company_commission = '0';
            $md_commission = '0';
            $ad_commission = '0';
            $dist_commission = '0';
            $retailer_commission = '0';
            $with_gst = $without_gst = 0;
            
            if (!empty($values)) {

                $trimax_commission = isset($values[0]->Trimax) ? round($values[0]->Trimax, 2) : '0.00';
                $company_commission = isset($values[0]->Rokad) ? round($values[0]->Rokad, 2) : '0.00';
                $md_commission = isset($values[0]->MD) ? round($values[0]->MD, 2) : '0.00';
                $ad_commission = isset($values[0]->AD) ? round($values[0]->AD, 2) : '0.00';
                $dist_commission = isset($values[0]->Distributor) ? round($values[0]->Distributor, 2) : '0.00';
                $retailer_commission = isset($values[0]->Retailer) ? round($values[0]->Retailer, 2) : '0.00';

                $recharge_amount = $input['recharge_amount']; //transaction_amt
                // $commission_percent = $input['type'] == 'JRI' ? JRI_TRIMAX_COMMISSION : CYBERPLAT_TRIMAX_COMMISSION; //trimax_comm_percent                     
                
                $commission_percent = !empty($total_commission) ? $total_commission : '0';

                $trimax_cal = calculateGstTds($recharge_amount, $commission_percent);
                //$earnings = $trimax_cal['earnings']; //trimax_earnings
                $earnings = TSO_COMMISSION_VAL;
                
                $gst = $trimax_cal['gst']; //trimax_gst                            
                $tds = $trimax_cal['tds']; //trimax_tds
                $final_amount = $trimax_cal['final_amount']; //trimax_final_amt                                                        

                $rokad_trimax_cal = calculateGstTds($earnings, $trimax_commission);
                $rokad_trimax_amt = $rokad_trimax_cal['earnings']; //rokad_trimax_amt
                
                if($trimax_commission != '0.00' && $agent_capped_value->bos_caped_rupees != '0'){
                    $rokad_trimax_amt = $agent_capped_value->bos_caped_rupees;
                }else{
                    $rokad_trimax_amt = '0.00';
                }
                
                
                $with_gst = $with_gst + $rokad_trimax_amt;
                $rokad_trimax_cal_with_tds_gst = calculateGstTds($final_amount, $trimax_commission);
                $rokad_trimax_amt_with_tds_gst = $rokad_trimax_cal_with_tds_gst['earnings']; //rokad_trimax_amt_with_tds_gst                            
                $without_gst = $without_gst + $rokad_trimax_amt_with_tds_gst;
                
                $company_cal = calculateGstTds($earnings, $company_commission);
                $company_amt = $company_cal['earnings']; //company_amt                             
                
                if($company_commission != '0.00' && $agent_capped_value->bos_caped_rupees != '0'){
                    $company_amt = $agent_capped_value->bos_caped_rupees;
                }else{
                    $company_amt = '0.00';
                }
                
                $with_gst = $with_gst + $company_amt;
                $company_cal_with_tds_gst = calculateGstTds($final_amount, $company_commission);
                $company_amt_with_tds_gst = $company_cal_with_tds_gst['earnings']; //company_amt_with_tds_gst                        
                $without_gst = $without_gst + $company_amt_with_tds_gst;
                
                $rd_cal = calculateGstTds($earnings, $md_commission);
                $rd_amt = $rd_cal['earnings']; //rd_amt
                if($md_commission != '0.00' && $agent_capped_value->bos_caped_rupees != '0'){
                    $rd_amt = $agent_capped_value->bos_caped_rupees;
                }else{
                    $rd_amt = '0.00';
                }
                
                $with_gst = $with_gst + $rd_amt;
                $rd_cal_with_tds_gst = calculateGstTds($final_amount, $md_commission);
                $rd_amt_with_tds_gst = $rd_cal_with_tds_gst['earnings']; //rd_amt_with_tds_gst
                $without_gst = $without_gst + $rd_amt_with_tds_gst;
                
                $dd_cal = calculateGstTds($earnings, $ad_commission);
                $dd_amt = $dd_cal['earnings']; //dd_amt                             
                
                if($ad_commission != '0.00' && $agent_capped_value->bos_caped_rupees != '0'){
                    $dd_amt = $agent_capped_value->bos_caped_rupees;
                }else{
                    $dd_amt = '0.00';
                }
                
                $with_gst = $with_gst + $dd_amt;
                $dd_cal_with_tds_gst = calculateGstTds($final_amount, $ad_commission);
                $dd_amt_with_tds_gst = $dd_cal_with_tds_gst['earnings']; //dd_amt_with_tds_gst
                $without_gst = $without_gst + $dd_amt_with_tds_gst;

                $ex_cal = calculateGstTds($earnings, $dist_commission);
                $ex_amt = $ex_cal['earnings']; //ex_amt
                
                if($dist_commission != '0.00' && $agent_capped_value->bos_caped_rupees != '0'){
                    $ex_amt = $agent_capped_value->bos_caped_rupees;
                }else{
                    $ex_amt = '0.00';
                }
                $with_gst = $with_gst + $ex_amt;
                $ex_cal_with_tds_gst = calculateGstTds($final_amount, $dist_commission);
                $ex_amt_with_tds_gst = $ex_cal_with_tds_gst['earnings']; //ex_amt_with_tds_gst
                $without_gst = $without_gst + $ex_amt_with_tds_gst;

                $sa_cal = calculateGstTds($earnings, $retailer_commission);
                $sa_amt = $sa_cal['earnings']; //sa_amt                             
                 if($retailer_commission != '0.00' && $agent_capped_value->agent_caped_rupees != '0'){
                    $sa_amt = $agent_capped_value->agent_caped_rupees;
                }else{
                    $sa_amt = '0.00';
                }
              
                $with_gst = $with_gst + $sa_amt;
                $sa_cal_with_tds_gst = calculateGstTds($final_amount, $retailer_commission);
                $sa_amt_with_tds_gst = $sa_cal_with_tds_gst['earnings']; //sa_amt_with_tds_gst              
                $without_gst = $without_gst + $sa_amt_with_tds_gst;
                $remaining_commission = round($with_gst, 2) - round($earnings, 2);
            }

             $comm_distribution_array['user_id'] = $agent_id;
            $comm_distribution_array['service_id'] = $service_id;
            $comm_distribution_array['commission_id'] = $commission_id;
            $comm_distribution_array['transaction_no'] = $trans_ref_no;
            $comm_distribution_array['transaction_amt'] = $recharge_amount;
            $comm_distribution_array['service_comm_percent'] = SERVICE_COMMISSION;
            $comm_distribution_array['trimax_comm_percent'] = TSO_COMMISSION_VAL;
            $comm_distribution_array['gst_percentage'] = GST_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['tds_percentage'] = TDS_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['trimax_earnings'] = $earnings;
            $comm_distribution_array['trimax_gst'] = $gst;
            $comm_distribution_array['trimax_tds'] = $tds;
            $comm_distribution_array['trimax_final_amt'] = $final_amount;
            $comm_distribution_array['rokad_trimax_per'] = $trimax_commission;
            $comm_distribution_array['rokad_trimax_amt'] = $rokad_trimax_amt;
            $comm_distribution_array['rokad_trimax_amt_with_tds_gst'] = $rokad_trimax_amt_with_tds_gst;
            $comm_distribution_array['company_per'] = $company_commission;
            $comm_distribution_array['company_amt'] = $company_amt;
            $comm_distribution_array['company_amt_with_tds_gst'] = $company_amt_with_tds_gst;
            $comm_distribution_array['rd_per'] = $md_commission;
            $comm_distribution_array['rd_amt'] = $rd_amt;
            $comm_distribution_array['rd_amt_with_tds_gst'] = $rd_amt_with_tds_gst;
            $comm_distribution_array['dd_per'] = $ad_commission;
            $comm_distribution_array['dd_amt'] = $dd_amt;
            $comm_distribution_array['dd_amt_with_tds_gst'] = $dd_amt_with_tds_gst;
            $comm_distribution_array['ex_per'] = $dist_commission;
            $comm_distribution_array['ex_amt'] = $ex_amt;
            $comm_distribution_array['ex_amt_with_tds_gst'] = $ex_amt_with_tds_gst;
            $comm_distribution_array['sa_per'] = $retailer_commission;
            $comm_distribution_array['sa_amt'] = $sa_amt;
            $comm_distribution_array['sa_amt_with_tds_gst'] = $sa_amt_with_tds_gst;
            $comm_distribution_array['status'] = 'Y';
            $comm_distribution_array['created_by'] = $user_id;
            $comm_distribution_array['commission_difference'] = $remaining_commission;
            $comm_distribution_array['created_date'] = date('Y-m-d H:i:s');

            $id = $this->Utilities_model->saveVasCommissionData($comm_distribution_array);
            // $comm_distribution_array['id'] = $id;
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'TSO_recharge.log', $comm_distribution_array, '$comm_distribution_array');

            return $comm_distribution_array;
        }
    }
   
}
