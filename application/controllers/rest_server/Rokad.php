<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Project            : Rokad
// Version            : 3
// CR ID            : BG 20241 - 3.1, BG 20241 - 3.2
// Author            : Sachin Chavan
// Created on            : 01-08-2018
//
// Srl.        Date        Modified By            Why & What

//01    01-08-2018        <sachin chavan>         1. change in login API to return service asigned to agent(BG 20241 - 3.1).
//02    02-08-2018        <sachin chavan>         1. Service Master API to return services details(BG 20241 - 3.2).
//                                                                                                                                                              
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Rokad extends REST_Controller {

    function __construct() {
        parent::__construct();

        ini_set('memory_limit', '1024M');

        $this->db2 = $this->load->database('default', TRUE);

        $model_array = array('rokad/rokad_model', 'user_tpin_model', 'user_otp_model', 'tickets_model', 'users_model',
            'user_role_model', 'wallet_model', 'Country_master_model', 'state_master_model', 'city_master_model', 'login_count_model', 'retailer_service_mapping_model');

        $this->load->model($model_array);

        $data = array();
        $data['url'] = current_url();
        $data['Request_type'] = $_SERVER['REQUEST_METHOD'];
        $data['get'] = $this->input->get();
        $data['post'] = $this->post();
        $data['api_post'] = $this->post();
        $data['header'] = getallheaders();
        $data['user'] = $this->input->server('PHP_AUTH_USER');
        $data['ip'] = $this->input->ip_address();
        $this->pos_encryption_key = 'QWRTEfnfdys635';
        log_data('rest/rokad/' . date('M') . '/' . date('d-m-Y') . '/request' . '.log', $data, 'api');
    }

    /* @Author      : Aparna Chalke
     * @function    : Login
     * @param       : $mobile_no,$password
     * @detail      : Login functionality
     */

    function login_post() {

        $input = $this->post();
        $config = array(
            array(
                'field' => 'mobile_no',
                'label' => 'Mobile Number',
                'rules' => 'trim|required',
                'errors' => array('required' => 'Please enter your registered mobile number')
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required',
                'errors' => array('required' => 'Password is mandatory')
            )
        );

        if (form_validate_rules($config) == FALSE) {
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
        } else {
            $mobile_no = trim($input['mobile_no']);
            $password = trim($input['password']);
            //echo $mobile_no; echo "<br>";  echo $password; die; 
            $date = date('Y-m-d');
            $encrypt_mobile = rokad_encrypt($mobile_no, $this->pos_encryption_key);
            $where_condition = array('username' => $encrypt_mobile);
            $aUser = $this->users_model->where($where_condition)->find_all();
            // echo "<pre>"; print_r($aUser); die;
            $today = date("d-m-Y");
            if (!empty($aUser) && $aUser[0]->status == 'Y') {
                $aUser_password = $aUser[0]->password;
                $aUser_salt = $aUser[0]->salt;
                $entered_pwd = md5($aUser_salt . $password);

                if ($entered_pwd == $aUser_password) {
                    $user_id = $aUser[0]->id;
                    $role_id = $aUser[0]->role_id;
                    $roleArray = $this->user_role_model->getRolename($role_id);

                    if (!empty($roleArray)) {
                        $role_name = $roleArray[0]['role_name'];
                    }
                    $wallet_array = $this->wallet_model->check_wallet_rec($user_id);
                    $wallet_amt = '';


                    if (!empty($wallet_array)) {
                        $wallet_amt = number_format($wallet_array[0]['amt'], 2);
                    }
                    $country_name = $this->Country_master_model->get_country_by_id($aUser[0]->country);
                    $state_name = $this->state_master_model->get_state_by_id($aUser[0]->state);
                    $city_name = $this->city_master_model->get_city_by_id($aUser[0]->city);


                    if (($role_id == COMPANY_ROLE_ID) || ($role_id == MASTER_DISTRIBUTOR_ROLE_ID) || ($role_id == AREA_DISTRIBUTOR_ROLE_ID) || ($role_id == DISTRIBUTOR || ($role_id == RETAILER_ROLE_ID))
                    ) {
                        $id = array();

                        if ($role_id == COMPANY_ROLE_ID) {
                            $level = COMPANY_LEVEL;
                            $column = 'level_' . $level;
                        } elseif ($role_id == MASTER_DISTRIBUTOR_ROLE_ID) {
                            $level = MASTER_DISTRIBUTOR_LEVEL;
                            $column = 'level_' . $level;
                        } elseif ($role_id == AREA_DISTRIBUTOR_ROLE_ID) {
                            $level = AREA_DISTRIBUTOR_LEVEL;
                            $column = 'level_' . $level;
                        } elseif ($role_id == DISTRIBUTOR) {
                            $level = POS_DISTRIBUTOR_LEVEL;
                            $column = 'level_' . $level;
                        } elseif ($role_id == RETAILER_ROLE_ID) {
                            $level = RETAILOR_LEVEL;
                            $column = 'level_' . $level;
                        }

                        $result = $this->users_model->get_users_under_master($column, $user_id);

                        if (is_array($result) && count($result) > 0) {
                            $result_c = count($result);
                            $res = $this->array_imp($result);
                        } else {
                            $result_c = '0';
                            $res = '0';
                        }

                        $ticketdetails_total = $this->tickets_model->ticket_report_success_master($res);
                        //show($ticketdetails_total,1);
                        if (is_array($ticketdetails_total) && count($ticketdetails_total) > 0) {
                            $ticketdetails_total = count($ticketdetails_total);
                        } else {
                            $ticketdetails_total = '0';
                        }

                        $pending_kyc_total = $this->users_model->pending_kyc_masters($res);
                        //show($pending_kyc_total,1);
                        if (is_array($pending_kyc_total) && count($pending_kyc_total) > 0) {
                            $pending_kyc_total = ($pending_kyc_total['0']['pendingkyc_masters']);
                        } else {
                            $pending_kyc_total = '0';
                        }

                        $pending_activation = $this->users_model->pending_activation_masters($res);
                        //show($pending_activation,1);
                        if (is_array($pending_activation) && count($pending_activation) > 0) {
                            $pending_activation = ($pending_activation['0']['pending_activation_masters']);
                        } else {
                            $pending_activation = '0';
                        }

                        $commission_earned_masters = $this->tickets_model->commission_earned_masters($res, $column, $user_id);

                        //show($commission_earned_masters,1);
                        if (is_array($commission_earned_masters) && count($commission_earned_masters) > 0) {
                            $commission_earned_masters = ($commission_earned_masters['0']['final_commission_masters']);
                        } else {
                            $commission_earned_masters = '0';
                        }

                        $total_transactn_amt = $this->tickets_model->total_transactn_amt($res);
                        //show($total_transactn_amt,1);
                        if (is_array($total_transactn_amt) && count($total_transactn_amt) > 0) {
                            $total_transactn_amt = ($total_transactn_amt['0']['total_tickets_amt_master']);
                        } else {
                            $total_transactn_amt = '0';
                        }

                        $network_transactn_commission = $this->tickets_model->network_transactn_commission($res, $column, $user_id);
                        //show($network_transactn_commission, 1);
                        if (is_array($network_transactn_commission) && count($network_transactn_commission) > 0) {
                            $commission_earned_chained = ($network_transactn_commission['0']['network_commission_masters']);
                        } else {
                            $commission_earned_chained = '0';
                        }

                        $data['ticketdetails_total'] = $ticketdetails_total;
                        $data['pending_kyc_total'] = $pending_kyc_total;
                        $data['pending_activation_total'] = $pending_activation;
                        $data['commission_earned_masters'] = $commission_earned_masters;
                        $data['total_transactn_amt'] = $total_transactn_amt;
                        $data['final_commission_chained'] = $commission_earned_chained;
                    } elseif ($role_id == SUPERADMIN_ROLE_ID || $role_id == TRIMAX_ROLE_ID) {
                        $pending_users_data = $this->users_model->pending_users();
                        $kyc = $this->users_model->kyc_pending();
                        $total_tickets = $this->tickets_model->ticket_report_success();
                        $total_tickets_amt = $this->tickets_model->ticket_total_amount_success();
                        $commission_earned = $this->tickets_model->commission_earned_today();
                        $commission_earned_chained = $this->tickets_model->commission_earned_chained();

                        if (empty($pending_users_data['0']['totaluserspending'])) {
                            $pending_users_data = '0';
                        }
                        if (empty($kyc['0']['kycpending'])) {
                            $kyc = '0';
                        }
                        if (empty($total_tickets['0']['ticketsucess'])) {
                            $total_tickets = '0';
                        }
                        if (empty($total_tickets_amt['0']['total_tickets_amt'])) {
                            $total_tickets_amt = '0';
                        }
                        if (empty($commission_earned['0']['final_commission'])) {
                            $commission_earned = '0';
                        }
                        if (empty($commission_earned_chained['0']['final_commission_chained'])) {
                            $commission_earned_chained = '0';
                        }

                        $data['ticketdetails_total'] = $total_tickets;
                        $data['pending_kyc_total'] = $kyc;
                        $data['pending_activation_total'] = $pending_users_data;
                        $data['commission_earned_masters'] = $commission_earned;
                        $data['total_transactn_amt'] = $total_tickets_amt;
                        $data['final_commission_chained'] = $commission_earned_chained;

                        $data['pending_activation_total'] = (isset($data['pending_activation_total']['0']['totaluserspending'])) ? ($values['pending_activation_total']['0']['totaluserspending']) : "0";
                        $data['pending_kyc_total'] = (isset($values['pending_kyc_total']['0']['kycpending'])) ? ($values['pending_kyc_total']['0']['kycpending']) : "0";
                        $data['ticketdetails_total'] = (isset($values['ticketdetails_total']['0']['ticketsucess'])) ? ($values['ticketdetails_total']['0']['ticketsucess']) : "0";
                        $data['total_transactn_amt'] = (isset($values['total_transactn_amt']['0']['total_tickets_amt'])) ? ($values['total_transactn_amt']['0']['total_tickets_amt']) : "0";
                        $data['commission_earned_masters'] = (isset($values['commission_earned_masters']['0']['final_commission'])) ? ($values['commission_earned_masters']['0']['final_commission']) : "0";
                        $data['final_commission_chained'] = (isset($values['final_commission_chained']['0']['final_commission_chained'])) ? ($values['final_commission_chained']['0']['final_commission_chained']) : "0";
                    }

                    $aData = array();
                    $aData[0]['id'] = rokad_encrypt($user_id, $this->pos_encryption_key);
                    $aData[0]['wallet_balance'] = rokad_encrypt($wallet_amt, $this->pos_encryption_key);
                    $aData[0]['first_name'] = rokad_encrypt($aUser[0]->first_name, $this->pos_encryption_key);
                    $aData[0]['last_name'] = rokad_encrypt($aUser[0]->last_name, $this->pos_encryption_key);
                    $aData[0]['username'] = $aUser[0]->username;
                    $aData[0]['gender'] = rokad_encrypt($aUser[0]->gender, $this->pos_encryption_key);
                    $aData[0]['email'] = $aUser[0]->email;
                    $aData[0]['address1'] = rokad_encrypt($aUser[0]->address1, $this->pos_encryption_key);
                    $aData[0]['address2'] = rokad_encrypt($aUser[0]->address2, $this->pos_encryption_key);
                    $aData[0]['dob'] = $aUser[0]->dob;
                    $aData[0]['pincode'] = $aUser[0]->pincode;
                    $aData[0]['country'] = rokad_encrypt($country_name, $this->pos_encryption_key);
                    $aData[0]['state'] = rokad_encrypt($state_name, $this->pos_encryption_key);
                    $aData[0]['city'] = rokad_encrypt($city_name, $this->pos_encryption_key);
                    $aData[0]['role_id'] = rokad_encrypt($aUser[0]->role_id, $this->pos_encryption_key);
                    $aData[0]['role_name'] = rokad_encrypt($role_name, $this->pos_encryption_key);
                    $aData[0]['phone_no'] = $aUser[0]->mobile_no;
                    $aData[0]['kyc_verify_status'] = rokad_encrypt($data['pending_kyc_total'], $this->pos_encryption_key);
                    $aData[0]['status'] = rokad_encrypt($data['pending_activation_total'], $this->pos_encryption_key);
                    $aData[0]['total_transaction'] = rokad_encrypt($data['ticketdetails_total'], $this->pos_encryption_key);
                    $aData[0]['total_transaction_amt'] = rokad_encrypt($data['total_transactn_amt'], $this->pos_encryption_key);
                    $aData[0]['commission_earned_per_day'] = rokad_encrypt($data['commission_earned_masters'], $this->pos_encryption_key);
                    $aData[0]['commission_chained'] = rokad_encrypt($data['final_commission_chained'], $this->pos_encryption_key);
                    $aData[0]['agent_code'] = rokad_encrypt($aUser[0]->agent_code, $this->pos_encryption_key);
                    $aData[0]['current_date'] = rokad_encrypt($today, $this->pos_encryption_key);

                    $where_clause = array(
                        'agent_id' => $aUser[0]->id,
                        'DATE(date_added)' => date('Y-m-d')
                    );

                    $login_exists = $this->login_count_model->where($where_clause)->count_all();

                    if ($login_exists) {
                        $is_login_updated = $this->db->where($where_clause)->set('login_count', 'login_count+1', FALSE)->update('login_count');

                        if (!$is_login_updated) {
                            $this->response(array('status' => 'failed', 'msg' => 'Some error!'), 200);
                        }
                    } else {
                        $insert_data = array(
                            'agent_id' => $aUser[0]->id,
                            'agent_code' => $aUser[0]->agent_code,
                            'login_count' => 1,
                            'date_added' => date('Y-m-d H:i:s')
                        );
                        $insert_id = $this->login_count_model->insert($insert_data);

                        if (!$insert_id) {
                            $this->response(array('status' => 'failed', 'msg' => 'Some error!'), 200);
                        }
                    }

                    $login_count_detail = $this->login_count_model->where($where_clause)->find_all();

                    if (!empty($login_count_detail)) {
                        $aData[0]['login_attempts'] = rokad_encrypt($login_count_detail[0]->login_count, $this->pos_encryption_key);
                    }

                    //sachin 01-08-2018
                    $agent_services = $this->retailer_service_mapping_model->getServicesForApiByAgentID($user_id);
                    $aData[0]['services'] = rokad_encrypt($agent_services, $this->pos_encryption_key);
                    
                    log_data("rest/rokad/" . date('M') . "/" . date('d-m-Y') . "/login_" . date("d-m-Y") . "_log.log", $aData, "api");

                    $this->response(array('status' => 'success', 'data' => $aData), 200);
                } else {
                    $this->response(array('status' => 'failed', 'data' => 'You have entered wrong mobile number or password'));
                }
            } else {
                $this->response(array('status' => 'failed', 'data' => 'User details not found'));
            }
        }
    }

    function change_password_post() {

        $input = $this->post();
        $config = array(
            array(
                'field' => 'user_id',
                'label' => 'User ID',
                'rules' => 'trim|required',
                'errors' => array('required' => 'User ID should not be empty.')
            ),
            array(
                'field' => 'old_password',
                'label' => 'old_Password',
                'rules' => 'trim|required',
                'errors' => array('required' => 'Provide old Password.')
            ),
            array(
                'field' => 'new_password',
                'label' => 'new_password',
                'rules' => 'trim|required|min_length[8]',
                'errors' => array(
                    "required" => 'Provide new password',
                    'min_length' => 'Please enter 8 character New Password'
                )
            )
        );

        if (form_validate_rules($config) == FALSE) {

            $data['error'] = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'msg' => $data), 200);
        } else {
            $where_condition = array('id' => $input['user_id']);
            $user_info = $this->users_model->where($where_condition)->find_all();
            $user_info = $user_info[0];

            if ($user_info) {
                if (md5($user_info->salt . $input['old_password']) == $user_info->password) {

                    $newsalt = getRandomId();

                    //generate new salt
                    $new_pass = md5($newsalt . $input['new_password']);

                    //concat salt & password
                    $user_arr = array(
                        'password' => $new_pass,
                        'salt' => $newsalt,
                    );

                    $insert_id = $this->users_model->update($user_info->id, $user_arr);

                    if ($insert_id) {
                        $this->response(array('status' => 'success', 'msg' => 'Password Changed successfully'), 200);
                    } else {
                        $this->response(array('status' => 'failed', 'msg' => 'Some error!'), 200);
                    }
                } else {
                    $this->response(array('status' => 'failed', 'msg' => 'Old password doesn\'t match'), 200);
                }
            } else {
                $this->response(array('status' => 'failed', 'msg' => 'User not found'), 200);
            }
        }
    }

    function change_email_post() {
        $input = $this->post();
        $config = array(
            array(
                'field' => 'username',
                'label' => 'User Name',
                'rules' => 'trim|required',
                'errors' => array('required' => 'User Name is mandatory and should not be empty.')
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required',
                'errors' => array('required' => 'Provide new email id.')
            )
        );

        if (form_validate_rules($config) == FALSE) {
            $data['error'] = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'msg' => $data), 200);
        } else {
            $username = rokad_encrypt($input['username'], $this->pos_encryption_key);
            $new_email = $input['email'];
            $where_condition = array('username' => $username);
            $user_info = $this->users_model->where($where_condition)->find_all();
            $user_info = $user_info[0];
            if (!empty($user_info)) {
                $userid = $user_info->id;
                $user_arr = array(
                    'email' => rokad_encrypt($new_email, $this->pos_encryption_key)
                );

                $insert_id = $this->users_model->update($userid, $user_arr);

                if ($insert_id) {
                    $this->response(array('status' => 'success', 'msg' => 'Email Changed successfully'), 200);
                } else {
                    $this->response(array('status' => 'failed', 'msg' => 'Some error!'), 200);
                }
            } else {
                $this->response(array('status' => 'failed', 'msg' => 'User not found'), 200);
            }
        }
    }

    function change_Tpin_post() {
        $input = $this->post();
        $config = array(
            array(
                'field' => 'old_tpin',
                'label' => 'Old Tpin',
                'rules' => 'trim|required',
                'errors' => array('required' => 'Old TPIN is mandatory.')
            ),
            array(
                'field' => 'new_tpin',
                'label' => 'New Tpin',
                'rules' => 'trim|required',
                'errors' => array('required' => 'Provide new TPIN.')
            ),
            array(
                'field' => 'confirm_tpin',
                'label' => 'Confirm Tpin',
                'rules' => 'trim|required',
                'errors' => array('required' => 'Confirm TPIN is required field.')
            ),
            array(
                'field' => 'user_id',
                'label' => 'User ID',
                'rules' => 'trim|required',
                'errors' => array('required' => 'User ID is required field.')
            )
        );

        if (form_validate_rules($config) == FALSE) {
            $data['error'] = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'msg' => $data), 200);
        } else {

            if (validate_tpin($input['old_tpin']) && validate_tpin($input['new_tpin']) && validate_tpin($input['confirm_tpin'])) {

                $new_tpin = $input['new_tpin'];
                $old_tpin = $input['old_tpin'];
                $confirm_tpin = $input['confirm_tpin'];

                if ($new_tpin === $confirm_tpin) {
                    $user_data = $this->users_model->where('id', $input['user_id'])->find_all();

                    if (count($user_data) > 0) {
                        $user_data = $user_data[0];
                        $tpin = $user_data->tpin;
                        $encrypted_old_tpin = rokad_encrypt($old_tpin, $this->config->item('pos_encryption_key'));

                        $username = rokad_decrypt($user_data->username, $this->config->item('pos_encryption_key'));

                        if ($tpin == $encrypted_old_tpin) {
                            $new_tpin_encrypted = rokad_encrypt($new_tpin, $this->config->item('pos_encryption_key'));

                            $last_tpins = $this->user_tpin_model->get_last_tpins($user_id);

                            $sametpin = 'N';

                            if (sizeof($last_tpins) > 0) {
                                foreach ($last_tpins as $key => $value) {
                                    if ($value['tpin'] == $new_tpin_encrypted) {
                                        $sametpin = 'Y';
                                    }
                                }
                            }

                            if ($sametpin == 'N') {

                                $response = save_n_send_otp($input['user_id'], $username, $user_data->display_name, RESET_USERNAME_OTP, 'reset_tpin');

                                if ($response['msg_type'] == 'success') {
                                    $this->response(array('status' => 'success', 'msg' => "OTP sent successfully on user's registered mobile no."), 200);
                                } else {
                                    $this->response(array('status' => 'failed', 'msg' => "Sending OTP failed"), 200);
                                }
                            } else {
                                $this->response(array('status' => 'error', 'msg' => 'New T-PIN cannot be same as last 5 T-PIN'), 200);
                            }
                        } else {
                            $this->response(array('status' => 'failed', 'msg' => 'Entered wrong Old TPIN'), 200);
                        }
                    } else {
                        $this->response(array('status' => 'failed', 'msg' => 'User details not found.'), 200);
                    }
                } else {
                    $this->response(array('status' => 'failed', 'msg' => 'Confirm TPIN should match with New TPIN'), 200);
                }
            } else {
                $this->response(array('status' => 'failed', 'msg' => 'Invalid TPIN format'), 200);
            }
        }
    }

    function saveNewTpin_post() {

        $input = $this->post(NULL, TRUE);
        $config = array(
            array(
                'field' => 'otp',
                'label' => 'Otp',
                'rules' => 'trim|required',
                'errors' => array('required' => 'OTP value is mandatory.')
            ),
            array(
                'field' => 'new_tpin',
                'label' => 'New Tpin',
                'rules' => 'trim|required',
                'errors' => array('required' => 'Provide new TPIN.')
            ),
            array(
                'field' => 'user_id',
                'label' => 'User ID',
                'rules' => 'trim|required',
                'errors' => array('required' => 'User ID is required field.')
            ),
            array(
                'field' => 'username',
                'label' => 'User Name',
                'rules' => 'trim|required',
                'errors' => array('required' => 'User Name is required field.')
            )
        );

        $otp = trim($input['otp']);

        $user_id = $input['user_id'];
        $username = $input['username'];
        if ($otp != "") {
            if (validate_otp($otp)) {

                $response = verify_otp($user_id, $username, $otp);

                if ($response['msg_type'] == 'success') {

                    $new_tpin = $input['new_tpin'];

                    $encrypted_new_tpin = rokad_encrypt($new_tpin, $this->config->item('pos_encryption_key'));

                    $this->users_model->save_tpin($user_id, $encrypted_new_tpin);

                    $tpin_data = array("user_id" => $user_id,
                        "tpin" => $encrypted_new_tpin,
                        "created_date" => date('Y-m-d H:i:s')
                    );

                    $this->user_tpin_model->save_tpin_log($tpin_data);

                    // log otp saved
                    $tpin_log_data = array(
                        "user_id" => $user_id,
                        "tpin" => $new_tpin,
                        "encrypted_new_tpin" => $encrypted_new_tpin,
                        "created_date" => $tpin_data['created_date']
                    );

                    log_data("tpin/tpin_" . date("d-m-Y") . "_log.log", $tpin_log_data, 'tpin_log_data array');

                    $this->response(array('status' => 'success', 'msg' => 'T-PIN Changed Successfully'), 200);
                } else {
                    $this->response(array('status' => 'failed', 'msg' => $response['msg']), 200);
                }
            } else { // invalid user or mobile no
                $this->response(array('status' => 'failed', 'msg' => 'Enter valid 6 digit otp number'), 200);
            }
        } else {
            $this->response(array('status' => 'failed', 'msg' => 'All fields are required'), 200);
        }
    }

    public function array_imp($result) {

        foreach ($result as $value) {
            $id[] = $value['id'];
        }
        //$id =  implode(',', $id);

        return $id;
    }

    public function forgot_password_post() {
        $config = array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'Provide email id',
                )
            ),
        );

        if (form_validate_rules($config) == FALSE) {
            $data['error'] = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'data' => $data), 200);
        } else {

            $encrypt_email = rokad_encrypt($this->post('email'), $this->pos_encryption_key);
            $where_condition = array('email' => $encrypt_email);
            $user_info = $this->users_model->where($where_condition)->find_all();

            if ($user_info) {
                $salt = getRandomId();
                $random_str = getRandomId(6);
                $password = md5($salt . $random_str);
                $this->users_model->update($user_info[0]->id, array('salt' => $salt, 'password' => $password));
                $this->response(array('status' => 'success', 'msg' => 'Password has been updated.', 'new pass' => $random_str), 200);
            } else {
                $this->response(array('status' => 'failed', 'msg' => 'Email id not found, Please check it again', 'new pass' => ''), 200);
            }
        }
    }

    
    
    /**
    * @description - To get service details
    * @param service ID
    * @return json - services details
    * @added by Sachin Chavan on 02-08-2018 For BG 20241 - 3.2(Services API)
    */
    public function service_master_post() {
        $input = $this->post();
        $service_id = isset($input['service_id']) ? $input['service_id'] : '';
        $serviceArr = $this->retailer_service_mapping_model->getServicesDetails($service_id);
        $this->response(array('data' => $serviceArr, 'status' => 'success'), 200);
    }

}

?>
