<?php


defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Master_data extends REST_Controller {
    protected $data;
    protected $sSecretKey = 'fdac7124c6a2b3f6';
    protected $vendor_Agent = '';
    

function __construct() 
   {
   
            parent::__construct();
    $this->load->model(array('city_master_model','state_master_model','booking_agent_model','country_master_model'));
    
   }
   
   function country_master_post()
    {
       $data['country_master']= $this->country_master_model
                                  
                                  ->as_array()
                                  ->find_all();
            $this->response(array('data' => $data, 'status' => 'success'),200);
       
    }
   
   function   city_master_post()
     {
       $data['city_master']= $this->city_master_model->column('sm.stState,city_master.stCity,city_master.intStateId,city_master.intCityId,cm.stCountryName')
                                  ->join('state_master sm','city_master.intStateId=sm.intStateId','inner')
                                  ->join('country_master cm','city_master.intCountryId=cm.intCountryId')
                                  ->as_array()
                                  ->find_all();
       
       
       
       
       $this->response(array('data' => $data, 'status' => 'success'),200);
     }
     
     function state_master_post()
        {
            $data['state_master']= $this->state_master_model->column('state_master.intStateId,state_master.stState,cm.intCountryId,cm.stCountryName')
                                    ->join('country_master cm','state_master.intCountryId=cm.intCountryId')
                                    ->as_array()
                                    ->find_all();
            $this->response(array('data' => $data, 'status' => 'success'),200);
        }
     
     function docs_cat_master_post()
         {
            $data=array();
            $res  =  $this->booking_agent_model->doc_type();
            $data['document_category']= $res;   
            
             
              $this->response(array('data' => $data, 'status' => 'success'),200);
       }
       function docs_name_master_post()
         {
          $res  =  $this->booking_agent_model->doc_type();
           foreach($res as $key=>$val)
              {
                 $data['document_name'][$val['category_name']]  =  $this->booking_agent_model->doc_name($val['doc_cat_id']);
             
              } 
           $this->response(array('data' => $data, 'status' => 'success'),200);
         }
     
}   

?>
