<?php

//Created by Sonali Kamble on 20-09-2018

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Search_bus extends REST_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model(array('Print_Ticket_model','Utilities_model'));
        $this->load->helper('various_helper');

        
    }

    public function bus_services_post() {
    	
       	$data = array();
        $input = $this->post();             

        if ($input['from'] == "" || $input['to'] == "" || $input['date'] == "") {
            $this->response(array('status' => 'Failed', 'data' => "", 'msg' => 'Please provide proper inputs.'), 200);
        } else { 
	        
	        $curl_url ="services";
        	$curlResult = curlForPostData_BOSRest($curl_url,$input);
			log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'bus_services_input_op.log', $curlResult, 'Bus Services_input_op');
	               
	        $this->response(array('status' => 'Success', 'data' => $curlResult), 200);
        
        }
    }

    public function get_service_stop_details_post()
    {
    	$data = array();
        $input = $this->post();
          
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'get_service_stop_details_input_rest.log', $input, 'Service Stop Details input rest');          

        if ($input['provider_id'] == "" || $input['service_id'] == "" || $input['date'] == "") {
            $this->response(array('status' => 'Failed', 'data' => "", 'msg' => 'Please provide proper inputs.'), 200);

        } else { 
        	$curl_url ="get_service_stop_details";
        	$curlResult = curlForPostData_BOSRest($curl_url,$input);       	 

	        
			//log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'bus_services_input_op.log', $sessionData['username'], 'Bus Services_input_op');
	               
	        $this->response(array('status' => 'Success', 'data' => $curlResult), 200);

   		}

	}

	public function get_seat_layout_post()
	{

		$data = array();
        $input = $this->post();
          
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'get_seat_layout.log', $input, 'get seat layout');          

        if ($input['provider_id'] == "" || $input['from'] == "" || $input['to'] == "" || $input['date'] == "" || $input['trip_id'] == "" || $input['service_id'] == "" || $input['inventory_type'] == "") {
            $this->response(array('status' => 'Failed', 'data' => "", 'msg' => 'Please provide proper inputs.'), 200);

        } else { 
        	$curl_url ="layout";
        	$curlResult = curlForPostData_BOSRest($curl_url,$input); 
	               
	        $this->response(array('status' => 'Success', 'data' => $curlResult), 200);

   		}

	}

	public function temp_booking_post()
	{
		$data = array();
        $input = $this->post();
          

	    if(empty($input))
	    {
	    	$this->response(array('status' => 'Failed', 'data' => "", 'msg' => 'Please provide proper inputs.',$input), 200);
	    }else
	    {
	    	$curl_url ="block_seat";
        	$curlResult = curlForPostData_BOSRest($curl_url,$input); 
	               
	        $this->response(array('status' => 'Success', 'data' => $curlResult), 200);
	    }
	}

	function confirm_booking_post()
	{
		$data = array();
        $input = $this->post();
          
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'confirm_booking_rest.log', $input, 'confirm booking rest');

         if ($input['block_key'] == "" || $input['unique_id'] == "" || $input['seat_count'] == "" ) {
            $this->response(array('status' => 'Failed', 'data' => "", 'msg' => 'Please provide proper inputs.'), 200);

        } else { 
        	$curl_url ="confirm_booking";
        	$curlResult = curlForPostData_BOSRest($curl_url,$input); 
	               
	        $this->response(array('status' => 'Success', 'data' => $curlResult), 200);

   		}

	}

	function is_ticket_cancel_post()
	{
		$data = array();
        $input = $this->post();
          
        //log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'is_ticket_cancel_rest.log', $input, 'is ticket cancel rest');

         if ($input['bos_key'] == "") {
            $this->response(array('status' => 'Failed', 'data' => "", 'msg' => 'Please provide proper inputs.'), 200);

        } else { 
        	$curl_url ="is_ticket_cancelable";
        	$curlResult = curlForPostData_BOSRest($curl_url,$input); 
	               
	        $this->response(array('status' => 'Success', 'data' => $curlResult), 200);

   		}

	}

	function cancel_ticket_post()
	{
		$data = array();
        $input = $this->post();
          
        //log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'cancel_ticket_rest.log', $input, 'cancel ticket rest');

         if ($input['bos_key'] == "") {
            $this->response(array('status' => 'Failed', 'data' => "", 'msg' => 'Please provide proper inputs.'), 200);

        } else { 
        	$curl_url ="cancel_ticket";
        	$curlResult = curlForPostData_BOSRest($curl_url,$input); 
	               
	        $this->response(array('status' => 'Success', 'data' => $curlResult), 200);

   		}

	}

	function print_ticket_post()
	{
		$data = array();
        $input = $this->post();
          
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'print_ticket_rest_ip.log', $input, 'print ticket  ip rest');

         if ($input['refrence_no'] == "" ) {
            $this->response(array('status' => 'Failed', 'data' => "", 'msg' => 'Please provide proper inputs.'), 200);

        } else { 
        	$aRes = $this->Print_Ticket_model->getTicketsDetails($input);
        	
        	log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'print_ticket_rest.log', $aRes, 'print ticket rest');
            $this->response(array('status' => 'Success', 'data' => $aRes), 200);


   		}

	}

    function calc_commission_post()
    {

        $input = $this->post();
        
        if (!empty($input)) {

            /*$config = array(
                array('field' => 'recharge_type', 'label' => 'Recharge Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter recharge type.')),
                array('field' => 'mobile_operator', 'label' => 'Mobile Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter mobile operator.')),
                array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.')),
                array('field' => 'recharge_amount', 'label' => 'Recharge Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter recharge amount.'))
            );*/

            $user_id = $input['user_id'];
            $transaction_no = $input['transaction_no'];           
            $base_fare = $input['base_fare'];

            $check_user_wallet = $this->Utilities_model->getUserWalletDetails($user_id);

           // log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'check_user_wallet.log', $check_user_wallet, 'user wallet');

            if ($check_user_wallet[0]['amt'] > $base_fare) {

                $commissionResult = $this->serviceRetailerCommissionDistribution($input, $transaction_no);

                $updated_wallet_amt = $check_user_wallet[0]['amt'] - $commissionResult['trimax_final_amt'];

                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'calc_commission_ip_rest.log', $updated_wallet_amt.'-'.$check_user_wallet[0]['amt'].'-'.$commissionResult['trimax_final_amt'], 'calc commssion ip rest');  
                
                $update_wallet = $this->Utilities_model->updateUserWallet($user_id, $updated_wallet_amt);

                /*$transaction_data = array(
                                'type' => $type,
                                'operator' => $operator,
                                'recharge_on' => $mobile_number,
                                'recharge_from' => $recharge_from,
                                'last_transaction_amount' => $commissionResult['trimax_final_amt'],
                                'transaction_id' => $transaction_no,
                                'created_by' => $commissionResult['created_by']
                            );

                $this->Utilities_model->saveTransactionData($transaction_data);*/
                $this->response(array('status' => 'Success', 'data' => ''), 200);
            }
            else
            {
                $this->response(array('status' => 'Failed', 'data' => '', 'msg' => 'Insufficient balance'), 200);
            }
        }
    }

    public function serviceRetailerCommissionDistribution($input = "", $trans_ref_no = "") {

        $user_id = $input['user_id'];
        $service_id = BOS_SERVICE_ID;

        $getRetailerServicesCommission = $this->Utilities_model->getRetailerServicesCommission($user_id, $service_id);

        if ($getRetailerServicesCommission) {

            $agent_id = $getRetailerServicesCommission[0]['user_id'];
            $agent_code = $getRetailerServicesCommission[0]['agent_code'];
            $distributor_id = $getRetailerServicesCommission[0]['level_5'];
            $Area_dis_id = $getRetailerServicesCommission[0]['level_4'];
            $master_dis_id = $getRetailerServicesCommission[0]['level_3'];
            $company_id = $getRetailerServicesCommission[0]['level_2'];
            $trimax_id = $getRetailerServicesCommission[0]['level_1'];
            $service_id = $getRetailerServicesCommission[0]['service_id'];
            $commission_id = $getRetailerServicesCommission[0]['commission_id'];

            $values = json_decode($getRetailerServicesCommission[0]['values']);
            
            $trimax_commission = '0';
            $company_commission = '0';
            $md_commission = '0';
            $ad_commission = '0';
            $dist_commission = '0';
            $retailer_commission = '0';

            if (!empty($values)) {

                $trimax_commission = round($values->Trimax, 2);
                $company_commission = round($values->Rokad, 2);
                $md_commission = round($values->MD, 2);
                $ad_commission = round($values->AD, 2);
                $dist_commission = round($values->Distributor, 2);
                $retailer_commission = round($values->Retailer, 2);

                $base_fare = $input['base_fare']; //transaction_amt
                $commission_percent = $input['commssion_percent']; //trimax_comm_percent                                                       
                $trimax_cal = calculateGstTds($base_fare, $commission_percent);
                $earnings = $trimax_cal['earnings']; //trimax_earnings
                $gst = $trimax_cal['gst']; //trimax_gst                            
                $tds = $trimax_cal['tds']; //trimax_tds
                $final_amount = $trimax_cal['final_amount']; //                trimax_final_amt                                                        

                $rokad_trimax_cal = calculateGstTds($earnings, $trimax_commission);
                $rokad_trimax_amt = $rokad_trimax_cal['earnings']; //rokad_trimax_amt                             
                $rokad_trimax_cal_with_tds_gst = calculateGstTds($final_amount, $trimax_commission);
                $rokad_trimax_amt_with_tds_gst = $rokad_trimax_cal_with_tds_gst['earnings']; //rokad_trimax_amt_with_tds_gst                            

                $company_cal = calculateGstTds($earnings, $company_commission);
                $company_amt = $company_cal['earnings']; //company_amt                             
                $company_cal_with_tds_gst = calculateGstTds($final_amount, $company_commission);
                $company_amt_with_tds_gst = $company_cal_with_tds_gst['earnings']; //company_amt_with_tds_gst                        

                $rd_cal = calculateGstTds($earnings, $md_commission);
                $rd_amt = $rd_cal['earnings']; //rd_amt                             
                $rd_cal_with_tds_gst = calculateGstTds($final_amount, $md_commission);
                $rd_amt_with_tds_gst = $rd_cal_with_tds_gst['earnings']; //rd_amt_with_tds_gst

                $dd_cal = calculateGstTds($earnings, $ad_commission);
                $dd_amt = $dd_cal['earnings']; //dd_amt                             
                $dd_cal_with_tds_gst = calculateGstTds($final_amount, $ad_commission);
                $dd_amt_with_tds_gst = $dd_cal_with_tds_gst['earnings']; //dd_amt_with_tds_gst

                $ex_cal = calculateGstTds($earnings, $dist_commission);
                $ex_amt = $ex_cal['earnings']; //ex_amt                             
                $ex_cal_with_tds_gst = calculateGstTds($final_amount, $dist_commission);
                $ex_amt_with_tds_gst = $ex_cal_with_tds_gst['earnings']; //ex_amt_with_tds_gst

                $sa_cal = calculateGstTds($earnings, $retailer_commission);
                $sa_amt = $sa_cal['earnings']; //sa_amt                             
                $sa_cal_with_tds_gst = calculateGstTds($final_amount, $retailer_commission);
                $sa_amt_with_tds_gst = $sa_cal_with_tds_gst['earnings']; //sa_amt_with_tds_gst              
            }

            $comm_distribution_array['user_id'] = $agent_id;
            $comm_distribution_array['service_id'] = $service_id;
            $comm_distribution_array['commission_id'] = $commission_id;
            $comm_distribution_array['transaction_no'] = $trans_ref_no;
            $comm_distribution_array['transaction_amt'] = $base_fare;
            $comm_distribution_array['service_comm_percent'] = $input['agent_commission'];
            $comm_distribution_array['trimax_comm_percent'] = $input['commssion_percent'];
            $comm_distribution_array['gst_percentage'] = GST_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['tds_percentage'] = TDS_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['trimax_earnings'] = $earnings;
            $comm_distribution_array['trimax_gst'] = $gst;
            $comm_distribution_array['trimax_tds'] = $tds;
            $comm_distribution_array['trimax_final_amt'] = $final_amount;
            $comm_distribution_array['rokad_trimax_per'] = $values->Trimax;
            $comm_distribution_array['rokad_trimax_amt'] = $rokad_trimax_amt;
            $comm_distribution_array['rokad_trimax_amt_with_tds_gst'] = $rokad_trimax_amt_with_tds_gst;
            $comm_distribution_array['company_per'] = $values->Rokad;
            $comm_distribution_array['company_amt'] = $company_amt;
            $comm_distribution_array['company_amt_with_tds_gst'] = $company_amt_with_tds_gst;
            $comm_distribution_array['rd_per'] = $values->MD;
            $comm_distribution_array['rd_amt'] = $rd_amt;
            $comm_distribution_array['rd_amt_with_tds_gst'] = $rd_amt_with_tds_gst;
            $comm_distribution_array['dd_per'] = $values->AD;
            $comm_distribution_array['dd_amt'] = $dd_amt;
            $comm_distribution_array['dd_amt_with_tds_gst'] = $dd_amt_with_tds_gst;
            $comm_distribution_array['ex_per'] = $values->Distributor;
            $comm_distribution_array['ex_amt'] = $ex_amt;
            $comm_distribution_array['ex_amt_with_tds_gst'] = $ex_amt_with_tds_gst;
            $comm_distribution_array['sa_per'] = $values->Retailer;
            $comm_distribution_array['sa_amt'] = $sa_amt;
            $comm_distribution_array['sa_amt_with_tds_gst'] = $sa_amt_with_tds_gst;
            $comm_distribution_array['status'] = 'Y';
            $comm_distribution_array['created_by'] = $user_id;
            $comm_distribution_array['created_date'] = date('Y-m-d H:i:s');

            $id = $this->Utilities_model->saveVasCommissionData($comm_distribution_array);
            $comm_distribution_array['id'] = $id;
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'tata_docomo_special.log', $comm_distribution_array, 'Commission Distribution Response');

            return $comm_distribution_array;
        }
    }

	

}