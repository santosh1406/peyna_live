<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Utilities_bbps extends REST_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model(array('Utilities_bbps_model','Utilities_model', 'wallet_trans_model', 'common_model'));
        $this->load->helper('utilities_bbps_helper','epaylater_helper');
        $this->header = array(
            'Content-Type: application/xml'
        );
    }

    public function bbps_serviceRetailerCommissionDistribution($input = "", $trans_ref_no = "", $created_by = "") {
        
        $user_id = $input['user_id'];
        $type = $input['type'];
        $service_id = BBPS_UTILITIES_SERVICE_ID;
        $input['recharge_amount'] = $input['recharge_amount'];
            
         if($input['recharge_amount'] >=1 && $input['recharge_amount'] <= 1000){
                $range = '1-1000';
         }elseif($input['recharge_amount'] >= 1001 && $input['recharge_amount'] <= 2000){
                $range = '1001-2000';
         }elseif($input['recharge_amount'] >= 2001){
                $range = '2001-above';
        }
        
        if($type == 'Electricity'){
            $servicecode = "Electricity ".$range;
            $servicename = "Electricity";
            
        }else if($type == 'Gas'){
             $servicecode = "Gas ".$range;
             $servicename = "Gas";
             
        }else if($type == 'Water'){
             $servicecode = "Water ".$range;
             $servicename = "Water";
             
        }else{
            $servicecode = "Telecom Postpaid ".$range;
            $servicename = "Telecom Postpaid";
        }
        
        $subService = $this->Utilities_bbps_model->getSubservices($range, $servicename, $servicecode); 
        
        $commission = $this->Utilities_bbps_model->getServicesCommission($range, $service_id, $subService[0]['id'], $type);
        //show($commission,1);
                 
        if($commission){
            $row = $commission[0];
            
            $getRetailerServicesCommission = $this->Utilities_bbps_model->bbps_getRetailerServicesCommission($user_id, $row->service_id, $row->sub_service_id,$row->created_by);
           //show($getRetailerServicesCommission);
        if ($getRetailerServicesCommission) {

            $agent_id = $getRetailerServicesCommission[0]['user_id'];
            $agent_code = $getRetailerServicesCommission[0]['agent_code'];
            $distributor_id = $getRetailerServicesCommission[0]['level_5'];
            $Area_dis_id = $getRetailerServicesCommission[0]['level_4'];
            $master_dis_id = $getRetailerServicesCommission[0]['level_3'];
            $company_id = $getRetailerServicesCommission[0]['level_2'];
            $trimax_id = $getRetailerServicesCommission[0]['level_1'];
            $service_id = $getRetailerServicesCommission[0]['service_id'];
            $commission_id = $getRetailerServicesCommission[0]['commission_id'];
            $total_commission = $getRetailerServicesCommission[0]['total_commission'];

            $values[0] = json_decode($getRetailerServicesCommission[0]['values']);

            $trimax_commission = '0';
            $company_commission = '0';
            $md_commission = '0';
            $ad_commission = '0';
            $dist_commission = '0';
            $retailer_commission = '0';
            $with_gst = $without_gst = 0;
            
            if (!empty($values)) {

                $trimax_commission = isset($values[0]->Trimax) ? round($values[0]->Trimax, 2) : '0.00';
                $company_commission = isset($values[0]->Rokad) ? round($values[0]->Rokad, 2) : '0.00';
                $md_commission = isset($values[0]->MD) ? round($values[0]->MD, 2) : '0.00';
                $ad_commission = isset($values[0]->AD) ? round($values[0]->AD, 2) : '0.00';
                $dist_commission = isset($values[0]->Distributor) ? round($values[0]->Distributor, 2) : '0.00';
                $retailer_commission = isset($values[0]->Retailer) ? round($values[0]->Retailer, 2) : '0.00';                                   
                
                $commission_percent = !empty($total_commission) ? $total_commission : '0';
                
                $recharge_amount = $input['recharge_amount']; //transaction_amt
              
                $customer_charge = $getRetailerServicesCommission[0]['customer_charge'];
                
                $trimax_cal = calculateGstTds($customer_charge, $commission_percent='0');
                
                //$earnings = $trimax_cal['earnings']; //trimax_earnings 
                
                $earnings = $customer_charge; //trimax_earnings
                
               
                $gst = $trimax_cal['gst']; //trimax_gst                            
                $tds = $trimax_cal['tds']; //trimax_tds
                $final_amount = $trimax_cal['final_amount']; //trimax_final_amt                                                        

                $rokad_trimax_cal = calculateGstTds($customer_charge, $trimax_commission);
                $rokad_trimax_amt = $rokad_trimax_cal['earnings']; //rokad_trimax_amt                             
                $rokad_trimax_cal_with_tds_gst = calculateGstTds($final_amount, $trimax_commission);
                $rokad_trimax_amt_with_tds_gst = $rokad_trimax_cal_with_tds_gst['earnings']; //rokad_trimax_amt_with_tds_gst                            

                $company_cal = calculateGstTds($customer_charge, $company_commission);
                $company_amt = $company_cal['earnings']; //company_amt                             
                $company_cal_with_tds_gst = calculateGstTds($final_amount, $company_commission);
                $company_amt_with_tds_gst = $company_cal_with_tds_gst['earnings']; //company_amt_with_tds_gst                        

                $rd_cal = calculateGstTds($customer_charge, $md_commission);
                $rd_amt = $rd_cal['earnings']; //rd_amt                             
                $rd_cal_with_tds_gst = calculateGstTds($final_amount, $md_commission);
                $rd_amt_with_tds_gst = $rd_cal_with_tds_gst['earnings']; //rd_amt_with_tds_gst

                $dd_cal = calculateGstTds($customer_charge, $ad_commission);
                $dd_amt = $dd_cal['earnings']; //dd_amt                             
                $dd_cal_with_tds_gst = calculateGstTds($final_amount, $ad_commission);
                $dd_amt_with_tds_gst = $dd_cal_with_tds_gst['earnings']; //dd_amt_with_tds_gst

                $ex_cal = calculateGstTds($customer_charge, $dist_commission);
                $ex_amt = $ex_cal['earnings']; //ex_amt                             
                $ex_cal_with_tds_gst = calculateGstTds($final_amount, $dist_commission);
                $ex_amt_with_tds_gst = $ex_cal_with_tds_gst['earnings']; //ex_amt_with_tds_gst

                $sa_cal = calculateGstTds($customer_charge, $retailer_commission);
                $sa_amt = $sa_cal['earnings']; //sa_amt                             
                $sa_cal_with_tds_gst = calculateGstTds($final_amount, $retailer_commission);
                $sa_amt_with_tds_gst = $sa_cal_with_tds_gst['earnings']; //sa_amt_with_tds_gst              
            }

            $comm_distribution_array['user_id'] = $agent_id;
            $comm_distribution_array['service_id'] = $service_id;
            $comm_distribution_array['commission_id'] = $commission_id;
            $comm_distribution_array['transaction_no'] = $trans_ref_no;
            $comm_distribution_array['transaction_amt'] = $recharge_amount;
            $comm_distribution_array['customer_charge'] = $customer_charge;
            $comm_distribution_array['service_comm_percent'] = SERVICE_COMMISSION;
            $comm_distribution_array['trimax_comm_percent'] = $total_commission;
            $comm_distribution_array['gst_percentage'] = GST_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['tds_percentage'] = TDS_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['trimax_earnings'] = $earnings;
            $comm_distribution_array['trimax_gst'] = $gst;
            $comm_distribution_array['trimax_tds'] = $tds;
            $comm_distribution_array['trimax_final_amt'] = $final_amount;
            $comm_distribution_array['rokad_trimax_per'] = $trimax_commission;
            $comm_distribution_array['rokad_trimax_amt'] = $rokad_trimax_amt;
            $comm_distribution_array['rokad_trimax_amt_with_tds_gst'] = $rokad_trimax_amt_with_tds_gst;
            $comm_distribution_array['company_per'] = $company_commission;
            $comm_distribution_array['company_amt'] = $company_amt;
            $comm_distribution_array['company_amt_with_tds_gst'] = $company_amt_with_tds_gst;
            $comm_distribution_array['rd_per'] = $md_commission;
            $comm_distribution_array['rd_amt'] = $rd_amt;
            $comm_distribution_array['rd_amt_with_tds_gst'] = $rd_amt_with_tds_gst;
            $comm_distribution_array['dd_per'] = $ad_commission;
            $comm_distribution_array['dd_amt'] = $dd_amt;
            $comm_distribution_array['dd_amt_with_tds_gst'] = $dd_amt_with_tds_gst;
            $comm_distribution_array['ex_per'] = $dist_commission;
            $comm_distribution_array['ex_amt'] = $ex_amt;
            $comm_distribution_array['ex_amt_with_tds_gst'] = $ex_amt_with_tds_gst;
            $comm_distribution_array['sa_per'] = $retailer_commission;
            $comm_distribution_array['sa_amt'] = $sa_amt;
            $comm_distribution_array['sa_amt_with_tds_gst'] = $sa_amt_with_tds_gst;
            $comm_distribution_array['status'] = 'Y';
            $comm_distribution_array['created_by'] = $user_id;
            $comm_distribution_array['created_date'] = date('Y-m-d H:i:s');
            
            $id = $this->Utilities_bbps_model->saveVasCommissionData($comm_distribution_array);
            // $comm_distribution_array['id'] = $id;
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'saveVasCommissionData.log', $comm_distribution_array, 'Commission Distribution Response');
            return $comm_distribution_array;
        }
      }else{
           log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'bbps_getuserbill.log', $commission, 'Commission error');
           $this->response(array('status' => 'Failed', 'data' => $commission, 'msg' => 'Error'), 200);
      }
    }

    public function bbpsOperators_post() {
        $data = array();
        $input = $this->post();

        if ($input['term'] == "" || $input['type'] == "") {
            $this->response(array('status' => 'Failed', 'data' => "", 'msg' => 'Please provide proper inputs.'), 200);
        } else {
            $aRes = $this->Utilities_bbps_model->getbbpsMobileOperators($input['term'], $input['type']);
            $this->response(array('status' => 'Success', 'data' => $aRes), 200);
        }
    }

    public function get_bill_post() {
        $data = array();
        $input = $this->post();
        
        //show($input,1);

        $error_messages = json_decode(file_get_contents('messages.json'), true);
        if (!empty($input)) {
            $config = $this->Utilities_bbps_model->get_validation($input['type'], $input['operator_id']);
            if (form_validate_rules($config) == FALSE) {
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {

                $user_id = $input['user_id'];
                $recharge_from = $input['recharge_from'];
                $operator = $input['operator'];
                $operator_id = $input['operator_id']; //"BSNLMOB00NAT01"; 
                $type = $input['type'];
                $number = $input['number'];
                $account_number = $input['account_number'];
                $mobile_number = $input['mobile_number'];
                $email = $input['email'];
                $city = $input['city'];
                $consumer_email = $input['useremail'];
                $consumer_mobile = $input['c_mob_number'];
                $authenticator = $input['authenticator'];
                $amount = $input['amount'];
                $bbps = $input['redirecttype'];
                $submitval = $input['submitval'];
                
                
                $error = false;
                $err_msg = '';

                //Generate biller tags as per the operator
                $getBillerTags = $this->Utilities_bbps_model->operatorBillerTags($operator_id, $type); //print_r($getBillerTags); die;
                
                $billertags = getbillertags($getBillerTags[0]['tag_name'], $type, $operator_id, $number, $mobile_number, $email, $consumer_email, $consumer_mobile, $amount, $account_number, $city);
                
                //API to fetch customer bill. For adhoc or quick pay no need to fetch the bill direct submit amount to bbps for payment
                $checkAdhocRequest = $this->Utilities_bbps_model->operatorDetailsByID($operator_id, $type);
                
                //API to fetch biller for the selected category
                $curl_url = BILLERLIST.$type.'&sourceId={'.SOURCEID.'}';
                
                $response = curlPostData($curl_url, '', $this->header);

                $xml_data = simplexml_load_string($response);
                $result = json_encode($xml_data);
                $response = json_decode($result, true);

                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'bbps_reposnse.log', $response, 'billerlist api response');                
                
                foreach ($response['biller'] as $key => $value) {
                    
                    if($value['id']==$input['operator_id']){
                       $adhoc_value = strtolower($value['acceptsAdhoc']);
                    } 
                }
                
               
                if (isset($adhoc_value) && !empty($adhoc_value) && $adhoc_value == 'true') {
                    //Directly submit payment request
//                        $recharge_response_check = recharge_with_bbps($input, $getOperatorDetails);
                    goto a;
                } else {
                        
                        $url = '<ns2:FetchRequest xmlns:ns2="http://bbps.rssoftware.co.in/schema">'.
                        '<origInst>IT01</origInst><customer mobile="9892650971"/><agent id="IT01IT02AGT000000006">'.
                        '<Device><Tag name="INITIATING_CHANNEL" value="AGT"/><Tag name="POSTAL_CODE" value="400013"/>'.
                        '<Tag name="TERMINAL_ID" value="1111111111"/><Tag name="MOBILE" value="9920980050"/>'.
                        '<Tag name="GEOCODE" value="19.2303,72.8264"/></Device></agent>' . $billertags . '<riskScores>'.
                        '<Score provider="IT01" type="TXNRISK" value="030"/></riskScores></ns2:FetchRequest>';
                        
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'bbps_getuserbill.log', $url, 'fetch url');
                        
                        $curl_url = encode_url(BASEURL, $url);
                        $response = curlPostData($curl_url, '', $this->header);
                        
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'bbps_getuserbill.log', $response, '0...fetch curl response from bbps');
                        $xml_data = simplexml_load_string($response);
                        $result = json_encode($xml_data);
                        $response = json_decode($result, true);

                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'bbps_getuserbill.log', $response, 'fetchbill api response'); 


    
//                        $recharge_amount = round($bill_amount[0]);
//                        
//                        $recharge_amount = round($bill_amount[0]);
                       
                        $input['dueDate'] = $response['billerResponse']['@attributes']['dueDate'];
                        $input['billDate'] = $response['billerResponse']['@attributes']['billDate'];
                        $input['billNumber'] = $response['billerResponse']['@attributes']['billNumber'];
                        $input['customerName'] = $response['billerResponse']['@attributes']['customerName'];
                        $input['billPeriod'] = $response['billerResponse']['@attributes']['billPeriod'];
                        $input['refID'] = $response['refId'];
                        $input['account_number'] = $number;
                        
                        $early_payment_value =  $response['additionalInfo']['Tag'][0]['@attributes']['value'];
                        $due_date = $response['billerResponse']['@attributes']['dueDate'];

                        if(date('Y-m-d')<=$early_payment_value ){
                           
                           $amount_value = $response['billerResponse']['tag'][1]['@attributes']['value'];
                           $amount_name = $response['billerResponse']['tag'][1]['@attributes']['name'];
                           
                        }elseif(date('Y-m-d') > $early_payment_value && date('Y-m-d') <= $due_date ){
                            
                            $amount_value = $response['billerResponse']['tag'][0]['@attributes']['value'];
                            $amount_name =  $response['billerResponse']['tag'][0]['@attributes']['name'];
                            
                        }else{
                            
                            $amount_value = $response['billerResponse']['tag'][2]['@attributes']['value'];
                            $amount_name = $response['billerResponse']['tag'][0]['@attributes']['name'];
                        }
                        
                        $bill_amount = explode('=',$amount_value);
                        
                        $amount = round($bill_amount[1]);
                        
                        $recharge_amount = $amount;
                        
                        $customer_charge = $this->Utilities_bbps_model->getCustomerCharge($amount);
                        $customer_charge = !empty($customer_charge) ? $customer_charge : 0;
                       
//                        $result['CUSTOMER_CHARGE'] = $customer_charge;
//                        $result['TOTAL_AMOUNT'] = $amount+$customer_charge; 
                        
                        $bill_amount = explode('|',$amount_name);
                        $bill_amt = $this->convert_into_paise(round($bill_amount[0])).'|'.$bill_amount[1];

                        $input['recharge_amount'] = $recharge_amount;
                        $input['bill_amount'] = $bill_amt;
                        

                        //show($input,1);
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'bbps_getuserbill.log', $input, '1...fetch user bill response from bbps');

                        
                }
                
                
                //Check response validation
                if (isset($response['errorMessages']) && !empty($response['errorMessages'])) {
                    $this->response(array('status' => 'Failed', 'data' => $response['errorMessages'], 'msg' => 'Error : ' . $response['errorMessages']['errorDtl']), 200);
                } else {
                    
                    $getOperatorDetails = $this->Utilities_bbps_model->operatorDetailsByID($operator_id, $type);
                    $result = array();

                    if (isset($submitval) && !empty($submitval) && $submitval == 1 && $adhoc_value == 'false') {
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'bbps_getuserbill.log', $input, 'sonali');
                        // First call to display bill details on screen
                        //$result['AMOUNT'] = $recharge_amount;
                        $result['BILL_DUE_DATE'] = $response['billerResponse']['@attributes']['dueDate'];
                        $result['BILL_DATE'] = $response['billerResponse']['@attributes']['billDate'];
                        $result['BILL_NUMBER'] = $response['billerResponse']['@attributes']['billNumber'];
                        $result['CUSTOMER_NAME'] = $response['billerResponse']['@attributes']['customerName'];
                        $result['REF_ID']= $response['refId'];
                        //$result['TAG_VALUES'] = $response['billerResponse']['tag'];
                        
                        $early_payment_value =  $response['additionalInfo']['Tag'][0]['@attributes']['value'];
                        $due_date = $response['billerResponse']['@attributes']['dueDate'];

                        if(date('Y-m-d')<=$early_payment_value ){
                           
                           $result['AMOUNT']= $response['billerResponse']['tag'][1]['@attributes']['value'];
                           $result['AMOUNT_OPTION']= $response['billerResponse']['tag'][1]['@attributes']['name'];
                           
                        }elseif(date('Y-m-d') > $early_payment_value && date('Y-m-d') <= $due_date ){
                            
                            $result['AMOUNT']= $response['billerResponse']['tag'][0]['@attributes']['value'];
                            $result['AMOUNT_OPTION']= $response['billerResponse']['tag'][0]['@attributes']['name'];
                            
                        }else{
                            
                            $result['AMOUNT']= $response['billerResponse']['tag'][2]['@attributes']['value'];
                            $result['AMOUNT_OPTION']= $response['billerResponse']['tag'][0]['@attributes']['name'];
                        }
                        
                        $bill_amount = explode('=',$result['AMOUNT']);
                        
                        $amount = round($bill_amount[1]);
                        
                        $recharge_amount = $amount;
                        
                        $customer_charge = $this->Utilities_bbps_model->getCustomerCharge($amount);
                        $customer_charge = !empty($customer_charge) ? $customer_charge : 0;
                       
                        $result['CUSTOMER_CHARGE'] = $customer_charge;
                        $result['TOTAL_AMOUNT'] = $amount+$customer_charge; 
                        
                        $bill_amount = explode('|',$result['AMOUNT_OPTION']);
                        $bill_amt = $this->convert_into_paise(round($bill_amount[0])).'|'.$bill_amount[1];

                        $input['recharge_amount'] = $recharge_amount;
                        $input['bill_amount'] = $bill_amt;
                         
                        
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'bbps_getuserbill.log', $result, '2...display user bill details on screen');
                        if (strtolower($response['reason']['@attributes']['responseReason']) == 'successful') {
                            $this->response(array('status' => 'Success', 'data' => $result, 'msg' => 'Success msg'), 200);
                        } else {
                            $this->response(array('status' => 'Failed', 'data' => $response['reason']['@attributes']['responseReason'], 'msg' => 'Output failed'), 200);
                        }
                        
                    } else {
                        
                        if (strtolower($response['reason']['@attributes']['responseReason']) == 'successful') {
                            a:
                            $check_user_wallet = $this->Utilities_bbps_model->getUserWalletDetails($user_id);
                            
                            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'bbps_getuserbill.log', $check_user_wallet, '3.....check_user_wallet');

                            if ($check_user_wallet[0]['amt'] > $recharge_amount) {
                                
                                $commissionResult = $this->Utilities_model->commission_from($this->sessionData["id"]);
                                $created_by = $commissionResult[0]['level_3'];
                                
                                $check_if_commission_available = $this->checkCommission($input,$created_by);
                                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'bbps_check_commssion_available.log', $check_if_commission_available, 'check_commssion_available');
                                if(count($check_if_commission_available) == 0){
                                    $this->response(array('status' => 'Failed', 'data' => $check_if_commission_available, 'msg' => 'Commission not set. Please contact your administrator.'), 200);
                                }
                                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'BBPS_recharge.log', $input, 'BBPS Recharge - input parameters');
                                $recharge_response_check = recharge_with_bbps($input, $getOperatorDetails, $adhoc_value, $billertags);
                                //$recharge_response_check == 'Error:Account(s) is either blocked/inactive/not found or there is insufficient balance in the account(s).';

                                if (strpos($recharge_response_check, 'Error:') !== false) {
                                   $this->response(array('status' => 'Failed', 'msg' => $recharge_response_check), 200);                                   
                                }
                                
                                
//                                show($recharge_response_check); 
//                                die;
                                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'bbps_getuserbill.log', $recharge_response_check, '4.....Recharge response');
                                
                               
                                $recharge_xml_data = simplexml_load_string($recharge_response_check);
                                $recharge_result = json_encode($recharge_xml_data);
                                $recharge_response_check = json_decode($recharge_result, true);
                                //show($recharge_response_check,1);
                                if (isset($recharge_response_check['errorMessages']) && !empty($recharge_response_check['errorMessages'])) {
                                    
                                    $this->response(array('status' => 'Failed', 'data' => $response['errorMessages'], 'msg' => 'Error : ' . $response['errorMessages']['errorDtl']), 200);
                                }else{
                                    $wallet_tran_id = substr(hexdec(uniqid()), 4, 12);
                                    
                                    $recharge_from_BBPS = array(
                                    "customerName" => $recharge_response_check['customerName'],
                                    "amount" => $this->convert_into_rupees($recharge_response_check['amount']),
                                    "customer_charge" => $customer_charge,
                                    "type" => $type,
                                    "rechargefrom" => $recharge_from,
                                    "mobileno" => $recharge_response_check['customerMobile'],
                                    "operator" => $recharge_response_check['billerName'],
                                    "status" => "Successful",
                                    //"recharge_full_status" => 'Successful', //$response['reason']['@attributes']['responseReason'],
                                    "reason" => 'Successful', //$response['reason']['@attributes']['responseReason'],
                                    "systemReference" => $recharge_response_check['clientRefId'],
                                    "transactionReference" => $recharge_response_check['transactionRefId'],
                                    "transaction_time" => $recharge_response_check['transactionDateTime'],
                                    "bill_date" => $recharge_response_check['billDate'],
                                    "operator_id" => $recharge_response_check['billerId'],
                                    "bill_number" => $recharge_response_check['billNumber'],
                                    "bill_period" => $recharge_response_check['billPeriod'],
                                    "wallet_transaction_no" => $wallet_tran_id,
                                   // "created_at" => date_format(date_create(), 'Y-m-d h:i:s'),
                                    "created_by" => $user_id
                                );
                                    //show($recharge_from_BBPS,1);
                                //$input['recharge_amount'] = $recharge_response_check['amount'];
                                     log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'BBPS_recharge.log', $recharge_from_BBPS, 'BBPS Recharge - API response');

                                    $result = $this->Utilities_bbps_model->saveBBPSData($recharge_from_BBPS);
                                    
//                                    echo 'bbps data'; print_r($recharge_from_BBPS);
                                    if (isset($recharge_response_check['transactionRefId']) && !empty($recharge_response_check['transactionRefId'])) {
                                        $transaction_no = $recharge_response_check['transactionRefId'];
                                        //$wallet_tran_id = substr(hexdec(uniqid()), 4, 12);
                                        //$user_result = $this->Utilities_model->commission_from();
                                        //print_r($user_result);
                                        $commissionResult = $this->bbps_serviceRetailerCommissionDistribution($input, $wallet_tran_id,$created_by);
                                        //show($commissionResult,1);
                                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'BBPS_Recharge.log', $commissionResult, 'BBPS Recharge - Commission result');

                                        $updated_wallet_amt = $check_user_wallet[0]['amt'] - $recharge_amount;
                                        $update_wallet = $this->Utilities_bbps_model->updateUserWallet($user_id, $updated_wallet_amt);
                                        //show($update_wallet,1);
                                        
                                        
                                        $wallet_trans_detail['transaction_no'] = $wallet_tran_id;
                                        $wallet_trans_detail['w_id'] = $update_wallet[0]['id'];
                                        $wallet_trans_detail['amt'] = $recharge_amount;
                                        $wallet_trans_detail['wallet_type'] = 'actual_wallet';
                                        $wallet_trans_detail['comment'] = "BBPS $type recharge";
                                        $wallet_trans_detail['status'] = 'Debited';
                                        $wallet_trans_detail['user_id'] = $user_id;
                                        $wallet_trans_detail['amt_before_trans'] = $check_user_wallet[0]['amt'];
                                        $wallet_trans_detail['amt_after_trans'] = $updated_wallet_amt;
                                        $wallet_trans_detail['added_by'] = $user_id;
                                        $wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                                        $wallet_trans_detail['ticket_id'] = '';
                                        // $wallet_trans_detail['transaction_type_id'] = '3';
                                        $wallet_trans_detail['transaction_type'] = 'Debited';
                                        $wallet_trans_detail['is_status'] = 'Y';
                                        $wallet_trans_detail['remark'] = "BBPS $type recharge";
                                        //echo '$wallet_trans_detail'; print_r($wallet_trans_detail);
                                       
                                        $wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail);

                                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'BBPS_recharge.log', $update_wallet, 'BBPS Recharge Response START');


                                        $transaction_data = array(
                                            'type' => $type,
                                            'operator' => $operator,
                                            'recharge_on' => $number,
                                            'recharge_from' => $recharge_from,
                                            'last_transaction_amount' => $recharge_amount,
                                            'wallet_tran_id' => $wallet_trans_id,
                                            'transaction_no' => $wallet_tran_id,
                                            'created_by' => $user_id
                                        );
                                         
                                        $this->Utilities_bbps_model->saveTransactionData($transaction_data);
                                        //echo '$transaction_data'; print_r($recharge_response_check); die;
                                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'BBPS_recharge.log', $recharge_response_check, 'BBPS Recharge Response END');
                                        $this->response(array('status' => 'Successful', 'data' => $recharge_response_check, 'msg' => 'Response - Payment successfull'), 200);
                                      
                                        
                                        
                                    } else {
                                        $this->response(array('status' => 'Failed', 'data' => $recharge_response_check, 'msg' => 'Failed output'), 200);
                                    }
                                }
                                
                            } else {
                                $this->response(array('status' => 'Failed', 'data' => $response, 'msg' => 'Insufficient balance'), 200);
                            }
                        } else {
                            $this->response(array('status' => 'Failed', 'data' => $response['reason'], 'msg' => $response['reason']['@attributes']['responseReason']), 200);
                        }
                    }
                }
            }
        } else {
            $this->response(array('status' => 'Failed', 'msg' => 'No Input Found.'), 200);
        }
    }
    
    public function checkCommission($input,$created_by){
        
       // echo $input['recharge_amount'] = $this->convert_into_rupees($input['recharge_amount']);
       
        $type = $input['type'];
        $service_id = BBPS_UTILITIES_SERVICE_ID;
        
         if($input['recharge_amount'] >=1 && $input['recharge_amount'] <= 1000){
                $range = '1-1000';
         }elseif($input['recharge_amount'] >= 1001 && $input['recharge_amount'] <= 2000){
                $range = '1001-2000';
         }elseif($input['recharge_amount'] >= 2001){
                $range = '2001-above';
        }
        
        if($type == 'Electricity'){
            $servicecode = "Electricity ".$range;
            $servicename = "Electricity";
            
        }else if($type == 'Gas'){
             $servicecode = "Gas ".$range;
             $servicename = "Gas";
             
        }else if($type == 'Water'){
             $servicecode = "Water ".$range;
             $servicename = "Water";
             
        }else{
            $servicecode = "Telecom Postpaid ".$range;
            $servicename = "Telecom Postpaid";
        }
        
        $subService = $this->Utilities_bbps_model->getSubservices($range, $servicename, $servicecode); 
        //show($subService,1);
        $LcSqlStr = "SELECT * from services_commissions 
            WHERE commission_name LIKE '%". $type.' '. str_replace(" ", "", $range) . "%' 
            AND service_id = '" . $service_id . "'
            AND  `values` NOT LIKE '{\"Trimax\":\"0\",\"Rokad\":\"0\",\"MD\":\"0\",\"AD\":\"0\",\"Distributor\":\"0\",\"Retailer\":\"0\"}'
            AND sub_service_id = '" . $subService[0]['id'] . "'";
                
        //show($LcSqlStr,1);
        $query = $this->db->query($LcSqlStr);
        return $query->result();

    }
    
    function convert_into_rupees($paise)
    {
        $rupees = $paise / 100;
        return $rupees;
    }
    
    function convert_into_paise($rupees){
        $paise = $rupees * 100;
        return $paise;
    }
}

