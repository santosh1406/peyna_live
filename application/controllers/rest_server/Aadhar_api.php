 <?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Aadhar_api extends REST_Controller {

   function __construct() 
        {
		parent::__construct();
		 $this->load->model('Common_api_model');
		 $this->load->model('Mainmodel');
         $this->load->model('Utility_model');
	     $this->load->helper(array('smartcard_helper','common_helper'));


                $this->apiCred = json_decode(SMART_CARD_CREDS, true);
                $header_value = $this->apiCred['X_API_KEY'];
                $this->header = array(
                    'X-API-KEY:' . $header_value
                );

                $this->basicAuth = array(
                    'username' => $this->apiCred['username'],
                    'password' => $this->apiCred['password']
                );
                
                $request = $this->post();
                $user_id = $request['apploginuser'];
                $session_id= $this->Mainmodel->get_session_id($this->input->post('session_id'),$user_id);
                $this->session_id = $session_id['itms_session_id'];
                $this->user_id =itms_user_id;
				
	         $insert_data = array('username' => $this->input->post('apploginuser'), 'ip' => $_SERVER['REMOTE_ADDR'], 'method' => $_SERVER['REQUEST_URI'], 'type' => 'get', 'version_id' => $this->input->post('version_id'));
                 $this->requestId = $this->Mainmodel->insert_audit_data($insert_data)."";
                  /*   added code disallow multiple login of same user code start here */
                $startTime = date("Y-m-d H:i:s");
                $cenvertedTime = date('Y-m-d H:i:s',strtotime(LOGIN_EXPIRE_TIME,strtotime($startTime))) ;
                $this->Mainmodel->upDateLastActivityTime($this->input->post('session_id'),$cenvertedTime);
                /*   added code disallow multiple login of same user code end here */
      
	}

	 public function AadharAPIConfig_post() {
            
            $URL = $this->apiCred['url'] . '/Aadhar_api/AadharAPIConfig';
            $request = $this->post();
            $post_val = $this->input->post();
            $itms_request = $this->post();
            $itms_request['apploginuser'] = $this->user_id ;
            $itms_request['session_id']   = $this->session_id ;		
            $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
            $json = json_decode($data);
			
			
			
         
			
			$log_data = array();
            $log_data['url'] = current_url();
            $log_data['Request_type'] = $_SERVER['REQUEST_METHOD'];
            $log_data['get'] = $this->input->get();
            $log_data['post'] = $this->post();
            $log_data['header'] = getallheaders();
            $log_data['user'] = $this->input->server('PHP_AUTH_USER');
            $log_data['ip'] = $this->input->ip_address();
            $log_data['request_id'] = $this->requestId;
            $log_data['created_by'] =$request['apploginuser'];
            $log_data['modified_by'] =$request['apploginuser'];
            // show($log_data);
            log_data('rest/smartcard/aadhar_api_' . date('d-m-Y') . '.log', $log_data, 'api');
			log_data('rest/smartcard/aadhar_api_' . date('d-m-Y') . '.log', $json, 'api');
            
			if (strtolower($json->status) == 'success') {
            }
            json_response($json);
        }
   
    public function Aadhar_auth_otp_post() {
            
            $URL = $this->apiCred['url'] . '/Aadhar_api/Aadhar_auth_otp';
            $request = $this->post();
            $post_val = $this->input->post();
            $itms_request = $this->post();
            $itms_request['apploginuser'] = $this->user_id ;
            $itms_request['session_id']   = $this->session_id ;		
            $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
            $json = json_decode($data);
			
			/* aadhar log code start here */
			
            $log_data = array();
            $log_data['url'] = current_url();
            $log_data['Request_type'] = $_SERVER['REQUEST_METHOD'];
            $log_data['get'] = $this->input->get();
            $log_data['header'] = getallheaders();
            $log_data['user'] = $this->input->server('PHP_AUTH_USER');
            $log_data['ip'] = $this->input->ip_address();
            $log_data['request_id'] = $this->requestId;
            $log_data['created_by'] =$request['apploginuser'];
            $log_data['modified_by'] =$request['apploginuser'];

            // show($log_data);
            log_data('rest/smartcard/aadhar_api_' . date('d-m-Y') . '.log', $log_data, 'api');
	    log_data('rest/smartcard/aadhar_api_' . date('d-m-Y') . '.log', $json, 'api');
		    
			/* aadhar log code end here */

            if (strtolower($json->status) == 'success') {
            }
            json_response($json);
            
        }
		
		 public function Aadhar_KYC_post() {
            $URL = $this->apiCred['url'] . '/Aadhar_api/Aadhar_KYC';
            $request = $this->post();
            $post_val = $this->input->post();
            $itms_request = $this->post();
            $itms_request['apploginuser'] = $this->user_id ;
            $itms_request['session_id']   = $this->session_id ;		
            $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
			//print_r($data);
            $json = json_decode($data);
           
		   // aadhar log code start here */
			
            $log_data = array();
            $log_data['url'] = current_url();
            $log_data['Request_type'] = $_SERVER['REQUEST_METHOD'];
            $log_data['get'] = $this->input->get();
            $log_data['header'] = getallheaders();
            $log_data['user'] = $this->input->server('PHP_AUTH_USER');
            $log_data['ip'] = $this->input->ip_address();
            $log_data['request_id'] = $this->requestId;
            $log_data['created_by'] =$request['apploginuser'];
            $log_data['modified_by'] =$request['apploginuser'];
            
            log_data('rest/smartcard/aadhar_api_' . date('d-m-Y') . '.log', $log_data, 'api');
	    log_data('rest/smartcard/aadhar_api_' . date('d-m-Y') . '.log', $json, 'api');
		    
	     /* aadhar log code end here */   
			
            if (strtolower($json->status) == 'success') {
            }
            json_response($json);
        }
	 
}
