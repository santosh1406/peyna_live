<?php

//Created by sonali kamble on 8-feb-2019

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Wallet extends REST_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('wallet_model');
    }

    public function agent_balance_post() {       
        
        $input = $this->post();
        if (!empty($input)) {
            $config = array(
                array('field' => 'agent_code', 'label' => 'Agent Code', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter agent code.')), 
                array('field' => 'amount', 'label' => 'Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter Amount.')),
                array('field' => 'waybill_no', 'label' => 'Waybill No', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter waybill no.')),

            );

            if (form_validate_rules($config) == False) {
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {               

                $from_wallet_details = $this->wallet_model->where('user_id',$input['agent_code'])->find_all();
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'wallet.log', $from_wallet_details, 'Agent Data');
                $amount = $input['amount'];
                $from_wallet_id = $from_wallet_details[0]->id;
                $wallet_amt = $from_wallet_details[0]->amt;  
               
                if($amount > $wallet_amt)
                {                    
                    $this->response(array('status' => 'failed', 'msg' => 'Insufficient Wallet balance'), 200);
                }else
                {         
                   $id= $this->updateWalletTxn($from_wallet_id,$amount,$wallet_amt,$input['agent_code']);
                   if(!empty($id))
                   {
                       $this->response(array('status' => 'success', 'msg' => 'Amount has been Deduct'), 200);
                   }
                    
                }                
            }
        }
    }
    
    function updateWalletTxn($from_wallet_id,$amount,$wallet_amt,$from_user_id){
        
       

        $transfer_amount = $amount;
        
        $fund_transfer_remark = "Wallet";
        
        
            $wallet_trans_detail = [
          "w_id" => $from_wallet_id,
          "amt" => $transfer_amount,
          "merchant_amount" => '',
          "wallet_type" => "actual_wallet",
          "comment" => "Wallet Transaction",
          "status" => 'Debited',
          "user_id" => $from_user_id,
          "amt_before_trans" => $wallet_amt,
          "amt_after_trans" => $wallet_amt - $transfer_amount,
          "remaining_bal_before" => 0,
          "remaining_bal_after" => 0,
          "cumulative_bal_before" => 0,
          "cumulative_bal_after" => 0,
          "amt_transfer_from" => $from_user_id,
          "amt_transfer_to" => 0,
         // "transaction_type_id" => '11',   //need to check this value
          "transaction_type" => 'Debited',
          "is_status" => "Y",
          "added_on" => date("Y-m-d H:i:s"),
          //"added_by" => $this->session->userdata('user_id'),
          "added_by" => $from_user_id,
          "transaction_no" => substr(hexdec(uniqid()), 4, 12),
          "remark" => $fund_transfer_remark
        ];
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'wallet_Trans.log', $wallet_trans_detail, 'Inserted Data');
        $this->db->insert('wallet_trans',$wallet_trans_detail);
        $wallet_trans_id= $this->db->insert_id();
            
            $update_wallet = [
            "amt" => $wallet_amt - $transfer_amount,
            "last_transction_id" => $wallet_trans_id,
            "updated_by" => $from_user_id,
            "updated_date" => date("Y-m-d H:i:s"),
        ];

        $this->db->where('user_id',$from_user_id);
        return $update_from_wallet = $this->db->update("wallet", $update_wallet);
        
}

}


