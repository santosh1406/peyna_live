<?php

//Created by harshada kulkarni on 24-07-2018

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Utilities extends REST_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model(array('Utilities_model', 'wallet_trans_model'));
        $this->load->helper('utilities_helper');
    }
    //Created by harshada kulkarni on 24-07-2018
    public function recharge_post() {

        $data = array();
        $input = $this->post();

        $error_messages = json_decode(file_get_contents('messages.json'), true);
        if (!empty($input)) {

            $config = array(
                array('field' => 'recharge_type', 'label' => 'Recharge Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter recharge type.')),
                array('field' => 'mobile_operator', 'label' => 'Mobile Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter mobile operator.')),
                array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.')),
                array('field' => 'recharge_amount', 'label' => 'Recharge Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter recharge amount.'))
            );

            if (form_validate_rules($config) == FALSE) {
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {
                $user_id = $input['user_id'];
                $recharge_from = $input['recharge_from'];
                $operator = $input['mobile_operator'];
                $type = $input['recharge_type'];
                $mobile_number = $input['mobile_number'];
                $recharge_amount = $input['recharge_amount'];
                $comment = $input['comment'];

                $check_user_wallet = $this->Utilities_model->getUserWalletDetails($user_id);

                if ($check_user_wallet[0]['amt'] > $recharge_amount) {
                    $getOperatorDetails = $this->Utilities_model->operatorDetails($operator, $type);
                    $recharge_response_check = mobile_recharge($mobile_number, $recharge_amount, $comment, $getOperatorDetails);

                    if ($recharge_response_check['check_result']['ERROR'] == 0) {
                        if ($recharge_response_check['pay_result']['ERROR'] == 0) {

                            $transaction_no = $recharge_response_check['pay_result']['TRANSID'];

                            $commissionResult = $this->serviceRetailerCommissionDistribution($input, $transaction_no);

                            $updated_wallet_amt = $check_user_wallet[0]['amt'] - $commissionResult['trimax_final_amt'];
                            $update_wallet = $this->Utilities_model->updateUserWallet($session_user_id, $updated_wallet_amt);

                            $transaction_data = array(
                                'type' => $type,
                                'operator' => $operator,
                                'recharge_on' => $mobile_number,
                                'recharge_from' => $recharge_from,
                                'last_transaction_amount' => $commissionResult['trimax_final_amt'],
                                'transaction_id' => $transaction_no,
                                'created_by' => $commissionResult['created_by']
                            );

                            $this->Utilities_model->saveTransactionData($transaction_data);
                            $this->response(array('status' => 'Success', 'data' => $recharge_response_check), 200);
                        } else {
                            $this->response(array('status' => 'Failed', 'data' => $recharge_response_check, 'msg' => $error_messages['messages'][$recharge_response_check['pay_result']['ERROR']]), 200);
                        }
                    } else {
                        $this->response(array('status' => 'Failed', 'data' => $recharge_response_check, 'msg' => $error_messages['messages'][$recharge_response_check['check_result']['ERROR']]), 200);
                    }
                } else {
                    $this->response(array('status' => 'Failed', 'data' => $recharge_response_check, 'msg' => 'Insufficient balance'), 200);
                }
            }
        }
    }

    public function jri_recharge_post1() {

        $data = array();
        $input = $this->post();
        $error_messages = json_decode(file_get_contents('messages.json'), true);
        if (!empty($input)) {

            $config = array(
                array('field' => 'recharge_type', 'label' => 'Recharge Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter recharge type.')),
                array('field' => 'mobile_operator', 'label' => 'Mobile Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter mobile operator.')),
                array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.')),
                array('field' => 'recharge_amount', 'label' => 'Recharge Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter recharge amount.'))
            );
            if (form_validate_rules($config) == FALSE) {
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {
                /* Check JRI Wallet Funds */
                $balance = $this->getJRIBal();
                if($balance->Balance == '0.00'){
                    $this->response(array('status' => 'Failed', 'data' => [], 'msg' => 'Insufficient Wallet balance'), 200);
                }

                $user_id = $input['user_id'];
                $recharge_from = $input['recharge_from'];
                $operator = $input['mobile_operator'];
                $type = $input['recharge_type'];
                $is_postpaid = $input['recharge_type'] == 'Prepaid Mobile' ? 'N' : 'Y';
                $mobile_number = $input['mobile_number'];
                $recharge_amount = $input['recharge_amount'];
                $comment = $input['comment'];
                // show($input, 1);
                $check_user_wallet = $this->Utilities_model->getUserWalletDetails($user_id);

                if ($check_user_wallet[0]['amt'] > $recharge_amount) {
                    $authenticate_JRI = authJRI();
                    $authenticate_JRI['mobile_number'] = $mobile_number;
                    $authenticate_JRI['operator'] = $operator;
                    $authenticate_JRI['recharge_amount'] = $recharge_amount;
                    $authenticate_JRI['service_type'] = 'M';
                    $authenticate_JRI['comments'] = $comment;
                    $authenticate_JRI['is_postpaid'] = $is_postpaid;
                    // show($authenticate_JRI, 1);
                    $recharge_from_JRI = recharge_with_JRI($authenticate_JRI);

                    // show($recharge_from_JRI, 1);
                    
                    // $recharge_from_JRI = array(
                    //     "Amount" => "10",
                    //     "IsPostpaid" => "",
                    //     "Location" => "",
                    //     "MobileNo" => "9819293097",
                    //     "Provider" => "Vodafone",
                    //     "ServiceType" => "M",
                    //     "Status" => "0 | Recharge Successful",
                    //     "SystemReference" => "5bd9522865f5d",
                    //     "TransactionReference" => "MRC0032273193"
                    // );
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $recharge_from_JRI, 'JRI Recharge - API response');

                    $check_errors = $this->checkErrorsInJRIResponse($recharge_from_JRI, 'M');
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $check_errors, 'JRI Recharge - JRI Errors Check');

                    if($check_errors['type'] == 'error'){
                        $this->response(array('status' => 'Failed', 'data' => $recharge_from_JRI, 'msg' => $recharge_from_JRI['reason']), 200);
                    }if($check_errors['type'] == 'pending'){
                        $this->response(array('status' => 'Failed', 'data' => $recharge_from_JRI, 'msg' => $recharge_from_JRI['reason']), 200);
                    }else{
                            // $transaction_no = $recharge_from_JRI;
                        $wallet_tran_id = $check_errors['tnx_id'];
                        $transaction_no = substr(hexdec(uniqid()), 4, 12);
                        
                        $commissionResult = $this->serviceRetailerCommissionDistribution($input, $wallet_tran_id);
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $commissionResult, 'JRI Recharge - Commission result');


                        $updated_wallet_amt = $check_user_wallet[0]['amt'] - $recharge_amount;
                        $update_wallet = $this->Utilities_model->updateUserWallet($user_id, $updated_wallet_amt);

                        $wallet_trans_detail['w_id'] = $update_wallet[0]['id'];
                        $wallet_trans_detail['amt'] = $recharge_amount;
                        $wallet_trans_detail['comment'] = 'JRI Mobile Recharge';
                        $wallet_trans_detail['status'] = 'Debited';
                        $wallet_trans_detail['user_id'] = $user_id;
                        $wallet_trans_detail['added_by'] = $user_id;
                        $wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                        $wallet_trans_detail['amt_before_trans'] = $check_user_wallet[0]['amt'];
                        $wallet_trans_detail['amt_after_trans'] = $updated_wallet_amt;
                        $wallet_trans_detail['ticket_id'] = '';
                        // $wallet_trans_detail['transaction_type_id'] = '3';
                        $wallet_trans_detail['wallet_type'] = 'actual_wallet';
                        $wallet_trans_detail['transaction_type'] = 'Debited';
                        $wallet_trans_detail['is_status'] = 'Y';
                        $wallet_trans_detail['transaction_no'] = $transaction_no;

                        $wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail);


                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $update_wallet, 'JRI Recharge Response START');
                            // show($update_wallet, 1);

                        $transaction_data = array(
                            'type' => $type,
                            'operator' => $operator,
                            'recharge_on' => $mobile_number,
                            'recharge_from' => $recharge_from,
                            'last_transaction_amount' => $recharge_amount,
                            'transaction_id' => $transaction_no,
                            'wallet_tran_id' => $wallet_tran_id,
                            'created_by' => date_format(date_create(), 'Y-m-d h:i:s')
                        );

                            // show($recharge_from_JRI, 1);
                        $this->Utilities_model->saveTransactionData($transaction_data);

                        $recharge_from_JRI['created_at'] = date_format(date_create(), 'Y-m-d h:i:s');
                        $recharge_from_JRI['recharge_full_status'] = trim($recharge_from_JRI['Status']);

                        $this->Utilities_model->saveJRIData($recharge_from_JRI);
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $recharge_from_JRI, 'JRI Recharge - End');

                        $this->response(array('status' => 'Success', 'data' => $recharge_from_JRI, 'msg' => $recharge_from_JRI['reason']), 200);
                    }

                } else {
                    $this->response(array('status' => 'Failed', 'data' => $recharge_from_JRI, 'msg' => 'Insufficient balance'), 200);
                }
            }
        }
    }

    public function jri_recharge_post() {

        $data = array();
        $input = $this->post();

        $error_messages = json_decode(file_get_contents('messages.json'), true);

        if (!empty($input)) {

            $config = array(
                array('field' => 'recharge_type', 'label' => 'Recharge Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter recharge type.')),
                array('field' => 'mobile_operator', 'label' => 'Mobile Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter mobile operator.')),
                array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.')),
                array('field' => 'recharge_amount', 'label' => 'Recharge Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter recharge amount.')),
                array('field' => 'user_id', 'label' => 'Recharge Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter user id.')),
                array('field' => 'recharge_from', 'label' => 'Recharge From', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter recharge_from (eg. recharge_from: web|mobile ).'))
            );


            if (form_validate_rules($config) == FALSE) {
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {
                /* Check JRI Wallet Funds */
                $balance = $this->getJRIBal();
                if($balance->Balance == '0.00'){
                    $this->response(array('status' => 'Failed', 'data' => [], 'msg' => 'Insufficient Wallet balance'), 200);
                }

                $user_id = $input['user_id'];
                $recharge_from = $input['recharge_from'];
                $operator = $input['mobile_operator'];
                $type = $input['recharge_type'];
                $is_postpaid = $input['recharge_type'] == 'Prepaid Mobile' ? 'N' : 'Y';
                $mobile_number = $input['mobile_number'];
                $recharge_amount = $input['recharge_amount'];
                $comment = $input['comment'];
                // show($input, 1);
                $check_user_wallet = $this->Utilities_model->getUserWalletDetails($user_id);

                if ($check_user_wallet[0]['amt'] > $recharge_amount) {
                    $authenticate_JRI = authJRI();
                    $authenticate_JRI['mobile_number'] = $mobile_number;
                    $authenticate_JRI['operator'] = $operator;
                    $authenticate_JRI['recharge_amount'] = $recharge_amount;
                    $authenticate_JRI['service_type'] = 'M';
                    $authenticate_JRI['comments'] = $comment;
                    $authenticate_JRI['is_postpaid'] = $is_postpaid;
                    $recharge_from_JRI = recharge_with_JRI($authenticate_JRI);

                    // show($recharge_from_JRI, 1);
                    
                    // $recharge_from_JRI = array(
                    //     "Amount" => "102",
                    //     "IsPostpaid" => "N",
                    //     "Location" => "",
                    //     "MobileNo" => "7790410000",
                    //     "Provider" => "Jio",
                    //     "ServiceType" => "M",
                    //     // "Status" => "Error: Invalid requested recharge amount",
                    //     // "Status" => "0 | Recharge Successful",
                    //     // "Status" => "1|Recharge in Process",
                    //     "Status" => "2|Recharge Unsuccessful| Reason: Invalid recharge amount",
                    //     "SystemReference" => "5bd9522865f5d",
                    //     "TransactionReference" => "MRC0010277123"
                    // );

                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $recharge_from_JRI, 'JRI Recharge - API response');

                    $check_errors = $this->checkErrorsInJRIResponse($recharge_from_JRI, 'M');
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $check_errors, 'JRI Recharge - JRI Errors Check');

                    $recharge_from_JRI['created_at'] = date_format(date_create(), 'Y-m-d h:i:s');
                    $recharge_from_JRI['recharge_full_status'] = trim($recharge_from_JRI['Status']);
                    
                    $reason = '';
                    if(strpos($recharge_from_JRI['Status'], '|') !== false){
                        $reason = explode("|", $recharge_from_JRI['Status']);
                         if(isset($reason[1])){
                            $new_status = $reason[1];
                            if(count($reason) == 3){
                                $reason = explode(':', $reason[2]);
                                $reason = $reason[1];
                            }else{
                                $reason = $reason[1];
                            }
                        }else{
                            $reason = 'Error';
                            $new_status = 'Error';
                        }
                    }

                    
                    if(strpos($recharge_from_JRI['Status'], 'Error') !== false){
                        $reason = explode("Error:", $recharge_from_JRI['Status']);
                        $reason = isset($reason[1]) ? $reason[1] : 'Error';
                    }
                    
                    $recharge_from_JRI['reason'] = trim($reason);
                    $recharge_from_JRI['Status'] = trim($new_status);
                    // show($recharge_from_JRI, 1);
                    $this->Utilities_model->saveJRIData($recharge_from_JRI);


                    if($check_errors['type'] == 'error'){
                        $this->response(array('status' => 'Failed', 'data' => $recharge_from_JRI, 'msg' => $recharge_from_JRI['reason']), 200);
                    }if($check_errors['type'] == 'pending'){
                        $this->response(array('status' => 'Failed', 'data' => $recharge_from_JRI, 'msg' => $recharge_from_JRI['reason']), 200);
                    }else{
                            // $transaction_no = $recharge_from_JRI;
                        $transaction_no = $check_errors['tnx_id'];
                        $wallet_tran_id = substr(hexdec(uniqid()), 4, 12);

                        $commissionResult = $this->serviceRetailerCommissionDistribution($input, $wallet_tran_id);
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $commissionResult, 'JRI Recharge - Commission result');


                        $updated_wallet_amt = $check_user_wallet[0]['amt'] - $recharge_amount;
                        $update_wallet = $this->Utilities_model->updateUserWallet($user_id, $updated_wallet_amt);

                        $wallet_trans_detail['w_id'] = $update_wallet[0]['id'];
                        $wallet_trans_detail['amt'] = $recharge_amount;
                        $wallet_trans_detail['comment'] = 'JRI Mobile Recharge';
                        $wallet_trans_detail['status'] = 'Debited';
                        $wallet_trans_detail['user_id'] = $user_id;
                        $wallet_trans_detail['added_by'] = $user_id;
                        $wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                        $wallet_trans_detail['amt_before_trans'] = $check_user_wallet[0]['amt'];
                        $wallet_trans_detail['amt_after_trans'] = $updated_wallet_amt;
                        $wallet_trans_detail['ticket_id'] = '';
                        // $wallet_trans_detail['transaction_type_id'] = '3';
                        $wallet_trans_detail['wallet_type'] = 'actual_wallet';
                        $wallet_trans_detail['transaction_type'] = 'Credited';
                        $wallet_trans_detail['is_status'] = 'Y';
                        $wallet_trans_detail['transaction_no'] = $wallet_tran_id;
                        // show($wallet_trans_detail, 1);

                        $wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail);


                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $update_wallet, 'JRI Recharge Response START');
                            // show($update_wallet, 1);

                        $transaction_data = array(
                            'type' => $type,
                            'operator' => $operator,
                            'recharge_on' => $mobile_number,
                            'recharge_from' => $recharge_from,
                            'last_transaction_amount' => $recharge_amount,
                            'wallet_tran_id' => $wallet_tran_id,
                            'transaction_id' => $transaction_no,
                            'created_by' => date_format(date_create(), 'Y-m-d h:i:s')
                        );

                            // show($recharge_from_JRI, 1);
                        $this->Utilities_model->saveTransactionData($transaction_data);


                        // $this->Utilities_model->saveJRIData($recharge_from_JRI);
                        // log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $recharge_from_JRI, 'JRI Recharge - End');

                        $this->response(array('status' => 'Success', 'data' => $recharge_from_JRI, 'msg' => $recharge_from_JRI['reason']), 200);
                    }

                    $this->Utilities_model->saveJRIData($recharge_from_JRI);
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $recharge_from_JRI, 'JRI Recharge - End');

                } else {
                    $this->response(array('status' => 'Failed', 'data' => $recharge_from_JRI, 'msg' => 'Insufficient balance'), 200);
                }
            }
        }else{
            $this->response(array('status' => 'failed', 'error' => 'No Input found'), 200);
        }
    }

    public function dth_post() {

        $data = array();
        $input = $this->post();     
        $error_messages = json_decode(file_get_contents('messages.json'), true);
        if (!empty($input)) {

            $config = array(
              //  array('field' => 'recharge_type', 'label' => 'Recharge Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter recharge type.')),
                array('field' => 'dth_operator', 'label' => 'Dth Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Dth operator.')),
                array('field' => 'customer_number', 'label' => 'Customer Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check Customer number.')),
                array('field' => 'recharge_amount', 'label' => 'Recharge Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter recharge amount.'))
            );

            if (form_validate_rules($config) == FALSE) {
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {
                $user_id = $input['user_id'];
                $dth_from = $input['dth_from'];
                $operator = $input['dth_operator'];
                $customer_number = $input['customer_number'];
                $recharge_amount = $input['recharge_amount'];
                $comment = $input['comment'];

                $check_user_wallet = $this->Utilities_model->getUserWalletDetails($user_id);                

                if ($check_user_wallet[0]['amt'] > $recharge_amount) {

                    $getOperatorDetails = $this->Utilities_model->dthOperator($operator);
                    $recharge_response_check = mobile_recharge($customer_number, $recharge_amount, $comment, $getOperatorDetails);

                    if ($recharge_response_check['check_result']['ERROR'] == 0) {
                        if ($recharge_response_check['pay_result']['ERROR'] == 0) {
                            $transaction_no = $recharge_response_check['pay_result']['TRANSID'];
                            $commissionResult = $this->serviceRetailerCommissionDistribution($input, $transaction_no);

                            $updated_wallet_amt = $check_user_wallet[0]['amt'] - $recharge_amount;
                            $update_wallet = $this->Utilities_model->updateUserWallet($session_user_id, $updated_wallet_amt);

                            $transaction_data = array(

                                'operator' => $operator,
                                'recharge_on' => $customer_number,
                                'dth_from' => $dth_from,
                                'last_transaction_amount' => $recharge_amount,
                                'transaction_id' => $recharge_response_check['pay_result']['TRANSID'],
                                'created_by' => $session_user_id
                            );

                            $this->Utilities_model->saveTransactionData($transaction_data);
                            $this->response(array('status' => 'Success', 'data' => $recharge_response_check), 200);
                            
                        } else {
                            $this->response(array('status' => 'Failed', 'data' => $recharge_response_check, 'msg' => $error_messages['messages'][$recharge_response_check['pay_result']['ERROR']]), 200);
                        }
                    } else {
                        $this->response(array('status' => 'Failed', 'data' => $recharge_response_check, 'msg' => $error_messages['messages'][$recharge_response_check['check_result']['ERROR']]), 200);
                    }
                } else {

                    $this->response(array('status' => 'Failed', 'data' => $recharge_response_check, 'msg' => 'Insufficient balance'), 200);
                }
            }
        }

    } 


    public function jri_dth_post1() {
        // show('test', 1);
        $data = array();
        $input = $this->post();     
        // show($input, 1);
        $error_messages = json_decode(file_get_contents('messages.json'), true);
        if (!empty($input)) {

            $config = array(
              //  array('field' => 'recharge_type', 'label' => 'Recharge Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter recharge type.')),
                array('field' => 'dth_operator', 'label' => 'Dth Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Dth operator.')),
                array('field' => 'customer_number', 'label' => 'Customer Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check Customer number.')),
                array('field' => 'recharge_amount', 'label' => 'Recharge Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter recharge amount.')),
                array('field' => 'user_id', 'label' => 'User ID', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter user id.'))

            );

            if (form_validate_rules($config) == FALSE) {
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {

                /* Check JRI Wallet Funds */
                $balance = $this->getJRIBal();
                if($balance->Balance == '0.00'){
                    $this->response(array('status' => 'Failed', 'data' => [], 'msg' => 'Insufficient Wallet balance'), 200);
                }

                $user_id = $input['user_id'];
                $dth_from = $input['dth_from'];
                $operator = $input['dth_operator'];
                $customer_number = $input['customer_number'];
                $recharge_amount = $input['recharge_amount'];
                $comment = $input['comment'];

                $check_user_wallet = $this->Utilities_model->getUserWalletDetails($user_id);

                if ($check_user_wallet[0]['amt'] > $recharge_amount) {

                    // process dth operations
                    $authenticate_JRI = authJRI();
                    $authenticate_JRI['mobile_number'] = $customer_number;
                    $authenticate_JRI['operator'] = $operator;
                    $authenticate_JRI['service_type'] = 'D';
                    $authenticate_JRI['recharge_amount'] = $recharge_amount;
                    $authenticate_JRI['comments'] = $comment;

                    $recharge_from_JRI = recharge_with_JRI($authenticate_JRI);

                    // show($recharge_from_JRI, 1);
                    
                    // $recharge_from_JRI = array(
                    //     "Amount" => "10",
                    //     "IsPostpaid" => "",
                    //     "Location" => "",
                    //     "MobileNo" => "9819293097",
                    //     "Provider" => "Airtel TV DTH",
                    //     "ServiceType" => "D",
                    //     "Status" => "0 | Recharge Successful",
                    //     "SystemReference" => "5bd9522865f5d",
                    //     "TransactionReference" => "MRC0032273193"
                    // );
                    // show($recharge_from_JRI, 1);
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $recharge_from_JRI, 'JRI Recharge - API response');

                    $check_errors = $this->checkErrorsInJRIResponse($recharge_from_JRI, 'D');
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $check_errors, 'JRI Recharge - JRI Errors Check');

                    if($check_errors['type'] == 'error'){
                        $this->response(array('status' => 'Failed', 'data' => $recharge_from_JRI, 'msg' => $recharge_from_JRI['reason']), 200);
                    }if($check_errors['type'] == 'pending'){
                        $this->response(array('status' => 'Failed', 'data' => $recharge_from_JRI, 'msg' => $recharge_from_JRI['reason']), 200);
                    }else{
                            // $transaction_no = $recharge_from_JRI;
                        $wallet_tran_id = $check_errors['tnx_id'];
                        $transaction_no = substr(hexdec(uniqid()), 4, 12);
                        $commissionResult = $this->serviceRetailerCommissionDistribution($input, $transaction_no);
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $commissionResult, 'JRI Recharge - Commission result');


                        $updated_wallet_amt = $check_user_wallet[0]['amt'] - $recharge_amount;
                        $update_wallet = $this->Utilities_model->updateUserWallet($user_id, $updated_wallet_amt);

                        $wallet_trans_detail['w_id'] = $update_wallet[0]['id'];
                        $wallet_trans_detail['amt'] = $recharge_amount;
                        $wallet_trans_detail['comment'] = 'JRI Mobile Recharge';
                        $wallet_trans_detail['status'] = 'Debited';
                        $wallet_trans_detail['user_id'] = $user_id;
                        $wallet_trans_detail['added_by'] = $user_id;
                        $wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                        $wallet_trans_detail['amt_before_trans'] = $check_user_wallet[0]['amt'];
                        $wallet_trans_detail['amt_after_trans'] = $updated_wallet_amt;
                        $wallet_trans_detail['ticket_id'] = '';
                        // $wallet_trans_detail['transaction_type_id'] = '3';
                        $wallet_trans_detail['wallet_type'] = 'actual_wallet';
                        $wallet_trans_detail['transaction_type'] = 'Debited';
                        $wallet_trans_detail['is_status'] = 'Y';
                        $wallet_trans_detail['transaction_no'] = $transaction_no;

                        $wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail);

                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $update_wallet, 'JRI Recharge Response START');

                        $transaction_data = array(
                            'type' => 'DTH',
                            'operator' => $operator,
                            'recharge_on' => $mobile_number,
                            'recharge_from' => $recharge_from,
                            'last_transaction_amount' => $recharge_amount,
                            'transaction_id' => $transaction_no,
                            'wallet_tran_id' => $wallet_tran_id,
                            'created_by' => date_format(date_create(), 'Y-m-d h:i:s')
                        );

                            // show($transaction_data, 1);
                        $this->Utilities_model->saveTransactionData($transaction_data);

                        $recharge_from_JRI['created_at'] = date_format(date_create(), 'Y-m-d h:i:s');
                        $recharge_from_JRI['recharge_full_status'] = trim($recharge_from_JRI['Status']);
                        $this->Utilities_model->saveJRIData($recharge_from_JRI);
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $recharge_from_JRI, 'JRI Recharge - End');

                        $this->response(array('status' => 'Success', 'data' => $recharge_from_JRI, 'msg' => $recharge_from_JRI['reason']), 200);

                    }
                } else {
                    $this->response(array('status' => 'Failed', 'data' => $recharge_response_check, 'msg' => 'Insufficient balance'), 200);
                }
            }
        } else {
            $this->response(array('status' => 'Failed', 'data' => $input, 'msg' => 'No Input Found'), 200);
        }

    } 


    public function jri_dth_post() {
        // show('test', 1);
        $data = array();
        $input = $this->post();     
        $error_messages = json_decode(file_get_contents('messages.json'), true);
        if (!empty($input)) {

            $config = array(
              //  array('field' => 'recharge_type', 'label' => 'Recharge Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter recharge type.')),
                array('field' => 'dth_operator', 'label' => 'Dth Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Dth operator.')),
                array('field' => 'customer_number', 'label' => 'Customer Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check Customer number.')),
                array('field' => 'recharge_amount', 'label' => 'Recharge Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter recharge amount.')),
                array('field' => 'user_id', 'label' => 'User ID', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter user id.')),
                array('field' => 'recharge_from', 'label' => 'Recharge From', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter recharge_from (eg. recharge_from: web|mobile ).'))
            );

            if (form_validate_rules($config) == FALSE) {
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {

                /* Check JRI Wallet Funds */
                $balance = $this->getJRIBal();
                if($balance->Balance == '0.00'){
                    $this->response(array('status' => 'Failed', 'data' => [], 'msg' => 'Insufficient Wallet balance'), 200);
                }

                $user_id = $input['user_id'];
                $dth_from = $input['dth_from'];
                $operator = $input['dth_operator'];
                $customer_number = $input['customer_number'];
                $recharge_amount = $input['recharge_amount'];
                $recharge_from = $input['recharge_from'];
                $comment = $input['comment'];

                $check_user_wallet = $this->Utilities_model->getUserWalletDetails($user_id);

                if ($check_user_wallet[0]['amt'] > $recharge_amount) {

                    // process dth operations
                    $authenticate_JRI = authJRI();
                    $authenticate_JRI['mobile_number'] = $customer_number;
                    $authenticate_JRI['operator'] = $operator;
                    $authenticate_JRI['service_type'] = 'D';
                    $authenticate_JRI['recharge_amount'] = $recharge_amount;
                    $authenticate_JRI['comments'] = $comment;

                    $recharge_from_JRI = recharge_with_JRI($authenticate_JRI);

                    // show($recharge_from_JRI, 1);
                    
                    // $recharge_from_JRI = array(
                    //     "Amount" => "10",
                    //     "IsPostpaid" => "",
                    //     "Location" => "",
                    //     "MobileNo" => "9819293097",
                    //     "Provider" => "Airtel TV DTH",
                    //     "ServiceType" => "D",
                    //     "Status" => "0 | Recharge Successful",
                    //     "SystemReference" => "5bd9522865f5d",
                    //     "TransactionReference" => "MRC0032273193"
                    // );
                    // show($recharge_from_JRI, 1);
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $recharge_from_JRI, 'JRI Recharge - API response');

                    $recharge_from_JRI['created_at'] = date_format(date_create(), 'Y-m-d h:i:s');
                    $recharge_from_JRI['recharge_full_status'] = trim($recharge_from_JRI['Status']);
                    $reason = '';
                    if(strpos($recharge_from_JRI['Status'], '|') !== false){
                        $reason = explode("|", $recharge_from_JRI['Status']);
                         if(isset($reason[1])){
                            $new_status = $reason[1];
                            if(count($reason) == 3){
                                $reason = explode(':', $reason[2]);
                                $reason = $reason[1];
                            }else{
                                $reason = $reason[1];
                            }
                        }else{
                            $reason = 'Error';
                            $new_status = 'Error';
                        }
                    }

                    
                    if(strpos($recharge_from_JRI['Status'], 'Error') !== false){
                        $reason = explode("Error:", $recharge_from_JRI['Status']);
                        $reason = isset($reason[1]) ? $reason[1] : 'Error';
                    }
                    
                    $recharge_from_JRI['reason'] = trim($reason);
                    $recharge_from_JRI['Status'] = trim($new_status);
                    // show($recharge_from_JRI, 1);
                    $this->Utilities_model->saveJRIData($recharge_from_JRI);

                    $check_errors = $this->checkErrorsInJRIResponse($recharge_from_JRI, 'D');
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $check_errors, 'JRI Recharge - JRI Errors Check');

                    if($check_errors['type'] == 'error'){
                        $this->response(array('status' => 'Failed', 'data' => $recharge_from_JRI, 'msg' => $recharge_from_JRI['reason']), 200);
                    }if($check_errors['type'] == 'pending'){
                        $this->response(array('status' => 'Failed', 'data' => $recharge_from_JRI, 'msg' => $recharge_from_JRI['reason']), 200);
                    }else{
                            // $transaction_no = $recharge_from_JRI;
                        $transaction_no = $check_errors['tnx_id'];
                        $wallet_tran_id = substr(hexdec(uniqid()), 4, 12);
                        $commissionResult = $this->serviceRetailerCommissionDistribution($input, $wallet_tran_id);
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $commissionResult, 'JRI Recharge - Commission result');


                        $updated_wallet_amt = $check_user_wallet[0]['amt'] - $recharge_amount;
                        $update_wallet = $this->Utilities_model->updateUserWallet($user_id, $updated_wallet_amt);

                        $wallet_trans_detail['w_id'] = $update_wallet[0]['id'];
                        $wallet_trans_detail['amt'] = $recharge_amount;
                        $wallet_trans_detail['comment'] = 'JRI Mobile Recharge';
                        $wallet_trans_detail['status'] = 'Debited';
                        $wallet_trans_detail['user_id'] = $user_id;
                        $wallet_trans_detail['added_by'] = $user_id;
                        $wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                        $wallet_trans_detail['amt_before_trans'] = $check_user_wallet[0]['amt'];
                        $wallet_trans_detail['amt_after_trans'] = $updated_wallet_amt;
                        $wallet_trans_detail['ticket_id'] = '';
                        // $wallet_trans_detail['transaction_type_id'] = '3';
                        $wallet_trans_detail['wallet_type'] = 'actual_wallet';
                        $wallet_trans_detail['transaction_type'] = 'Credited';
                        $wallet_trans_detail['is_status'] = 'Y';
                        // $wallet_trans_detail['wallet_tran_id'] = $wallet_tran_id;
                        $wallet_trans_detail['transaction_no'] = $transaction_no;

                        $wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail);

                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $update_wallet, 'JRI Recharge Response START');

                        $transaction_data = array(
                            'type' => 'DTH',
                            'operator' => $operator,
                            'recharge_on' => $customer_number,
                            'recharge_from' => $recharge_from,
                            'last_transaction_amount' => $recharge_amount,
                            'transaction_id' => $transaction_no,
                            'wallet_tran_id' => $wallet_tran_id,
                            'created_by' => date_format(date_create(), 'Y-m-d h:i:s')
                        );

                            // show($transaction_data, 1);
                        $this->Utilities_model->saveTransactionData($transaction_data);

                        
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $recharge_from_JRI, 'JRI Recharge - End');

                        $this->response(array('status' => 'Success', 'data' => $recharge_from_JRI, 'msg' => $recharge_from_JRI['reason']), 200);

                    }
                } else {
                    $this->response(array('status' => 'Failed', 'data' => $recharge_response_check, 'msg' => 'Insufficient balance'), 200);
                }
            }
        } else {
            $this->response(array('status' => 'Failed', 'data' => $input, 'msg' => 'No Input Found'), 200);
        }

    }


    //Created by harshada kulkarni on 24-07-2018
    public function serviceRetailerCommissionDistribution($input = "", $trans_ref_no = "") {

        $user_id = $input['user_id'];
        $service_id = UTILITIES_SERVICE_ID;
        $getRetailerServicesCommission = $this->Utilities_model->getRetailerServicesCommission($user_id, $service_id);
        // show($getRetailerServicesCommission, 1);

        if ($getRetailerServicesCommission) {

            $agent_id = $getRetailerServicesCommission[0]['user_id'];
            $agent_code = $getRetailerServicesCommission[0]['agent_code'];
            $distributor_id = $getRetailerServicesCommission[0]['level_5'];
            $Area_dis_id = $getRetailerServicesCommission[0]['level_4'];
            $master_dis_id = $getRetailerServicesCommission[0]['level_3'];
            $company_id = $getRetailerServicesCommission[0]['level_2'];
            $trimax_id = $getRetailerServicesCommission[0]['level_1'];
            $service_id = $getRetailerServicesCommission[0]['service_id'];
            $commission_id = $getRetailerServicesCommission[0]['commission_id'];

            $values[0] = json_decode($getRetailerServicesCommission[0]['values']);

            $trimax_commission = '0';
            $company_commission = '0';
            $md_commission = '0';
            $ad_commission = '0';
            $dist_commission = '0';
            $retailer_commission = '0';
            if (!empty($values)) {

                $trimax_commission = round($values[0]->Trimax, 2);
                $company_commission = round($values[0]->Rokad, 2);
                $md_commission = round($values[0]->MD, 2);
                $ad_commission = round($values[0]->AD, 2);
                $dist_commission = round($values[0]->Distributor, 2);
                $retailer_commission = round($values[0]->Retailer, 2);

                $recharge_amount = $input['recharge_amount']; //transaction_amt
                $commission_percent = $input['type'] == 'JRI' ? JRI_TRIMAX_COMMISSION :  CYBERPLAT_TRIMAX_COMMISSION; //trimax_comm_percent                                                       
                $trimax_cal = calculateGstTds($recharge_amount, $commission_percent);
                $earnings = $trimax_cal['earnings']; //trimax_earnings
                $gst = $trimax_cal['gst']; //trimax_gst                            
                $tds = $trimax_cal['tds']; //trimax_tds
                $final_amount = $trimax_cal['final_amount']; //trimax_final_amt                                                        

                $rokad_trimax_cal = calculateGstTds($earnings, $trimax_commission);
                $rokad_trimax_amt = $rokad_trimax_cal['earnings']; //rokad_trimax_amt                             
                $rokad_trimax_cal_with_tds_gst = calculateGstTds($final_amount, $trimax_commission);
                $rokad_trimax_amt_with_tds_gst = $rokad_trimax_cal_with_tds_gst['earnings']; //rokad_trimax_amt_with_tds_gst                            

                $company_cal = calculateGstTds($earnings, $company_commission);
                $company_amt = $company_cal['earnings']; //company_amt                             
                $company_cal_with_tds_gst = calculateGstTds($final_amount, $company_commission);
                $company_amt_with_tds_gst = $company_cal_with_tds_gst['earnings']; //company_amt_with_tds_gst                        

                $rd_cal = calculateGstTds($earnings, $md_commission);
                $rd_amt = $rd_cal['earnings']; //rd_amt                             
                $rd_cal_with_tds_gst = calculateGstTds($final_amount, $md_commission);
                $rd_amt_with_tds_gst = $rd_cal_with_tds_gst['earnings']; //rd_amt_with_tds_gst

                $dd_cal = calculateGstTds($earnings, $ad_commission);
                $dd_amt = $dd_cal['earnings']; //dd_amt                             
                $dd_cal_with_tds_gst = calculateGstTds($final_amount, $ad_commission);
                $dd_amt_with_tds_gst = $dd_cal_with_tds_gst['earnings']; //dd_amt_with_tds_gst

                $ex_cal = calculateGstTds($earnings, $dist_commission);
                $ex_amt = $ex_cal['earnings']; //ex_amt                             
                $ex_cal_with_tds_gst = calculateGstTds($final_amount, $dist_commission);
                $ex_amt_with_tds_gst = $ex_cal_with_tds_gst['earnings']; //ex_amt_with_tds_gst

                $sa_cal = calculateGstTds($earnings, $retailer_commission);
                $sa_amt = $sa_cal['earnings']; //sa_amt                             
                $sa_cal_with_tds_gst = calculateGstTds($final_amount, $retailer_commission);
                $sa_amt_with_tds_gst = $sa_cal_with_tds_gst['earnings']; //sa_amt_with_tds_gst              
            }

            $comm_distribution_array['user_id'] = $agent_id;
            $comm_distribution_array['service_id'] = $service_id;
            $comm_distribution_array['commission_id'] = $commission_id;
            $comm_distribution_array['transaction_no'] = $trans_ref_no;
            $comm_distribution_array['transaction_amt'] = $recharge_amount;
            $comm_distribution_array['service_comm_percent'] = SERVICE_COMMISSION;
            $comm_distribution_array['trimax_comm_percent'] = $input['type'] == 'JRI' ? JRI_TRIMAX_COMMISSION :  CYBERPLAT_TRIMAX_COMMISSION;;
            $comm_distribution_array['gst_percentage'] = GST_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['tds_percentage'] = TDS_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['trimax_earnings'] = $earnings;
            $comm_distribution_array['trimax_gst'] = $gst;
            $comm_distribution_array['trimax_tds'] = $tds;
            $comm_distribution_array['trimax_final_amt'] = $final_amount;
            $comm_distribution_array['rokad_trimax_per'] = $values[0]->Trimax;
            $comm_distribution_array['rokad_trimax_amt'] = $rokad_trimax_amt;
            $comm_distribution_array['rokad_trimax_amt_with_tds_gst'] = $rokad_trimax_amt_with_tds_gst;
            $comm_distribution_array['company_per'] = $values[0]->Rokad;
            $comm_distribution_array['company_amt'] = $company_amt;
            $comm_distribution_array['company_amt_with_tds_gst'] = $company_amt_with_tds_gst;
            $comm_distribution_array['rd_per'] = $values[0]->MD;
            $comm_distribution_array['rd_amt'] = $rd_amt;
            $comm_distribution_array['rd_amt_with_tds_gst'] = $rd_amt_with_tds_gst;
            $comm_distribution_array['dd_per'] = $values[0]->AD;
            $comm_distribution_array['dd_amt'] = $dd_amt;
            $comm_distribution_array['dd_amt_with_tds_gst'] = $dd_amt_with_tds_gst;
            $comm_distribution_array['ex_per'] = $values[0]->Distributor;
            $comm_distribution_array['ex_amt'] = $ex_amt;
            $comm_distribution_array['ex_amt_with_tds_gst'] = $ex_amt_with_tds_gst;
            $comm_distribution_array['sa_per'] = $values[0]->Retailer;
            $comm_distribution_array['sa_amt'] = $sa_amt;
            $comm_distribution_array['sa_amt_with_tds_gst'] = $sa_amt_with_tds_gst;
            $comm_distribution_array['status'] = 'Y';
            $comm_distribution_array['created_by'] = $user_id;
            $comm_distribution_array['created_date'] = date('Y-m-d H:i:s');

            $id = $this->Utilities_model->saveVasCommissionData($comm_distribution_array);
            // $comm_distribution_array['id'] = $id;
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'tata_docomo_special.log', $comm_distribution_array, 'Commission Distribution Response');

            return $comm_distribution_array;
        }
    }

    public function mobileOperators_post() {

        $data = array();
        $input = $this->post();

        if ($input['term'] == "" || $input['type'] == "") {
            $this->response(array('status' => 'Failed', 'data' => "", 'msg' => 'Please provide proper inputs.'), 200);
        } else {
            $aRes = $this->Utilities_model->getMobileOperators($input['term'], $input['type']);
            $this->response(array('status' => 'Success', 'data' => $aRes), 200);
        }
    }

       /**
     * @description - To process bill info
     * @param array of bill details
     * @added by Sachin on 14-08-2018 
     * @return int array
     */
       public function get_bill_post() {
        $data = array();
        $input = $this->post();
        $error_messages = json_decode(file_get_contents('messages.json'), true);
        if (!empty($input)) {
            $config = $this->Utilities_model->get_validation($input['type'], $input['operator_id']);

            if (form_validate_rules($config) == FALSE) {
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {
                $user_id = $input['user_id'];
                $recharge_from = $input['recharge_from'];
                $operator = $input['operator'];
                $operator_id = $input['operator_id'];
                $type = $input['type'];
                $number = $input['number'];
                $account_number = $input['account_number'];
                $comment = $input['comments'];
                $authenticator = $input['authenticator'];
                $recharge_amount = number_format($input['bill_amt'], 2, '.','');
                $input['recharge_amount'] = $recharge_amount;
                $bill_number_val = $input['bill_number'];
                
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'landline_bbps.log', $input, '1....print input');
                
                $getOperatorDetails = $this->Utilities_model->operatorDetailsByID($operator_id, $type);
                
                if (!empty($recharge_amount) && !empty($bill_number_val)) {
                    $check_user_wallet = $this->Utilities_model->getUserWalletDetails($user_id);

                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'landline_bbps.log', $check_user_wallet, '2.....check_user_wallet');
                    
                    if ($check_user_wallet[0]['amt'] > $recharge_amount) {

                        $recharge_response_check = bbps_recharge($number, $account_number, $comment, $getOperatorDetails, $operator, $recharge_amount, $authenticator);

                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'landline_bbps.log', $recharge_response_check, '3.....print');
                        
                        if ($recharge_response_check['check_result']['ERROR'] == 0) {
                            if ($recharge_response_check['pay_result']['ERROR'] == 0) {

                                $transaction_no = $recharge_response_check['pay_result']['TRANSID'];
                                $commissionResult = $this->serviceRetailerCommissionDistribution($input, $transaction_no);
//                                $updated_wallet_amt = $check_user_wallet[0]['amt'] - $commissionResult['trimax_final_amt'];
                                $updated_wallet_amt = $check_user_wallet[0]['amt'] - $recharge_amount;
                                $this->Utilities_model->updateUserWallet($user_id, $updated_wallet_amt);

                                $transaction_data = array(
                                    'type' => $type,
                                    'operator' => $operator,
                                    'recharge_on' => $number,
                                    'recharge_from' => $recharge_from,
                                    'last_transaction_amount' => isset($commissionResult['trimax_final_amt']) ? $commissionResult['trimax_final_amt'] :'0.00',
                                    'transaction_id' => $transaction_no,
                                    'created_by' => $user_id
                                );

//                                $this->Utilities_model->saveTransactionData($transaction_data);
                                $this->response(array('status' => 'Success', 'data' => $recharge_response_check, 'msg' => $error_messages['messages'][$recharge_response_check['pay_result']['ERROR']]), 200);
                            } else {
                                $this->response(array('status' => 'Failed', 'data' => $recharge_response_check, 'msg' => $error_messages['messages'][$recharge_response_check['pay_result']['ERROR']]), 200);
                            }
                        } else {
                            $this->response(array('status' => 'Failed', 'data' => $recharge_response_check, 'msg' => $error_messages['messages'][$recharge_response_check['check_result']['ERROR']]), 200);
                        }
                    } else {
                        $this->response(array('status' => 'Failed', 'data' => $recharge_response_check, 'msg' => 'Insufficient balance'), 200);
                    }
                } else {

                    // First call to get bill details

                    $recharge_response_check = bbps_recharge($number, $account_number, $comment, $getOperatorDetails, $operator, '', $authenticator);
                    if ($recharge_response_check['check_result']['ERROR'] == 0) {
                        $this->response(array('status' => 'Success', 'data' => $recharge_response_check), 200);
                    } else {
                        $this->response(array('status' => 'Failed', 'data' => $recharge_response_check, 'msg' => $error_messages['messages'][$recharge_response_check['check_result']['ERROR']]), 200);
                    }
                }
            }
        }
    }


    private function checkErrorsInJRIResponse($response, $type){

        $response_error = [];
        // if($type == 'M'){
            // show($status, 1);
        if(strpos($response['Status'], 'Recharge Unsuccessful') !== false){
            $status = explode('|', $response['Status']);
            $response_error['type'] = 'error';
            $response_error['msg'] = explode(':', $status[2])[1];
        }
        elseif(strpos($response['Status'], 'Error') !== false){
            $status = explode(':', $response['Status']);
            $response_error['type'] = 'error';
            $response_error['msg'] = $status[1];
        }elseif(strpos($response['Status'], 'Recharge Successful') !== false){
            $status = explode('|', $response['Status']);
            $response_error['type'] = 'success';
            $response_error['msg'] = $status[1];
            $response_error['tnx_id'] = $response['TransactionReference'];
        }else{
            $response_error['type'] = 'pending';
            $response_error['msg'] = 'Status Pending.';
        }
        // }

        return $response_error;
    }
// 
    public function datacard_recharge_post() {
        $data = array();
        $input = $this->post();
        // show($input, 1);
        $error_messages = json_decode(file_get_contents('messages.json'), true);
        if (!empty($input)) {

            $config = array(
                array('field' => 'mobile_operator', 'label' => 'Mobile Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter mobile operator.')),
                array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.')),
                array('field' => 'recharge_amount', 'label' => 'Recharge Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter recharge amount.')),
                array('field' => 'user_id', 'label' => 'Recharge Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter user id.')),
                array('field' => 'recharge_from', 'label' => 'Recharge From', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter recharge_from (eg. recharge_from: web|mobile ).'))
            );
            if (form_validate_rules($config) == FALSE) {
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {

                /* Check JRI Wallet Funds */
                $balance = $this->getJRIBal();
                if($balance->Balance == '0.00'){
                    $this->response(array('status' => 'Failed', 'data' => [], 'msg' => 'Insufficient Wallet balance'), 200);
                }

                $user_id = $input['user_id'];
                $recharge_from = $input['recharge_from'];
                $operator = $input['mobile_operator'];
                $type = $input['recharge_type'];
                $is_postpaid = $input['recharge_type'] == 'Prepaid Mobile' ? 'N' : 'Y';
                $mobile_number = $input['mobile_number'];
                $recharge_amount = $input['recharge_amount'];
                $comment = $input['comment'];
                // show($input, 1);
                $check_user_wallet = $this->Utilities_model->getUserWalletDetails($user_id);
                // show($check_user_wallet, 1);

                if ($check_user_wallet[0]['amt'] > $recharge_amount) {
                    $authenticate_JRI = authJRI();
                    $authenticate_JRI['mobile_number'] = $mobile_number;
                    $authenticate_JRI['operator'] = $operator;
                    $authenticate_JRI['recharge_amount'] = $recharge_amount;
                    $authenticate_JRI['service_type'] = 'R';
                    $authenticate_JRI['comments'] = $comment;
                    // $authenticate_JRI['is_postpaid'] = $is_postpaid;
                    // show($authenticate_JRI, 1);
                    $recharge_from_JRI = recharge_with_JRI($authenticate_JRI);

                    // show($recharge_from_JRI, 1);
                    
                    // $recharge_from_JRI = array(
                    //     "Amount" => "10",
                    //     "IsPostpaid" => "",
                    //     "Location" => "",
                    //     "MobileNo" => "9819293097",
                    //     "Provider" => "Vodafone",
                    //     "ServiceType" => "M",
                    //     "Status" => "0 | Recharge Successful",
                    //     "SystemReference" => "5bd9522865f5d",
                    //     "TransactionReference" => "MRC0032273193"
                    // );
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $recharge_from_JRI, 'JRI Recharge - API response');

                    $recharge_from_JRI['created_at'] = date_format(date_create(), 'Y-m-d h:i:s');
                    $recharge_from_JRI['recharge_full_status'] = trim($recharge_from_JRI['Status']);
                    $reason = '';
                    if(strpos($recharge_from_JRI['Status'], '|') !== false){
                        $reason = explode("|", $recharge_from_JRI['Status']);
                         if(isset($reason[1])){
                            $new_status = $reason[1];
                            if(count($reason) == 3){
                                $reason = explode(':', $reason[2]);
                                $reason = $reason[1];
                            }else{
                                $reason = $reason[1];
                            }
                        }else{
                            $reason = 'Error';
                            $new_status = 'Error';
                        }
                    }

                    
                    if(strpos($recharge_from_JRI['Status'], 'Error') !== false){
                        $reason = explode("Error:", $recharge_from_JRI['Status']);
                        $reason = isset($reason[1]) ? $reason[1] : 'Error';
                    }
                    
                    $recharge_from_JRI['reason'] = trim($reason);
                    $recharge_from_JRI['Status'] = trim($new_status);
                    // show($recharge_from_JRI, 1);
                    $this->Utilities_model->saveJRIData($recharge_from_JRI);

                    $check_errors = $this->checkErrorsInJRIResponse($recharge_from_JRI, 'M');
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $check_errors, 'JRI Recharge - JRI Errors Check');

                    if($check_errors['type'] == 'error'){
                        $this->response(array('status' => 'Failed', 'data' => $recharge_from_JRI, 'msg' => $recharge_from_JRI['reason']), 200);
                    }if($check_errors['type'] == 'pending'){
                        $this->response(array('status' => 'Failed', 'data' => $recharge_from_JRI, 'msg' => $recharge_from_JRI['reason']), 200);
                    }else{
                        $transaction_no = $check_errors['tnx_id'];
                        $wallet_tran_id = substr(hexdec(uniqid()), 4, 12);

                        $commissionResult = $this->serviceRetailerCommissionDistribution($input, $wallet_tran_id);
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $commissionResult, 'JRI Recharge - Commission result');


                        $updated_wallet_amt = $check_user_wallet[0]['amt'] - $recharge_amount;
                        $update_wallet = $this->Utilities_model->updateUserWallet($user_id, $updated_wallet_amt);

                        $wallet_trans_detail['w_id'] = $update_wallet[0]['id'];
                        $wallet_trans_detail['amt'] = $recharge_amount;
                        $wallet_trans_detail['comment'] = 'JRI Mobile Recharge';
                        $wallet_trans_detail['status'] = 'Debited';
                        $wallet_trans_detail['user_id'] = $user_id;
                        $wallet_trans_detail['added_by'] = $user_id;
                        $wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                        $wallet_trans_detail['amt_before_trans'] = $check_user_wallet[0]['amt'];
                        $wallet_trans_detail['amt_after_trans'] = $updated_wallet_amt;
                        $wallet_trans_detail['ticket_id'] = '';
                        // $wallet_trans_detail['transaction_type_id'] = '3';
                        $wallet_trans_detail['wallet_type'] = 'actual_wallet';
                        $wallet_trans_detail['transaction_type'] = 'Debited';
                        $wallet_trans_detail['is_status'] = 'Y';
                        // $wallet_trans_detail['wallet_tran_id'] = $wallet_tran_id;
                        $wallet_trans_detail['transaction_no'] = $transaction_no;

                        $wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail);

                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $update_wallet, 'JRI Recharge Response START');
                            // show($update_wallet, 1);

                        $transaction_data = array(
                            'type' => $type,
                            'operator' => $operator,
                            'recharge_on' => $mobile_number,
                            'recharge_from' => $recharge_from,
                            'last_transaction_amount' => $recharge_amount,
                            'transaction_id' => $transaction_no,
                            'wallet_tran_id' => $wallet_trans_id,
                            'created_by' => date_format(date_create(), 'Y-m-d h:i:s')
                        );

                            // show($recharge_from_JRI, 1);
                        $this->Utilities_model->saveTransactionData($transaction_data);

                        
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $recharge_from_JRI, 'JRI Recharge - End');

                        $this->response(array('status' => 'Success', 'data' => $recharge_from_JRI, 'msg' => $recharge_from_JRI['reason']), 200);
                    }

                } else {
                    $this->response(array('status' => 'Failed', 'data' => $recharge_from_JRI, 'msg' => 'Insufficient balance'), 200);
                }
            }
        }else{
            $this->response(array('status' => 'Failed', 'msg' => 'No Input Found.'), 200);
        }
    }


    public function getDataPlans_post(){
        // show($this->post(), 1);
        $popular_plans = fetchJRIPopularplans($this->post());
        // show($popular_plans, 1);

        $this->response($popular_plans);
        // echo json_encode($popular_plans);
    }

    public function jri_reverse_url_post(){

        $input = $_GET;
        $reverse_data = array();
        $reverse_data['TransactionReference'] = $input['TransactionReference'];
        $reverse_data['MobileNo'] = $input['MobileNo'];
        $reverse_data['Provider'] = $input['Provider'];
        $reverse_data['Amount'] = $input['Amount'];
        $reverse_data['ServiceType'] = $input['ServiceType'];
        $reverse_data['IsPostpaid'] = $input['IsPostpaid'];
        $reverse_data['SystemReference'] = $input['SystemReference'];
        $reverse_data['Status'] = $input['Status'];

        $this->common_model->insert('jri_reverse_push_data',$reverse_data);

        $this->response(['message' => 'Data saved successfully.']);

    }

    public function getJRIBal(){
        $details = authJRI();
        $input = [
            'CorporateId' => $details['CorporateId'],
            'SecurityKey' => $details['SecurityKey'],
            'AuthKey' => $details['AuthenticationKey']            
        ];
                // show($input, 1);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://spi.justrechargeit.com/JRICorporateRecharge.svc/GetCorporateCardBalance');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 50);
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($input));
        $header = array(
            'Content-Type: application/json'
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $httpCode = curl_getinfo($ch , CURLINFO_HTTP_CODE); // this results 0 every time
        $response = curl_exec($ch);
        if ($response === false) $response = curl_error($ch);


        // show($response, 1);
        // echo stripslashes($response);
        curl_close($ch);
        return json_decode($response);
    }


    /** Added by prabhat pal on 22-11-2018 */
    public function tso_dth_post() {

        $data = array();
        $input = $this->post();
        $user_id = $input['user_id'];
        $dth_from = $input['dth_from'];
        $operator = $input['dth_operator'];
        $customer_number = $input['customer_number'];
        $recharge_amount = $input['recharge_amount'];
        $recharge_from = $input['recharge_from'];
        $comment = $input['comment'];
        $rechargeType = $input['recharge_type'];
        $error_messages = json_decode(file_get_contents('messages.json'), true);
        
        if (!empty($input)) {

            $config = array(
                //  array('field' => 'recharge_type', 'label' => 'Recharge Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter recharge type.')),
                array('field' => 'dth_operator', 'label' => 'Dth Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Dth operator.')),
                array('field' => 'customer_number', 'label' => 'Customer Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please check Customer number.')),
                array('field' => 'recharge_amount', 'label' => 'Recharge Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter recharge amount.')),
                array('field' => 'user_id', 'label' => 'User ID', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter user id.')),
                array('field' => 'recharge_from', 'label' => 'Recharge From', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter recharge_from (eg. recharge_from: web|mobile ).'))
            );

            if (form_validate_rules($config) == FALSE) {
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {
                /* Check TSO Wallet Funds */
                $balance = $this->getTSOBal();
                
                if ($balance['STCODE'] != 0) {
                    $this->response(array('status' => 'Failed', 'data' => [], 'msg' => 'Insufficient Wallet balance', 'api_message' => $balance['STMSG']), 200);
                }
                $check_user_wallet = $this->Utilities_model->getUserWalletDetails($input['user_id']);
                
                if ($check_user_wallet[0]['amt'] > $recharge_amount) {

                    $recharge_from_tsoDth = recharge_with_TsoDth($input);
                    
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'tso_dth_recharge.log', $recharge_from_tsoDth, 'TSO DTH Recharge - API response');
                    $data_tsoDth['customer_number'] = $customer_number;
                    $data_tsoDth['amount'] = $recharge_amount;
                    $data_tsoDth['operator'] = $operator;
                    $data_tsoDth['request_type'] = 'DTH Rechagre';
                    $data_tsoDth['recharge_type'] = $rechargeType == 0 ? 'Topup' : 'Scheme';
                    $data_tsoDth['transaction_id'] =$recharge_from_tsoDth['TRNID'];
                    
                    if($recharge_from_tsoDth['STCODE'] == 0) {
                        $statusCode = 'Success';
                    } elseif($recharge_from_tsoDth['STCODE'] == 6) {
                        $statusCode = 'Request Accepted';
                    } elseif($recharge_from_tsoDth['STCODE'] == 1) {
                        $statusCode = 'Not Success';
                    } elseif($recharge_from_tsoDth['STCODE'] == 9) {
                        $statusCode = 'Error';
                    }
                    $data_tsoDth['status'] = $statusCode;
                    
                    if($recharge_from_tsoDth['TRNSTATUS'] == 1) {
                        $trnstatus = 'Success';
                    } elseif($recharge_from_tsoDth['TRNSTATUS'] == 2) {
                        $trnstatus = 'Provider Failed';
                    } elseif($recharge_from_tsoDth['TRNSTATUS'] == 3) {
                        $trnstatus = 'System Failed';
                    } elseif($recharge_from_tsoDth['TRNSTATUS'] == 4) {
                        $trnstatus = 'Hold';
                    } elseif($recharge_from_tsoDth['TRNSTATUS'] == 5) {
                        $trnstatus = 'Refunded';
                    } elseif($recharge_from_tsoDth['TRNSTATUS'] == 6) {
                        $trnstatus = 'Under Queue/Request Accepted';
                    }
                     
                    $data_tsoDth['trans_status'] = $trnstatus;
                    unset($trnstatus, $statusCode);
                    
                    $data_tsoDth['status_text'] = $recharge_from_tsoDth['STMSG'];
                    $data_tsoDth['trans_reference'] = $recharge_from_tsoDth['trans_reference'];
                    $data_tsoDth['created_at'] = date_format(date_create(), 'Y-m-d H:i:s');
                    
                    $this->Utilities_model->saveTSODTHData($data_tsoDth);
                    
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'tso_dth_recharge.log', $check_errors, 'TSO DTH  Recharge - TSO DTH Errors Check');
                    
                    if ($recharge_from_tsoDth['STCODE'] == 1 || $recharge_from_tsoDth['STCODE'] == 9) {
                        $this->response(array('status' => 'Failed', 'data' => $recharge_from_tsoDth, 'msg' => $recharge_from_tsoDth['STMSG']), 200);
                    } else {
                        $transaction_no = $recharge_from_tsoDth['TRNID'];
                        $wallet_tran_id = substr(hexdec(uniqid()), 4, 12);
                        
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'tso_dth_recharge.log', $commissionResult, 'TSO DTH Recharge - Commission result');

                        $updated_wallet_amt = $check_user_wallet[0]['amt'] - $recharge_amount;
                        $update_wallet = $this->Utilities_model->updateUserWallet($user_id, $updated_wallet_amt);

                        $wallet_trans_detail['w_id'] = $update_wallet[0]['id'];
                        $wallet_trans_detail['amt'] = $recharge_amount;
                        $wallet_trans_detail['comment'] = 'TSO DTH Recharge';
                        $wallet_trans_detail['status'] = 'Debited';
                        $wallet_trans_detail['user_id'] = $user_id;
                        $wallet_trans_detail['added_by'] = $user_id;
                        $wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                        $wallet_trans_detail['amt_before_trans'] = $check_user_wallet[0]['amt'];
                        $wallet_trans_detail['amt_after_trans'] = $updated_wallet_amt;
                        $wallet_trans_detail['ticket_id'] = '';
                        $wallet_trans_detail['wallet_type'] = 'actual_wallet';
                        $wallet_trans_detail['transaction_type'] = 'Debited';
                        $wallet_trans_detail['is_status'] = 'Y';
                        //$wallet_trans_detail['wallet_tran_id'] = $wallet_tran_id;
                        $wallet_trans_detail['transaction_no'] = $transaction_no;

                        $wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail);

                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'tso_dth_recharge.log', $update_wallet, 'TSO DTH  Recharge Response START');

                        $transaction_data = array(
                            'type' => 'DTH',
                            'operator' => $operator,
                            'recharge_on' => $customer_number,
                            'recharge_from' => $recharge_from,
                            'last_transaction_amount' => $recharge_amount,
                            'transaction_id' => $transaction_no,
                            'wallet_tran_id' => $wallet_tran_id,
                            'created_by' => date_format(date_create(), 'Y-m-d h:i:s')
                        );
                        
                        $this->Utilities_model->saveTransactionData($transaction_data);

                        unset($data_tsoDth, $wallet_trans_detail, $transaction_data);
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'TSO_DTH_recharge.log', $recharge_from_tsoDth, 'TSO DTH Recharge - End');

                        $this->response(array('status' => 'Success', 'data' => $recharge_from_tsoDth, 'msg' => $recharge_from_tsoDth['STMSG']), 200);
                    }
                } else {
                    $this->response(array('status' => 'Failed', 'data' => $check_user_wallet[0]['amt'], 'msg' => 'Insufficient balance'), 200);
                }
            }
        } else {
            $this->response(array('status' => 'Failed', 'data' => $input, 'msg' => 'No Input Found'), 200);
        }
    }


      /* Added by prabhat Pal on 22-11-2018*/
    public function getTsoBal() {
        return authTSO();
               
    }

    public function checkMNPJRI_post(){
        $mnp_details = fetchMNPDetails($this->post());
        $this->response($mnp_details);
    }



}
