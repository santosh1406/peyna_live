<?php

//Created by pooja kambali on 27-09-2018

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

use Blocktrail\CryptoJSAES\CryptoJSAES;

require APPPATH . '/libraries/CryptoJSAES.php';

class Mamt_VPN extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->sessionData["id"] = $this->session->userdata('user_id');
        $this->sessionData["username"] = "9000000000";
        $this->sessionData["password"] = "BoS@S123";

        $this->header = array(
            'Content-Type: multipart/form-data; charset=utf-8',
            'X-API-KEY: ' . X_API_KEY . '',
        );
    }

    public function cash_withdraw_post() {
        //error_reporting(1);
        //  @ini_set('display_errors', 1);

        $header_key = "982b0d01-b262-4ece-a2a2-45be82212ba1";
        $body_key = "8329fffc-b192-4c9b-80cf-ccbbdf48bd67";

        $data = array();
        $input = $this->post();
        //print_r($input);die;
        //echo 'gfdd';die;
        if (!empty($input)) {
            $config = array(
                array('field' => 'Amount', 'label' => 'Transaction Amount', 'rules' => 'trim|required|numeric|xss_clean|callback_check_acc_no', 'errors' => array('required' => 'Please Enter Transfer amount.')),
            );

            if (form_validate_rules($config) == FALSE) {
                //  die('pooja');
                $error = $this->form_validation->error_array();
                //print_r($error);exit;
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {

                
                $data['MerchantId'] = '9888661548'; 
                $data['SERVICEID'] = '156';
                $data['RETURNURL'] = "http://10.15.20.131:8023/PG/TestPage.aspx";
                $data['Version'] = '1000';
                $data['Amount'] = '100';
                $data['ClientRefID'] = "Bcapi2077";


                //echo "dfdf";exit;
                $res = json_encode($data);

                $encr_res = $this->encrypt_matm($res, $body_key);

                if (!empty($input)) {

                    //$curl_url = "http://10.15.20.131:8023/PG/PgHandler.ashx";
                    $curl_url = MATM_VPN_URL;

                    $credentials = $this->encrypt_matm('{"ClientId":40,"AuthKey":"bff9a593-8aae-4d82-9f07-c6513fd8cff7"}', $header_key);
                    $curlResult = curlForPostData_MatmVPN($curl_url, $encr_res, $credentials);
                    //print_r($curlResult);exit;
                    //  $a=$this->decrypt_fino($curlResult['ResponseData']);
                    //print_r($a);exit;
                    echo $curlResult;
                }
            }
        }
    }

    public function transaction_status_post() {
        // error_reporting(1);
        //@ini_set('display_errors', 1);

        $header_key = "982b0d01-b262-4ece-a2a2-45be82212ba1";
        $body_key = "8329fffc-b192-4c9b-80cf-ccbbdf48bd67";

        $data = array();
        $input = $this->post();
        //print_r($input);die;

        if (!empty($input)) {
            $config = array(
                array('field' => 'ClientUniqueID', 'label' => 'ClientUniqueID', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Client id.'))
            );


            if (form_validate_rules($config) == FALSE) {
                
                $error = $this->form_validation->error_array();
                //print_r($error);exit;
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {


                $data['ClientUniqueID'] = $input['ClientUniqueID'];
                //  print_r($data['ClientUniqueID']);
                $res = json_encode($data);

                $encr_res = $this->encrypt_matm($res, $body_key);

                $curl_url = DMT_URL . "txnstatusrequest";
                //$curl_url="http://10.15.20.131:8061/Uiservice.svc/FinoMoneyTransactionApi/txnstatusrequest";

                $credentials = $this->encrypt_fino('{"ClientId":40,"AuthKey":"bff9a593-8aae-4d82-9f07-c6513fd8cff7"}', $header_key);
                $curlResult = curlForPostData_fino($curl_url, $encr_res, $credentials);
                //print_r($curlResult);exit;
                echo $curlResult;
            }
        }
    }

    function encrypt_matm($text, $passphrase) {
        $encrypted = CryptoJSAES::encrypt($text, $passphrase);
        return $encrypted;
        //var_dump("Encrypted: ", $encrypted);  
    }

    function decrypt_matm($text, $passphrase) {
        $decrypted = CryptoJSAES::decrypt($text, $passphrase);
        return $decrypted;
        //var_dump("Decrypted: ", $decrypted);
    }

    function check_acc_no($a) {

        $value = (int) $a;
        if ($value > 0) {
            //return "please enter valid Account Number";
            return true;
        }

        return false;
    }

    function check_type($type) {

        //$value=(int)$a;
        if ($type == 'Imps' || $type == 'Neft') {
            //return "please enter valid Account Number";
            return true;
        }

        return false;
    }

    function check_recharge_from($r) {

        if ($r == 'mobile' || $r == 'web') {
            //return "please enter valid Account Number";
            return true;
        }

        return false;
    }

}
