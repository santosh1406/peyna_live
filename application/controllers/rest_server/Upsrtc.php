<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Upsrtc extends REST_Controller
{
    var $op_id,$db2;
	function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('up_db', TRUE);
        $this->load->library('soap_api/upsrtc_api');

         $lib = array();
        foreach (glob(APPPATH."libraries/soap_api/"."*.php") as $filename) {
            $path = 'soap_api/'.strtolower(basename($filename,'.php'));
            array_push($lib, $path );
        }

        $this->load->library( $lib );
        

        // show(getallheaders(),1,'api');
        
        // Configure limits on our controller methods. Ensure
        // you have created the 'limits' table and enabled 'limits'
        // within application/config/rest.php

       // $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
       // $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
       // $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key
        $this->op_id = 2;

        /*
         *  To load all the library in library soap_api
         */

        $lib = array();
        foreach (glob(APPPATH."libraries/soap_api/"."*.php") as $filename) {
            $path = 'soap_api/'.strtolower(basename($filename,'.php'));
            array_push($lib, $path );
        }
    }

    
    function getAllBusType_post()
    {

        $this->load->model(array(
                                    'bus_type_model' => 'bus_type',
                                    'op_bus_types_model' => 'op_bus_type'
                                )
                        );

        $bus = $this->op_bus_type
                        ->column("op_bus_type_name as busName, op_bus_type_cd as busCode")
                        // ->where('id',100000)
                        ->find_all();

        if($bus)
        {
            $this->response($bus, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'No bus exists'), 404);
        }
    }
    
    
    function getAvailableServices_post()
    {
        $res = $this->input->post();
        $this->load->model(array(
                                    'travel_destinations_model' => 'travel_destinations'
                                )
                          );
        
        
        $data = array();
        
        $this->db->select("om.op_name,t.trip_no,tm.op_id, bsm.bus_stop_name,tm.op_trip_no,osl.layout_name, tm.seat_layout_id,"
                . "concat(t.sch_departure_date, ' ', t.sch_departure_tm) as dept_tm, DATE_FORMAT(t.sch_departure_date, '%d-%m-%Y') as  sch_departure_date,"
                . " t.arrival_date, t.arrival_tm, t.day,bt.bus_type_id,bt.bus_type_name,"
                . "t.sch_departure_tm,t.arrival_tm,bs.op_bus_type_name,if(ISNULL(t.tot_seat_available),osl.ttl_seats,t.tot_seat_available) as tot_seat_available, t.seat_fare,"
                ."if(t.day>0,t.day -1, t.day) as arrival_day," 
                ."concat(date_add(t.sch_departure_date,interval if(t.day>0,t.day -1, t.day) day ),' ',t.arrival_tm) as arrival_time", false);

        $this->db->from("travel_destinations t");
        $this->db->join("trip_master tm", "tm.trip_no = t.trip_no", "inner");
        $this->db->join("bus_stop_master bsm", "t.destination_stop_id = bsm.bus_stop_id", "inner");
        $this->db->join("op_master om", "om.op_id = tm.op_id", "inner");
        $this->db->join("op_bus_types bs", "bs.op_bus_type_id = tm.op_bus_type_id", "inner");
        $this->db->join("op_seat_layout osl", "osl.seat_layout_id = tm.seat_layout_id", "inner");
        $this->db->join("bus_type bt", "bt.bus_type_id = bs.bus_type_id", "left");
        $this->db->join("op_stop_master osm", "bsm.bus_stop_id = osm.bus_stop_id");


        $this->db->where("DATE_FORMAT(t.sch_departure_date,'%Y-%m-%d')", date("Y-m-d",strtotime($res['date'])));
        // $this->db->where("t.boarding_stop_name", $res['from']);
        // $this->db->where("t.destination_stop_name", $res['to']);
        $this->db->where("osl.op_id",$this->op_id);

        if (isset($res['trip_no']) && count($res['trip_no']) != "") {
            $this->db->where('tm.trip_no', $res['trip_no']);
        }
        $this->db->group_by("t.trip_no");

        $query = $this->db->get();
        
        $data = $query->result_array();
        
        if($data)
        {
            $this->response($data, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'No bus exists'), 404);
        }
    }
    
    // getBusServiceStops
    function getBusServiceStops_post()
    {
        $res = $this->input->post();
        $this->db->select("td.trip_no, osm.op_stop_name as stop_name, osm.op_stop_cd as stop_code, bsm.tahsil_nm, "
                . "bsm.district_nm, bsm.state_nm,osm2.op_stop_name as stop_name2, osm2.op_stop_cd as stop_code2, "
                . "bsm2.tahsil_nm as tahsil_nm2, bsm2.district_nm as district_nm2, bsm2.state_nm as state_nm2,"
                . "td.sch_departure_date,td.sch_departure_tm,td.arrival_date,td.arrival_tm,td.day,"
                . " td.seat_fare,td.sleeper_fare,td.child_seat_fare");
        $this->db->from("travel_destinations td");
        $this->db->join("op_stop_master osm","osm.bus_stop_id = td.boarding_stop_id");
        $this->db->join("op_stop_master osm2","osm2.bus_stop_id = td.destination_stop_id");
        $this->db->join("bus_stop_master bsm","bsm.bus_stop_id = osm.bus_stop_id");
        $this->db->join("bus_stop_master bsm2","bsm2.bus_stop_id = osm2.bus_stop_id");
        $this->db->where(array("osm.op_id"=>$this->op_id, 'td.trip_no'=>$res['service_id']));
        $query  = $this->db->get();
        $bus_stops_data['busServiceStop']  = $query->result_array();
        
        $bus_stops_details = array();
        
        foreach($bus_stops_data['busServiceStop'] as $key=>$val) {
            
            $bus_stops_details['busServiceStop'][$key]['serviceId']    = $val['trip_no'];
            
            $bus_stops_details['busServiceStop'][$key]['from_stop']    = array('busStopName'  =>  $val['stop_name'],
                                                                               'busStopcode'  =>  $val['stop_code'],
                                                                               'taluka'       =>  $val['tahsil_nm'],
                                                                               'district'     =>  $val['district_nm'],
                                                                               'state'        =>  $val['state_nm']
                                                                              );
            
            $bus_stops_details['busServiceStop'][$key]['to_stop']      = array('busStopName'  =>  $val['stop_name2'],
                                                                               'busStopcode'  =>  $val['stop_code2'],
                                                                               'taluka'       =>  $val['tahsil_nm2'],
                                                                               'district'     =>  $val['district_nm2'],
                                                                               'state'        =>  $val['state_nm2']
                                                                              );
            
            $bus_stops_details['busServiceStop'][$key]['departureTime'] = $val['sch_departure_date']." ".$val['sch_departure_tm'];
            $bus_stops_details['busServiceStop'][$key]['arrivalTime']   = $val['arrival_date']." ".$val['arrival_tm'];
            $bus_stops_details['busServiceStop'][$key]['day']           = $val['day']   ;
            $bus_stops_details['busServiceStop'][$key]['fare']          = $val['seat_fare']   ;
            $bus_stops_details['busServiceStop'][$key]['sleeperFare']   = $val['sleeper_fare']   ;
            $bus_stops_details['busServiceStop'][$key]['childFare']     = $val['child_seat_fare']   ;
            
        }
        
       // show($bus_stops_details,1);
        
        if($bus_stops_details)
        {
            $this->response($bus_stops_details, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'No data found'), 404);
        }
    }
    
    // getAllBusFormats    
    function getSeatLayout_post() {
        $res = $this->input->post();
        
        // Get the detail of bus row count and column count
        
        $this->db->select('tm.op_bus_type_id, tm.seat_layout_id, obs.op_bus_type_name,osl.no_rows,osl.no_cols');
        $this->db->from('trip_master tm');
        $this->db->join('op_seat_layout osl','osl.seat_layout_id = tm.seat_layout_id');
        $this->db->join('op_bus_types obs','obs.op_bus_type_id = tm.op_bus_type_id');
        $this->db->where(array('osl.layout_name'=>$res['layout_name'],'osl.op_id'=>$this->op_id));
        $query = $this->db->get();
        $bus_data = $query->row_array();
        
        //Get the seat layout
        
        $this->db->select('old.seat_no, old.seat_type, old.row_no, old.col_no, old.quota_type');
        $this->db->from('op_layout_details old');
        $this->db->where('old.op_seat_layout_id',$bus_data['seat_layout_id']);
        
        $query = $this->db->get();
        $seat_layout = $query->result_array();
        $data['busDetail'] = $bus_data;
        $data['seatLayout'] = $seat_layout;
        if($data)
        {
            $this->response($data, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'No data found'), 404);
        }
        
    }
    
    
    function getSeatAvailability_post()
    {
        $res = $this->input->post();
        $data = $this->upsrtc_api->seatAvailability($res);
        
        if($data)
        {
            $this->response($data, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'No data found'), 404);
        }
    }
    
    function getcbBoardingStops_post() {
        $res  = $this->input->post();
       
        $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
                    <soapenv:Header/>
                    <soapenv:Body>
                       <ser:getBoardingStops>
                          <!--Optional:-->
                          <GetBoardingStopsRequest>
                             <!--Optional:-->
                             <authentication>
                                <!--Optional:-->
                                <userName>bos</userName>
                                <!--Optional:-->
                                <password>bos@123</password>
                                <!--Optional:-->
                                <userType>1</userType>
                             </authentication>
                             <serviceId>'.$res['serviceId'].'</serviceId>
                             <!--Optional:-->
                             <fromStopId>'.$res['fromStopCode'].'</fromStopId>
                             <!--Optional:-->
                             <toStopId>'.$res['toStopCode'].'</toStopId>
                             <!--Optional:-->
                             <typeAliBrd>'.$res['brd'].'</typeAliBrd>
                             <!--Optional:-->
                             <busTypeCode>'.$res['busTypeCD'].'</busTypeCode>
                          </GetBoardingStopsRequest>
                       </ser:getBoardingStops>
                    </soapenv:Body>
                 </soapenv:Envelope>' ;
       // show($xml_str,1);
        $data = get_soap_data($this->config->item("upsrtc_wsdl_url"), 'getBoardingStops', $xml_str);
        if($data)
        {
            $this->response($data, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'No data found'), 404);
        }
    }
    
    
    // current Booking function 
    /*method:getCbServices_post
     * author:pankaj
     *fetch current booking bus services
     * 
     */
    function getCbServices_post()
    {
                
    $cur_date      = $this->input->post();
    $current_date  = strtotime(date('Y-m-d'));
    $pass_date   = strtotime($cur_date['date']);
    
    if($cur_date['date']!="")
    {
        if((preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/",$cur_date['date'])) && $pass_date==$current_date)
            {
//        $sql= "select cb.bus_service_no,rs.route_no,concat(bus_st.BUS_STOP_NM,'To ',bus_st1.BUS_STOP_NM,IF(rs.VIA_STOP_CD IS NULL,'',concat(' VIA ',bus_st2.BUS_STOP_NM))) as route_name,
//        cb.bus_type_cd,bt.bus_type_nm,cb.DEPT_TM as Departure_time,cb.VEHICLE_NO,cb.CONDUCTOR_NM
//        from current_booking_buses cb
//        inner join bus_services  bs on cb.bus_Service_no=bs.bus_service_no
//        inner join routes rs on 
//        bs.route_no=rs.route_no
//        inner join bus_types bt on  bt.BUS_TYPE_CD=cb.BUS_TYPE_CD
//        inner join bus_stops bus_st
//        on  rs.from_stop_cd=bus_st.bus_stop_cd
//
//        inner join bus_stops bus_st1 on   rs.till_stop_cd=bus_st1.bus_stop_cd
//        LEFT join bus_stops bus_st2
//        on  rs.via_stop_cd=bus_st2.bus_stop_cd
//        where cb.dsa_no is NULL and cb.dept_tm > now()
//        and cb.booking_status='C' AND DATE(cb.dept_tm)=	'".$cur_date['date']."' ";
        
                $this->db2->select('cb.bus_service_no,rs.route_no,cb.bus_type_cd,bt.bus_type_nm,cb.DEPT_TM as Departure_time,cb.VEHICLE_NO,cb.CONDUCTOR_NM,concat(bus_st.BUS_STOP_NM, "To ",bus_st1.BUS_STOP_NM,IF(rs.VIA_STOP_CD IS NULL," ",concat(" VIA ",bus_st2.BUS_STOP_NM))) as route_name',FALSE);
                $this->db2->from("current_booking_buses cb");   
                $this->db2->join("bus_services bs","cb.bus_Service_no=bs.bus_service_no","inner");
                $this->db2->join("routes rs","bs.route_no=rs.route_no","inner");
                $this->db2->join("bus_types bt","bt.BUS_TYPE_CD=cb.BUS_TYPE_CD","inner");
                $this->db2->join("bus_stops bus_st","rs.from_stop_cd=bus_st.bus_stop_cd","inner");
                $this->db2->join("bus_stops bus_st1","rs.till_stop_cd=bus_st1.bus_stop_cd","inner");
                $this->db2->join("bus_stops bus_st2","rs.via_stop_cd=bus_st2.bus_stop_cd","left");
                $this->db2->where("cb.dsa_no",NULL);
                $this->db2->where("cb.dept_tm > now()");
                $this->db2->where("cb.booking_status","C");       
                $this->db2->where('DATE(cb.dept_tm)',date("Y-m-d",strtotime($cur_date['date'])));
                $query= $this->db2->get();
                $data=$query->result_array();
                foreach($data as $key=>$value)
                    {
                        $data[$key]['op_name']='UPSRTC';
                    }
       
        }
        else
        {
            $data =  array("error"=>"Either the date in not in format of dd-mm-yyyy or Please provide current date!");
        }
    }
    else
      {
         $data =  array("error"=>"Please provide Date");
      }
       if($data)
            {
                $this->response($data, 200); // 200 being the HTTP response code
            }
          else
            {
                $this->response(array('error' => 'No data found'), 404);
            }
    }
    
    
    
    function getfare_post()
        {
           $input=$this->input->post();
          
           

            $this->load->library('upsrtc_booking/calfunction.php');
            $paraArray['PROC']          =$input['proc'];
            $paraArray['DEPT_DT']       = $input['dept_dt'];
            $paraArray['BUS_SERVICE_NO'] =$input['bus_service_no'] ;
            $paraArray['ROUTE_NO']       =$input['route_no'] ;
       // $paraArray['BRD_STOP'] =$this->op_stop_master_model->bus_stop_cd($input['brd_stop'],$input['op_id']);
       // $paraArray['ALT_STOP'] =$this->op_stop_master_model->bus_stop_cd($input['alt_stop'],$input['op_id']);
            $paraArray['BRD_STOP']        =$input['boarding_stop'];
            $paraArray['ALT_STOP']        =$input['alighting_stop'];
            $paraArray['NO_PAX']          = 1;
            $paraArray['PAY_MODE']        =1;
            $paraArray['CAL_METHOD']      =$input['cal_method'];
            $paraArray['PAX_CONC']        ='A';
            $paraArray['PAX_AGE']=30;
            $paraArray['BUS_TYPE_CD'] =$input['bus_type'];
            $paraArray['BUS_TYPE_CD'] = 'ORD';
            $fareObj = new Calfunction();
            $fare['cal_fare'] = $fareObj->getFare($paraArray); 
     
         if($fare)
        {
            $this->response($fare, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'No data found'), 404);
        }
        
        }
    
     //  gets bus  stop
    function getcbbusstops_post()
    {
        $this->db2->from('bus_stops');
       
        $data=  $this->db2->get();
       
        $res=$data->result_array();
        
        foreach($res as $key=>$value)
        {
            $res[$key]['op_name']='UPSRTC';
        }
       
        
       
         if($res)
        {
            $this->response($res, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'No data found'), 404);
        }
        
        
    }
    
    
    function getcbroutes_post()
    {
        $this->db2->from('routes');     
        $data=  $this->db2->get();
       
        $res=$data->result_array();
        
        foreach($res as $key=>$value)
        {
            $res[$key]['op_name']='UPSRTC';
        }
       
         if($res)
        {
            $this->response($res, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'No data found'), 404);
        }
        
    }
    
    
    function getcbroutestops_post()
    {
        
        $input=$this->input->post();
        
        if($input['route_no']!="")
        {
        $this->db2->select('*'); 
        $this->db2->where('route_no',$input['route_no']);
        $this->db2->from('routes');
        $data=$this->db2->get();
        $cnt_route=$data->result_array();
        
            if(count($cnt_route)==0)
                {
                    $res=array("Route Number  does not exist");
                }
               else
               {    $this->db2->from('route_stops');
                    $this->db2->where('route_no',$input['route_no']);
                    $data   =  $this->db2->get();
                    $res    =  $data->result_array();
                if(!empty($res))
                    {
                    foreach($res as $key=>$value)
                        {
                            $res[$key]['op_name']='UPSRTC';
                        }
        
                    }
                }            
        }
        else
        {
             $this->response(array('error' => 'Please provide Route Number')); 
        }
         if($res)
        {
            $this->response($res, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'No data found'), 404);
        }
        
    }
    
    //get bus service wise route_stops
    function cbservicewisestops_post()
    {
         $input=$this->input->post();
        if($input['route_no']!="" || $input['bus_type']!="")
        {
       
        $this->db2->select('*'); 
        $this->db2->where('route_no',$input['route_no']);
        $this->db2->from('routes');
        $data=$this->db2->get();
        $cnt_route=$data->result_array();
           
        
        $this->db2->select('*'); 
        $this->db2->where('bus_type_cd',$input['bus_type']);
        $this->db2->from('bus_types');
        $data=$this->db2->get();
        $cnt_bus_types=$data->result_array();
        
        
        if(count($cnt_route)==0 || count($cnt_bus_types)==0)
        {
            $res=array("Route Number or Bus type does not exist");
        }
        else
        {
        $this->db2->from('service_route_stops');
        $this->db2->where('route_no',$input['route_no']);
        $this->db2->where('bus_type_cd',$input['bus_type']);
        $data=  $this->db2->get();
        $res=$data->result_array();
        if(!empty($res))
        {
          foreach($res as $key=>$value)
                {
                    $res[$key]['op_name']='UPSRTC';
                } 
        }
                 
        }
        }
        else
        {
          $this->response(array('error' => 'Please provide Route Number'));   
        }
        
            
        
         if($res)
        {
            $this->response($res, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'No data found'), 404);
        }
    }
    
    function getallbusservices_post()
    {
             $this->db2->from('bus_services');
             $this->db2->where('EFFECTIVE_TILL > now()');
            
            $data=  $this->db2->get();
            $this->db2->last_query();
       
            $res=$data->result_array();
            foreach($res as $key=>$value)
        {
            $res[$key]['op_name']='UPSRTC';
        }
            
            
       
            if($res)
                {
                    $this->response($res, 200); // 200 being the HTTP response code
                }
            else
               {
                    $this->response(array('error' => 'No data found'), 404);
               }
        
    }
    function getallbustypes_post()
    {
        $this->db2->from('bus_types');
        $data=  $this->db2->get();
         $res=$data->result_array();
        foreach($res as $key=>$value)
        {
            $res[$key]['op_name']='UPSRTC';
        }
       
         if($res)
            {
                $this->response($res, 200); // 200 being the HTTP response code
            }
         else
            {
                $this->response(array('error' => 'No data found'), 404);
            }
    }
    
    
    //get bus service stops
    
         function getbuservicestop_post()
          {
            
             
              $this->user_id 		 ='bos';
              $this->pass    		 ='bos@123';
              $this->user_type	 = 1;
              $this->url     		 = $this->config->item("upsrtc_wsdl_url");
              
              $service_data = $this->input->post();
              
             
              $service_id=$service_data['service_id'];
              $dept_date=date('d/m/Y',strtotime($service_data['date']));
             
              
              $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
                     <soapenv:Header/>
                     <soapenv:Body>
                        <ser:getBusServiceStops>
                           <!--Optional:-->
                           <BusServiceStopsRequest>
                              <!--Optional:-->
                              <authentication>
                                 <!--Optional:-->
                                 <userName>'.$this->user_id.'</userName>
                                 <!--Optional:-->
                                 <password>'.$this->pass.'</password>
                                 <!--Optional:-->
                                 <userType>'.$this->user_type.'</userType>
                              </authentication>
                              <serviceId>'.$service_id.'</serviceId>
                              <!--Optional:-->
                              <dateOfJourney>'.$dept_date.'</dateOfJourney>
                           </BusServiceStopsRequest>
                        </ser:getBusServiceStops>
                     </soapenv:Body>
                  </soapenv:Envelope>';
        
                    $data = get_soap_data($this->url,  'getBusServiceStops', $xml_str);
                
                if($data)
                    {
                        $this->response($data, 200); // 200 being the HTTP response code
                    }
                 else
                    {
                       $this->response(array('error' => 'No data found'), 404);
                    }
                     
            }

    function tempBooking_post1()
    {
        $input = $this->input->post();
       
        $data = $this->upsrtc_api->temporary_booking($input);
       // show($data,1);

        $this->db->select("om.op_name,t.trip_no,tm.op_id, bsm.bus_stop_name,tm.op_trip_no,tm.seat_layout_id,"
                . "t.boarding_stop_name as from_stop, t.destination_stop_name as to_stop,"
                . "concat(t.sch_departure_date, ' ', t.sch_departure_tm) as dept_tm, DATE_FORMAT(t.sch_departure_date, '%d-%m-%Y') as  sch_departure_date,"
                . " t.arrival_date, t.arrival_tm, t.day,bt.bus_type_id,bt.bus_type_name,"
                . "t.sch_departure_tm,t.arrival_tm,bs.op_bus_type_name,if(ISNULL(t.tot_seat_available),osl.ttl_seats,t.tot_seat_available) as tot_seat_available, t.seat_fare,"
                ."if(t.day>0,t.day -1, t.day) as arrival_day," 
                ."t.arrival_date as arrival_time", false);
        $this->db->from("travel_destinations t");
       $this->db->join("travel_destinations t2", "t.boarding_stop_id = t2.boarding_stop_id", "inner");
      
        $this->db->join("trip_master tm", "tm.trip_no = t.trip_no", "inner");
        $this->db->join("bus_stop_master bsm", "t.destination_stop_id = bsm.bus_stop_id", "left");
        $this->db->join("op_master om", "om.op_id = tm.op_id", "inner");
       $this->db->join("op_bus_types bs", "bs.op_bus_type_id = tm.op_bus_type_id", "inner");
        $this->db->join("op_seat_layout osl", "osl.seat_layout_id = tm.seat_layout_id", "inner");
        $this->db->join("bus_type bt", "bt.bus_type_id = bs.bus_type_id", "left");
        $this->db->join("op_stop_master osm", "bsm.bus_stop_id = osm.bus_stop_id");
        
      // $this->db->where("DATE_FORMAT(t.sch_departure_date,'%Y-%m-%d')", date("Y-m-d",strtotime($input['date'])));
        $this->db->where("DATE_FORMAT(t.sch_departure_date,'%Y-%m-%d') = '2015-06-20'");
      
         $this->db->where("t.boarding_stop_name", $input['from_stop']);
        $this->db->where("t.destination_stop_name", $input['to_stop'] );

       // $this->db->where("t.boarding_stop_name", "Charbagh");
       //$this->db->where("t.destination_stop_name",  "Allahabad" );

        $this->db->where('tm.op_trip_no', $input['service_id']); 
        $this->db->group_by("t.trip_no");
        $main_query = $this->db->get();
       $all_records= $main_query->result_array();  
        
      
        if($data['status']!="FAILURE")
        { 
           
                $ticket_no = $this->generateid_model->getRequiredId('processed_tickets');
                $gen_data = array('ID_CATEGORY' => 'processed_tickets', "CREATED_DATE" => date("Y-m-d H:i:s"));
                $this->db->insert("generateid", $gen_data);

                $ticket_no=$this->db->insert_id();

                $process_data_array=array(
                                            'ticket_no'=>$ticket_no,
                                            'ticket_type'=>'1',
                                            'request_type'=>'B',
                                            'ticket_status'=>'A',
                                            'bus_service_no'=>$input['service_id'], 
                                            'dept_time'=>$all_records[0]['dept_tm'],
                                            'boarding_time'=>$all_records[0]['sch_departure_date'], 
                                            'alighting_time'=>$all_records[0]['arrival_date'], 
                                            'from_stop_cd'=>$input['from_stop'], 
                                            'till_stop_cd'=>$input['to_stop'], 
                                            'from_stop_name'=>$all_records[0]['from_stop'],
                                            'till_stop_name'=>$all_records[0]['to_stop'], 
                                            'boarding_stop_cd'=>$input['from_stop'],
                                            'destination_stop_cd'=>$input['to_stop'], 
                                            'num_passgr'=>count($input['passenger']), 
                                            'cancellation_rate'=>'0',
                                            'issue_time'=>date('Y-m-d h:m:s:'), 
                                            'pay_id'=>'1',
                                            'session_no'=>'1', 
                                            'op_name'=>$input['op_name'],
                                            'user_email_id'=>$input['passenger'][0]['email'],
                                            'mobile_no'=>$input['passenger'][0]['mobile_no'],
                                            'inserted_date'=>date('Y-m-d h:m:s') ,
                                            'inserted_by'=>'1200', 
                                            'status'=>'Y'
                                            );


                $this->db->insert('processed_tickets', $process_data_array);
                $pro_result	= $this->db->insert_id();
                foreach($input['passenger'] as $key=>$value){
                    $i=1;
                $processed_details_array=array('ticket_no'=>$ticket_no, 'psgr_no'=>$i, 
                                                'psgr_name'=>$value['name'], 'psgr_age'=>$value['age'],
                                                'psgr_sex'=>$value['sex'], 
                                                'psgr_type'=>'A', 'concession_cd'=>'1', 
                                                'concession_rate'=>'1', 'discount'=>'1', 
                                                'is_home_state_only'=>'1',
                                                'fare_amt'=>$all_records[0]['seat_fare'], 
                                                'seat_no'=>$value['seat_no'],
                                                'seat_status'=>'Y', 
                                                'concession_proff'=>'1', 
                                                'proff_detail'=>'1', 
                                                'status'=>'Y');
                $this->db->insert('processed_ticket_details', $processed_details_array);
                $pro_details_result	= $this->db->insert_id();
                $i++;
            }
                $result='Data inserted in the processed_tickets & processed_tickets_details tables sucessfully ';
            $this->response($data, 200); // 200 being the HTTP response code
                      
        }
        else
        {
            foreach($input['passenger'] as $key=>$value){
                  $list[] = array($value['name'],$value['age'],$value['sex'],$value['mobile_no'],
                                $value['email'],$value['seat_no']);
              }
           $to_date=date('d-m-Y');
            $file = fopen("./uploads/processed_fail_transaction_".$to_date.".csv","a");

            foreach ($list as $line)
              {
              fputcsv($file,$line);
              }
             
            fclose($file);
            $this->response(array('error' => 'Some error occurs so data save in the CSV file'), 404);
        }
    }
   
    
    function tempBooking_post() {
        $input = $this->input->post();

        $all_records = $this->upsrtc_api->apiTempBooking($input);

        $data = $this->upsrtc_api->temporary_booking($input);
        
        if ($data['status'] != "FAILURE" && $all_records) {

            $ticket_no = $this->generateid_model->getRequiredId('processed_tickets');
            $gen_data = array('ID_CATEGORY' => 'processed_tickets', "CREATED_DATE" => date("Y-m-d H:i:s"));
            $this->db->insert("generateid", $gen_data);

            $ticket_no = $this->db->insert_id();
            
            
            if((isset($input['service_id']) && $input['service_id']!='') 
                    && (isset($input['from_stop_id']) && $input['from_stop_id']!='')
                    && (isset($input['to_stop_id']) && $input['to_stop_id']!='') 
                    && (isset($input['from_stop']) && $input['from_stop']!='') 
                    && (isset($input['to_stop']) && $input['to_stop']!='')
                    && (isset($input['date']) && $input['date']!='') 
                    && (isset($input['op_name']) && $input['op_name']!='') 
                    && (isset($input['mobile_no']) && $input['mobile_no']!='') 
                    && (isset($input['email']) && $input['email']!='') && $ticket_no)
            {   
                $date  = $input['date'];
                
                //Check if date is in dd-mm-yyyy format
                if(preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/",$date))
                {
                    $this->upsrtc_api->insert_processed_tkt_details($input,$ticket_no,$all_records);

                    $result = 'Data inserted in the processed_tickets & processed_tickets_details tables sucessfully ';
                    $this->response(array('success' => $result), 200); // 200 being the HTTP response code
                }
                else
                {
                    $this->response(array('error' => "Please provide date in dd-mm-yyyy format",404));
                }
            }
            else
            {
                $this->response(array('error' => "Please provide proper parameters"), 404);
            }
        } 
        else 
        {
            foreach ($input['passenger'] as $key => $value) {
                $list[] = array($value['name'], $value['age'], $value['sex'], $value['mobile_no'],
                    $value['email'], $value['seat_no']);
            }
            $to_date = date('d-m-Y');
            $file = fopen("./uploads/processed_fail_transaction_" . $to_date . ".csv", "a");

            foreach ($list as $line) {
                fputcsv($file, $line);
            }

            fclose($file);
            $this->response(array('error' => $data), 404);
        }
    }

}