<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class User extends REST_Controller
{
    protected $data;
    protected $sSecretKey = 'fdac7124c6a2b3f6';
    
    function __construct() {
        parent::__construct();
        
        //  $this->load->model(array);
        
        $log_data = array();
        $log_data['url'] = current_url();
        $log_data['Request_type'] = $_SERVER['REQUEST_METHOD'];
        $log_data['get'] = $this->input->get();
        $log_data['post'] = $this->post();
        
        // $log_data['api_post']       = $this->post();
        $log_data['header'] = getallheaders();
        $log_data['user'] = $this->input->server('PHP_AUTH_USER');
        $log_data['ip'] = $this->input->ip_address();
        
        // show($log_data);
        
        log_data('rest/user_api_' . date('d-m-Y') . '.log', $log_data, 'api');
        
        
        // $this->data = array('status' => 'failed');
    }
    
    /*
     * Add User Details
    */
    
    function register_post() {
        $input = $this->post();
        
        // show($this->user->id,1);
        // show($input,1);
        
        $password = $input['password'];
        
        $config = array(
                    array('field' => 'name', 'label' => 'Name', 'rules' => 'trim|required', 'errors' => array('required' => 'Please enter name.')),
                    array('field' => 'email', 'label' => 'Email', 'rules' => 'trim|required|valid_email|is_unique[users.email]', 'errors' => array('required' => 'Please enter valid email.', 'is_unique' => 'Email id already exist')), 
                    // array('field' => 'username', 'label' => 'Username', 'rules' => 'trim|required|alpha|callback_chk_usernm', 'errors' => array('required' => 'Please enter username.', 'chk_usernm' => 'User Name is already exist', 'alpha' => 'User Name should be valid')), 
                    array('field' => 'password', 'label' => 'Password', 'rules' => 'trim|required|min_length[8]', 'errors' => array('required' => 'Please enter Password.', 'min_length' => 'Password should be 8 character in length')), 
                    array('field' => 'dob', 'label' => 'Date of Birth', 'rules' => 'trim|required', 'errors' => array('required' => 'Please provide date of birth.')), 
                    // array('field' => 'confirm_password', 'label' => 'Confirm Password', 'rules' => 'trim|required|min_length[8]|matches[password]', 'errors' => array('required' => 'Please enter Password.', 'min_length' => 'Password should be 8 character in length', 'matches' => 'Password & Confirm password should be same')), 
                    array('field' => 'mob_no', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|min_length[10]|max_length[10]', 'errors' => array('required' => 'Please enter mobile_no.', 'min_length' => 'Mobile Number should be 8 character in length', 'max_length' => 'Mobile Number should be 8 character in length', 'numeric' => 'Please enter valid mobile number')));
        
        if (form_validate_rules($config) == FALSE) {
            $data['error'] = $this->form_validation->error_array();

            log_data('rest/user_api_' . date('d-m-Y') . '.log', $data, 'register error msg');
            $this->response(array('status' => 'failed', 'data' =>  $data), 200);
        } else {
            $salt = getRandomId();
            $pass = md5($salt . $password);
            $activation_key = getRandomId(8);
            
            $role = $this->role_model->where(array('role_name' => USER_ROLE_NAME))->find_all();
            
            $name_arr = explode(' ', $this->post('name'));
            $fname = $name_arr[0];
            $lname = '';
            
            if(count($name_arr) > 1)
            {
                $lname = end($name_arr);
            }
            
            if(empty($lname) && (!empty($this->post('last_name')))){
                $lname = $this->post('last_name');
            }

            $display_name = $fname . "  " . $lname;

            $user_arr = array(
                            'role_id' => $role[0]->id, 
                            'username' => $this->post('email'), 
                            'email' => $this->post('email'), 
                            'first_name' => $fname, 
                            'last_name' => $lname, 
                            'dob' => $this->post('dob'), 
                            'display_name' => $display_name, 
                            'salt' => $salt, 
                            'password' => $pass, 
                            'mobile_no' => $this->post('mob_no'), 
                            'address1' => $this->post('address'), 
                            'status' => 'N', 
                            'activate_key' => $activation_key, 
                            'vendor' => $this->user->id,
                            'inserted_by' => $this->user->id,
                          );
            
            //saves user data into user table
            
            $user_id = $this->users_model->insert($user_arr);
            
            if ($user_id) {
                $mailbody_array = array(
                                        "mail_title" => "email_verification", 
                                        "mail_client" => $this->post('email'), 
                                        "mail_replacement_array" => array(
                                                        '{{verification_mail_url}}' => base_url() . 'admin/login/activate/' . $activation_key, 
                                                        '{{fullname}}' => ucwords($display_name)
                                                      )
                                      );

                $mail_result = setup_mail($mailbody_array);
                
                $this->response(array( "status" => 'success', "msg" => 'User Registered Successfully Please Check Verification Mail To Login'), 200);
            } else {
                $this->response(array( "status" => 'failed', "msg" => 'Error in User Register'), 200);
            }
        }
    }
    
    function edit_post() {
        $input = $this->post();

        $config = array(
                    // array('field' => 'name', 'label' => 'Name', 'rules' => 'trim|required', 'errors' => array('required' => 'Please enter name.')),
                    array('field' => 'email', 'label' => 'Email', 'rules' => 'trim|required|valid_email', 'errors' => array('required' => 'Please enter valid email.', 'is_unique' => 'Email id already exist')), 
                    array('field' => 'dob', 'label' => 'Date of Birth', 'rules' => 'trim|required', 'errors' => array('required' => 'Please provide date of birth.')), 
                    array('field' => 'mob_no', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|min_length[10]|max_length[10]', 'errors' => array('required' => 'Please enter Password.', 'min_length' => 'Mobile Number should be 8 character in length', 'max_length' => 'Mobile Number should be 8 character in length', 'matches' => 'Password & Confirm password should be same', 'numeric' => 'Please enter valid mobile number') ),
                    array('field' => 'gender', 'label' => 'Gender', 'rules' => 'trim|required|callback_check_gender', 'errors' => array('required' => 'Please select Gender.', 'check_gender' => 'User Gender is not valid')), 
                );
        
        

        // $config = array(
        //               array('field' => 'user_id', 'label' => 'User ID', 'rules' => 'trim|required|callback_check_user_id', 'errors' => array('required' => ' Address1 should not be empty.', 'check_user_id' => 'User ID does not exist ')), 
        //               array('field' => 'add1', 'label' => 'Address1', 'rules' => 'trim|required', 'errors' => array('required' => ' Address1 should not be empty.')), 
        //               array('field' => 'add2', 'label' => 'Address2', 'rules' => 'trim|required', 'errors' => array('required' => 'Address2 should not be empty.')), 
        //               array('field' => 'first_name', 'label' => 'First Name', 'rules' => 'trim|required|alpha', 'errors' => array('required' => 'First Name should not be empty.', 'alpha' => 'First name should be valid')), 
        //               array('field' => 'last_name', 'label' => 'Last Name', 'rules' => 'trim|required|alpha', 'errors' => array('required' => 'Last Name should not be empty.', 'alpha' => 'Last Name should be valid')), 
        //               array('field' => 'gender', 'label' => 'Gender', 'rules' => 'trim|required|callback_check_gender', 'errors' => array('required' => 'Please select Gender.', 'check_gender' => 'User Gender is not valid')), 
        //               // array('field' => 'country', 'label' => 'country', 'rules' => 'trim|required', 'errors' => array('required' => 'Please select country name.')), 
        //               // array('field' => 'city', 'label' => 'city', 'rules' => 'trim|required', 'errors' => array('required' => 'Please Input City Name.')), 
        //               // array('field' => 'state', 'label' => 'State Name', 'rules' => 'trim|required', 'errors' => array("required" => 'Please select state name')), 
        //               // array('field' => 'pcode', 'label' => 'pcode', 'rules' => 'trim|required|numeric|callback_valid_pin|min_length[6]|max_length[6]', 'errors' => array('required' => 'Please Enter Pincode Number.', 'valid_pin' => 'Please Enter valid pin number', 'min_length' => 'Pin code number should be 6 digit')), 
        //               array('field' => 'userfile', 'label' => 'Userfile', 'rules' => 'callback_check_extension', 'errors' => array('check_extension' => 'Please upload Jpg/jpeg/gif/png image or path does not valid')), 
        //               array('field' => 'dob', 'label' => 'Date of birth', 'rules' => 'trim|required|callback_date_of_birth', 'errors' => array('required' => 'Date of birth should not be empty.', 'date_of_birth' => 'Please enter valid date of birth')), 
        //               array('field' => 'mob', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|min_length[10]|max_length[10]', 'errors' => array('required' => 'Please Enter mobile Number.', 'number' => 'Please Enter valid mobile number', 'min_length' => 'Please enter 10 digit mobile number'))
        //           );
        
        if (form_validate_rules($config) == FALSE) {
            $data['error'] = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'data' =>  $data), 200);
        } else {
            $user = $this->users_model->find_by(array('email' => $this->post('email') ));

                 
          if($user)
          {
            $uid        = $user->id;
            $img_path   = $input['userfile'];

            $img1_wid = 200;
            $img1_hei = 160;
            $img2_wid = 60;
            $img2_hei = 40;
            $small = 'iconic';
            $large = 'large';
            $medium = 'thumb';
            $pathname = "";

            if ($this->post('userfile') && getDataFromRemote( $this->post('userfile') )) {
                $image_detail = $img_arr = @getimagesize($input['userfile']);

                $image_extension = array(
                                        'image/jpeg' => '.jpeg', 
                                        'image/jpg' => '.jpg', 
                                        'image/png' => '.png'
                                      );
                
                $file_ext   = $image_extension[$image_detail['mime']];
                $file_name  = $uid . $file_ext;
                $pathname   = $file_name;
                $oldfile    = $user->profile_image;

                if ($oldfile != "") {
                    if (file_exists(UPLOAD_FOLDER . $uid . '/profile_img/' . $large . '/' . $oldfile)) {
                        unlink(UPLOAD_FOLDER . $uid . '/profile_img/' . $large . '/' . $oldfile);
                    }
                    if (file_exists(UPLOAD_FOLDER . $uid . '/profile_img/' . $medium . '/' . $oldfile)) {
                        unlink(UPLOAD_FOLDER . $uid . '/profile_img/' . $medium . '/' . $oldfile);
                    }
                    if (file_exists(UPLOAD_FOLDER . $uid . '/profile_img/' . $small . '/' . $oldfile)) {
                        unlink(UPLOAD_FOLDER . $uid . '/profile_img/' . $small . '/' . $oldfile);
                    }
                }
                
                /*
                 * Create user folder to store the images
                */
                
                $folder = "profile_img";
                $subfolder = array("large", "thumb", "iconic");
                
                create_main_folder('uploads');
                create_user_folder($uid, $folder, $subfolder);
                $save_img = UPLOAD_FOLDER . $uid . '/profile_img/large/' . $file_name;
                $new_img = UPLOAD_FOLDER . $uid . '/profile_img/thumb';
                save_remote_image($img_path, $save_img);
                
                $this->resize($save_img, $new_img, $img1_wid, $img1_hei);
                
                //$this->resize($save_img, $new_img, $img2_wid, $img2_hei, $small);
                
                
            } else {
                
                //show('success',1);
                $pathname = $user->profile_image;
            }

            if($this->post('name'))
            {
                $name_arr = explode(' ', $this->post('name'));
                $fname = $name_arr[0];
                $lname = '';
                
                if(count($name_arr) > 1)
                {
                    $lname = end($name_arr);
                }

                $user_array['first_name']     = $fname;
                $user_array['last_name']      = $lname;
                if($this->post('last_name')){
                    $lname     = $this->post('last_name');
                }

            }

            $user_array = array(
                                // 'address1'      => $this->post('address'),
                                // 'pincode'       => $this->post('pincode'),
                                // 'mobile_no'     => $this->post('mob_no'),
                                'first_name'    => $fname,
                                'last_name'     => $lname,
                                // 'profile_image' => $pathname,
                                'gender'        => $this->post('gender'),
                                'dob'           => date('Y-m-d', strtotime($this->post('dob'))),
                                'inserted_by'   => $this->user->id,
                            );

            $this->users_model->id          = $uid;

            

            if(!empty($this->post('mob_no')))
            {
              $user_array['mobile_no']     = $this->post('mob_no');
            }
            
            if(!empty($this->post('address')))
            {
              $user_array['address1']     = $this->post('address');
            }

            if(!empty($this->post('pincode')))
            {
              $user_array['pincode']     = $this->post('pincode');
            }


            if($this->post('userfile'))
            {
              $user_array['profile_image']     = $pathname;
            }

            if(!empty($this->post('gender')))
            {
              $user_array['gender']     = $this->post('gender');
            }

            if(!empty($this->post('country')))
            {
              $user_array['country']     = $this->post('country');
            }


            if(!empty($this->post('country')))
            {
              $user_array['country']     = $this->post('country');
            }
            
            if(!empty($this->post('state')))
            {
              $user_array['state']       = $this->post('state');
            }

            if(!empty($this->post('city')))
            {
              $user_array['city']        = $this->post('city');
            }
            
            // show($user_array,1);

            $this->users_model->update($uid, $user_array);

            $this->response( array("status" => "success", "msg" => "Profile updated successfully."), 200);
        }
        else
        {
          $this->response(array( "status" => 'failed', "msg" => 'User not found or create by other agent'), 200);
        }
      }
    }

    //change email
    
    function change_email_post() {
        $input    = $this->post();

        $user_id  = trim($input['user_id']);
        $email   = trim($input['email']);
        $sec_email = trim($input['sec_email']);

        
        
        $config = array(
                array('field' => 'user_id', 'label' => 'User ID', 'rules' => 'trim|required|callback_check_user_id[' . $user_id . ']', 'errors' => array('required' => 'User ID  should not be empty.', 'check_user_id' => 'User ID does not exist')), 
                array('field' => 'email', 'label' => 'Primary Email', 'rules' => 'trim|required|valid_email|callback_email_check[' . $user_id . ']', 'errors' => array('required' => 'Please Enter Primary email', 'email_check' => 'This Email ID is already registered')), 
                array('field' => 'sec_email', 'label' => 'Secondary Email', 'rules' => 'trim|callback_checkmat[' . $email . ']|callback_email_check[' . $user_id . ']|valid_email', 'errors' => array('checkmat' => 'Primary & secondary Email should not be same', 'valid_email' => 'Please enter valid Secondary email', 'email_check' => 'This Email ID is already exist')),
           );

        if (form_validate_rules($config) == FALSE) {
            $data['error'] = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'data' =>  $data), 200);
        } else {
            
            $user_info = $this->users_model->where(array('inserted_by' => $this->user->id ))
                                  ->find($input['user_id']);

            $activation_key = getRandomId(8);

            if($user_info)
            {
              $user_array  = array(
                                  'sec_email' => $sec_email,
                                  'verify_status' => 'N',
                                  'activate_key' => $activation_key,
                                  // 'email' => $input['email'],  
                              );

              $this->users_model->update($user_info->id, $user_array);

              // to send mail on registered mail id
              $mailbody_array = array(
                                    "mail_title" => "email_verification", 
                                    "mail_client" => $input['email'], 
                                    "mail_replacement_array" => array(
                                                                  '{{verification_mail_url}}' => base_url() . 'admin/login/activate/' . $activation_key . "/email", 
                                                                  '{{fullname}}' => ucwords($user_info->display_name))
                                                                );
              $mail_result = setup_mail($mailbody_array);

              $this->response( array( 'status' => 'success', 'msg' => 'Email Address Changed successfully.'), 200);
            }
            else
            {
              $this->response( array( 'status' => 'success', 'msg' => 'User not found'), 200);
            }
        }
    }
    
    function change_password_post() {
        $input = $this->post();

        $config = array(
                    array(
                      'field' => 'email',
                      'label' => 'eamil', 
                      'rules' => 'trim|required', 
                      'errors' => array('required' => 'Email ID  should not be empty.')
                    ), 
                    array(
                      'field' => 'old_password', 
                      'label' => 'old_Password', 
                      'rules' => 'trim|required', 
                      'errors' => array('required' => 'Provide old Password.')
                    ),
                    array(
                        'field' => 'new_password', 
                        'label' => 'new_password', 
                        'rules' => 'trim|required|min_length[8]', 
                        'errors' => array(
                                            "required" => 'Provide new password', 
                                            'min_length' => 'Please enter 8 character New Password'
                                        )
                    )
                );
        if (form_validate_rules($config) == FALSE) {
            
            $data['error'] = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'msg' =>  $data), 200);
        } else {

          $user_info = $this->users_model->where_not_in('register_using',['google','facebook'])->find_by(array('email' => $this->post('email') ));

          if($user_info){
            if( md5( $user_info->salt . $input['old_password']) == $user_info->password ){
              $newsalt = getRandomId();
              
              //generate new salt
              $new_pass = md5($newsalt . $input['new_password']);
              
              //concat salt & password
              $user_arr = array(
                                'password' => $new_pass,
                                'salt' => $newsalt,
                              );

              $insert_id = $this->users_model->update($user_info->id, $user_arr);

              if ($insert_id) {
                  $this->response( array( 'status' => 'success', 'msg' => 'Password Changed successfully'), 200);
              } else {
                  $this->response( array( 'status' => 'failed', 'msg' => 'Some error!'), 200);
              }
            }
            else
            {
              $this->response( array( 'status' => 'failed', 'msg' => 'Old password doesn\'t match'), 200);
            }
          }
          else
          {
            $this->response( array( 'status' => 'failed', 'msg' => 'User not found'), 200);
          }            
        }
    }

    function forgot_password_post()
    {
      $config = array(
                    array(
                      'field' => 'email', 
                      'label' => 'Email', 
                      'rules' => 'trim|required', 
                      'errors' => array(  
                                    'required' => 'Provide email id',
                                  )
                    ),
                );

        if (form_validate_rules($config) == FALSE) {
            
            $data['error'] = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'data' =>  $data), 200);
        } else {

          $user_info = $this->users_model
                                  // ->where(array('inserted_by' => $this->user->id ))
                                  ->find_by('email', $this->post('email'));
          if($user_info)
          {
            $salt       = getRandomId();
            $random_str   = getRandomId(6);
            $password = md5($salt . $random_str);

            // show($random_str);
            // show($password,1);
            $this->users_model->update( $user_info->id, array('salt' => $salt, 'password' => $password) );

            $mailbody_array = array(
                                "mail_title" => "forgot_password",
                                "mail_client" => $user_info->email,
                                "mail_replacement_array" => array(
                                                    '{{fullname}}' => $user_info->first_name.' '. $user_info->last_name,
                                                    '{{username}}' => $user_info->email,
                                                    '{{password}}' => $random_str
                                                  )
                               );

            // show($mailbody_array);
            $mail_result = setup_mail($mailbody_array);
            $this->response(array('status' => 'success', 'msg' =>  'Password has been send to your email.'), 200);
          }
          else{
            $this->response(array('status' => 'failed', 'msg' =>  'Email id not found, Please check it again'), 200);
          }
        }
    }

    function login_post(){
      $config = array(
                    array(
                      'field' => 'email', 
                      'label' => 'Email', 
                      'rules' => 'trim|required', 
                      'errors' => array(  
                                    'required' => 'Provide email id',
                                  )
                    ),                    
                    array(
                      'field' => 'password', 
                      'label' => 'Password', 
                      'rules' => 'trim|required', 
                      'errors' => array(  
                                    'required' => 'Provide password',
                                  )
                    ),
                );
      
        if (form_validate_rules($config) == FALSE) {
            
            $data['error'] = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'data' =>  $data), 200);
        } else {

            $this->db->select('id, username, first_name, last_name, email, gender, sec_email, display_name, address1, address2, pincode, mobile_no, country, state, city, dob, profile_image,  salt, password, status, verify_status');
            
            $user = $this->users_model->find_by( 'email', $this->post('email') );

              if($user)
              {
                if($user->status == 'Y' )
                {
                  if( md5($user->salt . $this->post('password') ) == $user->password)
                  {
                    unset($user->salt);
                    unset($user->password);
                    $data['user'] = $user;

                    $this->response( array( 'status' => 'success', 'data' => $data), 200);
                  }else{
                    $this->response( array( 'status' => 'failed', 'msg' =>'Invalid username or password'), 200);
                  }
                }
                else
                {
                  $this->response( array( 'status' => 'failed', 'msg' =>'User is not active. Please check the mail activation link.'), 200);
                }
              }
              else{
                  $this->response( array( 'status' => 'failed', 'msg' => 'User not found'), 200);
              }

        }
    }

    function social_login_post(){
        $config = array(
                    array(
                      'field' => 'email', 
                      'label' => 'Email', 
                      'rules' => 'trim|required', 
                      'errors' => array(  
                                    'required' => 'Provide email id',
                                  )
                    ),
                    array(
                      'field' => 'from', 
                      'label' => 'from', 
                      'rules' => 'trim|required', 
                      'errors' => array(  
                                    'required' => 'Provide login from',
                                  )
                    ),                    
                    array(
                      'field' => 'raw', 
                      'label' => 'raw', 
                      'rules' => 'trim|required', 
                      'errors' => array(  
                                    'required' => 'Provide raw',
                                  )
                    ),
                );
      
        if (form_validate_rules($config) == FALSE) {
            
            $data['error'] = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'data' =>  $data), 200);
        } else {

            $this->db->select('id, username, first_name, last_name, email, gender, sec_email, display_name, address1, address2, pincode, mobile_no, country, state, city, dob, profile_image,  salt, password, status, verify_status, register_using');
            
            $user = $this->users_model->find_by( 'email', $this->post('email') );


            if($user)
            {
                if($user->register_using == $this->post('from'))
                {
                    $this->response( array( 'status' => 'success', 'msg' => 'Login Successful', 'data' => $user), 200);
                }
                else{
                    $this->response( ['status' => 'failed','msg' => 'Email is used by '.$user->register_using.' loign', 'data' => ''], 200);
                }
            }
            else
            {
                $role = $this->role_model->where(array('role_name' => USER_ROLE_NAME))->find_all();

                $raw = (is_JSON($this->post('raw'))) ? json_decode($this->post('raw')) : $this->post('raw');

                if(!empty($raw->name)){
                    $name = explode(' ', $raw->name);
                    $fname = $name[0];
                    $lname  = (empty($name[1])) ? '': $name[1];

                    $display_name = $fname .' ' . $name;
                }

                $gender = '';
                if(!empty($raw->gender))
                {
                    $gender = (strtolower($raw->gender) == 'female') ? 'F' : 'M';
                }

                $dob = '';
                if(!empty($raw->birthday))
                {
                    $dob = date('Y-m-d',strtotime(str_replace('/', '-', $raw->birthday)));
                }
                

                $user_arr = array(
                            'role_id' => $role[0]->id, 
                            'username' => $this->post('email'), 
                            'email' => $this->post('email'), 
                            'first_name' => $fname, 
                            'last_name' => $lname, 
                            'dob' => $dob,
                            'display_name' => $display_name, 
                            // 'mobile_no' => $this->post('mob_no'), 
                            // 'address1' => $this->post('address'), 
                            'status' => 'Y', 
                            'gender' => $gender, 
                            // 'activate_key' => $activation_key, 
                            'vendor' => $this->user->id,
                            'inserted_by' => $this->user->id,
                            'register_using' => $this->post('from')
                          );
                
                $user_id = $this->users_model->insert($user_arr);

                if ($user_id) 
                {
                    $this->load->model(['user_metadata_model']);

                    if($this->post('raw'))
                    {
                        $this->user_metadata_model->insert([
                                                            'user_id' => $user_id, 
                                                            'user_value' => $this->post('raw')
                                                        ]);
                    }

                    $this->db->select('id, username, first_name, last_name, email, gender, sec_email, display_name, address1, address2, pincode, mobile_no, country, state, city, dob, profile_image,  salt, password, status, verify_status, register_using');
            
                    $user = $this->users_model->find( $user_id );

                    $this->response( array( 'status' => 'success', 'msg' => 'Login Successful', 'data' => $user), 200);
   
                }
                else
                {
                    $this->response( array( 'status' => 'failed', 'msg' => 'User login error'), 200);
                }
            }
        }
    }

    function resend_activation_link_post(){
      $config = array(
                    array(
                      'field' => 'email', 
                      'label' => 'Email', 
                      'rules' => 'trim|required', 
                      'errors' => array(  
                                    'required' => 'Provide email id',
                                  )
                    ),
                );
      
        if (form_validate_rules($config) == FALSE) {
            
            $data['error'] = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'data' =>  $data), 200);
        } else {

            $this->db->select('id, username, first_name, last_name, email, gender, sec_email, display_name, address1, address2, pincode, mobile_no, country, state, city, dob, profile_image,  salt, password, status, verify_status, activate_key');
            
            $user = $this->users_model->find_by( 'email', $this->post('email') );

            if($user)
            {
                if($user->verify_status == 'N' && trim($user->activate_key) != '')
                {   
                    $activation_key = $user->activate_key;
                    $display_name  = $user->first_name .' '. $user->last_name;

                    $mailbody_array = array(
                                            "mail_title" => "email_verification", 
                                            "mail_client" => $this->post('email'), 
                                            "mail_replacement_array" => array(
                                                            '{{verification_mail_url}}' => base_url() . 'admin/login/activate/' . $activation_key, 
                                                            '{{fullname}}' => ucwords($display_name)
                                                          )
                                          );

                    // show($mailbody_array,1);
                    $mail_result = setup_mail($mailbody_array);
                    
                    $this->response(array( "status" => 'success', "msg" => 'Activation link send to email id'), 200);
                }
                else
                {
                    $this->response(array( "status" => 'failed', "msg" => 'User already activated'), 200);
                }

            }
            else
            {
                $this->response( array( 'status' => 'failed', 'msg' => 'User not found'), 200);
            }

        }

    }

    function detail_post(){
        $config = array(
                    array(
                      'field' => 'email', 
                      'label' => 'Email', 
                      'rules' => 'trim|required', 
                      'errors' => array(  
                                    'required' => 'Provide email id',
                                  )
                    ),
                );
      
        if (form_validate_rules($config) == FALSE) {
            
            $data['error'] = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'data' =>  $data), 200);
        } else {
            // $this->db->select('id, username, first_name, last_name, email, gender, sec_email, display_name, address1, address2, pincode, mobile_no, country, state, city, dob, profile_image,  salt, password, status, verify_status, activate_key');
            $this->db->select('id, username, first_name, last_name, email, gender, sec_email, display_name, address1, address2, pincode, mobile_no, country, state, city, dob, profile_image, status, verify_status, activate_key');
            
            $user = $this->users_model->find_by('email', $this->post('email'));



            if($user)
            {
                $this->response( array( 'status' => 'success', 'data' =>  $user), 200);
            }
            else
            {
                $this->response( array( 'status' => 'failed', 'msg' => 'User not found'), 200);
            }
        }
    }

    
    /*validation function*/    
    function chk_email($em) {
        $usernm = '';
        
        $user_exists = $this->users_model->check_user_exists($usernm, trim($em));
        
        if ($user_exists == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    function chk_usernm($usernm) {
        $email = '';
        $user_exists = $this->users_model->check_user_exists(trim($usernm), $email);
        if ($user_exists == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    function valid_pin($pin) {
        if ($pin == 0) {
            return false;
        } else {
            return true;
        }
    }
    
    function check_gender($gender) {
        return ($gender != 'M' && $gender != 'F') ? false : true;
    }
    
    function check_extension($path) {
        $image_format_valid = array('image/jpeg', 'image/jpg', 'image/png');

        $img_detail = @getimagesize($path);

        return ($img_detail && in_array($img_detail['mime'], $image_format_valid) );            
    }

    function check_user_id($user_id) {
        $userexist = $this->users_model->get_user_by_id($user_id);
        
        //show($userexist,1);
        if ($userexist) {
            return true;
        } else {
            return false;
        }
    }
    
    /*resize Image*/
    public function resize($org_img, $new_img, $width, $height) {
        $this->load->library('image_lib');
        $config['image_library'] = 'gd2';
        $config['source_image'] = $org_img;
        $config['maintain_ratio'] = TRUE;
        $config['quality'] = "100";
        $config['new_image'] = $new_img;
        $config['width'] = $width;
        $config['height'] = $height;
        
        // $config['height'] =$height;
        
        $this->image_lib->initialize($config);
        if (!$this->image_lib->resize()) {
            echo $this->image_lib->display_errors();
        }
    }
    
    //date validation
    function date_of_birth($date) {
        if (isset($date) && $date != '' && (preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/", $date))) {
            return true;
        } else {
            return false;
        }
    }
    
    
    
    function email_check($email, $id) {
        
        $em = $this->users_model->check_mail($email, $id);
        if (count($em) > 0) {
            return false;
        } else {
            return true;
        }
    }
    function checkmat($str, $pemail) {
        if ($str == "") {
            return true;
        }
        
        if ($str === $pemail) {
            return false;
        } else {
            return true;
        }
    }
    
    function check_old_password($a, $para1) {
        
        $data = explode("-", $para1);
        $password = '';
        $pass = '';
        
        $this->users_model->id = $data[0];
        $userinfo = $this->users_model->select();
        if ($userinfo) {
            $salt = $userinfo->salt;
            $pass = $userinfo->password;
            $password = md5($salt . $data[1]);
            
            //show($password,1);
            
            
        }
        
        if ($pass == $password) {
            return true;
        } else {
            return false;
        }
    }
    
    function check_new_password($a, $para2) {
        
        $data = explode("-", $para2);
        $password = '';
        $pass = '';
        $this->users_model->id = $data[0];
        $userinfo = $this->users_model->select();
        if ($userinfo) {
            $pass = $userinfo->password;
            $salt = $userinfo->salt;
            $password = md5($salt . $data[1]);
        }
        
        //show($password,1);
        if ($pass == $password) {
            return false;
        } else {
            return true;
        }
    }
}
