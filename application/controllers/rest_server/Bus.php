<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Bus extends REST_Controller
{
    var $db2;
	function __construct()
    {

        parent::__construct();		

        ini_set('memory_limit', '1024M'); 

        $this->load->model('op_master_details_model','ticket_details_model','bus_operator_model','tickets_model','ticket_discount_description_model','payment_gateway_key_model','users_model','tickets_temp_model');
       
        // $this->load->library('buses_api');
        $this->load->library(array('buses_api','cca_payment_gateway','soap_api/msrtc_api','soap_api/upsrtc_api','soap_api/rsrtc_api','api_provider/etravelsmart_api','api_provider/hrtc_api','api_provider/its_api','discounts'));

        $this->db2 = $this->load->database('default', TRUE);

        $lib = array();

        foreach (glob(APPPATH."libraries/soap_api/"."*.php") as $filename) {
            $path = 'soap_api/'.strtolower(basename($filename,'.php'));
            array_push($lib, $path );
        }

        $data = array();
        $data['url']            = current_url();
        $data['Request_type']   = $_SERVER['REQUEST_METHOD'];
        $data['get']            = $this->input->get();
        $data['post']           = $this->post();
        $data['api_post']       = $this->post();
        $data['header']         = getallheaders();
        $data['user']           = $this->input->server('PHP_AUTH_USER');
        $data['ip']             = $this->input->ip_address();

        // show($data);
         
    
        log_data('rest/bus_new/'.date('M').'/'.date('d-m-Y').'/request'.'.log', $data, 'api');
    }

    
    function getAllBusType_post()
    {
        $input = $this->input->post();
        $operator_name = trim($input['operator']);
        if(isset($operator_name) && $operator_name !='')
        {
            $operator_id = $this->op_master_details_model->getOperatorId($operator_name);
            if($operator_id){
                $this->db->select("bt.op_bus_type_name as busName, bt.op_bus_type_cd as busCode, om.op_name");
                $this->db->from("op_bus_types bt");
                $this->db->join("op_master om","om.op_id=bt.op_id");
                $query = $this->db->get();
                $data =  $query->result_array();
                
                // log_data('rest/getallbustype_'.date('d-m-Y').'.log', $data, 'api');
                log_data('rest/bus_new/'.date('M').'/'.date('d-m-Y').'/getallbustype.log', $data, 'api');
                log_data('rest/bus_new/'.date('M').'/'.date('d-m-Y').'/api'.'.log', $data, 'getallbustype');

                if($data)
                {
                    $this->response($data, 200); // 200 being the HTTP response code
                }
                else
                {
                    $this->response(array('error' => 'No bus exists'), 200);
                }
            }
            else {
                $this->response(array('error'=>'For this operator data is not available, Please enter proper operator name'), 200);
            }
        }
        else
        {
            $this->response(array('error' => 'Please provide operator name'), 200);
        }
    }

    function getAllstops_post()
    {
        $this->db->select('bus_stop_name as value,bus_stop_name as label');
        $this->db->from('bus_stop_master');
        $this->db->order_by('bus_stop_name', "asc");

        $query = $this->db->get();
        $data = $query->result_array();

        $etravels_cities = $this->buses_api->etravelsmart_api->getSourceDestination();
        $response_string = $this->hrtc_api->getAllCities();
        $hrtc_cities = xml_to_array($response_string);
        if(count($hrtc_cities) > 0 && count($data) > 0)
        {
            $new_cities = array_merge($data,$hrtc_cities);
            $new_cities = array_unique($new_cities,SORT_REGULAR);

        }
        if(count($etravels_cities) > 0 && count($data) > 0)
        {
            $new_cities = array_merge($data,$etravels_cities);
            $new_cities = array_unique($new_cities,SORT_REGULAR);
        }
        else if(count($data) > 0)
        {
            $new_cities = $data;
        }
        else if(count($etravels_cities) > 0)
        {
            $new_cities = $etravels_cities;
        }
        if (count($new_cities) > 0) {

            array_multisort($new_cities,SORT_ASC); //For Sorting in ASC
            
            $this->response( array('status' => 'success', 'msg' => 'stop found', 'data' => $new_cities ), 200);
        }

        $this->response( array('status' => 'success', 'msg' => 'No stop found' ), 200);
    }
    
    
    function getAvailableServices_post() {

        
        $onward = array();
        $this->load->model(array('travel_destinations_model'));

        $input['operator_type'] = $this->post('operator_type');
        $input = $this->post();

        if (empty($input['operator_type'])) {
            $input['operator_type'] = "all";
        }
        $input['operator_type'] = strtolower($input['operator_type']);
        if ($input['operator_type'] == "all" || $input['operator_type'] == "private") {

            $this->load->library(array('api_provider/etravelsmart_api'));
        }

        $current_date = strtotime(date('Y-m-d'));


        $config = array(
            array('field' => 'from', 'label' => 'From stop', 'rules' => 'trim|required', 'errors' => array('required' => 'Please provide boarding stop')),
            array('field' => 'to', 'label' => 'To stop', 'rules' => 'trim|required', 'errors' => array('required' => 'Please provide destination stop')),
            array('field' => 'date', 'label' => 'Journey Date', 'rules' => 'trim|required', 'errors' => array('required' => 'Please provide Journey date')),
                // array('field' => 'arrival_date', 'label' => 'Return Journey Date', 'rules' => 'trim|required', 'errors' => array('required' => 'Please provide return journey date' )),
        );

        if (form_validate_rules($config) == FALSE) {
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
        } else {
            if (preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/", $this->post('date')) && strtotime($this->post('date')) >= strtotime(date('Y-m-d'))) {
                $data = array();
                $user_data = $this->post();

                log_data("track/mbors.log", $user_data, "posted data");
                $user_data['date_of_jour'] = $this->post('date');

                // $data['journey_bus_service'] = $this->travel_destinations_model->get_search($user_data);
                if ($input['operator_type'] == "all" || $input['operator_type'] == "stu") {
                    $onward = $this->buses_api->upsrtc_api->api_get_services($user_data);
                }
                log_data("track/mbors.log", $onward, "onward data");
                 
                $etravels_data = array();
                if (config_item('enable_etravelsmart') && ($input['operator_type'] == "all" || $input['operator_type'] == "private")) {
                    $etravels_data = $this->buses_api->etravelsmart_api->apiGetAvailabeBus($user_data,true);
                }

                if ($this->config->item("enable_hrtc_services") && ($input['operator_type'] == "all" || $input['operator_type'] == "stu")) {
                    //echo $input["from"].'-'.$input["to"];
                    $this->load->model('bus_source_destination_model');

                    $hrtc_availability = $this->bus_source_destination_model->where(["source_name" => $input["from"], "destination_name" => $input["to"]])->find_all();
                    if ($hrtc_availability) {
                        $hrtc_bus_services = $this->hrtc_api->apiGetAvailableBuses($input);
                        $onward = array_merge($onward, $hrtc_bus_services["bus_detail"]);
                    }
                }

                // * ***********ITS Start Here************************ //

                if ($this->config->item("enable_its_services") && ($input['operator_type'] == "all" || $input['operator_type'] == "private")) {
                    $this->load->model('bus_source_destination_model');
                    $its_availability = $this->bus_source_destination_model->where(["api_provider_id" => ITS_PROVIDER_ID, "source_name" => $input["from"], "destination_name" => $input["to"]])->as_array()->find_all();
                    $input["source_destination_data"] = $its_availability[0];
                   
                    if (!empty($its_availability)) {
                        $input["etravels_data"] = !empty($etravels_data['bus_detail']) ? $etravels_data : array();
                        $its_input_data = array("triptype" => "single",
                            "from" => $input["from"],
                            "to" => $input["to"],
                            "date_of_jour" => $input["date"],
                            "provider" => $input['operator_type'],
                            "searchbustype" => "",
                            "source_destination_data" => $input["source_destination_data"],
                            "etravels_data" => $input["etravels_data"]
                        );
                        $itsRes = $this->its_api->apigetAvailableBuses($its_input_data);
                        
                        if(!empty($itsRes['its_data']['bus_detail'])) {
                            $onward = array_merge($onward, $itsRes['its_data']);
                        }
                        if(!empty($itsRes['ets_data']['bus_detail'])) {
                            $etravels_data = $itsRes['ets_data']['bus_detail'];
                        }
                    } 
                    unset($input["source_destination_data"]);
                }
                if (!empty($etravels_data['compare_commission_data'])) {
                    unset($etravels_data['compare_commission_data']);
                }
                if(!empty($etravels_data)) {
                    $onward = array_merge($onward, $etravels_data);
                }
                
                // * ***********Calling ITS End   Here************************ 
                if (!empty($onward)) {
                    $data['journey_bus_service'] = $onward;
                }
                // Return Journy array
                if ($this->post('return_date')) {
                    $return_journ = array();
                    $r_user_data = array(
                        'date' => $this->post('return_date'),
                        'from' => $this->post('to'),
                        'to' => $this->post('from'),
                    );
                    $input["from"] = $this->post('to');
                    $input["to"] = $this->post('from');
                    $input["date"] = $this->post('return_date');
                    if (($input['operator_type'] == "all" || $input['operator_type'] == "stu")) {
                        $return_journ = $this->buses_api->upsrtc_api->api_get_services($r_user_data);
                    }

                    $etravels_data = array();
                    if (config_item('enable_etravelsmart') && ($input['operator_type'] == "all" || $input['operator_type'] == "private")) {
                        $etravels_data = $this->etravelsmart_api->apiGetAvailabeBus($r_user_data,true);
                    }

                    if ($this->config->item("enable_hrtc_services") && ($input['operator_type'] == "all" || $input['operator_type'] == "stu")) {
                        //echo $input["from"].'-'.$input["to"];
                        $this->load->model('bus_source_destination_model');

                        $hrtc_availability = $this->bus_source_destination_model->where(["source_name" => $input["from"], "destination_name" => $input["to"]])->find_all();
                        if ($hrtc_availability) {
                            $hrtc_bus_services = $this->hrtc_api->getAvailableBuses($input);
                            $return_journ = array_merge($return_journ, $hrtc_bus_services["bus_detail"]);
                        }
                    }

                    // * ***********ITS Start Here************************ //

                    if ($this->config->item("enable_its_services") && ($input['operator_type'] == "all" || $input['operator_type'] == "private")) {

                        $this->load->model('bus_source_destination_model');
                        $its_availability = array();
                        $its_availability = $this->bus_source_destination_model->where(["api_provider_id" => ITS_PROVIDER_ID, "source_name" => $input["from"], "destination_name" => $input["to"]])->as_array()->find_all();
                        $input["source_destination_data"] = $its_availability[0];

                        if (!empty($its_availability)) {

                            $input["etravels_data"] = !empty($etravels_data['bus_detail']) ? $etravels_data : array();
                            $its_input_data = array("triptype" => "round",
                                "from" => $input["from"],
                                "to" => $input["to"],
                                "date_of_jour" => $input["date"],
                                "provider" => $input['operator_type'],
                                "searchbustype" => "",
                                "source_destination_data" => $input["source_destination_data"],
                                "etravels_data" => $input["etravels_data"]
                            );

                            $itsRes = $this->its_api->apigetAvailableBuses($its_input_data);
                            if(!empty($itsRes['its_data']['bus_detail'])) {
                                $return_journ = array_merge($return_journ, $itsRes['its_data']);
                            }
                            if(!empty($itsRes['ets_data']['bus_detail'])) {
                                $etravels_data = $itsRes['ets_data']['bus_detail'];
                            }
                            
                        }
                        unset($input["source_destination_data"]);
                    }
                    // * ***********Calling ITS End   Here************************ 
                    if (!empty($etravels_data['compare_commission_data'])) {
                        unset($etravels_data['compare_commission_data']);
                    }
                    
                    if(!empty($etravels_data)) {
                        $return_journ = array_merge($return_journ, $etravels_data);
                    }
                    
                    if (!empty($return_journ)) {
                        $data['return_bus_service'] = $return_journ;
                    }
                }
               
                log_data("track/mbors.log", $data, "data");
                if ($data) {
                    log_data("track/mbors.log", "success", "success");
                    $this->response(array('status' => 'success', 'data' => $data), 200);
                } else {
                    log_data("track/mbors.log", "failed", "failed");
                    $this->response(array('status' => 'failed', 'msg' => array('No services found for provided date')), 200);
                }
            } else {
                $this->response(array('status' => 'failed', 'msg' => array('date' => 'Date is not in proper format')), 200);
            }
        }
    }

    // getBusServiceStops
    function getBusServiceStops_post() 
    {

       $this->load->model('op_master_details_model');
       $input = $this->input->post();

       $input = $this->post();

       $service_id = trim($input['service_id']);
       $operator_name = trim($input['operator']);
       $date = trim($input['date']);

       if($input['date'] != '' && strtotime($input['date']) >= strtotime(date('d-m-Y')) )
       {

           if(isset($service_id) && $service_id!='' && isset($operator_name) && $operator_name!='' && isset($date) && $date!='')
           {
               $operator_id = $this->op_master_details_model->getOperatorId($operator_name);
               $departure_date = date('Y-m-d',  strtotime($date));
                if($operator_id){
                    
                    $this->db->select("tm.op_trip_no, td.trip_no, osm.op_stop_name as stop_name, osm.op_stop_cd as stop_code, bsm.tahsil_nm, "
                            . "bsm.district_nm, bsm.state_nm,osm2.op_stop_name as stop_name2, osm2.op_stop_cd as stop_code2, "
                            . "bsm2.tahsil_nm as tahsil_nm2, bsm2.district_nm as district_nm2, bsm2.state_nm as state_nm2,"
                            . "td.sch_departure_date,td.sch_departure_tm,td.arrival_date,td.arrival_tm,td.day,"
                            . "td.seat_fare,td.sleeper_fare,td.child_seat_fare,om.op_name");
                    $this->db->from("travel_destinations td");
                    $this->db->join("trip_master tm", "tm.trip_no = td.trip_no", "inner");
                    $this->db->join("op_stop_master osm","osm.bus_stop_id = td.boarding_stop_id");
                    $this->db->join("op_stop_master osm2","osm2.bus_stop_id = td.destination_stop_id");
                    $this->db->join("bus_stop_master bsm","bsm.bus_stop_id = osm.bus_stop_id");
                    $this->db->join("bus_stop_master bsm2","bsm2.bus_stop_id = osm2.bus_stop_id");
                    $this->db->join("op_master om","om.op_id=osm.op_id");
                    $this->db->where(array("tm.op_trip_no"=>$input['service_id'], "osm.op_id"=>$operator_id['op_id'],"td.sch_departure_date"=>$departure_date, "td.sch_departure_date >= "=> date('Y-m-d')));
                    $query  = $this->db->get();
                    $bus_stops_data['busServiceStop']  = $query->result_array();

                    $bus_stops_details = array();

                    foreach($bus_stops_data['busServiceStop'] as $key=>$val) {

                        $bus_stops_details['busServiceStop'][$key]['serviceId']    = $val['op_trip_no'];
                        $bus_stops_details['busServiceStop'][$key]['op_name']      = $val['op_name'];

                        $bus_stops_details['busServiceStop'][$key]['from_stop']    = array('busStopName'  =>  $val['stop_name'],
                                                                                           'busStopcode'  =>  $val['stop_code'],
                                                                                           'taluka'       =>  $val['tahsil_nm'],
                                                                                           'district'     =>  $val['district_nm'],
                                                                                           'state'        =>  $val['state_nm']
                                                                                          );

                        $bus_stops_details['busServiceStop'][$key]['to_stop']      = array('busStopName'  =>  $val['stop_name2'],
                                                                                           'busStopcode'  =>  $val['stop_code2'],
                                                                                           'taluka'       =>  $val['tahsil_nm2'],
                                                                                           'district'     =>  $val['district_nm2'],
                                                                                           'state'        =>  $val['state_nm2']
                                                                                          );

                        $bus_stops_details['busServiceStop'][$key]['departureTime'] = $val['sch_departure_date']." ".$val['sch_departure_tm'];
                        $bus_stops_details['busServiceStop'][$key]['arrivalTime']   = $val['arrival_date'];
                        // $bus_stops_details['busServiceStop'][$key]['day']           = $val['day'];
                        $bus_stops_details['busServiceStop'][$key]['fare']          = $val['seat_fare']   ;
                        $bus_stops_details['busServiceStop'][$key]['sleeperFare']   = $val['sleeper_fare']   ;
                        $bus_stops_details['busServiceStop'][$key]['childFare']     = $val['child_seat_fare']   ;

                    }
                }
                else
                {
                    $this->response(array('error'=>'For this operator data is not available, Please enter proper operator name'), 400);
                }
           }
           else
           {
               $bus_stops_details  = array('error'=>'Service Id should not be blank');
           }
            if($bus_stops_details)
            {
                $this->response($bus_stops_details, 200); // 200 being the HTTP response code
            }
            else
            {
                $this->response(array('error' => 'No data found'), 404);
            }
        }
        else
        {
            $this->response(array('error' => 'Invalid date'), 200);
        }
    }
    
    // getAllBusFormats    
    function getSeatLayout_post() { 
        $res = $this->post();
        // echo "here";exit;
        $layout_name = trim($res['layout_name']);
        $operator_name = trim($res['operator']);
        if(isset($layout_name) && $layout_name!='' && isset($operator_name) && $operator_name!='')
        {

            $operator_id = $this->op_master_details_model->getOperatorId($operator_name);
            
            if($operator_id){
                // Get the detail of bus row count and column count

                $this->db->select('tm.seat_layout_id, obt.op_bus_type_name,osl.no_rows,osl.no_cols, om.op_name');
                $this->db->from('trip_master tm');
                $this->db->join('op_seat_layout osl','osl.id = tm.seat_layout_id');
                $this->db->join('op_bus_types obt','obt.id = tm.op_bus_type_id');
                $this->db->join('op_master om','om.op_id=osl.op_id');
                $this->db->where(array(
                                        'osl.layout_name'=>$layout_name,
                                        'tm.op_id'=>$operator_id['op_id']
                                        )
                                      );
                $query = $this->db->get();
                $bus_data = $query->row_array();

                //Get the seat layout

                $this->db->select('old.seat_no, old.seat_type, old.row_no, old.col_no, old.quota_type');
                $this->db->from('op_layout_details old');
                $this->db->where(array('old.op_seat_layout_id'=>$bus_data['seat_layout_id']));

                $query = $this->db->get();
                $seat_layout = $query->result_array();
                $data['busDetail'] = $bus_data;
                $data['seatLayout'] = $seat_layout;
            }
            else
            {
                 $this->response(array('error'=>'For this operator data is not available, Please enter proper operator name'), 400);
            }
        }
        else
        {
            $data = array('error'=>'Layout name should not be blank');
        }
        if($data)
        {
            $this->response($data, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'No data found'), 404);
        }
        
    }
    
    function getSeatAvailability_post() {
        $config = array(
            array(
                'field' => 'service_id',
                'label' => 'Service ID',
                'rules' => 'trim|required',
                'errors' => array('required' => 'service_id required')
            ),
            array(
                'field' => 'from',
                'label' => 'From stop',
                'rules' => 'trim|required',
                'errors' => array('required' => 'from stop required')
            ),
            array(
                'field' => 'to',
                'label' => 'To stop',
                'rules' => 'trim|required',
                'errors' => array('required' => 'to stop required')
            ),
            array(
                'field' => 'date',
                'label' => 'Journey Date',
                'rules' => 'trim|required',
                'errors' => array('required' => 'Date required')
            ),
            array(
                'field' => 'trip_no',
                'label' => 'Trip No',
                'rules' => 'trim|required',
                'errors' => array('required' => 'trip_no required')
            ),
            array(
                'field' => 'provider_id',
                'label' => 'Provider ID',
                'rules' => 'trim|required',
                'errors' => array('required' => 'provider_id required')
            ),
            array(
                'field' => 'provider_type',
                'label' => 'Provider Type',
                'rules' => 'trim|required',
                'errors' => array('required' => 'provider_type required')
            ),
            array(
                'field' => 'op_id',
                'label' => 'Operator  Id',
                'rules' => 'trim|required',
                'errors' => array('required' => 'op_id required')
            ),
            array(
                'field' => 'operator',
                'label' => 'Operator Name',
                'rules' => 'trim|required',
                'errors' => array('required' => 'operator required')
            ),
            array(
                'field' => 'inventory_type',
                'label' => 'Inventory Type',
                'rules' => 'trim|required',
                'errors' => array('required' => 'inventory type required')
            ),
            array(
                'field' => 'route_schedule_id',
                'label' => 'Operator Name',
                'rules' => 'trim|required',
                'errors' => array('required' => 'route schedule id required')
            )
        );
        // show($config,1);

        if (form_validate_rules($config) == FALSE) {
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
        }

        $input = $this->post();

        if (!(preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/", $input['date'])) > 0 || strtotime($input['date']) >= strtotime(date('Y-m-d'))) {
            $details = $this->common_model->get_provider_lib($this->post("provider_id"), $this->post("provider_type"), $this->post("op_id"));
            $library_name = strtolower(trim($details['library'])) . '_api';
            $data = $this->buses_api->{$library_name}->api_get_seat_layout($this->post());
            unset($data['data']['available_seat']);
            unset($data['data']['available_seat_fare']);
            unset($data['data']['seats_details']);
            if ($data) {
                // log_data('rest/bus_api_new_'.date('d-m-Y').'.log', $data, 'api');
                log_data('rest/bus_new/' . date('M') . '/' . date('d-m-Y') . '/api' . '.log', $data, 'getSeatAvailability');

                $this->response($data, 200);
            } else {
                $this->response(array('status' => 'failed', 'error' => $data), 200);
            }
        } else {
            $data = array("error" => "Either the date in not in format of dd-mm-yyyy or the date has been passed out!");
        }
    }

    function getBoardingStops_post()
    {

        $config = array(
                            array(
                                    'field' => 'service_id', 
                                    'label' => 'service_id', 
                                    'rules' => 'trim|required', 
                                    'errors' => array('required' => 'service_id stop required' )
                                ),
                            array(
                                    'field' => 'from', 
                                    'label' => 'from', 
                                    'rules' => 'trim|required', 
                                    'errors' => array('required' => 'from_stop required' )
                                ),
                            array(
                                    'field' => 'to', 
                                    'label' => 'to', 
                                    'rules' => 'trim|required', 
                                    'errors' => array('required' => 'to_stop required' )
                                ),
                            array(
                                    'field' => 'bus_type_code', 
                                    'label' => 'bus_type_code', 
                                    'rules' => 'trim|required', 
                                    'errors' => array('required' => 'bus_type_code required' )
                                ),
                            array(
                                    'field' => 'provider_id', 
                                    'label' => 'Provider ID', 
                                    'rules' => 'trim|required', 
                                    'errors' => array('required' => 'provider_id required' )
                                ),
                            array(
                                    'field' => 'provider_type', 
                                    'label' => 'Provider Type', 
                                    'rules' => 'trim|required', 
                                    'errors' => array('required' => 'provider_type required' )
                                ),
                            array(
                                    'field' => 'op_id', 
                                    'label' => 'Operator  Id', 
                                    'rules' => 'trim|required', 
                                    'errors' => array('required' => 'op_id required' )
                                ),
                            array(
                                    'field' => 'operator', 
                                    'label' => 'Operator Name', 
                                    'rules' => 'trim|required', 
                                    'errors' => array('required' => 'operator required' )
                                ),
                        );

        if (form_validate_rules($config) == FALSE) 
        {
            $error = $this->form_validation->error_array();
            $this->response(array( 'status' => 'failed' ,'error' => $error), 200);
      
        }
        else
        {
            $input = $this->post();

            if($this->post('provider_type') == 'operator')
            {
                $this->load->model(array(
                                    'tickets_model',
                                    'ticket_details_model',
                                    'ticket_tax_trans_model',
                                )
                            );

                $details = $this->common_model->get_provider_lib($this->post("provider_id"),$this->post("provider_type"),$this->post("op_id"));
                $library_name =  strtolower(trim($details['library'])).'_api';

                $data = $this->buses_api->{$library_name}->apiBoardingStop($input);

                if($data)
                {
                    $this->response(array('status' => 'success', 'data' => $data), 200);
                }
                else{
                    $this->response(array( 'status' => 'failed' , 'data' => 'No data found'), 200);
                }
            }
            else
            {
                $this->response(array('error' => 'Operator not found'), 200);
            }
        }
    }

    function tempBooking_post() 
    { 
        $config = array(
                        array(
                                'field' => 'provider_id', 
                                'label' => 'Provider ID', 
                                'rules' => 'trim|required', 
                                'errors' => array('required' => 'provider_id required' )
                            ),
                        array(
                                'field' => 'provider_type', 
                                'label' => 'Provider Type', 
                                'rules' => 'trim|required', 
                                'errors' => array('required' => 'provider_type required' )
                            ),
                        array(
                                'field' => 'op_id', 
                                'label' => 'Operator  Id', 
                                'rules' => 'trim|required', 
                                'errors' => array('required' => 'op_id required' )
                            ),
                        array(
                                'field' => 'operator', 
                                'label' => 'Operator Name', 
                                'rules' => 'trim|required', 
                                'errors' => array('required' => 'operator required' )
                            ),
                        );

        if (form_validate_rules($config) == FALSE) 
        {
            $error = $this->form_validation->error_array();
            $this->response(array( 'status' => 'failed' ,'error' => $error), 200);
      
        }
        else
        {
            $post = $this->post();
            
            if(!(preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/",$post['date'])) > 0 || strtotime($post['date'])  >= strtotime(date('Y-m-d')))
            {
                $this->load->model(array(
                                    'tickets_model',
                                    'ticket_details_model',
                                    'ticket_tax_trans_model',
                                    'bus_stop_master_model',
                                    'users_model'
                                )
                            );

                $details        = $this->common_model->get_provider_lib($this->post("provider_id"),$this->post("provider_type"),$this->post("op_id"));
                $library_name   = strtolower(trim($details['library'])).'_api';
             
                /*$post['user_id']        = $this->post('user');*/
                $post['mobile_api_user_id']         = $this->user->id;
                $post['api_user_role_name']         = $this->user->role_name;
                $post['api_user_role_id']           = $this->user->role_id;
               
                if(empty(!$post['email'])){
                    $email = $post['email'];
                    $this->users_model->email = $email;
                    $user_exists = $this->users_model->select();
                    if(empty($user_exists))
                    {
                        $this->users_model->email = $post['email'];
                        $this->users_model->mobile_no = $post['mobile_no'];
                        $this->users_model->role_id = USER_ROLE_ID;
                        $user_id = $this->users_model->save();
                    }
                    else
                    {
                        $user_id = $user_exists[0]->id;       
                    }

                    if(!isset($post['user_id']))
                    {
                        $post['user_id']            = $user_id;
                    }
                    // $post['user_id']            = $this->user->id;
                }

                $post['vendor_id']  = (strtolower($this->user->role_name) == 'vendor') ? $this->user->vendor->id : 0; 
                $data = $this->buses_api->{$library_name}->apiTempBooking($post);

                if($data)
                {
                    $this->response($data, 200);
                }
                else
                {
                    $this->response(array('status' => 'failed', 'data' => $data ), 200);
                }
            
            }
            else
            {
                $data =  array("error"=>"Either the date in not in format of dd-mm-yyyy or the date has been passed out!");
            }

        }
        
    }
       
    
    function get_coupon_discount_post()
    {  
        $return = [];
        $input = $this->input->post();
        $this->load->model('tickets_temp_model');
        $this->load->library('discounts');
        $total_psng = 0;
        $final_total_price = 0;
        $tot_fare_amt_without_discount = 0;
        $config = array(
                            array(
                                    'field' => 'promo_code', 
                                    'label' => 'promo code', 
                                    'rules' => 'trim|required', 
                                    'errors' => array('required' => 'promo_code required' )
                                ),
                            array(
                                    'field' => 'add', 
                                    'label' => 'add', 
                                    'rules' => 'trim|required', 
                                    'errors' => array('required' => 'add required' )
                                ),
                            
                        );

        if (form_validate_rules($config) == FALSE) 
        {
            $error = $this->form_validation->error_array();
            $this->response(array( 'status' => 'failed' ,'error' => $error), 200);
      
        }
        else
        {
            if(!empty($input['block_key']) || !empty($input['return_block_key'])){
            if(!empty($input['block_key']) && strtolower($input['add']) == 'add')
            { 
                if(!empty($input['block_key'])){
                    $t_data = $this->tickets_model->find_by(array("ticket_ref_no" => $input['block_key']));
                } else
                {
                    $this->response(array('status' => 'failed' , 'msg' => 'Invalid Block Key'), 200);
                }
                if(!empty($t_data)){
                    $ticketDetail = $this->tickets_model->where(array("ticket_ref_no" => $input['block_key']))->as_array()->find_all();
                    $total_psng += $ticketDetail[0]['num_passgr'];
                    $ticetId = $this->tickets_temp_model->insert($ticketDetail[0]);

                }
            }
            if(!empty($input['return_block_key']) && $input['add'] == 'add' )
            {
                $rt_data = $this->tickets_model->find_by(array("ticket_ref_no" => $input['return_block_key']));
              if(empty($rt_data)){
                    $this->response(array('status' => 'failed' , 'msg' => 'Invalid Return Block Key'), 200);
                }
                $ReturnticketDetail = $this->tickets_model->where(array("ticket_ref_no" => $input['return_block_key']))->as_array()->find_all();
                $total_psng += $ReturnticketDetail[0]['num_passgr'];
                $ticetId = $this->tickets_temp_model->insert($ReturnticketDetail[0]);   
            }
            if(!empty($input['block_key']) && strtolower($input['add']) == 'remove'){
                   
                        $ticketDetail = $this->tickets_temp_model->where(array("ticket_ref_no" => $input['block_key']))->as_array()->find_all();
                        if(!empty($ticketDetail[0]['total_fare_without_discount']))
$return['onward_journey'] = $this->discounts->calculate_generic_coupon_discount($ticketDetail[0]['total_fare_without_discount'],$input['promo_code'],$ticketDetail[0]['num_passgr'],$ticketDetail[0]['inserted_by']);
if($return['onward_journey']['flag'] == 'error')
{
     $this->response(array('status' => 'failed' , 'msg' => 'Coupon code can not apply'), 200);
}
                        $fields_to_update = ['coupon_discount_amount' => '0',
                            "coupon_id" => '0',
                            "tot_fare_amt_with_tax" => $ticketDetail[0]['tot_fare_amt_with_tax'],   
                            "discount_value" => $ticketDetail[0]['discount_value'],
                            "discount_per" => $ticketDetail[0]['discount_per'],
                            "discount_type" => $ticketDetail[0]['discount_type'],
                            "discount_flat_value" => $ticketDetail[0]['discount_flat_value'],
                            "discount_on" =>  $ticketDetail[0]['discount_on'],
                                ];
                        $this->tickets_model->update(array("ticket_ref_no" => $input['block_key']),$fields_to_update);
                        $tot_fare_amt_without_discount = $ticketDetail[0]['total_fare_without_discount'];
                        $final_total_price = $ticketDetail[0]['tot_fare_amt_with_tax'];

                        $return["onward_journey"]["discount_type"] = $ticketDetail[0]['discount_type'];
                        if($ticketDetail[0]['discount_type'] == 'percentage')
                            $return["onward_journey"]["discount_rate"] = isset($ticketDetail[0]['discount_per'])?$ticketDetail[0]['discount_per']:0;
                        else 
                            $return["onward_journey"]["discount_rate"] = isset($ticketDetail[0]['discount_flat_value'])?$ticketDetail[0]['discount_flat_value']:0;
                        $return["onward_journey"]["discount_value"] = isset($ticketDetail[0]['discount_value'])?$ticketDetail[0]['discount_value']:'0';
                        $return["onward_journey"]["discount_on"] =  $ticketDetail[0]['discount_on'];
                        $return["onward_journey"]['seat_fare_with_tax'] = $tot_fare_amt_without_discount;
                        $return["onward_journey"]['seat_fare_with_tax_with_discount'] = $final_total_price;
                        unset($return['onward_journey']['flag']);
                        unset($return['onward_journey']['coupon_id']);
                        unset($return['onward_journey']['total_ticket_discount']);
                        unset($return['onward_journey']['total_price']); 
                        unset($return['onward_journey']['discount_above']);
                        unset($return['onward_journey']['per_seat_ticket_discount']);

            }    

            if(!empty($input['return_block_key']) && strtolower($input['add']) == 'remove'){
                $ReturnticketDetail = $this->tickets_temp_model->where(array("ticket_ref_no" => $input['return_block_key']))->as_array()->find_all();
                if(!empty($ReturnticketDetail[0]['total_fare_without_discount']))
        $return['return_journey'] = $this->discounts->calculate_generic_coupon_discount($ReturnticketDetail[0]['total_fare_without_discount'],$input['promo_code'],$ReturnticketDetail[0]['num_passgr'],$ReturnticketDetail[0]['inserted_by']);     
        if($return['return_journey']['flag'] == 'error')
        {
             $this->response(array('status' => 'failed' , 'msg' => 'Coupon code can not apply'), 200);
        }
              
                $fields_to_update_return = ['coupon_discount_amount' => '0',
                    "coupon_id" => '0',
                    "tot_fare_amt_with_tax" => $ReturnticketDetail[0]['tot_fare_amt_with_tax'],   
                    "discount_value" => $ReturnticketDetail[0]['discount_value'],
                    "discount_per" => $ReturnticketDetail[0]['discount_per'],
                    "discount_type" => $ReturnticketDetail[0]['discount_type'],
                    "discount_flat_value" => $ReturnticketDetail[0]['discount_flat_value'],
                    "discount_on" =>  $ReturnticketDetail[0]['discount_on'],
                        ];
                $this->tickets_model->update(array("ticket_ref_no" => $input['return_block_key']),$fields_to_update_return);
                 $tot_fare_amt_without_discount = $ReturnticketDetail[0]['total_fare_without_discount'];
                $final_total_price = $ReturnticketDetail[0]['tot_fare_amt_with_tax'];

               $return["return_journey"]["discount_type"] = $ReturnticketDetail[0]['discount_type'];
                if($ReturnticketDetail[0]['discount_type'] == 'percentage')
                    $return["return_journey"]["discount_rate"] = isset($ReturnticketDetail[0]['discount_per'])?$ReturnticketDetail[0]['discount_per']:0;
                else 
                    $return["return_journey"]["discount_rate"] = isset($ReturnticketDetail[0]['discount_flat_value'])?$ReturnticketDetail[0]['discount_flat_value']:0;
                $return["return_journey"]["discount_value"] = isset($ReturnticketDetail[0]['discount_value'])?$ReturnticketDetail[0]['discount_value']:'0';
                $return["return_journey"]["discount_on"] =  $ReturnticketDetail[0]['discount_on'];   
                $return["return_journey"]['seat_fare_with_tax'] = $tot_fare_amt_without_discount;
                $return["return_journey"]['seat_fare_with_tax_with_discount'] = $final_total_price;
    $return["return_journey"]['discount_value'] = $tot_fare_amt_without_discount -$final_total_price;
    
                unset($return['return_journey']['flag']);
                unset($return['return_journey']['coupon_id']);
                unset($return['return_journey']['total_ticket_discount']);
                unset($return['return_journey']['total_price']); 
                unset($return['return_journey']['discount_above']);
                unset($return['return_journey']['per_seat_ticket_discount']);
           }
           if(!empty($return)){
             $this->response( array('status' => 'success', 'msg' => 'Discount Coupon Removed Successfully', 'data' => $return ), 200);
           }
           if((!empty($t_data))&& $input['add'] == 'add'){
                if(!empty($ticketDetail[0]['total_fare_without_discount']))
                $return['onward_journey'] = $this->discounts->calculate_generic_coupon_discount($ticketDetail[0]['total_fare_without_discount'],$input['promo_code'],$ticketDetail[0]['num_passgr'],$ticketDetail[0]['inserted_by']);
                    if($return['onward_journey']['flag'] == '@#success#@')
                    {
                        if(!empty($input['block_key']) && $input['add'] == 'add')
                        {  
                            $tot_fare_amt_without_discount = $ticketDetail[0]['total_fare_without_discount'];
                            $tot_fare_amt_with_tax = $ticketDetail[0]['total_fare_without_discount'] - ($return['onward_journey']['per_seat_ticket_discount'] * $ticketDetail[0]['num_passgr']);

                            $final_total_price +=  $tot_fare_amt_with_tax;
                            $tikcet_update_array = [
                                                    "tot_fare_amt_with_tax" => $tot_fare_amt_with_tax,
                                                    "discount_value" => '',
                                                    "discount_per" => '0.00',
                                                    "discount_type" => '',
                                                    "discount_flat_value" => '',
                                                    "discount_on" => '',
                                                    "discount_id" => '',
                                                    "coupon_discount_amount" => $return['onward_journey']['per_seat_ticket_discount'] * $ticketDetail[0]['num_passgr'],
                                                    "coupon_id" => $return['onward_journey']['coupon_id']
                                                    ];
                            $this->tickets_model->update_where(array("ticket_ref_no" => $input['block_key']),"",$tikcet_update_array);
                        $return['onward_journey']['seat_fare_with_tax'] = $tot_fare_amt_without_discount;
                        $return['onward_journey']['seat_fare_with_tax_with_discount'] = $final_total_price;
                        $return['onward_journey']['discount_value'] = $tot_fare_amt_without_discount - $final_total_price;
                        if($return['onward_journey']['per_seat_ticket_discount']){
                           $return['onward_journey']['discount_rate'] = $return['onward_journey']['per_seat_ticket_discount'];
                        }
                        unset($return['onward_journey']['flag']);
                        unset($return['onward_journey']['coupon_id']);
                        unset($return['onward_journey']['total_ticket_discount']);
                        unset($return['onward_journey']['total_price']); 
                        unset($return['onward_journey']['discount_above']);
                        unset($return['onward_journey']['per_seat_ticket_discount']);

                        }

                   }  else {
                   $this->response(array('status' => 'failed' , 'msg' => 'Coupon code can not apply'), 200);
                  }
            }
                    
           if((!empty($rt_data))&& $input['add'] == 'add'){                
                if(!empty($ReturnticketDetail[0]['total_fare_without_discount']))
                $return['return_journey'] = $this->discounts->calculate_generic_coupon_discount($ReturnticketDetail[0]['total_fare_without_discount'],$input['promo_code'],$ReturnticketDetail[0]['num_passgr'],$ReturnticketDetail[0]['inserted_by']);     
        if($return['return_journey']['flag'] == '@#success#@')
                    {
                        if(!empty($input['return_block_key']) && $input['add'] == 'add')
                        { 
                            $tot_fare_amt_without_discount = $ReturnticketDetail[0]['total_fare_without_discount'];
                           $tot_fare_amt_with_tax = $ReturnticketDetail[0]['total_fare_without_discount'] - ($return['return_journey']['per_seat_ticket_discount'] * $ReturnticketDetail[0]['num_passgr']);
                           $final_total_price = $tot_fare_amt_with_tax;
                            $tikcet_update_array = [
                                                    "tot_fare_amt_with_tax" => $tot_fare_amt_with_tax,
                                                    "discount_value" => '',
                                                    "discount_per" => '0.00',
                                                    "discount_type" => '',
                                                    "discount_flat_value" => '',
                                                    "discount_on" => '',
                                                    "discount_id" => '',
                                                    "coupon_discount_amount" => $return['return_journey']['per_seat_ticket_discount'] * $ReturnticketDetail[0]['num_passgr'],
                                                    "coupon_id" => $return['coupon_id']
                                                    ];
                            $this->tickets_model->update_where(array("ticket_ref_no" => $input['return_block_key']),"",$tikcet_update_array); 
                        $return['return_journey']['seat_fare_with_tax'] = $tot_fare_amt_without_discount;
                        $return['return_journey']['seat_fare_with_tax_with_discount'] = $final_total_price;
                        $return['return_journey']['discount_value'] = $tot_fare_amt_without_discount - $final_total_price;
                        if($return['return_journey']['per_seat_ticket_discount']){
                           $return['return_journey']['discount_rate'] = $return['return_journey']['per_seat_ticket_discount'];
                        }


                        unset($return['return_journey']['flag']);
                        unset($return['return_journey']['coupon_id']);
                        unset($return['return_journey']['total_ticket_discount']);
                        unset($return['return_journey']['total_price']); 
                        unset($return['return_journey']['discount_above']);
                        unset($return['return_journey']['per_seat_ticket_discount']);
                        }
                    } else {
                   $this->response(array('status' => 'failed' , 'msg' => 'Coupon code can not apply'), 200);
                  }

                    }
           if(!empty($return))
           $this->response( array('status' => 'success', 'msg' => 'Coupon Code Applied Successfully', 'data' => $return ), 200);
            }else
            {
                $this->response(array('status' => 'failed' , 'msg' => 'Please enter either block_key or return_block_key'), 200);
            }
          } 
		
	}

    function doConfirmBooking_post()
    {  
        $config = array(
                            array(
                                    'field' => 'block_key', 
                                    'label' => 'block key', 
                                    'rules' => 'trim|required', 
                                    'errors' => array('required' => 'block_key stop required' )
                                ),
                            array(
                                    'field' => 'unique_id', 
                                    'label' => 'unique_id', 
                                    'rules' => 'trim|required', 
                                    'errors' => array('required' => 'unique_id required' )
                                ),
                            array(
                                    'field' => 'seat_count', 
                                    'label' => 'Seat count', 
                                    'rules' => 'trim|required', 
                                    'errors' => array('required' => 'seat_count required' )
                                ),
                            
                        );

        if (form_validate_rules($config) == FALSE) 
        {
            $error = $this->form_validation->error_array();
            $this->response(array( 'status' => 'failed' ,'error' => $error), 200);
      
        }
        else
        {
            $input      = $this->post();
            $ticket     = $this->tickets_model
                                    // ->where('transaction_status !=', 'success')
                                    ->find_by(array('ticket_ref_no' => $input['block_key']));
            if($ticket)
            {
                if($ticket->pg_tracking_id == "")
                {
                    $this->response(array( 'status' => 'failed' , 'error' => 'Please make a payment to complete this transaction.'), 200);
                }

                if($ticket->transaction_status != 'success' && $ticket->pg_tracking_id != "")
                {
                    
                    $this->load->model(array(
                                        'tickets_model',
                                        'ticket_details_model',
                                        'ticket_tax_trans_model',
                                    )
                                );

                    $details = $this->common_model->get_provider_lib($ticket->provider_id,$ticket->provider_type,$ticket->op_id);
                    $library_name =  strtolower(trim($details['library'])).'_api';

                    $data = $this->buses_api->{$library_name}->apiDoconfirmBooking($input, (array)$ticket);

                    if($data)
                    {
                        $this->response($data, 200);
                    }
                    else{
                        $this->response(array( 'status' => 'failed' , 'api failed'), 200);
                    }
                }
                else
                {
                    $this->response(array( 'status' => 'failed' , 'error' => 'Seat already booked. Please select another seat !'), 200);    
                }
            }
            else
            {
                $this->response(array( 'status' => 'failed' , 'error' => 'Invalid Block key, Ticket not found.'), 200);
            }
        }
    }

    function is_ticket_cancelable_post()
    {
        $config = array(
                            array(
                                    'field' => 'bos_ref_no', 
                                    'label' => 'bos_ref_no', 
                                    'rules' => 'trim|required', 
                                    'errors' => array('required' => 'Bos reference number required.' )
                                ),
                        );

        if (form_validate_rules($config) == FALSE) 
        {
            $error = $this->form_validation->error_array();
            $this->response(array( 'status' => 'failed' ,'msg' => $error), 200);
      
        }
        else
        {
            $input = $this->post();
                
            $ticket = false;

            if(!empty($input['pnr']) )
            {
              $ticket = $this->tickets_model->find_by('pnr_no', $input["pnr"]);
            }
            elseif( !empty($input['block_key']))
            {
              $ticket = $this->tickets_model->find_by('ticket_ref_no', $input["block_key"]);
            }
            elseif( !empty($input['bos_ref_no']) )
            {
              $ticket =$this->tickets_model->find_by('boss_ref_no', $input["bos_ref_no"]);
        
            }

            if(!$ticket)
            {
                 $this->response(array('status' => 'failed' , 'msg' => 'No ticket found for the given detail'), 200);
            }            
            if(!in_array($ticket->payment_confirmation, array("full","partial","auto_confirmed")))
            {
                $this->response(array('status' => 'failed' , 'msg' => 'Your payment is not confirmed yet, Please try again after sometime.'), 200);
            }
            if((strtolower($ticket->payment_by) == "wallet") || (time() > strtotime("+60 minutes", strtotime($ticket->inserted_date))))
            {
                $details        = $this->common_model->get_provider_lib($ticket->provider_id, $ticket->provider_type, $ticket->op_id);

                $library_name   =  strtolower(trim($details['library'])).'_api';


                $data = $this->buses_api->{$library_name}->apiIsTicketCancelable($ticket, $input);

                log_data('rest/bus_new/'.date('M').'/'.date('d-m-Y').'/request'.'.log', $data, 'is_ticket_cancelable_post response');

                if($data)
                {
                    $this->response($data, 200);
                }
                else
                {
                    $this->response(array( 'status' => 'failed' , 'msg' => 'Api failed try again.'), 200);
                }
            }
            else
            {
                return array( 'status' => 'failed', 'msg' => 'Could not initiate refund till 1 hour after booking.');
            }
        }
    }

    function cancelTicket_post()
    {   
        $this->load->model(array('return_ticket_mapping_model'));
        /*$config = array(
                            array(
                                    'field' => 'bos_ref_no', 
                                    'label' => 'bos_ref_no', 
                                    'rules' => 'trim|require', 
                                    'errors' => array('required' => 'Bos reference number required' )
                                ),
                        );*/

        $config = array(
                array(
                    'field' => 'pnr', 
                    'label' => 'pnr', 
                    'rules' => 'trim|require', 
                    'errors' => array('required' => 'PNR number required' )
                ),
            );

        $input = $this->post();
        
         /*if (form_validate_rules($config) == FALSE) 
        {
            $error = $this->form_validation->error_array();
            $this->response(array( 'status' => 'failed' ,'msg' => $error), 200);
        }*/
        if(isset($input['pnr']) && $input['pnr'] == "")
        {
           $this->response(array( 'status' => 'failed' ,'msg' => "Please provide PNR number."), 200);
        }
        else
        {
            $input = $this->post();
                
            $ticket = false;

            if(!empty($input['pnr']) )
            {
              $ticket = $this->tickets_model->find_by('pnr_no', $input["pnr"]);
            }
            elseif( !empty($input['block_key']))
            {
              $ticket = $this->tickets_model->find_by('ticket_ref_no', $input["block_key"]);
            }
            elseif( !empty($input['bos_ref_no']) )
            {
              $ticket = $this->tickets_model->find_by('boss_ref_no', $input["bos_ref_no"]);
            }

            if(!$ticket)
            {
                $this->response(array( 'status' => 'failed' , 'msg' => 'No ticket found for the given detail'), 200);
            }
            if(!in_array($ticket->payment_confirmation, array("full","partial","auto_confirmed")))
            {
                $this->response(array( 'status' => 'failed' , 'msg' => 'Your payment is not confirmed yet, Please try again after sometime.'), 200);
            }

            if((strtolower($ticket->payment_by) == "wallet") || (time() > strtotime("+60 minutes", strtotime($ticket->inserted_date))))
            {
                if(strtolower($ticket->payment_by) == "wallet")
                {
                    $this->response(array( 'status' => 'failed' , 'msg' => 'Can not cancel ticket , Payment made by wallet.'), 200);
                }

                $check_is_return_ticket_refund = $this->return_ticket_mapping_model->check_is_return_ticket_refund($ticket->ticket_id);

                if($check_is_return_ticket_refund)
                {
                    if($check_is_return_ticket_refund["onward_transaction_status"] != "success" || $check_is_return_ticket_refund["return_transaction_status"] != "success" )
                    {
                        $this->response(array( 'status' => 'failed' , 'msg' => 'In case of Round trip, only one trip can be cancelled (Onward Or Return). You have already cancelled one of them therefore you cannot cancel the other trip.'), 200);
                    }
                }

                $details        = $this->common_model->get_provider_lib($ticket->provider_id, $ticket->provider_type, $ticket->op_id);

                $library_name   =  strtolower(trim($details['library'])).'_api';
                //echo $library_name; exit;
                $data = $this->buses_api->{$library_name}->apiCancelTicket($ticket, $input);

                log_data('rest/bus_new/'.date('M').'/'.date('d-m-Y').'/request'.'.log', $data, 'cancelTicket response');

                if($data)
                {
                    $this->response($data, 200);
                }
                else
                {
                    $this->response(array( 'status' => 'failed' , 'msg' => 'Api failed try again.'), 200);
                }
            }
            else
            {
                $this->response(array( 'status' => 'failed' , 'msg' => 'Could not initiate refund till 1 hour after booking.'), 200);
            }
        }
    }

    function cancelTicket_post_original()
    {   
        $config = array(
                            array(
                                    'field' => 'bos_ref_no', 
                                    'label' => 'bos_ref_no', 
                                    'rules' => 'trim|require', 
                                    'errors' => array('required' => 'Bos reference number required' )
                                ),
                        );

        if (form_validate_rules($config) == FALSE) 
        {
            $error = $this->form_validation->error_array();
            $this->response(array( 'status' => 'failed' ,'msg' => $error), 200);
        }
        else
        {
            $input = $this->post();
                
            $ticket = false;

            if( !empty($input['pnr']) )
            {
              $ticket = $this->tickets_model->find_by('pnr_no', $input["pnr"]);
            }
            elseif( !empty($input['block_key']))
            {
              $ticket = $this->tickets_model->find_by('ticket_ref_no', $input["block_key"]);
            }
            elseif( !empty($input['bos_ref_no']) )
            {
              $ticket =$this->tickets_model->find_by('boss_ref_no', $input["bos_ref_no"]);
            }

            if(!$ticket)
            {
                return array( 'status' => 'failed', 'msg' => 'No ticket found for the given detail');
            }

            $details        = $this->common_model->get_provider_lib($ticket->provider_id, $ticket->provider_type, $ticket->op_id);

            $library_name   =  strtolower(trim($details['library'])).'_api';


            $data = $this->buses_api->{$library_name}->apiCancelTicket($ticket, $input);

            log_data('rest/bus_new/'.date('M').'/'.date('d-m-Y').'/request'.'.log', $data, 'cancelTicket response');

            if($data)
            {
                $this->response($data, 200);
            }
            else
            {
                $this->response(array( 'status' => 'failed' , 'msg' => 'Api failed try again.'), 200);
            }
        }
    }

    function ticketTransaction_post()
    {

         $config = array(
                        array(
                                'field' => 'email', 
                                'label' => 'email', 
                                'rules' => 'trim|required', 
                                'errors' => array('required' => 'email required' )
                            ),
                        // array(
                        //         'field' => 'mobile', 
                        //         'label' => 'mobile', 
                        //         'rules' => 'trim|required|numeric', 
                        //         'errors' => array('required' => 'mobile required' )
                        //     ),
                        array(
                                'field' => 'from_date', 
                                'label' => 'from_date', 
                                'rules' => 'trim|required', 
                                'errors' => array('required' => 'from_date required' )
                            ), 
                        array(
                                'field' => 'till_date', 
                                'label' => 'till_date', 
                                'rules' => 'trim|required', 
                                'errors' => array('required' => 'till_date required' )
                            ),                        
                        );

        if (form_validate_rules($config) == FALSE) 
        {
            $error = $this->form_validation->error_array();
            $this->response(array( 'status' => 'failed' ,'error' => $error), 200);
      
        }
        else
        {
            $this->load->model(array('tickets_model'));


            $data = $ticket = $this->tickets_model->get_ticket_trans($this->post(), '');

            if($data)
            {
                $this->response(array( 'status' => 'success' ,'data' => $data), 200);   
            }
            else
            {
                $this->response(array( 'status' => 'failed' ,'data' => 'No Tickets Available'), 200);
            }

        } 
    }

    function cancellableTicketTransaction_post()
    {

         $config = array(
                        array(
                                'field' => 'email', 
                                'label' => 'email', 
                                'rules' => 'trim', 
                                'errors' => array('required' => 'email required' )
                            ),
                         array(
                                 'field' => 'mobile_no', 
                                 'label' => 'mobile_no', 
                                 'rules' => 'trim|numeric', 
                                 'errors' => array('required' => 'mobile required' )
                             ),
                             array(
                                 'field' => 'pnr_no', 
                                 'label' => 'pnr_no', 
                                 'rules' => 'trim', 
                                 'errors' => array('required' => 'pnr number required' )
                             ),
                              array(
                                 'field' => 'bos_ref', 
                                 'label' => 'bos_ref', 
                                 'rules' => 'trim', 
                                 'errors' => array('required' => 'bos reference number required' )
                             ),
                              array(
                                 'field' => 'ticket_ref_no', 
                                 'label' => 'ticket_ref_no', 
                                 'rules' => 'trim', 
                                 'errors' => array('required' => 'ticket ref no required' )
                             ),
                    );

         // show($this->form_validation);

        if(!form_validate_rules($config)) 
        {
            $error = $this->form_validation->error_array();
            if(empty($error))
            {
                $error = array( '0' => 'Please send the required parameter' );
            }

            $this->response(array( 'status' => 'failed' ,'error' => $error), 200);
      
        }
        else
        {
            $this->load->model(array('tickets_model'));
            // show($this->post(),1);
            $data = $ticket = $this->tickets_model->get_cancellable_ticket_trans($this->post(), $this->post('status'));

            if($data)
            {
                $this->response(array( 'status' => 'success' ,'data' => $data), 200);   
            }
            else
            {
                $this->response(array( 'status' => 'failed' ,'data' => 'No Ticket Available'), 200);
            }

        } 
    }

    function smsTicket_post(){
         $config = array(
                        // array(
                        //         'field' => 'email', 
                        //         'label' => 'email', 
                        //         'rules' => 'trim|required', 
                        //         'errors' => array('required' => 'email required' )
                        // ),
                        array(
                                'field' => 'pnr',
                                'label' => 'pnr',
                                'rules' => 'trim|required',
                                'errors' => array('required' => 'PNR required')
                            ),
                    );

        if(!form_validate_rules($config)) 
        {
            $error = $this->form_validation->error_array();
            $this->response(array( 'status' => 'failed' ,'msg' => $error), 200);
        }
        else
        {
            $response = sms_ticket($this->post('pnr'));
            $this->response($response, 200);
        }
    }

    function emailTicket_post(){
        $config = array(
                        // array(
                        //         'field' => 'email', 
                        //         'label' => 'email', 
                        //         'rules' => 'trim|required', 
                        //         'errors' => array('required' => 'email required' )
                        // ),
                        array(
                                'field' => 'pnr',
                                'label' => 'pnr',
                                'rules' => 'trim|required',
                                'errors' => array('required' => 'PNR required')
                            ),
                    );

        if(!form_validate_rules($config)) 
        {
            $error = $this->form_validation->error_array();
            $this->response(array( 'status' => 'failed' ,'msg' => $error), 200);
        }
        else
        {
            $response = mail_ticket($this->post('pnr'), $this->user->id);
            // show($response);

            $this->response($response, 200);
        }
    }

    /************************  End of ors Section   ************************/

    /************************  Current Booking Section start  ************************/
    
    //  gets bus  stop
    function getCbBusStops_post()
    {
        $this->db2->from('bus_stops');
        
        $page       = (isset($input['page']) && $input['page'] > 0 ) ? $input['page'] : 0;
        $per_page   = (isset($input['per_page']) && $input['per_page'] > 0 ) ? $input['per_page'] : 100;


        $data   = $this->db2
                        ->limit($per_page, $page)
                        ->get();
       
        $res=$data->result_array();
        
        foreach($res as $key=>$value)
        {
            $res[$key]['op_name']='UPSRTC';
        }
       
        if($res)
        {
            $this->response($res, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'No data found'), 404);
        }
    }
    
    
    function getCbRoutes_post()
    {
        $this->db2->from('routes');     
        $data=  $this->db2->get();
       
        $res=$data->result_array();
        
        foreach($res as $key=>$value)
        {
            $res[$key]['op_name']='UPSRTC';
        }
       
         if($res)
        {
            $this->response($res, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'No data found'), 404);
        }        
    }

    
    function getCbRouteStops_post()
    {        
        $input=$this->post();
        
        if($input['route_no']!="")
        {
            $this->db2->select('*'); 
            $this->db2->where('route_no',$input['route_no']);
            $this->db2->from('routes');
            $data=$this->db2->get();
            $cnt_route=$data->result_array();
            
            if(count($cnt_route)==0)
            {
                $res=array("Route Number  does not exist");
            }
           else
           {    
                $this->db2->from('route_stops');
                $this->db2->where('route_no',$input['route_no']);
                $data   =  $this->db2->get();
                $res    =  $data->result_array();
                if(!empty($res))
                {
                    foreach($res as $key=>$value)
                    {
                        $res[$key]['op_name']='UPSRTC';
                    }
        
                }
            }            
        }
        else
        {
             $this->response(array('error' => 'Please provide Route Number')); 
        }
         if($res)
        {
            $this->response($res, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'No data found'), 404);
        }
        
    }
    
    //get bus service wise route_stops
    function getCbServiceStops_post()
    {
        $input  = $this->post();
        if($input['route_no'] != "" || $input['bus_type'] != "")
        {       
            $this->db2->select('*'); 
            $this->db2->where('route_no',$input['route_no']);
            $this->db2->from('routes');
            $data=$this->db2->get();
            $cnt_route=$data->result_array();
               
            
            $this->db2->select('*'); 
            $this->db2->where('bus_type_cd',$input['bus_type']);
            $this->db2->from('bus_types');
            $data=$this->db2->get();
            $cnt_bus_types=$data->result_array();
            
            
            if(count($cnt_route) == 0 || count($cnt_bus_types) == 0)
            {
                $res=array("Route Number or Bus type does not exist");
            }
            else
            {
                $this->db2->from('service_route_stops');
                $this->db2->where('route_no',$input['route_no']);
                $this->db2->where('bus_type_cd',$input['bus_type']);
                $data=  $this->db2->get();
                $res=$data->result_array();
                if(!empty($res))
                {
                    foreach($res as $key=>$value)
                    {
                        $res[$key]['op_name']='UPSRTC';
                    } 
                }
                     
            }
        }
        else
        {
          $this->response(array('error' => 'Please provide Route Number'));   
        }   
        
        if($res)
        {
            $this->response($res, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'No data found'), 404);
        }
    }

    function getAllBusTypes_post()
    {
        $this->db2->from('bus_types');

        $data   = $this->db2->get();
        $res    = $data->result_array();
        
        foreach($res as $key=>$value)
        {
            $res[$key]['op_name']='UPSRTC';
        }
       
        if($res)
        {
            $this->response($res, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'No data found'), 404);
        }
    }

    function getAllCbBusServices_post()
    {
        $input = $this->post();

        $page       = (isset($input['page']) && $input['page'] > 0 ) ? $input['page'] : 0;
        $per_page   = (isset($input['per_page']) && $input['per_page'] > 0 ) ? $input['per_page'] : 100;

        $this->db2
                ->from('bus_services')
                ->limit($per_page, $page)
                ->where('EFFECTIVE_TILL > now()');

        $data   = $this->db2->get();

        $res    = $data->result_array();
        
        foreach($res as $key=>$value)
        {
            $res[$key]['op_name']='UPSRTC';
        }   
       
        if($res)
        {
            $this->response($res, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'No data found'), 404);
        }
        
    }


    /* Methods  : getCbServices_post
     * Author   : pankaj
     * Desc     : fetch current booking bus services
     * 
     */
    function getCbServices_post()
    {
                
        $input      = $this->post();
        $current_date  = strtotime(date('Y-m-d'));
        $pass_date   = strtotime($input['date']);
        
        if($input['date']!="")
        {
            if((preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/",$input['date'])) && $pass_date==$current_date)
            {
                $this->db2->select('cb.bus_service_no,rs.route_no,cb.bus_type_cd,bt.bus_type_nm,cb.DEPT_TM as Departure_time,cb.VEHICLE_NO,cb.CONDUCTOR_NM,concat(bus_st.BUS_STOP_NM, "To ",bus_st1.BUS_STOP_NM,IF(rs.VIA_STOP_CD IS NULL," ",concat(" VIA ",bus_st2.BUS_STOP_NM))) as route_name',FALSE);
                $this->db2->from("current_booking_buses cb");   
                $this->db2->join("bus_services bs","cb.bus_Service_no=bs.bus_service_no","inner");
                $this->db2->join("routes rs","bs.route_no=rs.route_no","inner");
                $this->db2->join("bus_types bt","bt.BUS_TYPE_CD=cb.BUS_TYPE_CD","inner");
                $this->db2->join("bus_stops bus_st","rs.from_stop_cd=bus_st.bus_stop_cd","inner");
                $this->db2->join("bus_stops bus_st1","rs.till_stop_cd=bus_st1.bus_stop_cd","inner");
                $this->db2->join("bus_stops bus_st2","rs.via_stop_cd=bus_st2.bus_stop_cd","left");
                $this->db2->where("cb.dsa_no",NULL);
                $this->db2->where("cb.dept_tm > now()");
                $this->db2->where("cb.booking_status","C");       
                $this->db2->where('DATE(cb.dept_tm)',date("Y-m-d",strtotime($input['date'])));
                
                $query  = $this->db2->get();
                $data   = $query->result_array();
                
                foreach($data as $key=>$value)
                {
                    $data[$key]['op_name']='UPSRTC';
                }   
            }
            else
            {
                $data =  array("error"=>"Either the date in not in format of dd-mm-yyyy or Please provide current date!");
            }
        }
        else
        {
            $data =  array("error"=>"Please provide Date");
        }

        if($data)
        {
            $this->response($data, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'No data found'), 404);
        }
    }    
    
    
    function getfare_post()
    {
        $input=$this->post();

        $this->load->library('upsrtc_booking/calfunction.php');
        $paraArray['PROC']          =$input['proc'];
        $paraArray['DEPT_DT']       = $input['dept_dt'];
        $paraArray['BUS_SERVICE_NO'] =$input['bus_service_no'] ;
        $paraArray['ROUTE_NO']       =$input['route_no'] ;
        // $paraArray['BRD_STOP'] =$this->op_stop_master_model->bus_stop_cd($input['brd_stop'],$input['op_id']);
        // $paraArray['ALT_STOP'] =$this->op_stop_master_model->bus_stop_cd($input['alt_stop'],$input['op_id']);
        $paraArray['BRD_STOP']        =$input['boarding_stop'];
        $paraArray['ALT_STOP']        =$input['alighting_stop'];
        $paraArray['NO_PAX']          = 1;
        $paraArray['PAY_MODE']        =1;
        $paraArray['CAL_METHOD']      =$input['cal_method'];
        $paraArray['PAX_CONC']        ='A';
        $paraArray['PAX_AGE']=30;
        $paraArray['BUS_TYPE_CD'] =$input['bus_type'];
        $paraArray['BUS_TYPE_CD'] = 'ORD';
        $fareObj = new Calfunction();
        $fare['cal_fare'] = $fareObj->getFare($paraArray); 
     
        if($fare)
        {
            $this->response($fare, 200); // 200 being the HTTP response code
        }
        else
        {
            $this->response(array('error' => 'No data found'), 404);
        }    
    }
     
     /* Methods : getBusOperatorOnHomePage_post
     * Author   : Sagar
     * Desc     : fetch Bus Operator on home page
     * 
     */
    
    function getBusOperator_post(){
        
        $this->load->model('bus_operator_model');
        $result = $this->bus_operator_model->column('id, name')->where(array('is_status'=>'Y'))->find_all();
        if($result)
        {
            $this->response( array('status' => 'success', 'msg' => 'Bus Operator found', 'data' => $result ), 200);
        }else{
            $this->response( array('status' => 'success', 'msg' => 'No Bus Operator found' ), 200);
        }
    }
    
    /* Methods : getBusTypesOnHomePage_post
     * Author   : Sagar
     * Desc     : fetch Bus Operator on home page
     * 
     */
    
    function getBusTypeOnHomePage_post(){
        
        $this->load->model('bus_type_home_model');
        $result = $this->bus_type_home_model->column('id, name')->where(array('is_status'=>'Y'))->find_all();
        if($result)
        {
            $this->response( array('status' => 'success', 'msg' => 'Bus Type found', 'data' => $result ), 200);
        }else{
            $this->response( array('status' => 'success', 'msg' => 'No Bus Type found' ), 200);
        }
    }
    
    /* Methods :  getChangeInStops
     * Author  : Sagar
     * Desc    : getChangeInStops
     * 
     */
    
    function getChangeInStops_post(){
        
        $date = date('Y-m-d');
        $newdate = strtotime("-1 day" , strtotime($date));
        $newdate = date("Y-m-d" , $newdate );
        $this->load->model('bus_stop_master_model');
        $result = $this->bus_stop_master_model->column('bus_stop_name as value,bus_stop_name as label')->where("update_date between '".$newdate."' and '".date("Y-m-d")."' or insert_date between '".$newdate."' and '".date("Y-m-d")."'")->find_all();
        if($result)
        {
            $this->response( array('status' => 'success', 'msg' => 'Change In Stops', 'data' => $result ), 200);
        }else{
            $this->response( array('status' => 'success', 'msg' => 'No Change In Stops' ), 200);
        }
    }
    
     
    /* Methods : Departure Master
     * Author   : Sagar
     * Desc     : get Departure time
     * 
     */
    
    function getDepartureTime_post(){
        
        $this->load->model('departure_time_master_model');
        $result = $this->departure_time_master_model->column('id, concat(concat_ws("-",start_time, end_time), display_name) as departure_time')->where(array('is_status'=>'Y'))->find_all();
        if($result)
        {
            $this->response( array('status' => 'success', 'msg' => 'Departure Time Found', 'data' => $result ), 200);
        }else{
            $this->response( array('status' => 'success', 'msg' => 'No Departure Time Found' ), 200);
        }
    }
    
    /* Methods : Duration Master
     * Author   : Sagar
     * Desc     : get Duration
     * 
     */
    
    function getDuration_post(){
        
        $this->load->model('duration_master_model');
        $result = $this->duration_master_model->column('id, duration')->where(array('is_status'=>'Y'))->find_all();
        if($result)
        {
            $this->response( array('status' => 'success', 'msg' => 'Duration found', 'data' => $result ), 200);
        }else{
            $this->response( array('status' => 'success', 'msg' => 'No Duration found' ), 200);
        }
    }
    
     /* Methods : BusType Master
     * Author   : Sagar
     * Desc     : get BusType
     * 
     */
    
    function getBusType_post(){
        
        $this->load->model('bus_type_model');
        $result = $this->bus_type_model->column('id, bus_type_name')->where(array('record_status'=>'Y'))->find_all();
        if($result)
        {
            $this->response(array('status' => 'success', 'msg' => 'Bus Type found', 'data' => $result ), 200);
        }else{
            $this->response(array('status' => 'success', 'msg' => 'No Bus Type found' ), 200);
        }
    }
    
     /* Methods :  getChangeInBusType
     * Author  : Sagar
     * Desc    : getChangeInBusType
     * 
     */
    
    function getChangeInBusType_post(){
        
        $date = date('Y-m-d');
        $newdate = strtotime("-1 day" , strtotime($date));
        $newdate = date("Y-m-d 00:00:01" , $newdate );
        $this->load->model('bus_type_model');
        $result = $this->bus_type_model->column('id, bus_type_name')->where("updated_date between '".$newdate."' and '".date("Y-m-d 23:59:59")."' or created_date between '".$newdate."' and '".date("Y-m-d 23:59:59")."'")->find_all();
        if($result)
        {
            $this->response(array('status' => 'success', 'msg' => 'Change In Bus Type', 'data' => $result), 200);
        }else{
            $this->response(array('status' => 'success', 'msg' => 'No Change In Bus Type'), 200);
        }
    }
    
     /* Methods :  getBosAndProviderBusType
     * Author  : Sagar
     * Desc    : Mapping between Bus Types of Bos and bus types from multiple operators
     * 
     */
    
    function getBosAndProviderBusType_post(){
//        $this->load->model('api_bus_types_model');
//        $this->load->model('op_bus_types_model');
//        $this->load->model('bus_type_model');
//        $this->db->select("api_bus_type, bt.bus_type_name");
//        $this->db->from("api_bus_types abt");
//        $this->db->join("bus_type bt","bt.id =abt.bus_type_id","left");
//        $query = $this->db->get();
//        $result = $query->result_array();
//        
//        $this->db->select("op_bus_type_name, bt.bus_type_name");
//        $this->db->from("op_bus_types obt");
//        $this->db->join("bus_type bt","bt.id =obt.bus_type_id","left");
//        $query = $this->db->get();
//        $result1 =  $query->result_array();
//        $result = array_merge($result, $result1);
         $bus_types = $this->bus_type_model->column("id, bus_type_name")->as_array()->find_all();
         $bus_type_ids = '';
         foreach ($bus_types as $bus_type) {
             $bus_type_ids .= "'".$bus_type['id']."',";
         }
        $bus_type_ids =  rtrim($bus_type_ids,',');
            
        $query = $this->db->query('select * from (SELECT op_bus_type_name, bus_type_id, bus_type.bus_type_name FROM `op_bus_types` left join bus_type on bus_type.id =   op_bus_types.bus_type_id where op_bus_types.bus_type_id in ('.$bus_type_ids.') union all SELECT api_bus_type, bus_type_id, bus_type.bus_type_name FROM `api_bus_types` left join bus_type on bus_type.id =   api_bus_types.bus_type_id where bus_type_id in ('.$bus_type_ids.')) t  order by t.bus_type_id asc');
        $results = $query->result_array();
        $final_result = array();
        foreach($bus_types as $bus_type){
            $final_result[$bus_type['bus_type_name']] = array();
        }
        foreach ($results as $result)
        {
            
            if(array_key_exists($result['bus_type_name'], $final_result)){ 
                $final_result[$result['bus_type_name']][] = $result['op_bus_type_name'];
            }
            
        }
        if($final_result)
        {
            $this->response(array('status' => 'success', 'msg' => 'Bos and Provider Bus Type', 'data' => $final_result), 200);
        }else{
            $this->response(array('status' => 'success', 'msg' => 'No Bos and Provider Bus Type'), 200);
        }
    }
    
     /* Methods :  getChangeInBosAndProviderBusType
     * Author  : Sagar
     * Desc    : If any change in Bus types, then this API will provide newly added or deleted bus types (Is there change & if yes, changed stops)
     * 
     */
    
    function getChangeInBosAndProviderBusType_post(){
        $date = date('Y-m-d');
        $newdate = strtotime("-1 day" , strtotime($date));
        $newdate = date("Y-m-d 00:00:01" , $newdate );
        
        $this->load->model('api_bus_types_model');
        $this->load->model('op_bus_types_model');
        $this->load->model('bus_type_model');
        $this->db->select("api_bus_type, bt.bus_type_name");
        $this->db->from("api_bus_types abt");
        $this->db->join("bus_type bt","bt.id = abt.bus_type_id","left");
        $this->db->where("abt.timestamp between '".$newdate."' and '".date("Y-m-d 23:59:59")."'");
        $query = $this->db->get();
        $result =  $query->result_array();
        
        $this->db->select("op_bus_type_name, bt.bus_type_name");
        $this->db->from("op_bus_types obt");
        $this->db->join("bus_type bt","bt.id =obt.bus_type_id");
        $this->db->where("obt.updated_date between '".$newdate."' and '".date("Y-m-d 23:59:59")."' or obt.created_date between '".$newdate."' and '".date("Y-m-d 23:59:59")."'");
        $query = $this->db->get();
        $result1 =  $query->result_array();
        $result = array_merge($result, $result1);
                
        if($result)
        {
            $this->response(array('status' => 'success', 'msg' => 'Bos and Provider Bus Type', 'data' => $result), 200);
        }else{
            $this->response(array('status' => 'success', 'msg' => 'No Change In Bos and Provider Bus Type'), 200);
        }
    }
    
    
     /* Methods :  getGroupedProviders
     * Author  : Sagar
     * Desc    : Grouping for Operator Master
     * 
     */
    
    function getGroupedProviders_post(){
        $this->db->select("op_name as provider_name");
        $this->db->from("op_master");
        $this->db->where("is_grouping_applicable","Y");
        $query = $this->db->get();
        $result =  $query->result_array();
        if($result)
        {
            $this->response(array('status' => 'success', 'msg' => 'Grouped Providers', 'data' => $result), 200);
        }else{
            $this->response(array('status' => 'success', 'msg' => 'No Grouped Providers'), 200);
        }
    }
    
    
     
     /* Methods :  getChangedGroupedProviders
     * Author  : Sagar
     * Desc    : If any change in Grouping for Operator Master, then this API will provide newly added or deleted operators (Is there change & if yes, changed stops)
     * 
     */
    
    function getChangedGroupedProviders_post(){
        $date = date('Y-m-d');
        $newdate = strtotime("-1 day" , strtotime($date));
        $newdate = date("Y-m-d 00:00:01" , $newdate );
        
        $this->db->select("op_name as provider_name");
        $this->db->from("op_master");
        $this->db->where("is_grouping_applicable","Y");
        $this->db->where("(updated_date between '".$newdate."' and '".date("Y-m-d 23:59:59")."' or created_date between '".$newdate."' and '".date("Y-m-d 23:59:59")."')");
        $query = $this->db->get();
        $result =  $query->result_array();
        
        if($result)
        {
            $this->response(array('status' => 'success', 'msg' => 'Changed Grouped Providers', 'data' => $result), 200);
        }else{
            $this->response(array('status' => 'success', 'msg' => 'No Changed Grouped Providers'), 200);
        }
    }
    
    /************************  Current Booking Section start  ************************/

    /************************  RSA Key Start  ************************/
    public function getRsaKey_post()
    { 
        $input  =   $this->post();
        $config = array(
                            array('field' => 'block_key', 'label' => 'Block Key', 'rules' => 'trim|required', 'errors' => array('required' => 'Please provide block key')),
                        );

        if (form_validate_rules($config) == FALSE) 
        {
            $error = $this->form_validation->error_array();
            $response_array = array( 'status' => 'failed' ,'error' => $error);
        }
        else
        {   
            $response_array   =   $this->cca_payment_gateway->getRsaKey_api($input);
        }
        
        $this->response($response_array, 200);
    }
    /************************  RSA Key End    ************************/


}
