<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Customer_registration extends REST_Controller {

    private $requestId;

	public function __construct() { 
	    parent::__construct();
	    $this->load->helper(array('smartcard_helper','common_helper'));
        
        //$this->load->model('smart_card_customer_registration_model');
            $this->apiCred = json_decode(SMART_CARD_CREDS,true);
            $header_value = $this->apiCred['X_API_KEY'];
            $this->header = array(
                'X-API-KEY:'.$header_value   
            );

            $this->basicAuth = array(
            'username' => $this->apiCred['username'],
            'password' => $this->apiCred['password']
            );
            $log_data = array();
            $log_data['url'] = current_url();
            $log_data['Request_type'] = $_SERVER['REQUEST_METHOD'];
            $log_data['get'] = $this->input->get();
            $log_data['post'] = $this->post();
            $log_data['header'] = getallheaders();
            $log_data['user'] = $this->input->server('PHP_AUTH_USER');
            $log_data['ip'] = $this->input->ip_address();
            // show($log_data);
            log_data('rest/smartcard/smart_card_customer_registration_api_' . date('d-m-Y') . '.log', $log_data, 'api');
            $this->load->model(array('Mainmodel','Customer_registration_model','Utility_model','Passfare_model','Utilities_model'));
            $this->load->library(array('PassFareCalc','common_library'));


            $insert_data = array('username' => $_POST['apploginuser'], 'ip' => $_SERVER['REMOTE_ADDR'], 'method' => $_SERVER['REQUEST_URI'], 'type' => 'get', 'version_id' => $_POST['version_id']);

            $this->requestId = $this->Mainmodel->insert_audit_data($insert_data)."";
            
            $request = $this->post();
            $user_id = $request['apploginuser'];
            $session_id= $this->Mainmodel->get_session_id($this->input->post('session_id'),$user_id); 
            $this->session_id = $session_id['itms_session_id'];
            $this->user_id =itms_user_id;
            
             /*   added code disallow multiple login of same user code start here */
        $startTime = date("Y-m-d H:i:s");
        $cenvertedTime = date('Y-m-d H:i:s',strtotime(LOGIN_EXPIRE_TIME,strtotime($startTime))) ;
        $this->Mainmodel->upDateLastActivityTime($this->input->post('session_id'),$cenvertedTime);
        /*   added code disallow multiple login of same user code end here */
	}

    public function cust_card_registration_post() {
         
        $URL = $this->apiCred['url'] . '/Customer_registration/cust_card_registration';
        $request = $this->post();
        
        if (!empty($request['pass_category_id']) && $request['pass_category_id'] == OTC_CARD) 
        {   
          $this->load->model(array('otc_card_inventory_model'));
          $otcInvenoryTxnResult = $this->otc_card_inventory_model->getOtcInvenoryTransaction($request['apploginuser']); 
          
         if(empty($otcInvenoryTxnResult[0]['total_cards'])  || $otcInvenoryTxnResult[0]['total_cards']=='0'){ 
              $responseMessage = ['responseCode' =>500,"responseMessage"=>"Card is not available in the inventory"];
              $this->response(array('status' => 'failure', 'msg' => 'Card is not available in the inventory', 'data'=>$responseMessage ), 200);
            } 
        }
        
        
        
	$itms_request = $this->post();		
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;
        if (is_array($_FILES) && !empty($_FILES)) {
            $config['upload_path'] = "./uploads/customerimages/";
            $config['allowed_types'] = 'jpg|jpeg|png';
            $this->load->library('upload', $config);
            if ($this->upload->do_upload("uploadimage")) {
                //$data = array('upload_data' => $this->upload->data());
                $image_data = $this->upload->data();
                //echo "<pre>"; print_r($image_data); die();
                if (function_exists('curl_file_create')) {
                    // php 5.5+ 
                    $cFile = curl_file_create($image_data['full_path']);
                } else {
                    //
                    $cFile = '@' . realpath($image_data['full_path']);
                }
                //$post = array('extra_info' => '123456','file_contents'=> $cFile);
                //$postVal['uploadimage'] = $image_data['full_path'];
                $itms_request['uploadimage'] = $cFile;
            }
        }

      

        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        
        $json = json_decode($data);
        
        log_data('rest/smartcard/smart_card_customer_registration_api_' . date('d-m-Y') . '.log', $data, 'api');

        if (isset($json->status)) {

            if (strtolower($json->status) == "success") {
                
                /* Check if commission available - sonali*/
        $input['apploginuser'] = $request['apploginuser'];
        $check_level = $this->Utilities_model->get_commission_level();
        $commissionResult = $this->Utilities_model->commission_setby_level($request['apploginuser'],$check_level->level);
         //show($commissionResult,1);
        $created_by = $commissionResult[0]['level'];
        $input['created_by'] = $created_by;      
        //$input['recharge_amount'] = '1';
        //$this->checkCommissionAvailable($input);
        
        $input['card_type'] = SMARTCARD_COMMISSION_NAME;
        $check_if_commission_available = $this->checkCommission($input);
        log_data('rest/smartcard/smart_card_ccas_api_check_commssion_available_' . date('d-m-Y') . '.log', $check_if_commission_available, 'check_commssion_available');
        //show($check_if_commission_available);

        if (count($check_if_commission_available) == 0) {            
            $this->response(array('status' => 'Failed', 'msg' => 'Commission not set. Please contact your administrator.', 'data'=>$check_if_commission_available ), 200);
        }
                        
        /* Check if commission available - sonali*/

                //$this->db->trans_begin();
                //customer master start here
                $user_name = $request['apploginuser'];

                $session_id = $request['session_id'];

               // $terminal_code = $this->Passfare_model->getTerminalCodeBytSessionId($session_id);

                $dob = date('Y-m-d', strtotime($request['date_of_birth']));

                $cust_data = array(
                    'prefix' => $request['prefix'],
                    'first_name' => $request['first_name'],
                    'middle_name' => isset($request['middle_name']) ? $request['middle_name'] : '',
                    'last_name' => $request['last_name'],
                    'first_name_lang' => isset($request['first_name_lang']) ? $request['first_name_lang'] : '',
		     'middle_name_lang' => isset($request['middle_name_lang']) ? $request['middle_name_lang'] : '',
		     'last_name_lang' => isset($request['last_name_lang']) ? $request['last_name_lang'] : '',
                    'telphone_number' => isset($request['telephone_number']) ? $request['telephone_number'] : '',
                    'mobile_number' => $request['mobile_number'],
                    'address' => $request['address'],
                    'pincode' => $request['pincode'],
                    'state_id' => $request['state_id'],
                    'district_id' => $request['district_id'],
                    'taluka_id' => $request['taluka_id'],
                    'city_id' => $request['city_id'],
                    'country_id' => 1,
                    'email' => $request['email'],
                    'gender' => $request['gender'],
                    'date_of_birth' => $dob,
                   // 'uid_number' => $request['uid_number'],
                    'uid_number' => isset($request['uid_number']) ? $request['uid_number'] : '',
                    'nationality_id' => 1,
                    'occupation_id' => isset($request['occupation_id']) ? $request['occupation_id'] : STUDENT_OCCUPATION,
                    'occupation_name' => isset($request['occupation_name']) ? $request['occupation_name'] : '',
                    'created_by' => $request['apploginuser'],
                    'msrtc_card_id' => $json->data->trimax_card_id, // need to changess
                    'is_active' => REGISTRATION_STATUS
                );

                $customer_master_id = $this->Customer_registration_model->insert_customer_reg_data($cust_data, CUSTOMER_MASTER);
               
                //customer master end here
                //customer student master start here
                if ($request['pass_category_id'] == STUDENT_CARD) {
                    $cust_stu_data = array(
                        'customer_id' => $customer_master_id,
                        'school_name' => $request['school_name'],
                        'school_address' => $request['school_address'],
                        'course_end_date' => isset($request['course_end_date']) ? $request['course_end_date'] : '',
                        'standard' => $request['standard'],
                        'created_by' => $request['apploginuser']
                    );

                    $customer_student_master_id = $this->Customer_registration_model->insert_customer_reg_data($cust_stu_data, CUSTOMER_STUDENT_MASTER);
                }


                $cust_card_data = array(
                    'customer_id' => $customer_master_id,
                    'registration_depo_cd' => $request['registration_depo_cd'],
                    'collection_depo_cd' => $request['collection_depo_cd'],
                    'name_on_card' => !empty($request['name_on_card']) ? $request['name_on_card'] : $request['first_name'] . ' ' . $request['middle_name'] . ' ' . $request['last_name'],
                    //'name_on_card_lang' => isset($request['name_on_card_lang']) ? $request['name_on_card_lang'] : '',
                    'name_on_card_lang' => !empty($request['name_on_card_lang']) ? $request['name_on_card_lang'] : $request['first_name_lang'].' '.$request['middle_name_lang'].' '.$request['last_name_lang'], 
                    
                    'screen_code' => isset($request['screen_code']) ? $request['screen_code'] : '',
                    'proof_type' => isset($request['proof_type']) ? $request['proof_type'] : '',
                    'proof_name' => isset($request['proof_name']) ? $request['proof_name'] : '',
                    'proof_ref_number' => isset($request['proof_ref_number']) ? $request['proof_ref_number'] : '',
                    'card_category_cd' => $request['card_category_cd'],
                    'cust_card_id' => ($request['pass_category_id'] == OTC_CARD) ? $request['cust_card_id'] : '',
                    'dispatch_date' => ($request['pass_category_id'] == OTC_CARD) ? date('Y-m-d') : '',
                   // 'card_validity_date' => $card_date['valid_from'],
                   // 'card_expiry_date' => $card_date['valid_to'],
                    //'card_status' => ($request['pass_category_id'] == OTC_CARD) ? 'A' : 'I',
                    'card_status' => ($request['pass_category_id'] == OTC_CARD) ? WRITING_STATUS : INITIATED_STATUS,
                    'created_by' => $request['apploginuser'],
                    'is_active' => REGISTRATION_STATUS
                );

                $customer_card_master_id = $this->Customer_registration_model->insert_customer_reg_data($cust_card_data, CUSTOMER_CARD_MASTER);


                //customer card master end here
                //customer card master image start here    

                if (is_array($_FILES) && !empty($_FILES)) {

                    $fdate = str_replace(":", " ", TODAY_DATE);

                    $folderPath = './uploads/customerimages/' . $fdate;
                    if (!is_dir($folderPath)) {
                        mkdir('./uploads/customerimages/' . $fdate, 0777, true);
                    }

                    $config['upload_path'] = './uploads/customerimages/' . $fdate . '/';
                    $config['allowed_types'] = 'jpeg|jpg|png';
                    //$config['max_size']     = '100';
                    $config['max_width'] = '1024';
                    $config['max_height'] = '768';
                    $config['overwrite'] = 'FALSE';

                    $rand_num = rand(1, 100);
                    $new_name = date('YmdHis') . $rand_num . $customer_card_master_id;
                    $config['file_name'] = $new_name;

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('uploadimage')) {
                        $this->load->library('image_lib');

                        $image_data = $this->upload->data();

                        $config['image_library'] = 'gd2';
                        $config['source_image'] = $image_data['full_path']; //get original image
                        $config['maintain_ratio'] = TRUE;
                        $config['width'] = 150;
                        $config['height'] = 100;

                        $this->image_lib->initialize($config);


                        $file_name = $image_data['full_path'];
                        // show($image_data,1);

                        $strlen = strpos($file_name, IMAGE_FOLDER_NAME);
                        $imagepath = substr_replace($file_name, '', 0, $strlen - 1);
                        $imagedata = array(
                            'customer_card_master_id' => $customer_card_master_id,
                            'customer_master_id' => $customer_master_id,
                            'image_path' => $imagepath,
                            'image_name' => $image_data['orig_name'],
                            'created_by' => $request['apploginuser'],
                            'modified_by' => $request['apploginuser'],
                            'is_active' => REGISTRATION_STATUS
                        );

                        $cust_card_image_id = $this->Customer_registration_model->insert_customer_reg_data($imagedata, IMAGE_DETAILS);
                        if (empty($cust_card_image_id)) {
                            //$this->db->trans_rollback();
                            $error = $this->upload->display_errors();
                            // $message = array('status' => 'failure', 'msg' => 'Error in inserting uploaded image data');
                            //  json_encode($this->response($message,200, FALSE));  
                        }
                    } else {
                        //$this->db->trans_rollback();
                        $error = $this->upload->display_errors();
                        // $message = array('status' => 'failure', 'msg' => 'Error in uploading image', 'data' => $error);
                        //  json_encode($this->response($message,200, FALSE));  
                    }
                }
                //customer card master image end here
                //card transaction start here

              //  $card_fee_master_id = $this->Customer_registration_model->get_card_fee_master_id($request);              
                
                if ($request['pass_category_id'] == REGULAR_CARD || $request['pass_category_id'] == STUDENT_CARD || $request['pass_category_id'] == TRAVEL_AS_U_LIKE_CARD) {
                    //$conc_service_rel_id = $this->Customer_registration_model->get_concs_service_id($request)['id'];
                     
                    $activation_date = $request['activation_date'];
                  //  $span_period = $card_fee_master_id['span_period_days'] - 1;
                 //   $expiry_date = date('Y-m-d', strtotime($activation_date." +".$span_period." days"));
                }else if($request['pass_category_id'] == CONC_CARD) 
                 {
		    $activation_date = $request['activation_date'];
				
	        }
                $vendor_id = $this->Customer_registration_model->get_vendor_id($request['apploginuser'])['vendor_id'];



                //$total_base_fare = isset($request['base_fare_1']) ? $request['base_fare_1'] : '';
                $bf = 1;
                
                if ($request['pass_category_id'] == TRAVEL_AS_U_LIKE_CARD) {
                    $request['base_fare_1'] = $request['tal_base_fare'];
                    $total_base_fare = $request['tal_base_fare'];
                } else {
                    $total_base_fare = isset($request['base_fare_1']) ? $request['base_fare_1'] : 0.00;
                    if (isset($request['base_fare_2'])) {
                        $bf = 2;
                        $total_base_fare = $request['base_fare_1'] + $request['base_fare_2'];
                    }
                }

                $card_trans_data = array(
                    'pass_type_id' => '1',
                    'pass_concession_id' => $request['pass_concession_id'],
                    'pass_category_id' => $request['pass_category_id'],
                    'activation_date' => isset($activation_date) ? $activation_date : '',
                 //'expiry_date' => isset($expiry_date) ? $expiry_date : '',
                //  'transaction_type_id' => $card_fee_master_id['ctrans_type_id'],
                    
                   'transaction_type_code' =>  isset($request['trans_type']) ? $request['trans_type'] : '',
                    'depot_cd' => $request['registration_depo_cd'],
                    'terminal_id' => $request['terminal_id'],
                    
                    'span_id' => isset($request['span_id']) ? $request['span_id'] : '',
                    
                    'span_name' => isset($request['span_name']) ? $request['span_name'] : '',
                    'span_period_days' => isset($request['span_period_days']) ? $request['span_period_days'] : '',
                    
                    'bus_type_cd' => isset($request['bus_type_cd']) ? $request['bus_type_cd'] : '',
                   // 'conc_service_rel_id' => isset($conc_service_rel_id) ? $conc_service_rel_id : '',
                     'concession_cd' => isset($request['concession_cd']) ? $request['concession_cd'] : '',
                     'concession_nm' => isset($request['pass_concession_nm']) ? $request['pass_concession_nm'] : '',
                     'kms_limit' => isset($request['kms_limit']) ? $request['kms_limit'] : '',
                   
                    //'card_fee_master_id' => $card_fee_master_id['id'],
                
                     
                    'amount' => $total_base_fare,
                    'session_id' => $request['session_id'],
                    'vendor_id' => $vendor_id,
                    'created_by' => $request['apploginuser'],
                    'is_active' => REGISTRATION_STATUS,
                    
                );
               
                $card_trans_id = $this->Customer_registration_model->insert_customer_reg_data($card_trans_data, CARD_TRANS);


                //card transaction end here
                //card transaction details start here

                $card_trans_details_id = $fare_data_details = $fare_data_details = $fare_data = array();

                for ($i = 1; $i <= $bf; $i++) {

                    $card_trans_detail_data = array(
                        'card_transaction_id' => $card_trans_id,
                      //'card_fee_master_id' => $card_fee_master_id['id'],
                        'route_no' => isset($request['route_no_' . $i]) ? $request['route_no_' . $i] : '',
                        'route_type' => $i,
                        'fare_type_cd' => isset($request['fare_type_cd_' . $i]) ? $request['fare_type_cd_' . $i] : '',
                        'service_number' => '',
                        'from_stop_code' => isset($request['from_stop_code_' . $i]) ? $request['from_stop_code_' . $i] : '',
                        'till_stop_code' => isset($request['till_stop_code_' . $i]) ? $request['till_stop_code_' . $i] : '',
                        'via_stop_code' => isset($request['via_stop_cd_' . $i]) ? $request['via_stop_cd_' . $i] : '',
                        'amount' => isset($request['base_fare_' . $i]) ? $request['base_fare_' . $i] : '',
                        'session_id' => $request['session_id'],
                        'created_by' => $request['apploginuser'],
                        'is_active' => REGISTRATION_STATUS
                    );

                   if( isset($request['base_fare_' . $i]) && !empty($request['base_fare_' . $i]) && $request['base_fare_' . $i] > 0  && !in_array($request['pass_category_id'], array(OTC_CARD,TRAVEL_AS_U_LIKE_CARD,CONC_CARD))) {

                       $fare_data_details = isset($request['fare_data_' . $i]) ? $request['fare_data_' . $i] : '';
                       $fare_data_details_arr = json_decode($fare_data_details, TRUE);
                       $per_day_base_fare = $fare_data_details_arr['per_day_base_fare'];
                       $card_trans_detail_data['per_day_base_fare'] = $per_day_base_fare;
                       $card_trans_detail_data['total_base_fare']   =  $fare_data_details_arr['total_base_fare'];
                       $card_trans_detail_data['total_asn_amount']  =  $fare_data_details_arr['total_asn_amount'];
                       $card_trans_detail_data['net_fare_amount']   = $fare_data_details_arr['net_fare_amount'];
                       $card_trans_detail_data['fare_before_rounding'] =  $fare_data_details_arr['fare_before_rounding'];
                       $card_trans_detail_data['fare_rounding_difference'] = $fare_data_details_arr['fare_before_rounding'] - $fare_data_details_arr['total_amt'];           
                       $card_trans_detail_data['reimbursement_amount'] =  $fare_data_details_arr['reimbursement_amount'];
                       
                    }
                    $card_trans_details_id[$i] = $this->Customer_registration_model->insert_customer_reg_data($card_trans_detail_data, CARD_TRANS_DETAILS);
                }



                //card transaction details end here
                // calculation of total fare and its details
                // For OTC fee charge list from cc and it is maintain in lib
           //     $cc_otc_data = array('terminal_code' => $terminal_code, 'requestId' => $this->requestId, 'user_name' => $user_name, 'session_id' => $session_id);
                
                $post_arr=$this->post() ;
                $post_arr['custCardId']=1;	 	

//               $total_fare_data = $this->common_library->total_fare_calc($card_fee_master_id['id'], $total_base_fare, array(), '', $request['pass_category_id'], $cc_otc_data,$post_arr);
                $total_fare_amt=0 ;
                  if(!empty($request['total_fare_amt']))
		     $total_fare_amt=$request['total_fare_amt'] ;

                //amount transaction start here
                    $amt_trans_data = array(
                    'card_transaction_id' => $card_trans_id,
                    'total_amount' => $total_fare_amt,
                   // 'transaction_type_id' => $card_fee_master_id['ctrans_type_id'],
                    'transaction_type_code' =>  isset($request['trans_type']) ? $request['trans_type'] : '',
                    'depot_cd' => $request['registration_depo_cd'],
                    'terminal_id' => $request['terminal_id'],
                    'session_id' => $request['session_id'],
                    'vendor_id' => $vendor_id,
                    'summary_no' => '',
                    'summary_status' => '',
                    'created_by' => $request['apploginuser'],
    		    'is_active' => REGISTRATION_STATUS
                );
                $amt_trans_id = $this->Customer_registration_model->insert_customer_reg_data($amt_trans_data, AMOUNT_TRANS);

          if(!empty($request['Card_fee_details']))
           {
              $Card_fee_details = isset($request['Card_fee_details']) ? $request['Card_fee_details'] : '';
              $total_fare_amt_arr = json_decode($Card_fee_details, TRUE);
                
                
               foreach($total_fare_amt_arr as $key => $value) {
                    $amt_trans_details_data[$i] = array(
                       'amount_transaction_id' => $amt_trans_id,
                        'amount_code' => $value['fee_type_cd'],
                        'amount' => $value['fee_type_value'],
                        'operand' => $value['operand'],
                        'created_by' => $request['apploginuser'],
                        'is_active' => REGISTRATION_STATUS
                    );
                    $i++;
                }

                $amt_trans_details_id = $this->Customer_registration_model->insert_customer_reg_bulk_data($amt_trans_details_data, AMOUNT_TRANS_DETAILS);
          }
                // amount transaction details end here
                //Inventory data start here
                $inventory_data = array(
                    'user_id' => $customer_master_id,
                    'card_no' => (isset($request['trimax_card_id']) ? $request['trimax_card_id'] : ''),
                    'trimax_card_id' => (isset($request['trimax_card_id']) ? $request['trimax_card_id'] : ''),
                    'cust_card_id' => (isset($request['cust_card_id']) ? $request['cust_card_id'] : ''),
                    'card_type' => $request['card_category_cd'],
                    'location_type' => (($request['pass_category_id'] == OTC_CARD) ? 3 : ''),
                    'location_at' => (($request['pass_category_id'] == OTC_CARD) ? $request['registration_depo_cd'] : ''),
                    'created_by' => $request['apploginuser'],
                    'is_active' => REGISTRATION_STATUS
                );

                $inventory_master_id = $this->Customer_registration_model->insert_customer_reg_data($inventory_data, INVENTORY);


                //Inventory data end here
                //$response_data = array();
               // $receipt_number = $this->Utility_model->receipt_number('RECEIPT', $request['apploginuser']);
                 $receipt_number = $json->data->receipt_number; 


                if ($request['pass_category_id'] != OTC_CARD) {

                    //Master Card Data start here
                    $age = calculateAge($dob);
                    $card_category_code = getCardType($age, $request['card_category_cd']);


                    $master_card_data = array(
                        'card_category_cd' => $card_category_code,
                        'customer_id' => $customer_master_id,
                        'created_by' => $request['apploginuser']
                    );
                    $master_card_id = $this->Customer_registration_model->insert_customer_reg_data($master_card_data, MASTER_CARD);


                    //Master Card Data end here

                  ///  $trimax_card_id = $this->Utility_model->gen_card_number(T_ID_ISSUE_NM, $request['registration_depo_cd'], $request['apploginuser']);
					
		 $trimax_card_id = $json->data->trimax_card_id; 
					
                } elseif ($request['pass_category_id'] == OTC_CARD) {
                    
                    /*OTC cod end */
                    $trimaxId = $json->data->trimax_card_id ;
                    $cardType = 'OTC';
                    $user_name= $request['apploginuser'];
                    $custCardId= $request['cust_card_id'];
                    $master_card_id = $this->Passfare_model->insertIntoMasterCard($custCardId, $trimaxId,$cardType, $user_name);
                    $master_card_where = array('id' => $master_card_id, 'is_active' => '1');
                    $master_card_update = array('is_whitelisted' => '1', 'modified_by' => $user_name);
                    $this->Mainmodel->update_master_card($master_card_where,$master_card_update);
                    $card_data['trimax_card_id'] = $trimaxId;
                    $card_data['id'] = $master_card_id;
                    /*OTC cod end */
                    $card_category_code = $request['card_category_cd'];
                    $trimax_card_id = $card_data['trimax_card_id'];
                   
                }
                $where_data = array('customer_master_id' => $customer_master_id, 'customer_card_master_id' => $customer_card_master_id, 'card_trans_id' => $card_trans_id, 'card_trans_details_id_1' => $card_trans_details_id[1], 'amt_trans_id' => $amt_trans_id, 'master_card_id' => $master_card_id, 'inventory_master_id' => $inventory_master_id);

                if (isset($card_trans_details_id[2])) {
                    $where_data['card_trans_details_id_2'] = $card_trans_details_id[2];
                }

                $this->Customer_registration_model->update_trimax_id($where_data, $trimax_card_id, $receipt_number, $user_name);

                //$response_data = array('trimax_card_id' => $trimax_card_id, 'receipt_number' => $receipt_number);
                $response_data = array('trimax_card_id' => $trimax_card_id, 'receipt_number' => $receipt_number, 'card_type' => $card_category_code);

               // $this->Utility_model->save_command_data($trimax_card_id, REG_COMMAND);
                 //wallet code added by sonali
//                $request['amount'] = $total_fare_data['data']['total_fare_amt'];
//                updateWalletTxn($request); 
            }
        }


         if(!empty($image_data['full_path']) && file_exists($image_data['full_path'])){
            unlink($image_data['full_path']);
        }
         json_response($json);
    }

    public function get_route_number_post() {
        $URL= $this->apiCred['url'].'/Customer_registration/get_route_number';        		
        
        $itms_request = $this->post();		
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_customer_registration_api_' . date('d-m-Y') . '.log', $json, 'api');
        //json_encode($this->response($json,200));

//        $this->output
//        ->set_status_header(200)
//        ->set_content_type('application/json', 'utf-8')
//        ->set_output(json_encode($json));
         json_response($json);
        
    }
    public function get_card_fee_data_post() 
    {
       $itms_request = $this->post();	
       
       if($itms_request['trans_type']=='BPSW'){
            if(empty($itms_request['trimax_card_id']))
             {
                  $this->response(array('status' => 'failure', 'msg' => 'trimaxId is required', 'data'=>'trimax card id is required' ), 200);

             } 
             if(empty($itms_request['terminal_id']))
             {
                  $this->response(array('status' => 'failure', 'msg' => 'terminal_id is required', 'data'=>'trimax card id is required' ), 200);

             } 
        }
       
       $URL= $this->apiCred['url'].'/Customer_registration/get_card_fee_data';
      
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
        /*code added to change order of GST code start here   */
		  $temp_arr=[] ;
		  foreach($json->data->card_fee_details as $key => $value) {
			   if($value->fee_type_cd=='BF' || $value->fee_type_cd=='GST' )
			   {
			    	$temp_arr[]= $value ;
			   }
		   }
		foreach($json->data->card_fee_details as $key => $value) {
			   if($value->fee_type_cd=='BF' || $value->fee_type_cd=='GST' )
			   {
			   }else{
			     $temp_arr[]= $value ;
			   }
		   } 
		  $json->data->card_fee_details=     $temp_arr ;
         /*code added to change order of GST code start here */ 
       // print_r($json);

        log_data('rest/smartcard/smart_card_customer_registration_api_' . date('d-m-Y') . '.log', $data, 'api');
        $json->data->cash_collected_from_customer ='0.0' ;
         //if($itms_request['trans_type']==PASS_RENEW_SW_TRANS_TYPE || $itms_request['trans_type']==PASS_RENEW_CASH_TRANS_TYPE  )
         if($itms_request['trans_type']==PASS_RENEW_SW_TRANS_TYPE   )
          { 
             
                $terminal_id  =$itms_request['terminal_id'] ;
                $itms_request['trimaxId'] = $itms_request['trimax_card_id'];

                $URL= $this->apiCred['url'].'/CCAS_api/last_load_api';
                $itms_request['apploginuser'] = $this->user_id ;
                $itms_request['session_id'] = $this->session_id ;
                $last_load_api_data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
                
              //  print_r($last_load_api_data);exit;
                $json_last_load = json_decode($last_load_api_data);
                
             ///    $d ='{"status":"success","msg":"Success","data":{"responseCode":200,"responseMessage":"Success","requestId":"214537","lastLoadIdentifier":{"timeStamp":"03:32:00 PM","dateStamp":"2019-04-08","RinitiatorId":"FNBHLD0001001"}}}';
            //  $json_last_load = json_decode($d);  
              
                if($json_last_load->status=='failure' )
                {
                    $temp_var=0;
                    foreach($json->data->card_fee_details as $key => $value) {
                           if($value->fee_type_nm=='EPurseFees' || strtolower($value->fee_type_nm)==BCCHARGES )
                           {
                               if($itms_request['trans_type']==PASS_RENEW_SW_TRANS_TYPE){
                                   $json->data->total_fare_amt= $json->data->total_fare_amt - ($json->data->card_fee_details[$key]->fee_type_value);
                                         }

                                         $temp_var=$temp_var+$json->data->card_fee_details[$key]->fee_type_value;
                           }
                        }

                        if(isset($itms_request['trans_type']) && $itms_request['trans_type']==PASS_RENEW_SW_TRANS_TYPE ) 
                        {    
        $json->data->cash_collected_from_customer =floatval($temp_var); ;
                        }else{
                            $json->data->cash_collected_from_customer ='0.0' ;
                        } 
                     // json_response($json_last_load);  
                }
                else
                {
                        $timeStamp = $json_last_load->data->lastLoadIdentifier->timeStamp ;
                        $timeStamp=date("G:i:s", strtotime($timeStamp));
                        $dateStamp = $json_last_load->data->lastLoadIdentifier->dateStamp ;
                        $dateStamp=str_replace("-","/",$dateStamp);
                        
                         $timestamp = strtotime($dateStamp.' '.$timeStamp);  
                         $timestamp = strtotime ( BUS_PASS_RENEWAL_CONFIGURE_TIME , $timestamp ) ;
                        
                        $configurable_time = strtotime(date("Y-m-d H:i:s"));
                      //  $configurable_time =strtotime(date("Y-m-d H:i:s", strtotime (BUS_PASS_RENEWAL_CONFIGURE_TIME)));
        
                        $RinitiatorId = $json_last_load->data->lastLoadIdentifier->RinitiatorId ;

                        if($RinitiatorId==$terminal_id  && $configurable_time < $timestamp)
                        {
                            $total_fare_amt=array();
                          //  $total_fare_amt_arr = json_decode($json->data->card_fee_details, TRUE);
                         //   print_R($json->data->card_fee_details);
                               foreach($json->data->card_fee_details as $key => $value) {
                                
                                   if($value->fee_type_nm=='EPurseFees' || strtolower($value->fee_type_nm)==BCCHARGES )
                                   {
                                    $json->data->total_fare_amt= $json->data->total_fare_amt - ($json->data->card_fee_details[$key]->fee_type_value);

                                          $json->data->card_fee_details[$key]->fee_type_value=0;
                                   }
                                 
                               }
                 }
                
           }
       } 
       json_response($json);
    }
    public function get_registration_receipt_post() {
       $URL= $this->apiCred['url'].'/Customer_registration/get_registration_receipt';
       
        $itms_request = $this->post();		
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        
        $json = json_decode($data);
       /*code added to change order of GST code start here   */
        $temp_arr=[] ;
        foreach($json->data[0]->card_fee_details as $key => $value) {
                 if($value->fee_type_cd=='BF'  )
                 {
                      $temp_arr[]= $value ;
                 }
         }
        foreach($json->data[0]->card_fee_details as $key => $value) {
                 if($value->fee_type_cd=='GST' )
                 {
                      $temp_arr[]= $value ;
                 }
         } 

        foreach($json->data[0]->card_fee_details as $key => $value) {
               if($value->fee_type_cd=='BF' || $value->fee_type_cd=='GST' )
                 {
                 }else{
                   $temp_arr[]= $value ;
                 }
         } 
        $json->data[0]->card_fee_details=     $temp_arr ;
         /*code added to change order of GST code start here */ 
        
        log_data('rest/smartcard/smart_card_customer_registration_api_' . date('d-m-Y') . '.log', $json, 'api');
       // json_encode($this->response($json,200));

//        $this->output
//        ->set_status_header(200)
//        ->set_content_type('application/json', 'utf-8')
//        ->set_output(json_encode($json));
         json_response($json);
        
    }
    public function check_trimax_id_post() {
        $URL= $this->apiCred['url'].'/Customer_registration/check_trimax_id';
        
        $itms_request = $this->post();		
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_customer_registration_api_' . date('d-m-Y') . '.log', $json, 'api');
		json_response($json);
       
    }
    
    
      
    
      
    public function checkCommission($input) {
        if ($input['card_type'] == SMARTCARD_COMMISSION_NAME) {            
            $code = strtolower($input['card_type']);
            $service_id = SMART_CARD_SERVICE;
        } 

        $LcSqlStr = "SELECT * from services_commissions 
        where status = 'Y'";
        $LcSqlStr .= " AND `values` NOT LIKE '{\"Trimax\":\"0\",\"Rokad\":\"0\",\"MD\":\"0\",\"AD\":\"0\",\"Distributor\":\"0\",\"Retailer\":\"0\"}'";
        $LcSqlStr .= " AND `commission_name` LIKE '%" . str_replace(" ", "", $code) . "%'";
        $LcSqlStr .= " AND `created_by` = '" . $input['created_by'] . "'";
        $LcSqlStr .= " AND service_id = '" . $service_id . "'";
        ;
        //log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/micro_atm/' . 'micro_atm_commission_query.log', $LcSqlStr, 'Commission query');
        log_data('rest/smartcard/smart_card_ccas_api_commission_query_' . date('d-m-Y') . '.log', $LcSqlStr, 'Commission query');
        $query = $this->db->query($LcSqlStr);
        return $query->result();
    }
    
    
    
   

}

?>
