<?php

class Pg_response extends CI_Controller {

    var $form_id;
    var $div_id;

    public function __construct()
    {
        parent::__construct();

       ini_set('memory_limit', '1024M'); 
       $this->load->model('op_master_details_model','ticket_details_model','bus_operator_model','tickets_model','payment_gateway_key_model','users_model','return_ticket_mapping_model');
        
        // $this->load->library('buses_api');
        $this->load->library(array('buses_api','cca_payment_gateway','soap_api/msrtc_api','soap_api/upsrtc_api','api_provider/etravelsmart_api'));

        $this->db2 = $this->load->database('up_db', TRUE);

        $lib = array();
        foreach (glob(APPPATH."libraries/soap_api/"."*.php") as $filename) {
            $path = 'soap_api/'.strtolower(basename($filename,'.php'));
            array_push($lib, $path );
        }
    }

    public function index() {

    }

    public function GetHandleResponse()
    {
        $pgres = $this->input->post();
        logging_data("mobile_pg/".date("d-m-Y")."_log.log",$pgres, 'Encrypted Response From CCAvenue PG');
        log_data("mobile_pg/".date("d-m-Y")."_log.log",$pgres, 'Encrypted Response From CCAvenue PG');
        /* 
        BELOW ARE LIST OF PARAMETERS THAT WILL BE RECEIVED BY MERCHANT FROM PAYMENT GATEWAY 
        */
        try
        {
            /*Variable Declaration*/
            /*=========================================================================================*/
            /*
                $merchant_param1 -> ticket_ref_no;
                $merchant_param2 -> ticket_id;
                $merchant_param3 -> user_id;
            */
            
            //Decrypt Response here using ALGORITHM SPECIFIED BY CCAVENUE
            //Crypto Decryption used as per the specified working key.
            $response_to_be_send = $rcvdString =  decrypt($pgres["encResp"],$this->config->item("cca_working_key"));
            /*$decryptValues  =   explode('&', $rcvdString);*/
            $cc_result = "";
            $order_status = "";

            parse_str($rcvdString);
            parse_str($rcvdString, $array_response);

            //for log Response
            logging_data("mobile_pg/".date("d-m-Y")."_log.log",$array_response, 'Response From CCAvenue PG');
            log_data("mobile_pg/".date("d-m-Y")."_log.log",$array_response, 'Response From CCAvenue PG');

            //IMP data to pass to view
            $data["ticket_ref_no"] = $merchant_param1;
            $data["ticket_id"] = $merchant_param2;
            $data["payment_by"] = "ccavenue";

            /*GENERATING TRANSACTION LOG FILE START HERE*/
            /*=========================================================================================*/
            $user_id = "";

            $title = array("order_id","tracking_id","bank_ref_no","order_status","failure_message","payment_mode","card_name","status_code","status_message","currency","amount","billing_ name","billing_ address","billing_ city","billing_ state","billing_ zip","billing_ country","billing_ tel","billing_ email","delivery_ name","delivery_ address","delivery_ city","delivery_ state","delivery_ zip","delivery_ country","delivery_ tel","merchant_param1","merchant_param2","merchant_param3","merchant_param4","merchant_param5","vault","offer_type","offer_code","discount_value","IP Address","User Agent");
            $val = array($order_id,$tracking_id,$bank_ref_no,$order_status,$failure_message,$payment_mode,$card_name,$status_code,$status_message,$currency,$amount,$billing_name,$billing_address,$billing_city,$billing_state,$billing_zip,$billing_country,$billing_tel,$billing_email,$delivery_name,$delivery_address,$delivery_city,$delivery_state,$delivery_zip,$delivery_country,$delivery_tel,$merchant_param1,$merchant_param2,$merchant_param3,$merchant_param4,$merchant_param5,$vault,$offer_type,$offer_code,$discount_value,$this->session->userdata("ip_address"),$this->session->userdata("user_agent"));
            $pgfile = PG_LOG."mobile_".date("d-m-Y")."_cca.csv";

            if(!file_exists($pgfile))
            {
                $file = fopen($pgfile,"a");
                fputcsv($file,$title);
            }

            $file = fopen($pgfile,"a");
            fputcsv($file,$val);

            fclose($file);
            /*GENERATING TRANSACTION LOG FILE END HERE*/
            /*/=================================================================================================  */
            if($order_id != '' && $failure_message == "")  
            { 

              /*
              IMPORTANT NOTE - MERCHANT SHOULD UPDATE 
              TRANACTION PAYMENT STATUS IN MERCHANT DATABASE AT THIS POSITION 
              AND THEN REDIRECT CUSTOMER ON RESULT PAGE
              */
                if ((strtolower($order_status) == "success"))
                {
                    /* NOTE - MERCHANT MUST LOG THE RESPONSE RECEIVED IN LOGS AS PER BEST PRACTICE */
                    /*
                    IMPORTANT NOTE - MERCHANT DOES RESPONSE HANDLING AND VALIDATIONS OF 
                    TRACK ID, AMOUNT AT THIS PLACE. THEN ONLY MERCHANT SHOULD UPDATE 
                    TRANACTION PAYMENT STATUS IN MERCHANT DATABASE AT THIS POSITION 
                    AND THEN REDIRECT CUSTOMER ON RESULT PAGE
                    */

                    /* !!IMPORTANT INFORMATION!!
                    During redirection, ME can pass the values as per ME requirement.
                    NOTE: NO PROCESSING should be done on the RESULT PAGE basis of values passed in the RESULT PAGE from this page. 
                    ME does all validations on the responseURL page and then redirects the customer to RESULT PAGE ONLY FOR RECEIPT PRESENTATION/TRANSACTION STATUS CONFIRMATION
                    For demonstration purpose the result and track id are passed to Result page
                    */
                    /* Hashing Response Successful  */

                    //update payment_transaction_fss table
                    $check_ticket_details = $this->tickets_model->where("ticket_id",$data["ticket_id"])->find_all();
                    if($check_ticket_details && $check_ticket_details[0]->transaction_status == "pfailed")
                    {
                        $array_where = array('track_id' => $order_id);
                        $fields_update = array('status' => strtolower($order_status),
                                               'pg_track_id' => $tracking_id,
                                               'bank_ref_no' => $bank_ref_no,  
                                               'response' => json_encode($array_response)
                                              );

                        
                        $pay_trans_true_false = $this->payment_transaction_fss_model->update_where($array_where,'', $fields_update);
                       
                        if($pay_trans_true_false)
                        {
                            //Insert into payment_transaction_metadata
                           
                            $trans_metadata = array("payment_transaction_id"=>$pay_trans_true_false,
                                                    "order_id"=>$order_id,
                                                    "tracking_id"=>$tracking_id,
                                                    "bank_ref_no"=>$bank_ref_no,
                                                    "order_status"=>$order_status,
                                                    "failure_message"=>$failure_message,
                                                    "payment_mode"=>$payment_mode,
                                                    "card_name"=>$card_name,
                                                    "status_code"=>$status_code,
                                                    "status_message"=>$status_message,
                                                    "currency"=>$currency,
                                                    "amount"=>$amount,
                                                    "billing_name"=>$billing_name,
                                                    "billing_address"=>$billing_address,
                                                    "billing_city"=>$billing_city,
                                                    "billing_state"=>$billing_state,
                                                    "billing_zip"=>$billing_zip,
                                                    "billing_country"=>$billing_country,
                                                    "billing_tel"=>$billing_tel,
                                                    "billing_email"=>$billing_email,
                                                    "delivery_name"=>$delivery_name,
                                                    "delivery_address"=>$delivery_address,
                                                    "delivery_city"=>$delivery_city,
                                                    "delivery_state"=>$delivery_state,
                                                    "delivery_zip"=>$delivery_zip,
                                                    "delivery_country"=>$delivery_country,
                                                    "delivery_tel"=>$delivery_tel,
                                                    "merchant_param1"=>$merchant_param1,
                                                    "merchant_param2"=>$merchant_param2,
                                                    "merchant_param3"=>$merchant_param3,
                                                    "merchant_param4"=>$merchant_param4,
                                                    "merchant_param5"=>$merchant_param5,
                                                    "vault"=>$vault,
                                                    "offer_type"=>$offer_type,
                                                    "offer_code"=>$offer_code,
                                                    "discount_value" => $discount_value
                                                    );

                            $ptm_id = $this->db->insert('payment_transaction_metadata', $trans_metadata); 
            
                            /*
                                $merchant_param1 -> ticket_ref_no;
                                $merchant_param2 -> ticket_id;
                                $merchant_param3 -> user_id;
                            */
                            $tickets_to_update = [
                                                    "transaction_status" => "psuccess",
                                                    "merchant_amount" => $array_response["amount"],
                                                    "pg_charges" => $array_response["amount"] - $check_ticket_details[0]->tot_fare_amt_with_tax,
                                                    "pg_tracking_id" => $array_response["tracking_id"]
                                                  ];

                            /*$this->db->where(array("ticket_id" => $merchant_param2,"transaction_status" => "pfailed"))->update("tickets",array("transaction_status" => "psuccess"));*/
                            $this->db->where(array("ticket_id" => $merchant_param2,"transaction_status" => "pfailed"))->update("tickets",$tickets_to_update);


                            $return_ticket_data = $this->return_ticket_mapping_model->get_rpt_mapping($merchant_param2);
                            if(count($return_ticket_data) > 0)
                            {
                                $return_transaction_status = array('transaction_status' => "psuccess",
                                                                    'payment_by' => "ccavenue",
                                                                    'pg_tracking_id' => $array_response["tracking_id"]
                                                                    );

                                $this->tickets_model->update($return_ticket_data[0]->ticket_id,$return_transaction_status);
                            }

                            $data["is_updated"] = true;
                            $cc_result = "CAPTURED";
                        }
                        else
                        {

                            $this->db->where(array("ticket_id" => $merchant_param2))->update("tickets",array("transaction_status" => "psuccess","failed_reason" => "payment transaction table not updated"));
                            $data["is_updated"] = false;

                            $custom_error_mail = array(
                                                        "custom_error_subject" => "Transaction table not updated.",
                                                        "custom_error_message" => "payment success but Ticket Not Booked because Transaction table not updated for ticket_id - ".$merchant_param2
                                                      );

                            $this->common_model->developer_custom_error_mail($custom_error_mail);
                        }

                        $data["pay_trans_true_false"] = $pay_trans_true_false;
                    }
                    else
                    {
                        $custom_error_mail = array(
                                                    "custom_error_subject" => "Double Hit (Page Refresh) & redirected to ".$redirected_to,
                                                    "custom_error_message" => "Page refreshed or Made double hit in the process after payment done. So we have redirected this user to the <b>".$redirected_to."</b> page. please find the use details for ticket id ".$data["ticket_id"]."<br/><br/>".json_encode($check_ticket_details)
                                                    );
                        $this->common_model->developer_custom_error_mail($custom_error_mail);
                    }
                }
                else 
                {
                    /************ Mail Start Here ********/
                    eval(ADMIN_EMAIL_ARRAY);
                    $pg_failed_data = array(
                                            "ticket_id" => $data["ticket_id"],
                                            "transaction_status" => (isset($order_status)) ? strtolower($order_status) : "failed",
                                            "site_url" => base_url(),
                                            "sent_to" => $admin_array
                                        );

                    $this->common_model->payment_gateway_failed_mail($pg_failed_data);

                    /************ Mail End Here ********/
                    $cca_status_message = "";

                    if(isset($status_message) && $status_message != "")
                    {
                        $cca_status_message = $status_message;
                    }
                    
                    $cc_update_tickets = array(
                                                "transaction_status" => strtolower($order_status), 
                                                "payment_by" => "ccavenue",
                                                "failed_reason" => $cca_status_message,
                                                "pg_tracking_id" => isset($tracking_id) ? $tracking_id : "",
                                                "payment_mode" => isset($payment_mode) ? $payment_mode : ""
                                              );

                    $this->db->where(array("ticket_id" => $merchant_param2))->update("tickets", $cc_update_tickets);

                    $rpt_data = $this->return_ticket_mapping_model->get_rpt_mapping($data["ticket_id"]);

                    if($rpt_data)
                    {
                        $this->db->where(array("ticket_id" => $rpt_data[0]->ticket_id))->update("tickets", $cc_update_tickets);
                    }

                    /*$this->db->where(array("ticket_id" => $merchant_param2))->update("tickets", array("transaction_status" => strtolower($order_status), "payment_by" => "ccavenue"));*/

                    $cc_update_payment_transaction_fss = array(
                                                "response" => json_encode($pgres),
                                                "status" => strtolower($order_status), 
                                                "payment_by" => "cca",
                                                "pg_track_id" => isset($tracking_id) ? $tracking_id : ""
                                              );

                    $this->db->where(array("track_id" => $order_id))->update("payment_transaction_fss",$cc_update_payment_transaction_fss);

                    $data["is_updated"] = false;
                    $data["error_msg"] = "Transaction not successfull";
                    // $REDIRECT = 'http://www.merchantdemo.com/StatusTRAN.php?ResResult='.$ResResult.'&ResTrackId='.$ResTrackID.'&ResAmount='.$ResAmount.'&ResPaymentId='.$ResPaymentId.'&ResRef='.$ResRef.'&ResTranId='.$ResTranId.'&ResError='.$ResErrorText.'&ResAuthResCode='.$ResauthRespCode;   
                    // header("location:". $REDIRECT );
                }
            } 
            else 
            {
                /************ Mail Start Here ********/
                $pg_failed_data = array(
                                            "ticket_id" => $data["ticket_id"],
                                            "transaction_status" => (isset($order_status)) ? strtolower($order_status) : "failed",
                                            "site_url" => base_url(),
                                            "sent_to" => $admin_array
                                        );

                $this->common_model->payment_gateway_failed_mail($pg_failed_data);

                /************ Mail End Here ********/
              /*
              ERROR IN TRANSACTION PROCESSING
              IMPORTANT NOTE - MERCHANT SHOULD UPDATE 
              TRANACTION PAYMENT STATUS IN MERCHANT DATABASE AT THIS POSITION 
              AND THEN REDIRECT CUSTOMER ON RESULT PAGE
              */
                $this->db->where(array("ticket_id" => $data["ticket_id"]))->update("tickets",array("transaction_status" => "pfailed", "payment_by" => "ccavenue", "failed_reason" => "error from ccavenue payment gateway. Failure Message - ".$failure_message));
                $data["is_updated"] = false;
                $data["error_msg"] = "Transaction not successfull";
            }
        }
        catch(Exception $e)
        {
          // var_dump($e);
        }

        $reponse_to_pg["response_to_be_send"] = $response_to_be_send;
        $reponse_to_pg["device"] = strtolower($merchant_param3);

        logging_data("mobile_pg/".date("d-m-Y")."_log.log",$response_to_be_send, 'Response to mobile app');
        log_data("mobile_pg/".date("d-m-Y")."_log.log",$response_to_be_send, 'Response to mobile app');

        $this->load->view(FRONT_VIEW."mobile_pg_redirect",$reponse_to_pg);
    }

}
