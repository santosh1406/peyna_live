<?php

//Created by pooja kambali on 27-09-2018

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

use Blocktrail\CryptoJSAES\CryptoJSAES;

require APPPATH .'/libraries/CryptoJSAES.php';



class Fino extends REST_Controller {

    function __construct() {
        parent::__construct();

        $this->sessionData["username"] = "9000000000";
        $this->sessionData["password"] = "BoS@S123";

        $this->header = array(
            'Content-Type: multipart/form-data; charset=utf-8',
         'X-API-KEY: ' . X_API_KEY . '',
        );
    }


    public function fino_post() {
          
        $header_key = "982b0d01-b262-4ece-a2a2-45be82212ba1";
        $body_key = "8329fffc-b192-4c9b-80cf-ccbbdf48bd67";

        $data = array();
        $input = $this->post();
     // print_r($input['type']);die;
       // $error_messages = json_decode(file_get_contents('messages.json'), true);
        if (!empty($input)) {
        	$config = array(
                array('field' => 'ClientUniqueID', 'label' => 'Client UniqueID', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Client id.')),
                array('field' => 'CustomerMobileNo', 'label' => 'Customer Mobile Number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter mobile number.')),
                array('field' => 'BeneIFSCCode', 'label' => 'Beneficiary Bank IFSC Code', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter IFSC code.')),
                array('field' => 'BeneAccountNo', 'label' => 'Beneficiary Bank Account Number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter account Number.')),
                array('field' => 'BeneName', 'label' => 'Beneficiary Name', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Beneficiary Name.')),
                array('field' => 'Amount', 'label' => 'Transaction Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter Transfer amount.')),
                array('field' => 'CustomerName', 'label' => 'Customer Name', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Customer Name.')),
                array('field' => 'RFU1', 'label' => 'Remark', 'rules' => 'trim|numeric|xss_clean', 'errors' => array('required' => 'Please enter Transfer amount.')),
                array('field' => 'RFU2', 'label' => 'Remark', 'rules' => 'trim|numeric|xss_clean', 'errors' => array('required' => 'Please enter Transfer amount.')),
                array('field' => 'RFU3', 'label' => 'Remark', 'rules' => 'trim|numeric|xss_clean', 'errors' => array('required' => 'Please enter Transfer amount.'))
            );


            if (form_validate_rules($config) == FALSE) {
  
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {

                $data['ClientUniqueID'] = $input['ClientUniqueID'];
                $data['CustomerMobileNo'] = $input['CustomerMobileNo'];
                $data['CustomerName'] = $input['CustomerName'];
                $data['BeneIFSCCode'] = $input['BeneIFSCCode'];
                $data['BeneAccountNo'] =  $input['BeneAccountNo'];
                $data['BeneName'] = $input['BeneName'];
                $data['Amount'] =   $input['Amount'];
                $data['RFU1'] =$input['RFU1'];
                $data['RFU2'] = $input['RFU2']; 
                $data['RFU3'] = $input['RFU3'];

                $res = json_encode($data);
                //echo $res;exit;
                $encr_res = $this->encrypt_fino($res, $body_key);
                //echo $encr_res;die;
              
             // print_r($input['type');die;

               if($input['type']==['Neft']){
                	//echo "poojak";die;
                $curl_url ="http://10.15.20.131:8001/Uiservice.svc/FinoMoneyTransactionApi/neftrequest";
                //	$curl_url = FINO_URL . "neftrequest/";
               }else if($input['type']==['Imps']){
               	echo "sdd";exit;
                $curl_url ="http://10.15.20.131:8001/Uiservice.svc/FinoMoneyTransactionApi/neftrequest";               	exit;
               }
              else{
                  
              }
                //echo $curl_url."\n".$encr_res;exit;
                $credentials=$this->encrypt_fino('{"ClientId":40,"AuthKey":"bff9a593-8aae-4d82-9f07-c6513fd8cff7"}',$header_key);
               // echo $encr_res;exit;
                //echo $credentials;exit;

                $curlResult = curlForPostData_fino($curl_url, $encr_res,$credentials);
               //echo $curlResult;exit;
               $output = json_decode($curlResult);
//		print_r($output);
              echo $output;
            }
        }
    }

    function encrypt_fino($text, $passphrase) {
        $encrypted = CryptoJSAES::encrypt($text, $passphrase);
        return $encrypted;
        //var_dump("Encrypted: ", $encrypted);  
    }

    function decrypt_fino($text, $passphrase) {
        $decrypted = CryptoJSAES::decrypt($text, $passphrase);
        return $decrypted;
        //var_dump("Decrypted: ", $decrypted);
    }

}
?>

