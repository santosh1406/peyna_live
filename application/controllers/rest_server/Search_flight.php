<?php

//Created by Shrasti Bansal on 01-02-2019

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Search_flight extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('various_helper');
        $this->headers = json_decode(BOS_FLIGHT_CREDS, true);  
        
        $log_data = array();
        $log_data['url'] = current_url();
        $log_data['Request_type'] = $_SERVER['REQUEST_METHOD'];
        $log_data['get'] = $this->input->get();
        $log_data['post'] = $this->post();
        $log_data['header'] = getallheaders();
        $log_data['user'] = $this->input->server('PHP_AUTH_USER');
        $log_data['ip'] = $this->input->ip_address();
        log_data('rest/flight/'.date('d-m-Y').'/flight_api_' . date('d-m-Y') . '.log', $log_data, 'api');
    }


    public function flight_services_post() {
       	$data = array();
        $input = $this->post();            
        // if ($input['from'] == "" || $input['to'] == "" || $input['date'] == "") {
        //     $this->response(array('status' => 'Failed', 'data' => "", 'msg' => 'Please provide proper inputs.'), 200);
        // } else { 
	        
	        $curl_url = $this->headers["url"]."search";
            $sessionData = array('username' => $this->headers["username"] , 'password' => $this->headers["password"]);
            $curlResult = curlForPostData_BOS_FLIGHT($curl_url,$input,$this->headers);
            $response = json_decode($curlResult);
			
            log_data('rest/flight/'.date('d-m-Y').'/flight_api_' . date('d-m-Y') . 'flight_services_post.log', $response, 'flight_services_post');
            
	    $this->response(array('status' => 'Success', 'data' => $response), 200);
        
     //}
    }

    public function airline_list_post(){
        $data = array();
        $input = $this->post();            
        $curl_url = $this->headers["url"]."airline_list";
        $sessionData = array('username' => $this->headers["username"] , 'password' => $this->headers["password"]);
        $curlResult = curlForPostData_BOS_FLIGHT($curl_url,$input,$this->headers);
        $response = json_decode($curlResult);
        log_data('rest/flight/'.date('d-m-Y').'/flight_api_' . date('d-m-Y') . 'airline_list_post.log', $response, 'airline_list_post');
      
        $this->response(array('status' => 'Success', 'data' => $response), 200);

    }

    public function fare_rule_post() {
        $data = array();
        $input = $this->post();            
        $curl_url = $this->headers["url"]."fare_rule";
        $sessionData = array('username' => $this->headers["username"] , 'password' => $this->headers["password"]);
        $curlResult = curlForPostData_BOS_FLIGHT($curl_url,$input,$this->headers);
        $response = json_decode($curlResult);
        log_data('rest/flight/'.date('d-m-Y').'/flight_api_' . date('d-m-Y') . 'fare_rule_post.log', $response, 'fare_rule_post');
        
        $this->response(array('status' => 'Success', 'data' => $response), 200);

    }

    public function review_post() {
        $data = array();
        $input = $this->post();            
        $curl_url = $this->headers["url"]."review";
        $sessionData = array('username' => $this->headers["username"] , 'password' => $this->headers["password"]);
        $curlResult = curlForPostData_BOS_FLIGHT($curl_url,$input,$this->headers);
        $response = json_decode($curlResult);
        log_data('rest/flight/'.date('d-m-Y').'/flight_api_' . date('d-m-Y') . 'review_post.log', $response, 'review_post');
        
        $this->response(array('status' => 'Success', 'data' => $response), 200);

    }
    public function book_post() {
        $data = array();
        $input = $this->post();            
        $curl_url = $this->headers["url"]."book";
        $sessionData = array('username' => $this->headers["username"] , 'password' => $this->headers["password"]);
        $curlResult = curlForPostData_BOS_FLIGHT($curl_url,$input,$this->headers);
        $response = json_decode($curlResult);
        log_data('rest/flight/'.date('d-m-Y').'/flight_api_' . date('d-m-Y') . 'book_post.log', $response, 'book_post');
      
        $this->response(array('status' => 'Success', 'data' => $response), 200);

    }
    public function book_details_post() {
        $data = array();
        $input = $this->post();            
        $curl_url = $this->headers["url"]."book_details";
        $sessionData = array('username' => $this->headers["username"] , 'password' => $this->headers["password"]);
        $curlResult = curlForPostData_BOS_FLIGHT($curl_url,$input,$this->headers);
        $response = json_decode($curlResult);
        log_data('rest/flight/'.date('d-m-Y').'/flight_api_' . date('d-m-Y') . 'book_details_post.log', $response, 'book_details_post');
    
        $this->response(array('status' => 'Success', 'data' => $response), 200);

    }
    public function ticket_print_post() {
        $data = array();
        $input = $this->post();            
        $curl_url = $this->headers["url"]."ticket_print";
        $sessionData = array('username' => $this->headers["username"] , 'password' => $this->headers["password"]);
        $curlResult = curlForPostData_BOS_FLIGHT($curl_url,$input,$this->headers);
        $response = json_decode($curlResult);
        log_data('rest/flight/'.date('d-m-Y').'/flight_api_' . date('d-m-Y') . 'ticket_print_post.log', $response, 'ticket_print_post');
       
        $this->response(array('status' => 'Success', 'data' => $response), 200);

    }


}