<?php

//Created by sonali kamble on 8-feb-2019

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Waybill_extra_topup extends REST_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model(array('waybill_extra_topup_model','wallet_model'));
    }

    public function agent_balance_post() {       
        
        $input = $this->post();
        if (!empty($input)) {
            $config = array(
                array('field' => 'agent_code', 'label' => 'Agent Code', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter agent id.')), 
                array('field' => 'amount', 'label' => 'Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter Amount.')),
                array('field' => 'waybill_no', 'label' => 'Waybill No', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter waybill no.')),

            );

            if (form_validate_rules($config) == False) {
                $error = $this->form_validation->error_array();
                $this->response(array('status' => 'failed', 'error' => $error), 200);
            } else {
                
                $waybillid = $this->waybill_extra_topup_model->check_waybill_status($input['waybill_no']);
                
                show($waybillid) ;
                die;
                $from_wallet_details = $this->wallet_model->where('user_id',$input['agent_code'])->find_all();
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'wallet.log', $from_wallet_details, 'Agent Data');
                $amount = $input['amount'];
                $wallet_amt = $from_wallet_details[0]->amt;                
                if($amount > $wallet_amt)
                {                    
                    $this->response(array('status' => 'failed', 'msg' => 'Insufficient Wallet balance'), 200);
                }else
                {                     
                    $this->response(array('status' => 'success', 'msg' => ''), 200);
                }                
            }
        }
    }


}