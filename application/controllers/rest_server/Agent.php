<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Agent extends REST_Controller {
    protected $data;
    protected $sSecretKey = 'fdac7124c6a2b3f6';
    protected $vendor_Agent = '';

    
    function __construct() {
        parent::__construct();

        $this->load->model(array('current_booking_report_model','rfid_agent_card_model','rfid_model','agent_model','booking_agent_model','city_master_model','country_master_model','state_master_model','bus_stop_master_model','wallet_model','wallet_trans_model','users_model', 'op_wallet_model'));
        
        ini_set('max_execution_time', 3000);
        ini_set("memory_limit",-1);

        $log_data = array();
        $log_data['url']            = current_url();
        $log_data['Request_type']   = $_SERVER['REQUEST_METHOD'];
        $log_data['get']            = $this->input->get();
        $log_data['post']           = $this->post();
        // $log_data['api_post']       = $this->post();
        $log_data['header']         = getallheaders();
        $log_data['user']           = $this->input->server('PHP_AUTH_USER');
        $log_data['ip']             = $this->input->ip_address();
        
        log_data('rest/agent/'.date('M').'/'.date('d-m-Y').'/request'.'.log', $log_data, 'api');


        $this->data = array('status' => 'failed');
        $this->vendor_Agent = $this->agent_model->get_by_user_id($this->user->id);


    }

    function index_get(){
        echo 'not supported';
    }
    
    /*
     * Add Agent Details
     */
    
    function addAgentDetails_post() {
        $input = $this->post();
        $this->db->trans_begin();
        $config = array(
            array('field' => 'fname', 'label' => 'Agent First Name', 'rules' => 'trim|required', 'errors' => array('required' => 'Please enter Agent First  Name.', 'alpha' => 'Please Enter valid name')),
            //array('field' => 'mname', 'label' => 'Agent Middle Name', 'rules' => 'trim|required|alpha', 'errors' => array('required' => 'Please enter Agent Middle Name.', 'alpha' => 'Please Enter valid name')),
            // array('field' => 'lname', 'label' => 'Agent Last Name', 'rules' => 'trim|required|alpha', 'errors' => array('required' => 'Please enter Agent Last Name.', 'alpha' => 'Please Enter valid name')),
            // array('field' => 'address', 'label' => 'Agent Address', 'rules' => 'trim|required', 'errors' => array('required' => 'Please enter Agent Address.')),
            // array('field' => 'gender', 'label' => 'Gender', 'rules' => 'trim|required|callback_check_gender', 'errors' => array('required' => 'Please Select Gender','check_gender'=>'Please provoid valid gender M or F')),
            array('field' => 'contact_no', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|min_length[10]|max_length[10]', 'errors' => array('required' => 'Please Enter Mobile Number.','min_length'=>"Please enter 10 digit mobile number",'max_length'=>'Please enter 10 digit mobile number')),
            // array('field' => 'dob', 'label' => 'Date of Birth', 'rules' => 'trim|required|callback_date_of_birth', 'errors' => array('required' => 'Please select date of birth.','date_of_birth'=>'Please enter valid date of birth')),
            // array('field' => 'pincode', 'label' => 'Pincode', 'rules' => 'trim|required|numeric|callback_valid_pin|max_length[6]|min_length[6]', 'errors' => array('required' => 'Please Enter Pincode Number', 'numeric' => 'Please Enter Valid Pincode Number', 'valid_pin' => 'Please enter valid pin number', 'min_length' => 'Pincode number should be 6 digit')),           
            // array('field' => 'country_name', 'label' => 'country Name', 'rules' => 'trim|required|callback_alpha_space', 'errors' => array('required' => 'Please enter country name')),
            // array('field' => 'state_name', 'label' => 'State Name', 'rules' => 'trim|required|callback_alpha_space', 'errors' => array('required' => 'Please enter state name')),
            // array('field' => 'city_name', 'label' => 'city Name', 'rules' => 'trim|required|callback_alpha_space', 'errors' => array('required' => 'Please enter city name')),
            array('field' => 'v_agent_code', 'label' => 'Vendor Agent Code', 'rules' => 'trim|required', 'errors' => array('required' => 'Please Provide vendors agent code')),
            array('field' => 'email', 'label' => 'Email', 'rules' => 'trim|required|valid_email', 'errors' => array('required' => 'Please Provide email id', 'valid_email'=>'Please enter valid email id')),
            array('field' => 'level', 'label' => 'Level', 'rules' => 'trim|required|numeric', 'errors' => array('required' => 'Please Provide level id', 'numeric'=>'Enter valid level')),
  
                  );
            if (form_validate_rules($config) == FALSE) 
            {
                $error = $this->form_validation->error_array();
                $this->response(array($error, 200));
          
            } 
            else 
            {
                if($input > 0) {
                    //Check if agent data is already available
                    $agent = $this->agent_model
                                               // ->where(array('created_by' => $this->user->id)) 
                                               ->or_where(
                                                    array(
                                                        'email' => $input['email'],
                                                        'contact_no' => $input['contact_no'],
                                                        'v_agent_code' => $input['v_agent_code'],

                                                    )
                                                )
                                               ->find_all();
                                               // ->get_by_vAgentCode($input['v_agent_code']);
                    $this->vendor_Agent = $this->agent_model->get_by_user_id($this->user->id);


                    // last_query();
                    // show($agent,1);

                    switch($input['level'])
                    {
                        case 4:
                            if(empty($input['level3']))
                            {
                                $this->response(array('error', 'Please provide level3'), 200);
                            }
                            break;
                        case 5:
                            if(empty($input['level3']) || empty($input['level4']) )
                            {
                                $this->response(array('error', 'Please provide level3 & level4'), 200);   
                            }
                            break;
                    }

                    // last_query(1);
                    if(!$agent)
                    {
                        //Generate agent code
                        $agent_code = $this->booking_agent_model->generate_agent_id();
                        if($agent_code) {
                            // if(!$agent) 
                            // {

                                //Check if the country is available in country_master table
                                $country_id = 1;
                                // if(isset($input['country_name']) && $input['country_name'] != '')
                                // {
                                //     $country_id = $this->country_master_model->get_insert_country_by_name($input['country_name']);
                                // }

                                //Check if the state is available in state_master table
                                $state_id = $input['state_name'];
                                // if(isset($input['state_name']) && $input['state_name'] != '')
                                // {   
                                //     $state_id = $this->state_master_model->get_insert_state_by_name($input['state_name'], $country_id);
                                // }


                                //Check if the city is available in city_master table
                                $city_id = $input['city_name'];
                                // if(isset($input['city_name']) && $input['city_name'] != '')
                                // {
                                //     $city_id = $this->city_master_model->get_insert_city_by_name($input['city_name'], $country_id, $state_id);
                                // }

                                if($input['level'] > 3 && !isset($input['level3']) && trim($input['level3']) == '')
                                {
                                   $this->response(array('error' => 'level 3 code required'), 200);
                                }

                                //find Id of level3 if available
                                $level3 = false;
                                if($input['level'] > 3 && isset($input['level3']) && trim($input['level3']) != '' )
                                {
                                    $level3 = $this->agent_model->get_by_agentCode($input['level3']);
                                }

                                 //find Id of level4 if available
                                $level4 = false;
                                if($input['level'] > 4 && isset($input['level4']) && trim($input['level4']) != '' )
                                {
                                    $level4 = $this->agent_model->get_by_agentCode($input['level4']);
                                }

                                if($input['level'] > 4 && !isset($input['level4']) && trim($input['level4']) == '')
                                {
                                    $this->response(array('error' => 'level 4 code required'), 200);
                                }

                                 //find Id of level5 if available
                                $level5 = false; 
                                if(isset($input['level5']) && trim($input['level5']) != '')
                                {
                                    // $level5 = $this->agent_model->get_by_agentCode($input['level5']);
                                }

                                if($input['level'] > 3)
                                {
                                    $po_level   = 'level'.($input['level']-1);

                                    $parent_obj = ( ($input['level']-1) > 2) ? ${$po_level} : false;

                                    $parent     = $parent_obj->agent_id;
                                }
                                else
                                {
                                    // $parent     = $this->user->id;
                                    $parent     = 2;
                                }
                                       $depot_data= $this->current_booking_report_model->depot_data_where($input['depot_code']);
                                       $zone_data=$this->current_booking_report_model->zone_data_where($depot_data[0]['DIVISION_CD']);
                                $insert_agent = array(
                                                    'fname'         => isset($input['fname']) ? $input['fname'] : '',
                                                    // 'mname'         => isset($input['mname']) ? $input['mname'] : '',
                                                    // 'lname'         => isset($input['lname']) ? $input['lname'] : '',
                                                    'agent_code'    => $agent_code,
                                                    'v_agent_code'  => isset($input['v_agent_code']) ? $input['v_agent_code'] : '',
                                                    'address'       => isset($input['address']) ? $input['address'] : '',
                                                    'country_id'    => $country_id,
                                                    'state_id'      => $state_id,
                                                    'city_id'       => $city_id,
                                                    'depot_name'    => isset($depot_data[0]['DEPOT_NM'])?$depot_data[0]['DEPOT_NM']:'',
                                                    'zone_name'     =>isset($zone_data[0]['DIVISION_NM'])?$zone_data[0]['DIVISION_NM']:'',
                                                    'depot_cd'      =>isset($input['depot_code'])?$input['depot_code']:'',
                                                    'zone_cd'       =>isset($depot_data[0]['DIVISION_CD'])?$depot_data[0]['DIVISION_CD']:'',
                                                    'gender'        => isset($input['gender']) ? $input['gender'] : '',
                                                    'dob'           => isset($input['dob']) ? date('Y-m-d',strtotime($input['dob'])) : '0000-00-00',
                                                    'email'         => isset($input['email']) ? $input['email'] : '',
                                                    'contact_no'    => isset($input['contact_no']) ? $input['contact_no'] : '',
                                                    'pincode'       => isset($input['pincode']) ? $input['pincode'] : '',
                                                    'parent'        => $parent,
                                                    'level'         => isset($input['level']) ? $input['level'] : '',
                                                    'level1'        => '1',
                                                    'level2'        => $this->vendor_Agent->agent_id,
                                                    'kyc'           => 'N',
                                                    'is_status'     => 1,
                                                    // 'is_deleted'    => $input['is_deleted'],
                                                    'created_by'    => $this->user->id,
                                                    // 'created_date'  => $input['created_date']
                                                );

                                if($level3){
                                     $insert_agent['level3'] = $level3->agent_id;
                                }

                                if($level4){
                                     $insert_agent['level4'] = $level4->agent_id;
                                }

                                if($level5){
                                     $insert_agent['level5'] = $level5->agent_id;
                                }


                                $agent_id = $this->agent_model->insert($insert_agent);

                                if($agent_id && !empty($this->user->id))
                                {

                                    $rfid = $this->rfid_model->unique_card_number_for_api_user($this->user->id);


                                    if($rfid)
                                    {
                                        $assign_id = $this->rfid_agent_card_model->assign_card_to_agent($rfid, $agent_id, $this->user->id);
                                    }

                                    if ($this->db->trans_status() === FALSE)
                                    {
                                        show(error_get_last());
                                        $this->db->trans_rollback();
                                    }
                                    else
                                    {
                                        $this->db->trans_commit();
                                        // $this->db->trans_rollback();
                                    }
                                    
                                    $unique_rfid  = $this->rfid_model->column('unique_card_numbers')
                                                    ->where('rfid_card_id',$rfid)
                                                    ->as_array()
                                                    ->find_all();
                                    $rfid_unique_no = $unique_rfid[0]['unique_card_numbers'];

                                    $response = array(
                                                    'msg' => 'Agent Added Successfully',
                                                    'agent_code' => $agent_code, 
                                                    'v_agent_code'=> $input['v_agent_code'], 
                                                    'rfid_number' => $rfid_unique_no, 
                                                    'status' => 'success'
                                                );
                                    
                                    $this->response( $response, 200);
                                }
                                else
                                {
                                    $this->response(array( 'status' => 'failed', 'error' => 'data not inserted Pls try again or contact provider'), 200);
                                }
                        }                
                        else
                        {
                             $this->response(array( 'status' => 'failed', 'error' => 'Agent code not generated'), 400);
                        }
                    } 
                    else 
                    {
                        $error = array();
                        if($agent[0]->email == $this->post('email') )
                        {
                            $error[] = 'Email already exists';
                        }

                        if($agent[0]->contact_no == $this->post('contact_no') )
                        {
                            $error[] = 'Contact no already exists';
                        }

                        if($agent[0]->v_agent_code == $this->post('v_agent_code') )
                        {
                            $error[] = 'Vendor code already exists';
                        }

                        $this->response(array( 'status' => 'failed', 'data' => $error), 400);
                    }
                }
                else
                {
                    $this->response(array( 'status' => 'failed','error' => 'No data found'), 400);
                }
            }
    }    
    
    /**************Save agent KYC detail************************/
    function uploadKycDetail_post() 
    {
        ini_set('max_execution_time', 300);

        $input  = $this->post();
        // show($this->post());

        $uid    = $input['agent_code'];
    

        $image_format_valid = array('image/jpeg','image/jpg','image/png');
        $image_extension    = array(
                                        'image/jpeg' => '.jpeg',
                                        'image/jpg' => '.jpg',
                                        'image/png' => '.png'
                                    );

        $agent = $this->agent_model->find_by('agent_code', $input['agent_code']);

        $agent_id = $agent->agent_id;

        if($agent)
        {
            $doc_cat_list       = array(1,2);

            $cat_name       = array(1 => "id_proof", 2 => "add_proof");

            // $id_proof_list      = $this->booking_agent_model->get_doc_by_category_id(1);
            // $addres_proof_list  = $this->booking_agent_model->get_doc_by_category_id(2);

            $category[1]    = $this->booking_agent_model->get_doc_by_category_id(1);
            $category[2]    = $this->booking_agent_model->get_doc_by_category_id(2);


            $id_proof_key   = array_column($category[1], 'agent_docs_list');
            $add_proof_key  = array_column($category[2], 'agent_docs_list');

            $cat[1]  = $id_proof_key;
            $cat[2]  = $add_proof_key;

            $flag =  true;
            $upload = array();

            // log_data('rest/agent_'.date('Y-m-d').'.log',$input['doc'], 'befor doc');
            log_data('rest/agent/'.date('M').'/'.date('d-m-Y').'/api'.'.log', $input['doc'], 'befor doc Upload_kyc doc');

            if(is_json($input['doc']))
            {
              $input['doc'] = json_decode($input['doc'],true);
            }

            // log_data('rest/agent_'.date('Y-m-d').'.log',$input['doc'], 'after docs');
            log_data('rest/agent/'.date('M').'/'.date('d-m-Y').'/api'.'.log', $input['doc'], 'after docs Upload_kyc doc');

            foreach ($input['doc'] as $key => $value) {
                $value['doc_path'] = str_replace(" ", "%20", $value['doc_path']);

                // show(var_dump(getDataFromRemote($value['doc_path'])),1);
                if(getDataFromRemote($value['doc_path']))
                {

                    if(in_array($value['doc_cat'],  $doc_cat_list))
                    {
                        if(in_array($value['doc_name'],  $cat[$value['doc_cat']]))
                        {   
                            $file_path  = strtolower(AGENT_DOCS_FOLDER . '/' . $agent_id . '/' . $cat_name[$value['doc_cat']]);


                            if(!file_exists($file_path . '/large')) 
                            {
                                mkdir($file_path . '/large', 0770, true);
                            }
                            if (!file_exists($file_path . '/thumb')) 
                            {
                                mkdir($file_path. '/thumb', 0770, true);
                            }

                            if($value['doc_path'] != '' && @getimagesize($value['doc_path']))
                            {
                                $img_detail =  @getimagesize($value['doc_path']);

                                if(!$img_detail && !in_array($img_detail['mime'], $image_format_valid) && remote_file_size($value['doc_path']) > 1000)
                                {
                                    $flag = false;
                                    $input['doc'][$key]['error'] = "Image should not be grater than 1 MB and extension should be JPG|JPEG|PNG ";
                                }
                            }
                            else
                            {
                                $flag = false;
                                $input['doc'][$key]['error'] = "Give proper image path";
                            }
                        }
                        else
                        {
                            $flag = false;
                            $input['doc'][$key]['error'] = "Document does not belongs to this category";
                        }
                    }
                    else
                    {
                        $flag = false;
                        $input['doc'][$key]['error'] = "Wrong category id";
                    }
                }
                else{
                    $flag = false;
                    $input['doc'][$key]['error'] = "Image not exsist";
                }               

            }

            if($flag)
            {
                if(!empty($input['doc']) && is_array($input['doc']))
                {
                    foreach ($input['doc'] as $key => $value) 
                    {
                        $value['doc_path'] = str_replace(" ", "%20", $value['doc_path']);
                        $img_arr = @getimagesize($value['doc_path']);
                        
                        $ext = $image_extension[$img_arr['mime']];

                        $doc_name = str_replace(' ', '_', $category[$value['doc_cat']][$value['doc_name']]['docs_name'].$ext);

                        $cat_folder = $cat_name[$value['doc_cat']];

                        // show(AGENT_DOCS_FOLDER.$uid.'/'.$cat_folder.'/large/'.$doc_name.$ext,1);
                        $file_path = AGENT_DOCS_FOLDER.$agent_id.'/'.$cat_folder;



                        
                        $large_image = strtolower($file_path.'/large/'.$doc_name);

                     

                        $insert_arr =  array(
                                            // 'agent_docs_id' => '',
                                            // 'agent_id' => $input['agent_code'],
                                            'agent_id' => $agent_id,
                                            'docs_cat' => strtolower($value['doc_cat']),
                                            'docs_name' => strtolower($value['doc_name']),
                                            // 'large_img_link' => $doc_name
                                        );


                        $adm_query =$this->db->from('agent_docs_master')->where($insert_arr)->get();

                        $adm_result = $adm_query->row();

                        
                        /* this is use for comment */
                        // $comment = '';
                        // if(isset($value['comment']) && $value['comment']=="")
                        //   {
                        //     if($adm_result)
                        //        {
                        //          if($adm_result->comment!="")
                        //            {
                        //              $comment = $adm_result->comment;
                        //            }
                        //            else
                        //            {
                        //                $comment = " ";
                        //            }  
                        //        }  
                        //    }
                        //   else
                        //   {
                        //       $comment  = $value['comment'];
                        //   }     

                        $insert_arr['large_img_link'] = $doc_name;
                        // $insert_arr['comment'] = $comment;

                        if(isset($value['comment']) && $value['comment'] != '')
                        {
                            $insert_arr['comment'] = $value['comment'];
                        }

                        $insert_arr['created_by'] = $this->user->id;

                        // chmod(AGENT_DOCS_FOLDER, 0770); 


                        
                        if($adm_query->num_rows() > 0)
                        {
                            // unlink(strtolower($file_path.'/large/'.$adm_result->large_img_link));
                            $this->db->update('agent_docs_master', $insert_arr, array('docs_cat' => $value['doc_cat'], 'docs_name' => $value['doc_name'], 'agent_id' => $agent_id));
                        }
                        else
                        {
                            $this->db->insert('agent_docs_master', $insert_arr);
                        }

                        save_remote_image($value['doc_path'],$large_image);

                        
                        $thumb_image = strtolower($file_path.'/thumb/'.$doc_name);

                        
                        // save_remote_image($value['doc_path'], $thumb_image);
                        $this->resize($large_image,$thumb_image, 130, 90);

                    }
                    
                    $response = array('SUCCESS'=>'Docs uploaded successfully!', 'status' => 'success');
                    
                    // log_data('rest/agent_api_'.date('d-m-Y').'.log', $response, 'add  agent  Kyc docs response ');
                    log_data('rest/agent/'.date('M').'/'.date('d-m-Y').'/api'.'.log', $response, 'Upload_kyc doc response');

                    $this->response( $response, 200);
                }
                else
                {
                    $this->response( array('error'=>'No docs uploaded or Invalid Json format', 'status' => 'failed'), 200);
                }
            }
            else
            {
                $this->response(array('data'=> $input['doc'], 'status' => 'failed'), 200);
            }
        }
        else
        {
            $this->response(array('error' => 'Agent No exists'), 200);
        }        
    }

    /********Resize Image**************/
    
    public function resize($org_img, $new_img , $width, $height) {
        $this->load->library('image_lib');
      
        $config['image_library'] = 'gd2';
        $config['source_image'] = $org_img;
        $config['maintain_ratio'] = TRUE;
        $config['quality'] = "100";
        $config['new_image'] = $new_img;
        $config['width'] = $width;
        // $config['height'] =$height;

        $this->image_lib->initialize($config);
        if (!$this->image_lib->resize()) {
            echo $this->image_lib->display_errors();
        }
        
    }

    /***Update agent record******/
    
       /*
     * @function    : update_Agent_detail
     * @param       : agent detail in array
     * @detail      : update agent detail in database
     * @author      : pankaj                
     */
    public function updateAgentDetail_post()
    {      
        $input = $this->post();
        
        $check_agent = $this->booking_agent_model->column('agent_code')
                ->where('agent_code',$input['agent_code'])
                ->where('v_agent_code',$input['v_agent_code'])
                ->as_array()
                ->find_all();
       
        if(count($input) > 0)
        {
           if($check_agent)
           {
                $config = array(
                //array('field' => 'fname', 'label' => 'Agent First Name', 'rules' => 'trim|required|alpha', 'errors' => array('required' => 'Please enter Agent First  Name.', 'alpha' => 'Please Enter valid name')),
                // array('field' => 'mname', 'label' => 'Agent Middle Name', 'rules' => 'trim|required|alpha', 'errors' => array('required' => 'Please enter Agent Middle Name.', 'alpha' => 'Please Enter valid name')),                array('field' => 'lname', 'label' => 'Agent Last Name', 'rules' => 'trim|required|alpha', 'errors' => array('required' => 'Please enter Agent Last Name.', 'alpha' => 'Please Enter valid name')),
                // array('field' => 'address', 'label' => 'Agent Address', 'rules' => 'trim|required', 'errors' => array('required' => 'Please enter Agent Address.')),
                // array('field' => 'gender', 'label' => 'Gender', 'rules' => 'trim|required|callback_check_gender', 'errors' => array('required' => 'Please Select Gender','check_gender'=>'Please provoid valid gender M or F')),
                array('field' => 'contact_no', 'label' => 'Mobile Number', 'rules' => 'trim|numeric|min_length[10]|max_length[10]', 'errors' => array('min_length'=>"Please enter 10 digit mobile number",'max_length'=>'Please enter 10 digit mobile number')),
                // array('field' => 'dob', 'label' => 'Date of Birth', 'rules' => 'trim|required|callback_date_of_birth', 'errors' => array('required' => 'Please select date of birth.','date_of_birth'=>'Please enter valid date of birth')),
                // array('field' => 'pincode', 'label' => 'Pincode', 'rules' => 'trim|required|numeric|callback_valid_pin|max_length[6]|min_length[6]', 'errors' => array('required' => 'Please Enter Pincode Number', 'numeric' => 'Please Enter Valid Pincode Number', 'valid_pin' => 'Please enter valid pin number', 'min_length' => 'Pincode number should be 6 digit')),           
                // array('field' => 'country_id', 'label' => 'country Name', 'rules' => 'trim|required', 'errors' => array('required' => 'Please Select country name')),
                // array('field' => 'state_id', 'label' => 'State Name', 'rules' => 'trim|required', 'errors' => array('required' => 'Please Select state name')),
                // array('field' => 'city_id', 'label' => 'city Name', 'rules' => 'trim|required', 'errors' => array('required' => 'Please Select city name')),
                    array('field' => 'agent_code', 'label' => 'BOS Agent Code', 'rules' => 'trim|required', 'errors' => array('required' => 'Please Provide BOS  agent code')),
                    array('field' => 'v_agent_code', 'label' => 'Vendor Agent Code', 'rules' => 'trim|required', 'errors' => array('required' => 'Please Provide vendors agent code')),
                //array('field' => 'email', 'label' => 'Email', 'rules' => 'trim|required|valid_email', 'errors' => array('required' => 'Please Provide email id', 'valid_email'=>'Please enter valid email id')),
//                array('field' => 'level', 'label' => 'Level', 'rules' => 'trim|required|numeric', 'errors' => array('required' => 'Please Provide level id', 'numeric'=>'Enter valid level')),
                      );
                if (form_validate_rules($config) == FALSE) 
                {
                    $error = $this->form_validation->error_array();
                    $this->response(array($error, 200));
              
                } 
                else 
                {
                    $update_to = array();
                   if( !empty($input['email']) )
                   {
                       $update_to['email'] = $input['email'];
                   }
                   
                   if( !empty($input['contact_no']) )
                   {
                       $update_to['contact_no'] = $input['contact_no'];
                   }
                   
                   if( !empty($input['pincode']) )
                   {
                       $update_to['pincode'] = $input['pincode'];
                   }
                   
                   
                   if( !empty($input['fname']) )
                   {
                       $update_to['fname'] = $input['fname'];
                   }
                   if( !empty($input['mname']) )
                   {
                       $update_to['mname'] = $input['mname'];
                   }
                     if( !empty($input['lname'])  )
                   {
                       $update_to['lname'] = $input['lname'];
                   } 
                   if( !empty($input['address']) )
                   {
                       $update_to['address'] = $input['address'];
                   } 
                     if( !empty($input['gender']) )
                   {
                       $update_to['gender'] = $input['gender'];
                   } 
                   
                   if( !empty($input['dob']) )
                   {
                       $update_to['dob'] = date("Y-m-d",strtotime($input['dob']));
                   }

                    if(count($update_to))
                    {
                        $this->booking_agent_model->update_where('agent_code',$input['agent_code'],$update_to);
                        $this->response(array('msg' => 'Agent has been sucessfully updated', 'status' => 'success'), 200);
                    }
                    else
                    {
                        $this->response(array('error' => 'Provide data for update'), 200);
                    }
                }
          
            }
            else
            {
                 $this->response(array('Msg' => 'Agent does not exist..so please provide valid agent code'), 404);
            }
        } 
        else
        {
                $this->response(array('Msg' =>'No data found'), 200);
        }   
              
    }
    
    function getAgentDetail_post()
    {
        $input = $this->post();
        $this->load->model(array('rfid_agent_card_model'));
         
        $config = array(
                    array('field'=>'vAgentCode', 'label'=>'Vendor Agent Code', 'rules'=>'required', 'errors'=>array('required'=>'Vendors agent code is required')),
                    array('field'=>'agent_code', 'label'=>'Agent Code', 'rules'=>'required', 'errors'=>array('required'=>'Agent Code is required')),
                    );
       if (form_validate_rules($config) == FALSE) 
        {
            $error = $this->form_validation->error_array();
            $this->response(array($error, 200));

        } 
        else 
        {
            $agent = $this->agent_model->where(array('agent_code'=>$input['agent_code'], 'v_agent_code'=>$input['vAgentCode']))->find_all();

            // last_query(1);
            // show($agent,1);
            if($agent)
            {
                if($agent[0]->kyc == 'Y')
                { 

                    $rfid = $this->rfid_agent_card_model
                                ->column('rfid_unique_card_numbers.unique_card_numbers')
                                ->where(array('agent_id'=>$agent[0]->agent_id))
                                ->join("rfid_unique_card_numbers","rfid_unique_card_numbers.rfid_card_id  = rfid_agent_card_detail.card_no")
                                ->find_all();
                    
                    if($rfid)
                    {
                      
                        $this->response (
                                            array(
                                                'agent_code'    =>  $agent[0]->agent_code,
                                                'v_agent_code'  =>   $agent[0]->v_agent_code,
                                                'rfid_card_no'  => $rfid[0]->unique_card_numbers,
                                                'hhm_serial_no' => '',
                                                'status'        => 'success'
                                            )
                                        ,200);
                    }else{
                        $this->response(array('error'=>'Rfid Is not assigned to this card id'),400); 
                    }
                }
                else
                {

                    $this->response(array('error' => 'KYC not approved'), 200);
                }
                
               
                
                //$this->response($data,200);      
            }
            else
            {
                $this->response(array('error'=>'Agent not found'),400);
            }

        }
    }

    /*******validation function*************/
    function check_gender($gender)
    {
        return ($gender!='M' && $gender!='F' )? false : true;        
    }

    function valid_pin($pin) 
    {
        return  ($pin == 0)? false : true;        
    }
        
    function valid_amt($amt) 
    {
        return  ($amt < 0 &&  $amt < 20)? false : true;        
    }

    function check_vagent_exist($v_agent_cd,$agent_cd)
    {
    
        $cnt  = $this->db->from('agent_master')->where(array('agent_code'=>trim($agent_cd),"v_agent_code"=>trim($v_agent_cd)))->get()->num_rows();
        return ($cnt == 0)? false : true;        
    }

    function date_of_birth($date)
    {
        if(isset($date) && $date!='' && (preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/",$date)))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function alpha_space($str) {
        return (!preg_match("/^([a-z ])+$/i", $str)) ? FALSE : TRUE;
    }


    
    /****save agent star seller point****/
    function saveStarSellerPoint_post()
    {
        $input=$this->post();

        $config = array(
                        array('field'=>'vAgentCode', 'label'=>'Vendor Agent Code', 'rules'=>'required', 'errors'=>array('required'=>'Vendors agent code is required')),
                        array('field'=>'agent_code', 'label'=>'Agent Code', 'rules'=>'required', 'errors'=>array('required'=>'Agent Code is required')),
                        array('field'=>'bus_stop_name', 'label'=>'Bus Stop Name', 'rules'=>'required', 'errors'=>array('required'=>'Bus stop name is required')),
                    );
        if (form_validate_rules($config) == FALSE) 
        {
            $error = $this->form_validation->error_array();
            $this->response(array($error, 200));

        }
        else 
        {
            $agent = $this->agent_model->find_by('agent_code', trim($input['agent_code']));
            $chk_bus_stop =$this->db->from('bus_stop_master')->where(array('bus_stop_name'=>trim($input['bus_stop_name'])))->get();
            $bus_stop_detail =$chk_bus_stop->row();
            if($agent)
            {
                if(!empty($bus_stop_detail))
                {
                    $chk_bus_stop =$this->db->from('ag_star_seller_detail')->where(array("agent_id"=>$agent->agent_id,"is_status"=>1,"is_deleted"=>0))->get()->num_rows();
                    $update_status_arr=array("is_status"=>0);
                    $insert_arr=array("agent_id"=>$agent->agent_id,"bus_stop_id"=>$bus_stop_detail->bus_stop_id,"bus_stop_name"=>$bus_stop_detail->bus_stop_name);                            
                    if($chk_bus_stop > 0)
                    {
                      $this->db->update('ag_star_seller_detail', $update_status_arr, array('agent_id'=>$agent->agent_id));
                    }                               
                    
                    $this->db->insert('ag_star_seller_detail',$insert_arr);
                    $this->response(array('msg'=>'Agent star selelr detail saved successfully', 'status' => 'success'),200);         
               }
               else
               {
                    $this->response(array('error'=>'Bus stop Name is not exist in system. So contact to BOS'),400);  
               }    
            }
            else
            {
                     $this->response(array('error'=>'Agent code does not exist'),400);                             
            }                   
        }                        
    }
    
    /*
     * Approve agent KYC
     */
    
    function mapAgentToVendor_post() 
    {
        $input = $this->post();
        $config = array(
                    array('field'=>'agent_code', 'label'=>'Agent Code', 'rules'=>'required', 'errors'=>array('required'=>'Agent Code is required')),
                    );
         if (form_validate_rules($config) == FALSE) 
        {
            $error = $this->form_validation->error_array();
            $this->response(array($error, 200));

        } 
        else 
        { 
            $vendors_user_id =  $this->user->id;
            $agentData       =  $this->agent_model->where(array('agent_code'=>$input['agent_code']))->find_all();
            $agent_id        =  $agentData[0]->agent_id;
            $updateKyc       =  $this->agent_model->map_agent_to_vendor($agent_id,$vendors_user_id);
            if($updateKyc) {
                
            } else {
                $this->response(array('error'=>'Kyc is already apporved'),200);
            }
        }
    }

    /*****agent top up***********/          
    function agent_top_up_post()
    {
        $input=$this->post();

        if(isset($input) && isset($input['agent_code']) && isset($input['v_agent_code']) && isset($input['top_up_amt']))
        {                                   
            $Agent_code = $input['agent_code'];
            $topupamt   = $input['top_up_amt'];
            $config = array(
                           array('field' => 'agent_code', 'label' => 'Agent code', 'rules' => 'trim|required', 'errors' => array('required' => 'Please  provide  BOS agent code')), 
                           array('field' => 'v_agent_code', 'label' => 'Vendor Agent code', 'rules' => 'trim|required|callback_check_vagent_exist['.$Agent_code.']', 'errors' => array('required' => 'Please  provide  Vendor agent code','check_vagent_exist'=>'Please check BOS agent code and Vendor agent code')), 
                           array('field' => 'top_up_amt', 'label' => 'Agent Top up amount', 'rules' => 'trim|required|numeric|callback_valid_amt', 'errors' => array('required' => 'Please  provide agent topup amount.', 'number' => 'Please provide valid amount','valid_amt'=>'Please provide amount above 30')),
                            
                         );
          
            if (form_validate_rules($config) == FALSE) 
            {
               $error = $this->form_validation->error_array();
               $this->response(array($error, 200));
            } 
            else
            {
                $user = $this->booking_agent_model->column('user_id')
                           ->where('agent_code',$input['agent_code'])
                           ->where('v_agent_code',$input['v_agent_code'])
                           // ->as_array()
                            ->find_all();

                if($user)
                {
                    $wallet_data = $this->wallet_model
                                       ->where('user_id',$user[0]->user_id)
                                       ->as_array()
                                       ->find_all();
                        

                    if($wallet_data)
                    {  
                        
                        $updated_balance=$wallet_data[0]['amt']+$topupamt;
                        $update_to=array("amt"=>$updated_balance,"comment"=>'Top up');
                        $update_id=$this->wallet_model->update_where('user_id',$wallet_data[0]['user_id'],$update_to);
                        
                        if($update_id)
                        {

                           $insert_wallet_trans=array("w_id"  =>  $wallet_data[0]['id'],
                                                   "amt"      =>   $topupamt,
                                                   "status"  => "Y",
                                                   "comment" => "Top up",
                                                   "status"  =>"Credit",
                                                   "user_id" =>1,
                                                   "added_by" => 1,
                                                   "amt_before_trans"=> $wallet_data[0]['amt'],
                                                   "amt_after_trans"=> $wallet_data[0]['amt']+$topupamt,
                                                   "added_on" => date('Y-m-d H:i:s', time())
                                                );

                            $this->wallet_trans_model->insert($insert_wallet_trans);
                         }                                               
                    }
                    else
                    {
                        $updated_balance=$topupamt;
                        $insert_arr=array("user_id"  => $user[0]->user_id,
                                            "amt"      => $topupamt,
                                            "status"  => "Y",
                                            "comment"  => "Credit",
                                            "added_by" => $this->user->id,
                                            "added_on" => date('Y-m-d H:i:s', time()));
                        $wallet_insert_id = $this->wallet_model->insert($insert_arr);

                        if($wallet_insert_id)
                        {  
                            $insert_wallet_trans = array("w_id"   => $wallet_insert_id,
                                                       "amt"      => $topupamt,
                                                       "status"   => "Y",
                                                       "comment"  => "Top up",
                                                       "status"   =>"Credit",
                                                       "user_id"  =>1,
                                                       "added_by" => 1,
                                                       "amt_before_trans"=>0,
                                                       "amt_after_trans"=> $wallet_data[0]['amt']+$topupamt,
                                                       "added_on" => date('Y-m-d H:i:s', time()));
                            
                            $this->wallet_trans_model->insert($insert_wallet_trans);

                        }                                                                                
                    }
                    $this->response(array('msg'=>'Agent Balance updated successfully!','balance' =>$updated_balance, 'status' => 'success'),200);                             
                }
                else
                {
                       $this->response(array('error'=>'This agent is not authorized,so contact to service provoder'),200);                             
                }                                                      
            }
        }
        else 
        { 
             $this->response(array('error'=>'Please provide proper data'),200);                             
        }                      
    }
    
    
    
    
    
    
    
    
    
    /*
     * Agents Wallet Balance
     */
    
    function walletBalance_post() 
    {
        $input = $this->post();
        $config = array(
                    array('field'=>'agent_code', 'label'=>'Agent Code', 'rules'=>'required', 'errors'=>array('required'=>'Agent Code is required')),
                    );
         if (form_validate_rules($config) == FALSE) 
        {
            $error = $this->form_validation->error_array();
            $this->response(array($error, 200));

        } 
        else 
        { 
            $agentData = $this->agent_model->where(array('agent_code'=> $input['agent_code'], 'is_status'=>'1', 'is_deleted'=>'0'))->find_all();
            if($agentData) {
                $user_id = $agentData[0]->user_id;
                $agentWalletData = $this->agent_model->getAgentBalance($user_id);
                if($agentWalletData) {
                    $this->response(array('data' => $agentWalletData, 'status' => 'success'),200);
                } else {
                    $this->response(array('error'=> "Data not found"),200);
                }
            } else {
                $this->response(array('error'=> "Agent with given agent code is not available"));
            }
        }
    }
    
    /*
     *  Agent ETIM  Balance
     */
    
    function eitmBalance_post() 
    {
        $this->load->model(array('wallet_model','etim_model','booking_agent_model'));
        $input      = $this->post();
        $config     = array(
                            array('field'=>'agent_code', 'label'=>'Agent Code', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Enter Agent code.', 'numeric' => 'Agent Code should be numbers')),
                            array('field'=>'etim_seriale_no', 'label'=>'Serial Number', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Enter ETIM Serial Number.')),
                        );
        
        if (form_validate_rules($config) == FALSE) 
        {            
            $error = $this->form_validation->error_array();
            $this->response(array($error, 200));
            
        }
        else 
        {
            //check if the boss code is available in table            
            // $agentUserId = $this->booking_agent_model->check_agent_code_available($input['agent_code']);
            $agent = $this->agent_model->where(array('agent_code'=> $input['agent_code'], 'is_status'=>'1', 'is_deleted'=>'0'))->find_all();
            
            if($agent) 
            {
                // Check if ETIM serial number is available in table

                $agent_id    = $agent[0]->user_id;
                $etimIdData  = $this->etim_model->check_etim_serial_no($input['etim_seriale_no']);

                if($etimIdData) {

                    //check if agent id is available in wallet table

                    $agentAvailability  = $this->wallet_model->check_agent_available($agent_id);

                    if($agentAvailability) {
                        // Etim Balance
                        $etim_balance = $this->etim_model->get_etim_balance($agentAvailability);

                        $this->response(array('data'=>$etim_balance, 'status' => 'success'), 200);
                    } else {

                        $this->response(array('error'=>'Agent dont have amount in wallet or is not active agent or amount is zero'),200);

                    }
                } 
                else
                {

                    $this->response(array('error'=>'ETIM with given serial number does not available'),200);
                }   
            }
            else 
            {            
                $this->response(array('error'=>'Agent with given agent code is not available'),200);
            }  
        }
        
    }
    

    function hhmTransactionUpdate_post()
    {
        $this->load->model(array('waybill_amt_transaction_model'));
        // show($this->post(),1);
        $config     = array(
                            array('field'=>'agent_code', 'label'=>'Agent Code', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Enter Agent code.', 'numeric' => 'Agent Code should be numbers')),
                            array('field'=>'wayBillId', 'label'=>'Way Bill Id', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Enter Way bill id.')),
                            array('field'=>'wayBillAllocationDate', 'label'=>'Allocation date', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please provide Allocation.')),
                            array('field'=>'openingBalance', 'label'=>'Opening Balance', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please provide Opening Balance')),
                            array('field'=>'wayBillCollectionDate', 'label'=>'Waybill Collection date', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please provide waybill collection date')),
                            array('field'=>'noOfTickets', 'label'=>'Number of Tickets', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please provide Number of Tickets.')),
                            array('field'=>'consumedamount', 'label'=>'Consumed amount', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please provide Consumed amount')),
                            array('field'=>'remainingBalance', 'label'=>'Remaining amount', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please provide remaining amount')),
                        );
                        

        
        if (form_validate_rules($config) == FALSE) 
        {            
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
            
        }
        else 
        {
            $input = $this->post();
            $this->load->model(array('agent_hhm_trans_model', 'waybill_no_model','waybill_amt_transaction_model'));

            $way_bill_data = $this->waybill_no_model->column('op.op_id as op_id')
                                ->where(
                                    array(
                                            'waybillprogramming.WAYBILL_NO' => $input['wayBillId'], 
                                            'waybillprogramming.WAYBILL_STATUS !=' => 'CLOSED'
                                        ) 
                                )
                                ->join('op_master op', 'waybillprogramming.OP_ID = op.op_name' )
                                ->find_all();


            if($way_bill_data)
            {

                $agent = $this->agent_model->column('agent_master.*, users.username, user_roles.role_name')
                                            ->where(array(
                                                        'agent_code' => $input['agent_code']
                                                        )
                                                    )
                                            ->where('v_agent_code !=','')
                                            ->where('user_roles.role_name ','Vendor')
                                            ->join('users', 'agent_master.created_by = users.id')
                                            ->join('user_roles', 'user_roles.id = users.role_id')
                                            ->find_all();

               // show($agent,1);
                if($agent)
                {
                    $insert_arr = array(  
                                            "agent_id"            => $agent[0]->agent_id,
                                            "way_bill_id"         => $input['wayBillId'],
                                            "allocation_date"     => $input['wayBillAllocationDate'],
                                         // "opening_balance"     => $input['openingBalance'],
                                            "collection_date"     => $input['wayBillCollectionDate'],
                                            "no_of_tickets"       => $input['noOfTickets'],
                                            "consumed_amount"     => $input['consumedamount'],

                                            "remaining_balance"   => $input['remainingBalance'],
											// "remaining_balance"   => 0,
					

						
                                        );
                    $coll_data['waybill_no']='';
                    $id = $this->agent_hhm_trans_model->insert($insert_arr);
			 $colle_transaction = array('hhm_consumed_amt' => $input['consumedamount'],
                                                    'hhm_remaining_amt' => $input['remainingBalance'],
						    'updated_date'=>date('Y-m-d h:m:s'),
							'collection_by'=>$agent[0]->agent_id,
							'collection_date'=>date('Y-m-d h:m:s'),
                                                        );
		
		    $waybill_amt_transa_id = $this->waybill_amt_transaction_model->update_where('waybill_no',$input['wayBillId'],$colle_transaction );
   			log_data('rest/agent/'.date('M').'/'.date('d-m-Y').'/request.log', 'waybill_amt_transa_id'. $waybill_amt_transa_id.last_query());
log_data('rest/agent/'.date('M').'/'.date('d-m-Y').'/request.log', 'waybill_amt_query'. last_query());
log_data('rest/agent/'.date('M').'/'.date('d-m-Y').'/request.log', 'waybill_imp_transa_id'.$input['wayBillId']);
                    if($id)
                    {

                        if($agent[0]->role_name = 'Vendor')
                        {
                            $this->load->model('vendor_model');

                           $vendor = $this->vendor_model->where(array('user_id' => $agent[0]->created_by) )->find_all();

                            if($vendor)
                            {
                                $api_data = array(  
                                        "agent_code"          => $input['agent_code'],
                                        "way_bill_id"         => $input['wayBillId'],
                                        "allocation_date"     => $input['wayBillAllocationDate'],
                                        "opening_balance"     => $input['openingBalance'],
                                        "collection_date"     => $input['wayBillCollectionDate'],
                                        "no_of_tickets"       => $input['noOfTickets'],
                                        "consumed_amount"     => $input['consumedamount'],
                                        "remaining_balance"   => $input['remainingBalance']
                                        );
                                $api = $vendor[0]->library.'_api';
                                $this->load->library('vendor/'.$api);


                                $api_res = $this->{$api}->hhmTransactionUpdate($api_data);
                                log_data('rest/agent/'.date('M').'/'.date('d-m-Y').'/request.log', 'hhmTransactionUpdate data'. $api_res['return']);
                                if($api_res['return'] == 'Fail')
                                {
                                    $this->response(array( 
                                                        'status'        => 'failed', 
                                                        'apiResponse'   => $api_res, 
                                                ),200);
                                }
                                else
                                {
                                    /*if($input['consumedamount'] > 0)
                                    {*/
                                        $id = $this->agent_hhm_trans_model->update($id, array('status' => 1) );

                                        // operator wallet transaction
                                        $op_wallet_data =  array(
                                                        'service_id'    => 2, // 2 for current booking
                                                      //  'amount'        => $input['consumedamount'],
                                                        'amount'        => $input['remainingBalance'],
                                                        //'comment'       => 'Balance used by Agent '.$agent[0]->fname.' '. $agent[0]->lname.'('.$agent[0]->agent_code.'|'.$agent[0]->v_agent_code.')' ,
                                                        //'comment'       => 'Remaining Balance credit to  Agent '.$agent[0]->fname.' '. $agent[0]->lname.'('.$agent[0]->agent_code.'|'.$agent[0]->v_agent_code.')' ,
                                                         'comment'       => 'Closing balance of Agent '.$agent[0]->fname.' '.'('.$agent[0]->agent_code.') in ETIM' ,
                                                        'transfer'      => $agent[0]->agent_id
                                                     );
                                        
                                        
                                    //    $this->op_wallet_model->do_trans($way_bill_data[0]->op_id, 'o', $op_wallet_data, 'credit',  $this->user->id);
                                 //   }
                                    

                                    // Agent wallet credit transaction (add)
                                    $wallet_data =  array(
                                                    'service_id'    => 2, // 2 for current booking
                                                    'amount'        => $input['openingBalance'],
                                                    'comment'       => 'Amount Received from ETIM Machine'
                                                );

                                    // agent wallet transaction
                                    $this->op_wallet_model->do_trans($agent[0]->agent_id, 'a', $wallet_data, 'credit',  $this->user->id);
                                    

                                    // Agent wallet debit transaction  (deduct) 
                                    $wallet_data =  array(
                                                    'service_id'    => 2, // 2 for current booking
                                                    'amount'        => $input['consumedamount'],
     						 					//'amount'        => 0,
                                                    'comment'       => 'Remaining Balance From Etim Api HHM Trasaction updated'
                                                );

                                    // agent wallet transaction
                                    $this->op_wallet_model->do_trans_debit($agent[0]->agent_id, 'a', $wallet_data, 'debit',  $this->user->id);

                                    /*
                                    // Agent wallet credit transaction (add)
                                    $wallet_data =  array(
                                                    'service_id'    => 2, // 2 for current booking
                                                    'amount'        => $input['remainingBalance'],
                                                    'comment'       => 'Remaining Balance From Etim Api HHM Trasaction updated'
                                                );

                                    // agent wallet transaction
                                    $this->op_wallet_model->do_trans($agent[0]->user_id, $wallet_data, 'credit',  $this->user->id);
                                    */
                                    
                                    
                                    $this->response(array( 
                                                            'status'        => 'success', 
                                                            'apiResponse'   => $api_res, 
                                                            'msg'         => 'HHM Trasaction inserted successfully'
                                                        ),200);
                                }

                            }

                        }
                        
                        $this->response(array( 'status' => 'success', 'error'=>'HHM Trasaction inserted successfully'),200);
                    }
                    else
                    {
                        $this->response(array( 'status' => 'failed', 'error'=>'Error occurred Please try again'),200);
                    }
                }
                else
                {
                    $this->response(array( 'status' => 'failed', 'error'=>'No agent found or invalid agent code'),200);
                }
            }
            else
            {
                $this->response(array( 'status' => 'failed', 'error'=>'Invalid waybill or waybill has been closed'),200);
            }
        }
    }
    
    /*
     * Add Agents Seller Point API
     */
    function add_agent_seller_points_post() {
        $config     = array(
                            array('field'=>'agent_code', 'label'=>'Agent Code', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Enter Agent code.', 'numeric' => 'Agent Code should be numbers')),
                        );
                        
        if (form_validate_rules($config) == FALSE) 
        {            
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
            
        }
        else 
        {
            $input = $this->post();
            $this->load->model(array('agent_hhm_trans_model'));

            $agent = $this->agent_model->column('agent_master.*, users.username, user_roles.role_name')
                                        ->where(array(
                                                    'agent_code' => $input['agent_code']
                                                    )
                                                )
                                        ->join('users', 'agent_master.created_by = users.id')
                                        ->join('user_roles', 'user_roles.id = users.role_id')
                                        ->find_all();
            
            if($agent)
            {

                if($agent[0]->role_name = 'Vendor')
                {
                    $this->load->model('vendor_model');

                   $vendor = $this->vendor_model->where(array('user_id' => $agent[0]->created_by) )->find_all();

                    if($vendor)
                    {
                        $api_data = array(  
                                "agent_code"          => $input['agent_code'],
                                );
                        $api = $vendor[0]->library.'_api';
                        $this->load->library('vendor/'.$api);

                        $api_res = $this->{$api}->addAgentSellerPoint($api_data);
                        show($api_res,1);

                    }

                }
               
            }
            else
            {
                $this->response(array( 'status' => 'failed', 'error'=>'No agent found or invalid agent code'),200);
            }
        }
    }
    
    /*
     * Add Agents Way Bill Master APi
     */
    function add_agent_way_bill_master_post() {
        $config     = array(
                            array('field'=>'agent_code', 'label'=>'Agent Code', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Enter Agent code.', 'numeric' => 'Agent Code should be numbers')),
                        );
                        
        if (form_validate_rules($config) == FALSE) 
        {            
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
            
        }
        else 
        {
            $input = $this->post();
            $this->load->model(array('agent_hhm_trans_model'));

            $agent = $this->agent_model->column('agent_master.*, users.username, user_roles.role_name')
                                        ->where(array(
                                                    'agent_code' => $input['agent_code']
                                                    )
                                                )
                                        ->join('users', 'agent_master.created_by = users.id')
                                        ->join('user_roles', 'user_roles.id = users.role_id')
                                        ->find_all();
            
            if($agent)
            {

                if($agent[0]->role_name = 'Vendor')
                {
                    $this->load->model('vendor_model');

                   $vendor = $this->vendor_model->where(array('user_id' => $agent[0]->created_by) )->find_all();

                    if($vendor)
                    {
                        $api_data = array(  
                                "agent_code"          => $input['agent_code'],
                                );
                        $api = $vendor[0]->library.'_api';
                        $this->load->library('vendor/'.$api);

                        $api_res = $this->{$api}->addAgentWayBillMaster($api_data);
                        show($api_res,1);

                    }

                }
               
            }
            else
            {
                $this->response(array( 'status' => 'failed', 'error'=>'No agent found or invalid agent code'),200);
            }
        }
    }
    
    /*
     *Balance Transfer Approval API 
     */
    
    function call_balance_transfer_approve_post() {
        $config     = array(
                            array('field'=>'agent_code', 'label'=>'Agent Code', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Enter Agent code.', 'numeric' => 'Agent Code should be numbers')),
                        );
                        
        if (form_validate_rules($config) == FALSE) 
        {            
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
            
        }
        else 
        {
            $input = $this->post();
            $this->load->model(array('agent_hhm_trans_model'));

            $agent = $this->agent_model->column('agent_master.*, users.username, user_roles.role_name')
                                        ->where(array(
                                                    'agent_code' => $input['agent_code']
                                                    )
                                                )
                                        ->join('users', 'agent_master.created_by = users.id')
                                        ->join('user_roles', 'user_roles.id = users.role_id')
                                        ->find_all();
            
            if($agent)
            {

                if($agent[0]->role_name = 'Vendor')
                {
                    $this->load->model('vendor_model');

                   $vendor = $this->vendor_model->where(array('user_id' => $agent[0]->created_by) )->find_all();

                    if($vendor)
                    {
                        $api_data = array(  
                                "agent_code"          => $input['agent_code'],
                                );
                        $api = $vendor[0]->library.'_api';
                        $this->load->library('vendor/'.$api);

                        $api_res = $this->{$api}->callBalanceTransferApprove($api_data);
                        show($api_res,1);

                    }

                }
               
            }
            else
            {
                $this->response(array( 'status' => 'failed', 'error'=>'No agent found or invalid agent code'),200);
            }
        }
    }
    
    /*
     *Balance Transfer Approval API 
     */
    
    function call_topup_balance_post() {
        $config     = array(
                            array('field'=>'agent_code', 'label'=>'Agent Code', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Enter Agent code.', 'numeric' => 'Agent Code should be numbers')),
                            array('field'=>'way_bill_no', 'label'=>'Way bill', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Enter WayBill No.', 'numeric' => 'Provide waybill no')),
                            array('field'=>'balance', 'label'=>'Balance', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Enter topup amount.', 'numeric' => 'Provide amount')),
                            array('field'=>'remaining_balance', 'label'=>'Remaining Balance', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Enter Remaining Balance.', 'numeric' => 'Provide amount')),
                        );
                        
        if (form_validate_rules($config) == FALSE) 
        {            
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
            
        }
        else 
        {
            $input = $this->post();
            $this->load->model(array('agent_hhm_trans_model','waybill_no_model'));

            $agent = $this->agent_model->column('agent_master.*, users.username, user_roles.role_name')
                                        ->where(array(
                                                        'agent_code' => $input['agent_code'],
                                                        'v_agent_code !=' => '',
                                                    )
                                                )
                                        ->join('users', 'agent_master.created_by = users.id')
                                        ->join('user_roles', 'user_roles.id = users.role_id')
                                        ->find_all();
              $way_bill_data = $this->waybill_no_model->column('op.op_id as op_id')
                                ->where(
                                    array(
                                            'waybillprogramming.WAYBILL_NO' => $input['way_bill_no'], 
                                            'waybillprogramming.WAYBILL_STATUS =' => 'OPEN'
                                        ) 
                                )
                                ->join('op_master op', 'waybillprogramming.OP_ID = op.op_name' )
                                ->find_all();
                log_data('rest/agent/'.date('M').'/'.date('d-m-Y').'/request.log', 'waybill data'. last_query());
            log_data('rest/agent/'.date('M').'/'.date('d-m-Y').'/request.log', 'waybill data'.$way_bill_data[0]->op_id.$way_bill_data[0]->op_id.'waybill no.'.$input['way_bill_no']);
       /*     
            if($way_bill_data)
            {
                  $op_wallet_data =  array(
                                                        'service_id'    => 2, // 2 for current booking
                                                        'amount'        => $input['balance'],
                                                        //'comment'       => 'Balance used by Agent '.$agent[0]->fname.' '. $agent[0]->lname.'('.$agent[0]->agent_code.'|'.$agent[0]->v_agent_code.')' ,
                                                        'comment'       => 'Amount allocated to ETIM by Agent '.$agent[0]->fname.' '. $agent[0]->lname.'('.$agent[0]->agent_code.') waybill number '.$input['way_bill_no'] ,
                                                        'transfer'      => $agent[0]->agent_id
                                                     );
                                        
                                        
                  $this->op_wallet_model->do_trans($way_bill_data[0]->op_id, 'o', $op_wallet_data, 'debit',  $this->user->id);
            }*/
            
            
            if($agent)
            {

                if($agent[0]->role_name = 'Vendor')
                {
                    $this->load->model('vendor_model');

                   $vendor = $this->vendor_model->where(array('user_id' => $agent[0]->created_by) )->find_all();

                    if($vendor)
                    {
                        $api_data = array(  
                                "agent_code"       => $input['agent_code'],
                                "way_bill_no"      => $input['way_bill_no'],
                                "balance"          => $input['balance'],
                                );
                        $api = $vendor[0]->library.'_api';
                        $this->load->library('vendor/'.$api);

                        $api_res = $this->{$api}->callTopupBalance($api_data);

                        if( $api_res['return'] == 'SUCCESS'){
				


				if($way_bill_data)
            {
                  $op_wallet_data =  array(
                                            'service_id'    => 2, // 2 for current booking
                                            'amount'        => $input['balance'],
                //'comment'       => 'Balance used by Agent '.$agent[0]->fname.' '. $agent[0]->lname.'('.$agent[0]->agent_code.'|'.$agent[0]->v_agent_code.')' ,
                                             'comment'       => 'Amount allocated to ETIM by Agent '.$agent[0]->fname.' '. $agent[0]->lname.'('.$agent[0]->agent_code.') waybill number '.$input['way_bill_no'] ,
                                             'transfer'      => $agent[0]->agent_id
                                                     );
                                        
                                        
                  $this->op_wallet_model->do_trans_debit($way_bill_data[0]->op_id, 'o', $op_wallet_data, 'debit',  $this->user->id);
            }
                            // for Topup
                            $wallet_data =  array(
                                                'service_id'    => 2, // 2 for current booking
                                                'amount'        => $input['balance'],
                                                'comment'       => 'Api Top up'
                                            );
                            $this->op_wallet_model->do_trans($agent[0]->agent_id, 'a', $wallet_data, 'credit',  $this->user->id);

                            // for Etim Transfer
                            $wallet_data['comment']  = 'Api Transfer to ETIM Device';
                            $this->op_wallet_model->do_trans_debit($agent[0]->agent_id, 'a', $wallet_data, 'debit',  $this->user->id);

                            // for remaining balance
             //               $wallet_data['amount']      = 0;
							$wallet_data['amount']      = $input['remaining_balance'];
                            $wallet_data['comment']     = 'Remaining balance transfer to ETIM';

                            $this->op_wallet_model->do_trans_debit($agent[0]->agent_id, 'a', $wallet_data, 'debit',  $this->user->id);

                            $this->response(array( 'status' => 'success', 'apiResponse'=> $api_res),200);   //Uncomment this line
                        }
                        else
                        {
                            $this->response(array( 'status' => 'failed', 'apiResponse'=> $api_res),200);
                        }
                    }
                    else
                    {
                        $this->response(array( 'status' => 'failed', 'msg' => 'vendor not found or not exists' ),200);
                    }
                }
                else
                {
                    $this->response(array( 'status' => 'failed', 'msg' => 'Not a vendors agent | Vendor not exists' ),200);
                }
               
            }
            else
            {
                $this->response(array( 'status' => 'failed', 'error'=>'No agent found or invalid agent code'),200);
            }
        }
    }
    
    /*
     *Create Retailer API 
     */
    
    function create_retailer_post() {
        $config     = array(
                            array('field'=>'agent_code', 'label'=>'Agent Code', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Enter Agent code.', 'numeric' => 'Agent Code should be numbers')),
                        );
                        
        if (form_validate_rules($config) == FALSE) 
        {            
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
            
        }
        else 
        {
            $input = $this->post();
            $this->load->model(array('agent_hhm_trans_model'));

            $agent = $this->agent_model->column('agent_master.*, users.username, user_roles.role_name')
                                        ->where(array(
                                                    'agent_code' => $input['agent_code']
                                                    )
                                                )
                                        ->join('users', 'agent_master.created_by = users.id')
                                        ->join('user_roles', 'user_roles.id = users.role_id')
                                        ->find_all();
            
            if($agent)
            {

                if($agent[0]->role_name = 'Vendor')
                {
                    $this->load->model('vendor_model');

                   $vendor = $this->vendor_model->where(array('user_id' => $agent[0]->created_by) )->find_all();

                    if($vendor)
                    {
                        $api_data = array(  
                                "agent_code"          => $input['agent_code'],
                                );
                        $api = $vendor[0]->library.'_api';
                        $this->load->library('vendor/'.$api);

                        $api_res = $this->{$api}->createRetailer($api_data);
                        show($api_res,1);

                    }

                }
               
            }
            else
            {
                $this->response(array( 'status' => 'failed', 'error'=>'No agent found or invalid agent code'),200);
            }
        }
    }
    /*
     *Delete Agent Seller point API 
     */
    
    function delete_agent_seller_point_post() {
        $config     = array(
                            array('field'=>'agent_code', 'label'=>'Agent Code', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Enter Agent code.', 'numeric' => 'Agent Code should be numbers')),
                        );
                        
        if (form_validate_rules($config) == FALSE) 
        {            
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
            
        }
        else 
        {
            $input = $this->post();
            $this->load->model(array('agent_hhm_trans_model'));

            $agent = $this->agent_model->column('agent_master.*, users.username, user_roles.role_name')
                                        ->where(array(
                                                    'agent_code' => $input['agent_code']
                                                    )
                                                )
                                        ->join('users', 'agent_master.created_by = users.id')
                                        ->join('user_roles', 'user_roles.id = users.role_id')
                                        ->find_all();
            
            if($agent)
            {

                if($agent[0]->role_name = 'Vendor')
                {
                    $this->load->model('vendor_model');

                   $vendor = $this->vendor_model->where(array('user_id' => $agent[0]->created_by) )->find_all();

                    if($vendor)
                    {
                        $api_data = array(  
                                "agent_code"          => $input['agent_code'],
                                );
                        $api = $vendor[0]->library.'_api';
                        $this->load->library('vendor/'.$api);

                        $api_res = $this->{$api}->deleteAgentSellerPoint($api_data);
                        show($api_res,1);

                    }

                }
               
            }
            else
            {
                $this->response(array( 'status' => 'failed', 'error'=>'No agent found or invalid agent code'),200);
            }
        }
    }
    /*
     *Delete Agent Waybill Master API
     */
    
    function delete_agent_waybill_master_post() {
        $config     = array(
                            array('field'=>'agent_code', 'layybel'=>'Agent Code', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Enter Agent code.', 'numeric' => 'Agent Code should be numbers')),
                        );
                        
        if (form_validate_rules($config) == FALSE) 
        {            
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
            
        }
        else 
        {
            $input = $this->post();
            $this->load->model(array('agent_hhm_trans_model'));

            $agent = $this->agent_model->column('agent_master.*, users.username, user_roles.role_name')
                                        ->where(array(
                                                    'agent_code' => $input['agent_code']
                                                    )
                                                )
                                        ->join('users', 'agent_master.created_by = users.id')
                                        ->join('user_roles', 'user_roles.id = users.role_id')
                                        ->find_all();
            
            if($agent)
            {

                if($agent[0]->role_name = 'Vendor')
                {
                    $this->load->model('vendor_model');

                   $vendor = $this->vendor_model->where(array('user_id' => $agent[0]->created_by) )->find_all();

                    if($vendor)
                    {
                        $api_data = array(  
                                "agent_code"          => $input['agent_code'],
                                );
                        $api = $vendor[0]->library.'_api';
                        $this->load->library('vendor/'.$api);

                        $api_res = $this->{$api}->deleteAgentWayBillMaster($api_data);
                        show($api_res,1);

                    }

                }
               
            }
            else
            {
                $this->response(array( 'status' => 'failed', 'error'=>'No agent found or invalid agent code'),200);
            }
        }
    }
    /*
     *Edit using Boss
     */
    
    function edit_using_bos_post() {
        $config     = array(
                            array('field'=>'agent_code', 'label'=>'Agent Code', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Enter Agent code.', 'numeric' => 'Agent Code should be numbers')),
                        );
                        
        if (form_validate_rules($config) == FALSE) 
        {            
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
            
        }
        else 
        {
            $input = $this->post();
            $this->load->model(array('agent_hhm_trans_model'));

            $agent = $this->agent_model->column('agent_master.*, users.username, user_roles.role_name')
                                        ->where(array(
                                                    'agent_code' => $input['agent_code']
                                                    )
                                                )
                                        ->join('users', 'agent_master.created_by = users.id')
                                        ->join('user_roles', 'user_roles.id = users.role_id')
                                        ->find_all();
            
            if($agent)
            {

                if($agent[0]->role_name = 'Vendor')
                {
                    $this->load->model('vendor_model');

                   $vendor = $this->vendor_model->where(array('user_id' => $agent[0]->created_by) )->find_all();

                    if($vendor)
                    {
                        $api_data = array(  
                                "agent_code"          => $input['agent_code'],
                                );
                        $api = $vendor[0]->library.'_api';
                        $this->load->library('vendor/'.$api);

                        $api_res = $this->{$api}->editUsingBos($api_data);
                        show($api_res,1);

                    }

                }
               
            }
            else
            {
                $this->response(array( 'status' => 'failed', 'error'=>'No agent found or invalid agent code'),200);
            }
        }
    }
    
    
    /*
     * Get newly Added Agent
     */
    
    function get_newly_added_agent_post() {
        $config     = array(
                            array('field'=>'agent_code', 'label'=>'Agent Code', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Enter Agent code.', 'numeric' => 'Agent Code should be numbers')),
                        );
                        
        if (form_validate_rules($config) == FALSE) 
        {            
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
            
        }
        else 
        {
            $input = $this->post();
            $this->load->model(array('agent_hhm_trans_model'));

            $agent = $this->agent_model->column('agent_master.*, users.username, user_roles.role_name')
                                        ->where(array(
                                                    'agent_code' => $input['agent_code']
                                                    )
                                                )
                                        ->join('users', 'agent_master.created_by = users.id')
                                        ->join('user_roles', 'user_roles.id = users.role_id')
                                        ->find_all();
            
            if($agent)
            {

                if($agent[0]->role_name = 'Vendor')
                {
                    $this->load->model('vendor_model');

                   $vendor = $this->vendor_model->where(array('user_id' => $agent[0]->created_by) )->find_all();

                    if($vendor)
                    {
                        $api_data = array(  
                                "agent_code"          => $input['agent_code'],
                                );
                        $api = $vendor[0]->library.'_api';
                        $this->load->library('vendor/'.$api);

                        $api_res = $this->{$api}->getNewlyAddedAgent($api_data);
                        show($api_res,1);

                    }

                }
               
            }
            else
            {
                $this->response(array( 'status' => 'failed', 'error'=>'No agent found or invalid agent code'),200);
            }
        }
    }
    
    
    /*
     * Update Agent Seller Point
     */
    
    function update_agent_seller_point_post() {
        $config     = array(
                            array('field'=>'agent_code', 'label'=>'Agent Code', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Enter Agent code.', 'numeric' => 'Agent Code should be numbers')),
                        );
                        
        if (form_validate_rules($config) == FALSE) 
        {            
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
            
        }
        else 
        {
            $input = $this->post();
            $this->load->model(array('agent_hhm_trans_model'));

            $agent = $this->agent_model->column('agent_master.*, users.username, user_roles.role_name')
                                        ->where(array(
                                                    'agent_code' => $input['agent_code']
                                                    )
                                                )
                                        ->join('users', 'agent_master.created_by = users.id')
                                        ->join('user_roles', 'user_roles.id = users.role_id')
                                        ->find_all();
            
            if($agent)
            {

                if($agent[0]->role_name = 'Vendor')
                {
                    $this->load->model('vendor_model');

                   $vendor = $this->vendor_model->where(array('user_id' => $agent[0]->created_by) )->find_all();

                    if($vendor)
                    {
                        $api_data = array(  
                                "agent_code"          => $input['agent_code'],
                                );
                        $api = $vendor[0]->library.'_api';
                        $this->load->library('vendor/'.$api);

                        $api_res = $this->{$api}->updateAgentSellerPoint($api_data);
                        show($api_res,1);

                    }

                }
               
            }
            else
            {
                $this->response(array( 'status' => 'failed', 'error'=>'No agent found or invalid agent code'),200);
            }
        }
    }
    
    
    /*
     * Update Agent waybill Master
     */
    
    function update_agent_waybill_master_post() {
        $config     = array(
                            array('field'=>'agent_code', 'label'=>'Agent Code', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Enter Agent code.', 'numeric' => 'Agent Code should be numbers')),
                        );
                        
        if (form_validate_rules($config) == FALSE) 
        {            
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
            
        }
        else 
        {
            $input = $this->post();
            $this->load->model(array('agent_hhm_trans_model'));

            $agent = $this->agent_model->column('agent_master.*, users.username, user_roles.role_name')
                                        ->where(array(
                                                    'agent_code' => $input['agent_code']
                                                    )
                                                )
                                        ->join('users', 'agent_master.created_by = users.id')
                                        ->join('user_roles', 'user_roles.id = users.role_id')
                                        ->find_all();
            
            if($agent)
            {

                if($agent[0]->role_name = 'Vendor')
                {
                    $this->load->model('vendor_model');

                   $vendor = $this->vendor_model->where(array('user_id' => $agent[0]->created_by) )->find_all();

                    if($vendor)
                    {
                        $api_data = array(  
                                "agent_code"          => $input['agent_code'],
                                );
                        $api = $vendor[0]->library.'_api';
                        $this->load->library('vendor/'.$api);

                        $api_res = $this->{$api}->updateAgentWayBillMaster($api_data);
                        show($api_res,1);

                    }

                }
               
            }
            else
            {
                $this->response(array( 'status' => 'failed', 'error'=>'No agent found or invalid agent code'),200);
            }
        }
    }
    
    
    /*
     * Update Topup Limit
     */
    
    function update_topup_limit_post() {
        $config     = array(
                            array('field'=>'agent_code', 'label'=>'Agent Code', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Enter Agent code.', 'numeric' => 'Agent Code should be numbers')),
                        );
                        
        if (form_validate_rules($config) == FALSE) 
        {            
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
            
        }
        else 
        {
            $input = $this->post();
            $this->load->model(array('agent_hhm_trans_model'));

            $agent = $this->agent_model->column('agent_master.*, users.username, user_roles.role_name')
                                        ->where(array(
                                                    'agent_code' => $input['agent_code']
                                                    )
                                                )
                                        ->join('users', 'agent_master.created_by = users.id')
                                        ->join('user_roles', 'user_roles.id = users.role_id')
                                        ->find_all();
            
            if($agent)
            {

                if($agent[0]->role_name = 'Vendor')
                {
                    $this->load->model('vendor_model');

                   $vendor = $this->vendor_model->where(array('user_id' => $agent[0]->created_by) )->find_all();

                    if($vendor)
                    {
                        $api_data = array(  
                                "agent_code"          => $input['agent_code'],
                                );
                        $api = $vendor[0]->library.'_api';
                        $this->load->library('vendor/'.$api);

                        $api_res = $this->{$api}->updateTopuplimit($api_data);
                        show($api_res,1);

                    }

                }
               
            }
            else
            {
                $this->response(array( 'status' => 'failed', 'error'=>'No agent found or invalid agent code'),200);
            }
        }
    }
    
    /*
     * function to encryption 
     */
    
    function fnEncrypt_post() {
        $config     = array(
                            array('field'=>'plain_text', 'label'=>'Plain Text', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Enter Plain text for ecryption.')),
                        );
                        
        if (form_validate_rules($config) == FALSE) 
        {            
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
            
        }
        else 
        {
           $this->load->model('agent_hhm_trans_model') ;
           $sValue = $this->input->post('plain_text');
           
           $plainArr = convert_str_to_array($sValue, '###');
           $agent_code = $plainArr['agent_code'];

           if($agent_code){
               
               $agent = $this->agent_model
                                            ->where(array('agent_code' => $agent_code, 'kyc'=>'Y', 'is_status'=> '1'))
                                            ->find_all();

               if($agent)
               {
                    $wallet = $this->op_wallet_model->where('service_id', 2 )
                                                    ->where('user_id', $agent[0]->agent_id)
                                                    ->where('user_type', 'a')
                                                    ->find_all();
                   
                    $remaining_balance = 0.00;
                    if($wallet){
                        $remaining_balance =  $wallet[0]->amount;
//			    		$remaining_balance =  0;
                    }

                    $plain_text = $sValue."###remaining_balance=".$remaining_balance."###acb=abc";

                    // log_data('rest/encryption_'.date('d-m-Y').'.log', 'Plain Text -------> '.$plain_text);
                    log_data('rest/agent/'.date('M').'/'.date('d-m-Y').'/encryption.log', 'Plain Text -------> '.$plain_text);
                    log_data('rest/agent/'.date('M').'/'.date('d-m-Y').'/api'.'.log', $plain_text, 'fnEncrypt Plain text');

                    $retval = encrypt($plain_text,$this->sSecretKey);

                    // log_data('rest/encryption_'.date('d-m-Y').'.log', 'Encrypted_text -------> '.$retval);
                    log_data('rest/agent/'.date('M').'/'.date('d-m-Y').'/encryption.log', 'Encrypted_text -------> '.$retval);
                    log_data('rest/agent/'.date('M').'/'.date('d-m-Y').'/api'.'.log', $retval, 'encryption text');

                    $this->response(array('status' => 'success', 'encrypted_string'=>$retval),200);
                   
               }
               else
               {
                   $this->response(array('status'=>'failed', 'error'=>'Agent is not available. Or KYC is not approved or Agent is not active.'),200);
               }
           }
           else
           {
               $this->response(array('status'=>'failed', 'error'=>'Please provide agent code'),200);
           }
           
        }
    }

    
    /*
     * function to encryption 
     */
    
    function fnDecrypt_post() {
        $config = array(
                            array('field'=>'encrypted_text', 'label'=>'Encrypted Text', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Enter Encrypted text for decryption.')),
                        );
                        
        if (form_validate_rules($config) == FALSE) 
        {            
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
            
        }
        else 
        {
           $encrypted_string = $this->input->post('encrypted_text');
           
           // log_data('rest/decryption_'.date('d-m-Y').'.log', 'Encrypted Text -------> '.$encrypted_string);
           log_data('rest/agent/'.date('M').'/'.date('d-m-Y').'/decryption.log', 'Encrypted Text -------> '.$encrypted_string);
           log_data('rest/agent/'.date('M').'/'.date('d-m-Y').'/api'.'.log', $encrypted_string, 'before decryption');

           // $decrypted_string = urldecode(trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->sSecretKey, base64_decode($encrypted_string), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
           
           $decrypted_string = trim(decrypt($encrypted_string,$this->sSecretKey));
           // $decrypted_string = trim(base64_decode($encrypted_string));

           // log_data('rest/decryption_'.date('d-m-Y').'.log', 'Decrypted Text -------> '.$decrypted_string);
           log_data('rest/agent/'.date('M').'/'.date('d-m-Y').'/decryption.log', 'Decrypted Text -------> '.$decrypted_string);
           log_data('rest/agent/'.date('M').'/'.date('d-m-Y').'/api'.'.log', $decrypted_string, 'after decryption');

           $this->response(array('status' => 'success', 'decrypted_string'=>$decrypted_string),200);
        }
    }
    
    /*
     * Validate Agent login
     */
    
    function validate_agent_login_post() {
        $config     = array(
                            array('field'=>'agent_username', 'label'=>'Agent Username', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Provide agent username.')),
                            array('field'=>'agent_password', 'label'=>'Agent Password', 'rules'=>'trim|required', 'errors'=>array('required'=>'Please Provide agent password.')),
                        );
                        
        if (form_validate_rules($config) == FALSE) 
        {            
            $error = $this->form_validation->error_array();
            $this->response(array('status' => 'failed', 'error' => $error), 200);
        }
        else 
        {   
            $input     =  $this->input->post();
            $username  =  $input['agent_username'];
            $passowrd  =  $input['agent_password'];
            $agentData =  $this->users_model->where('username', $username)->find_all();
            if (!empty($agentData)) {
                $user_array = $agentData[0];
                if ($user_array->status == "Y") {
                    //check if user is agent or not
                    $checkUser = $this->agent_model->where('user_id', $user_array->id)->find_all();
                    if($checkUser) {
                        $salt = $user_array->salt;
                        $user_password = $user_array->password;
                        $password = md5($salt . $passowrd);
                        if ($password == $user_password) {
                            $this->response(array('status'=>'success', 'error'=>'User authorized Successfully'),200);
                        } else {
                            $this->response(array('status'=>'failed', 'error'=>'Password is wrong'),200);
                        }
                    } else {
                        $this->response(array('status'=>'failed', 'error'=>'User is not a agent'),200);
                    }
                } else {
                    $this->response(array('status'=>'failed', 'error'=>'Users account is disabled'),200);
                }
            } else {
                $this->response(array('status'=>'failed', 'error'=>'User doesnot exist'),200);
            }
        }
    }
    
    /*
     * Function to get agent details from agent code
     */
    
    function get_agent_details_post() {
        $input = $this->post();
        $config = array(
                        array('field'=>'agent_code', 'label'=>'Agent Code', 'rules'=>'required', 'errors'=>array('required'=>'Agent Code is required')),
                        );
        if (form_validate_rules($config) == FALSE) {
            $error = $this->form_validation->error_array();
            $this->response(array($error, 200));
        } else { 
            $agentData = $this->agent_model->where(array('agent_code'=> $input['agent_code'], 'is_status'=>'1', 'is_deleted'=>'0'))->find_all();
            if($agentData) {
                $this->response(array('data' => $agentData, 'status' => 'success'),200);
            } else {
                $this->response(array('status' => 'failed','error'=> "Agent with given agent code is not available"),200);
            }
        }
    }
        
 } 