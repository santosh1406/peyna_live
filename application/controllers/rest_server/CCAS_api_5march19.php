<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class CCAS_api extends REST_Controller {

    private $requestId;

    public function __construct() {
        parent::__construct();
        $this->load->helper('smartcard_helper');
        $this->apiCred = json_decode(SMART_CARD_CREDS, true);
        $header_value = $this->apiCred['X_API_KEY'];
        $this->header = array(
            'X-API-KEY:' . $header_value
        );

        $this->basicAuth = array(
            'username' => $this->apiCred['username'],
            'password' => $this->apiCred['password']
        );
        $this->load->model(array('Mainmodel','Passfare_model','Customer_registration_model','Utility_model'));

		 $request = $this->post();
		 $user_id = $request['apploginuser'];
		 $session_id= $this->Mainmodel->get_session_id($this->input->post('session_id'),$user_id);
		 $this->session_id = $session_id['itms_session_id'];
		 $this->user_id =itms_user_id;
		 
		 /*log code added start here */
 
        $insert_data = array('username' => $this->input->post('apploginuser'), 'ip' => $_SERVER['REMOTE_ADDR'], 'method' => $_SERVER['REQUEST_URI'], 'type' => 'get', 'version_id' => $this->input->post('version_id'));
		
		 $insert_data['status'] = REQUEST_PENDING_STATUS;

        $this->requestId = $this->Mainmodel->insert_audit_data($insert_data)."";
        
        $log_data = array();
        $log_data['url'] = current_url();
        $log_data['Request_type'] = $_SERVER['REQUEST_METHOD'];
        $log_data['get'] = $this->input->get();
        $log_data['post'] = $this->post();
        $log_data['header'] = getallheaders();
        $log_data['user'] = $this->input->server('PHP_AUTH_USER');
        $log_data['ip'] = $this->input->ip_address();
            // show($log_data);
        log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $log_data, 'api');
        
       /*log code added end here */
          /*   added code disallow multiple login of same user code start here */
        $startTime = date("Y-m-d H:i:s");
        $cenvertedTime = date('Y-m-d H:i:s',strtotime(LOGIN_EXPIRE_TIME,strtotime($startTime))) ;
        $this->Mainmodel->upDateLastActivityTime($this->input->post('session_id'),$cenvertedTime);
        /*   added code disallow multiple login of same user code end here */
    }

    public function generate_otp_post() { 
        $URL = $this->apiCred['url'] . '/CCAS_api/generate_otp';
        $request = $this->post();
		$itms_request = $this->post();
		$itms_request['apploginuser'] = $this->user_id ;
		$itms_request['session_id'] = $this->session_id ;
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
        //json_encode($this->response($json));
        log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
       
         json_response($json);
    }

	
     public function wallet_creation_post() 
     {
        $custArr = $request = array();
        $URL = $this->apiCred['url'] . '/CCAS_api/wallet_creation';
        $request = $this->post();
		
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;
        
		/*check wallet balance added by sonali code start here */
			$trimaxId = $this->input->post('trimaxId');
			$custPassArr = $this->Passfare_model->getPassDetailsByTrimaxID($trimaxId);
			if (empty($custPassArr['total_amount'])) {
				$this->response(array('status' => 200, 'msg' => "Wrong Trimax Id", 'data'=>"Wrong Trimax Id" ), 200);
			}
			
			$request['amount'] = $custPassArr['total_amount'];
			
			$result = checkWalletTxn_Smartcard($request, $comments = "check wallet balance");
			$response = json_decode($result,200);
		   
			$responseMessage = ['responseCode' =>$response['responseCode'],"responseMessage"=>$response['msg']];
						  
			if ($response) {
				$this->response(array('status' => $response['status'], 'msg' => $response['status'], 'data'=>$responseMessage ), 200);
			}
        /*check wallet balance added by sonali code end here  */ 
         
	     $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
		
         $json = json_decode($data);
         log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
         
		 $is_reposting_api_call=false; 
         
     if($json->data->responseCode==REPOSTING_ERROR_CODE)
         {
           $URL = $this->apiCred['url'] . '/CCAS_api/wallet_transaction_reposting';  
           $itms_request['request_id'] = $json->data->requestId ;
		   
		   if(empty($json->data->txnRefNo))
		      $itms_request['txnRefNo'] = 'ssdsddadda' ;
           else{ 
		     $itms_request['txnRefNo'] = $json->data->txnRefNo ;
			} 
           $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
		  // print_r($data);
           $json = json_decode($data);
           log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
          // $is_reposting_api_call=true; 
         }
         
         
         if ((!empty($json->msg) && strtolower($json->msg) == 'success') || 
          (!empty($json->status) && strtolower($json->status) == 'failure' && strtolower($json->msg) == 'failure')) 
	     {
            $user_name = $this->input->post('apploginuser');
            $version_id = $this->input->post('version_id');
            $session_id = $this->input->post('session_id');
            $trimaxId = $this->input->post('trimaxId');
            $wallet_proof_type = $this->input->post('wallet_proof_type');
            $wallet_proof_ref_no = $this->input->post('wallet_proof_ref_no');
			/* added new code start here */
            $postVal = $this->input->post();
            $postVal['process_type'] = WALLET_CREATION_FUNC;
            $postVal['card_id'] = $postVal['trimaxId'];
            $postVal['request_id'] = $this->requestId;
            $this->Passfare_model->insertTransInquiryData($postVal);
            
            $custArr = $this->Passfare_model->getMobileNumberByTrimaxID($trimaxId);
          
            $custPassArr = $this->Passfare_model->getPassDetailsByTrimaxID($trimaxId);
            
           
            $cardType = $this->input->post('cardType');

            $this->Passfare_model->updateCustWalletProofData($custArr['cust_card_id'],$this->input->post());

            $dob = isset($custArr['date_of_birth']) ? $custArr['date_of_birth'] :"";
            $card_type = $this->input->post('cardType');
            $age = calculateAge($dob);
            $cardType = getCardType($age, $card_type);

            if($cardType == CARDTYPE_OTC_CC || $cardType == CARDTYPE_AOP_CC){
                $custCardId = $this->Passfare_model->getCustCardIdByTrimaxID($trimaxId);
            }
            if($cardType == CARDTYPE_PPC_CC || $cardType == CARDTYPE_U18_CC){
                $custCardId = '';
            }
				if (strtolower($json->status) == 'success' && strtolower($json->msg) == 'success') 
				{
                    $tw_created_status = W_STATUS_CREATED;
                    $sw_created_status = W_STATUS_CREATED;
                    if ($cardType == CARDTYPE_U18_CC) {
                        $sw_created_status = W_STATUS_NA;
                    }
					$whereData = array('trimax_card_id' => $trimaxId);
                    $this->updateActiveStatus($whereData); 
                    $this->Passfare_model->updateTWSWFlag($tw_created_status, $sw_created_status, $trimaxId, $user_name);
					
					 if(!empty($json->data->txnRefNo))
					 {
					    $update_req_txn['request_id'] = $this->requestId;
                        $update_req_txn['txn_ref_no'] = $json->data->txnRefNo;
						$whereData['is_active'] = '1';
                        $this->Mainmodel->updateReqTxnData($update_req_txn,$whereData);
					 }
       
                  if (!empty($this->input->post('cardType')) && $this->input->post('cardType') == CARDTYPE_OTC_CC) 
                    {  
                       $this->load->model(array('otc_card_inventory_model'));
                     // $this->load->model(array('otc_card_inventory_model'));
                      $otcInvenoryTxnResult = $this->otc_card_inventory_model->getOtcInvenoryTransaction($request['apploginuser']); 

                        $total_cards = $otcInvenoryTxnResult[0]['total_cards']-1 ;
                        $update_cards_data = array(
                            'user_id'=> $request['apploginuser'],
                            'total_cards'=> $total_cards,
                            'updated_date' =>date('Y-m-d H:i:s')
                            );
                          $this->otc_card_inventory_model->UpdateCardsByUser($update_cards_data);
                          
                          $card_fee_amount=0;
                          $fee_data = $this->Mainmodel->getFeeType($trimaxId,'CardFee');
                          
                          if(!empty($fee_data[0]['amount']))
                          {
                             $card_fee_amount = $fee_data[0]['amount'];
                          }
                          else  if(!empty($fee_data['amount']))
                          {
                             $card_fee_amount = $fee_data['amount'];
                          }
                          
                          $request['amount'] = ($custPassArr['total_amount'])-$card_fee_amount;
                          updateWalletTxn($request);  
                          
                          
                        // update wallet_trans_no code start - by sonali

                        $from_userid = $request['apploginuser'];
                        $getCardTransQuery = "select id from ps_card_transaction where trimax_card_id = $trimaxId and created_by = $from_userid limit 1";
                        $getCardTransId = $this->db->query($getCardTransQuery)->result_array();

                        $getWalletTransQuery = "select transaction_no from wallet_trans where user_id = $from_userid ORDER BY `id` DESC LIMIT 1 ";
                        $getWalletTransNo = $this->db->query($getWalletTransQuery)->result_array();

                        $updateWalletTransNo = ['wallet_trans_no' => $getWalletTransNo[0]['transaction_no']];
                        $this->db->where('id',$getCardTransId[0]['id']);
                        $this->db->update("ps_card_transaction", $updateWalletTransNo);

                         // update wallet_trans_no code end - by sonali
                         
                    }else if(!empty($custPassArr['total_amount']) && $custPassArr['total_amount'] >0 )
                    {
                        $request['amount'] = ($custPassArr['total_amount']);
                        updateWalletTxn($request); 
                        
                        // update wallet_trans_no code start - by sonali

                        $from_userid = $request['apploginuser'];
                        $getCardTransQuery = "select id from ps_card_transaction where trimax_card_id = $trimaxId and created_by = $from_userid limit 1";
                        $getCardTransId = $this->db->query($getCardTransQuery)->result_array();

                        $getWalletTransQuery = "select transaction_no from wallet_trans where user_id = $from_userid ORDER BY `id` DESC LIMIT 1 ";
                        $getWalletTransNo = $this->db->query($getWalletTransQuery)->result_array();

                        $updateWalletTransNo = ['wallet_trans_no' => $getWalletTransNo[0]['transaction_no']];
                        $this->db->where('id',$getCardTransId[0]['id']);
                        $this->db->update("ps_card_transaction", $updateWalletTransNo);

                         // update wallet_trans_no code end - by sonali
                        
                        
                    }
			
                }elseif (strtolower($json->status) == 'failure' && strtolower($json->msg) == 'failure')
                 {
                        $tw_created_status = W_STATUS_NOT_CREATED;
                        $sw_created_status = W_STATUS_NOT_CREATED;
                        if ($cardType == CARDTYPE_U18_CC) {
                            $sw_created_status = W_STATUS_NA;
                        }
                        $this->Passfare_model->updateTWSWFlag($tw_created_status, $sw_created_status, $trimaxId, $user_name);
                 }
         
        }
		//sleep(120);
        json_response($json);
    }

	

    public function tw_wallet_limit_post() {
        $URL = $this->apiCred['url'] . '/CCAS_api/tw_wallet_limit';
        $request = $this->post();
		
		$itms_request = $this->post();
		$itms_request['apploginuser'] = $this->user_id ;
		$itms_request['session_id'] = $this->session_id ;
		
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
		log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');

       // json_encode($this->response($json),200);
		json_response($json);
    }

    public function sw_wallet_limit_post() {
        $URL = $this->apiCred['url'] . '/CCAS_api/sw_wallet_limit';
        $request = $this->post();
		
		$itms_request = $this->post();
		$itms_request['apploginuser'] = $this->user_id ;
		$itms_request['session_id'] = $this->session_id ;
		
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
		//print_r($data);
        $json = json_decode($data);
		log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');

        //json_encode($this->response($json),200);
		json_response($json);

    }

    public function get_card_list_post() {
        $URL = $this->apiCred['url'] . '/CCAS_api/get_card_list';
        $request = $this->post();
		
		$itms_request = $this->post();
		$itms_request['apploginuser'] = $this->user_id ;
		$itms_request['session_id'] = $this->session_id ;
		
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
		log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');

      //  json_encode($this->response($json),200);
				json_response($json);

    }

    public function customer_search_post() {
        $URL = $this->apiCred['url'] . '/CCAS_api/customer_search';
        $request = $this->post();
		$itms_request = $this->post();
		$itms_request['apploginuser'] = $this->user_id ;
		$itms_request['session_id'] = $this->session_id ;
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
		
		//print_r( $data );
        $json = json_decode($data);
		log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');

       // json_encode($this->response($json),200);
				json_response($json);

    }

    
	
	 public function tw_wallet_topup_post() 
	 {
        $URL = $this->apiCred['url'] . '/CCAS_api/tw_wallet_topup';
        $request = $this->post();
		
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;
                
        /* check wallet balance added by sonali */
        $request['amount'] = $this->input->post('amount');

        $result = checkWalletTxn_Smartcard($request, $comments = "check wallet balance");
        $response = json_decode($result);  
        
        if (!empty($response)) {
            $responseMessage = ['responseCode' =>$response->responseCode,"responseMessage"=>$response->msg];   
            $this->response(array('status' => $response->status, 'msg' => $response->status, 'data'=>$responseMessage ), 200);
        }
        /* check wallet balance added by sonali */	
		
        $response_data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($response_data);
        log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
  
		if (strtolower($json->status) == 'success') 
		{
		    /*timeout handling code start here */
            $postVal = $this->input->post();
            $postVal['process_type'] = TW_TOPUP_FUNC;
            $postVal['card_id'] = $postVal['custCardId'];
            $postVal['request_id'] = $this->requestId;
            $this->Passfare_model->insertTransInquiryData($postVal);
			
			
			$where_data = array(
                                'process_type' => TW_TOPUP_FUNC,
                                'user_id' => $postVal['apploginuser'],
                                'card_id' => $postVal['custCardId'],
                                'reference_id' => $postVal['referenceId']
                            );
            $request_data = $this->Mainmodel->get_request_id($where_data);
            $this->Mainmodel->update_request_id($request_data['request_id'],REQUEST_DONE_STATUS);
			
			
             /*timeout handling code end here */
			 
            $input = $request;
            $custCardId = $this->input->post('custCardId');
            $EffectiveAmount = $this->input->post('EffectiveAmount');
            $custArr = $this->Passfare_model->getCustomerByCustCardId($custCardId,DISPATCH_STATUS);
            
            $topup_fee_details = $this->input->post('topup_fee_details');
            $topup_fee_details_arr = json_decode($topup_fee_details,1);
            if(isset($topup_fee_details_arr[0][0])){
                $topup_fee_details_arr = $topup_fee_details_arr[0];
            }
            $input['total_changes'] = 0.00;
            $k = 0;
            $newArr = array();
            $bccharges = 0 ;  
            
            foreach ($topup_fee_details_arr  as $key => $value) {
                $arrKey[] = $value['fee_type_nm'];
                $arrValue[] = $value['fee_type_value'];
                if($value['fee_type_nm'] != BASEFARE_KEY){
                    if($value['fee_type_nm'] != TOTALCHARGES_KEY){
                        $value['fee_type_value'] = number_format($value['fee_type_value'], 2, '.', '');
                        $value['fee_type_cd'] = $value['fee_type_nm'];
                        $value['operand'] = '+';
                        
                        $newArr[$k]['fee_type_value'] = number_format($value['fee_type_value'], 2, '.', '');
                        $newArr[$k]['fee_type_cd'] = $value['fee_type_cd'];
                        $newArr[$k]['fee_type_nm'] = $value['fee_type_nm'];
                        $newArr[$k]['operand'] = '+';
                        $k++;
                        
                        if($value['fee_type_nm'] != BONUS_KEY){
                            $input['total_changes'] += $value['fee_type_value'];  // total charges without bonus amt
                        }
                        
                        if(strtolower($value['fee_type_nm']) == BCCHARGES){
                            $bccharges = $value['fee_type_value'];  
                        }
                    }
                }
            }
            $resultArray = array_combine($arrKey, $arrValue);
            unset($resultArray['TopupAmount']);
            
            $input['fee_details'] = $newArr;
            $session_id = $this->input->post('session_id');
            $terminal_code = $this->Passfare_model->getTerminalCodeBytSessionId($session_id);
            $user_name = $this->input->post('apploginuser');
            $data = array(
                "txnType" => 'tw_topup',
                "paymentMode" => "CASH",
                "custIdentifier" => array(
                    "mobileNumber" => isset($custArr['mobile_number']) ? $custArr['mobile_number'] :"",
                    "custCardId" => $this->input->post('custCardId')
                ),
                "amount" => $this->input->post('amount'),
                "requestId" => $this->requestId,
                "initiatorId" => $terminal_code."",
                "initiatorType" => INITIATOR_TYPE,
                "loginId" => $user_name,
                "sessionId" => $session_id,
                "EffectiveAmount" => $EffectiveAmount,

            );
            
            $data = array_merge($data, $resultArray);
            $input['bonus_amt'] = $data['BonusAmt'];
            $insertId = $this->Passfare_model->insertTopupData($data,$input); //insert into temp table
            $returnArr = $json;
       		   
            if (!empty($returnArr)) 
			{
                if (isset($returnArr->data->responseCode)) {
                    
                    if ($returnArr->data->responseCode == RESPONSE_CODE_SUCCESS_CC) {
                        $result['responseCode'] = $returnArr->data->responseCode;
                        $result['responseMessage'] = $returnArr->data->responseMessage;
                        $result['amount'] = $returnArr->data->amount;
                        $result['custCardID'] = $returnArr->data->custCardID;
                        $result['walletType'] = $returnArr->data->walletType;
                        $result['twExpiry'] = $returnArr->data->twExpiry;
                        $result['txnRefNo'] = $returnArr->data->txnRefNo;
                        $result['requestId'] = $returnArr->data->requestId;
                        $this->Passfare_model->updateTopupData($insertId,$result); //update into temp table
                        $insertArr = $this->Passfare_model->insertTopupDetails($input, $result); //insert transaction details
                    } else {
                        $result['responseCode'] = $returnArr->data->responseCode;
                        $result['responseMessage'] = $returnArr->data->responseMessage;
                        $result['requestId'] = $returnArr->data->requestId;
                        
                        $this->Passfare_model->updateTopupData($insertId,$result); //update into temp table
                    }
                } 
            } 

             $request['amount'] = ($this->input->post('amount')+$bccharges);
             updateWalletTxn($request); 

	   }			
			
		//sleep(120);
	
        json_response($json);
    }

    public function sw_wallet_topup_post() 
	{
        $URL = $this->apiCred['url'] . '/CCAS_api/sw_wallet_topup';
        $request = $this->post();
		
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id']   = $this->session_id ;
        
	 /*check wallet balance added by sonali */
	$result = checkWalletTxn_Smartcard($request, $comments = "check wallet balance");
        $response = json_decode($result,200);        
        $responseMessage = ['responseCode' =>$response['responseCode'],"responseMessage"=>$response['msg']];
                      
        if ($response) {
            $this->response(array('status' => $response['status'], 'msg' => $response['status'], 'data'=>$responseMessage ), 200);
        }
         /*check wallet balance added by sonali */
        
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
        if ( strtolower($json->status) == 'success' ) {
		    
			/* Timeout handling code inserted start here  */
            $postVal = $this->input->post();
            $postVal['process_type'] = SW_TOPUP_FUNC;
            $postVal['card_id'] = $postVal['custCardId'];
            $postVal['request_id'] = $this->requestId;
            $postVal['referenceId'] = $postVal['referenceId'];
            $this->Passfare_model->insertTransInquiryData($postVal); 
			
			$where_data = array(
                                'process_type' => SW_TOPUP_FUNC,
                                'user_id' => $postVal['apploginuser'],
                                'card_id' => $postVal['custCardId'],
                                'reference_id' => $postVal['referenceId']
                            );
            $request_data = $this->Mainmodel->get_request_id($where_data);
            $this->Mainmodel->update_request_id($request_data['request_id'],REQUEST_DONE_STATUS);
			
            // Timeout handling code inserted end here 
			
            $user_name = $this->input->post('apploginuser');
            $version_id = $this->input->post('version_id');
            $session_id = $this->input->post('session_id');
            $terminal_code = $this->Passfare_model->getTerminalCodeBytSessionId($session_id);
            $custCardId = $request['custCardId'];
            $custArr = $this->Passfare_model->getCustomerByCustCardId($custCardId,DISPATCH_STATUS);
            
            $EffectiveAmount = $this->input->post('EffectiveAmount');
            $topup_fee_details = $this->input->post('topup_fee_details');            
            $topup_fee_details_arr = json_decode($topup_fee_details,1);
            if(isset($topup_fee_details_arr[0][0])){
                $topup_fee_details_arr = $topup_fee_details_arr[0];
            }
            $request['total_changes'] = 0.00;
            $k = 0;
            $newArr = $arrKey = $arrValue = $resultArray = $data = array(); 
            $bccharges=0 ;
            foreach ($topup_fee_details_arr  as $key => $value) {
                $arrKey[] = $value['fee_type_nm'];
                $arrValue[] = $value['fee_type_value'];
                if($value['fee_type_nm'] != BASEFARE_KEY){
                    if($value['fee_type_nm'] != TOTALCHARGES_KEY){
                        $value['fee_type_value'] = number_format($value['fee_type_value'], 2, '.', '');
                        $value['fee_type_cd'] = $value['fee_type_nm'];
                        $value['operand'] = '+';
                        
                        $newArr[$k]['fee_type_value'] = number_format($value['fee_type_value'], 2, '.', '');
                        $newArr[$k]['fee_type_cd'] = $value['fee_type_cd'];
                        $newArr[$k]['fee_type_nm'] = $value['fee_type_nm'];
                        $newArr[$k]['operand'] = '+';
                        $k++;
                        
                        if($value['fee_type_nm'] != BONUS_KEY){
                            $request['total_changes'] += $value['fee_type_value'];  // total charges without bonus amt
                        }
                        
                       if(strtolower($value['fee_type_nm']) == BCCHARGES){
                            $bccharges = $value['fee_type_value'];  
                        } 
                        
                    }
                }
            }
            
            $resultArray = array_combine($arrKey, $arrValue);
            unset($resultArray['TopupAmount']);
            
            $request['fee_details'] = $newArr;
            
            $data = array(
                "txnType" => 'sw_topup',
                "paymentMode" => "CASH",
                "custIdentifier" => array(
                    "mobileNumber" => isset($custArr['mobile_number']) ? $custArr['mobile_number'] :"",
                    "custCardId" => $this->input->post('custCardId')
                ),
                "auth" => array(
                    "otp" => $this->input->post('otp'),
                    "referenceId" => $this->input->post('referenceId')
                ),
                "amount" => $this->input->post('amount'),
                "requestId" => $this->requestId,
                "initiatorId" => $terminal_code."",
                "initiatorType" => INITIATOR_TYPE."",
                "loginId" => $user_name,
                "sessionId" => $session_id,
                "EffectiveAmount" => $EffectiveAmount,
            );
            
            $data = array_merge($data, $resultArray);
            
            $insertId = $this->Passfare_model->insertTopupData($data,$request);  //insert into temp table
            if (isset($json->data->responseCode) && $json->data->responseCode==RESPONSE_CODE_SUCCESS_CC) 
             {
                $result['responseCode'] = $json->data->responseCode;
                $result['responseMessage'] = $json->data->responseMessage;
                $result['amount'] = $json->data->amount;
                $result['custCardID'] = $json->data->custCardID;
                $result['walletType'] = $json->data->walletType;
                $result['swExpiry'] = $json->data->swExpiry;
                $result['txnRefNo'] = $json->data->txnRefNo;
                $result['requestId'] = $json->data->requestId;

                $this->Passfare_model->updateTopupData($insertId,$result);  //update into temp table
                $this->Passfare_model->insertTopupDetails($request, $result);  //insert transaction details
                // wallet update code added by sonali
                 

            }else
             {
                $result['responseCode'] = $json->data->responseCode;
                $result['responseMessage'] = $json->data->responseMessage;
                $result['requestId'] = $json->data->requestId;
                $this->Passfare_model->updateTopupData($insertId,$result);  //update into temp table
            }
            $request['amount'] = ($this->input->post('amount')+$bccharges);
            updateWalletTxn($request); 
            
        }
      //   sleep(120);

        json_response($json);
    }

    public function tw_via_sw_topup_post() 
	{
        $URL = $this->apiCred['url'] . '/CCAS_api/tw_via_sw_topup';
        $request = $this->post();        
        
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id']   = $this->session_id ;		
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        
        $json = json_decode($data);
		log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');

        if ( strtolower($json->status) == 'success' ) 
        {     
            $topup_fee_details = $this->input->post('topup_fee_details');
            $topup_fee_details_arr = json_decode($topup_fee_details,1);
            if(isset($topup_fee_details_arr[0][0])){
                $topup_fee_details_arr = $topup_fee_details_arr[0];
            }
            $request['total_changes'] = 0.00;
            $k = 0;
            $newArr = array();
            foreach ($topup_fee_details_arr  as $key => $value) {
                if($value['fee_type_nm'] != BASEFARE_KEY){
                    if($value['fee_type_nm'] != TOTALCHARGES_KEY){
                        $value['fee_type_value'] = number_format($value['fee_type_value'], 2, '.', '');
                        $value['fee_type_cd'] = $value['fee_type_nm'];
                        $value['operand'] = '+';
                        
                        $newArr[$k]['fee_type_value'] = number_format($value['fee_type_value'], 2, '.', '');
                        $newArr[$k]['fee_type_cd'] = $value['fee_type_cd'];
                        $newArr[$k]['fee_type_nm'] = $value['fee_type_nm'];
                        $newArr[$k]['operand'] = '+';
                        $k++;
                        
                        if($value['fee_type_nm'] != BONUS_KEY){
                            $request['total_changes'] += $value['fee_type_value'];  // total charges without bonus amt
                        }
                    }
                }
            }
            $request['fee_details'] = $newArr;
            
            $request['tw_via_sw_flag'] = 1;
            $custCardId = $this->input->post('custCardId');
           
            $returnArr = $this->Passfare_model->insertTopupDetails($request, ''); //insert transaction details
         } 
            
         json_response($json);
    }

    public function get_transaction_charges_post() {
        $URL = $this->apiCred['url'] . '/CCAS_api/get_transaction_charges';
        $request = $this->post();
		$itms_request = $this->post();
		$itms_request['apploginuser'] = $this->user_id ;
		$itms_request['session_id'] = $this->session_id ;
		
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
		log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');

         json_response($json);
    }

    public function get_tw_transaction_charges_post() {
        $URL = $this->apiCred['url'] . '/CCAS_api/get_tw_transaction_charges';
        $request = $this->post();
		
		$itms_request = $this->post();
		$itms_request['apploginuser'] = $this->user_id ;
		$itms_request['session_id'] = $this->session_id ;
		
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
		 log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');

         json_response($json);
    }

   public function otc_card_whitelisting_post() {
        $URL = $this->apiCred['url'] . '/CCAS_api/otc_card_whitelisting';
        $request = $this->post();
	   
	    $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id']   = $this->session_id ;
		
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');

        $user_name = $this->input->post('apploginuser');
        $version_id = $this->input->post('version_id');
        $session_id = $this->input->post('session_id');
        $terminal_code = $this->Passfare_model->getTerminalCodeBytSessionId($session_id);
       
        $custCardId = $this->input->post('custCardId');
     
            $card_data = $this->Passfare_model->getTrimaxIdBytCustCardId($custCardId);
          
            $cardType = 'OTC';
            if(empty($card_data)){
                $trimaxId = $this->Utility_model->gen_card_number(T_ID_ISSUE_NM,OTC_DEPOT_CD,$user_name);
           
                $master_card_id = $this->Passfare_model->insertIntoMasterCard($custCardId, $trimaxId,$cardType, $user_name);
               
                 

            } else {
                $master_card_id = $card_data[0]['id'];
                $trimaxId = $card_data[0]['trimax_card_id'];
                if($card_data[0]['is_whitelisted'] == '1') {
                 
                }
            }

                $responseCode= $json->data->responseCode;
                     //print_r($json);
                   if($responseCode == RESPONSE_CODE_SUCCESS_CC) {

                        $master_card_where = array('id' => $master_card_id, 'is_active' => '1');
                        //echo "SB";
                        $master_card_update = array('is_whitelisted' => '1', 'modified_by' => $user_name);
                        $update_master_card_res = $this->Mainmodel->update_master_card($master_card_where,$master_card_update);

                        $result['responseCode'] = $json->data->responseCode;
                        $result['responseMessage'] =$json->data->responseMessage;
                        $result['requestId'] =$json->data->requestId;
                        $result['trimax_card_id'] = $trimaxId;
                       
                    } else  {
                        $result['responseCode'] = $json->data->responseCode;
                        $result['responseMessage'] =$json->data->responseMessage;
                        $result['requestId'] = $json->data->requestId;
                       
                    }

                       json_response($json);
                
        } 

    public function otc_reg_trans_charges_post() {
        $URL = $this->apiCred['url'] . '/CCAS_api/otc_reg_trans_charges';
        $request = $this->post();
		$itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id']   = $this->session_id ;
		
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
		 log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');

         json_response($json);
    }

    public function get_block_card_list_post() {
        $URL = $this->apiCred['url'] . '/CCAS_api/get_block_card_list';
        $request = $this->post();
		$itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id']   = $this->session_id ;
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
		log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');

         json_response($json);
    }

    public function block_wallet_post() {
        $URL = $this->apiCred['url'] . '/CCAS_api/block_wallet';
        $request = $this->post();
        $post_val = $this->input->post();
		$itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id']   = $this->session_id ;
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
		log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');

        if (strtolower($json->status) == 'success') {
            $trimax_card_id = $this->Passfare_model->getTrimaxIdByCustCardId($request['custCardId']);
            $post_val['trimax_card_id'] = $trimax_card_id;

            $insert_data = array('cust_card_id' => $post_val['custCardId'], 'block_date' => date('Y-m-d H:i:s'), 'block_status' => 'Y','created_by' => $post_val['apploginuser']);

            $this->Mainmodel->update_card_status($post_val,BLOCK_STATUS,$insert_data,'ps_report_card_loss');
        }
         json_response($json);
    }
	
	 /// new api added on 04/02/2019 start here /
    
    public function bus_pass_renewal_post() { 
        $URL = $this->apiCred['url'] . '/CCAS_api/bus_pass_renewal';
        $request = $this->post();
        $post_val = $this->input->post();
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id']   = $this->session_id ;		
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
        if (strtolower($json->status) == 'success') {
        }
        json_response($json);
    }
    
     public function summary_post() {
        $URL = $this->apiCred['url'] . '/CCAS_api/summary';
        $request = $this->post();
        $post_val = $this->input->post();
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id']   = $this->session_id ;		
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
        if (strtolower($json->status) == 'success') {
        }
        json_response($json);
    }
    
    public function get_reissue_card_list_post() {
        $URL = $this->apiCred['url'] . '/CCAS_api/get_reissue_card_list';
        $request = $this->post();
        $post_val = $this->input->post();
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id']   = $this->session_id ;		
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
        if (strtolower($json->status) == 'success') {
		
		 if(!empty($json->data->custCardIdList)){
         	 foreach ($json->data->custCardIdList  as $key => &$value) 
              {
			      $where_data = array('trimax_card_id' => $value->trimaxId);
                 $cust_data = $this->Mainmodel->get_cust_data($where_data,'asc');
				
				  if(!empty($cust_data[0])){
				 $json->data->custCardIdList[$key]->dob=$cust_data[0]['date_of_birth'];  
                 }
             }
        }
	   }	
	
         json_response($json);
    }
    
     public function reissue_card_post() 
      {
        $URL = $this->apiCred['url'] . '/CCAS_api/reissue_card';
        //$request = $this->post();
        //$post_val = $this->input->post();
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id']   = $this->session_id ;		
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
        if (strtolower($json->status) == 'success') 
         { 
            $user_name = $this->input->post('apploginuser');
           // $resultArr = array();
            $post_data = $this->input->post();
            $session_id = $this->input->post('session_id');
            $vendor_id = $this->Customer_registration_model->get_vendor_id($post_data['apploginuser'])['vendor_id'];
            $terminal_code = $this->Passfare_model->getTerminalCodeBytSessionId($session_id);
            $terminal_id = $this->Mainmodel->getTermIdByTermCode($terminal_code)['terminal_id'];
            $card_trans_id = ($post_data['cardType'] == CARDTYPE_OTC_CC) ? REISSUE_OTC_TRANS_ID : REISSUE_PPC_TRANS_ID;

            $trans_charge_list = json_decode($post_data['chargeList'],1);

            //Customer Card Master Data
            $cust_card_data_arr = $this->Passfare_model->getCardCustData($post_data['oldCustCardId']);
            $cust_card_data = $cust_card_data_arr[0];
            $trimax_card_id = $cust_card_data['trimax_card_id'];
            $old_trimax_id = $trimax_card_id;

            $where_data = array('trimax_card_id' => $trimax_card_id);
            $card_trans_data = $this->Mainmodel->get_card_trans_id($where_data,'desc');
            
            if($post_data['cardType'] == CARDTYPE_OTC_CC) {
               $card_master_data = $this->Passfare_model->getCardDataByCid($post_data['custCardId']);
               $trimax_card_id = $card_master_data[0]['trimax_card_id'];
                $master_card_id = $card_master_data['id'];
            }

            $chkCardReissue = $this->Passfare_model->chkCardReissue($trimax_card_id);
            if(!empty($chkCardReissue)) {
//                $message = array('status' => 'failure', 'msg' => 'Card is reissued');
//                $this->set_response($message, 200, FALSE);
            }

            $card_date = $this->Customer_registration_model->get_card_val_exp_date($card_trans_data['pass_concession_id']);

            $card_date['valid_from'] = TODAY_DATE;
            if($card_date['expiry_type'] == 'C') {
                $card_date['valid_to'] = date('Y-m-d', strtotime("+".$card_date['valid_days']." days"));
            } else {

                $month_day = date('m-d', strtotime($card_date['valid_from']));
                $end_month = date('m',strtotime($card_date['valid_to']));
                $start_month = date('m',strtotime($card_date['valid_from']));

                $m_month_day = date('m-d', strtotime(CARD_YEAR_END_DATE));
                $card_date['valid_to'] = date('Y',strtotime($card_date['valid_from'])) .'-'. $m_month_day;

                if($start_month > $end_month) {
                    $card_date['valid_to'] = date('Y',strtotime("+1 year",strtotime($card_date['valid_from']))) .'-'. $month_day;
                }
            }

            $chk_reiisue_data = $this->Mainmodel->checkCardData($trimax_card_id,$card_trans_id,CITYCASH_PENDING_STATUS,REISSUE_STATUS);
            // show($chk_reiisue_data,1);
            if(!empty($chk_reiisue_data)) {
                $customer_card_master_id = $chk_reiisue_data['cust_card_id'];
                $card_trans_id = $chk_reiisue_data['card_trans_id'];
                $card_trans_details_id = $chk_reiisue_data['card_trans_details_id'];
                $amt_trans_id = $chk_reiisue_data['amt_trans_id'];
                $inventory_master_id = $chk_reiisue_data['inv_id'];
                $master_card_id = $chk_reiisue_data['card_master_id'];
            } else {
                
                if($post_data['cardType'] != CARDTYPE_OTC_CC) {
                    $master_card_data = array(
                                            'trimax_card_id' => $trimax_card_id,
                                            'display_card_id' => $trimax_card_id,
                                            'card_status' => CITYCASH_PENDING_STATUS,
                                            'card_category_cd' => $post_data['cardType'],
                                            'customer_id' => $cust_card_data['customer_id'],
                                            'created_by' => $post_data['apploginuser'],
                                            'is_active' => '2'
                                        );
                    $master_card_id = $this->Customer_registration_model->insert_customer_reg_data($master_card_data,MASTER_CARD);

                }
                //Master Card Data end here

                //customer card master start here
                unset($cust_card_data['id'],$cust_card_data['created_date'],$cust_card_data['modified_by'],$cust_card_data['modified_date'],$cust_card_data['is_active']);

                $cust_card_data['registration_depo_cd'] = $post_data['depotCode'];
                $cust_card_data['collection_depo_cd'] = $post_data['depotCode'];
                $cust_card_data['trimax_card_id'] = $trimax_card_id;
                $cust_card_data['cust_card_id'] = ($cust_card_data['card_category_cd'] == CARDTYPE_OTC_CC) ? $post_data['custCardId'] : '';
                $cust_card_data['display_card_id'] = $trimax_card_id;
                $cust_card_data['master_card_id'] = $master_card_id;
                $cust_card_data['dispatch_date'] = ($post_data['cardType'] == CARDTYPE_OTC_CC) ? date('Y-m-d') : '';
                $cust_card_data['card_validity_date'] = $card_date['valid_from'];
                $cust_card_data['card_expiry_date'] = $card_date['valid_to'];
                $cust_card_data['card_status'] = CITYCASH_PENDING_STATUS;
                $cust_card_data['created_by'] = $post_data['apploginuser'];
                $cust_card_data['is_active'] = '2';

                $customer_card_master_id = $this->Customer_registration_model->insert_customer_reg_data($cust_card_data,CUSTOMER_CARD_MASTER);

                //customer card master end here

                //card transaction start here

                $trans_type_map = ($post_data['cardType'] == CARDTYPE_OTC_CC) ? REISSUE_OTC_TRANS_TYPE : REISSUE_PPC_TRANS_TYPE;

                $card_fee_data = array('pass_concession_id' => $card_trans_data['pass_concession_id'], 'span_id' => $card_trans_data['span_id'], 'trans_type' => $trans_type_map);

                $card_fee_master_id = $this->Customer_registration_model->get_card_fee_master_id($card_fee_data);



                $getRegData = $this->Customer_registration_model->getCardTransDetails($card_trans_data['id']);

                unset($card_trans_data['id'],$card_trans_data['created_date'],$card_trans_data['modified_by'],$card_trans_data['modified_date'],$card_trans_data['is_active'],$card_trans_data['opening_balance'],$card_trans_data['closing_balance'],$card_trans_data['amount']);

            //    $receipt_number = $this->Utility_model->receipt_number('RECEIPT',$post_data['apploginuser']);
                
                $receipt_number = $json->data->$receipt_number;

                $card_trans_data['trimax_card_id'] = $trimax_card_id;
                $card_trans_data['transaction_type_id'] = $card_fee_master_id['ctrans_type_id'];
                $card_trans_data['write_flag'] = '0';
                $card_trans_data['depot_cd'] = $post_data['depotCode'];
                $card_trans_data['terminal_id'] = $terminal_id;
                $card_trans_data['receipt_number'] = $receipt_number;
                $card_trans_data['card_fee_master_id'] = $card_fee_master_id['id'];
                $card_trans_data['session_id'] = $session_id;
                $card_trans_data['vendor_id'] = $vendor_id;
                $card_trans_data['created_by'] = $user_name;
                $card_trans_data['is_active'] = '2';

                $card_trans_id = $this->Customer_registration_model->insert_customer_reg_data($card_trans_data,CARD_TRANS);

                //card transaction end here

                //card transaction details start here
                foreach ($getRegData as $key => $value) {
                    $card_trans_details_data[$key] = array(
                        'card_transaction_id' => $card_trans_id,
                        'trimax_card_id' => $trimax_card_id,
                        'card_fee_master_id' => $card_fee_master_id['id'],
                        'route_no' => $value['route_no'],
                        'route_type' => $value['route_type'],
                        'fare_type_cd' => $value['fare_type_cd'],
                        'service_number' => '0',
                        'from_stop_code'=>$value['from_stop_code'],
                        'till_stop_code'=>$value['till_stop_code'],
                        'via_stop_code'=>$value['via_stop_code'],
                        'stages'=>$value['stages'],
                        'session_id' => $session_id,
                        'created_by' => $user_name,
                        'is_active' => '2'
                    );
                }
                
                $card_trans_details_id = $this->Customer_registration_model->insert_customer_reg_bulk_data($card_trans_details_data,CARD_TRANS_DETAILS);

                //card transaction details end here
       $amt_trans_data = array(
                        'card_transaction_id' => $card_trans_id,
                        'trimax_card_id' => $trimax_card_id,
                        'total_amount' => $trans_charge_list['total_fare_amt'],
                        'transaction_type_id' => $card_fee_master_id['ctrans_type_id'],
                        'depot_cd' => $post_data['depotCode'],
                        'terminal_id' => $terminal_id,
                        'session_id' => $session_id,
                        'vendor_id' => $vendor_id,
                        'created_by' => $user_name,
                        'is_active' => '2'
                    );
                $amt_trans_id = $this->Customer_registration_model->insert_customer_reg_data($amt_trans_data,AMOUNT_TRANS);

                //amount transaction end here

                // amount transaction details start here
                $i = 0;
                foreach ($trans_charge_list['card_fee_details'] as $key => $value) {
                    $amt_trans_details_data[$i] = array(
                            'amount_transaction_id' => $amt_trans_id,
                            'amount_code' => $value['fee_type_cd'],
                            'amount' => $value['fee_type_value'],
                            'operand' => $value['operand'],
                            'created_by' => $user_name,
                            'is_active' => '2'
                        );
                    $i++;
                }

                $amt_trans_details_id = $this->Customer_registration_model->insert_customer_reg_bulk_data($amt_trans_details_data,AMOUNT_TRANS_DETAILS);
                //Inventory data start here
                $inventory_data = array(
                                        'user_id' => $cust_card_data['customer_id'],
                                        'card_no' => $trimax_card_id,
                                        'trimax_card_id' => $trimax_card_id,
                                        'cust_card_id' => ($cust_card_data['card_category_cd'] == CARDTYPE_OTC_CC) ? $post_data['custCardId'] : '',
                                        'status' => CITYCASH_PENDING_STATUS,
                                        'card_type' => $post_data['cardType'],
                                        'location_type' => (($post_data['cardType'] == CARDTYPE_OTC_CC) ? 3 : ''),
                                        'location_at' => (($post_data['cardType'] == CARDTYPE_OTC_CC) ? $cust_card_data['registration_depo_cd'] : ''),
                                        'created_by' => $user_name,
                                        'is_active' => '2'
                                    );
                $inventory_master_id = $this->Customer_registration_model->insert_customer_reg_data($inventory_data,INVENTORY);

            }
            
            $customer_data = $this->Mainmodel->getCustomerInfo($cust_card_data['customer_id']);
            $span_days = $this->Mainmodel->get_span_period($card_trans_data);

       
            if (!empty($json)) 
             {
                if (isset($json->data->responseCode)) 
                 {
                     if($json->data->responseCode == RESPONSE_CODE_SUCCESS_CC) 
                      {
                        $updated_card_status = ($post_data['cardType'] == CARDTYPE_OTC_CC) ? WRITING_STATUS : INITIATED_STATUS;
                        $update_master_data = array('card_status' => $updated_card_status, 'is_active' => '1', 'modified_by' => $user_name);

                        $where_data = array('id' => $master_card_id);
                        $this->Mainmodel->update_master_card($where_data, $update_master_data);

                        $where_data = array('id' => $customer_card_master_id);
                        $this->Mainmodel->update_cust_card($where_data, $update_master_data);

                        $update_data = array('status' => $updated_card_status, 'is_active' => '1', 'updated_by' => $user_name);
                        $where_data = array('id' => $inventory_master_id);
                        $this->Mainmodel->update_inventory_card($where_data, $update_data);

                        $master_id_arr = array('card_trans_id' => $card_trans_id, 'card_trans_details_id_1' => $card_trans_details_id, 'amt_trans_id' => $amt_trans_id);
                        $this->Mainmodel->updateActiveStatus($master_id_arr,$update_data);

                        //Update card_transaction write flag as 0 and current to 1
                        $trans_update_data = array('write_flag' => '0', 'modified_by' => $user_name);
                        $this->Mainmodel->update_card_tarns_write_flag($card_trans_id,$old_trimax_id,$trans_update_data);
                        $trans_update_data['write_flag'] = '1';
                        $this->Mainmodel->update_card_trans($card_trans_id,$trans_update_data);
                        //$this->Utility_model->save_command_data($trimax_card_id,REG_COMMAND);
                    } 
                } 
            } 
        }
        json_response($json);
    }
    
    
      public function get_all_card_list_post() {
        $URL = $this->apiCred['url'] . '/CCAS_api/get_all_card_list';
        $request = $this->post();
        $post_val = $this->input->post();
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id']   = $this->session_id ;		
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
        if (strtolower($json->status) == 'success') {
        }
        json_response($json);
    }
    
       public function get_surrender_transaction_charges_post() 
       {
        $URL = $this->apiCred['url'] . '/CCAS_api/get_surrender_transaction_charges';
        $request = $this->post();
        $post_val = $this->input->post();
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id']   = $this->session_id ;		
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
        if (strtolower($json->status) == 'success') {
        }
        json_response($json);
      }
    
       public function get_preSurrenderValidation_post() 
       {
        $URL = $this->apiCred['url'] . '/CCAS_api/get_preSurrenderValidation';
        $request = $this->post();
        $post_val = $this->input->post();
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id']   = $this->session_id ;		
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
        if (strtolower($json->status) == 'success') {
        }
        json_response($json);
      }
      
      
	  
	   public function tw_transaction_enquiry_post() 
       {
        $URL = $this->apiCred['url'] . '/CCAS_api/tw_transaction_enquiry';
        $request = $this->post();
        $post_val = $this->input->post();
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id']   = $this->session_id ;		
		
		
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
        if (strtolower($json->status) == 'success') {
            
            if (isset($json->data->responseCode)) {

                    if ($json->data->responseCode == RESPONSE_CODE_SUCCESS_CC) {
                        
                        $postVal = $this->input->post();
                        $topup_fee_details = $postVal['topup_fee_details'];
                        $topup_fee_details_arr = json_decode($topup_fee_details,1);
                        
                        if(isset($topup_fee_details_arr[0][0])){
                            $topup_fee_details_arr = $topup_fee_details_arr[0];
                        }

                        $input['total_changes'] = 0.00;
                        $k = 0;
                        $newArr = array();
                        foreach ($topup_fee_details_arr  as $key => $value) {
                            $arrKey[] = $value['fee_type_nm'];
                            $arrValue[] = $value['fee_type_value'];
                            if($value['fee_type_nm'] != BASEFARE_KEY){
                                if($value['fee_type_nm'] != TOTALCHARGES_KEY){
                                    $value['fee_type_value'] = number_format($value['fee_type_value'], 2, '.', '');
                                    $value['fee_type_cd'] = $value['fee_type_nm'];
                                    $value['operand'] = '+';

                                    $newArr[$k]['fee_type_value'] = number_format($value['fee_type_value'], 2, '.', '');
                                    $newArr[$k]['fee_type_cd'] = $value['fee_type_cd'];
                                    $newArr[$k]['fee_type_nm'] = $value['fee_type_nm'];
                                    $newArr[$k]['operand'] = '+';
                                    $k++;

                                    if($value['fee_type_nm'] != BONUS_KEY){
                                        $input['total_changes'] += $value['fee_type_value'];  // total charges without bonus amt
                                    }
                                }
                            }
                        }
                        $resultArray = array_combine($arrKey, $arrValue);
                        unset($resultArray['TopupAmount']);
                        $input['fee_details'] = $newArr;
                        
                        
                         $where_data = array(
                            'process_type' => TW_TOPUP_FUNC,
                            'user_id' => $postVal['apploginuser'],
                            'card_id' => $postVal['custCardId'],
                            'reference_id' => $postVal['referenceId']
                        );
                         $request_data = $this->Mainmodel->get_request_id($where_data);
                        $result['responseCode'] = $json->data->responseCode;
                        $result['responseMessage'] = $json->data->responseMessage;
                        $result['requestId'] = $json->data->requestId;

                        if($json->data->response_data->responseCode == RESPONSE_CODE_SUCCESS_CC) {

                            $result_ar =$json->data->response_data ;// $returnArr['ResponseData'];

                            $auditData = $this->Mainmodel->getAuditData($request_data['request_id']);
							//print_r($auditData);exit;
                            if(empty($auditData)) {
                                $this->Mainmodel->update_request_id($request_data['request_id'],REQUEST_DONE_STATUS);

                                $this->Passfare_model->updateTopupData($request_data['request_id'],$result_ar); //update into temp table

                                $this->Passfare_model->insertTopupDetails($input, $result_ar);
                            }
                         } 
                        
                    } else {
                        $result['responseCode'] = $json->data->responseCode;
                        $result['responseMessage'] = $json->data->responseMessage;
                        $result['requestId'] = $json->data->requestId;
                        $result['response_data'] = array();
                        $this->Passfare_model->updateTopupData($request_data['request_id'],$result); //update into temp table
                        $this->Mainmodel->update_request_id($request_data['request_id'],REQUEST_NOT_DONE_STATUS);
                    }
                }
            
        }
        json_response($json);
      }
      
      public function check_reactivation_post() 
       {
        $URL = $this->apiCred['url'] . '/CCAS_api/check_reactivation';
        $request = $this->post();
        $post_val = $this->input->post();
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id']   = $this->session_id ;		
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
        if (strtolower($json->status) == 'success') {
        }
        json_response($json);
      }
      
      
       public function reactivation_wallet_post() 
       {
        $URL = $this->apiCred['url'] . '/CCAS_api/reactivation_wallet';
        $request = $this->post();
        $post_val = $this->input->post();
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id']   = $this->session_id ;		
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
        if (strtolower($json->status) == 'success') {
        }
        json_response($json);
      }
      
      
      
       public function get_reactivation_charges_post() 
       {
            $URL = $this->apiCred['url'] . '/CCAS_api/get_reactivation_charges';
            $itms_request = $this->post();
            $itms_request['apploginuser'] = $this->user_id ;
            $itms_request['session_id']   = $this->session_id ;		
            $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
            $json = json_decode($data);
            log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
            if (strtolower($json->status) == 'success') {
            }
            json_response($json);
      }
      
       public function get_pass_data_post() 
       {
            $URL = $this->apiCred['url'] . '/CCAS_api/get_pass_data';
            $itms_request = $this->post();
            $itms_request['apploginuser'] = $this->user_id ;
            $itms_request['session_id']   = $this->session_id ;		
            $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
            $json = json_decode($data);
            log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
            if (strtolower($json->status) == 'success') {
            }
            json_response($json);
      }
      
      
       public function surrender_post() 
       {
            $URL = $this->apiCred['url'] . '/CCAS_api/surrender';
            $itms_request = $this->post();
             $postVal = $this->post();

            $itms_request['apploginuser'] = $this->user_id ;
            $itms_request['session_id']   = $this->session_id ;		
            $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
            $json = json_decode($data);
            log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
            if (!empty($json->status) && strtolower($json->status) == 'success') {
             $user_name = $this->input->post('apploginuser');

             $trimax_card_id = $this->Passfare_model->getTrimaxIdByCustCardId($postVal['custCardId']);
             $insert_data = array(
                            'cust_card_id' => $postVal['custCardId'],
                            'trimax_card_id' => $trimax_card_id,
                            'surrender_date' => date('Y-m-d H:i:s'),
                            'amount' => $json->data->bPayAmount,
                            'surrender_method' => 'surrender',
                            'created_by' => $user_name
                        );
              $postVal['apploginuser'] = $user_name;
              
              $this->Mainmodel->update_card_status($postVal,SURRENDER_STATUS,$insert_data,'ps_report_card_surrender');
            }
            json_response($json);
      }
      
      public function wallet_transaction_enquiry_post() 
      {
            $URL = $this->apiCred['url'] . '/CCAS_api/wallet_transaction_enquiry';
            $itms_request = $this->post();
             $postVal = $this->post();
        
            $itms_request['apploginuser'] = $this->user_id ;
            $itms_request['session_id']   = $this->session_id ;		
            $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
            $json = json_decode($data);
            log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
            
          
            
            if($json->data->responseCode==REPOSTING_ERROR_CODE)
            {
              $URL = $this->apiCred['url'] . '/CCAS_api/wallet_transaction_reposting';  
              $itms_request['request_id'] = $json->data->requestId ;
              $itms_request['txnRefNo'] = $json->data->txnRefNo ;
              $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
              $json = json_decode($data);
              log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
             
            }
            
           if (strtolower($json->status) == 'success' ) 
             {
                   $where_data = array(
                                  'process_type' => WALLET_CREATION_FUNC,
                                  'user_id' => $this->input->post('apploginuser'),
                                  'reference_id' => $this->input->post('referenceId'),
                                  'card_id' => $this->input->post('trimaxId')
                              );
                    $request_data = $this->Mainmodel->get_request_id($where_data);  

                    $user_name = $this->input->post('apploginuser');
                    $trimaxId = $this->input->post('trimaxId');
                    $custArr = $this->Passfare_model->getMobileNumberByTrimaxID($trimaxId);
                    $dob = $custArr['date_of_birth'];
                    $card_type = $custArr['card_category_cd'];
                    $age = calculateAge($dob);
                    $cardType = getCardType($age, $card_type);   
               if(isset($json->data->responseCode))     
               {   
                    if($json->data->responseCode == RESPONSE_CODE_SUCCESS_CC)
                    {    
                     if ($json->data->response_data->responseCode == RESPONSE_CODE_SUCCESS_CC) 
                     {  
                            $tw_created_status = W_STATUS_CREATED;
                            $sw_created_status = W_STATUS_CREATED;
                            
                           if($cardType == CARDTYPE_U18_CC){
                                $sw_created_status = W_STATUS_NA;
                            }
                        $auditData = $this->Mainmodel->getAuditData($request_data['request_id']);
					   			  
                        if(empty($auditData)) 
		        {
			  $this->Mainmodel->update_request_id($request_data['request_id'],REQUEST_DONE_STATUS);
			  $whereData = array('trimax_card_id' => $trimaxId);
                          $this->updateActiveStatus($whereData);
                          $this->Passfare_model->updateTWSWFlag($tw_created_status, $sw_created_status, $trimaxId, $user_name);

                          $update_req_txn['request_id'] =$json->data->response_data->requestId;    
                          $update_req_txn['txn_ref_no'] = $json->data->response_data->txnRefNo;
                          $whereData['is_active'] = '1';
                          $this->Mainmodel->updateReqTxnData($update_req_txn,$whereData);
                            
                        }
                         						
                       }
                    }else{
                        
                   $custCardData = $this->Mainmodel->getCustCardData(array('trimax_card_id' => $trimaxId));
                     if($custCardData[0]['tw_created_status'] != W_STATUS_CREATED) 
                      {
                        $tw_created_status = W_STATUS_NOT_CREATED;
                        $sw_created_status = W_STATUS_NOT_CREATED;
                        if($cardType == CARDTYPE_U18_CC){
                            $sw_created_status = W_STATUS_NA;
                        }
                        $this->Passfare_model->updateTWSWFlag($tw_created_status, $sw_created_status, $trimaxId, $user_name);
                        $this->Mainmodel->update_request_id($request_data['request_id'],REQUEST_NOT_DONE_STATUS);
                     }
                   }
               }
            }
			 //  log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');

            json_response($json);
      }
      
      
      
     public function updateActiveStatus($data) {
        $this->Mainmodel->updateRegActiveStatus($data);
       // $this->Utility_model->save_command_data($data['trimax_card_id'],REG_COMMAND);
    }
	
	 
	  
     public function sw_transaction_enquiry_post() 
       {
        $URL = $this->apiCred['url'] . '/CCAS_api/sw_transaction_enquiry';
        $request = $this->post();
        $post_val = $this->input->post();
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id']   = $this->session_id ;		
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
        if (strtolower($json->status) == 'success') {
            
            $postVal = $this->input->post();
            
             $returnArr['responseCode'] = $json->data->responseCode ;
             $returnArr['responseMessage'] = $json->data->responseMessage ;
             $returnArr['requestId'] = $json->data->requestId ;
             
            $topup_fee_details = $postVal['topup_fee_details'];
            $topup_fee_details_arr = json_decode($topup_fee_details,1);
            if(isset($topup_fee_details_arr[0][0])){
                $topup_fee_details_arr = $topup_fee_details_arr[0];
            }

            $input['total_changes'] = 0.00;
            $k = 0;
            $newArr = array();
            foreach ($topup_fee_details_arr  as $key => $value) {
                $arrKey[] = $value['fee_type_nm'];
                $arrValue[] = $value['fee_type_value'];
                if($value['fee_type_nm'] != BASEFARE_KEY){
                    if($value['fee_type_nm'] != TOTALCHARGES_KEY){
                        $value['fee_type_value'] = number_format($value['fee_type_value'], 2, '.', '');
                        $value['fee_type_cd'] = $value['fee_type_nm'];
                        $value['operand'] = '+';
                        
                        $newArr[$k]['fee_type_value'] = number_format($value['fee_type_value'], 2, '.', '');
                        $newArr[$k]['fee_type_cd'] = $value['fee_type_cd'];
                        $newArr[$k]['fee_type_nm'] = $value['fee_type_nm'];
                        $newArr[$k]['operand'] = '+';
                        $k++;
                        
                        if($value['fee_type_nm'] != BONUS_KEY){
                            $input['total_changes'] += $value['fee_type_value'];  // total charges without bonus amt
                        }
                    }
                }
            }
            $resultArray = array_combine($arrKey, $arrValue);
            unset($resultArray['TopupAmount']);
            
            $input['fee_details'] = $newArr;

           
            if (isset($returnArr['responseCode'])) 
             {
                    if ($returnArr['responseCode'] == RESPONSE_CODE_SUCCESS_CC) {

                        if($json->data->response_data->responseCode == RESPONSE_CODE_SUCCESS_CC) {

                         $result_ar = $json->data->response_data;
                            
                            $where_data = array(
                                'process_type' => SW_TOPUP_FUNC,
                                'user_id' => $postVal['apploginuser'],
                                'card_id' => $postVal['custCardId'],
                                'reference_id' => $postVal['referenceId']
                            );
                             $request_data = $this->Mainmodel->get_request_id($where_data);

                            $auditData = $this->Mainmodel->getAuditData($request_data['request_id']);
                            if(empty($auditData)) 
                             {
                                $this->Mainmodel->update_request_id($request_data['request_id'],REQUEST_DONE_STATUS);

                                $this->Passfare_model->updateTopupData($request_data['request_id'],$result_ar); //update into temp table

                                $insertArr = $this->Passfare_model->insertTopupDetails($input, $result_ar);//insert transaction details
                            }
                        } 
                        
                    } else {
                        $result['responseCode'] = $returnArr['responseCode'];
                        $result['responseMessage'] = $returnArr['responseMessage'];
                        $result['requestId'] = $returnArr['requestId'];
                        $result['response_data'] = array();
                        
                        $this->Passfare_model->updateTopupData($request_data['request_id'],$result); //update into temp table

                        $this->Mainmodel->update_request_id($request_data['request_id'],REQUEST_NOT_DONE_STATUS);

                    }
                }
            
        }
        json_response($json);
      }
    
      
    //new api added on 04/02/2019 end here//
      
     public function bcPoolDebit_post() 
      { 
        $URL = $this->apiCred['url'] . '/CCAS_api/bcPoolDebit';
        $request = $this->post();
        
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;
        
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
        //json_encode($this->response($json));
        log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
       
         json_response($json);
     }
      
      public function last_load_api_post() 
       {
            $URL = $this->apiCred['url'] . '/CCAS_api/last_load_api';
            $itms_request = $this->post();
            $itms_request['apploginuser'] = $this->user_id ;
            $itms_request['session_id']   = $this->session_id ;		
            $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
            $json = json_decode($data);
            log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
            if (strtolower($json->status) == 'success') {
            }
            json_response($json);
      }
      
      
        
       public function bcPoolDebit_enquiry_post() 
       {
            $URL = $this->apiCred['url'] . '/CCAS_api/bcPoolDebit_enquiry';
            $itms_request = $this->post();
            $itms_request['apploginuser'] = $this->user_id ;
            $itms_request['session_id']   = $this->session_id ;	
          //  $itms_request['trimaxId']     = $itms_request['trimaxId'] ;	

			
            $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
			//print_r($data);
            $json = json_decode($data);
            log_data('rest/smartcard/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
            if (strtolower($json->status) == 'success') 
            {
                $postVal = $this->post();
                $where_data = array(
                            'process_type' => BC_POOL_DEBIT_VIA_SW_FUNC,
                            'user_id' => $postVal['apploginuser'],
                            'card_id' => $postVal['trimaxId'],
                            'is_active' => '1'
                        );
                
               if($postVal['mode_type'] == VIA_CASH) 
               {
                    $where_data['process_type'] = BC_POOL_DEBIT_VIA_CASH_FUNC;   
               }
               
               $request_data = $this->Mainmodel->get_request_id($where_data);
               $auditData = $this->Mainmodel->getAuditData($request_data['request_id']);
               
                if(empty($auditData)) 
                {
                 $this->Mainmodel->update_request_id($request_data['request_id'],REQUEST_DONE_STATUS);

                }
				 //$json->data->ResponseData->responseCode == RESPONSE_CODE_SUCCESS_CC
				// echo $json->status ;
                    if(!empty($json->status) && $json->status!='success' )
                    {
                    	   
                      $result=$this->Mainmodel->fetchRecieptData($itms_request['trimaxId']);
					 
                      $receipt_number='';
                      $trimax_card_id='';  
					
                      
                      if(!empty($result['receipt_number'])){
                          $receipt_number=$result['receipt_number']; 
                      }
                     // echo '==='.$result['trimax_card_id'] ;
                      if(!empty($result['trimax_card_id']))
                      {
                        $trimax_card_id=$result['trimax_card_id'];   
                        $URL = $this->apiCred['url'] . '/CCAS_api/bcPoolDebit';
                        $itms_request['trimaxId'] =$trimax_card_id ;
                        $itms_request['mode_type'] ='SW' ;
                        $itms_request['passRenewalRequestId'] =$json->data->requestId  ;
                        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
					//	print_r($data);
                        $json_bcPoolDebit = json_decode($data);
                        $json_bcPoolDebit->data->receipt_number = $receipt_number ;
                        $json_bcPoolDebit->data->trimax_card_id = $result['trimax_card_id'] ;
                        $json_bcPoolDebit->data->bcpool_enquiry_request_id =  $result['request_id'];
                       }
                       
                    /*Bcpool code added end here */
                        json_response($json_bcPoolDebit);
                    }
                    else
                    {
					//echo '===========';
                      $this->Mainmodel->update_request_id($request_data['request_id'],REQUEST_NOT_DONE_STATUS);
					  
					  $result=$this->Mainmodel->fetchRecieptData($itms_request['trimaxId']);
				      $receipt_number='';
                      $trimax_card_id='';  
					  if(!empty($result['receipt_number'])){
                          $receipt_number=$result['receipt_number']; 
                      }
					 $json->data->receipt_number = $receipt_number ;
					 $json->data->trimax_card_id = $result['trimax_card_id'] ;
					 $json->data->bcpool_enquiry_request_id = $result['request_id']  ; 
					 json_response($json);
                    }
                
                
            }
           
      }
      


}
