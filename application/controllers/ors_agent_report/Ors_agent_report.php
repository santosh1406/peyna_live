<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ors_agent_report extends CI_Controller {
	
	public function __construct() {
        parent::__construct();

        is_logged_in();
        set_time_limit(0);
        
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);
	
        $this->load->model(array('tickets_model', 'agent_model', 'ticket_details_model', 'routes_model', 'bus_stop_master_model', 'op_master_model', 'booking_agent_model', 'Ors_agent_report_model', 'waybill_no_model', 'hand_held_machine_model', 'api_provider_model','city_master_model','credit_account_detail_model', 'office_master_model'));
        $this->load->library("Excel");
        $this->load->library("Datatables");
        $this->load->library('Pdf');
    }

 public function index() 
 {
        $data = array();
        $data['tran_type_data'] = array("failed" => "Failed Transaction",
            "success" => "Success Transaction",
            "incomplete" => "Incomplete Transaction",
            "cancel" => "Cancel Transaction",
            "pfailed" => "Payment Failed",
            "psuccess" => "Payment Success"
        );

    load_back_view(ORS_AGENT_TICKET_SUMMARY_REPORT_VIEW, $data);
 }
 
	 public function ors_agent_ticket_summary_report()
	 {
		$data = array();		
		$user_id = strtolower($this->session->userdata('user_id'));
		$role_name = strtolower($this->session->userdata('role_name'));
		$data["offices"] = array();
		$data["multi_office_agent"] = "";
		$data['city']=$this->city_master_model->get_all_city();
				
		load_back_view(ORS_AGENT_ADMIN_TICKET_SUMMARY_REPORT_VIEW, $data);
		
	}

	function get_ors_agent_users()
	{
		$user_id = strtolower($this->session->userdata('user_id'));
		$input=$this->input->post();
		$office_code = $input['agent_office_code'];		
		$user_result = array();
		if(is_string($office_code) && $office_code != '') {			
			$this->db->select("u.id as id,u.display_name");
			$this->db->from("users u");
			$this->db->where('u.parent_id', $user_id);
			$this->db->where('u.office_code', $office_code);
			$query = $this->db->get();
			$user_result = $query->result_array();		
		}		
		echo json_encode($user_result);				
	}
 
  function ors_agent_ticket_summary_report_result()
    {		
		$input=$this->input->post();
        $summary = $this->Ors_agent_report_model->summary_bos_tkt_detail_agent($input,'json');
	    echo $summary;
    }
	
	function ors_agent_users_ticket_summary_report_result()
    {		
		$input=$this->input->post();
        $summary = $this->Ors_agent_report_model->summary_bos_tkt_detail_agent_users($input,'json');
	    echo $summary;
    }
	
	function ors_agent_admin_ticket_summary_report_result()
    {		
		$input=$this->input->post();
        $summary = $this->Ors_agent_report_model->summary_bos_tkt_detail_agent_admin($input,'json');
	    echo $summary;
    }
	
	public function agent_ticket_summary_excel()
	{
            $input = $this->input->get();
            $ticket_result = $this->Ors_agent_report_model->summary_bos_tkt_detail_agent($input,'array');

            $fields = array("Sr No.", "Booking Date", "Ticket Count", "Passenger Count", "Ticket Fare", "Tax Amt", "Operator Service Charge", "Agent Service Charge", "Commission", "TDS", 
                "Total Ticket Fare", "Amount Paid","Cancellation Charge","Refund Amt","Profit Earned (INR)");
            $stockArr = array();
            $sr_no = 1;

            if(!empty($ticket_result))
            {
                foreach ($ticket_result['aaData'] as $key => $value) {
                   // $act_rev = $value['Reveue'] - $value['tot_can'];
                    $data[] = array('Sr no' => $sr_no,
                        'Booking Date' => $value['booking_date'],
                        'Ticket Count' => $value['total_ticket'],
                        'Passenger Count' => $value['total_passangers'],
                        'Ticket Fare' => $value['total_basic_fare'],
                        'Tax Amt' => $value['tax'],
                        'Operator Service Charge' => $value['op_service_charge'],
                        'Agent Service Charge' => $value['agent_service_tax'],
                        'Commission' => $value['agent_commission'],
                        'TDS' => $value['TDS'],
                        'Total Ticket Fare' => $value['total_fare'],
                        'Amount Paid' => $value['amt_paid'],
                        'Cancellation Charge' => $value['cancellation_charge'],
                        'Refund Amt' => $value['refund_amt'],
                        'Profit Earned (INR)' => $value['profit'],
                        );
                    $sr_no++;
                }
            }
            else
            {
                    $data = array(array());
            }

                $this->excel->data_2_excel('agent_ticket_summary_report' . date('Y-m-d') . '_' . time() . '.xls', $data);
                die;
        
	}
	
	public function agent_users_ticket_summary_excel()
	{
            $input = $this->input->get();
            $ticket_result = $this->Ors_agent_report_model->summary_bos_tkt_detail_agent_users($input,'array');

            $fields = array("Sr No.", "Booking Date", "Ticket Count", "Passenger Count", "Ticket Fare", "Tax Amt", "Operator Service Charge", "Agent Service Charge", "Commission", "TDS", 
                "Total Ticket Fare", "Amount Paid","Cancellation Charge","Refund Amt","Profit Earned (INR)");
            $stockArr = array();
            $sr_no = 1;

            if(!empty($ticket_result))
            {
                foreach ($ticket_result['aaData'] as $key => $value) {
                   // $act_rev = $value['Reveue'] - $value['tot_can'];
                    $data[] = array('Sr no' => $sr_no,
                        'Booking Date' => $value['booking_date'],
                        'Ticket Count' => $value['total_ticket'],
                        'Passenger Count' => $value['total_passangers'],
                        'Ticket Fare' => $value['total_basic_fare'],
                        'Tax Amt' => $value['tax'],
                        'Operator Service Charge' => $value['op_service_charge'],
                        'Agent Service Charge' => $value['agent_service_tax'],
                        'Commission' => $value['agent_commission'],
                        'TDS' => $value['TDS'],
                        'Total Ticket Fare' => $value['total_fare'],
                        'Amount Paid' => $value['amt_paid'],
                        'Cancellation Charge' => $value['cancellation_charge'],
                        'Refund Amt' => $value['refund_amt'],
                        'Profit Earned (INR)' => $value['profit'],
                        );
                    $sr_no++;
                }
            }
            else
            {
                    $data = array(array());
            }

                $this->excel->data_2_excel('agent_ticket_summary_report' . date('Y-m-d') . '_' . time() . '.xls', $data);
                die;
        
	}
	
	public function agent_admin_ticket_summary_excel()
	{
            $input = $this->input->get();
            $ticket_result = $this->Ors_agent_report_model->summary_bos_tkt_detail_agent_admin($input,'array');

            $fields = array("Sr No.", "Booking Date", "Ticket Count", "Passenger Count", "Ticket Fare", "Tax Amt", "Operator Service Charge", "Agent Service Charge", "Commission", "TDS", 
                "Total Ticket Fare", "Amount Paid","Cancellation Charge","Refund Amt","Profit Earned (INR)");
            $stockArr = array();
            $sr_no = 1;

            if(!empty($ticket_result))
            {
                foreach ($ticket_result['aaData'] as $key => $value) {
                   // $act_rev = $value['Reveue'] - $value['tot_can'];
                    $data[] = array('Sr no' => $sr_no,
                        'Booking Date' => $value['booking_date'],
                        'Ticket Count' => $value['total_ticket'],
                        'Passenger Count' => $value['total_passangers'],
                        'Ticket Fare' => $value['total_basic_fare'],
                        'Tax Amt' => $value['tax'],
                        'Operator Service Charge' => $value['op_service_charge'],
                        'Agent Service Charge' => $value['agent_service_tax'],
                        'Commission' => $value['agent_commission'],
                        'TDS' => $value['TDS'],
                        'Total Ticket Fare' => $value['total_fare'],
                        'Amount Paid' => $value['amt_paid'],
                        'Cancellation Charge' => $value['cancellation_charge'],
                        'Refund Amt' => $value['refund_amt'],
                        'Profit Earned (INR)' => $value['profit'],
                        );
                    $sr_no++;
                }
            }
            else
            {
                    $data = array(array());
            }

                $this->excel->data_2_excel('agent_ticket_summary_report' . date('Y-m-d') . '_' . time() . '.xls', $data);
                die;
        
	}
	
	function ors_agent_wallet_transaction_report()
	{
            $data = array();
            eval(FME_AGENT_ROLE_NAME);
                    $user_role_name=$this->session->userdata('role_name');
                    if(in_array($this->session->userdata('role_name'),$fme_agent_role_name)) {
                    $data['credit_detail'] = $this->credit_account_detail_model->where(array("user_id" => $this->session->userdata('user_id')))->as_array()->find_all();
                    $data['multioffice_flag'] = $this->users_model->column('id,multioffice_flag')->where(array("id" => $this->session->userdata('user_id')))->as_array()->find_all();
                    load_back_view(ORS_AGENT_WALLET_TRANSACTION_REPORT_VIEW, $data);
                }
            else
                {   
                    $data['agent_name'] = $this->users_model->ors_agent_details();
                    $data['city'] = $this->city_master_model->get_all_city();
                    load_back_view(ORS_AGENT_WALLET_TRANSACTION_REPORT_ADMIN, $data);
                }
	}
	
	function ors_agent_wallet_transaction_result()
        { 
            $input=$this->input->post();
            $summary = $this->Ors_agent_report_model->agent_wallet_transaction($input,'json');
                echo $summary;
        }
	
	function agent_wallet_transaction_excel()
	{
        $input = $this->input->get();
	$wallet_result = $this->Ors_agent_report_model->agent_wallet_transaction($input,'array');
        $stockArr = array();
        $sr_no = 1;
            foreach ($wallet_result['aaData'] as $key => $value) {
                $data_new['Sr no'] = $sr_no;
                $data_new['Wallet Opening Balance'] = $value['opening_balance'];
                if($input['flag'] == 0){
                $data_new['Opening Credit'] = $value['virtual_amt_before_trans'];
                }
                $data_new['Type'] = $value['transaction_type'];
                $data_new['Txn Type'] = $value['title'];
                $data_new['Txn Amt'] = $value['txn_amt'];
                $data_new['Txn Date'] = $value['txn_date'];
                $data_new['Rokad Ref No'] = $value['boss_ref_no'];
                $data_new['Wallet Closing Balance'] = $value['wallet_amt_after_trans'];
                if($input['flag'] == 0){
                $data_new['Closing Credit'] = $value['virtual_amt_after_trans'];
                $data_new['Outstanding'] = $value['outstanding_amount'];
                }
                $data_new['Txn Done By'] = $value['done_by'];
                $data_new['Booked By'] = $value['display_name'];
                if($input['flag2'] == 1){
                $data_new['Office Name'] = $value['office_name'];
                }
                $data_new['Transaction Remarks'] = $value['comment'];
                $data_new['Wallet Transaction ID'] = $value['id'];
                $data[] = $data_new;
                $sr_no++;
            }
			
            $this->excel->data_2_excel('agent_wallet_transaction_report' . date('Y-m-d') . '_' . time() . '.xls', $data);
            die;
        
	}
	
	function ors_agent_wallet_transaction_admin()
    { 
        $input=$this->input->post();
        $summary1 = $this->Ors_agent_report_model->agent_wallet_transaction_for_admin($input,'json');
	    echo $summary1;
    }
	
	function agent_wallet_transaction_admin_excel()
	{
        $input = $this->input->get();
		
        $wallet_result = $this->Ors_agent_report_model->agent_wallet_transaction_for_admin($input,'array');

        $fields = array("Sr No.", "Agent Name", "Email ID", "City", "Wallet Opening Balance", "Opening Credit","Txn ID", "Txn Type", "Credit","Debit", "Txn Date", "Rokad Ref No.","Wallet Closing Balance","Closing Credit","Outstanding","Txn Done By","Txn Remarks");
        $stockArr = array();
        $sr_no = 1;
      

            foreach ($wallet_result['aaData'] as $key => $value) {
               
					$data[] = array('Sr no' => $sr_no,
                    'Agent Name' => $value['display_name'],
                    'Email ID' => $value['email'],
                    'City' => $value['stCity'],
                    'Wallet Opening Balance' => $value['opening_balance'],
                    'Opening Credit' => $value['opening_credit'],
					'Txn ID'=>$value['id'],
                    'Txn Type' => $value['title'],
                    'Credit' => $value['credit'],
                    'Debit' => $value['debit'],
					'Txn Date'=> $value['txn_date'],
					'Rokad Ref No.'=> $value['bos_ref'],
					'Wallet Closing Balance'=> $value['closing_balance'],
					'Closing Credit'=> $value['closing_credit'],
					'Outstanding'=> $value['outstanding_amount'],
					'Txn Done By'=> $value['added_by_user'],
					'Txn Remarks'=> $value['comment']
                    );
                $sr_no++;
            }
			
            $this->excel->data_2_excel('agent_wallet_transaction_report' . date('Y-m-d') . '_' . time() . '.xls', $data);
            die;
        
	}
}
