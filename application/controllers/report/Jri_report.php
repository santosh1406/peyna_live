<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jri_report extends CI_Controller {

    public function __construct() {
        parent::__construct();
        is_logged_in();
        set_time_limit(0);

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);
        
        $this->load->library(array('Excel', 'Datatables', 'Pdf', 'pagination'));
        $this->load->helper("url");
    }

    public function index() {
        $data = array();
        load_back_view(JRI_TRANSACTION_REPORT,$data);
    }

    public function jri_reprots() {
        $input = $this->input->post();
        $data = $this->jri_report_details($input, 'json');
        echo $data;
    }

    
    public function jri_reports_excel() {
        $input = $this->input->get();
        $result_data  = $this->jri_report_details($input, 'array');
        $sr_no = 1;
        $data = array();
        foreach ($result_data['aaData'] as $key => $value) {
                  $data[] = array(  'Sr. No.' => $sr_no, 
                                    'Order No.' => $value['order_no'], 
                                    'Order Date' => $value['order_date'], 
                                    'Order Time' => $value['order_time'], 
                                    'Mobile/ DTH No' => $value['mobile_dthNO'], 
                                    'Nick Name' => $value['nick_name'], 
                                    'Amount' => $value['amount'], 
                                    'Service Name' => $value['service_name'],
                                    'Service Provider' => $value['service_provider'],
                                    'Location Name'=> $value['location_name'],
                                    'Service Type' => $value['service_type'],
                                    'Status' => $value['status'],
                                    'Reason'=> $value['reason']);
                                    $sr_no++;
        }
        $this->excel->data_2_excel('jri_transaction_report_' . time() . '.xls', $data);
        die;

     }
     
    public function jri_report_details($input, $response_type = '') {
        //echo '<pre>'; print_r($input); die();
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');
        $this->datatables->select('1,
                                jri.transactionReference as order_no,
                                DATE_FORMAT(jri.created_at,"%d-%b-%Y") as order_date,
                                DATE_FORMAT(jri.created_at,"%h:%i %p") as order_time,
                                jri.mobileno as mobile_dthNO,
                                jri.systemReference as nick_name,
                                jri.amount,
                                (CASE WHEN  jri.`servicetype` = "M" THEN "Mobile Recharge" WHEN jri.`servicetype` = "D" THEN "DTH Recharge" WHEN jri.`servicetype` = "R" THEN "Data Recharge" ELSE "NULL" END) as service_name,
                                jri.`provider` as service_provider,
                                jri.`location` as location_name,
                                (CASE WHEN  jri.`isPostpaid` = "Y" THEN "Postpaid" WHEN jri.`servicetype` = "M" AND jri.`isPostpaid` = "" THEN "Prepaid" ELSE "sdsds" END) as service_type,
                                jri.`status` as status,
                                jri.`reason` as reason');

        $this->datatables->from('jri_transaction_details jri');
        
        $this->datatables->join('recharge_transaction rt', 'rt.transaction_id = jri.transactionReference', 'inner');
        $this->datatables->join('wallet_trans wt', 'wt.transaction_no = rt.wallet_tran_id', 'inner');
        $this->datatables->join('users u', 'u.id = wt.user_id', 'inner');
        /*
        if (!empty($session_level) && isset($session_level)) {
            $this->datatables->where('u.id', $session_id);
        }*/
        
        if ($input['from_date']) {
            $this->datatables->where("DATE_FORMAT(jri.created_at, '%Y-%m-%d')>=", date('Y-m-d', strtotime($input['from_date'])));
        }
        if ($input['to_date']) {
            $this->datatables->where("DATE_FORMAT(jri.created_at, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }
        if ($session_level == RETAILOR_LEVEL) {
            $this->datatables->where("wt.user_id", $session_id);
        }
        
        $this->datatables->group_by('jri.transactionReference');
        $this->db->order_by('jri.created_at');
        if ($response_type != '') {
            $data = $this->datatables->generate($response_type);
        } else {
            $data = $this->datatables->generate();
        }
        //last_query(1);
        return $data;
    }

   public function statatics_reports()
   {
        $fromDate = $this->input->post('from_date');
        $toDate = $this->input->post('to_date');
        if($fromDate && $toDate){
             $fromDate = $this->input->post('from_date');
             $toDate = $this->input->post('to_date');
        } elseif($fromDate && !$toDate) {
            $fromDate = $this->input->post('from_date');
             $toDate = date('Y-m-d');
        }elseif(!$fromDate && $toDate) {
            $fromDate = date('Y-m-d');
             $toDate = $this->input->post('to_date');
        }else {
            $fromDate = date('Y-m-d');
            $toDate = date('Y-m-d');
        }
        
        
        $this->load->model('jri_report_model');
        $data = $operators = $data['statusWiseReport'] =$data['operatorWise'] =$data['reasonsWiseReport']= $operatorWise = array();
        $lastProvider = $data['reasons'] = '';
        
        /** Status wise reports summary**/
        $data['statusWiseReport'] = $this->jri_report_model->statusWiseReport($fromDate, $toDate);
        if($data['statusWiseReport']) {
            foreach ($data['statusWiseReport'] as $val) {
                $totalStatus += $val['count'];
            } 
        }
        $data['totalStatus'] = $totalStatus;
        
         /** Operators wise reports summary**/
        $operators = $this->jri_report_model->operatorsWiseReport($fromDate, $toDate);
        if($operators) {
            foreach ($operators as $key => $val) {
                    $provider['count'] = $val['count'];
                    $operatorWise[$val['provider']][$val['status']] = $provider;
                    $operatorWise[$val['provider']]['total'] += $val['count'];
                    $goperatorWise['Grand '.$val['status']] += $val['count'];
                    $goperatorWise['grantTotal'] += $val['count'];
            }
        }
        //echo '<pre>'; print_r($operatorWise); die();
        $data['operatorWise'] = $operatorWise;
        $data['goperatorWise'] = $goperatorWise;
        
        /** Reasons wise reports summary**/
        $data['reasonsWiseReport'] = $this->jri_report_model->reasonsWiseReport($fromDate, $toDate); 
        
        if($data['reasonsWiseReport']) {
            foreach ($data['reasonsWiseReport'] as $val) {
                $totalReason += $val['count'];
            }
            $data['reasons'] = $totalReason;
        }
        $data['from_date'] = $fromDate;
        $data['to_date'] = $toDate;
        //echo '<pre>'; print_r($data); die();
        load_back_view(JRI_STATASTICS_REPORT,$data);
   }
}
