<?php 
class Hhm_on extends CI_Controller {
   var $db_name;
    function __construct() {
        parent::__construct();
        is_logged_in();
       
        $this->load->model(array('cancel_ticket_model','op_master_model','report_model','agent_model'));
        $this->load->library("Excel");
        $this->load->library("Datatables");
        $this->load->library('Pdf');
         $this->db_name=get_database('default');
    }
    
    function index()
    {    
        $data=array();
        $data['waybill_no']=$this->report_model->waybill_no_data();
       
        $data['agent_name']=$this->agent_model->where(array('is_status'=>'1','is_deleted'=>'0'))->find_all();
        
        load_back_view(HHM_ON_REPORT_VIEW,$data)  ;
    }
    
     function list_star_seller() {
       
        $input=$this->input->post();
     
        $user_id = $this->session->userdata("user_id");
        $objDatatables = new Datatables();
          
        $objDatatables->set_database('upsrtc_current_booking');
       
        $this->datatables->select('bs.BUS_STOP_CD,w.WAYBILL_NO,w.DUTY_DT,'
                . 'ag.bus_stop_name,td.route_no,'
                . 'w.TICKET_AMT_PER_DAY,w.COLLECTION_TM,sum(td.full_amount) as fa,'
                . 'sum(td.half_amount) as amount');
         
        $this->datatables->from('waybillprogramming w'); 
        $this->datatables->join('ticket_data td', 'td.waybill_no=w.waybill_no','inner');
        $this->datatables->join($this->db_name.'.agent_master am', 'am.agent_id=w.AGENT_ID','inner');
        $this->datatables->join($this->db_name.'.ag_star_seller_detail ag', 'ag.agent_id =am.agent_id','left');
        $this->datatables->join('bus_stops bs','ag.bus_stop_id=bs.BUS_STOP_ID','left');
        
        if(isset($input) && count($input) > 0){
           $input['user_id']       =$this->session->userdata('user_id');
            
            if(isset($input['from_date']) && $input['from_date']!=''){
                $this->datatables->where("w.DUTY_DT >=",date('Y-m-d', strtotime($input['from_date'])));
              //  $this->datatables->where("w.COLLECTION_TM BETWEEN  '".date('Y-m-d 00:00:00', strtotime($input['from_date'])) ."' AND  '".date('Y-m-d 23:59:59', strtotime($input['to_date']))."' ");
                
                
            } 
            
            if(isset($input['to_date']) && $input['to_date']!=''){
                $this->datatables->where("w.DUTY_DT <=",date('Y-m-d', strtotime($input['to_date'])));
               // $this->datatables->where("w.COLLECTION_TM BETWEEN '".date('Y-m-d 00:00:00', strtotime($input['to_date'])) ."' AND '".date('Y-m-d 23:59:59', strtotime($input['to_date']))."' ");
                
                
            } 
            
            if(isset($input['waybill_no']) && $input['waybill_no']!=''){
                $this->datatables->where('w.WAYBILL_NO',$input['waybill_no']);
            } 
            
            if(isset($input['agent_id']) && $input['agent_id']!=''){
                $this->datatables->where('w.AGENT_ID',$input['agent_id']);
            }         
       }
        $this->datatables->group_by('w.waybill_no', 'desc');
        $data = $this->datatables->generate('json'); 
        
        echo $data;
    }
        
    public function export_pdf() { 
       $input=$this->input->get();
      
       $star_seller       = $this->report_model->star_seller_hhm_on_data($input);
       
       $pdf_gen           = $this->generatePdf($star_seller);
       show($pdf_gen,1);
       
    }
   
    public function generatePdf($result)
    {   
        ini_set("memory_limit", "512M");
        ob_start(); 
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('Star seller waybill sign on report');
        $pdf->SetHeaderMargin(20);
        $pdf->SetTopMargin(10);
        $pdf->setFooterMargin(20);
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);        
        $pdf->SetAutoPageBreak(true,PDF_MARGIN_FOOTER);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');
        $pdf->AddPage();
         $x = $pdf->pixelsToUnits('10');
        $y = $pdf->pixelsToUnits('10');
        $font_size = $pdf->pixelsToUnits('18');
        $txt = '';
        
        $pending_signoff=0;
        $sum_total =0;
        $full_amt=0;
        $half_amt=0;
        $clos_bal=0;
        
        $tot_ticket=0;
        $tot_sign_on=0;
        $tot_signoff=0;
        $tot_overdue=0;
        $pending_signoff=0;
        
        $pdf->SetFont ('helvetica', '', $font_size , '', 'default', true );
        $pdf->Text  ( $x, $y, $txt, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
        
        $html='<center>Star seller waybill sign on </center><br><br>
            <table width="100%" border="1" cellspacing="0" cellpadding="0"  class="table table-hover">
                <thead><tr style="background-color:gray; font-weight:bold;"><th>Sr No</th>
                 <th>Star Seller Code</th>
                                    <th >WayBill No </th>
                                    <th >Signon Date Time </th>
                                    <th>Star Seller Point</th>
                                    <th>No Routes</th>
                                    <th> Opening Balance</th>
                                    <th>Signoff date Time</th>
                                    <th> Ticket Sold Amount</th>
                                     <th>Closing Balance</th>

                </tr></thead><tbody>';
                    $sr_no=1;
                
        foreach ($result as $key =>$value): 

            $sum_total =0;
            $full_amt=0;
            $half_amt=0;
            $clos_bal=0;
            
            
            
        $sum_total = ((isset($value['fa'])?$value['fa']:'0') + (isset($value['amount'])?$value['amount']:'0'))/100;
        $tot_ticket =$tot_ticket+$sum_total;

        $clos_bal=((isset($value['TICKET_AMT_PER_DAY'])? $value['TICKET_AMT_PER_DAY']:'0')-$sum_total);


        $lock_date=date('Y-m-d', strtotime($value['DUTY_DT']. ' + 10 days'));


         if($value['COLLECTION_TM'] < $lock_date ) { 
             $show_msg='Not Alert';
             
         }
         else { 
             $show_msg='Alert';
             $tot_overdue=$tot_overdue+1;
         }


        if($value['COLLECTION_TM'] =='' )  { 
            $status='SignOn';
            $tot_sign_on=$tot_sign_on+1;
            $pending_signoff=$pending_signoff+1;
            
        }
        else {
            $status='SignOff';
            $tot_signoff=$tot_signoff+1;
        }
            
            $html.="<tr><td>".$sr_no ."</td>";
            $html.="<td>".$value['BUS_STOP_CD']."</td>";
            $html.="<td>".$value['WAYBILL_NO']."</td>";
            $html.="<td>".$value['DUTY_DT']."</td>";
            $html.="<td>".$value['bus_stop_name']."</td>";
            $html.="<td>".$value['route_no']."</td>"; 
            $html.="<td>".$value['TICKET_AMT_PER_DAY']."</td>";
            $html.="<td>".$value['COLLECTION_TM'] ."</td>";
            $html.="<td>".$sum_total ."</td>";

            $html.="<td>".$clos_bal."</td>";
            $html.="<td>".$lock_date ."</td>";
            $html.="<td>".$show_msg."</td>";
            $html.="<td>".$status ."</td>";

            $html .=  "</tr>";
                        
                        
        $sr_no++;
        endforeach;
        $html.='</tbody></table>';
        
         $html.='<div align="right">
                                <table class="table">
                                    <tbody><tr>
                                        <th style="width:50%">Total Signon:</th>
                                        <td><span id="tot_signon">'.$tot_sign_on.'</span></td>
                                    </tr>
                                    <tr>
                                        <th>Total Signoff:</th>
                                        <td><span id="tot_signoff">'.$tot_signoff.'</span></td>
                                    </tr>
                                    <tr>
                                        <th>Pending Signoff (except Overdue):</th>
                                        <td><span id="tot_pending">'.$pending_signoff.'</span></td>
                                    </tr>
                                    <tr>
                                        <th> Total Overdue agents:</th>
                                        <td><span id="tot_overdue">'.$tot_overdue.'</span></td>
                                    </tr>
                                    <tr>
                                        <th> Total Ticket Sold Amount:</th>
                                        <td><span id="tot_tickets">'.$tot_ticket.'</span></td>
                                    </tr>
                                </tbody></table>
                            </div>';
        $pdf->writeHTML($html, true, false, true, false, '');       
      
        $pdf->Output('Star seller waybill sign on off report.pdf', 'D');
        die;
    }
    
    function print_report() {
        $data['authorized_agents']    = $this->report_model->star_seller_hhm_on_data();
        load_back_view(STAR_SELLER_REPORT_VIEW,$data);
    }
    
    public function export_excel()
    {
        $input=$this->input->get();
        $star_seller       = $this->report_model->star_seller_hhm_on_data($input);
        
        $fields = array("Star Seller Code","WayBill No","Signon Date Time","Star Seller Point","No Routes","Opening Balance","Singoff date Time","Ticket Sold Amount");
        $stockArr = array();
              
        if(count($star_seller) > 0)
        {
            foreach ($star_seller as $row=>$value)
            {
                $TempreportArr = array();
                $sum_total =0;
                $full_amt=0;
                $half_amt=0;
                $clos_bal=0;
            
            
            
                $sum_total = ((isset($value['fa'])?$value['fa']:'0') + (isset($value['amount'])?$value['amount']:'0'))/100;


                $clos_bal=((isset($value['TICKET_AMT_PER_DAY'])? $value['TICKET_AMT_PER_DAY']:'0')-$sum_total);


                $lock_date=date('Y-m-d', strtotime($value['DUTY_DT']. ' + 10 days'));


                if($value['COLLECTION_TM'] < $lock_date ) { $show_msg='Not Alert';}
                else { $show_msg='Alert';}


                if($value['COLLECTION_TM'] =='' )  {
                    $status='SignOn';
                    
                }
                else {
                    $status='SignOff';
                    
                }
                array_push($TempreportArr,isset($value['BUS_STOP_CD'])?$value['BUS_STOP_CD']:"--NA--");
                array_push($TempreportArr,isset($value['WAYBILL_NO'])?$value['WAYBILL_NO']:"--NA--");
                array_push($TempreportArr,isset($value['DUTY_DT'])?$value['DUTY_DT']:"--NA--");
                array_push($TempreportArr,isset($value['bus_stop_name'])?$value['bus_stop_name']:"--NA--");
                array_push($TempreportArr,isset($value['route_no'])?$value['route_no']:"--NA--");
                array_push($TempreportArr,isset($value['TICKET_AMT_PER_DAY'])?$value['TICKET_AMT_PER_DAY']:"--NA--");
                
                array_push($TempreportArr,isset($value['COLLECTION_TM'])?$value['COLLECTION_TM']:"--NA--");
                
                array_push($TempreportArr,isset($sum_total)?$sum_total:"--NA--");
                array_push($TempreportArr,isset($clos_bal)?$clos_bal:"--NA--");
                array_push($TempreportArr,isset($lock_date)?$lock_date:"--NA--");
                array_push($TempreportArr,isset($show_msg)?$show_msg:"--NA--");
                array_push($TempreportArr,isset($status)?$status:"--NA--");
                
                $stockArr[] = $TempreportArr;
            }
        }
        
       // echo count($stockArr);
       $this->exportToExcel($fields,$stockArr,'star_seller_waybill_sign_on');    
    } 
    
    public function exportToExcel($fields,$data,$title = 'test',$actionName = 'download')
    {
       if(!is_array($fields) && count($fields) == 0)
            return;
        
        // Starting the PHPExcel library
        $this->load->library('excel');
       
            $objPHPExcel = new PHPExcel();
            //Set Borders 
            $styleArray = array(
                'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
             );
             $objPHPExcel->getDefaultStyle()->applyFromArray($styleArray);

         //Set background of first row    
        $objPHPExcel->getActiveSheet()
        ->getStyle('A1:M1')
        ->getFill()
        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
        ->getStartColor()
        ->setARGB('FF808080');
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
        //Font size of column names
        $objPHPExcel->getActiveSheet()->getStyle('1')->getFont()->setSize(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(19);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);

        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'Sr.No');
        $col = 1;
        foreach ($fields as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $col++;
        }
           
        // Fetching the table data
        $row = 2;
        if(is_array($data) && count($data) > 0){        
            foreach($data as $dataRow){
                $col1 = 0;
                foreach($dataRow as $dataSingle){
                    if((int)$col1 === 0){
                         $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col1, $row, ($row-1));
                          $col1++;
                    }
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col1, $row, $dataSingle);
                    $col1++;
                    //Height of the row
                    $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(23);
                }
                $row++;             
            }
        }     
        if(strtolower($actionName) == 'download'){ 
            $this->forceDownloadExcel($objPHPExcel,$title);
        }
        
         
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'HTML');
        $objWriter->save('php://output');
      
        // End of PHPExcel approach     
    }  
    
    /***
     * Force Doenload Excel File
     * @param $objectexcel PHPExcelObject 
     * $title string -- Name Of The file to be download --
     */
    private function forceDownloadExcel($objectexcel,$title)
    {
        $objWriter = PHPExcel_IOFactory::createWriter($objectexcel, 'Excel5');
       // Sending headers to force the user to download the file
       header('Content-Type: application/vnd.ms-excel');
       header("Content-Disposition: attachment; filename=".$title."_".date("d-m-Y").".xls");
       header('Cache-Control: max-age=0');
       $objWriter->save('php://output');
       exit;
    }
    
    
}
