<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tso_report extends CI_Controller {

    public function __construct() {
        parent::__construct();
        is_logged_in();
        set_time_limit(0);

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);
        
        $this->load->library(array('Excel', 'Datatables', 'Pdf', 'pagination'));
        $this->load->helper("url");
    }

    public function index() {
        $data = array(); 
     //   echo JRI_TRANSACTION_REPORT ;die;
        load_back_view(TSO_TRANSACTION_REPORT,$data);
    }

    public function tso_reports() {
        $input = $this->input->post();
        $data = $this->tso_report_details($input, 'json');
        echo $data;
    }

    
    public function tso_reports_excel() {
        $input = $this->input->get();
        $result_data  = $this->tso_report_details($input, 'array');
        $sr_no = 1;
        $data = array();
        foreach ($result_data['aaData'] as $key => $value) {
                  $data[] = array(  'Sr. No.' => $sr_no, 
                                    'Order No.' => $value['order_no'], 
                                    'Order Date' => $value['order_date'], 
                                    'Order Time' => $value['order_time'], 
                                    'Mobile/ DTH No' => $value['mobile_dthNO'], 
                                    'Amount' => $value['amount'], 
                                    'Service Name' => $value['service_name'],
                                    'Service Provider' => $value['service_provider'],
                                    'Service Type' => $value['service_type'],
                                    'Status' => $value['status'],
                                );
                  $sr_no++;
        }
        
        $this->excel->data_2_excel('tso_transaction_report_' . time() . '.xls', $data);
        die;

     }
     
    public function tso_report_details($input, $response_type = '') {
        //show($input,1);
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');
        $this->datatables->select('1,
                                tso.trans_reference as order_no,
                                DATE_FORMAT(tso.created_at,"%d-%b-%Y") as order_date,
                                DATE_FORMAT(tso.created_at,"%h:%i %p") as order_time,
                                tso.customer_number as mobile_dthNO,
                                tso.amount,
                                tso.request_type as service_name,
                                tso.`operator` as service_provider,
                                tso.plan_type as service_type,
                                tso.`status` as status
                                ');

        $this->datatables->from('tso_transaction_details tso');
        
        $this->datatables->join('recharge_transaction rt', 'rt.transaction_id = tso.transaction_id', 'inner');
        $this->datatables->join('wallet_trans wt', 'wt.transaction_no = rt.wallet_tran_id', 'inner');
        $this->datatables->join('users u', 'u.id = wt.user_id', 'inner');
        /*
        if (!empty($session_level) && isset($session_level)) {
            $this->datatables->where('u.id', $session_id);
        }*/
        
        if ($input['from_date']) {
            $this->datatables->where("DATE_FORMAT(tso.created_at, '%Y-%m-%d')>=", date('Y-m-d', strtotime($input['from_date'])));
        }
        if ($input['to_date']) {
            $this->datatables->where("DATE_FORMAT(tso.created_at, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }

        $this->datatables->group_by('tso.transaction_id');
        $this->db->order_by('tso.created_at');
        if ($response_type != '') {
            $data = $this->datatables->generate($response_type);
        } else {
            $data = $this->datatables->generate();
        }
        //show($data,1);
        return $data;
    }

   public function statatics_reports()
   {
        $fromDate = $this->input->post('from_date');
        $toDate = $this->input->post('to_date');
        if($fromDate && $toDate){
             $fromDate = $this->input->post('from_date');
             $toDate = $this->input->post('to_date');
        } elseif($fromDate && !$toDate) {
            $fromDate = $this->input->post('from_date');
             $toDate = date('Y-m-d');
        }elseif(!$fromDate && $toDate) {
            $fromDate = date('Y-m-d');
             $toDate = $this->input->post('to_date');
        }else {
            $fromDate = date('Y-m-d');
            $toDate = date('Y-m-d');
        }
        
        
        $this->load->model('tso_report_model');
        $data = $operators = $data['statusWiseReport'] =$data['operatorWise'] =$data['reasonsWiseReport']= $operatorWise = array();
        $lastProvider = $data['reasons'] = '';
        
        /** Status wise reports summary**/
        $data['statusWiseReport'] = $this->tso_report_model->statusWiseReport($fromDate, $toDate);
        if($data['statusWiseReport']) {
            foreach ($data['statusWiseReport'] as $val) {
                $totalStatus += $val['count'];
            } 
        }
        $data['totalStatus'] = $totalStatus;
        
         /** Operators wise reports summary**/
        $operators = $this->tso_report_model->operatorsWiseReport($fromDate, $toDate);
        //show($operators,1);
        if($operators) {
            foreach ($operators as $key => $val) {
                    $provider['count'] = $val['count'];
                    $operatorWise[$val['operator']][$val['status']] = $provider;
                    $operatorWise[$val['operator']]['total'] += $val['count'];
                    $goperatorWise['Grand '.$val['status']] += $val['count'];
                    $goperatorWise['grantTotal'] += $val['count'];
            }
        }
        //echo '<pre>'; print_r($operatorWise); die();
        $data['operatorWise'] = $operatorWise;
        $data['goperatorWise'] = $goperatorWise;
        
        /** Reasons wise reports summary**/
        $data['reasonsWiseReport'] = $this->tso_report_model->reasonsWiseReport($fromDate, $toDate); 
        
        if($data['reasonsWiseReport']) {
            foreach ($data['reasonsWiseReport'] as $val) {
                $totalReason += $val['count'];
            }
            $data['reasons'] = $totalReason;
        }
        $data['from_date'] = $fromDate;
        $data['to_date'] = $toDate;
        //echo '<pre>'; print_r($data); die();
        load_back_view(TSO_STATISTICS_REPORT,$data);
   }
}
