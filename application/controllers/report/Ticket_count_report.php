<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of Ticket_count_repot
     *
     * @author Sagar
     */
    class Ticket_count_report extends CI_Controller{
         public function __construct()
        {
            parent::__construct();

            is_logged_in();
            $this->load->model(array('tickets_model'));
            $this->load->model(array('users_model'));
            $this->load->model(array('agent_tickets_model'));
            $this->load->model(array('report_model'));
            $this->load->model(array('city_master_model'));
            $this->load->model(array('state_master_model'));
            $this->load->model(array('referrer_master_model'));
            $this->load->library("Excel");
            $this->load->helper('dompdf');
           // $this->load->helper('file');
            $this->load->library('Pdf');
            $this->load->library('Pagination');

        }
        
        public function index($id='') {  
            $per_page   = 10;
            $uri = $id;
            $page       = (isset($uri)&& $uri >0 ) ? $uri : 0;
            $index = $per_page*($page-1);
            $input = $this->input->post();
            if(!empty($input)){
                $data['from_date'] = $input['from_date'];
                $data['till_date'] = $input['till_date'];
                $data['trans_type'] = $input['trans_type'];
                $data['select_agent'] = $input['select_agent'];
                $data['select_state'] = $input['select_state'];
                $data['select_city'] = $input['select_city'];
                $data['select_limit'] = $input['select_limit'];
                $data['select_referrer'] = $input['select_referrer'];
            }
            eval(FME_AGENT_ROLE_ID);
            $data['role_id'] = $fme_agent_role_id;
            if($index <= 0){
                $index = 0;
            }
            $data['index'] = $index;
            if(!empty($data['trans_type'])){
                $this->session->set_userdata('trans_type',$data['trans_type']);
            }else if(empty($id) && ($this->uri->segment(3) != 'index')){
                $this->session->unset_userdata('trans_type');
            }

            $data['referrers']= $this->referrer_master_model->column('id, referrer_name,referrer_code')->as_array()->find_all();  
             $data['agents']= $this->report_model->users_data($data);
            $data['states']= $this->state_master_model->column('intStateId, stState')->as_array()->find_all();           
            $data['cities']= $this->city_master_model->column('intCityId, stCity')->as_array()->find_all();           
            
            if(!empty($data['trans_type'])){
                $this->session->set_userdata('trans_type',$data['trans_type']);
            }else if(empty($id) && ($this->uri->segment(3) != 'index')){
                $this->session->unset_userdata('trans_type');
            }
            
            if(!empty($data['select_state'])){
                $this->session->set_userdata('select_state',$data['select_state']);
            }else if(empty($id) && ($this->uri->segment(3) != 'index')){
                $this->session->unset_userdata('select_state');
            }
//         
            if(!empty($data['select_city'])){
                $this->session->set_userdata('select_city',$data['select_city']);
            }else if(empty($id) && ($this->uri->segment(3) != 'index')){
                $this->session->unset_userdata('select_city');
            }
            
            if(!empty($data['select_agent'])){
                $this->session->set_userdata('select_agent',$data['select_agent']);
            }else if(empty($id) && ($this->uri->segment(3) != 'index')){
                $this->session->unset_userdata('select_agent');
            }
             if(!empty($data['select_limit'])){
                $this->session->set_userdata('select_limit',$data['select_limit']);
             }
             else if(empty($id) && ($this->uri->segment(3) != 'index')){
                $this->session->unset_userdata('select_limit');
            }
            if(!empty($data['trans_type'])){
                $this->session->set_userdata('trans_type',$data['trans_type']);
            }else if(empty($id) && ($this->uri->segment(3) != 'index')){
                $this->session->unset_userdata('trans_type');
            }
            if(!empty($data['select_referrer'])){
                $this->session->set_userdata('select_referrer',$data['select_referrer']);
            }else if(empty($id) && ($this->uri->segment(3) != 'index')){
                $this->session->unset_userdata('select_referrer');
            }
          
               $data['ticket_report_result'] = $this->report_model->get_agent_ticket_count_report($per_page, $page);
            $inserted_by = array_column($data['ticket_report_result'], 'inserted_by');

            if(!empty($data['from_date'])){
                $this->session->set_userdata('from_date',$data['from_date']);
            }else if(empty($id) && ($this->uri->segment(3) != 'index')){
                $this->session->unset_userdata('from_date');
            }
            
            if(!empty($data['till_date'])){
                $this->session->set_userdata('till_date',$data['till_date']);
             }
             else if(empty($id) && ($this->uri->segment(3) != 'index')){
                $this->session->unset_userdata('till_date');
            }
            
            if((!empty($this->session->userdata('from_date')) || !empty($this->session->userdata('till_date')))){   
            $data['from_date']= $this->session->userdata('from_date');
            $data['till_date'] = $this->session->userdata('till_date');

            if(!empty($inserted_by))
            {
                $data['ticket_report_result_range'] = $this->report_model->get_agent_ticket_count_report_date($inserted_by);
            }
            
            $i = 0;
                foreach($data['ticket_report_result'] as $key1 => $value1){
                     foreach($data['ticket_report_result_range'] as $key2 => $value2){
                         if($data['ticket_report_result'][$key1]['inserted_by'] == $data['ticket_report_result_range'][$key2]['inserted_by']){     
                              $data['ticket_report_result'][$i] = array_merge($data['ticket_report_result'][$key1], $data['ticket_report_result_range'][$key2]);
                          }
                     }
                     $i++;
                } 
            }
        $total_rows = count($this->report_model->get_agent_ticket_count_report('',''));
        $data['total_rows']= $total_rows;
        
      
            $config["base_url"] = base_url() . "report/ticket_count_report/index";
                
        $config["total_rows"]        = $total_rows;
        $config['use_page_numbers']  = TRUE;
        $config["per_page"]          = $per_page;
        $config['num_links']         = $total_rows;
        $config['next_link']         = 'Next';
        $config['prev_link']         = 'Previous';
        
        $config['full_tag_open']     = '<ul class="pagination">';
        $config['full_tag_close']    = '</ul>';
        $config['prev_link']         = 'Previous';
        $config['prev_tag_open']     = '<li>';
        $config['prev_tag_close']    = '</li>';
        $config['next_link']         = 'Next';
        $config['next_tag_open']     = '<li>';
        $config['next_tag_close']    = '</li>';
        $config['cur_tag_open']      = '<li class="active"><a href="#">';
        $config['cur_tag_close']     = '</a></li>';
        $config['num_tag_open']      = '<li>';
        $config['num_tag_close']     = '</li>';

        $config['first_tag_open']    = '<li>';
        $config['first_tag_close']   = '</li>';
        $config['last_tag_open']     = '<li>';
        $config['last_tag_close']    = '</li>';

        $config['first_link']        = '&lt;&lt;';
        $config['last_link']         = '&gt;&gt;';
        if(empty($this->session->userdata('select_limit'))){
        };
            load_back_view(TICKET_COUNT_REPORT_VIEW,$data);
        }
        //put your code here

        public function ticket_count_report_export_excel()
       {
         $input = $this->input->get(); 

            $data1 = $this->report_model->get_agent_ticket_count_excel($input);

               $sr_no=1;
               $data = array();
                 foreach($data1['ticket_report_result'] as $key=> $value)
                { 
                
                  $data[]=array('Sr no'=> $sr_no  ,
                            'Agent Name' => $value['Agent_Name'],
                            'Agent Code' => $value['Agent_Code'],
                            'Agent Mobile No'=> $value['mobile_no']  ,
                            'State' => $value['state_name'],
                            'City'=>$value['city_name'],
                            'Referrer Name'=>$value['referrer_name'],
                            'Total Ticket'=>($value['Till_Date_Booking_Count']=='') ? 0:$value['Till_Date_Booking_Count'],
                            'Ticket Count for Date Range'=>($value['Till_Date_Booking_Count_Range'] == '' ) ? 0:$value['Till_Date_Booking_Count_Range'],
                            'Total Passengers'=> ($value['passengers'] =='')? 0:$value['passengers'],
                            'Passenger Count for Date Range'=> ($value['till_date_passengers'] =='') ? 0:$value['till_date_passengers'],
                            'Total Revenue'=>$value['Till_Date_Revenue'],
                            'Revenue for Date Range'=>($value['Till_Date_Revenue_Range'] == '')?"0.00":$value['Till_Date_Revenue_Range'],
                            'Total Markup'=>($value['Till_Date_Markup_Value'] == '')?"0.00":$value['Till_Date_Markup_Value'],
                            'Markup for Date Range'=>($value['Till_Date_Markup_Value_Range'] == '')?"0.00":$value['Till_Date_Markup_Value_Range'],
                            'Total Commission'=>($value['Till_Date_Commission']== '')?"0.00":$value['Till_Date_Commission'],
                            'Commission for Date Range'=> ($value['Till_Date_Commission_Range'] == '')?"0.00":$value['Till_Date_Commission_Range'],
                            'Last Ticket Booked Date'=>date('d-m-Y H:i:s',  strtotime($value['Last_Ticket_Booked_Date']))
                     );
                    $sr_no++;
                }
                $this->excel->data_2_excel('Agent_ticket_count_report'.time().'.xls', $data);
                die;  
        }

    public function export_pdf() {

       $input = $this->input->get();
       $i = 0;

       $data1 = $this->report_model->get_agent_ticket_count_excel($input);
       $pdf_gen = $this->generatePdf($data1);
    }
   
    public function generatePdf($result)
    {
        ini_set("memory_limit", "512M");
        ob_start();         
        /*$pdf = new Pdf('Landscape', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('Agent ticket count Report');
        $pdf->SetHeaderMargin(20);
        $pdf->SetTopMargin(10);
        $pdf->setFooterMargin(20);
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);        
        $pdf->SetAutoPageBreak(true,PDF_MARGIN_FOOTER);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');
        $pdf->AddPage();*/

        $pdf = new Pdf('L', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->setPageOrientation('L');
        $pdf->SetTitle('Agent ticket count Report');
        $pdf->SetHeaderMargin(20);
        $pdf->SetTopMargin(10);
        $pdf->setFooterMargin(20);
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);        
        $pdf->SetAutoPageBreak(true,PDF_MARGIN_FOOTER);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');
        $pdf->AddPage();
        
        $x = $pdf->pixelsToUnits('10');
        $y = $pdf->pixelsToUnits('10');
        $font_size = $pdf->pixelsToUnits('20');
        $txt = '';

        $pdf->SetFont ('helvetica', '', $font_size , '', 'default', true );
        $pdf->Text  ( $x, $y, $txt, false, false, true, 0, 0, 'C', false, '', 0, false, 'T', 'M', false );
        
        $html='<center>Agent Ticket Count Report:</center><br><br>
            <table width="100%" border="1" cellspacing="0" cellpadding="0"  class="table table-hover">
             <thead><tr>
            <th >Sr No.</th>
            <th >Agent Name</th>
            <th >Agent Mobile No</th>
            <th >State</th>
            <th >City</th>
            <th >Referrer Name</th>
            <th >Total Ticket</th>
            <th >Ticket Count for Date Range</th>
            <th >Total Passengers</th>
            <th >Passenger Count for Date Range</th>
            <th >Total Revenue</th>
            <th >Revenue for Date Range</th>
            <th >Total Markup</th>
            <th >Markup for Date Range</th>
            <th >Total Commission</th>
            <th >Commission for Date Range</th>
            <th >Last Ticket Booked Date</th>
           </tr></thead><tbody>';
                $sr_no=1;
                foreach ($result['ticket_report_result'] as $key =>$value): 
                   
                    $html.="<tr><td>".$sr_no ."</td>";
                    $html.="<td>".$value['Agent_Name']."</td>";
                    $html.="<td>".$value['mobile_no']."</td>";
                    $html.="<td>".$value['state_name']."</td>";
                    $html.="<td>".$value['city_name'] ."</td>";
                    $html.="<td>".$value['referrer_name'] ."</td>";
                    $html.="<td>".($value['Till_Date_Booking_Count']=='' ? '0':$value['Till_Date_Booking_Count'])."</td>";
                    $html.="<td>".($value['Till_Date_Booking_Count_Range']=='' ?'0':$value['Till_Date_Booking_Count_Range']) ."</td>";
                    $html.="<td>".($value['passengers']=='' ? '0':$value['passengers'])."</td>";
                    $html.="<td>".($value['till_date_passengers']=='' ? '0':$value['till_date_passengers'])."</td>";
                    $html.="<td>".$value['Till_Date_Revenue']."</td>";
                    $html.="<td>".($value['Till_Date_Revenue_Range']=='' ? "0.00":$value['Till_Date_Revenue_Range'])."</td>";
                    $html.="<td>".($value['Till_Date_Markup_Value']== '' ?"0.00":$value['Till_Date_Markup_Value'])."</td>";
                    $html.="<td>".($value['Till_Date_Markup_Value_Range'] ==''?"0.00":$value['Till_Date_Markup_Value_Range'])."</td>";
                    $html.="<td>".($value['Till_Date_Commission']== '' ?"0.00":$value['Till_Date_Commission'])."</td>";
                    $html.="<td>".($value['Till_Date_Commission_Range'] == ''?"0.00":$value['Till_Date_Commission_Range'])."</td>";
                    $html.="<td>".date('d-m-Y H:i:s',  strtotime($value['Last_Ticket_Booked_Date']))."</td></tr>";
                    
                    $sr_no++;
                endforeach;
        $html.='</tbody></table>';

        $pdf->writeHTML($html, true, false, true, false, '');       
        $pdf->Output('Agent_ticket_count_report.pdf', 'D');
        die;
    }
    
    }
    
