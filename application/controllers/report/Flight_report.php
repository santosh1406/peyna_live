<?php

class Flight_report extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        is_logged_in();
        set_time_limit(0);

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);
        
        $this->load->library(array('Excel'));
       
    }
    
    public function index() {
        $data = array();
        $data['flight_names'] = $this->get_flight_names();        
        load_back_view(FLIGHT_REPORT_VIEW,$data);
    }
    
    public function flight_details(){
        $input = $this->input->post();
        $data = $this->flight_booking_details($input, 'json');
        echo $data;
    }
    
    public function flight_booking_details($input, $response_type = '')
    {
         //echo '<pre>'; print_r($input); die();
        
        $this->datatables->select('1,
                                pass_det.first_name,
                                pass_det.age, 
                                pass_arr_det.flight_name,
                                pass.flight_no,
                                pass_arr_det.dep_city_name,
                                pass_arr_det.arr_city_name,
                                pass_arr_det.dep_date_time,
                                pass_arr_det.arr_date_time,
                                pass.total_amount,
                                pass.transaction_status
                                
                            ');

        $this->datatables->from('flight_passenger_details pass_det');        
        $this->datatables->join('flight_passenger_arrival_details pass_arr_det', 'pass_det.reference_id = pass_arr_det.ref_id', 'inner'); 
        $this->datatables->join('flight_passenger pass', 'pass.reference_id = pass_det.reference_id', 'inner');

        
        

        if (isset($input['from_date']) && $input['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(pass_arr_det.dep_date_time, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['to_date']) && $input['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(pass_arr_det.arr_date_time, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }
        
        if(isset($input['flight_names']) && $input['flight_names'] != '' ) {
            $this->datatables->where('pass_arr_det.flight_name',$input['flight_names']);
        }else {
            $this->datatables->like('pass_arr_det.flight_name',$input['flight_names']);
        } 
        
        if ($response_type != '') {
            $data = $this->datatables->generate($response_type);
        } else {
            $data = $this->datatables->generate();
        }
       // last_query(1);
        return $data;
    }
    
    public function get_flight_names()
    {        
        $this->db->distinct();
        $this->db->select('flight_name');
        $query = $this->db->get('flight_passenger_arrival_details');        
        return $query->result_array();
       
    }
}
