<?php

//Created by pooja kambali  on 29-09-2018 
class Dmt extends CI_Controller {

    public $sessionData;
    
    public function __construct() {
        parent::__construct();
        //$this->load->library('form_validation'); // previous
        //*** THIS ONE ****
        $this->load->library(array('form_validation','Excel'));
       
        //*** THIS ONE ****
        $this->load->model('Dmt_model');
        is_logged_in();
        $this->sessionData["id"] = $this->session->userdata('user_id');
        $this->sessionData["username"] = $this->session->userdata('username');
        $this->sessionData["password"] = $this->session->userdata('password');
        
         $this->header = array(
            'Content-Type: multipart/form-data; charset=utf-8',
            'X-API-KEY: ' . X_API_KEY . '',
        );
         

    }

    
    
    public function index() {
        $user_data = $this->session->userdata();
        $agent_id = $user_data['user_id'];
        $retailer_services = $this->retailer_service_mapping_model->getServiceByAgentId($agent_id);
        $service_array = array_column($retailer_services, 'service_id');
        if (in_array(Dmt, $service_array)) {
            load_back_view(DMT_VIEW);
        } else {
            $this->session->set_flashdata('msg', 'No access to module');
            redirect(base_url() . 'admin/dashboard');
        }
    }

    public function neft() {       
        $data = array();
        $input = $this->input->post();

        if (!empty($input)) {  
            $input['user_id'] = $this->sessionData['id'];
            $curl_url = base_url() . "rest_server/Dmt/neft";
            $config = array(
               array('field' => 'type', 'label' => 'Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select  type.')),
               array('field' => 'CustomerMobileNo', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|xss_clean|min_length[10]', 'errors' => array('required' => 'Please enter mobile number.')),
               array('field' => 'BeneIFSCCode', 'label' => 'IFSC Code', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter ifsc code.')),
               array('field' => 'BeneAccountNo', 'label' => 'Beneficiary Account Number.', 'rules' => 'trim|required|numeric|max_length[19]|min_length[19]|xss_clean', 'errors' => array('required' => 'Please enter Beneficiary Account number.')),
               array('field' => 'BeneName', 'label' => 'Beneficiary Name.', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Beneficiary Name.')),
               array('field' => 'Amount', 'label' => 'Amount', 'rules' => 'trim|required|numeric|less_than_equal_to['.MAX_DMT_AMT.']|xss_clean', 'errors' => array('required' => 'Please enter Amount.')),
               array('field' => 'CustomerName', 'label' => 'Customer Name.', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Customer Name.')),
               array('field' => 'rfu1', 'label' => 'Remarks', 'rules' => 'trim|xss_clean', 'errors' => array('required' => 'Please enter Remark')),
               array('field' => 'transfer_from', 'label' => 'transfer_from', 'rules' => 'trim|xss_clean', 'errors' => array('required' => 'Please enter Remark'))
                 );  

          if (form_validate_rules($config) == FALSE) {
                $data['error'] = $this->form_validation->error_array();
                load_back_view(DMT_VIEW,$data);               
            } else {
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dmt.log', $input, 'Input-Operator');
                $curlResult = curlForPostData_dmt_new($curl_url, $input, $this->sessionData, $this->header);
                
                $output = json_decode($curlResult);
                //echo"op: ";print_r($output); die;
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dmt.log', $output, 'Web DMT Response');
             // if(!empty($output)) { 
                    $result = json_decode($output->data);
//                    $this->session->set_tempdata('msg', $result->DisplayMessage, 3);
                     if(isset($output->data) && !empty($output->data)){
                         $this->session->set_tempdata('msg', $result->DisplayMessage, 3);
                    }else{
                          $this->session->set_tempdata('msg', $output->msg);
                    }
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dmt.log', $result, 'Web DMT Response');
                    redirect(site_url() . '/admin/dmt');

               /* }else {
                    $this->session->set_flashdata('msg_type', 'success');
                    $this->session->set_flashdata('msg', 'Something went wrong');
                    redirect(site_url() . '/admin/dmt');
                }*/
             }            
           
        }
      // redirect(base_url() . 'admin/Dmt');
   
    }
    

    public function transaction_list(){
      load_back_view(TRANSACTION_LIST1);             
        }
        
        public function transaction(){
        $data = $this->input->post();
       // print_r($data);die();
       // $this->datatables->select("'1',Clientuniqueid,type,CustomerMobileNo,BeneIFSCCode,BeneAccountNo,BeneName,Amount,CustomerName,created_on,message");//Prev code
        $this->datatables->select("'1',Clientuniqueid,type,CustomerMobileNo,BeneIFSCCode,BeneAccountNo,BeneName,Amount,CustomerName,created_on,message,customer_charge");
        $this->datatables->from('dmt_transfer_detail');
        
        
        
         if (isset($data['from_date']) && $data['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(created_on, '%Y-%m-%d') >=", date('Y-m-d', strtotime($data['from_date'])));
        }

        if (isset($data['to_date']) && $data['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(created_on, '%Y-%m-%d')<=", date('Y-m-d', strtotime($data['to_date'])));
        }
        $data = $this->datatables->generate('json');
        echo $data;
        
        }
        
        public function transaction_status(){
            
           
        $data = array();
        $input = $this->input->post();
           
        if (!empty($input)) {
            
          $curl_url = base_url() . "rest_server/Dmt/transaction_status";

            $config = array(
               array('field' => 'ClientUniqueID', 'label' => 'ClientUniqueID', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Client Number.')),
             
                 );
           
            
             if (form_validate_rules($config)){
        
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dmt.log', $input, 'Input-Operator');
                $curlResult = curlForPostData_dmt_new($curl_url, $input, $this->sessionData, $this->header);
                $output = json_decode($curlResult);
                $this->session->set_flashdata('message', $output->msg);
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dmt.log', $curlResult, 'Web Recharge Response');
               echo $curlResult;
           exit;
             }
             
           
        }
      // redirect(base_url() . 'admin/Dmt');
   
   
     load_back_view(TRANSACTION_STATUS);             
    }


    public function get_cc_from_slab(){
      $input = $this->input->post();
      $customer_charge = $this->Dmt_model->getCustomerCharge($input['amount']);
      
      $customer_charge = !empty($customer_charge) ? $customer_charge : 0;

      echo $customer_charge;
    }
    
     #******************* THIS ONE  **************************
     public function transaction_list_reports_excel() {
         
        $input = $this->input->get();
      
        $result_data  = $this->transaction_details($input,'array');
        $sr_no = 1;
        $data = array();
     
        foreach ($result_data['aaData'] as $key => $value) {
                  $data[] = array(  'Sr. No.' => $sr_no, 
                                    'Transaction Id.' => $value['Clientuniqueid'], 
                                    'Transaction Mode' => $value['type'], 
                                    'Mobile Number' => $value['CustomerMobileNo'], 
                                    'IFSC Code' => $value['BeneIFSCCode'], 
                                    'Account Number' => $value['BeneAccountNo'], 
                                    'Beneficiary Name' => $value['BeneName'],
                                    'Transaction Amount' => $value['Amount'],
                                    'Customer Name' => $value['CustomerName'],
                                    'Transaction Date' => $value['created_on'],
                                    'Status' => $value['message'],
                                    'Convenience charge' => $value['customer_charge'],
                                );
                  $sr_no++;
        }
        
        $this->excel->data_2_excel('transaction_details_report_'.time().'.xls',$data);
        die;

     }
     
      public function transaction_details($input,$response_type='') {
       
        $this->datatables->select("'1',Clientuniqueid,type,CustomerMobileNo,BeneIFSCCode,BeneAccountNo,BeneName,Amount,CustomerName,created_on,message,customer_charge");
        $this->datatables->from('dmt_transfer_detail');
        if (isset($input['from_date']) && $input['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(created_on, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['to_date']) && $input['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(created_on, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }
        //==================
        
          $session_level = $this->session->userdata('level');
        if ($session_level == RETAILOR_LEVEL) {
            $this->db->where("user_id = ".$this->session->userdata('user_id')."");
           
        }
        
        if ($response_type != '') {
            $data = $this->datatables->generate($response_type);
        } else {
            $data = $this->datatables->generate();
        }
        //===================
     //   $data = $this->datatables->generate('json');
        return $data;
     }
     #***************** THIS ONE ******************
    

}
