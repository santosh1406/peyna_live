<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Micro_atm_report extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        is_logged_in();
        set_time_limit(0);

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);
        
        $this->load->library(array('Excel'));
        $this->load->helper("url");
    }
    
    public function index(){
        
        $data['transaction_type'] = array(MATM_CASH_WITHDRAW=>'Cash Withdraw',MATM_BALANCE_ENQUIRY=>'Balance Enquiry',MATM_TRANSACTION_STATUS=>'Transaction Status');
        load_back_view(MATM_TRANSACTION_LIST_REPORT,$data);        
    }
    
    public function transaction() {
        $input = $this->input->post();
        $data = $this->matm_transaction_details($input, 'json');
        echo $data;        
    }
    
    public function matm_reports_excel() {
        $input = $this->input->get();
        $result_data  = $this->matm_transaction_details($input, 'array');
        //show($result_data,1);
        $sr_no = 1;
        $data = array();
        foreach ($result_data['aaData'] as $key => $value) {
                  $data[] = array(  'Sr. No.' => $sr_no, 
                                    'Client Id' => $value['ClientRefID'], 
                                    'agent_name' => $value['agent_name'],
                                    'mobile_no' => rokad_decrypt($value['mobile_no'], $this->config->item("pos_encryption_key")),
                                    'Transaction Mode' => $value['Service_name'], 
                                    'Account Number' => $value['AccountNo'],
                                    'Transaction Amount' => $value['TxnAmt'],
                                    'Available Balance' => $value['AvailableBalance'],
                                    'Transaction Date' => $value['TransactionDatetime'], 
                                    'Status' => $value['TxnDisplayMsg'],
                                    );
                                    $sr_no++;
        }
        $this->excel->data_2_excel('micro_atm_transaction_report_' . time() . '.xls', $data);
        die;
    }
    
    public function matm_transaction_details($input, $response_type = '')
    {
         //echo '<pre>'; print_r($input); die();
        $user_id = $this->session->userdata('user_id');
        $this->datatables->select('1,
                                request.ClientRefID, 
                                concat(u.first_name," ",u.last_name) as agent_name,
                                u.mobile_no, 
                                request.Service_name,
                                response.AccountNo,                                
                                response.TxnAmt,
                                response.AvailableBalance,
                                response.TransactionDatetime,
                                response.TxnDisplayMsg
                            ');

        $this->datatables->from('micro_atm_transaction_request request');        
        $this->datatables->join('micro_atm_transaction_response response', 'request.id = response.Transaction_id', 'inner');
        $this->datatables->join('users u', 'u.id = request.created_by', 'inner');   
        

        if (isset($input['from_date']) && $input['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(request.created_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['to_date']) && $input['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(request.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }
        
        if(isset($input['transaction_type']) && $input['transaction_type'] != '' ) {
            $this->datatables->where('request.SERVICEID',$input['transaction_type']);
        }else {
            $this->datatables->like('request.SERVICEID',$input['transaction_type']);
        } 
        
        $session_level = $this->session->userdata('level');
        if ($session_level == RETAILOR_LEVEL) {
            $this->db->where("request.created_by = ".$this->session->userdata('user_id')."");
           
        }
        
        
        
        if ($response_type != '') {
            $data = $this->datatables->generate($response_type);
        } else {
            $data = $this->datatables->generate();
        }
        //last_query(1);
        return $data;
    }

}
