<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Searchbus_report extends CI_Controller {

    public function __construct() {
        parent::__construct();
        is_logged_in();
        set_time_limit(0);
        $this->load->library('session');
        $this->load->helper("url");
        
        $this->load->library(array('Excel', 'Datatables', 'Pdf', 'pagination'));
        

        /*$this->sessionData['id'] = $this->session->userdata('user_id');
        $this->sessionData["username"] = $this->session->userdata('username');
        $this->sessionData["password"] = $this->session->userdata('password');*/
    }

    public function index() {
        load_back_view(TICKET_DETAILS_LIST);
        
    }

    public function ticket_details_transaction() {
        $input = $this->input->post();
        $data = $this->ticket_details_common($input, 'json');
        echo $data;
    }

    public function ticket_details_common($input, $response_type = '') {

        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');
// t.booked_by,

        $this->datatables->select('1,
        concat(u.first_name," ",u.last_name) as agent_name,
                        t.agent_mobile_no as agent_mobile_no,                       
                                t.boss_ref_no,
                                t.pnr_no,
                                t.api_ticket_no,                               
                                td.psgr_name,                                
                                t.user_email_id,                              
                                t.mobile_no,             
                                t.op_name,                           
                            t.from_stop_name,
                            t.till_stop_name,                         
                            
                            (case when td.psgr_no IS NULL then "NA" else td.psgr_no end) as psgr_no,
                            t.tot_fare_amt,
                            (case when t.service_tax IS NULL then "0" else t.service_tax end) as service_tax,
                            t.total_fare_without_discount,    
                            t.discount_per,                           
                            (case when td.refund_amt IS NULL then "0" else td.refund_amt end) as refund_amt,
                            (case when td.cancel_charge IS NULL then "0" else td.cancel_charge end) as cancel_charge,
                            t.transaction_status,
                            t.ticket_id,
                            (case when t.commision_percentage IS NULL then "NA" else t.commision_percentage end) as commision_percentage 
                    ');

        $this->datatables->from('tickets t');
        $this->datatables->join('ticket_details td', 't.ticket_id = td.ticket_id', 'inner');
        
         $this->datatables->join('users u', 'u.id = t.booked_by', 'inner');

        if (isset($input['from_date']) && $input['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(inserted_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['to_date']) && $input['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(inserted_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }
        
        if (isset($input['provider_type']) && $input['provider_type'] != '' && $input['provider_type']!='all') {
            $this->datatables->where("t.op_name", $input['provider_type']);
        }

        if (isset($input['trans_type']) && $input['trans_type'] != '' && $input['trans_type']!='all') {
            $this->datatables->where("t.transaction_status", $input['trans_type']);
        }
        
        if (isset($input['user_type']) && $input['user_type'] != '' && $input['user_type']!='all') {
                $this->datatables->where("t.booked_by", $input['user_type']);
        }

        //        if (isset($input['user_type']) && $input['user_type'] != '' && $input['user_type']!='all') {
        //            $this->datatables->where("t.transaction_status", $input['trans_type']);
        //        }
                
                //commnented bcz entry not inserted in table
        if (isset($input['departure_from_date']) && $input['departure_from_date'] != '') {
           $this->datatables->where("DATE_FORMAT(dept_time, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['departure_from_date'])));
       }

       if (isset($input['departure_till_date']) && $input['departure_till_date'] != '') {
           $this->datatables->where("DATE_FORMAT(alighting_time, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['departure_till_date'])));
       }


        if ($response_type != '') {
            $data = $this->datatables->generate($response_type);
        } else {
            $data = $this->datatables->generate();
        }
        return $data;
    }
    
    public function ticket_details_excel() {
        $input = $this->input->get();
        $result_data  = $this->ticket_details_common($input, 'array');     
        $sr_no = 1;
        $data = array();
        foreach ($result_data['aaData'] as $key => $value) {
            $data[] = array('Sr. No.' => $sr_no,
                            'Sale Agent Name' => $value['agent_name'],
                            'Agent Mobile No' =>$value['agent_mobile_no'],
                            'Bos Ref No' => $value['boss_ref_no'],
                            'PNR No' => $value['pnr_no'],
                            'ETS Ref No' => $value['api_ticket_no'],
                          /*  'User Type' => $value['booked_by'],*/
                            'User Name' => $value['psgr_name'],
                            'Email Id' => $value['user_email_id'],
                           /* 'User/Agent Id' => 'user_id',*/
                            'Mobile No.' => $value['mobile_no'],
                            'Operator Name' => $value['op_name'],
                            'From Stop' => $value['from_stop_name'],
                            'To Stop' => $value['till_stop_name'],
                            'No of passenger' => $value['psgr_no'],
                            'Basic Fare' => $value['tot_fare_amt'],
                            'Service Tax' => $value['service_tax'],
                            'Total Fare Without Discount' => $value['total_fare_without_discount'],
                            'Discount Percentage'=> $value['discount_per'],
                            'Refund Amount' => $value['refund_amt'],
                            'Cancel Charge' => $value['cancel_charge'],
                            'Transaction Status' => $value['transaction_status'],
                            'Ticket Id' => $value['ticket_id'],
                            'Commission Percentage' => $value['commision_percentage'],
                            
                            
            );
            $sr_no++;
        }
     
        $this->excel->data_2_excel('ticket_details_transaction_report_' . time() . '.xls', $data);
        die;

     }


}
