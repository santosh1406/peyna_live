<?php
/* 
 * @Author:- Nitin Kawane
 * @Date:- 19 May 2015
 * @function:- Report to dislay list of all booked tickets, cancel tickets by Operator.
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Booked_tickets_report extends CI_Controller {
    
    function __construct() {
        parent::__construct();
     
        $this->load->library('form_validation');
        $this->load->library("Excel");
        
        $this->load->library('Pdf');
        $this->load->library(array('pagination','form_validation'));
        is_logged_in();
        $this->load->model(array('tickets_model', 'ticket_details_model', 'agent_model', 'op_master_model','cancel_ticket_model'));
    }
        
    public function index(){
        $data['operator_data'] = $this->op_master_model->get_op_names();
       $this->get_booked_tickets();
      //  load_back_view(BOOKED_TICKETS_REPORT_VIEW,$data);
    }
    
    public function get_booked_tickets(){
        
        $input = $this->input->post();
        $url_segment = $this->uri->uri_to_assoc(4);
        //show($url_segment,1);
        $from   = ( isset($input['from_date']) && $input['from_date']!='' ) ? $input['from_date'] : ((isset($url_segment['from_date'])) ? $url_segment['from_date']  :'');
        $to     = ( isset($input['to_date']) && $input['to_date']!='' ) ? $input['to_date'] : ((isset($url_segment['to_date'])) ? $url_segment['to_date']  :'');
        $op_id  = ( isset($input['op_id']) && $input['op_id']!='' ) ? $input['op_id'] : ((isset($url_segment['op_id'])) ? $url_segment['op_id']  :'');
        $ticket_status   = ( isset($input['ticket_status']) && $input['ticket_status']!='' ) ? $input['ticket_status'] : ((isset($url_segment['ticket_status'])) ? $url_segment['ticket_status']  :'');
        
        
        $per_page   = 10;
        $uri = $this->uri->segment(14); 
        $page       = (isset($uri)&& $uri >0 ) ? $uri : 0;
        
        $search = array();
        
        $search['from_date']     = $from;
        $search['to_date']       = $to;
        $search['op_id']         = $op_id;
        $search['ticket_status'] = $ticket_status;
                
        
        
        $total_rows = $this->tickets_model->get_booked_tickets_count($search);
        $data['booked_ticket_result'] = $this->tickets_model->get_booked_tickets($search, $per_page, $page);
       // $data['booked_ticket_result'] = $this->tickets_model->get_booked_tickets($search, $per_page, $page);
        $config["base_url"] = base_url() . "report/booked_tickets_report/get_booked_tickets/from_date/".$from."/to_date/".$to."/op_id/".$op_id."/page/".$page."/ticket_status/".$ticket_status."/";
        $config["total_rows"]        = $total_rows;
        $config['use_page_numbers']  = TRUE;
        $config["per_page"]          = $per_page;
        $config['num_links']         = 5;
        $config['next_link']         = 'Next';
        $config['prev_link']         = 'Previous';
        
        $config['full_tag_open']     = '<ul class="pagination">';
        $config['full_tag_close']    = '</ul>';
        $config['prev_link']         = 'Previous';
        $config['prev_tag_open']     = '<li>';
        $config['prev_tag_close']    = '</li>';
        $config['next_link']         = 'Next';
        $config['next_tag_open']     = '<li>';
        $config['next_tag_close']    = '</li>';
        $config['cur_tag_open']      = '<li class="active"><a href="#">';
        $config['cur_tag_close']     = '</a></li>';
        $config['num_tag_open']      = '<li>';
        $config['num_tag_close']     = '</li>';

        $config['first_tag_open']    = '<li>';
        $config['first_tag_close']   = '</li>';
        $config['last_tag_open']     = '<li>';
        $config['last_tag_close']    = '</li>';

        $config['first_link']        = '&lt;&lt;';
        $config['last_link']         = '&gt;&gt;';

        $this->pagination->initialize($config); 
        $data['per_page']            = $per_page;
        $data['page']                = $page;
        $data['total_rows']          = $total_rows;
        $data['ticket_status']          = $ticket_status;
        $data['links']               = $this->pagination->create_links();
        $data['operator_data']       = $this->op_master_model->get_op_names();
        load_back_view(BOOKED_TICKETS_REPORT_VIEW,$data);
        
    }
    
    public function get_passenger_details() {
        $input = $this->input->post();
        $ticket_id = $input['ticket_id'];
        
        $data['passenger']= $this->ticket_details_model->get_passengers_details($ticket_id);
        
        $this->db->select('ticket_id,IFNULL(actual_refund_paid,0.00) as actual_refund_paid,cancel_charge,date_format(created_at,"%d-%m-%Y %h:%i:%s") as created_at');
        $this->cancel_ticket_model->ticket_id = $ticket_id;
        $cancel_ticket = $this->cancel_ticket_model->select();
        
        
        $data['cancel_ticket'] = (count($cancel_ticket) >0 ) ? $cancel_ticket[0]: $cancel_ticket;
                
        
        //show($data,1,'sdfsd');
        if($data['passenger'])
        {
             $data['flag'] = "@#success#@";
        }
        else
        {
            $data['flag'] = "@#failed#@";
        }
        data2json($data);
    }
    
    public function export_pdf() {
       $input                   = $this->input->get();
       
       $booked_ticket_result    = $this->tickets_model->get_booked_tickets($input,$per_page=NULL, $page=NULL);
       $pdf_gen                 = $this->generatePdf($booked_ticket_result);
       
    }
   
    public function generatePdf($result)
    {
        ini_set("memory_limit", "512M");
        ob_start();         
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('Booked Ticket Report');
        $pdf->SetHeaderMargin(20);
        $pdf->SetTopMargin(10);
        $pdf->setFooterMargin(20);
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);        
        $pdf->SetAutoPageBreak(true,PDF_MARGIN_FOOTER);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');
        $pdf->AddPage();
        $html='<center>Booked Ticket Report:</center><br><br>
            <table width="100%" border="1" cellspacing="0" cellpadding="0"  class="table table-hover">
             <thead><tr>
            <th >Sr No.</th>
            <th >Bos_ref_no No.</th>
            <th >Departure Time</th>
            <th >From</th>
            <th >To</th>
            <th >No. Of Passengers</th>
            <th >Fare Amount</th>
            <th >Operator Name</th>
            <th >Ticket status</th>
           </tr></thead><tbody>';
                    $sr_no=1;
                    foreach ($result as $key =>$value): 
                        $html.="<tr><td>".$sr_no ."</td>";
                        $html.="<td>".$value['boss_ref_no']."</td>";
                        $html.="<td>".$value['dept_time']."</td>";
                        $html.="<td>".$value['boarding_stop_name']."</td>";
                        $html.="<td>".$value['destination_stop_name'] ."</td>";
                        $html.="<td>".$value['num_passgr'] ."</td>";
                        $html.="<td>".$value['tot_fare_amt'] ."</td>";
                        $html.="<td>".$value['op_name'] ."</td>";
                        $html.="<td>".($value['transaction_status']=='success' ? 'Booked':'Canceled')."</td></tr>";
                        
                        $sr_no++;
                    endforeach;
        $html.='</tbody></table>';
        $pdf->writeHTML($html, true, false, true, false, '');       
      
        $pdf->Output('Booked Ticket Reports.pdf', 'D');
        die;
    }
    
    public function export_excel(){
        
        $input                   = $this->input->get();  
        $booked_ticket_result    = $this->tickets_model->get_booked_tickets($input,$per_page=NULL, $page=NULL);
        $sr_no=1;
        $data = array();
        if(!empty($booked_ticket_result))
        {

            foreach($booked_ticket_result as $key=> $value)
            { 
                $data[]=array('Sr no'=> $sr_no  ,
                                'Bos_ref_no.' => $value['boss_ref_no'],
                                'Departure Time'=> $value['dept_time']  ,
                                'From' => $value['boarding_stop_name'],
                                'To'=>$value['destination_stop_name'],
                                'No. Of Passengers'=>$value['num_passgr'],
                                'Fare Amount'=>$value['tot_fare_amt'] ,
                                'Operator Name'=>$value['op_name'],
                                'Transaction status'=> ($value['transaction_status']=='success' ? 'Booked':'Canceled')
                     );
                $sr_no++;
            }
        }
        else
        {
            $data = array(array());
        }
        $this->excel->data_2_excel('Booked_tickets_report'.time().'.xls', $data);
        die;  
    }
}