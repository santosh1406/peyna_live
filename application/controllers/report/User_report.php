<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_report extends CI_Controller {

    public function __construct() {
        parent::__construct();
       
        is_logged_in();
        set_time_limit(0);
        
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);

        $this->load->model(array('tickets_model', 'agent_model', 'ticket_details_model', 'routes_model', 'bus_stop_master_model', 'op_master_model', 'booking_agent_model', 'report_model', 'waybill_no_model', 'hand_held_machine_model', 'api_provider_model','city_master_model'));
        $this->load->library("Excel");
        $this->load->library("Datatables");
        $this->load->library('Pdf');
    }

    public function index() {
         $array_input = array();
        $data['tran_type_data'] = array(
             "success" => "Success",
             "incomplete" => "Incomplete",
            "pfailed" => "Payment Initiate",
             "aborted"=>"Aborted",
             "failure"=>"Payment Failed",
            "failed"=>"Failed",
            "cancel" => "Cancel"
            );
       
        $data['pnr_no'] = $this->tickets_model->column('pnr_no')
                ->where('pnr_no!=', '')
                ->as_array()
                ->find_all();

        $data['summary_details'] = $this->report_model->cal_summary_bos_tkt_details_rpt($array_input);
		//get All state
		$data['city_name'] = $this->city_master_model->get_all_city();
        //$this->bos_tkt_details();
        load_back_view(USER_TYPE_WISE_REPORT_VIEW, $data);
    }
    
    function user_report_view() {//((sum(t.total_fare_without_discount) * sum(t.commision_percentage)/100) - (sum(t.discount_value)) ) 

        $input = $this->input->post();
        $data = $this->user_report_view_common($input, 'json');
        echo $data;
    }
    public function user_type_report_export_excel() {
                
                $input = $this->input->get();
                $ticket_result = $this->user_report_view_common($input, 'array');
                $fields = array("Sr No.", "Date", "No of Registered Users", "No of Tickets Booked"/*, "No of Passenger" "Actual Fare Amount", "Discounted Amount", "Cancellation Charges", "Refund Amount", "Commission Amount", "Total Reveue", "Actual Revenue"*/);
                $stockArr = array();
                $sr_no = 1;
                if (count($ticket_result) > 0) {

                    foreach ($ticket_result['aaData'] as $key => $value) {
                        
                        $data[] = array('Sr no' => $sr_no,
                            'Date' => $value['dd_date'],
                            'User Type' => $value['roleid'],
                            'No of Registered Users' => $value['reg_user'],
                            'No of Tickets Booked' => $value['tkt_book']);
                     $sr_no++;
                     
                    }
                    $this->excel->data_2_excel('user_type_wise_report' . date('Y-m-d') . '_' . time() . '.xls', $data);
                    die;
                }
            }

        
    public function user_report_view_common($input,$response_type =''){
         
        if(isset($input['from_date']) && $input['from_date']!='' ){
            $from_date = date('Y-m-d', strtotime($input['from_date']));
        } if(isset($input['till_date']) && $input['till_date']!='' ){
            $till_date = date('Y-m-d', strtotime($input['till_date']));
        }
/*        $status=$input['tran_type'];
      //  $arr_status='';
        if ($input['tran_type'] && $input['tran_type']!='select_status') {
              $status=$input['tran_type'];
              
            if($status=='failed'){
                
                $status=array('psuccess','failed');
            }
        
        }*/



        $this->datatables->select(" 0,date_format(dd,'%d-%m-%Y') as dd_date,roleid,sum(reg_user) reg_user,sum(tkt_book)tkt_book ");
         $sql_query = "(SELECT dd,roleid,(gu_cnt+au_cnt+cu_cnt+su_cnt+fme+fme_agent_cnt) AS reg_user, 0 AS tkt_book
                        from (SELECT date(u.created_date) dd, (CASE  WHEN (u.role_id='7' && u.salt='') THEN 'Guest'                 
                        WHEN u.role_id='16' THEN 'Agent' WHEN (u.role_id='7' && u.is_employee!='1'  &&  u.salt !='' && u.password != '' ) THEN 'Customer'                 
                        WHEN (u.is_employee='1' ) THEN 'Trimax User' WHEN (u.role_id = '26') THEN 'Fme'  WHEN (u.role_id = '27') THEN 'Fme Agent' END ) roleid ,
                        IF((u.role_id='7' && u.salt=''),count(u.id),0) AS gu_cnt,IF(u.role_id='16',count(u.id),0) AS au_cnt,
                        IF((u.role_id='7' && u.is_employee!='1' &&  u.salt !='' && u.password != '' ) ,count(u.id),0) AS cu_cnt,
                        IF((u.is_employee='1') ,count(u.id),0) AS su_cnt,IF(u.role_id='27', count(u.id), 0) AS fme_agent_cnt,IF(u.role_id='26', count(u.id), 0) AS fme 
                        from users u 
                        where u.status = 'Y'" ;       
                        if($input['city']!='')
                       {
                           $sql_query.= " and u.city='".$input['city']."'";
                       }
                        $sql_query.= " GROUP BY date_format(u.created_date, '%Y-%m-%d'),         roleid) b
                        union all
                        SELECT   dd,role_id as roleid,0 AS reg_user,(g_cnt +a_cnt+c_cnt+s_cnt+fme_agent_tcnt+fme_tcnt)  AS tkt_book
                        from (SELECT Date_format(t.inserted_date, '%Y-%m-%d') AS dd,
                        (CASE WHEN (t.role_id='0' ) THEN 'Guest'  WHEN (t.role_id='16') THEN 'Agent'             
                        WHEN (t.role_id!='0' && u.role_id='7' && u.is_employee!='1' &&  u.salt !='' && u.password != '') THEN 'Customer'             
                        WHEN (u.is_employee='1') THEN 'Trimax User' WHEN (t.role_id = '26') THEN 'Fme'  WHEN (t.role_id = '27') THEN 'Fme Agent' END ) role_id,
                        IF(t.role_id='0',Count(t.ticket_id),0) as g_cnt,IF(t.role_id = '16', count(t.ticket_id),0) AS a_cnt,IF((t.role_id='7' && u.is_employee!='1' &&  u.salt !='' && u.password != ''),
                        count(t.ticket_id),0) AS c_cnt,IF((u.is_employee='1'),count(t.ticket_id),0) AS s_cnt,IF(t.role_id='27', count(u.id), 0) AS fme_agent_tcnt,IF(t.role_id='26', count(u.id), 0) AS fme_tcnt
                        FROM `tickets` `t` 
                        LEFT JOIN users u on u.id=t.inserted_by";
                    if ($input['tran_type'] && $input['tran_type']!='select_status') {
                    $status=$input['tran_type'];                  
                    if($status=='failed'){
                       $sql_query.= " WHERE t.`transaction_status` IN ('psuccess','failed') ";   
                    }else{
                          $sql_query.= " WHERE t.`transaction_status` IN ('".$status."') ";       
                         }
                    }
                    if($input['city']!='')
                    {
                        $sql_query.= " and u.city='".$input['city']."'";
                    }
                    	
                    else
                    {
                        if($input['city']!='')
                        {
                            $sql_query.= " where u.city='".$input['city']."'";
                        }
                    }
                $sql_query.= " GROUP BY date_format(t.`inserted_date`, '%Y-%m-%d') , role_id ) a) c where roleid is not null ";

                if ($input['from_date']!='' ) {
                    $sql_query.= " and dd >='$from_date'";
                }
                if ($input['till_date']!='') {
                    $sql_query.= " and dd <='$till_date'";
                }
                $sql_query.= "group by dd,roleid";
                $this->db->order_by("dd","desc");
                $this->datatables->from($sql_query);
                if ($response_type != '') {
                    $data = $this->datatables->generate($response_type);
                } else {
                $data = $this->datatables->generate();

                }
		return $data;			
            }
        }
        ?>