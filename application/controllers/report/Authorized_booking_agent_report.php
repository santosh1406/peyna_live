<?php 
class Authorized_booking_agent_report extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        is_logged_in();
        
         //ob_flush();
        $this->load->model(array('agent_model','state_master_model'));
        $this->load->library("Excel");
        $this->load->library('Pdf');
    }
    
    function index()
    { 
        $data['agent_code'] = $this->agent_model->get_all_agents();
        $data['state_name'] = $this->state_master_model->get_all_state();
        load_back_view(AUTHORIZED_AGENT_REPORT_VIEW,$data);  
    }
    /*
     * List Authorized agent
     */
    
    public function list_authorized_agents() {
        $input=$this->input->post();
        
        $user_id = $this->session->userdata("user_id");
        $this->datatables->select("am.agent_code, DATE_FORMAT(am.created_date,'%d-%m-%Y') as created_date, '', CONCAT(am.fname, ' ', am.mname, ' ', am.lname) AS aname, cm.stCity, sm.stState, am.address, am.contact_no, am.email, abd.bank_acc_no, abd.bank_name,'Pre-paid', am.level, ac.security_deposit_amt, CONCAT(am1.fname, ' ', am1.mname, ' ', am1.lname) AS parent,CONCAT(u.first_name,' ',last_name)created_by ,if(am.is_status=0,'Discontinue','Continue') as status", false);
        $this->datatables->from('agent_master am');
        $this->datatables->join('agent_config ac','ac.agent_id = am.agent_id','left');
        $this->datatables->join('agent_bank_detail abd','abd.agent_id = am.agent_id','left');
        $this->datatables->join('city_master cm','cm.intCityId = am.city_id','left');
        $this->datatables->join('agent_master am1','am.parent = am1.agent_id','left');
        $this->datatables->join('state_master sm','sm.intStateId = am.state_id','left');
        $this->datatables->join('users u','u.id = am.created_by','left');
        if($this->input->post('agent_code')){
            $this->datatables->where('am.agent_code',$this->input->post('agent_code'));
        }
        if($this->input->post('state_name')){
            $this->datatables->where('sm.intStateId',$this->input->post('state_name'));
        }
        if($this->input->post('city_name')){
            $this->datatables->where('cm.intCityId',$this->input->post('city_name'));
        }
        $data = $this->datatables->generate('json');
        //show($this->db->last_query(),1);
        echo $data;
    }
    
    
    public function export_pdf() { 
        $agent_code = ($this->input->get('agent_code')!='')?$this->input->get('agent_code'):'';
        $state = ($this->input->get('state')!='')?$this->input->get('state'):'';
        $city = ($this->input->get('city')!='')?$this->input->get('city'):'';
        //$authorized_agents    = $this->agent_model->get_authorized_agents($agent_code, $state, $city);
        $authorized_agents        = $this->agent_model->get_authorized_agents($agent_code, $state, $city);
        $pdf_gen                 = $this->generatePdf($authorized_agents);
       
    }
   
    public function generatePdf($result)
    {
        ini_set("memory_limit", "512M");
        ob_start(); 
        $pdf = new Pdf('L', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->setPageOrientation('L');
        $pdf->SetTitle('Authorized Booking Agent');
        $pdf->SetHeaderMargin(20);
        $pdf->SetTopMargin(10);
        $pdf->setFooterMargin(20);
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);        
        $pdf->SetAutoPageBreak(true,PDF_MARGIN_FOOTER);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');
        $pdf->AddPage();
        
        $x = $pdf->pixelsToUnits('10');
        $y = $pdf->pixelsToUnits('10');
        $font_size = $pdf->pixelsToUnits('18');
        $txt = '';

        $pdf->SetFont ('helvetica', '', $font_size , '', 'default', true );
        $pdf->Text  ( $x, $y, $txt, false, false, true, 0, 0, '', false, '', 0, false, 'T', 'M', false );
        $html='<center style="font-weight:bold;">Authorized Booking Agent:</center><br><br>
            <table width="100%" border="0.2" cellspacing="0" cellpadding="0">
            <thead><tr style="background-color:gray; font-weight:bold;">
            <th>Sr.No</th>
            <th>Agent Code</th>
            <th>Reg. Date</th>
            <th>Firm</th>
            <th>Agent Name</th>
            <th>City</th>
            <th>State</th>
            <th>Address</th>
            <th>Mobile No.</th>
            <th>Email Id</th>
            <th>Acc No.</th>
            <th>Bank Name</th>
            <th>Acc. Type</th>
            <th>No. Of Sub Agents</th>
            <th>Security Deposit Amt. </th>
            <th>Recommended By </th><th>Created By </th>
            <th>Status</th>

           </tr></thead><tbody>';
                    $sr_no=1;
                    if($result):
                        foreach ($result as $key =>$value): 
                            $html.="<tr><td>".$sr_no ."</td>";
                            $html.="<td>".$value['agent_code']."</td>";
                            $html.="<td>".$value['created_date']."</td>";
                            $html.="<td>"."--NA--"."</td>";
                            $html.="<td>".$value['aname'] ."</td>";
                            $html.="<td>".$value['stCity'] ."</td>";
                            $html.="<td>".$value['stState'] ."</td>";
                            $html.="<td>".$value['address'] ."</td>";
                            $html.="<td>".$value['contact_no'] ."</td>";
                            $html.="<td>".$value['email'] ."</td>";
                            $html.="<td>".$value['bank_acc_no'] ."</td>";
                            $html.="<td>".$value['bank_name'] ."</td>";
                            $html.="<td>".$value['Pre-paid'] ."</td>";
                            $html.="<td>".$value['level'] ."</td>";
                            $html.="<td>".$value['security_deposit_amt'] ."</td>";
                            $html.="<td>".$value['parent'] ."</td>";
                            $html.="<td>".$value['created_by'] ."</td>";
                            $html.="<td>".$value['status'] ."</td>";
                            $html .=  "</tr>";
                            $sr_no++;
                        endforeach;
                    endif;
        $html.='</tbody></table>';
        $pdf->writeHTML($html, true, false, true, false, '');       
      
        $pdf->Output('Authorized Agent Report.pdf', 'D');
        die;
    }
    
    function print_report() {
        $data['authorized_agents']    = $this->agent_model->get_authorized_agents();
        load_back_view(AUTHORIZED_AGENT_PRINT_VIEW,$data);
    }
    
    public function export_excel()
    {   
        $agent_code = ($this->input->get('agent_code')!='')?$this->input->get('agent_code'):'';
        $state = ($this->input->get('state')!='')?$this->input->get('state'):'';
        $city = ($this->input->get('city')!='')?$this->input->get('city'):'';
        $authorized_agents    = $this->agent_model->get_authorized_agents($agent_code, $state, $city);
        // Field names in the first row
        $fields = array("Agent Code","Registration Date","Firm Name","Agent Name","City","State","Address","Mobile No.","Email Id", "Acc No.", "Bank Name","Acc. Type","No. Of Sub Agents","Security Deposit Amt.","Recommended By ","Created By","Status");
        $stockArr = array();
        //echo '<pre>';print_r($reportResult->result_array());exit;
        
        if ($authorized_agents)
        {
            foreach ($authorized_agents as $row)
            {
                $TempreportArr = array();
                array_push($TempreportArr,isset($row['agent_code'])?$row['agent_code']:"--NA--");
                array_push($TempreportArr,isset($row['created_date'])?$row['created_date']:"--NA--");
                array_push($TempreportArr,isset($row['v_agent_code'])?$row['v_agent_code']:"--NA--");
                array_push($TempreportArr,isset($row['aname'])?$row['aname']:"--NA--");
                array_push($TempreportArr,isset($row['stCity'])?$row['stCity']:"--NA--");
                array_push($TempreportArr,isset($row['stState'])?$row['stState']:"--NA--");
                array_push($TempreportArr,isset($row['address'])?$row['address']:"--NA--");
                array_push($TempreportArr,isset($row['contact_no'])?$row['contact_no']:"--NA--");
                array_push($TempreportArr,isset($row['email'])?$row['email']:"--NA--");
                array_push($TempreportArr,isset($row['bank_acc_no'])?$row['bank_acc_no']:"--NA--");
                array_push($TempreportArr,isset($row['bank_name'])?$row['bank_name']:"--NA--");
                array_push($TempreportArr,isset($row['Pre-paid'])?$row['Pre-paid']:"--NA--");
                array_push($TempreportArr,isset($row['level'])?$row['level']:"--NA--");
                array_push($TempreportArr,isset($row['security_deposit_amt'])?$row['security_deposit_amt']:"--NA--");
                array_push($TempreportArr,isset($row['parent'])?$row['parent']:"--NA--");
                 array_push($TempreportArr,isset($row['created_by'])?$row['created_by']:"--NA--");
                array_push($TempreportArr,isset($row['status'])?$row['status']:"--NA--");
                $stockArr[] = $TempreportArr;
            }
        } else {
            //$stockArr = array('No data available');
        }
        
       // echo count($stockArr);
       $this->exportToExcel($fields,$stockArr,'Authorized_booking_agents');    
    } 
    
    public function exportToExcel($fields,$data,$title = 'test',$actionName = 'download')
    {
       if(!is_array($fields) && count($fields) == 0)
            return;
        
        // Starting the PHPExcel library
        $this->load->library('excel');
       
            $objPHPExcel = new PHPExcel();
            //Set Borders 
            $styleArray = array(
                'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
             );
             $objPHPExcel->getDefaultStyle()->applyFromArray($styleArray);

         //Set background of first row    
        $objPHPExcel->getActiveSheet()
        ->getStyle('A1:Q1')
        ->getFill()
        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
        ->getStartColor()
        ->setARGB('FF808080');
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
        //Font size of column names
        $objPHPExcel->getActiveSheet()->getStyle('1')->getFont()->setSize(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(19);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(7);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(12);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(12);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'Sr.No');
        $col = 1;
        foreach ($fields as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $col++;
        }
           
        // Fetching the table data
        $row = 2;
        if(is_array($data) && count($data) > 0){ 
            foreach($data as $dataRow){
                $col1 = 0;
                foreach($dataRow as $dataSingle){
                    if((int)$col1 === 0){
                         $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col1, $row, ($row-1));
                          $col1++;
                    }
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col1, $row, $dataSingle);
                    $col1++;
                    //Height of the row
                    $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(23);
                }
                $row++;     
            }
        }     
        if(strtolower($actionName) == 'download'){ 
            $this->forceDownloadExcel($objPHPExcel,$title);
        }
        
         
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'HTML');
        $objWriter->save('php://output');
      
        // End of PHPExcel approach     
    }  
    
    /***
     * Force Doenload Excel File
     * @param $objectexcel PHPExcelObject 
     * $title string -- Name Of The file to be download --
     */
    private function forceDownloadExcel($objectexcel,$title)
    {
        $objWriter = PHPExcel_IOFactory::createWriter($objectexcel, 'Excel5');
       // Sending headers to force the user to download the file
       header('Content-Type: application/vnd.ms-excel');
       header("Content-Disposition: attachment; filename=".$title."_".date("d-m-Y").".xls");
       header('Cache-Control: max-age=0');
       $objWriter->save('php://output');
       exit;
    }
    
    
}