<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Msrtc_report extends CI_Controller {

    public function __construct() {
        parent::__construct();
        is_logged_in();
	    set_time_limit(0);
        
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);
        $this->load->model(array('users_model'));
        $this->load->helper("url");

        $this->pos_encryption_key = 'QWRTEfnfdys635';

    }
    public function index()
    {
        $data = array();
        $data['report_data'] = array(
            "waybill_staus_report" => "Waybill Status Report",
            "connection_report" => "Connection Report",
           // "route_report" => "Route Report",
            //"index" => "Cb Report Md",
            "ticket_report" => "Ticket report"
        );

       load_back_view(MSRTC_REPORT_VIEW, $data);
    }

    public function msrtc_report($report_name) {

        $rpt_name = $report_name;
        $report_name = base64_decode($report_name);
        if(empty($rpt_name))
        {
            redirect(base_url().'report/msrtc_report');
        }
        $report_url = MSRTC_REPORT_URL;
        $sessionData = $this->session->userdata();
        if($sessionData)
        {
            $username = $sessionData['username'];
            $mobile_no =$sessionData['mobile_no'];
            $email = $sessionData['email'];
            $role_name = $sessionData['role_name'];
            $role_id = $sessionData['role_id'];
            $display_name = $sessionData['display_name'];
        }

        if($role_id == SUPERADMIN_ROLE_ID || $role_id == TRIMAX_ROLE_ID || $role_id == COMPANY_ROLE_ID || $role_id == ADMIN_ROLE_ID)
        {
           redirect(base_url().'admin/dashboard');
        }
        else
        {         
           redirect($report_url."/".$report_name."/".base64_encode($username)."/".base64_encode($mobile_no)."/".base64_encode($email)."/".base64_encode($role_id)."/"."");
        }
    }
}
