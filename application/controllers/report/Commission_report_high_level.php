<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Commission_report_high_level extends CI_Controller {

    public function __construct() {
        parent::__construct();
        is_logged_in();
        set_time_limit(0);

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);
        $this->load->model(array('tickets_model', 'users_model', 'state_master_model', 'agent_model', 'ticket_details_model', 'routes_model', 'bus_stop_master_model', 'op_master_model', 'booking_agent_model', 'report_model', 'api_provider_model', 'city_master_model', 'base_commission_model', 'office_master_model', 'Ors_agent_report_model', 'lookup_detail_model', 'cb_commission_distribution_model'));
        $this->load->library(array('Excel', 'Datatables', 'Pdf', 'pagination'));
        $this->load->helper("url");
    }
    public function daily_commission_report() {
        $data['service_type'] = $this->cb_commission_distribution_model->getServiceType();
        
        load_back_view(HIGH_LEVEL_COMMISSION_REPORT, $data);
    }

    public function daily_commission() {
        $input = $this->input->post();
        $data = $this->daily_commission_common($input, 'json');
        echo $data;
    }

    public function daily_commission_excel() {
        $input = $this->input->get();

        $result_data = $this->daily_commission_common($input, 'array');
        $sr_no = 1;
        $data = array();
        // show($result_data,1);
        foreach ($result_data['aaData'] as $key => $value) {
            $data[] = array('Sr. No.' => $sr_no,
                'Service Name' => $value['service_name'],                
                'RD' => $value['regional_name'],
                'DD' => $value['divisional_name'],
                'Executive' => $value['executive_name'],
                'Agent Name' => $value['agent_name'],
                'Commission Calculation Date' => $value['comm_calculate_date'],
                //'User Name' => $value['agent_name'], 
                'Transaction Amount (Rs.)' => $value['comm_applicable_amt'],
                //'Commission Percentage' => $value['comm_perc'], 
                'Commission Amount (Rs.)' => $value['comm_amt'],
                'Transaction Ref No' => $value['txn_ref_no'],

                    /* 'GST Percentage' => $value['gst_percent'],
                      'GST Amount' => $value['gst_amt'],
                      'Commission With GST (Rs.)' => $value['gst_app_amt'],
                      'TDS Percentage' => $value['tds_percent'],
                      'TDS Amount' => $value['tds_amt'],
                      'Net Commission Amount (Rs.)' => $value['comm_act_amt'] */
            );
            $sr_no++;
            $total_trans_amt +=$value['comm_applicable_amt'];
            $total_comm_amt +=$value['comm_amt'];

        }

        $data[] = array('Sr. No.' => '',
                'Service Name' => '',                
                'RD' => '',
                'DD' => '',
                'Executive' => '',
                'Agent Name' => 'Total',
                'Commission Calculation Date' => '',
                'Transaction Amount (Rs.)' => number_format($total_trans_amt,2),
                'Commission Amount (Rs.)' => number_format($total_comm_amt,2),
                    
            );
        $this->excel->data_2_excel('daily_commission_report' . time() . '.xls', $data);
        die;
    }

    public function daily_commission_common($input, $response_type = '') {
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');

        // $this->datatables->select('1,
        //                         DATE_FORMAT(cd.comm_calculate_date,"%d-%m-%Y %h:%i:%s") as comm_calculate_date,
        //                         concat(u.first_name," ",u.last_name) as agent_name,
        //                         round(sum(cd.`comm_applicable_amt`),2) as comm_applicable_amt,
        //                         round(ccd.`retailer_commission`,2) as retailer_commission,
        //                         round(sum(cd.`agent_comm`),2) as agent_comm,
        //                         round(cd.`tds_percentage`,2) as tds_percentage,
        //                         round(sum(cd.`agent_comm`)*(cd.`tds_percentage`/100),2) as tds_value,
        //                         round(sum(cd.`agent_comm`)-sum(cd.`agent_comm`)*(cd.`tds_percentage`/100),2) as net_comm_amt,
        //                        ');
        /* Added by prabhat */
        /* if($input['report_Type'] == 'VAS' || $input['report_Type'] == 'DTH') {
          return $this->daily_commission_common_vas($input,$response_type);
          } */
        /* end by prabhat */
        // echo"here: "; print_r($input); die;
        /* Added by Lilawati */
        
        if (!empty($input['report_type']) && $input['report_type'] != '2') {
            return $this->daily_commission_common_vas($input, $response_type);
        }
        /* end by lilawati */
        $this->datatables->select('1,
                                DATE_FORMAT(cd.comm_calculate_date,"%d-%m-%Y %h:%i:%s") as comm_calculate_date,
                                round(sum(cb.consume_amount),2) as comm_applicable_amt,                                
                                round(sum(cd.`md_comm`),2) as comm_amt,
                                round(ccd.`md_commission`,2) as comm_perc,
                                concat(u.first_name," ",u.last_name) as agent_name,
                                round(cd.`gst_percentage`,2) as gst_percent,
                                cd.`gst_percentage` as gst_amt,
                                round(sum(cd.`company_actual_comm`),2) as gst_app_amt,
                                round(cd.`tds_percentage`,2) as tds_percent,
                                cd.`tds_percentage` as tds_amt,
                                round(sum(cd.`md_actual_comm`),2) as comm_act_amt,

                                round(ccd.`ad_commission`,2) as ad_comm_perc,
                                round(ccd.`dist_commission`,2) as dist_comm_perc,
                                round(ccd.`company_commission`,2) as company_comm_perc,
                                round(ccd.`retailer_commission`,2) as agent_comm_perc,

                                round(sum(cd.`ad_comm`),2) as ad_comm_amt,
                                round(sum(cd.`di_comm`),2) as di_comm_amt,
                                round(sum(cd.`company_comm`),2) as company_comm_amt,
                                round(sum(cd.`agent_comm`),2) as agent_comm_amt,

                                round(sum(cd.`ad_actual_comm`),2) as ad_act_comm_amt,
                                round(sum(cd.`di_actual_comm`),2) as di_act_comm_amt,
                                round(sum(cd.`company_actual_comm`),2) as company_act_comm_amt,
                                 round(sum(cd.`agent_actual_comm`),2) as agent_act_comm_amt
                               ');

        $this->datatables->from('cb_commission_distribution cd');
        $this->datatables->join('2_commission_detail ccd', 'ccd.commission_id = cd.commission_template_id', 'inner');
        $this->datatables->join(MSRTC_DB_NAME . '.cb_waybillprogramming cb', 'cb.waybill_no = cd.waybill_no', 'inner');

        if (!empty($session_level) && isset($session_level)) {
            if ($this->session->userdata('role_id') == MASTER_DISTRIBUTOR_ROLE_ID) {
                $this->datatables->join('users u', 'u.id = cd.md_id', 'inner');
                $this->datatables->where('cd.md_id', $session_id);
            } elseif ($this->session->userdata('role_id') == AREA_DISTRIBUTOR_ROLE_ID) {
                $this->datatables->join('users u', 'u.id = cd.ad_id', 'inner');
                $this->datatables->where('cd.ad_id', $session_id);
                $this->datatables->edit_column('comm_perc', '$1', 'ad_comm_perc');
                $this->datatables->edit_column('comm_amt', '$1', 'ad_comm_amt');
                $this->datatables->edit_column('comm_act_amt', '$1', 'ad_act_comm_amt');
            } elseif ($this->session->userdata('role_id') == DISTRIBUTOR) {
                $this->datatables->join('users u', 'u.id = cd.di_id', 'inner');
                $this->datatables->where('cd.di_id', $session_id);
                $this->datatables->edit_column('comm_perc', '$1', 'dist_comm_perc');
                $this->datatables->edit_column('comm_amt', '$1', 'di_comm_amt');
                $this->datatables->edit_column('comm_act_amt', '$1', 'di_act_comm_amt');
            } elseif ($this->session->userdata('role_id') == COMPANY_ROLE_ID) {
                $this->datatables->join('users u', 'u.id = cd.company_id', 'inner');
                $this->datatables->where('cd.company_id', $session_id);
                $this->datatables->edit_column('comm_perc', '$1', 'company_comm_perc');
                $this->datatables->edit_column('comm_amt', '$1', 'company_comm_amt');
                $this->datatables->edit_column('comm_act_amt', '$1', 'company_act_comm_amt');
            } elseif ($this->session->userdata('role_id') == RETAILER_ROLE_ID) {
                $this->datatables->join('users u', 'u.id = cd.agent_id', 'inner');
                $this->datatables->where('cd.agent_id', $session_id);
                $this->datatables->edit_column('comm_perc', '$1', 'agent_comm_perc');
                $this->datatables->edit_column('comm_amt', '$1', 'agent_comm_amt');
                $this->datatables->edit_column('comm_act_amt', '$1', 'agent_act_comm_amt');
            }
        }
        $this->datatables->edit_column('gst_amt', '$1', 'getGstAmount(comm_amt)');
        $this->datatables->edit_column('gst_app_amt', '$1', 'getGstCommAmount(comm_amt,gst_amt)');
        $this->datatables->edit_column('tds_amt', '$1', 'getPercentageAmount(gst_app_amt,tds_percent)');

        if (!empty($input['from_date'])) {
            $this->datatables->where("DATE_FORMAT(cd.comm_calculate_date, '%Y-%m-%d')>=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (!empty($input['to_date'])) {
            $this->datatables->where("DATE_FORMAT(cd.comm_calculate_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }
        $this->datatables->where("cb.WAYBILL_STATUS=", "CLOSED");
        $this->datatables->group_by("DATE_FORMAT(cd.comm_calculate_date,'%Y-%m-%d')");

        if ($response_type != '') {
            $data = $this->datatables->generate($response_type);
        } else {
            $data = $this->datatables->generate();
        }
        
        return $data;
    }

    /**
     * Written by Prabhat
     * Desc: show and export the agent reports
     */
    function agent_wise_reports() {
        load_back_view(AGENTWISE_REPORT_VIEW);
    }

    function agent_wise_data() {
        $input = $this->input->post();
        $data = $this->list_of_agent_wise_reports($input, 'json');
        echo $data;
    }

    function list_of_agent_wise_reports($data, $res = '') {

        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');

        $this->datatables->select('1,
                                    DATE_FORMAT(cbcd.comm_calculate_date,"%d-%m-%Y %h:%i:%s") as comm_cal_date,
                                    cbcd.agent_code,cbcd.waybill_no,
                                    round(cbcd.comm_applicable_amt,2) as comm_applicable_amt,
                                    round(cd.retailer_commission,2) as retailer_commission,
                                    round(cbcd.agent_comm,2) as agent_comm,
                                    round(cbcd.tds_percentage,2) as tds_percentage,
                                    round(cbcd.agent_comm * cbcd.tds_percentage / 100, 2) as tds_value,
                                    round(cbcd.agent_comm - round(cbcd.agent_comm * cbcd.tds_percentage / 100, 2), 2) as net_commin_ant'
        );
        $this->datatables->from('cb_commission_distribution cbcd');
        $this->datatables->join('2_commission_detail cd', 'cd.commission_id = cbcd.commission_template_id', 'inner');
        $this->datatables->join('users u', 'u.agent_code=cbcd.agent_code', 'inner');

        if (isset($data['from_date']) && $data['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(cbcd.comm_calculate_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($data['from_date'])));
        }

        if (isset($data['to_date']) && $data['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(cbcd.comm_calculate_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($data['to_date'])));
        }
        if (!empty($session_level)) {
            if ($this->session->userdata('role_id') == RETAILER_ROLE_ID) {
                $this->datatables->where("u.id", $session_id);
            }
        }
        if ($res != '') {
            $data1 = $this->datatables->generate($res);
        } else {
            $data1 = $this->datatables->generate('json');
        }

        return $data1;
    }

    function agent_wise_excel() {

        $input = $this->input->get();
        $user_result = $this->list_of_agent_wise_reports($input, 'array');
        $sr_no = 1;
        $data = array();
        if (!empty($user_result)) {
            foreach ($user_result['aaData'] as $key => $value) {
                $data[] = array('Sr. No.' => $sr_no,
                    'Commission Calculate Date' => $value['comm_cal_date'],
                    'Agent Code' => $value['agent_code'],
                    'Waybill No' => $value['waybill_no'],
                    'Application Amount (Rs.)' => $value['comm_applicable_amt'],
                    'Commission Percentage' => $value['retailer_commission'],
                    'Commission Amount (Rs.)' => $value['agent_comm'],
                    'Tds Percentage' => $value['tds_percentage'],
                    'Tds Value' => $value['tds_value'],
                    'Net Commission Amount (Rs.)' => $value['net_commin_ant'],
                );
                $sr_no++;
            }
            $this->excel->data_2_excel('waybill_wise_commission_report_excel' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
            die;
        }
    }

    /**
     * End by prabhat
     */
    public function monthly_commission_report() {

        load_back_view(MONTHLY_COMMISSION_REPORT);
    }

    public function monthly_commission() {
        $input = $this->input->post();
        $data = $this->monthly_commission_common($input, 'json');
        echo $data;
    }

    public function monthly_commission_excel() {
        $input = $this->input->get();

        $result_data = $this->monthly_commission_common($input, 'array');
        $sr_no = 1;
        $data = array();
        foreach ($result_data['aaData'] as $key => $value) {
            $data[] = array('Sr. No.' => $sr_no,
                'Commission Date' => $value['com_month'],
                'User Name' => $value['agent_name'],
                'Commission Amount (Rs.)' => $value['comm_amt'],
                'GST Percentage' => $value['gst_percentage'],
                'GST Amount' => $value['gst_amt'],
                'Commission With GST (Rs.)' => $value['gst_app_amt'],
                'TDS Percentage' => $value['tds_percentage'],
                'TDS Amount' => $value['tds_app_amt'],
                'Net Commission Amount (Rs.)' => $value['net_com']
            );
            $sr_no++;
        }
        $this->excel->data_2_excel('monthly_commission_report' . time() . '.xls', $data);
        die;
    }

    public function monthly_commission_common($input, $response_type = '') {
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');
        $where = array();
        $join = '';

        if (!empty($session_level) && isset($session_level)) {
            if ($this->session->userdata('role_id') == COMPANY_ROLE_ID) {
                $this->datatables->select('1,
                                DATE_FORMAT(c.comm_calculate_date,"%M %Y") as com_month,
                                concat(u.first_name," ",u.last_name) as agent_name,
                                round(sum(c.`company_comm`),2) as comm_amt,
                                round(c.`gst_percentage`,2) as gst_percentage,
                                round((sum(c.`company_comm`) /1.18) * (c.`gst_percentage` / 100),2) as gst_amt,
                                round((((sum(c.`company_comm`)) - (sum(c.`company_comm`) /1.18) * (c.`gst_percentage` / 100))),2) as gst_app_amt,
                                c.`tds_percentage` as tds_percentage,
                                round((((sum(c.`company_comm`)) - (sum(c.`company_comm`) /1.18) * (c.`gst_percentage` / 100)))  * (c.`tds_percentage` / 100),2) as tds_app_amt,
                                round((((sum(c.`company_comm`)) - (sum(c.`company_comm`) /1.18) * (c.`gst_percentage` / 100))) - (((sum(c.`company_comm`)) - (sum(c.`company_comm`) /1.18) * (c.`gst_percentage` / 100)))  * (c.`tds_percentage` / 100),2) as net_com
                                ');

                $where['c.company_id'] = $session_id;
                $where['c.comm_distrubute_company'] = 'Y';
                $join1 = 'u.id = c.company_id';
                // $join = 'c.company_id = w.user_id';
            } elseif ($this->session->userdata('role_id') == MASTER_DISTRIBUTOR_ROLE_ID) {
                $this->datatables->select('1,
                                DATE_FORMAT(c.comm_calculate_date,"%M %Y") as com_month,
                                concat(u.first_name," ",u.last_name) as agent_name,
                                round(sum(c.`md_comm`),2) as comm_amt,
                                round(c.`gst_percentage`,2) as gst_percentage,
                                round((sum(c.`md_comm`) /1.18) * (c.`gst_percentage` / 100),2) as gst_amt,
                                round((((sum(c.`md_comm`)) - (sum(c.`md_comm`) /1.18) * (c.`gst_percentage` / 100))),2) as gst_app_amt,
                                c.`tds_percentage` as tds_percentage,
                                round((((sum(c.`md_comm`)) - (sum(c.`md_comm`) /1.18) * (c.`gst_percentage` / 100)))  * (c.`tds_percentage` / 100),2) as tds_app_amt,
                                round((((sum(c.`md_comm`)) - (sum(c.`md_comm`) /1.18) * (c.`gst_percentage` / 100))) - (((sum(c.`md_comm`)) - (sum(c.`md_comm`) /1.18) * (c.`gst_percentage` / 100)))  * (c.`tds_percentage` / 100),2) as net_com
                                ');
                $where['c.md_id'] = $session_id;
                $where['c.comm_distrubute_md'] = 'Y';
                $join1 = 'u.id = c.md_id';
                // $join = 'c.md_id = w.user_id';
            } elseif ($this->session->userdata('role_id') == AREA_DISTRIBUTOR_ROLE_ID) {
                $this->datatables->select('1,
                                DATE_FORMAT(c.comm_calculate_date,"%M %Y") as com_month,
                                concat(u.first_name," ",u.last_name) as agent_name,
                                round(sum(c.`ad_comm`),2) as comm_amt,
                                round(c.`gst_percentage`,2) as gst_percentage,
                                round((sum(c.`ad_comm`) /1.18) * (c.`gst_percentage` / 100),2) as gst_amt,
                                round((((sum(c.`ad_comm`)) - (sum(c.`ad_comm`) /1.18) * (c.`gst_percentage` / 100))),2) as gst_app_amt,
                                c.`tds_percentage` as tds_percentage,
                                round((((sum(c.`ad_comm`)) - (sum(c.`ad_comm`) /1.18) * (c.`gst_percentage` / 100)))  * (c.`tds_percentage` / 100),2) as tds_app_amt,
                                round((((sum(c.`ad_comm`)) - (sum(c.`ad_comm`) /1.18) * (c.`gst_percentage` / 100))) - (((sum(c.`ad_comm`)) - (sum(c.`ad_comm`) /1.18) * (c.`gst_percentage` / 100)))  * (c.`tds_percentage` / 100),2) as net_com
                                ');

                $where['c.ad_id'] = $session_id;
                $where['c.comm_distrubute_ad'] = 'Y';
                $join1 = 'u.id = c.ad_id';
                // $join = 'c.ad_id = w.user_id';
            } else if ($this->session->userdata('role_id') == DISTRIBUTOR) {
                $this->datatables->select('1,
                                DATE_FORMAT(c.comm_calculate_date,"%M %Y") as com_month,
                                concat(u.first_name," ",u.last_name) as agent_name,
                                round(sum(c.`di_comm`),2) as comm_amt,
                                round(c.`gst_percentage`,2) as gst_percentage,
                                round((sum(c.`di_comm`) /1.18) * (c.`gst_percentage` / 100),2) as gst_amt,
                                round((((sum(c.`di_comm`)) - (sum(c.`di_comm`) /1.18) * (c.`gst_percentage` / 100))),2) as gst_app_amt,
                                c.`tds_percentage` as tds_percentage,
                                round((((sum(c.`di_comm`)) - (sum(c.`di_comm`) /1.18) * (c.`gst_percentage` / 100)))  * (c.`tds_percentage` / 100),2) as tds_app_amt,
                                round((((sum(c.`di_comm`)) - (sum(c.`di_comm`) /1.18) * (c.`gst_percentage` / 100))) - (((sum(c.`di_comm`)) - (sum(c.`di_comm`) /1.18) * (c.`gst_percentage` / 100)))  * (c.`tds_percentage` / 100),2) as net_com
                                ');
                $where['c.di_id'] = $session_id;
                $where['c.comm_distrubute_di'] = 'Y';
                $join1 = 'u.id = c.di_id';
                // $join = 'c.di_id = w.user_id';            
            } elseif ($this->session->userdata('role_id') == RETAILER_ROLE_ID) {
                $this->datatables->select('1,
                                DATE_FORMAT(c.comm_calculate_date,"%M %Y") as com_month,
                                concat(u.first_name," ",u.last_name) as agent_name,
                                round(sum(c.`agent_comm`),2) as comm_amt,
                                round(c.`gst_percentage`,2) as gst_percentage,
                                round((sum(c.`agent_comm`) /1.18) * (c.`gst_percentage` / 100),2) as gst_amt,
                                round((((sum(c.`agent_comm`)) - (sum(c.`agent_comm`) /1.18) * (c.`gst_percentage` / 100))),2) as gst_app_amt,
                                c.`tds_percentage` as tds_percentage,
                                round((((sum(c.`agent_comm`)) - (sum(c.`agent_comm`) /1.18) * (c.`gst_percentage` / 100)))  * (c.`tds_percentage` / 100),2) as tds_app_amt,
                                round((((sum(c.`agent_comm`)) - (sum(c.`agent_comm`) /1.18) * (c.`gst_percentage` / 100))) - (((sum(c.`agent_comm`)) - (sum(c.`agent_comm`) /1.18) * (c.`gst_percentage` / 100)))  * (c.`tds_percentage` / 100),2) as net_com
                                ');
                $where['c.agent_id'] = $session_id;
                $where['c.comm_distrubute_agent'] = 'Y';
                $join1 = 'u.id = c.agent_id';
                // $join = 'c.agent_id = w.user_id';
            }

            $this->datatables->from('cb_commission_distribution c');
            // $this->datatables->join('cb_commission_distribution c', $join , 'inner');
            $this->datatables->join('users u', $join1, 'inner');
            // $this->datatables->where('w.added_by =','monthly cron');
            // $this->datatables->where("comment like '%msrtc commission amount added%'");
            $this->datatables->where($where);

            $this->datatables->group_by("DATE_FORMAT(c.comm_calculate_date,'%Y-%m')");
            // $this->datatables->group_by("DATE_FORMAT(w.added_on,'%Y-%m-%d')");

            if ($input['from_date']) {
                $this->datatables->where("DATE_FORMAT(c.comm_calculate_date, '%Y-%m-%d')>=", date('Y-m-d', strtotime($input['from_date'])));
            }

            if ($input['to_date']) {
                $this->datatables->where("DATE_FORMAT(c.comm_calculate_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
            }

            if ($response_type != '') {
                $data = $this->datatables->generate($response_type);
            } else {
                $data = $this->datatables->generate();
            }

            return $data;
        }
    }

    public function sms_log_report() {
        load_back_view(SMS_LOG_REPORT, $data);
    }

    public function sms_log_report_list() {
        $input = $this->input->post();
        $data = $this->report_model->get_sms_log_list($input, 'json');
        $decode_data = json_decode($data);
        if (!empty($decode_data->data)) {
            foreach ($decode_data->data as &$detail) {
                $detail['2'] = rokad_decrypt($detail['2'], $this->config->item('pos_encryption_key'));
            }
        }
        $data1 = json_encode($decode_data);
        echo $data1;
    }

    public function sms_log_report_list_excel() {
        $input = $this->input->get();
        $result = $this->report_model->get_sms_log_list($input, 'array');
        $result_array = $result['aaData'];
        $data = array();
        $sr_no = 1;
        if (!empty($result_array)) {
            foreach ($result_array as $key => $value) {
                $message = rokad_decrypt($value['message'], $this->config->item('pos_encryption_key'));
                $data[] = array('Sr. No.' => $sr_no,
                    'Mobile Number' => $value['supplied_mobile_number'],
                    'Message' => $message,
                    'Message Status' => $value['status'],
                    'Message Date & Time' => $value['date'],
                );
                $sr_no++;
            }
        }
        $this->excel->data_2_excel('sms_log_report_excel_' . time() . '.xls', $data);
        die;
    }

    /* function waybillwise_consoliated_report() {
      load_back_view(WAYBILLWISE_CONSOLIATED_REPORT);
      }

      function list_of_waybillwise_consoliated_report() {
      $data = $this->input->post();
      $user_result = $this->report_model->waybillwise_consilated_excel($input);

      }

      function waybillwise_consoliated_report_excel() {

      $input = $this->input->get();

      $user_result = $this->report_model->waybillwise_consilated_excel($input);
      $fields = array("Sr No.", "Registration Date", "User Name", "User Type","State", "City", "Mobile No", "Email");
      $stockArr = array();
      $sr_no = 1;

      foreach ($user_result as $key => $value) {

      $data[] = array('Sr No' => $sr_no,
      'Registration Date' => $value['created_date'],
      'User Name' => $value['Username'],
      'User Type' => $value['UserType'],
      'State' => $value['State'],
      'City' => $value['City'],
      'Mobile No' => rokad_decrypt($value['Mobile'], $this->config->item("pos_encryption_key")),
      'Email Id' => rokad_decrypt($value['Email'], $this->config->item("pos_encryption_key"))
      );
      $sr_no++;
      }
      $this->excel->data_2_excel('pending_user_report_excel' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
      }
     */

    //Ticket report
    function ticket_report() {
        load_back_view(TICKET_REPORT_VIEW);
    }

    function ticket_report_excel() {
        $input = $this->input->get();
        $type = "array";
        $ticket_nos = array();
        $ta = $bf = $aa = $sc = 0.00;
        $ticket_data = $this->report_model->ticket_report_excel($input);
        // show($ticket_data,1);

        if (count($ticket_data) > 0) {
            $sr_no = 1;
            foreach ($ticket_data as $row => $value) {
                foreach ($value as $k => $v) {
                    $data[] = array('Sr. No.' => $sr_no,
                        'Collection Date' => $v['collection_time'],
                        'Transaction Date' => $v['ticket_date'] . " " . $v['ticket_time'],
                        'Sales Agent Name' => $v['agent_name'],
                        'Sales Agent Code' => $v['agent_code'],
                        'Waybill No' => " " . $v['waybill_no'],
                        'Ticket No' => " " . $v['ticket_number'],
                        'Depot' => $v['deponame'],
                        'Total Amount' => $v['total_amt'],
                        'CAA Amount' => $v['caa']
                    );
                    $sr_no++;
                    // $aa += $v['ASN_amount'];
                    // $sc += $v['service_charge'];
                    // if(!in_array($v['ticket_number'],$ticket_nos)){
                    //     $ticket_nos[]= $v['ticket_number'];
                    //     $ta += $v['total_amt'];
                    //     $bf += $v['base_fare'];
                    // }
                }
            }
            $this->excel->data_2_excel('ticket_report_excel' . date('Y-m-d') . '_' . time() . '.xls', $data);
            die;
        } /* else {
          $data['result'] = 'Data not found...';
          load_back_view(TICKET_REPORT_VIEW,$data);
          } */
    }

    /**
     * [index description] : Use to see all user wallet transaction detail
     */
    public function users_wallet_transactions() {
        $data['tran_reason'] = $this->lookup_detail_model->get_relevant_reason();
        load_back_view(ALL_USER_WALLET_TRANS_VIEW, $data);
    }

    public function get_users_wallet_trans_detail() {
        is_ajax_request();
        $input = $this->input->post();
        $data = $this->report_model->get_users_wallet_trans_detail($input, 'json');
        echo $data;
    }

    public function users_wallet_transaction_report_excel() {
        $input = $this->input->get();
        $result_arr = $this->report_model->get_users_wallet_trans_detail($input, 'array');
        $sr_no = 1;

        if (!empty($result_arr)) {
            foreach ($result_arr['aaData'] as $key => $value) {
                $data[] = array('Sr.No.' => $sr_no,
                    'User Name' => $value['agent_name'],
                    'User Code' => $value['user_id'],
                    'User Type' => $value['user_role_nm'],
                    'Credited/Debited By' => $value['added_by'],
                    'Opening Balance (Rs.)' => $value['amt_before_trans'],
                    'Transaction Type' => $value['trans_type'],
                    'Amount (Rs.)' => $value['amt'],
                    'Closing Balance (Rs.)' => $value['amt_after_trans'],
                    'Transaction Date' => $value['added_date'],
                    'Comment' => $value['comment']
                );
                $sr_no++;
            }
        } else {
            $data = array();
        }
        $this->excel->data_2_excel('Users_wallet_transactions_report_excel_' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
        die;
    }

    public function agent_excel() {
        $input = $this->input->get();
        $user_result = $this->report_model->agent_registration_detail($input);
        $fields = array("Sr No.", "Registration Date", "AgentName", "AgentCode", "State", "City", "Mobile", "Email");
        $stockArr = array();
        $sr_no = 1;
        $status = '';
        foreach ($user_result as $key => $value) {

            if ($value['status'] == 'Y') {
                $status = 'Active';
            } else {
                $status = 'Deactive';
            }

            $data[] = array('Sr no' => $sr_no,
                'Registration Date' => $value['created_date'],
                'Registration Time' => $value['created_time'],
                'Agent Name' => $value['AgentName'],
                'Agent Code' => $value['AgentCode'],
                // 'State' => $value['State'],
                'City' => $value['City'],
                'Mobile No' => rokad_decrypt($value['Mobile'], $this->config->item("pos_encryption_key")),
                'Email Id' => rokad_decrypt($value['Email'], $this->config->item("pos_encryption_key")),
                'Interested Service' => $value['interested_service'],
                'Depot Name' => $value['signup_depot_name'],
                'Status' => $status,
                'Shop Location' => $value['shop_value'],
                'FME/DH Name' => $value['ref_empname'],
                'Current Business' => $value['current_business']
            );
            $sr_no++;
        }
        $this->excel->data_2_excel('agent_registration_detail_excel' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
    }

    /**
     * Written by Prabhat
     * Desc: show and export the agent reports
     * edited by sonali on 01march2019
     * edited by sonali on 29march2019
     * edited by sonali on 17march2019
     */
    public function daily_commission_common_vas($input, $response_type = '') {
        $role_id = $this->session->userdata(role_id);
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');
        $get_role_id =   trim($this->input->post('level_type'));
        $level_names =   trim($this->input->post('level_names'));
        $report_type = $input['report_type'];
        
        // modified by lilawati
        $this->datatables->select('1,
                                    cd.id as id,
                                    sm.service_name as service_name,                                    
                                    (select concat(uu.first_name," ",uu.last_name) from users uu where uu.id=u.level_3 ) as regional_name,
                                    (select concat(uu.first_name," ",uu.last_name) from users uu where uu.id=u.level_4 ) as divisional_name,
                                    (select concat(uu.first_name," ",uu.last_name) from users uu where uu.id=u.level_5 ) as executive_name,                                                                      
                                    concat(u.first_name," ",u.last_name) as agent_name,
                                    DATE_FORMAT(cd.created_date,"%d-%m-%Y %H:%i:%s") as comm_calculate_date,                                    
                                    round(sum(cd.`transaction_amt`+cd.customer_charge),2) as comm_applicable_amt, 
                                    round(sum(cd.`rd_amt_with_tds_gst`),2) as comm_amt,
                                    "nm",
                                    
                                    ct.txn_ref_no,
                                    (CASE WHEN cd.service_id= '.SMART_CARD_SERVICE.' THEN ct.txn_ref_no ELSE "Nan" END  ) as txn_ref_no,
                                        
                                    round(cd.`rd_per`,2) as comm_perc,
                                    
                                    (case when '.$role_id.' = '.MASTER_DISTRIBUTOR_ROLE_ID.' then u.`level_3` else u.`level_3`  end) as RD,  
                                    (case when '.$role_id.' = '.AREA_DISTRIBUTOR_ROLE_ID.' then u.`level_4` else u.`level_4`  end) as DD, 
                                    (case when '.$role_id.' = '.DISTRIBUTOR.' then u.`level_5` else u.`level_5`  end) as Executive,  
                                    round(cd.`gst_percentage`,2) as gst_percent,
                                    cd.`gst_percentage` as gst_amt,
                                    round(sum(cd.`company_amt_with_tds_gst`),2) as gst_app_amt,
                                    round(cd.`tds_percentage`,2) as tds_percent,
                                    cd.`tds_percentage` as tds_amt,
                                    round(sum(cd.`trimax_earnings`),2) as comm_act_amt,
                                   
                                    round(cd.`rd_per`,2) as rd_comm_perc,
                                    round(cd.`dd_per`,2) as dd_comm_perc,
                                    round(cd.`ex_per`,2) as ex_comm_perc,
                                    round(cd.`sa_per`,2) as sa_comm_perc,
                                    round(cd.`company_per`,2) as company_comm_perc,
                                    round(cd.`rokad_trimax_per`,2) as trimax_comm_perc,    

                                    round(sum(cd.`rd_amt`),2) as rd_comm_amt,
                                    round(sum(cd.`dd_amt`),2) as dd_comm_amt,
                                    round(sum(cd.`ex_amt`),2) as ex_comm_amt,
                                    round(sum(cd.`sa_amt`),2) as sa_comm_amt,
                                    round(sum(cd.`company_amt`),2) as company_comm_amt,
                                    round(sum(cd.`rokad_trimax_amt`),2) as trimax_comm_amt,
                                    
                                    round(sum(cd.`sa_amt`+ cd.`ex_amt`+ cd.`dd_amt` + cd.`rd_amt`+cd.`company_amt`+cd.`rokad_trimax_amt`),2) as total_comm_amt,

                                    round(sum(cd.`rd_amt_with_tds_gst`),2) as rd_act_comm_amt,
                                    round(sum(cd.`dd_amt_with_tds_gst`),2) as dd_act_comm_amt,
                                    round(sum(cd.`ex_amt_with_tds_gst`),2) as ex_act_comm_amt,
                                    round(sum(cd.`sa_amt_with_tds_gst`),2) as sa_act_comm_amt,
                                    round(sum(cd.`company_amt_with_tds_gst`),2) as company_act_comm_amt,
                                    round(sum(cd.`rokad_trimax_amt_with_tds_gst`),2) as trimax_act_comm_amt                                   
                                   ');

        $this->datatables->from('vas_commission_detail cd');
        $this->datatables->join('users u', 'u.id = cd.user_id', 'inner');
        $this->datatables->join('service_master sm', 'sm.id = cd.service_id', 'inner');  
        if (cd.service_id==SMART_CARD_SERVICE) {
        $this->datatables->join('ps_card_transaction ct', 'ct.wallet_trans_no = cd.transaction_no', 'inner');
        }else{
             $this->datatables->join('ps_card_transaction ct', 'ct.wallet_trans_no != cd.transaction_no', 'inner');
        }
        
        
        if (!empty($session_level) && isset($session_level)) {
            if ($this->session->userdata('role_id') == MASTER_DISTRIBUTOR_ROLE_ID) {//RD
                // $this->datatables->join('users u', 'u.id = cd.user_id', 'inner');
                // $this->datatables->where('cd.user_id',$session_id);
                if(!empty($level_names) && $level_names=='all'){
                    
                }  else {

                    if($get_role_id==AREA_DISTRIBUTOR_ROLE_ID){
                        $this->datatables->where("cd.user_id IN(select id from users where level_4='" . $level_names . "')");

                    }elseif($get_role_id==DISTRIBUTOR){
                        $this->datatables->where("cd.user_id IN(select id from users where level_5='" . $level_names . "')");

                    }elseif($get_role_id==RETAILER_ROLE_ID){
                        $this->datatables->where("cd.user_id IN(select id from users where id='" . $level_names . "')");
                    }

                }
                $this->datatables->where("cd.user_id IN(select id from users where level_3='" . $session_id . "')");
                $this->datatables->edit_column('comm_amt', '$1', 'total_comm_amt');
                $this->datatables->edit_column('comm_perc', '$1', 'rd_comm_perc');
                $this->datatables->edit_column('comm_act_amt', '$1', 'rd_act_comm_amt');
            } elseif ($this->session->userdata('role_id') == TRIMAX_ROLE_ID) {//rokad trimax value
                /* $this->datatables->join('users u', 'u.id = cd.user_id', 'inner');
                  $this->datatables->where('cd.user_id',$session_id); */
                if(!empty($level_names) && $level_names=='all'){
            
                }else {

                    if($get_role_id==MASTER_DISTRIBUTOR_ROLE_ID){
                        $this->datatables->where("cd.user_id IN(select id from users where level_3='" . $level_names . "')");

                    }elseif($get_role_id==AREA_DISTRIBUTOR_ROLE_ID){
                        $this->datatables->where("cd.user_id IN(select id from users where level_4='" . $level_names . "')");

                    }elseif($get_role_id==DISTRIBUTOR){
                        $this->datatables->where("cd.user_id IN(select id from users where level_5='" . $level_names . "')");

                    }elseif($get_role_id==RETAILER_ROLE_ID){
                        $this->datatables->where("cd.user_id IN(select id from users where id='" . $level_names . "')");
                    }

                }
                $this->datatables->where("cd.user_id IN(select id from users where level_1='" . $session_id . "')");
                $this->datatables->edit_column('comm_perc', '$1', 'trimax_comm_perc');
                $this->datatables->edit_column('comm_amt', '$1', 'total_comm_amt');
                $this->datatables->edit_column('comm_act_amt', '$1', 'trimax_act_comm_amt');
            } elseif ($this->session->userdata('role_id') == AREA_DISTRIBUTOR_ROLE_ID) {//dd
                /* $this->datatables->join('users u', 'u.id = cd.user_id', 'inner');
                  $this->datatables->where('cd.user_id',$session_id); */
               
                if(!empty($level_names) && $level_names=='all'){
             
                }  else {

                    if($get_role_id==DISTRIBUTOR){
                        $this->datatables->where("cd.user_id IN(select id from users where level_5='" . $level_names . "')");

                    }elseif($get_role_id==RETAILER_ROLE_ID){
                        $this->datatables->where("cd.user_id IN(select id from users where id='" . $level_names . "')");
                    }

                }
                $this->datatables->where("cd.user_id IN(select id from users where level_4='" . $session_id . "')");
                $this->datatables->edit_column('comm_perc', '$1', 'dd_comm_perc');
                $this->datatables->edit_column('comm_amt', '$1', 'total_comm_amt');
                $this->datatables->edit_column('comm_act_amt', '$1', 'dd_act_comm_amt');
            }/* elseif($this->session->userdata('role_id') == ROKAD_TRIMAX_ROLE_ID) {
              /*  $this->datatables->join('users u', 'u.id = cd.user_id', 'inner');
              $this->datatables->where('cd.user_id',$session_id); */
            //$this->datatables->where("cd.user_id IN(select id from users where level_3='".$session_id."')");

            /* $this->datatables->edit_column('comm_perc', '$1','rt_comm_perc');
              $this->datatables->edit_column('comm_amt', '$1','rt_comm_amt');
              $this->datatables->edit_column('comm_act_amt', '$1','rt_act_comm_amt');

              } */ elseif ($this->session->userdata('role_id') == COMPANY_ROLE_ID) {//company
                /* $this->datatables->join('users u', 'u.id = cd.user_id', 'inner');
                  $this->datatables->where('cd.user_id',$session_id); */
                if(!empty($level_names) && $level_names=='all'){
            
                }else {

                    if($get_role_id==MASTER_DISTRIBUTOR_ROLE_ID){
                        $this->datatables->where("cd.user_id IN(select id from users where level_3='" . $level_names . "')");

                    }elseif($get_role_id==AREA_DISTRIBUTOR_ROLE_ID){
                        $this->datatables->where("cd.user_id IN(select id from users where level_4='" . $level_names . "')");

                    }elseif($get_role_id==DISTRIBUTOR){
                        $this->datatables->where("cd.user_id IN(select id from users where level_5='" . $level_names . "')");

                    }elseif($get_role_id==RETAILER_ROLE_ID){
                        $this->datatables->where("cd.user_id IN(select id from users where id='" . $level_names . "')");
                    }

                }
                  
                $this->datatables->where("cd.user_id IN(select id from users where level_2='" . $session_id . "')");
                $this->datatables->edit_column('comm_perc', '$1', 'company_comm_perc');
                $this->datatables->edit_column('comm_amt', '$1', 'total_comm_amt');
                $this->datatables->edit_column('comm_act_amt', '$1', 'company_act_comm_amt');
                
            } elseif ($this->session->userdata('role_id') == DISTRIBUTOR) { //executive
               
                /* $this->datatables->where('cd.user_id',$session_id);
                  $this->datatables->join('users u', 'u.id = cd.user_id', 'inner'); */
                if(!empty($level_names) && $level_names=='all'){
             
                }  else {

                    if($get_role_id==RETAILER_ROLE_ID){
                        $this->datatables->where("cd.user_id IN(select id from users where id='" . $level_names . "')");
                    }

                }
                $this->datatables->where("cd.user_id IN(select id from users where level_5='" . $session_id . "')");

                $this->datatables->edit_column('comm_perc', '$1', 'ex_comm_perc');
                $this->datatables->edit_column('comm_amt', '$1', 'total_comm_amt');
                $this->datatables->edit_column('comm_act_amt', '$1', 'ex_act_comm_amt');
            } elseif ($this->session->userdata('role_id') == RETAILER_ROLE_ID) {//sales agent
                
                $this->datatables->where('cd.user_id', $session_id);
                //$this->datatables->join('users u', 'u.id = cd.user_id', 'inner');
                
                $this->datatables->edit_column('comm_perc', '$1', 'sa_comm_perc');
                $this->datatables->edit_column('comm_amt', '$1', 'total_comm_amt');
                $this->datatables->edit_column('comm_act_amt', '$1', 'sa_act_comm_amt');
            }
        }
        if($this->session->userdata('role_id') == SUPERADMIN_ROLE_ID){
           
            if(!empty($level_names) && $level_names=='all'){
            
            }else {

                if($get_role_id==MASTER_DISTRIBUTOR_ROLE_ID){
                    $this->datatables->where("cd.user_id IN(select id from users where level_3='" . $level_names . "')");

                }elseif($get_role_id==AREA_DISTRIBUTOR_ROLE_ID){
                    $this->datatables->where("cd.user_id IN(select id from users where level_4='" . $level_names . "')");

                }elseif($get_role_id==DISTRIBUTOR){
                    $this->datatables->where("cd.user_id IN(select id from users where level_5='" . $level_names . "')");

                }elseif($get_role_id==RETAILER_ROLE_ID){
                    $this->datatables->where("cd.user_id IN(select id from users where id='" . $level_names . "')");
                }

            }
                  
        }
        
        $this->datatables->edit_column('comm_amt', '$1', 'total_comm_amt');
        $this->datatables->edit_column('gst_amt', '$1', 'getGstAmount(comm_amt)');
        $this->datatables->edit_column('gst_app_amt', '$1', 'getGstCommAmount(comm_amt,gst_amt)');
        $this->datatables->edit_column('tds_amt', '$1', 'getPercentageAmount(gst_app_amt,tds_percent)');
        $this->datatables->group_by("DATE_FORMAT(cd.created_date,'%Y-%m-%d %H:%i:%s')");
        
        
        

        if ($input['from_date']) {
            $this->datatables->where("DATE_FORMAT(cd.created_date, '%Y-%m-%d')>=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if ($input['to_date']) {
            $this->datatables->where("DATE_FORMAT(cd.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }
        if($input['report_type']=='all'){
            
        }elseif ($input['report_type']!='') {
            $this->datatables->where("cd.service_id=", $input['report_type']);
        }
        $this->datatables->where("cd.status=", 'Y');

        if ($response_type != '') {
            $data = $this->datatables->generate($response_type);
        } else {
            $data = $this->datatables->generate();
        }
      // last_query(1);
        return $data;
    }
    
    
    function list_of_agents_view() {
        $data = [];
        $data['states'] = $this->state_master_model->column('intStateId, stState')->as_array()->find_all();
        $data['cities'] = $this->city_master_model->column('intCityId, stCity')->as_array()->find_all();
        load_back_view(AGENT_REPORT_VIEW, $data);
    }

    function list_of_agents() {
        $role_id_name = "";
        $role_id_name = $this->input->post('role_id');

        $input = $this->input->post();
        $ids = array(RETAILER_ROLE_ID);

        $from_date = date('Y-m-d H:i:s', strtotime($input['from_date']));
        $to_date = date('Y-m-d H:i:s', strtotime($input['to_date'] . '23:59:59'));
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');
        //  sm.stState,
        $this->datatables->select('1,DATE_FORMAT(u.created_date,"%d-%m-%Y") as created_date,DATE_FORMAT(u.created_date,"%h:%i:%s") as created_time,
            concat(first_name," ",last_name) as agent_name, u.agent_code as agent_code,
          cm.stCity,mobile_no,email,group_concat(s.service_name SEPARATOR ",") as interested_service,u.signup_depot_name,u.status,sl.shop_value,ref_empname,current_business');
        $this->datatables->from('users u');
        $this->datatables->join('state_master sm', 'u.state=sm.intStateId', 'left');
        $this->datatables->join('city_master cm', 'cm.intCityId=u.city', 'left');
        $this->datatables->join('user_roles ur', 'u.role_id=ur.id', 'left');
        $this->datatables->join('retailer_service_mapping  r', 'r.agent_id=u.id', 'inner');
        $this->datatables->join('service_master s', 'r.service_id=s.id', 'left');
        $this->datatables->join('shop_location sl', 'u.shop_location=sl.id', 'left');
        //$this->datatables->where_in('u.role_id', $ids);

        if (isset($input['from_date']) && $input['from_date'] != '') {
            //  $this->datatables->where("DATE_FORMAT(u.created_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
            $this->datatables->where("DATE_FORMAT(u.created_date, '%Y-%m-%d %H:%i:%s') >=", date('Y-m-d H:i:s', strtotime($input['from_date'] . '00:00:00')));
//$this->datatables->where("DATE_FORMAT(u.created_date, '%Y-%m-%d %H:%i:%s') >='2019-01-18 00:00:00'");
        }

        if (isset($input['to_date']) && $input['to_date'] != '') {
            //    $this->datatables->where("DATE_FORMAT(u.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
            $this->datatables->where("DATE_FORMAT(u.created_date, '%Y-%m-%d %H:%i:%s') <=", date('Y-m-d H:i:s', strtotime($input['to_date'] . '23:59:59')));
//$this->datatables->where("DATE_FORMAT(u.created_date, '%Y-%m-%d %H:%i:%s') <='2019-01-18 23:59:59'");
        }
        // $this->datatables->where("u.salt !='' and u.password != ''");

        /* if (isset($input['user_id']) && $input['user_id'] != '') {
          $this->datatables->where("u.id", $input['user_id']);
          } */
        if (isset($input['select_state']) && $input['select_state'] != '') {
            $this->datatables->where("u.state", $input['select_state']);
        }
        if (isset($input['select_city']) && $input['select_city'] != '') {
            $this->datatables->where("u.city", $input['select_city']);
        }

        if (!empty($session_level)) {
            if ($session_level > TRIMAX_LEVEL) {
                $this->datatables->where('level_' . $session_level, $session_id);
            } else {
                $this->datatables->where('level_' . TRIMAX_LEVEL, $session_id);
            }
        }
        //  $this->datatables->where("u.status = 'Y'");
        // $this->datatables->where("u.is_deleted = 'N'");
        //$this->datatables->where("u.role_id",RETAILER_ROLE_ID);
        $this->datatables->where("u.reg_from", 'F');
        $this->datatables->group_by('r.agent_id');
        //  $this->db->order_by('u.created_date', 'DESC');

        $data1 = $this->datatables->generate('json');
        $newdata_array = json_decode($data1);

        $sr_no = $this->input->post('start');

        if (!empty($newdata_array->data)) {
            foreach ($newdata_array->data as &$detail) {
                $sr_no++;
                $detail['0'] = $sr_no;
                $detail['6'] = rokad_decrypt($detail['6'], $this->config->item('pos_encryption_key'));
                $detail['7'] = rokad_decrypt($detail['7'], $this->config->item('pos_encryption_key'));
            }
        }
        echo json_encode($newdata_array);
    }

    public function commission_report() {

        $service_ids = $this->db->query("select distinct(service_id) from vas_commission_detail");
        $result = $service_ids->result_array();
        $commission_array = array();

        foreach ($result as $key => $value) {
            $comm_details = $this->db->query("select * from vas_commission_detail where status ='Y' and  service_id =" . $value['service_id']);
//        $comm_details = $this->db->query("select * from vas_commission_detail where service_id = 5");
            $data = $comm_details->result_array();
            //show($data,1);

            foreach ($data as $key => $val) {
                $level1 = $this->common_model->get_value('users', 'level_1', 'id=' . $val['user_id']);
                $rd_name = $this->common_model->get_value('users', 'first_name', 'id=' . $level1->level_1);
                $agent_name = $this->common_model->get_value('users', 'first_name', 'id=' . $val['user_id']);

                $service_name = $this->common_model->get_value('service_master', 'service_name', 'id=' . $val['service_id']);
                $total_comm = $this->common_model->get_value('services_commissions', 'total_commission', 'id=' . $val['commission_id']);
                $comm_values = $this->common_model->get_value('services_commissions', '`values`', 'id=' . $val['commission_id']);
                $comm_percent = json_decode($comm_values->values);
                $earnings = round($val['transaction_amt'] * ($total_comm->total_commission / 100), 2);

                $commission_array[$service_name->service_name][] = array('id' => $val['id'],
                    'trans_date' => date('Y-m-d',strtotime($val['created_date'])),
                    'service_name' => $service_name->service_name,
                    'trans_amt' => $val['transaction_amt'],
                    'total_commission' => $total_comm->total_commission,
                    'total_commission_amt' => $val['trimax_earnings'],
                    'rd_name' => $rd_name->first_name,
                    'trim_per' => $val['rokad_trimax_per'],
                    'trimax_commission' => $val['rokad_trimax_amt'],
                    'agent_name' => $agent_name->first_name,
                    'agent_per' => $val['sa_per'],
                    'agent_commission' => $val['sa_amt']);
            }
        }


        $message .= '<table border="1"><tr><th>Sr no</th><th colspan="3">Name</th><th colspan="7">Mobile Prepaid</th></tr><tr>
                <td></td>
                <td>Trans Date</td><td>Trimax</td><td>Agent Name</td>
                <td>Revenue</td><td colspan="2">Total Commission Percent/ Earnings</td>
                <td colspan="2">Trimax Per/ Trim Comm</td><td colspan="2">Agent Per / Agent commission</td></tr>';
        foreach ($commission_array['Mobile Prepaid'] as $key => $value) {

            $message .= "<tr>
                <td>$key</td><td>" . $value['trans_date'] . "</td><td>" . $value['rd_name'] . "</td><td>" . $value['agent_name'] . "</td>
                <td>" . $value['trans_amt'] . "</td><td>" . $value['total_commission'] . "</td><td>" . $value['total_commission_amt'] . "</td>
               <td>" . $value['trim_per'] . "</td><td>" . $value['trimax_commission'] . "</td> 
              <td>" . $value['agent_per'] . "</td><td>" . $value['agent_commission'] . "</td>
                </tr>";
        }
        $message .= '</table><br><br>';

        $message .= '<table border="1"><tr><th>Sr no</th><th colspan="3">Name</th><th colspan="7">Mobile Postpaid</th></tr>
            <tr><td></td> <td>Trans Date</td><td>Trimax</td><td>Agent Name</td><td>Revenue</td><td colspan="2">Total Commission Percent/ Earnings</td>
           <td colspan="2">Trimax Per/ Trim Comm</td><td colspan="2">Agent Per / Agent commission</td></tr>';
        foreach ($commission_array['Mobile Postpaid'] as $key => $value) {

            $message .= "<tr>
                <td>$key</td><td>" . $value['trans_date'] . "</td><td>" . $value['rd_name'] . "</td><td>" . $value['agent_name'] . "</td>
                <td>" . $value['trans_amt'] . "</td><td>" . $value['total_commission'] . "</td><td>" . $value['total_commission_amt'] . "</td>
               <td>" . $value['trim_per'] . "</td><td>" . $value['trimax_commission'] . "</td> 
              <td>" . $value['agent_per'] . "</td><td>" . $value['agent_commission'] . "</td>
                </tr>";
        }
        $message .= '</table><br><br>';

        $message .= '<table border="1"><tr><th>Sr no</th><th colspan="3">Name</th><th colspan="7">DTH</th></tr>
            <tr><td></td> <td>Trans Date</td><td>Trimax</td><td>Agent Name</td>
            <td>Revenue</td><td colspan="2">Total Commission Percent/ Earnings</td>
            <td colspan="2">Trimax Per/ Trim Comm</td><td colspan="2">Agent Per / Agent commission</td></tr>';
        foreach ($commission_array['DTH'] as $key => $value) {

            $message .= "<tr>
                <td>$key</td><td>" . $value['trans_date'] . "</td><td>" . $value['rd_name'] . "</td><td>" . $value['agent_name'] . "</td>
                <td>" . $value['trans_amt'] . "</td><td>" . $value['total_commission'] . "</td><td>" . $value['total_commission_amt'] . "</td>
               <td>" . $value['trim_per'] . "</td><td>" . $value['trimax_commission'] . "</td> 
              <td>" . $value['agent_per'] . "</td><td>" . $value['agent_commission'] . "</td>
                </tr>";
        }
        $message .= '</table><br><br>';

        $message .= '<table border="1"><tr><th>Sr no</th><th colspan="3">Name</th><th colspan="7">DMT</th></tr>
            <tr><td></td> <td>Trans Date</td><td>Trimax</td><td>Agent Name</td>
            <td>Revenue</td><td colspan="2">Total Commission Percent/ Earnings</td>
            <td colspan="2">Trimax Per/ Trim Comm</td><td colspan="2">Agent Per / Agent commission</td></tr>';
        foreach ($commission_array['Dmt'] as $key => $value) {

            $message .= "<tr>
                <td>$key</td><td>" . $value['trans_date'] . "</td><td>" . $value['rd_name'] . "</td><td>" . $value['agent_name'] . "</td>
                <td>" . $value['trans_amt'] . "</td><td>" . $value['total_commission'] . "</td><td>" . $value['total_commission_amt'] . "</td>
               <td>" . $value['trim_per'] . "</td><td>" . $value['trimax_commission'] . "</td> 
              <td>" . $value['agent_per'] . "</td><td>" . $value['agent_commission'] . "</td>
                </tr>";
        }
        $message .= '</table><br><br>';

        $message .= '<table border="1"><tr><th>Sr no</th><th colspan="3">Name</th><th colspan="7">BOS</th></tr>
            <tr><td></td> <td>Trans Date</td><td>Trimax</td><td>Agent Name</td>
            <td>Revenue</td><td colspan="2">Total Commission Percent/ Earnings</td><td colspan="2">Trimax Per/ Trim Comm</td><td colspan="2">Agent Per / Agent commission</td></tr>';
        foreach ($commission_array['BOS'] as $key => $value) {

            $message .= "<tr>
                <td>$key</td><td>" . $value['trans_date'] . "</td><td>" . $value['rd_name'] . "</td><td>" . $value['agent_name'] . "</td>
                <td>" . $value['trans_amt'] . "</td><td>" . $value['total_commission'] . "</td><td>" . $value['total_commission_amt'] . "</td>
               <td>" . $value['trim_per'] . "</td><td>" . $value['trimax_commission'] . "</td> 
              <td>" . $value['agent_per'] . "</td><td>" . $value['agent_commission'] . "</td>
                </tr>";
        }
        $message .= '</table><br><br>';

          $cb_comm_details = $this->db->query("select * from cb_commission_distribution");
        $cb_data = $cb_comm_details->result_array();
        
        $cb_commission_array = array();
        foreach ($cb_data as $key => $cb_val) {
           
             $agent_name = $this->common_model->get_value('users', 'first_name', 'id=' . $cb_val['agent_id']);
             $ad_name = $this->common_model->get_value('users', 'first_name', 'id=' . $cb_val['ad_id']);
             
             $cb_commission_array[] = array('id' => $cb_val['id'],
                    'comm_app_amt' => $cb_val['comm_applicable_amt'],
                    'agent_name' => $agent_name->first_name,
                    'ad_name' => $ad_name->first_name,
                    'ad_comm' => $cb_val['ad_comm'],
                    'ad_actual_comm' => $cb_val['ad_actual_comm'],
                    'comm_calculate_date'=>$cb_val['comm_calculate_date']
                 );
        }
      
         $message .= '<table border="1"><tr><th>Sr no</th><th colspan="3">Name</th><th colspan="4">CB Commission</th></tr><tr><td></td>
                <td>Date</td><td>AD Name</td><td>Agent Name</td>
                <td>Revenue</td>
                <td>Commission Applicable Amount</td>
                <td>AD actual commission</td>
                <td>AD commission</td>
                </tr>';
        foreach ($cb_commission_array as $key => $value) {
            $message .= "<tr>
                <td>$key</td>
                 <td>" . $value['comm_calculate_date'] . "</td><td>" . $value['ad_name'] . "</td><td>" . $value['agent_name'] . "</td>
                <td> -</td>
                <td>" . $value['comm_app_amt'] . "</td>
                <td>" . $value['ad_actual_comm'] . "</td> 
                 <td>" . $value['ad_comm'] . "</td>
               
                </tr>";
        }
        $message .= '</table><br><br>';
        
        
        echo $message;
        $mailbody_array = array("subject" => "Commission Data",
            "message" => $message,
            "to" => 'vaidehitrimax@gmail.com'
        );

        $mail_result = send_mail($mailbody_array);
        die;
    }
    
    /*get level wise data
     * added by sonali on 30march19
     */
    public function get_level_wise_data() {
        $data = array();
        $input = array();
        $new_data ['rd'] = array();
        $input = $this->input->post(NULL, TRUE);
        //show($input,1);
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');
        $data ['rd'] = $this->users_model->get_rd_names($input ['id'],$session_id,$session_level);
        if (!empty($data ['rd'])) {
            foreach ($data ['rd'] as $detail) {
                //$detail->mobile_no = rokad_decrypt($detail->mobile_no, $this->config->item('pos_encryption_key'));
                $new_data ['rd'] [] = $detail;
            }
        }
        data2json($new_data);
    }
}

