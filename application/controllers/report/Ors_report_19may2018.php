<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ors_report extends CI_Controller {

    public function __construct() {
        parent::__construct();

        is_logged_in();
        set_time_limit(0);
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);

        $this->load->model(array('users_model','lookup_detail_model','state_master_model','city_master_model'));
        $this->load->library("Excel");
        $this->load->library("Datatables");
        $this->load->library('Pdf');
     }

    public function wallet_trans_report() {
        // $data['trans_desc'] = $this->lookup_detail_model->get_reason();
        $data['states'] = $this->state_master_model->get_all_state();
        $data['cities']  = $this->city_master_model->get_all_city();
        load_back_view(WALLET_TRANSACTION_REPORT,$data);
    }

    public function wallet_trans() {
        $input = $this->input->post();
        $data  = $this->wallet_trans_common($input, 'json');
        echo $data;
     }

    public function user_type_view() {
        $input = $this->input->post();
        $res = $this->users_model->agent_details($input);
        data2json($res);
    }

    public function wallet_trans_excel() {
        $input = $this->input->get();
        $result_data  = $this->wallet_trans_common($input, 'array');
        $sr_no = 1;
        $data = array();
        foreach ($result_data['aaData'] as $key => $value) {
                  $data[] = array(  'Sr. No.' => $sr_no, 
                                    'Txn Date' => $value['txn_date'], 
                                    'User Name' => $value['Agent_name'], 
                                    'User Id' => $value['agent_id'], 
                                    'User Type' => $value['user_type'], 
                                    'User Wallet Id' => $value['w_id'], 
                                    'Transaction Id' => $value['trans_id'], 
                                    'Wallet Opening Balance(Rs.)' => $value['amt_before_trans'],
                                    'Txn Amt(Rs.)' => $value['amt'],
                                    'Wallet Closing Balance(Rs.)'=> $value['amt_after_trans'],
                                    'Txn Type' => $value['type'],
                                    'Txn Type Description' => $value['title'],
                                    'Topup Mode'=> $value['topup_by'], 
                                    'PG Tracking Id' => $value['pg_tracking_id'], 
                                    'Topup Transaction Date' => $value['created_date'],
                                    'Req Added Date'=> $value['transaction_date']);
                                    $sr_no++;
        }
        $this->excel->data_2_excel('wallet_transaction_report_' . time() . '.xls', $data);
        die;

     }
     public function wallet_trans_common($input,$response_type = '') {
      
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');
        $this->datatables->select('1,
                                DATE_FORMAT(wt.added_on,"%d-%m-%Y %h:%i:%s") as txn_date,
                                concat(u.first_name," ",u.last_name) AS Agent_name,
                                ur.role_name as user_type,
                                wt.`transaction_no` AS trans_id,
                                round(wt.amt_before_trans,2) as amt_before_trans,
                                round(wt.amt,2) as amt,
                                round(wt.amt_after_trans,2) as amt_after_trans,
                                ld.`type`,
                                ld.`title`,
                                (case when wtp.topup_by IS NULL then "NA" else wtp.topup_by end) as topup_by,
                                (case when wtp.pg_tracking_id IS NULL then "NA" else wtp.pg_tracking_id end) as pg_tracking_id,
                                (case when wtp.created_date IS NULL then "NA" else DATE_FORMAT(wtp.created_date,"%d-%m-%Y %h:%i:%s") end) as created_date,
                                (case when wtp.transaction_date IS NULL then "NA" else DATE_FORMAT(wtp.transaction_date,"%d-%m-%Y %h:%i:%s") end) as transaction_date
                                ');
        
        $this->datatables->from('wallet_trans wt');
        $this->datatables->join('users u', 'u.id = wt.user_id', 'left');
        $this->datatables->join('wallet_topup wtp', 'wtp.id = wt.topup_id', 'left');
        // $this->datatables->join('tickets t', 't.ticket_id = wt.ticket_id', 'left');
        $this->datatables->join('lookup_detail ld', 'ld.id = wt.transaction_type_id', 'left');
        $this->datatables->join('city_master cm', 'u.city = cm.intCityId','left');
        $this->datatables->join('state_master sm', 'u.state = sm.intStateId','left');
        $this->datatables->join('user_roles ur', 'u.role_id=ur.id', 'left');
  
        if (!empty($session_level) && isset($session_level)) {
                $this->datatables->where('u.id', $session_id);
            }
        if(!empty($input['select_user']) && isset($input['select_user']))
        {
            $this->datatables->where("u.id", $input['select_user']);
        } 
        if(!empty($input['user_type']) && isset($input['user_type']) && $input['user_type']!='all')
        {
//            $this->datatables->where("u.level", $input['user_type']);
            $this->datatables->where("u.role_id", $input['user_type']);
        } 
        if($input['from_date'])
        {
            $this->datatables->where("DATE_FORMAT(wt.added_on, '%Y-%m-%d')>=", date('Y-m-d', strtotime($input['from_date'])));
        }
        if($input['to_date'])
        {
            $this->datatables->where("DATE_FORMAT(wt.added_on, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        } 
        
        // if($input['txn_desc'])
        // {
        //     $this->datatables->where("wt.transaction_type_id", $input['txn_desc']);
        // }  
        if($input['txn_mode'])
        {
            $this->datatables->where("wtp.topup_by", $input['txn_mode']);
        }
        if (isset($input['select_state']) && $input['select_state'] != '') {
            $this->datatables->where("u.state", $input['select_state']);
        }
        if (isset($input['select_city']) && $input['select_city'] != '') {
            $this->datatables->where("u.city", $input['select_city']);
        }
        if($input['txn_type'])
        {
            $this->datatables->where("wt.transaction_type", $input['txn_type']);
        }
        // $this->db->order_by('wt.added_on','asc');
        if ($response_type != '') {
            $data = $this->datatables->generate($response_type);
        } else {
            $data = $this->datatables->generate();
        }
        return $data;
}

    public function wallet_trans_retailer_report() {
        $data['comment'] = $this->users_model->get_wallet_comment();
        load_back_view(WALLET_TRANSACTION_RETAILER_REPORT,$data);
    }

    public function wallet_trans_retailer() {
        $input = $this->input->post();
        $data  = $this->wallet_trans_retailer_common($input, 'json');
        echo $data;
    }

    public function wallet_trans_retailer_excel() {
        $input = $this->input->get();

        $result_data  = $this->wallet_trans_retailer_common($input, 'array');
        $sr_no = 1;
        $data = array();
        foreach ($result_data['aaData'] as $key => $value) {
            $data[] = array('Sr. No.' => $sr_no, 
                            'Txn Date' => $value['txn_date'], 
                            'User Name' => $value['agent_name'], 
                            'Agent Code' => $value['agent_code'], 
                            'Opening Balance (Rs.)' => $value['amt_before_trans'], 
                            'Txn Amt (Rs.)' => $value['amt'], 
                            'Closing Balance (Rs.)' => $value['amt_after_trans'], 
                            'Txn Type' => $value['transaction_type'],
                            'Waybill No' => $value['waybill_no'],
                            'Transaction Id'=> $value['trans_id'],
                            'Topup Amount (Rs.)' => $value['waybill_topup_amt'],
                            'Ticket Sold Amount (Rs.)' => $value['consumed_amt'],
                            'Remaining Balance (Rs.)' => $value['remaining_bal'],
                            'Comment' => $value['comment'],
                            );
            $sr_no++;
    }
        $this->excel->data_2_excel('msrtc_sales_executives_report_' . time() . '.xls', $data);
        die;

    }

    public function wallet_trans_retailer_common($input,$response_type = '') {
        $session_level = $this->session->userdata('level');
        $session_id    = $this->session->userdata('id');

        $this->datatables->select('1,
                                DATE_FORMAT(wt.added_on,"%d-%m-%Y %h:%i:%s") as txn_date,
                                concat(u.first_name," ",u.last_name) as agent_name,
                                u.`agent_code` as agent_code,
                                round(wt.`amt_before_trans`,2) as amt_before_trans,
                                round(wt.amt,2) as amt,
                                round(wt.`amt_after_trans`,2) as amt_after_trans,
                                wt.`transaction_type` as transaction_type,
                                wt.`waybill_no` as waybill_no,
                                wt.`transaction_no` as trans_id,
                                round(mat.`waybill_topup_amt`,2) as waybill_topup_amt,
                                round(mat.`consumed_amt`,2) as consumed_amt,
                                round(mat.`remaining_bal`,2) as remaining_bal,
                                wt.`comment` as comment
                               ');
        
        $this->datatables->from('wallet_trans wt');
        $this->datatables->join('users u', 'u.id = wt.user_id', 'inner');
        $this->datatables->join('cb_msrtc_agent_transaction mat', 'mat.wallet_transaction_no = wt.transaction_no', 'left');  

        if (!empty($session_level) && isset($session_level)) { 
            if($this->session->userdata('role_id') == RETAILER_ROLE_ID) {
                $this->datatables->where('u.role_id',$this->session->userdata('role_id'));
                $this->datatables->where('u.id', $session_id);
            }
        }
           
        if($input['from_date']) {
            $this->datatables->where("DATE_FORMAT(wt.added_on, '%Y-%m-%d')>=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if($input['to_date']) {
            $this->datatables->where("DATE_FORMAT(wt.added_on, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }    
        
        if($input['comment']) {
            $this->datatables->where('wt.comment',$input['comment']);
        } else {
            $this->datatables->like('wt.comment','waybill');
        }

        if ($response_type != '') {
            $data = $this->datatables->generate($response_type);
        } else {
            $data = $this->datatables->generate();
        }
        return $data;
    }

}
