<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Aeps_report extends CI_Controller{    
    
    public function __construct() {
        parent::__construct();
        is_logged_in();
        set_time_limit(0);

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);
        
        $this->load->library(array('Excel'));
        $this->load->helper("url");
    }
    
    public function index(){
        
        $data['transaction_type'] = array(AEPS_CASH_WITHDRAW=>'Cash Withdraw',AEPS_BALANCE_ENQUIRY=>'Balance Enquiry',AEPS_TRANSACTION_STATUS=>'Transaction Status');
        load_back_view(AEPS_TRANSACTION_REPORT,$data);        
    }
    
    public function transaction() {
        $input = $this->input->post();
        $data = $this->aeps_transaction_details($input, 'json');
        echo $data;        
    }
    
    public function aeps_reports_excel() {
        $input = $this->input->get();
        $result_data  = $this->aeps_transaction_details($input, 'array');
        $sr_no = 1;
        $data = array();
        foreach ($result_data['aaData'] as $key => $value) {
                  $data[] = array(  'Sr. No.' => $sr_no, 
                                    'Client Id' => $value['ClientRefID'], 
                                    'agent_name' => $value['agent_name'],
                                    'mobile_no' => rokad_decrypt($value['mobile_no'], $this->config->item("pos_encryption_key")),
                                    'Transaction Mode' => $value['Service_name'], 
                                    'BankName' => $value['BankName'],
                                    'AdhaarNo' => $value['AdhaarNo'],
                                    'Transaction Amount' => $value['Amount'],
                                    'Available Balance' => $value['AvailableBalance'], 
                                    'TxnDate' => $value['TxnDate'],
                                    'TxnTime' => $value['TxnTime'],
                                    'Status' => $value['DispalyMessage']
                                    );
                                    $sr_no++;
        }
        $this->excel->data_2_excel('aeps_transaction_report_' . time() . '.xls', $data);
        die;
    }
    
    public function aeps_transaction_details($input, $response_type = '')
    {
         //echo '<pre>'; print_r($input); die();
        $user_id = $this->session->userdata('user_id');
        $this->datatables->select('1,
                                request.ClientRefID,                             concat(u.first_name," ",u.last_name) as agent_name,
                                u.mobile_no,  
                                request.Service_name, 
                                response.BankName,
                                response.AdhaarNo,
                                response.Amount,
                                response.AvailableBalance,
                                response.TxnDate,
                                response.TxnTime,
                                response.DispalyMessage
                                	
                            ');

        $this->datatables->from('aeps_request request');        
        $this->datatables->join('aeps_response response', 'request.id = response.Transaction_id', 'inner');        
        $this->datatables->join('users u', 'u.id = request.created_by', 'inner');   
        if (isset($input['from_date']) && $input['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(request.created_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['to_date']) && $input['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(request.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }
        
        if(isset($input['transaction_type']) && $input['transaction_type'] != '' ) {
            $this->datatables->where('request.SERVICEID',$input['transaction_type']);
        }else {
            $this->datatables->like('request.SERVICEID',$input['transaction_type']);
        } 
        
        $session_level = $this->session->userdata('level');
        if ($session_level == RETAILOR_LEVEL) {
            $this->db->where("request.created_by = ".$this->session->userdata('user_id')."");
           
        }
        
        if ($response_type != '') {
             if ($response_type == 'json') {
                $data = $this->datatables->generate($response_type);
                $newdata_array = json_decode($data);
                if (!empty($newdata_array->data)) {
                    foreach ($newdata_array->data as &$detail) {
                        $detail['3'] = rokad_decrypt($detail['3'], $this->config->item('pos_encryption_key'));
                    }
                }
                $data= json_encode($newdata_array);
            } elseif ($response_type == 'array') {
              $data = $this->datatables->generate($response_type);
            }
            
        } else {
            $data = $this->datatables->generate();
        }
        return $data;
    }
}


