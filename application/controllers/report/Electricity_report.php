<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Electricity_report extends CI_Controller{    
    
    public function __construct() {
        parent::__construct();
        is_logged_in();
        set_time_limit(0);

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);
        
        $this->load->library(array('Excel'));
        $this->load->helper(array("url","utilities_bbps_helper","common_helper"));
        $this->load->model(array('common_model'));
    }
    
    public function index(){
        
        $data = array();
        load_back_view(ELECTRICITY_TRANSACTION_REPORT,$data);        
    }
    
    public function transaction() {
        $input = $this->input->post();
        $data = $this->electricity_transaction_details($input, 'json');
        echo $data;        
    }
    
    public function electricity_reports_excel() {
        $input = $this->input->get();
        $result_data  = $this->electricity_transaction_details($input, 'array');
        $sr_no = 1;
        $data = array();
        foreach ($result_data['aaData'] as $key => $value) {
                  $data[] = array(  'Sr. No.' => $sr_no, 
                                    'Customer Name' => $value['customerName'],
                                    'Operator' => $value['operator'],
                                    'Consumer No' => $value['recharge_on'],
                                    'Bill No' => $value['bill_number'],
                                    'Transaction Amount' => $value['amount'],
                                    'Customer Charge' => $value['customer_charge'],
                                    'Total Amount' => $value['total_amount'], 
                                    'Mobile No' => $value['mobileno'],                                    
                                    'TxnDate' => $value['created_on'],
                                    'Reference Id' => $value['transactionReference'], 
                                    'Wallet Ref Id' => $value['wallet_transaction_no'],
                                    'Status' => $value['reason'],
                                    
                                    );
                                    $sr_no++;
        }
        $this->excel->data_2_excel('electricity_transaction_report_' . time() . '.xls', $data);
        die;
    }
    
    public function electricity_transaction_details($input, $response_type = ''){
        $user_id = $this->session->userdata('user_id');
        
        $this->datatables->select('1,concat(u.first_name," ",u.last_name) as agent_name,
                                u.mobile_no,td.customerName,rt.operator,rt.recharge_on,td.bill_number,td.amount,td.customer_charge,sum(td.amount + td.customer_charge) as total_amount,td.mobileno,rt.created_on,td.transactionReference,td.wallet_transaction_no,td.reason,td.id');
        $this->datatables->from('bbps_transaction_details td');        
        $this->datatables->join('bbps_recharge_transaction rt','td.recharge_transaction_id = rt.id or td.wallet_transaction_no = rt.transaction_no','inner'); 
        $this->datatables->join('users u', 'u.id = rt.created_by', 'inner'); 
        
        if (isset($input['from_date']) && $input['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(rt.created_on, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['to_date']) && $input['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(rt.created_on, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }
        
        $session_level = $this->session->userdata('level');
        if ($session_level == RETAILOR_LEVEL) {
            $this->db->where("rt.created_by = ".$this->session->userdata('user_id')."");
           
        }
        
        $this->datatables->group_by('rt.id');         

        $this->datatables->add_column('id', '$1', 'checkStatus(id,reason)');
        
   
        if ($response_type != '') {
             if ($response_type == 'json') {
                $data = $this->datatables->generate($response_type);
                $newdata_array = json_decode($data);
                if (!empty($newdata_array->data)) {
                    foreach ($newdata_array->data as &$detail) {
                        $detail['2'] = rokad_decrypt($detail['2'], $this->config->item('pos_encryption_key'));
                    }
                }
                $data= json_encode($newdata_array);
            } elseif ($response_type == 'array') {
              $data = $this->datatables->generate($response_type);
            }
            
        } else {
            $data = $this->datatables->generate();
        }
        //last_query(1);
        return $data;
    }
    
    public function get_customer_details() {
        $input = $this->input->post();        
        $id = $input['id'];        
        $data['customer']= $this->get_customers_details($id); 
        
        if($data['customer'][0]['receipt_data'] == ''){
            $data['customer']= $this->get_customers_details($id);
                       
            $a_mail_replacement_array = array();
            $a_mail_replacement_array = array(
                                    '{{service_name}}' => ucwords($data['customer'][0]["type"]),
                                    "{{customer_name}}" => $data['customer'][0]['customerName'],
                                    "{{mobile_no}}" => $data['customer'][0]['mobileno'],
                                    "{{paid_by}}" => 'Cash',
                                    "{{amount}}" => $data['customer'][0]['amount'],   
                                    "{{customer_charge}}" => $data['customer'][0]['customer_charge'],
                                    "{{total_amount}}" =>  $data['customer'][0]['amount'] + $data['customer'][0]['customer_charge'],
                                    "{{amount_in_words}}" => ucwords(getIndianCurrency($data['customer'][0]['amount'] + $data['customer'][0]['customer_charge'])),
                                    "{{trans_ref_no}}" => $data['customer'][0]['transactionReference'],                                
                                    "{{rokad_trans_no}}" => $data['customer'][0]['wallet_transaction_no']
                                    );
          // show($a_mail_replacement_array,1);
        $data['receipt_data'] = str_replace(array_keys($a_mail_replacement_array),array_values($a_mail_replacement_array),BBPS_ELECTRICITY_RECEIPT);
        
        /*update table receipt data start*/
        $datetime = date('Y-m-d H:i:s');
        $filePath = $_SERVER['DOCUMENT_ROOT'] . '/electricity/';
        $filename = $filePath.'Electricity_Bill_Receipt'.'_'.$datetime.'.pdf';
        
        $transactionId = $this->getTransactionDetailsById($id);                
        //show($lastInsertedId,1);
        $update_data = array();
        
        $update_data = array(
        'receipt_name' => $filename,
        'receipt_data' => $data['receipt_data']
        );
        
        //show($update_data,1);
        $table_name = 'bbps_transaction_details';
        $id = $this->common_model->update($table_name, 'id', $transactionId, $update_data);
        
        /*update table receipt data end*/
        
        $content[] = array(
            'receipt_data' => $data['receipt_data'],
        );
        $data['customer'] = $content;
        
        }

        if($data['customer']){
             $data['flag'] = "@#success#@";
        }
        else
        {
            $data['flag'] = "@#failed#@";
        }
        data2json($data);
    } 
    
    public function get_customers_details($id) {
        $this->db->select('td.type,'
                . 'td.customerName,'
                . 'td.operator,'
                . 'rt.recharge_on,'
                . 'td.bill_number,'
                . 'td.amount,'
                . 'td.customer_charge,'
                . 'sum(td.amount + td.customer_charge) as total_amount,'
                . ' td.type,td.mobileno,td.transactionReference,'
                . 'td.wallet_transaction_no,'
                . 'td.receipt_data');
        $this->db->from('bbps_transaction_details td');        
        $this->db->join('bbps_recharge_transaction rt','td.wallet_transaction_no = rt.transaction_no','inner'); 
        $this->db->where('td.id',$id);
        $query=$this->db->get();
//        show($this->db->last_query(),1);
        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        return false;
        
    }
    
    
    public function getTransactionDetailsById($id){
        $this->db->select('id');
        $this->db->from('bbps_transaction_details');
        $this->db->where('id',$id);
        $query = $this->db->get()->result_array();
       // last_query(1);
       //echo $this->db->last_query();
        return $query[0]['id'];  
    }
    
}


