<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Operator_report extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        is_logged_in();
        $this->load->model(array('op_master_model'));
        $this->load->library("Excel");
        $this->load->helper('dompdf');
        $this->load->library('Pdf');
    }

    public function index(){
        $data=array();
        $data['op_name']=$this->op_master_model->get_op_names();
        load_back_view(OPERATOR_REPORT_VIEW,$data);
    }

        
    public function filter_get_list() {
        
       $input=array(); 
       $input=$this->input->post();
     
       $data='';
       $data['op_name']=$this->op_master_model->get_op_names();
       $data['from_date'] ='';
       $data['to_date'] ='';
       $data['op_id'] ='';
       if(isset($input) && count($input)>0){
            $input['user_id']       =$this->session->userdata('user_id');
            
            $data['result_data']    =$this->op_master_model->op_details_filter($input);
           if(isset($input['from_date']) && $input['from_date']!=''){
            $data['from_date']      =$input['from_date'];
           } if(isset($input['to_date']) && $input['to_date']!=''){
            $data['to_date']        =$input['to_date'];
           } if(isset($input['op_id']) && $input['op_id']!=''){
            $data['op_id']        =$input['op_id'];
           }         
       }
     
       load_back_view(OPERATOR_REPORT_VIEW,$data);
      
    }
    
   
        
    public function export_excel(){
        
        $input=$this->input->get();
        
        $data['user_id'] =$this->session->userdata('user_id');
      //  $data['op_id']   =$this->session->userdata('op_id');
        $result_data=$this->op_master_model->op_details_filter($input);
      
        $sr_no=1;
        $data = array();
        foreach($result_data as $key=> $value)
         { 
            $data[]=array('Sr no'=> $sr_no  ,'Operator Name' => $value['op_name'],
                            'Operator Type'=> $value['op_type']  ,
                            'Operator City' => $value['op_city'],
                            'Operator State'=>$value['op_state'],
                            'Operator Country'=>$value['op_country'],
                            'Operator email'=>$value['op_email'] ,
                            'Operator Phone'=>$value['op_phone_no'],
                            'Operator Created Date'=>$value['created_date']);
            $sr_no++;
         }
        $this->excel->data_2_excel('rpt_operator_report'.time().'.xls', $data);
        die;  
    }
        
    public function export_pdf(){

       $input=$this->input->get();
       $input['user_id'] =$this->session->userdata('user_id');
      // $input['op_id']   =$this->session->userdata('op_id');
       $dateRangeStr=date('Y-m-d');
       $result_data=$this->op_master_model->op_details_filter($input);
       $pdf_gen=$this->generatePdf($result_data);
       


   }
   
   public function generatePdf($result)
    {
        ini_set("memory_limit", "512M");
        ob_start(); 
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('Reservation Report');
        $pdf->SetHeaderMargin(20);
        $pdf->SetTopMargin(10);
        $pdf->setFooterMargin(20);
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);        
        $pdf->SetAutoPageBreak(true,PDF_MARGIN_FOOTER);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');
        $pdf->AddPage();
        $html='<center>Operator Report:</center><br><br>
            <table width="100%" border="1" cellspacing="0" cellpadding="0"  class="table table-hover">
             <thead><tr>
            <th >Sr No.</th>
            <th >Operator Name</th>
            <th >Operator Type</th>
            <th >Operator City</th>
            <th >Operator State</th>
            <th >Operator Country</th>
            <th >Operator email</th>
            <th >Operator Phone</th>
            <th >Operator Created Date</th>

           </tr></thead><tbody>';
                    $sr_no=1;
                    foreach ($result as $key =>$value): 
                        $html.="<tr><td>".$sr_no ."</td>";
                        $html.="<td>".$value['op_name']."</td>";
                        $html.="<td>".$value['op_type']."</td>";
                        $html.="<td>".$value['op_city']."</td>";
                        $html.="<td>".$value['op_state'] ."</td>";
                        $html.="<td>".$value['op_country'] ."</td>";
                        $html.="<td>".$value['op_email'] ."</td>";
                         $html.="<td>".$value['op_phone_no'] ."</td>";
                        $html.="<td>".$value['created_date'] ."</td></tr>";
                        
                        $sr_no++;
                    endforeach;
        $html.='</tbody></table>';
        $pdf->writeHTML($html, true, false, true, false, '');       
      
        $pdf->Output('Operator Report.pdf', 'D');
        die;
    }
    
    
}
