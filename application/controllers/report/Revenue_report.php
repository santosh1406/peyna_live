<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Revenue_report extends CI_Controller {

    public function __construct() {
        parent::__construct();

        is_logged_in();
        set_time_limit(0);
        
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);

        $this->load->model(array('revenue_model','tickets_model','users_model', 'agent_model', 'ticket_details_model', 'routes_model', 'bus_stop_master_model', 'op_master_model', 'booking_agent_model', 'report_model', 'waybill_no_model', 'hand_held_machine_model', 'api_provider_model'));
        $this->load->library(["pagination","Excel","Datatables","Pdf"]);
    }

	public function index($id = '')
	{ 
        $data = [];
        $ticket_report = [];
        $report = [];
        $ureport = [];

        $no_report = new stdClass();
        $no_report->total_ticket_booked = 0;
        $no_report->total_passangers = 0;
        $no_report->total_fare_without_discount = 0;
        $no_report->discount_value = 0;
        $no_report->total_commission = 0;
        $no_report->total_cancel_charge = 0;
        $no_report->total_refund_amount = 0;
        $no_report->booking_date = 0;
        $no_report->total_users = 0;

        $input = $this->input->post();
		$data['transaction_status_label'] = array(
                                                        "failed" => "Failed Transaction",
                                                        "success" => "Success Transaction",
                                                        "incomplete" => "Incomplete Transaction",
                                                        "cancel" => "Cancel Transaction",
                                                        "pfailed" => "Payment Failed",
                                                        "psuccess" => "Payment Success"
                                                          );
                
             $from_date = $this->input->post('from_date');
             $to_date = $this->input->post('till_date');
             $data['from_date'] = $from_date;
             $data['till_date'] = $to_date;
		////////To get registered users start here//// 
		 if(!empty($from_date && $to_date))
		 {
                 $users_where = [
		 				"username != " => "",
		 				"password != " => "",
		 				"salt != " => "",
		 				"status" => "Y",
		 				"verify_status" => "Y",
		 				"role_id = " => USER_ROLE_ID
		                ];

		
		 $users_report = $this->users_model->column("count(id) as total_users,DATE_FORMAT(created_date, '%d-%m-%Y') AS registered_date")
		 								  ->where($users_where)
		 								  ->where("DATE_FORMAT(created_date, '%Y-%m-%d')between '".date('Y-m-d 00:00:01', strtotime($this->session->userdata('from_date')))."' and '".date('Y-m-d 23:59:59', strtotime($this->session->userdata('till_date')))."'")
		 								 // ->where("DATE_FORMAT(created_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($this->session->userdata('till_date'))))
		 								  ->group_by("DATE_FORMAT(created_date, '%d-%m-%Y')")
		 								  ->order_by("created_date","DESC")
		 								  ->find_all();
		 }////////To get registered users end here//// 
//                 show($users_report);		//$page 					= ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$uri = $id;
                $page = (isset($uri)&& $uri >0 ) ? $uri : 0;
		$per_page               = 20;
		$index = $per_page*($page-1);
		 if($index <= 0)
		 {
		 	$index = 0;
		 }
        $data['index'] = $index;
		

		 if(!empty($input['from_date']))
		 {
                $this->session->set_userdata('from_date',$input['from_date']);
         }
         if(!empty($input['till_date']))
         {
                $this->session->set_userdata('till_date',$input['till_date']);
         }
	     if(!empty($input["trans_type"]))
	     {
	     	$this->session->set_userdata('trans_type',$input['trans_type']);
	     }
	     if(!empty($input["user_type"]))
	     {
	     	$this->session->set_userdata('user_type',$input['user_type']);
	     }

	     if(!empty($input["select_user"]))
	     {
	     	$this->session->set_userdata('select_user',$input['select_user']);
	     }
	     if($input["user_type"] == '0' || $input["user_type"] == 'all')
         {
            $this->session->unset_userdata('select_user');
         }
         $result 	= $this->revenue_model->revenue_report($per_page, $page);
//         show($result,1);
	    //unset data here
	    if(empty($uri) && ($this->uri->segment(3) != 'index'))
	         {
	            $this->session->unset_userdata('trans_type');
	            $this->session->unset_userdata('from_date');
	            $this->session->unset_userdata('till_date');
	            $this->session->unset_userdata('user_type');
	            $this->session->unset_userdata('select_user');
	         }

         if(!empty($this->session->userdata('from_date')) || !empty($this->session->userdata('till_date')) ||($this->session->userdata('trans_type')))
         {  
            $input['from_date'] =   $this->session->userdata('from_date');
            $input['till_date'] =   $this->session->userdata('till_date');
            $input['select_user'] = $this->session->userdata('select_user');
            $input["trans_type"] =  $this->session->userdata('trans_type');
            $input["user_type"]  =  $this->session->userdata('user_type');
         } //show ($this->session->userdata('from_date'),1);
		$total_rows	= $result['count'];
		$data['total_rows'] = $total_rows;
		if($result["data"])
		{
			//show($result["data"]);
			$ticket_report = $result["data"];
			foreach ($ticket_report as $tkey => $tvalue)
			{
				$report[$tvalue->booking_date] = $tvalue;
			 }
			// foreach ($users_report as $ukey => $uvalue)
			// {
			// 	$ureport[$uvalue->registered_date] = $uvalue;
			// }

//			  foreach ($users_report as $urkey => $urvalue)
//			  {
//			 	$report[$urvalue->registered_date] = $urvalue;//show($urvalue,1);
//				
//			  }

			// foreach ($report as $key => $value)
			// {
			// 	if(array_key_exists($value->booking_date,$ureport))
			// 	{
			// 		$report[$value->booking_date]->total_users = $ureport[$value->registered_date]->total_users;
			// 	}
			// 	else
			// 	{
			// 		$report[$value->booking_date]->total_users = 0;
			// 	}
			// }

			

			// foreach ($users_report as $urkey => $urvalue)
			// {
			// 	$ddate = "";
			// 	$ddate = $urvalue->registered_date;
			// 	if(!array_key_exists($ddate,$report))
			// 	{
			// 		$no_report->total_users = $urvalue->total_users;
			// 		$no_report->booking_date = $ddate;
			// 		show($no_report);
			// 		$report[$ddate] = $no_report;
			// 	}
			 }

		$data["report"] = $report;

		$config["base_url"] 			= base_url() . "report/revenue_report/index";
        $config["total_rows"]        	= $total_rows;
	   // }

		$config['use_page_numbers']  = TRUE;
        $config["per_page"]          = $per_page;
        $config['num_links']         = $total_rows;
        $config['next_link']         = 'Next';
        $config['prev_link']         = 'Previous';
        
        $config['full_tag_open']     = '<ul class="pagination">';
        $config['full_tag_close']    = '</ul>';
        $config['prev_link']         = 'Previous';
        $config['prev_tag_open']     = '<li>';
        $config['prev_tag_close']    = '</li>';
        $config['next_link']         = 'Next';
        $config['next_tag_open']     = '<li>';
        $config['next_tag_close']    = '</li>';
        $config['cur_tag_open']      = '<li class="active"><a href="#">';
        $config['cur_tag_close']     = '</a></li>';
        $config['num_tag_open']      = '<li>';
        $config['num_tag_close']     = '</li>';

        $config['first_tag_open']    = '<li>';
        $config['first_tag_close']   = '</li>';
        $config['last_tag_open']     = '<li>';
        $config['last_tag_close']    = '</li>';

        $config['first_link']        = '&lt;&lt;';
        $config['last_link']         = '&gt;&gt;';

        $this->pagination->initialize($config);                        

        $data['links']          =   $this->pagination->create_links();
        $data['input']          =   $input;
		load_back_view(REVENUE_REPORT, $data);
	}
}

