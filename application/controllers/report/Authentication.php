<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Authentication extends CI_Controller
{
	public function __construct()
	{

		parent::__construct();
       //$this->default_db = get_database('bos_dev');
		$this->load->model(array('authentication_model'));
		$this->load->library('Pagination');
		$this->load->helper('date');
       //$this->load->library("Excel");
       //$this->load->library('Pdf');
		is_logged_in();
	}

	function index($id = '') {

		$per_page = 50;
		$uri = $id;
		$page = (isset($uri) && $uri > 0 ) ? $uri : 0;  
		$index = $per_page * ($page - 1);
		if ($index <= 0) {
			$index = 0;
		}

		$data['index'] = $index;
		$page = ($page > 0 ? $page : 1);
		$offset = $per_page * ($page - 1);
		if($this->session->userdata('role_id') == DEPOT_USER)
		{
			$data['authentication_details']= $this->authentication_model->get_Details($per_page, $offset);
			$data['auth_total']=$this->authentication_model->get_count();
			$data["total_rows"] = $data['auth_total'];	
		}
		else if ($this->session->userdata('role_id') == RSRTC_USER_ROLE_ID)
		{
			$data['authentication_details']= $this->authentication_model->get_auth_Details($per_page, $offset);
			//show($data['authentication_details'],1);
			$data['auth_total']=$this->authentication_model->get_auth_count();
			$data["total_rows"] = $data['auth_total'];
		}
		
		$config["base_url"] = base_url() . "report/authentication/index";
		$config["total_rows"] = $data['auth_total'];
		$config['use_page_numbers'] = TRUE;
		$config["per_page"] = $per_page;
		$config['num_links'] = $data['auth_total'];
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';

		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['first_link'] = '&lt;&lt;';
		$config['last_link'] = '&gt;&gt;';
		$this->pagination->initialize($config);
     	//show($data,1);
		if($this->session->userdata('role_id') == DEPOT_USER)
		{
			load_back_view(AUTHENTICATION_VIEW, $data);
		}
		else
		{
			load_back_view(HO_AUTHENTICATION_VIEW, $data);	
		}
      
	} 

	function approve_bot()
	{
		if(isset($_POST['datastring']))
		{
			parse_str($_POST['datastring'], $output);
			unset($output['undefined']);
			$user_id = $this->session->userdata('user_id');
			$revenue = floatval(preg_replace('/[^\d.]/', '', $output['dsaRevenue']));
			$revenue1 =number_format($revenue, 2, '.', ',');
						
			if(((int)$output['ticketno'] == (int)$output['dsaTicket']) && ($output['totalamount'] == $revenue1))
			{
				$status='approved';
			}
			else
			{
				$status='unapproved';
			}

			$waybill_no =  $output['waybill'];

			$cb_auth_id = $this->authentication_model->chk_waybill_exist($waybill_no);

			if($cb_auth_id != '')
			{
					$auth_update_arr = array(
	                'dsa_ticket' => $output['dsaTicket'],
	                'dsa_revenue' => $revenue,
	                'depot_auth_created_by'=>$user_id,
	          		'depo_auth_status'=>($status =='approved')? '1':'0',
	          		'created_by'=>$user_id
	            );
				 $this->db->set('depo_auth_created_date', 'NOW()', FALSE);
				 $this->db->set('create_date', 'NOW()', FALSE);

				 $this->db->where('id=', $cb_auth_id);
            	 $auth_id = $this->db->update('cb_auth_data', $auth_update_arr);

			}
			else
			{	
				#echo $output['totalamount'];exit;
				$total_amount = floatval(preg_replace('/[^\d.]/', '',  $output['totalamount']));
				 $auth_insert_arr = array(
	                'depot_name' => $output['depotname'],
	                'agent_id' => $output['agent_id'], 
	                'waybill_no' => $output['waybill'],
	                'duty_date' => $output['update_dt'],
	                'trip_no' => $output['trip_no'],
	                'psgr_count' => $output['ticketno'],
	                'total_amount' => $total_amount,
	                'dsa_ticket' => $output['dsaTicket'],
	                'dsa_revenue' => $revenue,
	                'depot_auth_created_by'=>$user_id,
	          		'depo_auth_status'=>($status =='approved')? '1':'0',
	          		'created_by'=>$user_id,
	          		'is_status'=>'1',
	          		'is_deleted'=>'0'
	            );
				 $this->db->set('depo_auth_created_date', 'NOW()', FALSE);
				 $this->db->set('create_date', 'NOW()', FALSE);
				 
				 $auth_id = $this->authentication_model->insert($auth_insert_arr);
			}

			 
			 if($auth_id != '')
			 {
				echo $status;
			 }
		}
	}
	function approve_hoauth()
	{
		if(isset($_POST['element_id']) != '')
		{
			$user_id = $this->session->userdata('user_id');
            $data = array("ho_auth_created_by" => $user_id, "ho_auth_status" => '1');
            $this->db->set('ho_auth_created_date', 'NOW()', FALSE);
            $this->db->where('id=', $_POST['element_id']);
            $this->db->update('cb_auth_data', $data);

            echo 'approved';
           }
     }
}	
?>