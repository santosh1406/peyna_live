<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Consolidate_report extends CI_Controller{
    public function __construct() {
        parent::__construct();
        

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);
        
        $this->load->library(array('Excel'));
        
        
    }
    public function index(){
        load_back_view(CONSOLIDATE_REPORT_VIEW);
    }
    
    public function transaction() {
        $input = $this->input->post();
        $data = $this->consolidate_transaction_details($input, 'json');
        echo $data;        
    }
    
    public function consolidate_transaction_details($input, $response_type = ''){
       
        $this->datatables->select('1,
                                oit.agent_code,                                
                                rsp.terminal_code, 
                                rsp.depot_code,
                                depo.DEPOT_NM,
                                oit.total_cards,
                                '.PER_CARD_AMOUNT.' as per_card_Amount,
                                (oit.total_cards * '.PER_CARD_AMOUNT.') Total_Amount,                                
                                (CASE 
                                    WHEN oit.updated_date THEN oit.updated_date
                                    ELSE oit.created_date
                                    END) AS Order_Date 
                            ');

        $this->datatables->from('otc_invenory_transaction oit');        
        $this->datatables->join('retailer_service_mapping rsp', 'oit.user_id = rsp.agent_id', 'inner');    
        $this->datatables->join('msrtc_replication.depots depo', 'depo.DEPOT_CD = rsp.depot_code', 'inner');    
        

        if (isset($input['from_date']) && $input['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(oit.updated_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['to_date']) && $input['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(oit.updated_date, '%Y-%m-%d')<=", date('Y-m-d'.' 23:59:59', strtotime($input['to_date'])));
        }
        

       
        $this->datatables->where('rsp.service_id',SMART_CARD_SERVICE);
        $session_level = $this->session->userdata('level');
        if ($session_level == RETAILOR_LEVEL) {
            $this->db->where("oit.user_id = ".$this->session->userdata('user_id')."");
           
        }
        
        if ($response_type != '') {
            $data = $this->datatables->generate($response_type);
        } else {
            $data = $this->datatables->generate();
        }
        //last_query(1);
        return $data;
    }
    
    public function consolidate_reports_excel() {
        $input = $this->input->get();
        $result_data  = $this->consolidate_transaction_details($input, 'array');
       
        $sr_no = 1;
        $data = array();
        
        foreach ($result_data['aaData'] as $key => $value) {              
                
                $data[] = array('Sr. No.' => $sr_no,
                    'Agent Code' => $value['agent_code'],
                    'Terminal Code' => $value['terminal_code'],
                    'Depot Code' => $value['depot_code'],
                    'Depot Name' => $value['DEPOT_NM'],
                    'Total Cards' => $value['total_cards'],
                    'Per Card Amount' => $value['per_card_Amount'],
                    'Total Amount' => $value['Total_Amount'],
                    'Order Date' => $value['Order_Date'],
                    
                );
                $sr_no++;
            }
        $this->excel->data_2_excel('consolidate_report' . time() . '.xls', $data);
        die;
    }
            
    
}
