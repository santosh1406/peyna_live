<?php

class Smart_card_report extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        is_logged_in();
        set_time_limit(0);

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);
        
        $this->load->library(array('Excel', 'Datatables', 'Pdf', 'pagination'));
        //$this->load->helper("url");
        $this->load->model('Smart_card_model', 'smart_card');
        //$this->load->helper(array('smartcard_helper', 'common_helper'));
    }
    
    

    
    public function service_pass_reg()
    {
        $data['data'] = $this->getDivRegDepData();
        $input['depots_arr'] = $data['data']['depots_arr'];
        $data['report_data'] = $this->smart_card->get_service_pass_reg_data($input);
//        echo $this->session->userdata('location_cd');
//        die;
        //show($data['report_data'],1);
        load_back_view(SERVICE_PASS_REGISTRATION_ISSUANCE_VIEW,$data);
    }
    
    public function transaction() {
        $input = $this->input->post();
        $data = $this->student_pass_registration_and_issuance_details($input, 'json');
        echo $data;        
    }
    
    public function student_pass_registration_and_issuance_details($input, $response_type = ''){
         //echo '<pre>'; print_r($input); die();
        
        $this->datatables->select('1,
                                cust_master.first_name,
                                FLOOR(DATEDIFF (NOW(), cust_master.date_of_birth)/365) AS mAge ,                                
                                cust_card_master.cust_card_id                                
                                                
                             
                                
                               
                            ');

        $this->datatables->from('ps_customer_master cust_master');        
        $this->datatables->join('ps_customer_card_master cust_card_master', 'cust_master.id = cust_card_master.customer_id', 'inner');
        
        

        if (isset($input['from_date']) && $input['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(cust_master.created_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['to_date']) && $input['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(cust_master.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }
        
//        if(isset($input['transaction_type']) && $input['transaction_type'] != '' ) {
//            $this->datatables->where('request.SERVICEID',$input['transaction_type']);
//        }else {
//            $this->datatables->like('request.SERVICEID',$input['transaction_type']);
//        } 
        
        if ($response_type != '') {
            $data = $this->datatables->generate($response_type);
        } else {
            $data = $this->datatables->generate();
        }
       // last_query(1);
        return $data;
    }
    
    public function service_pass_reg_list() {
        
        $input = $this->input->post();
        
//        if($input['depot_code'] == 'all' || $input['division_code'] == 'all') {
//            $input['depots_arr'] = $this->getAllDepData($input);
////            show($input['depots_arr'],1);
////            die;
//        } elseif($this->session->userdata('role_id') == DEPOT_ROLE_ID){
//            $input['depots_arr'] = array($this->session->userdata('location_cd'));
//        } else {
//            $input['depots_arr'] = array($input['depot_code']);
//        }

//        $from_date = str_replace('/', '-', $input['from_date']); 
//        $input['from_date'] = date('Y-m-d', strtotime($from_date));
//        
//        $to_date = str_replace('/', '-', $input['to_date']); 
//        $input['to_date'] = date('Y-m-d', strtotime($to_date));
        
        $json_arr['data'] = $this->smart_card->get_service_pass_reg_data($input);
//        show($json_arr['data'],1);
//        die;
        echo json_encode($json_arr);
    }
    
    public function getAllDepData($input) {
        $locCode = $this->session->userdata('location_cd');

        if($this->session->userdata('role_id') == ADMIN_ROLE_ID) {
            $data['region_name'] = $this->report->getRegionsName();

        } elseif($this->session->userdata('role_id') == REGION_ROLE_ID) {
            if($input['division_code'] != 'all' && $input['depot_code'] == 'all') {
                $depot_data = $this->report->getDepotsName($locCode,$input['division_code']);
                $depot_filter = array_column($depot_data,'DEPOT_CD');
            } else {
                $depot_filter = $this->report->getDepotsViaRegion($locCode);
            }

        } elseif($this->session->userdata('role_id') == DIVISION_ROLE_ID) {
            $divisionData = $this->report->getRegionViaDiv($locCode);
            $regionCode = $divisionData[0]['REGION_CD'];
            $depot_data = $this->report->getDepotsName($regionCode,$locCode);
            $depot_filter = array_column($depot_data,'DEPOT_CD');
            
        } elseif($this->session->userdata('role_id') == DEPOT_ROLE_ID) {
            $depot_filter = array($locCode);
        }
        return $depot_filter;
    }
    
    public function getDivRegDepData() {
        $data = array();
        $locCode = $this->session->userdata('location_cd');

        if($this->session->userdata('role_id') == ADMIN_ROLE_ID) {
            $data['region_name'] = $this->report->getRegionsName();

        } elseif($this->session->userdata('role_id') == REGION_ROLE_ID) {
            $data['division_name'] = $this->report->getDivisionName($locCode);
            $data['depots_arr'] = $this->report->getDepotsViaRegion($locCode);

        } elseif($this->session->userdata('role_id') == DIVISION_ROLE_ID) {
            $divisionData = $this->report->getRegionViaDiv($locCode);
            $regionCode = $divisionData[0]['REGION_CD'];
            $data['depot_name'] = $this->report->getDepotsName($regionCode,$locCode);
            $data['depots_arr'] = array_column($data['depot_name'],'DEPOT_CD');
            
        } elseif($this->session->userdata('role_id') == DEPOT_ROLE_ID) {
            $data['depots_arr'] = array($locCode);
        }
        return $data;
    }
    
    public function concession_pass_report(){
        $data = array();
        load_back_view(CONCESSION_PASS_REPORT,$data);
    }
    
    public function concession_pass_transaction() {
        $input = $this->input->post();
        $data = $this->concession_pass_transaction_details($input, 'json');
        echo $data;        
    }
    
    public function concession_pass_transaction_details($input, $response_type = ''){
        
        $this->datatables->select('1,
                                u.agent_code      ,
                                concat(u.first_name," ",u.last_name) as agent_name,
                                ccm.registration_depo_cd as DEPOT_CD, 
                                DATE_FORMAT(cm.created_date, "%d-%m-%Y") as reg_date,
                                ccm.name_on_card,
                                DATE_FORMAT(NOW(), "%Y") - DATE_FORMAT(cm.date_of_birth, "%Y") - (DATE_FORMAT(NOW(), "00-%m-%d") < DATE_FORMAT(cm.date_of_birth, "00-%m-%d")) AS date_of_birth,
                                IF(ccm.trimax_card_id IS NULL or ccm.trimax_card_id = "", "NA", ccm.trimax_card_id) as cust_card_id, 
                                ccm.dispatch_date, 
                                DATE_FORMAT(ct.expiry_date, "%d-%m-%Y") as exp_date,
                                "0" as smart_card_fee, 
                                "0" as application_fee,
                                "0" as gst_fee, 
                                at.total_amount,
                                
                                ct.txn_ref_no
                             
                            ');

//        $this->datatables->from('ps_customer_master cm');        
//        $this->datatables->join('ps_customer_card_master ccm', 'ccm.customer_id = cm.id', 'inner');    
//        $this->datatables->join('ps_card_transaction ct', 'ct.trimax_card_id = ccm.trimax_card_id', 'inner');  
//        $this->datatables->join('ps_amount_transaction at', 'at.card_transaction_id = ct.id', 'inner');
//        $this->datatables->join('users u', 'u.id = ct.created_by', 'inner');
        
        $this->datatables->from('ps_card_transaction as ct');        
        $this->datatables->join('ps_customer_card_master as ccm', 'ct.trimax_card_id = ccm.trimax_card_id', 'inner');    
        $this->datatables->join('ps_amount_transaction as at', 'at.card_transaction_id = ct.id', 'inner');  
        $this->datatables->join('ps_customer_master as cm', 'ccm.customer_id = cm.id', 'inner');
        $this->datatables->join('users as u', 'u.id = ct.created_by', 'inner');
        
        

        if (isset($input['from_date']) && $input['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(ct.created_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['to_date']) && $input['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(ct.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }
        
        $this->datatables->where('ct.transaction_type_code',REG_PPC_TRANS_TYPE_CODE); 
        $this->datatables->where('ct.pass_category_id',CONC_CARD);
        
        $this->datatables->where('ct.is_active','1'); 
        $this->datatables->where('cm.is_itms_reg','0');  
        
        
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');     
        
        if (!empty($session_level)) {
            if ($session_level > TRIMAX_LEVEL) {
                $this->datatables->where('level_' . $session_level, $session_id);
            } else {
                $this->datatables->where('level_' . TRIMAX_LEVEL, $session_id);
            }
        }
        
        $this->datatables->group_by('at.id');        
        
        if ($response_type != '') {
            $data = $this->datatables->generate($response_type);
        } else {
            $data = $this->datatables->generate();
        }
        //last_query(1);
        return $data;
    }          

    public function concession_pass_reg_excel(){
        $input = $this->input->get();
        
            $result = $this->concession_pass_transaction_details($input, 'array');
            //show($result,1);
            $data = array();
            $sr_no = 1;
            foreach ($result['aaData'] as $key => $value) {
                
                $data[] = array('Sr. No.' => $sr_no,
                    'Agent Code'=> $value['agent_code'],    
                    'Agent Name'=> $value['agent_name'],
                    'Depot Code'=> $value['DEPOT_CD'],                    
                    'Registration Date' => $value['reg_date'],                    
                    'Name' => $value['name_on_card'],
                    'Age' => $value['date_of_birth'],
                    'Card Id' => $value['cust_card_id'],                    
                    'Issuance Date' => $value['dispatch_date'],                    
                    'Expiry Date' => $value['exp_date'],
                    'Smartcard Fee' => $value['smart_card_fee'],
                    'Application Fee' => $value['application_fee'],
                    'GST Fee' => $value['gst_fee'],
                    'Total Receive' => $value['total_amount'],
                    'Transaction Ref No' => $value['txn_ref_no']
                );
                $sr_no++;
            }
            //show($data,1);
            $this->excel->data_2_excel('concession_pass_reg_'.time().'.xls',$data);
            die();
        
    }
    
    public function servicePassExcel(){
        $input = $this->input->get();
        if($input['flag'] != ''){
            $result = $this->smart_card->get_service_pass_reg_data($input);
            $data = array();
            foreach ($result as $key => $value) {
                $sr_no = $key + 1;
                $data[] = array('Sr. No.' => $sr_no,
                    'Agent Code'=> $value['agent_code'],    
                    'Agent Name'=> $value['agent_name'],
                    'Depot Code'=> $value['DEPOT_CD'],
                    'Depot Name'=> $value['DEPOT_NM'],

                    'Registration Date' => $value['reg_date'],
                    'Customer Name' => $value['name_on_card'],
                    'Age' => $value['dob'],
                    'Card Id' => $value['cust_card_id'],
                    'Card Status' => $value['card_status'],
                    'From Stop Code' => $value['from_stop_code'],
                    'Till Stop Code' => $value['till_stop_code'],
                    'Pass Amount' => $value['pass_amount'],
                    'Smart Card Fee' => $value['smart_card_fee'],
                    'Application Fee' => $value['application_fee'],
                    'GST' => $value['gst_fee'],
                    'Total Amount' => $value['total_amount'],
                    'Pass Valid From' => $value['act_date'],
                    'Pass Valid Till' => $value['exp_date']
                );
            }
            //$input = count($data);
            $headerData['reportName'] = "Service Pass Registration and Issuance Report";
            $headerData['depotCD'] = $this->session->userdata('location_cd');
            $headerData['from_date'] = $input['from_date'];
            $headerData['to_date'] = $input['to_date'];
            $headerData['username'] = $this->session->userdata('first_name').' '.$this->session->userdata('last_name');
            $headerData['headerMerge'] = 'A1:O1';
            $headerData['headerMergest'] = 'A1';
            $headerData['dateMerge'] = 'A2:C2';
            $headerData['dateMergest'] = 'A2';
            $headerData['depotCdMerge'] = 'D2:F2';
            $headerData['depotCdMergest'] = 'D2';
            $headerData['userNameMerge'] = 'M2:O2';
            $headerData['userNameMergest'] = 'M2';
            //show($headerData,1);
            $this->excel->data_2_excel('service_pass_reg'.time().'.xls',$data,$headers = true,$headerData);
            die();
        }
    }
    
    
    /* shopping wallet report 
     * created by sonali on 7-march-19
     * edited by sonali on 4-july-19
    */
    public function sw_topup() {  
        $data= array();
//        $input = array();
//        $data['report_data'] = $this->smart_card->sw_topup_data($input);
        
        load_back_view(SW_TOPUP_REPORT, $data);
    }
    
    public function sw_topup_transaction() {
        $input = $this->input->post();
        $data = $this->sw_transaction_details($input, 'json');
        //show($data,1);
        echo $data;   
    }
    
    public function sw_transaction_details($input, $response_type = ''){
        
        $this->db->select("1,
                            u.agent_code,
                            concat(u.first_name,' ',u.last_name) as agent_name,
                            ccm.registration_depo_cd as DEPOT_CD,                             
                            IF(ccm.trimax_card_id IS NULL or ccm.trimax_card_id = '', 'NA', ccm.trimax_card_id) as cust_card_id,
                            ccm.name_on_card as holder_name,
                            DATE_FORMAT(ct.created_date, '%d-%m-%Y') as topup_date,
                            at.total_amount as topup_amt,
                            '0' as loading_fee,
                            '0' as gst_fee,
                            ct.amount as load_amt,
                            ct.opening_balance as bal_before_topup,
                            ct.closing_balance as bal_after_topup
                        ");
        $this->datatables->from('ps_card_transaction as ct');        
        $this->datatables->join('ps_customer_card_master as ccm', 'ct.trimax_card_id = ccm.trimax_card_id', 'inner');    
        $this->datatables->join('ps_amount_transaction as at', 'at.card_transaction_id = ct.id', 'inner');  
        $this->datatables->join('ps_customer_master as cm', 'ccm.customer_id = cm.id', 'inner');        
        $this->datatables->join('users as u', 'u.id = ct.created_by', 'inner'); 
        
        if (isset($input['from_date']) && $input['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(ct.created_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['to_date']) && $input['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(ct.created_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['to_date'])));
        }

        $this->datatables->where("ct.transaction_type_code",REG_PPC_TRANS_TYPE_CODE); // SW - Registration
        
        $this->datatables->where("ct.is_active = '1'");
        $this->datatables->where('cm.is_itms_reg','0');  
       
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');     
        
//        if (!empty($session_level)) {
//            if ($session_level > TRIMAX_LEVEL) {
//                $this->datatables->where('level_' . $session_level, $session_id);
//            } else {
//                $this->datatables->where('level_' . TRIMAX_LEVEL, $session_id);
//            }
//        }
        $this->datatables->group_by('at.id');

        if ($response_type != '') {
            $data = $this->datatables->generate($response_type);
        } else {
            $data = $this->datatables->generate();
        }
        //last_query(1);
        return $data;
    }
    
    public function sw_topup_excel(){
        $input = $this->input->get();
        if($input['from_date']!='' && $input['to_date']!=''){
            $input['from_date'] ; 
            $input['to_date'] ; 
        }
        //show($input,1);
        if($input['flag'] != ''){
            $result = $this->smart_card->sw_topup_data($input);
            $data = array();
            foreach ($result as $key => $value) {
                $sr_no = $key + 1;
                $data[] = array('Sr. No.' => $sr_no,
                    'Agent Code'=> $value['agent_code'],    
                    'Agent Name'=> $value['agent_name'],
                    'Depot Code'=> $value['DEPOT_CD'],
                    'Depot Name'=> $value['DEPOT_NM'],
                    'Card Id' => $value['cust_card_id'],
                    'Card Holder Name' => $value['holder_name'],
                    'Topup Date' => $value['topup_date'],
                    'Topup Amount' => $value['topup_amt'],
                    'Loading Fee' => $value['loading_fee'],
                    'GST On Banking Fee' => $value['gst_fee'],
                    'Load Amount after Fee & GST' => $value['load_amt'],
                    'Actual Load Amount' => $value['load_amt'],
                    'Wallet Balance Before Topup' => $value['bal_before_topup'],
                    'Wallet Balance After Topup' => $value['bal_after_topup']
                );
            }
            //$input = count($data);
            $headerData['reportName'] = "Shopping Wallet Topup Report";
            $headerData['depotCD'] = $this->session->userdata('location_cd');
            $headerData['from_date'] = $input['from_date'];
            $headerData['to_date'] = $input['to_date'];
            $headerData['username'] = $this->session->userdata('first_name').' '.$this->session->userdata('last_name');
            $headerData['headerMerge'] = 'A1:K1';
            $headerData['headerMergest'] = 'A1';
            $headerData['dateMerge'] = 'A2:C2';
            $headerData['dateMergest'] = 'A2';
            $headerData['depotCdMerge'] = 'D2:F2';
            $headerData['depotCdMergest'] = 'D2';
            $headerData['userNameMerge'] = 'I2:K2';
            $headerData['userNameMergest'] = 'I2';
            //show($headerData,1);
            $this->excel->data_2_excel('sw_topup_report'.time().'.xls',$data,$headers = true,$headerData);
            die();
        }
    }
    
    /* travel wallet report 
     * created by sonali on 25-march-19
    */
    public function tw_topup() {     
        $input = array();        
        $data['report_data'] = $this->smart_card->tw_topup_data($input);
        //show($data['report_data'],1);
        load_back_view(TW_TOPUP_REPORT, $data);
    }
    
    public function tw_topup_list() {
        
        $input = $this->input->post();
        
        $json_arr['data'] = $this->smart_card->tw_topup_data($input);
        echo json_encode($json_arr);
    }

    public function tw_topup_excel(){
        $input = $this->input->get();
        if($input['from_date']!='' && $input['to_date']!=''){
            $input['from_date'] ;
            $input['to_date'] ;
        }
        //show($input,1);
        if($input['flag'] != ''){
            $result = $this->smart_card->tw_topup_data($input);
            $data = array();
            foreach ($result as $key => $value) {
                $load_amt_gst_bfee = ($value['topup_amt'] - $value['loading_fee'] - $value['gst_fee']);
                $sr_no = $key + 1;
                $data[] = array('Sr. No.' => $sr_no,
                    'Agent Code'=> $value['agent_code'],    
                    'Agent Name'=> $value['agent_name'],
                    'Depot Code'=> $value['DEPOT_CD'],
                    'Depot Name'=> $value['DEPOT_NM'],
                    'Card Id' => $value['cust_card_id'],
                    'Card Holder Name' => $value['holder_name'],
                    'Topup Date' => $value['topup_date'],
                    'Topup Amount' => $value['topup_amt'],
                    'Loading Fee' => $value['loading_fee'],
                    'GST On Banking Fee' => $value['gst_fee'],
                    'Load Amount after Fee & GST' => $load_amt_gst_bfee,
                    'Bonus Point' => $value['bonus_fee'],
                    'Actual Load Amount' => $value['load_amt'],
                    'Wallet Balance Before Topup' => $value['bal_before_topup'],
                    'Wallet Balance After Topup' => $value['bal_after_topup']
                );
            }
            //$input = count($data);
            $headerData['reportName'] = "Travel Wallet Topup Report";
            $headerData['depotCD'] = $this->session->userdata('location_cd');
            $headerData['from_date'] = $input['from_date'];
            $headerData['to_date'] = $input['to_date'];
            $headerData['username'] = $this->session->userdata('first_name').' '.$this->session->userdata('last_name');
            $headerData['headerMerge'] = 'A1:L1';
            $headerData['headerMergest'] = 'A1';
            $headerData['dateMerge'] = 'A2:C2';
            $headerData['dateMergest'] = 'A2';
            $headerData['depotCdMerge'] = 'D2:F2';
            $headerData['depotCdMergest'] = 'D2';
            $headerData['userNameMerge'] = 'I2:L2';
            $headerData['userNameMergest'] = 'I2';
            //show($headerData,1);
            $this->excel->data_2_excel('tw_topup_report'.time().'.xls',$data,$headers = true,$headerData);
            die();
        }
    }
    
    public function otc_card(){
        $data = array();
        $data['data'] = $this->getDivRegDepData();
        $input['depots_arr'] = $data['data']['depots_arr'];
        $data['report_data'] = $this->smart_card->get_otc_card_data($input);
        load_back_view(OTC_CARD_REPORT,$data);
    }

}
