<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Top_defaulter_report extends CI_Controller
    {
        public function __construct()
        {
            parent::__construct();
            is_logged_in();
            $this->load->library("Excel");
            $this->load->helper('dompdf');
            $this->load->model(array('referrer_master_model','state_master_model','city_master_model'));
           // $this->load->helper('file');
            $this->load->library('Pdf');
            $this->load->library('Pagination');
        }
        
        public function index() 
        {   
          $data = array();
		  $data['referrers']= $this->referrer_master_model->column('id, referrer_name,referrer_code')->as_array()->find_all(); 
		  $data['states']= $this->state_master_model->column('intStateId, stState')->as_array()->find_all();           
		  $data['cities']= $this->city_master_model->column('intCityId, stCity')->as_array()->find_all(); 
		  load_back_view(TOP_DEFAULTER_REPORT_VIEW, $data);
        }

    
        function defaulter_details() 
        {
            $input      = $this->input->post();
            $data = $this->defaulter_details_common($input, 'json');
            echo $data;
        }
        public function defaulter_details_common($input,$response_type ='') {
            if(!empty($input))
            {
                //show($input, 1);
                $data['from_date']       = $input['from_date'];
                $data['no_of_days']      = $input['no_of_days'];
                $pDate                   = strtotime(' '.$data['from_date'].' - '.$data['no_of_days'].' days');
                $date                    = date('Y-m-d',$pDate);

                eval(FME_AGENT_ROLE_ID);
                $this->datatables->select('1,concat(users.first_name," ",users.last_name) as Agent_Name,users.mobile_no,users.email,state_master.stState,city_master.stCity,( SELECT COUNT(tickets.ticket_id)  FROM tickets WHERE users.id= tickets.inserted_by AND `tickets`.`transaction_status` IN("success", "cancel") AND `tickets`.`inserted_date` >= "'.$date.'" ) As Type1,( SELECT CASE WHEN DATE_FORMAT(MAX(tickets.inserted_date), "%d-%M-%y") IS NOT NULL THEN  DATE_FORMAT(MAX(tickets.inserted_date), "%d-%M-%y") ELSE "NO Ticket Booked" END FROM tickets WHERE users.id= tickets.inserted_by AND `tickets`.`transaction_status` IN("success", "cancel") ) As last_inserted_date');
                $this->datatables->from('users');
                $this->datatables->join('tickets', 'users.id = tickets.inserted_by', 'left');
                $this->datatables->join('city_master', 'users.city = city_master.intCityId', 'left');
                $this->datatables->join('state_master', 'users.state = state_master.intStateId', 'left');
                if(isset($input['city']) && $input['city']!='')
                {
                    $this->datatables->where('users.city', $input['city']);    
                }
                if(isset($input['state']) && $input['state']!='')
                {
                    $this->datatables->where('users.state', $input['state']);    
                }
                if(isset($input['select_referrer']) && $input['select_referrer']!='')
                {
                    $this->datatables->where('users.referrer_code', $input['select_referrer']);    
                }
                $this->datatables->where_in('users.role_id', $fme_agent_role_id);
                $this->datatables->group_by('users.id');
                $this->datatables->having('Type1','0');
                if ($response_type != '') {
                    $data = $this->datatables->generate($response_type);
                } else {
                    $data = $this->datatables->generate();

                }
		return $data;
            }
        } 
        
        function defaulter_details_summary_excel()
        {
            $input = $this->input->get();
            if(!empty($input))
            {
                if(!empty($input))
                {
                    eval(FME_AGENT_ROLE_ID);
                    $data['from_date']       = $input['from_date'];
                    $data['no_of_days']      = $input['no_of_days'];
                    $pDate                   = strtotime(' '.$data['from_date'].' - '.$data['no_of_days'].' days');
                    $date                    = date('Y-m-d',$pDate);
           
                    $ticket_result = $this->defaulter_details_common($input, 'array');
                    $fields        = array("Id", "Agent", "Agent Mobile", "Agent Email","State","City","Last Ticket Booked On");
                    $stockArr      = array();
                    $sr_no         = 1;
                    if(!empty($ticket_result['aaData']))
                    {
                      foreach ($ticket_result['aaData'] as $key => $value) 
                        {
                            $data[] = array('Id'        => $sr_no,
                                            "Agent Name"            => $value['Agent_Name'],
                                            "Agent Mobile"          => $value['mobile_no'],
                                            "Email"                 => $value['email'],
                                            "State"                 => $value['stState'],
                                            "City"                  => $value['stCity'],
                                            "Last Ticket Booked On" => $value['last_inserted_date']
                                            );

                            $sr_no++;
                        }
                    }
                    else
                    {
                        $data = array(array());
                    }
                    $this->excel->data_2_excel('top_defaulter_report_summary_report' . date('Y-m-d') . '_' . time() . '.xls', $data);  
                    die;
                }
            }
        }

        public function get_statewise_city()
        {
          $data=array();
          $input=array();
          $input = $this->input->post();
          $data['city'] = $this->city_master_model->get_city($input['state_id']);
          data2json($data);
        }
    }


        
        
