<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class Pass_renewal extends REST_Controller {

    public function __construct() {
        parent::__construct();
	   	$this->load->helper(array('smartcard_helper','common_helper'));
        $this->apiCred = json_decode(SMART_CARD_CREDS, true);
        $header_value = $this->apiCred['X_API_KEY'];
        $this->header = array(
            'X-API-KEY:' . $header_value
        );

        $this->basicAuth = array(
            'username' => $this->apiCred['username'],
            'password' => $this->apiCred['password']
        );

        $this->load->model(array('Mainmodel','Customer_registration_model','Utility_model'));
        $this->load->library('PassFareCalc', 'common_library');
		$request = $this->post();

		$user_id = $request['apploginuser'];
        $session_id= $this->Mainmodel->get_session_id($this->input->post('session_id'),$user_id);
        $this->session_id = $session_id['itms_session_id'];
        $this->user_id =itms_user_id;
        
        /*log code added start here */
        
        $log_data = array();
        $log_data['url'] = current_url();
        $log_data['Request_type'] = $_SERVER['REQUEST_METHOD'];
        $log_data['get'] = $this->input->get();
        $log_data['post'] = $this->post();
        $log_data['header'] = getallheaders();
        $log_data['user'] = $this->input->server('PHP_AUTH_USER');
        $log_data['ip'] = $this->input->ip_address();
            // show($log_data);
        log_data('rest/smartcard/smart_card_pass_renewal_api_' . date('d-m-Y') . '.log', $log_data, 'api');
        $insert_data = array('username' => $_POST['apploginuser'], 'ip' => $_SERVER['REMOTE_ADDR'], 'method' => $_SERVER['REQUEST_URI'], 'type' => 'get', 'version_id' => $_POST['version_id']);
        $this->requestId = $this->Mainmodel->insert_audit_data($insert_data)."";
        /*log code added end here */
        
             /*   added code disallow multiple login of same user code start here */
        $startTime = date("Y-m-d H:i:s");
        $cenvertedTime = date('Y-m-d H:i:s',strtotime(LOGIN_EXPIRE_TIME,strtotime($startTime))) ;
        $this->Mainmodel->upDateLastActivityTime($this->input->post('session_id'),$cenvertedTime);
        /*   added code disallow multiple login of same user code end here */ 
        
    }

    public function get_pass_data_desk_post() {

        $URL = $this->apiCred['url'] . '/Pass_renewal/get_pass_data_desk';
        // $request = $this->post();
        // $data = curlForPost($URL, $request, $this->basicAuth, $this->header);

        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;
        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header); 

        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_pass_renewal_api_' . date('d-m-Y') . '.log', $json, 'api');

   //      $this->output
			// ->set_status_header(200)
			// ->set_content_type('application/json', 'utf-8')
			// ->set_output(json_encode($json));

        json_response($json);
    }

     public function pass_renew_desk_post() {

        $URL = $this->apiCred['url'] . '/Pass_renewal/pass_renew_desk';
        $request = $this->post();
        $itms_request = $this->post();
        
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;
        
        /*check wallet balance added by sonali code start here  */
           
           $bccharges= 0 ; 
        if($request['trans_type']=='BPSW')
        {
           if(!empty($request['Card_fee_details'])) 
           {
	        $Card_fee_details = json_decode($request['Card_fee_details'],TRUE);
                
                foreach ($Card_fee_details as $key => $value) {
                    
                    if(strtolower($value['fee_type_nm']) == BCCHARGES){
                      $bccharges = $value['fee_type_value'];  
                     }
                    
                }
               
           }
                 
        }
        $request['amount'] = $request['total_fare_amt']-$bccharges;
         $result = checkWalletTxn_Smartcard($request, $comments = "check wallet balance");
         $response = json_decode($result,200);
         $responseMessage = ['responseCode' =>$response['responseCode'],"responseMessage"=>$response['msg']];
         if ($response) {
             $this->response_save(array('status' => $response['status'], 'msg' => $response['status'], 'data'=>$responseMessage ), 200);
         }
       /*check wallet balance added by sonali code end here */  
        
        $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_pass_renewal_api_' . date('d-m-Y') . '.log', $json, 'api');
        if (strtolower($json->status) == 'success') {
            $postVal = $this->input->post();
            $user_name = $this->input->post('apploginuser');
            $get_card_trans_data = $this->Mainmodel->get_card_trans_id($request, 'desc');

            $is_active = '1';
            $write_flag = '1';
            
            if($postVal['trans_type'] == PASS_RENEW_SW_TRANS_TYPE || (isset($postVal) && $postVal['request_from'] == REQUEST_FROM_WEB && $postVal['trans_type'] == PASS_RENEW_CASH_TRANS_TYPE)) {
                $is_active = RENEWAL_STATUS;
                $write_flag = '0';
            }
             
            $request['pass_concession_id'] = $get_card_trans_data['pass_concession_id'];
            $request['pass_category_id'] = $get_card_trans_data['pass_category_id'];
            $request['display_card_id'] = $request['trimax_card_id'];

            $curr_pass_exp_date = strtotime($get_card_trans_data['expiry_date']);

            $span_days = $this->Mainmodel->get_span_period($request);
            $total_days = $span_days['span_period_days'] - 1;

            $activation_date = date('Y-m-d', strtotime($get_card_trans_data['expiry_date'] . '+1 days'));
            if (strtotime(TODAY_DATE) > $curr_pass_exp_date) {
                $activation_date = TODAY_DATE;
            } elseif (isset($request['activation_date']) && !empty($request['activation_date'])) {
                $activation_date = $request['activation_date'];
            }
          //  $expiry_date = date('Y-m-d', strtotime($activation_date . "+" . $total_days . " days"));
           // $card_fee_master_id['ctrans_type_id'] = 23;
            //$card_fee_master_id['id'] = 2;

            $vendor_id = $this->Customer_registration_model->get_vendor_id($request['apploginuser'])['vendor_id'];
            
            $receipt_number = $json->data->receipt_number; 


            $total_base_fare = isset($request['base_fare_1']) ? $request['base_fare_1'] : '';
            $bf = 1;
            if(isset($request['base_fare_2'])) {
                $bf = 2;
                $total_base_fare = $request['base_fare_1'] + $request['base_fare_2'];
            }

            $card_trans_data = array(
                    'trimax_card_id' => $request['trimax_card_id'],
                    'pass_type_id' => '1',
                    'pass_concession_id' => $request['pass_concession_id'],
                    'pass_category_id' => $request['pass_category_id'],
                    'activation_date' => $activation_date,
                    'expiry_date' => isset($request['expiry_date']) ? $request['expiry_date'] : '', 
                  //   'transaction_type_id' => $card_fee_master_id['ctrans_type_id'],
                    'transaction_type_code' => isset($request['trans_type']) ? $request['trans_type'] : '', 
                    'depot_cd' => $request['depot_cd'],
                    'terminal_id' => $request['terminal_id'],
                    'receipt_number' => $receipt_number,
                  //  'span_id' => isset($request['span_id']) ? $request['span_id'] : '',
                    'span_name' => isset($request['span_name']) ? $request['span_name'] : '',
                    'bus_type_cd' => isset($request['bus_type_cd']) ? $request['bus_type_cd'] : '',
                   // 'conc_service_rel_id' => isset($conc_service_rel_id) ? $conc_service_rel_id : '',
                   // 'card_fee_master_id' => $card_fee_master_id['id'],
                    'amount' => $total_base_fare,
                    'session_id' => $request['session_id'],
                    'vendor_id' => $vendor_id,
                    'created_by' => $request['apploginuser'],
                    'is_active' => $is_active
                );
            $card_trans_id = $this->Customer_registration_model->insert_customer_reg_data($card_trans_data,CARD_TRANS);


            //Update card_transaction write flag as 0 and current to 1
            $update_data = array('write_flag' => '0', 'modified_by' => $user_name);
            $this->Mainmodel->update_card_tarns_write_flag($card_trans_id,$request['trimax_card_id'],$update_data);
             //card transaction details start here

            $card_trans_details_id = $fare_data_details = $fare_data_details = $fare_data = array();

            for ($i=1; $i <= $bf; $i++) {
                $card_trans_detail_data = array(
                    'card_transaction_id' => $card_trans_id,
                    'trimax_card_id' => $request['trimax_card_id'],
                   // 'card_fee_master_id' => $card_fee_master_id['id'],
                   // 'transaction_type_code' => isset($request['trans_type']) ? $request['trans_type'] : '', 
                    'route_no' => isset($request['route_no_'.$i]) ? $request['route_no_'.$i] : '',
                    'route_type' => isset($request['route_type_'.$i]) ? $request['route_type_'.$i] : '',
                    'fare_type_cd' => isset($request['fare_type_cd_'.$i]) ? $request['fare_type_cd_'.$i] : '',
                    'service_number' => '',
                    'from_stop_code' => isset($request['from_stop_code_'.$i]) ? $request['from_stop_code_'.$i] : '',
                    'till_stop_code' => isset($request['till_stop_code_'.$i]) ? $request['till_stop_code_'.$i] : '',
                    'via_stop_code' => isset($request['via_stop_cd_'.$i]) ? $request['via_stop_cd_'.$i] : '',
                    'amount' => isset($request['base_fare_'.$i]) ? $request['base_fare_'.$i] : '',
                    'session_id' => $request['session_id'],
                    'created_by' => $request['apploginuser'],
                    'is_active' => $is_active
                );

       
			if(isset($request['base_fare_'.$i]) && !empty($request['base_fare_'.$i]) && $request['base_fare_'.$i] > 0 && !in_array($request['pass_category_id'], array(OTC_CARD,TRAVEL_AS_U_LIKE_CARD))) {

                    $fare_data_details = isset($request['fare_data_'.$i]) ? $request['fare_data_'.$i] : '';

                    $fare_data_details_arr = json_decode($fare_data_details,TRUE);
                    $per_day_base_fare = $fare_data_details_arr['per_day_base_fare'];
                    $card_trans_detail_data['per_day_base_fare'] = $per_day_base_fare;
                    $card_trans_detail_data['total_base_fare'] = $fare_data_details_arr['total_base_fare'];
                    $card_trans_detail_data['total_asn_amount'] = $fare_data_details_arr['total_asn_amount'];
                    $card_trans_detail_data['net_fare_amount'] = $fare_data_details_arr['net_fare_amount'];
                    $card_trans_detail_data['fare_before_rounding'] = $fare_data_details_arr['fare_before_rounding'];
                    $card_trans_detail_data['fare_rounding_difference'] = $fare_data_details_arr['fare_before_rounding'] - $fare_data_details_arr['total_amt'];
                    $card_trans_detail_data['reimbursement_amount'] = $fare_data_details_arr['reimbursement_amount'];
                }

                $card_trans_details_id[$i] = $this->Customer_registration_model->insert_customer_reg_data($card_trans_detail_data,CARD_TRANS_DETAILS);

            }
            
            // calculation of total fare and its details
            // Need uncomment 
         //   $total_fare_data['data']['total_fare_amt'] = 0;
           
           $total_fare_amt = $request['total_fare_amt'];
            //amount transaction start here
            $amt_trans_data = array(
                    'card_transaction_id' => $card_trans_id,
                    'trimax_card_id' => $request['trimax_card_id'],
                    'total_amount' => $total_fare_amt,
                  //  'transaction_type_id' => $card_fee_master_id['ctrans_type_id'],
                    'transaction_type_code' => isset($request['trans_type']) ? $request['trans_type'] : '', 
                    'depot_cd' => $request['depot_cd'],
                    'terminal_id' => $request['terminal_id'],
                    'session_id' => $request['session_id'],
                    'vendor_id' => $vendor_id,
                    'created_by' => $request['apploginuser'],
                    'is_active' => $is_active
                );
            $amt_trans_id = $this->Customer_registration_model->insert_customer_reg_data($amt_trans_data,AMOUNT_TRANS);
          
           if(!empty($request['Card_fee_details'])) 
           {
		       $Card_fee_details = json_decode($request['Card_fee_details'],TRUE);
                foreach ($Card_fee_details as $key => $value) {
                    $amt_trans_details_data[] = array(
                            'amount_transaction_id' => $amt_trans_id,
                            'amount_code' => $value['fee_type_cd'],
                            'amount' => $value['fee_type_value'],
                            'operand' => $value['operand'],
                            'created_by' => $request['apploginuser'],
                            'is_active' => $is_active
                        );
                }
                $amt_trans_details_id = $this->Customer_registration_model->insert_customer_reg_bulk_data($amt_trans_details_data,AMOUNT_TRANS_DETAILS);
           }
           
             if($is_active == '1') {
                //$this->Utility_model->save_command_data($request['trimax_card_id'],RENEW_COMMAND);
                $update_req_txn['request_id'] = $this->requestId;
                $whereData['id'] = $card_trans_id;
                $whereData['is_active'] = '1';
                $this->Mainmodel->updateReqTxnData($update_req_txn,$whereData);
            }
           //array('trimax_card_id' => $request['trimax_card_id'], 'receipt_number' => $receipt_number);
            $request['amount'] = $request['total_fare_amt']-$bccharges;
            updateWalletTxn($request);
         
        }


        json_response($json);
    }


     public function update_pass_renewal_status_post() 
     {
        $URL = $this->apiCred['url'] . '/Pass_renewal/update_pass_renewal_status';
        $itms_request = $this->post();
        $itms_request['apploginuser'] = $this->user_id ;
        $itms_request['session_id'] = $this->session_id ;
        $data = curlForPost($URL,$itms_request,$this->basicAuth, $this->header); 
        $json = json_decode($data);
        log_data('rest/smartcard/smart_card_pass_renewal_api_' . date('d-m-Y') . '.log', $json, 'api');
        json_response($json);
        
           if (strtolower($json->status) == 'success') 
           {
                $user_name = $this->input->post('apploginuser');
                $trimax_card_id = $this->input->post('trimax_card_id');


              if($this->input->post('trans_type') == PASS_RENEW_CASH_TRANS_TYPE) {
                    $trans_type_id = PASS_RENEW_CASH_TRANS_TYPE_ID;
                } elseif($this->input->post('trans_type') == PASS_RENEW_SW_TRANS_TYPE) {
                    $trans_type_id = PASS_RENEW_SW_TRANS_TYPE_ID;
                } else {
                    $message = array('status' => 'failure', 'msg' => 'Invalid Transaction Type Id');
                    $this->set_response($message, 200,FALSE);
                }

               $chk_renewal_data = $this->Mainmodel->checkPassRenewalData($trimax_card_id,$trans_type_id,DISPATCH_STATUS,RENEWAL_STATUS);

               $card_trans_id = $chk_renewal_data['card_trans_id'];
               $card_trans_details_id = $chk_renewal_data['card_trans_details_id'];
               $amt_trans_id = $chk_renewal_data['amt_trans_id'];
               $trans_update_data = array('write_flag' => '0', 'modified_by' => $user_name);
               
               $this->Mainmodel->update_card_tarns_write_flag($card_trans_id,$trimax_card_id,$trans_update_data);
               
               $trans_update_data['write_flag'] = '1';
               
               $this->Mainmodel->update_card_trans($card_trans_id,$trans_update_data);
               
               $update_data = array('is_active' => '1', 'updated_by' => $user_name);
              $master_id_arr = array('card_trans_id' => $card_trans_id, 'card_trans_details_id_1' => $card_trans_details_id, 'amt_trans_id' => $amt_trans_id);
              
              
              $this->Mainmodel->updateActiveStatus($master_id_arr,$update_data);
              
              
              $card_update_data = array('is_active' => '1', 'modified_by' => $user_name);
              
              $this->Mainmodel->update_card_trans_detail($card_trans_id,$trimax_card_id,$card_update_data);
              
              $update_req_txn['request_id'] = $this->input->post('request_id');
              $update_req_txn['txn_ref_no'] = $this->input->post('txn_ref_no');
              $whereData['is_active'] = '1';
              $whereData['id'] =$card_trans_id;
              
              $this->Mainmodel->updateReqTxnData($update_req_txn,$whereData);
           }
    }
    
}
