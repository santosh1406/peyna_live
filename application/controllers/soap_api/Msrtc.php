<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// class Msrtc extends CI_Controller {
class Msrtc extends CI_Model {

  var $user_id;
  var $pass;
  var $url;
  var $user_type;
  var $op_id;
  var $dep_date;
  var $ci;

  public function __construct() {
    parent::__construct();

    ini_set('max_execution_time', 3000);
    ini_set("memory_limit",-1);


    $this->load->helper('soap_data');
    $this->load->model('soap_model/msrtc_model', 'soap_client_model');
    
    // $this->config->item("msrtc_wsdl_url")  
    $this->user_id     ='tyaari';
    $this->pass        ='msrtc';
    $this->url         = $this->config->item("msrtc_wsdl_url");
    $this->user_type   = 1;
    $this->op_id       = 3;
          
    $this->dep_date    = $this->dep_date && $this->dep_date !=''? $this->dep_date : date('Y-m-d');
    $this->dep_date    = date('d/m/Y',strtotime($this->dep_date.' +1 days'));
    
    // show($this->dep_date,1);
  }

  public function index()
  {
    // echo 'class loaded';
  }

  public function seatAvailability($all_det){
      $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
         <soapenv:Header/>
         <soapenv:Body>
            <ser:getSeatAvailability>
               <!--Optional:-->
               <SeatAvailabilityRequest>
                  <!--Optional:-->
                  <authentication>
                     <!--Optional:-->
                     <userName>'.$this->user_id.'</userName>
                     <!--Optional:-->
                     <password>'.$this->pass.'</password>
                     <!--Optional:-->
                     <userType>'.$this->user_type.'</userType>
                  </authentication>
                  <serviceId>'.$all_det['service_id'].'</serviceId>
                  <!--Optional:-->
                  <fromStopId>'.$all_det['from_stop'].'</fromStopId>
                  <!--Optional:-->
                  <toStopId>'.$all_det['to_stop'].'</toStopId>
                  <!--Optional:-->
                  <dateOfJourney>'.$all_det['date'].'</dateOfJourney>
               </SeatAvailabilityRequest>
            </ser:getSeatAvailability>
         </soapenv:Body>
      </soapenv:Envelope>';
      // show($xml_str);
      
      return $data = get_soap_data($this->url, 'getSeatAvailability', $xml_str);
  }

  public function temporary_booking($data = array())
  { 
    $pass = $data['passenger'];

    $date = date('d/m/Y',strtotime($data['date']));

    $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <ser:doTemporaryBooking>
                         <!--Optional:-->
                         <TentativeBookingRequest>
                            <!--Optional:-->
                            <authentication>
                               <!--Optional:-->
                               <userName>'.$this->user_id.'</userName>
                               <!--Optional:-->
                               <password>'.$this->pass.'</password>
                               <!--Optional:-->
                               <userType>'.$this->user_type.'</userType>
                            </authentication>
                            <serviceId>'.$data['service_id'].'</serviceId>
                            <!--Optional:-->
                            <fromBusStopId>'.$data['from_stop_id'].'</fromBusStopId>
                            <!--Optional:-->
                            <toBusStopId>'.$data['to_stop_id'].'</toBusStopId>
                            <!--Optional:-->
                            <dateOfJourney>'.$date.'</dateOfJourney>
                            <!--Optional:-->
                            <passengerDetails>
                               <!--Zero or more repetitions:--> 
                               ';

      foreach ($pass as $key => $value) {
                                                    
           $xml_str .= '      <passenger>
                                  <!--Optional:-->
                                  <passengerName>'.$value['name'].'</passengerName>
                                  <age>'.$value['age'].'</age>
                                  <!--Optional:-->
                                  <concessioncode>?</concessioncode>
                                  <!--Optional:-->
                                  <concessionproof>?</concessionproof>
                                  <!--Optional:-->
                                  <sex>'.$value['gender'].'</sex>
                                  <!--Optional:-->
                                  <contactNumber>'.$data['phone'].'</contactNumber>
                                  <!--Optional:-->
                                  <email>'.$data['email'].'</email>
                                  <seatNo>'.$value['seat_no'].'</seatNo>
                                  <totalFare>'.$value['fare'].'</totalFare>
                                  <!--Optional:-->
                                  <seatType>?</seatType>
                                  <!--Optional:-->
                                  <fareDetail>
                                     <!--Optional:-->
                                     <acc>?</acc>
                                     <adultFare>?</adultFare>
                                     <childFare>?</childFare>
                                     <reservationCharge>?</reservationCharge>
                                     <seniorfare>?</seniorfare>
                                  </fareDetail>
                               </passenger>';
                   }

        $xml_str .= '
                            </passengerDetails>
                         </TentativeBookingRequest>
                      </ser:doTemporaryBooking>
                   </soapenv:Body>
                </soapenv:Envelope>';
                
    $response = get_soap_data($this->url, 'doTemporaryBooking', $xml_str);
    
    if(isset($response['TentativeBookingResponse']) )
    {
      return $response['TentativeBookingResponse'];
    }
    else
    {
      return $response['TentativeBookingResponse'];
    }       

  }

  public function doconfirm_booking($data = array())
  { 
    // $date = date('d/m/Y',strtotime($data['date']));
    //show($data,1);
    $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
                            <soapenv:Header/>
                            <soapenv:Body>
                               <ser:doConfirmBooking>
                                  <!--Optional:-->
                                  <ConfirmBookingRequest>
                                     <!--Optional:-->
                                     <authentication>
                                        <!--Optional:-->
                                        <userName>'.$this->user_id.'</userName>
                                        <!--Optional:-->
                                        <password>'.$this->pass.'</password>
                                        <!--Optional:-->
                                        <userType>'.$this->user_type.'</userType>
                                     </authentication>
                                     <!--Optional:-->
                                     <uniqueTransactionId>'.$data['ticket_no'].'</uniqueTransactionId>
                                     <!--Optional:-->
                                     <billDeskResponse>?</billDeskResponse>
                                     <!--Optional:-->
                                     <blockKey>'.$data['ticket_ref_no'].'</blockKey>
                                     <seatCount>'.$data['seat_count'].'</seatCount>
                                  </ConfirmBookingRequest>
                               </ser:doConfirmBooking>
                            </soapenv:Body>
                         </soapenv:Envelope>';
                
                
                
                
                
   // show($xml_str);
    $response = get_soap_data($this->url, 'doTemporaryBooking', $xml_str);

     //show($response);
   if(isset($response['ConfirmBookingResponse']) && $response['ConfirmBookingResponse']['status'] == 'SUCCESS')
            {
              return $response['ConfirmBookingResponse'];
            }
            else
            {
              return $response['ConfirmBookingResponse'];
            }

  }

  function getAvailableServices()
  {
      $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
                       <soapenv:Header/>
                       <soapenv:Body>
                          <ser:getAvailableServices>
                             <!--Optional:-->
                             <AvailableServiceRequest>
                                <!--Optional:-->
                                <authentication>
                                   <!--Optional:-->
                                   <userName>'.$this->user_id.'</userName>
                                   <!--Optional:-->
                                   <password>'.$this->pass.'</password>
                                   <!--Optional:-->
                                   <userType>'.$this->user_type.'</userType>
                                </authentication>
                                <!--Optional:-->
                                <dateOfJourney>'.$this->dep_date.'</dateOfJourney>
                             </AvailableServiceRequest>
                          </ser:getAvailableServices>
                       </soapenv:Body>
                    </soapenv:Envelope>';

      $response = get_soap_data($this->url,  'getAvailableServices', $xml_str);
      
      // show($response,1,'after');
      $this->db->trans_begin();
      
      $data = $this->soap_client_model->insert_available_services($response, $this->op_id,$this);
      
      if ($this->db->trans_status() === FALSE)
      {
          show(error_get_last());
          $this->db->trans_rollback();
      }
      else
      {
          // show($this->db->queries,'', 'sucess database');
          // $this->db->trans_rollback();
          $this->db->trans_commit();
      }
      
      show($data, 1, 'getAvailableServices');
  }

  function GetAllBusTypes()
  {
    $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
                     <soapenv:Header/>
                     <soapenv:Body>
                        <ser:GetAllBusTypes>
                           <!--Optional:-->
                           <AllBusTypeRequest>
                              <!--Optional:-->
                              <authentication>
                                 <!--Optional:-->
                                 <userName>'.$this->user_id.'</userName>
                                 <!--Optional:-->
                                 <password>'.$this->pass.'</password>
                                 <!--Optional:-->
                                 <userType>'.$this->user_type.'</userType>
                              </authentication>
                           </AllBusTypeRequest>
                        </ser:GetAllBusTypes>
                     </soapenv:Body>
                  </soapenv:Envelope>';

    $response   = get_soap_data($this->url,  'GetAllBusTypes', $xml_str);
    
    // show($response,1);
    
    $seat_layout_id = $this->soap_client_model->insert_bus_type($response, $this->op_id);
    
    return $seat_layout_id;
  }

  function getAllBusFormats($bus_format_cd)
    {
        $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
                       <soapenv:Header/>
                       <soapenv:Body>
                          <ser:getAllBusFormats>
                             <!--Optional:-->
                             <AllBusFormatRequest>
                                <!--Optional:-->
                                <authentication>
                                   <!--Optional:-->
                                   <userName>'.$this->user_id.'</userName>
                                   <!--Optional:-->
                                   <password>'.$this->pass.'</password>
                                   <!--Optional:-->
                                   <userType>'.$this->user_type.'</userType>
                                </authentication>
                                <!--Optional:-->
                                <busFormatType>'.$bus_format_cd.'</busFormatType>
                             </AllBusFormatRequest>
                          </ser:getAllBusFormats>
                       </soapenv:Body>
                    </soapenv:Envelope>';

        $response = get_soap_data($this->url,  'getAllBusFormats', $xml_str);
        
        //show($response,1,'busFormat');       
        if(count($response)>0)
        {
            $seat_layout_id = $this->soap_client_model->insert_bus_format($response, $this->op_id,$bus_format_cd);
        }
        else
        {
            show($response,'','No Response');
            return '';
        }
        return $seat_layout_id;        
        // show($data,1, 'getAllBusFormats');
    }

    

    function getBoardingStops($data =array(), $ali_type)
    {

        $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
                         <soapenv:Header/>
                         <soapenv:Body>
                            <ser:getBoardingStops>
                               <!--Optional:-->
                               <GetBoardingStopsRequest>
                                  <!--Optional:-->
                                  <authentication>
                                     <!--Optional:-->
                                     <userName>'.$this->user_id.'</userName>
                                     <!--Optional:-->
                                     <password>'.$this->pass.'</password>
                                     <!--Optional:-->
                                     <userType>'.$this->user_type.'</userType>
                                  </authentication>
                                  <serviceId>'.$data['serviceId'].'</serviceId>
                                  <!--Optional:-->
                                  <fromStopId>'.$data['from'].'</fromStopId>
                                  <!--Optional:-->
                                  <toStopId>'.$data['to'].'</toStopId>
                                  <!--Optional:-->
                                  <typeAliBrd>'.$ali_type.'</typeAliBrd>
                                  <!--Optional:-->
                                  <busTypeCode>'.$data['bus_type_code'].'</busTypeCode>
                               </GetBoardingStopsRequest>
                            </ser:getBoardingStops>
                         </soapenv:Body>
                      </soapenv:Envelope>';

        $response = get_soap_data($this->url,  'getBoardingStops', $xml_str);

          $stops = array();
          if( isset($response['GetBoardingStopsResponse'])  
            && count($response['GetBoardingStopsResponse']) >0 
            && isset($response['GetBoardingStopsResponse']['boardingStopDetails']) 
            && isset($response['GetBoardingStopsResponse']['alightingStopDetails'])
            )
          {

            if($ali_type == 'BRD')
            {
              $stop_code =  $data['from'];
              if( isset($response['GetBoardingStopsResponse']['boardingStopDetails']['boardingStopDetail']) && count($response['GetBoardingStopsResponse']['boardingStopDetails']) >0  &&  count($response['GetBoardingStopsResponse']['boardingStopDetails']['boardingStopDetail']) >0)
              {

                $stops = $response['GetBoardingStopsResponse']['boardingStopDetails']['boardingStopDetail'];

                if(isset($stops['busStopName']))
                {
                    $stops = array($stops);
                }
              }

              $this->getBoardingStops($data, 'ALI');
              
            }
            else if($ali_type == 'ALI')
            {
              $stop_code =  $data['to'];
              if(isset($response['GetBoardingStopsResponse']['alightingStopDetails']['alightingStopDetail']) && count($response['GetBoardingStopsResponse']['alightingStopDetails']) >0  &&  count($response['GetBoardingStopsResponse']['alightingStopDetails']['alightingStopDetail']) >0)
              {
                $stops = $response['GetBoardingStopsResponse']['alightingStopDetails']['alightingStopDetail'];

                if(isset($stops['busStopName']))
                {
                    $stops = array($stops);
                }
              }
            }

            if(count($stops)>0)
            {
              $this->upsrtc_model->insert_boarding_stops($stops, $ali_type, $stop_code, $this->op_id, $data['trip_no']);
            }
          }
          else
          {
            show($response,'','getBoardingStops----error');
          }

        return $response;
    }

    public function cancel_booking($data)
    {
          $xml_str  =  '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
           <soapenv:Header/>
           <soapenv:Body>
              <ser:cancelTicket>
                 <!--Optional:-->
                 <CancelRequest>
                    <!--Optional:-->
                    <authentication>
                       <!--Optional:-->
                       <userName>'.$this->user_id.'</userName>
                       <!--Optional:-->
                       <password>'.$this->pass.'</password>
                       <!--Optional:-->
                       <userType>'.$this->user_type.'</userType>
                    </authentication>
                    <!--Optional:-->
                    <pnr>'.$data['pnr_no'].'</pnr>
                    <!--Optional:-->
                    <seatNumbers>'.$data['var_seat_no'].'</seatNumbers>
                 </CancelRequest>
              </ser:cancelTicket>
           </soapenv:Body>
        </soapenv:Envelope>';
        $response = get_soap_data($this->url, 'cancelTicket', $xml_str);


        if(isset($response['CancelResponse']) && $response['CancelResponse']['status'] == 'SUCCESS')
        {
          return $response['CancelResponse'];
        }
        else
        {        
          return $response['CancelResponse'];
        }

    }

    function getBusServiceStops($service_id, $trip_no = '', $op_bus_type = '')
    {
       $xml_str  =  '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.msrtc.trimax.com/">
                       <soapenv:Header/>
                       <soapenv:Body>
                          <ser:getBusServiceStops>
                             <!--Optional:-->
                             <BusServiceStopsRequest>
                                <!--Optional:-->
                                <authentication>
                                   <!--Optional:-->
                                   <userName>'.$this->user_id.'</userName>
                                   <!--Optional:-->
                                   <password>'.$this->pass.'</password>
                                   <!--Optional:-->
                                   <userType>'.$this->user_type.'</userType>
                                </authentication>
                                <serviceId>'.$service_id.'</serviceId>
                                <!--Optional:-->
                                <dateOfJourney>'.$this->dep_date.'</dateOfJourney>
                             </BusServiceStopsRequest>
                          </ser:getBusServiceStops>
                       </soapenv:Body>
                    </soapenv:Envelope>';
        
        $response   = get_soap_data($this->url,  'getBusServiceStops', $xml_str);
        
        // show($response,1);

        $bstop                     = array();
        $bstop['service_id']       = $service_id;
        $bstop['trip_no']          = $trip_no;
        $bstop['op_bus_type']      = $op_bus_type;
        

        $this->db->trans_begin();
        
        $data       = $this->upsrtc_model->insert_bus_service_stops($response,$this->op_id, $this->dep_date, $this,$bstop);
        
        if ($this->db->trans_status() === FALSE)
        {
            show($this->db->queries,'', 'sucess database');
            show(error_get_last());
            $this->db->trans_rollback();
        }
        else
        {
            
            // $this->db->trans_rollback();
            $this->db->trans_commit();
        }

        // $data       = $this->soap_client_model->insert_bus_service_stops($response,$this->op_id, $this->dep_date);
        
                
        // show($data,1, 'getBusServiceStops');
    }
}