<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Upsrtc extends CI_Model {
// class Upsrtc extends CI_Controller {

  var $user_id;
  var $pass;
  var $url;
  var $user_type;
  var $op_id;
  var $dep_date;
  var $ci;

  public function __construct() {
    parent::__construct();

    ini_set('max_execution_time', 3000);
    ini_set("memory_limit",-1);


    $this->load->helper('soap_data');
    $this->load->model(array('soap_model/upsrtc_model'));
    
    // $this->config->item("upsrtc_wsdl_url")	
    $this->user_id 			='BOS';
    $this->pass    			='bos@123';
    $this->url     			= $this->config->item("upsrtc_wsdl_url");
    $this->user_type		= 1;
    $this->op_id       	= 2;
	        
    $this->dep_date   	 	= $this->dep_date && $this->dep_date !=''? $this->dep_date : date('Y-m-d');
    $this->dep_date      = date('d/m/Y',strtotime($this->dep_date.' +1 days'));
    
    // show($this->dep_date,1);
    log_data('cron/bus/bus'.date('d-m-y').'.log','Cron start','Cron start');
  }

  function __destruct()
  {
     log_data('cron/bus/bus'.date('d-m-y').'.log','Cron end','Cron end');
  }

  public function index()
  {
    // echo 'class loaded';
  }

  public function seatAvailability($all_det){
      $xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
         <soapenv:Header/>
         <soapenv:Body>
            <ser:getSeatAvailability>
               <!--Optional:-->
               <SeatAvailabilityRequest>
                  <!--Optional:-->
                  <authentication>
                     <!--Optional:-->
                     <userName>'.$this->user_id.'</userName>
                     <!--Optional:-->
                     <password>'.$this->pass.'</password>
                     <!--Optional:-->
                     <userType>'.$this->user_type.'</userType>
                  </authentication>
                  <serviceId>'.$all_det['service_id'].'</serviceId>
                  <!--Optional:-->
                  <fromStopId>'.$all_det['from_stop'].'</fromStopId>
                  <!--Optional:-->
                  <toStopId>'.$all_det['to_stop'].'</toStopId>
                  <!--Optional:-->
                  <dateOfJourney>'.$all_det['date'].'</dateOfJourney>
               </SeatAvailabilityRequest>
            </ser:getSeatAvailability>
         </soapenv:Body>
      </soapenv:Envelope>';

  return $data = get_soap_data($this->url, 'getSeatAvailability', $xml_str);
  }

	public function temporary_booking($data = array())
	{	
		$pass = $data['passenger'];
               

		$date = date('d/m/Y',strtotime($data['date']));

		$xml_str = ' <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
                            <soapenv:Header/>
                            <soapenv:Body>
                               <ser:doTemporaryBooking>
                                  <!--Optional:-->
                                  <TentativeBookingRequest>
                                     <!--Optional:-->
                                     <authentication>
                                        <!--Optional:-->
                                        <userName>'.$this->user_id.'</userName>
                                        <!--Optional:-->
                                        <password>'.$this->pass.'</password>
                                        <!--Optional:-->
                                        <userType>'.$this->user_type.'</userType>
                                     </authentication>
                                     <serviceId>'.$data['service_id'].'</serviceId>
                                     <!--Optional:-->
                                     <fromBusStopId>'.$data['from_stop_id'].'</fromBusStopId>
                                     <!--Optional:-->
                                     <toBusStopId>'.$data['to_stop_id'].'</toBusStopId>
                                     <!--Optional:-->
                                     <dateOfJourney>'.$date.'</dateOfJourney>
                                     <!--Optional:-->
                                                          <passengerDetails>
                                                          <!--Zero or more repetitions:-->
                                 ';

        foreach ($pass as $key => $value) {
                                    	
                                    
        $xml_str .=                 '  <passenger>
                                        <!--Optional:-->
                                        <passengerName>'.$value['name'].'</passengerName>
                                        <age>'.$value['age'].'</age>
                                        <!--Optional:-->
                                        <sex>F</sex>
                                        <!--Optional:-->
                                        <contactNumber>'.$data["mobile_no"].'</contactNumber>
                                        <!--Optional:-->
                                        <email>'.$data["email"].'</email>
                                        <seatNo>'.$value['seat_no'].'</seatNo>
                                        <totalFare>'.$value['fare'].'</totalFare>
                                        <!--Optional:-->
                                        <seatType>?</seatType>
                                        <!--Optional:-->
                                        <fareDetail>
                                           <acc>?</acc>
                                           <adultFare>?</adultFare>
                                           <childFare>?</childFare>
                                           <!--Optional:-->
                                           <concode>?</concode>
                                           <hr>?</hr>
                                           <it>?</it>
                                           <octroi>?</octroi>
                                           <other>?</other>
                                           <reservationCharge>?</reservationCharge>
                                           <sleeperCharge>?</sleeperCharge>
                                           <state1>?</state1>
                                           <state2>?</state2>
                                           <state3>?</state3>
                                           <state4>?</state4>
                                           <state5>?</state5>
                                           <toll>?</toll>
                                        </fareDetail>
                                     </passenger>';
        }

        $xml_str .=            '</passengerDetails>
                                </TentativeBookingRequest>
                             </ser:doTemporaryBooking>
                          </soapenv:Body>
                       </soapenv:Envelope>
                       ';

        
        $response = get_soap_data($this->url, 'doTemporaryBooking', $xml_str);

        if(isset($response['TentativeBookingResponse']) )
        {
        	return $response['TentativeBookingResponse'];
        }
        else
        {
        	return $response['TentativeBookingResponse'];
        }
        

	}

	public function doconfirm_booking($data = array())
	{	
		// $date = date('d/m/Y',strtotime($data['date']));
		
		$xml_str = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
                            <soapenv:Header/>
                            <soapenv:Body>
                               <ser:doConfirmBooking>
                                  <!--Optional:-->
                                  <ConfirmBookingRequest>
                                     <!--Optional:-->
                                     <authentication>
                                        <!--Optional:-->
                                        <userName>'.$this->user_id.'</userName>
                                        <!--Optional:-->
                                        <password>'.$this->pass.'</password>
                                        <!--Optional:-->
                                        <userType>'.$this->user_type.'</userType>
                                     </authentication>
                                     <!--Optional:-->
                                     <uniqueTransactionId>'.$data['ticket_no'].'</uniqueTransactionId>
                                     <!--Optional:-->
                                     <billDeskResponse>?</billDeskResponse>
                                     <!--Optional:-->
                                     <blockKey>'.$data['ticket_ref_no'].'</blockKey>
                                     <seatCount>'.$data['seat_count'].'</seatCount>
                                  </ConfirmBookingRequest>
                               </ser:doConfirmBooking>
                            </soapenv:Body>
                         </soapenv:Envelope>';
                
                
                
                
                
   
    $response = get_soap_data($this->url, 'doTemporaryBooking', $xml_str);

     
   if(isset($response['ConfirmBookingResponse']) && $response['ConfirmBookingResponse']['status'] == 'SUCCESS')
    {
      return $response['ConfirmBookingResponse'];
    }
    else
    {
      return $response['ConfirmBookingResponse'];
    }

	}

  function getAvailableServices()
  {
      $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
                     <soapenv:Header/>
                     <soapenv:Body>
                        <ser:getAvailableServices>
                           <!--Optional:-->
                           <AvailableServiceRequest>
                              <!--Optional:-->
                              <authentication>
                                 <!--Optional:-->
                                 <userName>'.$this->user_id.'</userName>
                                 <!--Optional:-->
                                 <password>'.$this->pass.'</password>
                                 <!--Optional:-->
                                 <userType>'.$this->user_type.'</userType>
                              </authentication>
                              <!--Optional:-->
                              <dateOfJourney>'.$this->dep_date.'</dateOfJourney>
                           </AvailableServiceRequest>
                        </ser:getAvailableServices>
                     </soapenv:Body>
                  </soapenv:Envelope>';

      $response = get_soap_data($this->url,  'getAvailableServices', $xml_str);
      
      $this->db->trans_begin();
      
      $data = $this->upsrtc_model->insert_available_services($response, $this->op_id,$this);
      
      if ($this->db->trans_status() === FALSE)
      {
          show(error_get_last());
          $this->db->trans_rollback();
      }
      else
      {
          // show($this->db->queries,'', 'sucess database');
          // $this->db->trans_rollback();
          $this->db->trans_commit();
      }
      
      show($data,'', ' '.$this->dep_date. ' getAvailableServices');
  }

  function GetAllBusTypes()
  {
    $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
                     <soapenv:Header/>
                     <soapenv:Body>
                        <ser:GetAllBusTypes>
                           <!--Optional:-->
                           <AllBusTypeRequest>
                              <!--Optional:-->
                              <authentication>
                                 <!--Optional:-->
                                 <userName>'.$this->user_id.'</userName>
                                 <!--Optional:-->
                                 <password>'.$this->pass.'</password>
                                 <!--Optional:-->
                                 <userType>'.$this->user_type.'</userType>
                              </authentication>
                           </AllBusTypeRequest>
                        </ser:GetAllBusTypes>
                     </soapenv:Body>
                  </soapenv:Envelope>';

    $response   = get_soap_data($this->url,  'GetAllBusTypes', $xml_str);
    
    // show($response,1);
    
    $seat_layout_id = $this->upsrtc_model->insert_bus_type($response, $this->op_id);
    
    return $seat_layout_id;
    // show($response, '', 'response');
    // show($data,1, 'GetAllBusTypes');
  }

  function getAllBusFormats($bus_format_cd)
    {
        $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
                       <soapenv:Header/>
                       <soapenv:Body>
                          <ser:getAllBusFormats>
                             <!--Optional:-->
                             <AllBusFormatRequest>
                                <!--Optional:-->
                                <authentication>
                                   <!--Optional:-->
                                   <userName>'.$this->user_id.'</userName>
                                   <!--Optional:-->
                                   <password>'.$this->pass.'</password>
                                   <!--Optional:-->
                                   <userType>'.$this->user_type.'</userType>
                                </authentication>
                                <!--Optional:-->
                                <busFormatType>'.$bus_format_cd.'</busFormatType>
                             </AllBusFormatRequest>
                          </ser:getAllBusFormats>
                       </soapenv:Body>
                    </soapenv:Envelope>';
    
        $response = get_soap_data($this->url,  'getAllBusFormats', $xml_str, $bus_format_cd);
        
        
        if(count($response)>0)
        {
            $seat_layout_id = $this->upsrtc_model->insert_bus_format($response, $this->op_id,$bus_format_cd);
        }
        else
        {
            show($response,'','No Response');
            return '';
        }
        return $seat_layout_id;        
        
    }

    

    function getBoardingStops($data =array(), $ali_type)
    {

        $xml_str  = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.rsrtc.trimax.com/">
                        <soapenv:Header/>
                           <soapenv:Body>
                              <ser1:getBoardingStops xmlns:ser1="http://service.upsrtc.trimax.com/">
                                 <!--Optional:-->
                                 <GetBoardingStopsRequest>
                                    <!--Optional:-->
                                    <authentication>
                                       <!--Optional:-->
                                       <userName>'.$this->user_id.'</userName>
                                       <!--Optional:-->
                                       <password>'.$this->pass.'</password>
                                       <!--Optional:-->
                                       <userType>'.$this->user_type.'</userType>
                                    </authentication>
                                    <serviceId>'.$data['service_id'].'</serviceId>
                                    <!--Optional:-->
                                    <fromStopId>'.$data['from'].'</fromStopId>
                                    <!--Optional:-->
                                    <toStopId>'.$data['to'].'</toStopId>
                                    <!--Optional:-->
                                    <typeAliBrd>'.$ali_type.'</typeAliBrd>
                                    <!--Optional:-->
                                    <busTypeCode>'.$data['bus_type_code'].'</busTypeCode>
                                 </GetBoardingStopsRequest>
                              </ser1:getBoardingStops>
                           </soapenv:Body>
                        </soapenv:Envelope>';
   
        $response = get_soap_data($this->url,  'getBoardingStops', $xml_str);

          $stops = array();
          if( isset($response['GetBoardingStopsResponse'])  
            && count($response['GetBoardingStopsResponse']) >0 
            && ( isset($response['GetBoardingStopsResponse']['boardingStopDetails']) 
            || isset($response['GetBoardingStopsResponse']['alightingStopDetails']))
            )
          {

            if($ali_type == 'BRD')
            {
              $stop_code =  $data['from'];
              if( isset($response['GetBoardingStopsResponse']['boardingStopDetails']['boardingStopDetail']) && count($response['GetBoardingStopsResponse']['boardingStopDetails']) >0  &&  count($response['GetBoardingStopsResponse']['boardingStopDetails']['boardingStopDetail']) >0)
              {

                $stops = $response['GetBoardingStopsResponse']['boardingStopDetails']['boardingStopDetail'];

                if(isset($stops['busStopName']))
                {
                    $stops = array($stops);
                }
              }

              $this->getBoardingStops($data, 'ALI');
              
            }
            else if($ali_type == 'ALI')
            {
              $stop_code =  $data['to'];
              if(isset($response['GetBoardingStopsResponse']['alightingStopDetails']['alightingStopDetail']) && count($response['GetBoardingStopsResponse']['alightingStopDetails']) >0  &&  count($response['GetBoardingStopsResponse']['alightingStopDetails']['alightingStopDetail']) >0)
              {
                $stops = $response['GetBoardingStopsResponse']['alightingStopDetails']['alightingStopDetail'];

                if(isset($stops['busStopName']))
                {
                    $stops = array($stops);
                }
              }
            }

            if(count($stops)>0)
            {
              $this->upsrtc_model->insert_boarding_stops($stops, $ali_type, $stop_code, $this->op_id, $data['trip_no']);
            }
          }
          else
          {
            show($response,'','getBoardingStops----error');
          }

        return $response;
    }

    function getBusServiceStops($service_id, $trip_no = '', $op_bus_type = '')
    {

       $xml_str  =  '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
   						<soapenv:Header/>
                       	<soapenv:Body>
                          <ser:getBusServiceStops>
                             <!--Optional:-->
                             <BusServiceStopsRequest>
                                <!--Optional:-->
                                <authentication>
                                   <!--Optional:-->
                                   <userName>'.$this->user_id.'</userName>
                                   <!--Optional:-->
                                   <password>'.$this->pass.'</password>
                                   <!--Optional:-->
                                   <userType>'.$this->user_type.'</userType>
                                </authentication>
                                <serviceId>'.$service_id.'</serviceId>
                                <!--Optional:-->
                                <dateOfJourney>'.$this->dep_date.'</dateOfJourney>
                             </BusServiceStopsRequest>
                          </ser:getBusServiceStops>
                       </soapenv:Body>
                    </soapenv:Envelope>';
        
        $response   = get_soap_data($this->url,  'getBusServiceStops', $xml_str);
        
        $bstop                     = array();
        $bstop['service_id']       = $service_id;
        $bstop['trip_no']          = $trip_no;
        $bstop['op_bus_type']      = $op_bus_type;

        $this->db->trans_begin();
        
        if($response && is_array($response) && count($response) >0 )
        {
          $data       = $this->upsrtc_model->insert_bus_service_stops($response,$this->op_id, $this->dep_date, $this,$bstop);
          
          if ($this->db->trans_status() === FALSE)
          {
              show($this->db->queries,'', 'sucess database');
              show(error_get_last());
              $this->db->trans_rollback();
          }
          else
          {
              
              // $this->db->trans_rollback();
              $this->db->trans_commit();
          }
        }
        else
        {
          show($xml_str,'','getBusServiceStops');
          show($response,'','getBusServiceStops');

        }
        // $data       = $this->upsrtc_model->insert_bus_service_stops($response,$this->op_id, $this->dep_date);
                       
        // show($data,1, 'getBusServiceStops');
    }
    
    public function cancel_ticket($data){
      $xml_str='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <ser:cancelTicket>
                         <!--Optional:-->
                         <CancelRequest>
                            <!--Optional:-->
                            <authentication>
                               <!--Optional:-->
                               <userName>'.$this->user_id.'</userName>
                               <!--Optional:-->
                               <password>'.$this->pass.'</password>
                               <!--Optional:-->
                               <userType>'.$this->user_type.'</userType>
                            </authentication>
                            <!--Optional:-->
                            <pnr>'.$data['pnr_no'].'</pnr>
                            <!--Optional:-->
                            <seatNumbers>'.$data['seat_no'].'</seatNumbers>
                         </CancelRequest>
                      </ser:cancelTicket>
                   </soapenv:Body>
                </soapenv:Envelope>';
    $response = get_soap_data($this->url, 'cancelTicket', $xml_str);

    if(isset($response['CancelResponse']) && $response['CancelResponse']['status'] == 'SUCCESS')
    {
      return $response['CancelResponse'];
    }
    else
    {            
      return $response['CancelResponse'];
    }
  }

  public function cancel_booking($data)
  {
          $xml_str='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.upsrtc.trimax.com/">
         <soapenv:Header/>
         <soapenv:Body>
            <ser:cancelTicket>
               <!--Optional:-->
               <CancelRequest>
                  <!--Optional:-->
                  <authentication>
                     <!--Optional:-->
                     <userName>'.$this->user_id.'</userName>
                     <!--Optional:-->
                     <password>'.$this->pass.'</password>
                     <!--Optional:-->
                     <userType>'.$this->user_type.'</userType>
                  </authentication>
                  <!--Optional:-->
                  <pnr>'.$data['pnr_no'].'</pnr>
                  <!--Optional:-->
                  <seatNumbers>'.$data['var_seat_no'].'</seatNumbers>
               </CancelRequest>
            </ser:cancelTicket>
         </soapenv:Body>
      </soapenv:Envelope>';
      $response = get_soap_data($this->url, 'cancelTicket', $xml_str);


      if(isset($response['CancelResponse']) && $response['CancelResponse']['status'] == 'SUCCESS')
      {
        return $response['CancelResponse'];
      }
      else
      {        
        return $response['CancelResponse'];
      }

  }
  

  // function for api vendor

  function apiTempBooking($input) {
    if((isset($input['service_id']) && $input['service_id']!='') 
        // && (isset($input['from_stop_id']) && $input['from_stop_id']!='')
        // && (isset($input['to_stop_id']) && $input['to_stop_id']!='') 
        && (isset($input['from_stop']) && $input['from_stop']!='') 
        && (isset($input['to_stop']) && $input['to_stop']!='')
        && (isset($input['date']) && $input['date']!='') 
        && (isset($input['operator']) && $input['operator']!='') 
        && (isset($input['mobile_no']) && $input['mobile_no']!='') 
        && (isset($input['email']) && $input['email']!='') 
    )
    {
      if(!(preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/",$input['date'])) > 0 || strtotime($input['date']) < time())
      {
        return array("status" => "FAILURE", "error" => "Invalid Date, Date format is  dd-mm-yy and should be greater than equal to todays date");
      }

      $this->load->model(array("bus_stop_master_model"));
      $from = $this->bus_stop_master_model->get_detail_by_name($input['from_stop'], $input['operator']);
      $to   = $this->bus_stop_master_model->get_detail_by_name($input['to_stop'], $input['operator']);

      if(!$from || !$to){
        return array("status" => "FAILURE", "error" => "bus stop not found");
      }

      $tb_param = array(
                          "service_id"    => $input['service_id'],
                          "from_stop_id"  => $from->op_stop_cd,
                          "to_stop_id"    => $to->op_stop_cd,
                          "date"          => $input['date'],
                          "email"         => $input['email'],
                          "mobile_no"     => $input['mobile_no'],
                          "passenger"     => $input['passenger']
                      );
      $data = $this->temporary_booking($tb_param);

      if ($data['status'] == "SUCCESS") 
      {
        $date  = $input['date'];

        $this->db->select("om.op_name,t.trip_no,tm.op_id, bsm.bus_stop_name,tm.op_trip_no,tm.seat_layout_id,"
                          . "t.boarding_stop_name as from_stop, t.destination_stop_name as to_stop,"
                          . "concat(t.sch_departure_date, ' ', t.sch_departure_tm) as dept_tm, DATE_FORMAT(t.sch_departure_date, '%d-%m-%Y') as  sch_departure_date,"
                          . " t.arrival_date, t.arrival_tm, t.day,bt.bus_type_id,bt.bus_type_name,"
                          . "t.sch_departure_tm,t.arrival_tm,bs.op_bus_type_name,if(ISNULL(t.tot_seat_available),osl.ttl_seats,t.tot_seat_available) as tot_seat_available, t.seat_fare,"
                          ."if(t.day>0,t.day -1, t.day) as arrival_day," 
                          ."t.arrival_date as arrival_time", false);

        $this->db->from("travel_destinations t");
        $this->db->join("travel_destinations t2", "t.boarding_stop_id = t2.boarding_stop_id", "inner");

        $this->db->join("trip_master tm", "tm.trip_no = t.trip_no", "inner");
        $this->db->join("bus_stop_master bsm", "t.destination_stop_id = bsm.bus_stop_id", "left");
        $this->db->join("op_master om", "om.op_id = tm.op_id", "inner");
        $this->db->join("op_bus_types bs", "bs.op_bus_type_id = tm.op_bus_type_id", "inner");
        $this->db->join("op_seat_layout osl", "osl.seat_layout_id = tm.seat_layout_id", "inner");
        $this->db->join("bus_type bt", "bt.bus_type_id = bs.bus_type_id", "left");
        $this->db->join("op_stop_master osm", "bsm.bus_stop_id = osm.bus_stop_id");

        $this->db->where("DATE_FORMAT(t.sch_departure_date,'%Y-%m-%d')", date("Y-m-d",strtotime($input['date'])));

         $this->db->where("t.boarding_stop_name", $input['from_stop']);
        $this->db->where("t.destination_stop_name", $input['to_stop'] );

        // $this->db->where("t.boarding_stop_name", "Charbagh");
        //$this->db->where("t.destination_stop_name",  "Allahabad" );

        $this->db->where('tm.op_trip_no', $input['service_id']); 
        $this->db->group_by("t.trip_no");
        $query = $this->db->get();

        if($query->num_rows()>0)
        {
            $all_records  = $query->result_array();

            $ticket_no = $this->generateid_model->getRequiredId('processed_tickets');

            $far_detail = $data['passengerDetails']['passenger']['fareDetail'];

            $process_data_array = array(
                  'ticket_no'           => $ticket_no,
                  'ticket_type'         => '1',
                  'request_type'        => 'B',
                  'ticket_status'       => 'A',
                  'ticket_ref_no'       => $data['blockKey'],
                  'bus_service_no'      => $input['service_id'],
                  'dept_time'           => $all_records[0]['dept_tm'],
                  'boarding_time'       => $all_records[0]['sch_departure_date'],
                  'alighting_time'      => $all_records[0]['arrival_date'],
                  'from_stop_cd'        => $input['from_stop'],
                  'till_stop_cd'        => $input['to_stop'],
                  'from_stop_name'      => $all_records[0]['from_stop'],
                  'till_stop_name'      => $all_records[0]['to_stop'],
                  'boarding_stop_cd'    => $input['from_stop'],
                  'destination_stop_cd' => $input['to_stop'],
                  'num_passgr'          => count($input['passenger']),
                  
                  'tot_fare_amt'          => ($data['passengerDetails']['passenger']['totalFare'] * count($input['passenger']) ),

                  'tot_fare_amt_with_tax'   => ($data['passengerDetails']['passenger']['totalFare'] * count($input['passenger']) ),
                  'adult_basic_fare'        => $far_detail['adultFare'],
                  'child_basic_fare'        => $far_detail['childFare'],
                  'fare_acc'                => $far_detail['acc'],
                  'fare_hr'                 => $far_detail['hr'],
                  'fare_it'                 => $far_detail['it'],
                  'fare_octroi'             => $far_detail['octroi'],
                  'fare_other'              => $far_detail['other'],
                  'fare_reservationCharge'  => $far_detail['reservationCharge'],
                  'fare_sleeperCharge'      => $far_detail['sleeperCharge'],
                  'fare_toll'               => $far_detail['toll'],

                  'cancellation_rate'   => '0',
                  'issue_time'          => date('Y-m-d h:m:s:'),
                  'pay_id'              => '1',
                  'session_no'          => '1',
                  'op_name'             => $input['operator'],
                  'user_email_id'       => $input['email'],
                  'mobile_no'           => $input['mobile_no'],
                  'inserted_date'       => date('Y-m-d h:m:s'),
                  'inserted_by'         => '1200', 'status' => 'Y'
                );

            $this->db->insert('processed_tickets', $process_data_array);
            
            $pro_result = $this->db->insert_id();
            foreach ($input['passenger'] as $key => $value) {
                $i = 1;
                $processed_details_array = array(
                    'ticket_no' => $ticket_no,
                    'psgr_no' => $i,
                    'psgr_name' => $value['name'],
                    'psgr_age' => $value['age'],
                    'psgr_sex' => $value['sex'],
                    'psgr_type' => 'A',
                    'concession_cd' => '1',
                    'concession_rate' => '1',
                    'discount' => '1',
                    'is_home_state_only' => '1',
                    'fare_amt' => $all_records[0]['seat_fare'],
                    'seat_no' => $value['seat_no'],
                    'seat_status' => 'Y',
                    'concession_proff' => '1',
                    'proff_detail' => '1',
                    'status' => 'Y');
                $this->db->insert('processed_ticket_details', $processed_details_array);
                $pro_details_result = $this->db->insert_id();
                $i++;
            }

            // $this->insert_processed_tkt_details($input,$ticket_no,$all_records, $data);

            return $data;                
          
        } // end of travel detesination query
        else
        {
          return 'No services found';
        }
      }
      else
      {

        foreach ($input['passenger'] as $key => $value) {
            $list[] = array($value['name'], $value['age'], $value['sex'],$value['seat_no']);
        }
        $to_date = date('d-m-Y');
        $file = fopen("./uploads/processed_fail_transaction_" . $to_date . ".csv", "a");

        foreach ($list as $line) {
            fputcsv($file, $line);
        }

        fclose($file);

        return $data;
      }
    }
    else
    {
       return "Given data is improper";
    }


  }
            
  // 
  public function apiDoconfirmBooking($data = array())
  {
    // to fetch the processed tickets
    $pt_data    = $this->processed_tickets_model->as_array()->find_by(array('ticket_ref_no' => $data['block_key']));

    // to fetch the processed tickets detail
    $ptd_arr    = array('ticket_no' => $pt_data['ticket_no']);
    $ptd_data   = $this->processed_ticket_details_model
                          ->where($ptd_arr)
                          ->as_array()
                          ->find_all();

    if($pt_data && $ptd_arr)
    {
      $api_request = array(
                            "ticket_no" => $data['unique_id'],
                            "ticket_ref_no" => $data['block_key'],
                            "seat_count" => $data['seat_count'],
                          );

      $response = $this->doconfirm_booking($api_request);
      // show($response,'','response');
    
      if(isset($response) && $response['status'] == 'SUCCESS')
      {
        $insert_arr = array(
                              'block_key' => $data['block_key'],
                              'unique_id' => $data['unique_id'],
                            );

          $this->db->insert('vendor_ticket_detail', $insert_arr);
          
          $temp_pt_data = $pt_data;
          unset(
                  $temp_pt_data['ticket_id'],
                  $temp_pt_data['ticket_no'],
                  $temp_pt_data['inserted_date'],
                  $temp_pt_data['transaction_status']
              );

          /****** To Update BOS REFERENCE NUMBER and SMS Count Start***********/
          
          $ticket_no            = $this->generateid_model->getRequiredId('tickets');

          $temp_pt_data['ticket_no']    = $ticket_no;
          $temp_pt_data['pnr_no']       = $response['pnr'];
          $ticket_insert                = $temp_pt_data;
          
          $ticket_id = $this->tickets_model->insert($temp_pt_data);

          $bos_key_detail = getBossRefNo($ticket_id);

          $bos_key_pt_data['bos_ticket_digit']     = $bos_key_detail['bos_ticket_digit'];
          $bos_key_pt_data['bos_ticket_string']    = $bos_key_detail['bos_ticket_string'];
          $bos_key_pt_data['boss_ref_no']          = $bos_key_detail['bos_ref_no'];

          $response['bos_key']                     = $bos_key_detail['bos_ref_no'];
          $ticket_id = $this->tickets_model->update($ticket_id, $bos_key_pt_data);
          


          if($ticket_id > 0)
          {
              foreach ($ptd_data as $key => $value) {
                $temp_ptd_data = $value;
                unset(
                        $temp_ptd_data['ticket_details_id'],
                        $temp_ptd_data['ticket_no']
                      );

                $temp_ptd_data['ticket_no'] = $ticket_no;

                $ticket_detail_id = $this->ticket_details_model->insert($temp_ptd_data);                

              }
          }

          return $response;

          // show($ticket_id,1);

      }
      else
      {
        return $response;
      }
    }
    else
    {
      return array( "status" => "FAILURE", "error" => "not tickets found");
    }
  }

  // end function for api vendor

}