<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'core/Soap_controller.php');

class Upsrtc extends Soap_controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('soap_data_helper');


        $input_array = array('username' => 'xsd:string');
        
        // register_soap_function('hello', $input_array );


        $this->nusoap_server->register('hello',
                array('username' => 'xsd:string'),  //parameter
                array('return' => 'tsn:helloArray'),  //output
                'urn:server',   //namespace
                'urn:'.$this->ns.'hello',  //soapaction
                'jax-ws', // style jax-ws
                'encoded', // use
                'Just say hello');  //description       
    }

    

    public function index() {
    
        function hello($username){

            $retArr = array('eventId'=>1,'eventName'=>"test");


            return $retArr;  
        }
        $this->nusoap_server->wsdl->addComplexType(
            'helloArray',
            'complexType',
            'Array',
            'all',
            '',
            array(
                'eventId' => array('name'=>'eventId','type'=>'xsd:string'),
                'eventName' => array('name'=>'eventName','type'=>'xsd:string')
            )
        );

        $this->nusoap_server->service(file_get_contents("php://input"));// read raw data from request body        
    }

    public function test()
    {
        ini_set('memory_limit', -1);
        
        $xml_str =  '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:DATASERVER">
                       <soapenv:Header/>
                       <soapenv:Body>
                          <urn:ETIM_MR_002>
                             <param_un_1>user</param_un_1>
                             <param_pw_2>trimax</param_pw_2>
                             <param_mn_3>?</param_mn_3>
                          </urn:ETIM_MR_002>
                       </soapenv:Body>
                    </soapenv:Envelope>';

            // show($xml_str,1);

            $data = get_soap_data('http://192.168.1.98:447/etim_server_handheld/example1.php?wsdl', 'ETIM_MR_002', $xml_str);

            $fields['data'] = file_get_contents(APPPATH.'../route_details.csv');

            $url = "http://10.20.41.202/webservices/etim_client/abc.php";

            show(sendDataOverPost($url,$fields)); 

            echo "bfunc(".json_encode($data).")";
            // show($data,1);
    }

    public function ticket()
    {
        $xml_str =  '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:DATASERVER">
                       <soapenv:Header/>
                       <soapenv:Body>
                          <urn:ETIM_IW_003>
                             <param_un_1>user</param_un_1>
                             <param_pw_2>trimax</param_pw_2>
                             <param_ds_3>0000037352</param_ds_3>
                          </urn:ETIM_IW_003>
                       </soapenv:Body>
                    </soapenv:Envelope>';
            // show($xml_str,1);

            $data = get_soap_data('http://192.168.1.98:447/etim_server_handheld/example1.php?wsdl', 'ETIM_IW_003', $xml_str);

            // show($data,1);
            $fields['data'] = 'test files';

            $url = "http://10.20.41.202/webservices/etim_client/abc.php";

            show(sendDataOverPost($url,$fields)); 

            //echo "bfunc(".json_encode($data).")";
            echo json_encode($data);
            // show($data,1);
    }
}
