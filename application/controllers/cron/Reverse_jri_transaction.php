<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reverse_jri_transaction extends CI_Controller {

    public function __construct() {
        parent::__construct();
        set_time_limit(0);

        ini_set('max_execution_time',0);
        ini_set("memory_limit",-1);
        
        $this->load->model(array('benificiary_master_model', 'tickets_model', 'Jri_reverse_push_data_model', 'Utilities_model', 'wallet_trans_model'));
        $this->load->library(array('Excel'));      
    }

    public function reverse_data(){

        $data = $this->Jri_reverse_push_data_model->get_all_data();
        if(count($data) == 0){
            echo json_encode(['message' => 'No data to update.']);
            exit();
        }

        foreach ($data as $key => $value) {

            $query = "SELECT wt.user_id 
            FROM recharge_transaction re 
            inner join wallet_trans wt on re.wallet_tran_id = wt.transaction_no 
            where re.transaction_id = '" . $value['TransactionReference'] . "'";
            $query = $this->db->query($query)->result_array();
            $user_id = $query[0]['user_id'];

            $check_user_wallet = $this->Utilities_model->getUserWalletDetails($user_id);

            $wallet_tran_id = $value['TransactionReference'];
            $transaction_no = substr(hexdec(uniqid()), 4, 12);

            $updated_wallet_amt = $check_user_wallet[0]['amt'] + $value['Amount'];
            $update_wallet = $this->Utilities_model->updateUserWallet($user_id, $updated_wallet_amt);

            $wallet_trans_detail['w_id'] = $update_wallet[0]['id'];
            $wallet_trans_detail['amt'] = $value['Amount'];
            $wallet_trans_detail['comment'] = 'JRI Mobile Recharge Reversal';
            $wallet_trans_detail['status'] = 'Credited';
            $wallet_trans_detail['user_id'] = $user_id;
            $wallet_trans_detail['added_by'] = $user_id;
            $wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
            $wallet_trans_detail['amt_before_trans'] = $check_user_wallet[0]['amt'];
            $wallet_trans_detail['amt_after_trans'] = $check_user_wallet[0]['amt'] + $updated_wallet_amt;
            $wallet_trans_detail['ticket_id'] = '';
                // $wallet_trans_detail['transaction_type_id'] = '3';
            $wallet_trans_detail['wallet_type'] = 'actual_wallet';
            $wallet_trans_detail['transaction_type'] = 'Credited';
            $wallet_trans_detail['is_status'] = 'Y';
            $wallet_trans_detail['transaction_no'] = $transaction_no;

            $wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail);


            $query = "UPDATE jri_reverse_push_data 
            SET reversal_flag = 1
            where TransactionReference = '" . $value['TransactionReference'] . "'";
            $query = $this->db->query($query);

            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge_reversal.log', $value, 'JRI Recharge Reversal transaction');
                    // show($update_wallet, 1);

        }

        echo json_encode(['type' => 'success', 'message' => 'Cron complete.']);
        exit();

    }
}