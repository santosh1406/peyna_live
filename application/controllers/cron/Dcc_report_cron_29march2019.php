<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Dcc_report_cron extends CI_Controller{
    public function __construct() {
        parent::__construct();
        

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);
        
        $this->load->library(array('Excel'));
        $this->load->helper("url");
    }
    public function index(){
        $y_date = date('Y-m-d'.' 00:00:00',strtotime("-1 days"));
       
        $today_date = date('Y-m-d h:m:s');
        
        $result_data  = $this->test_query($y_date, $today_date);
        
        $sr_no = 1;
        $data = array();
        foreach ($result_data as $key => $value) {
                  $data[] = array(  'Sr. No.' => $sr_no, 
                                    'Transaction Id' => $value['Transaction Id'], 
                                    'Trimax Card Id' => $value['Trimax Card Id'], 
                                    'Smart Card ID' => $value['Smart Card ID'],
                                    'Card Type' => $value['Card Type'],
                                    'SW expiry' => $value['SW expiry'],
                                    'TW expiry' => $value['TW expiry'], 
                                    'SW Bal' => $value['SW Bal'],
                                    'TW Bal' => $value['TW Bal'],
                                    'Date' => $value['Date'],
                      
                                    );
                                    $sr_no++;
        }
//        echo '<pre>';
//        print_r($value);
//        die;
//        $this->excel->data_2_excel('micro_atm_transaction_report_' . time() . '.xls', $data);
//        die;
        $this->excel->data_save_in_excel('dcc_report' . date('Y-m-d') . '.xls', $data);
            $replacements = array();

            $filename = 'dcc_report' . date('Y-m-d') . '.xls';
            $subject = 'Dcc Report.';
            $msg = "Dcc Report";
             $attachment = FCPATH . "cron_ors_report/$filename";
            $mail_array = array("subject" => $subject,
                "message" => $msg,
                "to" => array('sonali.trimax@gmail.com','mohd.rashid@sts.in','surekha.trimax@gmail.com'),
                "attachment" => $attachment
            );

            $result = send_mail($mail_array, $replacements);
            echo 'Cron Run Successfully';
            die;
    }
     
    public function test_query($y_date, $today_date){
        $sql = 'SELECT cc_txn_id AS "Transaction Id",
                trimax_id AS "Trimax Card Id",
                cust_card_id AS "Smart Card ID",
                flag AS "Card Type",
                sw_expiry AS "SW expiry",
                tw_expiry AS "TW expiry",
                sw_bal AS "SW Bal",
                tw_bal AS "TW Bal",
                created_date AS "Date"
         FROM ps_card_issuance_details
         WHERE created_date BETWEEN "2019-03-01 00:00:00" AND "'.$today_date.'"
         ORDER BY created_date';
      //echo $this->db->query($sql);
      
       
       return $this->db->query($sql)->result_array();
//       echo $this->db->last_query();
//       die;
    }
}
