<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cancel_bus_services extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $lib = ['soap_api/upsrtc_api','soap_api/msrtc_api'];
        $this->load->library($lib);
        $this->load->model(array('cron/cron_model','trip_master_model','tickets_model','payid_summary_details_model','payout_summary_details_model','payment_gateway_transaction_model','msrtc_master_services_model', 'travel_destinations_model', 'tickets_model','api_balance_log_model','wallet_model','common_model'));
    }

    public function index()
    {
        // $param = $this->uri->segment();
    }

    /*
     * Author : Suraj Rathod
     * Function : getCancelListUpsrtc()
     * Detail : To cancel bus services
     */

    public function getCancelListUpsrtc()
    {
        $data = $this->upsrtc_api->getCancelList(array('date' => "2015-07-01 17:10:00"));
        $data = $data["CancelListResponse"];

        if (isset($data["cancelledServices"]) && isset($data["cancelledServices"]["service"])) {
            $cancelled_services = $data["cancelledServices"]["service"];
            foreach ($cancelled_services as $ckey => $services) {

                if ($this->cron_model->cancelBusService($services["serviceId"])) {
                    if ($this->cron_model->cancelTicket($services["serviceId"])) {
                        $users = $this->cron_model->getTicketDetails($services["serviceId"]);
                        foreach ($users as $key => $udetail) {
                            $cancel_detail = array("{{bos_ref_no}}" => $udetail->boss_ref_no,
                                                    "{{refund_amount}}" => $udetail->tot_fare_amt_with_tax,
                                                    "{{days}}" => "7" //This must be changed afterwards
                                                    );

                            $temp_sms_msg = getSMS("ticket_cancel");
                            $temp_sms_msg = str_replace(array_keys($cancel_detail), array_values($cancel_detail), $temp_sms_msg[0]->sms_template_content);
                            $is_send = sendSMS($udetail->mobile_no, $temp_sms_msg, array("ticket_id" => $udetail->ticket_id));
                        }
                    }
                }
            }// End of For Each
        }
    }


    /*
     * Author : Suraj Rathod
     * Function : cancel_upsrtc_services()
     * Detail : To cancel bus services
     */
    public function cancel_upsrtc_services()
    {
        /*error_reporting(E_ALL);
        ini_set('display_errors', 1);*/
        ini_set('memory_limit', '-1');
        $tickets_to_be_canceled = [];
        $canceled_services      = [];
        $begin  = new DateTime(date('Y-m-d'));
        $end    = new DateTime(date('Y-m-d', strtotime("+30 days")));

        $interval   = DateInterval::createFromDateString('1 day');
        $period     = new DatePeriod($begin, $interval, $end);

        foreach($period as $dt)
        {
            $data = [];
            $dep_date = $dt->format( "Y-m-d");
            
            logging_data("check/".$dep_date.".log",$data,"Services");
        
            $data = $this->upsrtc_api->getCancelList(["date" => date("Y-m-d H:i:s", strtotime($dep_date))]);

            logging_data("cancelled_services/for_date_".$dep_date.".log",$data,$dep_date);
        
            if(isset($data["CancelListResponse"]["cancelledServices"]["service"]))
            {
                $services = $data["CancelListResponse"]["cancelledServices"]["service"];

                foreach ($services as $key => $value)
                {
                    $canceled_services[] = $value;
                    $service_date   =   trim(explode('.0', $value["departuretime"])[0]);
                    $where_array    =   [
                                            "op_id" => $this->config->item("upsrtc_operator_id"),
                                            "op_trip_no" => $value["serviceId"],
                                            "sch_dept_time" => date("H:i:s",strtotime($service_date)),
                                            /*"route_no_name" => trim($value["routename"])*/
                                        ];
                    $trip_data = $this->trip_master_model->where($where_array)->find_all();

                    $delete_condition = [];

                    if($trip_data)
                    {
                        $delete_condition["trip_no"]            =   $trip_data[0]->trip_no;
                        $delete_condition["sch_departure_date"] =   date("Y-m-d",strtotime($service_date));
                        $delete_condition["sch_departure_tm"]   =   date("H:i:s",strtotime($service_date));
                        $this->db->delete('travel_destinations', $delete_condition);
                        logging_data("cancelled_services/upsrtc/deleted_for_date_".$dep_date.".log",$this->db->last_query(),$dep_date);
                    }

                    /*To Check If canceled services tickets is booked or not*/
                    /*$services_ticket_where  =   [
                                                    "bus_service_no" => $value["serviceId"],
                                                    "dept_time" => date("Y-m-d H:i:s",strtotime($service_date))
                                                ];*/

                    $tickets_to_be_canceled[$value["serviceId"]] = $this->tickets_model
                                                                        ->where(["bus_service_no" => $value["serviceId"]])
                                                                        ->where("(dept_time='".date("Y-m-d H:i:s",strtotime($service_date))."' OR boarding_time='".date("Y-m-d H:i:s",strtotime($service_date))."')")
                                                                        ->find_all();
                }
            }
        }

        $message_to_be_sent = "";
        foreach ($tickets_to_be_canceled as $sid => $svalue)
        {
            if(!empty($svalue))
            {
                foreach ($svalue as $key => $tvalue)
                {
                    $message_to_be_sent .= "Service ID - ".$sid."<br/>";
                    $message_to_be_sent .= "Ticket ID - ".$tvalue->ticket_id."<br/>";
                    $message_to_be_sent .= "PNR Number - ".$tvalue->pnr_no."<br/>";
                    $message_to_be_sent .= "Rokad Ref No - ".$tvalue->boss_ref_no."<br/>";
                    $message_to_be_sent .= "From Stop Name - ".$tvalue->from_stop_name."<br/>";
                    $message_to_be_sent .= "Till Stop Name - ".$tvalue->till_stop_name."<br/>";
                    $message_to_be_sent .= "Dept Time - ".$tvalue->dept_time."<br/>";
                    $message_to_be_sent .= "tot_fare_amt_with_tax - ".$tvalue->tot_fare_amt_with_tax."<br/>";
                    $message_to_be_sent .= "------------------------------------------ <br/><br/><br/>";
                }
            }
        }

        $mail_to_sent_message = ($message_to_be_sent != "") ? $message_to_be_sent : "No tickets";
        $custom_error_mail = array(
                                      "custom_error_subject" => "UPSRTC Tickets to be cancelled",
                                      "custom_error_message" => "Services get cancelled from UPSRTC end.<br/>The ticket shown below might get cancelled. Please Check. <br/>".$mail_to_sent_message
                                    );

        $this->developer_custom_error_mail($custom_error_mail);

        $canceled_services_string   =   (count($canceled_services) > 0) ? json_encode($canceled_services) : "No Service Cancelled";
        $custom_error_mail = array(
                                      "custom_error_subject" => "UPSRTC Services cancelled",
                                      "custom_error_message" => "Services get cancelled from UPSRTC end.<br/> Please Check. <br/>".$canceled_services_string
                                    );
        show(1,1);
    }


    /*
     * Author : Suraj Rathod
     * Function : cancel_msrtc_services()
     * Detail : To cancel bus services
     */
    public function cancel_msrtc_services()
    {
        ini_set('memory_limit', '-1');
        $tickets_to_be_canceled = [];
        $canceled_services      = [];
        $begin  = new DateTime(date('Y-m-d'));
        $end    = new DateTime(date('Y-m-d', strtotime("+30 days")));

        $interval   = DateInterval::createFromDateString('1 day');
        $period     = new DatePeriod($begin, $interval, $end);

        foreach($period as $dt)
        {
            $data = [];
            $dep_date = $dt->format( "Y-m-d");
            
            logging_data("check/".$dep_date.".log",$data,"Services");
        
            $data = $this->msrtc_api->getCancelList(["date" => date("Y-m-d H:i:s", strtotime($dep_date))]);

            logging_data("cancelled_services/for_date_".$dep_date.".log",$data,$dep_date);
        
            if(isset($data["CancelListResponse"]["cancelledServices"]["service"]))
            {
                $services = $data["CancelListResponse"]["cancelledServices"]["service"];

                foreach ($services as $key => $value)
                {
                    $canceled_services[] = $value;
                    /*$service_date   =   trim(explode('.0', $value["departuretime"])[0]);*/
                    $service_date   =   $value["departuretime"];
                    $where_array    =   [
                                            "op_id" => $this->config->item("msrtc_operator_id"),
                                            "op_trip_no" => $value["serviceId"],
                                            "sch_dept_time" => date("H:i:s",strtotime($service_date)),
                                            /*"route_no_name" => trim($value["routename"])*/
                                        ];
                    $trip_data = $this->trip_master_model->where($where_array)->find_all();

                    $delete_condition = [];

                    if($trip_data)
                    {
                        $delete_condition["trip_no"]            =   $trip_data[0]->trip_no;
                        $delete_condition["sch_departure_date"] =   date("Y-m-d",strtotime($service_date));
                        $delete_condition["sch_departure_tm"]   =   date("H:i:s",strtotime($service_date));
                        $this->db->delete('travel_destinations', $delete_condition);
                        logging_data("cancelled_services/msrtc/deleted_for_date_".$dep_date.".log",$this->db->last_query(),$dep_date);
                    }

                    /*To Check If canceled services tickets is booked or not*/
                    /*$services_ticket_where  =   [
                                                    "bus_service_no" => $value["serviceId"],
                                                    "dept_time" => date("Y-m-d H:i:s",strtotime($service_date))
                                                ];

                    $tickets_to_be_canceled[$value["serviceId"]] = $this->tickets_model->where($services_ticket_where)->find_all();*/
                    $tickets_to_be_canceled[$value["serviceId"]] = $this->tickets_model
                                                                        ->where(["bus_service_no" => $value["serviceId"]])
                                                                        ->where("(dept_time='".date("Y-m-d H:i:s",strtotime($service_date))."' OR boarding_time='".date("Y-m-d H:i:s",strtotime($service_date))."')")
                                                                        ->find_all();
                }
            }
        }

        $message_to_be_sent = "";
        foreach ($tickets_to_be_canceled as $sid => $svalue)
        {
            if(!empty($svalue))
            {
                foreach ($svalue as $key => $tvalue)
                {
                    $message_to_be_sent .= "Service ID - ".$sid."<br/>";
                    $message_to_be_sent .= "Ticket ID - ".$tvalue->ticket_id."<br/>";
                    $message_to_be_sent .= "PNR Number - ".$tvalue->pnr_no."<br/>";
                    $message_to_be_sent .= "Rokad Ref No - ".$tvalue->boss_ref_no."<br/>";
                    $message_to_be_sent .= "From Stop Name - ".$tvalue->from_stop_name."<br/>";
                    $message_to_be_sent .= "Till Stop Name - ".$tvalue->till_stop_name."<br/>";
                    $message_to_be_sent .= "Dept Time - ".$tvalue->dept_time."<br/>";
                    $message_to_be_sent .= "tot_fare_amt_with_tax - ".$tvalue->tot_fare_amt_with_tax."<br/>";
                    $message_to_be_sent .= "------------------------------------------ <br/><br/><br/>";
                }
            }
        }

        $mail_to_sent_message = ($message_to_be_sent != "") ? $message_to_be_sent : "No tickets";
        $custom_error_mail = array(
                                      "custom_error_subject" => "MSRTC Tickets to be cancelled",
                                      "custom_error_message" => "Services get cancelled from MSRTC end.<br/>The ticket shown below might get cancelled. Please Check. <br/>".$mail_to_sent_message
                                    );

        $this->common_model->developer_custom_error_mail($custom_error_mail);

        $canceled_services_string   =   (count($canceled_services) > 0) ? json_encode($canceled_services) : "No Service Cancelled";
        $custom_error_mail = array(
                                      "custom_error_subject" => "MSRTC Services cancelled",
                                      "custom_error_message" => "Services get cancelled from MSRTC end.<br/> Please Check. <br/>".$canceled_services_string
                                    );
        show(1,1);
    }
 }