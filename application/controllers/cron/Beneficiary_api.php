<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Beneficiary_api extends CI_Controller {

    public function __construct() {
        parent::__construct();
        set_time_limit(0);
    
        ini_set('max_execution_time',0);
        ini_set("memory_limit",-1);
        
        $this->load->model(array('benificiary_master_model', 'tickets_model'));
        $this->load->library(array('Excel'));      
    }


    public function generate_registration_file(){
        $beneficieries = $this->benificiary_master_model->column(' benificiary_location_master.*, benificiary_master.*')->where('escrow_registred_flag',0)->join('benificiary_location_master','benificiary_location_master.location_code = benificiary_master.location_code')->as_array()->find_all();
        $data = array();

        if(is_array($beneficieries) && count($beneficieries)){

            foreach ($beneficieries as $b) {
                $data[] = array(
                    'Beneficiary Code' => $b['code'],
                    'Beneficiary Name' => $b['name'],
                    'Address 1' => $b['address_1'],
                    'Address 2' => $b['address_2'],
                    'Address 3' => $b['address_3'],
                    'Location Name' => $b['location_name'],
                    'State Name' => $b['state_name'],
                    'Address 1' => $b['address_1'],
                    'Pin Code'  => $b['pin_code'],
                    'Email' => $b['email'],
                    'Mobile Number' => $b['mobile'],
                    'OverAll Limit' => $b['over_all_limit']                
                );
                
                // Update escrow status
                $this->benificiary_master_model->update($b['id'], array('escrow_registred_flag'=>'1'));
            }

            // $this->generate_csv('benificiary_registration_' . time() . '.txt', $data);die;
            $this->excel->data_2_excel('benificiary_registration_' . time() . '.xls', $data, false);die;
        }
    }

    public function generate_payment_mapping_file(){
        $beneficieries = $this->benificiary_master_model->as_array()->find_all();
        $data = array();
        if (is_array($beneficieries) && count($beneficieries)){
            foreach ($beneficieries as $b) {
                $data[] = array(
                    'Beneficiary Code' => $b['code'],
                    'Payment method Name' => $b['payment_metod_name'],
                    'Bene Account number' => $b['account_number'],
                    'IFSC Code' => $b['ifsc_code'],
                    'Bene Bank Name' => $b['bank_name'],
                    'Bene branch name' => $b['branch_name'],
                    'Bene account type code' => $b['account_type'],
                    'Address 1' => $b['address_1'],
                );
            }


            // $this->generate_csv('benificiary_payment_mapping_' . time() . '.txt', $data);die;
            $this->excel->data_2_excel('benificiary_payment_mapping_' . time() . '.xls', $data, false);die;
        }
    }

    public function generate_payment_file(){

        $yesterday = date('Y-m-d', strtotime("-1 days"));
        $whereClause = "transaction_status='success' AND inserted_date BETWEEN '{$yesterday} 00:00:00' AND '{$yesterday} 23:59:59'";

        $tickets = $this->tickets_model->column('ticket_id, pnr_no, boss_ref_no, tot_fare_amt_with_tax, inserted_date')->where($whereClause)->as_array()->find_all();
        $bene = $this->benificiary_master_model->as_array()->find(1);

        if (is_array($bene) && is_array($tickets) && count($tickets)){
            $data = array();
            foreach ($tickets as $t) {
                $data[] = array(
                    'Channel Identifier' => '111',
                    'CorporateIdentifier' => 'KRAHEJACORP',
                    'Additional Info 1' => 'CHHAYA',
                    'Additional Info 2' => 'BHAVESH',
                    'Additional Info 3' => 'BHAVSAR',
                    'Additional Info 4' => '',
                    'Additional Info 6' => '',
                    'Additional Info 7' => '',
                    'Payment Method Name' => strtoupper(substr($bene['payment_metod_name'], 0, 1)),
                    'Corporate Product Code' => 'ALL',
                    'CRN No' => $t['boss_ref_no'],
                    'Beneficiary Name' => $bene['name'],
                    'Payment Amount' => $t['tot_fare_amt_with_tax'],
                    'Debit Account No' => '028010200000550',
                    'Activation Date' => $t['inserted_date'],
                    'NEFT/RTGS IFSC Code' => $bene['ifsc_code'],
                    'Bene Account No' => $bene['account_number'],
                    'Bene Account Type Code' => $bene['account_type'],
                    'Email' => $bene['email'],
                    'Email Body' => '',
                    'Remarks' => '',
                );
            }
            
            // $this->generate_csv('payment_' . time() . '.txt', $data);die; 
            $this->excel->data_2_excel('payment_' . time() . '.xls', $data, false);die;    
        }        
    }

    /**
     * Not being used for now
     *
     */
    private function generate_csv($fileName='', $data = array()){
        $titleArray = array_keys($data[0]);
        $file = "export/$fileName";        
        if(!file_exists(dirname($file))){
            mkdir(dirname($file), 0770, true);
        }
        $handle = fopen($file,'w') or die("Can't open $file");
        header("Content-Type: application/octet-stream");
        header("Content-Disposition:attachment;filename=$fileName");

        // fputs($handle, '"' . implode('","', $titleArray) . '"'. PHP_EOL);
        foreach($data as $row) {
            fputs($handle, '"' . implode('","', $row) . '"'. PHP_EOL);
        }
        fclose($handle) or die("Can't close $file");
        readfile($file);
        exit;
    }

}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */