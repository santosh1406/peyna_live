<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Dcc_report_cron_consolidate extends CI_Controller{
    public function __construct() {
        parent::__construct();
        

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);
        
        $this->load->library(array('Excel'));
        $this->load->helper("url");
        
    }
    
    public function getCardIssuanceDetails($y_date, $today_date){
          $sql = 'SELECT cid.cc_txn_id AS "Transaction Id",
                cid.trimax_id AS "Trimax Card Id",
                cid.cust_card_id AS "Smart Card ID",
                cid.flag AS "Card Type",
                cid.sw_expiry AS "SW expiry",
                cid.tw_expiry AS "TW expiry",
                cid.sw_bal AS "SW Bal",
                cid.tw_bal AS "TW Bal",
                cid.created_date AS "Date",
                ccd.card_status,
				cid.pos_status_code,
				cid.pos_request_id,
				cct.request_id,
				cid.created_by,
                                cct.txn_ref_no,
                                cct.created_date,
                                cct.request_id  
         FROM ps_card_issuance_details cid
         join ps_customer_card_master ccd on cid.trimax_id  = ccd.trimax_card_id
		 left join ps_card_transaction cct on cct.pos_request_id  = cid.pos_request_id
         WHERE cid.created_date BETWEEN "'.$y_date.'" AND "'.$today_date.'" 	and ccd.is_active="1" ORDER BY cid.created_date';
//       $this->db->query($sql);
//               echo $this->db->last_query();
//              die;
       
       return $this->db->query($sql)->result_array();

    }
     public function getTransType(){
        $sql = 'select * from ps_card_trans_type';
       
       return $this->db->query($sql)->result_array();

    }
	

	
	 public function getUsers(){
        $sql = 'select au.id,rsm.terminal_id, rsm.terminal_code,
		 rsm.depot_code,  rsm.division_code,au.username,agent_code
           from users au
		 join retailer_service_mapping rsm on rsm.agent_id=au.id  where  rsm.service_id="'.SMART_CARD_SERVICE.'"';
         return $this->db->query($sql)->result_array();

    }
	
	public function getDepotDivision()
	{
        $sql = 'SELECT dpt.DEPOT_NM,dvsn.DIVISION_NM,dpt.DEPOT_CD,dvsn.DIVISION_CD  FROM msrtc_replication.`depots` AS dpt 
		               JOIN msrtc_replication.divisions AS dvsn ON dvsn.DIVISION_CD=dpt.`DIVISION_CD`';
         return $this->db->query($sql)->result_array();
    }
	
	
	 public function getCustomerName($ids){
        $sql = 'select * from ps_customer_master inner join 
		   ps_customer_card_master on ps_customer_master.id=ps_customer_card_master.customer_id where trimax_card_id in ('.$ids.')';
         return $this->db->query($sql)->result_array();

    }
	
	 public function getCustomerNameByCustIds($ids){
        $sql = 'select * from ps_customer_master inner join 
		   ps_customer_card_master on ps_customer_master.id=ps_customer_card_master.customer_id where cust_card_id in ('.$ids.')';
         return $this->db->query($sql)->result_array();

    }
	
	
   
    public function getTransactionDetails($y_date, $today_date){
        
        $sql = 'SELECT pccm.name_on_card AS "Name",
                pcm.mobile_number AS "Contact Number",
                category_name AS "Card Category",
                pccm.trimax_card_id AS "Trimax Card Id",
                pccm.cust_card_id AS "Smart Card ID",
                pmc.card_category_cd "Card Category",
                pct.transaction_type_code AS "Transaction Type",
                "Cash" AS "Transaction Mode",
                pct.receipt_number AS "Receipt Number",
                pct.created_date AS "Transaction Date",
                pccm.dispatch_date AS "Issuance Date",
                pct.concession_nm AS "Pass/Concession Type",
                pct.span_name AS "Pass Validity",
                if(pct.transaction_type_code ="TPTS",tw_via_sw_total_amount,total_amount) AS "Total Amount",
                max(CASE 
                        WHEN (patd.amount_code = "BF"
                              OR patd.amount_code = "PassCharge") THEN patd.amount
                        ELSE 0.00
                    END) AS "Pass Amount",
                max(CASE
                        WHEN patd.amount_code = "SCF" THEN patd.amount
                        ELSE 0.00
                    END) AS "Smart Card Fee",
                max(CASE
                        WHEN patd.amount_code = "AF" THEN patd.amount
                        ELSE 0.00
                    END) AS "Application Fee",
                max(CASE
                        WHEN patd.amount_code = "GST" THEN patd.amount
                        ELSE 0.00
                    END) AS "GST",
                max(CASE
                        WHEN patd.amount_code = "LoadingFee" THEN patd.amount
                        ELSE 0.00
                    END) AS "Loading Fee",
                max(CASE
                        WHEN patd.amount_code = "BCCharges" THEN patd.amount
                        ELSE 0.00
                    END) AS "BC Charge",
                max(CASE
                        WHEN patd.amount_code = "EPurseFees" THEN patd.amount
                        ELSE 0.00
                    END) AS "e-Purse Maintanence Fee",
                max(CASE
                        WHEN patd.amount_code = "CardFee" THEN patd.amount
                        ELSE 0.00
                    END) AS "Card Fee",
                max(CASE
                        WHEN patd.amount_code = "BonusAmt" THEN patd.amount
                        ELSE 0.00
                    END) AS "Bonus Amt",
				max(CASE
                        WHEN patd.amount_code = "PassCharge" THEN patd.amount
                        ELSE 0.00
                    END) AS "Pass Charge", 
					
				 if(pct.transaction_type_code="TPBP",
							  (max(CASE 
									WHEN (patd.amount_code = "BF"
										  OR patd.amount_code = "PassCharge") THEN patd.amount
									ELSE 0.00
								END) +  max(CASE
									WHEN patd.amount_code = "EPurseFees" THEN patd.amount
									ELSE 0.00
								END))
							 ,if(pct.transaction_type_code="RGOT", max(CASE 
									WHEN (patd.amount_code = "BF"
										  OR patd.amount_code = "PassCharge") THEN patd.amount
									ELSE 0.00
								END), if(pct.transaction_type_code="RG",(max(CASE 
									WHEN (patd.amount_code = "BF"
										  OR patd.amount_code = "PassCharge") THEN patd.amount
									ELSE 0.00
								END)+ max(CASE
                        WHEN patd.amount_code = "CardFee" THEN patd.amount
                        ELSE 0.00
                    END)) , if(pct.transaction_type_code="TPSW" or pct.transaction_type_code="TPTS" ,max(CASE
									WHEN patd.amount_code = "EPurseFees" THEN patd.amount
									ELSE 0.00
								END),if(pct.transaction_type_code="TPTC",(max(CASE 
									WHEN (patd.amount_code = "BF"
										  OR patd.amount_code = "PassCharge") THEN patd.amount
									ELSE 0.00
								END)+max(CASE
									WHEN patd.amount_code = "EPurseFees" THEN patd.amount
									ELSE 0.00
								END) )) ) )))	as "Settlement MSRTC", 
					
                pct.request_id    AS "Request ID",
                pct.txn_ref_no AS "Transaction Ref No.",
				pct.pos_txn_ref_no AS "pos_txn_ref_no",
                at.depot_code AS "Depot code",
                at.terminal_code AS "Terminal code",
                pcm.pincode AS "Pincode",
				au.id AS "AgentId",
			    au.agent_code AS "agent_code",
				pct.created_date AS "created_date",
				pct.id AS "txn_id",
				if(pct.transaction_type_code="TPTS",(sum(if(patd.amount_code="BonusAmt",patd.amount,0))+tw_via_sw_total_amount),pct.amount) AS "effective_amount",
				CASE WHEN pct.txn_ref_no IS NULL THEN "Failed" WHEN tw_created_status = "C" THEN "Success" ELSE "Failed" END as "Status"

		   FROM ps_customer_master pcm
         LEFT OUTER JOIN ps_customer_card_master pccm ON pcm.id = pccm.customer_id
         INNER JOIN ps_master_card pmc ON pccm.master_card_id = pmc.id
         INNER JOIN ps_card_transaction pct ON pccm.trimax_card_id = pct.trimax_card_id
         INNER JOIN ps_card_transaction_details pctd ON pct.id = pctd.card_transaction_id
         INNER JOIN ps_amount_transaction pat ON pct.id = pat.card_transaction_id
         INNER JOIN ps_amount_transaction_details patd ON patd.amount_transaction_id = pat.id
         INNER JOIN ps_pass_category ppc ON ppc.id = pass_category_id
         INNER JOIN users au ON pct.created_by = au.id
         INNER JOIN retailer_service_mapping at ON pct.terminal_id = at.terminal_id
         WHERE 1=1
         and tw_created_status = "C"
         AND pct.created_date BETWEEN "'.$y_date.'" AND "'.$today_date.'"
          and at.service_id="'.SMART_CARD_SERVICE.'" and at.status="Y"
        GROUP BY patd.amount_transaction_id
        ORDER BY pct.transaction_type_id,
         pct.created_date';
              ///AND pct.is_active = "1"
        
        /*$this->db->query($sql);
        echo $this->db->last_query();
        die;*/
       //pct.amount AS "effective_amount"
       return $this->db->query($sql)->result_array();
    }
    
  
    public function getWalletClaimDetails($y_date, $today_date){
        $sql = 'SELECT transaction_id AS "Transaction Id",
                pswd.cust_card_id  AS "Smart Card ID",
                mobile_number AS "Mobile No",
                opening_balance AS "Opening Bal",
                amount AS "Top Up Amt",
                closing_balance AS "Closing Bal",
                expiry_date AS "Expiry Date",
                topup_flag AS "Topup Flag",
				pswd.request_id AS "request_id",
				pswd.txn_ref_no AS "txn_ref_no",
				pswd.pos_request_id AS "pos_request_id",
				pswd.pos_txn_ref_no AS "pos_txn_ref_no",
                pswd.created_date AS "Date",
				pswd.created_by
				
         FROM ps_claim_wallet_details pswd
         WHERE pswd.created_date BETWEEN "'.$y_date.'" AND "'.$today_date.'"
         ORDER BY topup_flag,
                  pswd.created_date;';
        //  INNER JOIN ps_customer_card_master pccm ON pswd.cust_card_id = pccm.cust_card_id
        /* $this->db->query($sql);
        echo $this->db->last_query();
        die;*/
       
       return $this->db->query($sql)->result_array();
	   
    }
	
	
	
     public function getBusPassWalletClaimDetails($y_date, $today_date){
         $sql = 'SELECT 
		        trimax_card_id AS "Trimax Card Id",
                pccm.cust_card_id  AS "Smart Card ID",
                mobile_number AS "Mobile No",
				pass_data AS "pass_data",
            	pswd.request_id AS "request_id",
				pswd.txn_ref_no AS "txn_ref_no",
				pswd.pos_request_id AS "pos_request_id",
				pswd.pos_txn_ref_no AS "pos_txn_ref_no",
                pswd.created_date AS "Date",
				 pswd.created_by 
				
         FROM ps_claim_pass_details pswd
         INNER JOIN ps_customer_card_master pccm ON pswd.cust_card_id = pccm.cust_card_id
         WHERE pswd.created_date BETWEEN "'.$y_date.'" AND "'.$today_date.'"	
         ORDER BY  pswd.created_date;';
         return $this->db->query($sql)->result_array();
     }
   
      public function reconciliation_consolidate(){
       $yest_date = date('Y-m-d'.' 00:00:00',strtotime("-1 days"));   
       $yest_date_forFile = date('Y-m-d'.'',strtotime("-1 days"));   	   
       $today_date = date('Y-m-d').' 23:59:59';
      //  $yest_date = date('2019-06-11'.' 00:00:00',strtotime("-1 days"));       
        //$today_date = date('2019-06-13').' 23:59:59';
        $result_data  =  $this->getAllTransaction($yest_date, $today_date);
		if(!empty($result_data))
		{
        $getTransType  = $this->getTransType();
	    $TransTypeArr=array() ;
        foreach($getTransType as $TransType)
        {
           $TransTypeArr[$TransType['ctrans_type_code']]=$TransType['ctrans_type_name'];        
        }
		
		  $getDepotsDivisions  = $this->getDepotDivision();
		  $getDepotArr=array() ;
		  $getDivisionArr=array() ;
		  
		 foreach($getDepotsDivisions as $getDepotDivision)
		 {
		   $getDepotArr[$getDepotDivision['DEPOT_CD']]=$getDepotDivision['DEPOT_NM']; 
		   $getDivisionArr[$getDepotDivision['DIVISION_CD']]=$getDepotDivision['DIVISION_NM'];   			   
		 }
        
        $sr_no = 1;
        $data = array();
			///$result_data=array() ;
            foreach ($result_data as $key => $value) 
             {
			 $ReconID = '' ;
			 if($value['Transaction Ref No.']==''){
			     $ReconID = $value['pos_txn_ref_no'];
			  }else{
			     $ReconID = $value['Transaction Ref No.'] ;
			 }
			 
               $data_arr = array('Sr. No.' => $sr_no,
                    'Name' => $value['Name'],
                    'Transaction Type' => $TransTypeArr[$value['Transaction Type']],
                    'Trimax request id' => $value['Request ID'],
                    'Transaction Ref No.' => $value['Transaction Ref No.'],
                    'pos_request_id' => $value['pos_request_id'],
				    'Pos Transaction Ref No.' => $value['pos_txn_ref_no'],
					'Recon ID' => $ReconID,
                    'Card Category' => $value['Card Category'],
                    'Receipt Number' => $value['Receipt Number'],
                    'Trimax Card Id' => $value['Trimax Card Id'],
                    'Smart Card ID' => $value['Smart Card ID'],
                    'Contact Number' => $value['Contact Number'],
                    'Total Amount' => isset($value['Total Amount']) ? $value['Total Amount'] : '0', 
                    'Pass Amount' => $value['Pass Amount'],
                    'Smart Card Fee' => $value['Smart Card Fee'],
                    'Card Fee' => $value['Card Fee'],
                    'Application Fee' => $value['Application Fee'],
		            'Pass charges' => $value['Pass Charge'], 
                    'Loading Fee' => $value['Loading Fee'],
                    'GST' => $value['GST'],
                    'BC Charge' => $value['BC Charge'],
                    'e-Purse Maintenance Fee' => $value['e-Purse Maintenance Fee'],
                    'Bonus Amt' => $value['Bonus Amt'],
		            'Effective Amount' => $value['effective_amount'],
		            'MSRTC Payble' => $value['MSRTC Payble'], 
                    'Status' =>$value['Status'],
                    'Transaction Date' => $value['Transaction Date'],
                    'Terminal code' => $value['Terminal code'],
                    'agent_id' => $value['agent_id'],
                    'agent_code' => $value['agent_code'],
                    /*'Login Id' => rokad_decrypt($value['Login Id'],$this->config->item('pos_encryption_key')), */
                    'Depot Name' =>    $getDepotArr[$value['Depot Name']],
                    'Division name' => $getDivisionArr[$value['Division name']] ,
                    'Pass Validity' => $value['Pass Validity'],
                    'Pass/Concession Type' => $value['Pass/Concession Type'],
                    'Transaction Referece id (Registration)' => '',
                    'Registraion date' => '',
                    'Trimax Request ID(Registration)' => '',
                   
               );
				
                if(isset($_REQUEST['txn_id']))
                {
                   $data_arr['Transaction Id'] = $value['txn_id'] ;
                }
                $data[] = $data_arr ; 
                $sr_no++;
            }
        
           $result_data  = $this->getBusPassWalletClaimDetails($yest_date, $today_date);
		   $getUsers  = $this->getUsers();
			$getUsersArr=array() ;
			foreach($getUsers as $getUser)
			{
			   $getUsersArr[$getUser['id']]=$getUser;        
			}
		   if(!empty($result_data))
		   {
			 
			$triamx_id_arr=array();
            foreach ($result_data as $key => $value) 
            {
			$triamx_id_arr[] = $value['Trimax Card Id'] ;
			}
			$string_version = implode(',', $triamx_id_arr) ;
			$NameArray=$this->getCustomerName($string_version) ;
		
			$newNameArr =array() ;
			foreach($NameArray as $NameArr)
			{
			   $newNameArr[$NameArr['trimax_card_id']]=$NameArr;        
			}
            foreach ($result_data as $key => $value) 
            {
			    $ReconID = '' ;
				 if($value['txn_ref_no']==''){
					 $ReconID = $value['pos_txn_ref_no'];
				  }else{
					 $ReconID = $value['txn_ref_no'] ;
				 }
			
                 $data_arr = array('Sr. No.' => $sr_no,
                    'Name' => $newNameArr[$value['Trimax Card Id']]['name_on_card'],
                    'Transaction Type' => 'Bus Pass Claim',
                    'Trimax request id' => $value['request_id'],
                    'Transaction Ref No.' => $value['txn_ref_no'],
                    'pos_request_id' =>  $value['pos_request_id'],
		            'Pos Transaction Ref No.' => $value['pos_txn_ref_no'],
					'Recon ID' => $ReconID,
                    'Card Category' =>   $newNameArr[$value['Trimax Card Id']]['card_category_cd'], 
                    'Receipt Number' =>'',
                    'Trimax Card Id' => $value['Trimax Card Id'],
                    'Smart Card ID' =>  $value['Smart Card ID'],
                    'Contact Number' => $value['Mobile No'],
                    'Total Amount' => '0.00',
                    'Pass Amount' => '0.00',
                    'Smart Card Fee' => '0.00',
                    'Card Fee' => '0.00',
                    'Application Fee' => '0.00',
		            'Pass charges' => '0.00', 
                    'Loading Fee' => '0.00',
                    'GST' =>'0.00',
                    'BC Charge' => '0.00',
                    'e-Purse Maintenance Fee' => '0.00',
                    'Bonus Amt' => '0.00',
					'Effective Amount' => '0.00',
					'MSRTC Payble' => '0.00', 
                    'Status' => 'Success',
                    'Transaction Date' => $value['Date'],
                    'Terminal code' => $getUsersArr[$value['created_by']]['terminal_code'], 
                    'agent_id' =>      $getUsersArr[$value['created_by']]['id'] ,
                    'agent_code' =>    $getUsersArr[$value['created_by']]['agent_code'],
                    /*'Login Id' =>      rokad_decrypt($getUsersArr[$value['created_by']]['username'], $this->config->item('pos_encryption_key')), */
                    'Depot Name' =>  $getDepotArr[$getUsersArr[$value['created_by']]['depot_code']],  //$getUsersArr[$value['created_by']]['depot_code'],
                    'Division name' => $getDivisionArr[$getUsersArr[$value['created_by']]['division_code']], //$getUsersArr[$value['created_by']]['division_code'],
                    'Pass Validity' => '',
                    'Pass/Concession Type' => '',
                    'Transaction Referece id (Registration)' => '',
                    'Registraion date' => '',
                    'Trimax Request ID(Registration)' => '',
                 );
                $data[] = $data_arr ; 
                $sr_no++;
            }
		  }	
		 }	
		   $result_data  = $this->getWalletClaimDetails($yest_date, $today_date);
		   if(!empty($result_data ))
		   {
					$triamx_id_arr=array();
					foreach ($result_data as $key => $value) 
					{
					$cust_id_arr[] = $value['Smart Card ID'] ;
					}
					$string_version = implode(',', $cust_id_arr) ;
					$NameArray=$this->getCustomerNameByCustIds($string_version) ;
				
					$newNameArr =array() ;
					foreach($NameArray as $NameArr)
					{
					   $newNameArr[$NameArr['cust_card_id']]=$NameArr;        
					}
					
					foreach ($result_data as $key => $value) 
					{
					     $ReconID = '' ;
						 if($value['txn_ref_no']==''){
							 $ReconID = $value['pos_txn_ref_no'];
						  }else{
							 $ReconID = $value['txn_ref_no'] ;
						 }
					
						 $data_arr = array('Sr. No.' => $sr_no,
							'Name' => $newNameArr[$value['Smart Card ID']]['name_on_card'],
							'Transaction Type' => strtoupper($value['Topup Flag']).' Wallet Claim',
							'Trimax request id' => $value['request_id'],
							'Transaction Ref No.' => $value['txn_ref_no'],
							'pos_request_id' =>   $value['pos_request_id'],
							'Pos Transaction Ref No.' => $value['pos_txn_ref_no'],
							 'Recon ID' => $ReconID,

							'Card Category' =>  $newNameArr[$value['Smart Card ID']]['card_category_cd'],
							'Receipt Number' =>'',
							'Trimax Card Id' => $newNameArr[$value['Smart Card ID']]['trimax_card_id'],
							'Smart Card ID' =>  $value['Smart Card ID'],
							'Contact Number' => $newNameArr[$value['Smart Card ID']]['mobile_number'],
							'Total Amount' => '0.00',
							'Pass Amount' =>isset($value['Top Up Amt']) ? $value['Top Up Amt'] : '0.00',
							'Smart Card Fee' => '0.00',
							'Card Fee' => '0.00',
							'Application Fee' => '0.00',
							 'Pass charges' => '0.00', 
							'Loading Fee' => '0.00',
							'GST' =>'0.00',
							'BC Charge' => '0.00',
							'e-Purse Maintenance Fee' => '0.00',
							'Bonus Amt' => '0.00',
							'Effective Amount' => '0.00',
							'MSRTC Payble' => '0.00', 
							'Status' =>'Success',
							'Transaction Date' => $value['Date'],
							'Terminal code' => $getUsersArr[$value['created_by']]['terminal_code'], 
							'agent_id' =>      $getUsersArr[$value['created_by']]['id'] ,
							'agent_code' =>    $getUsersArr[$value['created_by']]['agent_code'],
							/*'Login Id' =>      rokad_decrypt($getUsersArr[$value['created_by']]['username'], $this->config->item('pos_encryption_key')), */
							'Depot Name' => $getDepotArr[$getUsersArr[$value['created_by']]['depot_code']] ,    //$getUsersArr[$value['created_by']]['depot_code'],
							'Division name' =>$getDivisionArr[$getUsersArr[$value['created_by']]['division_code']] , //$getUsersArr[$value['created_by']]['division_code'],
							'Pass Validity' => '',
							'Pass/Concession Type' => '',
                                                        'Transaction Referece id (Registration)' => '',
                                                        'Registraion date' => '',
                                                        'Trimax Request ID(Registration)' => '',
							
						 );
						$data[] = $data_arr ; 
						$sr_no++;
					}
		   }
           $result_data  = $this->getCardIssuanceDetails($yest_date, $today_date);
		   if(!empty($result_data))
		   {
				$triamx_id_arr=array();
				foreach ($result_data as $key => $value) 
				{
				$cust_id_arr[] = $value['Smart Card ID'] ;
				}
				$string_version = implode(',', $cust_id_arr) ;
				
				$NameArray=$this->getCustomerNameByCustIds($string_version) ;
			
				$newNameArr =array() ;
				foreach($NameArray as $NameArr)
				{
				   $newNameArr[$NameArr['cust_card_id']]=$NameArr;        
				}
				
			   
				foreach ($result_data as $key => $value) 
				{
				    $ReconID = '' ;
					 if($value['Transaction Id']==''){
						 $ReconID = $value['pos_txn_ref_no'];
					  }else{
						 $ReconID = $value['Transaction Id'] ;
					 }
				
				 switch ($value['card_status']) {
						case "I":
							$status = "Initiated when user register";
							break;
						case "P":
							$status = "Pending";
							break;
						case "T":
							$status = "Transport from region to depot";
							break;
						case "A":
							$status = "At depot writing the card";
							break;
						case "Y":
							$status = "Available for dispatch";
							break;
						case "D":
							$status = "Dispatched";
							break;
						case "L":
							$status = "Card Loss";
							break;
						case "F":
							$status = "Card Faulty";
							break;
						case "C":
							$status = "Cancelled by user";
							break;
						case "R":
							$status = "Rejected";
							break;
						case "S":
							$status = "Surrender";
							break;
						case "B":
							$status = "Block";
							break;
						case "X":
							$status = "Card not updated at CC";
							break;
						default:
							$status = "None";
					}
					 $data_arr = array('Sr. No.' => $sr_no,
						'Name' =>  $newNameArr[$value['Smart Card ID']]['name_on_card'],
						'Transaction Type' => 'Card Issuance',
						'Trimax request id' => $value['request_id'],
						'Transaction Ref No.' => $value['Transaction Id'],
						'pos_request_id' =>   $value['pos_request_id'],
						'Pos Transaction Ref No.' => $value['pos_txn_ref_no'],
						'Recon ID' => $ReconID,
                        'Card Category' =>   $value['Card Type'] ,
						'Receipt Number' =>'',
						'Trimax Card Id' => $newNameArr[$value['Smart Card ID']]['trimax_card_id'],
						'Smart Card ID' =>  $value['Smart Card ID'],
						'Contact Number' => $newNameArr[$value['Smart Card ID']]['mobile_number'],	
						'Total Amount' => '0.00',
						'Pass Amount' => isset($value['Top Up Amt']) ? $value['Top Up Amt'] : '0.00', 
						'Smart Card Fee' => '0.00',
						'Card Fee' => '0.00',
						'Application Fee' => '0.00',
						 'Pass charges' => '0.00', 
						'Loading Fee' => '0.00',
						'GST' =>'0.00',
						'BC Charge' => '0.00',
						'e-Purse Maintenance Fee' => '0.00',
						'Bonus Amt' => '0.00',
						'Effective Amount' => '0.00',
						'MSRTC Payble' => '0.00', 
						'Status' =>$status,
						'Transaction Date' => $value['Date'],
						'Terminal code' =>    $getUsersArr[$value['created_by']]['terminal_code'], 
						'agent_id' =>         $getUsersArr[$value['created_by']]['id'] ,
						'agent_code' =>       $getUsersArr[$value['created_by']]['agent_code'],
						/*'Login Id' =>         rokad_decrypt($getUsersArr[$value['created_by']]['username'], $this->config->item('pos_encryption_key')), */
						'Depot Name' =>   $getDepotArr[$getUsersArr[$value['created_by']]['depot_code']] ,     //$getUsersArr[$value['created_by']]['depot_code'],
						'Division name' =>  $getDivisionArr[$getUsersArr[$value['created_by']]['division_code']] ,  //$getUsersArr[$value['created_by']]['division_code'],
						'Pass Validity' => '',
						'Pass/Concession Type' => '',
                                                'Transaction Referece id (Registration)' => $value['txn_ref_no'],
                                                'Registraion date' => $value['created_date'],
                                                'Trimax Request ID(Registration)'=> $value['request_id'],
						
					   );
					$data[] = $data_arr ; 
					$sr_no++;
				}
		   }
                   log_data('cron/ROKAD_Report_' . date('d-m-Y') . '.log', $data, 'ROKAD_Report');
            $this->excel->data_save_in_excel('ROKAD_Report_' . $yest_date_forFile . '.xls', $data);
            $replacements = array();

            $filename = 'ROKAD_Report_' . $yest_date_forFile . '.xls';
            $subject = 'Demo Consolidate Transaction Report of '.$yest_date_forFile.'.'; 
           if(empty($data)){
		      $msg = "No Transaction found.";
           }else{
   		    $msg = "Demo Transaction Report of ".$yest_date;
            }
			
			$attachment = FCPATH . "cron_ors_report/$filename";
			//,'nagaraju.m@citycash.in'
	     //   define('DCC_REPORT_CRON1',json_encode(array('mohd.rashid@sts.in','surekha.trimax@gmail.com')));

			
            $mail_array = array("subject" => $subject,
                "message" => $msg,
                "to" => json_decode(DCC_REPORT_CRON), 
		// "to" => 'surekha.trimax@gmail.com',
                "attachment" => $attachment
            );

            send_mail($mail_array, $replacements);
            echo 'Cron Run Successfully';
            die;
      ///  }  
    } 
  
	
 
    
  public function getAllTransaction($y_date, $today_date){
        
        $sql = 'SELECT pccm.name_on_card AS "Name",
                pct.transaction_type_code AS "Transaction Type",
                pct.request_id    AS "Request ID",
                pct.txn_ref_no AS "Transaction Ref No.",
                pct.pos_txn_ref_no AS "pos_txn_ref_no",
                pccm.card_category_cd AS "Card Category",
                pct.receipt_number AS "Receipt Number",
                pccm.trimax_card_id AS "Trimax Card Id",
                pccm.cust_card_id AS "Smart Card ID",
                pcm.mobile_number AS "Contact Number",
                if(pct.transaction_type_code ="TPTS",tw_via_sw_total_amount,total_amount) AS "Total Amount",                          max(CASE 
                        WHEN (patd.amount_code = "BF"
                              OR patd.amount_code = "PassCharge") THEN patd.amount
                        ELSE 0.00
                    END) AS "Pass Amount",
                max(CASE
                        WHEN patd.amount_code = "SCF" THEN patd.amount
                        ELSE 0.00
                    END) AS "Smart Card Fee",
                max(CASE
                        WHEN patd.amount_code = "CardFee" THEN patd.amount
                        ELSE 0.00
                    END) AS "Card Fee",                    
               max(CASE
                        WHEN patd.amount_code = "AF" THEN patd.amount
                        ELSE 0.00
                    END) AS "Application Fee",
                  max(CASE
                        WHEN patd.amount_code = "PassCharge" THEN patd.amount
                        ELSE 0.00
                    END) AS "Pass Charge",                     
                max(CASE
                        WHEN patd.amount_code = "LoadingFee" THEN patd.amount
                        ELSE 0.00
                    END) AS "Loading Fee",
                max(CASE
                        WHEN patd.amount_code = "GST" THEN patd.amount
                        ELSE 0.00
                    END) AS "GST",
              max(CASE
                        WHEN patd.amount_code = "BCCharges" THEN patd.amount
                        ELSE 0.00
                    END) AS "BC Charge",
              
                max(CASE
                        WHEN patd.amount_code = "EPurseFees" THEN patd.amount
                        ELSE 0.00
                    END) AS "e-Purse Maintenance Fee",
                    
               max(CASE
                        WHEN patd.amount_code = "BonusAmt" THEN patd.amount
                        ELSE 0.00
                    END) AS "Bonus Amt",
               if(pct.transaction_type_code="TPTS",(sum(if(patd.amount_code="BonusAmt",patd.amount,0))+tw_via_sw_total_amount),pct.amount) AS "effective_amount",
               
			   if(pct.transaction_type_code="TPBP",
							  (max(CASE 
									WHEN (patd.amount_code = "BF"
										  OR patd.amount_code = "PassCharge") THEN patd.amount
									ELSE 0.00
								END) +  max(CASE
									WHEN patd.amount_code = "EPurseFees" THEN patd.amount
									ELSE 0.00
								END))
							 ,if(pct.transaction_type_code="RGOT", max(CASE 
									WHEN (patd.amount_code = "BF"
										  OR patd.amount_code = "PassCharge") THEN patd.amount
									ELSE 0.00
								END), if(pct.transaction_type_code="RG",(max(CASE 
									WHEN (patd.amount_code = "BF"
										  OR patd.amount_code = "PassCharge") THEN patd.amount
									ELSE 0.00
								END)+ max(CASE
                        WHEN patd.amount_code = "CardFee" THEN patd.amount
                        ELSE 0.00
                    END)) , if(pct.transaction_type_code="TPSW" ,
					            
								max(CASE
									WHEN patd.amount_code = "EPurseFees" THEN patd.amount
									ELSE 0.00
								END),if(pct.transaction_type_code="TPTC" or pct.transaction_type_code="TPTS",
								  (max(CASE 
									WHEN (patd.amount_code = "BF"
										  OR patd.amount_code = "PassCharge") THEN patd.amount
									ELSE 0.00
								END)+max(CASE
									WHEN patd.amount_code = "EPurseFees" THEN patd.amount
									ELSE 0.00
								END) ),0) ) )))	 as "MSRTC Payble", 
								
           CASE WHEN pct.txn_ref_no IS NULL THEN "Failed" WHEN tw_created_status = "C" THEN "Success" ELSE "Failed" END as "Status",
                pct.created_date AS "Transaction Date",
                at.terminal_code AS "Terminal code",
                au.id AS "agent_id",
		au.agent_code AS "agent_code",
                au.username AS "Login Id",
                at.depot_code AS "Depot Name",
                at.division_code AS "Division name",
                pct.span_name AS "Pass Validity",
                pct.concession_nm AS "Pass/Concession Type",
                pccm.dispatch_date AS "Issuance Date"

          FROM ps_customer_master pcm
         LEFT OUTER JOIN ps_customer_card_master pccm ON pcm.id = pccm.customer_id
         INNER JOIN ps_master_card pmc ON pccm.master_card_id = pmc.id
         INNER JOIN ps_card_transaction pct ON pccm.trimax_card_id = pct.trimax_card_id
         INNER JOIN ps_card_transaction_details pctd ON pct.id = pctd.card_transaction_id
         INNER JOIN ps_amount_transaction pat ON pct.id = pat.card_transaction_id
         INNER JOIN ps_amount_transaction_details patd ON patd.amount_transaction_id = pat.id
         INNER JOIN ps_pass_category ppc ON ppc.id = pass_category_id
         INNER JOIN users au ON pct.created_by = au.id
         INNER JOIN retailer_service_mapping at ON pct.terminal_id = at.terminal_id
         WHERE 1=1
         and (tw_created_status = "C" or tw_created_status = "NC")
         AND pct.created_date BETWEEN "'.$y_date.'" AND "'.$today_date.'"
          and at.service_id="'.SMART_CARD_SERVICE.'" and at.status="Y"
        GROUP BY patd.amount_transaction_id
        ORDER BY pct.transaction_type_id,
         pct.created_date';

       return $this->db->query($sql)->result_array();
    }
    
 		
}
