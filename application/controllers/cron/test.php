<?php 
class Test extends CI_Controller {
	
 	public function __construct() {
        parent::__construct();
        // $lib = array();
        // foreach (glob(APPPATH . "libraries/soap_api/" . "*.php") as $filename) {
        //     $path = 'soap_api/' . strtolower(basename($filename, '.php'));
        //     array_push($lib, $path);
        // }

        $lib = [
                    'soap_api/upsrtc_api',
                    'soap_api/msrtc_api',
                    'api_provider/hrtc_api',
                    'soap_api/rsrtc_api',
                    'api_provider/its_api',
                    'api_provider/etravelsmart_api'
        ];

        $this->load->library($lib);

        $this->load->model(array('tmp_agent_balance_track_model','cron/cron_model', 'payid_summary_details_model','payout_summary_details_model','payment_gateway_transaction_model','master_services_model', 'travel_destinations_model', 'tickets_model','api_balance_log_model','wallet_model','wallet_trans_model','wallet_topup_model','users_model','wallet_trans_log_model','common_model','commission_calculation_model'));
           # wallet_topup_trans_model'));
    }

	public function test_mail() {
        $mailbody_array = array("subject" => "testing mail",
                            "message" => "this test mail",
                            "to" => 'lilawati.bookonspot@gmail.com',
                            "cc" => array('lilawati.bookonspot@gmail.com'),
                            //"bcc" => array('lilawati.karale@gmail.com')
                        );                      
                        
        $mail_result = send_mail($mailbody_array);
    }
    
}

?>