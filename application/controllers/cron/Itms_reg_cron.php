<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Itms_reg_cron extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('smartcard_helper'));
        $this->load->model(array('Mainmodel', 'Customer_registration_model', 'Utility_model', 'Passfare_model'));
        $this->apiCred = json_decode(SMART_CARD_CREDS, true);
        $header_value = $this->apiCred['X_API_KEY'];
        $this->header = array(
            'X-API-KEY:' . $header_value
        );

        $this->basicAuth = array(
            'username' => $this->apiCred['username'],
            'password' => $this->apiCred['password']
        );
        $log_data = array();
        $log_data['url'] = current_url();
        $log_data['Request_type'] = $_SERVER['REQUEST_METHOD'];
        $log_data['get'] = $this->input->get();
        $log_data['post'] = $this->input->post();
        $log_data['header'] = getallheaders();
        $log_data['user'] = $this->input->server('PHP_AUTH_USER');
        $log_data['ip'] = $this->input->ip_address();
        // show($log_data);
        log_data('rest/smartcard_cron_' . date('d-m-Y') . '.log', $log_data, 'api');
    }

    public function get_reg_list() {
        $URL = $this->apiCred['url'] . '/Common_api/get_registration_Data';
      //  $itms_request['custCardId'] = '01185585629971328';
      // $itms_request['custCardId'] = '01202262987981696';
       // $itms_request['date'] = '2019-05-03';
 	  //$itms_request['date'] = date('Y-m-d');
	//  $itms_request['from'] =  date('Y-m-d'); //'2019-03-15';
      //$itms_request['to']   =  date('Y-m-d') ; ///'2019-03-16';
	  
	  $itms_request['from'] = date('Y-m-d'.' 00:00:00',strtotime("-1 days"));       
      $itms_request['to'] = date('Y-m-d').' 23:59:59';
	  
      $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
      $array_json = json_decode($data);
	  
      log_data('rest/smartcard/itms_reg_api_' . date('d-m-Y') . '.log', $array_json, 'api');

        if (strtolower($array_json->status) == "success") {

            foreach ($array_json->data as $key => $json1) {
                //echo "</br>Key = ".$key;
                //echo "<pre>";
                //print_r($json1);
                //echo "</pre>";die();
                foreach ($json1 as $key2 => $json) {
                    //echo "</br>--------------------------</br>Key2 = ".$key2;
                    //echo "<pre>";
                    //print_r($json2);
                    //echo "</pre>";
                    //echo "</br>cust_card_id = ".$json2->cust_card_id;
                    $uni_query = "select ccm.cust_card_id from ps_customer_card_master ccm
			inner join ps_customer_master cm on cm.id=ccm.customer_id
			inner join ps_card_transaction ct on ct.trimax_card_id = ccm.trimax_card_id
			inner join ps_card_transaction_details ctd on ctd.card_transaction_id = ct.id 
			inner join inventory inv on inv.trimax_card_id= ccm.trimax_card_id
			inner join ps_master_card psm on psm.trimax_card_id= ccm.trimax_card_id
			where ccm.cust_card_id='" . $json->cust_card_id . "'";
                    $result = $this->db->query($uni_query);
                    //print_r($result->result_array());      


                    if (empty($result->result_array())) {

                   
                        if ($key2 == 0) {

                            //print_r($json); 
                            $cust_data = array(
                                'prefix' => $json->prefix,
                                'first_name' => $json->first_name,
                                'middle_name' => isset($json->middle_name) ? $json->middle_name : '',
                                'last_name' => $json->last_name,
                                'first_name_lang' => isset($json->first_name_lang) ? $json->first_name_lang : '',
                                'middle_name_lang' => isset($json->middle_name_lang) ? $json->middle_name_lang : '',
                                'last_name_lang' => isset($json->last_name_lang) ? $json->last_name_lang : '',
                                'telphone_number' => isset($json->telephone_number) ? $json->telephone_number : '',
                                'mobile_number' => $json->mobile_number,
                                'address' => $json->address,
                                'pincode' => $json->pincode,
                                'state_id' => $json->state_id,
                                'district_id' => $json->district_id,
                                'taluka_id' => $json->taluka_id,
                                'city_id' => $json->city_id,
                                'country_id' => 1,
                                'email' => $json->email,
                                'gender' => $json->gender,
                                'date_of_birth' => $json->date_of_birth,
                                // 'uid_number' => $json->uid_number,
                                'uid_number' => isset($json->uid_number) ? $json->uid_number : '',
                                'nationality_id' => 1,
                                'occupation_id' => isset($json->occupation_id) ? $json->occupation_id : STUDENT_OCCUPATION,
                                'occupation_name' => isset($json->ocuupation_name) ? $json->ocuupation_name : '',
                                'created_by' => '',
                                'msrtc_card_id' => $json->trimax_card_id, // need to changess
                                'is_active' =>'1', //$json->is_active,
                                'is_itms_reg' => '1',
				'is_itms_reg_transaction'=>'0'
                            );
                            /* echo "</br>cust_data = ";
                              echo "<pre>";
                              print_r($cust_data);
                              echo "</pre>";
                              //die();/* */

                            $customer_master_id = $this->Customer_registration_model->insert_itms_cust_reg_data($cust_data, CUSTOMER_MASTER);
                            //  $customer_master_id = '2861';
                                
                            $master_card_data = array(
                                'card_hex_uid' => isset($json->card_hex_uid) ? $json->card_hex_uid : '',
                                'trimax_card_id' => $json->trimax_card_id,
                                'cust_card_id' => $json->cust_card_id,
                                'display_card_id' => isset($json->trimax_card_id) ? $json->trimax_card_id : '', // $json->display_card_id,
                                'card_status' => $json->card_status,
                                'card_category_cd' => $json->card_category_cd,
                                'customer_id' => $customer_master_id,
                                'is_whitelisted' => isset($json->is_whitelisted) ? $json->is_whitelisted : '', 
                                'created_date' => $json->created_date,
                                'created_by' => '',
                                // 'modified_date' => $json->modified_date,
                                'modified_by' => '',
                                'is_active' => '1',
                                'is_itms_reg' => '1',
				                 'is_itms_reg_transaction'=>'0'
                            );
                            /* echo "</br>master_card_data = ";
                              echo "<pre>";
                              print_r($master_card_data);
                              echo "</pre>";
                              //die();/* */
                            $master_card_data_id= $this->Customer_registration_model->insert_itms_cust_reg_data($master_card_data, 'ps_master_card');

                            
                            $cust_card_data = array(
                                'customer_id' => $customer_master_id,
                                'registration_depo_cd' => $json->registration_depo_cd,
                                'collection_depo_cd' => $json->collection_depo_cd,
                                'name_on_card' => !empty($json->name_on_card) ? $json->name_on_card : $json->first_name . ' ' . $json->middle_name . ' ' . $json->last_name,
                                //'name_on_card_lang' => isset($json->name_on_card_lang) ? $json->name_on_card_lang': '',
                                'name_on_card_lang' => !empty($json->name_on_card_lang) ? $json->name_on_card_lang : $json->first_name_lang . ' ' . $json->middle_name_lang . ' ' . $json->last_name_lang,
                                'screen_code' => isset($json->screen_code) ? $json->screen_code : '',
                                'proof_type' => isset($json->proof_type) ? $json->proof_type : '',
                                'proof_name' => '', //need from itms
                                'wallet_proof_type' => $json->wallet_proof_type,
                                'wallet_proof_type_name' => $json->proof_name,
                                'wallet_proof_ref_no' => $json->wallet_proof_ref_no,
                                'trimax_card_id' => $json->trimax_card_id,
                                'display_card_id' => $json->trimax_card_id,
                                'master_card_id' => $master_card_data_id,
                                'tw_created_status' => $json->tw_created_status,
                                'sw_created_status' => $json->sw_created_status,
                                'dispatch_date' => $json->dispatch_date,
                                'card_validity_date' => $json->card_validity_date,
                                'card_expiry_date' => $json->card_expiry_date,
                                'proof_ref_number' => $json->proof_ref_number,
                                'card_category_cd' => $json->card_category_cd,
                                'cust_card_id' => $json->cust_card_id,
                                'dispatch_date' => $json->dispatch_date,
                                'created_date' => $json->created_date,
                                //'modified_date' => '',
                                // 'modified_by' => ($json->pass_category_id== OTC_CARD) ? 'A' : 'I',
                                'card_status' => $json->card_status,
                                'created_by' => '',
                                'is_active' => '1'
                            );
                            /* echo "</br>cust_card_data = ";
                              echo "<pre>";
                              print_r($cust_card_data);
                              echo "</pre>";
                              //die();/* */

                            $customer_card_master_id = $this->Customer_registration_model->insert_itms_cust_reg_data($cust_card_data, CUSTOMER_CARD_MASTER);
                            // $customer_card_master_id = '2883';
                            $card_trans_data = array('trimax_card_id' => $json->trimax_card_id,
                                'pass_type_id' => $json->pass_type_id,
                                'pass_concession_id' => $json->pass_concession_id,
                                'concession_cd' => $json->concession_cd,
                                'concession_nm' => $json->concession_nm,
                                'kms_limit' => $json->kms_limit,
                                'transaction_type_code' => $json->ctrans_type_code,
                                'span_name' => $json->span_name,
                                'span_period_days' => $json->span_period_days,
                                'pass_type_id' => $json->pass_type_id,
                                'pass_concession_id' => $json->pass_concession_id,
                                'pass_category_id' => $json->pass_category_id,
                                'activation_date' => $json->activation_date,
                                'expiry_date' => $json->expiry_date,
                                'transaction_type_id' => $json->transaction_type_id,
                                'write_flag' => '1',//$json->write_flag,
                                'depot_cd' => $json->depot_cd,
                                'terminal_id' => $json->terminal_id,
                                'receipt_number' => $json->receipt_number,
                                'dedicated_trip_id' => $json->dedicated_trip_id,
                                'span_id' => $json->span_id,
                                'bus_type_cd' => $json->bus_type_cd,
                                'conc_service_rel_id' => $json->conc_service_rel_id,
                                'card_fee_master_id' => $json->card_fee_master_id,
                                'opening_balance' => $json->opening_balance,
                                'closing_balance' => $json->closing_balance,
                                'amount' => $json->amount,
                                'online_renewal_status' => $json->online_renewal_status,
                                'session_id' => $json->session_id,
                                'vendor_id' => $json->vendor_id,
                                'request_id' => $json->request_id,
                                'txn_ref_no' => $json->txn_ref_no,
                                'pos_request_id' => $json->pos_request_id,
                                'pos_txn_ref_no' => $json->pos_txn_ref_no,
                                'created_date' => $json->created_date,
                                'created_by' => '',
                                //'modified_date'=> $json->pos_txn_ref_no, 
                                //'modified_by'=> $json->pos_txn_ref_no, 
                                'is_active' => '1',
                            );
                            /*   echo "</br>card_trans_data = ";
                              echo "<pre>";
                              print_r($card_trans_data);
                              echo "</pre>";
                              die(); */
                            $card_trans_id = $this->Customer_registration_model->insert_itms_cust_reg_data($card_trans_data, CARD_TRANS);
echo "</br>card_trans_id = ".$card_trans_id;
                            //  $card_trans_id = '4641';

                            


                            $inventory_data = array('user_id' =>'',
                                'card_no' => $json->trimax_card_id,
                                'trimax_card_id' => $json->trimax_card_id,
                                'cust_card_id' => $json->cust_card_id,
                                'status' => $json->card_status,
                                'card_type' => $json->card_category_cd , //$json->card_type,
                                'location_type' => '',
                                'location_at' => '',
                                'created_by' => '',
                                'updated_by' => '',
                                'created_date' => $json->created_date,
                                'updated_date' => '',
                                'is_active' => '1'
                            );
                            /* echo "</br>inventory_data = ";
                              echo "<pre>";
                              print_r($inventory_data);
                              echo "</pre>";
                              //die();/* */
                            $inventory_details_id = $this->Customer_registration_model->insert_itms_cust_reg_data($inventory_data, 'inventory');



                            $card_trans_detail_data = array(
                                'card_transaction_id' => $card_trans_id,
                                'trimax_card_id' => $json->trimax_card_id,
                                'card_fee_master_id' => $json->card_fee_master_id,
                                'route_no' => $json->route_no,
                                'route_type' => $json->route_type,
                                'fare_type_cd' => $json->fare_type_cd,
                                'service_number' => $json->service_number,
                                'from_stop_code' => $json->from_stop_code,
                                'till_stop_code' => $json->till_stop_code,
                                'via_stop_code' => $json->via_stop_code,
                                'stages' => $json->stages,
                                'opening_balance' => $json->opening_balance,
                                'closing_balance' => $json->closing_balance,
                                'least_km' => $json->least_km,
                                'rounded_km' => $json->rounded_km,
                                'per_day_base_fare' => $json->per_day_base_fare,
                                'total_base_fare' => $json->total_base_fare,
                                'total_asn_amount' => $json->total_asn_amount,
                                'net_fare_amount' => $json->net_fare_amount,
                                'fare_before_rounding' => $json->fare_before_rounding,
                                'fare_rounding_difference' => $json->fare_rounding_difference,
                                'amount' => $json->amount,
                                'reimbursement_amount' => $json->reimbursement_amount,
                                'session_id' => '',
                                'created_date' => $json->created_date,
                                'created_by' => '',
                                'is_active' => '1',
                            );
                            /* echo "</br>card_trans_detail_data = ";
                              echo "<pre>";
                              print_r($card_trans_detail_data);
                              echo "</pre>";
                              //die();/* */
                            $card_trans_details_id = $this->Customer_registration_model->insert_itms_cust_reg_data($card_trans_detail_data, CARD_TRANS_DETAILS);
                        }//if
                        //perform transdetails table entry
                    }//if check cust_card_id exist
                    if ($key2 > 0) {
                        $card_trans_detail_data = array(
                            'card_transaction_id' => $card_trans_id,
                            'trimax_card_id' => $json->trimax_card_id,
                            'card_fee_master_id' => $json->card_fee_master_id,
                            'route_no' => $json->route_no,
                            'route_type' => $json->route_type,
                            'fare_type_cd' => $json->fare_type_cd,
                            'service_number' => $json->service_number,
                            'from_stop_code' => $json->from_stop_code,
                            'till_stop_code' => $json->till_stop_code,
                            'via_stop_code' => $json->via_stop_code,
                            'stages' => $json->stages,
                            'opening_balance' => $json->opening_balance,
                            'closing_balance' => $json->closing_balance,
                            'least_km' => $json->least_km,
                            'rounded_km' => $json->rounded_km,
                            'per_day_base_fare' => $json->per_day_base_fare,
                            'total_base_fare' => $json->total_base_fare,
                            'total_asn_amount' => $json->total_asn_amount,
                            'net_fare_amount' => $json->net_fare_amount,
                            'fare_before_rounding' => $json->fare_before_rounding,
                            'fare_rounding_difference' => $json->fare_rounding_difference,
                            'amount' => $json->amount,
                            'reimbursement_amount' => $json->reimbursement_amount,
                            'session_id' => '',
                            'created_date' => $json->created_date,
                            'created_by' => '',
                            'is_active' => $json->is_active,
                        );
                        /* echo "</br>card_trans_detail_data = ";
                          echo "<pre>";
                          print_r($card_trans_detail_data);
                          echo "</pre>";
                          //die();/* */
                        $card_trans_details_id = $this->Customer_registration_model->insert_itms_cust_reg_data($card_trans_detail_data, CARD_TRANS_DETAILS);
                    }//else if
                    //}//forach
                    //}//if
                }//forach
            }//foreach
            die();





















            /*    foreach ($array_json->data as $key => $json) {

              //print_r($json->prefix); die;
              $cust_data = array(
              'prefix' => $json->prefix,
              'first_name' => $json->first_name,
              'middle_name' => isset($json->middle_name) ? $json->middle_name : '',
              'last_name' => $json->last_name,
              'first_name_lang' => isset($json->first_name_lang) ? $json->first_name_lang : '',
              'middle_name_lang' => isset($json->middle_name_lang) ? $json->middle_name_lang : '',
              'last_name_lang' => isset($json->last_name_lang) ? $json->last_name_lang : '',
              'telphone_number' => isset($json->telephone_number) ? $json->telephone_number : '',
              'mobile_number' => $json->mobile_number,
              'address' => $json->address,
              'pincode' => $json->pincode,
              'state_id' => $json->state_id,
              'district_id' => $json->district_id,
              'taluka_id' => $json->taluka_id,
              'city_id' => $json->city_id,
              'country_id' => 1,
              'email' => $json->email,
              'gender' => $json->gender,
              'date_of_birth' => $json->date_of_birth,
              // 'uid_number' => $json->uid_number,
              'uid_number' => isset($json->uid_number) ? $json->uid_number : '',
              'nationality_id' => 1,
              'occupation_id' => isset($json->occupation_id) ? $json->occupation_id : STUDENT_OCCUPATION,
              'occupation_name' => isset($json->occupation_name) ? $json->occupation_name : '',
              'created_by' => '',
              'msrtc_card_id' => $json->trimax_card_id, // need to changess
              'is_active' => $json->is_active,
              'is_itms_reg' => '1'
              );

              //   echo 'idd' . $customer_master_id = $this->Customer_registration_model->insert_itms_cust_reg_data($cust_data, CUSTOMER_MASTER);
              $customer_master_id = '2861';
              $cust_card_data = array(
              'customer_id' => $customer_master_id,
              'registration_depo_cd' => $json->registration_depo_cd,
              'collection_depo_cd' => $json->collection_depo_cd,
              'name_on_card' => !empty($json->name_on_card) ? $json->name_on_card : $json->first_name . ' ' . $json->middle_name . ' ' . $json->last_name,
              //'name_on_card_lang' => isset($json->name_on_card_lang) ? $json->name_on_card_lang': '',
              'name_on_card_lang' => !empty($json->name_on_card_lang) ? $json->name_on_card_lang : $json->first_name_lang . ' ' . $json->middle_name_lang . ' ' . $json->last_name_lang,
              'screen_code' => isset($json->screen_code) ? $json->screen_code : '',
              'proof_type' => isset($json->proof_type) ? $json->proof_type : '',
              'proof_name' => '', //need from itms
              'wallet_proof_type' => $json->wallet_proof_type,
              // 'wallet_proof_type_name' => $json->wallet_proof_type_name,
              'wallet_proof_ref_no' => $json->wallet_proof_ref_no,
              'trimax_card_id' => $json->trimax_card_id,
              'display_card_id' => $json->display_card_id,
              'master_card_id' => $json->master_card_id,
              'tw_created_status' => $json->tw_created_status,
              'sw_created_status' => $json->sw_created_status,
              'dispatch_date' => $json->dispatch_date,
              'card_validity_date' => $json->card_validity_date,
              'card_expiry_date' => $json->card_expiry_date,
              'proof_ref_number' => $json->proof_ref_number,
              'card_category_cd' => $json->card_category_cd,
              'cust_card_id' => $json->cust_card_id,
              'dispatch_date' => $json->dispatch_date,
              'created_date' => $json->created_date,
              'modified_date' => $json->modified_date,
              // 'modified_by' => ($json->pass_category_id== OTC_CARD) ? 'A' : 'I',
              'card_status' => $json->card_status,
              'created_by' => '',
              'is_active' => $json->is_active
              );

              // echo  $customer_card_master_id = $this->Customer_registration_model->insert_itms_cust_reg_data($cust_card_data, CUSTOMER_CARD_MASTER);
              $customer_card_master_id = '2883';
              $card_trans_data = array('trimax_card_id' => $json->trimax_card_id,
              'pass_type_id' => $json->pass_type_id,
              'pass_concession_id' => $json->pass_concession_id,
              // 'concession_cd' => $json->concession_cd,
              //   'concession_nm' => $json->concession_nm,
              //    'kms_limit' => $json->kms_limit,
              //  'transaction_type_code' => $json->transaction_type_code,
              // 'span_name,' => $json->span_name,
              //    'span_period_days' => $json->span_period_days,
              'pass_type_id' => $json->pass_type_id,
              'pass_concession_id' => $json->pass_concession_id,
              'pass_category_id' => $json->pass_category_id,
              'activation_date' => $json->activation_date,
              'expiry_date' => $json->expiry_date,
              'transaction_type_id' => $json->transaction_type_id,
              'write_flag' => $json->write_flag,
              'depot_cd' => $json->depot_cd,
              'terminal_id' => $json->terminal_id,
              'receipt_number' => $json->receipt_number,
              'dedicated_trip_id' => $json->dedicated_trip_id,
              'span_id' => $json->span_id,
              'bus_type_cd' => $json->bus_type_cd,
              'conc_service_rel_id' => $json->conc_service_rel_id,
              'card_fee_master_id' => $json->card_fee_master_id,
              'opening_balance' => $json->opening_balance,
              'closing_balance' => $json->closing_balance,
              'amount' => $json->amount,
              'online_renewal_status' => $json->online_renewal_status,
              'session_id' => $json->session_id,
              'vendor_id' => $json->vendor_id,
              'request_id' => $json->request_id,
              'txn_ref_no' => $json->txn_ref_no,
              'pos_request_id' => $json->pos_request_id,
              'pos_txn_ref_no' => $json->pos_txn_ref_no,
              'created_date' => $json->created_date,
              'created_by' => '',
              //'modified_date'=> $json->pos_txn_ref_no,
              //'modified_by'=> $json->pos_txn_ref_no,
              'is_active' => $json->is_active,
              );
              // $card_trans_id = $this->Customer_registration_model->insert_itms_cust_reg_data($card_trans_data, CARD_TRANS);
              $card_trans_id = '4641';


              $card_trans_detail_data = array(
              'card_transaction_id' => $card_trans_id,
              'trimax_card_id' => $json->trimax_card_id,
              'card_fee_master_id' => $json->card_fee_master_id,
              'route_no' => $json->route_no,
              'route_type' => $json->route_type,
              'fare_type_cd' => $json->fare_type_cd,
              'service_number' => $json->service_number,
              'from_stop_code' => $json->from_stop_code,
              'till_stop_code' => $json->till_stop_code,
              'via_stop_code' => $json->via_stop_code,
              'stages'=> $json->stages,
              'opening_balance'=> $json->opening_balance,

              'closing_balance'=> $json->closing_balance,
              'least_km'=> $json->least_km,
              'rounded_km'=> $json->rounded_km,
              'per_day_base_fare'=> $json->per_day_base_fare,
              'total_base_fare'=> $json->total_base_fare,
              'total_asn_amount'=> $json->total_asn_amount,
              'net_fare_amount'=> $json->net_fare_amount,
              'fare_before_rounding'=> $json->fare_before_rounding,
              'fare_rounding_difference'=> $json->fare_rounding_difference,
              'amount' =>  $json->amount,
              'reimbursement_amount'=> $json->reimbursement_amount,
              'session_id' =>  '',
              'created_date'=> $json->created_date,
              'created_by' => '',
              'is_active' => $json->is_active,
              );
              $card_trans_details_id = $this->Customer_registration_model->insert_itms_cust_reg_data($card_trans_detail_data, CARD_TRANS_DETAILS);


              $master_card_data = array( 'card_hex_uid'=> $json->card_hex_uid,
              'trimax_card_id'=> $json->trimax_card_id,
              'cust_card_id'=> $json->cust_card_id,
              'display_card_id'=> $json->display_card_id,
              'card_status'=> $json->card_status,
              'card_category_cd'=> $json->card_category_cd,
              'customer_id'=> $json->customer_id,
              'is_whitelisted'=> $json->is_whitelisted,
              'created_date'=> $json->created_date,
              'created_by'=>'',
              'modified_date'=> $json->modified_date,
              'modified_by'=> '',
              'is_active'=> $json->is_active
              );
              $card_trans_details_id = $this->Customer_registration_model->insert_itms_cust_reg_data($master_card_data, 'ps_master_card');



              $inventory_data = array( 'user_id'=> $json->user_id,
              'card_no'=> $json->card_no,
              'trimax_card_id'=> $json->trimax_card_id,
              'cust_card_id'=> $json->cust_card_id,
              'status'=> $json->status,
              'card_type'=> $json->card_type,
              'location_type'=> $json->location_type,
              'location_at'=> $json->location_at,
              'created_by'=> '',
              'updated_by'=>'',
              'created_date'=> $json->created_date,
              'updated_date'=> '',
              'is_active'=> $json->is_active
              );
              $inventory_details_id = $this->Customer_registration_model->insert_itms_cust_reg_data($master_card_data, 'inventory');

              } */
        } else {
            echo 'data not found';
            exit();
        }
    }

}
?>



