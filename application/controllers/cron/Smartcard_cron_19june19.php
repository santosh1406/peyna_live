<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Smartcard_cron extends CI_Controller {
    public function __construct() { 
	    parent::__construct();
	    $this->load->helper(array('smartcard_helper'));
            
            $this->apiCred = json_decode(SMART_CARD_CREDS,true);
            $header_value = $this->apiCred['X_API_KEY'];
            $this->header = array(
                'X-API-KEY:'.$header_value   
            );

            $this->basicAuth = array(
            'username' => $this->apiCred['username'],
            'password' => $this->apiCred['password']
            );
            $log_data = array();
            $log_data['url'] = current_url();
            $log_data['Request_type'] = $_SERVER['REQUEST_METHOD'];
            $log_data['get'] = $this->input->get();
            $log_data['post'] = $this->input->post();
            $log_data['header'] = getallheaders();
            $log_data['user'] = $this->input->server('PHP_AUTH_USER');
            $log_data['ip'] = $this->input->ip_address();
            // show($log_data);
            log_data('rest/smartcard_cron_' . date('d-m-Y') . '.log', $log_data, 'api');
    }
    public function get_agent_list(){
        $service_id = '';
        $URL = CRON_URL."Common_api/genTerminalForRokad";
       
        $itms_request = array();
        $itms_request['version_id'] = 1.23;
        
        $this->db->select("id");
        $this->db->from("service_master");
        //$this->db->where("service_name","SmartCard");
        $this->db->where("id",SMART_CARD_SERVICE);
        $query = $this->db->get();
//        echo $this->db->last_query();
//        die;
        $service_id =  $query->row()->id;
        
        $this->db->select("u.id,rs.depot_code,rs.division_code,u.email,u.display_name");
        $this->db->from("users u");
        $this->db->join("retailer_service_mapping rs","rs.agent_id = u.id");
        $this->db->where(["service_id" => $service_id]);
        $where = "terminal_code IS NULL OR terminal_code=''"; 
		$this->db->where($where);  
       
	   $this->db->where(["service_id" => $service_id]);

       $rsquery = $this->db->get();
       // echo $this->db->last_query();
        $data =  $rsquery->result_array(); 
        //show($data,1);
//        echo '<pre>';
//        var_dump($data);
//        
//        die;
       
        $mail_replacement_array = array(); 
        
        if (!empty($data)) {    
            foreach ($data as $key => $value) {
                $result = '';
                $itms_request['rokad_user_id'] = $data[$key]["id"];
                $itms_request['depot_cd'] = $data[$key]["depot_code"];
               
                $response = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
                //echo "123";print_r($response);die;
                
                $terminal_res = json_decode($response, true);
                
                if ($terminal_res['status'] == "success") {
                    $terminal_data = array(
                        "terminal_id" => $terminal_res['data']['terminal_id'],
                        "terminal_code" => $terminal_res['data']['terminal_code']
                    );
                   
                    $this->db->where('agent_id', $data[$key]["id"]);
                    $this->db->where('service_id',$service_id); 
                    $this->db->where('status','Y');
                    $result = $this->db->update('retailer_service_mapping', $terminal_data);
                    show($result,1);
//                    print_r($terminal_res['data']['terminal_code']);
//                    die;
                    //added by sonali 
                    //send mail to agent when agent get terminal id
                    if($result) {
//                        echo $email = rokad_encrypt('sonali.trimax@gmail.com', $this->config->item("pos_encryption_key"));
//                        die;
                            $email = rokad_decrypt($data[$key]["email"], $this->config->item("pos_encryption_key"));

                    $a_mail_replacement_array = array(
                                        '{{username}}' => ucwords($data[$key]["display_name"]),
                                        "{{terminal_code}}" => $terminal_res['data']['terminal_code']
                                         );
       
                    $data['agent_mail_content'] = str_replace(array_keys($a_mail_replacement_array),array_values($a_mail_replacement_array),AGENT_TERMINAL_CODE);

                    $a_mailbody_array = array("subject" =>"New Terminal Code",
                              "message" => $data['agent_mail_content'],
                              "to" =>  $email
                              );
       
                    $mail_result = send_mail($a_mailbody_array);
                    
                     /*start citycash mail*/
//                    define('ENVIRONMENT', isset($_SERVER['CI_ENV']) ? $_SERVER['CI_ENV'] : 'development');
//                    echo ENVIRONMENT;
//                    die;
//                    
                    $mail_replacement_array[] = array(
                                        'username' => 'CityCash Team',
                                        'agent_code' => $data[$key]["id"],
                                        "agent_name" => ucwords($data[$key]["display_name"]),
                                        "terminal_code" => $terminal_res['data']['terminal_code'],
                                        "depot_code" => $data[$key]["depot_code"],
                                        "division_code" => $data[$key]["division_code"]
                                         );
//                    
                    /*end citycash mail*/                   
                    
                    }
                }
            }
            if (strpos(base_url(), 'localhost') !== false) {
            $environment_cron = 'UAT instance';
            }
            
            $data['citycash_mail_content'] = '';
            $data['citycash_mail_content'] .= '<table width="100%" border="1" style="border-collapse: collapse; border:1px solid #000; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:30px;">
                                <tr>
                                    <td>
                                        <table width="95%" border="0" style="margin:0 auto;">
                                            <tr></tr>
                                            <tr style="line-height:40px;">
                                                <td style="text-align:center;">
                                                    <span style="color:#ff940a; font-weight:bold; font-size:12px;"></span>
                                                    <span style="font-weight:bold; font-size:12px;">'.$environment_cron.' Terminal Code For Whitelist<br></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td><table width="95%" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:20px; margin:0 auto;">';            
           
            
            foreach($mail_replacement_array as $key => $value)
            {
                $data['citycash_mail_content'] .= '<tr>
                 <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Agent Code </span>'.$value['agent_code'].'</td> 
                 <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Agent Name </span>'.$value['agent_name'].'</td>
                 <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Terminal Code </span>'.$value['terminal_code'].'</td>
                 <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Depot code </span>'.$value['depot_code'].'</td>
                 <td valign="top"><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Division code </span>'.$value['division_code'].'</td>
                

                 </tr>';
            }
            $data['citycash_mail_content'] .='</table>
                                </td>
                                </tr>
                                </table>';

            $c_mailbody_array = array("subject" => "Terminal for whitelist",
                "message" => $data['citycash_mail_content'],
                "to" => json_decode(CITYCASH_EMAILS)
            );

            $mail_result = send_mail($c_mailbody_array);




            // $data[$key]['dob'] = rokad_decrypt($data[$key]['dob'], $this->pos_encryption_key);

            log_data('rest/smartcard_cron_' . date('d-m-Y') . '.log', $data, 'api');
            
            if ($result){
                //$this->response( array('status' => 'success', 'msg' => 'Success', 'data' => $data ), 200);
                echo 'Data Update Successfully';
                
            }
            else
            {
                //$this->response( array('status' => 'success', 'msg' => 'No records found' ), 200);
                echo 'Data not Update';
            }
        }
        else {
            echo 'No Records found';
        }
      exit();  
    }
}
?>

