<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Dcc_report_cron extends CI_Controller{
    public function __construct() {
        parent::__construct();
        

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);
        
        $this->load->library(array('Excel'));
        $this->load->helper("url");
        
    }
    public function index(){
        $y_date = date('Y-m-d'.' 00:00:00',strtotime("-1 days"));
       
        $today_date = date('Y-m-d').' 23:59:59';
        
        $result_data  = $this->getCardIssuanceDetails($y_date, $today_date);
        
        if (!empty($result_data)) {
            $sr_no = 1;
            $data = array();
            foreach ($result_data as $key => $value) {
                switch ($value['card_status']) {
    case "I":
        $status = "Initiated when user register";
        break;
    case "P":
        $status = "Pending";
        break;
    case "T":
        $status = "Transport from region to depot";
        break;
    case "A":
        $status = "At depot writing the card";
        break;
    case "Y":
        $status = "Available for dispatch";
        break;
    case "D":
        $status = "Dispatched";
        break;
    case "L":
        $status = "Card Loss";
        break;
    case "F":
        $status = "Card Faulty";
        break;
    case "C":
        $status = "Cancelled by user";
        break;
    case "R":
        $status = "Rejected";
        break;
    case "S":
        $status = "Surrender";
        break;
    case "B":
        $status = "Block";
        break;
    case "X":
        $status = "Card not updated at CC";
        break;
    default:
        $status = "None";
}
                
                $data[] = array('Sr. No.' => $sr_no,
                    'Transaction Id' => $value['Transaction Id'],
                    'Trimax Card Id' => $value['Trimax Card Id'],
                    'Smart Card ID' => $value['Smart Card ID'],
                    'Card Type' => $value['Card Type'],
                    'SW expiry' => $value['SW expiry'],
                    'TW expiry' => $value['TW expiry'],
                    'SW Bal' => $value['SW Bal'],
                    'TW Bal' => $value['TW Bal'],
                    'Date' => $value['Date'],
                    'Status' => $status
                );
                $sr_no++;
            }
//        echo '<pre>';
//        print_r($value);
//        die;
//        $this->excel->data_2_excel('micro_atm_transaction_report_' . time() . '.xls', $data);
//        die;
            $this->excel->data_save_in_excel('issuance_report' . date('Y-m-d') . '.xls', $data);
            $replacements = array();

            $filename = 'issuance_report' . date('Y-m-d') . '.xls';
            $subject = 'Issuance Report.';
            $msg = "Issuance Report";
            $attachment = FCPATH . "cron_ors_report/$filename";
            $mail_array = array("subject" => $subject,
                "message" => $msg,
                "to" => json_decode(DCC_REPORT_CRON),
                "attachment" => $attachment
            );

            $result = send_mail($mail_array, $replacements);
            echo 'Cron Run Successfully';
            die;
        } else {
            echo 'Data not found';
            die;
        }
    }
     
    public function getCardIssuanceDetails($y_date, $today_date){
        $sql = 'SELECT cid.cc_txn_id AS "Transaction Id",
                cid.trimax_id AS "Trimax Card Id",
                cid.cust_card_id AS "Smart Card ID",
                cid.flag AS "Card Type",
                cid.sw_expiry AS "SW expiry",
                cid.tw_expiry AS "TW expiry",
                cid.sw_bal AS "SW Bal",
                cid.tw_bal AS "TW Bal",
                cid.created_date AS "Date",
                ccd.card_status
         FROM ps_card_issuance_details cid
        join ps_customer_card_master ccd on cid.trimax_id  = ccd.trimax_card_id
         WHERE cid.created_date BETWEEN "'.$y_date.'" AND "'.$today_date.'"
         ORDER BY cid.created_date';
//        $this->db->query($sql);
//              last_query(1);
//               die;
       
       return $this->db->query($sql)->result_array();

    }
    
    public function transaction_details(){
        $yest_date = date('Y-m-d'.' 00:00:00',strtotime("-1 days"));       
        $today_date = date('Y-m-d').' 23:59:59';
        
        $result_data  = $this->getTransactionDetails($yest_date, $today_date);
//        echo '<pre>';
//        print_r($result_data);
//        die;
        
        if (!empty($result_data)) {
            $sr_no = 1;
            $data = array();
            foreach ($result_data as $key => $value) {
                $data[] = array('Sr. No.' => $sr_no,
                    'Name' => $value['Name'],
                    'Contact Number' => $value['Contact Number'],
                    'Card Category' => $value['Card Category'],
                    'Trimax Card Id' => $value['Trimax Card Id'],
                    'Smart Card ID' => $value['Smart Card ID'],
                    'Transaction Type' => $value['Transaction Type'],
                    'Transaction Mode' => $value['Transaction Mode'],
                    'Receipt Number' => $value['Receipt Number'],
                    'Transaction Date' => $value['Transaction Date'],
                    'Issuance Date' => $value['Issuance Date'],
                    'Pass/Concession Type' => $value['Pass/Concession Type'],
                    'Pass Validity' => $value['Pass Validity'],
                    'Total Amount' => $value['Total Amount'],
                    'Pass Amount' => $value['Pass Amount'],
                    'Smart Card Fee' => $value['Smart Card Fee'],
                    'Application Fee' => $value['Application Fee'],
                    'GST' => $value['GST'],
                    'Loading Fee' => $value['Loading Fee'],
                    'BC Charge' => $value['BC Charge'],
                    'e-Purse Maintanence Fee' => $value['e-Purse Maintanence Fee'],
                    'Card Fee' => $value['Card Fee'],
                    'Bonus Amt' => $value['Bonus Amt'],
                    'Request ID' => $value['Request ID'],
                    'Transaction Ref No.' => $value['Transaction Ref No.'],
                    'Depot code' => $value['Depot code'],
                    'Terminal code' => $value['Terminal code'],
                    'Login ID' => $value['Login ID'],
                    'Pincode' => $value['Pincode'],
                    
                );
                $sr_no++;
            }
//        echo '<pre>';
//        print_r($value);
//        die;
//        $this->excel->data_2_excel('micro_atm_transaction_report_' . time() . '.xls', $data);
//        die;
            $this->excel->data_save_in_excel('transaction_report' . date('Y-m-d') . '.xls', $data);
            $replacements = array();

            $filename = 'transaction_report' . date('Y-m-d') . '.xls';
            $subject = 'Transaction Report.';
            $msg = "Transaction Report";
            $attachment = FCPATH . "cron_ors_report/$filename";
            $mail_array = array("subject" => $subject,
                "message" => $msg,
                "to" => json_decode(DCC_REPORT_CRON),
                "attachment" => $attachment
            );

            $result = send_mail($mail_array, $replacements);
            echo 'Cron Run Successfully';
            die;
        } else {
            echo 'Data not found';
            die;
        }
    }
    
    public function getTransactionDetails($y_date, $today_date){
        
        $sql = 'SELECT pccm.name_on_card AS "Name",
                pcm.mobile_number AS "Contact Number",
                category_name AS "Card Category",
                pccm.trimax_card_id AS "Trimax Card Id",
                pccm.cust_card_id AS "Smart Card ID",
                pmc.card_category_cd "Card Category",
                pct.transaction_type_code AS "Transaction Type",
                "Cash" AS "Transaction Mode",
                pct.receipt_number AS "Receipt Number",
                pct.created_date AS "Transaction Date",
                pccm.dispatch_date AS "Issuance Date",
                pct.concession_nm AS "Pass/Concession Type",
                pct.span_name AS "Pass Validity",
                total_amount AS "Total Amount",
                max(CASE 
                        WHEN (patd.amount_code = "BF"
                              OR patd.amount_code = "PassCharge") THEN patd.amount
                        ELSE 0.00
                    END) AS "Pass Amount",
                max(CASE
                        WHEN patd.amount_code = "SCF" THEN patd.amount
                        ELSE 0.00
                    END) AS "Smart Card Fee",
                max(CASE
                        WHEN patd.amount_code = "AF" THEN patd.amount
                        ELSE 0.00
                    END) AS "Application Fee",
                max(CASE
                        WHEN patd.amount_code = "GST" THEN patd.amount
                        ELSE 0.00
                    END) AS "GST",
                max(CASE
                        WHEN patd.amount_code = "LoadingFee" THEN patd.amount
                        ELSE 0.00
                    END) AS "Loading Fee",
                max(CASE
                        WHEN patd.amount_code = "BCCharges" THEN patd.amount
                        ELSE 0.00
                    END) AS "BC Charge",
                max(CASE
                        WHEN patd.amount_code = "EPurseFees" THEN patd.amount
                        ELSE 0.00
                    END) AS "e-Purse Maintanence Fee",
                max(CASE
                        WHEN patd.amount_code = "CardFee" THEN patd.amount
                        ELSE 0.00
                    END) AS "Card Fee",
                max(CASE
                        WHEN patd.amount_code = "BonusAmt" THEN patd.amount
                        ELSE 0.00
                    END) AS "Bonus Amt",
                pct.request_id    AS "Request ID",
                pct.txn_ref_no AS "Transaction Ref No.",
                at.depot_code AS "Depot code",
                at.terminal_code AS "Terminal code",
                username AS "Login ID",
                pcm.pincode AS "Pincode"
         FROM ps_customer_master pcm
         LEFT OUTER JOIN ps_customer_card_master pccm ON pcm.id = pccm.customer_id
         INNER JOIN ps_master_card pmc ON pccm.master_card_id = pmc.id
         INNER JOIN ps_card_transaction pct ON pccm.trimax_card_id = pct.trimax_card_id
         INNER JOIN ps_card_transaction_details pctd ON pct.id = pctd.card_transaction_id
         INNER JOIN ps_amount_transaction pat ON pct.id = pat.card_transaction_id
         INNER JOIN ps_amount_transaction_details patd ON patd.amount_transaction_id = pat.id
         INNER JOIN ps_pass_category ppc ON ppc.id = pass_category_id
         INNER JOIN users au ON pct.created_by = au.id
         INNER JOIN retailer_service_mapping at ON pct.terminal_id = at.terminal_id
         WHERE 1=1
         and tw_created_status = "C"
         AND pct.created_date BETWEEN "'.$y_date.'" AND "'.$today_date.'"
         AND pct.is_active = "1" and at.service_id="'.SMART_CARD_SERVICE.'" and at.status="Y"
        GROUP BY patd.amount_transaction_id
        ORDER BY pct.transaction_type_id,
         pct.created_date';
                
        
//        $this->db->query($sql);
//        last_query(1);
//        die;
       
       return $this->db->query($sql)->result_array();
    }
    
    public function wallet_claim(){
        $yest_date = date('Y-m-d'.' 00:00:00',strtotime("-1 days"));       
        $today_date = date('Y-m-d').' 23:59:59';
        
        $result_data  = $this->getWalletClaimDetails($yest_date, $today_date);
//        echo '<pre>';
//        print_r($result_data);
//        die;
        
        if (!empty($result_data)) {
            $sr_no = 1;
            $data = array();
            foreach ($result_data as $key => $value) {
                $data[] = array('Sr. No.' => $sr_no,                    
                    'Transaction Id' => $value['Transaction Id'],
                    'Trimax Card Id' => $value['Trimax Card Id'],
                    'Smart Card ID' => $value['Smart Card ID'],
                    'Mobile No' => $value['Mobile No'],
                    'Opening Bal' => $value['Opening Bal'],
                    'Top Up Amt' => $value['Top Up Amt'],
                    'Closing Bal' => $value['Closing Bal'],
                    'Expiry Date' => $value['Expiry Date'],
                    'Topup Flag' => $value['Topup Flag'],
                    'Date' => $value['Date'],
                    
                    
                );
                $sr_no++;
            }
//        echo '<pre>';
//        print_r($value);
//        die;
//        $this->excel->data_2_excel('micro_atm_transaction_report_' . time() . '.xls', $data);
//        die;
            $this->excel->data_save_in_excel('wallet_claim_report' . date('Y-m-d') . '.xls', $data);
            $replacements = array();

            $filename = 'wallet_claim_report' . date('Y-m-d') . '.xls';
            $subject = 'Wallet Claim Report.';
            $msg = "Wallet Claim Report";
            $attachment = FCPATH . "cron_ors_report/$filename";
            $mail_array = array("subject" => $subject,
                "message" => $msg,
                "to" => json_decode(DCC_REPORT_CRON),
                "attachment" => $attachment
            );

            $result = send_mail($mail_array, $replacements);
            echo 'Cron Run Successfully';
            die;
        } else {
            echo 'Data not found';
            die;
        }
    }

    public function getWalletClaimDetails($y_date, $today_date){
        $sql = 'SELECT transaction_id AS "Transaction Id",
                trimax_card_id AS "Trimax Card Id",
                pccm.cust_card_id  AS "Smart Card ID",
                mobile_number AS "Mobile No",
                opening_balance AS "Opening Bal",
                amount AS "Top Up Amt",
                closing_balance AS "Closing Bal",
                expiry_date AS "Expiry Date",
                topup_flag AS "Topup Flag",
                pswd.created_date AS "Date"
         FROM ps_claim_wallet_details pswd
         INNER JOIN ps_customer_card_master pccm ON pswd.cust_card_id = pccm.cust_card_id
         WHERE pswd.created_date BETWEEN "'.$y_date.'" AND "'.$today_date.'"
         ORDER BY topup_flag,
                  pswd.created_date;';
        
        /* $this->db->query($sql);
        echo $this->db->last_query();
        die;*/
       
       return $this->db->query($sql)->result_array();
    }
}
