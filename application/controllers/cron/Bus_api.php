<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Bus_api extends CI_Controller {

    public function __construct() {
        parent::__construct();

        ini_set('max_execution_time', 3000);
        ini_set("memory_limit",-1);
        
        $this->load->helper( array( 'soap_data' ) );
        $this->load->library(array("api_provider/etravelsmart_api","soap_api/upsrtc_api", "soap_api/msrtc_api", "soap_api/rsrtc_api"));
        $this->load->model(array('travel_destinations_model','tickets_model','trip_master_model'));        
    }
    /**
     * [daily0001 description] : for fetching UPSRTC available services using cron 
     * @return [type] [description]
     */
    function daily0001() {
        $this->cron_up_get_available_services();
        $date = date('d-m-Y H:i:s');
        echo $date;
        log_data('cron/detail.log', $date, 'Cron run on'.$date);
    }

    /**
     * [daily0001 description] : for daily fetch service of same day
     * @return [type] [description]
     */
    function daily0002() {
        $this->cron_up_get_available_services_of_same_day();
        $date = date('d-m-Y H:i:s');
        echo $date;
        log_data('cron/detail.log', $date, 'Cron run on'.$date);
    }

    /**
     * [daily0100 description] : for fetching MSRTC available services using cron 
     * @return [type] [description]
     */
    function daily0100() {
        $this->cron_msrtc_get_available_services();
        $date = date('d-m-Y H:i:s');
        log_data('cron/detail.log', $date, 'Cron run on'.$date);
    }

    /**
     * [daily0102 description] for daily fetch service of same day
     * @return [type] [description]
     */
    function daily0102() {
        $this->cron_msrtc_get_available_services_of_same_day();
        $date = date('d-m-Y H:i:s');
        log_data('cron/detail.log', $date, 'Cron run on'.$date);
    }
    /**
     * [daily0101 description] : for fetching MSRTC available services using cron 
     * @return [type] [description]
     */
    function daily0101() {
        $this->cron_rsrtc_get_available_services();
        $date = date('d-m-Y H:i:s');
        log_data('cron/detail.log', $date, 'Cron run on'.$date);
    }

    /**
     * [daily0101 description] for daily fetch service of same day
     * @return [type] [description]
     */
    function daily0103() {
        $this->cron_rsrtc_get_available_services();
        $date = date('d-m-Y H:i:s');
        log_data('cron/detail.log', $date, 'Cron run on'.$date);
    }

    function new_daily0100() {
        $this->cron_msrtc_latest_get_available_services();
        $date = date('d-m-Y H:i:s');
        echo $date;
        log_data('cron/detail.log', $date, 'Cron run on'.$date);
    }
    
    public function index() {
        $data = '';
        load_back_view(CRON_BUS, $data);
    }

    public function release_bus_service_wise_view() {
        $data = '';

        load_back_view(RELEASE_BUS_SERVICE_WISE, $data);
    }

    public function release_bus_service_wise() {
        $input = $this->input->post();
        if($input['operator'] == 2) {
            $this->upsrtc_api->getBusServiceStops_bus_service_wise($input['from'], $input['operator_service_id']);
        }
        else if($input['operator'] == 3) {
            $this->msrtc_api->getBusServiceStops_bus_service_wise($input['from'], $input['operator_service_id']);
        }
    }

    function release() {
        $date = date('d-m-y');
        show($date);
        $this->upsrtc_api->release( $date );
    }
    
    public function get_available_services(){
        $input = $this->input->post();

        if($input['operator'] == 1) {
            $this->rsrtc_get_available_services();
        }
        else if($input['operator'] == 2) {
            $this->up_get_available_services();
        }
        else if($input['operator'] == 3) {
            $this->msrtc_get_available_services();
        }
    }

    public function up_get_available_services()
    {
        $input = $this->input->post();
        
        $last_sync_date =  $this->travel_destinations_model->get_last_sync_date(2);

        if($this->input->post('from') && $this->input->post('till'))
        {
            $begin  = new DateTime( date('Y-m-d', strtotime($input['from'])) );
            $end    = new DateTime( date('Y-m-d', strtotime($input['till']."+1day")) );
        }

        $interval   = DateInterval::createFromDateString('1 day');

        $period     = new DatePeriod($begin, $interval, $end);

        foreach ( $period as $dt )
        {           
            $this->upsrtc_api->dep_date     = $dt->format( "d/m/Y" );
            $this->upsrtc_api->getAvailableServices();
            show('service Generated for date : '.$dt->format( "Y-m-d H:i:s" ) );
        }


        show('Done',1);

        // echo date('m-01-Y 00:00:00',strtotime('next month')) . '<br/>';
        // echo date('m-t-Y 12:59:59',strtotime('next month')) . '<br/>';

        $last_day_month  = date('t',strtotime('next month'));

        for ($i=7; $i <= 21; $i++) { 
            $d = str_pad($i, 2, '0', STR_PAD_LEFT);

            $this->upsrtc_api->dep_date     = date($d.'/05/Y');
            $this->upsrtc_api->getAvailableServices();            
        }

        show('Done',1);
    }

    public function msrtc_get_available_services()
    {
        $input = $this->input->post();
        
        $last_sync_date =  $this->travel_destinations_model->get_last_sync_date(3);

        if($this->input->post('from') && $this->input->post('till'))
        {
            $begin  = new DateTime( date('Y-m-d', strtotime($input['from'])) );
            $end    = new DateTime( date('Y-m-d', strtotime($input['till']."+1day")) );
        }

        $interval   = DateInterval::createFromDateString('1 day');

        $period     = new DatePeriod($begin, $interval, $end);


        foreach ( $period as $dt )
        {           
            $this->msrtc_api->dep_date     = $dt->format( "d/m/Y" );
            // $this->msrtc_api->latestGetAvailableServices();
            $this->msrtc_api->getAvailableServices();
            show('service Generated for date : '.$dt->format( "Y-m-d H:i:s" ) );
        }


        show('Done',1);

        // echo date('m-01-Y 00:00:00',strtotime('next month')) . '<br/>';
        // echo date('m-t-Y 12:59:59',strtotime('next month')) . '<br/>';

        $last_day_month  = date('t',strtotime('next month'));

        for ($i=7; $i <= 21; $i++) { 
            $d = str_pad($i, 2, '0', STR_PAD_LEFT);

            $this->msrtc_api->dep_date     = date($d.'/05/Y');
            $this->msrtc_api->getAvailableServices();            
        }

        show('Done',1);
    }

    public function rsrtc_get_available_services()
    {
        $input = $this->input->post();
        
        $last_sync_date =  $this->travel_destinations_model->get_last_sync_date($this->config->item("rsrtc_operator_id"));

        if($this->input->post('from') && $this->input->post('till'))
        {
            $begin  = new DateTime( date('Y-m-d', strtotime($input['from'])) );
            $end    = new DateTime( date('Y-m-d', strtotime($input['till']."+1day")) );
        }

        $interval   = DateInterval::createFromDateString('1 day');

        $period     = new DatePeriod($begin, $interval, $end);


        foreach ( $period as $dt )
        {           
            $this->rsrtc_api->dep_date     = $dt->format( "d/m/Y" );
            $this->rsrtc_api->getAvailableServices();
            show('service Generated for date : '.$dt->format( "Y-m-d H:i:s" ) );
        }


        show('Done',1);

        // echo date('m-01-Y 00:00:00',strtotime('next month')) . '<br/>';
        // echo date('m-t-Y 12:59:59',strtotime('next month')) . '<br/>';

        $last_day_month  = date('t',strtotime('next month'));

        for ($i=7; $i <= 21; $i++) { 
            $d = str_pad($i, 2, '0', STR_PAD_LEFT);

            $this->rsrtc_api->dep_date     = date($d.'/05/Y');
            $this->rsrtc_api->getAvailableServices();            
        }

        show('Done',1);
    }

    public function cron_up_get_available_services()
    {
        log_data('cron/detail.log', 'entered', 'cron_up_get_available_services');

        $last_sync_date =  $this->travel_destinations_model->get_last_sync_date(2);

        $last_sync_date = $last_sync_date[0]->sch_departure_date;
        
        
        $begin  = new DateTime( date('Y-m-d', strtotime($last_sync_date."+1day")) );
        $end    = new DateTime( date('Y-m-d', strtotime($last_sync_date."+3day")) );
        

        $interval   = DateInterval::createFromDateString('1 day');

        $period     = new DatePeriod($begin, $interval, $end);

        foreach ( $period as $dt )
        {           
            show($dt->format( "d/m/Y" ));
            $this->upsrtc_api->dep_date     = $dt->format( "d/m/Y" );

            $this->upsrtc_api->getAvailableServices();
            show('service Generated for date : '.$dt->format( "Y-m-d H:i:s" ) );
        }
        
        show('Done',1);
    }
    

    public function cron_msrtc_get_available_services()
    {
        log_data('cron/detail.log', 'entered', 'cron_msrtc_get_available_services');

        $last_sync_date =  $this->travel_destinations_model->get_last_sync_date(3);

        $last_sync_date = $last_sync_date[0]->sch_departure_date;
        
        
        $begin  = new DateTime( date('Y-m-d', strtotime($last_sync_date."+1day")) );
        $end    = new DateTime( date('Y-m-d', strtotime($last_sync_date."+2day")) );
        

        $interval   = DateInterval::createFromDateString('1 day');

        $period     = new DatePeriod($begin, $interval, $end);

        foreach ( $period as $dt )
        {           
            show($dt->format( "d/m/Y" ));
            $this->msrtc_api->dep_date     = $dt->format( "d/m/Y" );

            log_data('cron/time_calculation.log', date("Y-m-d H:i:s"), 'New api started');
            $this->msrtc_api->latestGetAvailableServices();
            log_data('cron/time_calculation.log', date("Y-m-d H:i:s"), 'New api Ended');

            $this->msrtc_api->dep_date     = $dt->format( "d/m/Y" );
            log_data('cron/time_calculation.log', date("Y-m-d H:i:s"), 'Old api Started');
            $this->msrtc_api->getAvailableServices();
            log_data('cron/time_calculation.log', date("Y-m-d H:i:s"), 'Old api Ended');

            show('service Generated for date : '.$dt->format( "Y-m-d H:i:s" ) );
        }
        
        show('Done',1);
    }

    public function cron_msrtc_latest_get_available_services()
    {
        log_data('cron/detail.log', 'entered', 'cron_msrtc_latest_get_available_services');

        $last_sync_date =  $this->travel_destinations_model->get_last_sync_date(3);

        $last_sync_date = $last_sync_date[0]->sch_departure_date;
        
        
        $begin  = new DateTime( date('Y-m-d', strtotime($last_sync_date."+1day")) );
        $end    = new DateTime( date('Y-m-d', strtotime($last_sync_date."+2day")) );
        

        $interval   = DateInterval::createFromDateString('1 day');

        $period     = new DatePeriod($begin, $interval, $end);

        foreach ( $period as $dt )
        {           
            show($dt->format( "d/m/Y" ));
            $this->msrtc_api->dep_date     = $dt->format( "d/m/Y" );

            $this->msrtc_api->LatestGetAvailableServices();
            show('service Generated for date : '.$dt->format( "Y-m-d H:i:s" ) );
        }
        
        show('Done',1);
    }
    
    public function cancel_upsrt_services()
    {
        ini_set('memory_limit', '-1');
        $tickets_to_be_canceled = [];
        $begin  = new DateTime('2016-08-28');
        $end    = new DateTime('2016-09-20');

        $interval   = DateInterval::createFromDateString('1 day');
        $period     = new DatePeriod($begin, $interval, $end);

        foreach($period as $dt)
        {
            $data = [];
            $dep_date = $dt->format( "Y-m-d");
            
            logging_data("check/".$dep_date.".log",$data,"Services");
        
            $data = $this->upsrtc_api->getCancelList(["date" => date("Y-m-d H:i:s", strtotime($dep_date))]);

            logging_data("cancelled_services/for_date_".$dep_date.".log",$data,$dep_date);
        
            if(isset($data["CancelListResponse"]["cancelledServices"]["service"]))
            {
                $services = $data["CancelListResponse"]["cancelledServices"]["service"];
                
                foreach ($services as $key => $value)
                {
                    $service_date   =   trim(explode('.0', $value["departuretime"])[0]);
                    $where_array    =   [
                                            "op_id" => $this->config->item("upsrtc_operator_id"),
                                            "op_trip_no" => $value["serviceId"],
                                            "sch_dept_time" => date("H:i:s",strtotime($service_date)),
                                            "route_no_name" => trim($value["routename"])
                                        ];
                    $trip_data = $this->trip_master_model->where($where_array)->find_all();

                    $delete_condition = [];

                    if($trip_data)
                    {
                        $delete_condition["trip_no"]            =   $trip_data[0]->trip_no;
                       /* $delete_condition["sch_departure_date"] =   date("Y-m-d",strtotime($service_date));
                        $delete_condition["sch_departure_tm"]   =   date("H:i:s",strtotime($service_date));*/
                        log_data("delete_service_data/".date("Y-m-d").".log",$delete_condition,"Deleted Services");
                        $this->db->delete('travel_destinations', $delete_condition);
                    }

                    /*To Check If canceled services tickets is booked or not*/
                    $services_ticket_where  =   [
                                                    "bus_service_no" => $value["serviceId"],
                                                    "dept_time" => date("Y-m-d H:i:s",strtotime($service_date))
                                                ];

                    $tickets_to_be_canceled[$value["serviceId"]] = $this->tickets_model->where($services_ticket_where)->find_all();
                }
            }
        }

        $message_to_be_sent = "";
        foreach ($tickets_to_be_canceled as $sid => $svalue)
        {
            foreach ($svalue as $key => $tvalue)
            {
                $message_to_be_sent .= "Service ID - ".$sid."<br/>";
                $message_to_be_sent .= "Ticket ID - ".$tvalue->ticket_id."<br/>";
                $message_to_be_sent .= "PNR Number - ".$tvalue->pnr_no."<br/>";
                $message_to_be_sent .= "Rokad Ref No - ".$tvalue->boss_ref_no."<br/>";
                $message_to_be_sent .= "From Stop Name - ".$tvalue->from_stop_name."<br/>";
                $message_to_be_sent .= "Till Stop Name - ".$tvalue->till_stop_name."<br/>";
                $message_to_be_sent .= "Dept Time - ".$tvalue->dept_time."<br/>";
                $message_to_be_sent .= "tot_fare_amt_with_tax - ".$tvalue->tot_fare_amt_with_tax."<br/>";
                $message_to_be_sent .= "------------------------------------------ <br/><br/><br/>";
            }
        }

        $mail_to_sent_message = ($message_to_be_sent != "") ? $message_to_be_sent : "No tickets";
        $custom_error_mail = array(
                                      "custom_error_subject" => "Tickets to be cancelled",
                                      "custom_error_message" => "Services get cancelled from UPSRTC end.<br/>The ticket shown below might get cancelled. Please Check. <br/>".$mail_to_sent_message
                                    );

        $this->developer_custom_error_mail($custom_error_mail);
        show(1,1);
    }

    /**
     * [cron_rsrtc_get_available_services : cron for the rsrtc service]
     */
    public function cron_rsrtc_get_available_services()
    {
        log_data('cron/detail.log', 'entered', 'cron_rsrtc_get_available_services');

        $last_sync_date =  $this->travel_destinations_model->get_last_sync_date(1);
        $last_sync_date = $last_sync_date[0]->sch_departure_date;
        
        $begin  = new DateTime( date('Y-m-d', strtotime($last_sync_date."+1day")) );
        $end    = new DateTime( date('Y-m-d', strtotime($last_sync_date."+3day")) );
        
        $interval   = DateInterval::createFromDateString('1 day');

        $period     = new DatePeriod($begin, $interval, $end);

        foreach ( $period as $dt )
        {           
            show($dt->format( "d/m/Y" ));
            $this->rsrtc_api->dep_date     = $dt->format( "d/m/Y" );

            $this->rsrtc_api->getAvailableServices();
            show('service Generated for date : '.$dt->format( "Y-m-d H:i:s" ) );
        }
        
        show('Done',1);
    }

    public function cron_up_get_available_services_of_same_day()
    {
        log_data('cron/detail.log', 'entered', 'cron_up_get_available_services');

        $last_sync_date = date('Y-m-d');
        
        
        $begin  = new DateTime( date('Y-m-d', strtotime($last_sync_date)) );
        $end    = new DateTime( date('Y-m-d', strtotime($last_sync_date."+3day")) );
        

        $interval   = DateInterval::createFromDateString('1 day');

        $period     = new DatePeriod($begin, $interval, $end);

        foreach ( $period as $dt )
        {           
            show($dt->format( "d/m/Y" ));
            $this->upsrtc_api->dep_date     = $dt->format( "d/m/Y" );

            $this->upsrtc_api->getAvailableServices();
            show('service Generated for date : '.$dt->format( "Y-m-d H:i:s" ) );
        }
        
        show('Done',1);
    }

    public function cron_msrtc_get_available_services_of_same_day()
    {
        log_data('cron/detail.log', 'entered', 'cron_msrtc_get_available_services');

        $last_sync_date = date('Y-m-d');

        $begin  = new DateTime( date('Y-m-d', strtotime($last_sync_date)) );
        $end    = new DateTime( date('Y-m-d', strtotime($last_sync_date."+3day")) );
        

        $interval   = DateInterval::createFromDateString('1 day');

        $period     = new DatePeriod($begin, $interval, $end);

        foreach ( $period as $dt )
        {           
            show($dt->format( "d/m/Y" ));
            $this->msrtc_api->dep_date     = $dt->format( "d/m/Y" );

            log_data('cron/time_calculation.log', date("Y-m-d H:i:s"), 'New api started');
            $this->msrtc_api->latestGetAvailableServices();
            log_data('cron/time_calculation.log', date("Y-m-d H:i:s"), 'New api Ended');

            $this->msrtc_api->dep_date     = $dt->format( "d/m/Y" );
            log_data('cron/time_calculation.log', date("Y-m-d H:i:s"), 'Old api Started');
            $this->msrtc_api->getAvailableServices();
            log_data('cron/time_calculation.log', date("Y-m-d H:i:s"), 'Old api Ended');

            show('service Generated for date : '.$dt->format( "Y-m-d H:i:s" ) );
        }
        
        show('Done',1);
    }

    public function cron_rsrtc_get_available_services_of_same_day()
    {
        log_data('cron/detail.log', 'entered', 'cron_rsrtc_get_available_services');

        $last_sync_date = date('Y-m-d');
        
        $begin  = new DateTime( date('Y-m-d', strtotime($last_sync_date)) );
        $end    = new DateTime( date('Y-m-d', strtotime($last_sync_date."+3day")) );
        
        $interval   = DateInterval::createFromDateString('1 day');

        $period     = new DatePeriod($begin, $interval, $end);

        foreach ( $period as $dt )
        {           
            show($dt->format( "d/m/Y" ));
            $this->rsrtc_api->dep_date     = $dt->format( "d/m/Y" );

            $this->rsrtc_api->getAvailableServices();
            show('service Generated for date : '.$dt->format( "Y-m-d H:i:s" ) );
        }
        
        show('Done',1);
    }

}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */