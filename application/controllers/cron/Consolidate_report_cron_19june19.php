<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Consolidate_report_cron extends CI_Controller{
    public function __construct() {
        parent::__construct();
        

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);
        
        $this->load->library(array('Excel'));
        $this->load->helper("url");
        
    }
    public function index(){
}
    public function all_data($end_date){
        $month = date('m', strtotime($end_date));
        
        $month_name = date('M', strtotime($end_date));
        
        $year = date('Y', strtotime($end_date));
        
        $start_date = $year.'-'.$month.'-'.'01';
        $end_date = $end_date.' 23:59:59';
        
       // $y_date = date('Y-m-d'.' 23:59:59',strtotime("-1 days"));
     // $y_date = $y_date.' 23:59:59';
        
        $result_data  = $this->getConsolidateDetails($start_date,$end_date);
        
        if (!empty($result_data)) {
            $sr_no = 1;
            $data = array();
            foreach ($result_data as $key => $value) {
                
                
                $data[] = array('Sr. No.' => $sr_no,
                    'Agent Code' => $value['agent_code'],
                    'Terminal Code' => $value['terminal_code'],
                    'Depot Code' => $value['depot_code'],
                    'Depot Name' => $value['DEPOT_NM'],
                    'Total Cards' => $value['total_cards'],
                    'Per Card Amount' => $value['per_card_Amount'],
                    'Total Amount' => $value['Total_Amount'],
                    'Order Date' => $value['Order_Date'],
                    
                );
                $sr_no++;
            }

            $this->excel->data_save_in_excel('consolidate_report_' . date('m-Y') . '.xls', $data);
            $replacements = array();
            
            
            ///
            
            $result_data_by_group  = $this->getConsolidateDetailsByGroup($start_date,$end_date);
        //show($result_data1,1);
        if (!empty($result_data_by_group)) {
            $sr_no = 1;
            $data = array();
            foreach ($result_data_by_group as $key => $value) {               
                
                $mail_replacement_array[] = array(
                    'sr_no' => $sr_no,
                    'Date' => date('Y-m-d', strtotime($value['Order_Date'])),
                    "No_of_OTC_cards" => $value['total_cards'],
                    "Total_amount_to_be_debited" => $value['Total_Amount'],
                    
                   
                );
                $sr_no++;               
                
            }

            
        }
            
            $data['consolidate_mail_content'] = '';
            $data['consolidate_mail_content'] .= '
                

Please find attached sheet & the details of productions OTC cards for '.$month_name.' '.$year.' <br><br><table width="100%" border="1" style="border-collapse: collapse; border:1px solid #000; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:30px;">
                                    

                                    <tr>
                                    <td>
                                    <thead>
                                        <tr>
                                            <th><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Sr.no </span></th>
                                            <th><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Date </span></th>
                                            <th><span style="color:#ff940a; text-decoration:underline; line-height:27px;">No. of OTC cards </span></th>
                                            <th><span style="color:#ff940a; text-decoration:underline; line-height:27px;">Total amount to be debited </span></th>
                                        </tr>
                                    </thead>
                                    ';            
           
            
            foreach($mail_replacement_array as $key => $value)
            {
                $data['consolidate_mail_content'] .= '<tbody><tr>
                 <td valign="top" style="text-align: center;">'.$value['sr_no'].'</td> 
                 <td valign="top" style="text-align: center;">'.$value['Date'].'</td>
                 <td valign="top" style="text-align: center;">'.$value['No_of_OTC_cards'].'</td>
                 <td valign="top" style="text-align: center;">'.$value['Total_amount_to_be_debited'].'</td>
                 
                

                 </tr>
                 </tbody>';
            }
            $data['consolidate_mail_content'] .='
                                </td>
                                </tr>
                                </table>';
            
            

          //  show($data['consolidate_mail_content'],1);
            
            ///
            

            $filename = 'consolidate_report_' . date('m-Y') . '.xls';
            $subject = 'Consolidate Report.';
            //$msg = "Consolidate Report";
            $attachment = FCPATH . "cron_ors_report/$filename";
            $mail_array = array("subject" => $subject,
                "message" => $data['consolidate_mail_content'],
                "to" => 'sonali.trimax@gmail.com',
                //"to" => json_decode(CONSOLIDATE_REPORT_EMAILS),
                "attachment" => $attachment
            );

            send_mail($mail_array, $replacements);
            echo 'Cron Run Successfully';
            die;
        } else {
            echo 'Data not found';
            die;
        }
    }
     
    public function getConsolidateDetails($start_date,$end_date){
        $sql = 'SELECT oit.agent_code,                                
                rsp.terminal_code, 
                rsp.depot_code,
                depo.DEPOT_NM,
                oit.total_cards,
                '.PER_CARD_AMOUNT.' as per_card_Amount,
                (oit.total_cards * '.PER_CARD_AMOUNT.') Total_Amount,                                
                (CASE 
                    WHEN oit.updated_date THEN oit.updated_date
                    ELSE oit.created_date
                    END) AS Order_Date 
                FROM `otc_invenory` `oit`
                INNER JOIN `retailer_service_mapping` `rsp` ON `oit`.`user_id` = `rsp`.`agent_id`
                INNER JOIN msrtc_replication.depots depo on depo.DEPOT_CD = rsp.depot_code  
                WHERE oit.created_date between "'.$start_date.'" and "'.$end_date.'"
                AND `rsp`.`service_id` = '.SMART_CARD_SERVICE;
//        $this->db->query($sql);
//               echo $this->db->last_query();
//               die;
       
       return $this->db->query($sql)->result_array();

    }
    
    
    public function getConsolidateDetailsByGroup($start_date,$end_date){
        $sql = 'SELECT oit.agent_code,                                
                rsp.terminal_code, 
                rsp.depot_code,
                depo.DEPOT_NM,
                sum(oit.total_cards) as total_cards,
                sum('.PER_CARD_AMOUNT.') as per_card_Amount,
                sum(oit.total_cards * '.PER_CARD_AMOUNT.') Total_Amount,                                
                (CASE 
                    WHEN oit.updated_date THEN oit.updated_date
                    ELSE oit.created_date
                    END) AS Order_Date 
                FROM `otc_invenory` `oit`
                INNER JOIN `retailer_service_mapping` `rsp` ON `oit`.`user_id` = `rsp`.`agent_id`
                INNER JOIN msrtc_replication.depots depo on depo.DEPOT_CD = rsp.depot_code  
                WHERE oit.created_date between "'.$start_date.'" and "'.$end_date.'"
                AND `rsp`.`service_id` = '.SMART_CARD_SERVICE.' group by DATE_FORMAT(oit.created_date,"%Y-%m-%d")' ;
//        $this->db->query($sql);
//               echo $this->db->last_query();
//               die;
       
       return $this->db->query($sql)->result_array();

    }
}