<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ccavenue_cron_api extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('tickets_model', 'ticket_details_model', 'users_model', 'op_config_model','mail_template_model', 'ticket_dump_model', 'ticket_tax_trans_model', 'processed_ticket_tax_trans_model', 'etravels_bus_service_model', 'api_bus_types_model', 'temp_bus_types_model', 'payment_confirmation_model', 'payment_confirmation_metadata_model'));
        $this->load->library("soap_api/upsrtc_api");
        $this->load->library(array('api_provider/etravelsmart_api','cca_payment_gateway'));

        $this->load->model(array('state_model', 'bus_stop_master_model', 'bus_type_model', 
                                 'travel_destinations_model', 'operator_commission_model', 'agent_commission_model','Boarding_stop_model'
                                )
                          );

    }
    
    public function index()
    {

    }


   /*
    *Author : Suraj Rathod
    *Function : confirmOrder()
    *Detail : To confirm order
    */
    public function confirmOrder()
    {
        $data = $this->tickets_model->where(array("transaction_status" => "success","confirm_order" => ""))->find_all();
        show($data,1);
    }


   /*
    *Author : Suraj Rathod
    *Function : cancelOrder()
    *Detail : To cancel order
    */
    public function cancelOrder()
    {

        
    }

   /*
    *Author : Suraj Rathod
    *Function : refundOrder()
    *Detail : To refund order
    */
    public function refundOrder()
    {
        
        
    }


   /*
    *Author : Suraj Rathod
    *Function : orderStatusTracker()
    *Detail : To orderStatusTracker
    */
    public function orderStatusTracker()
    {

        
    }


   /*
    *Author : Suraj Rathod
    *Function : orderLookup()
    *Detail : To orderLookup
    */
    public function orderLookup()
    {

        
    }

   /*
    *Author : Suraj Rathod
    *Function : getPendingOrders()
    *Detail : To getPendingOrders
    */
    public function getPendingOrders()
    {

        
    }


   /*
    *Author : Suraj Rathod
    *Function : deleteCustomer()
    *Detail : To deleteCustomer
    */
    public function deleteCustomer()
    {

        
    }


   /*
    *Author : Suraj Rathod
    *Function : deleteCustomerPaymentOption()
    *Detail : To deleteCustomerPaymentOption
    */
    public function deleteCustomerPaymentOption()
    {

        
    }



   /*
    *Author : Suraj Rathod
    *Function : getCustomerPaymentOptions()
    *Detail : To getCustomerPaymentOptions
    */
    public function getCustomerPaymentOptions()
    {

        
    }


   /*
    *Author : Suraj Rathod
    *Function : generateInvoice()
    *Detail : To generateInvoice
    */
    public function generateInvoice()
    {

        
    }


   /*
    *Author : Suraj Rathod
    *Function : generateRecurringInvoice()
    *Detail : To generateRecurringInvoice
    */
    public function generateRecurringInvoice()
    {

        
    }

   /*
    *Author : Suraj Rathod
    *Function : generateQuickInvoice()
    *Detail : To generateQuickInvoice
    */
    public function generateQuickInvoice()
    {

        
    }

   /*
    *Author : Suraj Rathod
    *Function : getInvoiceItems()
    *Detail : To getInvoiceItems
    */
    public function getInvoiceItems()
    {

        
    }


   /*
    *Author : Suraj Rathod
    *Function : invoiceList()
    *Detail : To invoiceList
    */
    public function invoiceList()
    {

        
    }


   /*
    *Author : Suraj Rathod
    *Function : updateBillMerchantReferenceNo()
    *Detail : To updateBillMerchantReferenceNo
    */
    public function updateBillMerchantReferenceNo()
    {

        
    }

   /*
    *Author : Suraj Rathod
    *Function : updateMerchantParams()
    *Detail : To updateMerchantParams
    */
    public function updateMerchantParams()
    {

        
    }

   /*
    *Author : Suraj Rathod
    *Function : updateBillingDetails()
    *Detail : To updateBillingDetails
    */
    public function updateBillingDetails()
    {

        
    }

    /*
    *Author : Suraj Rathod
    *Function : autoconfirm()
    *Detail : To autoconfirm
    */
    public function autoconfirm()
    {
        $confirmation_array = array();
        $confirmation_object = new stdClass();
        /*$confirmation_object->reference_no = "304002656299";
        $confirmation_object->amount = "1197";*/
        $confirmation_object->reference_no = "304002656359";
        $confirmation_object->amount = "1050";
        $confirmation_array[] = $confirmation_object;

        $data = $this->cca_payment_gateway->paymentConfirmation($confirmation_array);
        show($data,1); 
    }


   /*
    *Author : Suraj Rathod
    *Function : confirmTransactions_cron()
    *Detail : To confirmTransactions_cron
    */
    public function confirmTransactions_cron()
    {
        $tickets_temp_data = array();
        $pg_tracking_id_array = array();
        $success_pg_tracking_ids = array();
        $confirmation_array = array();
        $pc_array = array();
        $developer_custom_mail_data = array();
        $mail_statement = "";
        $failed_tracking_id_reason = "";
        
        $success_ticket_condition = array("payment_confirmation" => "not_done","transaction_status" => "success", "pnr_no != " => "","payment_by" => "ccavenue");

        $tickets_data = $this->tickets_model->where($success_ticket_condition)->find_all();

        if($tickets_data)
        {
            foreach ($tickets_data as $key => $value)
            {
                $tickets_temp_data[$value->ticket_id] = $value;
                $pg_tracking_id_array[$value->pg_tracking_id]["ticket_id"][] = $value->ticket_id;
            }

            foreach ($tickets_temp_data as $temp_key => $temp_value)
            {
                $confirmation_object = new stdClass();
                $return_ticket_amount = 0; //Always 0, Because In onwar=return journey amount already updated in onward journey tickets column.
                $pg_amount_paid = "";

                $pg_tracking_id_array[$temp_value->pg_tracking_id]["full_partial"] = "full";

                if($temp_value->is_return_journey == 1)
                {
                    if(isset($tickets_temp_data[$temp_value->return_ticket_id]) && $tickets_temp_data[$temp_value->return_ticket_id]->transaction_status == "success")
                    {
                        // $return_ticket_amount = $tickets_temp_data[$temp_value->return_ticket_id]->merchant_amount;
                    }
                    else
                    {
                        $pg_tracking_id_array[$temp_value->pg_tracking_id]["full_partial"] = "partial"; 
                    }
                    
                    unset($tickets_temp_data[$temp_value->return_ticket_id]);
                }
                

                $pg_amount_paid = $temp_value->merchant_amount+$return_ticket_amount;

                if($pg_amount_paid > 0)
                {
                    $confirmation_object->reference_no = $temp_value->pg_tracking_id;
                    $confirmation_object->amount = $pg_amount_paid;
                    $confirmation_array[] = $confirmation_object;

                    $pc_array[] = array(
                                    "ticket_id" => $temp_value->ticket_id,
                                    "return_ticket_id" => $temp_value->return_ticket_id,
                                    "amount" => $pg_amount_paid,
                                    "pg_tracking_id" => $temp_value->pg_tracking_id
                                    );
                }
            }
        

            $mail_statement .= "Total transaction records sent to ccavenue is - ".count($tickets_temp_data)."<br/>";
            // $mail_statement .= "Transaction Request pg pg_tracking_id - ".json_encode($pg_tracking_id_array)."<br/>";
            
            $payment_confirmation_metadata_id = $this->payment_confirmation_metadata_model->insert(array("request_parameter" => json_encode($confirmation_array),"total_transaction_sent" => count($tickets_temp_data)));

            foreach ($pc_array as $pc_key => $pc_value)
            {
                $pc_array[$pc_key]["payment_confirmation_metadata_id"] = $payment_confirmation_metadata_id;
            }

            
            $this->db->insert_batch('payment_confirmation', $pc_array); 
            //IMP DONT REMOVE
            $pg_confirmation_response = $this->cca_payment_gateway->paymentConfirmation_cron($confirmation_array);

            $pg_tracking_failed_tansactions = count($tickets_temp_data)-$pg_confirmation_response["order_result"]->success_count;
            $this->payment_confirmation_metadata_model->update($payment_confirmation_metadata_id, array("response_parameter" => json_encode($pg_confirmation_response),"total_success_count" => $pg_confirmation_response["order_result"]->success_count,"total_failed_count" => count($tickets_temp_data)-$pg_confirmation_response["order_result"]->success_count));
            
            $mail_statement .= "Total transaction success - ".$pg_confirmation_response["order_result"]->success_count."<br/>";
            $mail_statement .= "Total transaction failed - ".$pg_tracking_failed_tansactions."<br/>";
            
            $success_pg_tracking_ids = $pg_tracking_id_array;
            $failed_pg_tracking_ids = [];
            $failed_to_update = [];
            
            if(isset($pg_confirmation_response["order_result"]->failed_List))
            {
                $failed_list_obj = $pg_confirmation_response["order_result"]->failed_List;

                if(isset($failed_list_obj->failed_order))
                {   
                    if(is_array($failed_list_obj->failed_order))
                    {
                        foreach ($failed_list_obj->failed_order as $pcrkey => $pcrvalue)
                        {
                            log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",$pcrvalue,'pcrvalue');

                            if($pcrvalue->reason == 'Order is already confirmed') {
                                $this->db->set('payment_confirmation', "Full"); 
                                $this->db->where('pg_tracking_id', $pcrvalue->reference_no); 
                                $this->db->update('tickets');
                                log_data("transaction_confirmation/ccavenue_confirmation_query".date("d-m-Y")."_log.log",$this->db->last_query(),'Last query');
                            }
                            $failed_pg_tracking_ids[] = $pcrvalue->reference_no;
                            $failed_to_update[] = array("pg_tracking_id" => $pcrvalue->reference_no, "payment_confirmation" => "not_done");
                            
                            unset($success_pg_tracking_ids[$pcrvalue->reference_no]);
                            $this->payment_confirmation_model->update_where(array("pg_tracking_id" => $pcrvalue->reference_no),"",array("failed_code" => $pcrvalue->error_code,"failed_reason" => $pcrvalue->reason));
                            $failed_tracking_id_reason .= "PG Tracking Id -> ".$pcrvalue->reference_no." , failed code -> ".$pcrvalue->error_code." , failed reason -> ".$pcrvalue->reason."<br/>";
                        }
                    }
                    else
                    {
                        log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",$failed_list_obj->failed_order,'single failed object');

                        $single_failed_object = $failed_list_obj->failed_order;

                        $failed_pg_tracking_ids[] = $single_failed_object->reference_no;
                        $failed_to_update[] = array("pg_tracking_id" => $single_failed_object->reference_no, "payment_confirmation" => "not_done");
                        
                        unset($success_pg_tracking_ids[$single_failed_object->reference_no]);
                        $this->payment_confirmation_model->update_where(array("pg_tracking_id" => $single_failed_object->reference_no),"",array("failed_code" => "","failed_reason" => $single_failed_object->reason));
                        $failed_tracking_id_reason .= "PG Tracking Id -> ".$single_failed_object->reference_no." , failed code ->  , failed reason -> ".$single_failed_object->reason."<br/>";
                    }
                }
            }
            else if($pg_confirmation_response["order_result"]->success_count == 0)
            {
                unset($success_pg_tracking_ids);
                $success_pg_tracking_ids = array();
            }
            
            //for updating tickets table
            $tickets_to_update = [];
            if($success_pg_tracking_ids)
            {
                log_data("transaction_confirmation/success_pg_tracking_ids_".date("d-m-Y")."_log.log",$success_pg_tracking_ids,'Tickets to be updated');
                foreach ($success_pg_tracking_ids as $spt_key => $spt_value)
                {
                    foreach ($spt_value["ticket_id"] as $ticket_key => $ticket_id)
                    {
                       $tickets_to_update[] = array("ticket_id" => $ticket_id, "payment_confirmation" => $spt_value["full_partial"]); 
                    }
                } 
            }
            //success tickets to update
            log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",json_encode($tickets_to_update),'Tickets before updation');
            if($tickets_to_update)
            {
                foreach($tickets_to_update as $value) {
                    $this->db->set('payment_confirmation', $value['payment_confirmation']); 
                    $this->db->where('ticket_id', $value['ticket_id']); 
                    $this->db->update('tickets');

                    log_data("transaction_confirmation/ccavenue_confirmation_query".date("d-m-Y")."_log.log",$this->db->last_query(),'Last query');
                }
                
            }

            log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",json_encode($tickets_to_update),'Tickets Updated as full');
            log_data("transaction_confirmation/ccavenue_confirmation_query".date("d-m-Y")."_log.log",$this->db->last_query(),'Last query');
            log_data("transaction_confirmation/failed_to_update_".date("d-m-Y")."_log.log",json_encode($failed_to_update),'Tickets failed to update');

            //failed tickets to update
            if($failed_to_update)
            {
                $this->db->update_batch('tickets', $failed_to_update, 'pg_tracking_id'); 
            }
            log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",json_encode($failed_to_update),'Tickets Updated as not_done');
            log_data("transaction_confirmation/ccavenue_confirmation_".date("d-m-Y")."_log.log",json_encode($failed_pg_tracking_ids),'failed_pg_tracking_ids');

            $mail_statement .= "Tickets Table Updated : <br>";
            $mail_statement .= json_encode($tickets_to_update)."<br>";
            $mail_statement .= "Trasaction failed summary : <br>";
            $mail_statement .= ($failed_tracking_id_reason != "") ? $failed_tracking_id_reason : "No Failed Transaction";
            $mail_statement .= "<br/><br/><br/><br/>";
            $mail_statement .= "Success Ticket Id : ".json_encode($success_pg_tracking_ids);
            $mail_statement .= "<br/><br/><br/><br/>";
            $mail_statement .= "Failed PG Tracking Id : ". (!empty($failed_to_update)) ? json_encode(array_column($failed_to_update, 'pg_tracking_id')) : "No failed";
            $mail_statement .= "<br/><br/><br/><br/>";
            $mail_statement .= "Request Parameter : ".json_encode($confirmation_array);
        }

        $mail_statement .= "No tickets found for confirmation.";
        $developer_custom_mail_data["custom_error_subject"] = "Payment Confirmation mail";
        $developer_custom_mail_data["custom_error_message"] = $mail_statement;
        $this->common_model->management_custom_error_mail($developer_custom_mail_data, "management");

        die();
    }


   /*
    * Author    : Rajkumar Shukla
    * Function  : AgentTopupConfirmTransaction_cron()
    * Detail    : To confirmTransactions of agent wallet topup
    */
    public function agent_topup_confirm_transactions_cron()
    {
        $wallet_temp_data = array();
        $pg_tracking_id_array = array();
        $success_pg_tracking_ids = array();
        $confirmation_array = array();
        $pc_array = array();
        $developer_custom_mail_data = array();
        $mail_statement = "";
        $failed_tracking_id_reason = "";

        $wallet_topup_data = $this->wallet_topup_model->confirm_topup_data();

        if($wallet_topup_data)
        {
            foreach ($wallet_topup_data as $key => $value)
            {
                $wallet_temp_data[$value->id] = $value;
                $pg_tracking_id_array[trim($value->pg_tracking_id)]["id"][] = $value->id;
            }

            foreach ($wallet_temp_data as $temp_key => $temp_value)
            {
                $confirmation_object = new stdClass(); 
                $pg_amount_paid = "";
                $pg_tracking_id_array[trim($temp_value->pg_tracking_id)]["full_partial"] = "full";
                $pg_amount_paid = $temp_value->merchant_amount;

                if($pg_amount_paid > 0)
                {
                    $confirmation_object->reference_no = $temp_value->pg_tracking_id;
                    $confirmation_object->amount = $pg_amount_paid;
                    $confirmation_array[] = $confirmation_object;

                    $pc_array[] = array(
                                        "wallet_topup_id" => $temp_value->id,
                                        "amount" => $pg_amount_paid,
                                        "pg_tracking_id" => $temp_value->pg_tracking_id
                                        );
                }
            }
            $mail_statement .= "Total transaction records sent to ccavenue is - ".count($wallet_temp_data)."<br/>";
            // $mail_statement .= "Transaction Request pg pg_tracking_id - ".json_encode($pg_tracking_id_array)."<br/>";
            
            $payment_confirmation_metadata_id = $this->payment_confirmation_metadata_model->insert(array("request_parameter" => json_encode($confirmation_array),"total_transaction_sent" => count($wallet_temp_data)));

            foreach ($pc_array as $pc_key => $pc_value)
            {
                $pc_array[$pc_key]["payment_confirmation_metadata_id"] = $payment_confirmation_metadata_id;
            }
            
            $this->db->insert_batch('payment_confirmation', $pc_array);
            
            //IMP DONT REMOVE
            $extra_value = [
                            "reason" =>     "wallet_topup"
                            ];
            $pg_confirmation_response = $this->cca_payment_gateway->topup_paymentConfirmation_cron($confirmation_array,$extra_value);

            $pg_tracking_failed_tansactions = count($wallet_temp_data)-$pg_confirmation_response["order_result"]->success_count;
            $this->payment_confirmation_metadata_model->update($payment_confirmation_metadata_id, array("response_parameter" => json_encode($pg_confirmation_response),"total_success_count" => $pg_confirmation_response["order_result"]->success_count,"total_failed_count" => count($wallet_temp_data)-$pg_confirmation_response["order_result"]->success_count));
            
            $mail_statement .= "Total transaction success - ".$pg_confirmation_response["order_result"]->success_count."<br/>";
            $mail_statement .= "Total transaction failed - ".$pg_tracking_failed_tansactions."<br/>";
            
            $success_pg_tracking_ids = $pg_tracking_id_array;
            $failed_pg_tracking_ids = [];
            $failed_to_update = [];
            
            if(isset($pg_confirmation_response["order_result"]->failed_List))
            {
                $failed_list_obj = $pg_confirmation_response["order_result"]->failed_List;

                if(isset($failed_list_obj->failed_order))
                {   
                    if(is_array($failed_list_obj->failed_order))
                    {
                        foreach ($failed_list_obj->failed_order as $pcrkey => $pcrvalue)
                        {
                            log_data("transaction_confirmation_wallet_topup/ccavenue_confirmation_".date("d-m-Y")."_log.log",$pcrvalue,'pcrvalue');

                            $failed_pg_tracking_ids[] = $pcrvalue->reference_no;
                            $failed_to_update[] = array("pg_tracking_id" => $pcrvalue->reference_no, "payment_confirmation" => "not_done");
                            unset($success_pg_tracking_ids[trim($pcrvalue->reference_no)]);
                            $this->payment_confirmation_model->update_where(array("pg_tracking_id" => $pcrvalue->reference_no),"",array("failed_code" => $pcrvalue->error_code,"failed_reason" => $pcrvalue->reason));
                            $failed_tracking_id_reason .= "PG Tracking Id -> ".$pcrvalue->reference_no." , failed code -> ".$pcrvalue->error_code." , failed reason -> ".$pcrvalue->reason."<br/>";
                        }
                    }
                    else
                    {
                        log_data("transaction_confirmation_wallet_topup/ccavenue_confirmation_".date("d-m-Y")."_log.log",$failed_list_obj->failed_order,'single failed object');

                        $single_failed_object = $failed_list_obj->failed_order;

                        $failed_pg_tracking_ids[] = $single_failed_object->reference_no;
                        $failed_to_update[] = array("pg_tracking_id" => $single_failed_object->reference_no, "payment_confirmation" => "not_done");
                        
                        unset($success_pg_tracking_ids[$single_failed_object->reference_no]);
                        $this->payment_confirmation_model->update_where(array("pg_tracking_id" => $single_failed_object->reference_no),"",array("failed_code" => "","failed_reason" => $single_failed_object->reason));
                        $failed_tracking_id_reason .= "PG Tracking Id -> ".$single_failed_object->reference_no." , failed code ->  , failed reason -> ".$single_failed_object->reason."<br/>";
                    }
                }
            } 
            else if($pg_confirmation_response["order_result"]->success_count == 0)
            {
                unset($success_pg_tracking_ids);
                $success_pg_tracking_ids = array();
            }
            
            //for updating wallet table

            $wallet_topup_to_update = [];
            if($success_pg_tracking_ids)
            {
                foreach ($success_pg_tracking_ids as $spt_key => $spt_value)
                {
                    foreach ($spt_value["id"] as $w_key => $w_trans_id)
                    {
                       $wallet_topup_to_update[] = array("id" => $w_trans_id, "payment_confirmation" => $spt_value["full_partial"]); 
                    }
                } 
            }
            //success wallet to update
            if($wallet_topup_to_update)
            {
                $this->db->update_batch('wallet_topup', $wallet_topup_to_update, 'id'); 
            }

            log_data("transaction_confirmation_wallet_topup/ccavenue_confirmation_".date("d-m-Y")."_log.log",json_encode($tickets_to_update),'Tickets Updated as full');

            //failed wallet_topup to update
            if($failed_to_update)
            {
                $this->db->update_batch('wallet_topup', $failed_to_update, 'pg_tracking_id'); 
            }
            log_data("transaction_confirmation_wallet_topup/ccavenue_confirmation_".date("d-m-Y")."_log.log",json_encode($failed_to_update),'wallet Updated as not_done');
            log_data("transaction_confirmation_wallet_topup/ccavenue_confirmation_".date("d-m-Y")."_log.log",json_encode($failed_pg_tracking_ids),'failed_pg_tracking_ids');

            $mail_statement .= "Wallet Table Updated : <br>";
            $mail_statement .= json_encode($wallet_topup_to_update)."<br>";
            $mail_statement .= "Trasaction failed summary : <br>";
            $mail_statement .= ($failed_tracking_id_reason != "") ? $failed_tracking_id_reason : "No Failed Transaction";
            $mail_statement .= "<br/><br/><br/><br/>";
            $mail_statement .= "Success Ticket Id : ".json_encode($success_pg_tracking_ids);
            $mail_statement .= "<br/><br/><br/><br/>";
            $mail_statement .= "Failed PG Tracking Id : ". (!empty($failed_to_update)) ? json_encode(array_column($failed_to_update, 'pg_tracking_id')) : "No failed";
            $mail_statement .= "<br/><br/><br/><br/>";
            $mail_statement .= "Request Parameter : ".json_encode($confirmation_array);
        }

        $mail_statement .= "No wallet topup found for confirmation.";
        $developer_custom_mail_data["custom_error_subject"] = "Wallet Payment Confirmation mail";
        $developer_custom_mail_data["custom_error_message"] = $mail_statement;
        $this->common_model->management_custom_error_mail($developer_custom_mail_data, "management");

        die();
    }
    
}//End of Class
