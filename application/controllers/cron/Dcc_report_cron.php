<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Dcc_report_cron extends CI_Controller{
    public function __construct() {
        parent::__construct();
        

        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);
        
        $this->load->library(array('Excel'));
        $this->load->helper("url");
        
    }
    public function index(){
        $y_date = date('Y-m-d'.' 00:00:00',strtotime("-1 days"));
       
        $today_date = date('Y-m-d').' 23:59:59';
		
	//$y_date = date('2019-05-09'.' 00:00:00',strtotime("-1 days"));       
        //$today_date = date('2019-05-09').' 23:59:59';
        
		
		
        $result_data  = $this->getCardIssuanceDetails($y_date, $today_date);
        
        if (!empty($result_data)) {
            $sr_no = 1;
            $data = array();
            foreach ($result_data as $key => $value) {
                switch ($value['card_status']) {
    case "I":
        $status = "Initiated when user register";
        break;
    case "P":
        $status = "Pending";
        break;
    case "T":
        $status = "Transport from region to depot";
        break;
    case "A":
        $status = "At depot writing the card";
        break;
    case "Y":
        $status = "Available for dispatch";
        break;
    case "D":
        $status = "Dispatched";
        break;
    case "L":
        $status = "Card Loss";
        break;
    case "F":
        $status = "Card Faulty";
        break;
    case "C":
        $status = "Cancelled by user";
        break;
    case "R":
        $status = "Rejected";
        break;
    case "S":
        $status = "Surrender";
        break;
    case "B":
        $status = "Block";
        break;
    case "X":
        $status = "Card not updated at CC";
        break;
    default:
        $status = "None";
}
                
                $data[] = array('Sr. No.' => $sr_no,
                    'Transaction Id' => $value['Transaction Id'],
                    'Trimax Card Id' => $value['Trimax Card Id'],
                    'Smart Card ID' => $value['Smart Card ID'],
                    'Card Type' => $value['Card Type'],
                    'SW expiry' => $value['SW expiry'],
                    'TW expiry' => $value['TW expiry'],
                    'SW Bal' => $value['SW Bal'],
                    'TW Bal' => $value['TW Bal'],
                    'Date' => $value['Date'],
                    'Status' => $status,
				
					
				     'RequestId' => $value['request_id']
                );
                $sr_no++;
            }
			//	'posRequestId' => $value['pos_request_id'],
			//'posStatusCode' => $value['pos_status_code'],
//        echo '<pre>';
//        print_r($value);
//        die;
//        $this->excel->data_2_excel('micro_atm_transaction_report_' . time() . '.xls', $data);
//        die;
			//define('DCC_REPORT_CRON1',json_encode(array('mohd.rashid@sts.in')));

            $this->excel->data_save_in_excel('demo_issuance_report' . date('Y-m-d') . '.xls', $data);
            $replacements = array();

            $filename = 'demo_issuance_report' . date('Y-m-d') . '.xls';
            $subject = 'Demo Issuance Report.';
            $msg = "Demo Issuance Report";
            $attachment = FCPATH . "cron_ors_report/$filename";
            $mail_array = array("subject" => $subject,
                "message" => $msg,
                //"to" => 'surekha.trimax@gmail.com',
                "to" => json_decode(DCC_REPORT_CRON),
                "attachment" => $attachment
            );

            $result = send_mail($mail_array, $replacements);
            echo 'Cron Run Successfully';
            die;
        } else {
            echo 'Data not found';
            die;
        }
    } 
      
    public function getCardIssuanceDetails($y_date, $today_date){
         $sql = 'SELECT cid.cc_txn_id AS "Transaction Id",
                cid.trimax_id AS "Trimax Card Id",
                cid.cust_card_id AS "Smart Card ID",
                cid.flag AS "Card Type",
                cid.sw_expiry AS "SW expiry",
                cid.tw_expiry AS "TW expiry",
                cid.sw_bal AS "SW Bal",
                cid.tw_bal AS "TW Bal",
                cid.created_date AS "Date",
                ccd.card_status,
				cid.pos_status_code,
				cid.pos_request_id,
				cct.request_id
         FROM ps_card_issuance_details cid
         join ps_customer_card_master ccd on cid.trimax_id  = ccd.trimax_card_id
		 left join ps_card_transaction cct on cct.pos_request_id  = cid.pos_request_id
         WHERE cid.created_date BETWEEN "'.$y_date.'" AND "'.$today_date.'" 	and ccd.is_active="1" ORDER BY cid.created_date';
        //$this->db->query($sql);
        //       echo $this->db->last_query();
        //       die;
       
       return $this->db->query($sql)->result_array();

    }
     public function getTransType(){
        $sql = 'select * from ps_card_trans_type';
       
       return $this->db->query($sql)->result_array();

    }
    public function transaction_details(){
        $yest_date = date('Y-m-d'.' 00:00:00',strtotime("-1 days"));       
        $today_date = date('Y-m-d').' 23:59:59';
        
	//$yest_date = date('2019-05-09'.' 00:00:00',strtotime("-1 days"));       
        //$today_date = date('2019-05-09').' 23:59:59';
        
		
        $result_data  = $this->getTransactionDetails($yest_date, $today_date);
        $getTransType  = $this->getTransType();
		$TransTypeArr=array() ;
        foreach($getTransType as $TransType)
        {
           $TransTypeArr[$TransType['ctrans_type_code']]=$TransType['ctrans_type_name'];        
		}
        
      
        
        if (!empty($result_data)) {
            $sr_no = 1;
            $data = array();
            foreach ($result_data as $key => $value) {
               $data_arr = array('Sr. No.' => $sr_no,
                    'Name' => $value['Name'],
                    'Contact Number' => $value['Contact Number'],
                    'Card Category' => $value['Card Category'],
                    'Trimax Card Id' => $value['Trimax Card Id'],
                    'Smart Card ID' => $value['Smart Card ID'],
                    'Transaction Type' => $TransTypeArr[$value['Transaction Type']],
                    'Transaction Mode' => $value['Transaction Mode'],
                    'Receipt Number' => $value['Receipt Number'],
                    'Transaction Date' => $value['Transaction Date'],
                    'Issuance Date' => $value['Issuance Date'],
                    'Pass/Concession Type' => $value['Pass/Concession Type'],
                    'Pass Validity' => $value['Pass Validity'],
                    'Total Amount' => $value['Total Amount'],
                    'Pass Amount' => $value['Pass Amount'],
					'Effective Amount' => $value['effective_amount'],
                    'Smart Card Fee' => $value['Smart Card Fee'],
                    'Application Fee' => $value['Application Fee'],
                    'GST' => $value['GST'],
                    'Loading Fee' => $value['Loading Fee'],
                    'BC Charge' => $value['BC Charge'],
                    'e-Purse Maintanence Fee' => $value['e-Purse Maintanence Fee'],
                    'Card Fee' => $value['Card Fee'],
                    'Bonus Amt' => $value['Bonus Amt'],
					'Pass charges' => $value['Pass Charge'], 
					'Settlement MSRTC' => $value['Settlement MSRTC'], 
                    'Request ID' => $value['Request ID'],
                    'Transaction Ref No.' => $value['Transaction Ref No.'],
					'Pos Transaction Ref No.' => $value['pos_txn_ref_no'],
                    'Depot code' => $value['Depot code'],
                    'Terminal code' => $value['Terminal code'],
                    'Pincode' => $value['Pincode'],
                    'Agent Id' => $value['AgentId'],
					'Agent Code' => $value['agent_code'],
					'Transaction date Time' => $value['created_date'],
					'Status' =>$value['Status'],
					
                );
				
				if(isset($_REQUEST['txn_id']))
				{
				   $data_arr['Transaction Id'] = $value['txn_id'] ;
				}
				$data[] = $data_arr ; 
                $sr_no++;
            }
//        echo '<pre>';
//        print_r($value);
//        die;
//        $this->excel->data_2_excel('micro_atm_transaction_report_' . time() . '.xls', $data);
//        die;
            $this->excel->data_save_in_excel('demo_transaction_report' . date('Y-m-d') . '.xls', $data);
            $replacements = array();

            $filename = 'demo_transaction_report' . date('Y-m-d') . '.xls';
            $subject = 'Demo Transaction Report.';
            $msg = "Demo Transaction Report";
            $attachment = FCPATH . "cron_ors_report/$filename";
			//define('DCC_REPORT_CRON1',json_encode(array('mohd.rashid@sts.in')));

			
            $mail_array = array("subject" => $subject,
                "message" => $msg,
                "to" => json_decode(DCC_REPORT_CRON), 
		// "to" => 'surekha.trimax@gmail.com',
                "attachment" => $attachment
            );

            $result = send_mail($mail_array, $replacements);
            echo 'Cron Run Successfully';
            die;
        } else {
            echo 'Data not found';
            die;
        }
    } 
    
    public function getTransactionDetails($y_date, $today_date){
        
        $sql = 'SELECT pccm.name_on_card AS "Name",
                pcm.mobile_number AS "Contact Number",
                category_name AS "Card Category",
                pccm.trimax_card_id AS "Trimax Card Id",
                pccm.cust_card_id AS "Smart Card ID",
                pmc.card_category_cd "Card Category",
                pct.transaction_type_code AS "Transaction Type",
                "Cash" AS "Transaction Mode",
                pct.receipt_number AS "Receipt Number",
                pct.created_date AS "Transaction Date",
                pccm.dispatch_date AS "Issuance Date",
                pct.concession_nm AS "Pass/Concession Type",
                pct.span_name AS "Pass Validity",
                if(pct.transaction_type_code ="TPTS",tw_via_sw_total_amount,total_amount) AS "Total Amount",
                max(CASE 
                        WHEN (patd.amount_code = "BF"
                              OR patd.amount_code = "PassCharge") THEN patd.amount
                        ELSE 0.00
                    END) AS "Pass Amount",
                max(CASE
                        WHEN patd.amount_code = "SCF" THEN patd.amount
                        ELSE 0.00
                    END) AS "Smart Card Fee",
                max(CASE
                        WHEN patd.amount_code = "AF" THEN patd.amount
                        ELSE 0.00
                    END) AS "Application Fee",
                max(CASE
                        WHEN patd.amount_code = "GST" THEN patd.amount
                        ELSE 0.00
                    END) AS "GST",
                max(CASE
                        WHEN patd.amount_code = "LoadingFee" THEN patd.amount
                        ELSE 0.00
                    END) AS "Loading Fee",
                max(CASE
                        WHEN patd.amount_code = "BCCharges" THEN patd.amount
                        ELSE 0.00
                    END) AS "BC Charge",
                max(CASE
                        WHEN patd.amount_code = "EPurseFees" THEN patd.amount
                        ELSE 0.00
                    END) AS "e-Purse Maintanence Fee",
                max(CASE
                        WHEN patd.amount_code = "CardFee" THEN patd.amount
                        ELSE 0.00
                    END) AS "Card Fee",
                max(CASE
                        WHEN patd.amount_code = "BonusAmt" THEN patd.amount
                        ELSE 0.00
                    END) AS "Bonus Amt",
				max(CASE
                        WHEN patd.amount_code = "PassCharge" THEN patd.amount
                        ELSE 0.00
                    END) AS "Pass Charge", 
					
					(
                 max(CASE
                        WHEN patd.amount_code = "SCF" THEN patd.amount
                        ELSE 0.00
                    END)+ max(CASE
                        WHEN patd.amount_code = "GST" THEN patd.amount
                        ELSE 0.00
                    END) +  max(CASE
                        WHEN patd.amount_code = "EPurseFees" THEN patd.amount
                        ELSE 0.00
                    END)+  max(CASE
                        WHEN patd.amount_code = "BF" THEN patd.amount
                        ELSE 0.00
                    END)
                  +  max(CASE
                        WHEN patd.amount_code = "PN" THEN patd.amount
                        ELSE 0.00
                    END)
                    
                   ) as "Settlement MSRTC", 
					
                pct.request_id    AS "Request ID",
                pct.txn_ref_no AS "Transaction Ref No.",
				pct.pos_txn_ref_no AS "pos_txn_ref_no",
                at.depot_code AS "Depot code",
                at.terminal_code AS "Terminal code",
                pcm.pincode AS "Pincode",
				au.id AS "AgentId",
			    au.agent_code AS "agent_code",
				pct.created_date AS "created_date",
				pct.id AS "txn_id",
				if(pct.transaction_type_code="TPTS",(sum(if(patd.amount_code="BonusAmt",patd.amount,0))+tw_via_sw_total_amount),pct.amount) AS "effective_amount",
				CASE WHEN pct.txn_ref_no IS NULL THEN "Failed" WHEN tw_created_status = "C" THEN "Success" ELSE "Failed" END as "Status"

		   FROM ps_customer_master pcm
         LEFT OUTER JOIN ps_customer_card_master pccm ON pcm.id = pccm.customer_id
         INNER JOIN ps_master_card pmc ON pccm.master_card_id = pmc.id
         INNER JOIN ps_card_transaction pct ON pccm.trimax_card_id = pct.trimax_card_id
         INNER JOIN ps_card_transaction_details pctd ON pct.id = pctd.card_transaction_id
         INNER JOIN ps_amount_transaction pat ON pct.id = pat.card_transaction_id
         INNER JOIN ps_amount_transaction_details patd ON patd.amount_transaction_id = pat.id
         INNER JOIN ps_pass_category ppc ON ppc.id = pass_category_id
         INNER JOIN users au ON pct.created_by = au.id
         INNER JOIN retailer_service_mapping at ON pct.terminal_id = at.terminal_id
         WHERE 1=1
         and tw_created_status = "C"
         AND pct.created_date BETWEEN "'.$y_date.'" AND "'.$today_date.'"
          and at.service_id="'.SMART_CARD_SERVICE.'" and at.status="Y"
        GROUP BY patd.amount_transaction_id
        ORDER BY pct.transaction_type_id,
         pct.created_date';
              ///AND pct.is_active = "1"
        
        /*$this->db->query($sql);
        echo $this->db->last_query();
        die;*/
       //pct.amount AS "effective_amount"
       return $this->db->query($sql)->result_array();
    }
    
    public function wallet_claim(){
        $yest_date = date('Y-m-d'.' 00:00:00',strtotime("-1 days"));       
        $today_date = date('Y-m-d').' 23:59:59';
	 //$yest_date = date('2019-05-09'.' 00:00:00',strtotime("-1 days"));       
        //$today_date = date('2019-05-09').' 23:59:59';
        
        $result_data  = $this->getWalletClaimDetails($yest_date, $today_date);
		$cust_card_id=array();
		  foreach ($result_data as $key => $value) {
		   $cust_card_id[]=$value['Smart Card ID'] ;
		  }
		 $ids= implode(",",$cust_card_id) ; 
		 
	$sql = 'SELECT 
                trimax_card_id ,
				cust_card_id
         FROM ps_customer_card_master pswd
         WHERE cust_card_id in ('.$ids.')';
        
         
         $rs=$this->db->query($sql)->result_array();
		 $trimaxArr=array() ;
	     foreach($rs  as  $Custdata){
		    $trimaxArr[$Custdata['cust_card_id']]=$Custdata['trimax_card_id'] ;
		 }
	
       
        
        if (!empty($result_data)) {
            $sr_no = 1;
            $data = array();
            foreach ($result_data as $key => $value) {
			
			    $value['Trimax Card Id']=$trimaxArr[$value['Smart Card ID']] ; 
            
   			  $data[] = array('Sr. No.' => $sr_no,                    
                    'Transaction Id' => $value['Transaction Id'],
                    'Trimax Card Id' => $value['Trimax Card Id'],
                    'Smart Card ID' => $value['Smart Card ID'],
                    'Mobile No' => $value['Mobile No'],
                    'Opening Bal' => $value['Opening Bal'],
                    'Top Up Amt' => $value['Top Up Amt'],
                    'Closing Bal' => $value['Closing Bal'],
                    'Expiry Date' => $value['Expiry Date'],
                    'Topup Flag' => $value['Topup Flag'],
					'request_id' => $value['request_id'],
					'txn_ref_no' => $value['txn_ref_no'],
					'pos_request_id' => $value['pos_request_id'],
					'pos_txn_ref_no' => $value['pos_txn_ref_no'],
                    'Date' => $value['Date'],
                    
                    
                );
                $sr_no++;
            }
     //   echo '<pre>';
       // print_r($data);
        //die;
//        $this->excel->data_2_excel('micro_atm_transaction_report_' . time() . '.xls', $data);
//        die;
    // define('DCC_REPORT_CRON1',json_encode(array('mohd.rashid@sts.in')));

            $this->excel->data_save_in_excel('demo_wallet_claim_report' . date('Y-m-d') . '.xls', $data);
            $replacements = array();

            $filename = 'demo_wallet_claim_report' . date('Y-m-d') . '.xls';
            $subject = 'Demo Wallet Claim Report.';
            $msg = "Demo Wallet Claim Report";
            $attachment = FCPATH . "cron_ors_report/$filename";
            $mail_array = array("subject" => $subject,
                "message" => $msg,
                "to" => json_decode(DCC_REPORT_CRON),
                "attachment" => $attachment
            );

            $result = send_mail($mail_array, $replacements);
            echo 'Cron Run Successfully';
            //die;
        } else {
            echo 'Data not found';
            //die;
        }
		
    }

    public function getWalletClaimDetails($y_date, $today_date){
        $sql = 'SELECT transaction_id AS "Transaction Id",
                pswd.cust_card_id  AS "Smart Card ID",
                mobile_number AS "Mobile No",
                opening_balance AS "Opening Bal",
                amount AS "Top Up Amt",
                closing_balance AS "Closing Bal",
                expiry_date AS "Expiry Date",
                topup_flag AS "Topup Flag",
				pswd.request_id AS "request_id",
				pswd.txn_ref_no AS "txn_ref_no",
				pswd.pos_request_id AS "pos_request_id",
				pswd.pos_txn_ref_no AS "pos_txn_ref_no",
                pswd.created_date AS "Date"
				
         FROM ps_claim_wallet_details pswd
         WHERE pswd.created_date BETWEEN "'.$y_date.'" AND "'.$today_date.'"
         ORDER BY topup_flag,
                  pswd.created_date;';
        //  INNER JOIN ps_customer_card_master pccm ON pswd.cust_card_id = pccm.cust_card_id
        /* $this->db->query($sql);
        echo $this->db->last_query();
        die;*/
       
       return $this->db->query($sql)->result_array();
	   
    }
	
	   public function busPass_wallet_claim(){
	   
        $yest_date = date('Y-m-d'.' 00:00:00',strtotime("-1 days"));       
        $today_date = date('Y-m-d').' 23:59:59';
	//$yest_date = date('2019-05-09'.' 00:00:00',strtotime("-1 days"));       
       // $today_date = date('2019-05-09').' 23:59:59';
        
        $result_data  = $this->getBusPassWalletClaimDetails($yest_date, $today_date);

        
        if (!empty($result_data)) {
            $sr_no = 1;
            $data = array();
			
			// 'pass_data' => base64_decode( $value['pass_data'] ) ,
			// echo base64_decode( $value['pass_data'] ) ;  
			//exit;
            foreach ($result_data as $key => $value) {
			
                $data[] = array('Sr. No.' => $sr_no,                    
                    'Trimax Card Id' => $value['Trimax Card Id'],
                    'Smart Card ID' => $value['Smart Card ID'],
                    'Mobile No' => $value['Mobile No'],
                    'request_id' => $value['request_id'],
					'txn_ref_no' => $value['txn_ref_no'],
					'pos_request_id' => $value['pos_request_id'],
					'pos_txn_ref_no' => $value['pos_txn_ref_no'],
                    'Date' => $value['Date'],
                );
                $sr_no++;
            }
		
			//define('DCC_REPORT_CRON1',json_encode(array('mohd.rashid@sts.in')));

            $this->excel->data_save_in_excel('demo_bus_pass_wallet_claim_report' . date('Y-m-d') . '.xls', $data);
			
            $replacements = array();

            $filename = 'demo_bus_pass_wallet_claim_report' . date('Y-m-d') . '.xls';
            $subject = 'Demo Bus Pass Wallet Claim Report.';
            $msg = "Demo Bus Pass Wallet Claim Report";
            $attachment = FCPATH . "cron_ors_report/$filename";
            $mail_array = array("subject" => $subject,
                "message" => $msg,
                "to" => json_decode(DCC_REPORT_CRON),
                "attachment" => $attachment
            );

            $result = send_mail($mail_array, $replacements);
            echo 'Cron Run Successfully';
            die;
        } else {
            echo 'Data not found';
            die;
        }
    }

	
     public function getBusPassWalletClaimDetails($y_date, $today_date){
        $sql = 'SELECT 
		        trimax_card_id AS "Trimax Card Id",
                pccm.cust_card_id  AS "Smart Card ID",
                mobile_number AS "Mobile No",
				pass_data AS "pass_data",
            	pswd.request_id AS "request_id",
				pswd.txn_ref_no AS "txn_ref_no",
				pswd.pos_request_id AS "pos_request_id",
				pswd.pos_txn_ref_no AS "pos_txn_ref_no",
                pswd.created_date AS "Date"
				
         FROM ps_claim_pass_details pswd
         INNER JOIN ps_customer_card_master pccm ON pswd.cust_card_id = pccm.cust_card_id
         WHERE pswd.created_date BETWEEN "'.$y_date.'" AND "'.$today_date.'"
         ORDER BY 
                  pswd.created_date;';
         return $this->db->query($sql)->result_array();
    }
	
		
}
