<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Commission extends CI_Controller {

    public function __construct() {
        parent::__construct();
        set_time_limit(0);
    
        ini_set('max_execution_time',0);
        ini_set("memory_limit",-1);     
    }

    public function disperse(){

        // echo "<pre>";
        $this->debit_pending_commission_from_roakd();

        $result = $this->db->select("GROUP_CONCAT(cc.id) as ids, cc.ticket_id, cc.user_id, SUM(cc.final_commission) as final_commission, cc.booking_flag, u.level")
               ->where("cc.booking_flag", 'N')
               ->where('t.transaction_status', 'success')
               ->where_in('u.level', array(1,2,3,4))
               ->join('users u',"u.id = cc.user_id")
               ->join('tickets t',"t.ticket_id = cc.ticket_id")
               ->group_by(array("cc.ticket_id", "cc.user_id"))
               ->order_by('cc.id','desc')
               ->get('commission_calculation cc')->result_array();
        
         //echo $this->db->last_query();
        // echo "<pre>"; print_r($result); die;

        $this->db->trans_strict(FALSE);

        if (is_array($result) && count($result)){

            foreach ($result as $commission){ 

                $this->db->trans_begin();
                if ($this->disperse_commission($commission) === FALSE){
                    // echo "Transaction Failure";
                    $this->db->trans_rollback();

                }else{
                    // echo "Transaction successfull";
                    $this->db->trans_commit();
                }
            }    
        }
        die;
    }


    private function disperse_commission($commission = array()){

        $wallet= $this->get_user_wallet($commission['user_id']);

        if($commission['user_id'] != $this->get_roakad_user_id())
        {
            // Do noting if wallet limit may exceed
            if($wallet->amt + $commission['final_commission'] > WALLET_LIMIT){
                return false;
            }
        }

        // if both debit & credit are successfull then continue
        if(!($this->debit_rokad_wallet($commission) && $this->credit_user_wallet($commission))){
            return false;
        }
       
        return true;

    }

    private function debit_pending_commission_from_roakd(){

        $result = $this->db->select("GROUP_CONCAT(cc.id) as ids, cc.ticket_id, cc.user_id, SUM(cc.final_commission) as final_commission, cc.booking_flag, cc.rokad_booking_flag, u.level")
           ->where(
                array(
                    "cc.booking_flag" => 'Y',
                    "cc.rokad_booking_flag"=>'N'
                )
            )
           ->where('t.transaction_status', 'success')
           ->where_in('u.level', array(5))
           ->join('users u',"u.id = cc.user_id")
           ->join('tickets t',"t.ticket_id = cc.ticket_id")
           ->group_by(array("cc.ticket_id", "cc.user_id"))
           ->get('commission_calculation cc')->result_array();

        // echo $this->db->last_query();
        // echo "<pre>"; print_r($result);die;

        $this->db->trans_strict(FALSE);

        if (is_array($result) && count($result)){

            foreach ($result as $commission){ 

                $this->db->trans_begin();
                if ($this->debit_rokad_wallet($commission) === FALSE){
                    // echo "Transaction Failure";
                    $this->db->trans_rollback();

                }else{
                    // echo "Transaction successfull";
                    $this->db->trans_commit();
                }
            }    
        }
    }

    private function get_roakad_user_id(){
        $result = $this->db->select('id')->where(array('level' => 1))
                          ->order_by('id','desc')
                          ->limit(1,0)
                          ->get('users')->result();
        return $result[0]->id;
    }

    private function get_user_wallet($userID){
        return $this->db->select("id, amt")
                    ->where("user_id", $userID)
                    ->get("wallet")->row();
    }

    private function debit_rokad_wallet($commission){
        
        if ($commission['final_commission'] != 0){
            
            $rokadUserID = $this->get_roakad_user_id();
            $wallet = $this->get_user_wallet($rokadUserID);
            $transaction_no = substr(hexdec(uniqid()), 4, 12);
            $insertData = array(
                "w_id" => $wallet->id,
                "amt"  => $commission['final_commission'],
                "wallet_type" => "actual_wallet",
                "comment"   => "Commission Topup (Debit)",
                "status"    => "Debited",
                "user_id"   => $rokadUserID,
                "amt_before_trans" => $wallet->amt,
                "amt_after_trans" => $wallet->amt - $commission['final_commission'],
                "amt_transfer_from" => 0,    // confirm this
                "amt_transfer_to"  => 0,     // confirm this
                "ticket_id" => $commission['ticket_id'],
                "transaction_type_id" => 16,
                "transaction_type" => 'Debited',
                "is_status" => 'Y',
                "added_on" => date('Y-m-d H:i:s'),
                "added_by" => 1,
                "transaction_no" => $transaction_no
            );

            if(!$this->db->insert('wallet_trans',$insertData))
                return false;

            $updateWalletDetails = array(
                'amt' => $wallet->amt - $commission['final_commission'],
            );

            if(!$this->db->update('wallet', $updateWalletDetails, array('id' => $wallet->id)))
                return false;
        }
        

        $this->db->where_in('id',explode(',', $commission['ids']));
        if(!$this->db->update('commission_calculation', array(
            'rokad_booking_flag'=>'Y',
            'rokad_booking_flag_date'=>date('Y-m-d H:i:s')
            )))
            return false;



        return true;
    }


    private function credit_user_wallet($commission){

        if ($commission['final_commission'] != 0){
            
            $wallet = $this->get_user_wallet($commission['user_id']);
            $transaction_no = substr(hexdec(uniqid()), 4, 12);
            $insertData = array(
                "w_id" => $wallet->id,
                "amt"  => $commission['final_commission'],
                "wallet_type" => "actual_wallet",
                "comment"   => "Commission Topup (Credit)",
                "status"    => "added",
                "user_id"   => $commission['user_id'],
                "amt_before_trans" => $wallet->amt,
                "amt_after_trans" => $wallet->amt + $commission['final_commission'],
                "amt_transfer_from" => 0,    // confirm this
                "amt_transfer_to"  => 0,     // confirm this
                "ticket_id" => $commission['ticket_id'],
                "transaction_type_id" => 15,
                "transaction_type" => 'Credited',
                "is_status" => 'Y',
                "added_on" => date('Y-m-d H:i:s'),
                "added_by" => 1,
                "transaction_no" => $transaction_no
            );

            if(!$this->db->insert('wallet_trans',$insertData))
                return false;

            $updateWalletDetails = array(
                'amt' => $wallet->amt + $commission['final_commission'],
            );

            if(!$this->db->update('wallet', $updateWalletDetails, array('id' => $wallet->id)))
                return false;
        
        }


        $this->db->where_in('id',explode(',', $commission['ids']));
        if(!$this->db->update('commission_calculation', array(
            'booking_flag'=>'Y',
            'booking_flag_date'=>date('Y-m-d H:i:s')
            )))
            return false;
        
        return true;
    }


    public function revoke(){

        $this->credit_pending_commission_to_rokad();

        $result = $this->db->select("GROUP_CONCAT(cc.id) as ids, cc.ticket_id, cc.user_id, SUM(cc.final_commission) as final_commission, cc.booking_flag, u.level")
               ->where(array(
                    "cc.booking_flag" => 'Y',
                    "cc.cancel_flag"  => 'N',
                ))
               ->where(array('td.status'=>'N','td.commission_cancel_flag'=>'N'))
               ->where_in('u.level', array(1,2,3,4))
               ->join('users u',"u.id = cc.user_id")
               ->join('tickets t',"t.ticket_id = cc.ticket_id")
               ->join('ticket_details td',"td.id = cc.ticket_detail_id")
               ->group_by(array("cc.ticket_id", "cc.user_id"))
               //->order_by('cc.id','desc')
               ->get('commission_calculation cc')->result_array();
        
        // echo $this->db->last_query();
        // echo "<pre>"; print_r($result); die;

        $this->db->trans_strict(FALSE);

        if (is_array($result) && count($result)){

            foreach ($result as $commission){ 

                $this->db->trans_begin();
                if ($this->revoke_commission($commission) === FALSE){
                    // echo "Transaction Failure";
                    $this->db->trans_rollback();

                }else{
                    // echo "Transaction successfull";
                    $this->db->trans_commit();
                }
            }    
        }
        die;

    }

    
    private function credit_pending_commission_to_rokad(){
        $result = $this->db->select("GROUP_CONCAT(cc.id) as ids, cc.ticket_id, cc.user_id, SUM(cc.final_commission) as final_commission, cc.cancel_flag, cc.rokad_cancel_flag, u.level")
               ->where(array(
                    "cc.cancel_flag" => 'Y',
                    "cc.rokad_cancel_flag" => 'N'
                ))
               ->where(array('td.status'=>'N','td.commission_cancel_flag'=>'N'))
               ->where_in('u.level', array(5))
               ->join('users u',"u.id = cc.user_id")
               ->join('tickets t',"t.ticket_id = cc.ticket_id")
               ->join('ticket_details td',"td.id = cc.ticket_detail_id")
               ->group_by(array("cc.ticket_id", "cc.user_id"))
               //->order_by('cc.id','desc')
               ->get('commission_calculation cc')->result_array();

        // echo $this->db->last_query();
        // echo "<pre>";print_r($result); die;

        $this->db->trans_strict(FALSE);

        if (is_array($result) && count($result)){

            foreach ($result as $commission){ 

                $this->db->trans_begin();
                if ($this->credit_rokad_wallet($commission) === FALSE){
                    // echo "Transaction Failure";
                    $this->db->trans_rollback();

                }else{
                    // echo "Transaction successfull";
                    $this->db->trans_commit();
                }
            }    
        }
    }

    private function revoke_commission($commission = array()){

        $wallet= $this->get_user_wallet($commission['user_id']);

        // if both debit & credit are successfull then continue
        if(!($this->debit_user_wallet($commission) && $this->credit_rokad_wallet($commission))){
            return false;
        }

        return true;

    }


    private function credit_rokad_wallet($commission){
        
        if ($commission['final_commission'] != 0){
            
            $rokadUserID = $this->get_roakad_user_id();
            $wallet = $this->get_user_wallet($rokadUserID);
            $transaction_no = substr(hexdec(uniqid()), 4, 12);
            $insertData = array(
                "w_id" => $wallet->id,
                "amt"  => $commission['final_commission'],
                "wallet_type" => "actual_wallet",
                "comment"   => "Commission Revoke (Credit)",
                "status"    => "added",
                "user_id"   => $rokadUserID,
                "amt_before_trans" => $wallet->amt,
                "amt_after_trans" => $wallet->amt + $commission['final_commission'],
                "amt_transfer_from" => 0,    // confirm this
                "amt_transfer_to"  => 0,     // confirm this
                "ticket_id" => $commission['ticket_id'],
                "transaction_type_id" => 18,
                "transaction_type" => 'Credited',
                "is_status" => 'Y',
                "added_on" => date('Y-m-d H:i:s'),
                "added_by" => 1,
                "transaction_no" => $transaction_no
            );

            if(!$this->db->insert('wallet_trans',$insertData))
                return false;

            $updateWalletDetails = array(
                'amt' => $wallet->amt + $commission['final_commission'],
            );

            if(!$this->db->update('wallet', $updateWalletDetails, array('id' => $wallet->id)))
                return false;
        }
        

        $this->db->where_in('id',explode(',', $commission['ids']));
        if(!$this->db->update('commission_calculation', array(
            'rokad_cancel_flag'=>'Y',
            'rokad_cancel_flag_date'=>date('Y-m-d H:i:s')
            )))
            return false;


        return true;
    }


    private function debit_user_wallet($commission){

        if ($commission['final_commission'] != 0){
            
            $wallet = $this->get_user_wallet($commission['user_id']);
            $transaction_no = substr(hexdec(uniqid()), 4, 12);
            $insertData = array(
                "w_id" => $wallet->id,
                "amt"  => $commission['final_commission'],
                "wallet_type" => "actual_wallet",
                "comment"   => "Commission Revoked (Debit)",
                "status"    => "Debited",
                "user_id"   => $commission['user_id'],
                "amt_before_trans" => $wallet->amt,
                "amt_after_trans" => $wallet->amt - $commission['final_commission'],
                "amt_transfer_from" => 0,    // confirm this
                "amt_transfer_to"  => 0,     // confirm this
                "ticket_id" => $commission['ticket_id'],
                "transaction_type_id" => 17,
                "transaction_type" => 'Debited',
                "is_status" => 'Y',
                "added_on" => date('Y-m-d H:i:s'),
                "added_by" => 1,
                "transaction_no" => $transaction_no
            );

            if(!$this->db->insert('wallet_trans',$insertData))
                return false;

            $updateWalletDetails = array(
                'amt' => $wallet->amt - $commission['final_commission'],
            );

            if(!$this->db->update('wallet', $updateWalletDetails, array('id' => $wallet->id)))
                return false;
        
        }


        $this->db->where_in('id',explode(',', $commission['ids']));
        if(!$this->db->update('commission_calculation', array(
            'cancel_flag'=>'Y',
            'cancel_flag_date'=>date('Y-m-d H:i:s')
            )))
            return false;
        
        return true;
    }

}