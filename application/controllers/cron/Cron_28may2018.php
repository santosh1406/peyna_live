<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron extends CI_Controller {

    public function __construct() {
        parent::__construct();
        // $lib = array();
        // foreach (glob(APPPATH . "libraries/soap_api/" . "*.php") as $filename) {
        //     $path = 'soap_api/' . strtolower(basename($filename, '.php'));
        //     array_push($lib, $path);
        // }

        $lib = [
                    'soap_api/upsrtc_api',
                    'soap_api/msrtc_api',
                    'api_provider/hrtc_api',
                    'soap_api/rsrtc_api',
                    'api_provider/its_api',
                    'api_provider/etravelsmart_api'
        ];

        $this->load->library($lib);

        $this->load->model(array('tmp_agent_balance_track_model','cron/cron_model', 'payid_summary_details_model','payout_summary_details_model','payment_gateway_transaction_model','master_services_model', 'travel_destinations_model', 'tickets_model','api_balance_log_model','wallet_model','wallet_trans_model','wallet_topup_model','users_model','wallet_trans_log_model','common_model','commission_calculation_model'));
           # wallet_topup_trans_model'));
    }

    public function index() {
        $param = $this->uri->segment();
        // show($param,1);
    }

    /*
     * Author : Suraj Rathod
     * Function : getCancelListUpsrtc()
     * Detail : To cancel bus services
     */

    public function getCancelListUpsrtc() {
        $data = $this->upsrtc_api->getCancelList(array('date' => "2015-07-01 17:10:00"));
        $data = $data["CancelListResponse"];

        if (isset($data["cancelledServices"]) && isset($data["cancelledServices"]["service"])) {
            $cancelled_services = $data["cancelledServices"]["service"];
            foreach ($cancelled_services as $ckey => $services) {

                if ($this->cron_model->cancelBusService($services["serviceId"])) {
                    if ($this->cron_model->cancelTicket($services["serviceId"])) {
                        $users = $this->cron_model->getTicketDetails($services["serviceId"]);
                        foreach ($users as $key => $udetail) {
                            $cancel_detail = array("{{bos_ref_no}}" => $udetail->boss_ref_no,
                                "{{refund_amount}}" => $udetail->tot_fare_amt_with_tax,
                                "{{days}}" => "7" //This must be changed afterwards
                            );

                            $temp_sms_msg = getSMS("ticket_cancel");
                            $temp_sms_msg = str_replace(array_keys($cancel_detail), array_values($cancel_detail), $temp_sms_msg[0]->sms_template_content);
                            $is_send = sendSMS($udetail->mobile_no, $temp_sms_msg, array("ticket_id" => $udetail->ticket_id));
                        }
                    }
                }
            }// End of For Each
        }
    }

    public function minute() {
        
    }

    public function hours() {
        
    }

    public function weekly() {
        
    }

    public function monthy() {
        
    }

    public function yearly() {
        
    }

    public function bos_tkt_details_cron()
    {
        $data = array();
        $this->load->model(array('report_model'));
        $this->load->library("Excel");
        $upsrtc_bal = '0';
        $msrtc_bal  = '0';
        $hrtc_bal  = '0';
        $its_bal = '0';
        $ets_bal = '0';
        $rsrtc_bal = '0';

        $current_date = date('Y-m-d', strtotime('-1 days'));
        $current_month_start_date = date("Y-m")."-01";
        $current_year_start_date = date("Y")."-01-01";
        
        $api_balance_upsrtc = $this->api_balance_log_model->api_balance_cron_daywise_upsrtc($current_date);
        foreach ($api_balance_upsrtc as $key => $value)
        {
            if($value['op_name']=='upsrtc')
            {
                $upsrtc_bal = number_format($value['api_balance'],2);
            }
        }
        $api_balance_msrtc = $this->api_balance_log_model->api_balance_cron_daywise_msrtc($current_date);
        foreach ($api_balance_msrtc as $key => $value) 
        {
          if($value['op_name']=='msrtc')
            {
                $msrtc_bal= number_format($value['api_balance'],2);
            }
        }
        $api_balance_hrtc = $this->api_balance_log_model->api_balance_cron_daywise_hrtc($current_date);
        foreach ($api_balance_hrtc as $key => $value) 
        {
          if($value['op_name']=='hrtc')
            {
                $hrtc_bal= number_format($value['api_balance'],2);
            }
        }
       $api_balance_rsrtc = $this->api_balance_log_model->api_balance_cron_daywise_rsrtc($current_date);
       foreach ($api_balance_rsrtc as $key => $value)
        {
            if($value['op_name']=='rsrtc')
            {
                $rsrtc_bal = number_format($value['api_balance'],2);
            }
        }

        $api_balance_its = $this->api_balance_log_model->api_balance_cron_daywise_its($current_date);
        foreach ($api_balance_its as $key => $value) 
        {
          if($value['op_name']=='its')
            {
                $its_bal= number_format($value['api_balance'],2);
            }
        }
        $api_balance_ets = $this->api_balance_log_model->api_balance_cron_daywise_etravelsmart($current_date);
        foreach ($api_balance_ets as $key => $value) 
        {
          if($value['op_name']== 'Etravelsmart')
            {
                $ets_bal= number_format($value['api_balance'],2);
            }
        }

        $previous_day_ticket_report = $this->report_model->bos_tkt_details_count_cron($current_date);
        $previous_day_user_report = $this->report_model->registered_user($current_date);

        $current_month_ticket_report = $this->report_model->bos_tkt_details_count_cron($current_month_start_date,$current_date,"month");
        $current_month_user_report  = $this->report_model->registered_user($current_month_start_date,$current_date,"month");

        $current_year_ticket_report = $this->report_model->bos_tkt_details_count_cron($current_year_start_date,$current_date,"year");
        $current_year_user_report  = $this->report_model->registered_user($current_year_start_date,$current_date,"year");

        $data[] = array(
                        'Date' => date("d-m-Y",strtotime($current_date)),
                        'No of Registered Users' => $previous_day_user_report['total_users'],
                        'No of Success Tickets (Total)' => $previous_day_ticket_report['success_ticket_cnt'],
                        'No of Success Tickets (Agent)' => $previous_day_ticket_report['agent_ticket_cnt'],
                        'No of Success Tickets (Reg_Usr)' => $previous_day_ticket_report['reg_usr_ticket_cnt'],
                        'No of Success Tickets (Other)' => $previous_day_ticket_report['other_ticket_cnt'],
                        'No of Success Passenger (Total)' => $previous_day_ticket_report['success_total_passangers'],
                        'No of Success Passenger (Agent)' => $previous_day_ticket_report['agent_passangers'],
                        'No of Success Passenger (Reg_Usr)' => $previous_day_ticket_report['registerd_passangers'],
                        'No of Success Passenger (Other)' => $previous_day_ticket_report['other_passangers'],
                        'Actual Fare Amount' => $previous_day_ticket_report['total_fare_without_discount'],
                        'Discounted Amount' => $previous_day_ticket_report['discount_value'],
                        'Commission Amount' => $previous_day_ticket_report['total_commission'],
                        'Total Revenue' => $previous_day_ticket_report['total_fare_without_discount'] - $previous_day_ticket_report['discount_value'],
                        'No of Cancel Tickets' => $previous_day_ticket_report['cancel_ticket_cnt'],
                        'No of Passenger(Cancel)' => $previous_day_ticket_report['cancel_total_passangers'],
                        'Cancellation Charges' => $previous_day_ticket_report['total_cancel_charge'],
                        'Refund Amount' => $previous_day_ticket_report['total_refund_amount'],
                        'Commission Amount' => $previous_day_ticket_report['total_commission'],
                        'Total Revenue' => $previous_day_ticket_report['total_fare_without_discount'] - $previous_day_ticket_report['discount_value'],
                        'MSRTC Balance'=>$msrtc_bal,
                        'UPSRTC Balance'=>$upsrtc_bal,
                        'HRTC Balance'=>$hrtc_bal,
                        'RSRTC Balance'=>$rsrtc_bal,
                        'ETRAVELSMART Balance'=>$ets_bal,
                        'ITS Balance'=>$its_bal,

                        );

        $data[] = array(
                        'Date' => date("F Y",strtotime($current_month_start_date)),
                        'No of Registered Users' => $current_month_user_report['total_users'],
                        'No of Success Tickets (Total)' => $current_month_ticket_report['success_ticket_cnt'],
                        'No of Success Tickets (Agent)' => $current_month_ticket_report['agent_ticket_cnt'],
                        'No of Success Tickets (Reg_Usr)' => $current_month_ticket_report['reg_usr_ticket_cnt'],
                        'No of Success Tickets (Other)' => $current_month_ticket_report['other_ticket_cnt'],
                        'No of Success Passenger (Total)' => $current_month_ticket_report['success_total_passangers'],
                        'No of Success Passenger (Agent)' => $current_month_ticket_report['agent_passangers'],
                        'No of Success Passenger (Reg_Usr)' => $current_month_ticket_report['registerd_passangers'],
                        'No of Success Passenger (Other)' => $current_month_ticket_report['other_passangers'],
                        'Actual Fare Amount' => $current_month_ticket_report['total_fare_without_discount'],
                        'Discounted Amount' => $current_month_ticket_report['discount_value'],
                        'Commission Amount' => $current_month_ticket_report['total_commission'],
                        'Total Revenue' => $current_month_ticket_report['total_fare_without_discount'] - $current_month_ticket_report['discount_value'],
                        'No of Cancel Tickets' => $current_month_ticket_report['cancel_ticket_cnt'],
                        'No of Passenger(Cancel)' => $current_month_ticket_report['cancel_total_passangers'],
                        'Cancellation Charges' => $current_month_ticket_report['total_cancel_charge'],
                        'Refund Amount' => $current_month_ticket_report['total_refund_amount'],
                        'Commission Amount' => $current_month_ticket_report['total_commission'],
                        'Total Revenue' => $current_month_ticket_report['total_fare_without_discount'] - $current_month_ticket_report['discount_value'],
                        );

        $data[] = array(
                        'Date' => date("Y",strtotime($current_year_start_date)),
                        'No of Registered Users' => $current_year_user_report['total_users'],
                        'No of Success Tickets (Total)' => $current_year_ticket_report['success_ticket_cnt'],
                        'No of Success Tickets (Agent)' => $current_year_ticket_report['agent_ticket_cnt'],
                        'No of Success Tickets (Reg_Usr)' => $current_year_ticket_report['reg_usr_ticket_cnt'],
                        'No of Success Tickets (Other)' => $current_year_ticket_report['other_ticket_cnt'],
                        'No of Success Passenger (Total)' => $current_year_ticket_report['success_total_passangers'],
                        'No of Success Passenger (Agent)' => $current_year_ticket_report['agent_passangers'],
                        'No of Success Passenger (Reg_Usr)' => $current_year_ticket_report['registerd_passangers'],
                        'No of Success Passenger (Other)' => $current_year_ticket_report['other_passangers'],
                        'Actual Fare Amount' => $current_year_ticket_report['total_fare_without_discount'],
                        'Discounted Amount' => $current_year_ticket_report['discount_value'],
                        'Commission Amount' => $current_year_ticket_report['total_commission'],
                        'Total Revenue' => $current_year_ticket_report['total_fare_without_discount'] - $current_year_ticket_report['discount_value'],
                        'No of Cancel Tickets' => $current_year_ticket_report['cancel_ticket_cnt'],
                        'No of Passenger(Cancel)' => $current_year_ticket_report['cancel_total_passangers'],
                        'Cancellation Charges' => $current_year_ticket_report['total_cancel_charge'],
                        'Refund Amount' => $current_year_ticket_report['total_refund_amount'],
                        'Commission Amount' => $current_year_ticket_report['total_commission'],
                        'Total Revenue' => $current_year_ticket_report['total_fare_without_discount'] - $current_year_ticket_report['discount_value'],
                        );
        
        $this->excel->data_save_in_excel('Bos_tickets_details' . date('Y-m-d', strtotime("-1 days")) . '.xls', $data);

        $mail_array = array();
        $replacements = array();

        $filename = 'Bos_tickets_details' . date('Y-m-d', strtotime("-1 days")) . '.xls';
        $client_email = BOS_TICKET_COUNT_EMAILS;
        $mail_template = $this->mail_bos_ticket_details();
        $mail_array['subject'] = 'Rokad ticket details count';
        $mail_array['from'] = array(COMMON_MAIL_ADDRESS => COMMON_MAIL_LABEL);
        $mail_array['to'] = $client_email;
        $mail_array['message'] = $mail_template;
        $mail_array['attachment'] = base_url() . "cron_ors_report/$filename";
        $result = send_mail($mail_array, $replacements);
    }

    public function mail_bos_ticket_details() {
        $mail_array = '<table width="100%" border="1" style="border-collapse: collapse; border:1px solid #000; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:30px;">
                                                              <tr>
                                                                <td><table width="95%" border="0" style="margin:0 auto;">
                                                                  <tr>
                                                                    <td><img alt="BoS" src="https://www.bookonspot.com/assets/travelo/front/images/ticket_logo.png" width="165" height="35"></td>
                                                                    <td style="float:right; border:none; text-align:center; color:#888888; font-weight:bold; font-size:15px;">
                                                                      
                                                                    </td>
                                                                  </tr>
                                                                  

                                                                  
                                                                  <tr>
                                                                    <td colspan="2" style="border-bottom:1px solid #000; line-height:6px;">&nbsp;</td>
                                                                  </tr>
                                                                  <tr style="line-height:40px;">
                                                                    <td valign="top"><span style="color:#ff940a; font-weight:bold; font-size:12px;">
                                                                      </span> <span style="font-weight:bold; font-size:12px;">Please find the Rokad ticket details count attachested to this mail.<br>
                                                                    </span></td>
                                                                   
                                                                  </tr>
                                                                </table></td>
                                                              </tr> </table>
                                                                </td>
                                                              </tr>


                                                                 
                                                              </table>';
        // "to" => "suraj.trimax@gmail.com"


        return $mail_array;

        // $this->load->view('welcome_message');
    }
    
    public function get_balance_details()
    {
      $ets_balance = $this->etravelsmart_api->getMyPlanAndBalance();
      if($ets_balance)
      {
          $ets_balance = ["api_provider_id" => 1,"api_provider_name" => "Bookonspot","op_id" => 6,"op_name" =>"Etravelsmart","api_balance" => $ets_balance->balanceAmount] ;
          $this->api_balance_log_model->insert($ets_balance);
      }

      $its_balance  = $this->its_api->GetCurrentAccountBalance();
      if($its_balance)
      {
          $its_balance = ["api_provider_id" => 1,"api_provider_name" => "Bookonspot","op_id" => 5,"op_name" =>"its","api_balance" => $its_balance['Balance']] ;
          $this->api_balance_log_model->insert($its_balance);
      }

      $upsrtc_balance = $this->upsrtc_api->getBalance();
      if(isset($upsrtc_balance["UserBalanceResponse"]) && isset($upsrtc_balance["UserBalanceResponse"]["balance"]))
      {
          $up_balance = ["api_provider_id" => 1,"api_provider_name" => "Bookonspot","op_id" => 2,"op_name" =>"upsrtc","api_balance" => $upsrtc_balance["UserBalanceResponse"]["balance"]];
          $this->api_balance_log_model->insert($up_balance);
      }  
    
      $msrtc_balance  = $this->msrtc_api->getBalance();
      
      if(isset($msrtc_balance["UserBalanceResponse"]) && isset($msrtc_balance["UserBalanceResponse"]["balance"]))
      {
          $ms_balance = ["api_provider_id" => 1,"api_provider_name" => "Bookonspot","op_id" => 3,"op_name" =>"msrtc","api_balance" => $msrtc_balance["UserBalanceResponse"]["balance"]];
          $this->api_balance_log_model->insert($ms_balance);
      }
     
      $hrtc_balance = $this->hrtc_api->getBalance();
      if($hrtc_balance)
      {
          $hr_balance = ["api_provider_id" => 1,"api_provider_name" => "Bookonspot","op_id" => 4,"op_name" =>"hrtc","api_balance" => $hrtc_balance] ;
          $this->api_balance_log_model->insert($hr_balance);
      }
      
      $rsrtc_balance  = $this->rsrtc_api->getBalance();
      if($rsrtc_balance)
      {
          $rs_balance = ["api_provider_id" => 1,"api_provider_name" => "Bookonspot","op_id" => 1,"op_name" =>"rsrtc","api_balance" => $rsrtc_balance['UserBalanceResponse']['balance']] ;
          $this->api_balance_log_model->insert($rs_balance);
      }
    
   }

    public function regenerate_msrtc_services()
    {
        ini_set('memory_limit', '-1');
        $last_sync_date =  $this->travel_destinations_model->get_last_sync_date(3);
        $last_sync_date = $last_sync_date[0]->sch_departure_date;
        

        $begin  = new DateTime( date('Y-m-d', strtotime($last_sync_date."+1day")) );
        $end    = new DateTime( date('Y-m-d', strtotime($last_sync_date."+2day")) );

        /*$begin  = new DateTime('2016-10-29');
        $end    = new DateTime('2016-10-30');*/

        $interval   = DateInterval::createFromDateString('1 day');
        $period     = new DatePeriod($begin, $interval, $end);

        foreach($period as $dt)
        {
            $dep_date = $dt->format( "Y-m-d");
            $current_data = $this->master_services_model->where([
                                                                        "op_id" => $this->config->item("msrtc_operator_id"),
                                                                        "service_effective_from <= " => $dt->format( "Y-m-d"),
                                                                        "service_effective_till >= " => $dt->format( "Y-m-d")
                                                                      ])
                                                                ->find_all();
            foreach ($current_data as $key => $value)
            {
                $travel_destinations_arr = [];
                $travel_destinations_arr["trip_no"]             = $value->trip_no;
                $travel_destinations_arr["boarding_stop_id"]    = $value->boarding_stop_id;
                $travel_destinations_arr["destination_stop_id"] = $value->destination_stop_id;
                $travel_destinations_arr["sch_departure_date"]  = $dt->format( "Y-m-d");
                $travel_destinations_arr["sch_departure_tm"]    = $value->sch_departure_tm;

                /*$travel_destinations_arr["start_stop_sch_departure_date"]    = date('Y-m-d H:i:s', strtotime($dt->format( "Y-m-d").' '.$value->start_stop_sch_departure_date) );*/
                $travel_destinations_arr["start_stop_sch_departure_date"]      = date('Y-m-d H:i:s', strtotime($dt->format( "Y-m-d").' '.date("H:i:s",strtotime($value->start_stop_sch_departure_date))) );

                $searvice_arr_day = ($value->day > 0) ? ($value->day -1 ) : $value->day;
                /*$travel_destinations_arr["end_stop_arrival_date"]          = date('Y-m-d H:i:s', strtotime($dep_date.' '.$value->arrival_tm. '+'.$searvice_arr_day.'day') );*/
                $travel_destinations_arr["end_stop_arrival_date"]            = date('Y-m-d H:i:s', strtotime($dep_date.' '.date("H:i:s",strtotime($value->end_stop_arrival_date)). '+'.$searvice_arr_day.'day') );

                $travel_destinations_data = $this->travel_destinations_model
                                                 ->where($travel_destinations_arr)
                                                 ->find_all();
                if(!$travel_destinations_data)
                {
                    $travel_destinations_arr["boarding_stop_name"]      = $value->boarding_stop_name;
                    $travel_destinations_arr["destination_stop_name"]   = $value->destination_stop_name;
                    $travel_destinations_arr["arrival_tm"]              = isset($value->arrival_tm) ? $value->arrival_tm : '00:00:00';
                    $travel_destinations_arr["seat_fare"]               = $value->seat_fare;
                    $travel_destinations_arr["senior_citizen_fare"]     = $value->senior_citizen_fare;
                    $travel_destinations_arr["child_seat_fare"]         = $value->child_seat_fare;
                    $travel_destinations_arr["sleeper_fare"]            = isset($value->sleeper_fare) ? $value->sleeper_fare : 0.00;
                    $travel_destinations_arr["day"]                     = $value->day;

                    $arr_day = ($value->day > 0) ? ($value->day -1 ) : $value->day;
                    $travel_destinations_arr["arrival_date"]          = date('Y-m-d H:i:s', strtotime($dep_date.' '.$value->arrival_tm. '+'.$arr_day.'day') );
                    $this->travel_destinations_model->insert($travel_destinations_arr);
                }
            }
        }
    }
    
    public function getccavenue_details()
    {
        $date  = date('d-m-Y');
        $paymentgateway_ref_no =$this->tickets_model->paymentgateway_ref_no($date);    
        
        if($paymentgateway_ref_no)
        {
            foreach ($paymentgateway_ref_no as $key => $value) 
            {
                $ccavenue_array_data  = array(
                                                "reference_no" => $value->pg_tracking_id,
                                                "ticket_id" => $value->ticket_id,
                                                );
                $ccavenue_data = $this->cca_payment_gateway->GetCcavenueData($ccavenue_array_data);
                if($ccavenue_data['status'] == "success")
                {
                    $data['ticket_id']              =   $ccavenue_array_data['ticket_id'];
                    $data['pg_tracking_id']         =   $ccavenue_data['response']->reference_no;
                    $data['order_bill_country']     =   $ccavenue_data['response']->order_bill_country;
                    $data['order_fee_flat']         =   $ccavenue_data['response']->order_fee_flat;
                    $data['order_bank_ref_no']      =   $ccavenue_data['response']->order_bank_ref_no;
                    $data['order_capt_amt']         =   $ccavenue_data['response']->order_capt_amt;
                    $data['order_bill_email']       =   $ccavenue_data['response']->order_bill_email;
                    $data['order_gtw_id']           =   $ccavenue_data['response']->order_gtw_id;
                    $data['order_no']               =   $ccavenue_data['response']->order_no;
                    $data['order_bill_state']       =   $ccavenue_data['response']->order_bill_state;
                    $data['order_ship_country']     =   $ccavenue_data['response']->order_ship_country;
                    $data['order_ship_address']     =   $ccavenue_data['response']->order_ship_address;
                    $data['order_tax']              =   $ccavenue_data['response']->order_tax;
                    $data['order_bank_response']    =   $ccavenue_data['response']->order_bank_response;
                    $data['order_bill_zip']         =   $ccavenue_data['response']->order_bill_zip;
                    $data['order_device_type']      =   $ccavenue_data['response']->order_device_type;
                    $data['order_gross_amt']        =   $ccavenue_data['response']->order_gross_amt;
                    $data['order_bill_city']        =   $ccavenue_data['response']->order_bill_city;
                    $data['order_fraud_status']     =   $ccavenue_data['response']->order_fraud_status;
                    $data['order_bill_tel']         =   $ccavenue_data['response']->order_bill_tel;
                    $data['order_ip']               =   $ccavenue_data['response']->order_ip;
                    $data['order_discount']         =   $ccavenue_data['response']->order_discount;
                    $data['order_TDS']              =   $ccavenue_data['response']->order_TDS;
                    $data['status']                 =   $ccavenue_data['response']->status;
                    $data['order_bill_name']        =   $ccavenue_data['response']->order_bill_name;
                    $data['order_ship_state']       =   $ccavenue_data['response']->order_ship_state;
                    $data['order_amt']              =   $ccavenue_data['response']->order_amt;
                    $data['order_card_name']        =   $ccavenue_data['response']->order_card_name;
                    $data['order_ship_city']        =   $ccavenue_data['response']->order_ship_city;
                    $data['order_option_type']      =   $ccavenue_data['response']->order_option_type;
                    $data['order_status']           =   $ccavenue_data['response']->order_status;
                    $data['order_ship_name']        =   $ccavenue_data['response']->order_ship_name;
                    $data['order_status_date_time'] =   $ccavenue_data['response']->order_status_date_time;
                    $data['order_fee_perc_value']   =   $ccavenue_data['response']->order_fee_perc_value;
                    $data['order_ship_tel']         =   $ccavenue_data['response']->order_ship_tel;
                    $data['order_date_time']        =   $ccavenue_data['response']->order_date_time;
                    $data['order_ship_zip']         =   $ccavenue_data['response']->order_ship_zip;
                    $data['order_notes']            =   $ccavenue_data['response']->order_notes;
                    $data['order_bill_address']     =   $ccavenue_data['response']->order_bill_address;
                    $data['order_currncy']          =   $ccavenue_data['response']->order_currncy;

                    $insert_ccavenue_data = $this->payment_gateway_transaction_model->insert($data);
                    
                    $query = $this->db->get_where('payment_gateway_transaction', 
                             array('pg_tracking_id' => $ccavenue_data['response']->reference_no )); 
                    $count = $query->num_rows();              
                    if ($count === 0) 
                    {                            
                      $insert_ccavenue_data = $this->payment_gateway_transaction_model->insert($data);        
                    }
                }
                else
                {
                    $custom_error_mail = array(
                                                "custom_error_subject" => ENVIRONMENT." - Incomplete Information CCAvenue",
                                                "custom_error_message" => json_encode($ccavenue_data)
                                                );
                    $this->common_model->developer_custom_error_mail($custom_error_mail);
                }
            }
        }
        else
        {
            show("No tickets Found",1);
        }
    }

     public function getpayId_details($payid_data)
     {
        if($payid_data['status'] == "success")
        {
            $payid_data['error_code']       = $payid_data['response']->error_code;
            $payid_data['error_desc']       = $payid_data['response']->error_desc;
            $payid_data['pay_id']           = $payid_data['response']->pay_id;
            $payid_data['amt_payable']      = $payid_data['response']->pay_id_txn_details_list->pay_id_txn_details['0']->amt_payable;
            $payid_data['bill_email']       = $payid_data['response']->pay_id_txn_details_list->pay_id_txn_details['0']->bill_email;
            $payid_data['fees']             = $payid_data['response']->pay_id_txn_details_list->pay_id_txn_details['0']->fees;
            $payid_data['bill_name']        = $payid_data['response']->pay_id_txn_details_list->pay_id_txn_details['0']->bill_name;
            $payid_data['order_no']         = $payid_data['response']->pay_id_txn_details_list->pay_id_txn_details['0']->order_no;
            $payid_data['currency']         = $payid_data['response']->pay_id_txn_details_list->pay_id_txn_details['0']->currency;
            $payid_data['amount']           = $payid_data['response']->pay_id_txn_details_list->pay_id_txn_details['0']->amount;
            $payid_data['tax']              = $payid_data['response']->pay_id_txn_details_list->pay_id_txn_details['0']->tax;
            $payid_data['ccavenue_ref_no']  = $payid_data['response']->pay_id_txn_details_list->pay_id_txn_details['0']->ccavenue_ref_no;
            $payid_data['bank_ref_no']      = $payid_data['response']->pay_id_txn_details_list->pay_id_txn_details['0']->bank_ref_no;
            $payid_data['date_time']        = $payid_data['response']->pay_id_txn_details_list->pay_id_txn_details['0']->date_time;
            $payid_data['txn_type']         = $payid_data['response']->pay_id_txn_details_list->pay_id_txn_details['0']->txn_type;
            $payid_data['sub_acc_id']       = $payid_data['response']->pauyout_summary_list->pauyout_summary_details['0']->sub_acc_id;

            $insert_payid_data = $this->payid_summary_details_model->insert($payid_data);
        }
        else
        {
            $custom_error_mail = array(
                                "custom_error_subject" => ENVIRONMENT." - Incomplete payIdDetails From  CCAvenue",
                                "custom_error_message" => json_encode($payid_data)
                                );
            $this->common_model->developer_custom_error_mail($custom_error_mail);
        }
    } 

    
    // Failure topup Wallet Refund //
    
        public function wallet_failure_topup_refund()
    {
        $this->load->library("Excel");
        $current_date = date('Y-m-d', strtotime('-1 days'));
        $walletdata = $this->wallet_topup_model->failure_wallet_transactions($current_date);

        if($walletdata)
        {
            $fields = array("Sr No.","User Id","User Name","User Email", "Amount", "Payment Transaction Id", "PG Trackig Id","Track Id", "Topup by", "Bank Name","Transaction Date","Transaction Status");
            
            $sr_no = 1;
           
            foreach ($walletdata as $key => $value) {
                 
                    $data[] = array('Sr no' => $sr_no,
                                    'User Id' => $value['user_id'],
                                    'User Name' => $value['display_name'],
                                    'User Email' => $value['email'],
                                    'Amount' => $value['amount'],
                                    'Payment Transaction Id' => $value['payment_transaction_id'],
                                    'PG Trackig Id' => $value['pg_track_id'],
                                    'Track Id' => $value['track_id'],
                                    'Topup by' => $value['topup_by'],
                                    'Bank Name' => $value['bank_name'],
                                    'Transaction Date' => $value['transaction_date'],
                                    'Transaction Status' => $value['transaction_status'],
                                   );
                    $sr_no++;
                }
        }

        $this->excel->data_save_in_excel('wallet_failure_topup_refund' . date('Y-m-d') . '.xls', $data);

        
        $mail_array = array();
        $replacements = array();

        $filename = 'wallet_failure_topup_refund' . date('Y-m-d') . '.xls';
        eval(WALLET_FAILURE_TRANS_EMAILS);
        $mail_array['subject'] = "(".ENVIRONMENT.") Wallet Failure Topups";
        $mail_array['from'] = array(COMMON_MAIL_ADDRESS => COMMON_MAIL_LABEL);
        $mail_array['to'] = $wallet_failure_trans_emails;
        $mail_array['message'] = 'Wallet Failure Topups';
        $mail_array['attachment'] = base_url() . "cron_ors_report/$filename";
        $result = send_mail($mail_array, $replacements);
        exit;
    }

	/*
	 * Author : Santosh Warang
	 * Function : sendRokadThresholdLimitEmail()
	 * Detail : Send Email To Rokad About His Threshold Limit
	 */

    public function sendRokadThresholdLimitEmail() {

        $RollId = COMPANY_ROLE_ID;
		$RokadData = $this->common_model->get_value('users','id,first_name,last_name,email','role_id='.$RollId);
		$RokadWallet = $this->common_model->get_value('wallet','id,amt,threshold_limit','user_id='.$RokadData->id);
		$RokadWalletAmount = round($RokadWallet->amt,2);
		$RokadWalletThresholdLimit = $RokadWallet->threshold_limit;

        if ($RokadWalletAmount <= $RokadWalletThresholdLimit) {
			
          $mail_replacement_array =   array (
                                        "{{username}}"         => $RokadData->first_name.' '.$RokadData->last_name,
                                        "{{amount}}"           => $RokadWalletAmount,
                                        "{{threshold_amount}}" => $RokadWalletThresholdLimit,
                                     ); 
						$data['mail_content']       =   str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),ROKAD_BALANCE_ALERT);
						
						
                        $mailbody_array_admin       =   array( "subject"  => "Rokad-Wallet Threshold Limit Reached",
                                                               "message"  => $data['mail_content'],
                                                               "to"       => rokad_decrypt($RokadData->email,$this->config->item('pos_encryption_key'))
															);
                        $mail_result_admin          =   send_mail($mailbody_array_admin);  
        }
    }	
	
	
	/*
	 * Author : Santosh Warang
	 * Function : getWalletAmountTicketCancel()
	 * Detail : update wallet amount after ticket cancel
	 */

    public function getWalletAmountTicketCancel() {

        $TicketCancelData = $this->tickets_model->getCancelTicketData();
		
		foreach($TicketCancelData as $Value){
		$CancelTicketId = $Value->id;
		$UserId = $Value->inserted_by;
		$TicketId = $Value->ticket_id;
		$Wallet = $this->common_model->get_value('wallet','id,amt','user_id='.$UserId);
		$WalletAmount = round($Wallet->amt,2);
		$AcualRefundPaid = $Value->actual_refund_paid;
		$AMOUNT = $WalletAmount + $AcualRefundPaid;
		if($AcualRefundPaid){
		if($AMOUNT < WALLET_LIMIT){
			
		$UserWalletId = $Wallet->id;
		$wallet_to_update = array(
							  'amt' => $AMOUNT,
							  'updated_by'=> $UserId,
							  'updated_date'=> date('Y-m-d h:m:s')
							  );
		$is_cancle_wallet_updated = $this->wallet_model->update($UserWalletId, $wallet_to_update);
	
		$title = array('User Id','Amount','Amount Befor','Amount After','Topup Id','Ticket Id','Date','Detail');
		$val = array ($UserId,$AcualRefundPaid,$WalletAmount,$AMOUNT,'',$TicketId,date("Y-m-d H:i:s"),'Function Detail : cancel_booking/cancel_booking, Detail :Add  Amount into wallet against ticket cancel process');
		$FileName = "wallet/user_wallet_detail/wallet_".$UserId.".csv";
		make_csv($FileName,$title,$val);
		$transaction_no = substr(hexdec(uniqid()), 4, 12);
		if($is_cancle_wallet_updated) {
			$wallet_trans_detail['w_id'] = $UserWalletId;
			$wallet_trans_detail['amt'] = $AcualRefundPaid;
			$wallet_trans_detail['comment'] = 'ticket cancel Process';
			$wallet_trans_detail['status'] = 'added';
			$wallet_trans_detail['user_id'] = $UserId;
			$wallet_trans_detail['added_by'] = $UserId;
			$wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
			$wallet_trans_detail['amt_before_trans'] = $WalletAmount;
			$wallet_trans_detail['amt_after_trans'] = $AMOUNT;
			$wallet_trans_detail['ticket_id'] = $TicketId;
			$wallet_trans_detail['transaction_type_id'] = '3';
			$wallet_trans_detail['wallet_type'] = 'actual_wallet';
			$wallet_trans_detail['transaction_type'] = 'Credited';
			$wallet_trans_detail['is_status'] = 'Y';
			$wallet_trans_detail['transaction_no'] = $transaction_no;
			
			$wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail);
			
			if($wallet_trans_id) {
				$this->deduct_retailer_commission_for_ticket($TicketId,$UserId);
				
				$cancel_ticket_to_update = array(
							  'is_refund' => '1',
							  'updated_by'=> $UserId,
							  'updated_at'=> date('Y-m-d h:m:s')
							  );
		       $is_cancle_ticket_updated = $this->common_model->update('cancel_ticket_data','id',$CancelTicketId,$cancel_ticket_to_update);
			}
	
		}
		else {
			$custom_error_mail = array(
									"custom_error_subject" => "Wallet Error - Wallet amount not added while processing cancel booking ",
									"custom_error_message" => "Function Name - cancel ticket. Wallet amount not added while processing cancel booking. User id - ".$UserId ." and Ticket id - " . $TicketId
								);

			$this->common_model->developer_custom_error_mail($custom_error_mail);
			$err_data['error'] = " Wallet amount not added while processing cancel booking";
			logging_data("booking/".date("M")."/bos_cancel_booking_from_wallet.log", $err_data,"Wallet not updated");
			$data['flag']   =   '@#failed#@';
			$data['error']  =   "Error in processing request. Please contact customer care.";
		}	
			
		}
		}
		
		}
    }
	
	private function deduct_retailer_commission_for_ticket($TicketId,$UserId) {
        $retailer_commission = $this->commission_calculation_model->retailer_commission_calculation_deduction($TicketId,$UserId);
        if(is_array($retailer_commission) && $retailer_commission[0]->total_commission > 0 ) {
            $this->db->trans_begin();

            $commission_amount = $retailer_commission[0]->total_commission;
            /******* Add Amount in to user wallet *******/ 
            // Get user wallet data 
            $user_wallet_data = $this->wallet_model->get_wallet_detail($UserId);
            if(!empty($user_wallet_data)) {
                $user_actual_wallet_amount = $user_wallet_data[0]->amt;
                $user_updated_wallet_amount = $user_actual_wallet_amount - $commission_amount;
                $transaction_no = substr(hexdec(uniqid()), 4, 12);
                // insert into wallet trans for user
                $wallet_trans_arr = array(
                                    'w_id'                => $user_wallet_data[0]->id,
                                    'amt'                 => $commission_amount,
                                    'user_id'             => $UserId,
                                    'amt_before_trans'    => $user_actual_wallet_amount,    
                                    'amt_after_trans'     => $user_updated_wallet_amount,
                                    'status'              => 'deducted',
                                    'transaction_type'    => 'Debited',
                                    'comment'             => 'Commission deducted for  ticket cancellation',
                                    'wallet_type'         => 'actual_wallet',
                                    'ticket_id'           => $TicketId,
                                    'is_status'           => 'Y',
                                    'transaction_type_id' => 17,
                                    'added_on'            => date('Y-m-d H:i:s'), 
                                    'added_by'            => $UserId,
                                    'transaction_no'      => $transaction_no
                                );
                $wallet_trans_id = $this->common_model->insert('wallet_trans',$wallet_trans_arr);
                
                if($wallet_trans_id){
                    $data = array(
                                'amt'          => $user_updated_wallet_amount,
                                'updated_by'   => $UserId,
                                'updated_date' => date('Y-m-d H:i:s'),
                                'last_transction_id' => $wallet_trans_id
                            );
                    $UpdateId = $this->common_model->update('wallet','user_id',$UserId,$data);

                    // Get rokad wallet data
                    if($UpdateId) {
                   
                        // update commission calculation table 
                        $update_arr = array('cancel_flag' => "Y",
                                            'cancel_flag_date' => date('Y-m-d H:i:s')
                                        );

                        $this->commission_calculation_model->update(array("user_id" => $UserId, "ticket_id" => $TicketId),$update_arr);
                        $data['msg'] = "success";  
                    }
                    else {
                        $custom_error_mail = array(
                                                "custom_error_subject" => " VVVVVIMP commission for retailer not deducted for ".$TicketId,
                                                "custom_error_message" => "commission for retailer not deducted for ".$TicketId." Reason : Commission Amount not update into wallet table , but inserted into wallet trans table. wallet trans id : ".$wallet_trans_id.", query for search wallet detail :".$this->db->last_query()
                                              );

                        $this->common_model->developer_custom_error_mail($custom_error_mail);
                        
                        $data['msg'] = "error";
                    }
                }
                else {
                    $custom_error_mail = array(
                                            "custom_error_subject" => " VVVIMP commission for retailer not deducted for ".$TicketId,
                                            "custom_error_message" => "commission for retailer not deducted for ".$TicketId." Reason : Not inserted into wallet trans table, query for search wallet detail :".$this->db->last_query()
                                          );

                    $this->common_model->developer_custom_error_mail($custom_error_mail);
                    
                    $data['msg'] = "error";
                }
            }
            else {
                $custom_error_mail = array(
                                            "custom_error_subject" => " VVVIMP commission for retailer not deducted for ".$TicketId,
                                            "custom_error_message" => "commission for retailer not deducted for ".$TicketId." Reason : wallet detail not found, query for search wallet detail :".$this->db->last_query()
                                          );

                $this->common_model->developer_custom_error_mail($custom_error_mail);
                
                $data['msg'] = "error";
            }

            if ($this->db->trans_status() === FALSE) {
                show(error_get_last());
                $this->db->trans_rollback();
            }
            else {
                $this->db->trans_commit();
            } 
        }
        else {
            $custom_error_mail = array(
                                        "custom_error_subject" => " VVVIMP commission detail not found while cancel ticket for ".$TicketId,
                                        "custom_error_message" => "commission detail not found for ".$TicketId." Query for search commission calculation detail :".$this->db->last_query()
                                      );

            $this->common_model->developer_custom_error_mail($custom_error_mail);
            
            $data['msg'] = "error";
        }

        return $data;
    }
     
     public function add_Topup_balance_to_wallet()
    {
       $find_remaingbal= $this->wallet_topup_trans_model->find_all();
       if(!empty($find_remaingbal))
       {
          foreach ($find_remaingbal as $key => $value) 
          {
              $UserId = $value->user_id;
              $request_touserid = $value->request_touserid;

              $walletdata = $this->wallet_model->where('user_id',$value->user_id)->find_all();
              $req_from_wallet_balance = $walletdata[0]->amt ? $walletdata[0]->amt : '0';
              $req_from_rem_balance = $walletdata[0]->remaining_bal ? $walletdata[0]->remaining_bal : '0';
              $req_from_cum_balance = $walletdata[0]->cummulative_bal ? $walletdata[0]->cummulative_bal : '0' ;

              $req_towallet = $this->wallet_model->where('user_id',$value->request_touserid)->find_all();
              $req_to_wallet_balance = $req_towallet[0]->amt ? $req_towallet[0]->amt : '0';
              $req_to_rem_balance = $req_towallet[0]->remaining_bal ? $req_towallet[0]->remaining_bal : '0';
              $req_to_cum_balance = $req_towallet[0]->cummulative_bal ? $req_towallet[0]->cummulative_bal : '0';
              
              $topup_amount = $value->topup_amount;
              $wallet_amount = $req_from_wallet_balance;
              $remaining_bal = $req_from_rem_balance;
              $cummulative_bal = $req_from_cum_balance;
              $wallet_bal_after_topup = $topup_amount + $wallet_amount;
              
              $added_amount = '';
              if($wallet_bal_after_topup > WALLET_BAL_LIMIT)
              {
                $topup_amount = $value->topup_amount;
                $wallet_amount = $req_from_wallet_balance;

                $amt_remaining_to_one_lakh = WALLET_BAL_LIMIT - $wallet_amount;
                $added_amount = $amt_remaining_to_one_lakh;
              }
              else
              {
                 $added_amount = $value->topup_amount;
              }

              if($wallet_amount < WALLET_BAL_LIMIT )
              {
                if($remaining_bal > $added_amount)
                {
                        $transaction_no = substr(hexdec(uniqid()), 4, 12);
                        //show($transaction_no,1);
                        $wallet_trans_detail['amt'] = $added_amount;
                        $wallet_trans_detail['w_id'] = $walletdata[0]->id;
                        $wallet_trans_detail['wallet_type'] = 'Actual wallet';
                        $wallet_trans_detail['comment'] = 'Amount Credited from cron';
                        $wallet_trans_detail['status'] = 'added';
                        $wallet_trans_detail['user_id'] = $request_touserid;
                        $wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                        $wallet_trans_detail['amt_before_trans'] = $req_from_wallet_balance;
                        $wallet_trans_detail['amt_after_trans'] = $req_from_wallet_balance + $added_amount;
                        $wallet_trans_detail['remaining_bal_before'] = $req_from_rem_balance;
                        $wallet_trans_detail['remaining_bal_after'] = $req_from_rem_balance - $added_amount;       

                        $wallet_trans_detail['cumulative_bal_before'] = $req_from_cum_balance;
                        $wallet_trans_detail['cumulative_bal_after'] = $req_from_cum_balance - $added_amount;
                        $wallet_trans_detail['transaction_type'] = 'Credited';
                        $wallet_trans_detail['is_status'] = 'Y';
                        $wallet_trans_detail['transaction_no'] = $transaction_no;
                        
                        $wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail); 

                         if($wallet_trans_id){
                                $data = array(
                                            'amt'          => $req_from_wallet_balance + $added_amount,
                                            'remaining_bal'=> $req_from_rem_balance - $added_amount,
                                            'cummulative_bal'=> $req_from_cum_balance - $added_amount,
                                            'updated_by'   => $UserId,
                                            'updated_date' => date('Y-m-d H:i:s'),
                                            'last_transction_id' => $wallet_trans_id
                                        );
                                $UpdateId = $this->common_model->update('wallet','user_id',$UserId,$data);
                         }    
                         if($UpdateId)
                         {
                             $trans_no = substr(hexdec(uniqid()), 4, 12);

                             $wallet_trans['amt'] = $added_amount;
                             $wallet_trans['w_id'] = $req_towallet[0]->id;
                             $wallet_trans['wallet_type'] = 'Actual wallet';
                             $wallet_trans['comment'] = 'Amount Credited from cron';
                             $wallet_trans['status'] = 'added';
                             $wallet_trans['user_id'] = $UserId;
                             $wallet_trans['added_on'] = date("Y-m-d H:i:s");
                             $wallet_trans['amt_before_trans'] = $req_to_wallet_balance;
                             $wallet_trans['amt_after_trans'] = $req_to_wallet_balance - $added_amount;
                             $wallet_trans['remaining_bal_before'] = $req_to_rem_balance;
                             $wallet_trans['remaining_bal_after'] = $req_to_rem_balance;
                             $wallet_trans['cumulative_bal_before'] = $req_to_cum_balance;
                             $wallet_trans['cumulative_bal_after'] = $req_to_cum_balance;
                             $wallet_trans['transaction_type'] = 'Credited';
                             $wallet_trans['is_status'] = 'Y';
                             $wallet_trans['transaction_no'] = $trans_no;
                            
                            $to_wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans); 

                             if($to_wallet_trans_id){
                                 $data = array(
                                            'amt'          => $req_to_wallet_balance - $added_amount,
                                            'remaining_bal'=> $req_to_rem_balance,
                                            'cummulative_bal'=> $req_to_cum_balance - $added_amount,
                                            'updated_by'   => $UserId,
                                            'updated_date' => date('Y-m-d H:i:s'),
                                            'last_transction_id' => $wallet_trans_id
                                        );
                                 $UpdateFrom = $this->common_model->update('wallet','user_id',$UserId,$data);
                            }    
                     }
                }           
                else
                {
                  // echo 'remaining balance should be greater than 1,00,000 lakh';
                }
              }
              else
              {
                //echo 'wallet balance should be less than 1,00,000 lakh';
              }
          }
       }
    }
    /*
     * Author : jayvant butere
     * Function : add_commission_to_msrtc_retailer()
     * Detail : Add commission to wallet
     */

    public function msrtcRetailerCommissionDistribution()
    {   
        $this->load->model(array('services/service_2/service_2_commission_detail_model','retailer_service_mapping_model','assign_package_log_model','package_detail_model','cb_msrtc_agent_transaction_model'));

        $msrtcRetailer = $this->users_model->getMsrtcRetailer();
        if(!empty($msrtcRetailer))
        {
           foreach ($msrtcRetailer as $key => $value) {
               
               $agent_id = $value['id'];
               $agent_code = $value['agent_code'];
               $distributor_id = $value['level_5'];
               $Area_dis_id = $value['level_4'];
               $master_dis_id = $value['level_3'];
               $company_id = $value['level_2'];
               $trimax_id = $value['level_1'];
               log_data('msrtc/commission_cron/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/'.$agent_id.'/commission_record'.'.log',$value,'Retailer data'); 

               $MasterDistrbutorDetails = $this->users_model->where('id',$master_dis_id)->find_all();

               log_data('msrtc/commission_cron/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/'.$agent_id.'/commission_record'.'.log',$MasterDistrbutorDetails,'Md data'); 
               if($MasterDistrbutorDetails)
               {
                     $packageDetailsForMD = $this->assign_package_log_model->packageDetailsForMD($master_dis_id);
                     $package_id ='';
                     if($packageDetailsForMD)
                     {
                        $package_id = $packageDetailsForMD[0]['package_id'];
                     }else
                     {
                       $defaultPackageId = $this->assign_package_log_model->defaultPackageId();
                       $package_id = $defaultPackageId[0]['id'];
                     }
                log_data('msrtc/commission_cron/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/'.$agent_id.'/commission_record'.'.log',$package_id,'Assign Package Id');    
                 if($package_id)
                 {
                     $pg_where = array('package_id'=>$package_id,'service_id' => MSRTC_SERVICE_ID);
                     $package_details = $this->package_detail_model->where($pg_where)->find_all();
                     $commission_id = $package_details[0]->commission_id;
                     
                     if($commission_id)
                     {
                         $getCommissionData = $this->service_2_commission_detail_model->where('commission_id',$commission_id)->find_all();

                         $trimax_commission = '0';
                         $company_commission ='0';
                         $md_commission = '0';
                         $ad_commission ='0';
                         $dist_commission = '0';
                         $retailer_commission = '0';
                         if($getCommissionData)
                         {
                            $trimax_commission = round($getCommissionData[0]->trimax_commission,2);
                            $company_commission = round($getCommissionData[0]->company_commission,2);
                            $md_commission = round($getCommissionData[0]->md_commission,2);
                            $ad_commission = round($getCommissionData[0]->ad_commission,2);
                            $dist_commission = round($getCommissionData[0]->dist_commission,2);
                            $retailer_commission = round($getCommissionData[0]->retailer_commission,2);
                         }

                        log_data('msrtc/commission_cron/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/'.$agent_id.'/commission_record'.'.log',$getCommissionData,'Commission Percentage for Users');    
                          
                        /*$AgentTicketCollectionData = $this->cb_msrtc_agent_transaction_model->getRetailerTicketCollection($agent_id);*/
                        
                        $AgentTicketCollectionData = $this->cb_msrtc_agent_transaction_model->getRetailerTicketCollectionFromCbComm($agent_code);
                        if(!empty($AgentTicketCollectionData))
                        {
                            $Appcommission_amount = $AgentTicketCollectionData[0]['comm_amount'];
                            
                            $this->config->load('commission_discount_config');
                            $tds = $this->config->item('commission_applicable_tds');
                            
                            if(empty($tds))
                            {
                                $tds = '0.00';
                            }
                            $commission_amount = $Appcommission_amount - $tds;
                            log_data('msrtc/commission_cron/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/'.$agent_id.'/commission_record'.'.log',$commission_amount,'Commission Amount');  
                            if($commission_amount && $commission_amount > 0)
                            {
                                //$trimaxAmt = getPercentageAmount($trimax_commission,$commission_amount);
                                $companyAmt = getPercentageAmount($company_commission,$commission_amount);
                                $mdAmt = getPercentageAmount($md_commission,$commission_amount);
                                $adAmt = getPercentageAmount($ad_commission,$commission_amount);
                                $disAmt = getPercentageAmount($dist_commission
                                    ,$commission_amount);
                                $retAmt = getPercentageAmount($retailer_commission,$commission_amount);
                                $trimaxAmt = $commission_amount - $companyAmt - $mdAmt - $adAmt - $disAmt - $retAmt;
                                show($trimaxAmt);
                            }
                        }
                        $distribution_array = array(
                            'trimax_commission' => $trimaxAmt,
                            'company_commission' => $companyAmt,
                            'master distributor_commission' => $mdAmt,
                            'area distributor_commission' => $adAmt,
                            'distributor_commission' => $disAmt,
                            'retailer_commission' => $retAmt,
                            );
                        log_data('msrtc/commission_cron/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/'.$agent_id.'/commission_record'.'.log',$distribution_array,'Indivisually commission distribution');    

                        $this->db->trans_begin();

                        if($retAmt && $retAmt > 0)
                        {
                            $Retwalletdata = $this->wallet_model->where('user_id',$agent_id)->find_all();
                            if($Retwalletdata)
                            {
                               $retWalletId = $Retwalletdata[0]->id;
                               $RetCurrentAmt = $Retwalletdata[0]->amt; 
                            }
                            $retAmtafterAdd = $RetCurrentAmt + $retAmt;
                            $retTranNo = substr(hexdec(uniqid()), 4, 12);

                            $ret_wallet_trans_detail['transaction_no'] = $retTranNo;
                            $ret_wallet_trans_detail['w_id'] = $retWalletId;
                            $ret_wallet_trans_detail['amt'] =  $retAmt;
                            $ret_wallet_trans_detail['wallet_type'] =  'actual_wallet';
                            $ret_wallet_trans_detail['comment'] = 'Msrtc commission amount added';
                            $ret_wallet_trans_detail['status'] = 'credited';
                            $ret_wallet_trans_detail['user_id'] = $agent_id;
                            $ret_wallet_trans_detail['added_by'] = 'monthly cron';
                            $ret_wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                            $ret_wallet_trans_detail['amt_before_trans'] = $RetCurrentAmt;
                            $ret_wallet_trans_detail['amt_after_trans'] = $retAmtafterAdd;
                            $ret_wallet_trans_detail['transaction_type_id'] = '15';
                            $ret_wallet_trans_detail['transaction_type'] = 'credited';
                            $ret_wallet_trans_detail['is_status'] = 'Y';
                            
                            log_data('msrtc/commission_cron/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/'.$agent_id.'/commission_record'.'.log',$ret_wallet_trans_detail,'retailer wallet transaction commission data');    

                            $ret_wallet_trans_id = $this->wallet_trans_model->insert($ret_wallet_trans_detail);

                            if($ret_wallet_trans_id)
                            {
                                 $r_data = array(
                                            'amt'          => $retAmtafterAdd,
                                            'updated_by'   => $this->session->userdata('user_id'),
                                            'comment'      => 'commission amount added by monthly cron',
                                            'updated_date' => date('Y-m-d H:i:s'),
                                            'last_transction_id' => $ret_wallet_trans_id
                                        );
                                $RetUpdateWallet = $this->wallet_model->updateWalletData($r_data,$agent_id);                   

                                if($RetUpdateWallet)
                                {
                                    $Diswalletdata = $this->wallet_model->where('user_id',$distributor_id)->find_all();
                                    if($Diswalletdata)
                                    {
                                       $disWalletId = $Diswalletdata[0]->id;
                                       $disCurrentAmt = $Diswalletdata[0]->amt; 
                                    }
                                    $disAmtafterAdd = $disCurrentAmt + $disAmt;
                                    $disTranNo = substr(hexdec(uniqid()), 4, 12);

                                    $dis_wallet_trans_detail['transaction_no'] = $disTranNo;
                                    $dis_wallet_trans_detail['w_id'] = $disWalletId;
                                    $dis_wallet_trans_detail['amt'] =  $disAmt;
                                    $dis_wallet_trans_detail['wallet_type'] =  'actual_wallet';
                                    $dis_wallet_trans_detail['comment'] = 'Msrtc commission amount added for agent '.$value['first_name'].' '.$value['last_name'];
                                    $dis_wallet_trans_detail['status'] = 'credited';
                                    $dis_wallet_trans_detail['user_id'] = $distributor_id;
                                    $dis_wallet_trans_detail['added_by'] = 'monthly cron';
                                    $dis_wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                                    $dis_wallet_trans_detail['amt_before_trans'] = $disCurrentAmt;
                                    $dis_wallet_trans_detail['amt_after_trans'] = $disAmtafterAdd;
                                    $dis_wallet_trans_detail['transaction_type_id'] = '15';
                                    $dis_wallet_trans_detail['transaction_type'] = 'credited';
                                    $dis_wallet_trans_detail['is_status'] = 'Y';
                                    
                                    log_data('msrtc/commission_cron/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/'.$agent_id.'/commission_record'.'.log',$dis_wallet_trans_detail,'distributor wallet transaction commission data');    

                                    $dis_wallet_trans_id = $this->wallet_trans_model->insert($dis_wallet_trans_detail);

                                    if($dis_wallet_trans_id)
                                    {
                                        $d_data = array(
                                            'amt'          => $disAmtafterAdd,
                                            'updated_by'   => 'monthly cron',
                                            'comment'      => 'commission amount added by monthly cron',
                                            'updated_date' => date('Y-m-d H:i:s'),
                                            'last_transction_id' => $dis_wallet_trans_id
                                        );
                                        $disUpdateWallet = $this->wallet_model->updateWalletData($d_data,$distributor_id);

                                        if($disUpdateWallet)
                                        {
                                            $adwalletdata = $this->wallet_model->where('user_id',$Area_dis_id)->find_all();
                                            if($adwalletdata)
                                            {
                                               $adWalletId = $adwalletdata[0]->id;
                                               $adCurrentAmt = $adwalletdata[0]->amt; 
                                            }
                                            $adAmtafterAdd = $adCurrentAmt + $adAmt;
                                            $adTranNo = substr(hexdec(uniqid()), 4, 12);

                                            $ad_wallet_trans_detail['transaction_no'] = $adTranNo;
                                            $ad_wallet_trans_detail['w_id'] = $adWalletId;
                                            $ad_wallet_trans_detail['amt'] =  $adAmt;
                                            $ad_wallet_trans_detail['wallet_type'] =  'actual_wallet';
                                            $ad_wallet_trans_detail['comment'] = 'Msrtc commission amount added for agent '.$value['first_name'].' '.$value['last_name'];
                                            $ad_wallet_trans_detail['status'] = 'credited';
                                            $ad_wallet_trans_detail['user_id'] = $Area_dis_id;
                                            $ad_wallet_trans_detail['added_by'] = 'monthly cron';
                                            $ad_wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                                            $ad_wallet_trans_detail['amt_before_trans'] = $adCurrentAmt;
                                            $ad_wallet_trans_detail['amt_after_trans'] = $adAmtafterAdd;
                                            $ad_wallet_trans_detail['transaction_type_id'] = '15';
                                            $ad_wallet_trans_detail['transaction_type'] = 'credited';
                                            $ad_wallet_trans_detail['is_status'] = 'Y';
                                            
                                            log_data('msrtc/commission_cron/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/'.$agent_id.'/commission_record'.'.log',$ad_wallet_trans_detail,'Area distributor wallet transaction commission data');    

                                            $ad_wallet_trans_id = $this->wallet_trans_model->insert($ad_wallet_trans_detail);

                                            if($ad_wallet_trans_id)
                                            {
                                                $ad_data = array(
                                                    'amt'          => $adAmtafterAdd,
                                                    'updated_by'   => 'monthly cron',
                                                    'comment'      => 'commission amount added by monthly cron',
                                                    'updated_date' => date('Y-m-d H:i:s'),
                                                    'last_transction_id' => $ad_wallet_trans_id
                                                 );
                                               $adUpdateWallet = $this->wallet_model->updateWalletData($ad_data,$Area_dis_id);

                                               if($adUpdateWallet)
                                               {
                                                    $mdwalletdata = $this->wallet_model->where('user_id',$master_dis_id)->find_all();
                                                    
                                                    if($mdwalletdata)
                                                    {
                                                       $mdWalletId = $mdwalletdata[0]->id;
                                                       $mdCurrentAmt = $mdwalletdata[0]->amt; 
                                                    }
                                                    $mdAmtafterAdd = $mdCurrentAmt + $mdAmt;
                                                    $mdTranNo = substr(hexdec(uniqid()), 4, 12);

                                                    $md_wallet_trans_detail['transaction_no'] = $mdTranNo;
                                                    $md_wallet_trans_detail['w_id'] = $mdWalletId;
                                                    $md_wallet_trans_detail['amt'] =  $mdAmt;
                                                    $md_wallet_trans_detail['wallet_type'] =  'actual_wallet';
                                                    $md_wallet_trans_detail['comment'] = 'Msrtc commission amount added for agent '.$value['first_name'].' '.$value['last_name'];
                                                    $md_wallet_trans_detail['status'] = 'credited';
                                                    $md_wallet_trans_detail['user_id'] = $master_dis_id;
                                                    $md_wallet_trans_detail['added_by'] = 'monthly cron';
                                                    $md_wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                                                    $md_wallet_trans_detail['amt_before_trans'] = $mdCurrentAmt;
                                                    $md_wallet_trans_detail['amt_after_trans'] = $mdAmtafterAdd;
                                                    $md_wallet_trans_detail['transaction_type_id'] = '15';
                                                    $md_wallet_trans_detail['transaction_type'] = 'credited';
                                                    $md_wallet_trans_detail['is_status'] = 'Y';
                                                    
                                                    log_data('msrtc/commission_cron/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/'.$agent_id.'/commission_record'.'.log',$md_wallet_trans_detail,'Master distributor wallet transaction commission data');    

                                                    $md_wallet_trans_id = $this->wallet_trans_model->insert($md_wallet_trans_detail);

                                                    if($md_wallet_trans_id)
                                                    {
                                                        $md_data = array(
                                                            'amt'          => $mdAmtafterAdd,
                                                            'updated_by'   => 'monthly cron',
                                                            'comment'      => 'commission amount added by monthly cron',
                                                            'updated_date' => date('Y-m-d H:i:s'),
                                                            'last_transction_id' => $md_wallet_trans_id
                                                         );
                                                        
                                                        $mdUpdateWallet = $this->wallet_model->updateWalletData($md_data,$master_dis_id);

                                                        if($mdUpdateWallet)
                                                        {
                                                            $cmpwalletdata = $this->wallet_model->where('user_id',$company_id)->find_all();
                                                            if($cmpwalletdata)
                                                            {
                                                               $cmpWalletId = $cmpwalletdata[0]->id;
                                                               $cmpCurrentAmt = $cmpwalletdata[0]->amt; 
                                                            }
                                                            $cmpAmtafterAdd = $mdCurrentAmt + $companyAmt;
                                                            $cmTranNo = substr(hexdec(uniqid()), 4, 12);

                                                            $cmp_wallet_trans_detail['transaction_no'] = $cmTranNo;
                                                            $cmp_wallet_trans_detail['w_id'] = $cmpWalletId;
                                                            $cmp_wallet_trans_detail['amt'] =  $companyAmt;
                                                            $cmp_wallet_trans_detail['wallet_type'] =  'actual_wallet';
                                                            $cmp_wallet_trans_detail['comment'] = 'Msrtc commission amount added for agent '.$value['first_name'].' '.$value['last_name'];
                                                            $cmp_wallet_trans_detail['status'] = 'credited';
                                                            $cmp_wallet_trans_detail['user_id'] = $company_id;
                                                            $cmp_wallet_trans_detail['added_by'] = 'monthly cron';
                                                            $cmp_wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                                                            $cmp_wallet_trans_detail['amt_before_trans'] = $cmpCurrentAmt;
                                                            $cmp_wallet_trans_detail['amt_after_trans'] = $cmpAmtafterAdd;
                                                            $cmp_wallet_trans_detail['transaction_type_id'] = '15';
                                                            $cmp_wallet_trans_detail['transaction_type'] = 'credited';
                                                            $cmp_wallet_trans_detail['is_status'] = 'Y';
                                                            
                                                            log_data('msrtc/commission_cron/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/commission_record'.'.log',$cmp_wallet_trans_detail,'company wallet transaction commission data');    

                                                            $cmp_wallet_trans_id = $this->wallet_trans_model->insert($cmp_wallet_trans_detail);
                                                       
                                                            if($cmp_wallet_trans_id)
                                                            {
                                                                 $cmp_data = array(
                                                                    'amt'          => $cmpAmtafterAdd,
                                                                    'updated_by'   => 'monthly cron',
                                                                    'comment'      => 'commission amount added by monthly cron',
                                                                    'updated_date' => date('Y-m-d H:i:s'),
                                                                    'last_transction_id' => $cmp_wallet_trans_id
                                                                  );
                                                        
                                                                $cmpUpdateWallet = $this->wallet_model->updateWalletData($cmp_data,$company_id); 

                                                                if($cmpUpdateWallet)
                                                                {

                                                                    if($trimaxAmt > 0)
                                                                    {
                                                                            $trimaxwalletdata = $this->wallet_model->where('user_id',$trimax_id)->find_all();
                                                                            if($cmpwalletdata)
                                                                            {
                                                                               $triWalletId = $trimaxwalletdata[0]->id;
                                                                               $triCurrentAmt = $trimaxwalletdata[0]->amt; 
                                                                            }
                                                                            $triAmtafterAdd = $triCurrentAmt + $trimaxAmt;
                                                                            $triTranNo = substr(hexdec(uniqid()), 4, 12);

                                                                            $tri_wallet_trans_detail['transaction_no'] = $triTranNo;
                                                                            $tri_wallet_trans_detail['w_id'] = $triWalletId;
                                                                            $tri_wallet_trans_detail['amt'] =  $trimaxAmt;
                                                                            $tri_wallet_trans_detail['wallet_type'] =  'actual_wallet';
                                                                            $tri_wallet_trans_detail['comment'] = 'Msrtc commission amount added for agent '.$value['first_name'].' '.$value['last_name'];
                                                                            $tri_wallet_trans_detail['status'] = 'credited';
                                                                            $tri_wallet_trans_detail['user_id'] = $trimax_id;
                                                                            $tri_wallet_trans_detail['added_by'] = 'monthly cron';
                                                                            $tri_wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                                                                            $tri_wallet_trans_detail['amt_before_trans'] = $triCurrentAmt;
                                                                            $tri_wallet_trans_detail['amt_after_trans'] = $triAmtafterAdd;
                                                                            $tri_wallet_trans_detail['transaction_type_id'] = '15';
                                                                            $tri_wallet_trans_detail['transaction_type'] = 'credited';
                                                                            $tri_wallet_trans_detail['is_status'] = 'Y';
                                                                            
                                                                            log_data('msrtc/commission_cron/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/commission_record'.'.log',$tri_wallet_trans_detail,'trimax wallet transaction commission data');    

                                                                            $trimax_wallet_trans_id = $this->wallet_trans_model->insert($tri_wallet_trans_detail);
                                                                            
                                                                            if($trimax_wallet_trans_id)
                                                                            {
                                                                                $tri_data = array(
                                                                                    'amt'          => $cmpAmtafterAdd,
                                                                                    'updated_by'   => 'monthly cron',
                                                                                    'comment'      => 'commission amount added by monthly cron',
                                                                                    'updated_date' => date('Y-m-d H:i:s'),
                                                                                    'last_transction_id' => $cmp_wallet_trans_id
                                                                                );
                                                                
                                                                                $triUpdateWallet = $this->wallet_model->updateWalletData($tri_data,$trimax_id);

                                                                                if($triUpdateWallet)
                                                                                {
                                                                                     $UpdateCommData = $this->cb_msrtc_agent_transaction_model->UpdateCommStatus($agent_code);
                                                                                     if($UpdateCommData)
                                                                                     {
                                                                                        $this->db->trans_commit();
                                                                                     } 
                                                                                     else {
                                                                                        $this->db->trans_rollback();
                                                                                     }                                  
                                                                                     log_data('msrtc/commission_cron/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/'.$agent_id.'/commission_record'.'.log','Success commission updation against agent '.$value['first_name'].' '.$value['last_name'],'success log');    
                                                                                }
                                                                                else
                                                                                {
                                                                                     log_data('msrtc/commission_cron/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/'.$agent_id.'/commission_record'.'.log','last update entry','success log');    
                                                                                }
                                                                            }
                                                                    }
                                                                   $this->db->trans_commit(); 
                                                                }
                                                                else
                                                                {
                                                                    $this->db->trans_rollback();
                                                                }
                                                            }
                                                        }
                                                    }
                                               }
                                            }    
                                        }
                                    }
                                }
                            }
                        }
                     }                       
                  }  
               }
            }
         }
      }  
      public function DistributeDailyCommAmount()
      {
        $this->load->model(array('services/service_2/service_2_commission_detail_model','retailer_service_mapping_model','assign_package_log_model','package_detail_model','cb_commission_distribution_model'));

        $getDetails = array();
        $getDetails = $this->cron_model->getCommissionData();
        if(!empty($getDetails))
        {
          foreach ($getDetails as $key => $value) {
              $id = $value['id'];
              $ticket_id = $value['ticket_id'];
              $waybill_no = $value['waybill_no'];
              $commission_amount = $value['agent_actual_comm_amt'];
              $agent_code = $value['agent_code'];
              if($agent_code)
              {
                 $UserData = $this->users_model->where('agent_code',$agent_code)->find_all();
                 if($UserData)
                 {
                       $agent_id = $UserData['0']->id;
                       $distributor_id = $UserData['0']->level_5;
                       $Area_dis_id = $UserData['0']->level_4;
                       $master_dis_id = $UserData['0']->level_3;
                       $company_id = $UserData['0']->level_2;
                       $trimax_id = $UserData['0']->level_1;
                    
                       if($master_dis_id)
                       {
                             $this->assign_package_log_model->update_expired_pkg($master_dis_id);
                             $packageDetailsForMD = $this->assign_package_log_model->packageDetailsForMD($master_dis_id);
                             $package_id ='';
                             if($packageDetailsForMD)
                             {
                                $package_id = $packageDetailsForMD[0]['package_id'];
                             }else
                             {
                               $defaultPackageId = $this->assign_package_log_model->defaultPackageId();
                               $package_id = $defaultPackageId[0]['id'];
                             }
                             if($package_id)
                             {
                                 $pg_where = array('package_id'=>$package_id,'service_id' => MSRTC_SERVICE_ID);
                                 $package_details = $this->package_detail_model->where($pg_where)->find_all();
                                 $commission_id = $package_details[0]->commission_id;
                                 $pkg_id = $package_details[0]->package_id;

                                 if($commission_id)
                                 {
                                     $getCommissionData = $this->service_2_commission_detail_model->where('commission_id',$commission_id)->find_all();
                                     $trimax_commission = '0';
                                     $company_commission ='0';
                                     $md_commission = '0';
                                     $ad_commission ='0';
                                     $dist_commission = '0';
                                     $retailer_commission = '0';

                                     if(!empty($getCommissionData))
                                     {
                                        $trimax_commission = round($getCommissionData[0]->trimax_commission,2);
                                        $company_commission = round($getCommissionData[0]->company_commission,2);
                                        $md_commission = round($getCommissionData[0]->md_commission,2);
                                        $ad_commission = round($getCommissionData[0]->ad_commission,2);
                                        $dist_commission = round($getCommissionData[0]->dist_commission,2);
                                        $retailer_commission = round($getCommissionData[0]->retailer_commission,2);
                                     }

                                     if($commission_amount && $commission_amount > 0)
                                     {
                                        //$trimaxAmt = getPercentageAmount($trimax_commission,$commission_amount);
                                        $companyAmt = round(getPercentageAmount($company_commission,$commission_amount),2);
                                        $mdAmt = round(getPercentageAmount($md_commission,$commission_amount),2);
                                        $adAmt = round(getPercentageAmount($ad_commission,$commission_amount),2);
                                        $disAmt = round(getPercentageAmount($dist_commission,$commission_amount),2);
                                        $retAmt = round(getPercentageAmount($retailer_commission,$commission_amount),2);
                                        $trimaxAmount = $commission_amount - $companyAmt - $mdAmt - $adAmt - $disAmt - $retAmt;
                                        $trimaxAmt = ($trimaxAmount >= 0) ? round($trimaxAmount,2) : 0;

                                        $companyAct_amt = round(getGstTdsAmount($companyAmt),2);
                                        $mdAct_amt = round(getGstTdsAmount($mdAmt),2);
                                        $adAct_amt = round(getGstTdsAmount($adAmt),2);
                                        $disAct_amt = round(getGstTdsAmount($disAmt),2);
                                        $retAct_amt = round(getGstTdsAmount($retAmt),2);
                                        $trimaxGstAmt = getGstTdsAmount($commission_amount);
                                        $trimaxFin_amt = $trimaxGstAmt - $companyAct_amt - $mdAct_amt - $adAct_amt - $disAct_amt - $retAct_amt;
                                        $trimaxAct_amt = ($trimaxFin_amt >= 0) ? round($trimaxFin_amt,2) : 0;

                                     }
                                     $distribution_array = array(
                                        'trimax_commission' => $trimaxAmt,
                                        'company_commission' => $companyAmt,
                                        'master distributor_commission' => $mdAmt,
                                        'area distributor_commission' => $adAmt,
                                        'distributor_commission' => $disAmt,
                                        'retailer_commission' => $retAmt,
                                        'trimax_actual_commission' => $trimaxAct_amt,
                                        'company_actual_commission' => $companyAct_amt,
                                        'md_actual_commission' => $mdAct_amt,
                                        'ad_actual_commission' => $adAct_amt,
                                        'distributor_actual_commission' => $disAct_amt,
                                        'retailer_actual_commission' => $retAct_amt,
                                      );
                                    // show($distribution_array,1);
                                    $now = date('Y-m-d H:i:s', time());
                                     
                                    //$comm_distribution_array['ticket_id'] = $ticket_id;
                                    $comm_distribution_array['agent_code'] = $agent_code;
                                    $comm_distribution_array['waybill_no'] =  $waybill_no;
                                    $comm_distribution_array['commission_template_id'] =  $commission_id;
                                    $comm_distribution_array['comm_applicable_amt'] = $commission_amount;
                                    $comm_distribution_array['trimax_id'] = $trimax_id;
                                    $comm_distribution_array['company_id'] = $company_id;
                                    $comm_distribution_array['md_id'] = $master_dis_id;
                                    $comm_distribution_array['ad_id'] = $Area_dis_id;
                                    $comm_distribution_array['di_id'] = $distributor_id;
                                    $comm_distribution_array['agent_id'] = $agent_id;
                                    $comm_distribution_array['trimax_comm'] = $trimaxAmt;
                                    $comm_distribution_array['company_comm'] = $companyAmt;
                                    $comm_distribution_array['md_comm'] = $mdAmt;
                                    $comm_distribution_array['ad_comm'] = $adAmt;
                                    $comm_distribution_array['di_comm'] = $disAmt;
                                    $comm_distribution_array['agent_comm'] = $retAmt;
                                    $comm_distribution_array['package_template_id'] = $pkg_id;
                                    $comm_distribution_array['trimax_actual_comm'] = $trimaxAct_amt;
                                    $comm_distribution_array['company_actual_comm'] = $companyAct_amt;
                                    $comm_distribution_array['md_actual_comm'] = $mdAct_amt;
                                    $comm_distribution_array['ad_actual_comm'] = $adAct_amt;
                                    $comm_distribution_array['di_actual_comm'] = $disAct_amt;
                                    $comm_distribution_array['agent_actual_comm'] = $retAct_amt;
                                    $comm_distribution_array['comm_calculate_date'] = date('Y-m-d',  strtotime("-1 days"));
                                    $comm_distribution_array['tds_percentage'] = TDS_APPLICABLE_MONTHLY_COMMISSION;
                                    $comm_distribution_array['gst_percentage'] = GST_APPLICABLE_MONTHLY_COMMISSION;
                                    $comm_distribution_array['created_at'] = $now;

                                    
                                    log_data('msrtc/daily_comm_distribution_cron/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/commission_record'.'.log',$comm_distribution_array,'Commission Distribution Array');    
                                    
                                    $check_tkt_id = $this->cb_commission_distribution_model->where('waybill_no',$waybill_no)->find_all();
                                    if(empty($check_tkt_id))
                                    {
                                       $dist_id = $this->cb_commission_distribution_model->insert($comm_distribution_array); 
                                       $update_status = $this->cron_model->UpdateCommStatus($id);         
                                       log_data('msrtc/daily_comm_distribution_cron/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/commission_record'.'.log',$dist_id,'Distribution Id');    
                                    }
                                    else
                                    {
                                       log_data('msrtc/daily_comm_distribution_cron/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/commission_record'.'.log',$ticket_id,'ticket id already exist in cb_commission_distribution');    
                                    }
                               }
                         } else {
                            log_data('msrtc/daily_comm_distribution_cron/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/commission_record'.'.log','No package assign or No default package created','Package Assign');
                        }  
                    } 
                 }
              }
           }
        }
        $subject = "Daily Commission Distribution Cron on ".ENVIRONMENT." server";
        $msg = "Daily Commission Distribution Cron is run at ". date('d-m-Y H:i:s') ." successfully.";
        $this->mail($subject,$msg);
     }

   /** public function DistributeDailyCommAmount()
      {
        $this->load->model(array('services/service_2/service_2_commission_detail_model','retailer_service_mapping_model','assign_package_log_model','package_detail_model','cb_commission_distribution_model'));

        $getDetails = array();
        $getDetails = $this->cron_model->getCommissionData();
        if(!empty($getDetails))
        {
          foreach ($getDetails as $key => $value) {
              $id = $value['id'];
              $ticket_id = $value['ticket_id'];
              $waybill_no = $value['waybill_no'];
              $commission_amount = $value['agent_actual_comm_amt'];
              $agent_code = $value['agent_code'];
              if($agent_code)
              {
                 $UserData = $this->users_model->where('agent_code',$agent_code)->find_all();
                 if($UserData)
                 {
                       $agent_id = $UserData['0']->id;
                       $distributor_id = $UserData['0']->level_5;
                       $Area_dis_id = $UserData['0']->level_4;
                       $master_dis_id = $UserData['0']->level_3;
                       $company_id = $UserData['0']->level_2;
                       $trimax_id = $UserData['0']->level_1;
                    
                       if($master_dis_id)
                       {
                             $packageDetailsForMD = $this->assign_package_log_model->packageDetailsForMD($master_dis_id);
                             $package_id ='';
                             if($packageDetailsForMD)
                             {
                                $package_id = $packageDetailsForMD[0]['package_id'];
                             }else
                             {
                               $defaultPackageId = $this->assign_package_log_model->defaultPackageId();
                               $package_id = $defaultPackageId[0]['id'];
                             }
                             if($package_id)
                             {
                                 $pg_where = array('package_id'=>$package_id,'service_id' => MSRTC_SERVICE_ID);
                                 $package_details = $this->package_detail_model->where($pg_where)->find_all();
                                 $commission_id = $package_details[0]->commission_id;

                                 if($commission_id)
                                 {
                                     $getCommissionData = $this->service_2_commission_detail_model->where('commission_id',$commission_id)->find_all();
                                     $trimax_commission = '0';
                                     $company_commission ='0';
                                     $md_commission = '0';
                                     $ad_commission ='0';
                                     $dist_commission = '0';
                                     $retailer_commission = '0';

                                     if(!empty($getCommissionData))
                                     {
                                        $trimax_commission = round($getCommissionData[0]->trimax_commission,2);
                                        $company_commission = round($getCommissionData[0]->company_commission,2);
                                        $md_commission = round($getCommissionData[0]->md_commission,2);
                                        $ad_commission = round($getCommissionData[0]->ad_commission,2);
                                        $dist_commission = round($getCommissionData[0]->dist_commission,2);
                                        $retailer_commission = round($getCommissionData[0]->retailer_commission,2);
                                     }

                                     if($commission_amount && $commission_amount > 0)
                                     {
                                        //$trimaxAmt = getPercentageAmount($trimax_commission,$commission_amount);
                                        $companyAmt = round(getPercentageAmount($company_commission,$commission_amount),2);
                                        $mdAmt = round(getPercentageAmount($md_commission,$commission_amount),2);
                                        $adAmt = round(getPercentageAmount($ad_commission,$commission_amount),2);
                                        $disAmt = round(getPercentageAmount($dist_commission
                                            ,$commission_amount),2);
                                        $retAmt = round(getPercentageAmount($retailer_commission,$commission_amount),2);
                                        $trimaxAmount = $commission_amount - $companyAmt - $mdAmt - $adAmt - $disAmt - $retAmt;
                                        $trimaxAmt = round($trimaxAmount,2);
                                     }
                                     $distribution_array = array(
                                        'trimax_commission' => $trimaxAmt,
                                        'company_commission' => $companyAmt,
                                        'master distributor_commission' => $mdAmt,
                                        'area distributor_commission' => $adAmt,
                                        'distributor_commission' => $disAmt,
                                        'retailer_commission' => $retAmt,
                                      );
                                     //show($distribution_array,1);
                                    $now = date('Y-m-d H:i:s', time());
                                     
                                    //$comm_distribution_array['ticket_id'] = $ticket_id;
                                    $comm_distribution_array['agent_code'] = $agent_code;
                                    $comm_distribution_array['waybill_no'] =  $waybill_no;
                                    $comm_distribution_array['commission_template_id'] =  $commission_id;
                                    $comm_distribution_array['comm_applicable_amt'] = $commission_amount;
                                    $comm_distribution_array['trimax_id'] = $trimax_id;
                                    $comm_distribution_array['company_id'] = $company_id;
                                    $comm_distribution_array['md_id'] = $master_dis_id;
                                    $comm_distribution_array['ad_id'] = $Area_dis_id;
                                    $comm_distribution_array['di_id'] = $distributor_id;
                                    $comm_distribution_array['agent_id'] = $agent_id;
                                    $comm_distribution_array['trimax_comm'] = $trimaxAmt;
                                    $comm_distribution_array['company_comm'] = $companyAmt;
                                    $comm_distribution_array['md_comm'] = $mdAmt;
                                    $comm_distribution_array['ad_comm'] = $adAmt;
                                    $comm_distribution_array['di_comm'] = $disAmt;
                                    $comm_distribution_array['agent_comm'] = $retAmt;
                                    $comm_distribution_array['comm_calculate_date'] = date('Y-m-d',  strtotime("-1 days"));
                                    $comm_distribution_array['tds_percentage']=TDS_APPLICABLE_MONTHLY_COMMISSION;
                                    $comm_distribution_array['created_at'] = $now;

                                    
                                    log_data('msrtc/daily_comm_distribution_cron/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/commission_record'.'.log',$comm_distribution_array,'Commission Distribution Array');    
                                    
                                    $check_tkt_id = $this->cb_commission_distribution_model->where('waybill_no',$waybill_no)->find_all();
                                    if(empty($check_tkt_id))
                                    {
                                       $dist_id = $this->cb_commission_distribution_model->insert($comm_distribution_array); 
                                       $update_status = $this->cron_model->UpdateCommStatus($id);         
                                       log_data('msrtc/daily_comm_distribution_cron/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/commission_record'.'.log',$dist_id,'Distribution Id');    
                                    }
                                    else
                                    {
                                       log_data('msrtc/daily_comm_distribution_cron/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/commission_record'.'.log',$ticket_id,'ticket id already exist in cb_commission_distribution');    
                                    }
                               }
                         }   
                    }
                 }
              }
           }
        }
     } **/

     public function MonthlyDistributeCommAmt()
     {
        $getDetails = array();
        $userData = array();
        $this->load->model('cb_commission_distribution_model');
        $whereArray = array('status' => 'Y','is_deleted' => 'N' );
        $userData = $this->users_model->column('id,role_id,first_name,last_name')->where($whereArray)->find_all();
        if($userData)
        {
            foreach ($userData as $key => $value) {
                $user_id = $value->id;
                $role_id = $value->role_id;
                
                if($role_id == TRIMAX_ROLE_ID || $role_id == COMPANY_ROLE_ID ||$role_id == MASTER_DISTRIBUTOR_ROLE_ID ||$role_id == AREA_DISTRIBUTOR_ROLE_ID ||$role_id == DISTRIBUTOR || $role_id == RETAILER_ROLE_ID)
               {
                    if($user_id)
                    {
                      $getCommData = $this->cron_model->getMonthlyCommData($user_id,$role_id);

                      $commission_amount = '';
                     
                     log_data('msrtc/monthly_commission_Distribution/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/commission_record'.'.log', $getCommData,'get comm data');    

                      if($getCommData)
                      {
                         $comm_amt = $getCommData[0]['comm_amount'];
                         $commission_amount = round(getGstTdsAmount($comm_amt),2);
                         // $tds_value = TDS_APPLICABLE_MONTHLY_COMMISSION;
                         // $commission_amount = getPercentageAmount($tds_value,$comm_amt);
                      }            
                     log_data('msrtc/monthly_commission_Distribution/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/commission_record'.'.log',  $commission_amount,' commission_amount');    
                    
                      if(isset($commission_amount) || !empty($commission_amount))
                      {
                            $this->db->trans_begin();
                            
                            $Walletdata = $this->wallet_model->where('user_id',$user_id)->find_all();
                            if($Walletdata)
                            {
                               $WalletId = $Walletdata[0]->id;
                               $CurrentAmt = $Walletdata[0]->amt; 
                            }
                            $AmtafterAdd = $CurrentAmt + $commission_amount;      

                            $TranNo = substr(hexdec(uniqid()), 4, 12);
                            $wallet_trans_detail['transaction_no'] = $TranNo;
                            $wallet_trans_detail['w_id'] = $WalletId;
                            $wallet_trans_detail['amt'] =  $commission_amount;
                            $wallet_trans_detail['wallet_type'] =  'actual_wallet';
                            $wallet_trans_detail['comment'] = 'Msrtc commission amount added';
                            $wallet_trans_detail['status'] = 'credited';
                            $wallet_trans_detail['user_id'] = $user_id;
                            $wallet_trans_detail['added_by'] = 'monthly cron';
                            $wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                            $wallet_trans_detail['amt_before_trans'] = $CurrentAmt;
                            $wallet_trans_detail['amt_after_trans'] = $AmtafterAdd;
                            $wallet_trans_detail['transaction_type_id'] = '15';
                            $wallet_trans_detail['transaction_type'] = 'credited';
                            $wallet_trans_detail['is_status'] = 'Y';
                            
                            log_data('msrtc/monthly_commission_Distribution/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/commission_record'.'.log',$wallet_trans_detail,'Wallet transaction Array');    

                            $wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail);
                           
                            if($wallet_trans_id)
                            {
                                 $update_data = array(
                                                    'amt'          => $AmtafterAdd,
                                                    'updated_by'   => 'monthly cron',
                                                    'comment'      => 'commission amount added by monthly cron',
                                                    'updated_date' => date('Y-m-d H:i:s'),
                                                    'last_transction_id' => $wallet_trans_id
                                                );
                                 log_data('msrtc/monthly_commission_Distribution/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/commission_record'.'.log',$update_data,'Wallet Updation Array');    

                                 $UpdateWallet = $this->wallet_model->updateWalletData($update_data,$user_id);

                                 $final_comm_gst_tds_amount = (isset($getCommData[0]['comm_gst_tds_amount']) && !empty($getCommData[0]['comm_gst_tds_amount'])) ? $getCommData[0]['comm_gst_tds_amount'] : 0;

                                 $monthlyCommDetail['user_id'] = $user_id;
                                 $monthlyCommDetail['commission_amt'] = round($comm_amt,2);
                                 $monthlyCommDetail['commission_gst_tds_amt'] = $final_comm_gst_tds_amount;
                                 $monthlyCommDetail['amount_after_comm'] = $AmtafterAdd;
                                 $monthlyCommDetail['amount_before_comm'] = $CurrentAmt;
                                 $monthlyCommDetail['wallet_id'] = $WalletId;
                                 $monthlyCommDetail['wallet_trans_id'] = $wallet_trans_id;
                                 $monthlyCommDetail['calc_gst_tds_amt'] = $commission_amount;
                                 $monthlyCommDetail['tds_percentage'] = TDS_APPLICABLE_MONTHLY_COMMISSION;
                                 $monthlyCommDetail['gst_percentage'] = GST_APPLICABLE_MONTHLY_COMMISSION;
                                 $this->cron_model->monthlyCommissionDetail($monthlyCommDetail);
                                
                                log_data('msrtc/monthly_commission_Distribution/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/commission_record'.'.log',$UpdateWallet,'Wallet Updation');

                                 if($UpdateWallet)
                                 {
                                    $updateCommDistribution = $this->cb_commission_distribution_model->UpdateCommDistribution($user_id,$role_id);
                                    
                                    if($updateCommDistribution)
                                    {
                                       $this->db->trans_commit();
                                    }else{
                                       $this->db->trans_rollback();
                                    }
                                 }
                            }
                       }
                   }
                }     
             }
         }
        $subject = "Monthly Commission Distribution Cron on ".ENVIRONMENT." server.";
        $msg = "Monthly Commission Distribution Cron is run at ". date('d-m-Y H:i:s') ." successfully";
        $this->mail($subject,$msg);
      }

    /*  public function MonthlyDistributeCommAmt()
     {
        $getDetails = array();
        $userData = array();
        $this->load->model('cb_commission_distribution_model');
        $whereArray = array('status' => 'Y','is_deleted' => 'N' );
        $userData = $this->users_model->column('id,role_id,first_name,last_name')->where($whereArray)->find_all();
        if($userData)
        {
            foreach ($userData as $key => $value) {
                $user_id = $value->id;
                $role_id = $value->role_id;
                
                if($role_id == TRIMAX_ROLE_ID || $role_id == COMPANY_ROLE_ID ||$role_id == MASTER_DISTRIBUTOR_ROLE_ID ||$role_id == AREA_DISTRIBUTOR_ROLE_ID ||$role_id == DISTRIBUTOR || $role_id == RETAILER_ROLE_ID)
               {
                    if($user_id)
                    {
                      $getCommData = $this->cron_model->getMonthlyCommData($user_id,$role_id);
                      $commission_amount = '';
                     
                     log_data('msrtc/monthly_commission_Distribution/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/commission_record'.'.log', $getCommData,'get comm data');    

                      if($getCommData)
                      {
                         $comm_amt = $getCommData[0]['comm_amount'];
                         $tds_value = TDS_APPLICABLE_MONTHLY_COMMISSION;
                         $commission_amount = getPercentageAmount($tds_value,$comm_amt);
                      }            
                     log_data('msrtc/monthly_commission_Distribution/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/commission_record'.'.log',  $commission_amount,' commission_amount');    
                    
                      if($commission_amount || !empty($commission_amount))
                      {
                            $this->db->trans_begin();
                            
                            $Walletdata = $this->wallet_model->where('user_id',$user_id)->find_all();
                            if($Walletdata)
                            {
                               $WalletId = $Walletdata[0]->id;
                               $CurrentAmt = $Walletdata[0]->amt; 
                            }
                            $AmtafterAdd = $CurrentAmt + $commission_amount;      

                            $TranNo = substr(hexdec(uniqid()), 4, 12);
                            $wallet_trans_detail['transaction_no'] = $TranNo;
                            $wallet_trans_detail['w_id'] = $WalletId;
                            $wallet_trans_detail['amt'] =  $commission_amount;
                            $wallet_trans_detail['wallet_type'] =  'actual_wallet';
                            $wallet_trans_detail['comment'] = 'Msrtc commission amount added';
                            $wallet_trans_detail['status'] = 'credited';
                            $wallet_trans_detail['user_id'] = $user_id;
                            $wallet_trans_detail['added_by'] = 'monthly cron';
                            $wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                            $wallet_trans_detail['amt_before_trans'] = $CurrentAmt;
                            $wallet_trans_detail['amt_after_trans'] = $AmtafterAdd;
                            $wallet_trans_detail['transaction_type_id'] = '15';
                            $wallet_trans_detail['transaction_type'] = 'credited';
                            $wallet_trans_detail['is_status'] = 'Y';
                            
                            log_data('msrtc/monthly_commission_Distribution/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/commission_record'.'.log',$wallet_trans_detail,'Wallet transaction Array');    

                            $wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail);
                           
                            if($wallet_trans_id)
                            {
                                 $update_data = array(
                                                    'amt'          => $AmtafterAdd,
                                                    'updated_by'   => 'monthly cron',
                                                    'comment'      => 'commission amount added by monthly cron',
                                                    'updated_date' => date('Y-m-d H:i:s'),
                                                    'last_transction_id' => $wallet_trans_id
                                                );
                                 log_data('msrtc/monthly_commission_Distribution/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/commission_record'.'.log',$update_data,'Wallet Updation Array');    

                                 $UpdateWallet = $this->wallet_model->updateWalletData($update_data,$user_id);
                                
                                log_data('msrtc/monthly_commission_Distribution/'.date('Y').'/'.date('M').'/'.date('d-m-Y').'/commission_record'.'.log',$UpdateWallet,'Wallet Updation');

                                 if($UpdateWallet)
                                 {
                                    $updateCommDistribution = $this->cb_commission_distribution_model->UpdateCommDistribution($user_id,$role_id);
                                    
                                    if($updateCommDistribution)
                                    {
                                       $this->db->trans_commit();
                                    }else{
                                       $this->db->trans_rollback();
                                    }
                                 }
                            }
                       }
                   }
                }     
             }
         }
      }*/

    public function mail($subject,$msg) {
        $mailbody_array = array("subject" => $subject,
                            "message" => $msg,
                            "to" => 'surekha.trimax@gmail.com',
                            "cc" => array('lilawati.bookonspot@gmail.com','zanzanepriyanka.trimax@gmail.com'),
                        );                      
                        
        $mail_result = send_mail($mailbody_array);
    }


      public function test_sms()
      {
        
                       //9987546362  8655333404
         $mail_result = sendSMS('9820112280','Dear Kiran Patil, Please enter OTP 978425 to complete your registration process.');
         show($mail_result,1);
   }  
   }  
