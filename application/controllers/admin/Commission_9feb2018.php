<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Commission extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('base_commission_model', 'commission_model', 'users_model', 'city_master_model', 'agent_commission_template_log_model','service_master_model','assign_package_log_model','package_detail_model'));
            $this->load->model(array('services/service_2/service_2_commission_model','services/service_2/service_2_commission_detail_model'));
        $this->load->library('form_validation');
        is_logged_in();
    }

    public function index() {
        $data['service_list']=$this->service_master_model->where("status", '1')->find_all();
        load_back_view(SELECT_SERVICE_VIEW,$data);
    }
    /* @Author      : Maunica Shah
     * @function    : list_commission
     * @detail      : Gives the lists of all the commission.
     */
 
    function list_commission() {
        $service_id= $this->session->userdata('service_id');
        $where = array('c.commission_default_flag' => '1', 'c.is_deleted' => '0');
        $this->datatables->select("c.id,c.name,DATE_FORMAT(c.created_at,'%d-%m-%Y') AS created_at,c.commission_default_flag");
        $this->datatables->from($service_id."_commission_master c");
        $this->datatables->where($where);
        $this->datatables->add_column('action', '<a href="' . base_url('admin/commission/view') . '/$1" class="btn btn-primary btn-xs show_btn" data-ref="$1" data-status="$2" title="View"><i class="fa fa-eye"></i> </a>
            <a href="' . base_url('admin/commission/delete') . '/$1" class="btn btn-primary btn-xs delete_btn" data-ref="$1" data-status="$2" title="Delete"> <i class="fa fa-trash"></i> </a>
                                                ', 'id,status');
        
        if($this->session->userdata('role_id') == COMPANY_ROLE_ID) {
            $this->datatables->or_where('c.created_by',$this->session->userdata('id'));
        }
        $this->datatables->where('c.is_deleted','0');

        $data = $this->datatables->generate('json');
        if (!empty($data)) {
            echo $data;
        } else {
            echo "No Result Found";
        }
    }

    /* @Author      : Maunica Shah
     * @function    : create
     * @detail      : To load the commmission create view.
     */

    public function create() {
        $data = array();
        $po_detail_array = array();
        $po_array = array();
        $input = array();
        $provider_operator_details = $this->base_commission_model->base_commission_detail($input);

        foreach ($provider_operator_details as $key => $value) {
            $po_detail_array[strtolower(trim($value->provider_name))][] = $value;
        }
        $data["details_provider_operator"] = $po_detail_array;
        load_back_view(USER_COMMISSION_FOLDER . '/create', $data);
    }

    /* @Author      : Maunica Shah
     * @function    : store
     * @detail      : To store the commmission.
     */

    public function store() {
        $input = $this->input->post();
        $insert_data = array();
        if (!empty($input)) {
            $this->commission_model->name = $input['name'];
            $this->commission_model->from = !empty($input['from']) ? (date('Y-m-d', strtotime($input['from'])) . " 00:00:00") : '';
            $this->commission_model->till = !empty($input['till']) ? (date('Y-m-d', strtotime($input['till'])) . " 23:59:59") : '';
            $this->commission_model->service_tax = $input['service_tax'];
            $this->commission_model->commission_with_service_tax = $input['commission_with_service_tax'];
            $this->commission_model->commission_default_flag = $input['default_template'];
            $this->commission_model->commission_type = "Incoming";
            $this->commission_model->created_by = $this->session->userdata('user_id');
            $this->commission_model->created_at = date("Y-m-d H:i:s", time());
            $this->commission_model->status = "1";
            $commission_id = $this->commission_model->save();

            if ($commission_id) {
                foreach ($input['details'] as $provider_name_key => $provider_name_value) {

                    if (($provider_name_key) == 'bookonspot') {
                        $insert_data['details'] = $provider_name_value;
                    }
                    if (($provider_name_key) == 'etravelsmart') {

                        $insert_data['ets_details'] = $provider_name_value;
                    }

                    if (($provider_name_key) == 'its') {

                        $insert_data['its_details'] = $provider_name_value;
                    }
                }

                $insert_data['commission_id'] = $commission_id;

                $res = $this->commission_model->insert_slab_wise_commission($insert_data);
                //exit;
                $this->session->set_userdata('commission_id', $commission_id);
                $data['msg_type'] = "success";
                $data['flag'] = "@success";
                $data['msg'] = "Commission Created successfully.";
            } else {
                $data['msg_type'] = "error";
                $data['flag'] = "@#error@";
                $data['msg'] = "Commission could not be created.";
            }
        } else {
            $data['msg_type'] = "error";
            $data['flag'] = "@#error@";
            $data['msg'] = "Some error occured.Please try again later.";
        }
        redirect(site_url() . '/admin/commission');
    }

    /* @Author     : Maunica Shah
     * @function    : is_commission_exists
     * @detail      : checks if commission name already exists.
     */

    public function is_commission_exists() {

        $this->load->model(array('services/service_1/service_1_commission_model','services/service_1/service_1_commission_detail_model'));
        $input = $this->input->post();
        $where_commission = array(
            "name" => $input['name']
        );

        $count = $this->service_1_commission_model
                ->where($where_commission)
                ->count_all();

        $flag = ($count) ? 'true' : 'false';
        echo $flag;
    }

    /* @Author     : Maunica Shah
     * @function    : view
     * @detail      : commission list view.
     */

    public function view($id) {
        $data = array();
        if (!empty($id)) {
            $commission_detail_array = array();
            $cd_detail = $this->commission_model->msrtc_commission_detail_by_id($id);
            if (!empty($cd_detail)) {
                $data["commission_id"] = $cd_detail[0]->commission_id;
                $data["commission_name"] = $cd_detail[0]->commission_name;
                $data["commission_from"] = date("d-m-Y", strtotime($cd_detail[0]->commission_from));
                $data["commission_till"] = date("d-m-Y", strtotime($cd_detail[0]->commission_till));
                $data["service_tax"] = $cd_detail[0]->service_tax;
                $data["commission_with_service_tax"] = $cd_detail[0]->commission_with_service_tax;
                $data["default_template"] = $cd_detail[0]->commission_default_flag;
                $operator_name = array();
                $provider_name = array();
                // $provider_operator_details = $this->base_commission_model->base_commission_detail($input);

                foreach ($cd_detail as $key => $value) {
                    $po_detail_array[strtolower(trim($value->provider_name))][] = $value;
                }
                //show($po_detail_array)
                $data["details_provider_operator"] = $po_detail_array;
                load_back_view(USER_COMMISSION_FOLDER . '/view', $data);
            } else {
                $this->session->set_flashdata(["msg" => "Commission detail not found. Please try again."]);
                redirect(site_url() . '/admin/commission');
            }
        } else {
            $this->session->set_flashdata(["msg" => "Commission detail not found. Please try again."]);
            redirect(site_url() . '/admin/commission');
        }
    }

    /* @Author     : Maunica Shah
     * @function    : delete
     * @detail      : delete commission.
     */

    public function delete($id) {
        $response = array();
        $user_info = $this->session->userdata('user_id');
        $commission = $this->service_2_commission_model->find_by(["id" => $id]);
        $pkg_comm = $this->package_detail_model->get_assign_pkg_comm($id);
        if($pkg_comm) {
            $this->session->set_flashdata("msg" ,"Commission <b>" . $commission->name . "</b> could not be deleted. It is assigned to some package");
        } else {
            if ($commission->is_deleted == 1) {
                $update_status_as = "0";
                $update_status_action = "deactived";
            } else if ($commission->is_deleted == 0) {
                $update_status_as = "1";
                $update_status_action = "activated";
            }
            if ($commission && isset($update_status_as)) {
                $commission_save = [
                    'updated_at' => date("Y-m-d H:i:s"),
                    'updated_by' => $user_info->id,
                    'is_deleted' => $update_status_as
                ];
                $uid = $this->service_2_commission_model->update($id, $commission_save);
                if ($uid) {
                    $this->session->set_flashdata('msg', "Commission <b>'" . $commission->name . "'</b> is <b>" . 'deleted' . "</b>.");
                } else {
                    $this->session->set_flashdata(["error" => "Commission <b>'" . $commission->name . "'</b> could not " . $update_status_action]);
                }
            } else {
                $this->session->set_flashdata(["error" => "Commission is not found."]);
            }
        }
        redirect(base_url('admin/commission/redirect_commission'));
    }

    /* @Author     : Maunica Shah
     * @function    : change_status
     * @detail      : change commission status.
     */

    public function change_status($status) {
        $status = $status == '1' ? '0' : '1';

        $data = array();
        $data["flag"] = '@#failed#@';
        $data["msg"] = 'Failed to update status.';

        if ($this->service_2_commission_model->update($this->input->post('id'), array('status' => $status))) {
            $data["flag"] = '@#success#@';
            $data["msg"] = 'Status changed successfully.';
        }
        data2json($data);
    }

    /* @Author     : Maunica Shah
     * @function    : assign_commission_template
     * @detail      : load page to assign commission template to MD
     */

    public function assign_commission_template() {
        $input = $this->input->post();
        // Get Agent list
        $data['agent_list'] = $this->users_model->fetch_all_md();
        // Get City List 
        $data['cities'] = $this->city_master_model->get_all_city();
        $data['city_name'] = $input['city'];
        $data['agent_name'] = $input['agent'];
        // Get Current Active Commission 
        $date = date('Y-m-d');
        $data['commissionDetail'] = $this->commission_model->where("status", '1')->as_array()->find_all();
        load_back_view(AGENT_COMMISSION_TEMPLATE, $data);
    }

    /* @Author     : Maunica Shah
     * @function    : save_agent_com_temp
     * @detail      : assign template to all md and their respective users.
     */

    public function save_agent_com_temp() {
        $input = $this->input->post();
        if (!empty($input['com_template'])) {
            $users_list_array = array(MASTER_DISTRIBUTOR_ROLE_ID, AREA_DISTRIBUTOR_ROLE_ID, DISTRIBUTOR, RETAILER_ROLE_ID);
            $commissionDetail = $this->commission_model->where("id", $input['com_template'])->as_array()->find_all();
            $agentDetail = $this->users_model->where_in('role_id', $users_list_array)->as_array()->find_all();
            foreach ($agentDetail as $detail) {
                // Insert into commission log table
                $agent_commission_log = [
                    "user_id" => $detail['id'],
                    "commission_id" => $input['com_template'],
                    "added_on" => date('Y-m-d H:i:s'),
                    "commission_from" => date("Y-m-d", strtotime($input['from'])) . " 00:00:00",
                    "commission_to" => date("Y-m-d", strtotime($input['till'])) . " 23:59:59",
                    "added_by" => $this->session->userdata('user_id')
                ];
                $id = $this->agent_commission_template_log_model->insert($agent_commission_log);

                if ($id) {
                    // Add Log into Csv File
                    $title = array('User Id', 'Commission Id', 'Added On', 'Added By');
                    $val = array($detail['id'], $input['com_template'], date('Y-m-d H:i:s'), $this->session->userdata('user_id'));

                    $FileName = "commission/agent_commission_template_log/" . date('Y-m-d H') . ".csv";

                    make_csv($FileName, $title, $val);

                    // Update user Commission Id 
                    $user_update_array = array("commission_id" => $input['com_template']
                    );

                    $user_update_where = array('id' => $detail['id']);

                    $is_user_updated = $this->users_model->update_where($user_update_where, "", $user_update_array);
                }
            }
        }


        $this->session->set_flashdata('msg_type', 'Success');
        $this->session->set_flashdata('msg', 'Successfully Commssion Template assign to MD');
        redirect('admin/commission');
    }

    /* @Author     : Maunica Shah
     * @function    : get_agent_detail
     * @detail      : filters and gets the details of the MD .
     */

    public function get_agent_detail() {
        $input = $this->input->post();
        $date = date('Y-m-d');

        $this->datatables->select("1,u.id,u.display_name,sm.stState AS state,cm.stCity AS city,c.name,date_format(ac.commission_from,'%d-%m-%Y'),date_format(ac.commission_to,'%d-%m-%Y'), IF(c.commission_default_flag = '1','Default',' ')");
        $this->datatables->from('users u');
        $this->datatables->join('(SELECT    MAX(id) max_id,user_id FROM agent_commission_template_log where status = "1" and "' . $date . '" between commission_from and commission_to  GROUP BY  user_id) c_max', 'c_max.user_id = u.id', 'left');
        $this->datatables->join('agent_commission_template_log ac', 'ac.id = c_max.max_id', 'left');
        $this->datatables->join('state_master sm', 'u.state=sm.intStateId', 'left');
        $this->datatables->join('city_master cm', 'u.city=cm.intCityId', 'left');
        $this->datatables->join('commission c', 'c.id=ac.commission_id', 'left');
        $this->datatables->where('u.role_id', MASTER_DISTRIBUTOR_ROLE_ID);
        $this->datatables->where('level', MASTER_DISTRIBUTOR_LEVEL);
        $this->datatables->where('u.status', 'Y');
        $this->datatables->where('u.is_deleted', 'N');
        if ($input['city'] > 0) {
            $this->datatables->where('u.city=' . $input['city']);
        }
        if ($input['agent'] > 0) {
            $this->datatables->where('u.id', $input['agent']);
        }
        $this->db->group_by('u.id');
        $this->db->order_by('ac.id', 'desc');
        $this->datatables->add_column('action', '<a href=' . base_url() . 'admin/commission/get_agent_commission_detail/$1 title="Apply Commission" class="views_wallet_trans margin" ref="$1"><button class="btn btn-primary" id="save" name="save" type="button">Apply Commission</button> </a>', 'id');

        $data = $this->datatables->generate('json');
        echo $data;
    }

    /* @Author     : Maunica Shah
     * @function    : save_agent_wise_com_temp
     * @detail      : saves commission assigned MD wise.
     */

    public function save_agent_wise_com_temp() {
        $input = $this->input->post();
        // Insert into commission log table
        $commissionDetail = $this->commission_model->column('commission.id,cd.id as comm_detail_id,commission.name,commission.from as commission_from,commission.till as commission_to,commission.commission_default_flag')->join("commission_detail cd", "commission.id = cd.commission_id", "inner")->group_by("cd.commission_id")->where("commission.id", $input['commissionId'])->as_array()->find_all();
        // Check commission already assign or not 
        $AssigncommissionDetail = $this->agent_commission_template_log_model->where(array("commission_id" => $input['commissionId'], "user_id" => $input['agentId']))->as_array()->find_all();

        if (count($AssigncommissionDetail) > 0 && !empty($AssigncommissionDetail)) {
            $data1['flag'] = "@#failed#@123";
        } else {
            $id = $input['agentId'];
            $agentDetail=array();
            $agentDetail = $this->users_model->where('level_2', $id)->as_array()->find_all();
            $agent_commission_log = [
                "user_id" => $input['agentId'],
                "commission_id" => $input['commissionId'],
                "added_on" => date('Y-m-d H:i:s'),
                "commission_from" => $input['from'] . " 00:00:00",
                "commission_to" => $input['till'] . " 23:59:59",
                "added_by" => $this->session->userdata('user_id')
            ];
            $id = $this->agent_commission_template_log_model->insert($agent_commission_log);
            if ($id) {
                $user_update_array = array("commission_id" => $input['commissionId']
                );
                $update_md_where = array('id' => $input['agentId']);
                $is_md_updated = $this->users_model->update_where($update_md_where, "", $user_update_array);
                if($agentDetail)
                {
                    foreach ($agentDetail as $detail) {
                    $agent_commission_log = [
                        "user_id" => $detail['id'],
                        "commission_id" => $input['commissionId'],
                        "added_on" => date('Y-m-d H:i:s'),
                        "commission_from" => $input['from'] . " 00:00:00",
                        "commission_to" => $input['till'] . " 23:59:59",
                        "added_by" => $this->session->userdata('user_id')
                    ];
                    $users_id = $this->agent_commission_template_log_model->insert($agent_commission_log);

                    if ($users_id) {
                        // Add Log into Csv File
                        $title = array('User Id', 'Commission Id', 'Added On', 'Added By');
                        $val = array($detail['id'], $input['commissionId'], date('Y-m-d H:i:s'), $this->session->userdata('user_id'));

                        $FileName = "commission/agent_commission_template_log/" . date('Y-m-d H') . ".csv";

                        make_csv($FileName, $title, $val);

                        // Update user Commission Id 
                        $user_update_array = array("commission_id" => $input['commissionId']
                        );

                        $user_update_where = array('id' => $detail['id']);

                        $is_user_updated = $this->users_model->update_where($user_update_where, "", $user_update_array);


                        if ($is_user_updated) {
                            $data1['flag'] = "@#success#@";
                        } else {
                            $data1['flag'] = "@#failed#@123";
                        }
                    } else {
                        $data1['flag'] = "@#failed#@123";
                    }
                }
                }
                 $data1['flag'] = "@#success#@";
            }
        }
        data2json($data1);
    }

    public function get_agent_commission_detail($id) {
        $input = $id;
        $date = date('Y-m-d');
        $data['active_commission_detail'] = $this->agent_commission_template_log_model->column('c.id as commission_id,agent_commission_template_log.id as log_id,c.name,agent_commission_template_log.commission_from,agent_commission_template_log.commission_to,c.commission_default_flag,c.status,agent_commission_template_log.status as log_status,agent_commission_template_log.added_on,c.created_at')->join("commission c", "c.id = agent_commission_template_log.commission_id", "left")->where(array("user_id" => $input))->as_array()->find_all();
        $data['agentId'] = $input;

        $data['activeComissionLogId'] = $this->agent_commission_template_log_model->column('agent_commission_template_log.id')->join("commission c", "c.id = agent_commission_template_log.commission_id", "left")->where(array("user_id" => $input, "agent_commission_template_log.status" => '1', "c.status" => '1'))->where('("' . $date . '" between commission_from and commission_to )')->order_by("agent_commission_template_log.id", "desc")->limit('1')->as_array()->find_all();
        // Get Current Active Commission 

        $data['commissionDetail'] = $this->commission_model->column('commission.id,cd.id as comm_detail_id,commission.name,commission.from as commission_from,commission.till as commission_to,commission.commission_default_flag')->join("commission_detail cd", "commission.id = cd.commission_id", "inner")->group_by("cd.commission_id")->where("commission.status", '1')->as_array()->find_all();
        $data['userData'] = $this->users_model->column("concat(first_name,' ',last_name) as full_name")->where(array("id" => $input))->find_all();
        load_back_view(COMMISSION_ASSIGN_TEMPLATE, $data);
    }

    /* @Author     : Maunica Shah
     * @function    : chagne_agent_commission_status
     * @detail      : changes commission status in agent_commission_template_log
     */

    public function chagne_agent_commission_status($status) {
        if ($status == 0) {
            $commission_id_to_update = $this->input->post('id');
        } else {
            
        }
        $status = $status == '1' ? '0' : '1';

        $data = array();
        $data["flag"] = '@#failed#@';
        $data["msg"] = 'Failed to update status.';

        if ($this->agent_commission_template_log_model->update($this->input->post('id'), array('status' => $status))) {
            $data["flag"] = '@#success#@';
            $data["msg"] = 'Status changed successfully.';
        }
        data2json($data);
    }
    
    /* @Author     : Maunica Shah
     * @function    : my_commission
     * @detail      : MD  my commission view.
     */
    
    public function my_commission()
    {
        
       /* $data=array();
        if($this->session->userdata('id') != "")
        {
            $userid=$this->session->userdata('id');
            $current_user_commission=$this->commission_model->get_active_commission($userid);
            $id=$current_user_commission[0]->active_commission_id;
            $data = array();
            if (!empty($id)) {
                $commission_detail_array = array();
                $cd_detail = $this->commission_model->commission_detail_by_id($id);
                if (!empty($cd_detail)) {
                    $data["commission_id"] = $cd_detail[0]->commission_id;
                    $data["commission_name"] = $cd_detail[0]->commission_name;
                    $data["commission_from"] = date("d-m-Y", strtotime($cd_detail[0]->commission_from));
                    $data["commission_till"] = date("d-m-Y", strtotime($cd_detail[0]->commission_till));
                    $data["service_tax"] = $cd_detail[0]->service_tax;
                    $data["commission_with_service_tax"] = $cd_detail[0]->commission_with_service_tax;
                    $data["default_template"] = $cd_detail[0]->commission_default_flag;
                    $operator_name = array();
                    $provider_name = array();
                    // $provider_operator_details = $this->base_commission_model->base_commission_detail($input);

                    foreach ($cd_detail as $key => $value) {
                        $po_detail_array[strtolower(trim($value->provider_name))][] = $value;
                    }
                    //show($po_detail_array)
                    $data["details_provider_operator"] = $po_detail_array;
                    load_back_view(MY_COMMISSION, $data);
                } else {
                    $this->session->set_flashdata(["msg" => "Commission detail not found. Please try again."]);
                    redirect(site_url() . '/admin/commission');
                }
            } else {
                $this->session->set_flashdata(["msg" => "Commission detail not found. Please try again."]);
                redirect(site_url() . '/admin/commission');
            }
        }
        else
        {
            $this->session->set_flashdata(["msg" => "Master Distributor ID not found."]);
            redirect(site_url() . '/admin/commission');
        }*/
       // $this->load->model(array('services/service_2/service_2_commission_model','services/service_2/service_2_commission_detail_model'));
       $data=array();
       if($this->session->userdata('role_id') == COMPANY_ROLE_ID) {
            $data['commission_data'] = $this->assign_package_log_model->get_company_mds();
            if(empty($data['commission_data'])) {
                $data['commission_data'] = $this->package_detail_model->get_default_pkg_comm();
            }
            // show($data,1);
            load_back_view(MY_COMPANY_COMMISSION, $data);
       } else {

            if($this->session->userdata('id') != "")
            {
                $user_id = $this->session->userdata('id');
                $user_role_id    = $this->session->userdata("role_id");
                $user_result = $this->users_model->where('id',$user_id)->as_array()->find_all();

                if(($this->session->userdata('role_id') == AREA_DISTRIBUTOR_ROLE_ID)
                    || ($this->session->userdata('role_id') == DISTRIBUTOR)
                    || ($this->session->userdata('role_id') == RETAILER_ROLE_ID) ) 
                {
                    $level_user_id = $user_result[0]['level_3'];
                }
                if($this->session->userdata('role_id') == MASTER_DISTRIBUTOR_ROLE_ID) {
                    $level_user_id = $user_id;
                }
                    $assign_package_result = $this->assign_package_log_model->where(array('user_id' => $level_user_id, 'pkg_delete_status' => '0', 'status' => '1'))->order_by("id", "desc")->limit('1')->as_array()->find_all();
                   // show($assign_package_result,1); 
                    $package_id = $assign_package_result[0]['package_id'];

                    if(!empty($package_id)) {

                        $package_result = $this->package_detail_model->where('package_id',$package_id)->as_array()->find_all();
                        
                        $commission_id = $package_result[0]['commission_id'];
                        if(!empty($commission_id)) {

                            $commission_master_result = 
                            $this->service_2_commission_model->where('id',$commission_id)->as_array()->find_all();
                           $data['commission_name'] = $commission_master_result[0]['name'];
                           $data['commission_default_flag'] = $commission_master_result[0]['commission_default_flag'];

                            $commission_detail_result = $this->service_2_commission_detail_model->where('commission_id',$commission_id)->as_array()->find_all();
                            $data['commission_detail_result'] = $commission_detail_result[0];
                            // show($data,1);
                            load_back_view(MY_COMMISSION, $data);

                        } else {
                            $this->session->set_flashdata('msg_type', 'success');
                            $this->session->set_flashdata('msg', 'Commission detail not found. Please try again.');
                            redirect(site_url() . '/admin/dashboard');
                    }
                    
                 }else {

                    $commission_data = $this->package_detail_model->get_default_pkg_comm();
                    $data['commission_name'] = $commission_data[0]['commission_name'];
                    $data['commission_default_flag'] = $commission_data[0]['commission_default_flag'];
                    $data['commission_detail_result'] = $commission_data[0];
                    // show($data,1);
                    if(!empty($commission_data)) {
                        load_back_view(MY_COMMISSION, $data);
                    }else {
                        $this->session->set_flashdata('msg_type', 'success');
                        $this->session->set_flashdata('msg', 'Commission detail not found. Please try again.');
                        redirect(site_url() . '/admin/dashboard');
                    }
                 }


            } else { 
                    $this->session->set_flashdata('msg_type', 'success');
                    $this->session->set_flashdata('msg', 'Commission detail not found. Please try again.');          
               		redirect(site_url() . '/admin/dashboard');
            }
        }
}


    public function redirect_commission() {
        $input=array();
        $input = $this->input->post();
        $service_id=$input['select_service'];
        if(empty($input))
        {
            $service_id=$this->session->userdata('service_id');
        }
        $this->session->set_userdata('service_id', $service_id);
        load_back_view(USER_COMMISSION);
    }
    
     public function redirect_new_commission() {
        $input = $this->input->post();
        $service_id= $this->session->userdata('service_id');
        if($service_id > 0) {
            $functionName = 'service_'.$service_id;
            if(method_exists($this,$functionName))
            {
                $this->$functionName($service_id);
            }
            else {
                $this->session->set_flashdata('msg_type', 'success');
                $this->session->set_flashdata('msg', 'Kindly Contact Developer,function not exists.');

                redirect(site_url() . '/admin/commission');
            }
            
        }
        else {
            $this->session->set_flashdata('msg_type', 'success');
            $this->session->set_flashdata('msg', 'Please select service first.');

            redirect(site_url() . '/admin/commission'); 
        }
    }
    
    public function service_1($serviceId) {
        $this->load->model(array('services/service_1/base_commission_model'));
        $data = array();
        $po_detail_array = array();
        $po_array = array();
        $input = array();
        $provider_operator_details = $this->base_commission_model->base_commission_detail($input);
        foreach ($provider_operator_details as $key => $value) {
            $po_detail_array[strtolower(trim($value->provider_name))][] = $value;
        }
        $data["details_provider_operator"] = $po_detail_array;
        $data['serviceId'] = $serviceId;
        $data['service_detail'] = $this->service_master_model->where(array('id' => $serviceId))->find_all();
        load_back_view(USER_COMMISSION_FOLDER . 'services/service_1', $data);
    }

    public function save_commission_service_1() {

        $this->load->model(array('services/service_1/service_1_commission_model','services/service_1/service_1_commission_detail_model'));
        $input = $this->input->post();
        $insert_data = array();
        if (!empty($input)) {
            $this->service_1_commission_model->name = $input['name'];
            $this->service_1_commission_model->from = !empty($input['from']) ? (date('Y-m-d', strtotime($input['from'])) . " 00:00:00") : '';
            $this->service_1_commission_model->till = !empty($input['till']) ? (date('Y-m-d', strtotime($input['till'])) . " 23:59:59") : '';
            $this->service_1_commission_model->service_tax = $input['service_tax'];
            $this->service_1_commission_model->commission_with_service_tax = $input['commission_with_service_tax'];
            $this->service_1_commission_model->commission_default_flag = $input['default_template'];
            $this->service_1_commission_model->commission_type = "Incoming";
            $this->service_1_commission_model->created_by = $this->session->userdata('user_id');
            $this->service_1_commission_model->created_at = date("Y-m-d H:i:s", time());
            $this->service_1_commission_model->updated_by = $this->session->userdata('user_id');
            $this->service_1_commission_model->updated_at = date("Y-m-d H:i:s", time());
            $this->service_1_commission_model->status = "1";
            $commission_id = $this->service_1_commission_model->save();
            
            if ($commission_id) {
                foreach ($input['details'] as $provider_name_key => $provider_name_value) {

                    if (($provider_name_key) == 'bookonspot') {
                        $insert_data['details'] = $provider_name_value;
                    }
                    if (($provider_name_key) == 'etravelsmart') {

                        $insert_data['ets_details'] = $provider_name_value;
                    }

                    if (($provider_name_key) == 'its') {

                        $insert_data['its_details'] = $provider_name_value;
                    }
                }

                $insert_data['commission_id'] = $commission_id;

                $res = $this->service_1_commission_model->insert_slab_wise_commission($insert_data);
                //exit;
                $this->session->set_userdata('commission_id', $commission_id);
                $data['msg_type'] = "success";
                $data['flag'] = "@success";
                $data['msg'] = "Commission Created successfully.";
            } else {
                $data['msg_type'] = "error";
                $data['flag'] = "@#error@";
                $data['msg'] = "Commission could not be created.";
            }
        } else {
            $data['msg_type'] = "error";
            $data['flag'] = "@#error@";
            $data['msg'] = "Some error occured.Please try again later.";
        }
        redirect(site_url() . '/admin/commission');
    }

    public function service_2($serviceId) {
        
        $data = array();
        $po_detail_array = array();
        $po_array = array();
        $input = array();
        
        $data['serviceId'] = $serviceId;
        $data['service_detail'] = $this->service_master_model->where(array('id' => $serviceId))->find_all();
        load_back_view(USER_COMMISSION_FOLDER . 'services/service_2', $data);
    }

    public function save_commission_service_2() {

        $this->load->model(array('services/service_2/service_2_commission_model','services/service_2/service_2_commission_detail_model'));
        $input = $this->input->post();
        $insert_data = array();
        $def_temp = array();
        if (!empty($input)) {
            if($input['default_template'] == 1) {
                $def_temp = $this->service_2_commission_model->where(array('is_deleted' => '0','commission_default_flag' => $input['default_template']))->find_all();
            }
            if(empty($def_temp)) {
                $this->service_2_commission_model->name = $input['name'];
                $this->service_2_commission_model->from = !empty($input['from']) ? (date('Y-m-d', strtotime($input['from'])) . " 00:00:00") : '';
                $this->service_2_commission_model->till = !empty($input['till']) ? (date('Y-m-d', strtotime($input['till'])) . " 23:59:59") : '';
               
                $this->service_2_commission_model->commission_default_flag = $input['default_template'];
                $this->service_2_commission_model->commission_type = "Incoming";
                $this->service_2_commission_model->created_by = $this->session->userdata('user_id');
                $this->service_2_commission_model->created_at = date("Y-m-d H:i:s", time());
                $this->service_2_commission_model->updated_by = $this->session->userdata('user_id');
                $this->service_2_commission_model->updated_at = date("Y-m-d H:i:s", time());
                $this->service_2_commission_model->status = "1";
                $commission_id = $this->service_2_commission_model->save();
                
                if ($commission_id) {
                    
                    $insert_data['commission_id'] = $commission_id;
                    $insert_data['trimax_commission'] = $input['msrtc_trimax_comm'];
                    $insert_data['company_commission'] = $input['msrtc_pos_commission'];
                    $insert_data['md_commission'] = $input['msrtc_md_commission'];
                    $insert_data['ad_commission'] = $input['msrtc_ad_commission'];
                    $insert_data['dist_commission'] = $input['msrtc_dist_commission'];
                    $insert_data['retailer_commission'] = $input['msrtc_retailer_commission'];
                    
                    
                    $this->service_2_commission_detail_model->commission_id = $commission_id;
                    $this->service_2_commission_detail_model->trimax_commission = $input['msrtc_trimax_comm'];
                    $this->service_2_commission_detail_model->company_commission = $input['msrtc_pos_commission'];

                    $this->service_2_commission_detail_model->md_commission = $input['msrtc_md_commission'];
                    $this->service_2_commission_detail_model->ad_commission = $input['msrtc_ad_commission'];
                    $this->service_2_commission_detail_model->dist_commission = $input['msrtc_dist_commission'];
                    $this->service_2_commission_detail_model->retailer_commission = $input['msrtc_retailer_commission'];
                    $res = $this->service_2_commission_detail_model->save();
                    
                    $this->session->set_userdata('commission_id', $commission_id);
                    $this->session->set_flashdata("msg" ,"Commission Created successfully.");
                } else {
                    $this->session->set_flashdata("msg" ,"Commission could not be created.");
                } 
            } else {
                $this->session->set_flashdata("msg" ,"Default Commission could not be created, as it is created already.");
            }
        } else {
            $this->session->set_flashdata("msg" ,"Some error occured.Please try again later.");
        }
        redirect(site_url() . '/admin/commission/redirect_commission');
    }

    public function gain_commission()
    {

    }
}
