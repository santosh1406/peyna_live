<?php

//Created on 21-12-2018
class Utilities_bbps extends CI_Controller {

    public $sessionData;
    public function __construct() {
        parent::__construct();
        $this->ci = & get_instance();
        
        $this->load->library('form_validation');
        $this->load->helper('utilities_bbps_helper');
        $this->load->library('Pdf');
        $this->load->helper('common_helper');

        $this->load->model(array('retailer_service_mapping_model','Utilities_bbps_model','common_model'));
        is_logged_in();
        

        $this->sessionData['id'] = $this->session->userdata('user_id');
        $this->sessionData["username"] = $this->session->userdata('username');
        $this->sessionData["password"] = $this->session->userdata('password');

        $this->header = array(
            'Content-Type: multipart/form-data; charset=utf-8',
            'X-API-KEY: ' . X_API_KEY . '',
        );
        $user_data = $this->session->userdata(); 
        $retailer_services = $this->retailer_service_mapping_model->getServiceByAgentId($user_data['user_id']);
        $this->service_array = array_column($retailer_services, 'service_id');
    }
    
    public function index() {       
        
        
        if(!in_array(BBPS_UTILITIES_SERVICE_ID, $service_array)){
            $this->ci->session->set_flashdata('msg', 'No access to module');
            redirect(base_url().'admin/dashboard');
        }
   }
     
    //Created by Vaidehi
    public function bbpsOperators() {
        if (isset($_GET['term'])) {
            $input['term'] = strtolower($_GET['term']);
            $input['type'] = $_GET['type'];

            if (!empty($input)) {
                
                $curl_url = base_url() . "rest_server/Utilities_bbps/bbpsOperators";
                $curlResult = curlForPostData($curl_url, $input, $this->sessionData, $this->header);
                $output = json_decode($curlResult);
                $result = json_encode($output->data);
                
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'ebix.log', $curlResult, 'Operators');
                echo $result;
            }
        }
    }
    
    /**
     * @description - To Pay bill 
     * @param array of bill details
     * @added by Vaidehi
     * 
     */
    public function pay_bbps_bill() {
        $input = $this->input->post();
        $session_user_id = $this->session->userdata('user_id');
        
        if (!empty($input)) {  
            $input['user_id'] = $session_user_id;
            $input['bill_amt'] = $this->session->userdata('bbps_amount');
                        
            $type = $input['type'];
            $bbps = $input['redirecttype'];
            $operatorId = $input['operator_id'];
            
            $config = $this->Utilities_bbps_model->get_validation($type, $operatorId); 
            
            if (form_validate_rules($config)) { 
                $curl_url = base_url() . "rest_server/utilities_bbps/get_bill";
                $curlResult = curlForPostData($curl_url, $input, $this->sessionData, $this->header);
                $output = json_decode($curlResult);
              //show($output,1);
                $this->session->set_flashdata('message', $output->msg);
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'bbps_recharge.log', $output, '4........pay_bill_final');
            }
        }
           redirect(base_url() . 'admin/utilities_bbps/' . $bbps .'bill');        
    }
    /**
     * @description - To get bill info 
     * @param array of bill details
     * @added by Vaidehi
     * @return json data
     */
    public function get_bill() {
        $data = array();
        
        $input['recharge_from'] = isset($_POST['recharge_from']) ? $_POST['recharge_from'] : '';
        $input['operator'] = isset($_POST['operator']) ? $_POST['operator'] : '';
        $input['operator_id'] = isset($_POST['operator_id']) ? $_POST['operator_id'] : '';
        $input['type'] = isset($_POST['type']) ? $_POST['type'] : '';
        $input['number'] = isset($_POST['number']) ? $_POST['number'] : '';
        $input['account_number'] = isset($_POST['account_number']) ? $_POST['account_number'] : '';
        $input['mobile_number'] = isset($_POST['mobile_number']) ? $_POST['mobile_number'] : '';
        $input['city'] = isset($_POST['city']) ? $_POST['city'] : '';
        $input['authenticator'] = isset($_POST['authenticator']) ? $_POST['authenticator'] : '';
        $input['c_mob_number'] = isset($_POST['c_mob_number']) ? $_POST['c_mob_number'] : '';
        $input['useremail'] = isset($_POST['useremail']) ? $_POST['useremail'] : '';
        $input['amount'] = isset($_POST['bill_amt']) ? $_POST['bill_amt'] : '';
        $session_user_id = $this->session->userdata('user_id');
        $input['user_id'] = $session_user_id;
        $input['redirecttype'] = isset($_POST['redirecttype']) ? $_POST['redirecttype'] : '';
        $input['submitval'] = isset($_POST['submitval']) ? $_POST['submitval'] : '';
        
        if (!empty($input)) {
            $config = $this->Utilities_bbps_model->get_validation($input['type'], $input['operator_id']); 
            if (form_validate_rules($config) == FALSE) {
                $error = $this->form_validation->error_array();
                $error['status_field'] = 'failed';
                $error['error_field'] = validation_errors("<div class='error'>", "</div>");
               // show($error);
                echo json_encode($error);
                exit;
            } else {
                $response = '';
                $curl_url = base_url() . "rest_server/utilities_bbps/get_bill";
                $response = curlForPostData($curl_url, $input, $this->sessionData, $this->header); 
                $bbps_amount = json_decode($response, true);
                //$bbps_amount = '10.00';//$bbps_amount['data']['check_result']['AMOUNT'];

                $this->session->set_userdata('bbps_amount', $bbps_amount);
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'get_bill_controller.log', $response, 'get_bill');

                $this->session->set_tempdata('msg', 'Response - Payment successfull', 3);                
                
                echo $response;
                exit;              

            }
        }
    }
    
    /**
    * @description - Load bbps gas utility page. 
    * @added by Vaidehi on 18-12-2018
    */
    public function bbpsgasbill() {
        if(in_array(BBPS_UTILITIES_SERVICE_ID, $this->service_array)){
             load_back_view(BBPS_GAS_VIEW);
        }else{
            $this->ci->session->set_flashdata('msg', 'No access to module');
            redirect(base_url().'admin/dashboard');
        }
    }
    
     /**
    * @description - Load bbps electricity utility page. 
    * @added by Vaidehi on 18-12-2018
    */
    public function bbpselectricitybill() {
        if(in_array(BBPS_UTILITIES_SERVICE_ID, $this->service_array)){
             load_back_view(BBPS_ELECTRICITY_VIEW);
        }else{
            $this->ci->session->set_flashdata('msg', 'No access to module');
            redirect(base_url().'admin/dashboard');
        }
    }
    
      /**
    * @description - Load bbps water utility page. 
    * @added by Vaidehi on 18-12-2018
    */
    public function bbpswaterbill() {
         if(in_array(BBPS_UTILITIES_SERVICE_ID, $this->service_array)){
             load_back_view(BBPS_WATER_VIEW);
        }else{
            $this->ci->session->set_flashdata('msg', 'No access to module');
            redirect(base_url().'admin/dashboard');
        }
    }
    
    /**
    * @description - Load bbps dth utility page. 
    * @added by Vaidehi on 18-12-2018
    */
    public function bbpsdth() {
        if(in_array(BBPS_UTILITIES_SERVICE_ID, $this->service_array)){
             load_back_view(BBPS_DTH_VIEW);
        }else{
            $this->ci->session->set_flashdata('msg', 'No access to module');
            redirect(base_url().'admin/dashboard');
        }
    }
    
    /**
   * @description - Load bbps mobile postpaid utility page. 
   * @added by Vaidehi on 18-12-2018
   */
    public function bbpsmobilepostpaid() {
         if(in_array(BBPS_UTILITIES_SERVICE_ID, $this->service_array)){
             load_back_view(BBPS_MOBILE_POSTPAID_VIEW);
        }else{
            $this->ci->session->set_flashdata('msg', 'No access to module');
            redirect(base_url().'admin/dashboard');
        }
    }
    
      /**
    * @description - Load bbps broadband utility page. 
    * @added by Vaidehi on 18-12-2018
    */
    public function bbpsbroadbandpostpaid() {
        if(in_array(BBPS_UTILITIES_SERVICE_ID, $this->service_array)){
              load_back_view(BBPS_BROADBAND_POSTPAID_VIEW);
        }else{
            $this->ci->session->set_flashdata('msg', 'No access to module');
            redirect(base_url().'admin/dashboard');
        }
    }
    
      /**
   * @description - Load bbps mobile postpaid utility page. 
   * @added by Vaidehi on 18-12-2018
   */
    public function bbpslandlinepostpaid() {
         if(in_array(BBPS_UTILITIES_SERVICE_ID, $this->service_array)){
             load_back_view(BBPS_LANDLINE_POSTPAID_VIEW);
        }else{
            $this->ci->session->set_flashdata('msg', 'No access to module');
            redirect(base_url().'admin/dashboard');
        }
    } 
    
    public function export_pdf($user_id = '') {
        if($user_id=="")
        {
          $user_id =  $this->session->userdata('user_id');
        }
        
        $trasaction_details = $this->Utilities_bbps_model->getTransactionReceipt($user_id);

        //show($trasaction_details,1);
        $pdf_gen                 = $this->generatePdf($trasaction_details);
       
    }
    
    public function generatePdf($user_id = ''){
        
        if($user_id==""){
              $user_id =  $this->session->userdata('user_id');
            }

        $trasaction_details = $this->Utilities_bbps_model->getTransactionReceipt($user_id);
        
        if(!empty($trasaction_details)){
            $result = $trasaction_details;
        }
        $datetime = date('Y-m-d H:i:s');
        $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $obj_pdf->SetCreator(PDF_CREATOR);
        $title = "Electricity Receipt";
        $obj_pdf->SetTitle($title);

        //$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
        $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $obj_pdf->SetDefaultMonospacedFont('helvetica');
        $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $obj_pdf->SetFont('helvetica', '', 9);
        $obj_pdf->setFontSubsetting(false);
        $obj_pdf->AddPage();
        ob_start();
        $a_mail_replacement_array = array(

                                    '{{service_name}}' => ucwords($result[0]["type"]),
                                    "{{customer_name}}" => $result[0]['customerName'],
                                    "{{mobile_no}}" => $result[0]['mobileno'],
                                    "{{paid_by}}" => 'Cash',
                                    "{{amount}}" => $result[0]['amount'],   
                                    "{{customer_charge}}" => $result[0]['customer_charge'],
                                    "{{total_amount}}" =>  $result[0]['amount'] + $result[0]['customer_charge'],
                                    "{{amount_in_words}}" => ucwords(getIndianCurrency($result[0]['amount'] + $result[0]['customer_charge'])),
                                    "{{trans_ref_no}}" => $result[0]['transactionReference'],                                
                                    "{{rokad_trans_no}}" => $result[0]['wallet_transaction_no']
                                    );
          // show($a_mail_replacement_array,1);
        $content = $data['agent_mail_content'] = str_replace(array_keys($a_mail_replacement_array),array_values($a_mail_replacement_array),BBPS_ELECTRICITY_RECEIPT);

           // show($content,1);     
//        ob_end_clean();
//        $obj_pdf->writeHTML($content, true, false, true, false, '');
//        $obj_pdf->Output('Electricity Bill Receipt.pdf', 'I');
        $filePath = $_SERVER['DOCUMENT_ROOT'] . 'electricity/';
        $filename = $filePath.'Electricity_Bill_Receipt'.'_'.$datetime.'.pdf';
        
        $lastInsertedId = $this->Utilities_bbps_model->getTransactionLastId($user_id);                
        //show($lastInsertedId,1);
        $update_data = array();
        
        $update_data = array(
        'receipt_name' => $filename,
        'receipt_data' => $content
        );
        
        //show($update_data,1);
        $table_name = 'bbps_transaction_details';
        $id = $this->common_model->update($table_name, 'id', $lastInsertedId, $update_data);
        // show($content,1);     
        ob_end_clean();
        $fileData = array();
        $fileData['file_name'] = $filename;
        $fileData['file_content'] = $content;
        return $fileData;
    }
    
//    public function print_ticket($wallet_trans_id) {
//       
//        
//        $trasaction_details = $this->Utilities_bbps_model->print_ticket($wallet_trans_id);
//
//        //show($trasaction_details,1);
//        $pdf_gen                 = $this->generatePdf($trasaction_details);
//       
//    }
    
    public function receipt_page(){
        $file_data = $this->generatePdf();
                  
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/bbps/' . 'bbps_receipt.log', $file_data, 'Electricity Receipt');
        $this->session->set_userdata('file_data', $file_data);
        load_back_view(ELECTRICITY_RECEIPT_PAGE);
    }
    
   
        
}
