<?php
class Merchant extends MY_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();

        is_logged_in();

        $this->load->model(array('merchant_model', 'city_master_model', 'state_master_model'));
    }


    public function index(){
        load_back_view(MERCHANT_VIEW);
    }

    public function list_of_groups(){
        $action = '';
        
        $this->datatables->select("id,CONCAT(first_name,' ',last_name) as name,company_name");
        $this->datatables->from("merchants");
          
        $action .= '<a href="' . site_url('admin/merchant/edit') . '/$1" class="btn btn-primary btn-xs edit_btn" data-ref="$1" title="Edit Merchant" > <i class="fa fa-edit"></i> </a>&nbsp;';
       
        $action .= ' <a href="' . site_url('admin/merchant/delete') . '/$1" class="btn btn-primary btn-xs delete_btn" data-ref="$1" title="Delete Merchant"> <i class="fa fa-trash"></i> </a>';
        $this->datatables->add_column('action', $action, 'id');
        
        $data = $this->datatables->generate('json');
        // echo "<pre>"; print_r($data);die;
        echo $data;die;
    }

    public function create(){
      $data['states'] = $data['states'] = $this->state_master_model->get_all_state();
      load_back_view(MERCHANT_CREATE_VIEW, $data);
    }

    public function save(){
      if ($postData = $this->input->post(NULL,TRUE)){
          $config = array(
            // array('field' => 'merchant_type', 'label' => 'Merchant Type', 'rules' => 'trim|required', 'errors' => array('required' => 'Please select a Merchant type.')),
            array('field' => 'company_name', 'label' => 'Name of Firm/Company', 'rules' => 'trim|xss_clean|required', 'errors' => array('required' => 'Please enter name of firm/company')),
            array('field' => 'address_line_1', 'label' => 'Address Line1', 'rules' => 'trim|xss_clean|required', 'errors' => array('required' => 'Please enter address line1')),
            array('field' => 'pincode', 'label' => 'Pincode', 'rules' => 'trim|xss_clean|required|integer', 'errors' => array('required' => 'Please enter pincode')),
            array('field' => 'pan_card', 'label' => 'Pancard number', 'rules' => 'trim|xss_clean|min_length[10]|max_length[10]'),
            array('field' => 'alternate_contact', 'label' => 'Contact No.(Alt)', 'rules' => 'trim|xss_clean|min_length[10]|max_length[10]'),
            array('field' => 'first_name', 'label' => 'First Name', 'rules' => 'trim|xss_clean|required', 'errors' => array('required' => 'Please enter first name.')),
            array('field' => 'last_name', 'label' => 'Last Name', 'rules' => 'trim|xss_clean|required', 'errors' => array('required' => 'Please enter last name.')),
            array('field' => 'email', 'label' => 'Email', 'rules' => 'trim|xss_clean|required|valid_email', 'errors' => array('required' => 'Please enter valid email.')),
            array('field' => 'mobile', 'label' => 'Mobile Numebr', 'rules' => 'trim|xss_clean|required|min_length[10]|max_length[10]'),
            array('field' => 'state_id', 'label' => 'State', 'rules' => 'trim|xss_clean|required'),
            array('field' => 'city_id', 'label' => 'City', 'rules' => 'trim|xss_clean|required'),
          );

          if (form_validate_rules($config)) {
            $postData['pan_card'] = rokad_encrypt($postData['pan_card'], $this->config->item("pos_encryption_key"));
            $postData['pincode'] = rokad_encrypt($postData['pincode'], $this->config->item("pos_encryption_key"));
            $postData['email'] = rokad_encrypt($postData['email'], $this->config->item("pos_encryption_key"));
            $postData['mobile'] = rokad_encrypt($postData['mobile'], $this->config->item("pos_encryption_key"));
            $this->merchant_model->insert($postData);
            $this->session->set_flashdata('msg_type', 'success');
            $this->session->set_flashdata('msg', 'Merchant Saved');
            redirect(base_url() . "admin/merchant");            
          }else{
            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'Please enter proper data.');
            redirect(base_url() . "admin/merchant/create");
          }
      }else{
        redirect(base_url() . "admin/merchant/create");
      }
    }

    public function edit($id){
      $data['merchant'] = $this->merchant_model->find($id);
      $data['states'] = $this->state_master_model->get_all_state();
      $data['cities'] = $this->get_statewise_city($data['merchant']->state_id);
      $data['cities'] = $data['cities']['city'];
      load_back_view(MERCHANT_EDIT_VIEW, $data);
    }

    public function update($id){
      if ($postData = $this->input->post(NULL,TRUE)){
          $config = array(
            // array('field' => 'merchant_type', 'label' => 'Merchant Type', 'rules' => 'trim|required', 'errors' => array('required' => 'Please select a Merchant type.')),
            array('field' => 'company_name', 'label' => 'Name of Firm/Company', 'rules' => 'trim|xss_clean|required', 'errors' => array('required' => 'Please enter name of firm/company')),
            array('field' => 'address_line_1', 'label' => 'Address Line1', 'rules' => 'trim|xss_clean|required', 'errors' => array('required' => 'Please enter address line1')),
            array('field' => 'pincode', 'label' => 'Pincode', 'rules' => 'trim|xss_clean|required|integer', 'errors' => array('required' => 'Please enter pincode')),
            array('field' => 'pan_card', 'label' => 'Pancard number', 'rules' => 'trim|xss_clean|min_length[10]|max_length[10]'),
            array('field' => 'alternate_contact', 'label' => 'Contact No.(Alt)', 'rules' => 'trim|xss_clean|min_length[10]|max_length[10]'),
            array('field' => 'first_name', 'label' => 'First Name', 'rules' => 'trim|xss_clean|required', 'errors' => array('required' => 'Please enter first name.')),
            array('field' => 'last_name', 'label' => 'Last Name', 'rules' => 'trim|xss_clean|required', 'errors' => array('required' => 'Please enter last name.')),
            array('field' => 'email', 'label' => 'Email', 'rules' => 'trim|xss_clean|required|valid_email', 'errors' => array('required' => 'Please enter valid email.')),
            array('field' => 'mobile', 'label' => 'Mobile Numebr', 'rules' => 'trim|xss_clean|required|min_length[10]|max_length[10]'),
            array('field' => 'state_id', 'label' => 'State', 'rules' => 'trim|xss_clean|required'),
            array('field' => 'city_id', 'label' => 'City', 'rules' => 'trim|xss_clean|required'),
          );

          if (form_validate_rules($config)) {
            $postData['pan_card'] = rokad_encrypt($postData['pan_card'], $this->config->item("pos_encryption_key"));
            $postData['pincode'] = rokad_encrypt($postData['pincode'], $this->config->item("pos_encryption_key"));
            $postData['email'] = rokad_encrypt($postData['email'], $this->config->item("pos_encryption_key"));
            $postData['mobile'] = rokad_encrypt($postData['mobile'], $this->config->item("pos_encryption_key"));
            $this->merchant_model->update($id, $postData);
            $this->session->set_flashdata('msg_type', 'success');
            $this->session->set_flashdata('msg', 'Merchant Saved');
            redirect(base_url() . "admin/merchant");            
          }else{
            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'Please enter proper data.');
            redirect(base_url() . "admin/merchant/create");
          }
      }else{
        redirect(base_url() . "admin/merchant/create");
      }
    }

    public function get_statewise_city($state_id = null) {
      $data = array();
      $input = array();
      $input = $this->input->post();
      $stateId = ($input['state_id'] != '') ? $input['state_id']:$state_id ;
      $data['city'] = $this->city_master_model->get_city($stateId);
      if ($state_id == null)
        data2json($data);
      else
        return $data;
    }

    public function is_registered() {
        $input = $this->input->post();
        $where_user = array(
            "email" => rokad_encrypt($postData['email'], $this->config->item("pos_encryption_key"))
        );

        $count = $this->merchant_model->where($where_user)->count_all();
        $flag = ($count) ? 'true' : 'false';
        echo $flag;
    }

    public function delete($id){
        $response = array();
        
        $role_data = $this->merchant_model
                        ->where("id", $id)
                        ->as_array()->find_all();
        //echo "<pre>"; var_dump(!$role_data); die;
       
        if ($role_data) {
            $this->db->where('id=', $id);
            $this->db->delete('merchants');
            $this->session->set_flashdata('msg', 'Merchant delete sucessfully');
        } else {
            $this->session->set_flashdata('msg', 'Merchant already deleted');
        }
        redirect(site_url('admin/merchant'));
    }
}

//end of login class