<?php

class Frontend_registration extends MY_Controller {

    public function __construct() {
        parent::__construct();
        is_logged_in();
        $this->load->library('form_validation');
        $this->load->library("Excel");
        $this->load->model(array(
            'users_model',
            'role_model',
            'user_login_count_model',
            'wallet_model',
            'state_master_model',
            'city_master_model',
            'commission_model',
            'agent_commission_template_log_model',
            'user_email_model',
            'buisness_type_model',
            'common_model',
            'agent_bank_detail_model',
            'retailer_service_mapping_model',
            'userwise_seller_point_model',
            'Sales_agent_mapping_model'
        ));
    }

    /*
     * @function : users
     * @param :
     * @detail : all users list.
     *
     */

    public function index() {
        $data = array();
        
        load_back_view(ALL_FRONTEND_REGISTERED_USERS, $data);
    }

    /*
     * @function : get_list
     * @param :
     * @detail : all users list according to role(operator,agent).
     *
     */

    function get_list() {
        is_ajax_request();
        $input = $this->input->post(NULL, TRUE);
        $summary = $this->users_model->fetch_all_users_regfromfront($input, 'json');
        echo $summary;
    }

    /*
     * @function : edit_detail
     * @param :
     * @detail : edit user detail.
     *
     */

    public function edit_details($id) {

        $id = base64_decode($id);
        if ($id != "") {
            $where_condition = array(
                'id' => $id
            );

            $data = $this->register_retailor_detail();

            $data ['edit_data'] = $this->users_model->get_alluser($where_condition);
            $data ['bank_data'] = $this->agent_bank_detail_model->get_data($id);
            $data ['user_seller_point'] = $this->userwise_seller_point_model->where('user_id', $id)->find_all();
            $data ['city'] = $this->city_master_model->get_all_city();
            $data ['states'] = $this->state_master_model->get_all_state();

            if ($data['edit_data'][0]['level'] == POS_DISTRIBUTOR_LEVEL) {
                //$result =$this->users_model->column('level_5,agent_code')->where('id',$id)->find_all();
                $data ['sellerpoints'] = $this->buisness_type_model->get_sellerpoints();
                $data ['user_seller_point'] = $this->userwise_seller_point_model->where('user_id', $id)->where('status', 'Y')->find_all();
                //$data['current_cb_executive_edit'] = TRUE;
            }

            // added by vijay
            $data['creat_role'] = RETAILER_ROLE_ID;
            $data["serviceDetails"] = $this->buisness_type_model->get_service();
            $data["agentServices"] = $this->buisness_type_model->get_agent_services($id);
            $data['division_cd'] =$this->Sales_agent_mapping_model->get_agent_division_code($id);            
            //added by sonali on 17jan2019
            $data["depotsCode"] = $this->Sales_agent_mapping_model->get_depots_list();
            //var_dump($data["depotsCode"]);die;
            //echo "<pre>";print_r($data);die;
            // added by vijay
            //show($data["depotsCode"],1);
            load_back_view(EDIT_FRONTEND_USERS, $data);
        } else {
            redirect(base_url() . 'admin/registration');
        }
    }

    private function register_retailor_detail() {
        //$data['states'] = $this->state_master_model->get_all_state();
        $data['btypes'] = $this->buisness_type_model->get_buisnessType();
        $data['schemes'] = $this->buisness_type_model->get_schemes();
        //$data['sellerpoints'] = $this->buisness_type_model->get_sellerpoints();
        //if ($this->session->userdata('role_id') == DISTRIBUTOR) {
        $user_id = $this->session->userdata("user_id");
        $data['sellerpoints'] = $this->users_model->get_executive_sellerpoints($user_id);
        //}
        // $data['depots'] = $this->buisness_type_model->get_depots();
        //$data['serviceDetails'] = $this->buisness_type_model->get_service();
        //show($data['serviceDetails'],1);
        //$data['creat_role'] = RETAILER_ROLE_ID;
        $data['level'] = RETAILOR_LEVEL;
        $data['role_id'] = rokad_encrypt(RETAILER_ROLE_ID, $this->config->item('pos_encryption_key'));

        $data ['cmp_on_change_flag'] = TRUE;
        $data ['md_on_change_flag'] = TRUE;
        $data ['ad_on_change_flag'] = TRUE;

        $data ['cmp_drop_down_flag'] = FALSE;
        $data ['md_drop_down_flag'] = FALSE;
        $data ['ad_drop_down_flag'] = FALSE;
        $data ['d_drop_down_flag'] = FALSE;

        $data['show_retailor_flag'] = TRUE;

        $user_id = $this->session->userdata('id');
        if ($this->session->userdata('level') < COMPANY_LEVEL) {
            $data ['cmp_drop_down_flag'] = TRUE;
            $data ['all_cmp'] = $this->users_model->fetch_all_cmp();
        }

        if ($this->session->userdata('level') < MASTER_DISTRIBUTOR_LEVEL) {
            $data ['md_drop_down_flag'] = TRUE;
            $data ['all_mds'] = $this->users_model->fetch_all_md($user_id);
        }

        if ($this->session->userdata('level') < AREA_DISTRIBUTOR_LEVEL) {
            $data ['ad_drop_down_flag'] = TRUE;
            $data ['area_distributor'] = $this->users_model->fetch_all_ad($user_id);
        }

        if ($this->session->userdata('level') < POS_DISTRIBUTOR_LEVEL) {
            $data ['d_drop_down_flag'] = TRUE;
            $data ['distributors'] = $this->users_model->fetch_all_distributors($user_id);
        }
        return $data;
    }

    /*
     * @function : updat_details
     * @param :
     * @detail : update user detail.
     *
     */

    public function update_details() {
        $input = $this->input->post(NULL, TRUE);
        //show($input,1);
        if (!empty($input['service_name'])) {
            $service_name = $input['service_name'];
        } else {
            $service_name = '';
        }
        array_push($service_name, EPAY_SERVICE_ID);
        $serviceArrStr = implode(",", $service_name);
        
        logging_data("update/update_profile_log.log", $input, "Post data for update profile");
        $encrypted_role_id = isset($input ['role']) ? trim($input ['role']) : "";
        $level = isset($input ['level']) ? trim($input ['level']) : "";
        $firm_name = isset($input ['firm_name']) ? trim($input ['firm_name']) : "";
        $first_name = isset($input ['first_name']) ? trim($input ['first_name']) : "";
        $last_name = isset($input ['last_name']) ? trim($input ['last_name']) : "";
        $display_name = $first_name . " " . $last_name;
        //$gender = isset($input ['gender']) ? trim($input ['gender']) : "";
        $address1 = isset($input ['address1']) ? trim($input ['address1']) : "";
        $address2 = isset($input ['address2']) ? trim($input ['address2']) : "";
        //$agent_state = isset($input ['agent_state']) ? trim($input ['agent_state']) : "";
        //$agent_city = isset($input ['agent_city']) ? trim($input ['agent_city']) : "";
        $pincode = isset($input ['pincode']) ? trim($input ['pincode']) : "";
        $pancard = isset($input ['pancard']) ? trim($input ['pancard']) : "";
        $adharcard = isset($input ['adharcard']) ? trim($input ['adharcard']) : "";
        $email = isset($input ['email']) ? trim($input ['email']) : "";
        $company_gst_no = isset($input ['company_gst_no']) ? trim($input ['company_gst_no']) : "";
        $mobile_no = isset($input ['mobile_no']) ? trim($input ['mobile_no']) : "";

        $topuplmit = isset($input ['topuplmit']) ? trim($input ['topuplmit']) : "";

        //$dob = isset($input ['dob']) ? date('Y-m-d', strtotime($input ['dob'])) : "";
        $badge_no = (isset($input['badge_no']) && trim($input['badge_no']) != '') ? trim($input['badge_no']) : NULL;
        $new_pincode = rokad_encrypt($pincode, $this->config->item("pos_encryption_key"));
        $new_dob = rokad_encrypt($dob, $this->config->item("pos_encryption_key"));

        $user_id = rokad_decrypt($input ['id'], $this->config->item("pos_encryption_key"));
        
        if ($input['creat_role'] != '' && $input['creat_role'] == RETAILER_ROLE_ID) {
            $agent_cd = "AGT" . str_pad(($user_id), 5, 0, STR_PAD_LEFT);
        } else {
            $agent_cd = '';
        }

        $level1 = $this->users_model->get_trimax_user_id();
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');
        $badge_no = (isset($input['badge_no']) && trim($input['badge_no']) != '') ? trim($input['badge_no']) : NULL;
        //echo $session_level; echo "<br>"; echo $level1; die;
        
        // encryption done here
        $new_pancard = rokad_encrypt($pancard, $this->config->item("pos_encryption_key"));
        $new_adharcard = rokad_encrypt($adharcard, $this->config->item("pos_encryption_key"));
        $new_mobile = rokad_encrypt($mobile_no, $this->config->item("pos_encryption_key"));
        $new_email = rokad_encrypt($email, $this->config->item("pos_encryption_key"));

        $ac_no = trim($input['accno']);
        $bank_nm = trim($input['bname']);
        $bran_nm = trim($input['brname']);
        $ifsc_code = trim($input['ifsc']);
        $accname = trim($input['accname']);

        if ($session_level == MASTER_DISTRIBUTOR_LEVEL) {
            $session_cmp_id = $this->users_model->get_parent_id($session_id, COMPANY_LEVEL);
        } else if ($session_level == AREA_DISTRIBUTOR_LEVEL) {
            $session_md_id = $this->users_model->get_parent_id($session_id, MASTER_DISTRIBUTOR_LEVEL);
            $session_cmp_id = $this->users_model->get_parent_id($session_md_id, COMPANY_LEVEL);
        } else if ($session_level == POS_DISTRIBUTOR_LEVEL) {
            $session_ad_id = $this->users_model->get_parent_id($session_id, AREA_DISTRIBUTOR_LEVEL);
            $session_md_id = $this->users_model->get_parent_id($session_ad_id, MASTER_DISTRIBUTOR_LEVEL);
            $session_cmp_id = $this->users_model->get_parent_id($session_md_id, COMPANY_LEVEL);
        }

        if ($level == COMPANY_LEVEL) {
            $level2 = NULL;
            $level3 = NULL;
            $level4 = NULL;
            $level5 = NULL;
            $level6 = NULL;
        } else if ($level == MASTER_DISTRIBUTOR_LEVEL) {
            if ($session_level == COMPANY_LEVEL) {
                $level2 = isset($session_id) ? trim($session_id) : NULL;
                $level3 = NULL;
                $level4 = NULL;
                $level5 = NULL;
                $level6 = NULL;
            } else {
                $level2 = isset($input ['cmp']) ? trim($input ['cmp']) : NULL;
                $level3 = NULL;
                $level4 = NULL;
                $level5 = NULL;
                $level6 = NULL;
            }
        } else if ($level == AREA_DISTRIBUTOR_LEVEL) {
            if ($session_level == COMPANY_LEVEL) {
                $level2 = isset($session_id) ? trim($session_id) : NULL;
                $level3 = isset($input ['md']) ? trim($input ['md']) : NULL;
                $level4 = NULL;
                $level5 = NULL;
                $level6 = NULL;
            } else if ($session_level == MASTER_DISTRIBUTOR_LEVEL) {
                $level2 = isset($session_cmp_id) ? trim($session_cmp_id) : NULL;
                $level3 = isset($session_id) ? trim($session_id) : NULL;
                $level4 = NULL;
                $level5 = NULL;
                $level6 = NULL;
            } else {
                $level2 = isset($input ['cmp']) ? trim($input ['cmp']) : NULL;
                $level3 = isset($input ['md']) ? trim($input ['md']) : NULL;
                $level4 = NULL;
                $level5 = NULL;
                $level6 = NULL;
            }
        } else if ($level == POS_DISTRIBUTOR_LEVEL) {
            if ($session_level == COMPANY_LEVEL) {
                $level2 = isset($session_id) ? trim($session_id) : NULL;
                $level3 = isset($input ['md']) ? trim($input ['md']) : NULL;
                $level4 = isset($input ['ad']) ? trim($input ['ad']) : NULL;
                $level5 = NULL;
                $level6 = NULL;
            } else if ($session_level == MASTER_DISTRIBUTOR_LEVEL) {
                $level2 = isset($session_cmp_id) ? trim($session_cmp_id) : NULL;
                $level3 = isset($session_id) ? trim($session_id) : NULL;
                $level4 = isset($input ['ad']) ? trim($input ['ad']) : NULL;
                $level5 = NULL;
                $level6 = NULL;
            } else if ($session_level == AREA_DISTRIBUTOR_LEVEL) {
                $level2 = isset($session_cmp_id) ? trim($session_cmp_id) : NULL;
                $level3 = isset($session_md_id) ? trim($session_md_id) : NULL;
                $level4 = isset($session_id) ? trim($session_id) : NULL;
                $level5 = NULL;
                $level6 = NULL;
            } else {
                $level2 = isset($input ['cmp']) ? trim($input ['cmp']) : NULL;
                $level3 = isset($input ['md']) ? trim($input ['md']) : NULL;
                $level4 = isset($input ['ad']) ? trim($input ['ad']) : NULL;
                $level5 = NULL;
                $level6 = NULL;
            }
        } else if ($level == RETAILOR_LEVEL) {
            if ($session_level == COMPANY_LEVEL) {
                $level2 = isset($session_id) ? trim($session_id) : NULL;
                $level3 = isset($input ['md']) ? trim($input ['md']) : NULL;
                $level4 = isset($input ['ad']) ? trim($input ['ad']) : NULL;
                $level5 = isset($input ['distributor']) ? trim($input ['distributor']) : NULL;
                $level6 = NULL;
            } else if ($session_level == MASTER_DISTRIBUTOR_LEVEL) {
                $level2 = isset($session_cmp_id) ? trim($session_cmp_id) : NULL;
                $level3 = isset($session_id) ? trim($session_id) : NULL;
                $level4 = isset($input ['ad']) ? trim($input ['ad']) : NULL;
                $level5 = isset($input ['distributor']) ? trim($input ['distributor']) : NULL;
                $level6 = NULL;
            } else if ($session_level == AREA_DISTRIBUTOR_LEVEL) {
                $level2 = isset($session_cmp_id) ? trim($session_cmp_id) : NULL;
                $level3 = isset($session_md_id) ? trim($session_md_id) : NULL;
                $level4 = isset($session_id) ? trim($session_id) : NULL;
                $level5 = isset($input ['distributor']) ? trim($input ['distributor']) : NULL;
                $level6 = NULL;
            } else if ($session_level == POS_DISTRIBUTOR_LEVEL) {
                $level2 = isset($session_cmp_id) ? trim($session_cmp_id) : NULL;
                $level3 = isset($session_md_id) ? trim($session_md_id) : NULL;
                $level4 = isset($session_ad_id) ? trim($session_ad_id) : NULL;
                $level5 = isset($session_id) ? trim($session_id) : NULL;
                $level6 = NULL;
            } else {
                $level2 = isset($input ['cmp']) ? trim($input ['cmp']) : NULL;
                $level3 = isset($input ['md']) ? trim($input ['md']) : NULL;
                $level4 = isset($input ['ad']) ? trim($input ['ad']) : NULL;
                $level5 = isset($input ['distributor']) ? trim($input ['distributor']) : NULL;
                $level6 = NULL;
            }
        }

        if ($level == MASTER_DISTRIBUTOR_LEVEL) {
            $active_commison = $this->commission_model->commission_id();
        }
        
        // decryption done herer
        $role_id = rokad_decrypt($encrypted_role_id, $this->config->item("pos_encryption_key"));

        $businesstype = trim($input['bType']);
        $scheme = isset($input['scheme']) ? trim($_POST['scheme']) : "";
        if ($_POST['sellerpoint'] != '') {
            $sellercode = trim($_POST['sellerpoint']);
            $selle_rname = $this->common_model->get_value('cb_bus_stops', 'BUS_STOP_NM', 'BUS_STOP_CD="' . $sellercode . '"');
            $sellername = $selle_rname->BUS_STOP_NM;
        } else {
            $sellercode = "";
            $sellername = "";
        }
        $sellerdescription = isset($input['sellerdesc']) ? trim($_POST['sellerdesc']) : "";
        $topUpLimit = isset($input['topUpLimit']) ? trim($_POST['topUpLimit']) : "";
        $hhmLimit = isset($input['hhmLimit']) ? trim($_POST['hhmLimit']) : "";
        $user_seller_point = isset($input['user_seller_point']) ? $_POST['user_seller_point'] : "";
        if ($_POST['depot_code'] != '') {
            $depot_code = trim($_POST['depot_code']);
            $depot_name = $this->common_model->get_value('cb_depots', 'DEPOT_NM', 'DEPOT_CD="' . $depot_code . '"');
            $depotname = $depot_name->DEPOT_NM;
        } else {
            $depot_code = "";
            $depot_name = "";
        }
        $securityDep = isset($input['securityDep']) ? trim($_POST['securityDep']) : "";
        // show($user_seller_point,1);



        $config = array(
            array(
                'field' => 'firm_name',
                'label' => 'Name of Firm/Company',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please enter name of firm/company'
                )
            ),
            array(
                'field' => 'first_name',
                'label' => 'First Name',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please enter first name'
                )
            ),
            array(
                'field' => 'last_name',
                'label' => 'Last Name',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please enter last nanme'
                )
            ),
            /*array(
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'trim|xss_clean'
            ),*/
            /* array(
              'field' => 'dob',
              'label' => 'Date of birth',
              'rules' => 'trim|xss_clean|required|callback_validate_age',
              'errors' => array(
              'required' => 'Please select your age'
              )
              ), */
            array(
                'field' => 'address1',
                'label' => 'Address1',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please enter the address'
                )
            ),
            array(
                'field' => 'address2',
                'label' => 'Address2',
                'rules' => 'trim|xss_clean'
            ),
            /*array(
                'field' => 'agent_state',
                'label' => 'Agent State',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please select the state'
                )
            ),
            array(
                'field' => 'agent_city',
                'label' => 'Agent City',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please select the city'
                )
            ),*/
            array(
                'field' => 'pincode',
                'label' => 'Pincode',
                'rules' => 'trim|xss_clean|required|numeric|min_length[6]|max_length[6]',
                'errors' => array(
                    'required' => 'Please enter the pin code'
                )
            )
        );
        if (form_validate_rules($config)) {
            $update_array_users = array(
                "role_id" => $role_id,
                "firm_name" => $firm_name,
                "first_name" => $first_name,
                "last_name" => $last_name,
                "display_name" => $display_name,
                "address1" => $address1,
                "address2" => $address2,
                //"state" => $agent_state,
                //"city" => $agent_city,
                "pincode" => $new_pincode,
                //"gender" => $gender,
                //"dob" => $new_dob,
                "topuplmit" => $topuplmit,
                "badge_no" => $badge_no,
                "agent_code" => $agent_cd,
                "level" => $level,
                "level_1" => $level1, // here only for staging.
                "level_2" => $level2,
                "level_3" => $level3,
                "level_4" => $level4,
                "level_5" => $level5,
                "level_6" => $level6,
                "btypeid" => $businesstype,
                "schemeid" => $scheme,
                "topuplmit" => $topUpLimit,
                "hmmlimit" => $hhmLimit,
                "securitydeposite" => $securityDep,
                "star_seller_code" => $sellercode,
                "star_seller_name" => $sellername,
                "location" => $sellerdescription,
                "depot_cd" => $depot_code,
                "depot_name" => $depotname,
                "reg_status" => 'Y',
                "updated_by" => $this->session->userdata("user_id"),
                "updated_date" => date("Y-m-d h:i:s:a"),
                "pancard" => $new_pancard,
                "adharcard" => $new_adharcard,
                "mobile_no" => $new_mobile,
                "company_gst_no" => $company_gst_no,
                "email" => $new_email
            );

            $update_array_bankdata = array(
                "bank_acc_name" => $accname,
                "bank_acc_no" => $ac_no,
                "bank_name" => $bank_nm,
                "bank_branch" => $bran_nm,
                "bank_ifsc_code" => $ifsc_code,
                "updated_by" => $this->session->userdata("user_id"),
                "updated_date" => date("Y-m-d h:i:s:a")
            );

            $update = $this->users_model->update_where('id', $user_id, $update_array_users);
            $update = $this->agent_bank_detail_model->update_where('agent_id', $user_id, $update_array_bankdata);

            if ($input['creat_role'] == RETAILER_ROLE_ID && !empty($service_name)) {
                $sql = "select service_id,depot_code,division_code from retailer_service_mapping where status = 'Y' and agent_id='" . $user_id . "' and service_id = '" . SMART_CARD_SERVICE . "' ";
                $run = $this->db->query($sql);                
                $res = $run->row();
                
                
//                echo $res->service_id;
//                show($res);
//                show($service_name,1);
                foreach ($service_name as $service_id) {
                    //if(count($res)>0){
                        if($service_id == $res->service_id){
                        
                        
                        $update_service = "update retailer_service_mapping set updated_at = '".date('Y-m-d h:i:s')."',created_by = '".$this->session->userdata('user_id')."',depot_code = '".$input['depot_cd']."',division_code ='".$input['division_cd']."' WHERE agent_id='" . $user_id . "' and service_id  = '".$res->service_id."'";
                        $this->db->query($update_service);
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/fronted_registration/' . 'fronted_registration.log', $update_service, 'update service');
                       
                    }
                        
                   // }
                    
                    else{
                        if($service_id == SMART_CARD_SERVICE ){
                             $data = array(
                            'agent_id' => $user_id,
                            'service_id' => $service_id,
                            'created_by' => $this->session->userdata('user_id'),
                            'depot_code' => $input['depot_cd'],
                            'division_code' => $input['division_cd']
                        
                            );
                            $insert_service = $this->retailer_service_mapping_model->saveServiceDetails($data);
                        }else{
                             $data = array(
                        'agent_id' => $user_id,
                        'service_id' => $service_id,
                        'created_by' => $this->session->userdata('user_id'),
                        
                    );
                            $insert_service = $this->retailer_service_mapping_model->saveServiceDetails($data);
                            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/fronted_registration/' . 'fronted_registration.log', $insert_service, 'insert  service');
                       
                        }
                            
                            }
                    
                }
                $logData = array(
                    'agent_id' => $user_id,
                    'service_id' => $serviceArrStr,
                    'status' => "Y",
                    'created_by' => $this->session->userdata('user_id'),
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $insert_service_log = $this->retailer_service_mapping_model->saveServiceDetailsLog($logData);
            }
            
            $account_no = substr(hexdec(uniqid()), 4, 12);
            $wallet_array = [
                'user_id' => $user_id,
                'amt' => 0.00,
                'status' => "Y",
                'comment' => "Created Account",
                'added_by' => $this->session->userdata('user_id'),
                'updated_by' => $this->session->userdata('user_id'),
                'account_no' => $account_no
            ];
            $wallet_id = $this->wallet_model->insert($wallet_array);
            
            if (!empty($active_commison)) {
                $agent_commission_log = [
                    "user_id" => $user_id,
                    "commission_id" => $active_commison [0]->id,
                    "commission_from" => $active_commison [0]->from,
                    "commission_to" => $active_commison [0]->till,
                    "status" => '1',
                    "added_on" => date('Y-m-d H:i:s'),
                    "added_by" => $this->session->userdata("user_id")
                ];
                $this->agent_commission_template_log_model->insert($agent_commission_log);
            }

            if ($input['creat_role'] == DISTRIBUTOR && !empty($user_seller_point)) {
                foreach ($user_seller_point as $key => $value) {
                    if (!empty($value)) {
                        $seller_data = array(
                            "user_id" => $last_insert_id_users,
                            "seller_code" => $value,
                            "status" => 'Y'
                        );
                        $this->userwise_seller_point_model->insert($seller_data);
                    }
                }
            }

            if ($input['creat_role'] == RETAILER_ROLE_ID && !empty($sellercode)) {
                $seller_data = array(
                    "user_id" => $user_id,
                    "seller_code" => $sellercode,
                    "depot_cd" => $depot_code,
                    "status" => 'Y'
                );
                $this->userwise_seller_point_model->insert($seller_data);
            }
            if (!empty($wallet_id)) {
                // Send sms
                $reg_sms_array = array(
                    "{{username}}" => ucwords($display_name),
                    "{{email}}" => $email
                );
                $temp_sms_msg = str_replace(array_keys($reg_sms_array), array_values($reg_sms_array), BOS_REGISTRATION);
            //    $is_send = sendSMS($new_mobile, $temp_sms_msg);

            //    $response = save_n_send_activation_key($user_id, $email, $display_name, 'email_verification');
                $this->session->set_flashdata('msg_type', 'success');
                $this->session->set_flashdata('msg', 'Registration completed successfully. Please check your registered Email ID for activation');
                redirect(base_url() . 'admin/frontend_registration/');
            }
        } else {
            $where_condition = array(
                'id' => $user_id
            );
            $data = $this->register_retailor_detail();

            $data ['edit_data'] = $this->users_model->get_alluser($where_condition);
            $data ['bank_data'] = $this->agent_bank_detail_model->get_data($user_id);
            $data ['user_seller_point'] = $this->userwise_seller_point_model->where('user_id', $user_id)->find_all();
            $data ['city'] = $this->city_master_model->get_all_city();
            $data ['states'] = $this->state_master_model->get_all_state();

            if ($data['edit_data'][0]['level'] == POS_DISTRIBUTOR_LEVEL) {
                //$result =$this->users_model->column('level_5,agent_code')->where('id',$id)->find_all();
                $data ['sellerpoints'] = $this->buisness_type_model->get_sellerpoints();
                $data ['user_seller_point'] = $this->userwise_seller_point_model->where('user_id', $user_id)->where('status', 'Y')->find_all();
                $data['current_cb_executive_edit'] = TRUE;
            }

            // added by vijay
            $data['creat_role'] = RETAILER_ROLE_ID;
            $data["serviceDetails"] = $this->buisness_type_model->get_service();
            $data["agentServices"] = $this->buisness_type_model->get_agent_services($user_id);
            load_back_view(EDIT_FRONTEND_USERS, $data);
        }
    }
    
    /*
     * @function : delete_user
     * @param :
     * @detail : soft delete user.
     *
     */

    public function delete_user() {
        $input = $this->input->post(NULL, TRUE);
        $this->users_model->id = $input ["id"];
        $rdata = $this->users_model->select();

        if (!empty($input ["id"])) {
            $this->users_model->id = $input ["id"];
            $this->users_model->is_deleted = "Y";
            $id = $this->users_model->save();
        } else {
            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'Please contact to customer support');
            $data ["flag"] = '@#error#@';
        }
        if ($id > 0) {
            $data ["flag"] = '@#success#@';
            $this->session->set_flashdata('msg_type', 'success');
            $this->session->set_flashdata('msg', 'User Deleted Successfully.');
        } else {
            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'You are not authorized to view or edit other users.');
            $data ["flag"] = '@#error#@';
        }
        data2json($data);
    }

    public function get_division(){
        $input = $this->input->post();
        $depot_cd = $input["depot_cd"];
        $division_data = $this->Sales_agent_mapping_model->get_division_data($depot_cd);
        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($division_data));
    }

    public function check_smartcard_id(){
        $smartcard_id = $this->Sales_agent_mapping_model->get_smartcard_id();
        return $this->output
                    ->set_content_type('application/json')
                    ->set_status_header(200)
                    ->set_output(json_encode($smartcard_id));
    }

}

?>