<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Password_reset extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('users_model', 'common_model', 'support_reset_password');
    }
    
    public function index()
	{
		// show(BACK_VIEW.'admin/support_password_reset/change_view', 1);
		load_back_view(BACK_VIEW.'admin/support_password_reset/change_view');
    }

    public function get_user_details(){
    	$input = $this->input->post();
    	$mobile = $input['mobile'];
    	$mobile = rokad_encrypt($mobile, $this->config->item('pos_encryption_key'));
    	
    	$details = $this->users_model->where('mobile_no', $mobile)->find_all();
    	$data = array();

    	if($details){
    		foreach ($details as $key => $value) {
    			$data[$key]['id'] = $value->id;
    			$data[$key]['role_id'] = $value->role_id;
	    		$data[$key]['username'] =  rokad_decrypt($value->username, $this->config->item('pos_encryption_key'));
	    		$data[$key]['email'] =  rokad_decrypt($value->email, $this->config->item('pos_encryption_key'));
	    		$data[$key]['display_name'] =  $value->display_name;
	    		$data[$key]['depot_name'] =  $value->depot_name;
	    		$data[$key]['depot_cd'] =  $value->depot_cd;
	    		$data[$key]['agent_code'] =  $value->agent_code;
	    	}
    	}

    	echo json_encode($data);

    }


    public function change_password(){
    	$input = $this->input->post();
    	$user_id = $input['user_id'];
    	$reason = $input['reason'];

    	$user_detail = $this->users_model->where('id', $user_id)->find_all();
    	$user_detail = $user_detail[0];

    	$data = $support_data = array();
    	$data['salt'] = 'BaxnHljj';
    	$data['password'] = '4fc8c4180966e198bf137196cd2ed514';


    	/* Enter support_details who changed the password */
    	$logged_in_user = $this->session->userdata('id');
    	$support_data['user_id'] = $user_id;
    	$support_data['support_id'] = $logged_in_user;
    	$support_data['old_salt'] = $user_detail->salt;
    	$support_data['old_password'] = $user_detail->password;
    	$support_data['new_salt'] = $data['salt'];
    	$support_data['new_password'] = $data['password'];
    	$support_data['reason'] = $reason;

    	$this->common_model->insert('support_reset_password', $support_data);

    	$Updateid = $this->common_model->update('users','id',$user_id, $data);
    	


		if ($Updateid > 0) {
			echo json_encode(['type' => 'success'] );
		}else{
			echo json_encode(['type' => 'failed'] );
		}
    }

}