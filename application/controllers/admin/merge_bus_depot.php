<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Merge_bus_depot extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		is_logged_in();
		$this->load->model(array('merge_bus_depot_model','common_model'));
        $this->load->library('form_validation');
		//echo "hi";exit;
		
	}
    
    public function index()
    {	   
        $data = array();
        // $data['bus_stop_info'] =  $this->merge_bus_depot_model->getAllBusStops($input['search_text']);
        // $data['bus_depot_info'] =  $this->merge_bus_depot_model->getAllBusDepots($input['search_text']);
        load_back_view(MERGE_BD_VIEW);             
    }
    
    public function get_bus_stop(){
        $input = $this->input->get(NULL,TRUE);
        
        $data['bus_stop_info'] =  $this->merge_bus_depot_model->getAllBusStops($input['search_text']);
        
        echo json_encode($data);
    }

    public function get_bus_depots(){
        $input = $this->input->get(NULL,TRUE);
        
        $data['bus_depot_info'] =  $this->merge_bus_depot_model->getAllBusDepots($input['search_text']);

        echo json_encode($data);
    }

    public function edit_merge_bus_depots($bus_stop_cd)
    {
        $data['bus_stop_info'] =  $this->merge_bus_depot_model->getBusStopInfo($bus_stop_cd);

        load_back_view(MERGE_BD_EDIT, $data);

    }

    public function save_bus_stop(){
        $input = $this->input->post(NULL,TRUE);

        $input['UPDATED_DT'] = date('Y-m-d');
        $input['UPDATED_BY'] = $this->session->userdata('user_id');

        $update = $this->merge_bus_depot_model->update_data($input);
        
    }

    public function getDataTableData(){
        // Datatables Variables
          $draw = intval($this->input->get("draw"));
          $start = intval($this->input->get("start"));
          $length = intval($this->input->get("length"));


         $bus_stop_info = $this->merge_bus_depot_model->get_all_data();

          $data = array();
          foreach ($bus_stop_info as $info) {
               $data[] = array(
                    $info->BUS_STOP_CD,
                    $info->BUS_STOP_NM,
                    $info->DEPOT_CD,
                    "<a href='" .
                        'merge_bus_depot/edit_merge_bus_depots/' .  $info->BUS_STOP_CD
                     . "'>Edit</a>                    
                     "      

                     // &nbsp;&nbsp;
                     // <a href='' class='delete' data-delete-id='" . $info->BUS_STOP_CD . "'>Delete</a>          
               );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => count($bus_stop_info),
                 "recordsFiltered" => count($bus_stop_info),
                 "data" => $data
            );
          echo json_encode($output);
          exit();       
        
    }


    public function add_bus_stop(){
        $input = $this->input->post(NULL,TRUE);
        $data = $this->merge_bus_depot_model->addBusStop($input);

        $data['DEPOT_CD'] = $input['bus_depot'];
        $data['INSERTED_BY'] = $this->session->userdata('user_id');
        $data['INSERTED_DT'] = date('Y-m-d');

        $data['UPDATED_DT'] = date('Y-m-d');
        $data['UPDATED_BY'] = $this->session->userdata('user_id');


        unset($data['STOP_TYPE']);

        $insert_data = $this->merge_bus_depot_model->insert_data($data);

        echo 'success';
    }
    
    public function remove_bus_info()
    {   
        $input = $this->input->post();
        $bus_stop_cd = $input['delete_id'];

        $id = $this->merge_bus_depot_model->delete_record($bus_stop_cd);
        
        if ($id > 0) {
          echo 'success';
        }else {
          echo 'failed';
        }
    }


}