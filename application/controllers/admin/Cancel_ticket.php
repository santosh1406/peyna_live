<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cancel_ticket extends CI_Controller {

    public function __construct() {
        parent::__construct();

        ini_set('memory_limit', -1);
        $this->load->library(array('cca_payment_gateway', 'pagination', 'Nusoap_lib', 'api_provider/etravelsmart_api', 'api_provider/hrtc_api', 'soap_api/upsrtc_api', 'soap_api/msrtc_api','soap_api/rsrtc_api', 'Datatables'));
        $this->load->model(array('soap_client_model', 'tickets_model', 'cancel_booking_model', 'agent_model', 'ticket_details_model', 'routes_model', 'bus_stop_master_model', 'op_master_model', 'booking_agent_model', 'report_model', 'api_provider_model', 'city_master_model', 'cancel_ticket_model', 'users_model', 'common_model', 'return_ticket_mapping_model', 'wallet_model', 'wallet_trans_model', 'wallet_trans_log_model','payment_gateway_master_model','payment_gateway_wallet_trans_model','payment_gateway_wallet_model','commission_calculation_model'));
        $this->load->helper('soap_data');


        $this->form_id = "";
        $this->div_id = "";
        $this->payment_by_array = array("wallet","csc");
        
    }

    public function index() {
        $data = array();
        load_back_view(CANCEL_TICKET_VIEW, $data);
    }

    /* CANCEL REFUND TICKET */

    public function cancel_refund_tickets() {
        $data = array();
        $seat_no = array();
        $request = array();
        $request['seat_no'] = array();
        $is_true = true;
        $input = $this->input->post();
        $request['pnr_no'] = $input['pnr_no'];
        $provider_id = $input['provider_id'];
        $provider_type = $input['proty'];
        $op_id = $input['op_id'];
        $ticket_id = $input['ticket_id'];

        $tinfo = $this->tickets_model->get_ticket_and_detail($input['email'], $input['pnr_no']);

        $check_is_return_ticket_exists = $this->return_ticket_mapping_model->as_array()->get_rpt_mapping($tinfo["ticket_id"]);

        $check_is_return_ticket = $this->return_ticket_mapping_model->as_array()->get_parent_ticketid_mapping($tinfo["ticket_id"]);

        if ($tinfo) {
            $to_allow_cancel = true;
            if ($to_allow_cancel) {
                if (in_array($tinfo["payment_confirmation"], array("full", "partial", "auto_confirmed"))) {
                    if ((in_array(strtolower($tinfo["payment_by"]),$this->payment_by_array)) || (time() > strtotime("+60 minutes", strtotime($tinfo["inserted_date"])))) {
                        $partial_cancellation = $tinfo["partial_cancellation"];
                        $ticket_id = $tinfo["ticket_id"];

                        if ($ticket_id != $input['ticket_id']) {
                            $hack_string = "User With user id = " . $tinfo["inserted_by"] . " & email id " . $tinfo["user_email_id"] . " tried to crack the system or chnaged parameter(Ticket Id - " . $input['ticket_id'] . ") while cancelling ticket.";
                            log_data("hack/hack_" . date("d-m-Y") . "_log.log", $hack_string, 'Ticket Cancel');
                        }

                        if (!$partial_cancellation) {
                            //This is partial cancellation false condition.
                            $seat_no = $request['seat_no'] = $tinfo["seat_no"];
                        } else if ($partial_cancellation && !empty($input['seat_no'])) {
                            //This is partial cancellation true condition.
                            $seat_no = $request['seat_no'] = $input['seat_no'];
                        } else {
                            $is_true = false;
                        }
                        if ($is_true) {
                            if ((strtolower($tinfo["payment_by"]) == "wallet") || (strtolower($tinfo["payment_by"]) == "csc")) {
                                $is_true = true;
                            } else {
                                $check_is_return_ticket_refund = $this->return_ticket_mapping_model->check_is_return_ticket_refund($tinfo["ticket_id"]);

                                if ($check_is_return_ticket_refund) {
                                    if ($check_is_return_ticket_refund[0]->onward_transaction_status == "success" && $check_is_return_ticket_refund[0]->return_transaction_status == "success") {
                                        $is_true = true;
                                    } else {
                                        $is_true = false;
                                    }
                                }
                            }

                            if ($is_true) {
                                if (isset($input['pnr_no']) && $input['pnr_no'] != "" && isset($provider_id) && $provider_id && isset($input['email']) && $input['email'] != "") {
                                    $ldetails = $this->common_model->get_provider_lib($provider_id, $provider_type, $op_id);
                                    $library_name = strtolower(trim($ldetails['library'])) . '_api';

                                    //for Etravelsmart api we need api_ticket_no
                                    $request["api_ticket_no"] = $tinfo["api_ticket_no"];
                                    $request["current_ticket_information"] = $tinfo;

                                    //in case of etravelsmart no api has to be hit
                                    if ($library_name == "etravelsmart_api") {
                                        $cancel_data['status'] = "success";
                                        if (($tinfo["payment_by"]) == "wallet") {
                                            $cancel_data['refundAmount'] = $tinfo["tot_fare_amt_with_tax"];
                                        } 
                                        else if(strtolower(trim($tinfo['payment_by'])) != 'csc') {
                                            $cancel_data['refundAmount'] = $tinfo["tot_fare_amt_with_tax"];
                                        }
                                        else {
                                            if ($check_is_return_ticket_exists || $check_is_return_ticket) {
                                                $cancel_data['refundAmount'] = $tinfo["tot_fare_amt_with_tax"];
                                            } else {
                                                $cancel_data['refundAmount'] = $tinfo["merchant_amount"];
                                            }
                                        }
                                        $cancel_data['cancellationCharge'] = '0';
                                    } else {
                                        $cancel_data = $this->{$library_name}->cancel_ticket($request);
                                    }
                                    /*                                     * **** etravelsmart changes ends here ****** */
                                    $refund_amt = $tinfo["tot_fare_amt"];
                                    if ($cancel_data['newPNR'] != "")
                                        $new_pnr = $cancel_data['newPNR'];
                                    else
                                        $new_pnr = "";

                                    if (strtolower($cancel_data['status']) == 'success') {
                                        $ticket_update_array = array("transaction_status" => "cancel",
                                            "status" => 'N'
                                        );

                                        $ticket_update_where = array('pnr_no' => $input['pnr_no'], "user_email_id" => $input['email'], "ticket_id" => $ticket_id);
                                        $is_ticket_updated = $this->tickets_model->update_where($ticket_update_where, "", $ticket_update_array);

                                        if ($is_ticket_updated) {
                                            /*                  NOTICE
                                             * After Discussion Handle PARTIAL CANCELLATION HERE
                                             * i.e. Handle seatwise cancellation
                                             */

                                            if ($partial_cancellation) {
                                                // $input['ticket_id']
                                                // $get_seat = $this->tickets_model->get_seat_nos_by_tid($ticket_id);
                                                // $is_ticket_detail_updated = $this->ticket_details_model->where_in("seat_no","")->update_where(array("ticket_id" => $ticket_id),"",array("status" => "N"));
                                            } else {
                                                $is_ticket_detail_updated = $this->ticket_details_model->update_where(array("ticket_id" => $ticket_id), "", array("status" => "N"));
                                            }

                                            $this->cancel_ticket_model->ticket_id = $ticket_id;
                                            $this->cancel_ticket_model->pnr_no = $input['pnr_no'];
                                            $this->cancel_ticket_model->new_pnr = $new_pnr;
                                            $this->cancel_ticket_model->seat_no = $seat_no;
                                            $this->cancel_ticket_model->refund_amt = $refund_amt;
                                            $this->cancel_ticket_model->cancel_charge = $cancel_data["cancellationCharge"];
                                            $this->cancel_ticket_model->cancel_percentage = isset($cancel_data["cancellationChargePer"]) ? str_replace("%", "", $cancel_data["cancellationChargePer"]) : "";
                                            $this->cancel_ticket_model->raw = json_encode($cancel_data);
                                            $id = $this->cancel_ticket_model->save();

                                            if ($id) {
                                                if ($tinfo['payment_by'] != 'wallet' && (strtolower(trim($tinfo['payment_by'])) != 'csc')) {
                                                    $paymentgateway_ref_no = "";
                                                    $paymentgateway_ref_no = $tinfo["pg_tracking_id"];

                                                    if ($check_is_return_ticket_exists || $check_is_return_ticket) {
                                                        $acual_refund_paid = $tinfo["tot_fare_amt_with_tax"];
                                                    } else {
                                                        $acual_refund_paid = $tinfo["merchant_amount"];
                                                    }
                                                    $merchant_array_data = array(
                                                        "reference_no" => $paymentgateway_ref_no,
                                                        "refund_amount" => $acual_refund_paid,
                                                        "refund_ref_no" => uniqid()
                                                    );

                                                    $refund_status = $this->cca_payment_gateway->refundOrder($merchant_array_data);

                                                    $cancel_data_ref_paid = $tinfo['tot_fare_amt_with_tax'];
                                                    if ($refund_status->reason == "" && !$refund_status->refund_status) {
                                                        $update_refund_status = array('is_refund' => "1",
                                                            "actual_refund_paid" => $cancel_data_ref_paid,
                                                            "pg_refund_amount" => $acual_refund_paid
                                                        );

                                                        $update_where_cancel = array("id" => $id);
                                                        $refund_id = $this->cancel_ticket_model->update($update_where_cancel, $update_refund_status);
                                                        if (!$refund_id) {
                                                            log_data("refund/success_but_not_updated_ccavenue_refund_" . date("d-m-Y") . "_log.log", "Refund is success but status is not updated for 'cancel_ticket_data table' for id " . $id, 'cancel_ticket_data table id');
                                                        }

                                                        $cancel_detail = array("{{bos_ref_no}}" => $tinfo["boss_ref_no"],
                                                            "{{amount_to_pay}}" => (isset($acual_refund_paid)) ? $acual_refund_paid : "NA",
                                                        );
                                                        $temp_sms_msg = str_replace(array_keys($cancel_detail), array_values($cancel_detail), BOS_CANCEL_TICKET);
                                                        log_data("trash/temp_sms_msg_" . date("d-m-Y") . "_log.log", $temp_sms_msg, 'cancel temp_sms_msg');
                                                        $is_send = sendSMS($tinfo["mobile_no"], $temp_sms_msg, array("ticket_id" => $ticket_id));

                                                        //TEMP success flag. WHEN SMS ACTIVE REMOVE THIS LINE
                                                        $data['flag'] = '@#success#@';

                                                        //KEEP THIS INSIDE SUCCESS FLAG
                                                        $data['refund_amt'] = $acual_refund_paid;
                                                        $data['cancellation_cahrge'] = $cancel_data["cancellationCharge"];

                                                        // cancelled_ticket
                                                        /*                                                         * ********** Mail Start Here ****** */
                                                        $mail_template_title = (isset($op_config_detail->mail_template_title) && ($op_config_detail->mail_template_title != "") ) ? $op_config_detail->mail_template_title : "Upsrtc Ticket Template";

                                                        $mail_replacement_array = array(
                                                            // "{{username}}" => ucwords($tinfo["psgr_name"]),
                                                            "{{username}}" => "User",
                                                            "{{from_stop}}" => ucwords($tinfo["from_stop_name"]),
                                                            "{{to_stop}}" => ucwords($tinfo["till_stop_name"]),
                                                            "{{bos_ref_no}}" => $tinfo["boss_ref_no"],
                                                            "{{amount_to_pay}}" => $acual_refund_paid,
                                                            "{{pnr_no}}" => $tinfo["pnr_no"],
                                                        );

                                                        $mailbody_array = array(
                                                            "mail_title" => "ticket_refund",
                                                            "mail_client" => $tinfo["user_email_id"],
                                                            "mail_replacement_array" => $mail_replacement_array
                                                        );

                                                        $mail_result = setup_mail($mailbody_array);

                                                        /*                                                         * ********** Mail End Here ******* */

                                                        if ($is_send) {
                                                            $data['flag'] = '@#success#@';
                                                        } else {
                                                            $data['flag'] = '@#failed#@';
                                                            $data['error'] = "Error while sending message.";
                                                        }
                                                    } else {
                                                        $data['flag'] = '@#failed#@';
                                                        $data['error'] = "Error in processing request. Please contact customer care.";
                                                    }
                                                } 
                                                else if ($tinfo['payment_by'] == 'wallet') {
                                                    $acual_refund_paid = getRefundAmountToPay($tinfo["tot_fare_amt_with_tax"], $cancel_data["cancellationCharge"]);
                                                    /* ------ Process for cancel ticket when pay from wallet ------- */

                                                    // get wallet Detail
                                                    $wallet_id = $this->wallet_model->get_walletid($ticket_id);
                                                    $wid = $wallet_id[0]['w_id'];
                                                    $user_id = $wallet_id[0]['user_id'];
                                                    
                                                    if($user_id > 0) {

                                                        $this->wallet_model->user_id = $user_id;
                                                        $walletdata = $this->wallet_model->check_wallet_rec($user_id);

                                                        $current_balance = $walletdata[0]['amt'];
                                                        if ($acual_refund_paid > 0) {
                                                            $balance_to_update = $current_balance + $acual_refund_paid;
                                                        }
                                                        $is_refund = '0';
                                                        if($balance_to_update < WALLET_LIMIT) {

                                                            $UserWalletId = $walletdata[0]['id'];
                                                            $wallet_to_update = array(
                                                                'amt' => $balance_to_update,
                                                                'updated_by' => $this->session->userdata('user_id'),
                                                                'updated_date' => date('Y-m-d h:m:s')
                                                            );

                                                            $is_cancle_wallet_updated = $this->wallet_model->update($UserWalletId, $wallet_to_update);

                                                            if($is_cancle_wallet_updated) {
                                                                // Make excel file
                                                                $title = array('User Id', 'Amount', 'Amount Befor', 'Amount After', 'Topup Id', 'Ticket Id', 'Date', 'Detail');
                                                                $val = array($user_id, $acual_refund_paid, $current_balance, $balance_to_update, '', $ticket_id, date("Y-m-d H:i:s"), 'Function Detail : cancel_booking/cancel_booking, Detail :Add  Amount into wallet against ticket cancel process');
                                                                $FileName = "wallet/user_wallet_detail/wallet_" . $user_id . ".csv";
                                                                make_csv($FileName, $title, $val);
                                                                $transaction_no = substr(hexdec(uniqid()), 4, 12);
                                                                $wallet_trans_detail = array("w_id" => $UserWalletId,
                                                                                            "amt" => $acual_refund_paid,
                                                                                            "comment" => "ticket cancel Process(service cancel)",
                                                                                            "status" => "added",
                                                                                            "user_id" => $user_id,
                                                                                            "added_by" => $user_id,
                                                                                            "added_on" => date("Y-m-d H:i:s"),
                                                                                            "amt_before_trans" => $current_balance,
                                                                                            "amt_after_trans" => $balance_to_update,
                                                                                            "ticket_id" => $ticket_id,
                                                                                            "transaction_type_id" => '4',
                                                                                            "wallet_type" => 'actual_wallet',
                                                                                            "transaction_type" => 'Credited',
                                                                                            "is_status" => 'Y',
                                                                                            "transaction_no" => $transaction_no
                                                                                        );

                                                                $wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail);

                                                                if($wallet_trans_id) {
                                                                    $this->deduct_commission_for_ticket($tinfo);
                                                                }
                                                                $is_refund = '1';
                                                            }
                                                            else {
                                                                $custom_error_mail = array(
                                                                                        "custom_error_subject" => "Wallet Error - Wallet amount not added while processing cancel booking ",
                                                                                        "custom_error_message" => "Function Name - cancel ticket. Wallet amount not added while processing cancel booking. User id - " . $user_id . " and Ticket id - " . $ticket_id
                                                                                        );

                                                                $this->common_model->developer_custom_error_mail($custom_error_mail);
                                                                $err_data['error'] = " Wallet amount not added while processing cancel booking";
                                                                log_data("booking/" . date("M") . "/bos_cancel_booking_from_wallet.log", $err_data, "Wallet not updated");

                                                                $data['flag'] = '@#failed#@';
                                                                $data['error'] = "Error in processing request. Please contact customer care.";
                                                            }

                                                        }
                                                            
                                                            
                                                        $cancel_data_ref_paid = $tinfo['tot_fare_amt_with_tax'];
                                                        $update_refund_status = array('is_refund' => $is_refund,
                                                            "actual_refund_paid" => $cancel_data_ref_paid,
                                                            "pg_refund_amount" => $acual_refund_paid
                                                        ); 

                                                        $update_where_cancel = array("id" => $id);

                                                        $refund_id = $this->cancel_ticket_model->update($update_where_cancel, $update_refund_status);

                                                        if (!$refund_id) {
                                                            log_data("refund/success_but_not_updated_ccavenue_refund_" . date("d-m-Y") . "_log.log", "Refund is success but status is not updated for 'cancel_ticket_data table' for id " . $id, 'cancel_ticket_data table id');
                                                        }

                                                        $cancel_detail = array("{{bos_ref_no}}" => $tinfo["boss_ref_no"],
                                                            "{{amount_to_pay}}" => (isset($acual_refund_paid)) ? $acual_refund_paid : "NA",
                                                        );

                                                        $temp_sms_msg = str_replace(array_keys($cancel_detail), array_values($cancel_detail), BOS_CANCEL_TICKET);
                                                        log_data("trash/temp_sms_msg_" . date("d-m-Y") . "_log.log", $temp_sms_msg, 'cancel temp_sms_msg');
                                                        $is_send = sendSMS($tinfo["mobile_no"], $temp_sms_msg, array("ticket_id" => $ticket_id));

                                                        //KEEP THIS INSIDE SUCCESS FLAG
                                                        $data['refund_amt'] = $acual_refund_paid;
                                                        $data['cancellation_cahrge'] = $cancel_data["cancellationCharge"];
                                                        // cancelled_ticket

                                                        /*                                                         * ********** Mail Start Here ****** */
                                                        $mail_template_title = (isset($op_config_detail->mail_template_title) && ($op_config_detail->mail_template_title != "") ) ? $op_config_detail->mail_template_title : "Upsrtc Ticket Template";
                                                    
                                                        $mail_replacement_array = array(
                                                            // "{{username}}" => ucwords($tinfo["psgr_name"]),
                                                            "{{username}}" => "User",
                                                            "{{from_stop}}" => ucwords($tinfo["from_stop_name"]),
                                                            "{{to_stop}}" => ucwords($tinfo["till_stop_name"]),
                                                            "{{bos_ref_no}}" => $tinfo["boss_ref_no"],
                                                            "{{amount_to_pay}}" => $acual_refund_paid,
                                                            "{{pnr_no}}" => $tinfo["pnr_no"],
                                                        );

                                                        $mailbody_array = array(
                                                            "mail_title" => "ticket_refund",
                                                            "mail_client" => $tinfo["user_email_id"],
                                                            "mail_replacement_array" => $mail_replacement_array
                                                        );


                                                        $mail_result = setup_mail($mailbody_array);
                                                        if ($mail_result !== "") {
                                                            $data['flag'] = '@#success#@';
                                                        } else {
                                                            $data['flag'] = '@#failed#@';
                                                            $data['error'] = "Error while sending mail.";
                                                        }

                                                        if ($is_send) {
                                                        $data['flag'] = '@#success#@';
                                                        } else {
                                                            $data['flag'] = '@#failed#@';
                                                            $data['error'] = "Error while sending message.";
                                                        }
                                                    }
                                                    else {
                                                        $custom_error_mail = array(
                                                            "custom_error_subject" => "Wallet Error - Wallet amount not added while processing cancel booking ",
                                                            "custom_error_message" => "Function Name - cancel ticket. Wallet amount not added while processing cancel booking. User id - " . $user_id . " and Ticket id - " . $ticket_id
                                                            );

                                                        $this->common_model->developer_custom_error_mail($custom_error_mail);
                                                        $err_data['error'] = " Wallet amount not added while processing cancel booking";
                                                        log_data("booking/" . date("M") . "/bos_cancel_booking_from_wallet.log", $err_data, "Wallet not updated");

                                                        $data['flag'] = '@#failed#@';
                                                        $data['error'] = "Error in processing request. Please contact customer care.";

                                                    }
                                                } 
                                                
                                            } else {
                                                $data['flag'] = '@#failed#@';
                                                $data['error'] = "Something went wrong please try again later.";
                                            }
                                        } else {
                                            $data['flag'] = '@#failed#@';
                                            $data['error'] = "Something went wrong please try again later.";
                                        }
                                    } else if (strtolower($cancel_data['status']) == 'error') {
                                        // $error_message = (isset($cancel_data["error_message"])) ? $cancel_data["error_message"] : (isset($cancel_data['serviceError']['errorReason'])) ? $cancel_data['serviceError']['errorReason'] : "Something unexpected happened. Please Try again.";
                                        $error_message = (isset($cancel_data["error_message"])) ? $cancel_data["error_message"] : "Something unexpected happened. Please Try again.";
                                        $data['flag'] = '@#failed#@';
                                        $data['error'] = $error_message;
                                    } else {
                                        $data['flag'] = '@#failed#@';
                                        $data['error'] = 'Please Try again after sometime';
                                    }
                                } else {
                                    $data['flag'] = '@#failed#@';
                                    $data['error'] = 'Ticket Error';
                                }
                            } else {
                                $data['flag'] = '@#failed#@';
                                $data['error'] = 'In case of Round trip, only one trip can be cancelled (Onward Or Return). You have already cancelled one of them therefore you cannot cancel the other trip.';
                            }
                        } else {
                            $data['flag'] = '@#failed#@';
                            $data['error'] = "Please Select Seat.";
                        }
                    } else {
                        $data['flag'] = '@#failed#@';
                        $data['error'] = "Could not initiate refund till 1 hour after booking.";
                    }
                } else {
                    $data['flag'] = '@#failed#@';
                    $data['error'] = "Your payment is not confirmed yet, Please try again after sometime.";
                }
            } else {
                $data['flag'] = '@#failed#@';
                $data['error'] = "You are not allowed to cancel ticket. Please contact customer support.";
            }
        } else {
            $data['flag'] = '@#failed#@';
            $data['error'] = "Please contact customer support.";
        }
        data2json($data);
    }

    /* check cancellable status ******** */

    function is_ticket_cancellable() {
        $data = array();
        $seat_no = array();
        $is_true = true;
        $input = $this->input->post();

        $request = array();
        $request['pnr_no'] = $input['pnr_no'];
        $provider_id = $input['pro_id'];
        $provider_type = $input['proty'];
        $op_id = $input['opi'];
        $ticket_id = $input['ticket_id'];

        $request['seat_no'] = array();


        $tinfo = $this->tickets_model->get_ticket_and_detail($input['email'], $input['pnr_no']);

        $check_is_return_ticket_exists = $this->return_ticket_mapping_model->as_array()->get_rpt_mapping($tinfo["ticket_id"]);

        $check_is_return_ticket = $this->return_ticket_mapping_model->as_array()->get_parent_ticketid_mapping($tinfo["ticket_id"]);

        if ($tinfo) {
            $to_allow_cancel = true;
            /*  if($tinfo["role_id"] ==  && $tinfo["booked_by"] == )
              {
              if(!$this->session->userdata('role_id') && $this->session->userdata('role_id') != 16 && !$this->session->userdata('role_name') && $this->session->userdata('role_name') !=)
              {
              $to_allow_cancel = false;
              }
              } */

            if ($to_allow_cancel) {
                if (in_array($tinfo["payment_confirmation"], array("full", "partial", "auto_confirmed"))) {

                    if ((in_array(strtolower($tinfo["payment_by"]),$this->payment_by_array)) || (time() > strtotime("+60 minutes", strtotime($tinfo["inserted_date"])))) {
                        $partial_cancellation = $tinfo["partial_cancellation"];
                        $ticket_id = $tinfo["ticket_id"];

                        if ($ticket_id != $input['ticket_id']) {
                            $hack_string = "User With user id = " . $tinfo["inserted_by"] . " & email id " . $tinfo["user_email_id"] . " tried to crack the system or chnaged parameter(Ticket Id - " . $input['ticket_id'] . ") while cancelling ticket.";
                            log_data("hack/hack_" . date("d-m-Y") . "_log.log", $hack_string, 'Ticket Cancel');
                        }

                        if (!$partial_cancellation) {
//This is partial cancellation false condition.

                            $seat_no = $request['seat_no'] = $tinfo["seat_no"];
                        } else if ($partial_cancellation && !empty($input['seat_no'])) {
//This is partial cancellation true condition.
                            $seat_no = $request['seat_no'] = $input['seat_no'];
                        } else {
                            $is_true = false;
                        }


                        if ($is_true) {
                            if ((strtolower($tinfo["payment_by"]) == "wallet")) {
                                $is_true = true;
                            } else {
                                $check_is_return_ticket_refund = $this->return_ticket_mapping_model->check_is_return_ticket_refund($tinfo["ticket_id"]);

                                if ($check_is_return_ticket_refund) {
                                    if ($check_is_return_ticket_refund[0]->onward_transaction_status == "success" && $check_is_return_ticket_refund[0]->return_transaction_status == "success") {
                                        $is_true = true;
                                    } else {
                                        $is_true = false;
                                    }
                                }
                            }

                            if ($is_true) {
                                if (isset($input['pnr_no']) && $input['pnr_no'] != "" && isset($provider_id) && $provider_id && isset($input['email']) && $input['email'] != "") {
                                    $ldetails = $this->common_model->get_provider_lib($provider_id, $provider_type, $op_id);
                                    $library_name = strtolower(trim($ldetails['library'])) . '_api';

                                    //for Etravelsmart api we need api_ticket_no
                                    $request["api_ticket_no"] = $tinfo["api_ticket_no"];
                                    $request["pnr_no"] = $input['pnr_no'];
                                    $request["current_ticket_information"] = $tinfo;


                                    //in case of etravelsmart no api has to be hit
                                    if ($library_name == "etravelsmart_api") {

                                        $cancel_data['status'] = "success";
                                        if (($tinfo["payment_by"]) == "wallet") {
                                            $cancel_data['refundAmount'] = $tinfo["tot_fare_amt_with_tax"];
                                        } 
                                        else if(strtolower($tinfo["payment_by"]) == "csc") {
                                            $cancel_data['refundAmount'] = $tinfo["tot_fare_amt_with_tax"];
                                        }
                                        else {
                                            if ($check_is_return_ticket_exists || $check_is_return_ticket) {
                                                $cancel_data['refundAmount'] = $tinfo["tot_fare_amt_with_tax"];
                                            } else {
                                                $cancel_data['refundAmount'] = $tinfo["merchant_amount"];
                                            }
                                        }

                                        $cancel_data['cancellationCharge'] = '0';
                                    } else {
                                        $cancel_data = $this->{$library_name}->is_ticket_cancellable($request);
                                    }
                                    /*                                     * ** etravelsmart changes ends *** */

                                    if (strtolower($cancel_data['status']) == 'success') {

                                        /* if($cancel_data["cancellationCharge"]=="0" && $tinfo['payment_by'] != 'wallet')
                                          { */
                                        if ($check_is_return_ticket_exists || $check_is_return_ticket) {
                                            $acual_refund_paid = $tinfo["tot_fare_amt_with_tax"];
                                        } else {
                                            if ($tinfo["payment_by"] == "wallet") {
                                                $acual_refund_paid = isset($tinfo["tot_fare_amt_with_tax"]) ? $tinfo["tot_fare_amt_with_tax"] : 0;
                                            } else {
                                                $acual_refund_paid = isset($tinfo["merchant_amount"]) ? $tinfo["merchant_amount"] : 0;
                                            }
                                        }

                                        /* 	}
                                          else
                                          {
                                          $acual_refund_paid = getRefundAmountToPay($tinfo["tot_fare_amt_with_tax"],$cancel_data["cancellationCharge"]);
                                          }
                                         */
                                        if ($acual_refund_paid > 0) {
                                            $data['flag'] = '@#success#@';
                                            $data['message'] = 'Ticket is cancellable. Please confirm cancellation.';
                                            $data['refund_amt'] = $acual_refund_paid;
                                            $data['cancellation_cahrge'] = $cancel_data["cancellationCharge"];
                                        } else {
                                            $data['flag'] = '@#failed#@';
                                            $data['error'] = 'Ticket is non-cancellable.';
                                        }
                                    } else if (strtolower($cancel_data['status']) == 'error') {
                                        $error_message = (isset($cancel_data["error_message"])) ? $cancel_data["error_message"] : "Something unexpected happened. Please Try again.";
                                        $data['flag'] = '@#failed#@';
                                        $data['error'] = $error_message;
                                    } else {
                                        $data['flag'] = '@#failed#@';
                                        $data['error'] = 'Please Try again after sometime';
                                    }
                                } else {
                                    $data['flag'] = '@#failed#@';
                                    $data['error'] = 'Please provide valid ticket information.';
                                }
                            } else {
                                $data['flag'] = '@#failed#@';
                                $data['error'] = 'In case of Round trip, only one trip can be cancelled (Onward Or Return). You have already cancelled one of them therefore you cannot cancel the other trip.';
                            }
                        } else {
                            $data['flag'] = '@#failed#@';
                            $data['error'] = "Please Select Seat.";
                        }
                    } else {
                        $data['flag'] = '@#failed#@';
                        $data['error'] = "Could not initiate refund till 1 hour after booking.";
                    }
                } else {
                    $data['flag'] = '@#failed#@';
                    $data['error'] = "Your payment is not confirmed yet, Please try again after sometime.";
                }
            } else {
                $data['flag'] = '@#failed#@';
                $data['error'] = "You are not allowed to cancel ticket. Please contact customer support.";
            }
        } else {
            $data['flag'] = '@#failed#@';
            $data['error'] = "Please contact customer support.";
        }

        data2json($data);
    }

    /** list of cancelled tickets *** */
    function bos_tkt_details() {
        $this->datatables->select('1,
t.ticket_id,
t.`transaction_status` AS "Transaction Status",
t.`boss_ref_no` AS "Boss Reference No",
t.pnr_no As "PNR NO",
t.ticket_ref_no AS "Ticket Ref No",

ap.name as "Povider Name",
t.`op_name` AS "Operator Name",

t.from_stop_name as "From Stop",                      

t.till_stop_name as "To Stop",


t.boarding_stop_name as "Boarding Stop",


t.`inserted_date` as "issued date",
dept_time as doj,
ctd.created_at as "Cancellation Date",
t.num_passgr as "No of passenger",

IFNULL(t.`tot_fare_amt`,0) AS "Basic Fare",
IFNULL(sum(td.service_tax),0) AS "Service Tax",
IFNULL(t.`total_fare_without_discount`,0) AS "Total Fare Without Discount",   


CASE t.`booked_from`    WHEN "upsrtc" THEN
IF(t.`total_fare_without_discount` IS NOT NULL,(t.`total_fare_without_discount` - t.`tot_fare_amt`),0) ELSE 0 END AS "UPSRTC Additional Charges (3% or Rs.50)",
IFNULL(t.`discount_on`,"-") AS "Discount On",
IFNULL(t.`discount_type`,"-") AS  "Discount Type",
t.`discount_per` AS "Discount Percentage",
t.`discount_value` AS "Discount Price",
t.`tot_fare_amt_with_tax` AS "Total Fare Paid",

IFNULL(ctd.`pg_refund_amount`,0) AS "Refund Amount",
IF(ctd.is_refund=1,(ctd.`cancel_charge`),0) AS "Cancel Charge",
t.pg_tracking_id AS "PG Track Id",if(t.`transaction_status` = "cancel",count(t.ticket_id),0) cancel_ticket,
if(t.`transaction_status` = "cancel",count(t.num_passgr),0) cancel_psgre
');
        $this->datatables->from('tickets t');

//$this->datatables->join('users u', 'u.id = t.inserted_by', 'inner');
        $this->datatables->join('ticket_details td', 'td.ticket_id = t.ticket_id', 'inner');
//$this->datatables->join('payment_transaction_fss ptf', 'ptf.ticket_id=t.ticket_id', 'left');
        $this->datatables->join('api_provider ap', 't.provider_id = ap.id', 'left');
        $this->datatables->join('cancel_ticket_data ctd', 'ctd.ticket_id = t.ticket_id', 'left');

        if ($this->input->post('provider_id')) {
            $this->datatables->where("t.provider_id", $this->input->post('provider_id'));
        }

        if ($this->input->post('from_date')) {
            $this->datatables->where("DATE_FORMAT(ctd.created_at, '%Y-%m-%d')>=", date('Y-m-d', strtotime($this->input->post('from_date'))));
        }

        if ($this->input->post('till_date')) {
            $this->datatables->where("DATE_FORMAT(ctd.created_at, '%Y-%m-%d')<=", date('Y-m-d', strtotime($this->input->post('till_date'))));
        }

        if ($this->input->post('tran_type') && $this->input->post('tran_type') != 'select_status') {
            $status = $this->input->post('tran_type');
            if ($status == 'failed') {
                $status = array('psuccess', 'pfailed', 'failed');
            }
            $this->datatables->where_in('t.transaction_status', $status);
        }

        if ($this->input->post('pnr_no')) {
            $this->datatables->where('t.pnr_no', $this->input->post('pnr_no'));
        }

        $this->datatables->group_by('t.ticket_id');
        $this->db->order_by("ctd.created_at", "desc");
        $data = $this->datatables->generate('json');
        echo $data;
    }

    private function deduct_commission_for_ticket($ticket_detail) {
        $retailer_commission = $this->commission_calculation_model->retailer_commission_calculation_deduction($ticket_detail['ticket_id'],$ticket_detail['inserted_by']);
        if(is_array($retailer_commission) && $retailer_commission[0]->total_commission > 0 ) {
            $this->db->trans_begin();

            $commission_amount = $retailer_commission[0]->total_commission;
            /******* Add Amount in to user wallet *******/ 
            // Get user wallet data 
            $user_wallet_data = $this->wallet_model->get_wallet_detail($ticket_detail['inserted_by']);
            if(!empty($user_wallet_data)) {
                $user_actual_wallet_amount = $user_wallet_data[0]->amt;
                $user_updated_wallet_amount = $user_actual_wallet_amount - $commission_amount;
                $transaction_no = substr(hexdec(uniqid()), 4, 12);
                // insert into wallet trans for user
                $wallet_trans_arr = array(
                                    'w_id'                => $user_wallet_data[0]->id,
                                    'amt'                 => $commission_amount,
                                    'user_id'             => $ticket_detail['inserted_by'],
                                    'amt_before_trans'    => $user_actual_wallet_amount,    
                                    'amt_after_trans'     => $user_updated_wallet_amount,
                                    'status'              => 'deducted',
                                    'transaction_type'    => 'Debited',
                                    'comment'             => 'Commission deducted for  ticket cancellation',
                                    'wallet_type'         => 'actual_wallet',
                                    'ticket_id'           => $ticket_detail['ticket_id'],
                                    'is_status'           => 'Y',
                                    'transaction_type_id' => 17,
                                    'added_on'            => date('Y-m-d H:i:s'), 
                                    'added_by'            => $this->session->userdata('user_id'),
                                    'transaction_no'      => $transaction_no
                                );
                $wallet_trans_id = $this->common_model->insert('wallet_trans',$wallet_trans_arr);
                
                if($wallet_trans_id){
                    $data = array(
                                'amt'          => $user_updated_wallet_amount,
                                'updated_by'   => $this->session->userdata('user_id'),
                                'updated_date' => date('Y-m-d H:i:s'),
                                'last_transction_id' => $wallet_trans_id
                            );
                    $UpdateId = $this->common_model->update('wallet','user_id',$ticket_detail['inserted_by'],$data);

                    // Get rokad wallet data
                    if($UpdateId) {
                        /*$RollID = COMPANY_ROLE_ID;
                        $RokadUserData = $this->common_model->get_value('users','id','role_id='.$RollID); 
                        $rokad_wallet_data = $this->wallet_model->get_wallet_detail($RokadUserData->id);    
                        if(!empty($rokad_wallet_data)) {
                            $rokad_actual_wallet_amount = $rokad_wallet_data[0]->amt;
                            $rokad_updated_wallet_amount = $rokad_actual_wallet_amount - $commission_amount;
                            
                            // insert into wallet trans for user
                            $wallet_trans_arr = array(
                                                'w_id'                => $rokad_wallet_data[0]->id,
                                                'amt'                 => $commission_amount,
                                                'user_id'             => $RokadUserData->id,
                                                'amt_before_trans'    => $rokad_actual_wallet_amount,    
                                                'amt_after_trans'     => $rokad_updated_wallet_amount,
                                                'status'              => 'deducted',
                                                'transaction_type'    => 'Debited',
                                                'comment'             => 'Commission for ticket booking',
                                                'is_status'           => 'Y',
                                                'wallet_type'         => 'actual_wallet',
                                                'ticket_id'           => $ticket_detail[0]->ticket_id,
                                                'transaction_type_id' => 16,
                                                'added_on'            => date('Y-m-d H:i:s'), 
                                                'added_by'            => $this->session->userdata('user_id')
                                            );
                            $wallet_trans_id = $this->common_model->insert('wallet_trans',$wallet_trans_arr);
                            
                            if($wallet_trans_id){
                                $data = array(
                                            'amt'          => $rokad_updated_wallet_amount,
                                            'updated_by'   => $this->session->userdata('user_id'),
                                            'updated_date' => date('Y-m-d H:i:s'),
                                            'last_transction_id' => $wallet_trans_id
                                        );
                                $UpdateId = $this->common_model->update('wallet','user_id',$RokadUserData->id,$data);
                                if($UpdateId) {
                                }
                                else {
                                    $custom_error_mail = array(
                                                            "custom_error_subject" => " VVVVVIMP commission for rokad not deducted for ".$ticket_detail[0]->ticket_id,
                                                            "custom_error_message" => "commission for rokad not deducted for ".$ticket_detail[0]->ticket_id." Reason : Commission Amount not update into wallet table , but inserted into wallet trans table. wallet trans id : ".$wallet_trans_id.", query for search wallet detail :".$this->db->last_query()
                                                          );

                                    $this->common_model->developer_custom_error_mail($custom_error_mail);
                                    
                                    $data['msg'] = "error";
                                }
                            }
                            else {
                                $custom_error_mail = array(
                                                        "custom_error_subject" => " VVVIMP commission for rokad not deducted for ".$ticket_detail[0]->ticket_id,
                                                        "custom_error_message" => "commission for rokad not deducted for ".$ticket_detail[0]->ticket_id." Reason : Not inserted into wallet trans table, query for search wallet detail :".$this->db->last_query()
                                                      );

                                $this->common_model->developer_custom_error_mail($custom_error_mail);
                                
                                $data['msg'] = "error";
                            }
                        }
                        else {
                            $custom_error_mail = array(
                                                        "custom_error_subject" => " VVVIMP commission for rokad not deducted for ".$ticket_detail[0]->ticket_id,
                                                        "custom_error_message" => "commission for rokad not deducted for ".$ticket_detail[0]->ticket_id." Reason : wallet detail not found, query for search wallet detail :".$this->db->last_query()
                                                      );

                            $this->common_model->developer_custom_error_mail($custom_error_mail);
                            
                            $data['msg'] = "error";
                        } */ 

                        // update commission calculation table 
                        $update_arr = array('cancel_flag' => "Y",
                                            'cancel_flag_date' => date('Y-m-d H:i:s')
                                        );

                        $this->commission_calculation_model->update(array("user_id" => $ticket_detail['inserted_by'], "ticket_id" => $ticket_detail['ticket_id']),$update_arr);
                        $data['msg'] = "success";  
                    }
                    else {
                        $custom_error_mail = array(
                                                "custom_error_subject" => " VVVVVIMP commission for retailer not deducted for ".$ticket_detail['ticket_id'],
                                                "custom_error_message" => "commission for retailer not deducted for ".$ticket_detail['ticket_id']." Reason : Commission Amount not update into wallet table , but inserted into wallet trans table. wallet trans id : ".$wallet_trans_id.", query for search wallet detail :".$this->db->last_query()
                                              );

                        $this->common_model->developer_custom_error_mail($custom_error_mail);
                        
                        $data['msg'] = "error";
                    }
                }
                else {
                    $custom_error_mail = array(
                                            "custom_error_subject" => " VVVIMP commission for retailer not deducted for ".$ticket_detail['ticket_id'],
                                            "custom_error_message" => "commission for retailer not deducted for ".$ticket_detail['ticket_id']." Reason : Not inserted into wallet trans table, query for search wallet detail :".$this->db->last_query()
                                          );

                    $this->common_model->developer_custom_error_mail($custom_error_mail);
                    
                    $data['msg'] = "error";
                }
            }
            else {
                $custom_error_mail = array(
                                            "custom_error_subject" => " VVVIMP commission for retailer not deducted for ".$ticket_detail['ticket_id'],
                                            "custom_error_message" => "commission for retailer not deducted for ".$ticket_detail['ticket_id']." Reason : wallet detail not found, query for search wallet detail :".$this->db->last_query()
                                          );

                $this->common_model->developer_custom_error_mail($custom_error_mail);
                
                $data['msg'] = "error";
            }

            if ($this->db->trans_status() === FALSE) {
                show(error_get_last());
                $this->db->trans_rollback();
            }
            else {
                $this->db->trans_commit();
            } 
        }
        else {
            $custom_error_mail = array(
                                        "custom_error_subject" => " VVVIMP commission detail not found while cancel ticket for ".$ticket_detail['ticket_id'],
                                        "custom_error_message" => "commission detail not found for ".$ticket_detail['ticket_id']." Query for search commission calculation detail :".$this->db->last_query()
                                      );

            $this->common_model->developer_custom_error_mail($custom_error_mail);
            
            $data['msg'] = "error";
        }

        return $data;
    }

}

?>
