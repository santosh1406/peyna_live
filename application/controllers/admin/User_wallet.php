<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_wallet extends MY_Controller {

    public function __construct() {
        parent::__construct();
        is_logged_in();
        $this->load->model(array('wallet_model','wallet_trans_model','users_model','booking_agent_model','wallet_topup_model','wallet_trans_log_model','lookup_detail_model'));
        $this->load->library('form_validation');
        $this->load->library("Excel");
        $this->load->library('session');
    }
    /**
     * [index description] : Use for see user wallet detail
     */
    public function index() {
        load_back_view(USER_WALLET);
    }
  
    /**
     * [index description] : Use for see user wallet detail
     */
    public function get_user_wallet() {
        $data = $this->get_user_wallet_detail('json');
        echo $data;
        
    }

    /**
     * [index description] : Use for get user wallet detail excel
     */
    public function get_user_wallet_excel() {
        $result = $this->get_user_wallet_detail('array');
        $sr_no = 1;
        if(!empty($result)) {  
           foreach($result['aaData'] as $key=>$value) {
                 $data[] = array('Sr. No.' => $sr_no,
                    'User Name' => $value['agent_name'],
                    'User Code' => $value['id'],
                    'User Type' => $value['role_name'],
                    'Balance(Rs.)' => $value['amt']
                    );
                $sr_no++;
            }
        }
        else {
            $data = array(array());
        }
        $this->excel->data_2_excel('User_Wallet_List_excel_' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
            die;
    }

    /**
     * [index description] : Use for get user wallet detail from database
     */
    private function get_user_wallet_detail1($response_type = '') {
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');
        $this->datatables->select("1,concat(u.first_name,' ',u.last_name) as agent_name,u.id,ur.role_name,round(w.amt,2) as amt,
            (SELECT CONCAT(uu.first_name, ' ', uu.last_name) FROM users uu WHERE uu.id=u.level_3 ) AS rd_name, 
            (SELECT CONCAT(uu.first_name, ' ', uu.last_name) FROM users uu WHERE uu.id=u.level_4 ) AS dd_name,
            (CASE WHEN m.agent_code IS NULL THEN 'N' ELSE 'Y' END) as cb_agent
            , u.agent_code");
        $this->datatables->from('wallet w');
        $this->datatables->join('users u','w.user_id=u.id','inner');
        $this->datatables->join('user_roles ur','ur.id=u.role_id','inner'); 
        
        $this->datatables->join('msrtc_bos_dev.machine_registration m','m.agent_code=u.agent_code','left');  
        $this->datatables->join('vas_commission_detail v','v.user_id=u.id','left'); 

        $this->datatables->where('u.is_deleted','N');
        if(!empty($session_level)){
                $this->datatables->where('level_'.$session_level,$session_id);
            }
        $this->datatables->add_column('action', '<a href='.base_url() . 'admin/user_wallet/wallet_transactions/$1 title="View Wallet Transacations" class="views_wallet_trans margin" ref="$1"><i  class="glyphicon glyphicon-th-list"></i> </a>', 'id');
        $this->datatables->group_by('w.id');
        if ($response_type != '') {
            $data = $this->datatables->generate($response_type);
        } else {
            $data = $this->datatables->generate();
        }
        //last_query(1);
        return $data;                  
    }
    
     private function get_user_wallet_detail($response_type = '') {
       $session_level = $this->session->userdata('level');
       $session_id = $this->session->userdata('id');
       $this->datatables->select("1,concat(u.first_name,' ',u.last_name) as agent_name, u.agent_code,
(SELECT CONCAT(uu.first_name,' ',uu.last_name) FROM users uu WHERE uu.id=u.level_3 ) AS rd_name,
       (SELECT CONCAT(uu.first_name,' ',uu.last_name) FROM users uu WHERE uu.id=u.level_4 ) AS dd_name,
u.id,ur.role_name,round(w.amt,2) as amt , (CASE WHEN m.agent_code IS NULL THEN 'N' ELSE 'Y' END) as cb_agent
");
       $this->datatables->from('wallet w');
       $this->datatables->join('users u','w.user_id=u.id','inner');
       $this->datatables->join('user_roles ur','ur.id=u.role_id','inner');
        $this->datatables->join('msrtc_bos_dev.machine_registration m','m.agent_code=u.agent_code','left');  
$this->datatables->join('vas_commission_detail v','v.user_id=u.id','left'); 
       $this->datatables->where('u.is_deleted','N');
       if(!empty($session_level) && $session_level!='6'){
           $this->datatables->where('level_'.$session_level,$session_id);
       } else {
           $this->datatables->where('w.user_id',$session_id);
       }
       $this->datatables->add_column('action', '<a href='.base_url() . 'admin/user_wallet/wallet_transactions/$1 title="View Wallet Transacations" class="views_wallet_trans margin" ref="$1"><i  class="glyphicon glyphicon-th-list"></i> </a>', 'id');
       $this->datatables->group_by('w.id');
if ($response_type != '') {
           $data = $this->datatables->generate($response_type);
       } else {
           $data = $this->datatables->generate();
       }
       return $data;                  
   }

    /**
     * [index description] : Use for see user wallet transaction detail(user wise)
     */
    public function wallet_transactions($id) {
        $data['id'] = $id;
        $data['tran_reason']=  $this->lookup_detail_model->get_relevant_reason();
        $data['user_data']=  $this->users_model->where(array("id" => $id))->as_array()->find_all();
        load_back_view(USER_WALLET_TRANS_VIEW, $data);
    }

    /**
     * [index description] : Use for get user wallet transaction detail(user wise)
     */
    public function get_wallet_trans($id) {
      
        $input = $this->input->post(NULL,TRUE);
        $data = $this->get_wallet_trans_detail($id,$input,'json');
        echo $data;  
    }

    /**
     * [index description] : Use for get user wallet transaction detail in excel(user wise)
     */
    public function wallet_transaction_report_excel($id) {
        $input = $this->input->get();
        $result = $this->get_wallet_trans_detail($id,$input,'array');
        $sr_no = 1;

        if(!empty($result)) {
           foreach($result['aaData'] as $key=>$value) {
                 $data[] = array('Sr No' => $sr_no,
                    'Opening Balance' => $value['amt_before_trans'],
                    'Transaction Type' => $value['transaction_type'],
                    'Closing Balance' => $value['amt_after_trans'],
                    'Amount' => $value['amt'],
                    'Transaction Date' => $value['added_date'],
                    'Comment' =>$value['comment']
                    );
                $sr_no++;
            }
        }
        else {
            $data = array(array());
        }
        $this->excel->data_2_excel('Wallet_transaction_report_excel_' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
        die;
    } 

    /**
     * [index description] : Use for get user wallet transaction detail from database(user wise)
     */
    private function get_wallet_trans_detail($id,$input,$response_type = '') {

        $this->datatables->select('1,round(wt.amt_before_trans,2) as amt_before_trans,wt.transaction_type,round(wt.amt_after_trans,2) as amt_after_trans,round(wt.amt,2) as amt,wt.added_on as added_date,wt.comment,',false);
        $this->datatables->from('wallet_trans wt');
        $this->datatables->join('tickets t', "t.ticket_id = wt.ticket_id",'left');
        $this->datatables->where('wt.user_id',$id);
        if($input['tran_type']) {
           $this->datatables->where('wt.transaction_type', $input['tran_type']);
        }
        if($input['from_date']) {
           $this->datatables->where("DATE_FORMAT(wt.added_on, '%Y-%m-%d')>=", date('Y-m-d', strtotime($input['from_date'])));
        }
        if($input['till_date']) {
           $this->datatables->where("DATE_FORMAT(wt.added_on, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['till_date'])));
        }
        if($input['tran_reason']) {
           $this->datatables->where('wt.transaction_type_id', $input['tran_reason']);
        }
        // $this->db->order_by('wt.id','desc');
        if ($response_type != '') {
            $data = $this->datatables->generate($response_type);
        } else {
            $data = $this->datatables->generate();
        }
        return $data;

    }
      
    /**
     * [index description] : Use for show user/sub-agent topup request
     */  
    public function user_topup() {
       if($this->session->userdata('role_id') == DISTRIBUTOR_ROLE_ID) {
           $data['subagent_list'] = $this->users_model->column('id,concat(first_name," ",last_name) as agent_name')->where(array('role_id'=> SUBAGENTS_ROLE_ID,'parent_id' => $this->session->userdata('user_id')))->find_all();
           load_back_view(DISTRIBUTOR_TOPUP,$data);
       }
       else {
            $data['agent_list']= $this->users_model->get_agent(0,1);
            load_back_view(USER_TOPUP,$data);
            }
    }
     
    /**
     * [index description] : Use for get user topup request
     */
    public function get_topup_request() {

        $input = $this->input->post();
        $data = $this->get_topup_request_detail($input,'json');
        echo $data;
    }

    /**
     * [index description] : Use for get user topup request from database(Only Pending Request)
     */
    private function get_topup_request_detail($input,$response_type = '') {
        $this->datatables->select('1,wt.id,concat(u.first_name," ",u.last_name),u.id as user_id,ur.role_name,u.mobile_no,wt.amount,wt.bank_name,wt.transaction_no,wt.transaction_remark,DATE_FORMAT(wt.transaction_date,"%d-%m-%Y %h:%i:%s"),DATE_FORMAT(wt.created_date,"%d-%m-%Y %h:%i:%s") as created_date,CASE wt.is_approvel when "NA" THEN "Pending" when "A" THEN "Accepted" ELSE "Rejected" END',false);
        $this->datatables->from('wallet_topup wt ');
        $this->datatables->join('users u','wt.user_id=u.id','inner');
        $this->datatables->join('user_roles ur','ur.id=u.role_id','inner');
        $this->datatables->where('u.role_id !=',SUBAGENTS_ROLE_ID);
        $this->datatables->where('wt.is_approvel','NA');
        $this->datatables->where('wt.transaction_status IS NULL');
        $this->datatables->add_column('action', '<a href='.base_url() . 'admin/user_wallet/edit_topup_transaction/$1 title="Accept or Reject Topup" class="change_request_btn" ref="$1"><i  class="glyphicon glyphicon-pencil"></i> </a>', 'id');
    
        if($input['from_date']) {
           $this->datatables->where("DATE_FORMAT(wt.transaction_date, '%Y-%m-%d')>=", date('Y-m-d', strtotime($input['from_date'])));
        }
        if($input['till_date']) {
           $this->datatables->where("DATE_FORMAT(wt.transaction_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['till_date'])));
        }
        if($input['select_agent']) {
           $this->datatables->where('wt.user_id', $input['select_agent']);
        }
        $this->db->order_by('wt.id','desc');
        if ($response_type != '') {
            $data = $this->datatables->generate($response_type);
        } else {
            $data = $this->datatables->generate();
        }
        return $data;
    }
	 
    /**
     * [index description] : Use for edit user topup request(Accept/Reject process)
     */
    public function edit_topup_transaction($id) {
        $data['id'] = $id;
        $topup_details = $this->wallet_topup_model
                              ->where('id', $id)
                              ->find_all();
        $data['agent_topup_details'] = $topup_details['0'];
        $agent_details = $this->users_model
                              ->column("id,first_name,last_name,mobile_no")
                              ->where('id', $data['agent_topup_details']->user_id)
                              ->as_array()->find_all();                      
        $data['agent_details'] = $agent_details['0'];
        if($this->session->userdata('role_id') == DISTRIBUTOR_ROLE_ID) {
            load_back_view(DISTRIBUTOR_TOPUP_TRANS_VIEW, $data);
            }
            else {
            load_back_view(USER_TOPUP_TRANS_VIEW, $data);
            }
    }

    /**
     * [index description] : Use for edit user topup request(Accept/Reject process)
     */
    public function get_reason() {
        $input = $this->input->post();
        $config =   array(
                        array('field' => 'status', 'lable'=>'Status','rules' => 'trim|required', 'errors' =>array('required' => 'Please select Status ')),
                    );
        
        if(form_validate_rules($config)) {
            $status = $input['status'];
            $data = array();
            $data['id'] = $input['request_id'];
            $topupdata = $this->wallet_topup_model->where('id',$data['id'])->as_array()->find_all();
            if($topupdata[0]['is_approvel'] == 'NA' && $topupdata[0]['topup_by'] != "PG") {
                if($this->config->item('refferrer_email_enable')) {
                    /* get refferrer data to send $referrer_email starts */
                    $referrer_email = $referrer_mobile = "";
                    if($this->session->userdata('user_id')) {
                        $usrid                  =  $topupdata[0]['user_id'];
                        $user_referrer_details  =  $this->users_model->user_referrer_details($usrid);
                        if(is_array($user_referrer_details) && count($user_referrer_details) > 0) {
                            $referrer_email  = $user_referrer_details[0]['referrer_email'];
                            $referrer_mobile = $user_referrer_details[0]['referrer_mobile']; 
                            $flag_referrer   = true; 
                        }
                    }
                    /* get refferrer data to send $referrer_email ends  */
                }
                if($status == 'A') {
                    $msg = 'Accepted';
                    $data['data_array']['is_approvel'] = 'A';
                    $data['data_array']['app_rej_date'] = date("Y-m-d H:i:s", time());
                    $data['data_array']['app_rej_by'] = $this->session->userdata('user_id');
                    $data['data_array']['updated_by'] = $this->session->userdata('user_id');
                    $data['data_array']['updated_date'] = date("Y-m-d H:i:s", time());
                    $update_topup_data = $this->wallet_topup_model->update_topup($data);
                    
                    if($update_topup_data)  {
                        $walletData = $this->wallet_model->where('user_id',$topupdata[0]['user_id'])->as_array()->find_all();
                    
                        $trns_data['w_id'] = $walletData[0]['id'];
                        $trns_data['amt'] = $topupdata[0]['amount'];
                        $trns_data['wallet_type'] = 'actual_wallet';
                        $trns_data['comment'] = $topupdata[0]['transaction_remark'];
                        $trns_data['status'] = 'Rokad Admin Credited';
                        $trns_data['user_id'] = $topupdata[0]['user_id'];
                        $trns_data['added_by'] = $this->session->userdata('user_id');
                        $trns_data['added_on'] = date("Y-m-d H:i:s", time());
                        $trns_data['topup_id'] = $data['id'];
                        $trns_data['transaction_type'] = 'Credited';
                        $trns_data['is_status'] = 'Y'; 
                        $trns_data['amt_before_trans'] = $walletData[0]['amt'];
                        $trns_data['amt_after_trans'] = $walletData[0]['amt']+$topupdata[0]['amount'];
                        
                        if($walletData[0]['current_outstanding'] > 0) {
                            if($walletData[0]['current_outstanding'] >= $topupdata[0]['amount']) {
                                $update_current_outstanding = $walletData[0]['current_outstanding'] - $topupdata[0]['amount'];
                                $update_virtual_balance = $walletData[0]['virtual_balance'] + $topupdata[0]['amount'];
                                $update_actual_wallet = $walletData[0]['actual_wallet_balance'];

                                // Add Data into wallet trans 
                                $trns_data['virtual_amount'] = $topupdata[0]['amount'];
                                $trns_data['wallet_amt_before_trans'] = $update_actual_wallet;
                                $trns_data['wallet_amt_after_trans'] = $update_actual_wallet;
                                $trns_data['virtual_amt_before_trans'] = $walletData[0]['virtual_balance'];
                                $trns_data['virtual_amt_after_trans'] = $update_virtual_balance; 
                                $trns_data['outstanding_amount'] = $update_current_outstanding;
                                $trns_data['transaction_type_id'] = '12';
                            }
                            else {
                                $update_current_outstanding = $walletData[0]['current_outstanding'] - $walletData[0]['current_outstanding'];
                                $update_virtual_balance = $walletData[0]['virtual_balance'] + $walletData[0]['current_outstanding'];
                                $update_actual_wallet = $walletData[0]['actual_wallet_balance'] + ($topupdata[0]['amount'] - $walletData[0]['current_outstanding']);

                                // Add Data into wallet trans 
                                $trns_data['virtual_amount'] = $walletData[0]['current_outstanding'];
                                $trns_data['actual_wallet_amount'] = ($topupdata[0]['amount'] - $walletData[0]['current_outstanding']);
                                $trns_data['wallet_amt_before_trans'] = $walletData[0]['actual_wallet_balance'];
                                $trns_data['wallet_amt_after_trans'] = $update_actual_wallet;
                                $trns_data['virtual_amt_before_trans'] = $walletData[0]['virtual_balance'];
                                $trns_data['virtual_amt_after_trans'] = $update_virtual_balance; 
                                $trns_data['outstanding_amount'] = $update_current_outstanding;
                                $trns_data['transaction_type_id'] = '12';

                            }
                        }
                        else {
                            $update_current_outstanding = $walletData[0]['current_outstanding'];
                            $update_virtual_balance = $walletData[0]['virtual_balance'];
                            $update_actual_wallet = $walletData[0]['actual_wallet_balance'] + $topupdata[0]['amount'];

                            // Add Data into wallet trans 
                            $trns_data['actual_wallet_amount'] = $topupdata[0]['amount'];
                            $trns_data['wallet_amt_before_trans'] = $walletData[0]['actual_wallet_balance'];
                            $trns_data['wallet_amt_after_trans'] = $update_actual_wallet;
                            $trns_data['virtual_amt_before_trans'] = $update_virtual_balance;
                            $trns_data['virtual_amt_after_trans'] = $update_virtual_balance;
                            $trns_data['outstanding_amount'] = $update_current_outstanding;
                            $trns_data['transaction_type_id'] = '1';
                        }
                        $topup_request = $this->wallet_trans_model->agent_topup_request($trns_data);
                        log_data("topup/".date("M")."/".date("d")."/agent_topup_request_log.log", $trns_data,"Wallet Trans Detail (Accept Request)");

                        if($walletData[0]['current_outstanding'] > 0) {
                            //Virtual Credit entry in wallet trans log
                            if($walletData[0]['current_outstanding'] >= $topupdata[0]['amount']) {
                                $wallet_trans_log = [
                                                    'user_id' => $topupdata[0]['user_id'],
                                                    'wallet_id' => $walletData[0]['id'],
                                                    'wallet_trans_id' => $topup_request,
                                                    'wallet_type' => 'virtual_wallet',
                                                    'amt' => $topupdata[0]['amount'],
                                                    'comment'=>$topupdata[0]['transaction_remark'],
                                                    'amt_before_trans'=>$walletData[0]['virtual_balance'],
                                                    'amt_after_trans'=>$walletData[0]['virtual_balance']+$topupdata[0]['amount'],
                                                    'outstanding_amount' => $update_current_outstanding,
                                                    'topup_id' => $data['id'],
                                                    'transaction_type_id' => '12',
                                                    'transaction_type' => 'Credited',
                                                    'added_on' => date('Y-m-d H:i:s'),
                                                    'added_by'=> $this->session->userdata('user_id')
                                                 ];

                                $this->wallet_trans_log_model->insert($wallet_trans_log);
                            }
                            else {
                                //Virtual Credit entry in wallet trans log
                                $wallet_trans_log = [
                                                    'user_id' => $topupdata[0]['user_id'],
                                                    'wallet_id' => $walletData[0]['id'],
                                                    'wallet_trans_id' => $topup_request,
                                                    'wallet_type' => 'virtual_wallet',
                                                    'amt' => $walletData[0]['current_outstanding'],
                                                    'comment'=>$topupdata[0]['transaction_remark'],
                                                    'amt_before_trans'=>$walletData[0]['virtual_balance'],
                                                    'amt_after_trans'=>$walletData[0]['virtual_balance']+$walletData[0]['current_outstanding'],
                                                    'outstanding_amount' => $update_current_outstanding,
                                                    'topup_id' => $data['id'],
                                                    'transaction_type_id' => '1',
                                                    'transaction_type' => 'Credited',
                                                    'added_on' => date('Y-m-d H:i:s'),
                                                    'added_by'=> $this->session->userdata('user_id')
                                                 ];

                                $this->wallet_trans_log_model->insert($wallet_trans_log);

                                // Actual Wallet entry in wallet trans log
                                $wallet_trans_log = [
                                                    'user_id' => $topupdata[0]['user_id'],
                                                    'wallet_id' => $walletData[0]['id'],
                                                    'wallet_trans_id' => $topup_request,
                                                    'wallet_type' => 'actual_wallet',
                                                    'amt' => $topupdata[0]['amount'] - $walletData[0]['current_outstanding'],
                                                    'comment'=>$topupdata[0]['transaction_remark'],
                                                    'amt_before_trans'=> $walletData[0]['actual_wallet_balance'],
                                                    'amt_after_trans'=>$update_actual_wallet,
                                                    'outstanding_amount' => $update_current_outstanding,
                                                    'topup_id' => $data['id'],
                                                    'transaction_type_id' => '1',
                                                    'transaction_type' => 'Credited',
                                                    'added_on' => date('Y-m-d H:i:s'),
                                                    'added_by'=> $this->session->userdata('user_id')
                                                 ];

                                $this->wallet_trans_log_model->insert($wallet_trans_log);
                            }
                        }
                        else {
                            // Actual Wallet entry in wallet trans log 
                            $wallet_trans_log = [
                                                'user_id' => $topupdata[0]['user_id'],
                                                'wallet_id' => $walletData[0]['id'],
                                                'wallet_trans_id' => $topup_request,
                                                'wallet_type' => 'actual_wallet',
                                                'amt' => $topupdata[0]['amount'],
                                                'comment'=>$topupdata[0]['transaction_remark'],
                                                'amt_before_trans'=>$walletData[0]['actual_wallet_balance'],
                                                'amt_after_trans'=>$walletData[0]['actual_wallet_balance']+$topupdata[0]['amount'],
                                                'topup_id' => $data['id'],
                                                'transaction_type_id' => '1',
                                                'transaction_type' => 'Credited',
                                                'added_on' => date('Y-m-d H:i:s'),
                                                'added_by'=> $this->session->userdata('user_id')
                                             ];

                            $this->wallet_trans_log_model->insert($wallet_trans_log);
                        }

                        if($topup_request) {
                            $wallet_data['user_id'] = $topupdata[0]['user_id'];
                            $wallet_data['data_array']['amt'] = $walletData[0]['amt']+$topupdata[0]['amount'];
                            $wallet_data['data_array']['actual_wallet_balance'] = $update_actual_wallet;
                            $wallet_data['data_array']['virtual_balance'] = $update_virtual_balance;
                            $wallet_data['data_array']['current_outstanding'] = $update_current_outstanding;
                            $wallet_data['data_array']['last_transction_id'] = $topup_request;
                            $wallet_data['data_array']['updated_by'] = $this->session->userdata('user_id');
                            $wallet_data['data_array']['updated_date'] = date("Y-m-d H:i:s", time());
                            $update_topup_data = $this->wallet_model->update_wallet($wallet_data);
                            
                            log_data("topup/".date("M")."/".date("d")."/agent_topup_request_log.log", $wallet_data,"Wallet Data (Accept Request)");

                            $title = array('User Id','Amount','Amount Befor','Amount After','Topup Id','Ticket Id','Date','Detail');
                            $val = array ($topupdata[0]['user_id'],$topupdata[0]['amount'],$walletData[0]['amt'],$walletData[0]['amt']+$topupdata[0]['amount'],$topup_request,'',date("Y-m-d H:i:s"),'Function Detail : user_wallet/get_reason, Detail : Accept topup request'); 
                            $FileName = "wallet/user_wallet_detail/wallet_".$topupdata[0]['user_id'].".csv";
                            make_csv($FileName,$title,$val);

                            if($update_topup_data) {
                                //send email and sms to agent and admin once accepted
                                $result_accepted        = $this->get_topup_emaildata($data['id']);
                                $mail_replacement_array =   array (
                                                "{{topup_by}}"                         => $result_accepted['topup_by'],
                                                "{{amount}}"                           => $result_accepted['amount'],
                                                "{{bank_name}}"                        => $result_accepted['bank_name'],
                                                "{{bank_acc_no}}"                      => $result_accepted['bank_acc_no'],
                                                "{{transaction_no}}"                   => $result_accepted['transaction_no'],
                                                "{{transaction_remark}}"               => $result_accepted['transaction_remark'],
                                                "{{username}}"                         => $result_accepted['agent_name'],
                                                "{{useremail}}"                        => $result_accepted['email'],
                                                "{{transaction_date}}"                 => $result_accepted['transaction_date'],
                                                "{{is_approvel}}"                      => $result_accepted['is_approvel']
                                            ); 
                                $data['mail_content']       =   str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),WALLET_TOPUP_REQUEST);
                                
                                // for admin
                                eval(ADMIN_EMAIL_ARRAY_FOR_AGENT);
                                $mailbody_array_admin       =   array( "subject"  => "(".ENVIRONMENT.") "."Approved TopUp Request of Rs ".$result_accepted['amount']." for Agent ".$result_accepted['agent_name']." ",
                                                                       "message"  => $data['mail_content'],
                                                                       "to"       => $admin_array_for_agent
                                                                );
                                $mail_result_admin          =   send_mail($mailbody_array_admin);
                                
                                //send email to refferrer
                                if($this->config->item('refferrer_email_enable') && $flag_referrer == true)
                                {
                                    $referrer_emailsend[]   = $referrer_email;
                                    //for refferrer
                                    $data['mail_content']       =   str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),WALLET_TOPUP_REQUEST);
                                    $mailbody_array             =   array( "subject"  => "(".ENVIRONMENT.") "."Approved TopUp Request of Rs ".$result_accepted['amount']." for Agent ".$result_accepted['agent_name']." ",
                                                                       "message"  => $data['mail_content'],
                                                                       "to"       => $referrer_emailsend
                                                                );
                                    $mail_result_refferrer      = send_mail($mailbody_array);  
                                    if($mail_result_refferrer)
                                    {
                                       $data1['flag_refferrer'] = "@#success#@";
                                    }
                                }
                                //for agent
                                $mailbody_array_agent       =   array(  "subject" => "Approved TopUp Request of Rs ".$result_accepted['amount']." ",
                                                                        "message" => $data['mail_content'],
                                                                        "to"      => $result_accepted['email']
                                                                    );
                                $mail_result_agent          = send_mail($mailbody_array_agent);  
                                
                                if($mail_result_admin && $mail_result_agent)
                                {
                                   $data['flag']       = "@#success#@";
                                }
                                $mail_replacement_arraysms =   array(
                                                                        "{{amount}}" => $wallet_data['data_array']['amt']
                                                                    ); 
                                //send sms to agent
                                $flag     = true;
                                if($flag == true) {
                                    $msg_to_sent = str_replace(array_keys($mail_replacement_arraysms),array_values($mail_replacement_arraysms),WALLET_TOPUP_REQUEST_SMS);
                                    $is_send = sendSMS($result_accepted['mobile_no'], $msg_to_sent);
                                    if($is_send) {
                                       $data1['sms']       = "@#success#@";
                                    }
                                }
                                //send sms to admin
                                $flags     = true;
                                if($flags == true) {
                                    $msg_to_sent_admin = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),WALLET_TOPUP_REQUEST_SMS_FOR_ADMIN_ACC);
                                    $sms_admin         = sendSMS($this->config->item("admin_mobile_no"), $msg_to_sent_admin);
                                    if($sms_admin) {
                                       $data1['sms_admin']       = "@#success#@";
                                    }
                                }
                                $this->session->set_flashdata('msg','Request Approve Successfully');
//                                $this->session->flashdata('msg');
                                redirect(base_url()."admin/user_wallet/user_topup");
                            }
                        }
                    }
                    else {
                        $data['error'] = "Data not updated in wallet top up table";
                        log_data("topup/".date("M")."/".date("d")."/agent_topup_request_log.log", $data,"Double Hit By User");
                        $this->session->set_flashdata('msg_type', 'error');
                        $this->session->set_flashdata('msg', 'Data not update.');
                        redirect(base_url()."admin/user_wallet/user_topup");
                    }
                }
                elseif($status == 'R') {
                    $msg = 'Rejected';
                    $data['data_array']['is_approvel'] = 'R';
                    $data['data_array']['reject_reason'] = $input['comment'];
                    $data['data_array']['app_rej_date'] = date("Y-m-d H:i:s", time());
                    $data['data_array']['app_rej_by'] = $this->session->userdata('user_id');
                    $data['data_array']['updated_by'] = $this->session->userdata('user_id');
                    $data['data_array']['updated_date'] = date("Y-m-d H:i:s", time());
                    
                    $update_topup_data = $this->wallet_topup_model->update_topup($data);
                    if($update_topup_data) {
                        log_data("topup/".date("M")."/".date("d")."/agent_topup_request_log.log", $data,"Request Reject Time Data");

                        $result_accepted        = $this->get_topup_emaildata($data['id']);
                        $mail_replacement_array =   array (
                                        "{{topup_by}}"                         => $result_accepted['topup_by'],
                                        "{{amount}}"                           => $result_accepted['amount'],
                                        "{{bank_name}}"                        => $result_accepted['bank_name'],
                                        "{{bank_acc_no}}"                      => $result_accepted['bank_acc_no'],
                                        "{{transaction_no}}"                   => $result_accepted['transaction_no'],
                                        "{{transaction_remark}}"               => $result_accepted['transaction_remark'],
                                        "{{username}}"                         => $result_accepted['agent_name'],
                                        "{{useremail}}"                        => $result_accepted['email'],
                                        "{{transaction_date}}"                 => $result_accepted['transaction_date'],
                                        "{{is_approvel}}"                      => $result_accepted['is_approvel'],
                                        "{{reject_reason}}"                    => $result_accepted['reject_reason'],
                                    ); 
                        
                        $data['mail_content']       =   str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),WALLET_TOPUP_REQUEST_REJECTED);
                                
                        // for admin
                        eval(ADMIN_EMAIL_ARRAY_FOR_AGENT);
                        $mailbody_array_admin       =   array( "subject"  => "(".ENVIRONMENT.") "."Rejected TopUp Request of Rs ".$result_accepted['amount']." for Agent ".$result_accepted['agent_name']." ",
                                                               "message"  => $data['mail_content'],
                                                               "to"       => $admin_array_for_agent
                                                        );
                        $mail_result_admin          =   send_mail($mailbody_array_admin);  

                        //send email to refferrer
                        if($this->config->item('refferrer_email_enable') && $flag_referrer == true) {
                            $referrer_emailsend[]       = $referrer_email;
                            $data['mail_content']       =   str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),WALLET_TOPUP_REQUEST_REJECTED);
                            $mailbody_array             =   array( "subject"  => "(".ENVIRONMENT.") "."Rejected TopUp Request of Rs ".$result_accepted['amount']." for Agent ".$result_accepted['agent_name']." ",
                                                               "message"  => $data['mail_content'],
                                                               "to"       => $referrer_emailsend
                                                        );
                            $mail_result_refferrer      = send_mail($mailbody_array);  
                            if($mail_result_refferrer) {
                               $data1['flag_refferrer'] = "@#success#@";
                            }
                        }

                        $mailbody_array_agent       =   array(  "subject" => "Rejected TopUp Request of Rs ".$result_accepted['amount']." ",
                                                                "message" => $data['mail_content'],
                                                                "to"      => $result_accepted['email']
                                                            );
                        $mail_result_agent          = send_mail($mailbody_array_agent);  
                                
                        if($mail_result_admin && $mail_result_agent) {
                           $data['flag']       = "@#success#@";
                        }

                        //send sms 
                        $flag     = true;
                        if($flag == true) {
                            $msg_to_sent = WALLET_TOPUP_REQUEST_REJECTION_SMS;
                            $is_send     = sendSMS($result_accepted['mobile_no'], $msg_to_sent);
                            if($is_send) {
                               $data1['sms']       = "@#success#@";
                            }
                        }
                                
                        //send sms to admin         
                        $flags     = true;
                        if($flags == true) {
                            $msg_to_sent_admin = str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),WALLET_TOPUP_REQUEST_SMS_FOR_ADMIN_REJ);
                            $is_send           = sendSMS($this->config->item("admin_mobile_no"), $msg_to_sent_admin);
                            if($is_send) {
                               $data1['sms_admin']       = "@#success#@";
                            }
                        }
                        $this->session->set_flashdata('regmsg','Topup Request Rejected');
                        $this->session->flashdata('regmsg');
                        redirect(base_url()."admin/user_wallet/user_topup");
                    }
                    else {
                        $custom_error_mail = array(
                                            "custom_error_subject" => "Data Not Updated Of Wallet TopUp.",
                                            "custom_error_message" => "Data Not Updated of wallet topup while rejecting wallet topup request - ".json_encode($data["data_array"])
                                            );

                        $this->common_model->developer_custom_error_mail($custom_error_mail);

                       log_data("topup/".date("M")."/".date("d")."/agent_topup_request_log.log", $data,"Request Reject Time Data Not Updated"); 
                    }
                }
                else {
                    $data['error'] = "Status ".$status." Selected";
                    $data['error_msg'] = " Request Status Not Proper";
                    log_data("topup/".date("M")."/".date("d")."/agent_topup_request_log.log", $data,"Request Process Error");
                    $this->session->set_flashdata('msg','Please Provide Proper Data.');
                    $this->session->flashdata('msg');
                    redirect(base_url()."admin/user_wallet/user_topup");
                }
            }
            else {
                $data['error'] = "Double Hit By User";
                log_data("topup/".date("M")."/".date("d")."/agent_topup_request_log.log", $data,"Double Hit By User");
                $this->session->set_flashdata('msg','Please select Status.');
                $this->session->flashdata('msg');
                redirect(base_url()."admin/user_wallet/user_topup");
                
            }
            
            
        }
        else {
            $data['error'] = $this->form_validation->error_array();
            log_data("topup/".date("M")."/".date("d")."/agent_topup_request_log.log", $data['error'],"Request Process Error");
            $this->session->set_flashdata('msg','Please select Status.');
            $this->session->flashdata('msg');
            redirect(base_url()."admin/user_wallet/user_topup");
        }
        
     }
	 
    /**
     * [index description] : Use for show user topup detail
     */
    public function user_topup_history() {
        if($this->session->userdata('role_id') == DISTRIBUTOR_ROLE_ID) {
             $data['subagent_list'] = $this->users_model->column('id,concat(first_name," ",last_name) as agent_name')->where(array('role_id'=> SUBAGENTS_ROLE_ID,'parent_id' => $this->session->userdata('user_id')))->as_array()->find_all();
            load_back_view(DISTRIBUTOR_TOPUP_HISTORY,$data);
        }
        else {
              $data['agent_list']= $this->users_model->get_agent(); 
              load_back_view(USER_TOPUP_HISTORY,$data);
        }
    }
	 
    /**
     * [index description] : Use for get user topup detail
     */
	public function get_topup_request_history() {
        $input = $this->input->post();
        $data = $this->get_topup_request_history_detail($input,'json');
        echo $data;     
    }

    /**
     * [index description] : Use for show user topup detail in excel
     */
    public function topup_request_history_excel() {
        $input = $this->input->get();
        $result = $this->get_topup_request_history_detail($input,'array');
        $sr_no = 1;
        if(!empty($result)) {
            foreach ($result['aaData'] as $key => $value) {
                $data[] = array('Sr no' => $sr_no,
                                'Agent Name' => $value['agent_name'],
                                'Agent Code' => $value['id'],
                                'Mobile Number' => $value['mobile_no'],
                                'Payment Mode' => $value['topup_by'],
                                'Pg tracking id' => $value['pg_tracking_id'],
                                'Amount' => $value['amount'],
                                'Transaction Type' => $value['topup_by'],
                                'Bank Name' => $value['bank_name'],
                                'Transaction Ref No' => $value['transaction_no'],
                                'Remark' => $value['transaction_remark'],
                                'Transaction Date' => $value['transaction_date'],
                                'Requested Date' => $value['requested_date'],
                                'Response Date' => $value['app_rej_date'],
                                'Reject Reason' => $value['reject_reason'], 
                                'Status' => $value['is_approvel_status']
                               );
                $sr_no++;
            }
        }
        else {
            $data = array(array());
        }
        $this->excel->data_2_excel('topup_request_history_excel' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
        die;
    }

    /**
     * [index description] : Use for show user topup detail from database
     */
    private function get_topup_request_history_detail($input,$response_type = '') {

        $this->datatables->select('1,concat(u.first_name," ",u.last_name) as agent_name,u.id,u.mobile_no,wt.topup_by,ifnull(wt.pg_tracking_id,"-") as pg_tracking_id,wt.amount,wt.bank_name,wt.transaction_no,wt.transaction_remark,DATE_FORMAT(wt.transaction_date,"%d-%m-%Y") as transaction_date,DATE_FORMAT(wt.created_date,"%d-%m-%Y %h:%i:%s") as requested_date,DATE_FORMAT(wt.app_rej_date,"%d-%m-%Y %h:%i:%s") as app_rej_date,wt.reject_reason,(CASE wt.is_approvel when "NA" THEN "Pending" when "A" THEN "Accepted" when "R" THEN "Rejected" ELSE `is_approvel` END) as is_approvel_status,',false); 
        $this->datatables->from('wallet_topup wt ');
        $this->datatables->join('users u','wt.user_id=u.id','inner');
        $this->datatables->where('wt.is_approvel !=" " ');
        if($input['from_date']) {
           $this->datatables->where("DATE_FORMAT(wt.transaction_date, '%Y-%m-%d')>=", date('Y-m-d', strtotime($input['from_date'])));
        }
        if($input['till_date']) {
           $this->datatables->where("DATE_FORMAT(wt.transaction_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['till_date'])));
        }
        if($input['select_agent']) {
           $this->datatables->where('wt.user_id', $input['select_agent']);
        }
        if($input['request_status']) {
           $this->datatables->where('wt.is_approvel', $input['request_status']);
        }
        $this->db->order_by('wt.updated_date','desc');
        if ($response_type != '') {
            $data = $this->datatables->generate($response_type);
        } else {
            $data = $this->datatables->generate();
        }
        return $data;
    }

    /**
     * [index description] : Use for save wallet topup request by admin
     */
    function wallet_trans_topup()  
    {
        $config =array(
                        array('field' => 'select_agent_pay', 'lable'=>'Agent Name','rules' => 'trim|required', 'errors' =>array('required' => 'Please select the Agent Name')),
                        array('field' => 'trans_type', 'lable'=>'Transaction','rules' => 'trim|required', 'errors' =>array('required' => 'Please select the Transaction Type')),
                        array('field' => 'enter_amount', 'lable'=>'Enter Amount','rules' => 'trim|required|numeric|greater_than[0]', 'errors' =>array('required' => 'Please enter the Topup Amount','greater_than' => 'Please enter greater than zero value.')),
                        array('field' => 'bank_name', 'lable'=>'Bank Name','rules' => 'trim', 'errors' =>array()),
                        array('field' => 'account_number', 'lable'=>'Account Number','rules' => 'trim|integer', 'errors' =>array('integer' => 'Please enter only Number')),
                        array('field' => 'tran_ref_no', 'lable'=>'Transaction Reference Number','rules' => 'trim', 'errors' =>array()),
                        array('field' => 'remarks', 'lable'=>'Remarks','rules' => 'trim|required', 'errors' =>array('required' => 'Please enter remark')),
                        array('field' => 'created_date', 'lable'=>'Created Date','rules' => 'trim|required', 'errors' =>array('Please enter the Created date')),
                );

        if(form_validate_rules($config)) {
            $input = $this->input->post();
            $data = [];
            $data['topup_by']  = $input['trans_type'];
            $data['amount'] = $input['enter_amount'];
            $data['bank_name'] = $input['bank_name'];
            $data['bank_acc_no'] = $input['account_number'];
            $data['transaction_no'] = $input['tran_ref_no'];
            $data['transaction_remark'] = $input['remarks'];
            $data['user_id'] = $input['select_agent_pay'];
            $data['transaction_date'] = date('Y-m-d', strtotime($input['created_date']));
            $data['created_by'] = $this->session->userdata('user_id');
            $data['created_date'] = date("Y-m-d H:i:s", time());
            $data['updated_by'] = $this->session->userdata('user_id');
            $data['updated_date'] = date("Y-m-d H:i:s", time());
            $data['is_status'] = 'Y';
            $data['is_approvel'] = 'NA';
            $topup_request = $this->wallet_topup_model->agent_topup_request($data);
            if($topup_request) {   
                $data1['flag'] = "@#success#@";
            }
            else {
                $data1['flag'] = "@#failed#@123";
            }
            data2json($data1);
        }
        else {
            $input = $this->input->post();
            $data['error'] = $this->form_validation->error_array();
            log_data("topup/".date("M")."/agent_topup_request_log.log", $input,"User Enters data");
            log_data("topup/".date("M")."/agent_topup_request_log.log", $data['error'],"form error");
            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'Please enter proper data .');
            
            $data1['flag'] = "@#failed#@";
            data2json($data1);
        }
    }

    /**
     * [get_topup_emaildata description] : use in get_reason function for get topup detail 
     * @param  [type] $id [description] : wallet topup id 
     * @return [type]     [description]
     */
	public function get_topup_emaildata($id)
	{
		if(isset($id) && $id != '') {
			$this->db->select('concat(u.first_name," ",u.last_name) as agent_name,u.mobile_no,wt.topup_by,u.email,wt.amount,,wt.bank_name,wt.bank_acc_no,wt.transaction_no,DATE_FORMAT(wt.transaction_date,"%d-%m-%Y %h:%i:%s") as transaction_date,wt.transaction_remark,(CASE wt.is_approvel when "NA" THEN "Pending" when "A" THEN "Accepted" when "R" then "Rejected" ELSE `is_approvel`  END) as is_approvel,wt.app_rej_date,wt.reject_reason');
			$this->db->from("wallet_topup wt ");
			$this->db->join('users u','wt.user_id=u.id','inner');
			$this->db->where("wt.id = " . $id);
			$query        = $this->db->get();
			$result_array = $query->row_array();

			return $result_array;
		}
	}
    
    /**
     * [top_up_reversal description] : get wallet trans data for the topup reversal process
     * @param  [type] $id [description] : user id 
     * @return [type]     [description]
     */
    public function top_up_reversal($id) {
        $data['id']          =  $id;
        $data['tran_reason'] =  $this->lookup_detail_model->get_reason();
        $data['user_data']   =  $this->users_model->where(array("id" => $id))->as_array()->find_all();
        eval(FME_AGENT_ROLE_ID);
        if (in_array($data['user_data'][0]['role_id'] , $fme_agent_role_id)) 
        {
            $data['wallet_bs']                 = $this->wallet_model->where(array('user_id' => $id))->find_all();
            $data['wallet_agent_balance_show'] = $data['wallet_bs'][0]->amt;
        }
        load_back_view(TOPUP_REVERSAL_VIEW, $data);
    }
    
    /**
     * [top_up_reversal description] : get wallet trans data for the topup reversal process
     * @param  [type] $id [description] : user id 
     * @return [type]     [description]
     */
    public function toup_reversal_trans($id) {
        $this->datatables->select('1,wt.topup_id,wt.amt_before_trans,wt.transaction_type,wt.amt_after_trans,wt.amt,DATE_FORMAT(wt.added_on,"%d-%m-%Y %h:%i:%s") as added_date,wt.comment,tp.topup_by,tp.id,tp.reversal_flag,',false);
        $this->datatables->from('wallet_trans wt');
        $this->datatables->join('wallet_topup tp', "tp.id = wt.topup_id",'left');
        $this->datatables->join('wallet w', "w.user_id = tp.user_id",'left');
        $this->datatables->where('tp.user_id',$id);
        $this->db->where_not_in('tp.topup_by','PG');
        $this->datatables->where_in('tp.is_approvel','A');
        $this->datatables->where('w.actual_wallet_balance >=','wt.amt',false);
        $this->datatables->add_column('action','$1' , 'toup_reversal_callback(reversal_flag,id)');
        
        if($this->input->post('from_date')) {
           $this->datatables->where("DATE_FORMAT(wt.added_on, '%Y-%m-%d')>=", date('Y-m-d', strtotime($this->input->post('from_date'))));
        }
        if($this->input->post('till_date')) {
           $this->datatables->where("DATE_FORMAT(wt.added_on, '%Y-%m-%d')<=", date('Y-m-d', strtotime($this->input->post('till_date'))));
        }
        if($this->input->post('tran_reason')) {
           $this->datatables->where('wt.transaction_type_id', $this->input->post('tran_reason'));
        }
		$this->db->order_by('tp.reversal_flag,tp.id','asc');
        echo $data = $this->datatables->generate('json');  
    }
   
    /**
     * [reversal description] : get topup detail for reversal
     * @return [type] [description]
     */
    public function reversal() {
        $input = $this->input->post();
        if($input['id'] != '') {
            $this->db->select('w.id as wallet_id,w.amt as current_wallet_amt,tp.user_id,wt.amt_before_trans,wt.transaction_type,wt.amt_after_trans,wt.amt,DATE_FORMAT(wt.added_on,"%d-%m-%Y %h:%i:%s") as added_date,wt.comment,tp.topup_by,tp.id',false);
            $this->db->from('wallet_trans wt');
            $this->db->join('wallet_topup tp', "tp.id = wt.topup_id",'left');
            $this->db->join('wallet w', "w.user_id = tp.user_id",'left');
            $this->db->where('tp.id',$input['id']);
            $this->db->where_not_in('tp.topup_by','PG');
            $this->db->where_in('tp.is_approvel','A');
            $this->db->where('w.actual_wallet_balance >=','wt.amt');
       
            $query = $this->db->get();
            $tdetails = $query->result_array(); 
            $data["flag"]         = '@#success#@';
            $data["wallet_data"]  = $tdetails[0];
        }
        else  {
            $data["flag"]     = '@#error#@';
            $data["msg_type"] = 'error';
            $data["msg"]      = 'Some error occured.';
        }
        
        data2json($data);
    }
    
    /**
     * [reversal description] : use for  topup reversal process
     * @return [type] [description]
     */
    public function reversalsave() {
        $input = $this->input->post();

        $topupDetail = $this->wallet_topup_model->where(array("id" => $input['id']))->as_array()->find_all();
        $walletDetail = $this->wallet_model->where(array("user_id" => $input['user_id']))->as_array()->find_all();
        if(!empty($walletDetail) && $walletDetail[0]['actual_wallet_balance'] >= $input['amt']) {
            if(!empty($topupDetail) && $topupDetail[0]['reversal_flag'] == '0') {
                if(!empty($input)) {
                    $wallet_trans_detail =  array(  
                        "w_id"                      => $input['wallet_id'],
                        "amt"                       => $input['amt'],
                        "actual_wallet_amount"      => $input['amt'],
                        "wallet_type"               => 'actual_wallet', 
                        "comment"                   => $input['reason'],
                        "status"                    => "Rokad Admin Debited",
                        "user_id"                   => $input['user_id'],
                        "amt_before_trans"          => $walletDetail[0]['amt'],
                        "amt_after_trans"           => ($walletDetail[0]['amt'] - $input['amt']),
                        "wallet_amt_before_trans"   => $walletDetail[0]['actual_wallet_balance'],
                        "wallet_amt_after_trans"    => ($walletDetail[0]['actual_wallet_balance'] - $input['amt']),
                        "virtual_amt_before_trans"  => $walletDetail[0]['virtual_balance'],
                        "virtual_amt_after_trans"   => $walletDetail[0]['virtual_balance'],
                        "outstanding_amount"        => $walletDetail[0]['current_outstanding'],
                        "added_by"                  => $this->session->userdata("user_id"),
                        "added_on"                  => date("Y-m-d H:i:s"),
                        "topup_id"                  => $input['id'],
                        "transaction_type_id"       => '7',
                        "transaction_type"          => 'Debited',
                        "is_status"                 => 'Y'
                    );

                    $wallet_trans_id    = $this->wallet_trans_model->insert($wallet_trans_detail);
                    if($wallet_trans_id > 0) {
                        $pg_array_where          = array('id' => $input['id']);
                        $fields_update           = array (
                            'reversal_flag' => '1',
                            'updated_by'    => $this->session->userdata('user_id'),
                            'updated_date'  => date('Y-m-d h:m:s')
                        );
                        // update wallet topup
                        $is_wallet_topup_updated = $this->wallet_topup_model->update_where($pg_array_where, '', $fields_update);
                        // Insert into wallet Trans Log                    
                        $wallet_trans_log =  array(  
                                    "user_id"                   => $input['user_id'],
                                    "wallet_id"                 => $input['wallet_id'],
                                    "wallet_trans_id"           => $wallet_trans_id,   
                                    "wallet_type"               => 'actual_wallet', 
                                    "amt"                       => $input['amt'],
                                    "outstanding_amount"        => $walletDetail[0]['current_outstanding'],
                                    "comment"                   => $input['reason'],
                                    "amt_before_trans"          => $walletDetail[0]['actual_wallet_balance'],
                                    "amt_after_trans"           => ($walletDetail[0]['actual_wallet_balance'] - $input['amt']),
                                    "added_by"                  => $this->session->userdata("user_id"),
                                    "added_on"                  => date("Y-m-d H:i:s"),
                                    "topup_id"                  => $input['id'],
                                    "transaction_type_id"       => '7',
                                    "transaction_type"          => 'Debited',
                        );
                        $this->wallet_trans_log_model->insert($wallet_trans_log);

                        if($is_wallet_topup_updated) {
                            $wallet_data['user_id']                             = $input['user_id'];
                            $wallet_data['data_array']['amt']                   = ($input['currentwalletamount'] - $input['amt']);
                            $wallet_data['data_array']['actual_wallet_balance'] = ($walletDetail[0]['actual_wallet_balance'] - $input['amt']);
                            $wallet_data['data_array']['last_transction_id']    = $wallet_trans_id;
                            $wallet_data['data_array']['updated_by']            = $this->session->userdata('user_id');
                            $wallet_data['data_array']['updated_date']          = date("Y-m-d H:i:s", time());

                            $update_topup_data = $this->wallet_model->update_wallet($wallet_data);

                            log_data("agentbalance_reversal/".date("M")."/".date("d")."/agentbalance_reversal.log", $wallet_data,"agentbalance_reversal (Request)");

                            $title = array('User Id','Amount','Amount Befor','Amount After','Topup Id','Ticket Id','Date','Detail');
                            $val   = array ($input['user_id'],$input['amt'],$input['currentwalletamount'],($input['currentwalletamount'] - $input['amt']),$wallet_trans_id,'',date("Y-m-d H:i:s"),'Function Detail : user_wallet/reversalsave, Detail : topup Reversal request');

                            $FileName = "reversal/user_topup_reversal/reversal_".$input['user_id'].".csv";
                            make_csv($FileName,$title,$val);

                            if($update_topup_data) {
                                echo json_encode(array('status'=>'success',"msg"=>"Data updated sucessfully"));
                                exit;
                            }
                        }
                    }
                }
                else {
                    echo json_encode(array('status'=>'success',"msg"=>"Invalid data"));
                    exit();
                }
            }
            else {
                echo json_encode(array('status'=>'success',"msg"=>"Invalid data"));
                exit();
            }
        }
        else {
            echo json_encode(array('status'=>'success',"msg"=>"Invalid data"));
            exit();
        }
    }
    /**
     * [get_distributer_wallet] : use for  Distributers sub-agent wallet to list
     * @return [type] [description]
     */
    public function get_distributer_wallet() {
        $data = $this->get_distributer_wallet_detail('json');
        echo $data;
    }
    
    /**
     * [index description] : Use for get Distributers sub-agent wallet detail excel
     */
    public function get_distributer_wallet_excel() {
        $result = $this->get_distributer_wallet_detail('array');
        $sr_no = 1;
        if(!empty($result)) {  
           foreach($result['aaData'] as $key=>$value) {
                 $data[] = array('Sr_no' => $sr_no,
                    'Agent Name' => $value['agent_name'],
                    'Agent Email ID' => $value['email'],
                    'Agent Code' => $value['id'],
                    'Balance' => $value['amt']
                    );
                $sr_no++;
            }
        }
        else {
            $data = array(array());
        }
        $this->excel->data_2_excel('Wallet_details_report_excel_' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
            die;
    }

    /**
     * [index description] : Use for get Distributers sub-agent wallet detail from database
     */
    private function get_distributer_wallet_detail($response_type = '') {

        if ($this->session->userdata('role_id') == DISTRIBUTOR_ROLE_ID) {
        $this->datatables->select("1,concat(u.first_name,' ',u.last_name) as agent_name,u.email,u.id,w.amt");
        $this->datatables->from('wallet w');
        $this->datatables->join('users u','w.user_id=u.id','inner');
        $this->datatables->where("u.parent_id", $this->session->userdata('user_id'));
        $this->datatables->where("u.role_id", SUBAGENTS_ROLE_ID);
        $this->datatables->add_column('action', '<a href='.base_url() . 'admin/user_wallet/wallet_transactions/$1 title="View Wallet Transacations" class="views_wallet_trans margin" ref="$1"><i  class="glyphicon glyphicon-th-list"></i> </a>', 'id');
        if ($response_type != '') {
            $data = $this->datatables->generate($response_type);
        } else {
            $data = $this->datatables->generate();
        }
        return $data;   
        }
        else
        {
            return FALSE;
        }
    }
    
     /**
     * [index description] : Use for get Distributers sub-agent topup request
     */
    public function get_topup_request_sub_agents() {

        $input = $this->input->post();
        $data = $this->get_subagent_topup_request_detail($input,'json');
        echo $data;
    }

    /**
     * [index description] : Use for get Distributers sub-agent topup request from database(Only Pending Request)
     */
    private function get_subagent_topup_request_detail($input,$response_type = '') {
        $this->datatables->select('1,wt.id,concat(u.first_name," ",u.last_name),u.id as user_id,u.mobile_no,wt.amount,wt.bank_name,wt.transaction_no,wt.transaction_remark,DATE_FORMAT(wt.transaction_date,"%d-%m-%Y %h:%i:%s"),DATE_FORMAT(wt.created_date,"%d-%m-%Y %h:%i:%s") as created_date,CASE wt.is_approvel when "NA" THEN "Pending" when "A" THEN "Accepted" ELSE "Rejected" END',false);
        $this->datatables->from('wallet_topup wt ');
        $this->datatables->join('users u','wt.user_id=u.id','inner');
        $this->datatables->where('wt.is_approvel','NA');
        $this->datatables->where("u.parent_id", $this->session->userdata('user_id'));
        $this->datatables->where("u.role_id", SUBAGENTS_ROLE_ID);
        $this->datatables->add_column('action', '<a href='.base_url() . 'admin/user_wallet/edit_topup_transaction/$1 title="Accept or Reject Topup" class="change_request_btn" ref="$1"><i  class="glyphicon glyphicon-pencil"></i> </a>', 'id');
    
        if($input['from_date']) {
           $this->datatables->where("DATE_FORMAT(wt.transaction_date, '%Y-%m-%d')>=", date('Y-m-d', strtotime($input['from_date'])));
        }
        if($input['till_date']) {
           $this->datatables->where("DATE_FORMAT(wt.transaction_date, '%Y-%m-%d')<=", date('Y-m-d', strtotime($input['till_date'])));
        }
        if($input['select_agent']) {
           $this->datatables->where('wt.user_id', $input['select_agent']);
        }
        $this->db->order_by('wt.id','desc');
        if ($response_type != '') {
            $data = $this->datatables->generate($response_type);
        } else {
            $data = $this->datatables->generate();
        }
        return $data;
    }
    
    /**
     * [index description] : Use for edit distributor topup request(Accept/Reject process)
     */
    public function distributor_fund_transfer() {
        $input = $this->input->post();
        $config =   array(
                        array('field' => 'status', 'lable'=>'Status','rules' => 'trim|required', 'errors' =>array('required' => 'Please select Status ')),
                        );
        
        if(form_validate_rules($config)) {
            $data = array();
            $request_id = $input['request_id'];
            $status = $input['status'];
            $amount = $input['amount'];
            //here fetching the wallet amount of distributor
            $distributor_wallet = $this->wallet_model->where(array('user_id'=>$this->session->userdata('user_id'),'status' => 'Y'))->find_all();
            //here checking the request  status in wallet top-up 
            $wallet_topup = $this->wallet_topup_model->column('id,user_id,is_approvel')->where(array('id'=>$request_id))->find_all();
            
            $actual_wallet_balance = $distributor_wallet[0]->actual_wallet_balance;
            $update_actual_wallet = ($actual_wallet_balance) - ($amount); 
            $wallet_current_amount = $distributor_wallet[0]->amt;
            $amt_after_trans     = $distributor_wallet[0]->amt - $amount;
            $virtual_amount = $distributor_wallet[0]->virtual_balance;
            $actual_current_outstanding = $distributor_wallet[0]->current_outstanding;
            $wallet_id = $distributor_wallet[0]->id;
            if($actual_wallet_balance >= $amount && $wallet_topup[0]->is_approvel == 'NA') {
                if($status == 'A') {
                    $update_wallet_topup = array('is_approvel' => 'A',
                                                'app_rej_date' => date("Y-m-d H:i:s"),
                                                'app_rej_by' => $this->session->userdata('user_id'),
                                                'updated_by' => $this->session->userdata('user_id'),
                                                'updated_date' => date("Y-m-d H:i:s")
                                                );
                    $update_where = array('id'=>$request_id); 
                    $update_topup_data = $this->wallet_topup_model->update_where($update_where,'' ,$update_wallet_topup);
                    if($update_topup_data > 0) {
                        $wallet_trans_debit =  array ("w_id" => $wallet_id,
                                                      "actual_wallet_amount" => $amount,
                                                      "virtual_amount"=> '0',
                                                      "amt" => $amount,
                                                      "outstanding_amount"=> $actual_current_outstanding,
                                                      "comment" => "Subagent topup transfer",
                                                      "status" => "Subagent topup",
                                                      "user_id" => $this->session->userdata('user_id'),
                                                      "added_by" => $this->session->userdata('user_id'),
                                                      "added_on" => date("Y-m-d H:i:s"),
                                                      "wallet_amt_before_trans"=> $actual_wallet_balance,
                                                      "wallet_amt_after_trans"=> $update_actual_wallet,
                                                      "virtual_amt_before_trans"=> $virtual_amount,
                                                      "virtual_amt_after_trans"=> $virtual_amount,
                                                      "amt_before_trans" => $wallet_current_amount,
                                                      "amt_after_trans" => $amt_after_trans,
                                                      "transaction_type_id" => '13',
                                                      "wallet_type" => 'actual_wallet',
                                                      "transaction_type" => 'Debited',
                                                      "is_status" => 'Y'
                                                    );
                        $insert_wallet_trans = $this->wallet_trans_model->insert($wallet_trans_debit);
                    if($insert_wallet_trans > 0){
                              $wallet_trans_log = [
                                                    'user_id' => $this->session->userdata("user_id"),
                                                    'wallet_id' => $wallet_id,
                                                    'wallet_trans_id' => $insert_wallet_trans,
                                                    'wallet_type' => 'actual_wallet',
                                                    'amt' => $amount,
                                                    'comment'=>'Subagent topup transfer',
                                                    'amt_before_trans'=>$wallet_current_amount,
                                                    'amt_after_trans'=>$update_actual_wallet,
                                                    'outstanding_amount' =>'0',
                                                    "transaction_type_id" => '13',
                                                    "transaction_type" => 'Debited',
                                                    'added_on' => date('Y-m-d H:i:s'),
                                                    'added_by'=> $this->session->userdata("user_id")
                                                  ];

                            $wallet_trans = $this->wallet_trans_log_model->insert($wallet_trans_log); 
                        }
                        else{
                        $this->session->set_flashdata('msg_type', 'error');
                        $this->session->set_flashdata('msg', 'Please contact to coustomer support wallet trans log entry not done.');
                        redirect(base_url()."admin/user_wallet/user_topup");
                        }
                    if($wallet_trans > 0){
                        $update_wallet = array(
                                                'amt' => $update_actual_wallet,
                                                'actual_wallet_balance' => $update_actual_wallet,
                                                'virtual_balance' => $virtual_amount,
                                                'current_outstanding' => $actual_current_outstanding,
                                                'updated_by'=> $this->session->userdata('user_id'),
                                                'updated_date'=> date('Y-m-d h:i:s'),
                                                'last_transction_id' => $insert_wallet_trans,
                                               );
                        $update_wallet_amt = $this->wallet_model->update_where('id', $wallet_id ,$update_wallet);
                    }
                    else{
                        $this->session->set_flashdata('msg_type', 'error');
                        $this->session->set_flashdata('msg', 'Please contact to coustomer support wallet transaction entry not done.');
                        redirect(base_url()."admin/user_wallet/user_topup");
                        }
                    }
                    else{
                        $this->session->set_flashdata('msg_type', 'error');
                        $this->session->set_flashdata('msg', 'Please contact to coustomer support wallet topup not updated');
                        redirect(base_url()."admin/user_wallet/user_topup");
                        }
                        
                        //here adding the balance in subagent wallet balance
                        $subagent_id = $wallet_topup[0]->user_id;
                        //here fetching the wallet amount of Subagent
                        $subagent_wallet = $this->wallet_model->where(array('user_id'=>$subagent_id,'status' => 'Y'))->find_all();
                        $actual_wallet_balance_subagent = $subagent_wallet[0]->actual_wallet_balance;
                        $update_actual_wallet_subagent = $actual_wallet_balance_subagent + $amount; 
                        $wallet_current_amount_subagent = $subagent_wallet[0]->amt;
                        $amt_after_trans_subagent = $subagent_wallet[0]->amt + $amount;
                        $virtual_amount_subagent = $subagent_wallet[0]->virtual_balance;
                        $actual_current_outstanding_subagent = $subagent_wallet[0]->current_outstanding;
                        $wallet_id_subagent = $subagent_wallet[0]->id;
                        
                        //entry in wallet trans table first
                        $wallet_trans_credit =  array ("w_id" => $wallet_id_subagent,
                                                  "actual_wallet_amount" => $amount,
                                                  "virtual_amount"=> '0',
                                                  "amt" => $amount,
                                                  "outstanding_amount"=>$actual_current_outstanding_subagent,
                                                  "comment" => "Distributer topup credited",
                                                  "status" => "Topup credited",
                                                  "user_id" => $subagent_id,
                                                  "added_by" => $this->session->userdata('user_id'),
                                                  "added_on" => date("Y-m-d H:i:s"),
                                                  "wallet_amt_before_trans"=> $actual_wallet_balance_subagent,
                                                  "wallet_amt_after_trans"=> $update_actual_wallet_subagent,
                                                  "virtual_amt_before_trans"=> $virtual_amount_subagent,
                                                  "virtual_amt_after_trans"=> $virtual_amount_subagent,
                                                  "amt_before_trans" => $wallet_current_amount_subagent,
                                                  "amt_after_trans" => $amt_after_trans_subagent,
                                                  "transaction_type_id" => '1',
                                                  "wallet_type" => 'actual_wallet',
                                                  "transaction_type" => 'Credited',
                                                  "is_status" => 'Y'
                                                );
                    $insert_wallet_trans_subagent = $this->wallet_trans_model->insert($wallet_trans_credit);
                    if($insert_wallet_trans_subagent > 0) {
                        $wallet_trans_log = [
                                                'user_id' => $subagent_id,
                                                'wallet_id' => $wallet_id_subagent,
                                                'wallet_trans_id' => $insert_wallet_trans_subagent,
                                                'wallet_type' => 'actual_wallet',
                                                'amt' => $amount,
                                                'comment'=>'Topup credited',
                                                'amt_before_trans'=>$actual_wallet_balance_subagent,
                                                'amt_after_trans'=>$update_actual_wallet_subagent,
                                                'outstanding_amount' =>$actual_current_outstanding_subagent,
                                                "transaction_type_id" => '1',
                                                "transaction_type" => 'Credited',
                                                'added_on' => date('Y-m-d H:i:s'),
                                                'added_by'=> $this->session->userdata("user_id")
                                            ];

                        $insert_wallet_log_subagent =  $this->wallet_trans_log_model->insert($wallet_trans_log);
                    }
                    else{
                        $this->session->set_flashdata('msg_type', 'error');
                        $this->session->set_flashdata('msg', 'Please contact to coustomer support entry not done in subagent wallet.');
                        redirect(base_url()."admin/user_wallet/user_topup");
                        }
                    if($insert_wallet_log_subagent) {
                        $update_subagent_wallet = array(
                                                        'amt' => $update_actual_wallet_subagent,
                                                        'actual_wallet_balance' => $update_actual_wallet_subagent,
                                                        'virtual_balance' => $virtual_amount_subagent,
                                                        'current_outstanding' => $actual_current_outstanding_subagent,
                                                        'updated_by'=> $this->session->userdata('user_id'),
                                                        'updated_date'=> date('Y-m-d h:i:s'),
                                                        'last_transction_id' => $insert_wallet_trans_subagent,
                                                     );
                        $update_subagent_wallet_amt = $this->wallet_model->update_where('id', $wallet_id_subagent ,$update_subagent_wallet);
                        $this->session->set_flashdata('msg_type', 'success');
                        $this->session->set_flashdata('msg', 'Fund transfer successful');
                        redirect(base_url()."admin/user_wallet/user_topup");
                    }
                    else{
                        $this->session->set_flashdata('msg_type', 'error');
                        $this->session->set_flashdata('msg', 'Please contact to coustomer support wallet transaction entry not done in subagent wallet.');
                        redirect(base_url()."admin/user_wallet/user_topup");
                        }
                }
                elseif($status == 'R') {
                    if($wallet_topup[0]->is_approvel == 'NA') {
                        $update_wallet_topup = array('is_approvel' => 'R',
                                                    'reject_reason'=>$input['comment'],
                                                    'app_rej_date' => date("Y-m-d H:i:s"),
                                                    'app_rej_by' => $this->session->userdata('user_id'),
                                                    'updated_by' => $this->session->userdata('user_id'),
                                                    'updated_date' => date("Y-m-d H:i:s")
                                                    );
                        $update_where = array('id'=>$request_id); 
                        $update_topup_data = $this->wallet_topup_model->update_where($update_where,'' ,$update_wallet_topup);
                        $this->session->set_flashdata('msg_type', 'success');
                        $this->session->set_flashdata('msg', 'Fund transfer request rejected');
                        redirect(base_url()."admin/user_wallet/user_topup");
                    }
                    else{
                        $this->session->set_flashdata('msg_type', 'error');
                        $this->session->set_flashdata('msg', 'Already requested');
                        redirect(base_url()."admin/user_wallet/user_topup");
                        }
                }
                else{
                        $this->session->set_flashdata('msg_type', 'error');
                        $this->session->set_flashdata('msg', 'Something went wrong please check and try again.');
                        redirect(base_url()."admin/user_wallet/user_topup");
                }
            }
            else{
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', "You don't have sufficient wallet amount");
                redirect(base_url()."admin/user_wallet/user_topup");
                }
        }
        else{
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', 'Please select status accept or reject');
                redirect(base_url()."admin/user_wallet/user_topup");
                }
    }
    /**
     * [index description] : Use for get distributor subagents topup detail
     */
	public function topup_request_history_suagent() {
        $input = $this->input->post();
        $data = $this->topup_request_history_detail_suagent($input,'json');
        echo $data;     
    }

    /**
     * [index description] : Use for show distributor subagents topup detail in excel
     */
    public function topup_request_history_subagent_excel() {
        $input = $this->input->get();
        $result = $this->topup_request_history_detail_suagent($input,'array');
        $sr_no = 1;
        if(!empty($result)) {
            foreach ($result['aaData'] as $key => $value) {
                $data[] = array('Sr no' => $sr_no,
                                'Agent Name' => $value['agent_name'],
                                'Agent Code' => $value['id'],
                                'Mobile Number' => $value['mobile_no'],
                                'Payment Mode' => $value['topup_by'],
                                'Amount' => $value['amount'],
                                'Transaction Type' => $value['topup_by'],
                                'Bank Name' => $value['bank_name'],
                                'Transaction Ref No' => $value['transaction_no'],
                                'Remark' => $value['transaction_remark'],
                                'Transaction Date' => $value['transaction_date'],
                                'Requested Date' => $value['requested_date'],
                                'Response Date' => $value['app_rej_date'],
                                'Reject Reason' => $value['reject_reason'], 
                                'Status' => $value['is_approvel_status']
                               );
                $sr_no++;
            }
        }
        else {
            $data = array(array());
        }
        $this->excel->data_2_excel('topup_request_history_subagent_excel' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
        die;
    }
}
