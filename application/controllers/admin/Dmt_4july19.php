<?php

//Created by pooja kambali  on 29-09-2018 
class Dmt extends CI_Controller {

    public $sessionData;
    
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('Pdf');
        $this->load->helper('common_helper');
        $this->load->model('Dmt_model');
        is_logged_in();
        $this->sessionData["id"] = $this->session->userdata('user_id');
        $this->sessionData["username"] = $this->session->userdata('username');
        $this->sessionData["password"] = $this->session->userdata('password');
        
         $this->header = array(
            'Content-Type: multipart/form-data; charset=utf-8',
            'X-API-KEY: ' . X_API_KEY . '',
        );
         

    }

    
    
    public function index() {
        $user_data = $this->session->userdata();
        $agent_id = $user_data['user_id'];
        $retailer_services = $this->retailer_service_mapping_model->getServiceByAgentId($agent_id);
        $service_array = array_column($retailer_services, 'service_id');
        if (in_array(Dmt, $service_array)) {
            load_back_view(DMT_VIEW);
        } else {
            $this->session->set_flashdata('msg', 'No access to module');
            redirect(base_url() . 'admin/dashboard');
        }
    }

    public function neft() {       
        $data = array();
        $input = $this->input->post();
   
        if (!empty($input)) {  
            $input['user_id'] = $this->sessionData['id'];   
          
            $curl_url = base_url() . "rest_server/Dmt/neft";
            $config = array(
               array('field' => 'type', 'label' => 'Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select  type.')),
               array('field' => 'CustomerMobileNo', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|xss_clean|min_length[10]', 'errors' => array('required' => 'Please enter mobile number.')),
               array('field' => 'BeneIFSCCode', 'label' => 'IFSC Code', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter ifsc code.')),
               array('field' => 'BeneAccountNo', 'label' => 'Beneficiary Account Number.', 'rules' => 'trim|required|numeric|max_length[19]|min_length[8]|xss_clean', 'errors' => array('required' => 'Please enter Beneficiary Account number.')),
               array('field' => 'BeneName', 'label' => 'Beneficiary Name.', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Beneficiary Name.')),
               array('field' => 'Amount', 'label' => 'Amount', 'rules' => 'trim|required|numeric|less_than_equal_to['.MAX_DMT_AMT.']|xss_clean', 'errors' => array('required' => 'Please enter Amount.')),
               array('field' => 'CustomerName', 'label' => 'Customer Name.', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Customer Name.')),
               array('field' => 'rfu1', 'label' => 'Remarks', 'rules' => 'trim|xss_clean', 'errors' => array('required' => 'Please enter Remark')),
               array('field' => 'transfer_from', 'label' => 'transfer_from', 'rules' => 'trim|xss_clean', 'errors' => array('required' => 'Please enter Remark')),
               array('field' => 'customer_charge', 'label' => 'Customer Charge', 'rules' => 'trim|required|numeric|xss_clean|is_natural_no_zero', 'errors' => array('required' => 'Please Provide Service Fee.','is_natural_no_zero'=>'Service Fee must be greater than zero.'))
            );  

          if (form_validate_rules($config) === FALSE) {
                $data['error'] = $this->form_validation->error_array();
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dmt.log',$data['error'], 'Form validation error');
                load_back_view(DMT_VIEW,$data);               
            } else { 
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dmt.log', $input, 'Input-Operator1');
                $curlResult = curlForPostData_dmt_new($curl_url, $input, $this->sessionData, $this->header);
                
                $output = json_decode($curlResult);

                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dmt.log', $output, 'Web DMT Response2');
             // if(!empty($output)) { 
                  $result = json_decode($output->data);
//                    $this->session->set_tempdata('msg', $result->DisplayMessage, 3);
                  if(isset($output->data) && !empty($output->data)){
                         $this->session->set_tempdata('msg', $result->DisplayMessage, 3);
                          if($result->DisplayMessage=="Transaction Successful") {
                              log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dmt.log', $result, 'success3');
                            $this->export_pdf($input);
                          }
                  }else if(isset($output->status) && $output->status=='failed'){
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dmt.log', $output, 'error4');
                        if(isset($output->error->customer_charge)) {
                              $this->session->set_tempdata('msg', $output->error->customer_charge);
                        }else if(isset($output->error->CustomerName)) {
                            $this->session->set_tempdata('msg', $output->error->CustomerName);
                        }else if(isset($output->error->BeneName)){
                            $this->session->set_tempdata('msg', $output->error->BeneName);
                        }
                }else{
                        $this->session->set_tempdata('msg', $output->msg);
                }
                redirect(site_url() . '/admin/dmt');

               /* }else {
                    $this->session->set_flashdata('msg_type', 'success');
                    $this->session->set_flashdata('msg', 'Something went wrong');
                    redirect(site_url() . '/admin/dmt');
                }*/
             }            
           
        }
      // redirect(base_url() . 'admin/Dmt');
   
    }
    

    public function transaction_list(){
      load_back_view(TRANSACTION_LIST);             
        }
        
        public function transaction(){
        $data = $this->input->post();
       // print_r($data);die();
        $this->datatables->select("'1',concat(u.first_name,' ',u.last_name) as agent_name,u.mobile_no,d.Clientuniqueid,d.type,d.CustomerMobileNo,d.BeneIFSCCode,d.BeneAccountNo,d.BeneName,d.Amount,d.CustomerName,d.created_on,d.message");
        $this->datatables->from('dmt_transfer_detail d');
        $this->datatables->join('users u','u.id=d.user_id','inner');
        
        
         if (isset($data['from_date']) && $data['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(d.created_on, '%Y-%m-%d') >=", date('Y-m-d', strtotime($data['from_date'])));
        }

        if (isset($data['to_date']) && $data['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(d.created_on, '%Y-%m-%d')<=", date('Y-m-d', strtotime($data['to_date'])));
        }

        $agent_user_id = $this->session->userdata("user_id");
        $user_role_id  = $this->session->userdata("role_id");

        if(!empty($agent_user_id) && $user_role_id==RETAILER_ROLE_ID){
          $this->datatables->where("user_id",$agent_user_id);
        }

        $data = $this->datatables->generate('json');
        $newdata_array = json_decode($data);
                if (!empty($newdata_array->data)) {
                    foreach ($newdata_array->data as &$detail) {
                        $detail['2'] = rokad_decrypt($detail['2'], $this->config->item('pos_encryption_key'));
                    }
                }
        $new_data= json_encode($newdata_array);
        echo $new_data;
       
        }
        
      public function transaction_status(){
        $data = array();
        $input = $this->input->post();
           
        if (!empty($input)) {
            
          $curl_url = base_url() . "rest_server/Dmt/transaction_status";

            $config = array(
               array('field' => 'ClientUniqueID', 'label' => 'ClientUniqueID', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Client Number.')),
             
                 );
             if (form_validate_rules($config)){
        
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dmt.log', $input, 'Input-Operator');
                $curlResult = curlForPostData_dmt_new($curl_url, $input, $this->sessionData, $this->header);
                $output = json_decode($curlResult);
                $this->session->set_flashdata('message', $output->msg);
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dmt.log', $curlResult, 'Web Recharge Response');
               echo $curlResult;
           exit;
             }
             
        }
      // redirect(base_url() . 'admin/Dmt');
     load_back_view(TRANSACTION_STATUS);             
    }


    public function get_cc_from_slab(){
      $input = $this->input->post();
      $customer_charge = $this->Dmt_model->getCustomerCharge($input['amount']);
      
      $customer_charge = !empty($customer_charge) ? $customer_charge : 0;

      echo $customer_charge;
    }
    
     public function export_pdf($input,$acct='') {
      $user_id =$input['user_id'];
      if($acct==1){
          $trasaction_details = $this->Dmt_model->getTransactionReceiptForAcctVerify($user_id);
          $acct=1;
          $pdf_gen    = $this->generatePdf($trasaction_details,$acct);
      } else {
        $trasaction_details = $this->Dmt_model->getTransactionReceipt($user_id);
        $pdf_gen    = $this->generatePdf($trasaction_details);
      }  
    }
    
    public function generatePdf($result,$acct=''){
    
    $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $title = "Domestic Money Transfer Receipt";
    $obj_pdf->SetTitle($title);
    
    //$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetDefaultMonospacedFont('helvetica');
    $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $obj_pdf->SetFont('helvetica', '', 9);
    $obj_pdf->setFontSubsetting(false);
    $obj_pdf->AddPage();
    ob_start();
    if($acct==1) {
      $final_amt = $result->Amount + $result->account_verification_charge;
      $service_name = 'Account Verification For Domestic Money Transfer';
      $customer_charge = $result->account_verification_charge;
      $service_fee = 'Account Verification Charge';
    }else {
      $final_amt = $result->Amount + $result->customer_charge;
      $service_name = 'Domestic Money Transfer';
      $customer_charge = $result->customer_charge;
      $service_fee = 'Service Fee';
    }
    
    $a_mail_replacement_array = array(
                                "{{home_url}}" => base_url().'admin/dmt',
                                '{{service_name}}' => $service_name,
                                "{{customer_name}}" => $result->CustomerName,
                                "{{customer_mobile_no}}" => $result->CustomerMobileNo,
                                "{{transfer_type}}" => $result->type,
                                "{{ifsc_code}}" => $result->BeneIFSCCode,
                                "{{account_no}}" => $result->BeneAccountNo,
                                "{{paid_by}}" => 'Wallet',
                                "{{amount}}" => $result->Amount,
                                "{{customer_charge}}" => $customer_charge,
                                "{{amount_in_words}}" => ucwords(getIndianCurrency($final_amt)),
                                "{{trans_no}}" => $result->transaction_no,
                                "{{final_amt}}" => $final_amt,
                                "{{agent_name}}" => $result->agent_name,
                                "{{agent_code}}" => $result->agent_code,
                                "{{service_fee}}" => $service_fee
                                );
       
    $content = $data['agent_mail_content'] = str_replace(array_keys($a_mail_replacement_array),array_values($a_mail_replacement_array),DMT_RECEIPT);
   
       // show($content,1);     
    ob_end_clean();
    $obj_pdf->writeHTML($content, true, false, true, false, '');
    $obj_pdf->Output('DMT Receipt.pdf', 'I');
    }

    /*Account verification */
    public function load_account(){
      load_back_view(DMT_ACCOUNT_VERIFICATION_VIEW); 
    }

    public function acct_verify_post() {       
        $data = array();
        $input = $this->input->post();
        //echo"sdf"; print_r($input); die;
        if (!empty($input)) {  
            $input['user_id'] = $this->sessionData['id'];
            $curl_url = base_url() . "rest_server/Dmt/acct_verify";
            $config = array(
               array('field' => 'type', 'label' => 'Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select  type.')),
               array('field' => 'CustomerMobileNo', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|xss_clean|min_length[10]', 'errors' => array('required' => 'Please enter mobile number.')),
               array('field' => 'BeneIFSCCode', 'label' => 'IFSC Code', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter ifsc code.')),
               array('field' => 'BeneAccountNo', 'label' => 'Beneficiary Account Number.', 'rules' => 'trim|required|numeric|max_length[19]|min_length[8]|xss_clean', 'errors' => array('required' => 'Please enter Beneficiary Account number.')),
               array('field' => 'BeneName', 'label' => 'Beneficiary Name.', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Beneficiary Name.')),
               array('field' => 'Amount', 'label' => 'Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter Amount.')),
               array('field' => 'CustomerName', 'label' => 'Customer Name.', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Customer Name.')),
               array('field' => 'rfu1', 'label' => 'Remarks', 'rules' => 'trim|xss_clean', 'errors' => array('required' => 'Please enter Remark')),
               array('field' => 'transfer_from', 'label' => 'transfer_from', 'rules' => 'trim|xss_clean', 'errors' => array('required' => 'Please enter Remark'))
                 );  

          if (form_validate_rules($config) == FALSE) {
                $data['error'] = $this->form_validation->error_array();
                load_back_view(DMT_ACCOUNT_VERIFICATION_VIEW,$data);               
            } else {
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dmt_acct.log', $input, 'ACCOUNT Verification Input-Operator');
                $curlResult = curlForPostData_dmt_new($curl_url, $input, $this->sessionData, $this->header);   
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dmt_acct.log', $curlResult, 'Web DMT Response');

                $output = json_decode($curlResult);
              // echo"test:"; print_r($output); die;
            
                $result = json_decode($output->data);

                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dmt_acct.log', $result, 'Web DMT Response message');
                //print_r($result); die;
                if(isset($output->data) && !empty($output->data)){
                       log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dmt_acct.log', $output, 'Error in resp1');
                  $this->session->set_tempdata('msg', $result->DisplayMessage, 3);
                  if($result->DisplayMessage=="Transaction Successful") {
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dmt_acct.log',$input, 'Account Verification Receipt generated');
                    $acct=1;
                    $this->export_pdf($input,$acct);
                  }
                }else if(isset($output->status) && $output->status == 'failed'){
                   log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dmt_acct.log', $output, 'Error in resp2');
                    if(isset($output->error->error->CustomerName)) {
                        $this->session->set_tempdata('msg', $output->error->CustomerName);
                    }else if(isset($output->error->error->BeneName)){
                        $this->session->set_tempdata('msg', $output->error->BeneName);
                    }else{
                        $this->session->set_tempdata('msg', $output->msg);
                    }
                }else{
                   log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dmt_acct.log', $output, 'Error in resp3');
                  $this->session->set_tempdata('msg', $output->msg);
                }
                redirect(site_url() . '/admin/dmt/load_account');
             }  
        } 
    }


    public function check_bene_acct_exists(){
      $input = $this->input->post();
      $exists='';
      $acct_exists = $this->Dmt_model->check_acct_exists($input['BeneAccountNo']);
      $data = array();
      if(count($acct_exists) >0 ){
      //  $data['acct_info'] = $acct_exists;
        //$data['exists']=1;
        $exists=1;        
      } else {
       // $data['exists'] =0;
       $exists=0;       
      }     
      echo json_encode($exists);
    }
    
     public function autofill_fields(){
      $input = $this->input->post();
      $acc_details_arr = array();
    
      $acc_details = $this->Dmt_model->getaccountDetails($input['BenAccountNo']);
     
        $id = $acc_details->id;
        $Clientuniqueid = $acc_details->Clientuniqueid;
        $CustomerMobileNo = $acc_details->CustomerMobileNo;
        $BeneIFSCCode = $acc_details->BeneIFSCCode;
        $BeneName = $acc_details->BeneName;
        $CustomerName = $acc_details->CustomerName;
        $RFU1 = $acc_details->RFU1;

        $acc_details_arr = array("id" => $id, "Clientuniqueid" => $Clientuniqueid,"CustomerMobileNo" => $CustomerMobileNo, "BeneIFSCCode" =>$BeneIFSCCode, 
            "BeneName" =>$BeneName, "CustomerName" =>$CustomerName, 'comments'=>$RFU1);

        // encoding array to json format
        echo json_encode($acc_details_arr);

    }
    
    public function dmt_reports_excel(){
        $input = $this->input->get();
        
            $result = $this->transaction();
            show($result,1);
            $data = array();
            $sr_no = 1;
            foreach ($result['aaData'] as $key => $value) {
                
                $data[] = array('Sr. No.' => $sr_no,
                    'Agent Code'=> $value['agent_code'],    
                    'Agent Name'=> $value['agent_name'],
                    'Depot Code'=> $value['DEPOT_CD'],                    
                    'Registration Date' => $value['reg_date'],                    
                    'Name' => $value['name_on_card'],
                    'Age' => $value['date_of_birth'],
                    'Card Id' => $value['cust_card_id'],                    
                    'Issuance Date' => $value['dispatch_date'],                    
                    'Expiry Date' => $value['exp_date'],
                    'Smartcard Fee' => $value['smart_card_fee'],
                    'Application Fee' => $value['application_fee'],
                    'GST Fee' => $value['gst_fee'],
                    'Total Receive' => $value['total_amount']
                );
                $sr_no++;
            }
            //show($data,1);
            $this->excel->data_2_excel('concession_pass_reg_'.time().'.xls',$data);
            die();
        
    }
}
