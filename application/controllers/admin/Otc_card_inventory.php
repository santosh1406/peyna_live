<?php
class Otc_card_inventory extends CI_Controller{
   
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');  
        
        is_logged_in();
        $this->sessionData["id"] = $this->session->userdata('user_id');
        $this->sessionData["username"] = $this->session->userdata('username');
        $this->sessionData["password"] = $this->session->userdata('password');
        
          
        $this->load->helper('smartcard_helper');
        $this->apiCred = json_decode(SMART_CARD_CREDS, true);
        $header_value = $this->apiCred['X_API_KEY'];
        $this->header = array(
            'X-API-KEY:' . $header_value
        );

        $this->basicAuth = array(
            'username' => $this->apiCred['username'],
            'password' => $this->apiCred['password']
        ); 
        $this->load->model(array('otc_card_inventory_model'));
        
        $agent_code = $this->otc_card_inventory_model->getAgentCode($this->sessionData["id"]);          
        $depot_code = $this->otc_card_inventory_model->getDepotcode($this->sessionData["id"]);
        
        
        $this->user_id = itms_user_id;       
        $this->agent_id = $agent_code;
        $this->version_id = '1.2.2';
        $this->depot_cd = $depot_code ;
        

    }
    
    private function checkAccessToModule()
    {
        $user_data = $this->session->userdata();
        $agent_id = $user_data['user_id'];
        $retailer_services = $this->retailer_service_mapping_model->getServiceByAgentId($agent_id);
        $service_array = array_column($retailer_services, 'service_id');
        if (in_array(SMART_CARD_SERVICE, $service_array)) {
            
            return true;
            
        } else {
            $this->session->set_flashdata('msg', 'No access to module');
            redirect(base_url() . 'admin/dashboard');
        }
    }
    
    public function index() {
        $this->checkAccessToModule();
        load_back_view(OTC_CARD_INVENTORY_VIEW);
    }
    
    /*check wallet balance added by sonali */
        
    public function check_wallet_balance($total_amount){
        $request['apploginuser'] = $this->sessionData["id"];
        $request['amount'] = $total_amount;      
        
        $result = checkWalletTxn_Smartcard($request, $comments = "check wallet balance");
        if (!empty($result)) {
            return $result;
        }
    }
        

         
    
    public function generate_otc_card(){
        
        $request = $this->input->post();
        $total_amount = $request['cards'] * PER_CARD_AMOUNT ;
        $balance_response = $this->check_wallet_balance($total_amount);
        if($balance_response){
            echo $balance_response;
            return false;
        }
        $itms_request['apploginuser'] = $this->user_id;
        $itms_request['agent_id'] =  $this->agent_id ;
        $itms_request['version_id'] = $this->version_id ;
        $itms_request['card_req_cnt'] = $request['cards'];
        $itms_request['depot_cd'] = $this->depot_cd ;
        
        $otc_inventory_details = $this->otc_card_inventory_model->getOtcInvenoryTransaction($this->sessionData["id"]);        
        log_data('rest/smartcard/smart_card_otc_card_inventory_api_' . date('d-m-Y') . '.log', $otc_inventory_details, 'otc_inventory_details');
        
        $total_cards = $otc_inventory_details[0]['total_cards'] + $request['cards'];
        log_data('rest/smartcard/smart_card_otc_card_inventory_api_' . date('d-m-Y') . '.log', $otc_inventory_details, 'otc_inventory_details');
        $insert_data = array(
            'user_id'=> $this->sessionData["id"],
            'agent_code'=>$this->agent_id ,
            'total_cards'=> $request['cards'],
            'per_card_amt'=> PER_CARD_AMOUNT,
            'total_amt'=> $total_amount,
            'created_date' => date('Y-m-d h:i:s')
        );
		
	if($request['cards'] >= MIN_CARD_AMOUNT){
            $this->otc_card_inventory_model->insert_data('otc_invenory',$insert_data);
	}
        
        if(empty($otc_inventory_details)) {
            $insert_trans_data = array(
            'user_id'=> $this->sessionData["id"],
            'agent_code'=>$this->agent_id ,
            'total_cards'=> $request['cards'],
            'created_date' =>date('Y-m-d H:i:s')
            );
            if($request['cards'] >= MIN_CARD_AMOUNT){
                $this->otc_card_inventory_model->insert_data('otc_invenory_transaction',$insert_trans_data);
            }
        }else{
            $update_cards_data = array(
            'user_id'=> $this->sessionData["id"],
            'total_cards'=> $total_cards,
            'updated_date' =>date('Y-m-d H:i:s')
            );
            if($request['cards'] >= MIN_CARD_AMOUNT){
                $this->otc_card_inventory_model->UpdateCardsByUser($update_cards_data);
            }
        }
        
        log_data('rest/smartcard/smart_card_otc_card_inventory_api_' . date('d-m-Y') . '.log', $itms_request, 'Input Parameters');
        
        $URL = $this->apiCred['url'] . '/Common_api/generate_OTC_card';        
        
        $json_response = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
        $request['apploginuser'] = $this->sessionData["id"];
        $request['amount'] = $total_amount;
        updateWalletTxn($request,$comments='Otc Inventory');
        
        // update otc_inventory wallet_trans_no code start - by sonali
        $inventoryquery = "select id from otc_invenory where user_id = ".$request['apploginuser']." ORDER BY `id` DESC LIMIT 1 ";                          
        $getOtcInventoryId = $this->db->query($inventoryquery)->result_array();
        
        $walletquery = "select transaction_no from wallet_trans where user_id = ".$request['apploginuser']." ORDER BY `id` DESC LIMIT 1 ";
        $getTransactionNo = $this->db->query($walletquery)->result_array();

        $updateWalletTransNo = ['wallet_trans_no' => $getTransactionNo[0]['transaction_no']];
        $this->db->where('id',$getOtcInventoryId[0]['id']);
        $this->db->update("otc_invenory", $updateWalletTransNo);

        // update wallet_trans_no code end - by sonali
           
        //json_encode($this->response($json));
        log_data('rest/smartcard/smart_card_otc_card_inventory_api_' . date('d-m-Y') . '.log', $json_response, 'api response');
       
        echo $json_response;
        
    }
    
    public function Card_assign_data(){
        $this->checkAccessToModule();
        load_back_view(CARD_ASSIGNDATA_VIEW);
    }
    
    public function get_card_assignData() {
        $input = $this->input->post();

        if (!empty($input)) {
            $config = array(
                array('field' => 'courier_no', 'label' => 'courier_no', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Courier Number.')),
            );

            if (form_validate_rules($config) == FALSE) {
                $data['error'] = $this->form_validation->error_array();
                load_back_view(CARD_ASSIGNDATA_VIEW, $data);
            } else {
                $itms_request['apploginuser'] = $this->user_id;
                $itms_request['agent_id'] = $this->agent_id;
                $itms_request['version_id'] = $this->version_id;
                $itms_request['courier_no'] = $input['courier_no'];
                $itms_request['depot_cd'] = $this->depot_cd;
                
                $URL = $this->apiCred['url'] . '/Common_api/get_card_assignData';
                
                $json_response = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
                
                echo $json_response;
            }
        }
    }
    
    public function accept_otc_card(){
        $input = $this->input->post();
        $card_list = '['.$input['card_list'].']';
        //show($card_list,1);
        $itms_request['apploginuser'] = $this->user_id;
                $itms_request['agent_id'] = $this->agent_id;
                $itms_request['version_id'] = $this->version_id;
                $itms_request['trimax_card_str'] = $card_list;
                
               // show($itms_request,1);
                
                $URL = $this->apiCred['url'] . '/Common_api/accept_otc_card';
                
                $data = curlForPost($URL, $itms_request, $this->basicAuth, $this->header);
                $json = json_decode($data);
                //show($json, 1);
                //json_encode($this->response($json));
                log_data('rest/smartcard/smart_card_otc_card_inventory_api_' . date('d-m-Y') . '.log', $json, 'api');

                json_response($json);
    }


}

