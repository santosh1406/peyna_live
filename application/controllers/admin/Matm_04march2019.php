<?php
// created by sonali on 24-jan-2019
 use Blocktrail\CryptoJSAES\CryptoJSAES;
 require APPPATH . '/libraries/CryptoJSAES.php';
class Matm extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');  
        
        is_logged_in();
        $this->sessionData["id"] = $this->session->userdata('user_id');
        $this->sessionData["username"] = $this->session->userdata('username');
        $this->sessionData["password"] = $this->session->userdata('password');
        
         $this->header = array(
            'Content-Type: multipart/form-data; charset=utf-8',
            'X-API-KEY: ' . X_API_KEY . '',
        );  
         
        $this->load->model(array('matm_transaction_model','wallet_model','Utilities_model','retailer_service_mapping_model'));
        
        $agent_id = $this->sessionData["id"] ;
        $retailer_services = $this->retailer_service_mapping_model->getServiceByAgentId($agent_id);  
//        show($retailer_services);
//        die;
        $service_array = array_column($retailer_services, 'service_id');
        if(in_array(MICRO_ATM_SERVICE_ID, $service_array)){

            
        }else{
            /* Redirect to Dashboard if agent has not enabled the service. - Gopal */
            $this->session->set_flashdata('msg', 'No access to module');
            redirect(base_url().'admin/dashboard');
        }

    }
    
    public function cash_withdraw()
    {  
        $data['SERVICEID'] = MATM_CASH_WITHDRAW;
        load_back_view(CASH_WITHDRAW_VIEW,$data);        
    }
    
    public function transaction_process() {
        $data = array();
        $input = $this->input->post();
        
        if (!empty($input)) {
            $input['user_id'] = $this->sessionData['id'];
                
            $config = array(
            array('field' => 'ClientUniqueID', 'label' => 'ClientUniqueID', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Transaction Number.')),
            array('field' => 'Amount', 'label' => 'Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter Amount.')),
            );

            if (form_validate_rules($config) == FALSE) {
                $data['error'] = $this->form_validation->error_array();
                load_back_view(CASH_WITHDRAW_VIEW, $data);
            } else {
                $serive_id = trim($input['service_id']);
                if($serive_id == MATM_CASH_WITHDRAW){                   
                
                $chk_data = array(
                        'user_id' => $this->sessionData["id"],
                        'cash_withdraw' => 'MicroatmMicro_atm',
                        'withdraw_amount' => trim($input['Amount']),
                        'type' => 'Micro_atm'
                    );
                    $check_user_wallet = $this->Utilities_model->getUserWalletDetails($this->sessionData["id"]);
                    if ($check_user_wallet[0]['amt'] >= trim($input['Amount'])) {
                        
                        /* Check if commission available - */
                        $commissionResult = $this->Utilities_model->commission_from($this->sessionData["id"]);
                        //show($commissionResult,1);
                        $created_by = $commissionResult[0]['level_3'];
                        $check_if_commission_available = $this->checkCommission($chk_data,$created_by);
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'micro_atm_check_commssion_available.log', $check_if_commission_available, 'check_commssion_available');
                        //show($check_if_commission_available);
                        
                        if(count($check_if_commission_available) == 0){
                            $this->session->set_tempdata(array('status' => 'Failed', 'data' => $check_if_commission_available, 'msg' => 'Commission not set. Please contact your administrator.'), 200);
                            redirect(base_url()."admin/matm/cash_withdraw");
                            
                        }
                    /* Check if commission available - */
                    } 
                   else{
                       $this->session->set_tempdata(array('status' => 'Failed',  'msg' => 'Wallet has insufficient balance'), 200);
                       redirect(base_url()."admin/matm/cash_withdraw");
                   } 
                }
                //$header_key = "982b0d01-b262-4ece-a2a2-45be82212ba1";
                //$body_key = "8329fffc-b192-4c9b-80cf-ccbbdf48bd67";
                
                $header_key = MATM_HEADER_KEY;
                $body_key = MATM_BODY_KEY;

                $client_unique_id = $input['user_id'] . 'M' . time();
                
                $client_ref_id = $input['ClientUniqueID'];                
                
                $data['MerchantId'] = MATM_MERCHANT_ID; 
                $data['SERVICEID'] = trim($input['service_id']);                 
                $data['RETURNURL'] = base_url()."admin/matm/transaction_response";
                $data['Version'] = MATM_VERSION_NO;
                $data['Amount'] = trim($input['Amount']);
                $data['ClientRefID'] = trim($client_ref_id);
                
                $this->matm_transaction_model->insert_data('micro_atm_transaction_request',$data);                               
                
                $res = json_encode($data);
                $encr_res = $this->encrypt_matm($res, $body_key);                

                $credentials = $this->encrypt_matm('{"ClientId":"'.MATM_CLIENT_ID.'","AuthKey":"'.MATM_AUTH_KEY.'"}', $header_key);
                
                $header = array(
                    'encData' => $encr_res,
                    'authentication' => $credentials,
                );

                load_back_view(MATM_TRANSACTION_VIEW, $header);
            }
        }
    }
    
    public function transaction_response()
    {
        $matm_inserted_data = array();
        $input = $this->input->post();
        $data = array();
        
        $body_key = MATM_BODY_KEY;

        if(!empty($input)){
            $wallet_txn_no = substr(hexdec(uniqid()), 4, 12);
            $matm_response =  json_decode($input['Response']);
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'micro_atm.log', $matm_response, 'Cash withdraw Response');
            
            $client_response= $this->decrypt_matm($matm_response->ClientRes,$body_key);
            $client_decoded_response = json_decode($client_response) ;
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'micro_atm.log', $client_decoded_response, 'Decrypted Client Response');
            
            $matm_inserted_data = array(
                
                'ClientRefID' => $matm_response->ClientRefID,
                'TxnDisplayMsg'=> $matm_response->DisplayMessage,
                'TxnStatus' => $client_decoded_response->TxnStatus?$client_decoded_response->TxnStatus:$client_decoded_response->BalanceEnquiryStatus,
                'TxnAmt' =>$client_decoded_response->TxnAmt?$client_decoded_response->TxnAmt:'0',
                'RRN' =>$client_decoded_response->RRN?$client_decoded_response->RRN:'',
                'CardNumber' =>$client_decoded_response->CardNumber?$client_decoded_response->CardNumber:'',
                'AvailableBalance' =>$client_decoded_response->AvailableBalance?$client_decoded_response->AvailableBalance:'0',
                'TransactionDatetime' =>$client_decoded_response->TransactionDatetime?$client_decoded_response->TransactionDatetime:'0000-00-00 00:00:00',
                'AccountNo' =>$client_decoded_response->AccountNo?$client_decoded_response->AccountNo:'',
                'TerminalID' =>$client_decoded_response->TerminalID?$client_decoded_response->TerminalID:'',
                'WalletTransTxnNo'=> $wallet_txn_no,
            );
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'micro_atm.log', $matm_inserted_data, 'Inserted data');
          
            $this->matm_transaction_model->insert_data('micro_atm_transaction_response',$matm_inserted_data);
            
            if($matm_response->DisplayMessage == 'Card Transaction Successfully'){
                if(!empty($client_decoded_response->TxnAmt)){   
                    $data = array(
                        'user_id' => $this->sessionData["id"],
                        'cash_withdraw' => 'MicroatmMicro_atm',
                        'withdraw_amount' => $client_decoded_response->TxnAmt,
                        'type' => 'Micro_atm'
                    );
                    $commissionResult = $this->Utilities_model->commission_from($this->sessionData["id"]);
//                        show($commissionResult,1);
//                        die;
                    $created_by = $commissionResult[0]['level_3'];    
                    $this->serviceRetailerCommissionDistributionForRecharges($data, $wallet_txn_no,$created_by);
                    $this->updateWalletTxn($client_decoded_response->TxnAmt,$wallet_txn_no);
                    
                    
                }  
                $this->session->set_tempdata('msg', $matm_response->DisplayMessage, 3);
            }elseif($matm_response->DisplayMessage == 'Balance Enquiry Successful' ){                
                $this->session->set_tempdata('msg', $matm_response->DisplayMessage, 3);
            }
            elseif($matm_response->DisplayMessage == 'Success'){                
                $this->session->set_tempdata('msg', $matm_response->DisplayMessage, 3);
            }
            else{
            	$this->session->set_tempdata('msg', $matm_response->DisplayMessage, 3);
            }
            
            if($client_decoded_response->TxnStatus =='Card Transaction Successfully' || $client_decoded_response->TxnStatus == 'Failed')
            {
                redirect(base_url()."admin/matm/cash_withdraw");
            }
            elseif ($client_decoded_response->BalanceEnquiryStatus =='Balance Enquiry Successful' ) 
            {
                redirect(base_url()."admin/matm/balance_enquiry");
            }
            elseif($client_decoded_response->TxnStatus == 'Fail')
            {
                redirect(base_url()."admin/matm/balance_enquiry");
            }
            elseif ($client_decoded_response->TxnStatus == 'Micro ATM  transaction details not found .') 
            {
                redirect(base_url()."admin/matm/transaction_status");
            }
            elseif($matm_response->DisplayMessage == 'Success' )
            {
                redirect(base_url()."admin/matm/transaction_status");
            }
        }
        
    }    
    
    public function balance_enquiry()
    {
        $data['SERVICEID'] = MATM_BALANCE_ENQUIRY;
        load_back_view(BALANCE_ENQUIRY_VIEW,$data);  
    }
    
    public function transaction_status()
    {
        $data['SERVICEID'] = MATM_TRANSACTION_STATUS;
        load_back_view(MATM_TRANSACTION_STATUS_VIEW,$data);  
    }
    
    function encrypt_matm($text, $passphrase) {
        $encrypted = CryptoJSAES::encrypt($text, $passphrase);
        return $encrypted;
        //var_dump("Encrypted: ", $encrypted);  
    }
    
    function decrypt_matm($text, $passphrase) {
        $decrypted = CryptoJSAES::decrypt($text, $passphrase);
        return $decrypted;
        //var_dump("Decrypted: ", $decrypted);
    }
    
    function updateWalletTxn($amount,$transaction_no){
        
        $from_userid = $this->sessionData["id"];

        $transfer_amount = $amount;
        
        $fund_transfer_remark = "Micro ATM Txn";

        $from_wallet_details = $this->wallet_model->where('user_id',$from_userid)->find_all();
        
        
        $from_wallet_id = $from_wallet_details[0]->id;
        $wallet_amt = $from_wallet_details[0]->amt;
        
        //$transaction_no = substr(hexdec(uniqid()), 4, 12);
        
        
        if($wallet_amt>=$transfer_amount)
        {
            $wallet_trans_detail = [
          "w_id" => $from_wallet_id,
          "amt" => $transfer_amount,
          "merchant_amount" => '',
          "wallet_type" => "actual_wallet",
          "comment" => "Micro ATM Transaction",
          "status" => 'Debited',
          "user_id" => $from_userid,
          "amt_before_trans" => $from_wallet_details[0]->amt,
          "amt_after_trans" => $from_wallet_details[0]->amt - $transfer_amount,
          "remaining_bal_before" => 0,
          "remaining_bal_after" => 0,
          "cumulative_bal_before" => 0,
          "cumulative_bal_after" => 0,
          "amt_transfer_from" => $from_userid,
          "amt_transfer_to" => 0,
         // "transaction_type_id" => '11',   //need to check this value
          "transaction_type" => 'Debited',
          "is_status" => "Y",
          "added_on" => date("Y-m-d H:i:s"),
          //"added_by" => $this->session->userdata('user_id'),
          "added_by" => $from_userid,
          "transaction_no" => $transaction_no,
          "remark" => $fund_transfer_remark
        ];
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'micro_atm_wallet_Trans.log', $wallet_trans_detail, 'Inserted Data');
        $this->db->insert('wallet_trans',$wallet_trans_detail);
        $wallet_trans_id= $this->db->insert_id();
            
            $update_wallet = [
            "amt" => $wallet_amt - $transfer_amount,
            "last_transction_id" => $wallet_trans_id,
            "updated_by" => $from_userid,
            "updated_date" => date("Y-m-d H:i:s"),
        ];

        $this->db->where('user_id',$from_userid);
        return $update_from_wallet = $this->db->update("wallet", $update_wallet);
        }
        else
        {            
            $this->session->set_tempdata('msg', 'Wallet has limited amount', 3);
            
        }
    }
    
    function chk_client_ref_id(){
        
        $ClientUniqueID = $this->input->post('ClientUniqueID');
        $query = $this->db->get_where('micro_atm_transaction_request',array('ClientRefID' => $ClientUniqueID,'SERVICEID' => MATM_CASH_WITHDRAW  )); 
       // echo $this->db->last_query();
        $count = $query->num_rows();  
                    
        if ($count > 0) 
        {
            echo  'This is valid transaction no.';
        }else {
            echo  'This is not valid transaction no.';
        }
        

    }
    
    
    
    public function checkCommission($input,$created_by){
        if($input['recharge_type'] == 'Prepaid Mobile'){
            $code = strtolower($input['aggregator'].$input['mobile_operator'].'Prepaid');
            $service_id = PREPAID_SERVICE_ID;
        }elseif($input['recharge_type'] == 'Postpaid Mobile'){
            $code = strtolower($input['aggregator'].$input['mobile_operator'].'Postpaid');
            $service_id = POSTPAID_SERVICE_ID;
        }elseif($input['recharge_type'] == 'DTH'){
            $code = strtolower($input['aggregator'].$input['dth_operator']);
            $service_id = DTH_SERVICE_ID;
        }elseif($input['recharge_type'] == '0' || $input['recharge_type'] == '1'){
            $code = strtolower($input['type'].$operator);
            $service_id = DTH_SERVICE_ID;
        }elseif($input['recharge_type'] == 'Prepaid Datacard'){
               $code = strtolower($input['type'].$input['mobile_operator'].'Prepaid');
                $service_id = DATACARD_SERVICE_ID;
   	 }elseif($input['recharge_type'] == 'Postpaid Datacard'){
               $code = strtolower($input['type'].$input['mobile_operator'].'Postpaid');
                $service_id = DATACARD_SERVICE_ID;
    }elseif($input['cash_withdraw'] == 'MicroatmMicro_atm'){
                $code = strtolower($input['cash_withdraw']);
                $service_id = MICRO_ATM_SERVICE_ID;
            }

        $LcSqlStr = "SELECT * from services_commissions 
        where status = 'Y'";
        $LcSqlStr .= " AND `values` NOT LIKE '{\"Trimax\":\"0\",\"Rokad\":\"0\",\"MD\":\"0\",\"AD\":\"0\",\"Distributor\":\"0\",\"Retailer\":\"0\"}'";
        $LcSqlStr .= " AND `commission_name` LIKE '%" . str_replace(" ", "", $code) . "%'";
        $LcSqlStr .= " AND `created_by` = '". $created_by . "'";
        $LcSqlStr .= " AND service_id = '" . $service_id . "'"; 
        ;
       log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'micro_atm_commission_query.log', $LcSqlStr, 'Commission query');
        $query = $this->db->query($LcSqlStr);
        return $query->result();

    }

    public function serviceRetailerCommissionDistributionForRecharges($input = "", $trans_ref_no = "",$created_by='') {
       
        
        $user_id = $input['user_id'];
        if(in_array($input['type'], ['Micro_atm']) ){
             
            if($input['recharge_type'] == 'Prepaid Mobile'){
                $code = strtolower($input['type'].$input['mobile_operator'].'Prepaid');
                $service_id = PREPAID_SERVICE_ID;
            }elseif($input['recharge_type'] == 'Postpaid Mobile'){
                $code = strtolower($input['aggregator'].$input['mobile_operator'].'Postpaid');
                $service_id = POSTPAID_SERVICE_ID;
            }elseif($input['recharge_type'] == '0' || $input['recharge_type'] == '1'){
                $operator = getTsoDTHOperators($input['dth_operator']);
                // show($operator);
                $code = strtolower($input['type'].$operator);
                $service_id = DTH_SERVICE_ID;
            }elseif($input['recharge_type'] == 'DTH'){
                $operator = str_replace(' ', '', $input['dth_operator']);
                $code = strtolower($input['type'].$operator);
                $service_id = DTH_SERVICE_ID;
            }elseif($input['recharge_type'] == 'Prepaid Datacard'){
               $code = strtolower($input['type'].$input['mobile_operator'].'Prepaid');
                $service_id = DATACARD_SERVICE_ID;
            }elseif($input['recharge_type'] == 'Postpaid Datacard'){
               $code = strtolower($input['type'].$input['mobile_operator'].'Postpaid');
                $service_id = DATACARD_SERVICE_ID;
            }elseif($input['recharge_type'] == 'BOS_TICKET'){
               $code = strtolower($input['recharge_type']);
               $service_id = BOS_SERVICE_ID;
            }elseif($input['cash_withdraw'] == 'MicroatmMicro_atm'){
                $code = strtolower($input['cash_withdraw']);
                $service_id = MICRO_ATM_SERVICE_ID;
            }
            $LcSqlStr = "SELECT * from services_commissions 
            where commission_name like '%" . str_replace(" ", "", $code) . "%' 
            and service_id = '" . $service_id . "'"
            ;

            // show($LcSqlStr, 1);
            
            $query = $this->db->query($LcSqlStr);
            $new = $query->result();
//            show($new,1);
//            die;
            $sql ="select bos_caped_rupees,agent_caped_rupees from sub_services
                 where  service_id = '" . $service_id . "'";
            $squery = $this->db->query($sql);
            $agent_capped_value = $squery->result();
 //           show($agent_capped_value[0],1);
//            die;
            if($new){ 
                
                $row = $new[0];
                $getRetailerServicesCommission = $this->Utilities_model->getRetailerServicesCommissionForRecharges($user_id, $row->service_id, $row->sub_service_id, $created_by);
//                echo '<pre>';
//                print_r($getRetailerServicesCommission);
                //           show($getRetailerServicesCommission);
//                die;
            }
        }else{ 
            $service_id = MICRO_ATM_SERVICE_ID;

            $getRetailerServicesCommission = $this->Utilities_model->getRetailerServicesCommission($user_id, $service_id, $created_by);

        }
       //  show($getRetailerServicesCommission, 1);
        if ($getRetailerServicesCommission) {

            $agent_id = $getRetailerServicesCommission[0]['user_id'];
            $agent_code = $getRetailerServicesCommission[0]['agent_code'];
            $distributor_id = $getRetailerServicesCommission[0]['level_5'];
            $Area_dis_id = $getRetailerServicesCommission[0]['level_4'];
            $master_dis_id = $getRetailerServicesCommission[0]['level_3'];
            $company_id = $getRetailerServicesCommission[0]['level_2'];
            $trimax_id = $getRetailerServicesCommission[0]['level_1'];
            $service_id = $getRetailerServicesCommission[0]['service_id'];
            $commission_id = $getRetailerServicesCommission[0]['commission_id'];
            $total_commission = $getRetailerServicesCommission[0]['total_commission'];

            $values[0] = json_decode($getRetailerServicesCommission[0]['values']);

            $trimax_commission = '0';
            $company_commission = '0';
            $md_commission = '0';
            $ad_commission = '0';
            $dist_commission = '0';
            $retailer_commission = '0';
            $with_gst = $without_gst = 0;
            if (!empty($values)) {

                $trimax_commission = round($values[0]->Trimax, 2);
                $company_commission = round($values[0]->Rokad, 2);
                $md_commission = round($values[0]->MD, 2);
                $ad_commission = round($values[0]->AD, 2);
                $dist_commission = round($values[0]->Distributor, 2);
                $retailer_commission = round($values[0]->Retailer, 2);
                
                
                $recharge_amount = $input['withdraw_amount']; 
                $commission_percent = !empty($total_commission) ? $total_commission : '0';                        
                $trimax_cal = calculateGstTds($recharge_amount, $commission_percent);
                $earnings = $trimax_cal['earnings']; //trimax_earnings
                
                $gst = $trimax_cal['gst']; //trimax_gst                            
                $tds = $trimax_cal['tds']; //trimax_tds
                $final_amount = $trimax_cal['final_amount']; //trimax_final_amt                                                        

                $rokad_trimax_cal = calculateGstTds($earnings, $trimax_commission);
                
                $rokad_trimax_actual_amt = $rokad_trimax_cal['earnings']; //rokad_trimax_amt
               
                if($rokad_trimax_actual_amt > $agent_capped_value[0]->bos_caped_rupees)
                {
                    $rokad_trimax_amt = $agent_capped_value[0]->bos_caped_rupees;
                } 
                else {
                    $rokad_trimax_amt =  $rokad_trimax_actual_amt;
                }   
              
                $with_gst = $with_gst + $rokad_trimax_cal['earnings'];
                $rokad_trimax_cal_with_tds_gst = calculateGstTds($final_amount, $trimax_commission);
                $rokad_trimax_amt_with_tds_gst = $rokad_trimax_cal_with_tds_gst['earnings']; //rokad_trimax_amt_with_tds_gst
                $without_gst = $without_gst + $rokad_trimax_amt_with_tds_gst;        

                $company_cal = calculateGstTds($earnings, $company_commission);
                $company_actual_amt = $company_cal['earnings'];//company gst
                if($company_actual_amt> $agent_capped_value[0]->agent_caped_rupees)
                {
                    $company_amt = $agent_capped_value[0]->agent_caped_rupees;
                }else
                {
                    $company_amt = $company_actual_amt;
                }
                 
                $with_gst = $with_gst + $company_cal['earnings'];
                
                $company_cal_with_tds_gst = calculateGstTds($final_amount, $company_commission);
                $company_amt_with_tds_gst = $company_cal_with_tds_gst['earnings']; //company_amt_with_tds_gst                    
                $without_gst = $without_gst + $company_amt_with_tds_gst;    

                $rd_cal = calculateGstTds($earnings, $md_commission);
                $rd_actual_amt = $rd_cal['earnings']; //rd_amt  
                if($rd_actual_amt> $agent_capped_value[0]->agent_caped_rupees)
                {
                    $rd_amt = $agent_capped_value[0]->agent_caped_rupees;
                }else
                {
                    $rd_amt = $rd_actual_amt;
                }
                $with_gst = $with_gst + $rd_cal['earnings'];  
               
                $rd_cal_with_tds_gst = calculateGstTds($final_amount, $md_commission);
                $rd_amt_with_tds_gst = $rd_cal_with_tds_gst['earnings']; //rd_amt_with_tds_gst
                $without_gst = $without_gst + $rd_amt_with_tds_gst;

                $dd_cal = calculateGstTds($earnings, $ad_commission);
                $dd_actual_amt = $dd_cal['earnings']; //dd_amt   
                if($dd_actual_amt> $agent_capped_value[0]->agent_caped_rupees)
                {
                    $dd_amt = $agent_capped_value[0]->agent_caped_rupees;
                }else
                {
                    $dd_amt = $dd_actual_amt;
                }
                $with_gst = $with_gst + $dd_cal['earnings'];                                 
                $dd_cal_with_tds_gst = calculateGstTds($final_amount, $ad_commission);
                $dd_amt_with_tds_gst = $dd_cal_with_tds_gst['earnings']; //dd_amt_with_tds_gst
                $without_gst = $without_gst + $dd_amt_with_tds_gst;

                $ex_cal = calculateGstTds($earnings, $dist_commission);
                $ex_actual_amt = $ex_cal['earnings']; //ex_amt    
                if($ex_actual_amt> $agent_capped_value[0]->agent_caped_rupees)
                {
                    $ex_amt = $agent_capped_value[0]->agent_caped_rupees;
                }else
                {
                    $ex_amt = $ex_actual_amt;
                }
                $with_gst = $with_gst + $ex_cal['earnings'];              
                $ex_cal_with_tds_gst = calculateGstTds($final_amount, $dist_commission);
                $ex_amt_with_tds_gst = $ex_cal_with_tds_gst['earnings']; //ex_amt_with_tds_gst
                $without_gst = $without_gst + $ex_amt_with_tds_gst;


                $sa_cal = calculateGstTds($earnings, $retailer_commission);
                $sa_actual_amt = $sa_cal['earnings']; //sa_amt                 
                $agent_capped_value =  $agent_capped_value[0];
                
                if($sa_actual_amt> $agent_capped_value->agent_caped_rupees)
                {
                    $sa_amt = $agent_capped_value->agent_caped_rupees;
                }else
                {
                    $sa_amt = $sa_actual_amt;
                }
                $with_gst = $with_gst + $sa_cal['earnings'];   
                
                $sa_cal_with_tds_gst = calculateGstTds($final_amount, $retailer_commission);
                $sa_amt_with_tds_gst = $sa_cal_with_tds_gst['earnings']; //sa_amt_with_tds_gst 
                $without_gst = $without_gst + $sa_amt_with_tds_gst;

                $remaining_commission = round($with_gst, 2) - round($earnings, 2);
                
                /*for commission adjustment  - Gopal */             
            }

            $comm_distribution_array['user_id'] = $agent_id;
            $comm_distribution_array['service_id'] = $service_id;
            $comm_distribution_array['commission_id'] = $commission_id;
            $comm_distribution_array['transaction_no'] = $trans_ref_no;
            $comm_distribution_array['transaction_amt'] = $recharge_amount;
            $comm_distribution_array['service_comm_percent'] = SERVICE_COMMISSION;
            // $comm_distribution_array['trimax_comm_percent'] = $input['type'] == 'JRI' ? JRI_TRIMAX_COMMISSION :  CYBERPLAT_TRIMAX_COMMISSION;;
            $comm_distribution_array['trimax_comm_percent'] = $total_commission;
            $comm_distribution_array['gst_percentage'] = GST_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['tds_percentage'] = TDS_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['trimax_earnings'] = $earnings;
            $comm_distribution_array['trimax_gst'] = $gst;
            $comm_distribution_array['trimax_tds'] = $tds;
            $comm_distribution_array['trimax_final_amt'] = $final_amount;
            $comm_distribution_array['rokad_trimax_per'] = $values[0]->Trimax; 
            $comm_distribution_array['rokad_trimax_actual_amt'] = $rokad_trimax_actual_amt;
            $comm_distribution_array['rokad_trimax_amt'] = $rokad_trimax_amt;
            $comm_distribution_array['rokad_trimax_amt_with_tds_gst'] = $rokad_trimax_amt_with_tds_gst;
            $comm_distribution_array['company_per'] = $values[0]->Rokad;
            $comm_distribution_array['company_actual_amt'] = $company_actual_amt;
            $comm_distribution_array['company_amt'] = $company_amt;
            $comm_distribution_array['company_amt_with_tds_gst'] = $company_amt_with_tds_gst;
            $comm_distribution_array['rd_per'] = $values[0]->MD;            
            $comm_distribution_array['rd_actual_amt'] = $rd_actual_amt;
            $comm_distribution_array['rd_amt'] = $rd_amt;
            $comm_distribution_array['rd_amt_with_tds_gst'] = $rd_amt_with_tds_gst;
            $comm_distribution_array['dd_per'] = $values[0]->AD;
            $comm_distribution_array['dd_actual_amt'] = $dd_actual_amt;
            $comm_distribution_array['dd_amt'] = $dd_amt;
            $comm_distribution_array['dd_amt_with_tds_gst'] = $dd_amt_with_tds_gst;
            $comm_distribution_array['ex_per'] = $values[0]->Distributor;
            $comm_distribution_array['ex_actual_amt'] = $ex_actual_amt;
            $comm_distribution_array['ex_amt'] = $ex_amt;
            $comm_distribution_array['ex_amt_with_tds_gst'] = $ex_amt_with_tds_gst;
            $comm_distribution_array['sa_per'] = $values[0]->Retailer;
            $comm_distribution_array['sa_actual_amt'] = $sa_actual_amt;
            $comm_distribution_array['sa_amt'] = $sa_amt;            
            $comm_distribution_array['sa_amt_with_tds_gst'] = $sa_amt_with_tds_gst;
            $comm_distribution_array['status'] = 'Y';
            $comm_distribution_array['created_by'] = $user_id;
            $comm_distribution_array['commission_difference'] = $remaining_commission;
            $comm_distribution_array['created_date'] = date('Y-m-d H:i:s');

            $id = $this->Utilities_model->saveVasCommissionData($comm_distribution_array);
            // show($id, 1);
            // $comm_distribution_array['id'] = $id;
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'micro_atm_commission.log', $comm_distribution_array, 'Commission Distribution Response');

            return $comm_distribution_array;
        }
    }

}


