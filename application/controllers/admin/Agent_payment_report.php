<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Agent_payment_report extends MY_Controller {

    public function __construct() {
         parent::__construct();
        is_logged_in();
        $this->load->model(array('wallet_model','wallet_trans_model','users_model','agent_payment_report_model'));
        $this->load->library('form_validation');
        $this->load->library("Excel");
        $this->load->library('session');
    }
    /**
     * [index description] : Use for see user wallet detail
     */
     
    public function index(){
       $inputData  = $this->input->post();
        // $reconFromDate  = date('Y-m-d 00:00:00');
        // $reconToDate    = date('Y-m-d 23:59:59');
        // $reconDate      = date("d-m-Y");
        // $userType       = 7; // 7 - Sale Agent Role ID

        // if(isset($inputData[dtReconDate]) && $inputData[dtReconDate] != '')
        // {
        //     $reconFromDate  = date("Y-m-d 00:00:00", strtotime($inputData[dtReconDate]));
        //     $reconToDate    = date("Y-m-d 23:59:59", strtotime($inputData[dtReconDate]));
        //     $reconDate      = date("d-m-Y", strtotime($inputData[dtReconDate]));
        // }//if
        if( $inputData['from_date'] == '')
              {
                $inputData['from_date']  =  date('d-m-Y');               
              }
              if(!isset($inputData['to_date']) || $inputData['to_date'] == '')
              {
                
                $inputData['to_date']  = date('d-m-Y');
               
              }
       $data['regionData'] = $this->agent_payment_report_model->getRegionDetail();
       $data['input_data'] = $inputData;
     $data['report_data'] = $this->agent_payment_report_model->genAgentPaymentReport($inputData);
        load_back_view(AGENT_PAYMENT_REPORT, $data);
    }
   
    /**
     * [index description] : Use for see user wallet detail
     */
    
    /**
     * [index description] : Use for get user wallet detail excel
     */
    public function agent_payment_report_list_excel() {
        $input = $this->input->get();
        //$result_data = $this->activeUsersDetails_test($input, 'array');
        $result = $this->agent_payment_report_model->genAgentPaymentReport($input);
        $sr_no = 1;
        if(!empty($result)) { 
            
           foreach($result as $key=>$value) {
                 $data[] = array('Sr. No.' => $sr_no,
                    'Agent ID' => $value['agent_id'],
                    'Agent Code' => $value['agent_code'],
                    'Agent Name' => $value['f_name'].' '.$value['l_name'],
                    'Mobile No.' => rokad_decrypt($value['agent_username'], $this->config->item('pos_encryption_key')),
                    'Bank Name' => $value['bank_name'],
                    'Transaction ID' => $value['pg_tracking_id'],
                    'Amount' => round($value['amount'],2),
                    'Address' => $value['address'],
                    'Depo Name' => $value['depot'],
                    'Depot Code' => $value['depot_cd'],
                    'Div Code' => $value['division'],
                    'Region' => $value['region_name'],
                  //  'Terminal ID' => $value['terminal_code'],
                  //  'Terminal Generate Date' => $value['terminal_code_generate_date'],
                    'KYC Status' => $value['kyc_verify_status'],
                    'Email Status' => $value['email_verify_status'],
                    'Verify Status' => $value['verify_status'],
                    'Signup Date' => $value['signup_date']
                    );
                $sr_no++;
            }
        }
        else {
            $data = array(array());
        }
        $this->excel->data_2_excel('User_payemt_Report' . date('Y-m-d') . '_' . time() . '.xlsx', $data);
            die;
    }

    /**
     * [index description] : Use for show user/sub-agent topup request
     */ 
   
    
     
 
}
