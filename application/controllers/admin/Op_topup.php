<?php
/*
 * This function is created for insert the top up for operators
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Op_topup extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model(array(
                                'op_topup_model', 
                                'op_master_model', 
                                'common_model',
                                'op_master_details_model',
                                'op_trans_details_model',
                                'wallet_model',
                                'users_model',
                                'op_wallet_model',
                                'api_provider_model',
                                'cb_op_master_details_model'
                                )
                          );
       $this->load->library('cca_payment_gateway'); 
    }
     
     function index() {
        
        $data['api_provider'] = $this->api_provider_model->column('id, name')
                                        ->where(array('is_status'=>'1'))
                                        ->as_array()
                                        ->find_all(); 

        $data['operator'] = $this->cb_op_master_details_model->column('user_id as id, op_name as name')
                                ->where(array('is_status'=>'1'))
				                ->where(array('op_name'=>'msrtc'))
                                ->as_array()
                                ->find_all();
        
        $data['service_name']       = $this->op_topup_model->get_services();

        load_back_view(OPERATOR_TOPUP_VIEW,$data);
        
    }
    
    function list_topup_data () {
        $input = $this->input->post(NULL,true);
        
        if ($this->session->userdata('role_id') == MSRTC_USER_ROLE_ID) {
            $this->datatables->select('otd.op_trans_id,otd.created_date AS CD, otd.transaction_code TCD, otd.depositor_bank_name BANK, otd.transaction_amt TAMT, (otd.transaction_amt * otd.commission_rate/100) AS GC, (((otd.transaction_amt *otd.commission_rate/100) * otd.tds_rate /100)) AS TDS, (otd.transaction_amt + ((otd.transaction_amt * otd.commission_rate/100) - (((otd.transaction_amt *otd.commission_rate/100) * otd.tds_rate /100)))) AS NC, otd.cumulative_opening_balance,(SELECT cumulative_closing_balance FROM op_trans_details WHERE ref_op_trans_id = otd.op_trans_id) AS cb, IF((otd.is_deleted ="1" && otd.is_status = "0"), "Rejected",  IF((otd.is_deleted ="0" && otd.is_status < "1"), "Pending", "Approved")) AS status, IF((otd.is_deleted ="0" && otd.ref_op_trans_id) > "0"," ","glyphicon glyphicon-check") as checked, IF(otd.ref_op_trans_id > "0"," ","") as edit,IF(otd.is_deleted ="0" && otd.is_status = "0","glyphicon glyphicon-file"," ")  as reject', false);
        } 
        else {
            $this->datatables->select('otd.op_trans_id,otd.created_date AS CD, otd.transaction_code TCD, otd.depositor_bank_name BANK, otd.transaction_amt TAMT, (otd.transaction_amt * otd.commission_rate/100) AS GC, (((otd.transaction_amt *otd.commission_rate/100) * otd.tds_rate /100)) AS TDS, (otd.transaction_amt + ((otd.transaction_amt * otd.commission_rate/100) - (((otd.transaction_amt *otd.commission_rate/100) * otd.tds_rate /100)))) AS NC, otd.cumulative_opening_balance,(SELECT cumulative_closing_balance FROM op_trans_details WHERE ref_op_trans_id = otd.op_trans_id) AS cb, IF((otd.is_deleted ="1" && otd.is_status = "0"), "Rejected",  IF((otd.is_deleted ="0" && otd.is_status < "1"), "Pending", "Approved")) AS status, IF(otd.ref_op_trans_id > "0", " ", "") AS checked, IF(otd.ref_op_trans_id > "0", " ", "glyphicon glyphicon-pencil") AS edit, IF(otd.is_deleted ="0" && otd.is_status = "0", " ", " ")  AS reject', false);
        }
        
        if (isset($input['from_date']) && $input['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(otd.transaction_date, '%Y-%m-%d') >=", date('Y-m-d', strtotime($input['from_date'])));
        }

        if (isset($input['to_date']) && $input['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(otd.transaction_date, '%Y-%m-%d') <=", date('Y-m-d', strtotime($input['to_date'])));
        }

        if ($this->input->post('op_id') != "") {
            $this->datatables->where('otd.user_id', $this->input->post('op_id'));
        }
        
        $this->datatables->from('op_trans_details otd');
        $this->datatables->where("otd.transaction_for", 'operator');
        $this->datatables->where('otd.commission_rate_type!=', 'percentage');
        
        $this->datatables->add_column('action', '&nbsp;<a href="" class="activate_operator margin" ref="$1" data-status="$2"><i  class="$3"></i></a>'
                . '<a href=' . base_url() . 'admin/op_topup/edit_topup?id=$1 class="edit_topup margin" ref="$1" data-status="$2"><i  class="$4"></i> </a>'
                . '<a href="" class="reject_operator margin" ref="$1" data-status="$2"><i  class="$5"></i></a>', 'op_trans_id,status,checked,edit,reject');
        $data = $this->datatables->generate('json');
        echo $data;
    }
    

    /*
     * Before Addding new entry of topup. Check previous all entries are approved
     */
    
    function check_approved_topup() {
        is_ajax_request();
        $data['approved_status'] =  $this->op_topup_model->check_approved_topup();
        data2json($data);
    }
    
     function check_approved_topup_server() {
        $data['approved_status'] =  $this->op_topup_model->check_approved_topup();
        return $data;
    }
    
    /*
     * Function for redirecting user to add topup form.
     */
    
    function add_topup() {
        $checkApprovedTrans = $this->check_approved_topup_server();
        if ($checkApprovedTrans['approved_status'] == 'approved') {
            $data['payment_type'] = $this->op_topup_model->get_payment_type();

            $data['operator_name'] = $this->cb_op_master_details_model
                    ->where(array('op_name' => 'msrtc'))
                    ->as_array()
                    ->find_all();
            $data['services'] = $this->op_topup_model->get_services();

            if (strtolower($this->input->post('save')) == 'submit') {

                //check the status of topup before adding amount.
                $input = $this->input->post(NULL,true);
                //show($input,1);
                $op_name = $input['op_name'];
                $service_type = $input['service'];
                $transaction_type = $input['transaction_type'];
                $amt = $input['amt'];
                $payment_type = $input['payment_type'];
                if (isset($input['payment_mode'])) {
                    $payment_mode = $input['payment_mode'];
                } else {
                    $payment_mode = '';
                }
                $depositor_bank_name = $input['depositor_bank_name'];
                $depositor_acc_no = $input['depositor_acc_no'];
                $operator_bank_name = $input['operator_bank_name'];
                $operator_acc_no = $input['operator_acc_no'];
                $transaction_code = $input['transaction_code'];
                $remark = $input['remark'];
                $transaction_date = $input['transaction_date'];

                if ($op_name != '') {
                    $this->form_validation->set_rules('op_name', 'Operator Name', 'required|numeric');
                } else {
                    $this->form_validation->set_rules('api_provider', 'Api Provider', 'required|numeric');
                }

                $this->form_validation->set_rules('amt', 'Amount', 'required|callback_validate_amount');
                $this->form_validation->set_rules('depositor_acc_no', 'Depositor Account Number', 'numeric');
                $this->form_validation->set_rules('operator_acc_no', 'Operator Account Number', 'numeric');
                if ($this->form_validation->run() == TRUE) {

                    //check if the operator has already entry in  op_topup_model
                    /* if ($op_name != '') {
                        $where = array('user_id' => $op_name, 'service_type' => $service_type);
                    } else {
                        $where = array('user_id' => $api_provider, 'service_type' => $service_type);
                    } */
                    $where = array('user_id' => (($op_name != '') ? $op_name : $api_provider), 'service_type' => $service_type);
                    $op_data = $this->op_topup_model->column('id, user_id, available_balance, available_commission, usable_balance')
                            ->where($where)
                            ->find_all();
                     
                               //fetch cumulative opening and closing bal
                    $s = "SELECT opening_balance,closing_balance,cumulative_opening_balance,cumulative_closing_balance FROM op_trans_details WHERE is_deleted = '0' AND is_status= '1' ORDER BY op_trans_id DESC LIMIT 1";
                    $q = $this->db->query($s);
                    $d = $q->result_array();
                    $cum_op_balance = !empty($d[0]['cumulative_closing_balance']) ? $d[0]['cumulative_closing_balance'] : 0;
                    $cum_cl_balance = $cum_op_balance + $amt;
                    
                    
                    if ($op_data) {
                        
                        $totup_id = $op_data[0]->id;
                        $user_id = $op_data[0]->user_id;
                        $operator_amt = $op_data[0]->usable_balance;
                        $available_commission = $op_data[0]->available_commission;

                        $updated_amt = $operator_amt + $amt;
                        if ($op_name != '') {
                            $where_wallet = array('user_id' => $op_name, 'user_type' => 'o');
                        } else {
                            $where_wallet = array('user_id' => $api_provider, 'user_type' => 'a');
                        }
                        $op_wallet_data = $this->op_wallet_model->column('user_id,user_type,service_id,opening_bal,trans_amount,amount')
                                ->where($where_wallet)
                                ->find_all();
                        if ($op_wallet_data) {
                            $updated_amt = 0;
                            $operator_amt = 0;
                            $operator_amt = $op_wallet_data[0]->amount;

                            $updated_amt = $operator_amt + $amt;
                        }
                        $this->op_trans_details_model->op_topup_id = $totup_id;
                        if ($op_name != '') {
                            $this->op_trans_details_model->user_id = $op_name;
                            $this->op_trans_details_model->provider_flag = 'O';
                        } else {
                            $this->op_trans_details_model->user_id = $api_provider;
                            $this->op_trans_details_model->provider_flag = 'A';
                        }
                        $this->op_trans_details_model->service_type = $service_type;
                        $this->op_trans_details_model->transaction_type = $transaction_type;
                        $this->op_trans_details_model->date = date('Y-m-d');
                        $this->op_trans_details_model->transaction_amt = $amt;
                        $this->op_trans_details_model->opening_balance = $operator_amt;
                        $this->op_trans_details_model->closing_balance = $updated_amt;
                        $this->op_trans_details_model->payment_type = $payment_type;
                        $this->op_trans_details_model->payment_mode = $payment_mode;
                        $this->op_trans_details_model->depositor_bank_name = $depositor_bank_name;
                        $this->op_trans_details_model->depositor_acc_no = $depositor_acc_no;
                        $this->op_trans_details_model->operator_bank_name = $operator_bank_name;
                        $this->op_trans_details_model->operator_acc_no = $operator_acc_no;
                        $this->op_trans_details_model->transaction_code = $transaction_code;
                        $this->op_trans_details_model->transaction_for = 'operator';
                        $this->op_trans_details_model->commission_rate = OP_COMMISSION_RATE;
                        $this->op_trans_details_model->tds_type = OP_TDS_TYPE;
                        $this->op_trans_details_model->tds_rate = OP_TDS_RATE;
                        $this->op_trans_details_model->credit = 'credit';
                        $this->op_trans_details_model->remark = $remark;
                        $this->op_trans_details_model->created_by = $this->session->userdata('user_id');
                        $this->op_trans_details_model->transaction_date = date('Y-m-d', strtotime($transaction_date));
                        $this->op_trans_details_model->created_date = date('Y-m-d H:i:s');
                        $this->op_trans_details_model->cumulative_opening_balance = $cum_op_balance;
                        $this->op_trans_details_model->cumulative_closing_balance = $cum_cl_balance;
                        $trans_id = $this->op_trans_details_model->save();
                        unset($s,$q,$d,$cum_op_balance);
                        
                        //Enter second transaction entry
                        if ($transaction_type == 'prepaid') {
                            $transaction_amt = ($amt * OP_COMMISSION_RATE) / 100;
                            $this->op_trans_details_model->op_topup_id = $totup_id;
                            if ($op_name != '') {
                                $this->op_trans_details_model->user_id = $op_name;
                                $this->op_trans_details_model->provider_flag = 'O';
                            } else {
                                $this->op_trans_details_model->user_id = $api_provider;
                                $this->op_trans_details_model->provider_flag = 'A';
                            }

                            $atds_value = 0;
                            if (OP_TDS_TYPE == "percentage") {
                                $atds_value = ($transaction_amt * OP_TDS_RATE) / 100;
                            } else if (OP_TDS_TYPE == "flat") {
                                $atds_value = OP_TDS_RATE;
                            }
                            
                            $n_cum_op_balance = $cum_cl_balance;
                            $n_cum_cl_balance = $n_cum_op_balance + ($transaction_amt - $atds_value);

                            $this->op_trans_details_model->service_type = $service_type;
                            $this->op_trans_details_model->transaction_type = $transaction_type;
                            $this->op_trans_details_model->date = date('Y-m-d');
                            $this->op_trans_details_model->transaction_amt = $transaction_amt;
                            $this->op_trans_details_model->opening_balance = $updated_amt;
                            $this->op_trans_details_model->closing_balance = $updated_amt + ($transaction_amt - $atds_value);
                            $this->op_trans_details_model->payment_type = 6;
                            $this->op_trans_details_model->payment_mode = $payment_mode;
                            $this->op_trans_details_model->transaction_for = 'operator';
                            $this->op_trans_details_model->commission_rate_type = 'percentage';
                            $this->op_trans_details_model->commission_rate = OP_COMMISSION_RATE;
                            $this->op_trans_details_model->tds_type = OP_TDS_TYPE;
                            $this->op_trans_details_model->tds_rate = OP_TDS_RATE;
                            $this->op_trans_details_model->tds = $atds_value;
                            $this->op_trans_details_model->credit = 'credit';
                            $this->op_trans_details_model->remark = $remark;
                            $this->op_trans_details_model->created_by = $this->session->userdata('user_id');
                            $this->op_trans_details_model->transaction_date = date('Y-m-d', strtotime($transaction_date));
                            $this->op_trans_details_model->ref_op_trans_id = $trans_id;
                            $this->op_trans_details_model->created_date = date('Y-m-d H:i:s');
                            $this->op_trans_details_model->cumulative_opening_balance = $n_cum_op_balance;
                            $this->op_trans_details_model->cumulative_closing_balance = $n_cum_cl_balance;
                            $trans_id = $this->op_trans_details_model->save();
                        }
                        $this->session->set_flashdata('op_status', 'Topup updated successfully');
                        //pr($this->db->queries);die;
                        redirect(base_url() . 'admin/op_topup/');
                    } 
                    else {
                        //$usable_balance = $amt + $available_commission;
                        if ($op_name != '') {
                            $this->op_topup_model->user_id = $op_name;
                            $this->op_topup_model->provider_flag = 'O';
                        } else {
                            $this->op_topup_model->user_id = $api_provider;
                            $this->op_topup_model->provider_flag = 'A';
                        }
                        $this->op_topup_model->service_type = $service_type;
                        $this->op_topup_model->created_by = $this->session->userdata('user_id');
                        $this->op_topup_model->created_date = date('Y-m-d H:i:s', time());
                        $top_id = $this->op_topup_model->save();

                        $this->op_trans_details_model->op_topup_id = $top_id;
                        if ($op_name != '') {
                            $this->op_trans_details_model->user_id = $op_name;
                            $this->op_trans_details_model->provider_flag = 'O';
                        } else {
                            $this->op_trans_details_model->user_id = $api_provider;
                            $this->op_trans_details_model->provider_flag = 'A';
                        }
                        $this->op_trans_details_model->service_type = $service_type;
                        $this->op_trans_details_model->transaction_type = $transaction_type;
                        $this->op_trans_details_model->date = date('Y-m-d');
                        $this->op_trans_details_model->transaction_amt = $amt;
                        $this->op_trans_details_model->opening_balance = '0.00';
                        $this->op_trans_details_model->closing_balance = $amt;
                        $this->op_trans_details_model->payment_type = $payment_type;
                        $this->op_trans_details_model->payment_mode = $payment_mode;
                        $this->op_trans_details_model->depositor_bank_name = $depositor_bank_name;
                        $this->op_trans_details_model->depositor_acc_no = $depositor_acc_no;
                        $this->op_trans_details_model->operator_bank_name = $operator_bank_name;
                        $this->op_trans_details_model->operator_acc_no = $operator_acc_no;
                        $this->op_trans_details_model->transaction_code = $transaction_code;
                        $this->op_trans_details_model->transaction_for = 'operator';
						$this->op_trans_details_model->commission_rate = OP_COMMISSION_RATE;
                        $this->op_trans_details_model->tds_type = OP_TDS_TYPE;
                        $this->op_trans_details_model->tds_rate = OP_TDS_RATE;
                        $this->op_trans_details_model->credit = 'credit';
                        $this->op_trans_details_model->remark = $remark;
                        $this->op_trans_details_model->created_by = $this->session->userdata('user_id');
                        $this->op_trans_details_model->transaction_date = date('Y-m-d', strtotime($transaction_date));
                        $this->op_trans_details_model->created_date = date('Y-m-d H:i:s');
                        $this->op_trans_details_model->cumulative_opening_balance = $cum_op_balance;
                        $this->op_trans_details_model->cumulative_closing_balance = $cum_cl_balance;
                        $trans_id = $this->op_trans_details_model->save();
                        unset($s,$q,$d,$cum_op_balance);

                        //Enter second transaction entry
                        if ($transaction_type == 'prepaid') {
                            $transaction_amt = ($amt * OP_COMMISSION_RATE) / 100;
                            $this->op_trans_details_model->op_topup_id = $top_id;
                            if ($op_name != '') {
                                $this->op_trans_details_model->user_id = $op_name;
                                $this->op_trans_details_model->provider_flag = 'O';
                            } else {
                                $this->op_trans_details_model->user_id = $api_provider;
                                $this->op_trans_details_model->provider_flag = 'A';
                            }

                            $tds_value = 0;
                            if (OP_TDS_TYPE == "percentage") {
                                $tds_value = ($transaction_amt * OP_TDS_RATE) / 100;
                            } else if (OP_TDS_TYPE == "flat") {
                                $tds_value = OP_TDS_RATE;
                            }

                            $n_cum_op_balance = $cum_cl_balance;
                            $n_cum_cl_balance = $n_cum_op_balance + ($transaction_amt - $tds_value);
                            
                            $this->op_trans_details_model->service_type = $service_type;
                            $this->op_trans_details_model->transaction_type = $transaction_type;
                            $this->op_trans_details_model->date = date('Y-m-d');
                            // $this->op_trans_details_model->transaction_amt            =    $transaction_amt;
                            $this->op_trans_details_model->transaction_amt = $transaction_amt;
                            $this->op_trans_details_model->opening_balance = $amt;
                            $this->op_trans_details_model->closing_balance = $amt + ($transaction_amt - $tds_value);
                            $this->op_trans_details_model->payment_type = 6;
                            $this->op_trans_details_model->payment_mode = $payment_mode;
                            $this->op_trans_details_model->transaction_for = 'operator';
                            $this->op_trans_details_model->commission_rate_type = 'percentage';
                            $this->op_trans_details_model->commission_rate = OP_COMMISSION_RATE;
                            $this->op_trans_details_model->tds_type = OP_TDS_TYPE;
                            $this->op_trans_details_model->tds_rate = OP_TDS_RATE;
                            $this->op_trans_details_model->tds = $tds_value;
                            $this->op_trans_details_model->credit = 'credit';
                            $this->op_trans_details_model->remark = $remark;
                            $this->op_trans_details_model->created_by = $this->session->userdata('user_id');
                            $this->op_trans_details_model->transaction_date = date('Y-m-d', strtotime($transaction_date));
                            $this->op_trans_details_model->ref_op_trans_id = $trans_id;
                            $this->op_trans_details_model->created_date = date('Y-m-d H:i:s');
                            $this->op_trans_details_model->cumulative_opening_balance = $n_cum_op_balance;
                            $this->op_trans_details_model->cumulative_closing_balance = $n_cum_cl_balance;
                            $trans_id = $this->op_trans_details_model->save();
                        }
                        $this->session->set_flashdata('op_status', 'Topup added successfully');
                        redirect(base_url() . 'admin/op_topup/');
                    }
                } else {
                    die('gere');
                }
            }
            load_back_view(OPERATOR_TOPUP_ADD, $data);
        } else {
            $this->session->set_flashdata('op_status', 'Please approve the previous transactions before adding new request.');
            redirect(base_url() . 'admin/op_topup/');
        }
    }

    /*
     * Edit Topup
     */
    
    function edit_topup() {
        $topup_id = $this->input->get('id');
        $data['top_up_data'] = $this->op_trans_details_model->as_array()->find($topup_id);
        $data['service_data'] = $this->op_topup_model->get_services($data['top_up_data']['service_type']);
        

        if($this->session->userdata('op_id')!="" && $this->session->userdata('op_id')!=$data['top_up_data']['op_id'])
        {
            $this->session->set_flashdata('op_status', 'This entry is not belongs to you');
            redirect('admin/op_topup/');
        }
        else
        {
        if($data['top_up_data']) {
            if($data['top_up_data']['is_status']==0){
                
                $data['payment_type']  = $this->op_topup_model->get_payment_type();
                
                if($this->session->userdata('op_id')!="")
                {
                   $this->cb_op_master_details_model->where('op_id', $this->session->userdata('op_id'));
                }
                
                $data['payment_mode'] = $this->op_topup_model->get_payment_mode($data['top_up_data']['payment_type']);
                
                if($data['top_up_data']['provider_flag'] == 'O'){
                    $data['user_details'] = $this->cb_op_master_details_model->find($data['top_up_data']['user_id']);
                }
                else if($data['top_up_data']['provider_flag'] == 'A'){
                    $data['user_details'] = $this->api_provider_model->find($data['top_up_data']['user_id']);
                }

                load_back_view(OPERATOR_TOPUP_EDIT, $data);
            } else {
                $this->session->set_flashdata('op_status', 'It is already approved. Not allow to update');
                redirect('admin/op_topup/');
            }
        } else {
            $this->session->set_flashdata('op_status', 'Somethign went wrong. Try Again');
            redirect('admin/op_topup/');
        }
        }
    }
    
    /*
     * Update Topup Data
     */
    
    function update_op_topup() {
         
        if(strtolower($this->input->post('update'))=='update') {
            $input                  =  $this->input->post(NULL,true);
             
            $op_trans_id            =  $input['op_trans_id'];
            $amt                    =  $input['amt'];
            $payment_type           =  $input['payment_type'];
            if(isset($input['payment_mode']))
                {
                    $payment_mode   =  $input['payment_mode'];
                }
                else
                {
                    $payment_mode   =  '';
                }
            $depositor_bank_name    =  $input['depositor_bank_name'];
            $depositor_acc_no       =  $input['depositor_acc_no'];
            $operator_bank_name     =  $input['operator_bank_name'];
            $operator_acc_no        =  $input['operator_acc_no'];
            $transaction_code       =  $input['transaction_code'];
            $remark                 =  $input['remark'];
            $transaction_date       =  $input['transaction_date'];
            $transaction_type       =  $input['transaction_type'];
            
            $this->form_validation->set_rules('amt','Amount','required|callback_validate_amount');
            $this->form_validation->set_rules('depositor_acc_no','Depositor Account Number','numeric');
            $this->form_validation->set_rules('operator_acc_no','Operator Account Number','numeric');
            
            if ($this->form_validation->run() == TRUE) {
                
                 $op_trans_data           =  $this->op_trans_details_model->as_array()->find($op_trans_id);
                 
                 $closing_balance         =  $op_trans_data['opening_balance']+$amt;
                 $topup_trans_arr = array(
                                        'transaction_amt'       =>  $amt,
                                        'closing_balance'       =>  $closing_balance,
                                        'payment_type'          =>  $payment_type,
                                        'payment_mode'          =>  $payment_mode,
                                        'depositor_bank_name'   =>  $depositor_bank_name,
                                        'depositor_acc_no'      =>  $depositor_acc_no,
                                        'operator_bank_name'    =>  $operator_bank_name,
                                        'operator_acc_no'       =>  $operator_acc_no,
                                        'transaction_code'      =>  $transaction_code,
                                        'remark'                =>  $remark,
                                        'transaction_date'      =>  date('Y-m-d',strtotime($transaction_date)),
                                        );

                    $this->op_trans_details_model->update_where('op_trans_id',$op_trans_id,$topup_trans_arr);

                        
                    //Reference Entry Upadate
                    if($transaction_type=="prepaid")
                    {
                        $op_trans_data_ref   =  $this->op_trans_details_model->as_array()->find($op_trans_id);
                        $closing_balance_ref =  $op_trans_data_ref['closing_balance'];
                        $ref_transation_amt  = ($amt * OP_COMMISSION_RATE)/100;
                        $ref_opening_bal     =  $closing_balance_ref ;
                        $ref_closing_balance = $ref_transation_amt + $closing_balance_ref;

                        $tds_value = 0;
                        if(OP_TDS_TYPE == "percentage")
                        {
                            $tds_value = ($ref_transation_amt * OP_TDS_RATE)/100;
                        }
                        else if(OP_TDS_TYPE == "flat")
                        {
                            $tds_value = OP_TDS_RATE;
                        }

                        $ref_topup_trans_arr = array(
                                            'transaction_amt'       =>  $ref_transation_amt,
                                            'opening_balance'       =>  $ref_opening_bal,
                                            /*'closing_balance'       =>  $ref_closing_balance,*/
                                            'closing_balance'       =>  $ref_closing_balance-$tds_value,
                                            'tds_type'              =>  OP_TDS_TYPE,
                                            'tds_rate'              =>  OP_TDS_RATE,
                                            'tds'                   =>  $tds_value,
                                            'transaction_date'      =>  date('Y-m-d',strtotime($transaction_date)),
                                            );

                        $this->op_trans_details_model->update_where('ref_op_trans_id',$op_trans_id,$ref_topup_trans_arr);
                    }
                    $this->session->set_flashdata('op_status', 'Topup updated successfully');
                redirect('admin/op_topup/');
            }
        }
    }
    
    /*
     * Enable or Disable the Topup
     */

    public function enableDisableTopup() {

        $input = $this->input->post();
        $id = trim($input['id']);
        $this->op_trans_details_model->op_trans_id = $id;
        $topup_detail = $this->op_trans_details_model->select();
        //show($topup_detail,1);        
        $tds_detail = $this->op_trans_details_model->where("ref_op_trans_id",$id)->find_all();
        $topup_status = $topup_detail->is_status;
        

        if ($topup_status == 1) {
            $data["msg"] = "You are not allowed to change status once it is approved";
            $this->session->set_flashdata('op_status', "You are not allowed to change status once it is approved");
        } else {
            
            $operator_bal = $this->op_topup_model->column('id, available_balance, available_commission')
                            ->where(array('user_id'=>$topup_detail->user_id, 'service_type'=>$topup_detail->service_type))
                            ->as_array()
                            ->find_all(); 
            
            $available_commission = $operator_bal[0]['available_commission']; 
            
            log_data("wallet_comission/".date("Y-m-d").".log",$operator_bal,"operator_bal");

            // To Update op_trans_details_model
            /*$this->op_trans_details_model->op_trans_id          =  $id;
            $topup_detail_referece                              =  $this->op_trans_details_model->select();*/
            if($topup_detail->transaction_type=="prepaid"){
                $calculate_new_commision                        =  (($topup_detail->transaction_amt * OP_COMMISSION_RATE)/100)-$tds_detail[0]->tds;
            } else {
                $calculate_new_commision                        =  0;
            }
            $updated_commission                                 =  $calculate_new_commision+$available_commission;
            $updated_usable_bal                                 =  $topup_detail->transaction_amt+$operator_bal[0]['available_balance'] + $updated_commission;
            
            //update amount in op_topup table
            $this->op_topup_model->id                           =  $operator_bal[0]['id'];
            $this->op_topup_model->available_balance            =  $topup_detail->transaction_amt+$operator_bal[0]['available_balance']-$tds_detail[0]->tds;
            $this->op_topup_model->available_commission         =  $updated_commission;
            $this->op_topup_model->usable_balance               =  $updated_usable_bal;
            $this->op_topup_model->save();

            log_data("wallet_comission/".date("Y-m-d").".log",$this->db->last_query(),"last query");
            
            if($topup_detail->transaction_type=="prepaid"){
                // Get the operator id
                if($topup_detail->provider_flag=='O') {
                    $operator_id = $this->cb_op_master_details_model->where('user_id',$topup_detail->user_id)->find_all();
                    
                    $op_id = $operator_id[0]->user_id;
                    
                    $operator_data  =  array(
                    'tds' => $tds_detail[0]->tds,
                    'service_id' => $topup_detail->service_type,
                    'amount' => $topup_detail->transaction_amt,
                    'comment' => 'Amount credited to Trimax'
                    );
                    
                    $trans_type = 'Credit';
                    
                    
                    $operator_data_commision  =  array(
                    'service_id' => $topup_detail->service_type,
                    'amount' => $calculate_new_commision,
                    'comment' => 'Topup of Rs. '.$topup_detail->transaction_amt,
                    );
                    
                    $this->op_wallet_model->do_trans($op_id, 'o', $operator_data,$trans_type, $this->session->userdata('id'));
                    $this->op_wallet_model->do_trans($op_id, 'o', $operator_data_commision,$trans_type, $this->session->userdata('id'));
                } 
                
            } else {
                if($topup_detail->provider_flag=='O') {
                    $operator_id = $this->op_master_details_model->where('op_id',$topup_detail->user_id)->find_all();
                    
                    $op_id = $operator_id[0]->op_id;
                    
                    $operator_data  =  array(
                    'service_id' => $topup_detail->service_type,
                    'amount' => $topup_detail->transaction_amt,
                    'comment' => 'Amount credited to Trimax'
                    );
                    
                    $trans_type = 'Credit';
                    
                    $this->op_wallet_model->do_trans($op_id, 'o', $operator_data, $trans_type);
                }
            }
            
           
            
            $this->op_trans_details_model->op_trans_id          =  $id;
            $this->op_trans_details_model->is_status            =  "1";
            $save = $this->op_trans_details_model->save();
            
            $agentChangeStatus = array('is_status'=>'1');
            $this->op_trans_details_model->update_where('ref_op_trans_id', $id, $agentChangeStatus);
            
            $data["flag"]           = '@#success#@';
            $data["msg_type"]       = 'success';
            $data["msg"]            = "Operator's Topup Enabled Successfully";
            $data["updated_data"]   = "Y";
            $this->session->set_flashdata('op_status', "Operator's Topup Enabled Successfully");
        }
        data2json($data);
    }
    
    
    function getAgentDetails() {
        $input = $this->input->post();
        $op_id = trim($input['op_id']);
        $service_type = trim($input['service_type']);
        $data['operator_details'] = $this->op_topup_model->getAgentDetails($op_id,$service_type);
        data2json($data);
            
    }
 
    /*
     * Get Payment mode
     */
    function get_payment_mode(){
        $input = $this->input->post();
        $payment_type_id = $input['payment_type_id'];
        $data['payment_mode'] = $this->op_topup_model->get_payment_mode($payment_type_id);
        if($data) {
            
        }
        data2json($data);
    }
    /*
     * Validate Amount
     */
    function validate_amount($str) {
        if(!preg_match('/^\d{0,7}(\.\d{0,2})?$/', $str)) {
             $this->form_validation->set_message(__FUNCTION__, 'Please enter valid Amount');
             return false;
        }
        return true;
    }
    
    public function reject_topup() {

        $input = $this->input->post();
        $id = trim($input['id']);
	    $remark=trim($input['remark']);

        log_data("wallet_comission/".date("Y-m-d").".log",$input,"input");
//        $id = "5";
        $this->op_trans_details_model->op_trans_id = $id;
        $topup_detail = $this->op_trans_details_model->select();

        $tds_detail = $this->op_trans_details_model->where("ref_op_trans_id",$id)->find_all();
        log_data("wallet_comission/".date("Y-m-d").".log",$tds_detail,"tds_detail");

        $topup_status = $topup_detail->is_status;
        

        if ($topup_status == 1) {
            $data["msg"] = "You are not allowed to change status once it is approved";
            $this->session->set_flashdata('op_status', "You are not allowed to change status once it is approved");
        } else {
            
           
            $agentChangeStatus = array('is_status'=>'0','is_deleted'=>'1','remark'=>$remark);
            $this->op_trans_details_model->update_where('ref_op_trans_id', $id, $agentChangeStatus);
            
            $agentChangeStatus = array('is_status'=>'0','is_deleted'=>'1','remark'=>$remark);
            $this->op_trans_details_model->update_where('op_trans_id', $id, $agentChangeStatus);
            
            $data["flag"]           = '@#success#@';
            $data["msg_type"]       = 'success';
            $data["msg"]            = "Operator's Topup Rejected Successfully";
            $data["updated_data"]   = "Y";
            $this->session->set_flashdata('op_status', "Operator's Topup Rejected Successfully");
        }
        data2json($data);
    }
    
    
    /*
     * List the topup of the operators
     */
    function list_topup_data_old() {
//        if($this->input->post('provider_type') == 'operator')
//        { 

        if ($this->session->userdata('role_id') == RSRTC_USER_ROLE_ID) {
            /* $this->datatables->select('
              otd.op_trans_id,
              om.op_name,
              otd.opening_balance,
              ROUND(if(otd.credit="credit",otd.transaction_amt,"0"),2) as credit,
              ROUND(if(otd.commission_rate_type = "percentage",otd.tds,0),2) as tds,
              ROUND(if(otd.commission_rate_type = "percentage",(otd.transaction_amt - otd.tds), otd.transaction_amt),2) as net_value,
              otd.closing_balance,
              opt.payment_type,
              DATE_FORMAT(otd.transaction_date,"%d-%m-%Y") as trans_date,
              if(otd.is_status < "1","Pending","Approved") as status,
              if(otd.ref_op_trans_id > "0"," ","glyphicon glyphicon-check") as checked,
              if(otd.ref_op_trans_id > "0"," ","") as edit  opening_balance
              ', false); */

            $this->datatables->select('otd.op_trans_id,otd.created_date,otd.op_trans_id as op_trans,otd.transaction_code,otd.depositor_bank_name,transaction_amt, (otd.transaction_amt *' . OP_COMMISSION_RATE . '/100) GC,(((otd.transaction_amt *' . OP_COMMISSION_RATE . '/100) *' . OP_TDS_RATE . '/100)) ,"gf",
                                            opening_balance,"jj", 
                                            if((otd.is_deleted ="1" && otd.is_status = "0"),"Rejected",if((otd.is_deleted ="0" && otd.is_status < "1"),"Pending","Approved")) as status,
                                            if((otd.is_deleted ="0" && otd.ref_op_trans_id) > "0"," ","glyphicon glyphicon-check") as checked, 
                                              
                                            if(otd.ref_op_trans_id > "0"," ","") as edit,  
                                          if(otd.is_deleted ="0" && otd.is_status = "0","glyphicon glyphicon-file"," ")  as reject    
                                        ', false);
        } else {
            /* $this->datatables->select('
              otd.op_trans_id,
              om.op_name,
              otd.opening_balance,
              ROUND(if(otd.credit="credit",otd.transaction_amt,"0"),2) as credit,
              ROUND(if(otd.commission_rate_type = "percentage",otd.tds,0),2) as tds,
              ROUND(if(otd.commission_rate_type = "percentage",(otd.transaction_amt - otd.tds), otd.transaction_amt),2) as net_value,
              otd.closing_balance,
              opt.payment_type,
              DATE_FORMAT(otd.transaction_date,"%d-%m-%Y") as trans_date,
              if(otd.is_status < "1","Pending","Approved") as status,
              if(otd.ref_op_trans_id > "0"," ","") as checked,
              if(otd.ref_op_trans_id > "0"," ","glyphicon glyphicon-pencil") as edit,
              ', false); */
            $this->datatables->select('otd.op_trans_id,otd.created_date,otd.op_trans_id as op_trans,otd.transaction_code,otd.depositor_bank_name,transaction_amt, (otd.transaction_amt *' . OP_COMMISSION_RATE . '/100),(((otd.transaction_amt *' . OP_COMMISSION_RATE . '/100) *' . OP_TDS_RATE . '/100)),"gf",
                                            opening_balance,"jj", 
                                          if((otd.is_deleted ="1" && otd.is_status = "0"),"Rejected",if((otd.is_deleted ="0" && otd.is_status < "1"),"Pending","Approved")) as status,
                                            if(otd.ref_op_trans_id > "0"," ","") as checked, 
                                            if(otd.ref_op_trans_id > "0"," ","glyphicon glyphicon-pencil") as edit, 
                                            if(otd.is_deleted ="0" && otd.is_status = "0"," "," ")  as reject    
                                        ', false);
        }

        $this->datatables->join("op_master om", 'om.op_id = otd.user_id and otd.provider_flag= "O"');

        if ($this->input->post('op_id') != "") {
            $this->datatables->where('otd.user_id', $this->input->post('op_id'));
        }
        //   }
        /*  else if($this->input->post('provider_type') == 'api_provider')
          {
          $this->datatables->select('
          otd.op_trans_id,
          ap.name,
          DATE_FORMAT(otd.date,"%d-%m-%Y") as dates,
          if(otd.credit="credit",otd.transaction_amt,"-") as credit,
          s.name,
          if(otd.provider_flag="O","Operator","API Provider") as provider,
          otd.transaction_type,
          sum(otd.opening_balance),
          otd.closing_balance,
          opt.payment_type,
          opm.payment_mode,
          otd.depositor_bank_name,
          otd.depositor_acc_no,
          otd.operator_bank_name,
          otd.operator_acc_no,
          otd.transaction_code,
          DATE_FORMAT(otd.transaction_date,"%d-%m-%Y") as trans_date,
          if(otd.is_status < "1","Pending","Approved") as status,
          if(otd.ref_op_trans_id > "0"," ","glyphicon glyphicon-check") as checked,
          if(otd.ref_op_trans_id > "0"," ","glyphicon glyphicon-pencil") as edit,
          ', false);

          $this->datatables->join("api_provider ap", 'ap.id=otd.user_id and otd.provider_flag= "A"');

          if($this->input->post('provider_id')!="")
          {
          $this->datatables->where('otd.user_id', $this->input->post('provider_id'));
          }
          } */

        $this->datatables->from('op_trans_details otd');

        $this->datatables->join("op_payment_type opt", 'opt.id = otd.payment_type', 'left');
        $this->datatables->join("op_payment_mode opm", 'opm.id = otd.payment_mode', 'left');
        $this->datatables->join("services s", 's.id = otd.service_type', 'left');
        $this->datatables->where("otd.transaction_for", 'operator');
        $this->datatables->where('otd.commission_rate_type!=', 'percentage');
        /* if($this->session->userdata('op_id')!="" && !in_array(strtolower($this->session->userdata('role_name')), array('super admin','admin')))
          {
          $this->datatables->where('otd.user_id', $this->session->userdata('op_id'));
          }

          if($this->input->post('service_type')!="")
          {
          $this->datatables->where('otd.service_type', $this->input->post('service_type'));
          } */
        $this->datatables->add_column('action', '&nbsp;<a href="" class="activate_operator margin" ref="$1" data-status="$2"><i  class="$3"></i></a>'
                . '<a href=' . base_url() . 'admin/op_topup/edit_topup?id=$1 class="edit_topup margin" ref="$1" data-status="$2"><i  class="$4"></i> </a>'
                . '<a href="" class="reject_operator margin" ref="$1" data-status="$2"><i  class="$5"></i></a>', 'op_trans_id,status,checked,edit,reject');
        $data = $this->datatables->generate('json');
        // show(last_query(),1);
        echo $data;
    }
    
    public function testemail() {
        $mail_content = "Hello, <br> Topup Reqested for amount : Rs. 1Crore<br> Thanks and regards";
        $subject = "Topup requested ";

        $mailbody_array = array("subject" => $mail_content,
            "message" => $subject,
            "to" =>  array("amolbos87@gmail.com","winamol@gmail.com") 
        );

        $mail_result = send_mail($mailbody_array);
        $res = mail("amolbos87@gmail.com",$subject,$mail_content);
        var_dump($res);
        die;
    }
    public function op_pg_wallet()
    {
        $input = $this->input->post(NULL,TRUE);
        $pg_amt = $input['pg_amt'];
        $config = array(
            array('field' => 'pg_amt', 'label' => 'Amount', 'rules' => 'trim|required|numeric|greater_than[0]|xss_clean', 'errors' => array('required' => 'Please enter valid Topup amount.')),
        );
        $users_data = $this->users_model
                        ->where("id", $this->session->userdata('user_id'))
                        ->as_array()->find_all();

        if (form_validate_rules($config) == FALSE) {

            $data['error'] = $this->form_validation->error_array();
            log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $input, "User Enters data");
            log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $data['error'], "form error");

            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', $data['error']['pg_amt']);
            redirect(base_url() . 'admin/fund_request/index');
            //redirect(base_url() . 'admin/fund_request/add_money_wallet');
        } else {
            $amount = intval($input['pg_amt']);
            if($amount <= 0){
                $data["flag"] = '@#failed#@';
                $data["msg_type"] = 'error';
                $data["msg"] = 'Amount Should Be Greater Than 0';  
            }else{
           /* $RollId = COMPANY_ROLE_ID;
            $RokadData = $this->common_model->get_value('users','id','role_id='.$RollId);
            $RokadWallet = $this->common_model->get_value('wallet','id,amt,threshold_limit','user_id='.$RokadData->id);
            $RokadWalletAmount = round($RokadWallet->amt,2);*/

            /*$RokadWalletThresholdLimit = $RokadWallet->threshold_limit;
            if($pg_amt > $RokadWalletAmount){ 
                $data1["flag"] = '@#failed#@';
                $data1["msg"] = 'Sorry, Company Balance is less than requested Amount.';
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', $data1['msg']);
                redirect(base_url() . 'admin/fund_request/index');
            }else{
            */
            $OwnAvaiableBalance = $this->common_model->get_value('op_wallet','id,user_id,amount','user_id='.$this->session->userdata('user_id'));
            $NowTotalBalance = $OwnAvaiableBalance->amount + $pg_amt;
           
            if($NowTotalBalance > MSRTC_WALLET_LIMIT){
                $data1["flag"] = '@#failed#@';
                $data1["msg"] = 'Sorry, Balance Request can not be processed as you are exceeding wallet balance limit.';
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', $data1['msg']);
                redirect(base_url() . 'admin/op_topup');
                
            }else {
            $php_uniqid = "agw" . uniqid() . getRandomId(4);
            $track_id = $php_uniqid;

            $top_up_pg_details = [
                'track_id' => $track_id,
                'payment_by' => 'ccavenue',
                'user_id' => $this->session->userdata('user_id'),
                'total_amount' => $pg_amt,
                'status' => "initialize",
                'ip_address' => $this->input->ip_address(),
                'user_agent' => $this->session->userdata("user_agent")
            ];

            $payment_tran_fss_id = $this->payment_transaction_fss_model->insert($top_up_pg_details);

            $wallet_trans = [
                'user_id' => $this->session->userdata('user_id'),
                'date' => date('Y-m-d h:i:s'),
                'provider_flag'=> 'o',
                'transaction_amt' => $pg_amt,
                'created_by' => $this->session->userdata('user_id'),
                'created_date' => date('Y-m-d h:i:s'),
                'is_status' => '0',
                'is_deleted' => '0',
                'credit' => 'credit',
                'topup_by' => 'pg',
                'opening_balance'=> $OwnAvaiableBalance->amount,
                'opening_balance'=> $OwnAvaiableBalance->amount,
                'closing_balance'=> $OwnAvaiableBalance->amount + $pg_amt,
                'pg_transaction_status' => 'pfailed',
            ];

            $wallet_topup_id = $this->op_trans_details_model->insert($wallet_trans);

            log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $wallet_topup, "Wallet Topup Pg Request data");

            if ($wallet_topup_id) {
                $ReqTid = "tid=" . time();
                $ReqorderId = $track_id;
                $ReqTotalAmount = $pg_amt;
                $merchant_param1 = $payment_tran_fss_id;
                $merchant_param2 = $wallet_topup_id;
                $merchant_param3 = '';
                $merchant_param4 = "";

                $ReqMerchantOrderId = "order_id=" . $ReqorderId;
                $ReqMerchantId = "merchant_id=" . $this->config->item("cca_merchant_id_agent");
                $ReqAmount = "amount=" . $pg_amt;
                $ReqCurrency = "currency=INR";
                $ReqRedirectUrl = "redirect_url=" . base_url() . "admin/op_topup/get_cca_response";
                $ReqCancelUrl = "cancel_url=" . base_url() . "admin/op_topup/get_cca_response";
                $ReqLanguage = "language=EN";
                $ReqMerchantPara1 = "merchant_param1=" . $merchant_param1;
                $ReqMerchantPara2 = "merchant_param2=" . $merchant_param2;
                $ReqMerchantPara3 = "merchant_param3=" . $merchant_param3;
                $ReqMerchantPara4 = "merchant_param4=" . $merchant_param4;

                $Strhashs = trim($ReqMerchantOrderId) . "&" . trim($ReqAmount) . "&" . trim($ReqCurrency) . "&" . trim($ReqMerchantPara1) . "&" . trim($ReqMerchantPara2) . "&" . trim($ReqMerchantPara3) . "&" . trim($ReqMerchantPara4);

                if (isset($users_data) && count($users_data) > 0) {
                    if (isset($users_data[0]['first_name'])) {
                        $ReqBillingName = "billing_name=" . trim($users_data[0]['first_name']);
                        $Strhashs = $Strhashs . "&" . trim($ReqBillingName);
                    }

                    if (isset($users_data[0]['email'])) {
                        $ReqBillingEmail = "billing_email=" . rokad_decrypt(trim($users_data[0]['email']),$this->config->item('pos_encryption_key'));
                        $Strhashs = $Strhashs . "&" . trim($ReqBillingEmail);
                    }

                    if (isset($users_data[0]['phone']) && $users_data[0]['phone'] != '0') {
                        $ReqBillingTelNo = "billing_tel=" . trim($users_data[0]['phone']);
                        $Strhashs = $Strhashs . "&" . trim($ReqBillingTelNo);
                    }
                }

                //For geo location
                //UNCOMMENT BELOW LINE ON LIVE
                //$ip_address = $this->session->userdata("ip_address");
                //COMMENT BELOW LINE ON LIVE
                $locationdata = array();
                $ip_address = $this->input->ip_address();

                $ReqBillingCountry = "billing_country=India";
                $Strhashs = $Strhashs . "&" . trim($ReqBillingCountry);
                //AFTERWARDS DO THIS OR CHANGE LOGIC FOR merchant_param5
                //Use hash method which is defined below for Hashing ,It will return Hashed valued of above string
                $hashedstring = hash('sha256', $Strhashs);
                $ReqMerchantPara5 = "merchant_param5=" . $hashedstring;
                $Reqparam = trim($ReqTid) . "&" . trim($ReqMerchantId) . "&" . trim($ReqMerchantOrderId) . "&" . trim($ReqAmount) . "&" . trim($ReqCurrency) . "&" . trim($ReqRedirectUrl) . "&" . trim($ReqCancelUrl) . "&" . trim($ReqLanguage) . "&" . trim($ReqMerchantPara1) . "&" . trim($ReqMerchantPara2) . "&" . trim($ReqMerchantPara3) . "&" . trim($ReqMerchantPara4);
                if (isset($ReqBillingName)) {
                    $Reqparam .= "&" . $ReqBillingName;
                }

                if (isset($ReqBillingEmail)) {
                    $Reqparam .= "&" . $ReqBillingEmail;
                }

                if (isset($ReqBillingTelNo)) {
                    $Reqparam .= "&" . $ReqBillingTelNo;
                }

                $Reqparam .= "&" . $ReqBillingCountry;
                $Reqparam = $Reqparam . "&" . trim($ReqMerchantPara5) . "&";

                log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $Reqparam, "Data For Payment");

                $encrypted_data = encrypt($Reqparam, $this->config->item("cca_working_key_agent"));
                $ccprocess_data["encrypted_data"] = $encrypted_data;

                log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $encrypted_data, "Encrypted Data For Payment");

                load_back_view(OP_PGPROCESSING_VIEW, $ccprocess_data);
            } else {
                $custom_error_mail = array(
                    "custom_error_subject" => "Data Not Add into Wallet TopUp.",
                    "custom_error_message" => "Data Not added into wallet topup while topup from pg - " . json_encode($wallet_topup)
                );
                $this->common_model->developer_custom_error_mail($custom_error_mail);

                log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $wallet_topup, "Pg Request Time Data");

                load_front_view(UNEXPECTED_ERROR);
            }
        }                    // Wallet Limit Condition
       }
      }
      }
       public function get_cca_response() {
        $data = $this->cca_payment_gateway->GetOperatorTopupResponse();

        $this->session->set_flashdata('msg_type', $data["page_msg_type"]);
        $this->session->set_flashdata('msg', $data["page_msg"]);

        redirect($data["page_redirect"]);
    }
    
}