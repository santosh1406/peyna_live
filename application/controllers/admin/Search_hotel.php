<?php
// created by Shrasti Bansal on 31-dec-2018
//modified by Sonali Kamble on 31-may-19
defined('BASEPATH') OR exit('No direct script access allowed');

class Search_hotel extends CI_Controller{
	public function __construct() {
        parent::__construct();
       
        $this->load->library('form_validation');
        $this->load->helper(array('hotel_helper'));       
        $this->load->model(array('Utilities_model','Search_hotel_model','retailer_service_mapping_model','wallet_model'));
      	is_logged_in();

      	$this->sessionData['id'] = $this->session->userdata('user_id');
      	$this->sessionData["username"] = $this->session->userdata('username');
        $this->sessionData["password"] = $this->session->userdata('password');
        
        $this->rokad_headers = json_decode(ROKAD_HOTEL_CREDS, true);
    }
    
    private function checkAccessToModule()
    {
        $user_data = $this->session->userdata();
        $agent_id = $user_data['user_id'];
        $retailer_services = $this->retailer_service_mapping_model->getServiceByAgentId($agent_id);
        $service_array = array_column($retailer_services, 'service_id');
        if (in_array(BOS_SERVICE_ID, $service_array)) {
            $checkCommissionExists = $this->checkCommissionExists($agent_id);
            if($checkCommissionExists) {
                return true;
            }
        } else {
            $this->session->set_flashdata('item', 'No access to module');
            redirect(base_url() . 'admin/dashboard');
        }
    }
	
    function index(){	
        //$this->checkAccessToModule();
        $cityList = $this->Search_hotel_model->cityList();
        $data['cityList'] = $cityList;        
        load_back_view(SEARCH_HOTEL_VIEW,$data);	 	
    }
    
    function test1(){
        $cityList = $this->Search_hotel_model->cityList();
        $data['cityList'] = $cityList;        
        load_back_view(SEARCH_HOTEL_VIEW1,$data);
        
    }
    
    function hotel_s(){
        $data= array();
        load_back_view(SEARCH_HOTEL_VIEW2,$data);
    }

    public function hotel_services() {
        $cityList = $this->Search_hotel_model->cityList();
        $data['cityList'] = $cityList;   
        
        //$this->checkAccessToModule();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('city', 'Destination', 'required');
        $this->form_validation->set_rules('check_in_date', 'check in date', 'required');
        $this->form_validation->set_rules('check_out_date', 'check out date', 'required');
        
        $rajax = isset($_POST['rajax']) && $this->input->post('rajax') ? $this->input->post('rajax') : '';
        
        if ($this->form_validation->run() == FALSE && $rajax !='rajax') {
            load_back_view(HOME_SEARCH);
        } else {
            
        }
        if (isset($_POST['room']) && $_POST['room'] != '') {
                $input['rooms_count'] = count($_POST['room']);
        }else{
                $input['rooms_count'] = '';
        }
        
        
        if (isset($_POST['check_in_date']) && $_POST['check_in_date'] != '') {
                $input['checkInDate'] = date('Y-m-d',strtotime($_POST['check_in_date']));
        }else{
                $input['checkInDate'] = '';
        }

        if (isset($_POST['check_out_date']) && $_POST['check_out_date'] != '') {
                 $input['checkOutDate'] = date('Y-m-d',strtotime($_POST['check_out_date']));
        }else{
                $input['checkOutDate'] = '';
        }
        
        if (isset($_POST['city']) && $_POST['city'] != '') {
                $input['city'] = $_POST['city'];
        }else{
                $input['city'] = '';
        }
        
        $input['total_days'] = timeDiff($input['checkInDate'], $input['checkOutDate']);
       
        $input['regionIds'] = '["'.'5C'.$input['city'].'"]';
        //show($_POST,1);    
        $rooms = array();

        $i= 0;
        foreach ($_POST['room'] as $key => $value) {
            if($value['group_children'][$i]!='0')
                {
                    $rooms[] = array(
                
                    'adults' => $value['group_adults'] ? $value['group_adults'][$i] : '',
                    'children' => ($value['group_children'][$i]=='0') ?  '' : $value['group_children'][$i],
                    'childrenAges' => $value['childrens_age'] ? $value['childrens_age'] : '',
            
            );
                }else{
                    $rooms[] = array(                
                        'adults' => $value['group_adults'] ? $value['group_adults'][$i] : '',
                            );
                }
            
                    
        }
        $i++;
        //show($rooms,1);


        
       // $rooms[] = array('adults' => $_POST['room'][1]['group_adults'][0]); //working

        
        $input['rooms'] = json_encode($rooms);
        $input['getRates'] = "true";
        $input['getRooms'] = "true";
        
         //  show($input,1);
        
        //$file_name = strtolower($input['checkInDate']) . "_" . strtolower($input['checkOutDate']) . '.json';
        $file_name = strtolower(date('Y-m-d')) . '.json';
        $file_name = str_replace(' ', '', $file_name);

        $this->session->set_userdata($input);
        $this->session->set_userdata('search_hotel_input',$input);

        if (!empty($input)) {
            
            
            $curl_url  = base_url() .  "rest_server/search_hotel/hotel_services";
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'curl.log', $curl_url, 'Hotel Services curl');
            $curlResult = curlForPostData_HOTEL($curl_url, $input, $this->rokad_headers);
            //$curlResult = '{"status":"Success","data":}';
            //echo $file_path = FCPATH . "json_data/";        
            $file_path = $_SERVER['DOCUMENT_ROOT'].'/sc_roakad/rokad/json_data/';

            //show($curlResult,1);
           // $jsonResult  = '{"status":"Success","data":{"searchId":"f1a3f359-880b-4856-9863-d2361c00005f","searcherIds":[5,6,7,8,9,28,29],"currency":"INR","hotelResults":[{"hotelInfo":{"hotelId":228866,"name":"Yogi Executive"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":4312.4245264379,"roomNightTaxes":635,"roomNightOriginalPrice":4312.4245264379,"agentPricing":{"roomNightNetPrice":4264.6251619657,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":228865,"name":"Hotel Yogi Metropolitan"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":2549.2053404255,"roomNightTaxes":27.237255319149,"roomNightOriginalPrice":2549.2053404255,"agentPricing":{"roomNightNetPrice":2549.2053404255,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26629,"name":"Ramada Powai Hotel And Convention Centre"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":6571.4358327192,"roomNightTaxes":967,"roomNightOriginalPrice":6571.4358327192,"agentPricing":{"roomNightNetPrice":6497.792334459,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":4614,"name":"Strand Hotel"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3997.1440808511,"roomNightTaxes":42.707910638298,"roomNightOriginalPrice":3997.1440808511,"agentPricing":{"roomNightNetPrice":3997.1440808511,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26118,"name":"Holiday Inn Mumbai International Airport"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":12908.991230815,"roomNightTaxes":2626,"roomNightOriginalPrice":12908.991230815,"agentPricing":{"roomNightNetPrice":12836.354257556,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":228874,"name":"Hotel Venkat Presidency"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":2726.7942978723,"roomNightTaxes":29.134723404255,"roomNightOriginalPrice":2726.7942978723,"agentPricing":{"roomNightNetPrice":2726.7942978723,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26634,"name":"Hotel Sea Lord"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":1835.3899776462,"roomNightTaxes":201,"roomNightOriginalPrice":1835.3899776462,"agentPricing":{"roomNightNetPrice":1807.9037194077,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":961026,"name":"Apex Executive"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":1582.7084893617,"roomNightTaxes":16.910617021277,"roomNightOriginalPrice":1582.7084893617,"agentPricing":{"roomNightNetPrice":1582.7084893617,"roomNightCommission":0},"deal":{"dealText":"Last Minute Special.  Rate includes 20% discount!"}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":23565,"name":"Lalit Mumbai"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":22062.31656202,"roomNightTaxes":4489,"roomNightOriginalPrice":22062.31656202,"agentPricing":{"roomNightNetPrice":21939.37600216,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":893186,"name":"Hotel Supreme Heritage"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":4554.1835896518,"roomNightTaxes":670,"roomNightOriginalPrice":4554.1835896518,"agentPricing":{"roomNightNetPrice":4502.8701493602,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":27663,"name":"InterContinental Marine Drive"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":11149.255352214,"roomNightTaxes":2268,"roomNightOriginalPrice":11149.255352214,"agentPricing":{"roomNightNetPrice":11086.904120014,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":665114,"name":"Mango Hotel Navi Mumbai"},"lowestPriceData":{"searcherId":7,"pricing":{"roomNightSellPrice":4449.2222887888,"roomNightTaxes":31.805401587847,"roomNightOriginalPrice":4449.2222887888,"agentPricing":{"roomNightNetPrice":4449.2222887888,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":962079,"name":"Hotel Shelter Palace"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":2119.4540425532,"roomNightTaxes":22.645531914894,"roomNightOriginalPrice":2119.4540425532,"agentPricing":{"roomNightNetPrice":2119.4540425532,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26385,"name":"Hotel New Bengal"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":1961.7725678421,"roomNightTaxes":215,"roomNightOriginalPrice":1961.7725678421,"agentPricing":{"roomNightNetPrice":1932.377077906,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26129,"name":"The Gordon House"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":5931.0213172322,"roomNightTaxes":872,"roomNightOriginalPrice":5931.0213172322,"agentPricing":{"roomNightNetPrice":5864.1939221735,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":231184,"name":"The Taj Mahal Palace And Tower"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":12942.816271234,"roomNightTaxes":2326,"roomNightOriginalPrice":12942.816271234,"agentPricing":{"roomNightNetPrice":12131.354300785,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":112151,"name":"Hilton Mumbai International Airport"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":16955.003172232,"roomNightTaxes":3998,"roomNightOriginalPrice":16955.003172232,"agentPricing":{"roomNightNetPrice":16914.441895436,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":228884,"name":"Pallavi Avida"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":1779.1692978723,"roomNightTaxes":19.009723404255,"roomNightOriginalPrice":1779.1692978723,"agentPricing":{"roomNightNetPrice":1779.1692978723,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":27674,"name":"Renaissance Mumbai Convention Centre Hotel"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":9390.4818122252,"roomNightTaxes":1911,"roomNightOriginalPrice":9390.4818122252,"agentPricing":{"roomNightNetPrice":9337.4137029147,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26139,"name":"Hotel hyatt Regency"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":10248.116305884,"roomNightTaxes":2085,"roomNightOriginalPrice":10248.116305884,"agentPricing":{"roomNightNetPrice":10190.498722871,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":228888,"name":"Hotel Solitaire"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":5044.7415021277,"roomNightTaxes":53.901076595745,"roomNightOriginalPrice":5044.7415021277,"agentPricing":{"roomNightNetPrice":5044.7415021277,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":61982,"name":"Fortune Select Exotica Navi Mumbai"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":5753.4646981243,"roomNightTaxes":846,"roomNightOriginalPrice":5753.4646981243,"agentPricing":{"roomNightNetPrice":5688.1975381009,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":868627,"name":"JW Marriott Hotel Mumbai - Juhu"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":25349.638364114,"roomNightTaxes":5157,"roomNightOriginalPrice":25349.638364114,"agentPricing":{"roomNightNetPrice":25208.169373384,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":875538,"name":"Hotel Beach Garden"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":5599.283587234,"roomNightTaxes":59.826140425532,"roomNightOriginalPrice":5599.283587234,"agentPricing":{"roomNightNetPrice":5599.283587234,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":233763,"name":"Hotel Regal Enclave"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":5819.724012766,"roomNightTaxes":62.181459574468,"roomNightOriginalPrice":5819.724012766,"agentPricing":{"roomNightNetPrice":5819.724012766,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":62498,"name":"Dragonfly Business Hotel"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":10239.457765957,"roomNightTaxes":109.40457446809,"roomNightOriginalPrice":10239.457765957,"agentPricing":{"roomNightNetPrice":10239.457765957,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":229927,"name":"Hotel Shilpa Residency"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":4178.5396680851,"roomNightTaxes":44.64605106383,"roomNightOriginalPrice":4178.5396680851,"agentPricing":{"roomNightNetPrice":4178.5396680851,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":241958,"name":"Royal Inn"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":5481.1853101357,"roomNightTaxes":806,"roomNightOriginalPrice":5481.1853101357,"agentPricing":{"roomNightNetPrice":5419.4495295569,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":22565,"name":"Midtown Pritam Hotel"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":7219.2411319149,"roomNightTaxes":77.13474893617,"roomNightOriginalPrice":7219.2411319149,"agentPricing":{"roomNightNetPrice":7219.2411319149,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":27175,"name":"Hotel Cosmo"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":1204.7768212766,"roomNightTaxes":12.872565957447,"roomNightOriginalPrice":1204.7768212766,"agentPricing":{"roomNightNetPrice":1204.7768212766,"roomNightCommission":0},"deal":{"dealText":"Last Minute Special.  Rate includes 35% discount!"}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":237098,"name":"Hotel Kohinoor Park"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":4679.3588085106,"roomNightTaxes":49.997106382979,"roomNightOriginalPrice":4679.3588085106,"agentPricing":{"roomNightNetPrice":4679.3588085106,"roomNightCommission":0},"deal":{"dealText":"Last Minute Special.  Rate includes 10% discount!"}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":230698,"name":"Hotel Satellite"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3173.0947574468,"roomNightTaxes":33.903268085106,"roomNightOriginalPrice":3173.0947574468,"agentPricing":{"roomNightNetPrice":3173.0947574468,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":868646,"name":"Hotel Evergreen"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":1849.1188085106,"roomNightTaxes":19.757106382979,"roomNightOriginalPrice":1849.1188085106,"agentPricing":{"roomNightNetPrice":1849.1188085106,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":27693,"name":"The Orchid Hotel"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":15356.890842553,"roomNightTaxes":164.08233191489,"roomNightOriginalPrice":15356.890842553,"agentPricing":{"roomNightNetPrice":15356.890842553,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":20018,"name":"Ramee Guestline Hotel, Khar"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":4407.2600510638,"roomNightTaxes":47.089838297872,"roomNightOriginalPrice":4407.2600510638,"agentPricing":{"roomNightNetPrice":4407.2600510638,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":1208865,"name":"Hotel Yogi Midtown"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":4100.3987044987,"roomNightTaxes":604,"roomNightOriginalPrice":4100.3987044987,"agentPricing":{"roomNightNetPrice":4053.9023577527,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":25399,"name":"Hotel Suba Palace"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":13529.558,"roomNightTaxes":144.558,"roomNightOriginalPrice":13529.558,"agentPricing":{"roomNightNetPrice":13529.558,"roomNightCommission":0},"deal":{"dealText":"Last Minute Special.  Rate includes 20% discount!"}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":232756,"name":"Privilege Inn"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":4134.7741787234,"roomNightTaxes":44.178434042553,"roomNightOriginalPrice":4134.7741787234,"agentPricing":{"roomNightNetPrice":4134.7741787234,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":230969,"name":"Hotel Marol Residency"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":2571.6829393925,"roomNightTaxes":328,"roomNightOriginalPrice":2571.6829393925,"agentPricing":{"roomNightNetPrice":2537.6288121845,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26426,"name":"Hotel Tip Top"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":1798.4712765957,"roomNightTaxes":19.215957446809,"roomNightOriginalPrice":1798.4712765957,"agentPricing":{"roomNightNetPrice":1798.4712765957,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":151097,"name":"Ibis Mumbai Airport"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":8150.1828603284,"roomNightTaxes":1220,"roomNightOriginalPrice":8150.1828603284,"agentPricing":{"roomNightNetPrice":8132.8158933993,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":20028,"name":"Executive Enclave"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":6232.3510096971,"roomNightTaxes":917,"roomNightOriginalPrice":6232.3510096971,"agentPricing":{"roomNightNetPrice":6161.872737644,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":1209135,"name":"Hotel Woodland"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":2721.0413404255,"roomNightTaxes":29.073255319149,"roomNightOriginalPrice":2721.0413404255,"agentPricing":{"roomNightNetPrice":2721.0413404255,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26174,"name":"Hotel Royal Castle(Wi Fi Enabled)"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":4206.9592700893,"roomNightTaxes":619,"roomNightOriginalPrice":4206.9592700893,"agentPricing":{"roomNightNetPrice":4159.3147273683,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":1208914,"name":"Lucky Hotel - Bandra"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3478.7004595745,"roomNightTaxes":37.168544680851,"roomNightOriginalPrice":3478.7004595745,"agentPricing":{"roomNightNetPrice":3478.7004595745,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":234304,"name":"GCC Hotel and Club"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3772.5529234043,"roomNightTaxes":40.308242553192,"roomNightOriginalPrice":3772.5529234043,"agentPricing":{"roomNightNetPrice":3772.5529234043,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":951117,"name":"Hotel Sea View"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":2764.5917659574,"roomNightTaxes":29.538574468085,"roomNightOriginalPrice":2764.5917659574,"agentPricing":{"roomNightNetPrice":2764.5917659574,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":234566,"name":"Novotel Mumbai Juhu Beach"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":10518.926082308,"roomNightTaxes":2140,"roomNightOriginalPrice":10518.926082308,"agentPricing":{"roomNightNetPrice":10459.813991773,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26951,"name":"The Ambassador"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":7520.8896595745,"roomNightTaxes":80.357744680851,"roomNightOriginalPrice":7520.8896595745,"agentPricing":{"roomNightNetPrice":7520.8896595745,"roomNightCommission":0},"deal":{"dealText":"Last Minute Special.  Rate includes 12% discount!"}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":234564,"name":"Vivanta by Taj - President"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":8323.1302581297,"roomNightTaxes":1081,"roomNightOriginalPrice":8323.1302581297,"agentPricing":{"roomNightNetPrice":7737.0377092296,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":234571,"name":"Marriott Executive Apartments, Lakeside Chalet Mumbai"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":8796.8956212766,"roomNightTaxes":93.991365957447,"roomNightOriginalPrice":8796.8956212766,"agentPricing":{"roomNightNetPrice":8796.8956212766,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":881989,"name":"Hotel City Point"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":4010.392012766,"roomNightTaxes":42.849459574468,"roomNightOriginalPrice":4010.392012766,"agentPricing":{"roomNightNetPrice":4010.392012766,"roomNightCommission":0},"deal":{"dealText":"Limited time offer. Rate includes 8% discount!"}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":62025,"name":"Travellers Inn"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":740.92715319149,"roomNightTaxes":7.916514893617,"roomNightOriginalPrice":740.92715319149,"agentPricing":{"roomNightNetPrice":740.92715319149,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":234569,"name":"Four Seasons Hotel Mumbai"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":14069.514629301,"roomNightTaxes":2863,"roomNightOriginalPrice":14069.514629301,"agentPricing":{"roomNightNetPrice":13990.924737221,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":234319,"name":"The Westin Mumbai Garden City"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":19363.038919573,"roomNightTaxes":3939,"roomNightOriginalPrice":19363.038919573,"agentPricing":{"roomNightNetPrice":19255.656598474,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":61516,"name":"Hotel Sea Sands"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":2988.5054723404,"roomNightTaxes":31.931004255319,"roomNightOriginalPrice":2988.5054723404,"agentPricing":{"roomNightNetPrice":2988.5054723404,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":148046,"name":"Hotel Parle International"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":6452.9794595745,"roomNightTaxes":68.947544680851,"roomNightOriginalPrice":6452.9794595745,"agentPricing":{"roomNightNetPrice":6452.9794595745,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":151119,"name":"Ramada Plaza Palm Grove"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":11903.90126383,"roomNightTaxes":127.18849787234,"roomNightOriginalPrice":11903.90126383,"agentPricing":{"roomNightNetPrice":11903.90126383,"roomNightCommission":0},"deal":{"dealText":"Last Minute Special.  Rate includes 25% discount!"}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":1208898,"name":"Hotel Sai Sharan Stay Inn"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":1991.2222340426,"roomNightTaxes":21.275425531915,"roomNightOriginalPrice":1991.2222340426,"agentPricing":{"roomNightNetPrice":1991.2222340426,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":61521,"name":"Metro Palace"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3774.5928745576,"roomNightTaxes":555,"roomNightOriginalPrice":3774.5928745576,"agentPricing":{"roomNightNetPrice":3732.184892575,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":234578,"name":"Z Luxury Residences"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":8086.7668231085,"roomNightTaxes":1189,"roomNightOriginalPrice":8086.7668231085,"agentPricing":{"roomNightNetPrice":7996.5420568005,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26961,"name":"Hotel City Palace"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3595.9985940626,"roomNightTaxes":529,"roomNightOriginalPrice":3595.9985940626,"agentPricing":{"roomNightNetPrice":3555.0787102273,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":21585,"name":"Hotel Suba International"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":13522.858761702,"roomNightTaxes":144.4864212766,"roomNightOriginalPrice":13522.858761702,"agentPricing":{"roomNightNetPrice":13522.858761702,"roomNightCommission":0},"deal":{"dealText":"Last Minute Special.  Rate includes 20% discount!"}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":12116,"name":"Hotel Bawa Continental"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":7789.8783270146,"roomNightTaxes":1146,"roomNightOriginalPrice":7789.8783270146,"agentPricing":{"roomNightNetPrice":7702.5436652562,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":232533,"name":"Eden Guest House"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":573.81764173724,"roomNightTaxes":13,"roomNightOriginalPrice":573.81764173724,"agentPricing":{"roomNightNetPrice":560.23277191149,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":61782,"name":"The Leela Mumbai"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":15133.77287234,"roomNightTaxes":161.69840425532,"roomNightOriginalPrice":15133.77287234,"agentPricing":{"roomNightNetPrice":15133.77287234,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":61529,"name":"Milan International"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":1846.0659797997,"roomNightTaxes":202,"roomNightOriginalPrice":1846.0659797997,"agentPricing":{"roomNightNetPrice":1818.2523848125,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":228954,"name":"The Resort"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":6295.3806851064,"roomNightTaxes":67.263663829787,"roomNightOriginalPrice":6295.3806851064,"agentPricing":{"roomNightNetPrice":6295.3806851064,"roomNightCommission":0},"deal":{"dealText":"Last Minute Special.  Rate includes 10% discount!"}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":234585,"name":"Sofitel Mumbai BKC"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":15098.264954431,"roomNightTaxes":3072,"roomNightOriginalPrice":15098.264954431,"agentPricing":{"roomNightNetPrice":15014.187117038,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":869974,"name":"The Roa Hotel"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":4793.5014564947,"roomNightTaxes":705,"roomNightOriginalPrice":4793.5014564947,"agentPricing":{"roomNightNetPrice":4739.5737502617,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":62302,"name":"Ginger Mumbai Mahakali"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":4619.211609938,"roomNightTaxes":940,"roomNightOriginalPrice":4619.211609938,"agentPricing":{"roomNightNetPrice":4592.7396897732,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":234589,"name":"Regent Hotel"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":7550.2145730427,"roomNightTaxes":1111,"roomNightOriginalPrice":7550.2145730427,"agentPricing":{"roomNightNetPrice":7465.4701315964,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":228957,"name":"Meluha - The Fern"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":9239.5850144325,"roomNightTaxes":1880,"roomNightOriginalPrice":9239.5850144325,"agentPricing":{"roomNightNetPrice":9187.7914592861,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":21604,"name":"Hotel Diplomat"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":4315.2404338654,"roomNightTaxes":635,"roomNightOriginalPrice":4315.2404338654,"agentPricing":{"roomNightNetPrice":4266.567308947,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":11878,"name":"Ramee Guestline Hotel Juhu"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":7224.342257787,"roomNightTaxes":1063,"roomNightOriginalPrice":7224.342257787,"agentPricing":{"roomNightNetPrice":7143.6815591305,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26475,"name":"Ajanta Hotel"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":5161.0157617021,"roomNightTaxes":55.143421276596,"roomNightOriginalPrice":5161.0157617021,"agentPricing":{"roomNightNetPrice":5161.0157617021,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":228968,"name":"Astoria Hotel"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":6920.8841045883,"roomNightTaxes":1409,"roomNightOriginalPrice":6920.8841045883,"agentPricing":{"roomNightNetPrice":6882.2289888645,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":236140,"name":"Sukh Hotel"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3834.6047001796,"roomNightTaxes":564,"roomNightOriginalPrice":3834.6047001796,"agentPricing":{"roomNightNetPrice":3792.0905884274,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":872546,"name":"Nice Guest House"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":1977.8674683505,"roomNightTaxes":216,"roomNightOriginalPrice":1977.8674683505,"agentPricing":{"roomNightNetPrice":1948.5213565246,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":875647,"name":"Citizen Hotel"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":4315.2404338654,"roomNightTaxes":635,"roomNightOriginalPrice":4315.2404338654,"agentPricing":{"roomNightNetPrice":4266.567308947,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":234609,"name":"Shahana Guest House"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":383.78529388666,"roomNightTaxes":9,"roomNightOriginalPrice":383.78529388666,"agentPricing":{"roomNightNetPrice":374.10191859536,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":21619,"name":"Landmark Suites"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":5930.7130914903,"roomNightTaxes":872,"roomNightOriginalPrice":5930.7130914903,"agentPricing":{"roomNightNetPrice":5864.9337876902,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26743,"name":"Hotel Fortune"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3178.4699952408,"roomNightTaxes":468,"roomNightOriginalPrice":3178.4699952408,"agentPricing":{"roomNightNetPrice":3142.7486580115,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":21624,"name":"The Mirador"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":7550.2145730427,"roomNightTaxes":1111,"roomNightOriginalPrice":7550.2145730427,"agentPricing":{"roomNightNetPrice":7465.4701315964,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":27000,"name":"Hotel Columbus"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3297.3371319149,"roomNightTaxes":35.23074893617,"roomNightOriginalPrice":3297.3371319149,"agentPricing":{"roomNightNetPrice":3297.3371319149,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":234618,"name":"iHOTEL"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":1714.4205773611,"roomNightTaxes":219,"roomNightOriginalPrice":1714.4205773611,"agentPricing":{"roomNightNetPrice":1691.3589062686,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":234616,"name":"Hotel Planet Plaza"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":1701.5850212766,"roomNightTaxes":18.180765957447,"roomNightOriginalPrice":1701.5850212766,"agentPricing":{"roomNightNetPrice":1701.5850212766,"roomNightCommission":0},"deal":{"dealText":"Last Minute Special.  Rate includes 40% discount!"}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":253053,"name":"Hotel Parklane"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3376.2168240991,"roomNightTaxes":497,"roomNightOriginalPrice":3376.2168240991,"agentPricing":{"roomNightNetPrice":3338.1998118707,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":1319060,"name":"Z Luxury Suites & Serviced Apartments"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":8258.7951659574,"roomNightTaxes":88.241974468085,"roomNightOriginalPrice":8258.7951659574,"agentPricing":{"roomNightNetPrice":8258.7951659574,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":234625,"name":"Royal Tulip Navi Mumbai"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":5402.3291618924,"roomNightTaxes":795,"roomNightOriginalPrice":5402.3291618924,"agentPricing":{"roomNightNetPrice":5341.5285153929,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":9602,"name":"Arma Court"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":2123.7015531915,"roomNightTaxes":22.690914893617,"roomNightOriginalPrice":2123.7015531915,"agentPricing":{"roomNightNetPrice":2123.7015531915,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26498,"name":"The Shalimar Hotel"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":10303.29946383,"roomNightTaxes":110.08669787234,"roomNightOriginalPrice":10303.29946383,"agentPricing":{"roomNightNetPrice":10303.29946383,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":886415,"name":"Hotel K Stars"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3281.6374723404,"roomNightTaxes":35.063004255319,"roomNightOriginalPrice":3281.6374723404,"agentPricing":{"roomNightNetPrice":3281.6374723404,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":1208983,"name":"The Oberoi"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":11855.736353015,"roomNightTaxes":2402,"roomNightOriginalPrice":11855.736353015,"agentPricing":{"roomNightNetPrice":11763.354388251,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":253062,"name":"Hotel Tuliip Residency"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":2427.5759914894,"roomNightTaxes":25.937693617021,"roomNightOriginalPrice":2427.5759914894,"agentPricing":{"roomNightNetPrice":2427.5759914894,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":881053,"name":"Hotel Sharanam"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3153.7612365032,"roomNightTaxes":625,"roomNightOriginalPrice":3153.7612365032,"agentPricing":{"roomNightNetPrice":3133.4344775435,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":875165,"name":"Hotel The Cottage."},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3441.4083914894,"roomNightTaxes":36.770093617021,"roomNightOriginalPrice":3441.4083914894,"agentPricing":{"roomNightNetPrice":3441.4083914894,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":875164,"name":"Hotel Krishna Avtar Stay Inn"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":2084.6137021277,"roomNightTaxes":22.273276595745,"roomNightOriginalPrice":2084.6137021277,"agentPricing":{"roomNightNetPrice":2084.6137021277,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":949657,"name":"Hotel Elphinstone"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":2453.7821951171,"roomNightTaxes":269,"roomNightOriginalPrice":2453.7821951171,"agentPricing":{"roomNightNetPrice":2416.8793530665,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":21912,"name":"Hotel Suncity Apollo"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":5393.09435611,"roomNightTaxes":794,"roomNightOriginalPrice":5393.09435611,"agentPricing":{"roomNightNetPrice":5332.7212364812,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":232346,"name":"Hotel Midland"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":2653.2639744681,"roomNightTaxes":28.349080851064,"roomNightOriginalPrice":2653.2639744681,"agentPricing":{"roomNightNetPrice":2653.2639744681,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":949650,"name":"AIFA Residency"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":1929.5194105263,"roomNightTaxes":246,"roomNightOriginalPrice":1929.5194105263,"agentPricing":{"roomNightNetPrice":1903.2293160709,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":235934,"name":"The Retreat Hotel And Convention"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":7047.1603497355,"roomNightTaxes":1037,"roomNightOriginalPrice":7047.1603497355,"agentPricing":{"roomNightNetPrice":6968.085935546,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":12189,"name":"Hotel Juhu Plaza"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":6669.8820851064,"roomNightTaxes":71.265063829787,"roomNightOriginalPrice":6669.8820851064,"agentPricing":{"roomNightNetPrice":6669.8820851064,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":875155,"name":"Hotel Grand Inn"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":1914.1971234043,"roomNightTaxes":20.452442553191,"roomNightOriginalPrice":1914.1971234043,"agentPricing":{"roomNightNetPrice":1914.1971234043,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":949649,"name":"Hotel Aarya International"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":4117.0206595745,"roomNightTaxes":43.988744680851,"roomNightOriginalPrice":4117.0206595745,"agentPricing":{"roomNightNetPrice":4117.0206595745,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":961169,"name":"Hotel Right Choice Inn"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":1305.2546425532,"roomNightTaxes":13.946131914894,"roomNightOriginalPrice":1305.2546425532,"agentPricing":{"roomNightNetPrice":1305.2546425532,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26529,"name":"Hotel Park View"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":4673.0789446809,"roomNightTaxes":49.930008510638,"roomNightOriginalPrice":4673.0789446809,"agentPricing":{"roomNightNetPrice":4673.0789446809,"roomNightCommission":0},"deal":{"dealText":"Last Minute Special.  Rate includes 10% discount!"}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":1209009,"name":"Hotel Oasis Fort"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":2567.3997404255,"roomNightTaxes":27.431655319149,"roomNightOriginalPrice":2567.3997404255,"agentPricing":{"roomNightNetPrice":2567.3997404255,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":949677,"name":"Hotel Ocean Residency"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":2493.9231829787,"roomNightTaxes":26.646587234043,"roomNightOriginalPrice":2493.9231829787,"agentPricing":{"roomNightNetPrice":2493.9231829787,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26789,"name":"Hotel Avon Ruby"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3618.4381829787,"roomNightTaxes":38.661587234043,"roomNightOriginalPrice":3618.4381829787,"agentPricing":{"roomNightNetPrice":3618.4381829787,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":145063,"name":"Svenska Mumbai"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":7009.5670137349,"roomNightTaxes":1031,"roomNightOriginalPrice":7009.5670137349,"agentPricing":{"roomNightNetPrice":6931.0877152245,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":12198,"name":"Tunga International"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":6291.3628353192,"roomNightTaxes":925,"roomNightOriginalPrice":6291.3628353192,"agentPricing":{"roomNightNetPrice":6220.7089147798,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":253093,"name":"The Fern Residency"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":10022.29706383,"roomNightTaxes":107.08429787234,"roomNightOriginalPrice":10022.29706383,"agentPricing":{"roomNightNetPrice":10022.29706383,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":63144,"name":"ITC Maratha Mumbai, A Luxury Collection Hotel"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":15608.710069633,"roomNightTaxes":3176,"roomNightOriginalPrice":15608.710069633,"agentPricing":{"roomNightNetPrice":15521.615047736,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":253099,"name":"Hotel Vihangs Inn"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":5661.7811361702,"roomNightTaxes":60.49390212766,"roomNightOriginalPrice":5661.7811361702,"agentPricing":{"roomNightNetPrice":5661.7811361702,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":63145,"name":"ITC Grand Central"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":19269.719148936,"roomNightTaxes":205.88936170213,"roomNightOriginalPrice":19269.719148936,"agentPricing":{"roomNightNetPrice":19269.719148936,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":253098,"name":"U Tan Farmhouse"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3577.6405744681,"roomNightTaxes":38.225680851064,"roomNightOriginalPrice":3577.6405744681,"agentPricing":{"roomNightNetPrice":3577.6405744681,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":9646,"name":"Hotel Bawa International"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":10265.798833949,"roomNightTaxes":2089,"roomNightOriginalPrice":10265.798833949,"agentPricing":{"roomNightNetPrice":10208.340998876,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":233901,"name":"Goldfinch Hotel (Ex Aura Grande), Mumbai"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":8086.7668231085,"roomNightTaxes":1189,"roomNightOriginalPrice":8086.7668231085,"agentPricing":{"roomNightNetPrice":7996.5420568005,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":4526,"name":"Tunga Paradise"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":9700.8561007222,"roomNightTaxes":1974,"roomNightOriginalPrice":9700.8561007222,"agentPricing":{"roomNightNetPrice":9645.8354018419,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":116142,"name":"Kohinoor Elite"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":21812.379932145,"roomNightTaxes":4438,"roomNightOriginalPrice":21812.379932145,"agentPricing":{"roomNightNetPrice":21690.245916733,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":4528,"name":"Sun N Sand Hotel Mumbai"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":11028.342373583,"roomNightTaxes":2244,"roomNightOriginalPrice":11028.342373583,"agentPricing":{"roomNightNetPrice":10967.211094741,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":887228,"name":"LA Hotel Metro"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":4439.2041257063,"roomNightTaxes":653,"roomNightOriginalPrice":4439.2041257063,"agentPricing":{"roomNightNetPrice":4389.5231290976,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":1208998,"name":"Courtyard by Marriott"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":12792.335355982,"roomNightTaxes":2603,"roomNightOriginalPrice":12792.335355982,"agentPricing":{"roomNightNetPrice":12721.214284473,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":4534,"name":"West End Hotel"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":6202.9259941644,"roomNightTaxes":913,"roomNightOriginalPrice":6202.9259941644,"agentPricing":{"roomNightNetPrice":6133.6106889459,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":968374,"name":"Metro Dormitory"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":519.3357958168,"roomNightTaxes":12,"roomNightOriginalPrice":519.3357958168,"agentPricing":{"roomNightNetPrice":507.31101156877,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26811,"name":"JC Chalet"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":2866.9513957447,"roomNightTaxes":30.632246808511,"roomNightOriginalPrice":2866.9513957447,"agentPricing":{"roomNightNetPrice":2866.9513957447,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":63164,"name":"Trident Bandra Kurla"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":17976.434235813,"roomNightTaxes":3545,"roomNightOriginalPrice":17976.434235813,"agentPricing":{"roomNightNetPrice":17605.812016913,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26814,"name":"Hotel Airlines International"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":2686.636567573,"roomNightTaxes":294,"roomNightOriginalPrice":2686.636567573,"agentPricing":{"roomNightNetPrice":2645.6006070299,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":888268,"name":"Lalco Residency"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":7885.0422922662,"roomNightTaxes":1160,"roomNightOriginalPrice":7885.0422922662,"agentPricing":{"roomNightNetPrice":7796.8366762206,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":107968,"name":"Four Points by Sheraton Navi Mumbai Vashi"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":8621.6628402437,"roomNightTaxes":1779.5634627171,"roomNightOriginalPrice":8621.6628402437,"agentPricing":{"roomNightNetPrice":8621.6628402437,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":22466,"name":"Hotel Manama"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3369.7979257441,"roomNightTaxes":496,"roomNightOriginalPrice":3369.7979257441,"agentPricing":{"roomNightNetPrice":3331.3346799402,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":897231,"name":"Hotel Suncity Premiere"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":5480.2780170213,"roomNightTaxes":58.554612765957,"roomNightOriginalPrice":5480.2780170213,"agentPricing":{"roomNightNetPrice":5480.2780170213,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":949709,"name":"Royal Park Hotel"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":2835.1870695031,"roomNightTaxes":310,"roomNightOriginalPrice":2835.1870695031,"agentPricing":{"roomNightNetPrice":2792.7134433188,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":22468,"name":"Sahara Star"},"lowestPriceData":{"searcherId":7,"pricing":{"roomNightSellPrice":16896.461164718,"roomNightTaxes":120.78486932682,"roomNightOriginalPrice":16896.461164718,"agentPricing":{"roomNightNetPrice":16896.461164718,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":949706,"name":"Regency Hotel"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":5661.9037238001,"roomNightTaxes":833,"roomNightOriginalPrice":5661.9037238001,"agentPricing":{"roomNightNetPrice":5598.8275120857,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26308,"name":"Hotel Krishna Palace"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":6390.5141702128,"roomNightTaxes":68.280127659574,"roomNightOriginalPrice":6390.5141702128,"agentPricing":{"roomNightNetPrice":6390.5141702128,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":241860,"name":"Causeway Hotel"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":2815.1317659574,"roomNightTaxes":30.078574468085,"roomNightOriginalPrice":2815.1317659574,"agentPricing":{"roomNightNetPrice":2815.1317659574,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26825,"name":"Hotel Shubhangan"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3757.8318042553,"roomNightTaxes":40.150953191489,"roomNightOriginalPrice":3757.8318042553,"agentPricing":{"roomNightNetPrice":3757.8318042553,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":1319135,"name":"The E Hotel at Eskay Resorts"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3918.914612766,"roomNightTaxes":41.872059574468,"roomNightOriginalPrice":3918.914612766,"agentPricing":{"roomNightNetPrice":3918.914612766,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":239567,"name":"Hotel Konark Inn"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":1890.5194105263,"roomNightTaxes":207,"roomNightOriginalPrice":1890.5194105263,"agentPricing":{"roomNightNetPrice":1861.5180861243,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":902848,"name":"Aboos Hotel Concord Galaxy"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3602.3083957447,"roomNightTaxes":38.489246808511,"roomNightOriginalPrice":3602.3083957447,"agentPricing":{"roomNightNetPrice":3602.3083957447,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":966848,"name":"JW Marriott Mumbai Sahar"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":19833.47832633,"roomNightTaxes":4035,"roomNightOriginalPrice":19833.47832633,"agentPricing":{"roomNightNetPrice":19722.436712653,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":25038,"name":"Grand Hyatt Mumbai"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":15061.487525858,"roomNightTaxes":3064,"roomNightOriginalPrice":15061.487525858,"agentPricing":{"roomNightNetPrice":14976.992006265,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":878531,"name":"Hotel Aircraft International"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3230.3423243223,"roomNightTaxes":475,"roomNightOriginalPrice":3230.3423243223,"agentPricing":{"roomNightNetPrice":3193.9490099704,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":1209052,"name":"Hotel Sea N Rock"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":4439.1376403917,"roomNightTaxes":653,"roomNightOriginalPrice":4439.1376403917,"agentPricing":{"roomNightNetPrice":4388.3825030928,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":949696,"name":"Landmark Residency"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3835.6335241071,"roomNightTaxes":564,"roomNightOriginalPrice":3835.6335241071,"agentPricing":{"roomNightNetPrice":3792.1214161573,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":21967,"name":"Royal Orchid Central Grazia"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":4902.6226015018,"roomNightTaxes":864,"roomNightOriginalPrice":4902.6226015018,"agentPricing":{"roomNightNetPrice":4861.628450804,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26836,"name":"Hotel Galaxy Residency"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":1890.5194105263,"roomNightTaxes":207,"roomNightOriginalPrice":1890.5194105263,"agentPricing":{"roomNightNetPrice":1861.5180861243,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":1319105},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3082.3270680851,"roomNightTaxes":32.93345106383,"roomNightOriginalPrice":3082.3270680851,"agentPricing":{"roomNightNetPrice":3082.3270680851,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":228822,"name":"Grand Residency Hotel & Serviced Apartments"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":53403.6273819,"roomNightTaxes":10865,"roomNightOriginalPrice":53403.6273819,"agentPricing":{"roomNightNetPrice":53106.553349626,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26587,"name":"Bentleys Hotel"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":2858.6343830519,"roomNightTaxes":313,"roomNightOriginalPrice":2858.6343830519,"agentPricing":{"roomNightNetPrice":2815.6517465796,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":241891,"name":"Abbott Hotel"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":4315.2404338654,"roomNightTaxes":635,"roomNightOriginalPrice":4315.2404338654,"agentPricing":{"roomNightNetPrice":4266.567308947,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":10978,"name":"Fariyas Mumbai."},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":6684.4152966647,"roomNightTaxes":983,"roomNightOriginalPrice":6684.4152966647,"agentPricing":{"roomNightNetPrice":6610.069836005,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":10980,"name":"VITS Mumbai"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":9417.2687446809,"roomNightTaxes":100.61980851064,"roomNightOriginalPrice":9417.2687446809,"agentPricing":{"roomNightNetPrice":9417.2687446809,"roomNightCommission":0},"deal":{"dealText":"Limited time offer. Rate includes 20% discount!"}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":241892,"name":"Hotel Accord"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3989.3681186097,"roomNightTaxes":587,"roomNightOriginalPrice":3989.3681186097,"agentPricing":{"roomNightNetPrice":3944.778736481,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":253163,"name":"The Regenza by Tunga"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":7789.8783270146,"roomNightTaxes":1146,"roomNightOriginalPrice":7789.8783270146,"agentPricing":{"roomNightNetPrice":7702.5436652562,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":10986,"name":"Bawa Suites"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":6590.6925277841,"roomNightTaxes":969,"roomNightOriginalPrice":6590.6925277841,"agentPricing":{"roomNightNetPrice":6517.3182115338,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":4586,"name":"Hotel Silver Inn"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":5152.7764892671,"roomNightTaxes":758,"roomNightOriginalPrice":5152.7764892671,"agentPricing":{"roomNightNetPrice":5094.9481168632,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":880358,"name":"Ibis Navi Mumbai"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":4501.1086756112,"roomNightTaxes":674,"roomNightOriginalPrice":4501.1086756112,"agentPricing":{"roomNightNetPrice":4491.0253215093,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":228847,"name":"Hotel Devanshi Inn"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":2743.875319838,"roomNightTaxes":404,"roomNightOriginalPrice":2743.875319838,"agentPricing":{"roomNightNetPrice":2712.1661174738,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":1209062,"name":"City Guest House"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":3958.9164851064,"roomNightTaxes":42.299463829787,"roomNightOriginalPrice":3958.9164851064,"agentPricing":{"roomNightNetPrice":3958.9164851064,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":232437,"name":"The Regale by Tunga"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":8682.1026189253,"roomNightTaxes":1277,"roomNightOriginalPrice":8682.1026189253,"agentPricing":{"roomNightNetPrice":8585.1364908292,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":10999,"name":"Bawa Regency"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":5393.09435611,"roomNightTaxes":794,"roomNightOriginalPrice":5393.09435611,"agentPricing":{"roomNightNetPrice":5332.7212364812,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":233466,"name":"Hotel Planet Residency"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":4314.1739485509,"roomNightTaxes":635,"roomNightOriginalPrice":4314.1739485509,"agentPricing":{"roomNightNetPrice":4265.4266829421,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":872439,"name":"Ascot Inn Hotel"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":2213.5089243063,"roomNightTaxes":242,"roomNightOriginalPrice":2213.5089243063,"agentPricing":{"roomNightNetPrice":2180.223448456,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":61946,"name":"Hotel Broadway"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":2876.0449935339,"roomNightTaxes":423,"roomNightOriginalPrice":2876.0449935339,"agentPricing":{"roomNightNetPrice":2843.8983888063,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":25851,"name":"Ruyale Inn"},"lowestPriceData":{"searcherId":28,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":1584.2031829787,"roomNightTaxes":16.926587234043,"roomNightOriginalPrice":1584.2031829787,"agentPricing":{"roomNightNetPrice":1584.2031829787,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26107,"name":"Grand Hotel Mumbai"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":5273.4830773095,"roomNightTaxes":776,"roomNightOriginalPrice":5273.4830773095,"agentPricing":{"roomNightNetPrice":5214.4204035396,"roomNightCommission":0}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":25852,"name":"Trident, Nariman Point Mumbai"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":9632.9976409306,"roomNightTaxes":1951,"roomNightOriginalPrice":9632.9976409306,"agentPricing":{"roomNightNetPrice":9558.2862469846,"roomNightCommission":0},"deal":{}},"priority":false,"priorityContext":""}},{"hotelInfo":{"hotelId":26621,"name":"The Residence Hotel & Apartments"},"lowestPriceData":{"searcherId":4,"payLater":true,"freeCancellation":false,"pricing":{"roomNightSellPrice":5393.09435611,"roomNightTaxes":794,"roomNightOriginalPrice":5393.09435611,"agentPricing":{"roomNightNetPrice":5332.7212364812,"roomNightCommission":0}},"priority":false,"priorityContext":""}}],"searchInfo":{"checkIn":"2019-06-11","checkOut":"2019-06-12","roomNights":1,"regionId":"5C271","regionName":"Mumbai,Maharashtra,India","rooms":[{"adults":[{}],"children":[]}],"city":"Mumbai","country":"India","state":164938},"Z":{"showSupplier":false,"showEmailFare":true,"A":false,"B":false}}}';
                
            if (file_exists($file_path . $file_name)) {
                $json_data = file_get_contents($file_path . $file_name);               
                         
            }else{
                file_put_contents($file_path . $file_name, $curlResult);
            }
            
            $request_type = $this->input->post('request_type'); 
            
            //show($json_data,1);
            $data['hotelResult'] = '';
            $data['hotelResult'] = json_decode($curlResult); 
            if($data['hotelResult']!=''){
                $data['hotelResultCount'] = count($data['hotelResult']->data->hotelResults);
                $data['searchId']= $data['hotelResult']->data->searchId;
                $data['checkIn'] = $data['hotelResult']->data->searchInfo->checkIn;
                $data['checkOut'] = $data['hotelResult']->data->searchInfo->checkOut;
                
                if(isset($data['hotelResult']->data->hotelResults) && !(empty($data['hotelResult']->data->hotelResults)))
                {
                   foreach ($data['hotelResult']->data->hotelResults as $key => $value) {                    
                        $data['hotelDetails'][] = array(
                            'hotelId' => $value->hotelInfo->hotelId,
                            'hotelName' => $value->hotelInfo->name,
                            'price' => round($value->lowestPriceData->pricing->agentPricing->roomNightNetPrice, 2),
                            
                        );
                    } 
                }
 
                
            }
            
            //show($data,1);

            
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'hotel_booking.log', $curlResult, 'Hotels Services');
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'hotel_booking_input.log', $input, 'Search Hotel Input Data');
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'hotel_booking_data.log', $data, 'Hotel Services_data');
            
            if ($request_type == 'ajax_req' || $rajax == 'rajax') {
                $results = $this->load->view('adminlte/back/admin/hotel_booking/hotel_services_view', $data, true);
                $ajax_request = array('status' => 'success', 'results' => $results);
                echo json_encode($ajax_request);
            } else {
                //load_back_view(HOTEL_SERVICES_VIEW, $data);   
                load_back_view(ADVANCE_SEARCH_HOTEL_VIEW, $data);

                
            }

           
            

        }

       // }
    }
    
    public function get_rooms() {
        
        //show($_POST,1);
        
        $input = array(
            'hotel_id' => $this->input->post('hotel_id'),
            'hotelIds' => '['.$this->input->post('hotel_id').']',
            'searchId' => $this->input->post('search_id'),     
            'hotelName' => $this->input->post('hotel_name'),   
            
       
        );
        
        $hotel_id = $this->input->post('hotel_id');

        $this->session->set_userdata('room_details', $input);
        
        if (!empty($input)) {
            
            $curl_url  = base_url() .  "rest_server/search_hotel/get_rooms";
            $curlResult = curlForPostData_HOTEL($curl_url, $input, $this->rokad_headers);
            //$curlResult = '{"status":"Success","data":}';
            //show($curlResult);
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'curl.log', $curl_url, 'get rooms curl');
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'hotel_booking.log', $curlResult, 'get rooms');
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'hotel_booking_input.log', $input, 'get rooms input');  
            
            $file_name = $this->session->userdata('checkInDate').'_'.$this->input->post('hotel_id') . '.json';
            $file_name = str_replace(' ', '', $file_name);


            $file_path = $_SERVER['DOCUMENT_ROOT'].'/sc_roakad/rokad/json_data/';
           
            
            if (file_exists($file_path . $file_name)) {
                $json_data = file_get_contents($file_path . $file_name); 
                
                
//                if(strtolower($curlResult)== 'failed to connect to 180.92.175.107 port 80: timed out'){
//                    $response = $json_data;
//                }else{
//                    $response = json_decode($json_data);
//                }
                         
            }else{
                file_put_contents($file_path . $file_name, $curlResult);
            }
            $response = json_decode($curlResult);
            //show($response);
            
            $data['rooms']= $response->data->rooms->$hotel_id;
            //show($data,1);
            if(isset($data['rooms'])){
                foreach ($data['rooms'] as $key => $value) {
                
                $data['room_details'][] = array(
                    'room_name' => $value->roomName,
                    'itineraryKey' => $value->ratePlans[0]->itineraryKey,
                    'price' => round($value->ratePlans[0]->pricing->roomNightSellPrice,2),
                    'tax' => round($value->ratePlans[0]->pricing->totalServiceTax,2),
                    'info' => $value->ratePlans[0]->impInfo[0],
                );
                }
            }
            
            //show($data,1);
            echo json_encode($data);
            
            
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'hotel_booking_data.log', $data, 'get rooms_data');
           
            
        }
        
        
    }

    public function reprice(){
        
        
        
        $input = array(            
            'itineraryKey' => $this->input->post('itinerary_key'),    
            'roomName' => $this->input->post('room_name'),   
            'per_room_amt' => $this->input->post('amount'),     
            'room_info' => $this->input->post('room_info'),  
  
        );
        //show($input,1);
        $this->session->set_userdata('reprice_details', $input);
        
        if (!empty($input)) {
            
            $curl_url  = base_url() .  "rest_server/search_hotel/reprice";
            $curlResult = curlForPostData_HOTEL($curl_url, $input, $this->rokad_headers);
            
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'curl.log', $curl_url, 'reprice curl');
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'hotel_booking.log', $curlResult, 'reprice Data');
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'hotel_booking_input.log', $input, 'reprice input');
            
           // $response = json_decode($curlResult);
            //show($response,1);            
            
            $file_name = $this->session->userdata('checkInDate').'_'.$this->input->post('itinerary_key') . '.json';
            $file_name = str_replace(' ', '', $file_name);

            //show($curlResult);
            $file_path = $_SERVER['DOCUMENT_ROOT'].'/sc_roakad/rokad/json_data/';
           
            
//            if (file_exists($file_path . $file_name)) {
//                $json_data = file_get_contents($file_path . $file_name); 
//                
//                
////                if(strtolower($curlResult)== 'failed to connect to 180.92.175.107 port 80: timed out'){
////                    $response = $json_data;
////                }else{
////                    $response = json_decode($json_data);
////                }
//                         
//            }else{
//                file_put_contents($file_path . $file_name, $curlResult);
//            }
            $response = json_decode($curlResult);
            //show($response,1);
            $data['status'] = $response->data->status;
            $data['bookingKey']= $response->data->bookingKey;
            
            if(strtolower($data['status'])=='failed'){
                $data['error'] = $response->data->error;
            }else{
                if(isset($data['bookingKey']) && count($data['bookingKey'])>0)
                {
                    $data['redirect'] = base_url() . "admin/search_hotel/hotel_booking";

                }
            }
           
            
            $this->session->set_userdata('reprice', $data);
            //show($data,1);
            echo json_encode($data);
            
            
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'hotel_booking_data.log', $data, 'reprice data');
           
            
        }
        
        
    }
    
    public function hotel_booking(){
    	load_back_view(HOTEL_BOOKING_VIEW);
    }
    
    public function confirm_booking(){
        
        //show($_POST,1);
        $data['details'] = $_POST['psgr'];
        
        foreach ($data['details'] as $value) {           
            
                $paxDetails['paxDetails'][] = array(
                "title" => $value["title"] ,
                "passengerType" => "ADULT",
                "lastName" => $value["lname"],
                "firstName" => $value["fname"] 
                 ) ;
        }
        
        
      
       $paxDetails = [$paxDetails];
       
        $deliveryData = array(            
            'mobile' => $this->input->post('mobile'), 
            'email' => $this->input->post('email'), 
        );
        


        $input = array (  
            'bookingKey' => $this->input->post('bookingKey'),
            'deliveryData' => json_encode($deliveryData),                  
            'roomPaxDetails' => json_encode($paxDetails),

           // 'block' -
                );
        
        //show($input,1);
        
       

        $this->session->set_userdata('booking_details', $input);
        
        if (!empty($input)) {
            
            $curl_url  = base_url() .  "rest_server/search_hotel/book";
            $curlResult = curlForPostData_HOTEL($curl_url, $input, $this->rokad_headers);
            
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'curl.log', $curl_url, 'book curl');
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'hotel_booking.log', $curlResult, 'book data');
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'hotel_booking_input.log', $input, 'book input');          
            
            $response = json_decode($curlResult);
            //show($response,1);           
                                
            $data['response'] = $response;
//            echo $data['response']->status;
//            show($data['response'],1);
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'hotel_booking_data.log', $data, 'booking data');            
            if(strtolower($data['response']->status) == 'success' ){
                
                if(strtolower($data['response']->data->status) == 'in_progress' ){
                    
                    $cityDetails = $this->Search_hotel_model->getCityDetails($this->session->userdata('city'));            
            
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'city_insert.log', $cityDetails, 'Save hotel_booking response');            
            
                    $bookingInput = array(
                        'city_id' => $this->session->userdata('city'),
                        'city' => $cityDetails['name'],
                        'hotel_id' => $this->session->userdata['room_details']['hotel_id'],
                        'hotel_name' => $this->session->userdata['room_details']['hotelName'],

        //                'hotel_address' => ,
                        'checkin_date' => date("Y-m-d H:i:s", strtotime(trim($this->session->userdata('checkInDate')))),
                        'checkout_date' => date("Y-m-d H:i:s", strtotime(trim($this->session->userdata('checkOutDate')))),
                        'day' =>  $this->session->userdata('total_days'),
                        'room_name' => $this->session->userdata['reprice_details']['roomName'],
                        'total_adult' => '1',
                        'total_child' => '0',
        //                'booking_status'
                        'booking_date' => date('Y-m-d h:i:s'), 
                        'created_by' => $this->sessionData['id'],
                        'created_date' => date('Y-m-d h:i:s'), 
                        'booking_status' => $data['response']->data->status,
                     );
            
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'temp_booking.log', $bookingInput, 'temp_booking Post data');
            
                    $save_booking_data = $this->Search_hotel_model->saveBookingData($bookingInput);
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'hotel_booking_insert.log', $save_booking_data, 'Save hotel_booking response');

                    $hotel_booking_temp_input = array(
                        'hotel_booking_id' => $save_booking_data,
                        'booking_key' => $this->session->userdata['booking_details']['bookingKey'],
                        'search_id' => $this->session->userdata['room_details']['searchId'],
                        'hotel_name' => $this->session->userdata['room_details']['hotelName'],
                        'hotel_id' => $this->session->userdata['room_details']['hotel_id'],
                        'day' => $this->session->userdata('total_days'),

                    );
            
                    $this->Search_hotel_model->saveBookingTempData($hotel_booking_temp_input);
            
                    foreach ($data['details'] as $value) {           

                        $hotel_booking_guest_details[] = array(
                        'hotel_booking_id' => $save_booking_data,
                        "title" => $value["title"] ,
                        "passenger_type" => "ADULT",
                        "last_name" => $value["lname"],
                        "first_name" => $value["fname"] ,
                        'mobile' => $this->input->post('mobile'), 
                        'email' => $this->input->post('email'), 
                        'age' => $value['age'],
                        'sex' => $value['sex'],
                        'room_name' => $this->session->userdata['reprice_details']['roomName'],
                         ) ;
                    }  
            

            
//            show($this->session->userdata);
//            show($bookingInput);        
//            show($hotel_booking_temp_input,1);
            
                $this->Search_hotel_model->batchInsertHotelGuestDetails('hotel_booking_guest_details',$hotel_booking_guest_details);
                $amount = $this->session->userdata['reprice_details']['per_room_amt']; 
              
                
                $wallet_txn_no = substr(hexdec(uniqid()), 4, 12);
                $this->updateWalletTxn($amount, $wallet_txn_no);
            //if(strtolower($data['response']->status) == 'success' ){
               $status =  'Hotel Booking is '.$data['response']->data->status;
            //}
            
                $this->session->set_tempdata('msg', $status, 3);            
                redirect(base_url().'admin/search_hotel');
                }else{
                    $this->session->set_tempdata(array('status' => 'Failed', 'msg' => $data['response']->data), 200);
                    redirect(base_url().'admin/search_hotel');
                }
                
                
                
            }else{
                $this->session->set_tempdata(array('status' => 'Failed', 'msg' => 'Hotel Booking Failed'), 200);
                redirect(base_url().'admin/search_hotel');
            }
        } 
    }
    
    public function print_ticket() {
        load_back_view(PRINT_TICKET);
    }

    /*
    * @Author      : Sonali Kamble
    * @function    : ticket_by_sms_email()
    * @param       : 
    * @detail      : get ticket by sms and email or view and print ticket.
    * @date        : 15-june-19      
    */
    public function ticket_by_sms_email (){
        //booking_retrieve_post
        //is_ajax_request();
        $ticket_dump_data = array();
        $input = $this->input->post();
        //show($input,1);
        if (!empty($input)) {
            /////////////////
        
            //same functionality implemented in two pages. is_history_task used to differentiate.
            $is_history_task = $input["is_history_task"];
            $ticket_mode = $input["ticket_mode"];

            if($is_history_task){
                
               $ticket_ref_no = $input["ticket_ref_no"];
            }
            else{
                
                $input['referenceNo'] = $input["ref_no"];
                $input['emailOrMobile'] = $input["email"] ? $input["email"] : $input["mobile"];
            }

            if($ticket_mode == "print" || $ticket_mode == "sms" || $ticket_mode == "mail"){
                
                if($is_history_task){
                    
                    if($this->session->userdata("user_id") > 0){
                        
                        $ticket_dump_data = $this->tickets_model->get_history_ticket_sms_email($ticket_ref_no,$this->session->userdata("user_id"));
                        $bos_ref_no = $ticket_dump_data[0]->boss_ref_no;
                    }
                }
                else{
                   //$ticket_dump_data =  $this->Search_bus_model->getTicketsDetails($bos_ref_no);

                    $curl_url  = base_url() .  "rest_server/search_hotel/booking_retrieve";
                    $curlResult = curlForPostData_HOTEL($curl_url, $input, $this->rokad_headers);

                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'curl.log', $curl_url, 'booking_retrieve curl');
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'hotel_booking.log', $curlResult, 'booking_retrieve data');
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'hotel_booking_input.log', $input, 'booking_retrieve input');          

                    $response = json_decode($curlResult);
                               

                    $data['response'] = $response;
                    
//                    echo $data['response']->status;                    
                    //show($data['response']->data,1);

                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'hotel_booking_data.log', $data, 'booking_retrieve data');  
                }

                if(strtolower($data['response']->status)== 'success'){
                    
                    
                        if($ticket_mode == "print")
                        {
                            $data['ticket_details'] = $data['response']->data;                        
                            $data["flag"] = '@#success#@';
                            $data['type'] = "print"; 
                        }
                        else if ($ticket_mode == "sms")
                        {
                            //Send ticket on Message. 

                            /* sms send */
                            $flag='';
                            $ticketDetails = $passengerDetails = array();

                            $ticketDetails = $this->Search_bus_model->getTicketsDetails($bos_ref_no);
                            $ticketId = $ticketDetails[0]['ticket_id'];

                            $passengerDetails = $this->Search_bus_model->getPassengerDetails($ticketId);

                            $datas['passenger_ticket'] = $ticketDetails[0];
                            $datas['passenger_ticket_details'] = $passengerDetails;


                            $mobile_array[] = $ticketDetails[0]['mobile_no'];

                            if (empty($flag) || $flag != 'pdf') {
                                $is_send = advaSendTicketBookedSms($datas, $mobile_array);
                                $eticket_count = ($is_send) ? 1 : 0;
                                if ($eticket_count == 0) {
                                    echo 'message not send';
                                    die();
                                }
                            }
                            /* end sms */



                            if($is_send)
                                {
    //                                $this->tickets_model->ticket_id = $ticket_dump_data[0]->ticket_id;
    //                                $this->tickets_model->bos_ref_no = $bos_ref_no;
    //                                $this->tickets_model->eticket_count = $ticket_dump_data[0]->eticket_count + 1; 
    //                                $this->tickets_model->save();

                                    $data["flag"] = '@#success#@';
                                    $data['type'] = "sms";
                                    $data['msg_type'] = "success"; 
                                    $data['msg'] = "Your ticket is sent to ".$ticket_dump_data[0]['mobile_no'];
                                }
                                else
                                {
                                    $data["flag"] = '@#error#@';
                                    $data['type'] = "sms";
                                    $data['msg_type'] = "error"; 
                                    $data['msg'] = "Problem occured while sending message on ".$ticket_dump_data[0]['mobile_no'];
                                }

                            /*
                            * The Above SMS Code is temp and For testing purpose only
                            */
                        }
                        else if ($ticket_mode == "mail") {


                            $ticketId = 0; $onwadTicket = $returnTicket ='';

                            $ticketDetails = $passengerDetails = array();

                            $ticketDetails = $this->Search_bus_model->getTicketsDetails($bos_ref_no);
                            if (empty($ticketDetails) && empty($ticketDetails[0]['pnr_no'])) {
                                die('Your tickets can not view please contact with our support team!');
                            }
                            $ticketId = $ticketDetails[0]['ticket_id'];
                            $passengerDetails = $this->Search_bus_model->getPassengerDetails($ticketId);

                            $onwadTicket = $this->sendSmsEmail($ticketDetails, $passengerDetails);



                            // if($mail_result > 0)
                            // {
                               $data['flag'] = "@#success#@";
                               $data['type'] = "mail";  
                               $data['msg_type'] = "success"; 
                               $data['msg'] = "Mail Sent successfully to ".$ticket_dump_data[0]['user_email_id'];  
                            // }
                        }
                    
                    
                }
                else
                {
                    $data["flag"] = '@#error#@';
                    $data['msg_type'] = "error"; 
                    $data['msg'] = "failed";
                }
            }
            else
            {
                $data["flag"] = '@#error#@';
                $data['msg_type'] = "error"; 
                $data['msg'] = "Please select ticket method";
            }

            data2json($data);
        }
    } // End of ticket_by_sms_email
    
    
    function updateWalletTxn($amount, $transaction_no) {

        $from_userid = $this->sessionData["id"];

        $transfer_amount = $amount;

        $fund_transfer_remark = "Hotel Booking";

        $from_wallet_details = $this->wallet_model->where('user_id', $from_userid)->find_all();


        $from_wallet_id = $from_wallet_details[0]->id;
        $wallet_amt = $from_wallet_details[0]->amt;

        //$transaction_no = substr(hexdec(uniqid()), 4, 12);


        if ($wallet_amt >= $transfer_amount) {
            $wallet_trans_detail = [
                "w_id" => $from_wallet_id,
                "amt" => $transfer_amount,
                "merchant_amount" => '',
                "wallet_type" => "actual_wallet",
                "comment" => "Hotel Booking",
                "status" => 'Debited',
                "user_id" => $from_userid,
                "amt_before_trans" => $from_wallet_details[0]->amt,
                "amt_after_trans" => $from_wallet_details[0]->amt,
                "remaining_bal_before" => 0,
                "remaining_bal_after" => 0,
                "cumulative_bal_before" => 0,
                "cumulative_bal_after" => 0,
                "amt_transfer_from" => $from_userid,
                "amt_transfer_to" => 0,
                // "transaction_type_id" => '11',   //need to check this value
                "transaction_type" => 'Debited',
                "is_status" => "Y",
                "added_on" => date("Y-m-d H:i:s"),
                //"added_by" => $this->session->userdata('user_id'),
                "added_by" => $from_userid,
                "transaction_no" => $transaction_no,
                "remark" => $fund_transfer_remark
            ];
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'hotel_booking_data.log', $wallet_trans_detail, 'wallet trans Data');
            $this->db->insert('wallet_trans', $wallet_trans_detail);
            $wallet_trans_id = $this->db->insert_id();

            $update_wallet = [
                "amt" => $wallet_amt,
                "last_transction_id" => $wallet_trans_id,
                "updated_by" => $from_userid,
                "updated_date" => date("Y-m-d H:i:s"),
            ];

            $this->db->where('user_id', $from_userid);
            return $update_from_wallet = $this->db->update("wallet", $update_wallet);
        } else {
            $this->session->set_tempdata('msg', 'Wallet has limited amount', 3);
        }
    }
    

    
}


				    		