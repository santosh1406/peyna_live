<?php

class Cb_machine_master extends MY_Controller {

    public function __construct() {
        parent::__construct();

        is_logged_in();
 $this->load->model(array('cb_machine_master_model', 'cb_assign_machine_agent_model')
        );
       
    }

    public function index() {

        $first  = $this->uri->segment(1);
        $second = $this->uri->segment(2);
        $third  = $this->uri->segment(3);

        $get_acesss = get_my_menu($first,$second,$third);
        $data['permission_list'] = $get_acesss;
        load_back_view(CB_MASTER_VIEW, $data);
    }


    public function list_of_masters()
    {
        $action = '';
        $action_q = 
        $this->datatables->select("id,IMEI_NO,SIM_NO,is_status");
        $this->datatables->from("cb_machine_master");
   
        $action .= '<a href="' . site_url('admin/cb_machine_master/edit') . '/$1" class="btn btn-primary btn-xs edit_btn" data-ref="$1" title="Edit machine master" > <i class="fa fa-edit"></i> </a>&nbsp;';
       // $action .= ' <a href="' . site_url('admin/cb_machine_master/delete') . '/$1" class="btn btn-primary btn-xs delete_btn" data-ref="$1" title="Delete machine master"> <i class="fa fa-trash"></i> </a>';
        
        $this->datatables->add_column('action', $action, 'id');
        
        $data = $this->datatables->generate('json');

        echo $data;
    }
   
   public function create_machine_master()
   {
        load_back_view(CREATE_MACHINE_MASTER_VIEW);
   } 
   
   public function save_machine_master()
   {
        $input = $this->input->post();

        $config = array(
            array('field' => 'imei_no', 'label' => 'IMEI No', 'rules' => 'trim|required|is_unique[cb_machine_master.IMEI_NO]|max_length[15]|numeric', 'errors' => array('required' => 'Please enter IMEI No.','is_unique' => 'This IMEI NO is already exist.')),
            array('field' => 'sim_no', 'label' => 'SIM No', 'rules' => 'trim|numeric|max_length[20]', 'errors' => array()),
            array('field' =>'mobile_no','label'=>'Mobile No','rules'=>'trim|xss_clean|max_length[10]|numeric','errors' => array())
        );

        if (form_validate_rules($config) == FALSE) {
            $data['error'] = $this->form_validation->error_array();   
         
            load_back_view(CREATE_MACHINE_MASTER_VIEW,$data);    
        } else {


            $group =  array(
                            'IMEI_NO'     => $input['imei_no'],
                            'SIM_NO'   => $input['sim_no'],
                            'mobile_no'   => $input['mobile_no'],                           
                            'is_status' => 'Y',                            
                            'created_by' =>  $this->session->userdata('user_id'),
                            'created_date' =>  date('Y-m-d H:i:s')
                        );

            //echo "<pre>"; print_r($group); die;

            $this->cb_machine_master_model->insert($group);
            $this->session->set_flashdata('msg_type', 'success');
            $this->session->set_flashdata('msg', 'Machine Added Sucessfully.');
            redirect(site_url() . '/admin/cb_machine_master');
        }
    }

    public function edit($id) {
        $data['group'] = $this->cb_machine_master_model->find($id);
        load_back_view(EDIT_MACHINE_MASTER_VIEW,$data);
    }

    public function update($id) {

        $input = $this->input->post();
        
        $data['pro_data'] = $this->cb_machine_master_model->find($id);

        $config = array(
            array('field' => 'imei_no', 'label' => 'imei_no', 'rules' => 'trim|required', 'errors' => array('required' => 'Please enter IMEI No.')),
             array('field' => 'sim_no', 'label' => 'SIM No', 'rules' => 'trim|numeric|max_length[20]', 'errors' => array()),
            array('field' =>'mobile_no','label'=>'Mobile No','rules'=>'trim|xss_clean|max_length[10]|numeric','errors' => array('max_length'=>'Mobile No should be 10 digit number.'))           
        );
        if (form_validate_rules($config) == FALSE) {

            $data['error'] = $this->form_validation->error_array();            
            redirect(site_url('admin/cb_machine_master/edit/'.$id),$data);
            
        } else {

            $groups = array(
                'IMEI_NO' => $input['imei_no'],
                'SIM_NO' => $input['sim_no'],
                'mobile_no' => $input['mobile_no'],
                'is_status' => $input['status'],               
                'updated_date' => date('y-m-d H:i:s')            
            );
           // show($groups);

           $result = $this->cb_machine_master_model->update(array('id'=>$id), $groups); //show($result,1);         
            $this->session->set_flashdata('msg_type', 'success');
            $this->session->set_flashdata('msg', 'Machine update successfully.');
            redirect(site_url() . '/admin/cb_machine_master');
        }
    }

    public function delete($id) {
        $response = array();
        
        $role_data = $this->cb_machine_master_model
                        ->where("id", $id)->where("is_status","Y")
                        ->as_array()->find_all();
        
       
        if ($role_data) {
             $groups = [
               
                'is_status' =>'N',
               'is_deleted'=>'Y',
                'updated_date' => date('y-m-d H:i:s')            
            ];


            $this->cb_machine_master_model->update($id, $groups);            
           
            $this->session->set_flashdata('msg', 'Machine delete sucessfully');

        } else {
            $this->session->set_flashdata('msg', 'Machine already deleted');
        }

        redirect(site_url('admin/cb_machine_master'));
    }
     public function assignDevice()
    {
      $sessionData = $this->session->userdata();
      $role_id = $sessionData['role_id'];
      $user_id  = $this->session->userdata("user_id");
      $where_condition['role_id'] = '6';
      $data['agent_details'] = $this->users_model->column('id,first_name,last_name,level_2')->where($where_condition)->find_all();
      // $data['imei_details'] = $this->cb_machine_master_model->column('id,IMEI_NO')->where("is_status","Y")->as_array()->find_all();
        $data['imei_details'] = $this->cb_machine_master_model->get_imei_no();
      load_back_view(ASSIGN_DEVICE_VIEW, $data);
      
     
    }
    
     public function list_agent_Device()
    {
        $first  = $this->uri->segment(1);
        $second = $this->uri->segment(2);
        $third  = $this->uri->segment(3);

        $get_acesss = get_my_menu($first,$second,$third);
        $data['permission_list'] = $get_acesss;
        load_back_view(LIST_ASSIGN_DEVICE_VIEW, $data);
      
     
    }
     public function list_of_agent_device()
    {
        $this->datatables->select("cd.user_id,concat(first_name,' ',last_name),count(machine_master_id)");
        $this->datatables->from("users u");
         $this->datatables->join('cb_assign_machine_agent cd', 'cd.user_id = u.id', 'inner');
         $this->datatables->group_by('cd.user_id');
       // $action .= '<a href="' . site_url('admin/cb_machine_master/edit') . '/$1" class="btn btn-primary btn-xs edit_btn" data-ref="$1" title="Edit machine master" > <i class="fa fa-edit"></i> </a>&nbsp;';
        $action .= ' <a href="' . site_url('admin/cb_machine_master/assign_machine_view') . '/$1" class="btn btn-primary btn-xs view_btn" data-ref="$1" title="View Machines"> <i class="fa fa-eye"></i> </a>';
        
        $this->datatables->add_column('action', $action, 'user_id');
        
        $data = $this->datatables->generate('json');

        echo $data;
    }
    public function assign_machine_view($id='') {
       
        $data['agent_id']=$id;
        $result = $this->cb_machine_master_model->getAgentDetails($id);
         $data['agent_name'] = $result->agent_name;
      // print_r($data);die;
        load_back_view(LIST_AGENT_ASSIGN_DEVICE_VIEW,$data);
    }
    
    
     public function save_assign_machine_master()
   {
        $input = $this->input->post();

        $config = array(
            array('field' => 'agent_id', 'label' => 'agent_id', 'rules' => 'trim|required', 'errors' => array('required' => 'Please Select Agent Name.')),
            array('field' => 'imei_no', 'label' => 'imei_no', 'rules' => 'trim|required', 'errors' => array('required' => 'Please enter IMEI NO.')),
           
        );

        if (form_validate_rules($config) == FALSE) {

            $data['error'] = $this->form_validation->error_array();
            $this->session->set_flashdata('old', $this->input->post());
            $this->session->set_flashdata($data);

            redirect(site_url('admin/cb_machine_master/list_agent_Device'));
         } else {
            $group = [
                            'machine_master_id'     => $input['imei_no'],
                            'user_id' => $input['agent_id'],
                            'is_status'=>'Y',
                            'created_by' =>  $this->session->userdata('user_id'),
                            'created_date' =>  date('Y-m-d H:i:s')
                        ];

           // echo "<pre>"; print_r($group); die;

            $this->cb_assign_machine_agent_model->insert($group);
            $this->session->set_flashdata('msg_type', 'success');
            $this->session->set_flashdata('msg', 'Machine assign sucessfully.');

            redirect(site_url() . '/admin/cb_machine_master/list_agent_Device');
        }
    }
    
    
     public function list_of_agent_device_data()
    {
      
        $input=$this->input->post();
 
        $this->datatables->select("u.id,cd.IMEI_NO,cd.SIM_NO");
        $this->datatables->from('users u');
        $this->datatables->join('cb_assign_machine_agent ad','u.id=ad.user_id', 'inner');
        $this->datatables->join('cb_machine_master cd','cd.id=ad.machine_master_id', 'inner');
        $this->datatables->where('ad.user_id',$input['agent_id']);        
        $data = $this->datatables->generate('json');

        echo $data;
    }
    
}

