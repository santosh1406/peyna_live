<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fund_self_request extends CI_Controller {

	public function __construct() {

		parent::__construct();
		if($this->session->userdata('role_id') != TRIMAX_ROLE_ID) {
			redirect('admin/dashboard');
		}
		is_logged_in();
		$this->load->model(array('fund_request_model','common_model','users_model', 'payment_transaction_fss_model','wallet_topup_model','rokad_topup_model'));
		$this->load->library('form_validation');
	}
		
	public function self_fund_request() {

   		$data = array();
		$data['user_data']  = $this->session->userdata();
		$data['user_type']  = $data['user_data']['role_name'];
		$RollID = $this->session->userdata("role_id");
		$UserID = $this->session->userdata("user_id");
		$LvlId  = 1;
		$data['Pay_Trans_Mode'] = $this->fund_request_model->getPaymentTransferMode();

		load_back_view(BACK_SELF_FUND_REQUEST, $data);             
	}
	
	/** Save Function For Self Funds Request */
	public function save_self_fund_request() {

	   	$input = $this->input->post(NULL,TRUE);
		$config = array(
			array('field' => 'amount', 'label' => 'Amount', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please Enter Amount.')),
			array('field' => 'transanction_mode', 'label' => 'Transfer Mode', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please Select Transfer Mode.'))
		);		
		
		if (form_validate_rules($config)) {
				$amount = intval($input['amount']);
		   		if($amount <= 0) {
					$data["flag"] = '@#failed#@';
					$data["msg_type"] = 'error';
					$data["msg"] = 'Amount Should Be Greater Than 0';  
				} else {
					$UserID = $this->session->userdata("user_id");
					
					$now = date('Y-m-d H:i:s', time());
				
						$TransMode =  $this->common_model->get_value('payment_transfer_mode','mode','id='.$input['transanction_mode']);

						$data = array(
							'user_id'	        => $UserID,
							'request_touserid'	=> $this->session->userdata('user_id'),
							'amount'	        => $input['amount'], 
							'topup_by'	        => $TransMode->mode,
							'transaction_no'    => $input['transanction_ref'],
							'bank_name'	        => $input['bank_name'], 
							'bank_acc_no'	    => $input['bank_account_no'],
							'transaction_date'	=> $now,				
							'transaction_remark'=> $input['remark'], 
							'is_approvel'	    => 'A',
							'is_status'	        => 'Y',
							'created_by'	    => $UserID,
							'created_date'      => $now
						);

						$id = $this->common_model->insert('wallet_topup', $data);
						log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $input, "User Enters data");
						log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $data, "Data Added In to Wallet Table");
						if ($id > 0) {

							$data["flag"] = '@#success#@';
							$data["msg_type"] = 'success';
							$data["msg"] = 'Self Balance Request Added successfully.';						

							// send email						
							// $firstname = $this->session->userdata('first_name');
							// $lastname = $this->session->userdata('last_name');
							// $ToEmail =  $this->common_model->get_value('users','email','id='.TRIMAX_ROLE_ID);
							// $ToMobile =  $this->common_model->get_value('users','mobile_no','id='.TRIMAX_ROLE_ID);
							// $ToName =  $this->common_model->get_value('users','first_name,last_name','id='.TRIMAX_ROLE_ID);
							// $mail_replacement_array = array(
							// 	"{{topup_by}}" => $TransMode->mode,
							// 	"{{amount}}" => round($input['amount'],2),
							// 	"{{bank_name}}" => $input['bank_name'],
							// 	"{{bank_acc_no}}" => $input['bank_account_no'],
							// 	"{{transaction_no}}" => $input['transanction_ref'],
							// 	"{{transaction_remark}}" => $input['remark'],
							// 	"{{username}}" => $firstname . ' ' . $lastname,
							// 	"{{useremail}}" => $this->session->userdata('email'),
							// 	"{{transaction_date}}" => date('d-m-Y H:i:s', strtotime($now)),
							// 	"{{is_approvel}}" => 'Pending'
							// );
							// //for admin and not for Retailer
							// $data['mail_content'] = str_replace(array_keys($mail_replacement_array), array_values($mail_replacement_array), WALLET_TOPUP_REQUEST);

							// $mailbody_array = array("subject" => "Self Wallet Topup Request of Rs " .round($input['amount'],2). " by " . $firstname . ' ' . $lastname . " ",
							// "message" => $data['mail_content'],
							// "to" => rokad_decrypt($ToEmail->email,$this->config->item('pos_encryption_key'))
							// );						

							// $mail_result = send_mail($mailbody_array);
							// log_data('log1.log',$mail_result,"mail_result");
							// if ($mail_result) {
							// 	$data1['flag'] = "@#success#@";
							// }

							//ADMIN sms
							// $flag = true;
							// if ($flag == true) {
							// 	$sms_array = array (
							// 		"{{request_to_username}}" => $ToName->first_name.' '.$ToName->last_name,
							// 		"{{request_by_role_name}}" => $this->session->userdata('role_name'),
							// 		"{{request_by_username}}" => $firstname . ' ' . $lastname,
							// 		"{{amount}}" => round($input['amount'],2)
							// 	);
							// 	$msg_to_sent = str_replace(array_keys($sms_array), array_values($sms_array), WALLET_TOPUP_REQUEST_TO);
							// 	$is_send = sendSMS(rokad_decrypt($ToMobile->mobile_no,$this->config->item('pos_encryption_key')), $msg_to_sent);
							// 	if ($is_send) {
							// 		$data1['sms'] = "@#success#@";
							// 	}
							// }

							$RequestBy = $this->common_model->get_value('wallet','amt,id','user_id='.$this->session->userdata('user_id'));
							$AmntAfterTransfer = $RequestBy->amt + $input['amount'];
							$transaction_no = substr(hexdec(uniqid()), 4, 12);
							$creditdata = array(
								'transaction_no'      => $transaction_no,
								'w_id'	              => $RequestBy->id,
								'amt'	              => $input['amount'],
								'user_id'	          => $this->session->userdata('user_id'),
								'amt_before_trans'    => $RequestBy->amt,	 
								'amt_after_trans'	  => $AmntAfterTransfer ,
								'status'              => 'added',
								'transaction_type'	  => 'Credited',
								'comment'	          => 'Self Top Up Request Accept',
								'is_status'	          => 'Y',
								'transaction_type_id' => 1,
								'added_on'            => $now,
								'topup_id'			  => $id, 
								'added_by'            => $UserID
							);
							$CreditIdBy = $this->common_model->insert('wallet_trans',$creditdata);
							log_data("topup/" . date("M") . "/" . date("d") . "/topup_accepted_log.log", $creditdata, "Credit In to Wallet Trans Table");
							if($CreditIdBy){
								$data2 = array(
									'amt'	       => $AmntAfterTransfer,
									'updated_by'   => $UserID,
									'updated_date' => $now);
								$UpdateId = $this->common_model->update('wallet','user_id',$this->session->userdata('user_id'),$data2);	
							}
						} else {
							$data["flag"] = '@#failed#@';
							$data["msg_type"] = 'error';
							$data["msg"] = 'Self Balance Request could not be Added.';						
						}
					// }
				}
		} else {
			$data["flag"] = '@#failed#@';
			$data["msg_type"] = 'error';
			$data["msg"] = 'Self Balance Request could not be added/updated without All * Neccessary Data Not Fill.';
		}		
		data2json($data);
	}

	/** Load Funds Request Approval Page*/
	public function self_topup_list() {
	   load_back_view(BACK_SELF_REQUEST_LIST);             
    }
	
    /** Get List For Funds Request Approval*/
    public function self_topup_approval_list() {
		
        $aColumns = array('id', 'created_date', 'amount','topup_by', 'transaction_remark','is_approvel', 'a.id');

        $DataTableArray = getDefinedDataTable($aColumns);
        $sWhere = $DataTableArray['sWhere'];
        $sOrder = $DataTableArray['sOrder'];
        $sLimit = $DataTableArray['sLimit'];

		$RollID = $this->session->userdata("role_id");
		$UserID = $this->session->userdata("user_id");
		
		$sWhere = '';

		$sWhere .= "WHERE topup_by != 'pg'";

		if($RollID == TRIMAX_ROLE_ID){
			if($UserID != "")
			{
				if($sWhere<>''){
					$sWhere .= " and request_touserid  = '".$UserID."'";
				}else{
					$sWhere .= " WHERE request_touserid  = '".$UserID."'";
				}
				$sWhere .= " and user_id  = '".$UserID."'";
			}
		}	
		
		$DataTableArray = $this->rokad_topup_model->LoadRokadApprovalDataTable($sWhere, $sOrder, $sLimit);
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $DataTableArray['iTotal'],
            "iTotalDisplayRecords" => $DataTableArray['iTotal'],
            "aaData" => array()
        );
        $aHeader = array('id', 'Date','amount', 'topup_by', 'transaction_remark', 'is_approvel', 'Actions');
        $site_url = base_url();
        
        foreach ($DataTableArray['ResultSet'] as $aRow) {
            $row = array();
            $TotalHeader = count($aHeader);

            for ($i = 0; $i < $TotalHeader; $i++) {
                if ($aHeader[$i] == "amount") {
					$Amnt = (string)$aRow['amount'];
					$Amt =strpos($Amnt,'.');
					$Amount =substr($Amnt,0,$Amt+3);
					$row[] = $Amount;
                } else if ($aHeader[$i] == "is_approvel") {
					$TopupStatus = $aRow['is_approvel'];
					if($TopupStatus =='A'){
					$Tstatus =	'Accepted';
					}elseif($TopupStatus =='R'){
					$Tstatus =	'Rejected';	
					}else{
					$Tstatus =	'Pending';	
					}
					$row[] = $Tstatus;
                } else if ($aHeader[$i] == "Actions") {
					
					if($aRow['is_approvel'] == 'A' or $aRow['is_approvel'] == 'R') { 
						$Action = '<a style="padding: 0 4px;" class="pull-left" href="'.$site_url.'admin/rokad_topup/view_rokad_topup_approval/'.base64_encode($aRow['id']).'/'.base64_encode('self').'" ref="'.$aRow['id'].'" rel="tooltip" data-placement="top" data-original-title="View" title="Topup View"><i class="fa fa-eye" style="font-size:18px;color:#337ab7;"></i></a>&nbsp;&nbsp;&nbsp;';
					}
					$row[] = $Action;
                } else if ($aHeader[$i] != ' ') {
                    /* General output */
                    $row[] = $aRow[$aHeader[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode($output);
    }

}