<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of login
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 *  
 * 
 */
class Login extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
		
        $this->load->model(array('users_model', 'role_model', 'user_login_count_model', 'wallet_model', 'user_email_model', 'user_otp_model', 'user_tpin_model', 'user_password_model'));
        $this->config->load('facebook');
    }

    /*
     * @function    : login_view
     * @param       : 
     * @detail      : login page.
     *                 
     */

    public function login_view() {
        $data = [];
        $data["metadata_title"] = $this->config->item('title', 'login_metadata');
        $data["metadata_description"] = $this->config->item('description', 'login_metadata');
        $data["metadata_keywords"] = $this->config->item('keywords', 'login_metadata');

        $user_id = $this->session->userdata('user_id');		
        if ($user_id == "") {
            load_front_view(LOGIN_VIEW, $data);
        } else {
            redirect(base_url());
        }
    }

    /*
     * @function    : forgot_password_view
     * @param       : 
     * @detail      : login forgot password view.
     *                 
     */

    public function forgot_password_view() {
        // $captcha = getCiCaptcha();
        // $this->session->set_userdata('bos_forgot_captcha', $captcha['word']);
        $this->load->view(FORGOT_PASSWORD_VIEW);
    }

    public function forgot_password() {
        $login_post = $this->input->post();		
		if(!is_string($login_post["email"]) || !is_string($login_post["g-recaptcha-response"]) || !is_string($login_post["hiddenRecaptcha"]))
		{
			redirect(base_url() . "admin/login/login_view");
		}		
        $email = trim($login_post["email"]);
        //$captcha = trim($login_post["captcha"]);
        $g_recaptcha_response = trim($login_post['g-recaptcha-response']);
		$hiddenRecaptcha = trim($login_post["hiddenRecaptcha"]);
		
		$input_data = array(
		"email" => $email,
		"g-recaptcha-response" => $g_recaptcha_response,
		"hiddenRecaptcha" => $hiddenRecaptcha
		);
		
		if (isset($login_post['g-recaptcha-response'])) {
            if ($email != "") {

                //Google Captcha
                $g_captcha_data = google_captcha($g_recaptcha_response);

                if ($g_captcha_data->success) {
                    /* $this->users_model->email = $email;
                      $udata = $this->users_model->select(); */
					$enc_email = rokad_encrypt($email, $this->config->item('pos_encryption_key'));	
                    $where_array = array("email" => $enc_email);
                    $udata = $this->users_model->where($where_array)->find_all();

                    if ($udata) {
                        $user_array = $udata[0];
                        if ($user_array->status == "Y") {
                            $password = getRandomId(10);
                            $salt = getRandomId();
                            $pass = md5($salt . $password);
                            $this->users_model->id = $user_array->id;
                            $this->users_model->salt = $salt;
                            $this->users_model->password = $pass;
                            $last_insert_id = $this->users_model->save();
							
							$pass_data = array("user_id" => $user_array->id,
							"salt" => $salt,			
							"password" => $pass,
							"created_date" => date('Y-m-d H:i:s')
							);	

							$this->user_password_model->save_password_log($pass_data);						
							
                            $display_name = $user_array->display_name;
							
							$username = rokad_decrypt($user_array->username, $this->config->item('pos_encryption_key'));			
														
                            $fullname = ucwords(strtolower($user_array->first_name . " " . $user_array->last_name));
                            if ($last_insert_id > 0) {
                                $mailbody_array = array("mail_title" => "forgot_password",
                                    "mail_client" => $email,
                                    "mail_replacement_array" => array('{{fullname}}' => $fullname,
																	  '{{username}}' => $username,
																	  '{{password}}' => $password,
																	  '{{rokad_support_mail}}' => ROKAD_SUPPORT_MAIL,
																	  '{{home_page_url}}' => base_url(),
							                                          '{{logo}}' => '<img src="'.DOMAIN_NAME_VAL.'/assets/travelo/front/images/logo.png" style="width:104px;"  />'
																	)
                                );

                                $mail_result = setup_mail($mailbody_array);
								$roledata = $this->role_model->find($user_array->role_id); 
                                if ($mail_result) {
                                    //insert into user action log										
									$action_log_msg = "Password reset successful"; 

                                    $this->session->set_flashdata('msg_type', 'success');
                                    $this->session->set_flashdata('msg', 'Password recovery email is sent to your Email ID.');
                                    // redirect(base_url()."home1");
                                } else {
                                    //insert into user action log
                                   $action_log_msg = "Password reset failed"; 

                                    $this->session->set_flashdata('msg_type', 'success');
                                    $this->session->set_flashdata('msg', 'Password could not reset.');
                                    // redirect(base_url()."home");
                                }

                                //insert into user action log
								$users_action_log = array(
								"user_id" => $user_array->id,
								"name" => $user_array->first_name . " " . $user_array->last_name,
								"rmn" => rokad_decrypt($user_array->username,$this->config->item('pos_encryption_key')),
								"rollname" => $roledata->role_name,
								"url" => current_url(),
								"db_function" => "update",
								"db_table_name" => "users",
								"function_name" => "reset password",
								"input_data" => json_encode($input_data),
								"message" => $action_log_msg
								);
								users_action_log_new($users_action_log);
                            }
                        } else {
                            $this->session->set_flashdata('msg_type', 'error');
                            $this->session->set_flashdata('msg', 'Could not reset passsword because your account is disabled.');
                            // redirect(base_url()."home");
                        }
                    } else {
                        $this->session->set_flashdata('msg_type', 'error');
                        $this->session->set_flashdata('msg', 'Email Id not exists.');
                        // redirect(base_url()."home");
                    }
                } else {
                    $this->session->set_flashdata('msg_type', 'error');
                    $this->session->set_flashdata('msg', 'Please verify captcha.');
                    // redirect(base_url()."home");
                }
            } else {
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', 'username should not be empty.');
                // redirect(base_url()."home");
            }
        } else {
            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'Please verify captcha.');
            redirect(base_url() . "admin/login/login_view");
        }

        redirect(base_url() . "admin/login/login_view");
    }

    /*
     * @function    : signup_view
     * @param       : 
     * @detail      : signup view page.
     *                 
     */

    public function signup_view() {
        $data = [];
        $data["metadata_title"] = $this->config->item('title', 'signup_metadata');
        $data["metadata_description"] = $this->config->item('description', 'signup_metadata');
        $data["metadata_keywords"] = $this->config->item('keywords', 'signup_metadata');

        $user_id = $this->session->userdata('user_id');
        if ($user_id == "") {
            load_front_view(SIGNUP_VIEW, $data);
        } else {
            redirect(base_url());
        }
    }

    /*
     * @function    : login_authenticate
     * @param       : 
     * @detail      : To check use credential. If user credential is correct
     *                then create a textfile of usermenu,usermenu permission,menu 
     *                and redirect to particular page else redirect to login 
     *                page.               
     */

    public function login_authenticate() {
		$login_post = $this->input->post(NULL,TRUE);
		if(!is_string($login_post["username"]) || !is_string($login_post["password"]) || !is_string($login_post["g-recaptcha-response"]))
		{
			redirect(base_url() . "admin/login/login_view");
		}
		
        $username = trim($login_post["username"]);
		$pass     = trim($login_post["password"]);		
		$pass 	  = trim($this->cryptoJsAesDecrypt($username, $pass));       
        $g_recaptcha_response = trim($login_post['g-recaptcha-response']);
		
		$g_captcha_data = google_captcha($g_recaptcha_response);
		if ($g_captcha_data->success)
		{			
			$user_data = $this->session->userdata();		
			if (!isset($user_data['id']))
			{           			
				$username = rokad_encrypt($username, $this->config->item('pos_encryption_key'));          
				$this->auth->login($username, $pass);
			} 
			else 
			{				
				redirect(base_url() . "home");
			}
		}
		else {
			$this->session->set_flashdata('msg_type', 'error');
			$this->session->set_flashdata('msg', 'Please verify captcha.');
			redirect(base_url() . "admin/login/login_view");
		}	
    }

    /*
     * @function    : logout
     * @param       : 
     * @detail      : To delete usermenu,usermenu permission and menu textfiles 
     *                and redirect to login page.
     *                
     */

    public function logout() {        
        $user_data = $this->session->userdata();
        $id = $this->session->userdata('id');
        $UserID = $this->session->userdata('user_id');
		$UserUpdate = array("session_id" => 'NULL');
		$UpdateId = $this->common_model->update('users','id',$UserID,$UserUpdate);
        /*         * **************** check if remember is on start ******************* */
        if ($this->input->cookie("remember_me") != "") {
            $this->users_model->id = $id;
            $this->users_model->remember_me = "";
            $remem_id = $this->users_model->save();

            if ($remem_id > 0) {
                delete_cookie("remember_me");
            }
        }
		
		$users_action_log = array(
		"user_id" => $user_data['user_id'],
		"name" => $user_data['display_name'],
		"rmn" => $user_data['username'],
		"rollname" => $user_data['role_name'],
		"url" => current_url(),
		"db_function" => "",
		"db_table_name" => "",
		"function_name" => "logout",
		"input_data" => "",
		"message" => "Logged out"
		);        

        users_action_log_new($users_action_log);
        /*         * *************** check if remember is on end ******************* */

        $this->session->sess_destroy();
        unset($_SESSION);

        $this->output->clear_all_cache();
        if ($this->session->userdata('user_id') == "") {

            redirect(base_url() . "home");
        }
    }

    /*
     * @function    : do_register
     * @param       : 
     * @detail      : register form
     *                 
     */

    public function do_register() {

        $user_id = $this->session->userdata('user_id');

        if ($user_id == "") {
            load_front_view(REGISTER_VIEW);
        } else {
            redirect("acl/acl/aclmenu");
        }
    }

    /*
     * @function    : register_authenticate
     * @param       : 
     * @detail      : to register user.               
     */

    public function register_authenticate() {

        $data = array();
        //insert user in table
        $input = $this->input->post(NULL,TRUE);

        log_data("registration/" . date("M") . "/user_registration_log.log", $input, "Post data for registration");
        $first_name = trim($input['first_name']);
        $last_name = trim($input['last_name']);
        $display_name = $first_name . " " . $last_name;
        $email = trim($input['email']);
        $username = trim($input['username']);
        $password = trim($input['password']);
        $confirm_password = trim($input['confirm_password']);
        $mobileno = trim($input['mobno']);
        //$captcha = trim($input['captcha']);
        $g_recaptcha_response = trim($input['g-recaptcha-response']);

        $salt = getRandomId();
        // for server side validation
        $config = array(
            array('field' => 'first_name', 'label' => 'First Name', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter first name.')),
            array('field' => 'last_name', 'label' => 'Last Name', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter last name.')),
            array('field' => 'email', 'label' => 'Email', 'rules' => 'trim|required|valid_email|xss_clean', 'errors' => array('required' => 'Please enter valid email.')),
            array('field' => 'username', 'label' => 'Username', 'rules' => 'trim|required|min_length[5]|xss_clean'),
            array('field' => 'password', 'label' => 'password', 'rules' => 'trim|required|min_length[8]|xss_clean'),
            array('field' => 'mobno', 'label' => 'Mobile Numebr', 'rules' => 'trim|required|min_length[10]|max_length[10]|xss_clean'),
            array('field' => 'confirm_password', 'label' => 'Confirm Password', 'rules' => 'trim|required|matches[password]|xss_clean')
        );

        if (form_validate_rules($config)) {
            if (isset($g_recaptcha_response)) {
                if ($password != "" and $password == $confirm_password) {
                    //Google Captcha
                    $g_captcha_data = google_captcha($g_recaptcha_response);

                    if ($g_captcha_data->success) {
                        if ($username != "" and $email != "" and $display_name != "") {

                            $user_exists = $this->users_model->check_user_registered($username, $email);

                            if (empty($user_exists)) {

                                // $user_id = $this->generateid_model->getRequiredId('users');
                                // if ($user_id != "") {
                                $pass = md5($salt . $password);
                                //insert user in table
                                // to get role id
                                $this->role_model->role_name = USER_ROLE_NAME;
                                $rdata = $this->role_model->select();
                                $user_role_id = $rdata[0]->id;
                                //$role_array = $this->role_model->get_roleid_by_name(USER_ROLE_NAME);
                                //insert user in table
                                $activation_key = getRandomId(8);

                                // $this->users_model->user_id = $user_id;
                                $this->users_model->role_id = $user_role_id;
                                $this->users_model->username = $username;
                                $this->users_model->email = $email;
                                $this->users_model->first_name = $first_name;
                                $this->users_model->last_name = $last_name;
                                $this->users_model->display_name = $display_name;
                                $this->users_model->salt = $salt;
                                $this->users_model->password = $pass;
                                $this->users_model->mobile_no = $mobileno;
                                $this->users_model->status = 'N';
                                $this->users_model->activate_key = $activation_key;
                                $this->users_model->ip_address = $this->input->ip_address();
                                $this->users_model->user_agent = $this->input->user_agent();
                                $user_id = $this->users_model->save();

                                if ($user_id > 0) {
                                    //To generate wallet account
                                    // $this->wallet_model->w_id = $this->generateid_model->getRequiredId('wallet');
                                    $this->wallet_model->user_id = $user_id;
                                    $this->wallet_model->amt = 0.00;
                                    $this->wallet_model->status = "Y";
                                    $this->wallet_model->comment = "Created Account";
                                    $this->wallet_model->added_by = $user_id;
                                    $this->wallet_model->save();


                                    $this->session->set_flashdata('msg_type', 'success');
                                    $this->session->set_flashdata('msg', 'Registration completed successfully. Please check mail to verify your account.');

                                    //Send sms
                                    $reg_sms_array = array("{{username}}" => ucwords($display_name), "{{email}}" => $email);
                                    $temp_sms_msg = str_replace(array_keys($reg_sms_array), array_values($reg_sms_array), BOS_REGISTRATION);
                                    $is_send = sendSMS($mobileno, $temp_sms_msg);

                                    // to send mail on registered mail id
                                    $mailbody_array = array("mail_title" => "email_verification",
                                        "mail_client" => $email,
                                        "mail_replacement_array" =>
                                        array('{{verification_mail_url}}' => base_url() . 'admin/login/activate/' . $activation_key,
                                            '{{fullname}}' => ucwords($display_name)
                                        )
                                    );

                                    $mail_result = setup_mail($mailbody_array);
                                    log_data("registration/" . date("M") . "/user_registration_log.log", "Registration Success", "Registration Success");

                                    $data["fb_event_code"] = "CompleteRegistration"; // IMP : for facebook pixel. Don't Remove this line.
                                    load_front_view(THANK_YOU, $data);

                                    // redirect(base_url()."login/thank");
                                } else {
                                    log_data("registration/" . date("M") . "/user_registration_log.log", "User not saved in database", "DB Query");

                                    $this->session->set_flashdata('msg_type', 'error');
                                    $this->session->set_flashdata('msg', 'Some error occured. Please try again.');
                                    redirect(base_url() . "admin/login/login_view");
                                }
                                // } else {
                                // 	$this->session->set_flashdata('msg_type', 'error');
                                // 	$this->session->set_flashdata('msg', 'Some error occured. Please try again.');
                                // 	redirect(base_url()."home");
                                // }
                            } else {

                                $is_registered = $this->users_model->check_is_registered($username, $email);

                                if (!$is_registered && !empty($user_exists)) {
                                    $pass = md5($salt . $password);
                                    //update user in table
                                    // to get role id
                                    $this->role_model->role_name = USER_ROLE_NAME;
                                    $rdata = $this->role_model->select();
                                    $user_role_id = $rdata[0]->id;
                                    $activation_key = getRandomId(8);

                                    $update_array = array(
                                        "username" => $username,
                                        "email" => $email,
                                        "first_name" => $first_name,
                                        "last_name" => $last_name,
                                        "display_name" => $first_name . " " . $last_name,
                                        "salt" => $salt,
                                        "password" => $pass,
                                        "mobile_no" => $mobileno,
                                        "status" => "N",
                                        "activate_key" => $activation_key,
                                        "ip_address" => $this->input->ip_address(),
                                        "user_agent" => $this->input->user_agent(),
                                    );

                                    $updated_id = $this->users_model->update($user_exists[0]->id, $update_array);

                                    if ($updated_id) {
                                        $this->wallet_model->user_id = $user_exists[0]->id;
                                        $this->wallet_model->amt = 0.00;
                                        $this->wallet_model->status = "Y";
                                        $this->wallet_model->comment = "Created Account";
                                        $this->wallet_model->added_by = $user_exists[0]->id;
                                        $this->wallet_model->save();


                                        $this->session->set_flashdata('msg_type', 'success');
                                        $this->session->set_flashdata('msg', 'Registration completed successfully. Please check mail to verify your account.');

                                        //Send sms
                                        $reg_sms_array = array("{{username}}" => ucwords($display_name), "{{email}}" => $email);
                                        $temp_sms_msg = str_replace(array_keys($reg_sms_array), array_values($reg_sms_array), BOS_REGISTRATION);
                                        $is_send = sendSMS($mobileno, $temp_sms_msg);

                                        // to send mail on registered mail id
                                        $mailbody_array = array("mail_title" => "email_verification",
                                            "mail_client" => $email,
                                            "mail_replacement_array" =>
                                            array('{{verification_mail_url}}' => base_url() . 'admin/login/activate/' . $activation_key,
                                                '{{fullname}}' => ucwords($display_name)
                                            )
                                        );

                                        $mail_result = setup_mail($mailbody_array);

                                        log_data("registration/" . date("M") . "/user_registration_log.log", "User Updated/Registered with id" . $user_exists[0]->id, "Alredy entered email user registered");


                                        $data["fb_event_code"] = "CompleteRegistration"; // IMP : for facebook pixel. Don't Remove this line.
                                        load_front_view(THANK_YOU, $data);
                                        // redirect(base_url()."login/thank");
                                    } else {
                                        log_data("registration/" . date("M") . "/user_registration_log.log", "Problem while updating.", "update user problem");
                                        $this->session->set_flashdata('msg_type', 'error');
                                        $this->session->set_flashdata('msg', 'Some error occured. Please try again');
                                        redirect(base_url() . "home");
                                    }
                                } else {
                                    log_data("registration/" . date("M") . "/user_registration_log.log", "Your id is already register with us. Please login", "Alredy Register");
                                    $this->session->set_flashdata('msg_type', 'error');
                                    $this->session->set_flashdata('msg', 'Your id is already register with us. Please login.');
                                    redirect(base_url() . "home");
                                }
                            }
                        } else {
                            log_data("registration/" . date("M") . "/user_registration_log.log", "Username,email,displayname empty", "empty fields");
                            $this->session->set_flashdata('msg_type', 'error');
                            $this->session->set_flashdata('msg', 'Please provide information to register.');
                            redirect(base_url() . "home");
                        }
                    } else {
                        log_data("registration/" . date("M") . "/user_registration_log.log", "Captcha failed", "Captcha");
                        $this->session->set_flashdata('msg_type', 'error');
                        $this->session->set_flashdata('msg', 'Please verify captcha.');
                        redirect(base_url() . "home");
                    }
                } else {
                    $string_to_save = "Password missmatched. password entered is " . $password . " and confirm password entered is" . $confirm_password;
                    log_data("registration/" . date("M") . "/user_registration_log.log", $string_to_save, "Post data for registration");

                    $this->session->set_flashdata('msg_type', 'error');
                    $this->session->set_flashdata('msg', 'Password missmatched.');
                    redirect(base_url() . "home");
                }
            } else {
                log_data("registration/" . date("M") . "/user_registration_log.log", "Captcha not verified", "Captcha");
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', 'Please verify captcha.');
                redirect(base_url() . "home");
            }
        } else {
            log_data("registration/" . date("M") . "/user_registration_log.log", "Form validation failed", "Form Validation");
            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'Please enter proper data.');
            redirect(base_url() . "home");
        }
    }

//end of register_authenticate


    /*
     * @function    : agent_register_authenticate
     * @param       : 
     * @detail      : to register agent.               
     */

    public function agent_register_authenticate() {
        //insert user in table
        $input = $this->input->post(NULL,TRUE);
        $agent_name = trim($input['agent_name']);
        $email = trim($input['email']);
        $organiazation = trim($input['organiazation']);
        $contact_no = trim($input['contact_no']);
        $agent_address = trim($input['agent_address']);
        $pincode = trim($input['pincode']);
        // for server side validation
        $config = array(
            array('field' => 'agent_name', 'label' => 'Agent name', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter agent name.')),
            array('field' => 'email', 'label' => 'Email', 'rules' => 'trim|required|valid_email|xss_clean', 'errors' => array('required' => 'Please enter valid email.')),
            array('field' => 'organiazation', 'label' => 'Organiazation', 'rules' => 'trim|required|xss_clean'),
            array('field' => 'contact_no', 'label' => 'Contact Number', 'rules' => 'trim|required|is_natural|max_length[11]|xss_clean'),
            array('field' => 'agent_address', 'label' => 'Agent Address', 'rules' => 'trim|required|xss_clean'),
            array('field' => 'pincode', 'label' => 'Pincode', 'rules' => 'trim|required|is_natural|max_length[6]|xss_clean')
        );

        if (form_validate_rules($config)) {
            $this->load->model('agent_model');
            $this->agent_model->email = $email;
            $agentr_exists = $this->agent_model->select();
            //$agentr_exists = $this->agent_model->check_agent_exists($email);
            if (empty($agentr_exists)) {
                $agent_id = $this->generateid_model->getRequiredId('booking_agents');
                if ($agent_id != "") {
                    //insert user in table

                    $this->agent_model->agent_id = $agent_id;
                    $this->agent_model->agent_name = $agent_name;
                    $this->agent_model->organization = $organization;
                    $this->agent_model->email = $email;
                    $this->agent_model->contact_no = $contact_no;
                    $this->agent_model->agent_address = $agent_address;
                    $this->agent_model->pincode = $pincode;
                    $last_insert_id = $this->agent_model->save();

                    //to add agent  and set session after user data entery in database
                    if ($last_insert_id > 0) {
                        $this->session->set_flashdata('msg_type', 'success');
                        $this->session->set_flashdata('msg', 'Registration completed successfully..');
                        redirect(base_url() . "home");
                    } else {
                        $this->session->set_flashdata('msg_type', 'error');
                        $this->session->set_flashdata('msg', 'Some error occured. Please try again.');
                        redirect(base_url() . "home");
                    }
                } else {
                    $this->session->set_flashdata('msg_type', 'error');
                    $this->session->set_flashdata('msg', 'Some error occured. Please try again.');
                    redirect(base_url() . "home");
                }
            } else {
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', 'Your id is already register with us. Please login.');
                redirect(base_url() . "home");
            }
        } else {
            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'Please enter proper data.');
            redirect(base_url() . "home");
        }
    }

    function thank() {
        load_front_view(THANK_YOU);
        // $this->load->view(THANK_YOU);
    }

//end of agent_register_authenticate


    /*
     * @function    : user_guest_booking
     * @param       : 
     * @detail      : to enter email and phone number of user in user table as guest user.               
     */

    public function user_guest_booking() {

        //insert user in table
        $input = $this->input->post();
        $email = trim($input['email']);
        $mobile_no = trim($input['mobile_no']);

        if ($email != "") {
            $this->load->model('users_model');
            $user_exists = $this->users_model->check_user_exists("", $email);
            if ($user_exists == 0) {
                $user_id = $this->generateid_model->getRequiredId('users');
                if ($user_id != "") {
                    // to get role id
                    $this->load->model('role_model');
                    $role_array = $this->role_model->get_roleid_by_name(USER_ROLE_NAME);

                    $userdata = array(
                        'user_id' => $user_id,
                        'role_id' => $role_array["role_id"],
                        "email" => $email,
                        "mobile_no" => $mobile_no
                    );

                    if ($this->users_model->add_user($userdata)) {
                        return $user_id;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {

                $guest_user = $this->users_model->get_user_by_email($email);
                return $guest_user["user_id"];
            }
        } else {
            return false;
        }
    }

//end of user_guest_booking


    /*
     * @Author      : Suraj Rathod
     * @function    : activate
     * @param       : $key -. key sent in mail
     * @detail      : to activate users.               
     */
    function activate($key, $validate_email = "") {

        if ($key != "") {
            $custom = array();
            $userdata = $this->users_model
                    ->where('activate_key', $key)
                    ->find_all();

            if ($userdata) {

                if ($userdata[0]->status != "Y") {
                    if ($userdata[0]->agent_type == "ors_agent" || $userdata[0]->agent_type == "distributor") {
                        if ($userdata[0]->email_verify_status != "Y") {
                            $to_update = array('email_verify_status' => "Y", "activate_key" => "");
                            $id = $this->users_model->update($userdata[0]->id, $to_update);
                            if ($id) {
                                $this->session->set_flashdata('msg_type', 'success');
                                $this->session->set_flashdata('msg', 'Email verified successfully.');
                                $data = array();
                                load_front_view(THANK, $data);
                            } else {
                                $this->session->set_flashdata('msg_type', 'error');
                                $this->session->set_flashdata('msg', 'Please try again.');
                                load_front_view(UNEXPECTED_ERROR);
                            }
                        }
                    } else {
                        $to_update = array('status' => "Y", 'verify_status' => "Y", "activate_key" => "");
                        $custom["verification_type"] = "account";
                        $custom["mail_title"] = "email_verification_success";
                        $this->activation_email($userdata, $to_update, $custom);
                    }
                } else if ($userdata[0]->status == "Y") {
                    if ($userdata[0]->agent_type == "ors_agent" || $userdata[0]->agent_type == "distributor") {
                        
                    } else {
                        if ($validate_email != "" && trim($validate_email) == "email") {
                            $to_update = array('verify_status' => "Y", "activate_key" => "");
                            $custom["verification_type"] = "email";
                            $custom["mail_title"] = "verify_email_id_success";
                            $this->activation_email($userdata, $to_update, $custom);
                        }
                    }
                    $this->session->set_flashdata('msg_type', 'success');
                    $this->session->set_flashdata('msg', 'User already activated.');

                    redirect(base_url() . "admin/login/login_view");
                } else {
                    $this->session->set_flashdata('msg_type', 'success');
                    $this->session->set_flashdata('msg', 'Some unexpected has happend. Please try again');

                    redirect(base_url() . "admin/login/login_view");
                }
            } else {
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', 'Invalid key. Please contact our customer support.');
                load_front_view(INVALID_KEY);
            }
        } else {
            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'Some unexpected has occured.');
            load_front_view(INVALID_KEY);
        }
    }

    /*
     * @Author      : Suraj Rathod
     * @function    : activate_user
     * @param       : $key -. key sent in mail
     * @detail      : to activate users.               
     */

    function activate_user_view($key) {
        $data = array();

        if ($key != "") {
            $userdata = $this->users_model
                    ->where('activate_key', $key)
                    ->find_all();

            if ($userdata && $this->input->cookie("akey") < 4) {
                if ($userdata[0]->verify_status != "Y") {
                    $input = $this->input->post(NULL,TRUE);

                    log_data("registration/" . date("M") . "/user_registration_log.log", $input, "Post data for registration");
                    $data["user_details"] = $userdata[0];
                    load_front_view(USER_REGISTRATION, $data);
                }
            } else {
                if ($this->input->cookie("akey") != "") {
                    $wrong_key = $this->input->cookie("akey");
                    $this->input->set_cookie("akey",  ++$wrong_key, 7776000, '', '/');
                } else {
                    $this->input->set_cookie("akey", 0, 7776000, '', '/');
                }

                if ($this->input->cookie("akey") < 4) {
                    $this->session->set_flashdata('msg_type', 'error');
                    $this->session->set_flashdata('msg', 'Invalid key. Please contact our customer support.');
                    load_front_view(INVALID_KEY);
                } else {
                    $data["custom_title"] = "Security Warning";
                    $data["custom_heading"] = "Suspicious Activity";
                    $data["custom_type"] = "error";
                    $data["custom_message"] = "Security error detected Due to Suspicious Activity found on your computer.";
                    load_front_view(CUSTOM_ERROR, $data);
                }
            }
        } else {
            if ($this->input->cookie("akey") != "") {
                $wrong_key = $this->input->cookie("akey");
                $this->input->set_cookie("akey",  ++$wrong_key, 7776000, '', '/');
            } else {
                $this->input->set_cookie("akey", 0, 7776000, '', '/');
            }
            /* $this->session->set_flashdata('msg_type', 'error');
              $this->session->set_flashdata('msg', 'Invalid key. Please contact our customer support.');
              load_front_view(INVALID_KEY); */
            if ($this->input->cookie("akey") < 4) {
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', 'Invalid key. Please contact our customer support.');
                load_front_view(INVALID_KEY);
            } else {
                $data["custom_title"] = "Security Warning";
                $data["custom_heading"] = "Suspicious Activity";
                $data["custom_type"] = "error";
                $data["custom_message"] = "Security error detected Due to Suspicious Activity found on your computer.";
                load_front_view(CUSTOM_ERROR, $data);
            }
        }
    }

    /*
     * @Author      : Suraj Rathod
     * @function    : activate_user
     * @param       : $key -. key sent in mail
     * @detail      : to activate users.               
     */

    function activate_user($key) {
        $input = $this->input->post(NULL,TRUE);

        log_data("registration/" . date("M") . "/user_registration_log.log", $input, "Post data for registration");
        log_data("registration/" . date("M") . "/user_registration_log.log", $key, "KEY for registration");

        if ($key != "") {
            $custom = array();
            $userdata = $this->users_model
                    ->where('activate_key', $key)
                    ->find_all();
            if ($userdata) {
                $first_name = trim($input['first_name']);
                $last_name = trim($input['last_name']);
                $display_name = $first_name . " " . $last_name;
                $username = trim($input['username']);
                $password = trim($input['password']);
                $confirm_password = trim($input['confirm_password']);
                $g_recaptcha_response = trim($input['g-recaptcha-response']);

                $salt = getRandomId();
                // for server side validation
                $config = array(
                    array('field' => 'first_name', 'label' => 'First Name', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter first name.')),
                    array('field' => 'last_name', 'label' => 'Last Name', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter last name.')),
                    array('field' => 'username', 'label' => 'Username', 'rules' => 'trim|required|min_length[5]|xss_clean'),
                    array('field' => 'password', 'label' => 'password', 'rules' => 'trim|required|min_length[8]|xss_clean'),
                    array('field' => 'confirm_password', 'label' => 'Confirm Password', 'rules' => 'trim|required|matches[password]|xss_clean')
                );

                if (form_validate_rules($config)) {
                    if (isset($g_recaptcha_response)) {
                        if ($password != "" and $password == $confirm_password) {
                            //Google Captcha
                            $g_captcha_data = google_captcha($g_recaptcha_response);

                            if ($g_captcha_data->success) {
                                if ($username != "" && $display_name != "") {
                                    $user_exists = $this->users_model->check_user_registered($username);

                                    if (empty($user_exists)) {
                                        $pass = md5($salt . $password);
                                        $this->role_model->role_name = USER_ROLE_NAME;
                                        $rdata = $this->role_model->select();
                                        $user_role_id = $rdata[0]->id;

                                        $user_fields_update = array(
                                            "role_id" => $user_role_id,
                                            "username" => $username,
                                            "first_name" => $first_name,
                                            "last_name" => $last_name,
                                            "display_name" => $display_name,
                                            "salt" => $salt,
                                            "password" => $pass,
                                            "status" => "Y",
                                            "verify_status" => "Y",
                                            "activate_key" => "",
                                            "inserted_by" => $userdata[0]->id,
                                            /* "ip_address" => $this->input->ip_address(),
                                              "user_agent" => $this->input->user_agent(), */
                                            "ip_address" => $this->input->ip_address(),
                                            "user_agent" => $this->input->user_agent(),
                                        );
                                        $user_id = $this->users_model->update(array("activate_key" => $key), $user_fields_update);

                                        if ($user_id) {
                                            //To generate wallet account
                                            // $this->wallet_model->w_id = $this->generateid_model->getRequiredId('wallet');
                                            $this->wallet_model->user_id = $userdata[0]->id;
                                            $this->wallet_model->amt = 0.00;
                                            $this->wallet_model->status = "Y";
                                            $this->wallet_model->comment = "Created Account";
                                            $this->wallet_model->added_by = $user_id;
                                            $this->wallet_model->save();



                                            $reg_username = $display_name;
                                            $reg_sms_array = array("{{username}}" => ucwords($reg_username));
                                            $temp_sms_msg = str_replace(array_keys($reg_sms_array), array_values($reg_sms_array), BOS_ACCOUNT_ACTIVATION);
                                            $is_send = sendSMS($userdata[0]->mobile_no, $temp_sms_msg);


                                            $mailbody_array = array("mail_title" => "email_verification_success",
                                                "mail_client" => $userdata[0]->email,
                                                "mail_replacement_array" => array("{{username}}" => ucwords($display_name))
                                            );

                                            $mail_result = setup_mail($mailbody_array);

                                            $this->session->set_flashdata('msg_type', 'success');
                                            $this->session->set_flashdata('msg', 'Registration completed successfully. Please login to verify your account.');

                                            redirect(base_url() . "admin/login/login_view");
                                        } else {
                                            log_data("registration/" . date("M") . "/user_registration_log.log", "User not saved in database", "DB Query");

                                            $this->session->set_flashdata('msg_type', 'error');
                                            $this->session->set_flashdata('msg', 'Some error occured. Please try again.');
                                            redirect(base_url() . "admin/login/login_view");
                                        }
                                    }
                                } else {
                                    log_data("registration/" . date("M") . "/user_registration_log.log", "Username,email,displayname empty", "empty fields");
                                    $this->session->set_flashdata('msg_type', 'error');
                                    $this->session->set_flashdata('msg', 'Please provide information to register.');
                                    redirect(base_url() . "home");
                                }
                            } else {
                                log_data("registration/" . date("M") . "/user_registration_log.log", "Captcha failed", "Captcha");
                                $this->session->set_flashdata('msg_type', 'error');
                                $this->session->set_flashdata('msg', 'Please verify captcha.');
                                redirect(base_url() . "home");
                            }
                        } else {
                            $string_to_save = "Password missmatched. password entered is " . $password . " and confirm password entered is" . $confirm_password;
                            log_data("registration/" . date("M") . "/user_registration_log.log", $string_to_save, "Post data for registration");
                            $this->session->set_flashdata('msg_type', 'error');
                            $this->session->set_flashdata('msg', 'Password missmatched.');
                            redirect(base_url() . "home");
                        }
                    } else {
                        log_data("registration/" . date("M") . "/user_registration_log.log", "Captcha not verified", "Captcha");
                        $this->session->set_flashdata('msg_type', 'error');
                        $this->session->set_flashdata('msg', 'Please verify captcha.');
                        redirect(base_url() . "home");
                    }
                } else {
                    log_data("registration/" . date("M") . "/user_registration_log.log", "Form validation failed", "Form Validation");
                    $this->session->set_flashdata('msg_type', 'error');
                    $this->session->set_flashdata('msg', 'Please enter proper data.');
                    redirect(base_url() . "home");
                }
            } else {
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', 'Invalid key. Please contact our customer support.');
                load_front_view(INVALID_KEY);
            }
        } else {
            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'Some unexpected has occured.');
            load_front_view(INVALID_KEY);
        }
    }

    /*
     * @Author      : Suraj Rathod
     * @function    : activation_email
     * @param       : $key -. key sent in mail
     * @detail      : to email.               
     */

    function activation_email($userdata, $to_update, $custom_data = array()) {
        $id = $this->users_model->update($userdata[0]->id, $to_update);

        if ($id) {
            //Send sms
            $reg_username = ($userdata[0]->display_name != "") ? $userdata[0]->display_name : $userdata[0]->first_name . " " . $userdata[0]->last_name;
            $reg_sms_array = array("{{username}}" => ucwords($reg_username));
            $temp_sms_msg = str_replace(array_keys($reg_sms_array), array_values($reg_sms_array), BOS_ACCOUNT_ACTIVATION);
            $is_send = sendSMS($userdata[0]->mobile_no, $temp_sms_msg);

            if (isset($custom_data["mail_title"])) {
                $mailbody_array = array("mail_title" => $custom_data["mail_title"],
                    "mail_client" => $userdata[0]->email,
                    "mail_replacement_array" =>
                    array("{{username}}" => ucwords(strtolower($userdata[0]->first_name . " " . $userdata[0]->last_name))
                    )
                );

                $mail_result = setup_mail($mailbody_array);

                $this->session->set_flashdata('msg_type', 'success');

                if (!empty($custom_data) && isset($custom_data["verification_type"])) {
                    if (trim($custom_data["verification_type"]) == "account") {
                        $this->session->set_flashdata('msg', 'User activated successfully. Please login in to your account.');
                    } else if (trim($custom_data["verification_type"]) == "email") {
                        $this->session->set_flashdata('msg', 'Email verified successfully.');
                    }
                }

                redirect(base_url() . "admin/login/login_view");
            } else {
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', 'Verification done successfully.');
                redirect(base_url() . "admin/login/login_view");
            }
        } else {
            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'Please try again.');
            load_front_view(UNEXPECTED_ERROR);
        }
    }
		
	function resend_otp() {
		$response['msg_type'] = "";
		$response['msg'] = "";
		
		$input = $this->input->post();
		$resend_key = $input['resend_key'];
		$continue = true;
		
		$user_common_data = unserialize($this->session->userdata('user_common_data'));		
		
		$user_id = $user_common_data['user_id'];
		$display_name = $user_common_data['display_name'];
		$mobile_no = $user_common_data['username'];
		
		if($resend_key === "registration")
		{
			$sms_template = REGISTRATION_OTP;
			$reason = 'registration';
		}
		else if($resend_key === "reset_rmn_old")
		{
			$sms_template = RESET_USERNAME_OTP;
			$reason = 'reset_username';
		}
		else if($resend_key === "reset_rmn_new")
		{
			$mobile_no = $user_common_data['new_username'];
			$sms_template = RESET_USERNAME_OTP;
			$reason = 'reset_username';
		}
		else
		{
			$continue = false;
		}
		
		if($continue)
		{		
			$otp_response = check_otp_attempts('registration_otp');
			if($otp_response['msg_type'] == 'success')
			{			
				$response = save_n_send_otp($user_id, $mobile_no, $display_name, $sms_template, $reason);
			}
			else
			{
				$response['msg_type'] = "error";
				$response['msg'] = $otp_response['msg'];
			}	
		}
		
		data2json($response);
	}
	
    function user_verify($verify_key="", $hashid="") {		
		$this->session->unset_userdata('user_common_data');			
		$data = array();
		$verify_email = false;
		$verify_mobile = false;
		$verify_adhar = false;
				
		$response = verify_email($verify_key, $hashid);		
		if($response['msg_type'] == 'success')
		{
			$user_id = $response['msg'];
			$user_data = $this->users_model->where('id', $user_id)->find_all();			
			if(count($user_data) > 0)
			{
				$user_data = $user_data[0];
				$display_name = $user_data->display_name;
				$mobile_no = rokad_decrypt($user_data->username, $this->config->item('pos_encryption_key'));
				$email = rokad_decrypt($user_data->email, $this->config->item('pos_encryption_key'));
				$adharcard = rokad_decrypt($user_data->adharcard, $this->config->item('pos_encryption_key'));
				
				$user_common_data = array('user_id' => $user_id,
						'display_name' => $display_name,
						'username' => $mobile_no,
						'email' => $email,
						'adharcard' => $adharcard
				);			
				$this->session->set_userdata('user_common_data', serialize($user_common_data));
				
				// check if mobile is verified
				if($user_data->otp_verify_status == "Y")
				{					
					/* if($user_data->adhar_verify_status == "Y")
					{ 
						// all details are verified
						$data['msg_type'] = "success";
						$data['msg'] = "All details have been verified successfully";
					}
					else
					{ // redirect to verify aadhar card						
						$random_key = sha1(getRandomId(8));
						$this->session->set_userdata('random_key', $random_key);
						$this->session->set_userdata('verify_mobile', 'Y');
						redirect('admin/login/show_adhar_form/'.$random_key);
					} */
					
					// all details are verified
					$data['msg_type'] = "success";
					$data['msg'] = "All details have been verified successfully";
				}
				else
				{
					$response = save_n_send_otp($user_id, $mobile_no, $display_name, REGISTRATION_OTP, 'registration');
					if($response['msg_type'] == 'success')
					{
						// redirect to verify RMN
						$random_key = sha1(getRandomId(8));
						$this->session->set_userdata('random_key', $random_key);
						$this->session->set_userdata('verify_email', 'Y');						
						redirect('admin/login/show_otp_form/'.$random_key);
					}
					else
					{
						$data['msg_type'] = "error";
						$data['msg'] = $response['msg'];
					}
				}			
			}
			else
			{
				$data['msg_type'] = "error";
				$data['msg'] = "Invalid user";
			}		
		}
		else
		{
			$data["msg_type"] = 'error';
			$data["msg"] = $response['msg'];
		}
		
		load_front_view(VERIFY_MSG, $data);
	}
	
	function show_otp_form($random_key="") {
		if($random_key == $this->session->userdata('random_key'))
		{
			if($this->session->userdata('verify_email') == "Y")
			{			
				$data['msg_type'] = 'success';
				$data['msg'] = "Email verified successfully <br> OTP has been sent on your RMN";
				$data['show_OTP_form'] = true;		
				load_front_view(EMAIL_VERIFY, $data);
			}	
		}
	}
	
	function authenticate_otp() {
		clear_otp_attempts();
		$input = $this->input->post();
		$otp = trim($input['otp_code']);
		$data['msg_type'] = "";
		$data['msg'] = "";
		$data['show_OTP_form'] = true;
				
		$this->form_validation->set_rules('otp_code', 'OTP', 'trim|required|numeric|exact_length[6]|xss_clean');		
		if ($this->form_validation->run() == FALSE)
		{	  
		  	load_front_view(EMAIL_VERIFY, $data);
		}
		else
		{
			$user_common_data = unserialize($this->session->userdata('user_common_data'));			
			if($user_common_data)
			{
				if(sizeof($user_common_data) > 0)
				{					
					$user_id = $user_common_data['user_id'];
					$username = $user_common_data['username'];
					
					$response = verify_otp($user_id, $username, $otp);
					if($response['msg_type'] == 'success')
					{
						$this->users_model->verify_user_email($user_id);
						$this->users_model->verify_user_mobile($user_id);
						
						// redirect to verify aadhar card
						/* $random_key = sha1(getRandomId(8));
						$this->session->set_userdata('random_key', $random_key);
						$this->session->set_userdata('verify_mobile', 'Y');
						redirect('admin/login/show_adhar_form/'.$random_key);	 */				
						
						 if( $this->save_login_send_welcome_mail($user_id) )
						 {
						 $data['msg_type'] = "success";
						 $data['msg'] = "TPIN is sent on your registered Mobile No.";
						 $this->session->unset_userdata('user_common_data');
						 }
					}
					else
					{
						$data["msg_type"] = 'error';
						$data["msg"] = $response['msg'];
					}					
				}
				
			}
			load_front_view(EMAIL_VERIFY, $data);
		}
	}

    function email_exists() {
        $input = $this->input->post(NULL,TRUE);

        $this->db->where('email', $input['email']);
        $this->db->from('users');

        $count = $this->db->count_all_results();

        $flag = ($count) ? 'true' : 'false';
        echo $flag;
    }

    /*
     * @Author      : Suraj Rathod
     * @function    : is_registered
     * @param       : 
     * @detail      :            
     */

    function is_registered() {
        $input = $this->input->post(NULL,TRUE);

        $where_user = array(
            "username !=" => "",
            "salt !=" => "",
            "password !=" => "",
            "email" => $input['email'],
                /* "role_id" => USER_ROLE_ID, */
        );

        $count = $this->users_model
                ->where($where_user)
                ->count_all();

        $flag = ($count) ? 'true' : 'false';
        echo $flag;
    }

    /*
     * @Author      : Suraj Rathod
     * @function    : is_user_exists
     * @param       : 
     * @detail      :            
     */

    function is_user_exists() {
        $input = $this->input->post();

        $where_user = array(
            "salt !=" => "",
            "password !=" => "",
            "role_id" => USER_ROLE_ID,
        );

        $where_or = array(
            "username" => $input['username'],
            "email" => $input['username'],
        );

        $count = $this->users_model
                ->or_where($where_or)
                ->where($where_user)
                ->count_all();

        $flag = ($count) ? 'true' : 'false';
        echo $flag;
    }

    function user_exists() {
        $input = $this->input->post(NULL,TRUE);
        $this->db->where('username', $input['username']);
        $this->db->or_where('email', $input['username']);
        $this->db->from('users');

        $count = $this->db->count_all_results();

        $flag = ($count) ? 'true' : 'false';
        echo $flag;
    }
	
	function welcome_mail() {
        //show('welcome_mail', 1);
		$display_name = 'Keshav';
				$mailbody_array = array(
			  "mail_title" => "rokad_registration",	  
			  "mail_client" => 'keshavw@bookonspot.com',
			  "mail_replacement_array" => 
			   array(
					'{{fullname}}' => ucwords($display_name),
					'{{username}}' => '34245555151',
					'{{password}}' => 'abcd@123'									
					)
			);				
			//show($mailbody_array, 1);				
			$mail_result = setup_mail($mailbody_array);
    }
	
	function change_email_verify($verify_key="", $hashid="") {
		
		if(!empty($verify_key) && !empty($hashid))
		{
			$data = array();
			$verify_email = false;			
			$mail_verification_expiry = 24;
			$user_email_details = $this->user_email_model->get_userby_key_n_id($verify_key, $hashid);
			//show($user_email_details);			
			if (count($user_email_details) > 0) {
				
				$user_id = $user_email_details['user_id'];
				$user_email_encrypted = $user_email_details['user_mail'];
				
				$curr_date = date('Y-m-d H:i:s');
				$diff = get_difference_in_time($user_email_details['sent_date'], $curr_date, 3600);					
				if($diff <= $mail_verification_expiry)
				{					
					$this->user_email_model->update_verify_status($user_email_details['id']);
					$user_data = $this->users_model->where('id', $user_id)->find_all();
					if($user_data)
					{
						$display_name = $user_data[0]->display_name;
						$data['user_id'] = $user_id;
						
						//show($data, 1);					
						if($user_data[0]->email_verify_status == "Y" && $user_data[0]->email == $user_email_encrypted)
						{
							$data['msg_type'] = "success";
							$data['msg'] = "This email id is already verified";						
							$verify_email = true;
						}
						else
						{
							$this->users_model->update_user_email($user_id, $user_email_encrypted);
							$this->users_model->verify_user_email($user_id);						
							$data['msg_type'] = "success";
							$data['msg'] = "Email verified successfully.";	
							$verify_email = true;					
						}
						$this->session->sess_destroy();
						unset($_SESSION);						
					}
					else
					{
						$data['msg_type'] = "error";
						$data['msg'] = "User not found";
					}
					load_front_view(CHANGE_EMAIL_VERIFY, $data);
				}
				else
				{
					$data['msg_type'] = "error";
					$data['msg'] = "Verification link expired";
					load_front_view(CHANGE_EMAIL_VERIFY, $data);					
				}
			} else {
				$data['msg_type'] = "error";
				$data['msg'] = "Invalid Link";
				load_front_view(CHANGE_EMAIL_VERIFY, $data);
			}
		}
		else
		{			
			$data['msg_type'] = "error";
			$data['msg'] = "Invalid Parameters in url";
			load_front_view(CHANGE_EMAIL_VERIFY, $data);
		}
	}
	
	function reset_username_verify($verify_key="", $hashid="") {
		$this->session->unset_userdata('user_common_data');
		if(!empty($verify_key) && !empty($hashid))
		{
			$data = array();
			$verify_email = false;			
			$mail_verification_expiry = 24;
			$data['show_RMN_form'] = false;	
			$user_email_details = $this->user_email_model->get_userby_key_n_id($verify_key, $hashid);
			
			if (count($user_email_details) > 0) {
				
				$user_id = $user_email_details['user_id'];			
				$curr_date = date('Y-m-d H:i:s');
				$diff = get_difference_in_time($user_email_details['sent_date'], $curr_date, 3600);					
				if($diff <= $mail_verification_expiry)
				{  
                    $where = array('id' => $user_id, 'is_deleted' => 'N', 'status' => 'Y');			
					$user_data = $this->users_model->where($where)->find_all();					
					if($user_data)
					{
						$user_data = $user_data[0];
						$user_common_data = array('user_id' => $user_id,
						'display_name' => $user_data->display_name,			
						'username' => rokad_decrypt($user_data->username,$this->config->item('pos_encryption_key')),
						'email' => rokad_decrypt($user_data->email,$this->config->item('pos_encryption_key'))
						);							
						$this->session->set_userdata('user_common_data', serialize($user_common_data));
						$data['show_RMN_form'] = true;
						load_front_view(RESET_USERNAME_VIEW, $data);
					}
					else
					{
						$data['msg_type'] = "error";
						$data['msg'] = "User Disabled/Deleted or Invalid Link";
						load_front_view(RESET_USERNAME_VIEW, $data);					
					}
				}
				else
				{
					$data['msg_type'] = "error";
					$data['msg'] = "Verification link expired";
					load_front_view(RESET_USERNAME_VIEW, $data);					
				}
			} else {
				$data['msg_type'] = "error";
				$data['msg'] = "Invalid Link";
				load_front_view(RESET_USERNAME_VIEW, $data);
			}
		}
		else
		{			
			$data['msg_type'] = "error";
			$data['msg'] = "Invalid Parameters in url";
			load_front_view(RESET_USERNAME_VIEW, $data);
		}
	}
		
	function reset_username_authenticate() {
		clear_otp_attempts();
		$data['msg_type'] = "";
		$data['msg'] = "";
		
		$input = $this->input->post(NULL,TRUE);		
		$inp_usernamme = trim(base64_decode($input['username_hidden']));
		$user_common_data = unserialize($this->session->userdata('user_common_data'));		
		if($user_common_data)
		{
			if(sizeof($user_common_data) > 0)
			{
				$user_id = $user_common_data['user_id'];
				$display_name = $user_common_data['display_name'];
				$username = $user_common_data['username'];
				$email = $user_common_data['email'];
				
				if($inp_usernamme == $username)
				{
					//show($user_data, 1);
					$response = save_n_send_otp($user_id, $username, $display_name, RESET_USERNAME_OTP, 'reset_username');
					if($response['msg_type'] == 'success')
					{
						$data['msg_type'] = $response['msg_type'];
						$data['msg'] = $response['msg'];
					}
					else
					{
						$data['msg_type'] = 'error';
						$data['msg'] = $response['msg'];
					}
				}
				else
				{ // invalid user or mobile no
					$data['msg_type'] = "error";
					$data['msg'] = "Enter valid registered mobile number";
				}				
			}
			else
			{ 
				$data['msg_type'] = "error";
				$data['msg'] = "Error";
			}
		}
		else
		{ 
			$data['msg_type'] = "error";
			$data['msg'] = "Error";
		}
		data2json($data);		
	}
	
	function validate_username_otp() {
		clear_otp_attempts();
		$data['msg_type'] = "";
		$data['msg'] = "";

		$input = $this->input->post(NULL,TRUE);		
		$otp = trim(base64_decode($input['otp_hidden']));
		$new_username = trim(base64_decode($input['new_username_hidden']));
		$user_common_data = unserialize($this->session->userdata('user_common_data'));					 

		$user_id = $user_common_data['user_id'];
		$display_name = $user_common_data['display_name'];
		$username = $user_common_data['username'];
		$email = $user_common_data['email'];
		
		if(validate_otp($otp))
		{			
			if(validate_username($new_username))
			{
				if($new_username != $username)
				{				
					$response = verify_otp($user_id, $username, $otp);	
					if($response['msg_type'] == 'success')
					{					
						$encrypted_new_username = rokad_encrypt($new_username, $this->config->item('pos_encryption_key')); 
						$exists = $this->users_model->check_username_exists($encrypted_new_username);
						if($exists)
						{
							$data['msg_type'] = "error";
							$data['msg'] = "Mobile no already registered";														
						}
						else
						{
							$response = save_n_send_otp($user_id, $new_username, $display_name, RESET_USERNAME_OTP, 'reset_username');	
							if($response['msg_type'] == 'success')
							{
								$data['msg_type'] = $response['msg_type'];
								$data['msg'] = $response['msg'];
								$user_common_data['new_username'] = $new_username;
								$this->session->set_userdata('user_common_data', serialize($user_common_data));
							}
							else
							{
								$data['msg_type'] = $response['msg_type'];
								$data['msg'] = $response['msg'];										
							}
						}					
					}
					else
					{				
						$data["msg_type"] = 'error';
						$data["msg"] = $response['msg'];				 
					}					
				}
				else
				{  	// invalid user or mobile no
					$data['msg_type'] = "error";
					$data['msg'] = "New mobile no can not be same as RMN.";			
				}	
			}
			else
			{ // invalid user or mobile no
				$data['msg_type'] = "error";
				$data['msg'] = "Enter valid 10 digit new mobile number";			
			}			
		}
		else
		{ // invalid user or mobile no
			$data['msg_type'] = "error";
			$data['msg'] = "Enter valid 6 digit otp number";			
		}		
		data2json($data);
	}
	
	function validate_newusername_otp() {
		clear_otp_attempts();
		$data['msg_type'] = "";
		$data['msg'] = "";
		
		$input = $this->input->post(NULL,TRUE);		
		$otp = trim(base64_decode($input['new_otp_hidden']));
		$user_common_data = unserialize($this->session->userdata('user_common_data'));					 

		$user_id = $user_common_data['user_id'];
		$display_name = $user_common_data['display_name'];
		$username = $user_common_data['username'];
		$email = $user_common_data['email'];
		$new_username = $user_common_data['new_username'];
		$encrypted_new_username = rokad_encrypt($new_username, $this->config->item('pos_encryption_key')); 
		
		if(validate_otp($otp))
		{
			$response = verify_otp($user_id, $new_username, $otp);	
			if($response['msg_type'] == 'success')
			{
				$this->users_model->update_username($user_id, $encrypted_new_username);								
				$data['msg_type'] = "success";
				$data['msg'] = "You have successfully changed Mobile No. Please login with new Mobile No.";	

				$this->session->sess_destroy();
				unset($_SESSION);							
			}
			else
			{				
				$data["msg_type"] = 'error';
				$data["msg"] = $response['msg'];				 
			}
		}
		else
		{ // invalid user or mobile no
			$data['msg_type'] = "error";
			$data['msg'] = "Enter valid 6 digit otp number";			
		}		
		data2json($data);
	}
	
	function cryptoJsAesDecrypt($passphrase, $jsonString){
    $jsondata = json_decode($jsonString, true);
    try {
        $salt = hex2bin($jsondata["s"]);
        $iv  = hex2bin($jsondata["iv"]);
    } catch(Exception $e) { return null; }
    $ct = base64_decode($jsondata["ct"]);
    $concatedPassphrase = $passphrase.$salt;
    $md5 = array();
    $md5[0] = md5($concatedPassphrase, true);
    $result = $md5[0];
    for ($i = 1; $i < 3; $i++) {
        $md5[$i] = md5($md5[$i - 1].$concatedPassphrase, true);
        $result .= $md5[$i];
    }
    $key = substr($result, 0, 32);
    $data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
    return json_decode($data, true);
	}
	
	function show_adhar_form($random_key="") {
		if($random_key == $this->session->userdata('random_key'))
		{			
			if($this->session->userdata('verify_mobile') == "Y")
			{
				$user_common_data = unserialize($this->session->userdata('user_common_data'));
				if($user_common_data)
				{
					if(sizeof($user_common_data) > 0)
					{
						$user_id = $user_common_data['user_id'];
						$data['display_name'] = $user_common_data['display_name'];
						$data['mobile_no'] = $user_common_data['username'];
						$adharcard = $user_common_data['adharcard'];						
						if($this->users_model->get_adhar_status($user_id) == 'Y')
						{
							$data['msg_type'] = "error";
							$data['msg'] = "Aadhar Details are already verified.";
							load_front_view(VERIFY_MSG, $data);
						}
						else
						{
							$data['show_adhar_verify_link'] = false;
							$data['show_adhar_form'] = false;							
							if($adharcard == '')
							{
								//show('i am here', 1);
								$data['msg_type'] = 'success';
								$data['msg'] = "Mobile number verified successfully";
								$data['show_adhar_form'] = true;
							}
							else
							{
								$response = get_adhar_details($adharcard);
								if($response['msg_type'] == "success")
								{
									$data['adhar_details'] = $response['msg'];
									$data['show_adhar_verify_link'] = true;
								}
								else
								{
									$data['msg_type'] = "error";
									$data['msg'] = $response['msg'];
									$data['show_adhar_form'] = true;
								}
							}							
							load_front_view(AADHAR_FORM, $data);							
						}				
					}
				}				
			}			
		}		
	}
	
	function authenticate_adhar() {	
		$input = $this->input->post();
		//show($input);
		$adharcard = trim($input['adharcard']);
		$data['msg_type'] = "";
		$data['msg'] = "";
		
		$data['show_adhar_verify_link'] = false;
		$data['show_adhar_form'] = false;
		
		$this->form_validation->set_rules('adharcard', 'Aadhar Card', 'trim|required|numeric|exact_length[12]|xss_clean');
		if ($this->form_validation->run() == FALSE)
		{			
			$data['show_adhar_form'] = true;
			load_front_view(AADHAR_FORM, $data);
		}
		else
		{			
			$response = get_adhar_details($adharcard);			
			if($response['msg_type'] == "success")
			{				
				$user_common_data = unserialize($this->session->userdata('user_common_data'));
				if($user_common_data)
				{
					if(sizeof($user_common_data) > 0)
					{
						$user_common_data['adharcard'] = $adharcard; // update user aadhar id
						$this->session->set_userdata('user_common_data', serialize($user_common_data));
						
						$data['display_name'] = $user_common_data['display_name'];
						$data['mobile_no'] = $user_common_data['username'];
						
						$data['adhar_details'] = $response['msg'];
						$data['show_adhar_verify_link'] = true;						
					}
				}			
			}
			else
			{
				$data['msg_type'] = "error";
				$data['msg'] = $response['msg'];
				$data['show_adhar_form'] = true;
			}
			load_front_view(AADHAR_FORM, $data);
		}
	}
	
	function adhar_init_success(){
		$inp = $this->input->get(NULL, TRUE);
		if(sizeof($inp) > 0)
		{
			if($inp['hash'] != "" && $inp['uuid'] != "" && $inp['requestId'] != "" && $inp['status'] != "")
			{
				$concat_char = '|';					
				
				$salt		= $this->config->item("aadhar_salt");				
				$saCode		= $this->config->item("aadhar_saCode");
				
				$user_common_data = unserialize($this->session->userdata('user_common_data'));				
				if($user_common_data)
				{
					if(sizeof($user_common_data) > 0)
					{
						$adharId = $user_common_data['adharcard'];
						$user_id = $user_common_data['user_id'];						
						if($this->users_model->get_adhar_status($user_id) == 'Y')
						{
							$data['msg_type'] = "error";
							$data['msg'] = "Aadhar Details are already verified.";
							load_front_view(VERIFY_MSG, $data);
						}
						else
						{
							$response_sequence  = $salt . $concat_char . $inp['requestId'] . $concat_char . $adharId . $concat_char . $saCode;
							$response_hash = hash('sha256', $response_sequence);
							
							if($inp['status'] == "success")
							{
								if($response_hash == $inp['hash'])
								{ // hash matched at response so now we can fetch kyc data
									/* $ekyc_request_url	= $this->config->item("aadhar_ekyc_request_url");
									 $headers= array('Accept: application/json','Content-Type: application/json');
									 
									 $ekyc_request_sequence  = $response_uuid . $concat_char . $saCode . $concat_char . $adharId . $concat_char . $response_requestId . $concat_char . $salt;
									 $ekyc_request_hash      = hash('sha256', $ekyc_request_sequence);
									 
									 $ekyc_request_data = array(
									 "saCode" => $saCode,
									 "uuid" => $response_uuid,
									 "requestId" => $response_requestId,
									 "aadhaarId" => $adharId,
									 "hash" => $ekyc_request_hash
									 );
									 
									 $ch = curl_init($ekyc_request_url);
									 curl_setopt($ch, CURLOPT_POST, TRUE);
									 curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
									 curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($ekyc_request_data));
									 curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
									 curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
									 curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
									 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
									 $result['response'] = curl_exec($ch);
									 curl_close($ch);
									 $response = json_decode($result['response']); */
									
									// if success then get the data from response and save to database
									// inform user that his kyc details are updated
									
									// if failure then show user error message
									
									$result = get_sample_response();
									$result = json_decode($result, true);
									
									//show($result);
									$ekyc_response_sequence = $salt . $concat_char . $result['requestId'] . $concat_char . $adharId . $concat_char . $saCode . $concat_char . $result['uuid'];
									$ekyc_response_hash = hash('sha256', $ekyc_response_sequence);
									
									if($ekyc_response_hash == $result['hash'])
									{
										if($result['success'] == true)
										{
											$user_photo = $this->save_adhar_photo($user_id, $result);
											//show($user_photo, 1);
											if($user_photo)
											{
												if( $this->save_adhar_details($user_id, $result, $user_photo) )
												{
													if( $this->save_login_send_welcome_mail($user_id, $adharId, $user_photo) )
													{
														$data['msg_type'] = "success";
														$data['msg'] = "Aadhar id is verified.<br> TPIN is sent on your registered Mobile No.";
														$this->session->unset_userdata('user_common_data');
													}
												}
											}
										}
										else
										{
											$response_errorcode = $result['aadhaar-status-code'];
											$error_msg = get_aadhar_error_msg($response_errorcode);
											if($error_msg == 'error')
											{
												$error_msg = 'Error Occured, Please contact support';
											}
											$data['msg_type'] = "error";
											$data['msg'] = $error_msg;
										}
									}
									else
									{
										$data['msg_type'] = "error";
										$data['msg'] = "Hash does not match, try again";
									}
								}
								else
								{
									$data['msg_type'] = "error";
									$data['msg'] = "Hash does not match, try again";
								}
							}
							load_front_view(VERIFY_MSG, $data);
						}											
					}					
				}						
			}			
		}		
	}	
	
	function adhar_init_failure(){
		$inp = $this->input->get(NULL, TRUE);
		$data['msg_type'] = "";
		$data['msg'] = "";
		if(sizeof($inp) > 0)
		{
			if($inp['hash'] != "" && $inp['uuid'] != "" && $inp['requestId'] != "" && $inp['status'] != "")
			{
				$concat_char = '|';
				
				$salt		= $this->config->item("aadhar_salt");				
				$saCode		= $this->config->item("aadhar_saCode");
				
				$user_common_data = unserialize($this->session->userdata('user_common_data'));
				if($user_common_data)
				{
					if(sizeof($user_common_data) > 0)
					{
						$adharId = $user_common_data['adharcard'];
						
						$response_sequence  = $salt . $concat_char . $inp['requestId'] . $concat_char . $adharId . $concat_char . $saCode;
						$hash 				= hash('sha256', $response_sequence);
						
						if($inp['status'] == "failure")
						{
							if($hash == $inp['hash'])
							{
								$error_msg = get_aadhar_error_msg($inp['err']);
								if($error_msg == 'error')
								{
									$error_msg = 'Error Occured, Please contact support';
								}
								$data['msg_type'] = "error";
								$data['msg'] = $error_msg;
							}
							else
							{
								$data['msg_type'] = "error";
								$data['msg'] = "Hash does not match, try again";
							}
						}						
					}
				}						
			}			
			load_front_view(VERIFY_MSG, $data);
		}
	}	
	
	private function save_login_send_welcome_mail($user_id, $adharId="", $user_photo="")
	{
		$result = false;
		
		$user_data = $this->users_model->where('id', $user_id)->find_all();	
		$user_data = $user_data[0];
		
		$display_name = $user_data->display_name;
		$email		  = rokad_decrypt($user_data->email, $this->config->item('pos_encryption_key'));
		$username 	  = rokad_decrypt($user_data->username, $this->config->item('pos_encryption_key'));
		
		// generate password and save to database
		$password = getRandomId(8);
		$salt = getRandomId();
		$pass = md5($salt . $password);		
		
		if($this->users_model->save_password($user_id, $pass, $salt))
		{
			$pass_data = array("user_id" => $user_id,
					"salt" => $salt,
					"password" => $pass,
					"created_date" => date('Y-m-d H:i:s')
			);
			
			if($this->user_password_model->save_password_log($pass_data))
			{
				// generate TPIN and save to database
				$tpin = generate_tpin();
				$encrypted_tpin = rokad_encrypt($tpin, $this->config->item('pos_encryption_key'));
				
				if($this->users_model->save_tpin($user_id, $encrypted_tpin))
				{
					$tpin_data = array(
							"user_id" => $user_id,
							"tpin" => $encrypted_tpin,
							"created_date" => date('Y-m-d H:i:s')
					);
					
					if($this->user_tpin_model->save_tpin_log($tpin_data))	
					{
						// log tpin saved
						$tpin_log_data = array(
								"user_id" => $user_id,
								"tpin" => $tpin,
								"encrypted_tpin" => $encrypted_tpin,
								"sent_date" => date('Y-m-d H:i:s')
						);
						
						log_data("tpin/tpin_".date("d-m-Y")."_log.log",$tpin_log_data, 'otp_data array');
						// log tpin saved
						
						// send tpin sms to user
						$reg_sms_array = array("{{username}}" => ucwords($display_name), "{{tpin}}" => $tpin);
						$temp_sms_msg = str_replace(array_keys($reg_sms_array), array_values($reg_sms_array), REGISTRATION_TPIN);
						$is_send = sendSMS($username, $temp_sms_msg);
						
						// send welcome email to user
						$mailbody_array = array(
								"mail_title" => "rokad_registration",
								"mail_client" => $email,
								"mail_replacement_array" =>
								array(
										'{{fullname}}' => ucwords($display_name),
										'{{username}}' => $username,
										'{{password}}' => $password,
										'{{rokad_support_mail}}' => ROKAD_SUPPORT_MAIL,
										'{{home_page_url}}' => base_url(),
										'{{logo}}' => '<img src="'.DOMAIN_NAME_VAL.'/assets/travelo/front/images/logo.png" style="width:104px;"  />'
								)
						);
						//show($mailbody_array, 1);
						$mail_result = setup_mail($mailbody_array);
						
						// activate aadhar status
						if($adharId != "" && $user_photo != "")
						{
							$enc_adharId = rokad_encrypt($adharId, $this->config->item('pos_encryption_key'));
							$this->users_model->verify_user_adhar($user_id, $enc_adharId, $user_photo);
						}					
						
						// auto activate user
						$this->users_model->auto_activate_user($user_id);						
						$result = true;
						
					}				
				}
			}			
		}
		
		return $result;
	}	
	
	private function save_adhar_details($user_id, $result_data, $user_photo)
	{
		$this->load->model('aadhar_details_model');
		$kyc_data = $result_data['kyc'];
		
		$exists = $this->aadhar_details_model->is_user_exists($user_id);		
		if($exists != "" || $exists != NULL)
		{
			$this->aadhar_details_model->id = $exists['id'];
		}
				
		$this->aadhar_details_model->user_id = $user_id;
		$this->aadhar_details_model->aadhaar_id = $result_data['aadhaar-id'];
		$this->aadhar_details_model->photo_base64 = $kyc_data['photo'];
		$this->aadhar_details_model->photo_binary = $user_photo;
		$this->aadhar_details_model->name = $kyc_data['poi']['name'];
		$this->aadhar_details_model->dob = $kyc_data['poi']['dob'];
		$this->aadhar_details_model->gender = $kyc_data['poi']['gender'];
		$this->aadhar_details_model->phone = $kyc_data['poi']['phone'];
		$this->aadhar_details_model->email = $kyc_data['poi']['email'];
		
		$this->aadhar_details_model->father_name = $kyc_data['poa']['co'];	
		$this->aadhar_details_model->house_num = $kyc_data['poa']['house'];	
		$this->aadhar_details_model->landmark = $kyc_data['poa']['lm'];	
		$this->aadhar_details_model->colony_name = $kyc_data['poa']['lc'];
		$this->aadhar_details_model->subdistrict = $kyc_data['poa']['subdist'];
		$this->aadhar_details_model->district = $kyc_data['poa']['dist'];
		$this->aadhar_details_model->state = $kyc_data['poa']['state'];	
		$this->aadhar_details_model->country = $kyc_data['poa']['country'];
		$this->aadhar_details_model->pincode = $kyc_data['poa']['pc'];
		$this->aadhar_details_model->post_office_name = $kyc_data['poa']['po'];
		
		$this->aadhar_details_model->local_data = json_encode($kyc_data['local-data']);
		$this->aadhar_details_model->raw_CmpResp = $kyc_data['raw-CmpResp'];
		$this->aadhar_details_model->aadhaar_reference_code = $result_data['aadhaar-reference-code'];
		$this->aadhar_details_model->hash = $result_data['hash'];
		$this->aadhar_details_model->uuid = $result_data['uuid'];
		$this->aadhar_details_model->requestId = $result_data['requestId'];
		$this->aadhar_details_model->date_added = date('Y-m-d H:i:s');
		
		if($this->aadhar_details_model->save())
		{			
			return true;
		}		
		return false;
	}	
	
	private function save_adhar_photo($user_id, $result_data)
	{		
		$image_type = 'data:image/jpg;base64,';
		$image_data = str_replace($image_type, '', $result_data['kyc']['photo']);
		$image_data = str_replace(' ', '+', $image_data);
		$image_decoded = base64_decode($image_data); // Decode image using base64_decode
		$filename = rand(100,999);
		$filename .= time() . '.jpg';
		if(file_put_contents(PROFILE_IMAGE.$filename, $image_decoded))
		{
			return $filename;
		}		
		return false;
	}
}

//end of login class
