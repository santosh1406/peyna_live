<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Subservice extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('subservice_model', 'service_master_model','providers_master_model','servicetype_model'));
        $this->load->library(array('form_validation', 'bookonspot'));
        is_logged_in();
    }

    public function index() {
        $data = array();
        $data['subservice_list'] = $this->subservice_model->find_all();
        // show($data, 1);
        load_back_view(SUBSERVICE_VIEW, $data);
    }

    /*
     * @function    : get_list
     * @param       : 
     * @detail      : all service  list.
     *                 
     */

    function get_list() {

        $this->datatables->select("sm.id,sm.service_name, service_id, status");
        $this->datatables->from("sub_services sm");

        // $action = '<a href="javascript:void" class="edit_btn margin" ref="$1"> 
        // <i  class="glyphicon glyphicon-pencil"></i></a>
        // <a href="javascript:void" class="change_status margin" ref="$1"> 
        // <i  class="glyphicon glyphicon-flag" title="De-activate"></i> 
        // </a>';
        // $action = 'service_id';

        // $this->datatables->add_column('action', $action, 'id');
        $data = $this->datatables->generate('json');
        echo $data;
    }

    public function create_subservice() {

        $services = $this->service_master_model->get_all_services();
        $provider = $this->providers_master_model->find_all();
        $types = $this->servicetype_model->get_all_service_types();
        
        load_back_view(CREATE_SUBSERVICE_VIEW, ['services' => $services,'providers' => $provider,'types' =>$types]);
    }

    public function is_subservice_exists() {
        $input = $this->input->post();
        $count = $this->subservice_model->where('service_name', $input['name'])->count_all();
        $flag = ($count) ? 'true' : 'false';
        echo $flag;
    }
    
    // edited by sonali on 16-feb-2019 
    public function save_subservice() {
        $input = $this->input->post();
        //show($input,1);
       if($input['types'] == 'DMT'){
           $input['service'] = '4';
       }elseif($input['types'] == 'BBPS'){
           $input['service'] = BBPS_UTILITIES_SERVICE_ID;
       }
       
            $group = [
                'service_name' => $input['service_name'],
                'type' => $input['types'],
                'service_id' => !empty($input['service']) ? $input['service']: '',
                'provider_id' => !empty($input['provider_id']) ? $input['provider_id']: '',
                'lower_slab' => !empty($input['lower_slab']) ? $input['lower_slab']: '',
                'upper_slab' => !empty($input['upper_slab']) ? $input['upper_slab']: '',
                'customer_charge' => !empty($input['cust_charge']) ? $input['cust_charge']: '',
                
                'bos_caped_per' => !empty($input['bos_caped_per']) ? $input['bos_caped_per']: '',
                'bos_caped_rupees' => !empty($input['bos_caped_rupees']) ? $input['bos_caped_rupees']: '',
                'agent_caped_per' => !empty($input['agent_caped_per']) ? $input['agent_caped_per']: '',
                'agent_caped_rupees' => !empty($input['agent_caped_rupees']) ? $input['agent_caped_rupees']: '',
                
                'service_code' => ($input['types'] == 'DMT' || $input['types'] == 'BBPS') ? $input['service_name'] : trim(str_replace(' ','',$input['service_name'])).$input['types'],
            ];
           //show($group,1);
            $serviceId = $this->subservice_model->insert($group);

        //edited by harshada kulkarni on 30-08-2018
        /*
          // create library for the service
          $file = FCPATH . 'application/libraries/services/service_' . $serviceId . '.php';
          make_dir($file, 0777, true);

          file_put_contents($file, PHP_EOL . "<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); " . PHP_EOL, FILE_APPEND);
          file_put_contents($file, PHP_EOL . "class Service_" . $serviceId . " extends CI_Model {" . PHP_EOL, FILE_APPEND);
          file_put_contents($file, PHP_EOL . "public function __construct() {" . PHP_EOL, FILE_APPEND);
          file_put_contents($file, PHP_EOL . "parent::__construct();" . PHP_EOL, FILE_APPEND);
          file_put_contents($file, PHP_EOL . "}" . PHP_EOL, FILE_APPEND);
          file_put_contents($file, PHP_EOL . "}" . PHP_EOL, FILE_APPEND);

          // create commission view for the service
          $file = FCPATH . 'application/views/adminlte/back/admin/commission/services/service_' . $serviceId . '.php';
          make_dir($file, 0777, true);
          file_put_contents($file, PHP_EOL . " " . PHP_EOL, FILE_APPEND);

          $update_array = array(
          "library_name" => 'service_' . $serviceId,
          );
          $update = $this->service_master_model->update_where('id', $serviceId, $update_array);
         * 
         */
        $this->session->set_flashdata('msg_type', 'success');
        $this->session->set_flashdata('msg', 'Sub Service Added Sucessfully.');

        redirect(site_url() . '/admin/subservice');
     
    }

    /*
     * @function    : edit_service
     * @param       : $id
     * @detail      : edit service.
     *                 
     */

    public function edit_subservice() {
        $input = $this->input->post();

        $this->subservice_model->id = $input["id"];
        $servicedata = $this->subservice_model->select();

        if (count($servicedata) > 0) {
            $data["flag"] = '@#success#@';
            $data["service_data"] = $servicedata;
        } else {
            $data["flag"] = '@#failed#@';
        }


        data2json($data);
    }

    /**
     * [update_service description] : update the service name or service detail
     * @return [type] [description]
     * @author mihir shah
     */
    public function update_subservice() {
        $input = $this->input->post();
        //by pallavi on 4-2-19
        $this->subservice_model->service_name = $input["service_name"];
            $servicedata1 = $this->subservice_model->select();

        $config = array(
            array('field' => 'service_name', 'label' => 'Name', 'rules' => 'trim|required', 'errors' => array('required' => 'Please enter Subservice name.')),
        );

        if (form_validate_rules($config) == FALSE) {

            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'Sub service not updated,kindly do again.');

            redirect(base_url() . 'admin/subservice');
        }else if(count($servicedata1) > 0){//by pallavi
            $this->session->set_flashdata('msg_type', 'success');
            $this->session->set_flashdata('msg', 'Sub service Name alredy exist.');
             redirect(base_url() . 'admin/subservice');
            } else {
		        if($input['type'] == 'DMT'){
	           $service_code = $input['service_name'];
	       }else{
	           $service_code = trim(str_replace(' ','',$input['service_name'])).$input['type'];
		}
            $update_array = array(
                "service_name" => $input['service_name'],
                'service_code' => $service_code,
            );

            $update = $this->subservice_model->update_where('id', $input['id'], $update_array);
            $this->session->set_flashdata('msg_type', 'success');
            $this->session->set_flashdata('msg', 'Sub service updated successfully');

            redirect(base_url() . 'admin/subservice');
        }
    }

    public function redirect_commission() {
        $input = $this->input->post();
        $library = trim($input['library']);
        if (!empty($library)) {
            $this->{$library}->commission_redirect();
        }
    }

    public function change_subservice_status(){
      $input = $this->input->post();
      $this->subservice_model->id = $input["id"];
      $servicedata = $this->subservice_model->select();

      $update_array = array(
            "status" => ($servicedata->status == '0' ? '1' : '0')
      );

      $update = $this->subservice_model->update_where('id', $input['id'], $update_array);

      data2json(['type' => 'success']);
    }

    public function getSubServicesByServiceID(){
      $input = $this->input->post();
      $subservices = $this->subservice_model->getSubServicebyServiceID($input['id']);
      
      data2json(['data' => $subservices]);

      // show($subservices, 1);


    }

}
