<?php

class Ticket_window_topup extends CI_Controller {

	public function __construct() {
		parent::__construct();
		is_logged_in();
		$this->load->model(array('cb_payment_transaction_fss_model','users_model'));
		$this->load->library('form_validation');
		$this->load->library('cca_payment_gateway');

	}

	public function index() { 
		load_back_view(TICKET_WINDOW_TOPUP_VIEW);
	}
	

	public function save_amt(){
		$input = $this->input->post(NULL,true);
		 
		$topup_amt = $input['topup_amt'];
		$config = array(
			 			array('field' => 'topup_amt','label' => 'Topup Amount','rules'=>'trim|required|numeric|xss_clean|greater_than[0]','error'=>array('required'=>'Please enter valid topup amount'))
			 		);
			 			if(form_validate_rules($config) == FALSE) {
			 				$data['error'] = $this->form_validation->error_array();
			 				log_data('ticket_window_topup/'.date('Y').'/'.date('m').'/'.date('d').'/ticket_window_log.log','User Entry Data',$input);
			 				log_data('ticket_window_topup/'.date('Y').'/'.date('m').'/'.date('d').'/'.date('m').'/'.date('Y').'/ticket_window_log.log','Form Error Data',$data['error']);
			 				$this->session->set_flashdata('msg_type','success');
			 				$this->session->set_flashdata('msg',$data['error']['topup_amt']);
			 				redirect(site_url().'admin/ticket_window_topup/index');
			 			} else {
			 				$topupAmt = intval($topup_amt);
			 				if($topupAmt <=0)
			 				{
			 					$data['flag'] = "@#failed#@";
			 					$data['msg_type'] = 'error';
			 					$data['msg'] = 'Amount should be greater than 0';			 					
			 				} else {
			 					
			 					$user_data = $this->users_model->where('id',$this->session->userdata('user_id'))->find_all();
			 					$unique_id = "agw".uniqid().getRandomId(4);

			 					$track_id=$unique_id;

			 					 $top_up_pg_details = [
					                'track_id' => $track_id,
					                'payment_by' => 'ccavenue',
					                'user_id' => $this->session->userdata('user_id'),
					                'total_amount' => $topup_amt,
					                'status' => "initialize",
					                'ip_address' => $this->input->ip_address(),
					                'user_agent' => $this->session->userdata("user_agent")
					            ];
					            
            					$payment_tran_fss_id = $this->cb_payment_transaction_fss_model->insert($top_up_pg_details);
            					
            					$op_trans_topup = array(
						                'user_id' => $this->session->userdata('user_id'),
						                'transaction_amt' => $topup_amt,
						                
						                'payment_transaction_id' => $payment_tran_fss_id,
						                'transaction_status' => "pfailed",
						                'payment_confirmation_status' => "not_done",
						                /* 'pg_tracking_id' => $track_id, */
						                'topup_by' => 'PG',
						                'remark' => 'payment_by_online',
						                'created_by' => $this->session->userdata('user_id'),
						                'created_date' => date('Y-m-d h:i:s'),
						                'transaction_date' => date('Y-m-d h:i:s'),
						                'is_status' => 'N',
						                //'is_approvel' => 'NA',
						            );
						        $msrtc_db_name = MSRTC_DB_NAME;
						       // show($op_trans_topup);
						        $op_trans_topup_id = $this->db->insert($msrtc_db_name.'.op_trans_details',$op_trans_topup);
						        $op_trans_topup_id  = $this->db->insert_id();
						        //show("tid: ".$op_trans_topup_id,1);
						        log_data("ticket_window_topup/" . date('Y').'/'.date('m').'/'.date('d') . "/ticket_window_log.log", $op_trans_topup_id, "tid");
						        log_data("ticket_window_topup/" . date('Y').'/'.date('m').'/'.date('d') . "/ticket_window_log.log", $op_trans_topup, "Topup Pg Request data");
						        if ($op_trans_topup_id) {
						        	$ReqTid = "tid=".time();
						        	$ReqorderId = $track_id;
					                $ReqTotalAmount = $topup_amt;
					                $merchant_param1 = $payment_tran_fss_id;
					                $merchant_param2 = $op_trans_topup_id;
					                $merchant_param3 = '';
					                $merchant_param4 = "";

					                $ReqMerchantOrderId = "order_id=" . $ReqorderId;
                					$ReqMerchantId = "merchant_id=" . $this->config->item("cca_merchant_id_agent");

                					$ReqAmount = "amount=" . $topup_amt;
		               				$ReqCurrency = "currency=INR";
		                			$ReqRedirectUrl = "redirect_url=" . base_url() . "admin/ticket_window_topup/get_cca_response";
		                			$ReqCancelUrl = "cancel_url=" . base_url() . "admin/ticket_window_topup/get_cca_response";
		                			$ReqLanguage = "language=EN";
		                			$ReqMerchantPara1 = "merchant_param1=" . $merchant_param1;
		                			$ReqMerchantPara2 = "merchant_param2=" . $merchant_param2;
		                			$ReqMerchantPara3 = "merchant_param3=" . $merchant_param3;
		                			$ReqMerchantPara4 = "merchant_param4=" . $merchant_param4;

		                			$ReqMerchantOrderId = "order_id=" . $ReqorderId;
                					$ReqMerchantId = "merchant_id=" . $this->config->item("cca_merchant_id_agent");
                					$ReqAmount = "amount=" . $topup_amt;
                					$ReqCurrency = "currency=INR";
                					$ReqRedirectUrl = "redirect_url=" . base_url() . "admin/ticket_window_topup/get_cca_response";
                					$ReqCancelUrl = "cancel_url=" . base_url() . "admin/ticket_window_topup/get_cca_response";
                					$ReqLanguage = "language=EN";
                					$ReqMerchantPara1 = "merchant_param1=" . $merchant_param1;
                					$ReqMerchantPara2 = "merchant_param2=" . $merchant_param2;
                					$ReqMerchantPara3 = "merchant_param3=" . $merchant_param3;
                					$ReqMerchantPara4 = "merchant_param4=" . $merchant_param4;

                					$Strhashs = trim($ReqMerchantOrderId) . "&" . trim($ReqAmount) . "&" . trim($ReqCurrency) . "&" . trim($ReqMerchantPara1) . "&" . trim($ReqMerchantPara2) . "&" . trim($ReqMerchantPara3) . "&" . trim($ReqMerchantPara4);

	                				if (isset($users_data) && count($users_data) > 0) {
	                    				if (isset($users_data[0]['first_name'])) {
	                        				$ReqBillingName = "billing_name=" . trim($users_data[0]['first_name']);
	                        				$Strhashs = $Strhashs . "&" . trim($ReqBillingName);
	                    				}

	                    				if (isset($users_data[0]['email'])) {
	                        				$ReqBillingEmail = "billing_email=" . rokad_decrypt(trim($users_data[0]['email']),$this->config->item('pos_encryption_key'));
	                        				$Strhashs = $Strhashs . "&" . trim($ReqBillingEmail);
	                   					}

	                    				if (isset($users_data[0]['phone']) && $users_data[0]['phone'] != '0') {
	                        				$ReqBillingTelNo = "billing_tel=" . trim($users_data[0]['phone']);
	                        				$Strhashs = $Strhashs . "&" . trim($ReqBillingTelNo);
	                    				}
	                				}

                					//For geo location
                					//UNCOMMENT BELOW LINE ON LIVE
                					//$ip_address = $this->session->userdata("ip_address");
                				   //COMMENT BELOW LINE ON LIVE
                					$locationdata = array();
                					$ip_address = $this->input->ip_address();

                					$ReqBillingCountry = "billing_country=India";
                					$Strhashs = $Strhashs . "&" . trim($ReqBillingCountry);
                					//AFTERWARDS DO THIS OR CHANGE LOGIC FOR merchant_param5
                					//Use hash method which is defined below for Hashing ,It will return Hashed valued of above string
                					$hashedstring = hash('sha256', $Strhashs);
                					$ReqMerchantPara5 = "merchant_param5=" . $hashedstring;
                					$Reqparam = trim($ReqTid) . "&" . trim($ReqMerchantId) . "&" . trim($ReqMerchantOrderId) . "&" . trim($ReqAmount) . "&" . trim($ReqCurrency) . "&" . trim($ReqRedirectUrl) . "&" . trim($ReqCancelUrl) . "&" . trim($ReqLanguage) . "&" . trim($ReqMerchantPara1) . "&" . trim($ReqMerchantPara2) . "&" . trim($ReqMerchantPara3) . "&" . trim($ReqMerchantPara4);
                					if (isset($ReqBillingName)) {
                    					$Reqparam .= "&" . $ReqBillingName;
                					}

                					if (isset($ReqBillingEmail)) {
                    						$Reqparam .= "&" . $ReqBillingEmail;
                					}

                					if (isset($ReqBillingTelNo)) {
                    						$Reqparam .= "&" . $ReqBillingTelNo;
                					}

                					$Reqparam .= "&" . $ReqBillingCountry;
                					$Reqparam = $Reqparam . "&" . trim($ReqMerchantPara5) . "&";

                					log_data("ticket_window_topup/" . date('Y').'/'.date('m').'/'.date('d'). "/ticket_window_log.log", $Reqparam, "Data For Payment");

                					$encrypted_data = encrypt($Reqparam, $this->config->item("cca_working_key_agent"));
                
                					$ccprocess_data["encrypted_data"] = $encrypted_data;

                					log_data("ticket_window_topup/" . date('Y').'/'.date('m').'/'.date('d') . "/ticket_window_log.log", $encrypted_data, "Encrypted Data For Payment");

               						load_front_view(AGENT_PGPROCESSING_VIEW, $ccprocess_data);
            				} else {
                						$custom_error_mail = array(
                    						"custom_error_subject" => "Data Not Add into  TopUp.",
                    						"custom_error_message" => "Data Not added into  topup while topup from pg - " . json_encode($wallet_topup)
                						);
                						$this->common_model->developer_custom_error_mail($custom_error_mail);

                						log_data("ticket_window_topup/" . date('Y').'/'.date('m').'/'.date('d'). "/ticket_window_log.log", $op_trans_topup, "Pg Request Time Data");

                						load_front_view(UNEXPECTED_ERROR);
            						}
            			}

					}
	}

	public function get_cca_response()
	{
		$data = $this->cca_payment_gateway->GetCCATopupResponse();
        $this->session->set_flashdata('msg_type', $data["page_msg_type"]);
        $this->session->set_flashdata('msg', $data["page_msg"]);
        redirect($data["page_redirect"]);			
	}

			 				
}

?>