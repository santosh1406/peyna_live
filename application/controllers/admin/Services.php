<?php
class Services extends CI_Controller
{
	public function __construct() {
        parent::__construct();
       
        $this->load->library('form_validation');
       
        $this->load->model(array('Service_master_model'));
      	//is_logged_in();

      	$this->sessionData['id'] = $this->session->userdata('user_id');
      	$this->sessionData["username"] = $this->session->userdata('username');
        $this->sessionData["password"] = $this->session->userdata('password');
        
    }

    public function index()
    {
    	
    	$data['services'] = $this->Service_master_model->get_all_services();
    	$data["agentServices"] = $this->Service_master_model->get_agent_services();
    	load_back_view(SERVICES_VIEW,$data);
    }

    public function getSubServicesbyService()
    {
	    	// POST data 
	    $postData = $this->input->post();

	    // get data 
	    $data = $this->Service_master_model->getSubServicebyServices($postData);
	    echo json_encode($data); 
    }

   	public function update_service()
    {
    	$input['sub_service_name'] = $this->input->post('sub_service_name');
    	$input['service_name'] = $this->input->post('service_name');
    	

    	$sQuery = "DELETE FROM service_mapping 
					where service_id = ".$input['service_name'] ." ";
        $query = $this->db->query($sQuery);
    	

    	if (!empty($input['sub_service_name'])) {
                
                foreach ($input['sub_service_name'] as $sub_service_id) {
                	
                    $data = array(
                        
                        'service_id' => $this->input->post('service_name'),
                        'sub_service_id' => $sub_service_id,
                        'updated_at' => date('Y-m-d H:i:s'),
                    );
                    
                    $insert_service = $this->Service_master_model->saveServiceDetails($data);
                }
                
                
            }
            

            $this->session->set_flashdata('msg_type', 'success');
            $this->session->set_flashdata('msg', 'Services updated successfully');
            redirect(base_url() . 'admin/services/');
    }




    

}

?>