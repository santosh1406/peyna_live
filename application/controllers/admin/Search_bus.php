<?php

// created by Pabhat Pal on 20-Des-2018
defined('BASEPATH') OR exit('No direct script access allowed');

class Search_bus extends CI_Controller {

    public $sessionData;
    public $agent_email;
    public $agent_mobile_no;

    public function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->library('Commission_distribution_api');
        $this->load->helper('utilities_helper');
        $this->load->model(array('Utilities_model', 'Search_bus_model','retailer_service_mapping_model'));
        is_logged_in();

        $this->sessionData['id'] = $this->session->userdata('user_id');
        $this->sessionData["username"] = $this->session->userdata('username');
        $this->sessionData["password"] = $this->session->userdata('password');
        $this->sessionData["email"] = $this->session->userdata('email');
        $this->sessionData["mobile_no"] = $this->session->userdata('mobile_no');
        $this->agent_mobile_no = $this->session->userdata('mobile_no');
        $this->agent_email = $this->session->userdata('email');
        $this->base_url = 'https://rokad.in/';
        if(empty($this->sessionData["password"]))
        {
            $this->sessionData["password"] = 'Trimax@123';
        }
       // $this->checkAccessToModule();
    }
    
    private function checkAccessToModule()
    {
        $user_data = $this->session->userdata();
        $agent_id = $user_data['user_id'];
        $retailer_services = $this->retailer_service_mapping_model->getServiceByAgentId($agent_id);
        $service_array = array_column($retailer_services, 'service_id');
        if (in_array(BOS_SERVICE_ID, $service_array)) {
            $checkCommissionExists = $this->checkCommissionExists($agent_id);
            if($checkCommissionExists) {
                return true;
            }
        } else {
            $this->session->set_flashdata('item', 'No access to module');
            redirect(base_url() . 'admin/dashboard');
        }
    }
    function index() {
        $this->checkAccessToModule();
        $this->session->unset_userdata('onwardJourny');
        $this->session->unset_userdata('returnJourny');
        $this->session->unset_userdata('passenger_details');
        $this->session->unset_userdata('bus_services_input');
        $this->session->unset_userdata('operator_name');
        $this->session->unset_userdata('bus_details');
        $this->session->unset_userdata('block_key');
        $this->session->unset_userdata('from');
        $this->session->unset_userdata('to');
        $this->session->unset_userdata('date');
        load_back_view(HOME_SEARCH);
    }

    function search() {
        $this->checkAccessToModule();
        load_back_view(ADVANCE_SEARCH);
    }

    public function search_services() {
       // print_r($_POST); exit;
        $this->checkAccessToModule();
        $this->session->unset_tempdata('from');$this->session->unset_tempdata('to');$this->session->unset_tempdata('date');
        $this->session->unset_tempdata('return_date');$this->session->unset_tempdata('searchbustype');
        $totalSeat = $min_fare = $max_fare = 0; $roundTrip = '';
        $data = $fare = $departure = $diffTime = array();
        $busType  = $operatorTypes = array();
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('from_stop', 'From Stop', 'required');
        $this->form_validation->set_rules('to_stop', 'to stop', 'required');
        $this->form_validation->set_rules('departure_date', 'Departure Date', 'required');
        $roundTrip = $this->input->post('triptype');

        if($roundTrip == 'round') {
            $this->form_validation->set_rules('return_date', 'Return Date', 'required');
        }
        $rajax = isset($_POST['rajax']) && $this->input->post('rajax') ? $this->input->post('rajax') : '';
        if ($this->form_validation->run() == FALSE && $rajax !='rajax') {
            load_back_view(HOME_SEARCH);
        } else {

            if (isset($_POST['from_stop'])) {
                $input['from'] =  $_POST['from_stop'];
                $input['to'] =  $_POST['to_stop'];
                $input['date'] = $_POST['departure_date'];
                $input['return_date'] = $_POST['return_date'];
                $input['timeslot'] = $_POST['timeslot'];
                $input['searchbustype'] = $_POST['searchbustype'];
                $input['provider'] = $_POST['provider'];
                $input['triptype'] = $_POST['triptype'];

                $from = preg_replace("/[^a-zA-Z]/", "", $input['from']);
                $to = preg_replace("/[^a-zA-Z]/", "", $input['to']);
                $date = date("d_m_Y", strtotime($input['date']));
                $file_name = strtolower($from) . "_" . strtolower($to) . "_" . $date . '.json';
                $file_name = str_replace(' ', '', $file_name);
                
                $this->session->set_userdata($input);
                $this->session->set_userdata('bus_services_input', $input);
                $bus_service_arr = array();
                $volume = array();
                if (!empty($input)) {

                    $curl_url  = $this->base_url .  "rest_server/search_bus/bus_services";
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'curl.log', $curl_url, 'Bus Services curl');
                    $file_path = FCPATH . "json_data/";
                   // if (file_exists($file_path . $file_name)) {
                      //  $json_data = file_get_contents($file_path . $file_name);
                       /* $output = json_decode($json_data); 
                        $data['result'] = $output->data->data;
                        $data['input_data'] = $input;
                        $json_data = json_encode($data); */
                         
                   // } else {
                        $curlResult = curlForPostData_BOS($curl_url, $input, $this->sessionData, $getPost = "POST");
                        $output = json_decode($curlResult);
 
                        $data['result'] = $output->data->data;
                        
                        $data['input_data'] = $input;
                        $json_data = json_encode($data);
                        if ($output->data && !empty($data['result'])) {
                            if (!file_exists($file_path . $file_name)) {
                                file_put_contents($file_path . $file_name, json_encode($data));
                            }
                        }

                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'bus_services.log', $curlResult, 'Bus Services');
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'bus_services_input.log', $input, 'Bus Services_input');
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'bus_service_data.log', $data, 'Bus Services_data');
                  //  }
                    $request_type = $this->input->post('request_type');
                    $data['serviceResult'] = json_decode($json_data, true);
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'newww.log', $data['serviceResult'], 'result');
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'newww1.log', $data['serviceResult'], 'result');
                    //filter result based on bus operator
                    if(isset($data['serviceResult']['result']) && !empty($data['serviceResult']['result'])){
                        if (empty($input['provider']) || strtolower($input['provider']) == 'all'){ //all
                            foreach ($data['serviceResult']['result'] as $key => $result) {
                                   $bus_service_arr['result'][] = $result;
                           }

                       }elseif(strtolower($input['provider']) == 'operator'){ //State transport operator
                           foreach ($data['serviceResult']['result'] as $key => $result) {
                               if($result['provider_id'] == '1' || $result['provider_id'] == '2' || $result['provider_id'] == '4' || $result['provider_id'] == '5'){
                                   $bus_service_arr['result'][] = $result;
                               }
                           }
                       }elseif (strtolower($input['provider']) == 'api') { //Private operator
                            foreach ($data['serviceResult']['result'] as $key => $result) {
                               if($result['provider_id'] == '3' || $result['provider_id'] == '6'){
                                   $bus_service_arr['result'][] = $result;
                               }
                           }
                       }

                       //echo "<pre>"; print_r($data['serviceResult']); die();
                       log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'bus_services_serviceResult'.$input['provider'].'.log', $bus_service_arr, 'Bus Services_arr'); 
                       //display state transport first
                       usort($bus_service_arr['result'], function($a, $b) {
                           return $a['provider_id'] - $b['provider_id'];
                       });

                       $data['serviceResult'] = $bus_service_arr;
                   }
                    if ($data['serviceResult']['result']) {
                        foreach ($data['serviceResult']['result'] as $k => $val) {
                           $opname = '';
                            $totalSeat += $val['available_seat'];
                            $departure[] = $val['departure_date'];
                            $fare[] = $val['seat_fare'];
                            $diffTime[] = timeDiff($val['departure_date'], $val['arrival_date']);
                            $data['serviceResult']['result'][$k]['duration'] = timeDiff($val['departure_date'], $val['arrival_date']);
                            if(is_array($val['bus_type_id'])){
                                $busType[$val['bus_type_id']['Seating']] = isset($val['bus_type_id']['Seating']) ? $val['bus_type_id']['Seating']:""; 
                            }else
                            {
                                $busType[$val['bus_type_id']] = isset($val['bus_type_name']) ? $val['bus_type_name']:""; 
                            }
                            $busTypes[] = $val['bus_type_name'];
                            if(strtolower($val['operator_name']) == 'travelyaari'){
                                $opname = $val['company_name'];
                            }else{
                                 $opname = $val['operator_name'];
                            } 
                            $operatorTypes[] = array('provider_id' =>$val['provider_id'],'op_name'=>$opname); 
                        }
                        $data['startTime'] = min($departure);
                        $data['min_fare'] = min($fare);
                        $data['max_fare'] = max($fare);
                        $data['min_time'] = min($diffTime);
                        $data['max_time'] = max($diffTime);
                         
                    }
                    
                    $data['totalSeat'] = $totalSeat;
                    $data['busType'] = $busType;
                    $data['operatorTypes'] = array_unique($operatorTypes, SORT_REGULAR);
                    //echo "<pre>"; print_r($data); die();
                     
                    $this->session->set_userdata($input);
                    $this->session->set_userdata('serviceResult', $data['serviceResult']);
                    if ($request_type == 'ajax_req' || $rajax == 'rajax') {
                        $results = $this->load->view('adminlte/back/admin/advanced_booking/services_view', $data, true);
                        $ajax_request = array('status' => 'success', 'results' => $results);
                        echo json_encode($ajax_request);
                    } else {
                        load_back_view(ADVANCE_SEARCH, $data);
                    }
                }else{
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'aaa.log', 'empty ip'.$input, 'ajax_req');
                }
            }
            //load_back_view(ADVANCE_SEARCH,$data);          
        }
    }

    public function get_stop() {
        if (isset($_GET['term'])) {
            $input['term'] = strtolower($_GET['term']);

            if (!empty($input)) {
                $aRes = $this->Search_bus_model->getStops($input['term']);
                $result = json_encode($aRes);
                echo $result;
            }
        }
    }

    function load_bus_services() {
        load_back_view(BUS_MODULE);
    }

    function get_service_stop_details() {

        $input = array(
            'provider_id' => $this->input->post('provider_id'),
            'service_id' => $this->input->post('service_id'),
            'date' => date("m/d/Y", strtotime($this->input->post('date'))),
        );

        if (!empty($input)) {
            $curl_url = $this->base_url .  "rest_server/search_bus/get_service_stop_details";
            $curlResult = curlForPostData_BOS($curl_url, $input, $this->sessionData, $getPost = "POST");
            $output = json_decode($curlResult);

            $data['item'] = $output->data->msg;
            $data['from'] = $output->data->data->from_service_stops;
            $data['to'] = $output->data->data->to_service_stops;
            echo json_encode($data);

            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'get_service_stop_details.log', $curlResult, 'Service Stop Details');
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'get_service_stop_details_ip.log', $input, 'Service Stop Details ip');
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'get_service_stop_details_op.log', $data, 'Service Stop Details op');
        }
    } 

    public function get_seat_layout() {

        $newdata = array('operator_name' => $this->input->post('operator_name'),);
        $data = array();
        $upper_berth = array();
        $lower_berth = array();
        $row_val_low = $row_val_up = array();
        
        $this->session->set_userdata('operator_name', $newdata);

        $operator_name = $this->input->post('operator_name');
        $providerId = $this->input->post('provider_id');
        if ($operator_name == 'Travelyaari') {
            $trip_id = $this->input->post('service_id');
        } else {
            $trip_id = $this->input->post('trip_id');
        }
        if($providerId == 7)
        {
            echo json_encode($data);
            return false;
        }
        $input = array(
            'provider_id' => $this->input->post('provider_id'),
            'from' => $this->input->post('from'),
            'to' => $this->input->post('to'),
            'date' => date("m/d/Y", strtotime($this->input->post('date'))),
            'trip_id' => $trip_id,
            'service_id' => $this->input->post('service_id'),
            'inventory_type' => $this->input->post('inventory_type')
        );


        $this->session->set_userdata('bus_details', $input);

        if (!empty($input)) {

            $curl_url = $this->base_url .  "rest_server/search_bus/get_seat_layout";
            $curlResult = curlForPostData_BOS($curl_url, $input, $this->sessionData, $getPost = "POST");
            $output = json_decode($curlResult);
            
            if($this->input->post('provider_id') == 6) {
                $data['seats'] = $output->data->data->seats[0];
                $data['total_rows'] = $output->data->data->total_rows;
                $data['total_cols'] = $output->data->data->total_cols;
            } else{
                $data['seats'] = $output->data->data->seats;
                $data['total_rows'] = $output->data->data->total_rows;
                $data['total_cols'] = $output->data->data->total_cols;
            }
            if($this->input->post('provider_id') != 4) {
                $data['boarding_stops'] = $output->data->data->boarding_stops;
                $data['Dropoffs'] = $output->data->data->Dropoffs;
                $data['inventory_type'] = $output->data->data->inventory_type;
                $data['no_of_berth'] = $output->data->data->no_of_berth;
                //$data['total_rows'] = $output->data->data->total_rows;
                //$data['total_cols'] = $output->data->data->total_cols;
                $data['total_rows'] = !empty($output->data->data->total_rows)? $output->data->data->total_rows:9;
                $data['total_cols'] = !empty($output->data->data->total_cols) ? $output->data->data->total_cols: 5;
                $data['layout_orientation'] = $output->data->data->layout_orientation;
               if($this->input->post('provider_id') == 6)
               {    $travelYariBD = array(
                            'boarding_stops' => $output->data->data->boarding_stops,
                            'Dropoffs' => $output->data->data->Dropoffs
                    );
                    $this->session->set_userdata('travelYari_boarding_stops', $travelYariBD);
               }
            } else
            {
                $data['seats'] = $output->data->seats;
                $data['boarding_stops'] = $output->data->boarding_stops;
                $data['Dropoffs'] = $output->data->Dropoffs;
                $data['inventory_type'] = $output->data->inventory_type;
                $data['no_of_berth'] = $output->data->no_of_berth;
                $data['total_rows'] = !empty($output->data->total_cols) ? $output->data->total_cols +2:'';
                $data['total_cols'] = !empty($output->data->total_rows)? $output->data->total_rows + 2:'';
                $data['layout_orientation'] = $output->data->layout_orientation;
            }
               
            if($this->input->post('provider_id') == 6 || $this->input->post('provider_id') == 3) {      
//                if($output->data->no_of_berth > 1 ||  $output->data->data->no_of_berth >1){
                    foreach ($data['seats'] as $key => $seat) {
                        if($seat->berth == '0'){
                            $data['seat_layout'][1][$key] = $seat;
                        }else{
                            $data['seat_layout'][2][$key] = $seat; 
                        }
                    }
//                }
            }
         
        
            
        if($this->input->post('provider_id') == 6 || $this->input->post('provider_id') == 3) { 
           $output = '';  
           $seat_layout = $this->objectToArray($data['seat_layout']);
           
           $ii = 1;
            foreach($data['seat_layout'] as $keys =>$seats){
                
                 $arr_cnt = '0';
                 $output_arr = array();
                 $output .= '<ul class="deckrow deckrow_'.$this->input->post('service_id').'">';
                 
                   if(count($data['seat_layout']) > 1) {   
                        $berth_name = ($ii == 1) ? "Lower Berth" : "Upper Berth";
                        $output .= '<li class="berth_header"><span style="font-weight:bold;">'.$berth_name.'</span></li>';
                    }
//                    if($ii == 1){
//                         $output .= '<li><a class=" myBusDriveSeatImage driver_booked" href="#" style="width: 20px; height: 20px;"></a></li>';
//                     }
                 for ($i = 0; $i < $data['total_cols']; $i++) {
                    $output .= "<li>";
                    $output .= "<ul>";
                    
                    $output_arr = array_slice($data['seat_layout'][$ii], $arr_cnt, $data['total_rows']); 
                    $output_cnt = count($data['seat_layout'][$ii]);
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'seat_layout.log', $output_arr, 'inside $output_arr');
                    $arr_cnt = $arr_cnt + $data['total_rows'];
                    //for ($j = 0; $j < $data['total_rows']; $j++) {
                foreach($output_arr as $key => $seats){
                    $cnt = count($seats);
                     
                    //log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'seat_layout.log', $seats, 'inside$seats$seats$seats');
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'seat_layout.log', $seats->seat_no, '$seats$seats$key');
//                    if (i === 0 && j === 0) {
//                         $output .= '<li><a class=" myBusDriveSeatImage driver_booked" href="#" style="width: 20px; height: 20px;"></a></li>';
//                    } else if (j === 0) {
//                        $output .= '<li><a href="#" style="width: 20px; height: 20px;">   </a></li>';
//                    }
                   
                     $seat_width  = "20px"; 
                     $seat_height = "20px"; 
                        if($seats->seat_orientation == 'horizontal') {
                           $seat_width  = "20px";
                           $seat_height = "20px"; 
                        }else if($seats->seat_orientation == 'vertical') {
                           $seat_width  = "20px";
                           $seat_height = "20px"; 
                        }
                        // if($seats[$key]->available  == "Y") {
                        
                            $current_seat_class = "available";
                            if($seats->sleeper == "true") {
                                 $seat_width  = "30px";
                                $seat_height = "16px"; 
                                if($seats->available == "N") {
                                   $current_seat_class = 'sleeper_booked';
                                }else{
                                    $current_seat_class = 'available sleeper_available';
                                }
                            }else{
                                
                                if($seats->available == "N") {
                                   $current_seat_class = 'booked';
                                }else{
                                    $current_seat_class = 'available';
                                }
                            }
                            
                            if($seats->seat_type != "") {
                                $current_seat_class = $seats->seat_type."_".$current_seat_class;
                                $data_seat_select = $seats->seat_type."_selected";
                            }
                            $ac_seat = isset($seats->ac) ? $seats->ac : "True";
                             if($seats->ac == 'false'){
                                $ac_seat = 'false';
                            }else{
                                $ac_seat = 'true';
                            }
                            if($seats->ladies_seat == ''){
                                $ladies_seat = 'false';
                            }else{
                                $ladies_seat = 'true';
                            }

                            
                             if($seats->sleeper == 'false'){
                                $sleeper = 'false';
                            }else{
                                $sleeper = 'true';
                            }
                            $berth =  isset($seats->berth) ? $seats->berth : "0";
                            $total_fare = isset($seats->total_fare) ? ($seats->total_fare) : ($seats->sub_total_fare);
                            $service_id = $this->input->post('service_id');
                            if ($ladies_seat == 'false'){
                                $gender = 'M';
                            }else{
                                $gender = 'F';
                            }

                            $output .= "<li><a class='seat $current_seat_class' href='javascript:void(0)' style='width: $seat_width; height: $seat_height' data-bus-travel-id='$service_id' data-seat-no='".$seats->seat_no."' data-fare='".$seats->sub_total_fare. "' data-current_seat_type ='" .$seats->seat_type . "' data-total-fare='" .$total_fare. "' title='Seat No " .$seats->seat_no. " | Fare : Rs. " .$seats->sub_total_fare. "' data-ac= '$ac_seat' data-sleeper='$sleeper' data-ladies-seat='$ladies_seat' data-gender='$gender' data-berth='".$seats->berth."'></a></li>";
                       
                    }
                    $output .= "</ul>";
                    $output .= "</li>";
                 }
                 $output .= "</ul>";
                 $ii++;
            }
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'seat_layout.log', $output, 'get seat layout $output');
            $data['output'] = $output;
        }
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'get_seat_layout.log', $curlResult, 'get seat layout');
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'get_seat_layout_ip.log', $input, 'get seat layout ip');
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'get seat_layout_op.log', $data, 'get seat layout op');
            echo json_encode($data);
 
        }
    }

    function save_temp_booking() {
        $start_date = new DateTime($this->input->post('dept_date'));
        $end_date = new DateTime($this->input->post('arr_date'));
        $interval = $start_date->diff($end_date);

        $days = $interval->format('%d');
        $hours = $interval->format('%h');
        $minutes = $interval->format('%i');
        //echo  'Diff. in minutes is: '.($days * 1440 + $hours * 60 + $minutes);
        $time_diff = $hours . ':' . $minutes;

        $this->session->userdata('bus_details');

        $from = $this->session->userdata['bus_details']['from'];
        $to = $this->session->userdata['bus_details']['to'];
        $arr = $dept = '';
        $arr = $this->input->post('onwardJourny')['boarding_time'];
        $dept = $this->input->post('onwardJourny')['droping_time'];
        if (!empty($arr) && !empty($dept)) {
            $arr = explode('-', $arr);
            $dept = explode('-', $dept);
        }
        $boarding_point = isset($arr[1]) ? date('Y-m-d', strtotime($this->input->post('onwardJourny')['dept_time'])) . ' ' . trim($arr[1]) : $this->input->post('onwardJourny')['dept_time'];
        $dropping_poit = isset($dept[1]) ? date('Y-m-d', strtotime($this->input->post('onwardJourny')['alighting_time'])) . ' ' . trim($dept[1]) : $this->input->post('onwardJourny')['alighting_time'];
        $input = array(
            'onwardJourny' => $this->input->post('onwardJourny'),
            'total_travel_time' => timeDiff($boarding_point, $dropping_poit),
            'boarding_point' => $boarding_point,
            'dropping_point' => $dropping_poit,
        );
        
        $this->session->set_userdata('onwardJourny', $input);
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'onwardJourny.log', $input, '$onwardJourny');
        if(!empty($this->input->post('returnJourny'))) {
            $rarr = $this->input->post('returnJourny')['boarding_time'];
            $rdept = $this->input->post('returnJourny')['droping_time'];
            if (!empty($rarr) && !empty($rdept)) {
                $rarr = explode('-', $rarr);
                $rdept = explode('-', $rdept);
            }
            $rboarding_point = isset($rarr[1]) ? date('Y-m-d', strtotime($this->input->post('returnJourny')['dept_time'])) . ' ' . trim($rarr[1]) : $this->input->post('returnJourny')['dept_time'];
            $rdropping_poit = isset($rdept[1]) ? date('Y-m-d', strtotime($this->input->post('returnJourny')['alighting_time'])) . ' ' . trim($rdept[1]) : $this->input->post('returnJourny')['alighting_time'];
            $rinput = array(
                'returnJourny' => $this->input->post('returnJourny'),
                'total_travel_time' => timeDiff($boarding_point, $dropping_poit),
                'boarding_point' => $rboarding_point,
                'dropping_point' => $rdropping_poit,
            );
            $this->session->set_userdata('returnJourny', $rinput);
        }
       
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'save_temp_booking_ip.log', $input, 'save temp booking ip');
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'save_temp_booking_ip.log', $rinput, 'Return Journy save temp booking ip');

        $data['redirect'] = base_url() . "admin/search_bus/do_temp_booking";
        echo json_encode($data);
    }

    function do_temp_booking() {

        load_back_view(PASSENGER_DETAILS);
    }

    function temp_booking() {
        
        show($_POST,1);

        $onwardJourny = $returnJourny = $seats_details = $seats_info = $postData = $postInput = $rpostInput = $newTicketDetailsData = $newonwardJourny = array();
        $countSeats = $tbirth = $convey_charge_value = 0;
        $convey_charge_type = '';
        $user_id = $this->sessionData['id'];
        $input = $this->input->post();
        $walletTransId = substr(hexdec(uniqid()), 4, 12);
        $onwardJourny = $this->session->userdata('onwardJourny');
        $returnJourny = $this->session->userdata('returnJourny');
        
        if ($onwardJourny) {
            $newonwardJourny = $onwardJourny['onwardJourny'];
            
            $provider_id = $newonwardJourny['provider_id'];
            $from = $newonwardJourny['from'];
            $to = $newonwardJourny['to'];
            $inventory_type = $newonwardJourny['inventory_type'];
            $date = $newonwardJourny['date'];
            $trip_id = $provider_id == 6 ? $newonwardJourny['service_id']: $newonwardJourny['trip_no'];
            $service_id = $newonwardJourny['service_id'];
            $boarding_stop = ($newonwardJourny['boarding_stop_name']) ? ($newonwardJourny['boarding_stop_name']) : ($from);
            $dropping_stop = ($newonwardJourny['dropping_stop_name']) ? ($newonwardJourny['dropping_stop_name']) : ($to);
            $customer_email = $input['email'];
            $customer_mobile = $input['mobile'];
            $customer_id_type = $this->input->post('id_type1');
            $customer_id_number = $this->input->post('id_number1');
            $customer_name = $this->input->post('psgr_name1');
            $customer_age = $this->input->post('psgr_age1');
            $customer_sex = $this->input->post('psgr_sex1');
            $operator_name = $newonwardJourny['op_name'];
            $sch_departure_time = date("Y-m-d H:i:s", strtotime($newonwardJourny['dept_time']));
            $sch_alighting_time = date("Y-m-d H:i:s", strtotime($newonwardJourny['alighting_time']));
            $sch_boarding_time = $onwardJourny['boarding_point'];
            $sch_dropping_time = $onwardJourny['dropping_point'];
            $total_travel_time = $onwardJourny['total_travel_time'];

            $seats_details = $input['seat'];
            $seats_info = $newonwardJourny['seats'];

            if (count($seats_details) > 0 && count($seats_info) > 0) {
                foreach ($seats_info as $birth => $lseat) {
                    $tbirth = $birth;
                    $i = 0;
                    foreach ($seats_details[$tbirth] as $key => $val) {
                        $subtot_amt = $tot_amt = '';
                        if($provider_id == 2){
                            if($val['age'] >= 65){
                                $subtot_amt = $lseat[$i]['senior_citizen_fare'];
                                $tot_amt = $lseat[$i]['senior_citizen_fare'];
                            }else{
                                $subtot_amt = $lseat[$i]['seat_fare'];
                                $tot_amt = $lseat[$i]['total_fare'];
                            }
                        }else{
                             $subtot_amt = $lseat[$i]['seat_fare'];
                             $tot_amt = $lseat[$i]['total_fare'];
                        }
                        $postData[] = array(
                            'name' => $val['name'],
                            "age" => $val['age'],
                            "sex" => $val['sex'],
                            "birth" => $tbirth,
                            "seat_no" => $val['seat_no'],
                            "subtotal" => $subtot_amt,//($val['age'] >= 65) ? $lseat[$i]['senior_citizen_fare']:$lseat[$i]['seat_fare'],
                            "total" =>   $tot_amt,//($val['age'] >= 65) ? $lseat[$i]['senior_citizen_fare']:$lseat[$i]['total_fare'],
                            "is_ladies" => $lseat[$i]['is_ladies'],
                            "mobile" => $customer_mobile,
                            "title" => ($val['sex'] == 'M') ? 'Mr' : 'Mrs',
                            "email" => $customer_email,
                            "id_type" => $val['id_type'],
                            "id_number" => $val['id_number'],
                            "name_on_id" => "",
                            "primary" => "True",
                            "ac" => $lseat[$i]['is_ac'],
                            "sleeper" => $lseat[$i]['is_sleeper']
                        );


                        $ticketDetailsData[] = array(
                            'psgr_no' => $i + 1,
                            'psgr_name' => $val['name'],
                            'psgr_age' => $val['age'],
                            'psgr_sex' => $val['sex'],
                            'psgr_type' => 'A',
                            'concession_cd' => 1, // if exists /in case of rsrtc 60 pass ct 
                            'concession_rate' => 1, // from api /if exists /in case of rsrtc 60 pass ct 
                            'discount' => 1, // if available from api
                            'is_home_state_only' => 1,
                            'adult_basic_fare' => $newonwardJourny['adult_fare'],
                            'child_basic_fare' => $newonwardJourny['child_fare'],// Y or No //not required now
                            'senior_citizen_fare' => $newonwardJourny['senior_fare'],
                            'fare_amt' => $tot_amt,//($val['age'] >= 65) ? $lseat[$i]['senior_citizen_fare']:$lseat[$i]['seat_fare'],//$lseat[$i]['seat_fare'],
                            'convey_name' => "Convenience charge",
                            'convey_type' => $convey_charge_type,
                            'convey_value' => $convey_charge_value,
                            'fare_convey_charge' => $convey_charge_value,
                            'seat_no' => $val['seat_no'],
                            'berth_no' => $tbirth,
                            'seat_status' => 'Y',
                            'concession_proff' => 1, // if cct then required // voter id
                            'proff_detail' => 1, // if varified
                            'status' => 'Y'
                        );

                        $i++;
                    }
                }
            }
            $this->session->set_userdata('passenger_details', $postData);
            $passenger_details = json_encode($postData);
            $traveYariBD = $address = "";
            $bs_stop = $sboard_stop = $pvt_boarding_stop = "";
            if($provider_id == 6)
            {
                $traveYariBD = $this->session->userdata('travelYari_boarding_stops');
                $tboarding_stop = $traveYariBD['boarding_stops'][0]->PickupCode;
                $tdropping_stop = $traveYariBD['Dropoffs'][0]->DropoffCode;
                $address = json_encode($traveYariBD);
            }
           if($provider_id == 3){
               $sboard_stop = explode('#', $newonwardJourny['sboarding_stop']);
               $pvt_boarding_stop = json_encode(array('id'=>$sboard_stop[0],'location'=>$sboard_stop[1],'time'=>$sboard_stop[2]));
           }
           if($provider_id == 6){
               $bs_stop = $tboarding_stop;
           }else if($provider_id == 3){
               $bs_stop = $pvt_boarding_stop;
           }else{
               $bs_stop = $boarding_stop;
           }
            $postInput = array(
                'provider_id' => $provider_id,
                'from' => $boarding_stop,
                'to' => $dropping_stop,
                'date' => date("m/d/Y", strtotime($date)),
                'boarding_stop' => $bs_stop,//$provider_id == 6 ? $tboarding_stop :$boarding_stop,
                'dropping_stop' => $provider_id == 6 ? $tdropping_stop: $dropping_stop,
                'trip_id' => $trip_id,
                'email' => $customer_email,
                'mobile' => $customer_mobile,
                'passenger' => $passenger_details,
                'name' => 'sagar',
                'address' => $address,
                'inventory_type' => $inventory_type,
            );
        }
        /*if(!empty($returnJourny)){
            echo "<pre>"; print_r($returnJourny); die();
        }*/
       
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'temp_booking.log', $postInput, 'temp_booking Post data');

        if (!empty($postInput)) {

            $curl_url = $this->base_url .  "rest_server/search_bus/temp_booking";

            $curlResult = curlForPostData_BOS($curl_url, $postInput, $this->sessionData, $getPost = "POST");

            $output = json_decode($curlResult);
            //echo "<pre>"; print_r($postInput);
            //echo "<pre>"; print_r($this->session->userdata()); die();
            $data['output'] = json_decode($curlResult);
            $status = $output->data->status;

            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'temp_booking.log', $output, 'temp booking op');

            if (strtolower($status) == 'success') {
                $boarding_stop_name = $boarding_stop;
                $fare_reservationCharge = $asn_fare = $ac_service_charges = $op_service_charge = $fare_acc = $fare_it = $fare_toll = $fare_hr = $service_tax = $fare_convey_charge = 0;
                $destination_stop_name = $dropping_stop;
                if(is_object($output->data->data->ticket_detail->msrtc))
                {
                   $fare_reservationCharge = $output->data->data->ticket_detail->msrtc->reservation_charge; 
                   $asn_fare = $output->data->data->ticket_detail->msrtc->asn_amount; 
                   $ac_service_charges = $output->data->data->ticket_detail->msrtc->ac_service_charges; 
                }elseif(is_object($output->data->data->ticket_detail->upsrtc))
                {
                    $fare_reservationCharge = $output->data->data->ticket_detail->upsrtc->reservation_charge; 
                    $fare_acc = $output->data->data->ticket_detail->upsrtc->acc_sur; 
                    $fare_it = $output->data->data->ticket_detail->upsrtc->it_charge;
                    $fare_toll = $output->data->data->ticket_detail->upsrtc->toll_charge;
                    $fare_hr = $output->data->data->ticket_detail->upsrtc->hr_charge;
                    $service_tax = $output->data->data->ticket_detail->upsrtc->gstamt;
                    $fare_convey_charge = $output->data->data->ticket_detail->upsrtc->convey_charges;
                }elseif(is_object($output->data->data->ticket_detail->rsrtc))
                {
                    $fare_reservationCharge = $output->data->data->ticket_detail->rsrtc->service_charges; 
                    
                }elseif(is_object($output->data->data->ticket_detail->hrtc))
                {
                    $fare_reservationCharge = $output->data->data->ticket_detail->hrtc->levy_fare; 
                    $fare_toll = $output->data->data->ticket_detail->toll_fare;
                    $service_tax = $output->data->data->ticket_detail->hrtc->service_charges;
                }
                 $from_stop_name = $this->session->userdata['bus_details']['from'];
                 $to_stop_name = $this->session->userdata['bus_details']['to'];
                 
                //--------------Get dynamic cancellation policy using trip_id from stored json file----------------
                
                $search_date = date("d_m_Y", strtotime($date));
                $fromstopname = $this->session->userdata['bus_services_input']['from'];
                $tostopname = $this->session->userdata['bus_services_input']['to'];
                $file_name = strtolower($fromstopname) . "_" . strtolower($tostopname) . "_" . $search_date . '.json';
                $file_name = str_replace(' ', '', $file_name);
                $file_path = FCPATH . "json_data/";
               
                if (file_exists($file_path . $file_name)) {
                     $cancel_policy = '';
                    $json_data = file_get_contents($file_path . $file_name);
                    $output_result = json_decode($json_data); 
                    $data['result'] = $output_result->result;
                    $data['input_data'] = $output_result->input_data;
//                    $data['getCancelPolicy'] = json_decode($json_data, true);
                    if(!empty($data['result'])){
                        foreach ($data['result'] as $key => $values) {
                            if($values->trip_id == $trip_id){
                                foreach($values->cancellation_policy as $cancel_pol){
                                    //$cancel_policy .= '<li>Cancellation within '.$cancel_pol->cutoffTime.' hrs. Refund in % '.$cancel_pol->refundInPercentage.'%</li>';
                                    if(isset($cancel_pol->refundInPercentage) && !empty($cancel_pol->refundInPercentage)){
                                        $cancel_policy .= '<li>'.$cancel_pol->refundInPercentage.'% ticket fare will get deducted if the ticket is cancelled '. $cancel_pol->cutoffTime.' hrs. before the departure time.</li>';
                                    }else if(isset($cancel_pol->Mins) && !empty($cancel_pol->Mins)){
                                        $cancel_policy .= '<li>'.$cancel_pol->Pct.'% ticket fare will get deducted if the ticket is cancelled '. $cancel_pol->Mins.' minutes. before the departure time.</li>';
                                    }
                                    
                            }
                           
                        }
                     }
                      $cancel_policy .= '<li>The departure time means departure time of the orignating bus station.</li><li>Reservation charges and convenience charges including GST charge are not refundable.</li>';
                      log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dynamic_policy_single.log', $cancel_policy, 'dynamic policy for'.$file_name.''.$from_stop_name.''.$to_stop_name);
                   }
                }
                //----------------------End------------------------------------------------------------------------------
                
                 $ticket_data = array(
                    'ticket_ref_no' => $output->data->data->block_key,
                    'total_basic_amount' => $output->data->data->base_fare,
                    'total_basic_with_ac' => $output->data->data->total_fare,
                    'tot_fare_amt' => $output->data->data->total_fare,
                    'tot_fare_amt_with_tax' => $output->data->data->transaction_amount,
                    'total_fare_without_discount' => $output->data->data->total_fare,
                    'asn_fare' => $asn_fare,
                    'ac_service_tax' => $ac_service_charges,
                    'fare_reservationCharge' =>$fare_reservationCharge,
                    'fare_acc' => $fare_acc,
                    'fare_it' => $fare_it,
                    'fare_toll' => $fare_toll,
                    'fare_hr' => $fare_hr,
                    'service_tax' => $service_tax,
                    'fare_convey_charge' => $fare_convey_charge,
                    'status' => 'Y',
                    'transaction_status' => 'temp_booked',
                    'user_email_id' => $customer_email,
                    'agent_email_id' => $this->agent_email,
                    'agent_mobile_no' => $this->agent_mobile_no,
                    'mobile_no' => $customer_mobile,
                    'from_stop_name' => $newonwardJourny['from'],
                    'till_stop_name' => $newonwardJourny['to'],
                    'boarding_stop_name' => $boarding_stop,
                    'destination_stop_name' => $dropping_stop,
                    'pickup_address' => $address,
                    'tds_per' => $output->data->data->agent_tds_value,
                    'num_passgr' => count($postData),
                    'booked_from' => $operator_name,
                    'booked_by' =>!empty($user_id) ? $user_id :trim($this->session->userdata('user_id')),
                    'op_name' => $operator_name,
                    'inventory_type' => $inventory_type,
                    'bus_service_no' => $trip_id,
                    'bus_type' => $newonwardJourny['data_bus_type'],
                    'provider_type' => $newonwardJourny['provider_type'],
                    'provider_id' => $newonwardJourny['provider_id'],
                    'dept_time' => $sch_departure_time,
                    'alighting_time' => $sch_alighting_time,
                    'boarding_time' => $sch_boarding_time,
                    'droping_time' => $sch_dropping_time,
                    'wallet_trans_id' => $walletTransId,
                    'dynamic_cancellation_policy' => $cancel_policy,
                     'route_no_name' =>  $newonwardJourny['route_no_name']
                );

                $block_key = $output->data->data->block_key;

                $this->session->set_userdata('block_key', $block_key);
                $this->session->set_userdata('ticket_data', $ticket_data);
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'tickets_details_insert.log', $ticket_data, 'Tickets details insert');
                $save_data = $this->Search_bus_model->saveTicketData($ticket_data);
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'tickets_details_insert.log', $save_data, 'Save ticket details response');
                if ($save_data) {
                    for ($m = 0; $m < count($ticketDetailsData); $m++) {
                        $newTicketDetailsData[] = array('ticket_id' => $save_data) + $ticketDetailsData[$m];
                    }
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'prabhat.log', $newTicketDetailsData, 'temp booking op');
                    $save_data = $this->Search_bus_model->batchInsertTicketDetails('ticket_details', $newTicketDetailsData);
                    
                    /*return jorney block seat*/
                    $returnDataResponse = array();
                    if(!empty($returnJourny))
                    {
                        $returnDataResponse = $this->returnTempbooking($input, $returnJourny, $walletTransId);
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'return_tempbooking.log', $returnDataResponse, 'temp booking op');
                    }
                    $this->book_payment($output->data->data, count($postData), $returnDataResponse);
                    // redirect(base_url() . "admin/search_bus/book_payment/" . $block_key);
                }
            } else {
                $msg = !empty($output->data->msg) ? $output->data->msg :"Seat already book try other seat.";
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'temp_booking_error.log', $msg, 'temp booking op');
                $this->session->set_flashdata('item', array('message' => $msg,'class' => 'alert-danger'));
                redirect('admin/search_bus/'); 
            }
        };
        }

    function book_payment($tempData, $totalSeat, $returnDataResponse) {
        $transactionAmount = $payableAmount = $totalCommission = 0;
        if ($tempData) {
            $data['tempData'] = $tempData;
            $data['totalSeat'] = $totalSeat;
            $transactionAmount = $tempData->transaction_amount;
            $payableAmount = $tempData->total_fare;
            if(!empty($returnDataResponse))
            {
                $data['rtempData'] = $returnDataResponse['rdata'];
                $data['rtotalSeat'] = $returnDataResponse['rtotalSeat'];
                $transactionAmount = $tempData->transaction_amount + $data['rtempData']->transaction_amount;
                $payableAmount = $tempData->total_fare + $data['rtempData']->total_fare;
            }
            $totalCommission = $payableAmount - $transactionAmount;
            $data['transactionAmount'] = $transactionAmount;
            $data['payableAmount'] = $payableAmount;
            $data['totalCommission'] = $totalCommission;
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'view_tempbooking.log', $data, 'temp booking op');
            load_back_view(BOOK_PAYMENT, $data);
        }
    }

    public function confirm_booking() {

        $output = $updateData = array();
        $blockKey = '';
        $unique_id = substr(hexdec(uniqid()), 4, 9);   //mt_rand(10000000, 99999999);
        $blockKey = $this->input->post('block_key');
        $rblockKey = $this->input->post('rblock_key');
        $user_id = $this->sessionData['id'];
        $conf_input = array(
            'block_key' => $this->input->post('block_key'),
            'unique_id' => $unique_id,
            'seat_count' => $this->input->post('count_seat'),
        );
        /* Here payment method we check wallet details */
        $paymentTransaction = $this->process_payment_confirm_booking($user_id, $unique_id);
       
        if (!$paymentTransaction) {
            $msg = "Insufficient wallet banlance.";
            $this->session->set_flashdata('item', array('message' => $msg,'class' => 'alert-danger'));
            $updateData['transaction_status'] == 'failed';
            $updateData['redirect'] = base_url()."admin/search_bus/";
            $updateData['message'] = $msg;
            echo json_encode($updateData);
        } else {

            /* end wallet details */
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'confirm_booking_ip.log', $conf_input, 'confirm booking ip');
            if (!empty($conf_input)) {

                $curl_url = $this->base_url .  "rest_server/search_bus/confirm_booking";

                $curlResult = curlForPostData_BOS($curl_url, $conf_input, $this->sessionData, $getPost = "POST");

                $output = json_decode($curlResult, true);
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'confirm_booking.log', $curlResult, 'confirm_booking');

                if (isset($output['data']['status']) && strtolower($output['data']['status']) == 'success') {
                    $updateData = array(
                        'transaction_status' => $output['data']['status'],
                        'pnr_no' => !empty($output['data']['data']['ticket_no'])?$output['data']['data']['ticket_no']:$output['data']['data']['pnr'],
                        'boss_ref_no' => $output['data']['data']['bos_key'],
                        'rokad_ref_no' => $unique_id,
                        'issue_time' => date('Y-m-d H:i:s'),
                        'payment_confirmation' => 'full',
                        'payment_mode' => 'Wallet',
                        'booked_by' => $user_id,
                    );
                } else {
                    $updateData = array(
                        'transaction_status' => $output['data']['status'],
                    );
                }
                $this->session->set_userdata('final_data', $updateData);
                if ($updateData) {
                    $ticketsUpdate = $this->Search_bus_model->newUpdateTicketData('tickets', $updateData, $blockKey);
                    if(!empty($rblockKey)){
                        $routput = array();
                        $unique_id = substr(hexdec(uniqid()), 4, 9);
                        $rconf_input = array(
                            'block_key' => $rblockKey,
                            'unique_id' => $unique_id,
                            'seat_count' => $this->input->post('rcount_seat'),
                        );
                        $curl_url = $this->base_url .  "rest_server/search_bus/confirm_booking";
                        $rcurlResult = curlForPostData_BOS($curl_url, $rconf_input, $this->sessionData, $getPost = "POST");
                        $routput = json_decode($rcurlResult, true);
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'confirm_booking.log', $rcurlResult, 'Return confirm_booking');
                        if (isset($routput['data']['status']) && strtolower($routput['data']['status']) == 'success') {
                            $rupdateData = array(
                                'transaction_status' => $routput['data']['status'],
                                'pnr_no' => !empty($routput['data']['data']['ticket_no'])?$routput['data']['data']['ticket_no']:$routput['data']['data']['pnr'],
                                'boss_ref_no' => $routput['data']['data']['bos_key'],
                                'rokad_ref_no' => $unique_id,
                                'issue_time' => date('Y-m-d H:i:s'),
                                'payment_confirmation' => 'full',
                                'payment_mode' => 'Wallet',
                                'booked_by' => $user_id,
                            );
                        } else {
                            $rupdateData = array(
                                'transaction_status' => $routput['data']['status'],
                            );
                        }
                        $this->session->set_userdata('rfinal_data', $rupdateData);
                        $ticketsUpdate = $this->Search_bus_model->newUpdateTicketData('tickets', $rupdateData, $rblockKey);
                        $returnTicketId = $this->Search_bus_model->getReturnTicketId($rblockKey);
                        $rticketsid = array(
                                'return_ticket_id' => $returnTicketId[0]['ticket_id'],
                            );
                        $ticketsUpdate = $this->Search_bus_model->newUpdateTicketData('tickets', $rticketsid, $blockKey);
                    }
                }
                $updateData['redirect'] = base_url() . "admin/search_bus/view_tickets/" . $output['data']['data']['bos_key'].'/'.$routput['data']['data']['bos_key'];
                //$data = $output->data;
                echo json_encode($updateData);
                //return $data;
            }
        }
    }

    function cancel_ticket() {
        $this->checkAccessToModule();
        load_back_view(CANCEL_MODULE);
    }

    function check_ticket_details() {
        $this->checkAccessToModule();
        //echo "<pre>"; print_r($_POST); die();
        $agentMobileNum = $this->sessionData['mobile_no'];
        $result = $data = array();
        $email = $this->input->post('email');
        $rokad_key = $this->input->post('rokadReference');
        $pnr_no = $this->input->post('pnrNumber');
        $this->load->library('form_validation');
        $crDate = new DateTime("+10 Minutes", new DateTimeZone('Asia/Kolkata'));
        $currentDate =  $crDate->format('Y-m-d H:i:s');
        $this->form_validation->set_rules('email', 'Email address', 'required|valid_email');
        if (($rokad_key == '' && $pnr_no == '') || ($rokad_key == '' && $pnr_no == '')) {
            $this->form_validation->set_rules('rokadReference', 'Rokad reference', 'required|min_length[6]|max_length[12]');
            $this->form_validation->set_rules('pnrNumber', 'PNR number', 'required|min_length[6]|max_length[12]');
        } elseif ($rokad_key != '' && $pnr_no == '') {

            $this->form_validation->set_rules('rokadReference', 'Rokad reference', 'required|min_length[6]|max_length[12]');
        } elseif ($rokad_key == '' && $pnr_no != '') {
            $this->form_validation->set_rules('pnrNumber', 'PNR number', 'required|min_length[6]|max_length[12]');
        }

        if ($this->form_validation->run() == FALSE) {
            load_back_view(CANCEL_MODULE);
        } else {

            $data['email'] = $email;
            $data['rokad_key'] = !empty($rokad_key) ? $rokad_key : '';
            $data['pnr_no'] = !empty($pnr_no) ? $pnr_no : '';
            if ($rokad_key) {
                $query = $this->db->query("select t.*,td.* from tickets t  join ticket_details td on td.ticket_id=t.ticket_id where t.boss_ref_no = '$rokad_key' AND t.dept_time > '$currentDate' AND t.transaction_status = 'success' AND t.agent_mobile_no = '$agentMobileNum' limit 1 ");
            } elseif ($pnr_no) {
                $query = $this->db->query("select t.*,td.* from tickets t  join ticket_details td on td.ticket_id=t.ticket_id where t.pnr_no = '$pnr_no' AND t.dept_time > '$currentDate' AND t.transaction_status = 'success'  AND t.agent_mobile_no = '$agentMobileNum' limit 1 ");
            } else {
                $data['error'] = 'error';
            }
            $result = $query->result_array();
            if ($result) {

                $data['ticketDetail'] = isset($result[0]) ? $result[0] : array();
            }
            load_back_view(CANCEL_MODULE_LIST, $data);
        }
    }

    public function cancel_ticket_details() {
        $seats = $save_cancel_ticket = $check_user_wallet = $wallet_details = $get_ticket_id = $seatNo = array();
        $input = array(
            'bos_key' => $this->input->post('bos_key'),
        );
        $cancelWalletTransId = substr(hexdec(uniqid()), 4, 12);
        if (!empty($input)) {
            $curl_url = $this->base_url .  "rest_server/search_bus/cancel_ticket";
            $curlResult = curlForPostData_BOS($curl_url, $input, $this->sessionData, $getPost = "POST");
            $output = json_decode($curlResult);
            $data['result'] = $output->data;

            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'cancel_ticket.log', $input, 'cancel ticket ip');
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'cancel_ticket.log', $curlResult, 'cancel_ticket');

            /*             * **** data save in cancel_ticket_data table  **** */
            
            if (isset($output->data->msg) && strtolower($output->data->msg) == 'ticket is not cancellable') {
                $data = $output->data->msg;
                echo json_encode($data['result']);
            } elseif(isset($output->data->data) && $output->data->data->new_pnr == ''){
                $data['status'] = "failed";
                $data['item'] = "Might be your ticket already cancel! Please contact customer care.";
                $data['result'] = array();
                echo json_encode($data);
            }else {
                $bos_key = isset($output->data->data->bos_key) && !empty($output->data->data->bos_key) ? $output->data->data->bos_key : $this->input->post('bos_key');
                $refundAmount = isset($output->data->data->refund_amt) ? $output->data->data->refund_amt : 0;

                $query = $this->db->query("select t.ticket_id,t.op_name, t.rokad_ref_no, t.wallet_trans_id, td.seat_no from tickets t join ticket_details td on td.ticket_id=t.ticket_id where t.boss_ref_no = '$bos_key' ");
                $get_ticket_id = $query->result_array();
                $ticketUpdate = array(
                    'transaction_status'=>'cancel',
                    'cancel_wallet_trans_id' => $cancelWalletTransId
                );
                $this->Search_bus_model->cancelUpdateTicketData($get_ticket_id[0]['ticket_id'], $ticketUpdate);
                /** 
                 * update the vas services details in table
                 */
                $vasStatus = array('status' => 'N');
                $this->Search_bus_model->cancelUpdateVasData($get_ticket_id[0]['wallet_trans_id'], $vasStatus);
                
                
                /* unique id is rokad reference number */
                $unique_id = !empty($get_ticket_id[0]['rokad_ref_no']) ? $get_ticket_id[0]['rokad_ref_no'] : substr(hexdec(uniqid()), 4, 12);
                if (count($get_ticket_id) > 0)
                    foreach ($get_ticket_id as $key => $value) {

                        $seatNo[] = $value['seat_no'];
                    }
                $save_cancel_ticket = array(
                    'ticket_id' => isset($get_ticket_id[0]['ticket_id']) ? $get_ticket_id[0]['ticket_id'] : '',
                    'pnr_no' => isset($output->data->data->pnr) ? $output->data->data->pnr : '',
                    'new_pnr' => isset($output->data->data->new_pnr) ? $output->data->data->new_pnr : '',
                    'seat_no' => !empty($seatNo) && count($seatNo) > 1 ? implode(',', $seatNo) : $seatNo[0],
                    'cancel_charge' => isset($output->data->data->cancel_charge) ? $output->data->data->cancel_charge : 0,
                    'cancel_charge_operator' => isset($output->data->data->cancel_charge) ? $output->data->data->cancel_charge : 0,
                    'refund_amt' => $refundAmount,
                    'actual_refund_paid' => $refundAmount,
                    'raw' => $curlResult,
                    'is_status' => 1,
                    'refund_amount_date' => date('Y-m-d'),
                    'is_refund' => '1',
                    'commission' => isset($output->data->data->commission) ? $output->data->data->commission : ''
                );

                $user_id = $this->sessionData['id'];
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'Cancel_ticket.log', $save_cancel_ticket, 'Cancel ticket details.log');
                if (!empty($save_cancel_ticket)) {
                    $cancelTicket = $this->Search_bus_model->saveCancelTicketData($save_cancel_ticket);
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'Cancel_ticket.log', $save_cancel_ticket, '1 Cancel ticket details.log');
                }
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'Cancel_ticket.log', $save_cancel_ticket, '2 Cancel ticket details.log');
                $check_user_wallet = $this->Utilities_model->getUserWalletDetails($user_id);
                if (!empty($check_user_wallet)) {
                    $updated_wallet_amt = $check_user_wallet[0]['amt'] + $refundAmount;
                    $update_wallet = $this->Utilities_model->updateUserWallet($user_id, $updated_wallet_amt);
                }

                $query = $this->db->query("select id from wallet where user_id = $user_id  ");
                $get_wallet_id = $query->row();

                $wallet_details = array(
                    'transaction_no' => $cancelWalletTransId,
                    'w_id' => $get_wallet_id->id,
                    'amt' => $refundAmount,
                    'wallet_type' => 'actual_wallet',
                    'comment' => 'Book On Spot',
                    'status' => 'Credited',
                    'user_id' => $user_id,
                    'amt_before_trans' => $check_user_wallet[0]['amt'],
                    'amt_after_trans' => $updated_wallet_amt,
                    'transaction_type' => 'Credited'
                );
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'Cancel_Wallet_Trans.log', $wallet_details, 'Cancel Wallet Trans.log');
                $insert_wallet = $this->Search_bus_model->saveWalletTransactionData($wallet_details);
                
                   //======After ticket cancellation debit the commission =======
                $qry = $this->db->query("select v.id from vas_commission_detail v where v.transaction_no = ".$get_ticket_id[0]['wallet_trans_id']);
                $get_vas_id = $qry->result_array();
                $comm_data = $this->commission_distribution_api->debit_vas_commission($get_vas_id[0]['id']);                   
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'Cancel_ticket.log', $comm_data.' Commission debited for'.$get_vas_id[0]['id'], 'Cancel ticket debit commission');
                //======After ticket cancellation debit the commission =======
                
                //$send_cancel_mail = $this->cancel_email($output->data->data->pnr,'');
                
                echo json_encode($data['result']);
            }
            
            /**             * ***data save in cancel_ticket_data table**** */
        }
    }

    public function is_ticket_cancel() {
        $input = array(
            'bos_key' => $this->input->post('bos_key'),
        );

        if (!empty($input)) {

            $curl_url = $this->base_url .  "rest_server/search_bus/is_ticket_cancel";

            $curlResult = curlForPostData_BOS($curl_url, $input, $this->sessionData, $getPost = "POST");

            $output = json_decode($curlResult);
            $data = $output->data;
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'is_ticket_cancel.log', $curlResult, 'is ticket cancel');
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'is_ticket_cancel.log', $data, 'is ticket cancel op');

            if (strtolower($data->status) == 'failed') {
                $response['status'] = $data->status;
                $response['item'] = $data->api_msg->serviceError->errorReason;
            } else {
                //if MSRTC ticket is cancellable then show seats to be cancelled
                if(strtolower($data->data->cancellable) == 'true'){
                     $bos_key = $this->input->post('bos_key');
                     $query = $this->db->query("select * from tickets t join ticket_details td on td.ticket_id=t.ticket_id where t.boss_ref_no = '$bos_key' ");
                     $get_ticket_id = $query->result_array();
                     log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'partial_cancel.log', $get_ticket_id, '$get_ticket_id');
                     $details = array();
                     foreach ($get_ticket_id as $key => $tickets) {
                         log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'partial_cancel.log', $tickets, '$tickets');
                         if($tickets['provider_id'] == 2){
                             //$details['ticket_id'] = array('')
                        }
                     }
                     
                }
                $response = $data;
            }

            echo json_encode($response);
        }
    }

    public function print_ticket2() {
        load_back_view(PRINT_TICKET2);
    }

    public function print_ticket() {
        load_back_view(PRINT_TICKET);
    }

    public function do_print_ticket() {
        $refrence_no = $this->input->get('refrence_no');

        $input = array(
            'refrence_no' => ($refrence_no) ? ($refrence_no) : $this->input->post('refrence_no1'),
            'email' => $this->input->post('email1')
        );

        // echo "<pre>"; print_r($input); die();
        if (!empty($input)) {

            $curl_url = $this->base_url .  "rest_server/search_bus/print_ticket";

            $curlResult = curlForPostData_BOS($curl_url, $input, $this->sessionData, $getPost = "POST");
            $output = json_decode($curlResult);
            $data['result'] = $output->data;

            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'print_ticket.log', $curlResult, 'print ticket');
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'print_ticket_ip.log', $input, 'print ticket ip');
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'print_ticket_op.log', $data, 'print ticket op');
            if ($this->input->post('refrence_no1')) {
                echo json_encode($data);
            } else {
                load_back_view(PRINT_TICKET, $data);
            }
        }
    }

    public function calc_commission($input = "", $transaction_no = "") {
        $input['transaction_no'] = $transaction_no;
        if (!empty($input)) {

            $curl_url = $this->base_url .  "rest_server/search_bus/calc_commission";

            $curlResult = curlForPostData_BOS($curl_url, $input, $this->sessionData, $getPost = "POST");

            return $curlResult;

            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'calc_commission.log', $curlResult, 'calc commssion');
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'calc_commission_ip.log', $input, 'calc commssion ip');
        }
    }

    function view_tickets($bos_key = '', $rbos_key = '') {
        $baseUrl = base_url('admin/search_bus/'); 
        $savePDF = base_url('admin/search_bus/savePDF/'.$bos_key.'/'.$rbos_key); 
        $ticketId = 0; $onwadTicket = $returnTicket ='';
        $data['bosKey'] = $bos_key;
        $data['rbosKey'] = $rbos_key;
        $ticketDetails = $passengerDetails = array();
        
        $ticketDetails = $this->Search_bus_model->getTicketsDetails($bos_key);
        if (empty($ticketDetails) && empty($ticketDetails[0]['pnr_no'])) {
            die('Your tickets can not view please contact with our support team!');
        }
        $ticketId = $ticketDetails[0]['ticket_id'];
        $passengerDetails = $this->Search_bus_model->getPassengerDetails($ticketId);
        
        $onwadTicket = $this->sendSmsEmail($ticketDetails, $passengerDetails);
        if(!empty($data['rbosKey']) && !empty($rbos_key)) {
            $ticketDetails = $this->Search_bus_model->getTicketsDetails($rbos_key);
            $ticketId = $ticketDetails[0]['ticket_id'];
            $passengerDetails = $this->Search_bus_model->getPassengerDetails($ticketId);
            $returnTicket = $this->sendSmsEmail($ticketDetails, $passengerDetails);
            if(!empty($returnTicket))
            {
                $returnTicket = "<br/><br/><strong> Return Journey Ticket </strong> <br/><br/><br/>".$returnTicket;
            }
        }
        $backNprint = '<table style="font-family:Arial, Helvetica, sans-serif; font-size:12px; border-collapse:collapse; margin:0 auto;" cellpadding="0" border="0" width="15%" height="8%"><tbody><tr>
                        <td style="vertical-align:middle; font-size:15px; font-weight:bold; line-height:24px;" valign="top" align="center">
                                <a href ='.$baseUrl.'>Back to Home</a>
                        </td>
                        <td style="vertical-align:middle; font-size:15px; font-weight:bold; line-height:24px;" valign="top" align="center"> 
                                <a href='.$savePDF.'> Save Ticket </a>
                        </td></tr></tbody></table>';
        
        echo $onwadTicket. $returnTicket."<br/>".$backNprint."<br/>"."<br/>"."<br/>";
        $this->session->unset_userdata('onwardJourny');
        $this->session->unset_userdata('returnJourny');
        $this->session->unset_userdata('passenger_details');
        $this->session->unset_userdata('rpassenger_details');
        $this->session->unset_userdata('bus_services_input');
        $this->session->unset_userdata('operator_name');
        $this->session->unset_userdata('bus_details');
        $this->session->unset_userdata('block_key');
        $this->session->unset_userdata('rblock_key');
        $this->session->unset_userdata('from');
        $this->session->unset_userdata('to');
        $this->session->unset_userdata('date');
        $this->session->unset_userdata('rfinal_data');
        $this->session->unset_userdata('final_data');
        $this->session->unset_userdata('rticket_data');
    }

    function process_payment_confirm_booking($userId, $walletTransId) {
        $check_user_wallet = $ticketDetails = $rticketDetails = array();
        $walletCurrentBal = $updated_wallet_amt = $totalTicketFare = $totalTicketFareWithoutDisc = $totalCommission = 0;

        $blockKey = $this->session->userdata('block_key');
        $rblockKey = $this->session->userdata('rblock_key');
        if ($blockKey) {
            $ticketDetails = $this->Search_bus_model->getTicketDetails($blockKey);
            if (!empty($ticketDetails)) {
                $check_user_wallet = $this->Utilities_model->getUserWalletDetails($this->sessionData['id']);
                if (!empty($check_user_wallet)) {
                    $walletCurrentBal = isset($check_user_wallet[0]['amt']) ? $check_user_wallet[0]['amt'] : 0;
                    $totalTicketFare = isset($ticketDetails[0]['tot_fare_amt_with_tax']) ? $ticketDetails[0]['tot_fare_amt_with_tax'] : 0;
                    $totalTicketFareWithoutDisc = isset($ticketDetails[0]['total_fare_without_discount']) ? $ticketDetails[0]['total_fare_without_discount'] : 0;
                    
                    //here work on vas_commission_details
                     $commissionResult = $this->Utilities_model->commission_from($userId);
                    if($rblockKey && !empty($rblockKey)) {
                        /* return journey */
                        $rticketDetails = $this->Search_bus_model->getTicketDetails($rblockKey);
                        //$totalTicketFare = $totalTicketFare + $rticketDetails[0]['tot_fare_amt_with_tax'];
                       // $totalTicketFareWithoutDisc = $totalTicketFareWithoutDisc + $rticketDetails[0]['total_fare_without_discount'];
                        
                        $rtotalTicketFare = $rticketDetails[0]['tot_fare_amt_with_tax'];
                        $rtotalTicketFareWithoutDisc = $rticketDetails[0]['total_fare_without_discount'];
                        $rtotalCommission = $rtotalTicketFareWithoutDisc - $rtotalTicketFare;
                        if ($walletCurrentBal >= $rtotalTicketFareWithoutDisc) {
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'vas_commission_details.log', $commissionResult, 'return  Wallet Trans.log');
                           if($commissionResult){
                               $created_by = $commissionResult[0]['level_3'];
                               $rwallet_tran_id = $rticketDetails[0]['wallet_trans_id'];
                               $input = array('user_id'=>$userId,'type'=>'BOS','recharge_type'=>'BOS_TICKET','recharge_amount'=>$rtotalTicketFareWithoutDisc, 'totalCommission'=>$rtotalCommission );
                               $rcommissionResult = $this->serviceRetailerCommissionDistributionForRecharges($input, $rwallet_tran_id,$created_by);
                               log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'vas_commission_details.log', $rcommissionResult, 'return  Wallet Trans  joureny.log');
                           }else
                           {
                               $msg = "You don't have any level, please contact to customer.";
                               log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'vas_commission_details.log', array('message' => $msg,'class' => 'alert-danger'), 'return  Wallet Trans.log');
                               $this->session->set_flashdata('item', array('message' => $msg,'class' => 'alert-danger'));
                               return false;
                           }
                            $query = $this->db->query("select id from wallet where user_id = $userId");
                            $get_wallet_id = $query->row();
                            $rupdated_wallet_amt = $walletCurrentBal - $rtotalTicketFareWithoutDisc;
                            $rwallet_details = array(
                            'transaction_no' => $rticketDetails[0]['wallet_trans_id'],
                            'w_id' => $get_wallet_id->id,
                            'amt' => $rtotalTicketFareWithoutDisc,
                            'wallet_type' => 'actual_wallet',
                            'comment' => 'Book On Spot',
                            'status' => 'Debited',
                            'user_id' => $userId,
                            'amt_before_trans' => $walletCurrentBal,
                            'amt_after_trans' => $rupdated_wallet_amt,
                            'transaction_type' => 'Debited'
                        );
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'Wallet_Trans.log', $rwallet_details, 'return  Wallet Trans.log');
                        $insert_wallet = $this->Search_bus_model->saveWalletTransactionData($rwallet_details);
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'Wallet_Trans.log', $insert_wallet, 'return  Wallet Trans id.log');
                        if ($insert_wallet) {
                            $update_wallet = $this->Utilities_model->updateUserWallet($userId, $rupdated_wallet_amt);
                            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'Wallet_Trans.log', $update_wallet, 'return Update Wallet Trans id.log');
                            
                        } else {
                            return false;
                        }
                        }else {
                            $msg = "Insufficient wallet banlance.";
                            $this->session->set_flashdata('item', array('message' => $msg,'class' => 'alert-danger'));
                        }
                    }
                    
                    $totalCommission = $totalTicketFareWithoutDisc - $totalTicketFare;
                    $check_user_wallet = $this->Utilities_model->getUserWalletDetails($this->sessionData['id']); 
                    $walletCurrentBal = isset($check_user_wallet[0]['amt']) ? $check_user_wallet[0]['amt'] : 0;
                    if ($walletCurrentBal >= $totalTicketFareWithoutDisc) {
                        
                        //here work on vas_commission_details
                       // $commissionResult = $this->Utilities_model->commission_from($userId);
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'vas_commission_details.log', $commissionResult, 'Wallet Trans.log');
                           if($commissionResult){
                               $created_by = $commissionResult[0]['level_3'];
                               $wallet_tran_id = $ticketDetails[0]['wallet_trans_id'];
                               $input = array('user_id'=>$userId,'type'=>'BOS','recharge_type'=>'BOS_TICKET','recharge_amount'=>$totalTicketFareWithoutDisc, 'totalCommission'=>$totalCommission );
                               $commissionResult = $this->serviceRetailerCommissionDistributionForRecharges($input, $wallet_tran_id,$created_by);
                               log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'vas_commission_details.log', $commissionResult, 'Wallet Trans.log');
                           } else
                           {
                               $msg = "You don't have any level, please contact to customer.";
                               log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'vas_commission_details.log', array('message' => $msg,'class' => 'alert-danger'), 'Wallet Trans.log');
                               $this->session->set_flashdata('item', array('message' => $msg,'class' => 'alert-danger'));
                               return false;
                           }
                       //end vas_commission_details
                        
                        $query = $this->db->query("select id from wallet where user_id = $userId");
                        $get_wallet_id = $query->row();
                        $updated_wallet_amt = $walletCurrentBal - $totalTicketFareWithoutDisc;
                        $wallet_details = array(
                            'transaction_no' => $ticketDetails[0]['wallet_trans_id'],
                            'w_id' => $get_wallet_id->id,
                            'amt' => $totalTicketFareWithoutDisc,
                            'wallet_type' => 'actual_wallet',
                            'comment' => 'Book On Spot',
                            'status' => 'Debited',
                            'user_id' => $userId,
                            'amt_before_trans' => $walletCurrentBal,
                            'amt_after_trans' => $updated_wallet_amt,
                            'transaction_type' => 'Debited'
                        );
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'Wallet_Trans.log', $wallet_details, 'Wallet Trans.log');
                        $insert_wallet = $this->Search_bus_model->saveWalletTransactionData($wallet_details);
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'Wallet_Trans.log', $insert_wallet, 'Wallet Trans id.log');
                        if ($insert_wallet) {
                            $update_wallet = $this->Utilities_model->updateUserWallet($userId, $updated_wallet_amt);
                            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'Wallet_Trans.log', $update_wallet, 'Update Wallet Trans id.log');
                            return $insert_wallet;
                        } else {
                            return false;
                        }
                    } else {
                        $msg = "Insufficient wallet banlance.";
                        $this->session->set_flashdata('item', array('message' => $msg,'class' => 'alert-danger'));
                    }
                } else {
                    $msg = "User wallet details not found.";
                    $this->session->set_flashdata('item', array('message' => $msg,'class' => 'alert-danger'));
                }
            } else {
                $msg = "Temp booking is not available.";
                $this->session->set_flashdata('item', array('message' => $msg,'class' => 'alert-danger'));
            }
        } else {
            $msg = "Block key is not available.";
            $this->session->set_flashdata('item', array('message' => $msg,'class' => 'alert-danger'));
        }
    }

    function test() {
        //show('sklfdkslk',1);
        //here work on vas_commission_details
        $walletTransId = substr(hexdec(uniqid()), 5, 10);
        $totalTicketFareWithoutDisc = '1680';
        $totalCommission = '44.15';
        $userId = '338';//$this->sessionData['id'];
//        $commissionResult = $this->Utilities_model->commission_from($userId);
//        if($commissionResult){
            $created_by = '333';//$commissionResult[0]['level_3'];
            $wallet_tran_id = $walletTransId;
            $input = array('user_id'=>$userId,'type'=>'BOS','recharge_type'=>'BOS_TICKET','recharge_amount'=>$totalTicketFareWithoutDisc, 'totalCommission'=>$totalCommission );
            $commissionResult = $this->serviceRetailerCommissionDistributionForRecharges($input, $wallet_tran_id,$created_by);
            show($commissionResult,1); die();
//        } else
//        {
//            $msg = "You don't have any level, please contact to customer.";
//            $this->session->set_flashdata('item', array('message' => $msg,'class' => 'alert-danger'));
//            return false;
//        }
        //end vas_commission_details
        
        /*$this->db->select('u.id as user_id,u.agent_code,u.level_1,u.level_2,u.level_3,u.level_4,u.level_5,rsm.service_id as service_id,sc.id as commission_id,sc.values');
        $this->db->from("users u");
        $this->db->join('retailer_service_mapping rsm', 'u.id= rsm.agent_id', 'left');
        $this->db->join('services_commissions sc', 'rsm.service_id = sc.service_id', 'left');
        $this->db->where('rsm.agent_id =', $user_id);
        $this->db->where('rsm.service_id =', $service_id);
        $this->db->where('sc.status', 'Y');
        $this->db->where('sc.from >=', date('Y-m-d H:i:s'));
        $this->db->where('sc.till <=', date('Y-m-d H:i:s'));
        $user_result = $this->db->get();
        echo $this->db->last_query();
        $user_result_array = $user_result->result_array();

        //load_back_view(TEST);*/
    }

    function get_test($trans_ref_no = "590062499346",$created_by='333') {
        $input = array('user_id'=>'784','type'=>'BOS','recharge_type'=>'BOS_TICKET','recharge_amount'=>'1680','totalCommission'=>'');
        
	$user_id = $input['user_id'];
        if(in_array($input['type'], ['jri', 'JRI', 'tso', 'TSO', 'BOS', 'bos']) ){
            if($input['recharge_type'] == 'BOS_TICKET'){
               $code = strtolower($input['type']);
                $service_id = BOS_SERVICE_ID;
            }
           
            $LcSqlStr = "SELECT * from services_commissions 
            where commission_name like '%" . str_replace(" ", "", $code) . "%' 
            and service_id = '" . $service_id . "'" ;
           
            $query = $this->db->query($LcSqlStr);
            $new = $query->result();
                        
            if($new){ 
                $row = $new[0];
                $getRetailerServicesCommission = $this->Utilities_model->getRetailerServicesCommissionForRecharges($user_id, $row->service_id, $row->sub_service_id, $created_by);
            }
        }else{
            $service_id = UTILITIES_SERVICE_ID;
            $getRetailerServicesCommission = $this->Utilities_model->getRetailerServicesCommission($user_id, $service_id, $created_by);
        }

        if ($getRetailerServicesCommission) {

            $agent_id = $getRetailerServicesCommission[0]['user_id'];
            $agent_code = $getRetailerServicesCommission[0]['agent_code'];
            $distributor_id = $getRetailerServicesCommission[0]['level_5'];
            $Area_dis_id = $getRetailerServicesCommission[0]['level_4'];
            $master_dis_id = $getRetailerServicesCommission[0]['level_3'];
            $company_id = $getRetailerServicesCommission[0]['level_2'];
            $trimax_id = $getRetailerServicesCommission[0]['level_1'];
            $service_id = $getRetailerServicesCommission[0]['service_id'];
            $commission_id = $getRetailerServicesCommission[0]['commission_id'];
            $total_commission = $getRetailerServicesCommission[0]['total_commission'];

            $values[0] = json_decode($getRetailerServicesCommission[0]['values']);

            $trimax_commission = '0';
            $company_commission = '0';
            $md_commission = '0';
            $ad_commission = '0';
            $dist_commission = '0';
            $retailer_commission = '0';
            $with_gst = $without_gst = 0;
            if (!empty($values)) {

                $trimax_commission = round($values[0]->Trimax, 2);
                $company_commission = round($values[0]->Rokad, 2);
                $md_commission = round($values[0]->MD, 2);
                $ad_commission = round($values[0]->AD, 2);
                $dist_commission = round($values[0]->Distributor, 2);
                $retailer_commission = round($values[0]->Retailer, 2);
                if($input['recharge_type'] != 'BOS_TICKET'){
                    $recharge_amount = $input['recharge_amount']; 
                    $commission_percent = !empty($total_commission) ? $total_commission : '0';                        
                    $trimax_cal = calculateGstTds($recharge_amount, $commission_percent);
                    $earnings = $trimax_cal['earnings']; //trimax_earnings
                    $gst = $trimax_cal['gst']; //trimax_gst                            
                    $tds = $trimax_cal['tds']; //trimax_tds
                    $final_amount = $trimax_cal['final_amount']; //trimax_final_amt                                                        
                } else
                {
                    $recharge_amount = $input['recharge_amount'];
                    $totalCommission = $input['totalCommission'];
                    $trimax_cal = bosCalculateGstTds($recharge_amount, $totalCommission);
                    $earnings = $trimax_cal['earnings']; //trimax_earnings
                    $gst = $trimax_cal['gst']; //trimax_gst                            
                    $tds = $trimax_cal['tds']; //trimax_tds
                    $final_amount = $trimax_cal['final_amount']; //trimax_final_amt  
                }
                $rokad_trimax_cal = calculateGstTds($earnings, $trimax_commission);
                $rokad_trimax_amt = $rokad_trimax_cal['earnings']; //rokad_trimax_amt
                $with_gst = $with_gst + $rokad_trimax_cal['earnings'];
                $rokad_trimax_cal_with_tds_gst = calculateGstTds($final_amount, $trimax_commission);
                $rokad_trimax_amt_with_tds_gst = $rokad_trimax_cal_with_tds_gst['earnings']; //rokad_trimax_amt_with_tds_gst
                $without_gst = $without_gst + $rokad_trimax_amt_with_tds_gst;        

                $company_cal = calculateGstTds($earnings, $company_commission);
                $company_amt = $company_cal['earnings'];//company gst
                $with_gst = $with_gst + $company_cal['earnings'];
                $company_cal_with_tds_gst = calculateGstTds($final_amount, $company_commission);
                $company_amt_with_tds_gst = $company_cal_with_tds_gst['earnings']; //company_amt_with_tds_gst                    
                $without_gst = $without_gst + $company_amt_with_tds_gst;    

                $rd_cal = calculateGstTds($earnings, $md_commission);
                $rd_amt = $rd_cal['earnings']; //rd_amt            
                $with_gst = $with_gst + $rd_cal['earnings'];                 
                $rd_cal_with_tds_gst = calculateGstTds($final_amount, $md_commission);
                $rd_amt_with_tds_gst = $rd_cal_with_tds_gst['earnings']; //rd_amt_with_tds_gst
                $without_gst = $without_gst + $rd_amt_with_tds_gst;

                $dd_cal = calculateGstTds($earnings, $ad_commission);
                $dd_amt = $dd_cal['earnings']; //dd_amt   
                $with_gst = $with_gst + $dd_cal['earnings'];                                 
                $dd_cal_with_tds_gst = calculateGstTds($final_amount, $ad_commission);
                $dd_amt_with_tds_gst = $dd_cal_with_tds_gst['earnings']; //dd_amt_with_tds_gst
                $without_gst = $without_gst + $dd_amt_with_tds_gst;

                $ex_cal = calculateGstTds($earnings, $dist_commission);
                $ex_amt = $ex_cal['earnings']; //ex_amt      
                $with_gst = $with_gst + $ex_cal['earnings'];              
                $ex_cal_with_tds_gst = calculateGstTds($final_amount, $dist_commission);
                $ex_amt_with_tds_gst = $ex_cal_with_tds_gst['earnings']; //ex_amt_with_tds_gst
                $without_gst = $without_gst + $ex_amt_with_tds_gst;
                $sa_cal = calculateGstTds($earnings, $retailer_commission);
                $sa_amt = $sa_cal['earnings']; //sa_amt     
                $with_gst = $with_gst + $sa_cal['earnings'];              
                $sa_cal_with_tds_gst = calculateGstTds($final_amount, $retailer_commission);
                $sa_amt_with_tds_gst = $sa_cal_with_tds_gst['earnings']; //sa_amt_with_tds_gst 
                $without_gst = $without_gst + $sa_amt_with_tds_gst;
                $remaining_commission = round($with_gst, 2) - round($earnings, 2);
            }

            $comm_distribution_array['user_id'] = $agent_id;
            $comm_distribution_array['service_id'] = $service_id;
            $comm_distribution_array['commission_id'] = $commission_id;
            $comm_distribution_array['transaction_no'] = $trans_ref_no;
            $comm_distribution_array['transaction_amt'] = $recharge_amount;
            $comm_distribution_array['service_comm_percent'] = SERVICE_COMMISSION;
            
            $comm_distribution_array['trimax_comm_percent'] = $total_commission;
            $comm_distribution_array['gst_percentage'] = GST_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['tds_percentage'] = TDS_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['trimax_earnings'] = $earnings;
            $comm_distribution_array['trimax_gst'] = $gst;
            $comm_distribution_array['trimax_tds'] = $tds;
            $comm_distribution_array['trimax_final_amt'] = $final_amount;
            $comm_distribution_array['rokad_trimax_per'] = $values[0]->Trimax;
            $comm_distribution_array['rokad_trimax_amt'] = $rokad_trimax_amt;
            $comm_distribution_array['rokad_trimax_amt_with_tds_gst'] = $rokad_trimax_amt_with_tds_gst;
            $comm_distribution_array['company_per'] = $values[0]->Rokad;
            $comm_distribution_array['company_amt'] = $company_amt;
            $comm_distribution_array['company_amt_with_tds_gst'] = $company_amt_with_tds_gst;
            $comm_distribution_array['rd_per'] = $values[0]->MD;
            $comm_distribution_array['rd_amt'] = $rd_amt;
            $comm_distribution_array['rd_amt_with_tds_gst'] = $rd_amt_with_tds_gst;
            $comm_distribution_array['dd_per'] = $values[0]->AD;
            $comm_distribution_array['dd_amt'] = $dd_amt;
            $comm_distribution_array['dd_amt_with_tds_gst'] = $dd_amt_with_tds_gst;
            $comm_distribution_array['ex_per'] = $values[0]->Distributor;
            $comm_distribution_array['ex_amt'] = $ex_amt;
            $comm_distribution_array['ex_amt_with_tds_gst'] = $ex_amt_with_tds_gst;
            $comm_distribution_array['sa_per'] = $values[0]->Retailer;
            $comm_distribution_array['sa_amt'] = $sa_amt;
            $comm_distribution_array['sa_amt_with_tds_gst'] = $sa_amt_with_tds_gst;
            $comm_distribution_array['status'] = 'Y';
            $comm_distribution_array['created_by'] = $user_id;
            $comm_distribution_array['commission_difference'] = $remaining_commission;
            $comm_distribution_array['created_date'] = date('Y-m-d H:i:s');
            
            $id = $this->Utilities_model->saveVasCommissionData($comm_distribution_array);
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'tata_docomo_special.log', $comm_distribution_array, 'Commission Distribution Response');
        }
    }
    
    private function returnTempbooking($input, $returnJourny, $walletTransId)
    {
        $walletTransId = substr(hexdec(uniqid()), 4, 12);
        if(!empty($returnJourny)){
            $convey_charge_value = 0;
            $convey_charge_type = '';
            $newreturnJourny = $rpostData = $rticketDetailsData = $rpassenger_details = array();
            $newreturnJourny = $returnJourny['returnJourny'];
            $provider_id = $newreturnJourny['provider_id'];
            $from = $newreturnJourny['from'];
            $to = $newreturnJourny['to'];
            $inventory_type = $newreturnJourny['inventory_type'];
            $date = $newreturnJourny['date'];
            $trip_id = $provider_id == 6 ? $newreturnJourny['service_id'] :$newreturnJourny['trip_no'];
            $service_id = $newreturnJourny['service_id'];
            $boarding_stop = ($newreturnJourny['boarding_stop_name']) ? ($newreturnJourny['boarding_stop_name']) : ($from);
            $dropping_stop = ($newreturnJourny['dropping_stop_name']) ? ($newreturnJourny['dropping_stop_name']) : ($to);
            $customer_email = $input['email'];
            $customer_mobile = $input['mobile'];
            $customer_id_type = $this->input->post('id_type1');
            $customer_id_number = $this->input->post('id_number1');
            $customer_name = $this->input->post('psgr_name1');
            $customer_age = $this->input->post('psgr_age1');
            $customer_sex = $this->input->post('psgr_sex1');
            $operator_name = $newreturnJourny['op_name'];
            $sch_departure_time = date("Y-m-d H:i:s", strtotime($newreturnJourny['dept_time']));
            $sch_alighting_time = date("Y-m-d H:i:s", strtotime($newreturnJourny['alighting_time']));
            $sch_boarding_time = $returnJourny['boarding_point'];
            $sch_dropping_time = $returnJourny['dropping_point'];
            $total_travel_time = $returnJourny['total_travel_time'];

            $seats_details = $input['rseat'];
            $seats_info = $newreturnJourny['seats'];

            if (count($seats_details) > 0 && count($seats_info) > 0) {
                foreach ($seats_info as $birth => $lseat) {
                    $tbirth = $birth;
                    $i = 0;
                    foreach ($seats_details[$tbirth] as $key => $val) {
                         $subtot_amt = $tot_amt = '';
                        if($provider_id == 2){
                            if($val['age'] >= 65){
                                $subtot_amt = $lseat[$i]['senior_citizen_fare'];
                                $tot_amt = $lseat[$i]['senior_citizen_fare'];
                            }else{
                                $subtot_amt = $lseat[$i]['seat_fare'];
                                $tot_amt = $lseat[$i]['total_fare'];
                            }
                        }else{
                             $subtot_amt = $lseat[$i]['seat_fare'];
                             $tot_amt = $lseat[$i]['total_fare'];
                        }
                        $rpostData[] = array(
                            'name' => $val['name'],
                            "age" => $val['age'],
                            "sex" => $val['sex'],
                            "birth" => $tbirth,
                            "seat_no" => $val['seat_no'],
//                            "subtotal" => $lseat[$i]['seat_fare'],
//                            "total" => $lseat[$i]['total_fare'],
                            "subtotal" => $subtot_amt,//($val['age'] >= 65) ? $lseat[$i]['senior_citizen_fare']:$lseat[$i]['seat_fare'],
                            "total" =>   $tot_amt,//($val['age'] >= 65) ? $lseat[$i]['senior_citizen_fare']:$lseat[$i]['total_fare'],
                            "is_ladies" => $lseat[$i]['is_ladies'],
                            "mobile" => $customer_mobile,
                            "title" => ($val['sex'] == 'M') ? 'Mr' : 'Mrs',
                            "email" => $customer_email,
                            "id_type" => $val['id_type'],
                            "id_number" => $val['id_number'],
                            "name_on_id" => "",
                            "primary" => "True",
                            "ac" => $lseat[$i]['is_ac'],
                            "sleeper" => $lseat[$i]['is_sleeper']
                        );


                        $rticketDetailsData[] = array(
                            'psgr_no' => $i + 1,
                            'psgr_name' => $val['name'],
                            'psgr_age' => $val['age'],
                            'psgr_sex' => $val['sex'],
                            'psgr_type' => 'A',
                            'concession_cd' => 1, // if exists /in case of rsrtc 60 pass ct 
                            'concession_rate' => 1, // from api /if exists /in case of rsrtc 60 pass ct 
                            'discount' => 1, // if available from api
                            'is_home_state_only' => 1, // Y or No //not required now
                            'adult_basic_fare' => $newreturnJourny['adult_fare'],
                            'child_basic_fare' => $newreturnJourny['child_fare'],
                            'senior_citizen_fare' => $newreturnJourny['senior_fare'],
                            'fare_amt' => $tot_amt,//($val['age'] >= 65) ? $lseat[$i]['senior_citizen_fare']:$lseat[$i]['seat_fare'],//$lseat[$i]['seat_fare'],
                            'convey_name' => "Convenience charge",
                            'convey_type' => $convey_charge_type,
                            'convey_value' => $convey_charge_value,
                            'fare_convey_charge' => $convey_charge_value,
                            'seat_no' => $val['seat_no'],
                            'berth_no' => $tbirth,
                            'seat_status' => 'Y',
                            'concession_proff' => 1, // if cct then required // voter id
                            'proff_detail' => 1, // if varified
                            'status' => 'Y'
                        );

                        $i++;
                    }
                }
            }
            $this->session->set_userdata('rpassenger_details', $rpostData);
            $rpassenger_details = json_encode($rpostData);

            $rpostInput = array(
                'provider_id' => $provider_id,
                'from' => $boarding_stop,
                'to' => $dropping_stop,
                'date' => date("m/d/Y", strtotime($date)),
                'boarding_stop' => $boarding_stop,
                'dropping_stop' => $dropping_stop,
                'trip_id' => $trip_id,
                'email' => $customer_email,
                'mobile' => $customer_mobile,
                'passenger' => $rpassenger_details,
                'name' => 'sagar',
                'address' => '',
                'inventory_type' => $inventory_type,
            );
        }
        
            $curl_url = $this->base_url .  "rest_server/search_bus/temp_booking";
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'temp_booking_return.log', $rpostInput, 'input temp booking op');
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'temp_booking_return.log', $rticketDetailsData, 'input temp booking op');
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'temp_booking_return.log', $rpostData, 'input temp booking op');
            $curlResult = curlForPostData_BOS($curl_url, $rpostInput, $this->sessionData, $getPost = "POST");
            $output = json_decode($curlResult);
            //echo "<pre>"; print_r($postInput);
            //echo "<pre>"; print_r($this->session->userdata()); die();
            $data['output'] = json_decode($curlResult);
            $status = $output->data->status;
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'temp_booking_return.log', $output, 'temp booking op');
            if (strtolower($status) == 'success') {
                $boarding_stop_name = $boarding_stop;
                $fare_reservationCharge = $asn_fare = $ac_service_charges = $op_service_charge = $fare_acc = $fare_it = $fare_toll = $fare_hr = $service_tax = $fare_convey_charge = 0;
                $destination_stop_name = $dropping_stop;
                if(is_object($output->data->data->ticket_detail->msrtc))
                {
                   $fare_reservationCharge = $output->data->data->ticket_detail->msrtc->reservation_charge; 
                   $asn_fare = $output->data->data->ticket_detail->msrtc->asn_amount; 
                   $ac_service_charges = $output->data->data->ticket_detail->msrtc->ac_service_charges; 
                }elseif(is_object($output->data->data->ticket_detail->upsrtc))
                {
                    $fare_reservationCharge = $output->data->data->ticket_detail->upsrtc->reservation_charge; 
                    $fare_acc = $output->data->data->ticket_detail->upsrtc->acc_sur; 
                    $fare_it = $output->data->data->ticket_detail->upsrtc->it_charge;
                    $fare_toll = $output->data->data->ticket_detail->upsrtc->toll_charge;
                    $fare_hr = $output->data->data->ticket_detail->upsrtc->hr_charge;
                    $service_tax = $output->data->data->ticket_detail->upsrtc->gstamt;
                    $fare_convey_charge = $output->data->data->ticket_detail->upsrtc->convey_charges;
                }
                $from_stop_name = $this->session->userdata['bus_details']['from'];
                $to_stop_name = $this->session->userdata['bus_details']['to'];
                 //--------------Get dynamic cancellation policy using trip_id from stored json file----------------
                
                $search_date = date("d_m_Y", strtotime($date));
                $fromstopname = $this->session->userdata['bus_services_input']['from'];
                $tostopname = $this->session->userdata['bus_services_input']['to'];
                $file_name = strtolower($fromstopname) . "_" . strtolower($tostopname) . "_" . $search_date . '.json';
                $file_name = str_replace(' ', '', $file_name);
                $file_path = FCPATH . "json_data/";
                if (file_exists($file_path . $file_name)) {
                     $cancel_policy = '';
                    $json_data = file_get_contents($file_path . $file_name);
                    $output_result = json_decode($json_data); 
                    $data['result'] = $output_result->result;
                    $data['input_data'] = $output_result->input_data;
//                    $data['getCancelPolicy'] = json_decode($json_data, true);
                   if(!empty($data['result'])){
                    foreach ($data['result'] as $key => $values) {
                        if($values->trip_id == $trip_id){
                            foreach($values->cancellation_policy as $cancel_pol){
                                //$cancel_policy .= '<li>'.$cancel_pol->refundInPercentage.'% ticket fare will get deducted if the ticket is cancelled '. $cancel_pol->cutoffTime.' hrs. before the departure time.</li>';
                                if(isset($cancel_pol->refundInPercentage) && !empty($cancel_pol->refundInPercentage)){
                                    $cancel_policy .= '<li>'.$cancel_pol->refundInPercentage.'% ticket fare will get deducted if the ticket is cancelled '. $cancel_pol->cutoffTime.' hrs. before the departure time.</li>';
                                }else if(isset($cancel_pol->Mins) && !empty($cancel_pol->Mins)){
                                    $cancel_policy .= '<li>'.$cancel_pol->Pct.'% ticket fare will get deducted if the ticket is cancelled '. $cancel_pol->Mins.' minutes. before the departure time.</li>';
                                }
                            }
                        }
                    }
                    $cancel_policy .= '<li>The departure time means departure time of the orignating bus station.</li><li>Reservation charges and convenience charges including GST charge are not refundable.</li>';
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dynamic_policy_return.log', $cancel_policy, 'dynamic policy for'.$file_name.''.$from_stop_name.''.$to_stop_name);
                   }
                }
                //----------------------End------------------------------------------------------------------------------
                $ticket_data = array(
                    'ticket_ref_no' => $output->data->data->block_key,
                    'total_basic_amount' => $output->data->data->base_fare,
                    'total_basic_with_ac' => $output->data->data->total_fare,
                    'tot_fare_amt' => $output->data->data->total_fare,
                    'tot_fare_amt_with_tax' => $output->data->data->transaction_amount,
                    'total_fare_without_discount' => $output->data->data->total_fare,
                    'asn_fare' => $asn_fare,
                    'ac_service_tax' => $ac_service_charges,
                    'fare_reservationCharge' =>$fare_reservationCharge,
                    'fare_acc' => $fare_acc,
                    'fare_it' => $fare_it,
                    'fare_toll' => $fare_toll,
                    'fare_hr' => $fare_hr,
                    'service_tax' => $service_tax,
                    'fare_convey_charge' => $fare_convey_charge,
                    'status' => 'Y',
                    'transaction_status' => 'temp_booked',
                    'user_email_id' => $customer_email,
                    'agent_email_id' => $this->agent_email,
                    'agent_mobile_no' => $this->agent_mobile_no,
                    'mobile_no' => $customer_mobile,
                    'from_stop_name' => $from_stop_name,//$newreturnJourny['to'],
                    'till_stop_name' => $to_stop_name,//$newreturnJourny['from'],
                    'boarding_stop_name' => $boarding_stop,
                    'destination_stop_name' => $dropping_stop,
                    'tds_per' => $output->data->data->agent_tds_value,
                    'num_passgr' => count($rpostData),
                    'booked_from' => $operator_name,
                    'booked_by' =>!empty($user_id) ? $user_id :trim($this->session->userdata('user_id')),
                    'op_name' => $operator_name,
                    'bus_type' => $newreturnJourny['data_bus_type'],
                    'provider_type' => $newreturnJourny['provider_type'],
                    'provider_id' => $newreturnJourny['provider_id'],
                    'is_return_journey' => '1',
                    'inventory_type' => $inventory_type,
                    'bus_service_no' => $trip_id,
                    'dept_time' => $sch_departure_time,
                    'alighting_time' => $sch_alighting_time,
                    'boarding_time' => $sch_boarding_time,
                    'droping_time' => $sch_dropping_time,
                    'wallet_trans_id' => $walletTransId,
                    'dynamic_cancellation_policy' => $cancel_policy,
                    'route_no_name' =>  $newreturnJourny['route_no_name']
                );

                $block_key = $output->data->data->block_key;
                $this->session->set_userdata('rblock_key', $block_key);
                $this->session->set_userdata('rticket_data', $ticket_data);

                $save_data = $this->Search_bus_model->saveTicketData($ticket_data);

                if ($save_data) {
                    for ($m = 0; $m < count($rticketDetailsData); $m++) {
                        $newTicketDetailsData[] = array('ticket_id' => $save_data) + $rticketDetailsData[$m];
                    }
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'rprabhat.log', $newTicketDetailsData, 'temp booking op');
                    $save_data = $this->Search_bus_model->batchInsertTicketDetails('ticket_details', $newTicketDetailsData);
                    
                    $rData = array();
                    $rData = array(
                        'rdata' => $output->data->data,
                        'rtotalSeat' => count($rpostData)
                    );
                    return $rData;
                }
            } else {
                return false;
            }
        }
       
    private function sendSmsEmail($ticketDetails, $passengerDetails, $flag='')
    {
        $ticketDump = $data  = $ticketView = array();
        $ticketId = $ticketReference = $pnrNo = $passengerEmail = $passengerMobile = $userId = 0; $tripNo = '';
        $ticketId = $ticketDetails[0]['ticket_id'];
        $ticketReference = $ticketDetails[0]['ticket_ref_no'];
        $pnrNo = $ticketDetails[0]['pnr_no'];
        $passengerEmail = $ticketDetails[0]['user_email_id'];
        $passengerMobile = $ticketDetails[0]['mobile_no'];
        $userId = $this->sessionData['id'];
        /* sms send */
         $datas['passenger_ticket'] = $ticketDetails[0];
         $datas['passenger_ticket_details'] = $passengerDetails;
              
        //if ticket booked by ors_agent two sms send one for agent and second for pessanger //
        $mobile_array[] = $ticketDetails[0]['mobile_no'];
        if($ticketDetails[0]['agent_mobile_no'] != "")
        {
            $mobile_array[] = $ticketDetails[0]['agent_mobile_no'];
        }
        if(empty($flag) || $flag != 'pdf') {
            $is_send = advaSendTicketBookedSms($datas, $mobile_array);
            $eticket_count = ($is_send) ? 1 : 0;
            if($eticket_count == 0) {
                echo 'message not send'; die();
            }
        }
        /* end sms*/
        
        if (!empty($passengerDetails)) {
            $psgr_no = 1;
            $reservationCharge = $totalBasicFare = 0;
            $passanger_detail = "";
            $adult_basic_fare = 0;
            $senior_basic_fare = 0;
            $child_basic_fare = 0;
            $seatList ="";
            foreach ($passengerDetails as $key => $value) {
                $reservationCharge += $value['fare_reservationCharge'];

                if ($value['psgr_age'] > 12 && $value['psgr_type'] == "A") {
                    $senior_basic_fare += $value['adult_basic_fare'];
                    $adult_child = "Adult";
                } else if ($value['psgr_age'] <= 12 && $value['psgr_type'] == "C") {
                    $child_basic_fare += $value['child_basic_fare'];
                    $adult_child = "Children";
                }

                $total_calculate_basic_fare = $adult_basic_fare + $senior_basic_fare + $child_basic_fare;
                // $adult_child = ($value->psgr_age > 12) ? "Adult" : "Children";
                $passanger_detail .= '<tr style="border:1px solid #000;">
                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">
                          ' . ucwords($value['psgr_name']) . '
                        </td>
                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                          ' . ucwords($value['psgr_age']) . '
                        </td>
                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                          ' . $adult_child . '
                        </td>
                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                          ' . ucwords($value['psgr_sex']) . '
                        </td>
                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                          ' . $value['seat_no'] . '
                        </td>
                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                          NA
                        </td>
                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                          NA
                        </td>
                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                          NA
                        </td>
                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                          NA
                        </td>
                      </tr>';
                $psgr_no++;
                $seatList = $seatList."".$value['seat_no'] .", ";
            }
            $pickup_address = $ticketDetails[0]['boarding_stop_name'];
            $pickup_address .= ($ticketDetails[0]['pickup_address'] != "") ? " (" . $ticketDetails[0]['pickup_address'] . ")" : "";
            if(trim(strtolower($ticketDetails[0]['op_name'])) == 'msrtc') {
                $totalBasicFare = ($ticketDetails[0]['total_basic_amount'] + $ticketDetails[0]['ac_service_tax']);
            } elseif(trim(strtolower($ticketDetails[0]['op_name'])) == 'upsrtc') {
                 $totalBasicFare = ($ticketDetails[0]['total_basic_with_ac'] + $ticketDetails[0]['ac_service_tax']) - $ticketDetails[0]['service_tax'] - $ticketDetails[0]['fare_convey_charge'];
            }
            $contactDetails = [];
            $contactJson = $ticketDetails[0]['pickup_address'];
            if(!empty($contactJson))
            {
                $contactDetails = json_decode($contactJson, true);
            }
            $mail_replacement_array = array(
                "{{bos_ref_no}}" => $ticketDetails[0]['boss_ref_no'],
                "{{pnr_no}}" => $ticketDetails[0]['pnr_no'],
                "{{ticekt_no}}" => $ticketDetails[0]['ticket_ref_no'],
                "{{bank_transaction_no}}" => $ticketDetails[0]['wallet_trans_id'],
                "{{user_email}}" => $ticketDetails[0]['user_email_id'],
                "{{mobile_number}}" => $ticketDetails[0]['mobile_no'],
                "{{trip_no}}" => $ticketDetails[0]['bus_service_no'],
                // "{{ticket_no}}" => $pt_data[0]->ticket_id,
                "{{ticket_no}}" => $ticketDetails[0]['pnr_no'],
                "{{route_name}}" => isset($ticketDetails[0]['route_no_name']) ? $ticketDetails[0]['route_no_name']:'',
                "{{from_stop_name}}" => strtoupper($ticketDetails[0]['from_stop_name']),
                "{{till_stop_name}}" => strtoupper($ticketDetails[0]['till_stop_name']),
                "{{boarding_stop_name}}" => strtoupper($ticketDetails[0]['boarding_stop_name']),
                "{{destination_stop_name}}" => strtoupper($ticketDetails[0]['destination_stop_name']),
                "{{dept_time_date}}" => date("d/m/Y", strtotime($ticketDetails[0]['dept_time'])),
                "{{dept_time_his}}" => date("H:i", strtotime($ticketDetails[0]['dept_time'])) . " Hrs",
                "{{approx_boarding_time}}" => date("H:i", strtotime($ticketDetails[0]['boarding_time'])) . " Hrs",
                "{{bus_type}}" => $ticketDetails[0]['bus_type'],
                "{{num_passgr}}" => $ticketDetails[0]['num_passgr'],
                "{{bus_types}}" => $ticketDetails[0]['bus_type'], //$service_detail["bus_type"],
                "{{tot_fare_amt}}" => "Rs. " . $ticketDetails[0]['total_basic_amount'], //$totalBasicFare, //total_basic_amount
                "{{fare_reservationCharge}}" => "Rs. " . !empty($ticketDetails[0]['fare_reservationCharge']) ? $ticketDetails[0]['fare_reservationCharge']:0,
                "{{tot_fare_amt_with_tax}}" => "Rs. " . !empty($ticketDetails[0]['total_fare_without_discount'])? $ticketDetails[0]['total_fare_without_discount']:0,
                "{{asn_amount}}" => "Rs. " . !empty($ticketDetails[0]['asn_fare']) ? $ticketDetails[0]['asn_fare']:0 ,
                "{{ac_service_charges}}" => "Rs. " . !empty($ticketDetails[0]['ac_service_tax']) ? $ticketDetails[0]['ac_service_tax']:0,
                "{{passanger_details}}" => $passanger_detail,
                "{{no_of_seats}}" => $psgr_no - 1,
                "{{gst_charges}}" => "Rs. " . !empty($ticketDetails[0]['service_tax']) ? $ticketDetails[0]['service_tax']:0,
                "{{other_charges}}" => "Rs. " . !empty($ticketDetails[0]['fare_convey_charge']) ? $ticketDetails[0]['fare_convey_charge']:0,
                "{{booked_date}}" => date("jS F Y",strtotime($ticketDetails[0]['issue_time'])),
                "{{agent_details}}" => 'Travelyari',
                "{{from_to_journey}}" => strtoupper($ticketDetails[0]['from_stop_name']) . " To ".strtoupper($ticketDetails[0]['till_stop_name']),
                "{{journey_date}}" =>  date("l \- jS F Y",strtotime($ticketDetails[0]['dept_time'])),
                "{{op_name}}" => $ticketDetails[0]['op_name'],
                "{{seat_no}}" => $seatList,
                "{{alighting_time}}" => date("h:i A",strtotime($ticketDetails[0]['alighting_time'])),
                "{{boarding_point_contact_no}}" => isset($contactDetails['boarding_stops'][0]['Contact']) ? $contactDetails['boarding_stops'][0]['Contact']: '',
                "{{boarding_point_address}}" => isset($contactDetails['boarding_stops'][0]['Address']) ? $contactDetails['boarding_stops'][0]['Address']: '',
                "{{boarding_point_landmark}}" => isset($contactDetails['boarding_stops'][0]['Landmark']) ? $contactDetails['boarding_stops'][0]['Landmark']: '',
                "{{dynamic_cancellation_policy}}" => isset($ticketDetails[0]['dynamic_cancellation_policy']) ? $ticketDetails[0]['dynamic_cancellation_policy']:'', 
            );
            
            /* * ********** Mail Start Here ******* */
            if(strtolower($ticketDetails[0]['booked_from']) == 'msrtc')
            {
                $data['mail_content'] = str_replace(array_keys($mail_replacement_array), array_values($mail_replacement_array), MSRTC_TICKET_TEMPLATE);
            } elseif(strtolower($ticketDetails[0]['booked_from']) == 'upsrtc')
            {
                $data['mail_content'] = str_replace(array_keys($mail_replacement_array), array_values($mail_replacement_array), UPSRTC_TICKET_TEMPLATE);
            } /*elseif(strtolower($ticketDetails[0]['booked_from']) == 'etravelsmart')
            {
                $data['mail_content'] = str_replace(array_keys($mail_replacement_array), array_values($mail_replacement_array), UPSRTC_TICKET_TEMPLATE);
            } */
            elseif(strtolower($ticketDetails[0]['booked_from']) == 'rsrtc')
            {
                $data['mail_content'] = str_replace(array_keys($mail_replacement_array), array_values($mail_replacement_array), RSRTC_TICKET_TEMPLATE);
            }elseif(strtolower($ticketDetails[0]['booked_from']) == 'travelyaari' || strtolower($ticketDetails[0]['booked_from']) == 'etravelsmart')
            {
                $data['mail_content'] = str_replace(array_keys($mail_replacement_array), array_values($mail_replacement_array), TRAVELYAARI_BOS_TICKET_TEMPLATE);
            }
            $mailbody_array = array("subject" => "Rokad Ticket",
                "message" => $data['mail_content'],
                "to" => $ticketDetails[0]['user_email_id']
            );
            if(empty($flag) || $flag != 'pdf') {
                $mail_result = send_mail($mailbody_array);
                //****if agent booked tkt send mail to both agent as well as customer****//
                $email_agent = $ticketDetails[0]['agent_email_id'];
                if ($email_agent != "" && ($email_agent != $email)) {
                    $agent_mailbody_array = array("subject" => "Rokad Ticket",
                        "message" => $data['mail_content'],
                        "to" => $email_agent
                    );
                    $agent_mail_result = send_mail($agent_mailbody_array);
                }
            }
        }
        
        if ($ticketDetails) {
            $ticketDump = array(
                'ticket_id' => $ticketId,
                'bos_ref_no' => $ticketDetails[0]['boss_ref_no'],
                'ticket_ref_no' => $ticketReference,
                'pnr_no' => $ticketDetails[0]['pnr_no'],
                'email' => $passengerEmail,
                'mobile' => $passengerMobile,
                'journey_date' => $ticketDetails[0]['boarding_time'],
                'ticket' => $data['mail_content'],
                'created_by' => $this->sessionData['id']
            );
            if (!empty($ticketDump)) {
                $checkExistTicket = $this->Search_bus_model->checkExistTicket($ticketId);
                if(!$checkExistTicket){
                     $this->Search_bus_model->insertTicketDump('ticket_dump', $ticketDump);
                }
                return $data['mail_content'];
            } else {
                $ticketView = $this->Search_bus_model->getTickets($bos_key);
                return $ticketView[0]->ticket;
            }
        }
    }
    
    public function savePDF($bos_key = '', $rbos_key = '')
    {
        $this->load->library('Pdf');
        $pdf = new Pdf('L');
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->SetFont('dejavusans', '', 10);
        $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
        $pdf->SetDisplayMode('real');
        $pdf->AddPage('L');
        
        $ticketId = 0; $onwadTicket = $returnTicket ='';
        $data['bosKey'] = $bos_key;
        $data['rbosKey'] = $rbos_key;
        $ticketDetails = $passengerDetails = array();
        
        $ticketDetails = $this->Search_bus_model->getTicketsDetails($bos_key);
        
        if (empty($ticketDetails) && empty($ticketDetails[0]['pnr_no'])) {
            die('Your tickets can not view please contact with our support team!');
        }
        $ticketId = $ticketDetails[0]['ticket_id'];
        $passengerDetails = $this->Search_bus_model->getPassengerDetails($ticketId);
        
        $onwadTicket = $this->sendSmsEmail($ticketDetails, $passengerDetails, 'pdf');
        
        if(!empty($data['rbosKey']) && !empty($rbos_key)) {
            $ticketDetails = $this->Search_bus_model->getTicketsDetails($rbos_key);
            $ticketId = $ticketDetails[0]['ticket_id'];
            $passengerDetails = $this->Search_bus_model->getPassengerDetails($ticketId);
            $returnTicket = $this->sendSmsEmail($ticketDetails, $passengerDetails, 'pdf');
            $bothTickets = $onwadTicket."<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><H3>
Return Journey Ticket </H3><br/><b>".$returnTicket;
            $pdf->writeHTML($bothTickets, true, false, false, false);
            $pdf->Output('rokad_onward_return_ticket_' . date('Y-m-d') . '_' . time() . '.pdf', 'D');
        }
        
        $pdf->writeHTML($onwadTicket, true, false, false, false);
        $pdf->Output('rokad_onward_ticket_' . date('Y-m-d') . '_' . time() . '.pdf', 'D');
        
    }
    
    private function serviceRetailerCommissionDistributionForRecharges($input = "", $trans_ref_no = "",$created_by='') {
        
        $user_id = $input['user_id'];
        if(in_array($input['type'], ['jri', 'JRI', 'tso', 'TSO', 'BOS', 'bos']) ){
            if($input['recharge_type'] == 'BOS_TICKET'){
               $code = strtolower($input['type']);
                $service_id = BOS_SERVICE_ID;
            }
           
            $LcSqlStr = "SELECT * from services_commissions 
            where commission_name like '%" . str_replace(" ", "", $code) . "%' 
            and service_id = '" . $service_id . "'" ;
           
            $query = $this->db->query($LcSqlStr);
            $new = $query->result();
                        
            if($new){ 
                $row = $new[0];
                $getRetailerServicesCommission = $this->Utilities_model->getRetailerServicesCommissionForRecharges($user_id, $row->service_id, $row->sub_service_id, $created_by);
            }
        }else{
            $service_id = UTILITIES_SERVICE_ID;
            $getRetailerServicesCommission = $this->Utilities_model->getRetailerServicesCommission($user_id, $service_id, $created_by);
        }

        if ($getRetailerServicesCommission) {

            $agent_id = $getRetailerServicesCommission[0]['user_id'];
            $agent_code = $getRetailerServicesCommission[0]['agent_code'];
            $distributor_id = $getRetailerServicesCommission[0]['level_5'];
            $Area_dis_id = $getRetailerServicesCommission[0]['level_4'];
            $master_dis_id = $getRetailerServicesCommission[0]['level_3'];
            $company_id = $getRetailerServicesCommission[0]['level_2'];
            $trimax_id = $getRetailerServicesCommission[0]['level_1'];
            $service_id = $getRetailerServicesCommission[0]['service_id'];
            $commission_id = $getRetailerServicesCommission[0]['commission_id'];
            $total_commission = $getRetailerServicesCommission[0]['total_commission'];

            $values[0] = json_decode($getRetailerServicesCommission[0]['values']);

            $trimax_commission = '0';
            $company_commission = '0';
            $md_commission = '0';
            $ad_commission = '0';
            $dist_commission = '0';
            $retailer_commission = '0';
            $with_gst = $without_gst = 0;
            if (!empty($values)) {

                $trimax_commission = round($values[0]->Trimax, 2);
                $company_commission = round($values[0]->Rokad, 2);
                $md_commission = round($values[0]->MD, 2);
                $ad_commission = round($values[0]->AD, 2);
                $dist_commission = round($values[0]->Distributor, 2);
                $retailer_commission = round($values[0]->Retailer, 2);
                if($input['recharge_type'] != 'BOS_TICKET'){
                    $recharge_amount = $input['recharge_amount']; 
                    $commission_percent = !empty($total_commission) ? $total_commission : '0';                        
                    $trimax_cal = calculateGstTds($recharge_amount, $commission_percent);
                    $earnings = $trimax_cal['earnings']; //trimax_earnings
                    $gst = $trimax_cal['gst']; //trimax_gst                            
                    $tds = $trimax_cal['tds']; //trimax_tds
                    $final_amount = $trimax_cal['final_amount']; //trimax_final_amt                                                        
                } else
                {
                    $recharge_amount = $input['recharge_amount'];
                    $totalCommission = $input['totalCommission'];
                    $trimax_cal = bosCalculateGstTds($recharge_amount, $totalCommission);
                    $earnings = $trimax_cal['earnings']; //trimax_earnings
                    $gst = $trimax_cal['gst']; //trimax_gst                            
                    $tds = $trimax_cal['tds']; //trimax_tds
                    $final_amount = $trimax_cal['final_amount']; //trimax_final_amt  
                }
                $rokad_trimax_cal = calculateGstTds($earnings, $trimax_commission);
                $rokad_trimax_amt = $rokad_trimax_cal['earnings']; //rokad_trimax_amt
                $with_gst = $with_gst + $rokad_trimax_cal['earnings'];
                $rokad_trimax_cal_with_tds_gst = calculateGstTds($final_amount, $trimax_commission);
                $rokad_trimax_amt_with_tds_gst = $rokad_trimax_cal_with_tds_gst['earnings']; //rokad_trimax_amt_with_tds_gst
                $without_gst = $without_gst + $rokad_trimax_amt_with_tds_gst;        

                $company_cal = calculateGstTds($earnings, $company_commission);
                $company_amt = $company_cal['earnings'];//company gst
                $with_gst = $with_gst + $company_cal['earnings'];
                $company_cal_with_tds_gst = calculateGstTds($final_amount, $company_commission);
                $company_amt_with_tds_gst = $company_cal_with_tds_gst['earnings']; //company_amt_with_tds_gst                    
                $without_gst = $without_gst + $company_amt_with_tds_gst;    

                $rd_cal = calculateGstTds($earnings, $md_commission);
                $rd_amt = $rd_cal['earnings']; //rd_amt            
                $with_gst = $with_gst + $rd_cal['earnings'];                 
                $rd_cal_with_tds_gst = calculateGstTds($final_amount, $md_commission);
                $rd_amt_with_tds_gst = $rd_cal_with_tds_gst['earnings']; //rd_amt_with_tds_gst
                $without_gst = $without_gst + $rd_amt_with_tds_gst;

                $dd_cal = calculateGstTds($earnings, $ad_commission);
                $dd_amt = $dd_cal['earnings']; //dd_amt   
                $with_gst = $with_gst + $dd_cal['earnings'];                                 
                $dd_cal_with_tds_gst = calculateGstTds($final_amount, $ad_commission);
                $dd_amt_with_tds_gst = $dd_cal_with_tds_gst['earnings']; //dd_amt_with_tds_gst
                $without_gst = $without_gst + $dd_amt_with_tds_gst;

                $ex_cal = calculateGstTds($earnings, $dist_commission);
                $ex_amt = $ex_cal['earnings']; //ex_amt      
                $with_gst = $with_gst + $ex_cal['earnings'];              
                $ex_cal_with_tds_gst = calculateGstTds($final_amount, $dist_commission);
                $ex_amt_with_tds_gst = $ex_cal_with_tds_gst['earnings']; //ex_amt_with_tds_gst
                $without_gst = $without_gst + $ex_amt_with_tds_gst;
                $sa_cal = calculateGstTds($earnings, $retailer_commission);
                $sa_amt = $sa_cal['earnings']; //sa_amt     
                $with_gst = $with_gst + $sa_cal['earnings'];              
                $sa_cal_with_tds_gst = calculateGstTds($final_amount, $retailer_commission);
                $sa_amt_with_tds_gst = $sa_cal_with_tds_gst['earnings']; //sa_amt_with_tds_gst 
                $without_gst = $without_gst + $sa_amt_with_tds_gst;
                $remaining_commission = round($with_gst, 2) - round($earnings, 2);
            }

            $comm_distribution_array['user_id'] = $agent_id;
            $comm_distribution_array['service_id'] = $service_id;
            $comm_distribution_array['commission_id'] = $commission_id;
            $comm_distribution_array['transaction_no'] = $trans_ref_no;
            $comm_distribution_array['transaction_amt'] = $recharge_amount;
            $comm_distribution_array['service_comm_percent'] = SERVICE_COMMISSION;
            
            $comm_distribution_array['trimax_comm_percent'] = $total_commission;
            $comm_distribution_array['gst_percentage'] = GST_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['tds_percentage'] = TDS_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['trimax_earnings'] = $earnings;
            $comm_distribution_array['trimax_gst'] = $gst;
            $comm_distribution_array['trimax_tds'] = $tds;
            $comm_distribution_array['trimax_final_amt'] = $final_amount;
            $comm_distribution_array['rokad_trimax_per'] = $values[0]->Trimax;
            $comm_distribution_array['rokad_trimax_amt'] = $rokad_trimax_amt;
            $comm_distribution_array['rokad_trimax_amt_with_tds_gst'] = $rokad_trimax_amt_with_tds_gst;
            $comm_distribution_array['company_per'] = $values[0]->Rokad;
            $comm_distribution_array['company_amt'] = $company_amt;
            $comm_distribution_array['company_amt_with_tds_gst'] = $company_amt_with_tds_gst;
            $comm_distribution_array['rd_per'] = $values[0]->MD;
            $comm_distribution_array['rd_amt'] = $rd_amt;
            $comm_distribution_array['rd_amt_with_tds_gst'] = $rd_amt_with_tds_gst;
            $comm_distribution_array['dd_per'] = $values[0]->AD;
            $comm_distribution_array['dd_amt'] = $dd_amt;
            $comm_distribution_array['dd_amt_with_tds_gst'] = $dd_amt_with_tds_gst;
            $comm_distribution_array['ex_per'] = $values[0]->Distributor;
            $comm_distribution_array['ex_amt'] = $ex_amt;
            $comm_distribution_array['ex_amt_with_tds_gst'] = $ex_amt_with_tds_gst;
            $comm_distribution_array['sa_per'] = $values[0]->Retailer;
            $comm_distribution_array['sa_amt'] = $sa_amt;
            $comm_distribution_array['sa_amt_with_tds_gst'] = $sa_amt_with_tds_gst;
            $comm_distribution_array['status'] = 'Y';
            $comm_distribution_array['created_by'] = $user_id;
            $comm_distribution_array['commission_difference'] = $remaining_commission;
            $comm_distribution_array['created_date'] = date('Y-m-d H:i:s');
            
           $id = $this->Utilities_model->saveVasCommissionData($comm_distribution_array);
           log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'vas_commission_details.log', $comm_distribution_array, 'Commission Distribution Response');
           if($id){
                $comm_date = date('Y-m-d');
                $comm_data = $this->commission_distribution_api->credit_vas_amount($service_id,$comm_date,'',$trans_ref_no);   
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'vas_commission_details.log', $comm_data.' Comm distributed for '.$trans_ref_no, 'BOS commission distributed');
           }
            return $comm_distribution_array;
        }
    }
    
    /**
     * @desc checking balance of rokad api
     *  
     * **/
    public function getBalanceDetails() {
        $currentBalance = 0;
        $curl_url = $this->base_url .  "rest_server/search_bus/getBalance";
        $input = array('email' => BOS_SUPERADMIN_EMAIL);
        
        $curlResult = curlForPostData_BOS($curl_url, $input, $this->sessionData, $getPost = "POST");
        $output = json_decode($curlResult);
        
        if(isset($output->data->status) && strtolower($output->data->status) == 'success') {
            $currentBalance = $output->data->data[0]->amount;
        }
        $this->data['status'] = $output->data->status;
        $this->data['balance'] = $currentBalance;
        $this->data['requestType'] = "BOS";
        load_back_view(BOS_WALLET_BALANCE_VIEW, $this->data);
    }
    
    /**
     * @Desc Check commission is assign or not for current login agent
     * @return boolean
     */
    private function checkCommissionExists($user_id) {
        $commissionResult = $this->Utilities_model->commission_from($user_id);
        $created_by = $commissionResult[0]['level_3'];
        $check_if_commission_available = $this->checkCommission($created_by);
        //echo "<pre>"; print_r($check_if_commission_available); die();
        if(count($check_if_commission_available) == 0){
            $this->session->set_flashdata('item', 'Commission not set. Please contact your administrator.');
            redirect(base_url() . 'admin/dashboard');
       } else
       {
           return true;
       }
    }
    
    /**
     * 
     * @param type $created_by
     * @return type array 
     */
    private function checkCommission($created_by){
        $service_id = BOS_SERVICE_ID;
        $LcSqlStr = "SELECT * from services_commissions 
        where status = 'Y'";
        $LcSqlStr .= " AND `values` NOT LIKE '{\"Trimax\":\"0\",\"Rokad\":\"0\",\"MD\":\"0\",\"AD\":\"0\",\"Distributor\":\"0\",\"Retailer\":\"0\"}'";
        $LcSqlStr .= " AND `created_by` = '". $created_by . "'";
        $LcSqlStr .= " AND service_id = '" . $service_id . "'"; 
        
        $query = $this->db->query($LcSqlStr);
        return $query->result();

    }
    
    private function cmp($a, $b){
        return strcmp($a["provider_id"], $b["provider_id"]);
    }
    
    /*
    * @Author      : Sonali Kamble
    * @function    : ticket_by_sms_email()
    * @param       : 
    * @detail      : get ticket by sms and email or view and print ticket.
    * @date        : 20-april-19      
    */
    public function ticket_by_sms_email ()
    {
        //is_ajax_request();
        $ticket_dump_data = array();
        $input = $this->input->post();
        
        //same functionality implemented in two pages. is_history_task used to differentiate.
        $is_history_task = $input["is_history_task"];
        $ticket_mode = $input["ticket_mode"];

        if($is_history_task)
        {
           $ticket_ref_no = $input["ticket_ref_no"];
        }
        else
        {
            $bos_ref_no = $input["bos_ref_no"];
            $email = $input["email"];
        }
       
        if($ticket_mode == "print" || $ticket_mode == "sms" || $ticket_mode == "mail")
        {
            if($is_history_task)
            {
                if($this->session->userdata("user_id") > 0)
                {
                    $ticket_dump_data = $this->tickets_model->get_history_ticket_sms_email($ticket_ref_no,$this->session->userdata("user_id"));
                    $bos_ref_no = $ticket_dump_data[0]->boss_ref_no;
                }
            }
            else
            {
               $ticket_dump_data =  $this->Search_bus_model->getTicketsDetails($bos_ref_no);
               
               // $ticket_dump_data = $this->tickets_model->get_ticket_sms_email($bos_ref_no,$email);
            }

            if(count($ticket_dump_data) > 0)
            {
                if($ticket_dump_data[0]['status'] == "Y")
                {
                    if($ticket_mode == "print")
                    {
                        $data['ticket_details'] = $ticket_dump_data[0];                        
                        $data["flag"] = '@#success#@';
                        $data['type'] = "print"; 
                    }
                    else if ($ticket_mode == "sms")
                    {
                        //Send ticket on Message. 



                        /* sms send */
                        $flag='';
                        $ticketDetails = $passengerDetails = array();
                        
                        $ticketDetails = $this->Search_bus_model->getTicketsDetails($bos_ref_no);
                        $ticketId = $ticketDetails[0]['ticket_id'];
                        
                        $passengerDetails = $this->Search_bus_model->getPassengerDetails($ticketId);
                        
                        $datas['passenger_ticket'] = $ticketDetails[0];
                        $datas['passenger_ticket_details'] = $passengerDetails;
                        
                        
                        $mobile_array[] = $ticketDetails[0]['mobile_no'];
                        
                        if (empty($flag) || $flag != 'pdf') {
                            $is_send = advaSendTicketBookedSms($datas, $mobile_array);
                            $eticket_count = ($is_send) ? 1 : 0;
                            if ($eticket_count == 0) {
                                echo 'message not send';
                                die();
                            }
                        }
                        /* end sms */



                        if($is_send)
                            {
//                                $this->tickets_model->ticket_id = $ticket_dump_data[0]->ticket_id;
//                                $this->tickets_model->bos_ref_no = $bos_ref_no;
//                                $this->tickets_model->eticket_count = $ticket_dump_data[0]->eticket_count + 1; 
//                                $this->tickets_model->save();

                                $data["flag"] = '@#success#@';
                                $data['type'] = "sms";
                                $data['msg_type'] = "success"; 
                                $data['msg'] = "Your ticket is sent to ".$ticket_dump_data[0]['mobile_no'];
                            }
                            else
                            {
                                $data["flag"] = '@#error#@';
                                $data['type'] = "sms";
                                $data['msg_type'] = "error"; 
                                $data['msg'] = "Problem occured while sending message on ".$ticket_dump_data[0]['mobile_no'];
                            }
                        
                        /*
                        * The Above SMS Code is temp and For testing purpose only
                        */
                    }
                    else if ($ticket_mode == "mail") {
                        
                       
                        $ticketId = 0; $onwadTicket = $returnTicket ='';
        
                        $ticketDetails = $passengerDetails = array();

                        $ticketDetails = $this->Search_bus_model->getTicketsDetails($bos_ref_no);
                        if (empty($ticketDetails) && empty($ticketDetails[0]['pnr_no'])) {
                            die('Your tickets can not view please contact with our support team!');
                        }
                        $ticketId = $ticketDetails[0]['ticket_id'];
                        $passengerDetails = $this->Search_bus_model->getPassengerDetails($ticketId);

                        $onwadTicket = $this->sendSmsEmail($ticketDetails, $passengerDetails);
                        
                        

                        // if($mail_result > 0)
                        // {
                           $data['flag'] = "@#success#@";
                           $data['type'] = "mail";  
                           $data['msg_type'] = "success"; 
                           $data['msg'] = "Mail Sent successfully to ".$ticket_dump_data[0]['user_email_id'];  
                        // }
                    }
                }
                else
                {
                    $data["flag"] = '@#error#@';
                    $data['type'] = "sms";
                    $data['msg_type'] = "error"; 
                    $data['msg'] = "Ticket is cancelled. Could not perform operation."; 
                }
            }
            else
            {
                $data["flag"] = '@#error#@';
                $data['msg_type'] = "error"; 
                $data['msg'] = "Please provide appropriate details.";
            }
        }
        else
        {
            $data["flag"] = '@#error#@';
            $data['msg_type'] = "error"; 
            $data['msg'] = "Please select ticket method";
        }
        
        data2json($data);
    } // End of ticket_by_sms_email
    
     public  function get_modal_data(){
        if(isset($_POST["trip_id"])){  
            $output = '';  
            $arr_icon_class = '';
            $dep_icon_class = '';
            $trip_id = $_POST['trip_id'];
            $bus_travel_id = $_POST['bus_travel_id'];
            $details_array = array();
            $serviceResult = $this->session->userdata('serviceResult');
            
            foreach ($serviceResult['result'] as $key => $result) {
                if($result['trip_id'] == $trip_id){
                    if(!empty($result['operator_name'])){
                        if(strtolower($result['operator_name']) == 'travelyaari'){
                            $travel_name =  $result['company_name'];
                        }else{
                            $travel_name =  $result['operator_name'];
                        }
                    }else{
                       $travel_name ='';
                    } 
                   $departure_time = date('H:i',  strtotime($result['departure_date']));
                   $arrival_time = date('H:i',  strtotime($result['arrival_date']));
                   $total_duration = timeDiff($result['departure_date'], $result['arrival_date']);
                    //for icons in arrival time
                    if($arrival_time >= "04:00:00" && $arrival_time <= "18:00:00"){
                        $arr_icon_class = 'fa-sun-o sun_color';
                    }else{
                        $arr_icon_class = 'fa-moon-o moon_color';
                    }

                    //for icons in departure time
                    if($departure_time >= "04:00:00" && $departure_time <= "18:00:00"){
                       $dep_icon_class = 'fa-sun-o sun_color';
                    }
                    else{
                       $dep_icon_class = 'fa-moon-o moon_color';
                    }
                    if($result['mticket_allowed']){
                        $class= 'active';
                        $tooltip_title = 'Mticket is allowed to board the bus.';
                    }else{
                        $class= '';
                       $tooltip_title = 'Mticket is NOT allowed to board the bus.';
                    }

                       $output .= '  
                       <div class="table-responsive">
                        <table class="table details_all">
                            <tr>
                                <th>Bus Operator</th>
                                <th>Departure</th>
                                <th>Duration</th>
                                <th>Arrival</th>
                                <th>mTicket</th>
                                <th>Seats</th>
                                <th>Fare</th>
                            </tr>
                            <tr>
                                <td class="operator" style="color: #01b7f2;font-size: 14px;font-weight: 700;">'.$travel_name.'</td>
                                <td class="departure">
                                    <span class="icon fa '. $dep_icon_class.'"></span><br/>
                                    <b class="time">'.$departure_time.'</b>
                                </td>
                                <td class="travel_duration">
                                    <span class="fa fa-arrow-right" style="color: #7db921;font-size: 14px;"></span><br/>
                                    <span class="total_time" style=" color: #7db921;font-size: 14px;">'.$total_duration.'</span>
                                </td>
                                <td class="arrival">
                                    <span class="icon fa '. $arr_icon_class.' "></span><br/>
                                    <b class="time">'.$arrival_time.'</b>
                                </td>
                                <td class="">
                                   <div class="amenities">
                                      <i class="fa fa-mobile circle'.$active.'" title="'.$tooltip_title.'"></i>
                                  </div>
                                </td>
                                <td class="seats">'.$result['available_seat'].'</td>
                                <td class="price" style="color: #01b7f2;font-size: 14px;font-weight: 700;">'.$result['seat_fare'].'</td>
                            </tr>
                        </table>
                    </div>

                    <hr>

                    <!-- tab container start-->
                    <div class="tab-container hide-tab-container full-width-style white-bg" style="text-align: center;">
                        
                            <a href="#pick-up-details" data-toggle="tab"><i class="soap-icon-passenger circle"></i>Pick Up</a>
                            <a href="#drop-off-details" data-toggle="tab"><i class="soap-icon-adventure circle"></i>Drop Off</a>
                            <a href="#cancellation-policy" data-toggle="tab"><i class="fa fa-exclamation-circle circle"></i>Policy</a>
                        
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="pick-up-details">
                                <div class="table-responsive">
                                    <table class="table pick-drop-policy" style="margin-top: 30px;">
                                        <thead>
                                            <tr>
                                                <th width="30%">Pickup Area</th>
                                                <th width="60%">Pickup Address</th>
                                                <th width="10%">Time</th>
                                            </tr>
                                        </thead>
                                        <tbody>';
                                        if(isset($result['boarding_stops']) && !empty($result['boarding_stops'])){
                                            foreach($result['boarding_stops'] as $pick_up){
                                                 $output .= '<tr  style="text-align: left"><td>'.$pick_up['location'].'</td><td>'.$pick_up['location'].'</td><td>'.$pick_up['time'].'</td></tr>';
                                             }
                                        }else{
                                             $output .= '<tr  style="text-align: left"><td>'.$result['from_stop'].'</td><td>N.A.</td><td>'.$departure_time.'</td></tr>';
                                        }
                                        
                                        $output .= '
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="drop-off-details">
                                <div class="table-responsive">
                                    <table class="table pick-drop-policy" style="width: 50%;border: 1px solid;margin-left: 232px;margin-top: 30px;">
                                        <thead>
                                            <tr>
                                                <th style="text-align: left">Drop Off Location</th>
                                                <th  style="text-align: left">Time</th>
                                            </tr>
                                        </thead>
                                       <tbody>';
                                      if(isset($result['dropping_stops']) && !empty($result['dropping_stops'])){
                                        foreach($result['dropping_stops'] as $drop_stop){
                                             $output .= '<tr style="text-align: left"><td>'.$drop_stop['location'].'</td><td>'.$drop_stop['time'].'</td></tr>';
                                        }
                                        }else{
                                            $output .= '<tr  style="text-align: left"><td>'.$result['to_stop'].'</td><td>N.A.</td><td>'.$arrival_time.'</td></tr>';
                                        }
                                  $output .= '</tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="cancellation-policy">
                                <div class="table-responsive">
                                    <table class="table pick-drop-policy" style="width: 50%;border: 1px solid;margin-left: 232px;margin-top: 30px;">
                                        <thead>
                                            <tr>
                                                <th  style="text-align: left">Cutoff Time</th>
                                                <th  style="text-align: left">Refund in Percentage</th>
                                            </tr>
                                        </thead>
                                        <tbody>';
                                        foreach($result['cancellation_policy'] as $cancel_policy){
                                             $output .= '<tr  style="text-align: left"><td>Cancellation cutoff <b>'.$cancel_policy['cutoffTime'].' hrs</b></td><td>'.$cancel_policy['refundInPercentage'].'%</td></tr>';
                                        }
                                    $output .= '</tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table pick-drop-policy-conditions hide">
                                        <thead>
                                            <tr>
                                                <th>Terms & Conditions</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> ';  
    //             }  
                 $output .= "</table></div>";  
                }
            }
             echo $output; 
        }  
     }
     
     public function get_cancel_policy() {
         if(isset($_POST["trip_id"])){  
            $output = '';  
            $arr_icon_class = '';
            $dep_icon_class = '';
            $trip_id = $_POST['trip_id'];
            $bus_travel_id = $_POST['bus_travel_id'];
            
            $details_array = array();
            $serviceResult = $this->session->userdata('serviceResult');
            
            foreach ($serviceResult['result'] as $key => $result) {
                if($result['trip_id'] == $trip_id){
                   
            $output .= '  
                <div class="ttp_ticket_cancellation_policy arrow_box">
                      <div class="table-responsive">
                        <table class="table font13">
                            <thead>
                              <tr>
                                <th>Cutoff Time</th>
                                <th>Refund in Percentage</th>
                              </tr>
                            </thead>
                            <tbody>';
                                foreach($result['cancellation_policy'] as $cancel_policy){
                                     $output .= '<tr  style="text-align: left"><td>Cancellation cutoff time <b>'.$cancel_policy['cutoffTime'].' hrs</b></td><td>'.$cancel_policy['refundInPercentage'].'%</td></tr>';
                                }
                            $output .= '</tbody>
                        </table>
                      </div>
                  </div>';
                    
                }
            }
             echo $output; 
        }  
     }
     
     function ntest() {
        $onwardJourny = $this->session->userdata('onwardJourny');
        show($onwardJourny,1);
        echo  $search_date = date("d_m_Y", strtotime($newonwardJourny['date']));
        echo $fromstopname = $this->session->userdata['bus_details']['from'];
        echo $tostopname = $this->session->userdata['bus_details']['to'];
        $file_name = strtolower($fromstopname) . "_" . strtolower($tostopname) . "_" . $search_date . '.json';
        $file_name = str_replace(' ', '', $file_name);
        $file_path = FCPATH . "json_data/";
        if (file_exists($file_path . $file_name)) {
            $json_data = file_get_contents($file_path . $file_name);
            $output_result = json_decode($json_data); 
            $data['result'] = $output_result['result'];
            $data['input_data'] = $output_result['input_data'];
            $json_data = json_encode($data); 
        }
        $data['getCancelPolicy'] = json_decode($json_data, true);
//        foreach ($data['getCancelPolicy'] as $key => $values) {
//
//        }
        die;
    }
    
    public function cancel_email($pnr_no,$flag=''){
        $ticketDetails = $passengerDetails = array();
        $ticketDetails = $this->Search_bus_model->getcancelTicketsDetails($pnr_no);

        if (empty($ticketDetails) && empty($ticketDetails[0]['pnr_no'])) {
            die('Your tickets can not view please contact with our support team!');
        }
        $ticketId = $ticketDetails[0]['ticket_id'];
        $passengerDetails = $this->Search_bus_model->getPassengerDetails($ticketId);
//        print_r($passengerDetails);
//        show($ticketDetails,1);
        
         if (!empty($passengerDetails)) {
            $psgr_no = 1;
            $reservationCharge = $totalBasicFare = 0;
            $passanger_detail = "";
            $adult_basic_fare = 0;
            $senior_basic_fare = 0;
            $child_basic_fare = 0;
            $seatList ="";
            foreach ($passengerDetails as $key => $value) {
                $reservationCharge += $value['fare_reservationCharge'];

                if ($value['psgr_age'] > 12 && $value['psgr_type'] == "A") {
                    $senior_basic_fare += $value['adult_basic_fare'];
                    $adult_child = "Adult";
                } else if ($value['psgr_age'] <= 12 && $value['psgr_type'] == "C") {
                    $child_basic_fare += $value['child_basic_fare'];
                    $adult_child = "Children";
                }

                $total_calculate_basic_fare = $adult_basic_fare + $senior_basic_fare + $child_basic_fare;
                // $adult_child = ($value->psgr_age > 12) ? "Adult" : "Children";
                $passanger_detail .= '<tr style="border:1px solid #000;">
                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px; background:#e0e0e0;">
                          ' . ucwords($value['psgr_name']) . '
                        </td>
                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                          ' . ucwords($value['psgr_age']) . '
                        </td>
                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                          ' . $adult_child . '
                        </td>
                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                          ' . ucwords($value['psgr_sex']) . '
                        </td>
                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                          ' . $value['seat_no'] . '
                        </td>
                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                          NA
                        </td>
                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                          NA
                        </td>
                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                          NA
                        </td>
                        <td align="left" valign="top" style="border:1px solid #000; padding-left:7px;">
                          NA
                        </td>
                      </tr>';
                $psgr_no++;
                $seatList = $seatList."".$value['seat_no'] .", ";
            }
            $pickup_address = $ticketDetails[0]['boarding_stop_name'];
            $pickup_address .= ($ticketDetails[0]['pickup_address'] != "") ? " (" . $ticketDetails[0]['pickup_address'] . ")" : "";
            if(trim(strtolower($ticketDetails[0]['op_name'])) == 'msrtc') {
                $totalBasicFare = ($ticketDetails[0]['total_basic_amount'] + $ticketDetails[0]['ac_service_tax']);
            } elseif(trim(strtolower($ticketDetails[0]['op_name'])) == 'upsrtc') {
                 $totalBasicFare = ($ticketDetails[0]['total_basic_with_ac'] + $ticketDetails[0]['ac_service_tax']) - $ticketDetails[0]['service_tax'] - $ticketDetails[0]['fare_convey_charge'];
            }
            $contactDetails = [];
            $contactJson = $ticketDetails[0]['pickup_address'];
            if(!empty($contactJson))
            {
                $contactDetails = json_decode($contactJson, true);
            }
         }
            
        $mail_replacement_array = array(
                "{{bos_ref_no}}" => $ticketDetails[0]['boss_ref_no'],
                "{{ticket_no}}" => $ticketDetails[0]['ticket_ref_no'],
                "{{new_pnr}}" => $ticketDetails[0]['new_pnr'],
                "{{user_email}}" => $ticketDetails[0]['user_email_id'],
                "{{mobile_number}}" => $ticketDetails[0]['mobile_no'],
                "{{trip_no}}" => $ticketDetails[0]['bus_service_no'],
                "{{ticket_no}}" => $ticketDetails[0]['pnr_no'],
                "{{route_name}}" => isset($ticketDetails[0]['route_no_name']) ? $ticketDetails[0]['route_no_name']:'',
                "{{from_stop_name}}" => strtoupper($ticketDetails[0]['from_stop_name']),
                "{{till_stop_name}}" => strtoupper($ticketDetails[0]['till_stop_name']),
                "{{boarding_stop_name}}" => strtoupper($ticketDetails[0]['boarding_stop_name']),
                "{{destination_stop_name}}" => strtoupper($ticketDetails[0]['destination_stop_name']),
                "{{dept_time_date}}" => date("d/m/Y", strtotime($ticketDetails[0]['dept_time'])),
                "{{dept_time_his}}" => date("H:i", strtotime($ticketDetails[0]['dept_time'])) . " Hrs",
                "{{approx_boarding_time}}" => date("H:i", strtotime($ticketDetails[0]['boarding_time'])) . " Hrs",
                "{{bus_type}}" => $ticketDetails[0]['bus_type'],
                "{{num_passgr}}" => $ticketDetails[0]['num_passgr'],
                "{{bus_types}}" => $ticketDetails[0]['bus_type'], //$service_detail["bus_type"],
                "{{tot_fare_amt}}" => "Rs. " . $ticketDetails[0]['tot_fare_amt_with_tax'], 
                "{{fare_reservationCharge}}" => "Rs. " . !empty($ticketDetails[0]['fare_reservationCharge']) ? $ticketDetails[0]['fare_reservationCharge']:0,
                "{{tot_fare_amt_with_tax}}" => "Rs. " . !empty($ticketDetails[0]['total_fare_without_discount'])? $ticketDetails[0]['total_fare_without_discount']:0,
                "{{asn_amount}}" => "Rs. " . !empty($ticketDetails[0]['asn_fare']) ? $ticketDetails[0]['asn_fare']:0 ,
                "{{ac_service_charges}}" => "Rs. " . !empty($ticketDetails[0]['ac_service_tax']) ? $ticketDetails[0]['ac_service_tax']:0,
                "{{passanger_details}}" => $passanger_detail,
                "{{no_of_seats}}" => $psgr_no - 1,
                "{{gst_charges}}" => "Rs. " . !empty($ticketDetails[0]['service_tax']) ? $ticketDetails[0]['service_tax']:0,
                "{{other_charges}}" => "Rs. " . !empty($ticketDetails[0]['fare_convey_charge']) ? $ticketDetails[0]['fare_convey_charge']:0,
                "{{booked_date}}" => date("jS F Y",strtotime($ticketDetails[0]['issue_time'])),
                "{{agent_details}}" => 'Travelyari',
                "{{from_to_journey}}" => strtoupper($ticketDetails[0]['from_stop_name']) . " To ".strtoupper($ticketDetails[0]['till_stop_name']),
                "{{journey_date}}" =>  date("l \- jS F Y",strtotime($ticketDetails[0]['dept_time'])),
                "{{op_name}}" => $ticketDetails[0]['op_name'],
                "{{seat_no}}" => $seatList,
                "{{refund_amt}}" => $ticketDetails[0]['refund_amt'],
                "{{cancellation_cahrge}}" => "Rs. " . !empty($ticketDetails[0]['cancel_charge']) ? $ticketDetails[0]['cancel_charge']:0,
            );
            
            /* * ********** Mail Start Here ******* */
            if(strtolower($ticketDetails[0]['booked_from']) == 'msrtc')
            {
                $data['mail_content'] = str_replace(array_keys($mail_replacement_array), array_values($mail_replacement_array), MSRTC_CANCEL_TICKET_TEMPLATE);
            } 

             $mailbody_array = array("subject" => "Rokad Ticket Cancelled  for ".$ticketDetails[0]['boss_ref_no'],"message" => $data['mail_content'],"to" => $ticketDetails[0]['user_email_id']);
            if(empty($flag) || $flag != 'pdf') {
                $mail_result = send_mail($mailbody_array);
                //****if agent booked tkt send mail to both agent as well as customer****//
                $email_agent = $ticketDetails[0]['agent_email_id'];
                if ($email_agent != "" && ($email_agent != $email)) {
                    $agent_mailbody_array = array("subject" => "Rokad Ticket Cancelled for ".$ticketDetails[0]['boss_ref_no'],
                        "message" => $data['mail_content'],
                        "to" => $email_agent
                    );
                    $agent_mail_result = send_mail($agent_mailbody_array);
                }
            }
    }
    
    public function objectToArray($o) {
        if (is_object($o)) {
            $o = get_object_vars($o);
        }
        if (is_array($o)) {
            return array_map(array($this,__METHOD__), $o);
        }else{
            return $o;
        }
    }
    
}
