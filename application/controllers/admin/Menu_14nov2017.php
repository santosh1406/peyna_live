<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of menu
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
class Menu extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();

        $this->load->model(array('menu_model'));
    }

    /*
     * @function    : menu_view
     * @param       : 
     * @detail      : all menu list.
     *                 
     */

    public function index() {

        $data["menu_array"] = $this->menu_model->select();
        load_back_view(MENU_VIEW, $data);
    }

    /*
     * @function    : menu_view
     * @param       : 
     * @detail      : all menu list.
     *                 
     */

    function get_list() {
        $this->datatables->select("m.id,m.display_name,mn.display_name as parent_name,m.link,m.class,m.record_status");
        $this->datatables->from("menu m");
        $this->datatables->join('menu mn', 'm.parent = mn.id', 'left');
        $this->datatables->group_by("m.id");

        $this->datatables->add_column('action', '<a href="javascript:void" class="edit_btn margin" ref="$1"> <i  class="glyphicon glyphicon-pencil"></i> </a> <a href="javascript:void" class="del_btn" ref="$1" data-ref="$2">  <i  class="glyphicon glyphicon-trash"></i> </a>', 'id,record_status');
        $data = $this->datatables->generate('json');

        echo $data;
    }

    /*
     * @function    : savemenu to add menu in database(Menu Table)
     * @param       : 
     * @detail      : to add menu in table.
     *                 
     */

    public function savemenu() {
        $append_msg = "";
        $istrue = true;
        $input = $this->input->post();

        $config = array(
            array('field' => 'menu_name', 'label' => 'Menu Name', 'rules' => 'trim|required', 'errors' => array('required' => 'Please enter menu name.')),
            array('field' => 'parent', 'label' => 'Parent', 'rules' => 'trim|required', 'errors' => array('required' => 'Please select parent.'))
        );

        if (form_validate_rules($config)) {

            $max_seq_no = $this->menu_model->find_seq_no($input["parent"]);
            if ($max_seq_no["seq_no"] != "") {
                $seq_no = $max_seq_no["seq_no"] + 1;
            } else {
                $seq_no = 1;
            }

            $where = "display_name = '" . $input['menu_name'] . "'";
            $menu_exists = $this->menu_model->where($where, "", false)->find_all();

            if (isset($input['id']) and $input['id'] != "" && $menu_exists) {

                // show($menu_exists ,1);
                $this->menu_model->id = $input['id'];
                $append_msg = "updated";

                // for comparing parent menu with previous one to update sequence number
                $menu_parent = $menu_exists[0]->parent;
                $this->menu_model->updated_by = $this->session->userdata("user_id");
                $this->menu_model->updated_date = date("Y-m-d H:i:s");
                if ($menu_parent != $input['parent']) {
                    $this->menu_model->seq_no = $seq_no;
                }
            }
                $this->menu_model->display_name = $input['menu_name'];
                $this->menu_model->link = $input['menu_link'];
                $this->menu_model->parent = $input['parent'];
                $this->menu_model->class = $input['menu_class'];
                $this->menu_model->created_by = $this->session->userdata("user_id");
                $id = $this->menu_model->save();
                if ($id > 0) {
                    $data["flag"] = '@#success#@';
                    $data["msg_type"] = 'success';
                    $data["msg"] = 'Menu ' . $append_msg . ' successfully.';
                } else {
                    $data["flag"] = '@#failed#@';
                    $data["msg_type"] = 'error';
                    $data["msg"] = 'Menu could not be ' . $append_msg . '.';
                }
        }
         else {
            $data["flag"] = '@#failed#@';
            $data["msg_type"] = 'error';
            $data["msg"] = 'Menu could not be added/updated without menu name and menu parent.';
        }

        data2json($data);
    }

    // end of savemenu



    /*
     * @function    : edit_menu
     * @param       : $id
     * @detail      : view edit menu.
     *                 
     */
    public function edit_menu() {
        $input = $this->input->post();

        $this->menu_model->id = $input["id"];
        $rdata = $this->menu_model->select();

        $data["flag"] = '@#success#@';
        $data["menu_data"] = $rdata;

        data2json($data);
    }

    // end of edit_menu


    /*
     * @function    : delete_menu
     * @param       : 
     * @detail      : delete menu.
     *                 
     */
    public function delete_menu() {
        $input = $this->input->post();
        $this->menu_model->id = $input["id"];
        $this->menu_model->record_status = $input["postyn"];
        $id = $this->menu_model->save();

        if ($id > 0) {
            $data["flag"] = '@#success#@';
            $this->session->set_flashdata('msg_type', 'success');
            $this->session->set_flashdata('msg', 'Menu deleted successfully.');
        } else {
            $data["flag"] = '@#failed#@';
            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'Menu could not be deleted.');
        }
        data2json($data);
    }

// end of delete_role
}

//end of menu class