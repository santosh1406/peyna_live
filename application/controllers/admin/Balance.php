<?php

//////////////////////////////////////////////////////////////////////////////////////////////////////
// Project            : Rokad
// Version            : 2.0
// CR ID              : 20233 - 3.1   
// Author             : Vijay Ghadi
// Created on         : 30-07-2018
//
// Srl.        Date        Modified By            Why & What
// 01    30-07-2018        <Vijay Ghadi>         1. To Show Wallet Balance using API
// Srl.        Date        Modified By            Why & What
// 02    29-10-2018        <Sonali Kamble>       2. To Show BOS Wallet Balance using API                                                                                                                                                            
//////////////////////////////////////////////////////////////////////////////////////////////////////


defined('BASEPATH') OR exit('No direct script access allowed');

class Balance extends CI_Controller {
    
    public $sessionData;
    public function __construct() {
        parent::__construct();
        is_logged_in();
        $this->sessionData['id'] = $this->session->userdata('user_id');
        $this->sessionData["username"] = $this->session->userdata('username');
        $this->sessionData["password"] = $this->session->userdata('password');
    }

    /**
     * @description - To get Cyberplat Wallet Balance
     * @param param
     * @return int array
     * @added by Vijay on 30-07-2018 For CR - 20233 - 3.1
     */
    public function cyberplate() {
        $ch = curl_init();
        $curl_url = base_url() . "rest_server/wallet_balance/cyberplate_balance";
        curl_setopt($ch, CURLOPT_URL, $curl_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $header = array(
            'Content-Type: multipart/form-data; charset=utf-8',
            'X-API-KEY:' . X_API_KEY . '',
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_USERPWD, $this->sessionData['username'] . ":" . $this->sessionData['password']);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        $response = curl_exec($ch);
        $output = json_decode($response,true);
        //show($output,'1');
        $this->data['status'] = $output['status'];
        $this->data['balance'] = $output['data'];
        $this->data['msg'] = $output['msg'];
        
        $this->session->set_flashdata('message', $output['msg']);
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'cyberplate_balance.log', $response, 'Web Recharge Response');
        load_back_view(WALLET_BALANCE_VIEW, $this->data);
    }

    /**

     * @description - To get BOS Wallet Balance
     * @param param
     * @return int array
     * @added by Sonali on 29-10-2018 
     */
    public function bos()
    {
        
            $input['email'] = 'ketang@bookonspot.com';

            if (!empty($input)) {

                $curl_url = base_url() . "rest_server/wallet_balance/bos"; 
                          
                $curlResult = curlForPostData_BOS($curl_url, $input, $this->sessionData);
                $output = json_decode($curlResult);
               
                $data['status'] = $output->data->status;              
                $data['balance'] =$output->data->data;  
                
                   
                    
                //echo json_encode($data);

                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'bos_balance.log', $curlResult, 'Bus Services');
                
                
                load_back_view(BOS_WALLET_BALANCE, $data);

            }

        }
                       
    /*
     * @description - To get JRI Wallet Balance
     * @param param
     * @return int array
     * @added by Gopal on 05-11-2018
     */
    public function jri() {
        $ch = curl_init();
        $curl_url = base_url() . "rest_server/wallet_balance/jri_balance";
        curl_setopt($ch, CURLOPT_URL, $curl_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $header = array(
            'Content-Type: multipart/form-data; charset=utf-8',
            'X-API-KEY:' . X_API_KEY . '',
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_USERPWD, $this->sessionData['username'] . ":" . $this->sessionData['password']);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        $response = curl_exec($ch);
        
        $output = json_decode($response, true);
        $this->data['status'] = $output['status'];
        $this->data['balance'] = $output['data']['Balance'];
        // show($this->data, 1);
        $this->session->set_flashdata('message', $output['msg']);
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'jri_balance.log', $response, 'Web Recharge Response');
        load_back_view(JRI_WALLET_BALANCE_VIEW, $this->data);

    }

}

?>