<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {


    public function __construct() {
        parent::__construct();
        is_logged_in();
        $this->load->model(array('users_model','api_balance_log_model', 'role_model', 'tickets_model', 'dashboard_model','cb_commission_distribution_model'));
    }

    public function index()
    {   
        $data                   = array();
        $data["analytics_data"] = array();
        $tickets_analytics      = array();
        $user_role_name         = $this->session->userdata("role_name");
        $user_role_id           = $this->session->userdata("role_id");
        $data["user_role_name"] = $user_role_name;
        $data["user_role_id"]   = $user_role_id;

        if  (	   ($this->session->userdata('role_id') == COMPANY_ROLE_ID)
        		|| ($this->session->userdata('role_id') == MASTER_DISTRIBUTOR_ROLE_ID)
                || ($this->session->userdata('role_id') == AREA_DISTRIBUTOR_ROLE_ID)
                || ($this->session->userdata('role_id') == DISTRIBUTOR
                || ($this->session->userdata('role_id') == RETAILER_ROLE_ID)        )
            )  
        {        	
            $temp_val = $this->all_entities_dashboard($data);
            $data['name'] = $temp_val;
            //show($data, 1);
        } 
        elseif($this->session->userdata('role_id') == SUPERADMIN_ROLE_ID || $this->session->userdata('role_id') == TRIMAX_ROLE_ID)
        { 
            $values                        = $this->admin_dashboard();
           
            $data['active_users']                  = (isset($values['usersdata']['0']['totalusersactive']))  ? ($values['usersdata']['0']['totalusersactive']) : "0"; ;
            $data['pending_users_data']            = (isset($values['pending_users_data']['0']['totaluserspending'])) ?  ($values['pending_users_data']['0']['totaluserspending']):"0";
            $data['kycpending']                    = (isset($values['pending_kyc']['0']['kycpending'])) ? ($values['pending_kyc']['0']['kycpending']) : "0";
            $data['ticket_report_success']         = (isset($values['total_tickets']['0']['ticketsucess'])) ? ($values['total_tickets']['0']['ticketsucess']) : "0";
            $data['total_tickets_amt']             = (isset($values['total_tickets_amt']['0']['total_tickets_amt_master'])) ? ($values['total_tickets_amt']['0']['total_tickets_amt_master']) : "0";
            $data['final_commission']              = (isset($values['final_commission']['0']['final_commission'])) ? ($values['final_commission']['0']['final_commission']): "0";
            $data['final_commission_chained']      = (isset($values['final_commission_chained']['0']['final_commission_chained'])) ? ($values['final_commission_chained']['0']['final_commission_chained']): "0";
        } 
        // show($data,1);
        load_back_view(BACK_VIEW.'admin/dashboard/dashboard',$data);
    }
    
    public function admin_dashboard()
    {
        $data                        = array();
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');
        $users_data                  = $this->users_model->active_user_list($session_level,$session_id);      
        $pending_users_data          = $this->users_model->pending_users_list($session_level,$session_id);
        $kyc                         = $this->users_model->kyc_pending_list($session_level,$session_id);
        $total_tickets               = $this->tickets_model->ticket_report_success();
        $total_tickets_amt           = $this->tickets_model->get_total_ticket_msrtc_amt();
        $commission_earned           = $this->tickets_model->commission_earned_today();
        $commission_earned_chained   = $this->tickets_model->commission_earned_chained();
        
        //show($total_tickets_amt, 1);
        if(empty($users_data['0']['totalusersactive']))
        {
            $users_data          = '0';
        }
        if(empty($pending_users_data['0']['totaluserspending']))
        {
            $pending_users_data  = '0';
        }
        if(empty($kyc['0']['kycpending']))
        {
            $kyc                 = '0';
        }
        if(empty($total_tickets['0']['ticketsucess']))
        {
            $total_tickets       =  '0';
        }
        if(empty($total_tickets_amt['0']['total_tickets_amt_master']))
        {
            $total_tickets_amt       =  '0';
        }
        if(empty($commission_earned['0']['final_commission']))
        {
            $commission_earned   = '0';
        }
        if(empty($commission_earned_chained['0']['final_commission_chained']))
        {
            $commission_earned_chained   = '0';
        }
        
        $data['usersdata']                   = $users_data;
        $data['pending_users_data']          = $pending_users_data;
        $data['pending_kyc']                 = $kyc;
        $data['total_tickets']               = $total_tickets;
        $data['total_tickets_amt']           = $total_tickets_amt;
        $data['final_commission']            = $commission_earned;
        $data['final_commission_chained']    = $commission_earned_chained;
        
        
        return $data;
    }
    
    public function all_entities_dashboard($data)
    {
        $user_id  = $this->session->userdata("user_id");
        $id = array();
        
        //show($data);
        
        if($data['user_role_id'] == COMPANY_ROLE_ID)
        {
        	$level    = COMPANY_LEVEL;
        	$column   = 'level_'.$level;
        	
        	//show($user_id);
        	
        	//show($column);
        	$result   = $this->users_model->get_users_under_master($column,$user_id);
        	if(is_array($result) && count($result) > 0)
        	{
        		$result_c = count($result);
        		$res      = $this->array_imp($result);
        	}
        	else
        	{
        		$result_c = '0';
        		$res      = '0';
        	}
        	
        	
        	$ticketdetails_total = $this->tickets_model->ticket_report_success_master($res);
        	//show($ticketdetails_total, 1);
        	if(is_array($ticketdetails_total) && count($ticketdetails_total) > 0)
        	{
        		$ticketdetails_total  = count($ticketdetails_total);
        	}
        	else
        	{
        		$ticketdetails_total = '0';
        	}
        	
        	$pending_kyc_total   = $this->users_model->pending_kyc_masters($res);
        	//show($pending_kyc_total);
        	if(is_array($pending_kyc_total) && count($pending_kyc_total) > 0)
        	{
        		$pending_kyc_total  = ($pending_kyc_total['0']['pendingkyc_masters']);
        	}
        	else
        	{
        		$pending_kyc_total = '0';
        	}
        	
        	$pending_activation = $this->users_model->pending_activation_masters($res);
        	//show($pending_activation);
        	if(is_array($pending_activation) && count($pending_activation) > 0)
        	{
        		$pending_activation  = ($pending_activation['0']['pending_activation_masters']);
        	}
        	else
        	{
        		$pending_activation = '0';
        	}
        	
        	// $commission_earned_masters = $this->tickets_model->commission_earned_masters($res,$column,$user_id);
        	// //show($pending_activation);
        	// if(is_array($commission_earned_masters) && count($commission_earned_masters) > 0)
        	// {
        	// 	$commission_earned_masters  = ($commission_earned_masters['0']['final_commission_masters']);
        	// }
        	// else
        	// {
        	// 	$commission_earned_masters = '0';
        	// }
        	
            $commission_earned_masters = $this->cb_commission_distribution_model->getDailyEarnedCommission($data['user_role_id'],$user_id);
           // show($commission_earned_masters);
            if(is_array($commission_earned_masters) && count($commission_earned_masters) > 0)
            {
                $commission_earned_masters  = $commission_earned_masters['0']['final_earned_commission'];
            }
            else
            {
                $commission_earned_masters = '0';
            }

        	// $total_transactn_amt = $this->tickets_model->total_transactn_amt($res);
            $total_transactn_amt = $this->tickets_model->get_total_ticket_msrtc_amt();
        	//show($pending_activation);
        	if(is_array($total_transactn_amt) && count($total_transactn_amt) > 0)
        	{
        		$total_transactn_amt  = ($total_transactn_amt['0']['total_tickets_amt_master']);
        	}
        	else
        	{
        		$total_transactn_amt = '0';
        	}
        	
        	// $network_transactn_commission = $this->tickets_model->network_transactn_commission($res,$column,$user_id);
        	// //show($pending_activation, 1);
        	// if(is_array($network_transactn_commission) && count($network_transactn_commission) > 0)
        	// {
        	// 	$network_transactn_commission  = ($network_transactn_commission['0']['network_commission_masters']);
        	// }
        	// else
        	// {
        	// 	$network_transactn_commission = '0';
        	// }

            $network_transactn_commission = $this->cb_commission_distribution_model->getChainCommissionData($data['user_role_id'],$user_id);
            //show($network_transactn_commission,1);
            if(is_array($network_transactn_commission) && count($network_transactn_commission) > 0)
            {
                $network_transactn_commission  = ($network_transactn_commission['0']['chain_commission_masters']);
            }
            else
            {
                $network_transactn_commission = '0';
            }
        	
        }
        elseif($data['user_role_id'] == MASTER_DISTRIBUTOR_ROLE_ID)
        {
            $level    = MASTER_DISTRIBUTOR_LEVEL;
            $column   = 'level_'.$level;
            $result   = $this->users_model->get_users_under_master($column,$user_id);
            if(is_array($result) && count($result) > 0)
            {
                $result_c = count($result);
                $res      = $this->array_imp($result);
            }
            else
            {
                $result_c = '0';
                $res      = '0';
            }
            $ticketdetails_total = $this->tickets_model->ticket_report_success_master($res);
            if(is_array($ticketdetails_total) && count($ticketdetails_total) > 0)
            {
                $ticketdetails_total  = count($ticketdetails_total);
            }
            else
            {
                $ticketdetails_total = '0';
            }
            
            $pending_kyc_total   = $this->users_model->pending_kyc_masters($res);
            //show($pending_kyc_total, 1);
            if(is_array($pending_kyc_total) && count($pending_kyc_total) > 0)
            {
                $pending_kyc_total  = ($pending_kyc_total['0']['pendingkyc_masters']);
            }
            else
            {
                $pending_kyc_total = '0';
            }
            
            $pending_activation = $this->users_model->pending_activation_masters($res);
            if(is_array($pending_activation) && count($pending_activation) > 0)
            {
                $pending_activation  = ($pending_activation['0']['pending_activation_masters']);
            }
            else
            {
                $pending_activation = '0';
            }
            
            // $commission_earned_masters = $this->tickets_model->commission_earned_masters($res,$column,$user_id);
            // if(is_array($commission_earned_masters) && count($commission_earned_masters) > 0)
            // {
            //     $commission_earned_masters  = ($commission_earned_masters['0']['final_commission_masters']);
            // }
            // else
            // {
            //     $commission_earned_masters = '0';
            // }

            $commission_earned_masters = $this->cb_commission_distribution_model->getDailyEarnedCommission($data['user_role_id'],$user_id);
            //show($commission_earned_masters);
            if(is_array($commission_earned_masters) && count($commission_earned_masters) > 0)
            {
                $commission_earned_masters  = $commission_earned_masters['0']['final_earned_commission'];
            }
            else
            {
                $commission_earned_masters = '0';
            }

            // $total_transactn_amt = $this->tickets_model->total_transactn_amt($res);
            $total_transactn_amt = $this->tickets_model->get_total_ticket_msrtc_amt();
            if(is_array($total_transactn_amt) && count($total_transactn_amt) > 0)
            {
                $total_transactn_amt  = ($total_transactn_amt['0']['total_tickets_amt_master']);
            }
            else
            {
                $total_transactn_amt = '0';
            }
            
            // $network_transactn_commission = $this->tickets_model->network_transactn_commission($res,$column,$user_id);
            // if(is_array($network_transactn_commission) && count($network_transactn_commission) > 0)
            // {
            //     $network_transactn_commission  = ($network_transactn_commission['0']['network_commission_masters']);
            // }
            // else
            // {
            //     $network_transactn_commission = '0';
            // }

            $network_transactn_commission = $this->cb_commission_distribution_model->getChainCommissionData($data['user_role_id'],$user_id);
            //show($network_transactn_commission,1);
            if(is_array($network_transactn_commission) && count($network_transactn_commission) > 0)
            {
                $network_transactn_commission  = ($network_transactn_commission['0']['chain_commission_masters']);
            }
            else
            {
                $network_transactn_commission = '0';
            }
            
        }
        elseif($data['user_role_id'] == AREA_DISTRIBUTOR_ROLE_ID)
        {
            $level    = AREA_DISTRIBUTOR_LEVEL;
            $column   = 'level_'.$level;
            $result   = $this->users_model->get_users_under_master($column,$user_id);
            if(is_array($result) && count($result) > 0)
            {
                $result_c = count($result);
                $res = $this->array_imp($result);

            }
            else
            {
                $result_c = '0';
                $res      = '0';
            }
            $ticketdetails_total = $this->tickets_model->ticket_report_success_master($res);
            if(is_array($ticketdetails_total) && count($ticketdetails_total) > 0)
            {
                $ticketdetails_total  = count($ticketdetails_total);
            }
            else
            {
                $ticketdetails_total = '0';
            }
            
            $pending_kyc_total   = $this->users_model->pending_kyc_masters($res);
            if(is_array($pending_kyc_total) && count($pending_kyc_total) > 0)
            {
                $pending_kyc_total  = ($pending_kyc_total['0']['pendingkyc_masters']);
            }
            else
            {
                $pending_kyc_total = '0';
            }
            
            $pending_activation = $this->users_model->pending_activation_masters($res);
            if(is_array($pending_activation) && count($pending_activation) > 0)
            {
                $pending_activation  = ($pending_activation['0']['pending_activation_masters']);
            }
            else
            {
                $pending_activation = '0';
            }
            
            // $commission_earned_masters = $this->tickets_model->commission_earned_masters($res,$column,$user_id);
            // if(is_array($commission_earned_masters) && count($commission_earned_masters) > 0)
            // {
            //     $commission_earned_masters  = ($commission_earned_masters['0']['final_commission_masters']);
            // }
            // else
            // {
            //     $commission_earned_masters = '0';
            // }

            $commission_earned_masters = $this->cb_commission_distribution_model->getDailyEarnedCommission($data['user_role_id'],$user_id);
            //show($commission_earned_masters);
            if(is_array($commission_earned_masters) && count($commission_earned_masters) > 0)
            {
                $commission_earned_masters  = $commission_earned_masters['0']['final_earned_commission'];
            }
            else
            {
                $commission_earned_masters = '0';
            }
            
            // $total_transactn_amt = $this->tickets_model->total_transactn_amt($res,$column,$user_id);
            $total_transactn_amt = $this->tickets_model->get_total_ticket_msrtc_amt();
            if(is_array($total_transactn_amt) && count($total_transactn_amt) > 0)
            {
                $total_transactn_amt  = ($total_transactn_amt['0']['total_tickets_amt_master']);
            }
            else
            {
                $total_transactn_amt = '0';
            }
            
            // $network_transactn_commission = $this->tickets_model->network_transactn_commission($res,$column,$user_id);
            // if(is_array($network_transactn_commission) && count($network_transactn_commission) > 0)
            // {
            //     $network_transactn_commission  = ($network_transactn_commission['0']['network_commission_masters']);
            // }
            // else
            // {
            //     $network_transactn_commission = '0';
            // }

             $network_transactn_commission = $this->cb_commission_distribution_model->getChainCommissionData($data['user_role_id'],$user_id);
            //show($network_transactn_commission,1);
            if(is_array($network_transactn_commission) && count($network_transactn_commission) > 0)
            {
                $network_transactn_commission  = ($network_transactn_commission['0']['chain_commission_masters']);
            }
            else
            {
                $network_transactn_commission = '0';
            }
            
        }
        elseif($data['user_role_id'] == DISTRIBUTOR)
        {
            $level  = POS_DISTRIBUTOR_LEVEL;
            $column = 'level_'.$level;
            $result = $this->users_model->get_users_under_master($column,$user_id);
            if(is_array($result) && count($result) > 0)
            {
                $result_c = count($result);
                $res = $this->array_imp($result);
            }
            else
            {
                $result_c = '0';
                $res      = '0';
            }
            $ticketdetails_total = $this->tickets_model->ticket_report_success_master($res);
            if(is_array($ticketdetails_total) && count($ticketdetails_total) > 0)
            {
                $ticketdetails_total  = count($ticketdetails_total);
            }
            else
            {
                $ticketdetails_total = '0';
            }
            
            $pending_kyc_total   = $this->users_model->pending_kyc_masters($res);
            if(is_array($pending_kyc_total) && count($pending_kyc_total) > 0)
            {
                $pending_kyc_total  = ($pending_kyc_total['0']['pendingkyc_masters']);
            }
            else
            {
                $pending_kyc_total = '0';
            }
            
            $pending_activation = $this->users_model->pending_activation_masters($res);
            if(is_array($pending_activation) && count($pending_activation) > 0)
            {
                $pending_activation  = ($pending_activation['0']['pending_activation_masters']);
            }
            else
            {
                $pending_activation = '0';
            }
            
            // $commission_earned_masters = $this->tickets_model->commission_earned_masters($res,$column,$user_id);
            // if(is_array($commission_earned_masters) && count($commission_earned_masters) > 0)
            // {
            //     $commission_earned_masters  = ($commission_earned_masters['0']['final_commission_masters']);
            // }
            // else
            // {
            //     $commission_earned_masters = '0';
            // }
            
            $commission_earned_masters = $this->cb_commission_distribution_model->getDailyEarnedCommission($data['user_role_id'],$user_id);
            //show($commission_earned_masters,1);
            if(is_array($commission_earned_masters) && count($commission_earned_masters) > 0)
            {
                $commission_earned_masters  = $commission_earned_masters['0']['final_earned_commission'];
            }
            else
            {
                $commission_earned_masters = '0';
            }

            // $total_transactn_amt = $this->tickets_model->total_transactn_amt($res,$column,$user_id);
            $total_transactn_amt = $this->tickets_model->get_total_ticket_msrtc_amt();
            if(is_array($total_transactn_amt) && count($total_transactn_amt) > 0)
            {
                $total_transactn_amt  = ($total_transactn_amt['0']['total_tickets_amt_master']);
            }
            else
            {
                $total_transactn_amt = '0';
            }
            
            // $network_transactn_commission = $this->tickets_model->network_transactn_commission($res,$column,$user_id);
            // if(is_array($network_transactn_commission) && count($network_transactn_commission) > 0)
            // {
            //     $network_transactn_commission  = ($network_transactn_commission['0']['network_commission_masters']);
            // }
            // else
            // {
            //     $network_transactn_commission = '0';
            // }

             $network_transactn_commission = $this->cb_commission_distribution_model->getChainCommissionData($data['user_role_id'],$user_id);
            //show($network_transactn_commission,1);
            if(is_array($network_transactn_commission) && count($network_transactn_commission) > 0)
            {
                $network_transactn_commission  = ($network_transactn_commission['0']['chain_commission_masters']);
            }
            else
            {
                $network_transactn_commission = '0';
            }
        }
        else if($data['user_role_id'] == RETAILER_ROLE_ID)
        {
            $level  = RETAILOR_LEVEL;
            $column = 'level_'.$level;
            $ticketdetails_total = $this->tickets_model->ticket_report_success_master($user_id);
            if(is_array($ticketdetails_total) && count($ticketdetails_total) > 0)
            {
                $ticketdetails_total  = count($ticketdetails_total);
            }
            else
            {
                $ticketdetails_total = '0';
            }
            
            // $commission_earned_masters = $this->tickets_model->commission_earned_masters($user_id,$column,$user_id);
            // if(is_array($commission_earned_masters) && count($commission_earned_masters) > 0)
            // {
            //     $commission_earned_masters  = ($commission_earned_masters['0']['final_commission_masters']);
            // }
            // else
            // {
            //     $commission_earned_masters = '0';
            // }

            $commission_earned_masters = $this->cb_commission_distribution_model->getDailyEarnedCommission($data['user_role_id'],$user_id);
            //show($commission_earned_masters,1);
            if(is_array($commission_earned_masters) && count($commission_earned_masters) > 0)
            {
                $commission_earned_masters  = $commission_earned_masters['0']['final_earned_commission'];
            }
            else
            {
                $commission_earned_masters = '0';
            }

            // $total_transactn_amt = $this->tickets_model->total_transactn_amt($user_id,$column,$user_id);
            $total_transactn_amt = $this->tickets_model->get_total_ticket_msrtc_amt();
            if(is_array($total_transactn_amt) && count($total_transactn_amt) > 0)
            {
                $total_transactn_amt  = ($total_transactn_amt['0']['total_tickets_amt_master']);
            }
            else
            {
                $total_transactn_amt = '0';
            }
        }
        //show($commission_earned_masters,1);
        $data['result_c']                      = $result_c;
        $data['ticketdetails_total']           = $ticketdetails_total;
        $data['pending_kyc_total']             = $pending_kyc_total;
        $data['pending_activation_total']      = $pending_activation;
        $data['commission_earned_masters']     = round($commission_earned_masters,2);
        $data['total_transactn_amt']           = $total_transactn_amt;
        $data['network_transactn_commission']  = round($network_transactn_commission,2);
       // show($data,1);
        return $data;
    }
    
    public function array_imp($result)
    {
        
        foreach($result as $value)
        {
            $id[] = $value['id'];
       
        }
        //$id =  implode(',', $id);
        
        return $id;
    }
    
    public function export_success_cancel_ticket()
    {
        $output_array = array();
        $input = $this->input->post();

        $image = $input["image"];
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $image = str_replace('[removed]', '', $image);
        $output_array["image3"] = $image;
        $image = base64_decode(trim($image));
        $output_array["image4"] = $image;
        $file = TMP_PATH.'export_success_cancel_ticket.png';
        file_put_contents($file, $image);

        eval(ADMIN_EMAIL_ARRAY);
        $mail_content = array(
                                "subject" => "Last 30 Days Ticket Report",
                                "message" => "Please find the attached image",
                                "to" => $admin_array,
                                "attachment" => TMP_PATH.'export_success_cancel_ticket.png'
                             );

        send_mail($mail_content);
    }
}