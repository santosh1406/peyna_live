<?php

// created by Shrasti Bansal on 31-jan-2019
defined('BASEPATH') OR exit('No direct script access allowed');

class Search_flight extends CI_Controller {

    public $sessionData;
    public $agent_email;
    public $agent_mobile_no;

    public function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('utilities_helper');
        $this->load->model('flight_model');

        /* access checks code start here  */
        $user_data = $this->session->userdata();
        $agent_id = $user_data['user_id'];
        $retailer_services = $this->retailer_service_mapping_model->getServiceByAgentId($agent_id);
        $service_array = array_column($retailer_services, 'service_id');

        if (!in_array(FlightService, $service_array) && false) {
            $this->session->set_flashdata('msg', 'No access to module');
            redirect(base_url() . 'admin/dashboard');
        }
        /* access checks code end here  */

        $this->sessionData['id'] = $this->session->userdata('user_id');
        $this->sessionData["username"] = $this->session->userdata('username');
        $this->sessionData["password"] = $this->session->userdata('password');
        $this->sessionData["email"] = $this->session->userdata('email');
        $this->sessionData["mobile_no"] = $this->session->userdata('mobile_no');
        $this->agent_mobile_no = $this->session->userdata('mobile_no');
        $this->agent_email = $this->session->userdata('email');

        $this->rokad_headers = json_decode(ROKAD_FLIGHT_CREDS, true);
    }

    function index() {
        if(!empty($this->session->userdata('bookingRequest'))){
            $this->session->unset_userdata('bookingRequest');   
        }
        load_back_view(FLIGHT_SEARCH_VIEW, $data);
    }

    function search() {

        load_back_view(ADVANCE_SEARCH);
    }

    public function flight_services() {

        if (empty($_POST['source']) || $_POST['source'] == '') {
            $_POST['source'] = $_POST['source_value'];
        }

        if (empty($_POST['destination']) || $_POST['destination'] == '') {
            $_POST['destination'] = $_POST['destination_value'];
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('source', 'From Stop', 'required');
        $this->form_validation->set_rules('destination', 'to stop', 'required');
        $this->form_validation->set_rules('departure_date', 'Departure Date', 'required');

        if ($this->form_validation->run() == FALSE) {
            load_back_view(FLIGHT_SEARCH_VIEW);
        } else {

            if (isset($_POST['source'])) {
                $paxCount = array("adt" => $_POST['adult'], "chd" => $_POST['child'], "inf" => $_POST['infant']);
                $departure_date = date("Y-m-d", strtotime($_POST['departure_date']));
                if (strtolower($_POST['triptype']) == 'round') {
                    $return_date = date("Y-m-d", strtotime($_POST['return_date']));
                    $sectorInfos = array(array("src" => array("code" => $_POST['source']), "dest" => array("code" => $_POST['destination']), "date" => $departure_date),
                        array("src" => array("code" => $_POST['source']), "dest" => array("code" => $_POST['destination']), "date" => $return_date));
                } else {
                    $sectorInfos = array(array("src" => array("code" => $_POST['source']), "dest" => array("code" => $_POST['destination']), "date" => $departure_date));
                }

                $input['sectorInfos'] = json_encode($sectorInfos);
                $input['paxCount'] = json_encode($paxCount);
                $input['route'] = $_POST['routingType'];
                $input['triptype'] = $_POST['triptype'];

                if (isset($_POST['airline'])) {
                    $prefAirlines = array(array("code" => $_POST['airline']));
                    $input['prefAirlines'] = json_encode($prefAirlines);
                }

                $input['class'] = $_POST['preferredClass'];
                $input['multiCity'] = 'false';
                $input['multiHop'] = 'false';

                $this->session->set_userdata('paxCount', $paxCount);

                if (!empty($input)) {

                    $curl_url = base_url() . "rest_server/search_flight/flight_services";
                    $file_path = FCPATH . "json_data/";
                    if (file_exists($file_path . $file_name) && false) {
                        $json_data = file_get_contents($file_path . $file_name);
                    } else {
                        $curlResult = curlForPostData_FLIGHT($curl_url, $input, $this->rokad_headers);
                        $output = json_decode($curlResult);
//                         echo '123';
                        //echo '<pre>'; print_r($output->data);
                        $data['result'] = $output->data->data;
                        $data['input_data'] = $input;
                        $json_data = json_encode($data);
                        if (strtolower($output->data->status) == 'success') {
                            //file_put_contents($file_path . $file_name, json_encode($data));
                            $data['serviceResult'] = json_decode($json_data, true);
                            if (strtolower($input['triptype']) == 'round') {
                                if ($data['serviceResult']['result']) {
                                    foreach ($data['serviceResult']['result']['onwardJourneys'] as $flightDetails) {
                                        $viewData['onwardJourneysDetails'][] = $flightDetails;
                                    }
                                    foreach ($data['serviceResult']['result']['returnJourneys'] as $flightDetails) {
                                        $viewData['returnJourneysDetails'][] = $flightDetails;
                                    }
                                    $viewData['onwardJourneyCount'] = count($viewData['onwardJourneysDetails']);
                                    $viewData['returnJourneyCount'] = count($viewData['returnJourneysDetails']);
                                }
                            } else {
                                if ($data['serviceResult']['result']) {
                                    foreach ($data['serviceResult']['result']['onwardJourneys'] as $flightDetails) {
                                        $viewData['onwardJourneysDetails'][] = $flightDetails;
                                    }
                                    $viewData['onwardJourneyCount'] = count($viewData['onwardJourneysDetails']);
                                }
                            }
                        } else {
                            $this->session->set_tempdata('message', trim($output->data->status), 3);
                        }
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/flight/' . 'flight_service_output.log', $curlResult, 'Flight Services Output');
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/flight/' . 'flight_service_output.log', $input, 'Flight Services_input');
                    }
                    $request_type = $this->input->post('request_type');

                    $this->session->set_userdata($input);
                    if ($request_type == 'ajax_req') {
                        $results = $this->load->view('adminlte/back/admin/air_and_hotel_booking/flight_services_view', $data, true);
                        $ajax_request = array('status' => 'success', 'results' => $results);
                        echo json_encode($ajax_request);
                    } else {
                        if (strtolower($input['triptype']) == 'round') {
//                            echo '<pre>';print_r($viewData['returnJourneysDetails']);
//                            echo '<pre>';print_r($viewData['returnJourneysDetails'][0]);die;
//                            show($viewData['returnJourneysDetails'][0],1);
                            load_back_view(ADVANCE_FLIGHT_SEARCH_VIEW_RETURN, $viewData);
                        } else {
                            load_back_view(ADVANCE_FLIGHT_SEARCH_VIEW, $viewData);
                        }
                    }
                }
            }
        }
    }

    public function flight_booking_form() {
        $data = array();
        $this->load->helper(array('smartcard_helper', 'common_helper'));
        $this->load->model('state_master_model');
        $data['states'] = $this->state_master_model->get_all_state();
        $this->load->model('Country_master_model');
        $this->load->model('city_master_model');
        $data['country'] = $this->Country_master_model->get_country();
        
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/flight/' . 'flight_review.log', $this->session->userdata('bookingRequest'), 'Flight bookingRequest');
        if(!empty($this->session->userdata('bookingRequest'))){
            $bookingRequest = $this->session->userdata('bookingRequest');
//            show($bookingRequest,1);
            if(!empty($bookingRequest['adt_title'])){
                foreach ($bookingRequest['adt_title'] as $key => $info) {
                   $age =  (date('Y',strtotime(date('Y-m-d'))) - date('Y',strtotime($bookingRequest['adt_dob'][$key])));
                    $data['adt_info'][] = array("title" => $bookingRequest['adt_title'][$key], "firstName" => $bookingRequest['adt_firstname'][$key], "lastName" => $bookingRequest['adt_lastname'][$key], "age" => $age, "dob" => date("d-m-Y", strtotime($bookingRequest['adt_dob'][$key])));
                }
            }
            if (!empty($bookingRequest['chd_title'])) {
                foreach ($bookingRequest['chd_title'] as $key => $info) {
                   $age =  (date('Y',strtotime(date('Y-m-d'))) - date('Y',strtotime($bookingRequest['chd_dob'][$key])));
                   $data['chd_info'][] = array("title" => $bookingRequest['chd_title'][$key], "firstName" => $bookingRequest['chd_firstname'][$key], "lastName" => $bookingRequest['chd_lastname'][$key], "age" => $age, "dob" => date("d-m-Y", strtotime($bookingRequest['chd_dob'][$key])));
                }
            }
            if (!empty($bookingRequest['inf_title'])) {
                foreach ($bookingRequest['inf_title'] as $key => $info) {
                   $age =  (date('Y',strtotime(date('Y-m-d'))) - date('Y',strtotime($bookingRequest['inf_dob'][$key])));
                   $data['inf_info'][] = array("title" => $bookingRequest['inf_title'][$key], "firstName" => $bookingRequest['inf_firstname'][$key], "lastName" => $bookingRequest['inf_lastname'][$key], "age" => $age, "dob" => date("d-m-Y", strtotime($bookingRequest['inf_dob'][$key])));
                }
            }
            $data['mobile_number'] = $bookingRequest['mobile_number'];
            $data['email'] = $bookingRequest['email'];
            $data['c_country'] = $bookingRequest['country'];
            $data['state'] = $bookingRequest['state'];
            $data['city'] = $this->city_master_model->get_city_by_id($bookingRequest['city']);
            $data['city_id'] = $bookingRequest['city'];
            $data['street'] = $bookingRequest['street'];
            $data['pincode'] = $bookingRequest['pincode'];
            
        }
//        echo '<pre>'; print_r($data);die;
        load_back_view(FLIGHT_BOOKING_FORM_VIEW, $data);
    }

    public function set_flight_key() {
        if (isset($_POST['key']) && !empty($_POST['key'])) {
            $this->session->set_userdata('flightKey', $_POST['key']);
        } else {
            $this->session->set_userdata('onwardflightKey', $_POST['onwardkey']);
            $this->session->set_userdata('returnflightKey', $_POST['returnkey']);
        }
        // print_r($_POST);
    }

    public function flight_review() {
        $input = array();

        if ($this->session->userdata('triptype') == 'round') {
            $keys = json_encode([$this->session->userdata('onwardflightKey'), $this->session->userdata('returnflightKey')]);
        } else {
            $keys = json_encode([$this->session->userdata('flightKey')]);
        }

        $post = $_POST;
        $this->session->set_userdata('bookingRequest', $_POST);

        $input['keys'] = $keys;
        $input['isSSRReq'] = 'true';
        $input['isExReq'] = 'false';
        $input['currency'] = 'INR';

        $curl_url = base_url() . "rest_server/search_flight/review";
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/flight/' . 'flight_review.log', $input, 'Flight review input');
        $curlResult = curlForPostData_FLIGHT($curl_url, $input, $this->rokad_headers);
        $output = json_decode($curlResult);
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/flight/' . 'flight_review.log', $output, 'Flight review $output');
        $data['result'] = $output->data->data;
        $data['input_data'] = $input;
        //print_r($output);exit;
        $json_data = json_encode($data);
        $this->session->set_userdata('itinKey', $output->data->data->itinKey);
        $this->session->set_userdata('amountToCharge', $output->data->data->fare->totalFare->total->amount);
        $viewData['amountToCharge'] = $output->data->data->fare->totalFare->total->amount;
        $data['bookingReview'] = json_decode($json_data, true);

        if ($data['bookingReview']['result']) {

            $viewData['reviewDetails'] = $data['bookingReview']['result'];
            $viewData['deliveryData'] = $post;
            $users_info = array();
        }

        // unset($_SESSION['flightKey']);
        load_back_view(FLIGHT_REVIEW_VIEW, $viewData);
    }

    public function flight_booking() {
        $bookingRequest = $this->session->userdata('bookingRequest');
        $this->load->helper(array('smartcard_helper', 'common_helper'));
        $request['amount'] = $this->session->userdata('amountToCharge');   //$input['amountToCharge'];
        $request['apploginuser'] = $this->sessionData['id'];
        $result = checkWalletTxn($request, $comments = "wallet deduction");
            
        if ($result == true) {
             //$departure_date = date("Y-m-d",strtotime($bookingRequest['departure_date']));
        $travellersData = array();

        if (!empty($bookingRequest['adt_title'])) {
            foreach ($bookingRequest['adt_title'] as $key => $info) {
                $age = ageCalculator(date("Y-m-d", strtotime($bookingRequest['adt_dob'][$key])));
                $travellersData[] = array("pType" => "adt", "title" => $bookingRequest['adt_title'][$key], "firstName" => $bookingRequest['adt_firstname'][$key], "lastName" => $bookingRequest['adt_lastname'][$key], "age" => $age, "dob" => date("Y-m-d", strtotime($bookingRequest['adt_dob'][$key])));
            }
        }
        if (!empty($bookingRequest['chd_title'])) {
            foreach ($bookingRequest['chd_title'] as $key => $info) {
                $age = ageCalculator(date("Y-m-d", strtotime($bookingRequest['chd_dob'][$key])));

                $travellersData[] = array("pType" => "chd", "title" => $bookingRequest['chd_title'][$key], "firstName" => $bookingRequest['chd_firstname'][$key], "lastName" => $bookingRequest['chd_lastname'][$key], "age" => $age, "dob" => date("Y-m-d", strtotime($bookingRequest['chd_dob'][$key])));
            }
        }
        if (!empty($bookingRequest['inf_title'])) {
            foreach ($bookingRequest['inf_title'] as $key => $info) {
                $age = ageCalculator(date("Y-m-d", strtotime($bookingRequest['inf_dob'][$key])));

                $travellersData[] = array("pType" => "inf", "title" => $bookingRequest['inf_title'][$key], "firstName" => $bookingRequest['inf_firstname'][$key], "lastName" => $bookingRequest['inf_lastname'][$key], "age" => $age, "dob" => date("Y-m-d", strtotime($bookingRequest['inf_dob'][$key])));
            }
        }

        /* this is not */
        $deliveryData = array("mobile" => $bookingRequest['mobile_number'], "email" => $bookingRequest['email'], "street" => $bookingRequest['street'], "city" => $bookingRequest['city_element'], "pincode" => $bookingRequest['pincode'], "state" => $bookingRequest['state_element'], "country" => $bookingRequest['country_element']);

//        $deliveryData = array("mobile" => '8447541340',"email" => 'mohd.rashid@sts.in',"street"=> 'test',"city" => 'Mumbai',"pincode" => '400053',"state" => 'maharashtra ',"country" => 'india');
//       
        $input['itinKey'] = $this->session->userdata('itinKey');
        $input['amountToCharge'] = $this->session->userdata('amountToCharge');
        $input['orderType'] = 'false';
        $input['travellersData'] = json_encode($travellersData);
        $input['deliveryData'] = json_encode($deliveryData);
        $deliveryData = json_encode($deliveryData);

        $curl_url = base_url() . "rest_server/search_flight/book";
        $curlResult = curlForPostData_FLIGHT($curl_url, $input, $this->rokad_headers);
        $output = json_decode($curlResult);
        //$_SESSION['test']=$output;
        //   print_r($output);
        if (strtolower($output->data->status) != 'failed') {
            if ($output->data->data->refId) {
                $retrieve_data = $this->getretrive_book($output->data->data->refId);
                $retrieve_data = json_decode($retrieve_data);
                if (isset($retrieve_data->data->data->orderId) && $retrieve_data->data->data->orderId != "") {

                    $data = $retrieve_data->data->data;
                    $result = $this->flight_model->insert_flight_passenger($data, json_decode($deliveryData));
                    if ($result == true) {
                        $request['amount'] = $input['amountToCharge'];
                        $request['apploginuser'] = $this->sessionData['id'];
                        $response = updateWalletTxn($request, "Flight book transaction");
			if(!empty($response) && $response =='failure'){
				$this->session->unset_userdata('bookingRequest');
	                        $this->session->set_flashdata('msg', 'Insufficient wallet balance');
	                        echo "NotBooked";				
			}else{
				$this->session->unset_userdata('bookingRequest');
	                        $this->session->set_flashdata('msg', 'Flight booking successful');
	                        echo "confirmed";
			}
                        exit;
                    }
                }
            }
        } else {
            $this->session->unset_userdata('bookingRequest');
            if(!empty($output->data->data->detail)){
                $error = $output->data->data->detail;
                $this->session->set_flashdata('msg', $error);
            }else{
                $this->session->set_flashdata('msg', 'Error occurred during booking, please try again');
            }
            echo "NotBooked";
            exit;
        }
        }else{
             $this->session->unset_userdata('bookingRequest');
            $this->session->set_flashdata('msg', 'Insufficient wallet balance');
            echo "NotBooked";	
    }

    }

    public function airline_list() {
        $input = array();
        $carrierArr = [];
        $airlineArr = [];
        $flightCarriers = [];
        $departure_date = date("Y-m-d", strtotime($_POST['departure_date']));
        $paxCount = array("adt" => $_POST['adult'], "chd" => $_POST['child'], "inf" => $_POST['infant']);
        $sectorInfos = array(array("src" => array("code" => $_POST['source']), "dest" => array("code" => $_POST['destination']), "date" => $departure_date));
        $input['sectorInfos'] = json_encode($sectorInfos);
        $input['paxCount'] = json_encode($paxCount);
        $input['route'] = $_POST['routingType'];
        $input['class'] = $_POST['preferredClass'];
        $input['multiCity'] = 'false';
        $input['multiHop'] = 'false';



        $curl_url = base_url() . "rest_server/search_flight/airline_list";
        $curlResult = curlForPostData_FLIGHT($curl_url, $input, $this->rokad_headers);

        $output = json_decode($curlResult);

        if (isset($output->data->data->searchList)) {
            $airlineList = $output->data->data->searchList;

            for ($i = 0; $i < count($airlineList); $i++) {
                $carrierArr[$i] = $airlineList[$i]->carriers;
            }

            for ($i = 0; $i < count($carrierArr); $i++) {
                for ($j = 0; $j < count($carrierArr[$i]); $j++) {
                    $airlineArr["code"] = $carrierArr[$i][$j]->code;
                    $airlineArr["name"] = $carrierArr[$i][$j]->name;
                    $flightCarriers["carriers"][] = $airlineArr;
                }
            }


            $flightCarriers["carriers"] = array_map("unserialize", array_unique(array_map("serialize", $flightCarriers["carriers"])));

            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($flightCarriers));
        } else {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output($curlResult);
        }
    }

    public function getretrive_book($refId = "") {

        if (isset($refId)) {
            $input['referenceId'] = $refId;
            $curl_url = base_url() . "rest_server/search_flight/book_details";
            $curlResult = curlForPostData_FLIGHT($curl_url, $input, $this->rokad_headers);
            log_data("flight/retrieve.log", $curlResult, 'Retrieve Response');
            return $curlResult;
        }
    }

    public function fare_rule() {
        $input = array();
        $input['keys'] = '57d27aea-adbe-4601-b643-7877dc4645de@@$101|BOM|DEL|345|2|1550769300|1550777700||false|ZO9RBINX|_A1|0|0_O'; //$_POST['keys'] ;

        $input['keys'] = $_POST['keys'];
        $curl_url = base_url() . "rest_server/search_flight/fare_rule";
        $curlResult = curlForPostData_FLIGHT($curl_url, $input, $this->rokad_headers);
        $output = json_decode($curlResult);
        $data['additionalTextData'] = $output->data->data->fareRules[0]->fareRule;
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    // added by Lilawati karale on 21-02-2019
    public function get_city() {
        if (isset($_GET['term'])) {
            $input['term'] = strtolower($_GET['term']);

            if (!empty($input)) {
                $aRes = $this->flight_model->getCities($input['term']);
                $result = json_encode($aRes);
                echo $result;
            }
        }
    }

}
