<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Session_check extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

    }
    
    public function check_session()
	{
		$SessionID = $this->session->userdata('session_id');
	    $UserID = $this->session->userdata('user_id');

		if($UserID){
	    	$Flag = checkUserAlreadyLoggedIn($SessionID,$UserID);
			if($Flag == 1){
				$this->session->sess_destroy();
				unset($_SESSION);
				$this->output->clear_all_cache();
				if ($this->session->userdata('user_id') == "") {
	       			$data = array("response" => "false");
				}
			}else{
				$data = array("response" => "true");
			}
		}else{
   			$data = array("response" => "false");
		}

   		echo json_encode($data);
    }
}