<?php

class Role_menu_rel extends MY_Controller {

    public function __construct() {
        parent::__construct();

        is_logged_in();
        $rname = $this->session->userdata("role_name");
        $this->load->model(array('role_menu_rel_model', 'role_model', 'access_permission_model', 'menu_model'));
    }

    /*
     * @function    : permission_view
     * @param       : 
     * @detail      : all permission list.
     *                 
     */

    public function index() {

        $this->role_model->record_status = "Y";
        $rolename = $this->role_model->select();

        $data["permission_array"] = "";
        $data["rolename"] = $rolename;
        $data["menu_data"] = $this->menu_model->get_menu();
        load_back_view(ROLE_MENU_REL_VIEW, $data);
    }

    /*
     * @function    : get_role_permissions
     * @param       : 
     * @detail      : to role permissions.
     *                 
     */

    public function get_role_permissions($role_id) {

        $this->role_model->record_status = "Y";
        $rolename = $this->role_model->select();
        $permission_array = $this->access_permission_model->get_menu_permission($role_id);
        $data["permission_array"] = $permission_array;
        $data["rolename"] = $rolename;
        $data["role_id"] = $role_id;
        $data["menu_data"] = $this->menu_model->get_menu();
        load_back_view(ROLE_MENU_REL_VIEW, $data);
    }

    /*
     * @function    : set_role_permissions
     * @param       : 
     * @detail      : to set role permissions.
     *                 
     */

    public function set_role_permissions() {

        $input   = $this->input->post();
        $type    = $input["type"];
        $role_id = $input["role"];
        $menu_id = $input["menu"];
        //show($input,1);
        $checkboxvalue = $input["checkboxvalue"];
        $action = (($checkboxvalue == "Y") ? "granted" : (($checkboxvalue == "N") ? "revoked" : ("")));
       
        $this->role_menu_rel_model->role_id = $role_id;
        $this->role_menu_rel_model->menu_id = $menu_id;
        $role_menu_rel = $this->role_menu_rel_model->select();
        
        if (count($role_menu_rel) > 0) {
            $this->role_menu_rel_model->id = $role_menu_rel[0]->id;
            $this->role_menu_rel_model->$type = $checkboxvalue;

            if (strtolower($checkboxvalue) == "n" and $type == "view") {

                /***to get all child and subchild of parent upto n level**** */
                $all_menu = $this->menu_model->get_menu();
                $allmenu = array('items' => array(), 'parents' => array());
                foreach ($all_menu as $menu_key => $menu_value) {
                    // create parent menu relation
                    $allmenu['items'][$menu_value['menu_id']] = $menu_value;
                    $allmenu['parents'][$menu_value['parent']][] = $menu_value['menu_id'];
                }

                $childrens = $this->get_child_hierarchy($menu_id, $allmenu);
                $childrens .= $menu_id;
                $childrens = trim($childrens, ",");
                $hierarchy = explode(",", $childrens);
                if ($this->role_menu_rel_model->delete_role_menu_rel($role_id, $hierarchy)) {
                    $data["msg_type"] = "success";
                    $data["msg"] = "Permission revoked successfully";
                } else {
                    $data["msg_type"] = "error";
                    $data["msg"] = "Permission could not be revoked";
                }
                data2json($data); // Change this after discussion.
                /*                 * ********************************************************** */
            }
        } else {
            $this->role_menu_rel_model->role_id = $role_id;
            $this->role_menu_rel_model->menu_id = $menu_id;
            $this->role_menu_rel_model->$type   = $checkboxvalue;

            $sub_menu = $this->menu_model->where('parent',$menu_id)->find_all();
            if($sub_menu !='')
            {

                for($i = 0;$i < count($sub_menu); $i++)
                {
                     $menu_id_role = $this->role_menu_rel_model->where('role_id',$role_id)->find_by('menu_id',$sub_menu[$i]->id);
                     if(empty($menu_id_role))
                     { 
                       $submenu_details = $this->db->insert('role_menu_rel', 
                                           array( "role_id"       => $role_id,
                                                  "menu_id"       => $sub_menu[$i]->id,
                                                  "view"          => 'Y',
                                                  "created_by"    => $this->session->userdata("user_id"),
                                                  "record_status" => 'Y'));
                     }
                     else
                     {
                          $data1 = array(
                                         'view' => 'Y'
                                       );
                          $this->db->where('role_id', $role_id);
                          $this->db->where('menu_id', $sub_menu[$i]->id);
                          $this->db->update('role_menu_rel', $data1); 
                     }
                }
            }
        }
 
      $this->role_menu_rel_model->created_by = $this->session->userdata("user_id");
      $id = $this->role_menu_rel_model->save();
       if ($id > 0) {
            $data["msg_type"] = "success";
            $data["msg"] = "Permission " . $action . " successfully";
        } else {
            $data["msg_type"] = "error";
            $data["msg"] = "Permission could not be " . $action;
        }

        data2json($data);
    }

    /*
     * @function    : get_child_hierarchy
     * @param       : $parent_id -> id of parent
     *                $menu -> all menu data
     * @detail      : child and sub-child upto N-LEVEL of $parent_id will be returned.
     *                 
     */

    public function get_child_hierarchy($parent_id, $menu) {

        $childrens = "";
        if (isset($menu['parents'][$parent_id])) {
            foreach ($menu['parents'][$parent_id] as $child_id) {
                $childrens .= $child_id . ",";
                if (array_key_exists($child_id, $menu['parents'])) {
                    $childrens .= $this->get_child_hierarchy($child_id, $menu);
                }
            }
            return $childrens;
        }
    }

    /*
     * @function    : menu_sequence
     * @param       : 
     * @detail      : menu sequence page
     *                 
     */

    public function menu_sequence() {

        $data["menu_parent"] = $this->menu_model->get_parent_menu();
        load_back_view(MENU_SEQUENCE_VIEW, $data);
    }
    
    
     /*
     * @function    : menu_resequence
     * @param       : 
     * @detail      : menu re sequence
     *                 
     */

    public function menu_resequence() {
        
        $input = $this->input->post();
        $new_sequence = $input["new_sequence"];
        $sequence = explode(",",$new_sequence);
        $sequence_update = array();
        $seq = 0;
        $is_check = 0;
        $this->db->trans_begin();
        foreach($sequence as $menu_id)
        {
            $seq++;
            // created for batch update
            //$sequence_update[$index] = array("id"=>$menu_id, "seq_no"=>$seq);
            $this->menu_model->id = $menu_id;
            $this->menu_model->seq_no = $seq;
            $id = $this->menu_model->save();
            if($id > 0)
            {
                $is_check++;
            }
        }
        
        if(count($sequence) == $is_check)
        {
            $this->db->trans_commit();
            echo "success";
        }
        else
        {
            $this->db->trans_rollback();
            echo "error";
        }
    }
    public function select_all_menu()
    {
        $input = $this->input->post();
        $role_id = $input["role"];
        $delete_all_menu = $this->role_menu_rel_model->delete_all_menu_rel($role_id);
        $get_menu = $this->menu_model->get_all_menu_role_id();
        
        $insert_batch = array();
        if($delete_all_menu)
        {
              for($i = 0;$i < count($get_menu); $i++)
              {
                $insert_batch[] = array( "role_id"       => $role_id,
                                            "menu_id"       => $get_menu[$i]['id'],
                                            "view"          => 'Y',
                                            "add"           => 'Y',
                                            "edit"          => 'Y',
                                            "delete"        => 'Y',
                                            "export"        => 'Y',
                                            "pdf"           => 'Y',
                                            "approve"       => 'Y',
                                            "created_by"    => $this->session->userdata("user_id"),
                                            "record_status" => 'Y');
              }
              $insert_menu_details = $this->db->insert_batch('role_menu_rel',$insert_batch);
              if($insert_menu_details)
              {
                 $data["msg_type"] = "success";
                 $data["msg"] = "Permissions Granted successfully";
              }
              else
              {
                $data["msg_type"] = "error";
                $data["msg"] = "Permission Not Granted successfully";
              }
         }
         else
         {
                $data["msg_type"] = "error";
                $data["msg"] = "Something Went Wrong";
         }
         data2json($data);
    }
    public function remove_all_permission()
    {
        $input = $this->input->post();
        $role_id = $input["role"];
        $revoke_all_menu = $this->role_menu_rel_model->delete_all_menu_rel($role_id);
        if($revoke_all_menu)
        {
            $data["msg_type"] = "success";
            $data["msg"]      = "Permissions revoked Successfully";
        }
        else
        {
            $data["msg_type"] = "success";
            $data["msg"]      = "Something Went Wrong";
        }
        data2json($data);
    }
    public function all_per_per_menu()
    {
     $input= $this->input->post();
     $parent_id = $input["menu"];
     $role_id = $input["role"];
     $menu_id_role = $this->role_menu_rel_model->where('role_id',$role_id)->find_by('menu_id',$parent_id);
     
     if(empty($menu_id_role))
     {   
         $insert_menu_details = $this->db->insert('role_menu_rel', 
                            array( "role_id"       => $role_id,"menu_id"  => $parent_id,
                                   "view"          => 'Y',"add"           => 'Y',
                                   "edit"          => 'Y',"delete"        => 'Y',
                                   "export"        => 'Y',"pdf"           => 'Y',
                                   "approve"       => 'Y',
                                   "created_by"    => $this->session->userdata("user_id"),
                                   "record_status" => 'Y'));
     }
     else
     {
        $data1 = array(    "view"          => 'Y',"add"           => 'Y',
                           "edit"          => 'Y',"delete"        => 'Y',
                           "export"        => 'Y',"pdf"           => 'Y',
                           "approve"       => 'Y'
                      );
        $this->db->where('role_id', $role_id);
        $this->db->where('menu_id', $parent_id);
        $update_menu = $this->db->update('role_menu_rel', $data1);
     }
     $get_parents_submenu = $this->menu_model->where('parent',$parent_id)->find_all();
     if(!empty($get_parents_submenu))
     {
        for($i = 0;$i < count($get_parents_submenu); $i++)
        {
             $menu_id_role = $this->role_menu_rel_model->where('role_id',$role_id)->find_by('menu_id',$get_parents_submenu[$i]->id);
             if(empty($menu_id_role))
             { 
               $insert_submenu = $this->db->insert('role_menu_rel', 
                                   array( "role_id"       => $role_id,
                                          "menu_id"       => $get_parents_submenu[$i]->id,
                                          "view"          => 'Y',"add"          => 'Y',
                                          "delete"        => 'Y',"edit"         => 'Y',
                                          "export"        => 'Y',"pdf"          => 'Y',
                                          "approve"       => 'Y',
                                          "created_by"    => $this->session->userdata("user_id"),
                                          "record_status" => 'Y'));
             }
             else
             {
                  $data1 = array(
                                 "view"          => 'Y',"add"           => 'Y',
                                 "edit"          => 'Y',"delete"        => 'Y',
                                 "export"        => 'Y',"pdf"           => 'Y',
                                 "approve"  => 'Y'
                               );
                  $this->db->where('role_id', $role_id);
                  $this->db->where('menu_id', $get_parents_submenu[$i]->id);
                  $this->db->update('role_menu_rel', $data1); 
             }
          }
       }
    
      $data["msg_type"] = "success";
      $data["msg"]      = "Permissions Granted Successfully";
      
      data2json($data);
      
    }
    public function remove_per_per_menu()
    {
      $input= $this->input->post();
      $role_id = $input["role"];
      $parent_id = $input["menu"];
      
      $revoke_menu_permission = $this->role_menu_rel_model->delete_menu_rel_permission($role_id,$parent_id);
      
      $get_child = $this->menu_model->where('parent',$parent_id)->find_all();
      if(!empty($get_child))
      {
           for($i = 0;$i < count($get_child); $i++)
         {
             $delete_submenu = $this->db->delete('role_menu_rel', array('menu_id' => $get_child[$i]->id));
          }
      }
      if($revoke_menu_permission && $delete_submenu)
      {
        $data["msg_type"] = "success";
        $data["msg"]      = "Permissions revoked Successfully";
      }
      else
      {
        $data["msg_type"] = "success";
        $data["msg"]      = "Permission Could Not be revoked";
      }
     data2json($data);
  }

}

//end of Role_menu_rel class