<?php

// created by sonali on 1-march-2019
use Blocktrail\CryptoJSAES\CryptoJSAES;

require APPPATH . '/libraries/CryptoJSAES.php';

class Aeps extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('Pdf');
        $this->load->helper('common_helper');

        is_logged_in();
        $this->sessionData["id"] = $this->session->userdata('user_id');
        $this->sessionData["username"] = $this->session->userdata('username');
        $this->sessionData["password"] = $this->session->userdata('password');

        $this->header = array(
            'Content-Type: multipart/form-data; charset=utf-8',
            'X-API-KEY: ' . X_API_KEY . '',
        );

        $this->load->model(array('aeps_transaction_model', 'wallet_model', 'Utilities_model', 'retailer_service_mapping_model', 'wallet_trans_model', 'users_model','common_model'));
        $this->load->helper(array('epaylater_helper'));

        $agent_id = $this->sessionData["id"];
        $retailer_services = $this->retailer_service_mapping_model->getServiceByAgentId($agent_id);
        $service_array = array_column($retailer_services, 'service_id');

        if (!in_array(AEPS_SERVICE_ID, $service_array)) {
            /* Redirect to Dashboard if agent has not enabled the service. */
            $this->session->set_flashdata('msg', 'No access to module');
            redirect(base_url() . 'admin/dashboard');
        } else {
            
        }
    }

    public function cash_withdraw() {
        $data['SERVICEID'] = AEPS_CASH_WITHDRAW;
        load_back_view(AEPS_CASH_WITHDRAW_VIEW, $data);
    }

    public function transaction_process() {
        $data = array();
        $input = $this->input->post();

        if (!empty($input)) {
            $input['user_id'] = $this->sessionData['id'];

            $config = array(
                array('field' => 'ClientUniqueID', 'label' => 'ClientUniqueID', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Transaction Number.')),
                array('field' => 'Amount', 'label' => 'Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter Amount.')),
            );

            if (form_validate_rules($config) == FALSE) {
                $data['error'] = $this->form_validation->error_array();
                load_back_view(AEPS_CASH_WITHDRAW_VIEW, $data);
            } else {
                $serive_id = trim($input['service_id']);
                if ($serive_id == AEPS_CASH_WITHDRAW) {


                    $chk_data = array(
                        'user_id' => $this->sessionData["id"],
                        'cash_withdraw' => AEPS_COMMISSION_NAME,
                        'withdraw_amount' => trim($input['Amount']),
                        'type' => 'AEPS'
                    );
                    $check_user_wallet = $this->Utilities_model->getUserWalletDetails($this->sessionData["id"]);
                    //if ($check_user_wallet[0]['amt'] >= trim($input['Amount'])) {

                        /* Check if commission available - */
                        $commissionResult = $this->Utilities_model->commission_from($this->sessionData["id"]);
                        //show($commissionResult,1);
                        $created_by = $commissionResult[0]['level_3'];
                        $check_if_commission_available = $this->checkCommission($chk_data, $created_by);
                        //show($check_if_commission_available,1);
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/aeps/' . 'aeps_check_commssion_available.log', $check_if_commission_available, 'check_commssion_available');
                        //show($check_if_commission_available);

                        if (count($check_if_commission_available) == 0) {
                            $this->session->set_tempdata(array('status' => 'Failed', 'data' => $check_if_commission_available, 'msg' => 'Commission not set. Please contact your administrator.'), 200);
                            redirect(base_url() . "admin/aeps/cash_withdraw");
                        }

                        /* Check if commission available - */
//                    } else {
//                        $this->session->set_tempdata(array('status' => 'Failed', 'msg' => 'Wallet has insufficient balance'), 200);
//                        redirect(base_url() . "admin/aeps/cash_withdraw");
//                    }
                }
                //$header_key = "982b0d01-b262-4ece-a2a2-45be82212ba1";
                //$body_key = "8329fffc-b192-4c9b-80cf-ccbbdf48bd67";

                $header_key = AEPS_HEADER_KEY;
                $body_key = AEPS_BODY_KEY;

                //$client_unique_id = $input['user_id'] . 'M' . time();

                $client_ref_id = $input['ClientUniqueID'];

                $data['MerchantId'] = AEPS_MERCHANT_ID;
                $data['SERVICEID'] = trim($input['service_id']);
                $data['RETURNURL'] = base_url() . "admin/aeps/transaction_response";
                $data['Version'] = AEPS_VERSION_NO;
                $data['Amount'] = trim($input['Amount']);
                $data['ClientRefID'] = trim($client_ref_id);

                $this->aeps_transaction_model->insert_data('aeps_request', $data);

                $res = json_encode($data);
                $encr_res = $this->encrypt_aeps($res, $body_key);

                $credentials = $this->encrypt_aeps('{"ClientId":"' . AEPS_CLIENT_ID . '","AuthKey":"' . AEPS_AUTH_KEY . '"}', $header_key);

                $header = array(
                    'encData' => $encr_res,
                    'authentication' => $credentials,
                );

                load_back_view(AEPS_TRANSACTION_VIEW, $header);
            }
        }
    }

    public function transaction_response() {
        //get last inserted id of aeps_request table user wise
        $aeps_last_trans_id = $this->aeps_transaction_model->get_last_trans_id();
        $aeps_inserted_data = array();
        $input = $this->input->post();
        $data = array();

        $body_key = AEPS_BODY_KEY;
//        show($input);
//          die('jii');
        if (!empty($input)) {
            $wallet_txn_no = substr(hexdec(uniqid()), 4, 12);
            $aeps_response = json_decode($input['Response']);
//            echo '<pre>';
//            print_r($aeps_response);
            //show($aeps_response,1);
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/aeps/' . 'aeps.log', $aeps_response, 'Cash withdraw Response');

            $client_response = $this->decrypt_aeps($aeps_response->ClientRes, $body_key);
            $client_decoded_response = json_decode($client_response);
//            echo '<pre>';
//            print_r($client_decoded_response);
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/aeps/' . 'aeps.log', $client_decoded_response, 'Decrypted Client Response');
            //show($client_decoded_response,1);
            $txnDate = str_replace('/', '-', $client_decoded_response->TxnDate);
            $aeps_inserted_data = array(
                'ClientRefID' => $aeps_response->ClientRefID,
                'DispalyMessage' => $aeps_response->DisplayMessage,
                'Amount' => $client_decoded_response->Amount ? $client_decoded_response->Amount : '0',
                'AdhaarNo' => $client_decoded_response->AdhaarNo ? $client_decoded_response->AdhaarNo : ' ',
                'TxnTime' => $client_decoded_response->TxnTime ? $client_decoded_response->TxnTime : ' ',
                'TxnDate' => $client_decoded_response->TxnDate ? date('Y-m-d', strtotime($txnDate)) : ' ',
                'BankName' => $client_decoded_response->BankName ? $client_decoded_response->BankName : ' ',
                'RRN' => $client_decoded_response->RRN ? $client_decoded_response->RRN : ' ',
                'Status' => $client_decoded_response->Status ? $client_decoded_response->Status : '',
                'CustomerMobile' => $client_decoded_response->CustomerMobile ? $client_decoded_response->CustomerMobile : '',
                'AvailableBalance' => $client_decoded_response->AvailableBalance ? $client_decoded_response->AvailableBalance : '0',
                'LedgerBalance' => $client_decoded_response->LedgerBalance ? $client_decoded_response->LedgerBalance : '0',
                'WalletTransTxnNo' => $wallet_txn_no,
                'Transaction_id' => $aeps_last_trans_id->id,
            );
//            echo '<pre>';
//            print_r($aeps_inserted_data);
//            die;
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/aeps/' . 'aeps.log', $aeps_inserted_data, 'Inserted data');

            $this->aeps_transaction_model->insert_data('aeps_response', $aeps_inserted_data);

            if (strtolower($aeps_response->DisplayMessage) == 'aeps transaction success') {
                if (!empty($client_decoded_response->Amount)) {
                    $data = array(
                        'user_id' => $this->sessionData["id"],
                        'cash_withdraw' => AEPS_COMMISSION_NAME,
                        'withdraw_amount' => $client_decoded_response->Amount,
                        'type' => 'AEPS'
                    );

                    $commissionResult = $this->Utilities_model->commission_from($this->sessionData["id"]);
                    $created_by = $commissionResult[0]['level_3'];
                    $this->serviceRetailerCommissionDistributionForRecharges($data, $wallet_txn_no, $created_by);
                    $this->updateWalletTxn($client_decoded_response->Amount, $wallet_txn_no);
                }
                $this->session->set_tempdata('msg', $aeps_response->DisplayMessage, 3);
  
            } elseif (strtolower($aeps_response->DisplayMessage) == 'aeps balance enquiry success') {
                $this->session->set_tempdata('msg', $aeps_response->DisplayMessage, 3);
            } elseif (strtolower($aeps_response->DisplayMessage) == 'success') {
                $this->session->set_tempdata('msg', $aeps_response->DisplayMessage, 3);
            } else {

                $this->session->set_tempdata('msg', $aeps_response->DisplayMessage, 3);
            }

            if (strtolower($aeps_response->DisplayMessage) == 'aeps transaction success' ||
                    strtolower($aeps_response->DisplayMessage) == 'transaction under process. please do trnsaction enquiry !' ||
                    strtolower($aeps_response->DisplayMessage) == 'failed to authenticate user.' || strtolower($client_decoded_response->Status) == 'failed' || strtolower($client_decoded_response->Status) == 'success') {
                redirect(base_url() . "admin/aeps/cash_withdraw");
            } elseif (strtolower($aeps_response->DisplayMessage) == 'aeps balance enquiry success') {
                redirect(base_url() . "admin/aeps/balance_enquiry");
            } elseif (strtolower($aeps_response->DisplayMessage) == 'success') {
                redirect(base_url() . "admin/aeps/transaction_status");
            } else {
                redirect(base_url() . "admin/aeps/cash_withdraw");
            }
        }else{
              log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/aeps/' . 'aeps.log', $input, 'empty data');
        }
    }

    public function balance_enquiry() {
        $data['SERVICEID'] = AEPS_BALANCE_ENQUIRY;
        load_back_view(AEPS_BALANCE_ENQUIRY_VIEW, $data);
    }

    public function transaction_status() {
        $data['SERVICEID'] = AEPS_TRANSACTION_STATUS;
        load_back_view(AEPS_TRANSACTION_STATUS_VIEW, $data);
    }

    function encrypt_aeps($text, $passphrase) {
        $encrypted = CryptoJSAES::encrypt($text, $passphrase);
        return $encrypted;
        //var_dump("Encrypted: ", $encrypted);  
    }

    function decrypt_aeps($text, $passphrase) {
        $decrypted = CryptoJSAES::decrypt($text, $passphrase);
        return $decrypted;
        //var_dump("Decrypted: ", $decrypted);
    }

    function updateWalletTxn($amount, $transaction_no) {

        $from_userid = $this->sessionData["id"];

        $transfer_amount = $amount;

        $fund_transfer_remark = "AEPS Txn";

        $from_wallet_details = $this->wallet_model->where('user_id', $from_userid)->find_all();


        $from_wallet_id = $from_wallet_details[0]->id;
        $wallet_amt = $from_wallet_details[0]->amt;

        //$transaction_no = substr(hexdec(uniqid()), 4, 12);


        //if ($wallet_amt >= $transfer_amount) {
            $wallet_trans_detail = [
                "w_id" => $from_wallet_id,
                "amt" => $transfer_amount,
                "merchant_amount" => '',
                "wallet_type" => "actual_wallet",
                "comment" => "AEPS Transaction",
                "status" => 'Debited',
                "user_id" => $from_userid,
                "amt_before_trans" => $from_wallet_details[0]->amt,
                "amt_after_trans" => $from_wallet_details[0]->amt,
                "remaining_bal_before" => 0,
                "remaining_bal_after" => 0,
                "cumulative_bal_before" => 0,
                "cumulative_bal_after" => 0,
                "amt_transfer_from" => $from_userid,
                "amt_transfer_to" => 0,
                // "transaction_type_id" => '11',   //need to check this value
                "transaction_type" => 'Debited',
                "is_status" => "Y",
                "added_on" => date("Y-m-d H:i:s"),
                //"added_by" => $this->session->userdata('user_id'),
                "added_by" => $from_userid,
                "transaction_no" => $transaction_no,
                "remark" => $fund_transfer_remark
            ];
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/aeps/' . 'wallet_Trans.log', $wallet_trans_detail, 'Inserted Data');
            $this->db->insert('wallet_trans', $wallet_trans_detail);
            $wallet_trans_id = $this->db->insert_id();

            $update_wallet = [
                "amt" => $wallet_amt,
                "last_transction_id" => $wallet_trans_id,
                "updated_by" => $from_userid,
                "updated_date" => date("Y-m-d H:i:s"),
            ];

            $this->db->where('user_id', $from_userid);
            return $update_from_wallet = $this->db->update("wallet", $update_wallet);
//        } else {
//            $this->session->set_tempdata('msg', 'Wallet has limited amount', 3);
//        }
    }

    function chk_client_ref_id() {

        $ClientUniqueID = trim($this->input->post('ClientUniqueID'));
        $query = $this->db->get_where('aeps_request', array('ClientRefID' => $ClientUniqueID, 'SERVICEID' => AEPS_CASH_WITHDRAW));
        // echo $this->db->last_query();
        $count = $query->num_rows();

        if ($count > 0) {
            echo 'This is valid transaction no.';
        } else {
            echo 'This is not valid transaction no.';
        }
    }

    public function checkCommission($input, $created_by) {

        if ($input['recharge_type'] == 'Prepaid Mobile') {
            $code = strtolower($input['aggregator'] . $input['mobile_operator'] . 'Prepaid');
            $service_id = PREPAID_SERVICE_ID;
        } elseif ($input['recharge_type'] == 'Postpaid Mobile') {
            $code = strtolower($input['aggregator'] . $input['mobile_operator'] . 'Postpaid');
            $service_id = POSTPAID_SERVICE_ID;
        } elseif ($input['recharge_type'] == 'DTH') {
            $code = strtolower($input['aggregator'] . $input['dth_operator']);
            $service_id = DTH_SERVICE_ID;
        } elseif ($input['recharge_type'] == '0' || $input['recharge_type'] == '1') {
            $code = strtolower($input['type'] . $operator);
            $service_id = DTH_SERVICE_ID;
        } elseif ($input['recharge_type'] == 'Prepaid Datacard') {
            $code = strtolower($input['type'] . $input['mobile_operator'] . 'Prepaid');
            $service_id = DATACARD_SERVICE_ID;
        } elseif ($input['recharge_type'] == 'Postpaid Datacard') {
            $code = strtolower($input['type'] . $input['mobile_operator'] . 'Postpaid');
            $service_id = DATACARD_SERVICE_ID;
        } elseif ($input['cash_withdraw'] == AEPS_COMMISSION_NAME) {

            $code = strtolower($input['cash_withdraw']);
            $service_id = AEPS_SERVICE_ID;
        }

        $LcSqlStr = "SELECT * from services_commissions 
        where status = 'Y'";
        $LcSqlStr .= " AND `values` NOT LIKE '{\"Trimax\":\"0\",\"Rokad\":\"0\",\"MD\":\"0\",\"AD\":\"0\",\"Distributor\":\"0\",\"Retailer\":\"0\"}'";
        $LcSqlStr .= " AND `commission_name` LIKE '%" . str_replace(" ", "", $code) . "%'";
        $LcSqlStr .= " AND `created_by` = '" . $created_by . "'";
        $LcSqlStr .= " AND service_id = '" . $service_id . "'";
        ;
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/aeps/' . 'aeps_commission_query.log', $LcSqlStr, 'Commission query');
        $query = $this->db->query($LcSqlStr);
        return $query->result();
    }

    public function serviceRetailerCommissionDistributionForRecharges($input = "", $trans_ref_no = "", $created_by = '') {


        $user_id = $input['user_id'];
        if (in_array($input['type'], ['AEPS'])) {

            if ($input['recharge_type'] == 'Prepaid Mobile') {
                $code = strtolower($input['type'] . $input['mobile_operator'] . 'Prepaid');
                $service_id = PREPAID_SERVICE_ID;
            } elseif ($input['recharge_type'] == 'Postpaid Mobile') {
                $code = strtolower($input['aggregator'] . $input['mobile_operator'] . 'Postpaid');
                $service_id = POSTPAID_SERVICE_ID;
            } elseif ($input['recharge_type'] == '0' || $input['recharge_type'] == '1') {
                $operator = getTsoDTHOperators($input['dth_operator']);
                // show($operator);
                $code = strtolower($input['type'] . $operator);
                $service_id = DTH_SERVICE_ID;
            } elseif ($input['recharge_type'] == 'DTH') {
                $operator = str_replace(' ', '', $input['dth_operator']);
                $code = strtolower($input['type'] . $operator);
                $service_id = DTH_SERVICE_ID;
            } elseif ($input['recharge_type'] == 'Prepaid Datacard') {
                $code = strtolower($input['type'] . $input['mobile_operator'] . 'Prepaid');
                $service_id = DATACARD_SERVICE_ID;
            } elseif ($input['recharge_type'] == 'Postpaid Datacard') {
                $code = strtolower($input['type'] . $input['mobile_operator'] . 'Postpaid');
                $service_id = DATACARD_SERVICE_ID;
            } elseif ($input['recharge_type'] == 'BOS_TICKET') {
                $code = strtolower($input['recharge_type']);
                $service_id = BOS_SERVICE_ID;
            } elseif ($input['cash_withdraw'] == AEPS_COMMISSION_NAME) {
                $code = strtolower($input['cash_withdraw']);
                $service_id = AEPS_SERVICE_ID;
            }
            $LcSqlStr = "SELECT * from services_commissions 
            where commission_name like '%" . str_replace(" ", "", $code) . "%' 
            and service_id = '" . $service_id . "'"
            ;

            // show($LcSqlStr, 1);

            $query = $this->db->query($LcSqlStr);
            $new = $query->result();
            //    show($new);
            $sql = "select bos_caped_rupees,agent_caped_rupees from sub_services
                 where  service_id = '" . $service_id . "'";
            $squery = $this->db->query($sql);
            $agent_capped_value = $squery->result();
            //show($agent_capped_value[0]);
            if ($new) {

                $row = $new[0];
                $getRetailerServicesCommission = $this->Utilities_model->getRetailerServicesCommissionForRecharges($user_id, $row->service_id, $row->sub_service_id, $created_by);
                //show($getRetailerServicesCommission, 1);
            }
        } else {
            $service_id = AEPS_SERVICE_ID;

            $getRetailerServicesCommission = $this->Utilities_model->getRetailerServicesCommission($user_id, $service_id, $created_by);
        }
        //  show($getRetailerServicesCommission, 1);
        if ($getRetailerServicesCommission) {

            $agent_id = $getRetailerServicesCommission[0]['user_id'];
            $agent_code = $getRetailerServicesCommission[0]['agent_code'];
            $distributor_id = $getRetailerServicesCommission[0]['level_5'];
            $Area_dis_id = $getRetailerServicesCommission[0]['level_4'];
            $master_dis_id = $getRetailerServicesCommission[0]['level_3'];
            $company_id = $getRetailerServicesCommission[0]['level_2'];
            $trimax_id = $getRetailerServicesCommission[0]['level_1'];
            $service_id = $getRetailerServicesCommission[0]['service_id'];
            $commission_id = $getRetailerServicesCommission[0]['commission_id'];
            $total_commission = $getRetailerServicesCommission[0]['total_commission'];

            $values[0] = json_decode($getRetailerServicesCommission[0]['values']);

            $trimax_commission = '0';
            $company_commission = '0';
            $md_commission = '0';
            $ad_commission = '0';
            $dist_commission = '0';
            $retailer_commission = '0';
            $with_gst = $without_gst = 0;
            if (!empty($values)) {

                $trimax_commission = round($values[0]->Trimax, 2);
                $company_commission = round($values[0]->Rokad, 2);
                $md_commission = round($values[0]->MD, 2);
                $ad_commission = round($values[0]->AD, 2);
                $dist_commission = round($values[0]->Distributor, 2);
                $retailer_commission = round($values[0]->Retailer, 2);


                $recharge_amount = $input['withdraw_amount'];
                $commission_percent = !empty($total_commission) ? $total_commission : '0';
                $trimax_cal = calculateGstTds($recharge_amount, $commission_percent);

                $actual_earnings = $trimax_cal['earnings']; //trimax_earnings

                if ($actual_earnings > $agent_capped_value[0]->bos_caped_rupees) {
                    $earnings = $agent_capped_value[0]->bos_caped_rupees;
                } else {
                    $earnings = $actual_earnings;
                }

                $gst = $trimax_cal['gst']; //trimax_gst                            
                $tds = $trimax_cal['tds']; //trimax_tds
                $final_amount = $trimax_cal['final_amount']; //trimax_final_amt                                                        

                $rokad_trimax_cal = calculateGstTds($earnings, $trimax_commission);

                $rokad_trimax_actual_amt = $rokad_trimax_cal['earnings']; //rokad_trimax_amt

                if ($rokad_trimax_actual_amt > $agent_capped_value[0]->bos_caped_rupees) {
                    $rokad_trimax_amt = $agent_capped_value[0]->bos_caped_rupees;
                } else {
                    $rokad_trimax_amt = $rokad_trimax_actual_amt;
                }

                $with_gst = $with_gst + $rokad_trimax_actual_amt;
                $rokad_trimax_cal_with_tds_gst = calculateGstTds($final_amount, $trimax_commission);
                $rokad_trimax_amt_with_tds_gst = $rokad_trimax_cal_with_tds_gst['earnings']; //rokad_trimax_amt_with_tds_gst
                $without_gst = $without_gst + $rokad_trimax_amt_with_tds_gst;

                $company_cal = calculateGstTds($earnings, $company_commission);
                $company_actual_amt = $company_cal['earnings']; //company gst
                if ($company_actual_amt > $agent_capped_value[0]->agent_caped_rupees) {
                    $company_amt = $agent_capped_value[0]->agent_caped_rupees;
                } else {
                    $company_amt = $company_actual_amt;
                }

                $with_gst = $with_gst + $company_actual_amt;

                $company_cal_with_tds_gst = calculateGstTds($final_amount, $company_commission);
                $company_amt_with_tds_gst = $company_cal_with_tds_gst['earnings']; //company_amt_with_tds_gst                    
                $without_gst = $without_gst + $company_amt_with_tds_gst;

                $rd_cal = calculateGstTds($earnings, $md_commission);
                $rd_actual_amt = $rd_cal['earnings']; //rd_amt  
                if ($rd_actual_amt > $agent_capped_value[0]->agent_caped_rupees) {
                    $rd_amt = $agent_capped_value[0]->agent_caped_rupees;
                } else {
                    $rd_amt = $rd_actual_amt;
                }
                $with_gst = $with_gst + $rd_actual_amt;

                $rd_cal_with_tds_gst = calculateGstTds($final_amount, $md_commission);
                $rd_amt_with_tds_gst = $rd_cal_with_tds_gst['earnings']; //rd_amt_with_tds_gst
                $without_gst = $without_gst + $rd_amt_with_tds_gst;

                $dd_cal = calculateGstTds($earnings, $ad_commission);
                $dd_actual_amt = $dd_cal['earnings']; //dd_amt   
                if ($dd_actual_amt > $agent_capped_value[0]->agent_caped_rupees) {
                    $dd_amt = $agent_capped_value[0]->agent_caped_rupees;
                } else {
                    $dd_amt = $dd_actual_amt;
                }
                $with_gst = $with_gst + $dd_actual_amt;
                $dd_cal_with_tds_gst = calculateGstTds($final_amount, $ad_commission);
                $dd_amt_with_tds_gst = $dd_cal_with_tds_gst['earnings']; //dd_amt_with_tds_gst
                $without_gst = $without_gst + $dd_amt_with_tds_gst;

                $ex_cal = calculateGstTds($earnings, $dist_commission);
                $ex_actual_amt = $ex_cal['earnings']; //ex_amt    
                if ($ex_actual_amt > $agent_capped_value[0]->agent_caped_rupees) {
                    $ex_amt = $agent_capped_value[0]->agent_caped_rupees;
                } else {
                    $ex_amt = $ex_actual_amt;
                }
                $with_gst = $with_gst + $ex_actual_amt;
                $ex_cal_with_tds_gst = calculateGstTds($final_amount, $dist_commission);
                $ex_amt_with_tds_gst = $ex_cal_with_tds_gst['earnings']; //ex_amt_with_tds_gst
                $without_gst = $without_gst + $ex_amt_with_tds_gst;


                $sa_cal = calculateGstTds($earnings, $retailer_commission);
                $sa_actual_amt = $sa_cal['earnings']; //sa_amt                 
                $agent_capped_value = $agent_capped_value[0];

                if ($sa_actual_amt > $agent_capped_value->agent_caped_rupees) {
                    $sa_amt = $agent_capped_value->agent_caped_rupees;
                } else {
                    $sa_amt = $sa_actual_amt;
                }
                $with_gst = $with_gst + $sa_actual_amt;

                $sa_cal_with_tds_gst = calculateGstTds($final_amount, $retailer_commission);
                $sa_amt_with_tds_gst = $sa_cal_with_tds_gst['earnings']; //sa_amt_with_tds_gst 
                $without_gst = $without_gst + $sa_amt_with_tds_gst;

                $remaining_commission = round($with_gst, 2) - round($earnings, 2);

                /* for commission adjustment  - Gopal */
            }

            $comm_distribution_array['user_id'] = $agent_id;
            $comm_distribution_array['service_id'] = $service_id;
            $comm_distribution_array['commission_id'] = $commission_id;
            $comm_distribution_array['transaction_no'] = $trans_ref_no;
            $comm_distribution_array['transaction_amt'] = $recharge_amount;
            $comm_distribution_array['service_comm_percent'] = SERVICE_COMMISSION;
            // $comm_distribution_array['trimax_comm_percent'] = $input['type'] == 'JRI' ? JRI_TRIMAX_COMMISSION :  CYBERPLAT_TRIMAX_COMMISSION;;
            $comm_distribution_array['trimax_comm_percent'] = $total_commission;
            $comm_distribution_array['gst_percentage'] = GST_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['tds_percentage'] = TDS_APPLICABLE_MONTHLY_COMMISSION;
            $comm_distribution_array['trimax_actual_earnings'] = $actual_earnings;
            $comm_distribution_array['trimax_earnings'] = $earnings;
            $comm_distribution_array['trimax_gst'] = $gst;
            $comm_distribution_array['trimax_tds'] = $tds;
            $comm_distribution_array['trimax_final_amt'] = $final_amount;
            $comm_distribution_array['rokad_trimax_per'] = $values[0]->Trimax;
            $comm_distribution_array['rokad_trimax_actual_amt'] = $rokad_trimax_actual_amt;
            $comm_distribution_array['rokad_trimax_amt'] = $rokad_trimax_amt;
            $comm_distribution_array['rokad_trimax_amt_with_tds_gst'] = $rokad_trimax_amt_with_tds_gst;
            $comm_distribution_array['company_per'] = $values[0]->Rokad;
            $comm_distribution_array['company_actual_amt'] = $company_actual_amt;
            $comm_distribution_array['company_amt'] = $company_amt;
            $comm_distribution_array['company_amt_with_tds_gst'] = $company_amt_with_tds_gst;
            $comm_distribution_array['rd_per'] = $values[0]->MD;
            $comm_distribution_array['rd_actual_amt'] = $rd_actual_amt;
            $comm_distribution_array['rd_amt'] = $rd_amt;
            $comm_distribution_array['rd_amt_with_tds_gst'] = $rd_amt_with_tds_gst;
            $comm_distribution_array['dd_per'] = $values[0]->AD;
            $comm_distribution_array['dd_actual_amt'] = $dd_actual_amt;
            $comm_distribution_array['dd_amt'] = $dd_amt;
            $comm_distribution_array['dd_amt_with_tds_gst'] = $dd_amt_with_tds_gst;
            $comm_distribution_array['ex_per'] = $values[0]->Distributor;
            $comm_distribution_array['ex_actual_amt'] = $ex_actual_amt;
            $comm_distribution_array['ex_amt'] = $ex_amt;
            $comm_distribution_array['ex_amt_with_tds_gst'] = $ex_amt_with_tds_gst;
            $comm_distribution_array['sa_per'] = $values[0]->Retailer;
            $comm_distribution_array['sa_actual_amt'] = $sa_actual_amt;
            $comm_distribution_array['sa_amt'] = $sa_amt;
            $comm_distribution_array['sa_amt_with_tds_gst'] = $sa_amt_with_tds_gst;
            $comm_distribution_array['status'] = 'Y';
            $comm_distribution_array['created_by'] = $user_id;
            $comm_distribution_array['commission_difference'] = $remaining_commission;
            $comm_distribution_array['created_date'] = date('Y-m-d H:i:s');

            $this->Utilities_model->saveVasCommissionData($comm_distribution_array);
            // show($id, 1);
            // $comm_distribution_array['id'] = $id;
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/aeps/' . 'aeps_commission.log', $comm_distribution_array, 'Commission Distribution Response');

            return $comm_distribution_array;
        }
    }

    public function save_settlement_sheet() {

        $this->load->library("Excel");

        $fileName = 'trimax_uat.xlsx';
        $filePath = $_SERVER['DOCUMENT_ROOT'] . 'aeps_settlement/';
        $inputFileName = $filePath . $fileName;
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'aeps_settlement.log', $inputFileName, 'Aeps');

        try {
            //  Read your Excel workbook
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);

            //  Get worksheet dimensions
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            #$highestColumn = $sheet->getHighestColumn();
            $highestColumn = 'M';

            //  Loop through each row of the worksheet in turn
            $insertData = array();
            for ($row = 2; $row <= $highestRow; $row++) {
                $rowDataArr = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $rowData = $rowDataArr[0];

                // remove white space from all values
                $rowData = array_map('trim', $rowData);
//                show($rowData,1);
                $dt_expl_arr = explode('-', $rowData[8]);
               // $txn_date = $dt_expl_arr[0] . '-' . $dt_expl_arr[2] . '-' . $dt_expl_arr[1];
                //Check if excel record already exitst in dmt_settlementsheet for particular date. Do not enter duplicate data
                $check_data_exists = $this->aeps_transaction_model->check_data_exist($rowData[1], date('Y-m-d', strtotime($rowData[8])));
                if ($check_data_exists == '0') {

                    $tempData = array();
                    $tempData['app_id'] = $rowData[0];
                    $tempData['client_txn_id'] = $rowData[1];
                    $tempData['rrn'] = $rowData[2];
                    $tempData['client_merchant_id'] = $rowData[3];
                    $tempData['bank_name'] = $rowData[4];
                    $tempData['txn_amount'] = $rowData[5];
                    $tempData['txn_mode'] = $rowData[6];
                    $tempData['user_track_id'] = $rowData[7];
                    $tempData['txn_date'] = $rowData[8];
                    $tempData['txn_time'] = $rowData[9];
                    $tempData['txn_status'] = $rowData[10];
                    $tempData['txn_message'] = $rowData[11];
                    $tempData['reference_no'] = $rowData[12];
                    $tempData['file_upload_name'] = $fileName;
                    $tempData['file_upload_date'] = date('Y-m-d H:i:s');

                    $insertData[] = $tempData;
                } else {
                    //               echo 'Record already exists in aeps_settlement_sheet for id :' .$client_txn_id;
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'aeps_settlement.log', 'Record already exists in aeps_settlement_sheet for id :' . $rowData[1], 'Error');
                }
            }

            $dataCount = count($insertData);
            if ($dataCount > 0) {
                $chunkSize = 500;
                $chunk_data = array_chunk($insertData, $chunkSize);
                foreach ($chunk_data as $insertDataChunk) {
                    $udatedRowsCount = $this->aeps_transaction_model->insert_settlement_data($insertDataChunk);
                }

                // update `agent_id` in aeps_settlement_sheet table
                $updated_trimax_agent_ids = $this->aeps_transaction_model->update_agent_id();
            }
        } catch (Exception $e) {
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'aeps_settlement.log', 'Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage(), 'sheet uploading error');
        }
        die;
    }

    /*
     * This function read the date from `aeps_settlement_sheet` table date wise
     * and put date in `aeps_agent_settlement` table
     * `aeps_agent_settlement` table contain total settlement amount by individual agent with date
     */

    public function agent_amount_settlement() {
        $start_date = '2019-01-01'; //date('Y-m-d  00:00:00', strtotime("-1 days"));
        $end_date = '2019-04-30'; //date('Y-m-d  00:00:00');

        $result = $this->aeps_transaction_model->get_trans_amount($start_date, $end_date);
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'aeps_agent_settlement.log', $result, 'agent_amount_settlement');

        if (!empty($result)) {
            $bulkInsert = array();
            $user_ids = array();
            foreach ($result as $eachRow) {
                $checkexists = $this->aeps_transaction_model->checkDataExists($eachRow['agent_id'],$eachRow['txn_date'],$eachRow['txn_amount']);
                
                if($checkexists == '0'){
                    $tempArr = array();
                    $tempArr['user_id'] = $eachRow['agent_id'];
                    $tempArr['amount'] = $eachRow['txn_amount'];
                    $tempArr['transaction_date'] = date('Y-m-d', strtotime($eachRow['txn_date'])); // $start_date;
                    $tempArr['created_date'] = date('Y-m-d H:i:s');

                    $user_ids[] = $eachRow['agent_id'];
                    $bulkInsert[] = $tempArr;
                }
            }
            
            $udatedRowsCount = $this->aeps_transaction_model->insert_agentsettlement_data($bulkInsert);
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'aeps_agent_settlement.log', $bulkInsert, 'insert bulk data in `aepsagent_settlement`table');
        }
        die;
    }

    /*
     * This funcitonlity use by account people to approve aeps settlement amount
     * Account person will see all individual agent settlement amount by date
     * and then he approve the settlemet ammount
     */

    public function check_and_approve_amt_settlement() {
        $get_input = $this->input->get();
        $input = $this->input->post();
        $where = array();

        if ($input) {
            if (!empty($input['trans_date'])) {
                $where['transaction_date'] = date('Y-m-d', strtotime($input['trans_date']));
                $data['trans_date'] = $input['trans_date'];
            }
        } else {
            // get value
            if ($get_input && isset($get_input['date'])) {
                $where['transaction_date'] = date('Y-m-d', strtotime($get_input['date']));
                $data['trans_date'] = $get_input['date'];
            } else {
                // defaul value
                $data['trans_date'] = date('d-m-Y', strtotime("-1 days"));
                $where['transaction_date'] = date('Y-m-d', strtotime("-1 days"));
            }
        }

        $data['result'] = $this->aeps_transaction_model->get_all_rows(null, null, $where);

        $data['total_amount'] = null;
        $data['amt_transfer_info'] = null;
        if (count($data['result']) > 0 && !empty($where['transaction_date'])) {
            // Get total amount for `transaction_date`
            $cond['user_id !='] = '0';
            $data['total_amount'] = $this->aeps_transaction_model->get_total_amount($where, $cond);
            // Get all info for `transaction_date`
            $data['amt_transfer_info'] = $this->aeps_transaction_model->get_amt_transfer_info($where);
        }
//       show($data,1);
        $view = AEPS_CHECK_AMOUNT_SETTLEMENT;
        load_back_view($view, $data);
    }

    /*
     *  form submition to submit settlement amount in out rokad system
     */

    public function check_and_approve_amt_settlement_submit() {
        $input = $this->input->post();
        if ($input) {

            $config = array(
                array('field' => 'total_aeps_settlement_amount_in_day', 'label' => 'Total Settlement Amount', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter valid amount.')),
                array('field' => 'transaction_date_for_approval', 'label' => 'Transaction date', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter transaction date.')),
                array('field' => 'comments', 'label' => 'Comments', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter comments.'))
            );

            if (form_validate_rules($config)) {
                $insertData = array();
                $insertData['credit_amount'] = base64_decode($input['total_aeps_settlement_amount_in_day']);
                $insertData['transaction_date'] = date('Y-m-d', strtotime(base64_decode($input['transaction_date_for_approval'])));
                $insertData['current_date'] = date('Y-m-d H:i:s');
                $insertData['comments'] = $input['comments'];
                $insertData['status'] = 'Y';
                $insertData['is_trimax_wallet_transfer'] = 'NO';
                $insertData['is_agent_wallet_transfer'] = 'NO';

                $last_id = $this->aeps_transaction_model->insert_transac_amount($insertData);
                if ($last_id) {
                    $this->session->set_flashdata('msg', 'Settlement amount approved successfully');
                    redirect(base_url() . 'admin/aeps/show_amount_approval_history');
                } else {
                    $this->session->set_flashdata('msg', 'Error : Settlement amount approval failed');
                    redirect(base_url() . 'admin/aeps/check_and_approve_amt_settlement');
                }
            } else {
                redirect(base_url() . 'admin/aeps/check_and_approve_amt_settlement');
            }
        } else {
            redirect(base_url() . 'admin/aeps/check_and_approve_amt_settlement');
        }
    }

    function ajax_view_agent_transactions_by_date() {
        $input = $this->input->post();

        $epay_agent_settlmnt_tbl_id = $input['id'];
        $transaction_date = $input['transaction_date'];
        $user_id = $input['user_id'];

        $sheet_result = $this->aeps_transaction_model->get_trimax_order_ids($user_id, $transaction_date);
//        print_r($sheet_result); die;
        $final_data = array();
        if (!empty($sheet_result)) {
            $i = 1;
            foreach ($sheet_result as $eachRow) {
                $temp = array();
                $temp['No'] = $i;
                $temp['AgentName'] = $eachRow['display_name'];
                $temp['Type'] = $eachRow['txn_mode'];
                $temp['TxnDate'] = $eachRow['txn_date'];
                $temp['Amount'] = $eachRow['txn_amount'];
                $temp['TrimaxId'] = $eachRow['client_txn_id'];
                $temp['WalletTranId'] = $eachRow['WalletTransTxnNo'];

                $final_data[] = $temp;
                $i++;
            }
        }
        echo '{ "data": ' . json_encode($final_data) . ' }';
        die;
    }

    /*
     * Dispaly settlement amount history with respect to date
     * this functionality use for account person
     */

    public function show_amount_approval_history() {
        $this->load->library('pagination');
        $where = array();

        $input = $this->input->post();
        if ($input) {
            if (!empty($input['trans_date'])) {
                $where['transaction_date'] = date('Y-m-d', strtotime($input['trans_date']));
                $data['trans_date'] = $input['trans_date'];
            }
        }

        $config['base_url'] = base_url('admin/aeps/show_amount_approval_history/');
        $config['per_page'] = 10;
        $config['total_rows'] = $this->aeps_transaction_model->get_count($where);
        $config['use_page_numbers'] = TRUE;

        $this->pagination->initialize($config);
        $page = $this->uri->segment(4) ? $this->uri->segment(4) : 0;
        $data['pagi_links'] = $this->pagination->create_links();
        $data['result'] = $this->aeps_transaction_model->get_aeps_transfer_amount($config['per_page'], ($page), $where);

        $view = AEPS_CREDIT_AMOUNT;
        load_back_view($view, $data);
    }

    /*
     * CRON script to update trimax wallet after account team approve the settlemetn amount
     */

    public function update_trimax_wallet($yesterday = '') {
        $TRIMAX_USER_ID = $this->users_model->get_trimax_user_id();

        if (empty($yesterday)) {
            $yesterday = date('Y-m-d', strtotime("-1 days"));
        }
        
        $where = array('transaction_date' => $yesterday);
        $result = $this->aeps_transaction_model->get_user_amount_by_date($yesterday);

        $transfer_amount_info = $this->aeps_transaction_model->get_aeps_transfer_amount(null, null, $where);
        $aeps_credit_amount = $transfer_amount_info[0]['credit_amount'];
        
        
        if(empty($transfer_amount_info)){
             log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'aeps_update_trimax_Wallet.log', 'No data found');
             die('No data found');
        }else if(trim($transfer_amount_info[0]['is_trimax_wallet_transfer']) != 'NO') {
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'aeps_update_trimax_Wallet.log', 'Already updated the TRIMAX user account');
            die('Already updated the TRIMAX user account');
        }
        
//         echo '<pre>'; print_r($result); die;
        if (!empty($result)) {
            foreach ($result as $eachRecord) {
//         echo '<pre>'; print_r($eachRecord); die;
                // adding in trimax account in `wallet` table
                $trimax_wallet_info = $new_trimax_amount = '';
                $wallet_tran_id = substr(hexdec(uniqid()), 4, 12);
                $trimax_wallet_info = $this->aeps_transaction_model->get_user_details($TRIMAX_USER_ID);

                $new_trimax_amount = $trimax_wallet_info['amt'] + $eachRecord['txn_amount'];
                $update_trimax_wallet = $this->Utilities_model->updateUserWallet($TRIMAX_USER_ID, $new_trimax_amount, $wallet_tran_id);

                $logTxt = "Trimax Old amount -> " . $trimax_wallet_info ['amt'] . " New amount -> $new_trimax_amount,  value added by aeps -> " . $eachRecord['txn_amount'];
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'aeps_update_trimax_Wallet.log', $logTxt, 'updating trimax wallet amount (adding amount)');

                $wallet_trans_detail = array();

                $wallet_trans_detail['w_id'] = $trimax_wallet_info['id'];
                $wallet_trans_detail['amt'] = $eachRecord['txn_amount'];
                $wallet_trans_detail['comment'] = 'AEPS T+1 transaction';
                $wallet_trans_detail['status'] = 'Credited';
                $wallet_trans_detail['user_id'] = $TRIMAX_USER_ID;
                $wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
                $wallet_trans_detail['amt_before_trans'] = $trimax_wallet_info['amt'];
                $wallet_trans_detail['amt_after_trans'] = $new_trimax_amount;
                $wallet_trans_detail['added_by'] = 'AEPS Cron Script ' . $eachRecord['txn_date'];
                $wallet_trans_detail['wallet_type'] = 'actual_wallet';
                $wallet_trans_detail['transaction_type'] = 'Credited';
                $wallet_trans_detail['is_status'] = 'Y';
                $wallet_trans_detail['transaction_no'] = $wallet_tran_id;
                $wallet_trans_detail['remark'] = 'aeps_settlement_sheet '.$eachRecord['id'];
                
                $wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail);
                
            }
            // update status in `epaylater_transfer_amount` table to avoid repeated execution
            $this->aeps_transaction_model->update_cron_status_for_transfer_amount('is_trimax_wallet_transfer', 'YES', $where);
        }
    }

    /*
     * CRON SCRIPT update Agent wallet
     */

    public function update_agent_wallet($yesterday = '') {

        $TRIMAX_USER_ID = $this->users_model->get_trimax_user_id();
        if (empty($yesterday)) {
            $yesterday = date('Y-m-d', strtotime("-1 days"));
        }
        $yesterday = date('Y-m-d', strtotime($yesterday));
        $result = $this->aeps_transaction_model->get_user_amount_by_date($yesterday);

        $where = array('transaction_date' => $yesterday);
        $transfer_amount_info = $this->aeps_transaction_model->get_aeps_transfer_amount(null, null, $where);
//        show($transfer_amount_info,1);
        if(empty($transfer_amount_info)){
             log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'aeps_update_trimax_Wallet.log', 'No data found');
             die('No data found');
        }else if(trim($transfer_amount_info[0]['is_agent_wallet_transfer']) != 'NO') {
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'aeps_update_trimax_Wallet.log', 'Already updated the AGENT user account');
            die('Already updated the Agent account');
        }

        if (!empty($result)) {
            foreach ($result as $eachRecord) {
//                echo '<pre>'; print_r($eachRecord); die;
                // deduct from company user
                $trimax_amount_deduct = $new_amount= '';
                $trimax_wallet_info = $this->aeps_transaction_model->get_user_details($TRIMAX_USER_ID);

                $trimax_amount_deduct = $trimax_wallet_info['amt'] - $eachRecord['txn_amount'];
                $update_trimax_wallet = $this->aeps_transaction_model->update_user_wallet($trimax_amount_deduct, $TRIMAX_USER_ID);
                $logTxt = "Trimax Old amount -> " . $trimax_wallet_info['amt'] . " New amount -> $trimax_amount_deduct,  amount deduct -> " . $eachRecord['txn_amount'];
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'aeps_update_trimax_Wallet.log', $logTxt, 'updating trimax wallet (deduct amount)');

                //adding new entry in `wallet_trans` table for trimax account deduction
                $tras_detail = array();
                $tras_detail['transaction_no'] = substr(hexdec(uniqid()), 4, 12);
                $tras_detail['w_id'] = $trimax_wallet_info['id'];
                $tras_detail['amt'] = $eachRecord['txn_amount'];
                $tras_detail['status'] = 'Debited';
                $tras_detail['user_id'] = $TRIMAX_USER_ID;
                $tras_detail['amt_before_trans'] = $trimax_wallet_info['amt'];
                $tras_detail['amt_after_trans'] = $trimax_amount_deduct;
                $tras_detail['transaction_type'] = 'Debited';

                $tras_detail['added_by'] = 'AEPS Cron Script for ' . $eachRecord['txn_date'];
                $tras_detail['remark'] = 'aeps_settlement_sheet '.$eachRecord['id'];
                $this->update_wallet_trans_table($tras_detail);

                // adding in user's account (agent wallet)
                $user_wallet_info = $this->aeps_transaction_model->get_user_details($eachRecord['agent_id']);
//                show($user_wallet_info,1);
                $new_amount = $user_wallet_info['amt'] + $eachRecord['txn_amount'];
                $update_agent_wallet = $this->aeps_transaction_model->update_user_wallet($new_amount, $eachRecord['agent_id']);
                $logTxt = "Agent ".$eachRecord['agent_id']." Old amount -> " . $user_wallet_info['amt'] . " New amount -> $new_amount,  amount deduct -> " . $eachRecord['txn_amount'];
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'aeps_update_trimax_Wallet.log', $logTxt, 'updating user wallet (adding amount)');

                //adding new entry in `wallet_trans` table for trimax account deduction
                $tras_detail = array();
                $tras_detail['transaction_no'] = substr(hexdec(uniqid()), 4, 12);
                $tras_detail['w_id'] = $user_wallet_info['id'];
                $tras_detail['amt'] = $eachRecord['txn_amount'];
                $tras_detail['status'] = 'Credited';
                $tras_detail['user_id'] = $eachRecord['agent_id'];
                $tras_detail['amt_before_trans'] = $user_wallet_info['amt'];
                $tras_detail['amt_after_trans'] = $new_amount;
                $tras_detail['transaction_type'] = 'Credited';
                $tras_detail['added_by'] = 'AEPS Cron Script for ' . $eachRecord['txn_date'];
                $tras_detail['remark'] = 'aeps_settlement_sheet '.$eachRecord['id'];
                
                $this->update_wallet_trans_table($tras_detail);

                $update_status = $this->aeps_transaction_model->update_status($eachRecord['id'], 'Y', 'Amount credited in Agent account', $tras_detail['transaction_no']);
            }

            // update status in `aeps_transfer_amount` table to avoid repeated execution
            $this->aeps_transaction_model->update_cron_status_for_transfer_amount('is_agent_wallet_transfer', 'YES', $where);
        }
    }

    private function update_wallet_trans_table($detail) {
        $wallet_trans_detail = array();
        $wallet_trans_detail['transaction_no'] = $detail['transaction_no'];
        $wallet_trans_detail['w_id'] = $detail['w_id'];
        $wallet_trans_detail['amt'] = $detail['amt'];
        $wallet_trans_detail['comment'] = 'AEPS T+1 Transaction';
        $wallet_trans_detail['status'] = $detail['status'];
        $wallet_trans_detail['user_id'] = $detail['user_id'];
        $wallet_trans_detail['added_by'] = $detail['added_by'];
        $wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
        $wallet_trans_detail['amt_before_trans'] = $detail['amt_before_trans'];
        $wallet_trans_detail['amt_after_trans'] = $detail['amt_after_trans'];
        $wallet_trans_detail['wallet_type'] = 'actual_wallet';
        $wallet_trans_detail['transaction_type'] = $detail['transaction_type'];
        $wallet_trans_detail['is_status'] = 'Y';
	$wallet_trans_detail['remark'] =  $detail['remark'];

        $wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail);
        // log_data($this->logFileName, $wallet_trans_id, 'adding new entry in wallet_trans table for Trimax');
    }
    
    public function export_pdf($user_id) {
        $user_id = $user_id;
      
        
        $trasaction_details = $this->aeps_transaction_model->getTransactionReceipt($user_id);

//        show($trasaction_details,1);
       $pdf_gen                 = $this->generatePdf($trasaction_details);
       
    }
    
    public function generatePdf($result){
    
    $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $title = "AEPS Receipt";
    $obj_pdf->SetTitle($title);
    
    //$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetDefaultMonospacedFont('helvetica');
    $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $obj_pdf->SetFont('helvetica', '', 9);
    $obj_pdf->setFontSubsetting(false);
    $obj_pdf->AddPage();
    ob_start();
    $a_mail_replacement_array = array(
                                '{{service_name}}' => ucwords($result[0]["Service_name"]),
                                "{{agent_name}}" => $result[0]['display_name'],
                                "{{mobile_no}}" => $result[0]['display_name'],
                                "{{paid_by}}" => 'Wallet',
                                "{{amount}}" => $result[0]['TxnAmt'],
                                "{{amount_in_words}}" => ucwords(getIndianCurrency($result[0]['TxnAmt'])),
                                "{{trans_ref_no}}" => $result[0]['RRN'],
                                "{{mobile_no}}" => rokad_decrypt($result[0]['username'], $this->config->item('pos_encryption_key')),
                                "{{rokad_trans_no}}" => $result[0]['transaction_no']
                                );
       
    $content = $data['agent_mail_content'] = str_replace(array_keys($a_mail_replacement_array),array_values($a_mail_replacement_array),AEPS_RECEIPT);
   
       // show($content,1);     
    ob_end_clean();
    $obj_pdf->writeHTML($content, true, false, true, false, '');
    $obj_pdf->Output('Micro ATM Receipt.pdf', 'I');
    }
    
    

}

