<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Lock extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('datatables');		
	}

	public function index() {
	
		load_back_view(LOCK_VIEW);		
	}
	public function get_lock_list() {		
		$session_level = $this->session->userdata('level');
        $user_id = $this->session->userdata('user_id'); 
        
		$this->datatables->select('id,CONCAT(first_name," ",last_name) as agent_name,agent_code,lock_status');
		$this->datatables->from('users');
		$this->datatables->where('kyc_verify_status','Y');
		$this->datatables->where('status','Y');
		$this->datatables->where("agent_code like '%AGT%'");
		if($this->session->userdata('role_id') == MASTER_DISTRIBUTOR_ROLE_ID)
        {
            $level    = MASTER_DISTRIBUTOR_LEVEL;           
            $this->datatables->where('level_'.$level,$user_id);
        }
        if($this->session->userdata('role_id') == AREA_DISTRIBUTOR_ROLE_ID)
        {
            $level    = AREA_DISTRIBUTOR_LEVEL;           
            $this->datatables->where('level_'.$level,$user_id);
        }
		if($this->session->userdata('role_id') == DISTRIBUTOR)
        {
            $level    = POS_DISTRIBUTOR_LEVEL;           
            $this->datatables->where('level_'.$level,$user_id);
        }

        $this->datatables->add_column('action', '<a href="javascript:void" class="del_btn" ref="$1" data-ref="$2">  <i class="fa fa-square-o"></i> </a>', 'id,lock_status');
        $data = $this->datatables->generate('json');
        echo $data;		
	}

	public function change_status () {
		$input = $this->input->post();
		
		$this->users_model->id = $input['id'];
		$this->users_model->lock_status = $input['postyn'];
		if($input['postyn'] == "Y") {
			$msg = "Locked";
		} else if($input['postyn'] == "N") {
			$msg = "Unlocked";
		}
		$id = $this->users_model->save();
		if($id>0) {
			 $data["flag"] = '@#success#@';
			$this->session->set_flashdata('msg_type','success');
			$this->session->set_flashdata('msg','This users get '.$msg);

		} else {
            $data["flag"] = '@#failed#@';
            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'User could not be locked.');
        }
        data2json($data);
	}

}