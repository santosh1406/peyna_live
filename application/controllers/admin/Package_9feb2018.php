<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Package extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('package_master_model','package_detail_model','service_master_model','Common_model','users_model','assign_package_log_model'));
        $this->load->library('form_validation');
        is_logged_in();
    }

    public function index() {
        load_back_view(USER_PACKAGE);
    }
    
     /* @Author      : Maunica Shah
     * @function    : list_package
     * @detail      : Gives the lists of all the packages.
     */

    function list_package() {
        $where = array('pm.is_deleted' => "0", 'pm.default' => '1');
        $this->datatables->select("pm.id,pm.package_name,DATE_FORMAT(pm.from,'%d-%m-%Y') AS from_date,DATE_FORMAT(pm.till,'%d-%m-%Y') AS till,DATE_FORMAT(pm.created_date,'%d-%m-%Y') AS created_at,pm.status,pm.default");
        $this->datatables->from("package_master pm");
        $this->datatables->where($where);
        $this->datatables->add_column('action', '<a href="' . base_url('admin/commission/view') . '/$1" class="btn btn-primary btn-xs show_btn" data-ref="$1" data-status="$2" title="View"><i class="fa fa-eye"></i> </a>
                                                <a href="' . base_url('admin/commission/delete') . '/$1" class="btn btn-primary btn-xs delete_btn" data-ref="$1" data-status="$2" title="Delete"> <i class="fa fa-trash"></i> </a>
                                                ', 'id,status');

        if($this->session->userdata('role_id') == COMPANY_ROLE_ID) {
            $this->datatables->or_where('pm.created_by',$this->session->userdata('id'));
        }
        $this->datatables->where('pm.is_deleted','0');
        
        $data = $this->datatables->generate('json');
        if (!empty($data)) {
            echo $data;
        } else {
            echo "No Result Found";
        }
    }
    
    /* @Author      : Lilawati Karale
     * @function    : list_service
     * @detail      : Give the lists of all the services.
     */

    function list_service() {
        $this->datatables->select("s.id,s.name,s.code,s.status,s.deleted,s.created_by,DATE_FORMAT(s.created_at,'$d-%m-%Y') AS created_at,DATE_FORMAT(s.updated_at,'%d-%m-%Y) as updated_at'");
        $data = $this->datatables->generate('json');
        if(!empty($data)) {
            echo $data;
        } else {
            echo"No result found";
        }
   }

    /* @Author      : Lilawati Karale
     * @function    : create_package
     * @detail      : Load the create package view
     */

    public function create_package()
    {   
        $data['service'] = $this->service_master_model->where("status","1")->as_array()->find_all();
        load_back_view(PACKAGE_CREATE, $data);
    }
    
    /* @Author      : Lilawati Karale
     * @function    : list_commission_detail
     * @detail      : Gives the commission value depending on service id
     */
    public function list_commission_detail()
    {   
        $input = $this->input->post();
        $default_pkg = $input['default_package'];
        $table = $input['service_id']."_commission_master";
        $data = array();
        $data = $this->common_model->getCommission($table,$default_pkg);  
        if($data)
        {
          $count_array = count(array_keys($data));
          foreach ($data as $key => $value) 
          {
            $comm_id = $value['id'];
            $final_data = $this->package_detail_model->column('commission_id,commission_name')->where('commission_id',$comm_id)->as_array()->find_all();
            if($final_data)
            {
              unset($data[$key]);
            }
          }
        }   
        data2json($data);
    }

    /* @Author      : Lilawati Karale
     * @function    : package_unique
     * @detail      : Check whether package is unique or not
     */

    public function package_unique() {
        $input =$this->input->post();
        $where = array(
                        "package_name" => $input['name']
                    );
        $count = $this->package_master_model->where($where)->count_all();
        $flag= ($count) ? 'true' : 'false';
        echo $flag; 
    }
    
    /* @Author      : Lilawati Karale
     * @function    : create
     * @detail      : Save the package data into db
     */
    public function create() { 
        $input = $this->input->post();
        $def_temp = array();
        if(!empty($input)) 
        {
                if($input['default_package'] == 1) {
                  $def_temp = $this->package_master_model->where(array('is_deleted' => '0','default' => $input['default_package']))->find_all();
                }
                if(empty($def_temp)) {

                      $this->package_master_model->package_name = $input['name'];
                      $this->package_master_model->from = !empty($input['from']) ? (date('Y-m-d', strtotime($input['from']))." 00:00:00") : '';
                      $this->package_master_model->till = !empty($input['till']) ? (date('Y-m-d', strtotime($input['till'])) . " 23:59:59") : '';
                      $this->package_master_model->default = $input['default_package'];
                      $this->package_master_model->status = 1;
                      $this->package_master_model->created_by = $this->session->userdata('user_id');
                      $this->package_master_model->updated_by = $this->session->userdata('user_id');
                      $this->package_master_model->created_date =  date('Y-m-d H:i:s' ,time());
                      $this->package_master_model->updated_date = date('Y-m-d H:i:s', time()); 
                      $package_id = $this->package_master_model->save();
                      if(!empty($package_id)) 
                      {
                          if(is_array($input['service_name']) && !empty($input['service_name'])) {
                          
                              foreach ($input['service_name'] as $key => $value) {
                                  $table = $value."_commission_master";
                                  $data = $this->common_model->getCommission($table,$input['default_package']);

                                  foreach ($data as $k => $v) { 
                                      if(in_array($v['id'],$input['select_commission']) ) {
                                          $this->package_detail_model->package_id = $package_id;
                                          $this->package_detail_model->service_id = $value;
                                          $service_detail = $this->service_master_model->find_by(["id" => $value]);
                                          $this->package_detail_model->service_name = $service_detail->service_name;
                                          $this->package_detail_model->commission_id = $v[id];
                                          $this->package_detail_model->commission_name = $v['name']; 
                                          $this->package_detail_model->status = 1; 
                                          $this->package_detail_model->created_by = $this->session->userdata('user_id');
                                          $this->package_detail_model->updated_by = $this->session->userdata('user_id');
                                          $this->package_detail_model->created_date =  date('Y-m-d H:i:s' ,time());
                                          $this->package_detail_model->updated_date = date('Y-m-d H:i:s', time());
                                          $package_detail_id = $this->package_detail_model->save();
                                          if($package_detail_id)
                                          {
                                              $this->session->set_flashdata('msg', 'Package Created Successfully');
                                          }
                                      }
                                  }     
                              } 
                          }
                      }
                      else 
                     {    
                        $this->session->set_flashdata("msg" ,"Package could not be created.");
                     }
                } else {
                    $this->session->set_flashdata("msg" ,"Default Package could not be created, as it is created already.");
                }   
        }  
        else 
        {
          $this->session->set_flashdata("msg" ,"Some error occured.Please try again later.");
        }
        redirect(site_url().'/admin/package');
    } 
    
    /* @Author     : Maunica Shah
     * @function    : change_status
     * @detail      : change package status.
     */

    public function change_status($status) {
        $status = $status == '1' ? '0' : '1';
        
        $data = array();
        $data["flag"] = '@#failed#@';
        $data["msg"] = 'Failed to update status.';

        if ($this->package_master_model->update($this->input->post('id'), array('status' => $status))) {
            $data["flag"] = '@#success#@';
            $data["msg"] = 'Status changed successfully.';
        }
        data2json($data);
    }

     /* @Author     : Maunica Shah
     * @function    : delete
     * @detail      : delete package.
     */

    public function delete($id) {
        $response = array();
        $user_info = $this->session->userdata('user_id');
        $commission = $this->package_master_model->find_by(["id" => $id]);
        $pkg_data = $this->assign_package_log_model->find_by(["package_id" => $id,'pkg_delete_status'=>'0','created_by' => $user_info]);

        if($pkg_data){
          $this->session->set_flashdata('msg', "Package <b>" . $commission->name . "</b> cannot be deleted. It is assigned to some users.");
        } else {
          if ($commission->is_deleted == 1) {
              $update_status_as = "0";
              $update_status_action = "deactived";
          } else if ($commission->is_deleted == 0) {
              $update_status_as = "1";
              $update_status_action = "activated";
          }

          if ($commission && isset($update_status_as)) 
          {
              $commission_save = [
                  'updated_date' => date("Y-m-d H:i:s"),
                  'updated_by' => $user_info->id,
                  'is_deleted' => $update_status_as
              ];
              $uid = $this->package_master_model->update($id, $commission_save);

              $assignPackage = [
                  'updated_date' => date("Y-m-d H:i:s"),
                  'pkg_delete_status' => $update_status_as
              ];

              $updateAssignLog = $this->assign_package_log_model->updateDeletePkg($id, $assignPackage);

              if ($uid) {
                  $this->session->set_flashdata('msg', "Package <b>" . $commission->name . "</b> is <b>" . 'deleted' . "</b>.");
              } else {
                  $this->session->set_flashdata(["error" => "Package <b>" . $commission->name . "</b> could not " . $update_status_action]);
              }
          } else {
              $this->session->set_flashdata(["error" => "Package is not found."]);
          }
        }
        redirect(base_url('admin/package'));
    }
    
     /*@Author     : Lilawati Karale
     * @function    : view
     * @detail      : Perticular package detail view.
     */

    public function view($id) {
        $data = array();
        if (!empty($id)) {
            $pd_detail = $this->package_master_model->package_detail_by_id($id);
            if (!empty($pd_detail)) {
                $data["package_id"] = $pd_detail[0]->package_id;
                $data["package_name"] = $pd_detail[0]->package_name;
                $data["package_from"] = date("d-m-Y", strtotime($pd_detail[0]->package_from));
                $data["package_till"] = date("d-m-Y", strtotime($pd_detail[0]->package_till));
                $data["default_package"] = $pd_detail[0]->default;
                $result =array ();
                $i = 0;
                foreach ($pd_detail as $key => $value) { 
                    $result[$i]["package_detail_id"] = $value->package_detail_id;
                    $result[$i]["service_id"] = $value->service_id;
                    $result[$i]["service_name"]  =  $value->service_name;
                    $result[$i]["commission_id"] = $value->commission_id;
                    $result[$i]["commission_name"]   = $value->commission_name;
                    $i++;
                }
                $data["package_detail"] = $result; 
                load_back_view(PACKAGE_VIEW, $data);
            } else {
                $this->session->set_flashdata(["msg" => "Package detail not found. Please try again."]);
                redirect(site_url() . '/admin/package');
            }
        } else {
            $this->session->set_flashdata(["msg" => "Package detail not found. Please try again."]);
            redirect(site_url() . '/admin/package');
        }
    }
    public function assignPackage()
    {
      $sessionData = $this->session->userdata();
      $role_id = $sessionData['role_id'];
      $user_id  = $this->session->userdata("user_id");

       if($role_id == COMPANY_ROLE_ID || $role_id == TRIMAX_ROLE_ID)
      {
          
          $where_condition = array('role_id' => MASTER_DISTRIBUTOR_ROLE_ID ,
            'status' => 'Y', 'is_deleted' => 'N' );
          if($role_id == COMPANY_ROLE_ID) {
            $where_condition['level_2'] = $user_id;
          }
           $data['md_details'] = $this->users_model->column('id,first_name,last_name,level_2')->where($where_condition)->find_all();
          // show($data['md_details'],1);
           $data['service'] = $this->service_master_model->where("status","1")->as_array()->find_all();
          load_back_view(ASSIGN_PACKAGE_VIEW, $data);
      }
      else
      {
        redirect(base_url().'admin/dashboard');
      }
    }
     public function list_package_detail()
    {   
        $input = $this->input->post();
        $service_id = $input['service_id'];
        $data = $this->package_detail_model->getpackagedetails($service_id);
        data2json($data);
    }
    public function assignPackageToMd()
    {
        $input = $this->input->post();
        $user_id = $input['md_id'];
        $package_name = $input['select_package'];
        if(!empty($input))
        {
             if(is_array($input['service_name']) && !empty($input['service_name']) && is_array($input['select_package']) && !empty($input['select_package'])) 
             {
              $this->common_model->update('assign_package_log','user_id',$user_id,array('pkg_delete_status' => '1'));
             foreach ($input['service_name'] as $key => $service_id) 
              {
                 $this->assign_package_log_model->user_id = $user_id;
                 $this->assign_package_log_model->service_id = $service_id; 
                 $this->assign_package_log_model->package_id = $package_name[$key]; 
                 $this->assign_package_log_model->status = '1'; 
                 $this->assign_package_log_model->created_by = $this->session->userdata('user_id');
                 $package_assign_log_id = $this->assign_package_log_model->save();

              } 
              
             if($package_assign_log_id)
            {
                $this->session->set_flashdata('msg_type', 'Success');
                $this->session->set_flashdata('msg', 'Assign package done successfully');
                redirect(base_url().'admin/package');
             }
          }
       }
    }

      public function current_package_detail()
    {   
        $input = $this->input->post();
        $md_id = $input['md_id']; 
        $where = array('user_id' => $md_id,'pkg_delete_status'=>'0');
        $data = $this->assign_package_log_model->where($where)->order_by("id", "desc")->limit('1')->find_all();
        if(!empty($data)) {
          // $where1 = array('id' => $data[0]->package_id);
          $pkg_data = $this->package_master_model->where('id',$data[0]->package_id)->find_all();
          $data['package_name'] = $pkg_data[0]->package_name;
        }
        data2json($data);
    }
}
