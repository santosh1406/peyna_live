<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class Fund_transfer extends CI_Controller 
{
  public function __construct() {
    parent::__construct();
    is_logged_in();
    set_time_limit(0);

    $this->load->model(array('users_model','wallet_model','wallet_trans_model','assign_package_log_model','package_detail_model'));
     $this->load->model(array('services/service_2/service_2_commission_model','services/service_2/service_2_commission_detail_model'));
  }

  public function index() {

    $data=[];
    $role_id = $this->session->userdata('role_id');
    $user_id = $this->session->userdata('user_id');
    $data['user_data']  = $this->session->userdata();
    
    //show($role_id);
    //show($user_id);    
    
    if(!empty($role_id)) {
      $userdata = $this->users_model->where('id',$user_id)->find_all();
      if($userdata) {
        $wallet_balance = $this->wallet_model->where('user_id',$userdata[0]->id)->find_all();
        if(!empty($wallet_balance)) {
          $data['wallet_balance'] = $wallet_balance[0]->amt;
        }
        else {
          $data['wallet_balance'] = '0.00';
        }
        $data['users_details'] = $userdata;    
      }
    }    
    
   if($role_id == COMPANY_ROLE_ID) {
    	$where = array('level' => MASTER_DISTRIBUTOR_LEVEL,'role_id' => MASTER_DISTRIBUTOR_ROLE_ID,'status' => 'Y','is_deleted' => 'N','level_2' => $user_id,'kyc_verify_status' => 'Y');
    	$find_md = $this->users_model->column('id,first_name,last_name,level')->where($where)->find_all();
    	$data['master_distributor'] = $find_md;
    }    
    else if($role_id == MASTER_DISTRIBUTOR_ROLE_ID) {
    	$where = array('level' => AREA_DISTRIBUTOR_LEVEL,'role_id' => AREA_DISTRIBUTOR_ROLE_ID,'status' => 'Y','is_deleted' => 'N','level_3' => $user_id,'kyc_verify_status' => 'Y');
      $find_ad = $this->users_model->column('id,first_name,last_name,level')->where($where)->find_all();
      $data['area_distributor'] = $find_ad;
    }
    else if($role_id == AREA_DISTRIBUTOR_ROLE_ID) {
    	$where = array('level' => POS_DISTRIBUTOR_LEVEL,'role_id' => DISTRIBUTOR,'status' => 'Y','is_deleted' => 'N','level_4'=> $user_id,'kyc_verify_status' => 'Y');
      $find_di = $this->users_model->column('id,first_name,last_name,level')->where($where)->find_all();
      $data['distributor'] = $find_di;
    }
    else if($role_id == DISTRIBUTOR) {
    	$where = array('level' => RETAILOR_LEVEL,'role_id' => RETAILER_ROLE_ID,'status' => 'Y','is_deleted' => 'N','level_5' => $user_id,'kyc_verify_status' => 'Y');
      $find_ret = $this->users_model->column('id,first_name,last_name,level')->where($where)->find_all();
      $data['retailer'] = $find_ret;
    }
    else {
    	$where = array('level' => COMPANY_LEVEL,'status' => 'Y','is_deleted' => 'N','kyc_verify_status' => 'Y');
      $find_cmp = $this->users_model->column('id,first_name,last_name,level')->where($where)->find_all();
      $data['companies'] = $find_cmp;
    }
    
    //show($data['companies'], 1);
    
    $data['role_id'] = $role_id;
    load_back_view(FUND_TRANSFER_VIEW, $data);
  }
  
  public function get_md_data() {
  	$data=array();
  	$input=array();
  	$input = $this->input->post(NULL,TRUE);
  	$wallet_balance = $this->wallet_model->where('user_id',$input['id'])->find_all();
  	if($wallet_balance) {
  		$wallet_amt = $wallet_balance[0]->amt;
  	}
  	else {
  		$wallet_amt ='0.00';
  	}
  	$data['response_data'] = $this->users_model->get_md_data($input['id']);
  	$data['wallet_balance'] = number_format($wallet_amt,2);
  	data2json($data);
  }

  public function get_ad_data() {
    $data=array();
    $input=array();
    $input = $this->input->post(NULL,TRUE);
    $wallet_balance = $this->wallet_model->where('user_id',$input['id'])->find_all();
    if($wallet_balance) {
      $wallet_amt = $wallet_balance[0]->amt;
    }
    else {
      $wallet_amt ='0.00';
    }
    $data['response_data'] = $this->users_model->get_ad_data($input['id']);
    $data['wallet_balance'] = number_format($wallet_amt,2);
    data2json($data);
  }

  public function get_distributor_data() {
    $data=array();
    $input = $this->input->post(NULL,TRUE);
    $wallet_balance = $this->wallet_model->where('user_id',$input['id'])->find_all();
    if($wallet_balance) {
      $wallet_amt = $wallet_balance[0]->amt;
    }
    else {
      $wallet_amt ='0.00';
    }
    $data['response_data'] = $this->users_model->get_distributor_data($input['id']);
    $data['wallet_balance'] = number_format($wallet_amt,2);
    data2json($data);
  }

  public function get_retailer_data() {
    $data=array();
    $input = $this->input->post(NULL,TRUE);
    $wallet_balance = $this->wallet_model->where('user_id',$input['id'])->find_all();
    if($wallet_balance) {
      $wallet_amt = $wallet_balance[0]->amt;
    }
    else {
      $wallet_amt ='0.00';
    }
    $data['wallet_balance'] = number_format($wallet_amt,2);
    $data['response_data'] = $this->users_model->get_retailer_data($input['id']);
    data2json($data);
  }

  public function retailer_wallet_amount() {
    $input = $this->input->post(NULL,TRUE);
    $wallet_balance = $this->wallet_model->where('user_id',$input['id'])->find_all();
    if($wallet_balance) {
      $wallet_amt = $wallet_balance[0]->amt;
    }
    else {
      $wallet_amt ='0.00';
    }
    $data['wallet_balance'] = number_format($wallet_amt,2);
    data2json($data);
  }

  public function proceed() {
    $data =[];
    $input = $this->input->post(NULL,TRUE);
    $role_id = $this->session->userdata('role_id');
    $user_id = $this->session->userdata('user_id');
    $from_bal = $input['from_bal'];
    $company = $input['company'];
    $md_type = $input['md_type'];
    $area_di = $input['area_di'];
    $only_di = $input['only_di'];
    $retailer = $input['retailer'];
    $transfer_amount = $input['amount'];

    $from_wallet_detail = $this->wallet_model->where('user_id',$user_id)->find_all();
    $from_details = $this->users_model->where('id',$user_id)->find_all();

    if($from_details[0]->kyc_verify_status == 'N') {
      $this->session->set_flashdata('msg_type', 'success');
      $this->session->set_flashdata('msg', 'Your kyc approve Status on pending');
      redirect(site_url() . '/admin/fund_transfer');
    }  
    if($transfer_amount < 1) {
      $this->session->set_flashdata('msg_type', 'success');
      $this->session->set_flashdata('msg', 'amount Should be greater than 0 ');
      redirect(site_url() . '/admin/fund_transfer');
    }

    if(!empty($retailer)) {
      $benefaciary_details = $this->users_model->where('id',$retailer)->find_all();
    }
    elseif (!empty($only_di)) {
      $benefaciary_details = $this->users_model->where('id',$only_di)->find_all();
    }
    elseif (!empty($area_di)) {
      $benefaciary_details = $this->users_model->where('id',$area_di)->find_all();
    }
    elseif (!empty($md_type)) {
      $benefaciary_details = $this->users_model->where('id',$md_type)->find_all();
    }
    elseif (!empty($company)) {
    	$benefaciary_details = $this->users_model->where('id',$company)->find_all();
    }
    else {
      $benefaciary_details= [];
    }
    if($from_wallet_detail[0]->amt >= $transfer_amount) {
      if($benefaciary_details) {
        $benefaciary_id = $benefaciary_details[0]->id;
        $get_wallet_balance = $this->wallet_model->where('user_id',$benefaciary_id)->find_all();

        if(!empty($get_wallet_balance)) {
          $current_wallet_amt = $get_wallet_balance[0]->amt; 
        }
        else {
          //wallet creation
          $account_no = substr(hexdec(uniqid()), 4, 12);
          $new_wallet_entry = [
            "user_id" => $benefaciary_id,
            "amt" => '0.00',
            "status" => 'Y',
            "comment" => 'Account Created by'.$this->session->userdata('role_name'),
            "added_by" => $this->session->userdata('user_id'),
            "added_on" => date("Y-m-d H:i:s"),
            "added_by" => $this->session->userdata('user_id'),
            "is_deleted" => 'N',
            "account_no" => $account_no
          ];
          $new_wallet_id = $this->wallet_model->insert($new_wallet_entry);
          $current_wallet_amt = "0.00";
        }
        $amount_after_trasfer = $current_wallet_amt + $transfer_amount ;
        if($amount_after_trasfer <= WALLET_LIMIT) {
          $data['benefaciary_details'] = $benefaciary_details;
          $role_id = $this->session->userdata('role_id');
          if($role_id) {
            $get_role_details = $this->users_model->where('id',$user_id)->find_all();
            $from_id = $get_role_details[0]->id;
            $from_wallet = $this->wallet_model->where('user_id',$from_id)->find_all();
            if(!empty($from_wallet)) {
              $from_wallet_balance = $from_wallet[0]->amt;    
            }
            else
            {
              $from_wallet_balance = '0.00';
            }
            $data['transfer_from_firstname'] = $get_role_details[0]->first_name;
            $data['transfer_from_lastname'] = $get_role_details[0]->last_name;
            $data['transfer_from_userid'] = $get_role_details[0]->id;
            $data['transfer_to_userid'] = $benefaciary_id;
            $data['transfer_from_wallet_bal'] = $from_wallet_balance;
            $data['transfer_to_wallet_bal'] = $current_wallet_amt;                     
            $data['transfer_amount'] = $transfer_amount;
            $this->session->set_userdata('transfer_from_userid',$get_role_details[0]->id);
            $this->session->set_userdata('transfer_to_userid',$benefaciary_id);

            load_back_view(PROCEED_VIEW, $data);
          }
        } 
        else {
          $this->session->set_flashdata('msg_type', 'success');
          $this->session->set_flashdata('msg', 'Beneficiary Balance Should be less than 100000.');
          redirect(site_url() . '/admin/fund_transfer'); 
        }
      }
      else {
        $this->session->set_flashdata('msg_type', 'success');
        $this->session->set_flashdata('msg', 'Please Select Beneficiary');
        redirect(site_url() . '/admin/fund_transfer');
      }
    }
    else {
      $this->session->set_flashdata('msg_type', 'success');
      $this->session->set_flashdata('msg', 'You Dont have sufficient balance to transfer');
      redirect(site_url() . '/admin/fund_transfer');
    }
  }

  public function confirm_transaction() {
    $input = $this->input->post(NULL,TRUE);
    $from_userid = $input['from_userid'];
    $to_userid = $input['to_userid'];
    $from_bal = $input['from_bal'];
    $to_bal = $input['to_bal'];
    $transfer_amount = $input['transfer_amount'];
    $txpn = $input['txpn'];
    if(!empty($to_userid)) {
      $to_userdata = $this->users_model->where('id',$to_userid)->find_all();
      $to_firstname = $to_userdata[0]->first_name;
      $to_lastname = $to_userdata[0]->last_name;
      $to_userid = $to_userdata[0]->id;
    }
    if(!empty($from_userid)) {
      $from_userdata = $this->users_model->where('id',$from_userid)->find_all();
      $from_txpn = $from_userdata[0]->tpin;
      $from_userid = $from_userdata[0]->id;
    }

    if(!empty($txpn)) {
      $pos_encryption_key = $this->config->item("pos_encryption_key");
      $access_key = $pos_encryption_key;
      $tpin = $txpn;

      $encryptedText = rokad_encrypt($tpin,$access_key);

      if($encryptedText == $from_txpn) {
        if(!empty($from_bal) && !empty($to_bal)) {
          $to_total_bal_after_trans = $to_bal + $transfer_amount;
          if($to_total_bal_after_trans <= WALLET_LIMIT) {
            $from_wallet_details = $this->wallet_model->where('user_id',$from_userid)->find_all();
            $from_wallet_id = $from_wallet_details[0]->id;
            $to_wallet_details = $this->wallet_model->where('user_id',$to_userid)->find_all();
            $to_wallet_id = $to_wallet_details[0]->id;
            if($from_bal >= $transfer_amount) {
              $transaction_no = substr(hexdec(uniqid()), 4, 12);
              $wallet_trans_detail = [
                "w_id" => $from_wallet_id,
                "amt" => $transfer_amount,
                "merchant_amount" => '',
                "wallet_type" => "actual_wallet",
                "comment" => "Amount Transfer",
                "status" => 'Debited',
                "user_id" => $from_userid,
                "amt_before_trans" => $from_wallet_details[0]->amt,
                "amt_after_trans" => $from_wallet_details[0]->amt - $transfer_amount,
                "amt_transfer_from" => $from_userid,
                "amt_transfer_to" => $to_userid,
                "transaction_type_id" => '11',
		"transaction_type" => 'Debited',
                "is_status" => "Y",
                "added_on" => date("Y-m-d H:i:s"),
                "added_by" => $this->session->userdata('user_id'),
                "transaction_no" => $transaction_no
              ];
              $this->wallet_trans_model->insert($wallet_trans_detail);

              //Changes in wallet table
              $update_wallet = [
                "amt" => $from_bal - $transfer_amount
              ];
              $update_from_wallet = $this->wallet_model->update(array("user_id" => $from_userid),$update_wallet);

              if($update_from_wallet) {
                  $transaction_no = substr(hexdec(uniqid()), 4, 12);
                  $wallet_trans_detail_to = [
                  "w_id" => $to_wallet_id,
                  "amt" => $transfer_amount,
                  "wallet_type" => "actual_wallet",
                  "comment" => "Amount Transfer",
                  "status" => 'added',
                  "user_id" => $to_userid,
                  "amt_before_trans" => $to_wallet_details[0]->amt,
                  "amt_after_trans" => $to_wallet_details[0]->amt + $transfer_amount,
                  "amt_transfer_from" => $from_userid,
                  "amt_transfer_to" => $to_userid,
                //  "transaction_type_id" => '11',
                  "transaction_type_id" => '1',
                  "transaction_type" => 'Credited',
                  "is_status" => "Y",
                  "added_on" => date("Y-m-d H:i:s"),
                  "added_by" => $this->session->userdata('user_id'),
                  "transaction_no" => $transaction_no
                  ];
                  $this->wallet_trans_model->insert($wallet_trans_detail_to);

                  $update_wallet_array = [
                  "amt" => $to_bal + $transfer_amount
                  ];
                  $update_to_wallet = $this->wallet_model->update(array("user_id" => $to_userid),$update_wallet_array);

                  if($update_to_wallet) {
                    // SMS and Email

                    // SMS for transfer to
                    $TransferToUserData = $this->common_model->get_value('users','first_name,last_name,mobile_no','id='.$to_userid);
                    $TransferByUserData = $this->common_model->get_value('users','first_name,last_name','id='.$from_userid);
                    $sms_approval_by_array = array (
                                              "{{transfer_to_username}}" => $TransferToUserData->first_name.' '.$TransferToUserData->last_name,
                                              "{{transfer_by_username}}" => $TransferByUserData->first_name.' '.$TransferByUserData->last_name,
                                              "{{transaction_amount}}" => $transfer_amount,
                                              "{{wallet_amount}}" => $to_bal + $transfer_amount
                                          );
                    $msg_to_sent_admin = str_replace(array_keys($sms_approval_by_array),array_values($sms_approval_by_array),FUND_TRANSFER_TO);
                    $is_send     = sendSMS(rokad_decrypt($TransferToUserData->mobile_no,$this->config->item('pos_encryption_key')), $msg_to_sent_admin);
                    if($is_send) {
                      $data1['sms']       = "@#success#@";
                    } 

                    $data['amount'] = $transfer_amount;
                    $data['name'] = $to_firstname.' '.$to_lastname;
                    load_back_view(TRANSFER_SUCCESS_VIEW,$data);
                  }
                }
              }
              else {
                $this->session->set_flashdata('msg_type', 'success');
                $this->session->set_flashdata('msg', 'you dont have sufficiewnt balance to transfer amount');
                redirect(site_url() . '/admin/fund_transfer');
              }
            }
            else {
              $this->session->set_flashdata('msg_type', 'success');
              $this->session->set_flashdata('msg', 'benefaciary total Balance amount Should be less than 1,00,00');
              redirect(site_url() . '/admin/fund_transfer');
            }
          }
          else {
            $this->session->set_flashdata('msg_type', 'success');
            $this->session->set_flashdata('msg', 'Something Went wrong');
            redirect(site_url() . '/admin/fund_transfer/proceed');
          }
        }
        else {
          $this->session->set_flashdata('msg_type', 'success');
          $this->session->set_flashdata('msg', 'Please Provide Valid T-pin');
          redirect(site_url() . '/admin/fund_transfer');
        } 
      }
      else {
        $this->session->set_flashdata('msg_type', 'success');
        $this->session->set_flashdata('msg', 'Please Provide Valid T-pin');
        redirect(site_url() . '/admin/fund_transfer');

      }
    }  
public function check_tpn()
{
$input = $this->input->post(NULL,TRUE);
$id = $input['id'];
$txpn = $input['txpn'];

$pos_encryption_key = $this->config->item("pos_encryption_key");
$tpin = $txpn;

$encryptedText = rokad_encrypt($tpin,$pos_encryption_key);
$where = array('id' => $id,'tpin' => $encryptedText);
$check_tpn = $this->users_model->where($where)->find_all();
if(!empty($check_tpn))
{
$data['valid_tpn'] = 'true';
}
else
{
$data['valid_tpn'] = 'false';
}
data2json($data);
}


public function percentage()
{        
        // $this->load->model(array('services/service_2/service_2_commission_model','services/service_2/service_2_commission_detail_model'));
        $data=array();
        if($this->session->userdata('id') != "")
        {
            $user_id = $this->session->userdata('id');
            $user_role_id    = $this->session->userdata("role_id");
            $user_result = $this->users_model->where('id',$user_id)->as_array()->find_all();

            if(($this->session->userdata('role_id') == AREA_DISTRIBUTOR_ROLE_ID)
                || ($this->session->userdata('role_id') == DISTRIBUTOR)
                || ($this->session->userdata('role_id') == RETAILER_ROLE_ID) ) 
            {
                $level_user_id = $user_result[0]['level_3'];
            }
            if($this->session->userdata('role_id') == MASTER_DISTRIBUTOR_ROLE_ID) {
                $level_user_id = $user_id;
            }

                $assign_package_result = $this->assign_package_log_model->where('user_id',$level_user_id)->order_by("id", "desc")->limit('1')->as_array()->find_all();

                $package_id = $assign_package_result[0]['package_id'];

                if(!empty($package_id)) {

                    $package_result = $this->package_detail_model->where('package_id',$package_id)->as_array()->find_all();
                    $commission_id = $package_result[0]['commission_id'];
                    if(!empty($commission_id)) {

                        $commission_master_result = 
                        $this->service_2_commission_model->where('id',$commission_id)->as_array()->find_all();
                        $data['commission_name'] = $commission_master_result[0]['name'];
                        
                        $commission_detail_result = 
                        $this->service_2_commission_detail_model->where('commission_id',$commission_id)->as_array()->find_all();
                        $data['commission_detail_result'] = $commission_detail_result[0];
                        load_back_view(MY_COMMISSION, $data);

                    } else {
                    $this->session->set_flashdata(["msg" => "Commission detail not found. Please try again."]);
                    redirect(site_url() . '/admin/commission');
                }
                
             }else {
                    $this->session->set_flashdata(["msg" => "Commission detail not found. Please try again."]);
                    redirect(site_url() . '/admin/commission');
                }


        } else {
            $this->session->set_flashdata(["msg" => "Commission detail not found. Please try again."]);
                        redirect(site_url() . '/admin/commission');
        }
}

}
?>
