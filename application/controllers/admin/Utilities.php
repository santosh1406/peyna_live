<?php

//Created by harshada kulkarni on 24-07-2018 
class Utilities extends CI_Controller {

    public $sessionData;

    private $error_message; 

    public function __construct() {
        parent::__construct();
        $this->ci = & get_instance();
        $this->load->model(array("Utilities_model"));
        $this->load->library('form_validation');
        $this->load->helper('utilities_helper');

        $this->load->model(array('services/service_5/service_5_commission_detail_model', 'retailer_service_mapping_model', 'assign_package_log_model', 'package_detail_model', 'cb_msrtc_agent_transaction_model', 'tmp_agent_balance_track_model', 'cron/cron_model', 'payid_summary_details_model', 'payout_summary_details_model', 'payment_gateway_transaction_model', 'master_services_model', 'travel_destinations_model', 'tickets_model', 'api_balance_log_model', 'wallet_model', 'wallet_trans_model', 'wallet_topup_model', 'users_model', 'wallet_trans_log_model', 'common_model', 'commission_calculation_model', 'cb_commission_distribution_model', 'service_master_model'));
        is_logged_in();
        

        $this->sessionData['id'] = $this->session->userdata('user_id');
        $this->sessionData["username"] = $this->session->userdata('username');
        $this->sessionData["password"] = $this->session->userdata('password');

        $this->header = array(
            'Content-Type: multipart/form-data; charset=utf-8',
            'X-API-KEY: ' . X_API_KEY . '',
        );
    }
    //Created by harshada kulkarni on 24-07-2018 
    public function index() {

        $type = $this->db->query('select pm.id as provider_id, um.id as utility_id, pm.provider_name, um.utility_name from providers_master pm
            JOIN providers_utilities pu on pu.provider_id = pm.id
            JOIN utilities_master um on pu.utilities_id = um.id
            WHERE um.id = "' . MOBILE_PREPAID . '"' 
        )->result();
        $data['type'] = !empty($type[0]->provider_name) ? $type[0]->provider_name : 'JRI';
        $data['operators'] = json_decode(JRI_MOBILE_OPERATORS, true);
        $data['locations'] = json_decode(LOCATIONS, true);
        
        /* Redirect to Dashboard if agent has not enabled the service. - Gopal */
        $user_data = $this->session->userdata();
        $agent_id = $user_data['user_id'];
        $retailer_services = $this->retailer_service_mapping_model->getServiceByAgentId($agent_id);
        $service_array = array_column($retailer_services, 'service_id');

        // show($this->ci->session->flashdata('message'), 0, 'i ci session');
        // show($this->session->flashdata('message'), 0, 'i session');
        // $data['message'] = $this->ci->session->flashdata('message');
        // show($this->ci->session->flashdata('message'), 0, 'test');
        // show($data['message'], 1);
        // if($this->session->flashdata('message')){
        //     show($this->session->flashdata('message'), 1, 'message');
        // }

        if(in_array(JRI, $service_array)){
            load_back_view(PREPAID_RECHARGE_VIEW, $data);
        }else{
            $this->ci->session->set_flashdata('msg', 'No access to module');
            redirect(base_url().'admin/dashboard');
        }

    }
    //Created by harshada kulkarni on 24-07-2018 
    public function mobileOperators() {

        if (isset($_GET['term'])) {
            $input['term'] = strtolower($_GET['term']);
            $input['type'] = $_GET['type'];

            if (!empty($input)) {

                $curl_url = base_url() . "rest_server/utilities/mobileOperators";

                $curlResult = curlForPostData($curl_url, $input, $this->sessionData, $this->header);
                $output = json_decode($curlResult);
                $result = json_encode($output->data);

                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'tata_docomo_special.log', $curlResult, 'Mobile Operators');
                echo $result;
            }
        }
    }


    public function mobile_recharge(){
        
        $user_data = $this->session->userdata();
        $agent_id = $user_data['user_id'];
        $retailer_services = $this->retailer_service_mapping_model->getServiceByAgentId($agent_id);
        $service_array = array_column($retailer_services, 'service_id');
        if(in_array(RECHARGE, $service_array)){

            /* Get the proper aggregator for the selected service. - Gopal */
            $type = 'Recharge';
            $service = $this->service_master_model->get_aggregator($type);
            if($service == 'JRI'){
                $data['operators'] = json_decode(JRI_MOBILE_OPERATORS, true);
                $data['locations'] = json_decode(LOCATIONS, true);
                $view = PREPAID_RECHARGE_VIEW;
            }elseif($service == 'TSO'){
                $data['pre_operators'] = json_decode(TSO_PRE_MOBILE_OPERATORS, true);
                $data['post_operators'] = json_decode(TSO_POST_MOBILE_OPERATORS, true);
                $view = TSO_PREPAID_RECHARGE_VIEW;
            }
            load_back_view($view, $data);
        }else{
            /* Redirect to Dashboard if agent has not enabled the service. - Gopal */
            $this->ci->session->set_flashdata('msg', 'No access to module');
            redirect(base_url().'admin/dashboard');
        }
        
    }

    //Created by harshada kulkarni on 24-07-2018 
    public function recharge() {

        $data = array();
        $input = $this->input->post();
        // show($input, 1);
        $error_messages = json_decode(file_get_contents('messages.json'), true);

        if (!empty($input)) {

            $input['user_id'] = $this->sessionData['id'];

            $config = array(
                array('field' => 'recharge_type', 'label' => 'Recharge Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter recharge type.')),
                array('field' => 'mobile_operator', 'label' => 'Mobile Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter mobile operator.')),
                array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.')),
                array('field' => 'recharge_amount', 'label' => 'Recharge Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter recharge amount.'))
            );

            if (form_validate_rules($config)) {
                // show($input, 1);
                if($input['type'] == 'JRI'){
                    $curl_url = base_url() . "rest_server/utilities/jri_recharge";
                    // show($curl_url, 1);
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $input, 'Input-Operator');
                    $curlResult = curlForPostData($curl_url, $input, $this->sessionData, $this->header);
                    $output = json_decode($curlResult);
                    // $this->ci->output->clear_all_cache();
                    $this->session->set_tempdata('message', trim($output->msg), 3);
                    // $this->session->keep_flashdata('message');
                    
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $curlResult, 'JRI Recharge Response Final');


                }else{
                    $curl_url = base_url() . "rest_server/utilities/recharge";

                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'tata_docomo_special.log', $input, 'Input-Operator');
                    $curlResult = curlForPostData($curl_url, $input, $this->sessionData, $this->header);
                    $output = json_decode($curlResult);
                    $this->session->set_flashdata('message', $output->msg);

                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'tata_docomo_special.log', $curlResult, 'Web Recharge Response');
                }
                
            }
        }

        redirect(base_url() . 'admin/Utilities');
    }

    /**
     * @description - To Pay bill 
     * @param array of bill details
     * @added by Sachin on 14-08-2018
     * 
     */
    public function pay_bill() {
        $input = $this->input->post();
        $session_user_id = $this->session->userdata('user_id');
        if (!empty($input)) {
            $input['user_id'] = $session_user_id;
            $input['bill_amt'] = $this->session->userdata('bbps_amount');
            $type = $input['type'];
            $operatorId = $input['operator_id'];
            $config = $this->Utilities_model->get_validation($type, $operatorId);
            if (form_validate_rules($config)) {
                $curl_url = base_url() . "rest_server/utilities/get_bill";
                $curlResult = curlForPostData($curl_url, $input, $this->sessionData, $this->header);
                $output = json_decode($curlResult);
                $this->session->set_flashdata('message', $output->msg);
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'landline_bbps.log', $output, '4........pay_bill_final');
            }
        }
        redirect(base_url() . 'admin/Utilities/' . $type . 'bill');
    }

    /**
     * @description - To get bill info 
     * @param array of bill details
     * @added by Sachin on 14-08-2018
     * @return json data
     */
    public function get_bill() {
        $data = array();
        $input['recharge_from'] = isset($_POST['recharge_from']) ? $_POST['recharge_from'] : '';
        $input['operator'] = isset($_POST['operator']) ? $_POST['operator'] : '';
        $input['operator_id'] = isset($_POST['operator_id']) ? $_POST['operator_id'] : '';
        $input['type'] = isset($_POST['type']) ? $_POST['type'] : '';
        $input['number'] = isset($_POST['number']) ? $_POST['number'] : '';
        $input['account_number'] = isset($_POST['account_number']) ? $_POST['account_number'] : '';
        $input['comments'] = isset($_POST['comments']) ? $_POST['comments'] : '';
        $input['authenticator'] = isset($_POST['authenticator']) ? $_POST['authenticator'] : '';
        $session_user_id = $this->session->userdata('user_id');
        $input['user_id'] = $session_user_id;

        if (!empty($input)) {
            $config = $this->Utilities_model->get_validation($input['type'], $input['operator_id']);
            if (form_validate_rules($config) == FALSE) {
                $error = $this->form_validation->error_array();
                $error['status_field'] = 'Failed';
                $error['error_field'] = validation_errors("<div class='error'>", "</div>");
                echo json_encode($error);
                exit;
            } else {
                $curl_url = base_url() . "rest_server/utilities/get_bill";
                $response = curlForPostData($curl_url, $input, $this->sessionData, $this->header);
                $bbps_amount = json_decode($response, true);
                $bbps_amount = $bbps_amount['data']['check_result']['AMOUNT'];

                $this->session->set_userdata('bbps_amount', $bbps_amount);
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'landline_bbps.log', $response, 'get_bill');
                echo $response;
                exit;
            }
        }
    }

    /**
     * @description - Load landline utility page. 
     * @added by Sachin on 14-08-2018
     */
    public function landlinebill() {
        load_back_view(LANDLINE_VIEW);
    }

    /**
     * @description - Load electricity utility page. 
     * @added by Sachin on 14-08-2018
     */
    public function electricitybill() {
        load_back_view(ELECTRICITY_VIEW);
    }

    /**
     * @description - Load gas utility page. 
     * @added by Sachin on 14-08-2018
     */
    public function gasbill() {
        load_back_view(GAS_VIEW);
    }

    /**
     * @description - Load water bill utility page. 
     * @added by Sachin on 14-08-2018
     */
    public function waterbill() {
        load_back_view(WATER_VIEW);
    }

    /**
     * @description - Load broadband bill utility page. 
     * @added by Sachin on 14-08-2018
     */
    public function broadbandbill() {
        load_back_view(BROADBAND_VIEW);
    }

    public function dthOperators() {
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            //$type = $_GET['type'];
            $aRes = $this->Utilities_model->getDthOperators($q);
            return $aRes;
        }
    }

      public function dth() {

        // $type = $this->db->query('select pm.id as provider_id, um.id as utility_id, pm.provider_name, um.utility_name from providers_master pm
        //     JOIN providers_utilities pu on pu.provider_id = pm.id
        //     JOIN utilities_master um on pu.utilities_id = um.id
        //     WHERE um.id = "' . MOBILE_PREPAID . '"' 
        // )->result();
        // $data['type'] = !empty($type[0]->provider_name) ? $type[0]->provider_name : 'JRI';
        // $data['operators'] = json_decode(JRI_DTH_OPERATORS, true);
        // $data['locations'] = json_decode(LOCATIONS, true);
        // $user_data = $this->session->userdata();
        // $agent_id = $user_data['user_id'];
        // $retailer_services = $this->retailer_service_mapping_model->getServiceByAgentId($agent_id);
        // $service_array = array_column($retailer_services, 'service_id');;
        // if(in_array(JRI, $service_array)){
        //     load_back_view(DTH_VIEW, $data);
        // }else{
        //     $this->ci->session->set_flashdata('msg', 'No access to module');
        //     redirect(base_url().'admin/dashboard');
        // }

        $user_data = $this->session->userdata();
        $agent_id = $user_data['user_id'];
        $retailer_services = $this->retailer_service_mapping_model->getServiceByAgentId($agent_id);
        $service_array = array_column($retailer_services, 'service_id');
        if(in_array(RECHARGE, $service_array)){

            /* Get the proper aggregator for the selected service. - Gopal */
            $type = 'DTH';
            $service = $this->service_master_model->get_aggregator($type);
            // show($service, 1);
            if($service == 'JRI'){
                $data['operators'] = json_decode(JRI_DTH_OPERATORS, true);
                $view = DTH_VIEW;
            }elseif($service == 'TSO'){

                $data['operators'] = json_decode(TSO_DTH_OPERATORS, true);
                $view = TSO_DTH_VIEW;
            }
            $data['type'] = $service;            
            $data['locations'] = json_decode(LOCATIONS, true);
            // $view = DTH_VIEW;

            load_back_view($view, $data);
        }else{
            /* Redirect to Dashboard if agent has not enabled the service. - Gopal */
            $this->ci->session->set_flashdata('msg', 'No access to module');
            redirect(base_url().'admin/dashboard');
        }

    }


    public function dth_recharge() {
        $data = array();
        $input = $this->input->post();
        $session_user_id = $this->session->userdata('user_id');
        // show($input, 1);

        $error_messages = json_decode(file_get_contents('messages.json'), true);

        if (!empty($input)) {
            $input['user_id'] = $session_user_id;
            $config = array(
                array('field' => 'dth_operator', 'label' => 'DTH Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter dth operator.')),
                array('field' => 'customer_number', 'label' => 'Customer Number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please check Customer number.')),
                array('field' => 'recharge_amount', 'label' => 'Recharge Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter recharge amount.'))
            );


            if (form_validate_rules($config)) {
                if($input['type'] == 'JRI'){
                    $curl_url = base_url() . "rest_server/utilities/jri_dth";
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'jri_dth.log', $input, 'Input-Operator');
                }elseif($input['type'] == 'TSO') {
                    // curl_setopt($ch, CURLOPT_URL, base_url() . "rest_server/utilities/tso_dth");
                    $curl_url = base_url() . "rest_server/utilities/tso_dth";
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'tso_dth.log', $input, 'Input-Operator');
                }else{
                    $curl_url = base_url() . "rest_server/utilities/dth";
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'dth.log', $input, 'Input-Operator');
                }
                
                $curlResult = curlForPostData($curl_url, $input, $this->sessionData, $this->header);
                $output = json_decode($curlResult);
               //  show($output, 1);
                 $this->ci->session->set_flashdata('message', $output->msg);
                // $this->session->set_tempdata('message', trim($output->msg), 3);
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $curlResult, 'JRI Recharge Response Final');
            }
        }
        
        redirect(base_url() . 'admin/Utilities/dth');
    }

    function validate_member($str)
    {
       $input = $this->input->post(); 
       $field_value = $str; //this is redundant, but it's to show you how
       //the content of the fields gets automatically passed to the method
       $dth_operator = $input['dth_operator'];
       if(!empty($dth_operator)){

         switch ($dth_operator) {
             case 'Airtel TV DTH':
                 $max_length = 10;
                 show("^[3,7-9]{1}[0-9]{" . ($max_len-1) . "}$", 1);
                 $regex = preg_match("^[3,7-9]{1}[0-9]{" . ($max_len-1) . "}$", $str);
                 break;
             case 'RELIANCE BIG TV':
                 $max_length = 12;
                 $regex = preg_match("^[2,7-9]{1}[0-9]{" . ($max_len-1) . "}$", $str);
                 break;
             case 'SUN DIRECT':
                 $max_length = 11;
                 $regex = preg_match("^[1,4,7-9]{1}[0-9]{" . ($max_len-1) . "}$", $str);
                 break;
             case 'DISH TV DTH':
                 $max_length = 11;
                 $regex = preg_match("^[0,7-9]{1}[0-9]{" . ($max_len-1) . "}$", $str);
                 break;
             case 'TATA SKY':
                 $max_length = 10;
                 $regex = preg_match("^[1,7-9]{1}[0-9]{" . ($max_len-1) . "}$", $str);
                 break;
             case 'Videocon D2H':
                 $max_length = 10;
                 $regex = preg_match("^[7-9]{1}[0-9]{" . ($max_len-1) . "}$", $str);
                 break;
             default:
                 break;
         }

         show($regex, 1);

       }else{
         $this->error_message = 'Please select DTH Operator first';
       }

       show($field_value, 1);

       
    }


    //Created by Gopal Panadi on 05-11-2018 
    public function datacard() {

        $type = $this->db->query('select pm.id as provider_id, um.id as utility_id, pm.provider_name, um.utility_name from providers_master pm
            JOIN providers_utilities pu on pu.provider_id = pm.id
            JOIN utilities_master um on pu.utilities_id = um.id
            WHERE um.id = "' . DATACARD . '"' 
        )->result();
        $data['type'] = !empty($type[0]->provider_name) ? $type[0]->provider_name : 'JRI';
        $data['operators'] = json_decode(JRI_DATACARD_OPERATORS, true);
        $data['locations'] = json_decode(LOCATIONS, true);
        $user_data = $this->session->userdata();
        $agent_id = $user_data['user_id'];
        $retailer_services = $this->retailer_service_mapping_model->getServiceByAgentId($agent_id);
        $service_array = array_column($retailer_services, 'service_id');;
        if(in_array(JRI, $service_array)){
            load_back_view(DATACARD_RECHARGE_VIEW, $data);
        }else{
            $this->ci->session->set_flashdata('msg', 'No access to module');
            redirect(base_url().'admin/dashboard');
        }
        // load_back_view(DATACARD_RECHARGE_VIEW, $data);
    }

    public function datacard_recharge() {

        $data = array();
        $input = $this->input->post();
        // show($input, 1);
        $error_messages = json_decode(file_get_contents('messages.json'), true);

        if (!empty($input)) {

            $input['user_id'] = $this->sessionData['id'];

            $config = array(
                array('field' => 'mobile_operator', 'label' => 'Mobile Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter mobile operator.')),
                array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.')),
                array('field' => 'recharge_amount', 'label' => 'Recharge Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter recharge amount.'))
            );

            if (form_validate_rules($config)) {
                // show($input, 1);
                if($input['type'] == 'JRI'){

                    $curl_url = base_url() . "rest_server/utilities/datacard_recharge";
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_datacard_recharge.log', $input, 'Input-Operator');
                    $curlResult = curlForPostData($curl_url, $input, $this->sessionData, $this->header);
                    $output = json_decode($curlResult);
                    // show($output, 1);
                    $this->session->set_tempdata('message', trim($output->msg), 3);
                    
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $curlResult, 'JRI Recharge Response Final');
                }
                
            }
        }
        redirect(base_url() . 'admin/Utilities/datacard');
    }


    public function getDataPlans(){
        $input = $this->input->post();
        $curl_url = base_url() . "rest_server/utilities/getDataPlans";
        $curlResult = curlForPostData($curl_url, $input, $this->sessionData, $this->header);
        $output = $curlResult;
        $output = '{ "data": ' . $output . ' }';
        echo $output;
    }

    public function checkMNPJRI(){
        $input = $this->input->post();
        $curl_url = base_url() . "rest_server/utilities/checkMNPJRI";
        $curlResult = curlForPostData($curl_url, $input, $this->sessionData, $this->header);
        $output = $curlResult;
        // $output = '{"MobileNo":8655469196,"SystemReferenceNo":"5bfbd7322f613","CorpRefNo":"RMNP0000000166","CurrentOperator":"Vodafone","CurrentLocation":"Mumbai","PreviousOperator":"DOCOMO","PreviousLocation":"Mumbai","Ported":true,"Charged":0.05,"Error":""}';

        echo $output;
    }

}
