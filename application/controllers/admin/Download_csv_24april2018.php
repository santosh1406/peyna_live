<?php  defined('BASEPATH') OR exit('No direct script access allowed');

class Download_csv extends CI_Controller {

	public function __construct() { 
		parent::__construct();
		$this->load->library('datatables');	
		$this->load->model(array('Cb_bus_stops_model'));
        $this->load->helper('download');	
	}

    public function download() {
    	$data['bus_service'] = $this->Cb_bus_stops_model->get_bus_services();
		load_back_view(DOWNLOAD_CSV_VIEW,$data);            
	} 

	function get_route_list() {
        $input = $this->input->post();

        if(!empty($input['bus_service_id'])) {

            $route_info = $this->Cb_bus_stops_model->get_bus_services_routes($input['bus_service_id']);
            if(!empty($route_info)) {               
			   $data['type'] = "success";
			   $data['msg'] = $route_info;                
            }
            else {
                $data['type'] ="failed";
                $data['msg'] = "No Routes Found";
            }
        }     
        data2json($data);
    }

    function save_routes(){
        $input = $this->input->post();
       
        $config = array(
            array('field' => 'seller_point',
                'label' =>'Seller Point',
                'rules' =>'trim|required',
                'errors' => array(
                    'required' =>"Please select seller point")
                ),
            array('field' =>'route_no[]',
                'label' =>'Route No',
                'rules'=>'trim|required','errors' => array(
                    'required' =>"Please select route no.")
                )
            );

        if(form_validate_rules($config) == FALSE) {
       
            $data['error'] = $this->form_validation->error_array();
            load_back_view(DOWNLOAD_CSV_VIEW,$data);
           
        } else{
            if(!empty($input['route_no'])) {
                $route_file = $input['seller_point'].".csv.gz";
                $path = 'seller_point_files/';
                    //folder not exists    
                    if ((!file_exists($path))) {
                          mkdir($path,0777,true);
                    }

                   //Delete existing file.then generate new file.
                    if ((file_exists($path . $route_file))) {
                           unlink($path . $route_file);
                    }
                     $cnt=1;
                $final_route_string = "#### Bus Service Details ####\n";   
                foreach ($input['route_no'] as $key => $value) {

                    $route_query = $this->db->query("select * FROM msrtc_replication.bus_services WHERE ROUTE_NO ='".$value."' and EFFECTIVE_TILL >= CURDATE()");
                    $result = $route_query->result_array();
                    $head_string = "'".implode("','", array_keys($result[0]))."'";
                    //$head_string = implode(",", array_keys($result[0]));
                   
                    if($cnt==1) {
                        $final_route_string .= $head_string."\r\n";
                    }
                    $cnt++;
                    if(count($result) > 0) {
                        //$route_count = count($result);
                       // $route_string ="\n\n";
                        //$route_string .= "######## ".$value." Route Details ###### \r\n"; 
                        //Total Count:".$route_count."\r \n";
                        foreach ($result as $key => $val) {
                            $route_string .="'".implode("','", $val)."'\r\n";
                        }
                        //$enc_route_srting = msrtc_encrypt($route_string,$this->pos_encryption_key)."#\r\n";  
                    } 

                } 
                $final_route_string .= $route_string;
                $gzdata = gzencode($final_route_string, 9); 
                $fp = fopen($path . $route_file, "w");
                fwrite($fp, $gzdata);
                fclose($fp);

                if (is_file($path . $route_file)) {                            
                    header('Content-type: text/plain; charset=utf-8');
                }    
            } 
            force_download($path.$route_file, NULL);    
        }
          
    }


    /*function save_routes(){
    	$input = $this->input->post();
    	
    	$config = array(
    		array('field' => 'bus_service',
    			'label' =>'Bus Service',
    			'rules' =>'trim|required',
    			'errors' => array(
    				'required' =>"Please select bus service")
    			),
    		array('field' =>'route_no[]',
    			'label' =>'Route No',
    			'rules'=>'trim|required','errors' => array(
    				'required' =>"Please select route no.")
    			)
    		);

    	if(form_validate_rules($config) == FALSE) {
       
    		$data['error'] = $this->form_validation->error_array();
            load_back_view(DOWNLOAD_CSV_VIEW,$data);
           
    	} else{
            if(!empty($input['route_no'])) {

                foreach ($input['route_no']as $key => $value) {                   
                   
                    $route_file = $input['bus_service']. "_".$value . ".csv.gz";
                    $path = 'bus_service_files/';
                    //folder not exists    
                    if ((!file_exists($path))) {
                          mkdir($path,0777,true);
                    }

                   //Delete existing file.then generate new file.
                    if ((file_exists($path . $route_file))) {
                           unlink($path . $route_file);
                    }

            		$route_query = $this->db->query("select * FROM msrtc_replication.bus_services WHERE ROUTE_NO ='".$value."' and EFFECTIVE_TILL >= CURDATE()");
            		$result = $route_query->result_array();
            		if(count($result) > 0) {
            			$route_count = count($result);
            			$route_string .= "######### ".$input['bus_seervice']."Bus Service Details ######### \r\n ";
                        $route_string .= "######## Route Details ###### ".$route_count."\r \n";
                        $route_string .= "'".implode("','", array_keys($result[0]))."' \r\n";
                        foreach ($result as $key => $val) {
                            $route_string .="'".implode("','", $val)."' \r\n";
                        }
                        $enc_route_srting = msrtc_encrypt($route_string,$this->pos_encryption_key)."#\r\n";
                        $final_route_string = $route_string.$enc_route_srting;
                        $gzdata = gzencode($final_route_string, 9); 
                        $fp = fopen($path . $route_file, "w");
                        fwrite($fp, $gzdata);
                        fclose($fp);

                        if (is_file($path . $route_file)) {
                            // header('Content-type: application/octet-stream; charset=utf-8');
                            header('Content-type: text/plain; charset=utf-8');
                            // header('Content-Disposition: attachment; filename='.$path.$route_file);
                            // header('Content-Length: ' . filesize($path.$route_file));
                            // readfile($path.$route_file,false);
                        }                        
            		}
    		    }
            }
            echo("Done"); die;
    	}

    }*/



}
