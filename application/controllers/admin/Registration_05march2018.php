<?php
class Registration extends MY_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->library ( 'form_validation' );
		$this->load->library("Excel");
		$this->load->model ( array (
				'users_model',
				'role_model',
				'user_login_count_model',
				'wallet_model',
				'state_master_model',
				'city_master_model',
				'commission_model',
				'agent_commission_template_log_model',
				'user_email_model',
				'buisness_type_model' ,
				'common_model',
				'agent_bank_detail_model',
				'retailer_service_mapping_model'
		) );
	}
	
	/*
	 * @function : users
	 * @param :
	 * @detail : all users list.
	 *
	 */
	public function index() {
		$roles = "";
		$role_name = strtolower ( $this->session->userdata ( 'role_name' ) );
		if ($role_name == "admin" || $role_name == "superadmin") {
			$roles = $this->role_model->select ();
		}
		if($this->session->userdata('role_id') == TRIMAX_ROLE_ID || $this->session->userdata('role_id') == SUPPORT_ROLE_ID) {
			$data["role"] = $this->role_model->get_roles();
		} elseif($this->session->userdata('role_id') == COMPANY_ROLE_ID) {
			$data["role"] = $this->role_model->where_in('id',array(MASTER_DISTRIBUTOR_ROLE_ID,AREA_DISTRIBUTOR_ROLE_ID,DISTRIBUTOR,RETAILER_ROLE_ID))->as_array()->find_all();
		} elseif($this->session->userdata('role_id') == MASTER_DISTRIBUTOR_ROLE_ID) {
			$data["role"] = $this->role_model->where_in('id',array(AREA_DISTRIBUTOR_ROLE_ID,DISTRIBUTOR,RETAILER_ROLE_ID))->as_array()->find_all();
		} elseif($this->session->userdata('role_id') == AREA_DISTRIBUTOR_ROLE_ID) {
			$data["role"] = $this->role_model->where_in('id',array(DISTRIBUTOR,RETAILER_ROLE_ID))->as_array()->find_all();
		} elseif($this->session->userdata('role_id') == DISTRIBUTOR) {
			$data["role"] = $this->role_model->where_in('id',array(RETAILER_ROLE_ID))->as_array()->find_all();
		}
		$data ["roles"] = $roles;
		
		load_back_view ( ALL_ROKAD_USERS, $data );
	}
	
	/*
	 * @function : get_list
	 * @param :
	 * @detail : all users list according to role(operator,agent).
	 *
	 */
	function get_list() {
		is_ajax_request();
		$input = $this->input->post ( NULL, TRUE );
		$summary = $this->users_model->fetch_all_rokad_users ( $input, 'json' );
		echo $summary;
	}

	function get_export_list() {
		$input = $this->input->get ();
		$summary = $this->users_model->fetch_all_rokad_users( $input, 'array' );
		//show($input,1);
		if(count($summary) > 0)
        {
            foreach ($summary as $row=>$value)
            {   
                foreach ($value as $k => $v) {   
                    $data[] = array(
                        'User Id' => $v['id'],
                        'Display_name' => $v['display_name'],
                        'User Type' => $v['role_name'],
                        'RMN' =>  rokad_decrypt($v['mobile_no'], $this->config->item('pos_encryption_key')),
                        'Status' => $v['status'],
                        'RMN Verified' => $v['mobile_verified'],                
                        'KYC Status' => $v['kyc_status'],
                        'Email  Status' => $v['email_verify_status']
                    );               
                }
            }          
        }              
        $this->excel->data_2_excel('user_list_excel' . date('Y-m-d') . '_' . time() . '.xls', 
           $data);
        die;
	}

	public function company_registration() {
		$data = $this->company_registration_detail ();
		load_back_view ( MD_REGISTRATION_VIEW, $data );
	}
	private function company_registration_detail() {
		
		$data ['states'] = $this->state_master_model->get_all_state ();
		$data ['creat_role'] = COMPANY_ROLE_ID;
		$data ['level'] = COMPANY_LEVEL;
		$data ['role_id'] = rokad_encrypt ( COMPANY_ROLE_ID, $this->config->item ( 'pos_encryption_key' ) );
		$data ['cmp_on_change_flag'] = FALSE;
		$data ['md_on_change_flag']  = FALSE;
		$data ['ad_on_change_flag']  = FALSE;
		
		$data ['cmp_drop_down_flag'] = FALSE;
		$data ['md_drop_down_flag']  = FALSE;
		$data ['ad_drop_down_flag']  = FALSE;
		$data ['d_drop_down_flag']   = FALSE;
		
		return $data;
	}
	
	/*
	 * @Author : Rajkumar Shukla
	 * @function : md_registration_view
	 * @param : Should be superadmin or POS login.
	 * @detail : to register MD.
	 */
	public function md_registration() {
		$data = $this->md_registration_detail ();		
		load_back_view ( MD_REGISTRATION_VIEW, $data );
	}
	private function md_registration_detail() {
		
        $data = array();
		$data ['states'] = $this->state_master_model->get_all_state ();
		$data ['creat_role'] = MASTER_DISTRIBUTOR_ROLE_ID;
		$data ['level'] = MASTER_DISTRIBUTOR_LEVEL;
		$data ['role_id'] = rokad_encrypt ( MASTER_DISTRIBUTOR_ROLE_ID, $this->config->item ( 'pos_encryption_key' ) );
		$data ['all_cmp'] = $this->users_model->fetch_all_cmp();
		
		$data ['cmp_on_change_flag'] = FALSE;
		$data ['md_on_change_flag']  = FALSE;
		$data ['ad_on_change_flag']  = FALSE;		
		
		$data ['cmp_drop_down_flag'] = FALSE;
		$data ['md_drop_down_flag']  = FALSE;
		$data ['ad_drop_down_flag']  = FALSE;
		$data ['d_drop_down_flag']   = FALSE;
		
		if($this->session->userdata ( 'level' ) < COMPANY_LEVEL)
		{
			$data ['cmp_drop_down_flag'] = TRUE;
		}
		return $data;
	}
	
	/*
	 * @Author : Rajkumar Shukla
	 * @function : ad_registration
	 * @param : Should be superadmin or POS or MD login.
	 * @detail : to register AD.
	 */
	public function ad_registration() {
		$data = $this->ad_registration_detail ();
		//show($data,1);
		load_back_view ( MD_REGISTRATION_VIEW, $data );
		//show($data,1);
	}
	private function ad_registration_detail() {
		
		$data ['states'] = $this->state_master_model->get_all_state ();
		$data ['creat_role'] = AREA_DISTRIBUTOR_ROLE_ID;
		$data ['level'] = AREA_DISTRIBUTOR_LEVEL;
		$data ['role_id'] = rokad_encrypt ( AREA_DISTRIBUTOR_ROLE_ID, $this->config->item ( 'pos_encryption_key' ) );		
		
		$data ['cmp_on_change_flag'] = TRUE;
		$data ['md_on_change_flag']  = FALSE;
		$data ['ad_on_change_flag']  = FALSE;
		
		$data ['cmp_drop_down_flag'] = FALSE;
		$data ['md_drop_down_flag']  = FALSE;
		$data ['ad_drop_down_flag']  = FALSE;
		$data ['d_drop_down_flag']   = FALSE;
		
		$user_id = $this->session->userdata ('id');		
		if($this->session->userdata ( 'level' ) < COMPANY_LEVEL)
		{			
			$data ['cmp_drop_down_flag'] = TRUE;
			$data ['all_cmp'] = $this->users_model->fetch_all_cmp();
		}
		
		if($this->session->userdata ( 'level' ) < MASTER_DISTRIBUTOR_LEVEL)
		{			
			$data ['md_drop_down_flag'] = TRUE;
			$data ['all_mds'] = $this->users_model->fetch_all_md($user_id);
		}
		return $data;
	}
	
	/*
	 * @Author : Rajkumar Shukla
	 * @function : register_distributor
	 * @param : Should be upper level login to creat distributor.
	 * @detail : to register AD.
	 */
	public function register_distributor() {
		$data = $this->register_distributor_detail ();
		load_back_view ( MD_REGISTRATION_VIEW, $data );
	}
	private function register_distributor_detail() {
		
		$data ['states'] = $this->state_master_model->get_all_state ();
		$data ['creat_role'] = DISTRIBUTOR;
		$data ['level'] = POS_DISTRIBUTOR_LEVEL;
		$data ['role_id'] = rokad_encrypt ( DISTRIBUTOR, $this->config->item ( 'pos_encryption_key' ) );		
		
		$data ['cmp_on_change_flag'] = TRUE;
		$data ['md_on_change_flag']  = TRUE;
		$data ['ad_on_change_flag']  = FALSE;
		
		$data ['cmp_drop_down_flag'] = FALSE;
		$data ['md_drop_down_flag']  = FALSE;
		$data ['ad_drop_down_flag']  = FALSE;
		$data ['d_drop_down_flag']   = FALSE;
		
		$user_id = $this->session->userdata ('id');
		if($this->session->userdata ( 'level' ) < COMPANY_LEVEL)
		{
			$data ['cmp_drop_down_flag'] = TRUE;
			$data ['all_cmp'] = $this->users_model->fetch_all_cmp();
		}
		
		if($this->session->userdata ( 'level' ) < MASTER_DISTRIBUTOR_LEVEL)
		{
			$data ['md_drop_down_flag'] = TRUE;
			$data ['all_mds'] = $this->users_model->fetch_all_md($user_id);
		}
		
		if($this->session->userdata ( 'level' ) < AREA_DISTRIBUTOR_LEVEL)
		{
			$data ['ad_drop_down_flag'] = TRUE;
			$data ['area_distributor'] = $this->users_model->fetch_all_ad($user_id);
			
			//show($data ['area_distributor'], 1);
		}
		
		return $data;
	}
	
	/*
	 * @Author : Rajkumar Shukla
	 * @function : register_retailor
	 * @param : Should be upper level login to creat retailor.
	 * @detail : to register AD.
	 */
	public function register_retailor() {
		$data = $this->register_retailor_detail ();
		load_back_view ( MD_REGISTRATION_VIEW, $data );
	}
	private function register_retailor_detail() {
		$data['states'] = $this->state_master_model->get_all_state ();
		$data['btypes'] = $this->buisness_type_model->get_buisnessType ();
		$data['schemes'] = $this->buisness_type_model->get_schemes();
        $data['sellerpoints'] = $this->buisness_type_model->get_sellerpoints();
        $data['depots'] = $this->buisness_type_model->get_depots();
        $data['serviceDetails'] = $this->buisness_type_model->get_service();
        //show($data['serviceDetails'],1);
		$data['creat_role'] = RETAILER_ROLE_ID;
		$data['level'] = RETAILOR_LEVEL;
		$data['role_id'] = rokad_encrypt ( RETAILER_ROLE_ID, $this->config->item ( 'pos_encryption_key' ) );		
		
		$data ['cmp_on_change_flag'] = TRUE;
		$data ['md_on_change_flag']  = TRUE;
		$data ['ad_on_change_flag']  = TRUE;
		
		$data ['cmp_drop_down_flag'] = FALSE;
		$data ['md_drop_down_flag']  = FALSE;
		$data ['ad_drop_down_flag']  = FALSE;
		$data ['d_drop_down_flag']   = FALSE;

		$data['show_retailor_flag'] = TRUE;

		$user_id = $this->session->userdata ('id');
		if($this->session->userdata ( 'level' ) < COMPANY_LEVEL)
		{
			$data ['cmp_drop_down_flag'] = TRUE;
			$data ['all_cmp'] = $this->users_model->fetch_all_cmp();
		}
		
		if($this->session->userdata ( 'level' ) < MASTER_DISTRIBUTOR_LEVEL)
		{
			$data ['md_drop_down_flag'] = TRUE;
			$data ['all_mds'] = $this->users_model->fetch_all_md($user_id);
		}
		
		if($this->session->userdata ( 'level' ) < AREA_DISTRIBUTOR_LEVEL)
		{
			$data ['ad_drop_down_flag'] = TRUE;
			$data ['area_distributor'] = $this->users_model->fetch_all_ad($user_id);
		}
		
		if($this->session->userdata ( 'level' ) < POS_DISTRIBUTOR_LEVEL)
		{
			$data ['d_drop_down_flag'] = TRUE;
			$data ['distributors'] = $this->users_model->fetch_all_distributors($user_id);
		}	
		return $data;
	}
	
	/*
	 * @Author : Rajkumar Shukla
	 * @function : is_registered
	 * @param : mobile no.
	 * @detail : To check the mobile no is unique.
	 */
	function is_mobile_no_registered() {
		$input = $this->input->post ( NULL, TRUE );
		$is_exsit = $this->users_model->check_mobile_no_registered ( $input );
		echo $is_exsit;
	}
	
	/*
	 * @Author : Rajkumar Shukla
	 * @function : is_email_registered
	 * @param : email id.
	 * @detail : To check the email id is unique.
	 */
	function is_email_registered() {
		$input = $this->input->post ( NULL, TRUE );
		$is_exsit = $this->users_model->check_email_registered ( $input );
		echo $is_exsit;
	}
	
	/*
	 * @function : saveuser
	 * @param :
	 * @detail : to save user.
	 */
	public function saveuser() {
		$input = $this->input->post ( NULL, TRUE );
		//show($input,1);
		if(!empty($input['service_name']))
		{
		   $service_name = $input['service_name'];
		}else{
		   $service_name ='';
		}	
		//show($service_name,1);
	    logging_data ( "registration/registration_log.log", $input, "Post data for registration" );
		$encrypted_role_id = isset ( $input ['role'] ) ? trim ( $input ['role'] ) : "";
		$level = isset ( $input ['level'] ) ? trim ( $input ['level'] ) : "";
		$firm_name = isset ( $input ['firm_name'] ) ? trim ( $input ['firm_name'] ) : "";
		$first_name = isset ( $input ['first_name'] ) ? trim ( $input ['first_name'] ) : "";
		$last_name = isset ( $input ['last_name'] ) ? trim ( $input ['last_name'] ) : "";
		$display_name = $first_name . " " . $last_name;
		$email = isset ( $input ['email'] ) ? trim ( $input ['email'] ) : "";
		$company_gst_no = isset ( $input ['company_gst_no'] ) ? trim ( $input ['company_gst_no'] ) : "";
		$mobile_no = isset ( $input ['mobile_no'] ) ? trim ( $input ['mobile_no'] ) : "";
		$gender = isset ( $input ['gender'] ) ? trim ( $input ['gender'] ) : "";
		$dob = isset ( $input ['dob'] ) ? date ( 'Y-m-d', strtotime ( $input ['dob'] ) ) : "";
		$address1 = isset ( $input ['address1'] ) ? trim ( $input ['address1'] ) : "";
		$address2 = isset ( $input ['address2'] ) ? trim ( $input ['address2'] ) : "";
		$agent_state = isset ( $input ['agent_state'] ) ? trim ( $input ['agent_state'] ) : "";
		$agent_city = isset ( $input ['agent_city'] ) ? trim ( $input ['agent_city'] ) : "";
		$pincode = isset ( $input ['pincode'] ) ? trim ( $input ['pincode'] ) : "";
		$pancard = isset ( $input ['pancard'] ) ? trim ( $input ['pancard'] ) : "";
		$adharcard= isset ( $input ['adharcard'] ) ? trim ( $input ['adharcard'] ) : "";
		$country_id = COUNTRY_ID;		
		$level1 = $this->users_model->get_trimax_user_id();
		
		$session_level = $this->session->userdata ( 'level' );
		$session_id = $this->session->userdata ( 'id' );
		
		//show ( $session_level);
		//show ( $session_id, 1 );
		
		if ($session_level == MASTER_DISTRIBUTOR_LEVEL) {
			$session_cmp_id = $this->users_model->get_parent_id ( $session_id, COMPANY_LEVEL );
		} else if ($session_level == AREA_DISTRIBUTOR_LEVEL) {
			$session_md_id = $this->users_model->get_parent_id ( $session_id, MASTER_DISTRIBUTOR_LEVEL);
			$session_cmp_id = $this->users_model->get_parent_id ( $session_md_id, COMPANY_LEVEL );
		} else if ($session_level == POS_DISTRIBUTOR_LEVEL) {
			$session_ad_id = $this->users_model->get_parent_id ( $session_id, AREA_DISTRIBUTOR_LEVEL);
			$session_md_id = $this->users_model->get_parent_id ( $session_ad_id, MASTER_DISTRIBUTOR_LEVEL);
			$session_cmp_id = $this->users_model->get_parent_id ( $session_md_id, COMPANY_LEVEL);
		}	
		
		if ($level == COMPANY_LEVEL) {						
			$level2 = NULL;
			$level3 = NULL;
			$level4 = NULL;
			$level5 = NULL;
			$level6 = NULL;			
		} else if ($level == MASTER_DISTRIBUTOR_LEVEL) {
			if ($session_level == COMPANY_LEVEL) {				
				$level2 = isset ( $session_id ) ? trim ( $session_id ) : NULL;
				$level3 = NULL;
				$level4 = NULL;
				$level5 = NULL;
				$level6 = NULL;
			} else {				
				$level2 = isset ( $input ['cmp'] ) ? trim ( $input ['cmp'] ) : NULL;
				$level3 = NULL;
				$level4 = NULL;
				$level5 = NULL;
				$level6 = NULL;
			}
		} else if ($level == AREA_DISTRIBUTOR_LEVEL) {			
			if ($session_level == COMPANY_LEVEL) {				
				$level2 = isset ( $session_id ) ? trim ( $session_id ) : NULL;
				$level3 = isset ( $input ['md'] ) ? trim ( $input ['md'] ) : NULL;
				$level4 = NULL;
				$level5 = NULL;
				$level6 = NULL;
			} else if ($session_level == MASTER_DISTRIBUTOR_LEVEL) {				
				$level2 = isset ( $session_cmp_id ) ? trim ( $session_cmp_id ) : NULL;
				$level3 = isset ( $session_id ) ? trim ( $session_id ) : NULL;
				$level4 = NULL;
				$level5 = NULL;
				$level6 = NULL;
			} else {				
				$level2 = isset ( $input ['cmp'] ) ? trim ( $input ['cmp'] ) : NULL;
				$level3 = isset ( $input ['md'] ) ? trim ( $input ['md'] ) : NULL;
				$level4 = NULL;
				$level5 = NULL;
				$level6 = NULL;
			}
		} else if ($level == POS_DISTRIBUTOR_LEVEL) {			
			if ($session_level == COMPANY_LEVEL) {				
				$level2 = isset ( $session_id ) ? trim ( $session_id ) : NULL;
				$level3 = isset ( $input ['md'] ) ? trim ( $input ['md'] ) : NULL;
				$level4 = isset ( $input ['ad'] ) ? trim ( $input ['ad'] ) : NULL;
				$level5 = NULL;
				$level6 = NULL;
			} else if ($session_level == MASTER_DISTRIBUTOR_LEVEL) {				
				$level2 = isset ( $session_cmp_id ) ? trim ( $session_cmp_id ) : NULL;
				$level3 = isset ( $session_id ) ? trim ( $session_id ) : NULL;
				$level4 = isset ( $input ['ad'] ) ? trim ( $input ['ad'] ) : NULL;
				$level5 = NULL;
				$level6 = NULL;
			} else if ($session_level == AREA_DISTRIBUTOR_LEVEL) {				
				$level2 = isset ( $session_cmp_id ) ? trim ( $session_cmp_id ) : NULL;
				$level3 = isset ( $session_md_id ) ? trim ( $session_md_id ) : NULL;
				$level4 = isset ( $session_id ) ? trim ( $session_id ) : NULL;
				$level5 = NULL;
				$level6 = NULL;
			} else {				
				$level2 = isset ( $input ['cmp'] ) ? trim ( $input ['cmp'] ) : NULL;
				$level3 = isset ( $input ['md'] ) ? trim ( $input ['md'] ) : NULL;
				$level4 = isset ( $input ['ad'] ) ? trim ( $input ['ad'] ) : NULL;
				$level5 = NULL;
				$level6 = NULL;
			}
		} else if ($level == RETAILOR_LEVEL) {
			if ($session_level == COMPANY_LEVEL) {				
				$level2 = isset ( $session_id ) ? trim ( $session_id ) : NULL;
				$level3 = isset ( $input ['md'] ) ? trim ( $input ['md'] ) : NULL;
				$level4 = isset ( $input ['ad'] ) ? trim ( $input ['ad'] ) : NULL;
				$level5 = isset ( $input ['distributor'] ) ? trim ( $input ['distributor'] ) : NULL;
				$level6 = NULL;
			} else if ($session_level == MASTER_DISTRIBUTOR_LEVEL) {				
				$level2 = isset ( $session_cmp_id ) ? trim ( $session_cmp_id ) : NULL;
				$level3 = isset ( $session_id ) ? trim ( $session_id ) : NULL;
				$level4 = isset ( $input ['ad'] ) ? trim ( $input ['ad'] ) : NULL;
				$level5 = isset ( $input ['distributor'] ) ? trim ( $input ['distributor'] ) : NULL;
				$level6 = NULL;
			} else if ($session_level == AREA_DISTRIBUTOR_LEVEL) {				
				$level2 = isset ( $session_cmp_id ) ? trim ( $session_cmp_id ) : NULL;
				$level3 = isset ( $session_md_id ) ? trim ( $session_md_id ) : NULL;
				$level4 = isset ( $session_id ) ? trim ( $session_id ) : NULL;
				$level5 = isset ( $input ['distributor'] ) ? trim ( $input ['distributor'] ) : NULL;
				$level6 = NULL;
			} else if ($session_level == POS_DISTRIBUTOR_LEVEL) {				
				$level2 = isset ( $session_cmp_id ) ? trim ( $session_cmp_id ) : NULL;
				$level3 = isset ( $session_md_id ) ? trim ( $session_md_id ) : NULL;
				$level4 = isset ( $session_ad_id ) ? trim ( $session_ad_id ) : NULL;
				$level5 = isset ( $session_id ) ? trim ( $session_id ) : NULL;
				$level6 = NULL;
			} else {				
				$level2 = isset ( $input ['cmp'] ) ? trim ( $input ['cmp'] ) : NULL;
				$level3 = isset ( $input ['md'] ) ? trim ( $input ['md'] ) : NULL;
				$level4 = isset ( $input ['ad'] ) ? trim ( $input ['ad'] ) : NULL;
				$level5 = isset ( $input ['distributor'] ) ? trim ( $input ['distributor'] ) : NULL;
				$level6 = NULL;
			}
		}
		
		if ($level == MASTER_DISTRIBUTOR_LEVEL) {
			$active_commison = $this->commission_model->commission_id ();
		}
		// encryption done here
		$new_pancard = rokad_encrypt ( $pancard, $this->config->item ( "pos_encryption_key" ) );
		$new_adharcard = rokad_encrypt ( $adharcard, $this->config->item ( "pos_encryption_key" ) );
		$new_mobile = rokad_encrypt ( $mobile_no, $this->config->item ( "pos_encryption_key" ) );
		$new_pincode = rokad_encrypt ( $pincode, $this->config->item ( "pos_encryption_key" ) );
		$new_email = rokad_encrypt ( $email, $this->config->item ( "pos_encryption_key" ) );
		$new_dob = rokad_encrypt ( $dob, $this->config->item ( "pos_encryption_key" ) );
		// decryption done herer
		$role_id = rokad_decrypt ( $encrypted_role_id, $this->config->item("pos_encryption_key"));
        
	   // if($input['creat_role'] == RETAILER_ROLE_ID)
	   // {
		    $ac_no = trim($input['accno']);
	        $bank_nm = trim($input['bname']);
	        $bran_nm = trim($input['brname']);
	        $ifsc_code = trim($input['ifsc']);
	        $accname = trim($input['accname']);
       //}

	    $businesstype = trim($input['bType']);
        $scheme = isset($input['scheme']) ? trim($_POST['scheme']) : "";
            if($_POST['sellerpoint'] !=''){
        $sellercode = trim($_POST['sellerpoint']) ;
        $selle_rname = $this->common_model->get_value('cb_bus_stops', 'BUS_STOP_NM', 'BUS_STOP_CD="'.$sellercode.'"');
        $sellername = $selle_rname->BUS_STOP_NM;
            }else{
        $sellercode = "" ;
        $sellername = "" ;
            }
        $sellerdescription = isset($input['sellerdesc']) ? trim($_POST['sellerdesc']) : "";
        $topUpLimit = isset($input['topUpLimit']) ? trim($_POST['topUpLimit']) : "";
        $hhmLimit = isset($input['hhmLimit']) ? trim($_POST['hhmLimit']) : "";
        if($_POST['depot_code'] !=''){
        $depot_code = trim($_POST['depot_code']);
        $depot_name = $this->common_model->get_value('cb_depots', 'DEPOT_NM', 'DEPOT_CD="'.$depot_code.'"');
        $depotname = $depot_name->DEPOT_NM;
            }else{
        $depot_code = "" ;
        $depot_name = "" ;
            }
        $securityDep = isset($input['securityDep']) ? trim($_POST['securityDep']) : "";


		$config = array (
				array (
						'field' => 'firm_name',
						'label' => 'Name of Firm/Company',
						'rules' => 'trim|xss_clean|required',
						'errors' => array (
								'required' => 'Please enter name of firm/company',
								'xss_clean' => 'CSRF is not allowed' 
						) 
				),
				array (
						'field' => 'first_name',
						'label' => 'First Name',
						'rules' => 'trim|xss_clean|required',
						'errors' => array (
								'required' => 'Please enter first name' 
						) 
				),
				array (
						'field' => 'last_name',
						'label' => 'Last Name',
						'rules' => 'trim|xss_clean|required',
						'errors' => array (
								'required' => 'Please enter last nanme' 
						) 
				),
				array (
						'field' => 'email',
						'label' => 'Email',
						'rules' => 'trim|xss_clean|required|valid_email|is_unique[users.email]',
						'errors' => array (
								'required' => 'Please enter valid email',
								'valid_email' => 'Please enter the valid email',
								'is_unique' => 'Email id already exsist with us' 
						) 
				),
				array (
						'field' => 'mobile_no',
						'label' => 'Mobile Numebr',
						'rules' => 'trim|xss_clean|required|min_length[10]|max_length[10]|callback_mobileno_check' 
				),
				array (
						'field' => 'gender',
						'label' => 'Gender',
						'rules' => 'trim|xss_clean' 
				),
				array (
						'field' => 'dob',
						'label' => 'Date of birth',
						'rules' => 'trim|xss_clean|required|callback_validate_age',
						'errors' => array (
								'required' => 'Please select your age' 
						) 
				),
				array (
						'field' => 'address1',
						'label' => 'Address1',
						'rules' => 'trim|xss_clean|required',
						'errors' => array (
								'required' => 'Please enter the address' 
						) 
				),
				array (
						'field' => 'address2',
						'label' => 'Address2',
						'rules' => 'trim|xss_clean' 
				),
				array (
						'field' => 'agent_state',
						'label' => 'Agent State',
						'rules' => 'trim|xss_clean|required',
						'errors' => array (
								'required' => 'Please select the state' 
						) 
				),
				array (
						'field' => 'agent_city',
						'label' => 'Agent City',
						'rules' => 'trim|xss_clean|required',
						'errors' => array (
								'required' => 'Please select the city' 
						) 
				),
				array (
						'field' => 'pincode',
						'label' => 'Pincode',
						'rules' => 'trim|xss_clean|required|numeric|min_length[6]|max_length[6]',
						'errors' => array (
								'required' => 'Please enter the pin code' 
						) 
				),	
				array (
						'field' => 'company_gst_no',
						'label' => 'company_gst_no',
						'rules' => 'trim|xss_clean|required|min_length[15]|max_length[15]',
						'errors' => array (
								'required' => 'Please enter valid Company GST No' 
						) 
				),			
				array (
						'field' => 'pancard',
						'label' => 'Pancard',
						'rules' => 'trim|xss_clean|required|min_length[10]|max_length[10]',
						'errors' => array (
								'required' => 'Please enter valid Pancard Number' 
						) 
				),
				array (
						'field' => 'adharcard',
						'label' => 'Aadhaar Card',
						'rules' => 'trim|xss_clean|required|min_length[12]|max_length[12]',
						'errors' => array (
								'required' => 'Please enter valid Aadhaar Card Number'
						)
				)
		);
		// logging_data("registration/" . date("M") . "/registration_log.log", $config, "Validation error here");
		$agent_cd = $this->users_model->generate_agent_id();

		if($input['creat_role'] != '' && $input['creat_role'] == RETAILER_ROLE_ID)
		{
			$agent_cd = $this->users_model->generate_agent_id();
		}
		else{
			$agent_cd = '';
		}

		if (form_validate_rules ( $config )) {
			
			$this->users_model->agent_code = $agent_cd;
			$this->users_model->role_id = $role_id;
			$this->users_model->username = $new_mobile;
			$this->users_model->firm_name = $firm_name;
			$this->users_model->first_name = $first_name;
			$this->users_model->last_name = $last_name;
			$this->users_model->gender = $gender;
			$this->users_model->email = $new_email;
			$this->users_model->company_gst_no = $company_gst_no;
			$this->users_model->address1 = $address1;
			$this->users_model->address2 = $address2;
			$this->users_model->pincode = $new_pincode;
			$this->users_model->pancard = $new_pancard;
			$this->users_model->adharcard = $new_adharcard;
			$this->users_model->mobile_no = $new_mobile;
			$this->users_model->country = $country_id;
			$this->users_model->state = $agent_state;
			$this->users_model->city = $agent_city;
			$this->users_model->dob = $new_dob;
			$this->users_model->display_name = $display_name;
			$this->users_model->kyc_verify_status = "N";
			$this->users_model->email_verify_status = "N";
			$this->users_model->verify_status = "N";
			$this->users_model->status = "N";
			$this->users_model->is_deleted = "N";
			$this->users_model->level = $level;
			$this->users_model->level_1 = $level1; // here only for staging.
			$this->users_model->level_2 = $level2;
			$this->users_model->level_3 = $level3;
			$this->users_model->level_4 = $level4;
			$this->users_model->level_5 = $level5;
			$this->users_model->level_6 = $level6;

			$this->users_model->btypeid = $businesstype;
            $this->users_model->schemeid = $scheme;
            $this->users_model->topuplmit = $topUpLimit;
            $this->users_model->hmmlimit = $hhmLimit;
            $this->users_model->securitydeposite = $securityDep;
            $this->users_model->star_seller_code = $sellercode;
            $this->users_model->star_seller_name = $sellername;
            $this->users_model->location = $sellerdescription;
            $this->users_model->depot_cd = $depot_code;
            $this->users_model->depot_name = $depotname;

			$this->users_model->inserted_by = $this->session->userdata ( "user_id" );
			$this->users_model->created_date = date ( "Y-m-d h:i:s:a" );
			$this->users_model->ip_address = $this->input->ip_address();
			$this->users_model->user_agent = $this->input->user_agent();
			if ($level == MASTER_DISTRIBUTOR_LEVEL) {
				$this->users_model->commission_id = $active_commison [0]->id;
			}			
			
			//show('object_ready', 1);
			
			$last_insert_id_users = $this->users_model->save ();
			if (! empty ( $last_insert_id_users )) {
				// agent bank details
				//if($input['creat_role'] != '' && $input['creat_role'] == RETAILER_ROLE_ID)
				//{
		            $this->agent_bank_detail_model->agent_id = $last_insert_id_users;
		            $this->agent_bank_detail_model->bank_acc_name = $accname;
		            $this->agent_bank_detail_model->bank_acc_no = $ac_no;
		            $this->agent_bank_detail_model->bank_name = $bank_nm;
		            $this->agent_bank_detail_model->bank_branch = $bran_nm;
		            $this->agent_bank_detail_model->bank_ifsc_code = $ifsc_code;
		            $this->agent_bank_detail_model->inserted_by = $this->session->userdata('user_id');
		            $this->agent_bank_detail_model->is_status = 1;
		            $this->agent_bank_detail_model->inserted_date = date('Y-m-d H:i:s' );
		            $this->agent_bank_detail_model->is_deleted = 0;
		            $this->agent_bank_detail_model->save();
				//}
				if($input['creat_role'] == RETAILER_ROLE_ID && !empty($service_name))
				{
					foreach($service_name as $service_id) 
			        {
					    $data= array(
					        'agent_id' => $last_insert_id_users,
					        'service_id' => $service_id,
					        'created_by' => $this->session->userdata('user_id'),
					    );
			           $insert_service = $this->retailer_service_mapping_model->saveServiceDetails($data);
				    }
				}
				$account_no = substr ( hexdec ( uniqid () ), 4, 12 );
				$wallet_array = [ 
						'user_id' => $last_insert_id_users,
						'amt' => 0.00,
						'status' => "Y",
						'comment' => "Created Account",
						'added_by' => $this->session->userdata ( 'user_id' ),
						'updated_by' => $this->session->userdata ( 'user_id' ),
						'account_no' => $account_no 
				];
				
				$wallet_id = $this->wallet_model->insert ( $wallet_array );
				if (! empty ( $active_commison )) {
					$agent_commission_log = [ 
							"user_id" => $last_insert_id_users,
							"commission_id" => $active_commison [0]->id,
							"commission_from" => $active_commison [0]->from,
							"commission_to" => $active_commison [0]->till,
							"status" => '1',
							"added_on" => date ( 'Y-m-d H:i:s' ),
							"added_by" => $this->session->userdata ( "user_id" ) 
					];
					$this->agent_commission_template_log_model->insert ( $agent_commission_log );
				}
				if (! empty ( $wallet_id )) {
					// Send sms
					$reg_sms_array = array (
							"{{username}}" => ucwords ( $display_name ),
							"{{email}}" => $email 
					);
					$temp_sms_msg = str_replace ( array_keys ( $reg_sms_array ), array_values ( $reg_sms_array ), BOS_REGISTRATION );
					$is_send = sendSMS ( $mobile_no, $temp_sms_msg );
					
					$response = save_n_send_activation_key ( $last_insert_id_users, $email, $display_name, 'email_verification' );
					$this->session->set_flashdata ( 'msg_type', 'success' );
					$this->session->set_flashdata ( 'msg', 'Registration completed successfully. Please check your registered Email ID for activation' );
					redirect ( base_url () . 'admin/registration' );
				}
			}
		} else {
			$input = $this->input->post ( NULL, TRUE );
			eval ( LEVEL_CHAIN_CONSTANTS );
			$data ['creat_role'] = $input ['creat_role'];
			$data ['level'] = $input ['level'];
			$data ['role'] = $input ['role'];
			$data ['md_drop_down_flag'] = $input ['md_drop_down_flag'];
			$data ['ad_drop_down_flag'] = $input ['ad_drop_down_flag'];
			$data ['d_drop_down_flag'] = $input ['d_drop_down_flag'];
			//$data ['all_cmp'] = $this->users_model->fetch_all_cmp();
			/* if (! empty ( $data ['ad_drop_down_flag'] ) && in_array ( $session_level, $level_chain_constants )) {
				$data ['area_distributor'] = $this->users_model->get_ad_data_by_id ( $session_id );
			}
			if (! empty ( $data ['d_drop_down_flag'] ) && in_array ( $session_level, $level_chain_constants )) {
				$data ['distributor'] = $this->users_model->get_distributor_data_by_id ( $session_id );
			} */
			$data ['states'] = $this->state_master_model->get_all_state ();
			$data ['all_cmp'] = $this->users_model->fetch_all_cmp();
			load_back_view ( MD_REGISTRATION_VIEW, $data );
		}
	}
	
	/*
	 * @Author : Rajkumar Shukla
	 * @function : mobileno_check
	 * @param : $mobile no.
	 * @detail : To chcek the server side duplicate entery.
	 */
	public function mobileno_check($mobileno) {
		$chk_mob = $this->users_model->where ( array (
				'mobile_no' => $mobileno,
				"is_deleted" => 'N' 
		) )->find_all ();
		if ($chk_mob) {
			$this->form_validation->set_message ( 'mobileno_check', 'The mobile no allready exsits with us.' );
			return FALSE;
		} else {
			return TRUE;
		}
	}
	
	/*
	 * @Author : Rajkumar Shukla
	 * @function : validate_age
	 * @param : $age.
	 * @detail : To chcek the server side age shuld 18.
	 */
	public function validate_age($age) {
		$dob = new DateTime ( $age );
		$now = new DateTime ();
		if ($now->diff ( $dob )->y >= 18) {
			return TRUE;
		} else {
			$this->form_validation->set_message ( 'validate_age', 'Entered Age DOB should be above 18 years' );
			return FALSE;
		}
	}
	
	
	public function get_md_data() {
		$data = array ();
		$input = array ();
		$new_data ['md'] = array ();
		$input = $this->input->post ( NULL, TRUE );
		$data ['md'] = $this->users_model->get_md_data( $input ['id'] );
		if (! empty ( $data ['md'] )) {
			foreach ( $data ['md'] as $detail ) {
				$detail->mobile_no = rokad_decrypt ( $detail->mobile_no, $this->config->item ( 'pos_encryption_key' ) );
				$new_data ['md'] [] = $detail;
			}
		}
		data2json ( $new_data );
	}
	
	/*
	 * @Author : Rajkumar Shukla
	 * @function : get_ad_data
	 * @param : MD id.
	 * @detail : To fetch the area distributor details.
	 */
	public function get_ad_data() {
		$data = array ();
		$input = array ();
		$new_data ['ad'] = array ();
		$input = $this->input->post ( NULL, TRUE );
		$data ['ad'] = $this->users_model->get_ad_data ( $input ['id'] );
		if (! empty ( $data ['ad'] )) {
			foreach ( $data ['ad'] as $detail ) {
				$detail->mobile_no = rokad_decrypt ( $detail->mobile_no, $this->config->item ( 'pos_encryption_key' ) );
				$new_data ['ad'] [] = $detail;
			}
		}
		data2json ( $new_data );
	}
	
	/*
	 * @Author : Rajkumar Shukla
	 * @function : get_distributor
	 * @param : AD id.
	 * @detail : To fetch the distributor details.
	 */
	public function get_distributor() {
		$data = array ();
		$new_data ['distributor'] = array ();
		$input = $this->input->post ( NULL, TRUE );
		$data ['distributor'] = $this->users_model->get_distributor_data ( $input ['id'] );
		if (! empty ( $data ['distributor'] )) {
			foreach ( $data ['distributor'] as $detail ) {
				$detail->mobile_no = rokad_decrypt ( $detail->mobile_no, $this->config->item ( 'pos_encryption_key' ) );
				$new_data ['distributor'] [] = $detail;
			}
		}
		data2json ( $new_data );
	}
	
	/*
	 * @function : disable_user
	 * @param :
	 * @detail : soft delete user.
	 *
	 */
	public function disable_user() {
		$input = $this->input->post ( NULL, TRUE );
		$this->users_model->id = $input ["id"];
		$rdata = $this->users_model->select ();
		if (! empty ( $input ['postyn'] )) {
			$this->users_model->id = $input ["id"];
			$this->users_model->status = $input ["postyn"];
			$id = $this->users_model->save ();
			
			// clear login attempts when user is enabled
			if ($input ["postyn"] == "Y") {
				$user_data = $this->users_model->where ( 'id', $input ["id"] )->find_all ();
				$user_data = $user_data [0];
				$this->auth->clear_login_attempts ( $user_data->username );
			}
			// clear login attempts when user is enabled
		} else {
			$this->session->set_flashdata ( 'msg_type', 'error' );
			$this->session->set_flashdata ( 'msg', 'Please contact to customer support' );
			$data ["flag"] = '@#error#@';
		}
		
		if ($input ["postyn"] == "Y") {
			$append = "enabled";
		} else if ($input ["postyn"] == "N") {
			$append = "disabled";
		}
		
		if ($id > 0) {
			$data ["flag"] = '@#success#@';
			$this->session->set_flashdata ( 'msg_type', 'success' );
			$this->session->set_flashdata ( 'msg', 'User ' . $append . ' successfully.' );
		} else {
			$this->session->set_flashdata ( 'msg_type', 'error' );
			$this->session->set_flashdata ( 'msg', 'You are not authorized to view or edit other users.' );
			$data ["flag"] = '@#error#@';
		}
		
		data2json ( $data );
	}
	
	/*
	 * @function : delete_user
	 * @param :
	 * @detail : soft delete user.
	 *
	 */
	public function delete_user() {
		$input = $this->input->post ( NULL, TRUE );
		$this->users_model->id = $input ["id"];
		$rdata = $this->users_model->select ();
		
		if (! empty ( $input ["id"] )) {
			$this->users_model->id = $input ["id"];
			$this->users_model->is_deleted = "Y";
			$id = $this->users_model->save ();
		} else {
			$this->session->set_flashdata ( 'msg_type', 'error' );
			$this->session->set_flashdata ( 'msg', 'Please contact to customer support' );
			$data ["flag"] = '@#error#@';
		}
		if ($id > 0) {
			$data ["flag"] = '@#success#@';
			$this->session->set_flashdata ( 'msg_type', 'success' );
			$this->session->set_flashdata ( 'msg', 'User Deleted Successfully.' );
		} else {
			$this->session->set_flashdata ( 'msg_type', 'error' );
			$this->session->set_flashdata ( 'msg', 'You are not authorized to view or edit other users.' );
			$data ["flag"] = '@#error#@';
		}
		data2json ( $data );
	}
	
	/*
	 * @function : edit_detail
	 * @param :
	 * @detail : edit user detail.
	 *
	 */
	public function edit_details($id) {
		$id = base64_decode ( $id );
		if ($id != "") {
			$where_condition = array (
					'id' => $id 
			);
			$data ['edit_data'] = $this->users_model->get_alluser ( $where_condition );
			$data ['city'] = $this->city_master_model->get_all_city ();
			$data ['states'] = $this->state_master_model->get_all_state ();

			/*$where = array('service_id' => CB_SERVICE_ID,'agent_id' => $id);
			$checkMsrtcCb = $this->retailer_service_mapping_model->where($where)->find_all();
			if(!empty($checkMsrtcCb))
			{
		        $data['sellerpoints'] = $this->buisness_type_model->get_sellerpoints();
		        $data['depots'] = $this->buisness_type_model->get_depots();
		        $data['current_cb_agent_edit'] = TRUE;
			}*/
                        
                        if($data['edit_data'][0]['level'] == RETAILOR_LEVEL) {
                            $data['sellerpoints'] = $this->buisness_type_model->get_sellerpoints();
                            $data['depots'] = $this->buisness_type_model->get_depots();
                            $data['current_cb_agent_edit'] = TRUE;
			}
			//show($data,1);
			load_back_view ( EDIT_ROKAD_USERS, $data );
		} else {
			redirect ( base_url () . 'admin/registration' );
		}
	}
	
	/*
	 * @function : updat_details
	 * @param :
	 * @detail : update user detail.
	 *
	 */
	public function update_details() {
		$input = $this->input->post ( NULL, TRUE );

		logging_data ( "update/update_profile_log.log", $input, "Post data for update profile" );
		$firm_name = isset ( $input ['firm_name'] ) ? trim ( $input ['firm_name'] ) : "";
		$first_name = isset ( $input ['first_name'] ) ? trim ( $input ['first_name'] ) : "";
		$last_name = isset ( $input ['last_name'] ) ? trim ( $input ['last_name'] ) : "";
		$display_name = $first_name . " " . $last_name;
		$gender = isset ( $input ['gender'] ) ? trim ( $input ['gender'] ) : "";
		$address1 = isset ( $input ['address1'] ) ? trim ( $input ['address1'] ) : "";
		$address2 = isset ( $input ['address2'] ) ? trim ( $input ['address2'] ) : "";
		$agent_state = isset ( $input ['agent_state'] ) ? trim ( $input ['agent_state'] ) : "";
		$agent_city = isset ( $input ['agent_city'] ) ? trim ( $input ['agent_city'] ) : "";
		$pincode = isset ( $input ['pincode'] ) ? trim ( $input ['pincode'] ) : "";

		$topuplmit = isset ( $input ['topuplmit'] ) ? trim ( $input ['topuplmit'] ) : "";
//		$depot_code = isset ( $input ['depot_code'] ) ? trim ( $input ['depot_code'] ) : "";
		$Seller_point = isset ( $input ['Seller_point'] ) ? trim ( $input ['Seller_point'] ) : "";

		$dob = isset ( $input ['dob'] ) ? date ( 'Y-m-d', strtotime ( $input ['dob'] ) ) : "";
		// encryption done here
		$new_pincode = rokad_encrypt ( $pincode, $this->config->item ( "pos_encryption_key" ) );
		$new_dob = rokad_encrypt ( $dob, $this->config->item ( "pos_encryption_key" ) );
		
		$user_id = rokad_decrypt ( $input ['id'], $this->config->item ( "pos_encryption_key" ) );
		$depotname = '';
		$star_seller_name='';
		
		if(!empty($depot_code) || !empty($Seller_point))
		{ 
//            $depot_name = $this->common_model->get_value('cb_depots', 'DEPOT_NM', 'DEPOT_CD="'.$depot_code.'"');
//            $depotname = $depot_name->DEPOT_NM;
//            $depotname = isset ( $depotname) ? trim ( $depotname ) : "";

            $selle_rname = $this->common_model->get_value('cb_bus_stops', 'BUS_STOP_NM', 'BUS_STOP_CD="'.$Seller_point.'"');
            $star_seller_name = $selle_rname->BUS_STOP_NM;

            //$this->common_model->insert_depot_log_data($input,$user_id);

		}
               // show(last_query());show($depotname,1);
		
		$config = array (
				array (
						'field' => 'firm_name',
						'label' => 'Name of Firm/Company',
						'rules' => 'trim|xss_clean|required',
						'errors' => array (
								'required' => 'Please enter name of firm/company' 
						) 
				),
				array (
						'field' => 'first_name',
						'label' => 'First Name',
						'rules' => 'trim|xss_clean|required',
						'errors' => array (
								'required' => 'Please enter first name' 
						) 
				),
				array (
						'field' => 'last_name',
						'label' => 'Last Name',
						'rules' => 'trim|xss_clean|required',
						'errors' => array (
								'required' => 'Please enter last nanme' 
						) 
				),
				array (
						'field' => 'gender',
						'label' => 'Gender',
						'rules' => 'trim|xss_clean' 
				),
				array (
						'field' => 'dob',
						'label' => 'Date of birth',
						'rules' => 'trim|xss_clean|required|callback_validate_age',
						'errors' => array (
								'required' => 'Please select your age' 
						) 
				),
				array (
						'field' => 'address1',
						'label' => 'Address1',
						'rules' => 'trim|xss_clean|required',
						'errors' => array (
								'required' => 'Please enter the address' 
						) 
				),
				array (
						'field' => 'address2',
						'label' => 'Address2',
						'rules' => 'trim|xss_clean' 
				),
				array (
						'field' => 'agent_state',
						'label' => 'Agent State',
						'rules' => 'trim|xss_clean|required',
						'errors' => array (
								'required' => 'Please select the state' 
						) 
				),
				array (
						'field' => 'agent_city',
						'label' => 'Agent City',
						'rules' => 'trim|xss_clean|required',
						'errors' => array (
								'required' => 'Please select the city' 
						) 
				),
				array (
						'field' => 'pincode',
						'label' => 'Pincode',
						'rules' => 'trim|xss_clean|required|numeric|min_length[6]|max_length[6]',
						'errors' => array (
								'required' => 'Please enter the pin code' 
						) 
				) 
		);
		if (form_validate_rules ( $config )) {
			$update_array_users = array (
					"firm_name" => $firm_name,
					"first_name" => $first_name,
					"last_name" => $last_name,
					"display_name" => $display_name,
					"address1" => $address1,
					"address2" => $address2,
					"state" => $agent_state,
					"city" => $agent_city,
					"pincode" => $new_pincode,
					"gender" => $gender,
					"dob" => $new_dob,
					"topuplmit" => $topuplmit,
//					"depot_cd" => $depot_code,
//					"depot_name" => $depotname,
					"star_seller_code" => $Seller_point,
					"star_seller_name"=> $star_seller_name,
			);
			
			$update = $this->users_model->update_where ( 'id', $user_id, $update_array_users );
			$this->session->set_flashdata ( 'msg_type', 'success' );
			$this->session->set_flashdata ( 'msg', 'User detail updated successfully' );
			
			redirect ( base_url () . 'admin/registration/' );
		} else {
			$where_condition = array (
					'id' => $user_id 
			);
			$data ['edit_data'] = $this->users_model->get_alluser ( $where_condition );
			$data ['city'] = $this->city_master_model->get_all_city ();
			$data ['states'] = $this->state_master_model->get_all_state ();
			load_back_view ( EDIT_ROKAD_USERS, $data );
		}
	}
	
	/*
	 * @Author : Maunica Shah
	 * @function : upload_document
	 * @detail : update user detail.
	 */
	public function upload_document() {
		$input = $this->input->get ();
		$user_id = $input ['ref'];
		redirect ( base_url () . 'admin/agent/agent_docs_view?agid=' . $user_id );
	}
	
	/*
	 * @Author : Maunica Shah
	 * @function : view_details
	 * @detail : view user detail.
	 */
	public function view_details() {
		$input = $this->input->get ();
		$user_id = base64_decode ( $input ['ref'] );
		$where_condition = array (
				'id' => $user_id 
		);
		$data ['view_data'] = $this->users_model->get_alluser ( $where_condition );
		$data ['state_name'] = $this->state_master_model->where ( array (
				'intStateId' => $data ['view_data'] [0] ['state'] 
		) )->column ( 'stState' )->find_all ();
		$data ['city_name'] = $this->city_master_model->where ( array (
				'intCityId' => $data ['view_data'] [0] ['city'] 
		) )->column ( 'stCity' )->find_all ();
		// show($data['view_data'],1);
		if ($data ['view_data'] [0] ['level_2'] != "") {
			$data ['level_2'] = $this->users_model->where ( array (
					'id' => $data ['view_data'] [0] ['level_2'] 
			) )->column ( 'display_name' )->find_all ();
		}
		if ($data ['view_data'] [0] ['level_3'] != "") {
			$data ['level_3'] = $this->users_model->where ( array (
					'id' => $data ['view_data'] [0] ['level_3'] 
			) )->column ( 'display_name' )->find_all ();
		}
		if ($data ['view_data'] [0] ['level_4'] != "") {
			$data ['level_4'] = $this->users_model->where ( array (
					'id' => $data ['view_data'] [0] ['level_4'] 
			) )->column ( 'display_name' )->find_all ();
		}		

			$data ['bank_detail'] = $this->agent_bank_detail_model->where ( array (
					'agent_id' => $user_id 
			) )->find_all();
			//show($data,1);
		load_back_view ( VIEW_ROKAD_USERS, $data );
	}

	public function users_registration() {

        $data = array();
        $data ['states'] = $this->state_master_model->get_all_state ();

        $data["roles"] = $this->role_model->get_admin_roles();
        // show($data,1);
        load_back_view(ADMIN_REGISTRATION_VIEW, $data);
    }

    public function technical_users_list() {

    	$data['users_list'] = $this->users_model->fetch_all_technical_users();
    	load_back_view(TECHNICAL_USERS_LIST,$data);
    }

    public function save_admin_users() {
		$input = $this->input->post ( NULL, TRUE );
		// show($input,1);
	    logging_data ( "registration/registration_log.log", $input, "Post data for admin level registration" );
		$encrypted_role_id = isset ( $input ['role_id'] ) ? trim ( $input ['role_id'] ) : "";
		$first_name = isset ( $input ['first_name'] ) ? trim ( $input ['first_name'] ) : "";
		$last_name = isset ( $input ['last_name'] ) ? trim ( $input ['last_name'] ) : "";
		$display_name = $first_name . " " . $last_name;
		$email = isset ( $input ['email'] ) ? trim ( $input ['email'] ) : "";
		$mobile_no = isset ( $input ['mobile_no'] ) ? trim ( $input ['mobile_no'] ) : "";
		$gender = isset ( $input ['gender'] ) ? trim ( $input ['gender'] ) : "";
		$dob = isset ( $input ['dob'] ) ? date ( 'Y-m-d', strtotime ( $input ['dob'] ) ) : "";
		$address1 = isset ( $input ['address1'] ) ? trim ( $input ['address1'] ) : "";
		$address2 = isset ( $input ['address2'] ) ? trim ( $input ['address2'] ) : "";
		$agent_state = isset ( $input ['agent_state'] ) ? trim ( $input ['agent_state'] ) : "";
		$agent_city = isset ( $input ['agent_city'] ) ? trim ( $input ['agent_city'] ) : "";
		$pincode = isset ( $input ['pincode'] ) ? trim ( $input ['pincode'] ) : "";
		
		$session_level = $this->session->userdata ( 'level' );
		$session_id = $this->session->userdata ( 'id' );
				
		// encryption done here
		$new_mobile = rokad_encrypt ( $mobile_no, $this->config->item ( "pos_encryption_key" ) );
		$new_pincode = rokad_encrypt ( $pincode, $this->config->item ( "pos_encryption_key" ) );
		$new_email = rokad_encrypt ( $email, $this->config->item ( "pos_encryption_key" ) );
		$new_dob = rokad_encrypt ( $dob, $this->config->item ( "pos_encryption_key" ) );
		// decryption done herer
		$role_id = rokad_decrypt ( $encrypted_role_id, $this->config->item("pos_encryption_key"));
        
		$config = array (
				
			array (
					'field' => 'first_name',
					'label' => 'First Name',
					'rules' => 'trim|xss_clean|required',
					'errors' => array (
							'required' => 'Please enter first name' 
					) 
			),
			array (
					'field' => 'last_name',
					'label' => 'Last Name',
					'rules' => 'trim|xss_clean|required',
					'errors' => array (
							'required' => 'Please enter last nanme' 
					) 
			),
			array (
					'field' => 'email',
					'label' => 'Email',
					'rules' => 'trim|xss_clean|required|valid_email|is_unique[users.email]',
					'errors' => array (
							'required' => 'Please enter valid email',
							'valid_email' => 'Please enter the valid email',
							'is_unique' => 'Email id already exsist with us' 
					) 
			),
			array (
					'field' => 'mobile_no',
					'label' => 'Mobile Numebr',
					'rules' => 'trim|xss_clean|required|min_length[10]|max_length[10]|callback_mobileno_check' 
			),
			array (
					'field' => 'role_id',
					'label' => 'Role',
					'rules' => 'trim|xss_clean|required',
					'errors' => array (
							'required' => 'Please select role' 
					) 
			),	
			array (
					'field' => 'gender',
					'label' => 'Gender',
					'rules' => 'trim|xss_clean' 
			),
			array (
					'field' => 'dob',
					'label' => 'Date of birth',
					'rules' => 'trim|xss_clean|required|callback_validate_age',
					'errors' => array (
							'required' => 'Please select your age' 
					) 
			),
			array (
					'field' => 'address1',
					'label' => 'Address1',
					'rules' => 'trim|xss_clean|required',
					'errors' => array (
							'required' => 'Please enter the address' 
					) 
			),
			array (
					'field' => 'address2',
					'label' => 'Address2',
					'rules' => 'trim|xss_clean' 
			),
			array (
					'field' => 'agent_state',
					'label' => 'Agent State',
					'rules' => 'trim|xss_clean|required',
					'errors' => array (
							'required' => 'Please select the state' 
					) 
			),
			array (
					'field' => 'agent_city',
					'label' => 'Agent City',
					'rules' => 'trim|xss_clean|required',
					'errors' => array (
							'required' => 'Please select the city' 
					) 
			),
			array (
					'field' => 'pincode',
					'label' => 'Pincode',
					'rules' => 'trim|xss_clean|required|numeric|min_length[6]|max_length[6]',
					'errors' => array (
							'required' => 'Please enter the pin code' 
					) 
			),	
		);

		if (form_validate_rules ( $config )) {
			
			$this->users_model->role_id = $role_id;
			$this->users_model->username = $new_mobile;
			$this->users_model->first_name = $first_name;
			$this->users_model->last_name = $last_name;
			$this->users_model->gender = $gender;
			$this->users_model->email = $new_email;
			$this->users_model->address1 = $address1;
			$this->users_model->address2 = $address2;
			$this->users_model->pincode = $new_pincode;
			$this->users_model->mobile_no = $new_mobile;
			$this->users_model->country = COUNTRY_ID;
			$this->users_model->state = $agent_state;
			$this->users_model->city = $agent_city;
			$this->users_model->dob = $new_dob;
			$this->users_model->display_name = $display_name;
			$this->users_model->kyc_verify_status = "Y";
			$this->users_model->email_verify_status = "N";
			$this->users_model->verify_status = "N";
			$this->users_model->status = "N";
			$this->users_model->is_deleted = "N";

			$this->users_model->inserted_by = $this->session->userdata ( "user_id" );
			$this->users_model->created_date = date ( "Y-m-d h:i:s:a" );
			$this->users_model->ip_address = $this->input->ip_address();
			$this->users_model->user_agent = $this->input->user_agent();
			// show($this->users_model,1);			
			$last_insert_id_users = $this->users_model->save ();
			if (! empty ( $last_insert_id_users )) {
				// agent bank details
				
				$account_no = substr ( hexdec ( uniqid () ), 4, 12 );
				$wallet_array = array( 
						'user_id' => $last_insert_id_users,
						'amt' => 0.00,
						'status' => "Y",
						'comment' => "Created Account",
						'added_by' => $this->session->userdata ( 'user_id' ),
						'updated_by' => $this->session->userdata ( 'user_id' ),
						'account_no' => $account_no 
				);
				
				$wallet_id = $this->wallet_model->insert ( $wallet_array );
				if (! empty ( $wallet_id )) {
					// Send sms
					$reg_sms_array = array (
							"{{username}}" => ucwords ( $display_name ),
							"{{email}}" => $email 
					);
					$temp_sms_msg = str_replace ( array_keys ( $reg_sms_array ), array_values ( $reg_sms_array ), BOS_REGISTRATION );
					$is_send = sendSMS ( $mobile_no, $temp_sms_msg );
					
					$response = save_n_send_activation_key ( $last_insert_id_users, $email, $display_name, 'email_verification' );
					$this->session->set_flashdata ( 'msg_type', 'success' );
					$this->session->set_flashdata ( 'msg', 'Registration completed successfully. Please check your registered Email ID for activation' );
					redirect ( base_url () . 'admin/registration/technical_users_list' );
				}
			}
		} else {
			$data ['states'] = $this->state_master_model->get_all_state ();
			load_back_view ( ADMIN_REGISTRATION_VIEW, $data );
		}
	}
}
