<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Agent extends CI_Controller {

    var $agent_data;

    public function __construct() {
        parent::__construct();
        //  is_logged_in();
        $this->load->library('datatables');
        $this->load->model(array('users_model', 'agent_model')
        );
    }

    function agent_docs_view() {
        $data = array();
        $var_1 = base64_decode($this->input->get('agid'));
        $this->users_model->id = $var_1;
        $agent_data = $this->users_model->select();
        $data['full_name'] = $agent_data->display_name;
        $data['agent_id'] = $var_1;
        $data['kyc_status'] = $agent_data->kyc_verify_status;
        load_back_view(BACK_AGENT_NAME_VIEW, $data);
    }

    function docs_list_view() {
        $data = array();
        $id = base64_decode($this->input->get('id'));
        $kyc_status = $this->users_model->column('kyc_verify_status')->where(array('id' => $id))->find_all();
        $kyc = $kyc_status[0]->kyc_verify_status;
        $this->datatables->select("adm.agent_id,
                                   u.display_name as agent_name,
                                   IF(dc.category_name='ID PROOF','id_proof', IF(dc.category_name='ADDRESS PROOF','add_proof','photo')) as cat,
                                   adl.docs_name,
                                   IF(adm.is_status=1,'glyphicon glyphicon-signal','glyphicon glyphicon-trash') as icon ,
                                   adm.is_status,adm.docs_name as doc_id,lower(large_img_link) as large_img_link");
        $this->datatables->from("agent_docs_master adm");
        $this->datatables->join('users u', 'u.id=adm.agent_id', 'inner');
        $this->datatables->join('doc_category  dc', 'adm.docs_cat=dc.doc_cat_id', 'inner');
        $this->datatables->join('agent_docs_list adl', 'adl.agent_docs_list=adm.docs_name', 'inner');
        if (isset($id) && $id != '') {
            $this->datatables->where('adm.agent_id', $id);
        }
        $this->datatables->unset_column('icon');
        $this->datatables->unset_column('doc_id');
        $this->datatables->unset_column('large_img_link');

        $this->datatables->unset_column('is_status');
        $this->datatables->add_column('img', '<a href=' . base_url('uploads/agent_docs/' . $id . '/$2/large/$1 ') . ' data-lightbox="image-1" data-title="My caption" width="1000px" height="1000px"><img src=' . base_url('uploads/agent_docs/' . $id . '/$2/thumb/$1') . '></a>', 'large_img_link,cat');
        if ($kyc == "N") {
            $this->datatables->add_column('action', '<a href="javascript:void(0)" class="del_file"  title="click to delete file"  ref="$1" data-img-path=$2   data-doc-name="$5">'
                    . '<i  class="glyphicon glyphicon-remove"></i></a>'
                    . '&nbsp;<a href=' . base_url('uploads/agent_docs/' . $id . '/$4/large/$3 ') . 'class="download_file" target="_blank" download="$3"  title="click to Download  file"  ref="$1" data-img-path=$2 >'
                    . '<i  class="glyphicon glyphicon-download-alt"></i></a>', 'agent_id,doc_id,large_img_link,cat,docs_name');
        } else {
            $this->datatables->add_column('action', '<a href=' . base_url('uploads/agent_docs/' . $id . '/$4/large/$3 ') . 'class="download_file" target="_blank" download="$3"  title="click to Download  file"  ref="$1" data-img-path=$2 >' . '<i  class="glyphicon glyphicon-download-alt"></i></a>', 'agent_id,doc_id,large_img_link,cat,docs_name');
        }

        $this->datatables->where('adm.is_status', 1);
        $this->datatables->where('adm.is_deleted', 0);
        $data = $this->datatables->generate('json');
        echo $data;
    }

    function add_new_doc() {
        $data['res'] = $this->agent_model->doc_type();
        $user_id = $this->session->userdata('user_id');
        $var_1 = base64_decode($this->input->get('id'));
        $this->users_model->id = $var_1;
        $agent_data = $this->users_model->select();
        $data['full_name'] = $agent_data->display_name;
        $data['agent_id'] = $var_1;
        load_back_view(BACK_AGENT_DOCS_ADD, $data);
    }

    function agent_docs_display() {
        $id = base64_decode($this->input->get('id'));
        $data1 = array();
        $data2 = array();
        $data = $this->agent_model->get_agent_data($id);
        if (!empty($data)) {
            foreach ($data as $key => $val):
                if ($val['docs_cat'] == 1) {
                    $data1[] = array("link" => 'uploads/agent_docs/' . $id . '/id_proof/thumb/' . strtolower($val['large_img_link']), "docs" => $val['docs_name'], "large_image" => 'uploads/agent_docs/' . $id . '/id_proof/large/' . strtolower($val['large_img_link']));
                }
                if ($val['docs_cat'] == 2) {
                    $data2[] = array("link" => 'uploads/agent_docs/' . $id . '/add_proof/thumb/' . strtolower($val['large_img_link']), "docs" => $val['docs_name'], "large_image" => 'uploads/agent_docs/' . $id . '/add_proof/large/' . strtolower($val['large_img_link']));
                }
            endforeach;
        } else {
            $data1 = array();
            $data2 = array();
        }

        $final_array['idproof'] = $data1;
        $final_array['addressproof'] = $data2;
        load_back_view(BACK_AGENT_DOC_VIEW, $final_array);
    }

    function get_doc_category() {
        $id = $this->input->post('cat_id');
        $docs_nm['res'] = $this->agent_model->doc_name($id);
        data2json($docs_nm, 1);
    }

    function upload_docs() {

        $this->load->library('upload');
        $in = $this->input->post();
        $input = isset($in['doccat']) ? $in['doccat'] : "";
        $doc_nm = isset($in['docnm']) ? $in['docnm'] : " ";
        $docname = isset($in['dropdowntext']) ? $in['dropdowntext'] : "";
        $tmp = array();
        //gets a agent wise  doc names from database
        $uid = base64_decode($this->input->post('agid'));
        $res = $this->agent_model->get_agent_data($uid);

        if (isset($res) && count($res) < AGENT_DOC_IMAGE_UPLOAD_LIMIT) {

            $agent_doc_name = array();

            //unset($tmp);  

            foreach ($res as $key => $value) {
                $agent_doc_name[] = $value['docs_name'];
            }

            if(!empty($docname))
            {
                foreach ($docname as $key => $val) {
                    if (in_array($val, $agent_doc_name)) {
                        array_push($tmp, $val);
                    }
                }
            }

            if (empty($tmp)) {  // check for empty array 
                $send_data = array();

                if (!file_exists(AGENT_DOCS_FOLDER . '/' . $uid)) {
                    $old = umask(0);
                    mkdir(AGENT_DOCS_FOLDER . '/' . $uid, 0770, true);
                    umask($old);
                }
                $subfolder = array("id_proof", 'add_proof', 'photo');
                for ($c = 0; $c < count($subfolder); $c++) {
                    if (!file_exists(AGENT_DOCS_FOLDER . '/' . $uid . '/' . $subfolder[$c] . '/large')) {
                        mkdir(AGENT_DOCS_FOLDER . '/' . $uid . '/' . $subfolder[$c] . '/large', 0770, true);
                    }
                    if (!file_exists(AGENT_DOCS_FOLDER . '/' . $uid . '/' . $subfolder[$c] . '/thumb')) {
                        mkdir(AGENT_DOCS_FOLDER . '/' . $uid . '/' . $subfolder[$c] . '/thumb', 0770, true);
                    }
                }

                $files = $_FILES;

                $img1_wid = AGENT_DOC_IMAGE_WIDTH;
                $img1_hei = AGENT_DOC_IMAGE_HEIGHT;
                $medium = 'thumb';
                $data = array();
                $i = 0;
                $cpt = count($_FILES['img_doc']['name']);
                if ($_FILES && $_FILES['img_doc']['name'] !== "") {

                    for ($i = 0; $i < $cpt; $i++) {

                        $_FILES['img_doc']['name'] = $files['img_doc']['name'][$i];
                        $_FILES['img_doc']['type'] = $files['img_doc']['type'][$i];
                        $_FILES['img_doc']['tmp_name'] = $files['img_doc']['tmp_name'][$i];
                        $_FILES['img_doc']['error'] = $files['img_doc']['error'][$i];
                        $_FILES['img_doc']['size'] = $files['img_doc']['size'][$i];
                        $info = explode('.', $files['img_doc']['name'][$i]);

                        $file_ext = $info[1];
                        $date = date_create();
                        $time_stamp = date_timestamp_get($date);

                        $file_name = str_replace(" ", "_", strtolower($docname[$i])) . "." . strtolower($file_ext);
                        $pathname[$i] = $file_name;
                        //show($input[$i]);

                        if ($input[$i]!="" && $input[$i] == 1) {
                            $fold[$i] = 'id_proof';
                        }
                        if ($input[$i]!="" && $input[$i] == 2) {
                            $fold[$i] = 'add_proof';
                        }
                        if ($input[$i]!="" && $input[$i] == 3) {
                            $fold[$i] = 'photo';
                        }
                        $this->upload->initialize($this->set_upload_options($pathname[$i], $fold[$i], $uid));

                        if ($this->upload->do_upload('img_doc')) {
                            $img_data = $this->upload->data();

                            $old_path = $img_data['full_path'];
                            $new_path = AGENT_DOCS_FOLDER . $uid . '/' . $fold[$i] . '/thumb/' . $file_name;

                            $this->resize($old_path, $new_path, AGENT_DOC_IMAGE_WIDTH, AGENT_DOC_IMAGE_HEIGHT);


                            $send_data[] = array("agent_id" => $uid,
                                "docs_cat" => $input[$i],
                                "docs_name" => strtolower($doc_nm[$i]),
                                "large_img_link" => $pathname[$i],
                                "thum_img_link" => $pathname[$i],
                                "is_status" => 1,
                                "is_deleted" => 0,
                                "created_by" => $this->session->userdata('user_id'),
                            );
                        } else {
                            $data['upload_errors'][$i] = $this->upload->display_errors();
                        }
                    }
//                $this->resize($data, $uid, $img1_wid, $img1_hei);
                    $this->agent_model->save_agent_docs_detail_batch($send_data);
                }
                //$data['msg_status'] = "success";
                $data['msg_type'] = "success";
                $data['flag'] = "@success";
                $data['msg'] = "Document Uploaded successfully.";
            } else {
                $data['msg_type'] = "success";
                $data['flag'] = "@#error@";
                $data['msg'] = "Document could not be uploaded.Document already exists!!";
            }
        } else {
            $data['msg_type'] = "error";
            $data['flag'] = "@#error@";
            $data['msg'] = 'Do not try to upload more than 10 Documents';
        }
        data2json($data);
    }

    private function set_upload_options($pathname, $folder, $uid) {
        $config = array();


        $config['upload_path'] = AGENT_DOCS_FOLDER . $uid . '/' . $folder . '/large';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['overwrite'] = FALSE;
        $config['file_name'] = $pathname;
        return $config;
    }

    public function resize($org_img, $new_img, $width, $height) {
        $this->load->library('image_lib');

        $config['image_library'] = 'gd2';
        $config['source_image'] = $org_img;
        $config['maintain_ratio'] = TRUE;
        $config['quality'] = "100";
        $config['new_image'] = $new_img;
        $config['width'] = $width;
        // $config['height'] =$height;

        $this->image_lib->initialize($config);
        if (!$this->image_lib->resize()) {
            echo $this->image_lib->display_errors();
        }
    }

    function kyc_status() {
        $input = $this->input->post();
        $this->load->model(array('agent_docs_master_model'));

        $agent_id = base64_decode($input['agid']);

        $kyc_data = $this->users_model->column('kyc_verify_status')
                ->where('id', $agent_id)
                ->as_array()
                ->find_all();

        $document = $this->agent_docs_master_model->where(array('agent_id' => $agent_id))->find_all();

        if (!$document) {
            $data["flag"] = '@#error#@';
            $data["msg_type"] = 'success';
            $data["msg"] = "No document found. Please upload it first.";
        } else {
            if (count($document) >= 2) {
                $this->users_model->update_where('id', $agent_id, array('kyc_verify_status' => 'Y'));
                $data["flag"] = '@#success#@';
                $data["msg_type"] = 'success';
                $data["msg"] = "KYC status updated successfully";
            } else {
                $data["flag"] = '@#error#@';
                $data["msg_type"] = 'success';
                $data["msg"] = "Please upload minimum 2 documents for KYC verification.";
            }
        }
        data2json($data);
    }

    function delete_img() {

        $doc_name = $this->input->post('docs_id');
        $ag_id = base64_decode($this->input->post('ag_id'));
        $ima_nm = $this->agent_model->get_image_name($doc_name, $ag_id);
        if ($ima_nm[0]['docs_cat'] == 1) {
            $doc_type = "/id_proof";
        } else {
            $doc_type = "/add_proof";
        }


        if (file_exists(AGENT_DOCS_FOLDER . $ag_id . $doc_type . '/large/' . $ima_nm[0]['large_img_link'])) {
            unlink(AGENT_DOCS_FOLDER . $ag_id . $doc_type . '/large/' . $ima_nm[0]['large_img_link']);
            unlink(AGENT_DOCS_FOLDER . $ag_id . $doc_type . '/thumb/' . $ima_nm[0]['large_img_link']);
        }

        $this->agent_model->delete_doc($doc_name, $ag_id);
        $data['msg_status'] = 'success';
        data2json($data);
    }

}

?>
