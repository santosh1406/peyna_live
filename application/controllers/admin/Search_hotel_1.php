<?php
// created by Shrasti Bansal on 31-dec-2018
//modified by Sonali Kamble on 31-may-19
defined('BASEPATH') OR exit('No direct script access allowed');

class Search_hotel extends CI_Controller{
	public function __construct() {
        parent::__construct();
       
        $this->load->library('form_validation');
        $this->load->helper('various_helper');
        $this->load->model(array('Utilities_model','Search_hotel_model','retailer_service_mapping_model'));
      	is_logged_in();

      	$this->sessionData['id'] = $this->session->userdata('user_id');
      	$this->sessionData["username"] = $this->session->userdata('username');
        $this->sessionData["password"] = $this->session->userdata('password');
        $this->apiCred = json_decode(VIA_COM_CREDS, true);
        
    }
    
    private function checkAccessToModule()
    {
        $user_data = $this->session->userdata();
        $agent_id = $user_data['user_id'];
        $retailer_services = $this->retailer_service_mapping_model->getServiceByAgentId($agent_id);
        $service_array = array_column($retailer_services, 'service_id');
        if (in_array(BOS_SERVICE_ID, $service_array)) {
            $checkCommissionExists = $this->checkCommissionExists($agent_id);
            if($checkCommissionExists) {
                return true;
            }
        } else {
            $this->session->set_flashdata('item', 'No access to module');
            redirect(base_url() . 'admin/dashboard');
        }
    }
	
    function index(){	
        //$this->checkAccessToModule();
        $cityList = $this->Search_hotel_model->cityList();
        $data['cityList'] = $cityList;        
        load_back_view(SEARCH_HOTEL_VIEW,$data);	 	
    }
    
    function test1(){
        $cityList = $this->Search_hotel_model->cityList();
        $data['cityList'] = $cityList;        
        load_back_view(SEARCH_HOTEL_VIEW1,$data);
        
    }
    
    public function get_rooms() {

        $curl_url = $this->apiCred['url'] . '/hotel/get-rooms';
        //echo "Hi";die;
       // $input['hotelIds'] = array($_POST['hotelIds']);
        $input['hotelIds'] = [4548];
        $input['searchId'] = $_POST['searchId'];
        $data = curlForPostData_VIA($curl_url, $input, $this->sessionData,$this->apiCred);
        var_dump($data);die;
         //$json = json_decode($data);
      $this->response($data);
//         log_data('rest/smart_card_ccas_api_' . date('d-m-Y') . '.log', $json, 'api');
       $this->output
           ->set_status_header(200)
           ->set_content_type('application/json', 'utf-8')
           ->set_output(json_encode($json));
        // json_response($data);
    }

    public function search_hotel() {
        show($_POST,1);
        $this->checkAccessToModule();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('city', 'Destination', 'required');
        $this->form_validation->set_rules('check_in_date', 'check in date', 'required');
        $this->form_validation->set_rules('check_out_date', 'check out date', 'required');
        
        $rajax = isset($_POST['rajax']) && $this->input->post('rajax') ? $this->input->post('rajax') : '';
        
        if ($this->form_validation->run() == FALSE && $rajax !='rajax') {
            load_back_view(HOME_SEARCH);
        } else {
            
        }
        
        if (isset($_POST['check_in_date']) && $_POST['check_in_date'] != '') {
                $input['check_in_date'] = date('Y-m-d',strtotime($_POST['check_in_date']));
        }else{
                $input['check_in_date'] = '';
        }

        if (isset($_POST['check_out_date']) && $_POST['check_out_date'] != '') {
                 $input['check_out_date'] = date('Y-m-d',strtotime($_POST['check_out_date']));
        }else{
                $input['check_out_date'] = '';
        }

        $input['regionIds'] = array('5C'.$_POST['city']);
        $rooms = array('adults' => $_POST['adults'],'children' =>$_POST['children'],'childrenAges' =>array($_POST['children_age']));
        $input['rooms'] = array($rooms);
        $input['getRates'] = "true";
        $input['getRooms'] = "true";
        // $request = json_encode($input);

        $this->session->set_userdata($input);
        $this->session->set_userdata('search_hotel_input',$input);

        if (!empty($input)) {

            $curl_url =  $this->apiCred['url'].'/hotel/search';
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'curl.log', $curl_url, 'Hotel Search curl');
            	$hotelResults =  array (
  'hotelInfo' => 
  array (
    'hotelId' => 12345,
  ),
  'lowestPriceData' => 
  array (
    'searcherId' => 4,
    'payLater' => false,
    'pricing' => 
    array (
      'roomNightSellPrice' => 546.3999999999999772626324556767940521240234375,
      'roomNightOriginalPrice' => 510,
      'agentPricing' => 
      array (
        'roomNightNetPrice' => 510,
        'roomNightCommission' => 30,
        'totalNetPrice' => 510,
        'totalCommission' => 30,
        'tds' => 6.4000000000000003552713678800500929355621337890625,
      ),
      'deal' => 
      array (
        'dealText' => 'string',
        'dealLogo' => 'string',
        'discountInfo' => 
        array (
          'value' => 6.4000000000000003552713678800500929355621337890625,
          'type' => 0,
        ),
      ),
    ),
  ),
);   
      
                
                $curlResult = curlForPostData_VIA($curl_url, $input, $this->sessionData,$this->apiCred);
                $output = json_decode($curlResult);
                $output->hotelResults = $hotelResults;
 
                //echo '<pre>'; print_r( $output);die;
                $data['result'] =$output;  
                $data['msg'] =$output->data->msg;  
                $data['input_data']=$input;
                $data['hotelList'] = array(array("id" =>4528,"name" =>"Sun N Sand Hotel Mumbai",'address' =>"2nd Floor- 3/1- Palmgrove Road- Victoria Layout"),array("id" =>4526,"name" =>"Tunga Paradise",'address' =>"P 16- MIDC Central Road- Andheri(E)"),array("id" =>4544,"name" =>"Hotel Metro International",'address' =>"Sakinaka Junction- Andheri Kurla Road- Andheri (E"));            
                $data['searchId'] = $output->searchId;
                //echo '<pre>'; print_r($data['hotelList']);die;

                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'search_hotel.log', $curlResult, 'Hotels List');
               	log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/hotel/' . 'search_hotel.log', $input, 'Search Hotel Input Data');
               	load_back_view(HOTEL_SERVICES_VIEW,$data);

            }

       // }
    }

    

    function load_bus_services()
    {    	
    	load_back_view(BUS_MODULE);
    }

    function get_service_stop_details()
    {
    	$input = array(
           	'provider_id' => $this->input->post('provider_id')	,
           	'service_id' => $this->input->post('service_id')	,
            'date' => date("m/d/Y", strtotime($this->input->post('date'))), 	
           );


    	if (!empty($input)) {

                $curl_url = base_url() . "rest_server/search_bus/get_service_stop_details";                
                $curlResult = curlForPostData_BOS($curl_url, $input, $this->sessionData);
                $output = json_decode($curlResult);

                $data['msg'] =$output->data->msg; 
                $data['from'] =$output->data->data->from_service_stops;
                $data['to'] =$output->data->data->to_service_stops;

                echo json_encode($data);

                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'get_service_stop_details.log', $curlResult, 'Service Stop Details');
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'get_service_stop_details_ip.log', $input, 'Service Stop Details ip');		        
		        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'get_service_stop_details_op.log', $data, 'Service Stop Details op');
                            
                
            }
    	 
    }

    public function get_seat_layout()
	{	
		
		 $newdata=  array('operator_name' => $this->input->post('operator_name'), );

		
		$this->session->set_userdata('operator_name',$newdata);

		$operator_name= $this->input->post('operator_name');

		if($operator_name=='Travelyaari'){$trip_id=$this->input->post('service_id');}else{$trip_id=$this->input->post('trip_id');}

		$input= array(
		'provider_id' =>$this->input->post('provider_id'),
		'from' =>$this->input->post('from'),
		'to' =>$this->input->post('to'),
		'date' => date("m/d/Y", strtotime($this->input->post('date'))), 
		'trip_id' =>$trip_id,
		'service_id'=>$this->input->post('service_id'),
		'inventory_type' =>$this->input->post('inventory_type')			
			
		);	

		
		$this->session->set_userdata('bus_details',$input);

		if (!empty($input)) {

                $curl_url = base_url() . "rest_server/search_bus/get_seat_layout";
                
                $curlResult = curlForPostData_BOS($curl_url, $input, $this->sessionData);

                $output = json_decode($curlResult);
                
                $data['seats']=$output->data->data->seats;
                $data['boarding_stops']=$output->data->data->boarding_stops;
                $data['Dropoffs']=$output->data->data->Dropoffs;
                $data['inventory_type']=$output->data->data->inventory_type;
                
       			echo json_encode($data);

                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'get_seat_layout.log', $curlResult, 'get seat layout');
		        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'get_seat_layout_ip.log', $input, 'get seat layout ip');
		        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'get seat layout_op.log', $data, 'get seat layout op');
                /*log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'bus_services_input.log', $input, 'Bus Services_input');
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'bus_services_op.log', $data, 'Bus Services_op');
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'bus_services_op1.log', $output, 'Bus Services_op1');*/            
                
            }
	}

	function save_temp_booking()
	{
		$this->session->userdata('bus_details');  
		
		$from = $this->session->userdata['bus_details']['from'];
  		$to = $this->session->userdata['bus_details']['to'];
		
		$input  = array(
			'seat_nos' => $this->input->post('seat_no'),

    		//'seat_no1' => preg_replace('/(?<=\d)\s+(?=\d)/', '', $str);, 
    		'sub_total' => $this->input->post('sum_of_seat_fare'),
    		'total_fare' => $this->input->post('sum_of_total_fare'),
    		'birth' => $this->input->post('birth'),
    		'gender' =>$this->input->post('gender'),
    		'is_ladies' =>$this->input->post('is_ladies'),
    		'sleeper' =>$this->input->post('sleeper'),
    		'ac' =>$this->input->post('ac'),
    		'boarding_stop' =>$this->input->post('boarding_stop'),
    		'dropping_stop' =>$this->input->post('dropping_stop'),
    		'from_stop_name' =>($this->input->post('from_stop_name'))?($this->input->post('from_stop_name')):($from),
    		'to_stop_name' =>($this->input->post('to_stop_name'))?($this->input->post('to_stop_name')):($to),

    		



    		);	
		$this->session->set_userdata('temp_details',$input);
		log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'save_temp_booking_ip.log', $input, 'save temp booking ip');

		$data['redirect'] = base_url()."admin/search_bus/do_temp_booking";
		echo json_encode($data);
		

	}

	function do_temp_booking()
    {
    	
    	$this->session->userdata('bus_details');    	
	   	$this->session->userdata('temp_details');   	   		
    	load_back_view(TEMP_BOOKING);
    	
    	
    }

	function temp_booking()
	{
		$this->session->userdata('bus_details');
		$this->session->userdata('temp_details');

  		$provider_id = $this->session->userdata['bus_details']['provider_id'];
    	$from = $this->session->userdata['bus_details']['from'];
  		$to = $this->session->userdata['bus_details']['to'];
    	$inventory_type = $this->session->userdata['bus_details']['inventory_type'];
    	$date =$this->session->userdata['bus_details']['date'];
    	$trip_id = $this->session->userdata['bus_details']['trip_id'];
    	$service_id = $this->session->userdata['bus_details']['service_id'];



    	
       

        $boarding_stop = ($this->session->userdata['temp_details']['boarding_stop'])?($this->session->userdata['temp_details']['boarding_stop']):($from);
        $dropping_stop = ($this->session->userdata['temp_details']['dropping_stop'])?($this->session->userdata['temp_details']['dropping_stop']):($to);


       	$customer_email=$this->input->post('email1');
        $customer_mobile=$this->input->post('mobile1');
        $customer_id_type=$this->input->post('id_type1');
        $customer_id_number=$this->input->post('id_number1');
        $customer_name=$this->input->post('psgr_name1');
        $customer_age=$this->input->post('psgr_age1');
        $customer_sex=$this->input->post('psgr_sex1');       
        $operator_name=$this->input->post('operator_name');       

		
		
		$countSeats =0;
		$seats = explode(',',$this->session->userdata['temp_details']['seat_nos']);
		//$seats = array('6'); 

		 foreach ($seats as $key => $value) {
		 	$countSeats += count($value);
		 }
		 $countSeats = $countSeats;
		 
		 $ladies_seats = explode(',',$this->session->userdata['temp_details']['is_ladies']);
		 $seat_fare = explode(',',$this->session->userdata['temp_details']['sub_total']);		 
		 $total_fare = explode(',',$this->session->userdata['temp_details']['total_fare']);		 
		 $sleepers = explode(',',$this->session->userdata['temp_details']['sleeper']);
		 $acs = explode(',',$this->session->userdata['temp_details']['ac']);
		 $births = explode(',',$this->session->userdata['temp_details']['birth']);
		// print_r($births);
		 
		 $i = 0;
        foreach($customer_name as $row){
            $data2[] =array(
                'name'=>$customer_name[$i],
                "age"=>$customer_age[$i],
                "sex"=>$customer_sex[$i],
		 		"birth"=>$births[$i],
			 	"seat_no"=>$seats[$i],
			 	"subtotal"=>$seat_fare[$i],
			 	"total"=>$total_fare[$i],
			 	"is_ladies"=>$ladies_seats[$i],
			 	"mobile"=>$customer_mobile,
			 	"title"=>"mr",
			 	"email"=>$customer_email,
			 	"id_type"=>$customer_id_type,
			 	"id_number"=>$customer_id_number,
			 	"name_on_id"=>"Rohitesh KUmar",
			 	"primary"=>"True",
			 	"ac"=>$acs[$i],
			 	"sleeper"=>$sleepers[$i]
            );
            $i++;
        }

		$passenger_details=json_encode($data2);

		if($operator_name=='Travelyaari'){$trip_id1=$service_id;}else{$trip_id1=$trip_id;}
		
		$input = array(
		'provider_id'=> $provider_id,
      	'from'=> $from,
      	'to'=> $to,
     	'date'=> date("m/d/Y", strtotime($date)),     	
     	'boarding_stop'=> $boarding_stop,
     	'dropping_stop'=>$dropping_stop,
     	'trip_id'=>$trip_id1,
     	'email'=> $customer_email,
    	'mobile' => $customer_mobile,    	
    	'passenger' =>$passenger_details,
		'name' => 'sagar',
		'address'=>'Dadar east',
		'inventory_type' =>$inventory_type,
		
			);
		
		log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'temp_booking_ip.log', $input, 'temp_booking ip');
		
		if (!empty($input)) {

                $curl_url = base_url() . "rest_server/search_bus/temp_booking";
                
                $curlResult = curlForPostData_BOS($curl_url, $input, $this->sessionData);

                $output = json_decode($curlResult);                
                
                $status = $output->data->status;

               // echo json_encode($output);

                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'temp_booking.log', $curlResult, 'temp booking');
		        
		        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'temp_booking_op.log', $output, 'temp booking op');
                 
                if($status == 'success') 
                {
                	$boarding_stop_name = explode('-',trim($this->session->userdata['temp_details']['from_stop_name']));

                	$destination_stop_name = explode('-',trim($this->session->userdata['temp_details']['to_stop_name']));
                   	 
                	
		            $ticket_data= array(
		    		'ticket_ref_no' => $output->data->data->block_key, 
		    		'total_basic_amount' =>$output->data->data->total_fare,
		    		'total_basic_with_ac' =>$output->data->data->total_fare,
		    		'tot_fare_amt' => $output->data->data->total_fare,
		    		'tot_fare_amt_with_tax' =>$output->data->data->total_fare,
		    		'total_fare_without_discount' =>$output->data->data->total_fare,
		    		
		    		'status' => 'Y',
		    		'transaction_status' => 'temp_booked',
		    		//'user_email_id' => $temp_booking['passenger']['email'],
		    		'user_email_id' => $customer_email,
		    		'mobile_no' => $customer_mobile,
		    		'agent_email_id' => $customer_email,
		    		'agent_mobile_no' => $customer_mobile,
		    		'from_stop_name' => $this->session->userdata['bus_services_input']['from'],
		    		'till_stop_name' => $this->session->userdata['bus_services_input']['to'],
		    		'boarding_stop_name' => ($boarding_stop_name[0])?($boarding_stop_name[0]):$from,
		    		'destination_stop_name' => ($destination_stop_name[0])?($destination_stop_name[0]):$to,
		    		'tds_per' => $output->data->data->agent_tds_value ,
		    		'num_passgr' => $countSeats,
		    		'booked_from' => $operator_name,
		    		'op_name' => $operator_name,
		    		'inventory_type' => $inventory_type,
		    		'dept_time'=>$date
		    		);
		    		
		    		$block_key = $output->data->data->block_key; 
		    			
		    		$this->session->set_userdata('block_key',$block_key);
		            
		        	log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'tickets_tbl.log', $ticket_data, 'tickets tbl');
		    		
		    		$s = $this->db->insert('tickets',$ticket_data);
		    		if($s)
		    		{
		    			$insert= 'insert temp success';
		    		}
		    		else
		    		{
		    			$insert= 'fail';
		    		}

		    		$wallet_balance = $this->wallet_model->where('user_id',$this->session->userdata('user_id'))->find_all();

					if($wallet_balance)
	                {
	                    $wallet_amt = to_currency($wallet_balance[0]->amt);
	                }
	                else
	                {
	                    $wallet_amt ='0.00';
	                }
	            	$transaction_amount= $output->data->data->transaction_amount;            
	                 
	                $wallet_amt1=ltrim($wallet_amt,"Rs. ");
	                $balance_amount= (float)$wallet_amt1-(float)$transaction_amount;
	                 'balance_amount=>'.  $balance_amount;



	                /*confirm booking*/
	                $unique_id=  mt_rand(10000000, 99999999);
		
					$conf_input =array(
						'block_key' => $output->data->data->block_key,
						'unique_id' => $unique_id,
						'seat_count' => $countSeats,
						 );
					
					 log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'confirm_booking_ip.log', $conf_input, 'confirm booking ip');

						$conf_op = $this->confirm_booking($conf_input);

						
					
					/*if (!empty($conf_input)) {
						
			                $curl_url1 = base_url() . "rest_server/search_bus/confirm_booking";
			                
			                $curlResult1 = curlForPostData_BOS($curl_url1, $conf_input, $this->sessionData);
			                
			                $output1 = json_decode($curlResult1);
			                
			                $data1=$output1->data->data;
			                

			                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'confirm_booking.log', $curlResult1, 'confirm_booking');
					        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'confirm_booking_ip.log', $conf_input, 'confirm booking ip');
					        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'confirm_booking_op.log', $data1, 'confirm booking op');			                            
			                
			            }*/
			       		  
			            if($conf_op->status=='success')
			            {
			            	$update_ticket_data =array(
				        	'boss_ref_no'=>$conf_op->data->bos_key,
				        	'api_ticket_no'=>$conf_op->data->ticket_no,
				        	'pnr_no'=>($conf_op->data->pnr)?($conf_op->data->pnr):$conf_op->data->ticket_no,
				        	'transaction_status' => 'success',
				        	);	
				        		
				        		       		
					        $this->db->where('ticket_ref_no',$output->data->data->block_key);
					       	$u= $this->db->update('tickets',$update_ticket_data);
					       	if($u)
				    		{ $update ='update success';}
				    		else
				    		{ $update = 'fail';}

						    	/*commssion part start*/
						    $commission_data =  array(
						    	'user_id' => $this->sessionData['id'], 
						    	'base_fare' => $output->data->data->base_fare,
						    	'commssion_percent' => $output->data->data->operator_commission_rate,
						    	'agent_commission' => $output->data->data->agent_net_commission,
						    	);
			                
			               	$transaction_no = $conf_op->data->bos_key;

			              	$this->calc_commission($commission_data, $transaction_no);

			              	
			                /*commssion part end*/

				    			/****save details in ticket_details ****/
				    		$bos_key=$conf_op->data->bos_key;
				    		$query= $this->db->query("select ticket_id from tickets where boss_ref_no = '$bos_key' ");
				    		$get_ticket_id= $query->row();
			
							$j = 0;	
							$fare_reservationCharge =  ($output->data->data->ticket_detail->msrtc->reservation_charge)?($output->data->data->ticket_detail->msrtc->reservation_charge):'0.00';

							$ac_service_tax = ($output->data->data->ticket_detail->msrtc->ac_service_charges)?($output->data->data->ticket_detail->msrtc->ac_service_charges):'0.00';

							$asn_fare = ($output->data->data->ticket_detail->msrtc->asn_amount)?($output->data->data->ticket_detail->msrtc->asn_amount):'0:00';

					        foreach($customer_name as $row){
					            $ticket_details_data[] =array(
								 	'ticket_id' => $get_ticket_id->ticket_id,
								 	"seat_no"=>$seats[$j],
								 	'berth_no' =>$births[$j],
								 	'psgr_name' =>$customer_name[$j],
								 	'psgr_age' =>$customer_age[$j],
								 	"psgr_sex"=>$customer_sex[$j],
								 	'fare_reservationCharge' =>$fare_reservationCharge,
									'ac_service_tax' =>$ac_service_tax,
									'asn_fare'  => $asn_fare,			 	
									'agent_commission_amount' =>$output->data->data->agent_net_commission,

					            );
					            $j++;
					        }	//end foreach

         						log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ticket_details_data.log', $ticket_details_data, 'ticket details ip');
				    	
				    			$s = $this->db->insert_batch('ticket_details',$ticket_details_data);
					    		if($s)
					    		{$save= 'insert details in ticket_details';

					    		}
					    		else
					    		{$save= 'fail';}
    						/****save details in ticket_details ****/

			            } //$output1->data->status
			            else
			            {$conf_op->status;}			        
	                /*confirm booking*/
                }
                else
                {
                	$output->data->msg;
                }
            };
            if($transaction_no)
            {
            	//$data=  $transaction_no;
            	$data['redirect'] = base_url()."admin/search_bus/do_print_ticket";
				$data['transaction_no'] = $transaction_no;
             	echo json_encode($data);
         	}
        	else
        	{$data ='error'; }
           
            //redirect(base_url().'admin/search_bus/do_print_ticket/'.$transaction_no);

            /*$data['redirect1'] = base_url()."admin/search_bus/do_print_ticket".$transaction_no;
			echo json_encode($data);*/


	}

	public function confirm_booking($conf_input)
	{
		
		if (!empty($conf_input)) {
						
			$curl_url = base_url() . "rest_server/search_bus/confirm_booking";
				                
			$curlResult = curlForPostData_BOS($curl_url, $conf_input, $this->sessionData);
				                
			$output = json_decode($curlResult);
				                
			$data=$output->data;
				                
			return $data;

			log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'confirm_booking.log', $curlResult, 'confirm_booking');
			log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'confirm_booking_ip.log', $conf_input, 'confirm booking ip');
			log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'confirm_booking_op.log', $data1, 'confirm booking op');

		}
	}

	function cancel_ticket()
	{
		load_back_view(CANCEL_MODULE);
	}

	function check_ticket_details()
	{
		$bos_key=$this->input->post('boss_ref_no1');
	    $email=$this->input->post('email1');
	    $pnr_no=$this->input->post('pnr_no1');

	    if($bos_key)
	    {
	    	$query= $this->db->query("select t.*,td.* from tickets t join ticket_details td on td.ticket_id=t.ticket_id where t.boss_ref_no = '$bos_key' ");
	    }
	    elseif($pnr_no)
	    {
	    	$query= $this->db->query("select t.*,td.* from tickets t join ticket_details td on td.ticket_id=t.ticket_id where t.pnr_no = '$pnr_no' ");
	    }
    	$data['ticket_details']= $query->result_array();
    	//echo $this->db->last_query();
    	echo json_encode($data);
    	
	}

	public function cancel_ticket_details()
	{
		
	    $input = array(
	        'bos_key'=>$this->input->post('bos_key'),	        
	        );	
		

		if (!empty($input)) {

                $curl_url = base_url() . "rest_server/search_bus/cancel_ticket";
                
                $curlResult = curlForPostData_BOS($curl_url, $input, $this->sessionData);

                $output = json_decode($curlResult);
                
                $data=$output->data;

                echo json_encode($data);
                

                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'cancel_ticket.log', $curlResult, 'cancel_ticket');
		        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'cancel_ticket_ip.log', $input, 'cancel ticket ip');
		        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'cancel_ticket_op.log', $data, 'cancel ticket op');

		         /******data save in cancel_ticket_data table*****/
		        $bos_key=$output->data->bos_key;
    			$query= $this->db->query("select ticket_id from tickets where boss_ref_no = '$bos_key' ");

    			$get_ticket_id= $query->row();

    			$this->session->userdata('temp_details');
       
        		$seat_no1 = $this->session->userdata['temp_details']['seat_nos'];

    			  $save_cancel_ticket = array(
    			'ticket_id' => $get_ticket_id->ticket_id,
		       	'pnr_no' => $output->data->data->pnr, 
		       	'new_pnr' =>$output->data->data->new_pnr,
		       	'seat_no' => $seat_no1,
		       	'cancel_charge'=>$output->data->data->cancel_charge,
		       	'cancel_charge_operator'=> 'msrtc',
		       	'refund_amt' => $output->data->data->refund_amt,
		       	'refund_amount_date' => date('Y-m-d'),
		       	'is_refund' => '1'
		       	);
    			  print_r($save_cancel_ticket);

    			  $s = $this->db->insert('cancel_ticket_data',$save_cancel_ticket);
	    		if($s)
	    		{
	    			echo 'insert details in cancel_ticket_data';
	    		}
	    		else
	    		{
	    			echo 'fail';
	    		}
		       

    			/******data save in cancel_ticket_data table*****/
                            
                
            }
	}

	public function is_ticket_cancel()
	{
		$input = array(
	        'bos_key'=>$this->input->post('bos_key'),	        
	        );	

		if (!empty($input)) {

                $curl_url = base_url() . "rest_server/search_bus/is_ticket_cancel";
                
                $curlResult = curlForPostData_BOS($curl_url, $input, $this->sessionData);

                $output = json_decode($curlResult);
                
                $data=$output->data;
                

                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'is_ticket_cancel.log', $curlResult, 'is ticket cancel');
		        //log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'is_ticket_cancel_ip.log', $input, 'is ticket cancel ip');
		        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'is_ticket_cancel_op.log', $data, 'is ticket cancel op');

		        echo json_encode($data);
                            
                
            }

	}

	public function print_ticket()
    {
    	load_back_view(PRINT_TICKET);
    }

    public function do_print_ticket()
    {
    	$refrence_no = $this->input->get('refrence_no');
    	
    	$input = array(
    		'refrence_no' => ($refrence_no)?($refrence_no):$this->input->post('refrence_no1'), 
    		'email' => $this->input->post('email1')
    		);
    	
   		if (!empty($input)) {

                $curl_url = base_url() . "rest_server/search_bus/print_ticket";
                
                $curlResult = curlForPostData_BOS($curl_url, $input, $this->sessionData);         
                $output = json_decode($curlResult);
                $data['result'] = $output->data;

                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'print_ticket.log', $curlResult, 'print ticket');
		        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'print_ticket_ip.log', $input, 'print ticket ip');
		        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'print_ticket_op.log', $data, 'print ticket op');
		        if($this->input->post('refrence_no1'))
		        {
		        	echo json_encode($data);
		        }
		        else
		        {
		        	 load_back_view(PRINT_TICKET,$data);	
		        }
		      

		       


                
            }
          
            

    	/*//$query = $this->db->query("select * from tickets where boss_ref_no='$refrence_no1' and user_email_id='$email1'");
    	$query = $this->db->query("select t.*,td.*, sum(t.tot_fare_amt + td.fare_reservationCharge + td.asn_fare) as total_fare  from tickets t join ticket_details td on t.ticket_id=td.ticket_id where t.boss_ref_no='$refrence_no1' and t.user_email_id='$email1'	");

    	
		$data['details'] = $query->result_array();
		echo json_encode($data);*/
    	

    
    }

    function test()
    {
    	$this->db->select('u.id as user_id,u.agent_code,u.level_1,u.level_2,u.level_3,u.level_4,u.level_5,rsm.service_id as service_id,sc.id as commission_id,sc.values');
        $this->db->from("users u");
        $this->db->join('retailer_service_mapping rsm', 'u.id= rsm.agent_id', 'left');
        $this->db->join('services_commissions sc', 'rsm.service_id = sc.service_id', 'left');
        $this->db->where('rsm.agent_id =', $user_id);
        $this->db->where('rsm.service_id =', $service_id);
        $this->db->where('sc.status', 'Y');
        $this->db->where('sc.from >=', date('Y-m-d H:i:s'));
        $this->db->where('sc.till <=', date('Y-m-d H:i:s'));
        $user_result = $this->db->get();
        echo $this->db->last_query();
        $user_result_array = $user_result->result_array();

    	//load_back_view(TEST);
    }

    function get_test()
    {
    	

    	$name =explode(',',$this->input->post('psgr_name1'));


    	$i=0;
    	foreach ($name as $key => $value) {
    		$data[]= array(
    			'psgr_name'=>$name[$i],
    			);
    	$i++;	
    	}

    	/*$data=array(
    		array('psgr_name'=>'sona'),
    		array('psgr_name'=>'deepa'),
    		);*/

    	var_dump($data);

    		$s = $this->db->insert_batch('ticket_details',$data);
					    		if($s)
					    		{echo 'insert details in ticket_details';}
					    		else
					    		{echo 'fail';}
    }

    public function calc_commission($input = "", $transaction_no = "")
    {
    	$input['transaction_no'] = $transaction_no;
		if (!empty($input)) {

                $curl_url = base_url() . "rest_server/search_bus/calc_commission";
                
                $curlResult = curlForPostData_BOS($curl_url, $input, $this->sessionData);

                //$output = json_decode($curlResult);               

              
               // echo json_encode($curlResult);

                return $curlResult;

                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'calc_commission.log', $curlResult, 'calc commssion');
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'calc_commission_ip.log', $input, 'calc commssion ip');		        
		        //log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'calc_commission_op.log', $output, 'calc commssion op');
                            
                
            }
    	
    }

    
}


				    		