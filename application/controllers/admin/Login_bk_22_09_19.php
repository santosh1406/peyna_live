<?php

class Login extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        //print_r($db['default']); exit;
        $this->load->library('form_validation');
        $this->load->model(array('users_model', 'role_model', 'user_login_count_model', 'wallet_model',
            'user_email_model', 'user_otp_model', 'user_tpin_model', 'user_password_model',
            'state_master_model', 'city_master_model', 'commission_model', 'buisness_type_model',
            'agent_bank_detail_model', 'agent_model', 'retailer_service_mapping_model','depot_master_model'));
        $this->config->load('facebook');
    }

    /*
     * @function    : login_view
     * @param       : 
     * @detail      : login page.
     *                 
     */

    public function login_view() {

        $data = [];
        $data["metadata_title"] = $this->config->item('title', 'login_metadata');
        $data["metadata_description"] = $this->config->item('description', 'login_metadata');
        $data["metadata_keywords"] = $this->config->item('keywords', 'login_metadata');

        $user_id = $this->session->userdata('user_id');
        if ($user_id == "") {
            load_front_view(LOGIN_VIEW, $data);
        } else {
            redirect(base_url());
        }
    }

    /*
     * @function    : forgot_password_view
     * @param       : 
     * @detail      : login forgot password view.
     *                 
     */

    public function forgot_password_view() {
        // $captcha = getCiCaptcha();
        // $this->session->set_userdata('bos_forgot_captcha', $captcha['word']);
        $this->load->view(FORGOT_PASSWORD_VIEW);
    }

    public function forgot_password() {
        $login_post = $this->input->post();
        if (!is_string($login_post["email"])) { // || !is_string($login_post["g-recaptcha-response"]) || !is_string($login_post["hiddenRecaptcha"]))
            redirect(base_url() . "admin/login/login_view");
        }
        $email = trim($login_post["email"]);
        //$captcha = trim($login_post["captcha"]);
        // $g_recaptcha_response = trim($login_post['g-recaptcha-response']);
        //$hiddenRecaptcha = trim($login_post["hiddenRecaptcha"]);

        $input_data = array(
            "email" => $email,
                //"g-recaptcha-response" => $g_recaptcha_response,
                //"hiddenRecaptcha" => $hiddenRecaptcha
        );

        //if (isset($login_post['g-recaptcha-response'])) {
        if ($email != "") {

            //Google Captcha
            //$g_captcha_data = google_captcha($g_recaptcha_response);
            //if ($g_captcha_data->success) {
            /* $this->users_model->email = $email;
              $udata = $this->users_model->select(); */
            $enc_email = rokad_encrypt($email, $this->config->item('pos_encryption_key'));
            $where_array = array("email" => $enc_email);
            $udata = $this->users_model->where($where_array)->find_all();

            if ($udata) {
                $user_array = $udata[0];
                if ($user_array->status == "Y") {
                    $password = getRandomId(10);
                    $salt = getRandomId();
                    /* $handle = fopen("dd.txt", 'w') or die('Cannot open file:  '.dd.txt);
                      fwrite($handle, $salt ."---". $password);
                      //file_put_contents("dd.txt",$salt . $password); */
                    $pass = md5($salt . $password);
                    $this->users_model->id = $user_array->id;
                    $this->users_model->salt = $salt;
                    $this->users_model->password = $pass;
                    $last_insert_id = $this->users_model->save();

                    $pass_data = array("user_id" => $user_array->id,
                        "salt" => $salt,
                        "password" => $pass,
                        "created_date" => date('Y-m-d H:i:s')
                    );

                    $this->user_password_model->save_password_log($pass_data);

                    $display_name = $user_array->display_name;

                    $username = rokad_decrypt($user_array->username, $this->config->item('pos_encryption_key'));

                    $fullname = ucwords(strtolower($user_array->first_name . " " . $user_array->last_name));
                    if ($last_insert_id > 0) {
                        $mailbody_array = array("mail_title" => "forgot_password",
                            "mail_client" => $email,
                            "mail_replacement_array" => array('{{fullname}}' => $fullname,
                                '{{username}}' => $username,
                                '{{password}}' => $password,
                                '{{rokad_support_mail}}' => ROKAD_SUPPORT_MAIL,
                                '{{home_page_url}}' => base_url(),
                                '{{logo}}' => '<img src="' . DOMAIN_NAME_VAL . '/assets/travelo/front/images/logo.png" style="width:104px;"  />'
                            )
                        );


                        $mail_result = setup_mail($mailbody_array);
                        $roledata = $this->role_model->find($user_array->role_id);
                        if ($mail_result) {
                            //insert into user action log                                       
                            $action_log_msg = "Password reset successful";

                            $this->session->set_flashdata('msg_type', 'success');
                            $this->session->set_flashdata('msg', 'Password recovery email is sent to your Email ID.');
                            // redirect(base_url()."home1");
                        } else {
                            //insert into user action log
                            $action_log_msg = "Password reset failed";

                            $this->session->set_flashdata('msg_type', 'success');
                            $this->session->set_flashdata('msg', 'Password could not reset.');
                            // redirect(base_url()."home");
                        }

                        //insert into user action log
                        $users_action_log = array(
                            "user_id" => $user_array->id,
                            "name" => $user_array->first_name . " " . $user_array->last_name,
                            "rmn" => rokad_decrypt($user_array->username, $this->config->item('pos_encryption_key')),
                            "rollname" => $roledata->role_name,
                            "url" => current_url(),
                            "db_function" => "update",
                            "db_table_name" => "users",
                            "function_name" => "reset password",
                            "input_data" => json_encode($input_data),
                            "message" => $action_log_msg
                        );
                        users_action_log_new($users_action_log);
                    }
                } else {
                    $this->session->set_flashdata('msg_type', 'error');
                    $this->session->set_flashdata('msg', 'Could not reset passsword because your account is disabled.');
                    // redirect(base_url()."home");
                }
            } else {
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', 'Email Id not exists.');
                // redirect(base_url()."home");
            }
            // } else {
            //     $this->session->set_flashdata('msg_type', 'error');
            //     $this->session->set_flashdata('msg', 'Please verify captcha.');
            //     // redirect(base_url()."home");
            // }
        } else {
            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'username should not be empty.');
            // redirect(base_url()."home");
        }
        // } else {
        //     $this->session->set_flashdata('msg_type', 'error');
        //     $this->session->set_flashdata('msg', 'Please verify captcha.');
        //     redirect(base_url() . "admin/login/login_view");
        // }

        redirect(base_url() . "admin/login/login_view");
    }

    /*
     * @function    : signup_view
     * @param       : 
     * @detail      : signup view page.
     *                 
     */

    public function signup_view() {
        $data = [];
        $data = $this->register_retailor_detail();
        $data["metadata_title"] = $this->config->item('title', 'signup_metadata');
        $data["metadata_description"] = $this->config->item('description', 'signup_metadata');
        $data["metadata_keywords"] = $this->config->item('keywords', 'signup_metadata');

        $user_id = $this->session->userdata('user_id');
        $data['id_proof_names'] = $this->get_doc_category(1);
        $data['address_proof_names'] = $this->get_doc_category(2);
        $data['photo'] = $this->get_doc_category(3);
        $data["serviceDetails"] = $this->buisness_type_model->get_static_service();
        //echo SIGNUP_VIEW.'----------------';
        // show($data["serviceDetails"],1);
        if ($user_id == "") {
            load_front_view(SIGNUP_VIEW, $data);
        } else {
            redirect(base_url());
        }
    }

    /*
     * @function    : login_authenticate
     * @param       : 
     * @detail      : To check use credential. If user credential is correct
     *                then create a textfile of usermenu,usermenu permission,menu 
     *                and redirect to particular page else redirect to login 
     *                page.               
     */

    public function login_authenticate() {

        $login_post = $this->input->post(NULL, TRUE);
        $username = trim($login_post["username"]);
        $pass = trim($login_post["password"]);
        // show($username,0,'username');
        // show($pass,0,'pass');
        //show($login_post,1,'login_info');
        $pass = trim($this->cryptoJsAesDecrypt($username, $pass));

        // show($pass, 1);
        if ($pass) {
            $user_data = $this->session->userdata();
            // show($user_data, 1);
            if (!isset($user_data['id'])) {
                $username = rokad_encrypt($username, $this->config->item('pos_encryption_key'));
                $this->auth->login($username, $pass);
            } else {
                redirect(base_url() . "home");
            }
        } else {

            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'Some error occur. Try again.');
            redirect(base_url() . "admin/Login/login_view");
        }
    }

    /*
     * @function    : logout
     * @param       : 
     * @detail      : To delete usermenu,usermenu permission and menu textfiles 
     *                and redirect to login page.
     *                
     */

    public function logout() {
        $user_data = $this->session->userdata();
        $id = $this->session->userdata('id');
        $UserID = $this->session->userdata('user_id');
        $UserUpdate = array("session_id" => 'NULL');
        $UpdateId = $this->common_model->update('users', 'id', $UserID, $UserUpdate);
        /*         * **************** check if remember is on start ******************* */
        if ($this->input->cookie("remember_me") != "") {
            $this->users_model->id = $id;
            $this->users_model->remember_me = "";
            $remem_id = $this->users_model->save();

            if ($remem_id > 0) {
                delete_cookie("remember_me");
            }
        }

        $users_action_log = array(
            "user_id" => $user_data['user_id'],
            "name" => $user_data['display_name'],
            "rmn" => $user_data['username'],
            "rollname" => $user_data['role_name'],
            "url" => current_url(),
            "db_function" => "",
            "db_table_name" => "",
            "function_name" => "logout",
            "input_data" => "",
            "message" => "Logged out"
        );

        users_action_log_new($users_action_log);
        /*         * *************** check if remember is on end ******************* */

        $this->session->sess_destroy();
        unset($_SESSION);

        $this->output->clear_all_cache();
        if ($this->session->userdata('user_id') == "") {

            redirect(base_url() . "home");
        }
    }

    /*
     * @function    : do_register
     * @param       : 
     * @detail      : register form
     *                 
     */

    public function do_register() {

        $user_id = $this->session->userdata('user_id');

        if ($user_id == "") {
            load_front_view(REGISTER_VIEW);
        } else {
            redirect("acl/acl/aclmenu");
        }
    }

    private function register_retailor_detail() {
        $data['states'] = $this->state_master_model->get_all_state();
        return $data;
    }

    function is_email_registered() {
        $input = $this->input->post(NULL, TRUE);
        $is_exsit = $this->users_model->check_email_registered($input);
        echo $is_exsit;
    }

    public function get_statewise_city() {
        $data = array();
        $input = array();
        $input = $this->input->post();
        $data['city'] = $this->city_master_model->get_city($input['state_id']);
        //show($data['city'],1);
        data2json($data);
    }

    public function validate_age($age) {
        $dob = new DateTime($age);
        $now = new DateTime ();
        if ($now->diff($dob)->y >= 18) {
            return TRUE;
        } else {
            $this->form_validation->set_message('validate_age', 'Entered Age DOB should be above 18 years');
            return FALSE;
        }
    }

    public function mobileno_check($mobileno) {
        $chk_mob = $this->users_model->where(array(
                    'mobile_no' => $mobileno,
                    "is_deleted" => 'N'
                ))->find_all();
        if ($chk_mob) {
            $this->form_validation->set_message('mobileno_check', 'The mobile no allready exsits with us.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /*
     * @function    : register_authenticate
     * @param       : 
     * @detail      : to register user.               
     */

    public function register_authenticate() {
        $input = $this->input->post(NULL, TRUE);
        
        $service_name = '';

        logging_data("registration/registration_log.log", $input, "Post data for registration");
        //$encrypted_role_id = isset($input ['role']) ? trim($input ['role']) : "";
        //$level = isset($input ['level']) ? trim($input ['level']) : "";
        $firm_name = isset($input ['firm_name']) ? trim($input ['firm_name']) : "";
        $first_name = isset($input ['first_name']) ? trim($input ['first_name']) : "";
        $last_name = isset($input ['last_name']) ? trim($input ['last_name']) : "";
        $display_name = $first_name . " " . $last_name;
        $email = isset($input ['email']) ? trim($input ['email']) : "";
        $company_gst_no = isset($input ['company_gst_no']) ? trim($input ['company_gst_no']) : "";
        $mobile_no = isset($input ['mobile_no']) ? trim($input ['mobile_no']) : "";
        $gender = isset($input ['gender']) ? trim($input ['gender']) : "";
        $dob = isset($input ['dob']) ? date('Y-m-d', strtotime($input ['dob'])) : "";
        $address1 = isset($input ['address1']) ? trim($input ['address1']) : "";
        $agent_state = isset($input ['agent_state']) ? trim($input ['agent_state']) : "";
        $agent_city = isset($input ['agent_city']) ? trim($input ['agent_city']) : "";
        $pincode = isset($input ['pincode']) ? trim($input ['pincode']) : "";

        $address2 = isset($input ['address2']) ? trim($input ['address2']) : "";
        $stand_name = isset($input ['stand_name']) ? trim($input ['stand_name']) : "";
        $shop_location = isset($input ['shop_location']) ? trim($input ['shop_location']) : "";
        $agent_state1 = isset($input ['state1']) ? trim($input ['state1']) : "";
        $agent_city1 = isset($input ['city1']) ? trim($input ['city1']) : "";
        $pincode1 = isset($input ['pincode1']) ? trim($input ['pincode1']) : "";

        $pancard = isset($input ['pancard']) ? trim($input ['pancard']) : "";
        $adharcard = isset($input ['adharcard']) ? trim($input ['adharcard']) : "";
        $country_id = COUNTRY_ID;
        $refempname = isset($input ['emp_name']) ? trim($input ['emp_name']) : "";
        $refempmob = isset($input ['emp_mob']) ? trim($input ['emp_mob']) : "";
        $ref_emp_designation = isset($input ['ref_emp_designation']) ? trim($input ['ref_emp_designation']) : "";
        $current_business = isset($input ['current_business']) ? trim($input ['current_business']) : "";

        //by pallavi on date 24-01-19 for depot//
        $depot_name = isset($input ['depot_name']) ? trim($input ['depot_name']) : "";
        $depot_cd = isset($input ['depot_cd']) ? trim($input ['depot_cd']) : ""; 
        $division_cd = isset($input ['division_cd']) ? trim($input ['division_cd']) : "";
        
        $retailer_service_id = isset($input ['service_id']) ? trim($input ['service_id']) : "";

        $payment_mode = isset($input ['payment_mode']) ? trim($input ['payment_mode']) : "";
        $cheque_no = isset($input ['cheque_no']) ? trim($input ['cheque_no']) : "";
        $cheque_date = isset($input ['cheque_date']) ? trim(date("Y-m-d", strtotime($input ['cheque_date']))) : ""; //edited by sonali
        $branch = isset($input ['branch']) ? trim($input ['branch']) : "";
        $bank_name = isset($input ['bank_name']) ? trim($input ['bank_name']) : "";
        $amount = isset($input ['amount']) ? trim($input ['amount']) : "";
        $utr_no = isset($input ['utr_no']) ? trim($input ['utr_no']) : "";
        $transfer_date = isset($input ['transfer_date']) ? trim(date("Y-m-d", strtotime($input ['transfer_date']))) : ""; // edited by sonali




        //end pallavi code 
        //echo $refempmob; exit;
        // encryption done here
        $new_pancard = rokad_encrypt($pancard, $this->config->item("pos_encryption_key"));
        $new_adharcard = rokad_encrypt($adharcard, $this->config->item("pos_encryption_key"));
        $new_mobile = rokad_encrypt($mobile_no, $this->config->item("pos_encryption_key"));
        $new_pincode = rokad_encrypt($pincode, $this->config->item("pos_encryption_key"));
        $new_email = rokad_encrypt($email, $this->config->item("pos_encryption_key"));
        $new_dob = rokad_encrypt($dob, $this->config->item("pos_encryption_key"));
        // decryption done herer
        $role_id = rokad_decrypt($encrypted_role_id, $this->config->item("pos_encryption_key"));

        // if($input['creat_role'] == RETAILER_ROLE_ID)
        // {
        $ac_no = trim($input['accno']);
        $bank_nm = trim($input['bname']);
        $bran_nm = trim($input['brname']);
        $ifsc_code = trim($input['ifsc']);
        $accname = trim($input['accname']);
        //}

        $config = array(
            array(
                'field' => 'firm_name',
                'label' => 'Name of Firm/Company',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please enter name of firm/company',
                    'xss_clean' => 'CSRF is not allowed'
                )
            ),
            array(
                'field' => 'first_name',
                'label' => 'First Name',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please enter first name'
                )
            ),
            array(
                'field' => 'last_name',
                'label' => 'Last Name',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please enter last nanme'
                )
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
//                'rules' => 'trim|xss_clean|required|valid_email|is_unique[users.email]',
                 'rules' => 'trim|xss_clean|required|valid_email|callback_email_check',
                'errors' => array(
                    'required' => 'Please enter valid email',
                    'valid_email' => 'Please enter the valid email'
                    //'is_unique' => 'Email id already exsist with us'
                )
            ),
            array(
                'field' => 'mobile_no',
                'label' => 'Mobile Numebr',
                'rules' => 'trim|xss_clean|required|min_length[10]|max_length[10]|callback_mobileno_check'
            ),
            array(
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'trim|xss_clean'
            ),
            array(
                'field' => 'dob',
                'label' => 'Date of birth',
                'rules' => 'trim|xss_clean|required|callback_validate_age',
                'errors' => array(
                    'required' => 'Please select your age'
                )
            ),
            array(
                'field' => 'address1',
                'label' => 'Address1',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please enter the address'
                )
            ),
            array(
                'field' => 'agent_state',
                'label' => 'Agent State',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please select the state'
                )
            ),
            array(
                'field' => 'agent_city',
                'label' => 'Agent City',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please select the city'
                )
            ),
            array(
                'field' => 'pincode',
                'label' => 'Pincode',
                'rules' => 'trim|xss_clean|required|numeric|min_length[6]|max_length[6]',
                'errors' => array(
                    'required' => 'Please enter the pin code'
                )
            ),
            array(
                'field' => 'address2',
                'label' => 'Address2',
                'rules' => 'trim|xss_clean'
            ),
            array(
                'field' => 'depot_name',
                'label' => 'Depot Name',
                'rules' => 'trim|xss_clean'
            ),
            array(
                'field' => 'stand_name',
                'label' => 'Stand Name',
                'rules' => 'trim|xss_clean'
            ),
            array(
                'field' => 'shop_location',
                'label' => 'Shop Location',
                'rules' => 'trim|xss_clean'
            ),
            array(
                'field' => 'state1',
                'label' => 'State',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please select the state'
                )
            ),
            array(
                'field' => 'city1',
                'label' => 'City',
                'rules' => 'trim|xss_clean',
                'errors' => array(
                    'required' => 'Please select the city'
                )
            ),
            array(
                'field' => 'pincode1',
                'label' => 'Pincode',
                'rules' => 'trim|xss_clean|required|numeric|min_length[6]|max_length[6]',
                'errors' => array(
                    'required' => 'Please enter the pin code'
                )
            ),
            array(
                'field' => 'current_business',
                'label' => 'current_business',
                'rules' => 'trim|xss_clean|required',
                'errors' => array(
                    'required' => 'Please enter current business'
                )
            ),
            array(
                'field' => 'pancard',
                'label' => 'Pancard',
                'rules' => 'trim|xss_clean|required|min_length[10]|max_length[10]',
                'errors' => array(
                    'required' => 'Please enter valid Pancard Number'
                )
            ),
            array(
                'field' => 'adharcard',
                'label' => 'Aadhaar Card',
                'rules' => 'trim|xss_clean|required|min_length[12]|max_length[12]',
                'errors' => array(
                    'required' => 'Please enter valid Aadhaar Card Number'
                )
            )
        );
        // logging_data("registration/" . date("M") . "/registration_log.log", $config, "Validation error here");
        //$agent_cd = $this->users_model->generate_agent_id();
        // show(form_validate_rules($config), 1);
        if (form_validate_rules($config)) {
            //echo $refempmob; exit;
            //$this->users_model->agent_code = $agent_cd;
            //$this->users_model->role_id = $role_id;
            $this->users_model->username = $new_mobile;
            $this->users_model->firm_name = $firm_name;
            $this->users_model->first_name = $first_name;
            $this->users_model->last_name = $last_name;
            $this->users_model->gender = $gender;
            $this->users_model->email = $new_email;
            $this->users_model->company_gst_no = $company_gst_no;
            $this->users_model->address1 = $address1;
            $this->users_model->state = $agent_state;
            $this->users_model->city = $agent_city;
            $this->users_model->pincode = $new_pincode;

            $this->users_model->address2 = $address2;
            $this->users_model->stand_name = $stand_name;
            $this->users_model->shop_location = $shop_location;
            $this->users_model->state1 = $agent_state1;
            $this->users_model->city1 = $agent_city1;
            $this->users_model->pincode1 = $pincode1;

            $this->users_model->pancard = $new_pancard;
            $this->users_model->adharcard = $new_adharcard;
            $this->users_model->mobile_no = $new_mobile;
            $this->users_model->country = $country_id;
            $this->users_model->dob = $new_dob;
            $this->users_model->display_name = $display_name;
            $this->users_model->kyc_verify_status = "N";
            $this->users_model->email_verify_status = "N";
            $this->users_model->verify_status = "N";
            $this->users_model->status = "N";
            $this->users_model->is_deleted = "N";
            $this->users_model->created_date = date("Y-m-d h:i:s:a");
            $this->users_model->ip_address = $this->input->ip_address();
            $this->users_model->user_agent = $this->input->user_agent();
            $this->users_model->reg_from = "F";
            $this->users_model->reg_status = "N";
            $this->users_model->ref_empname = $refempname;
            $this->users_model->ref_empmob = $refempmob;
            $this->users_model->ref_emp_designation = $ref_emp_designation;
            $this->users_model->current_business = $current_business;
            $this->users_model->stand_name = $stand_name;
            $this->users_model->shop_location = $shop_location;
            //by pallavi on date 24-01-19
            $this->users_model->signup_depot_name = $depot_cd;
            $this->users_model->signup_depot_cd = $depot_name;

             
            $this->users_model->payment_mode = $payment_mode;
            $this->users_model->cheque_no = $cheque_no;
            $this->users_model->cheque_date = $cheque_date;
            $this->users_model->branch = $branch;
            $this->users_model->bank_name = $bank_name;  //added by sonali kamble 
            $this->users_model->amount = $amount;
            $this->users_model->utr_no = $utr_no;
            $this->users_model->transfer_date = $transfer_date;
            //end pallavi changes
            //
           //show($depot_name);
            //show($input['services']);
             //show($this->users_model, 1);
            //print_r($input); 
            //$ud = $this->upload_docs($input, 10);
            //var_dump($ud);die;
            $last_insert_id_users = $this->users_model->save();
            
            if (!empty($last_insert_id_users)) {

                /* Start: Update services - Gopal */
                if (!empty($input['services'])) {
                    if (in_array('10', $input['services'])) {
                        array_push($input['service_name'], '5');
                        array_push($input['service_name'], '6');
                    }
                    //show($input['services'],1);
                    foreach ($input['services'] as $service_id) {
                        if($service_id == '1'){
                           
                            $data['test'] = array(
                                'depot_code' => $depot_name,
                                'division_code' => $division_cd,
                                'service_id' => $retailer_service_id
                            );
                        }  else {
                           
                            $data['test'] = array(
                                'service_id' => $service_id
                            );
                            
                        }
                            
                        $data['test2'] = array(
                            'agent_id' => $last_insert_id_users,                            
                            'created_by' => $last_insert_id_users,
                            'updated_by' => $last_insert_id_users,
                            'updated_at' => date('Y-m-d H:i:s'),
                            
                        );
                        $data['insert_data']=  array_merge($data['test'],$data['test2']);
                        
                        $insert_service = $this->retailer_service_mapping_model->saveServiceDetails($data['insert_data']);
                        
                    }
                    //show($data['insert_data'],1);
                    $sQuery = "UPDATE retailer_service_mapping_log SET status='N' WHERE agent_id='" . $last_insert_id_users . "'";
                    $query = $this->db->query($sQuery);

                    //added by harshada kulkarni on 13-07-2018 for CR 335 -start
                    $serviceArrStr = implode(",", $input['services']);
                    
                    $logData = array(
                        'agent_id' => $last_insert_id_users,
                        'service_id' => $serviceArrStr,
                        'status' => "Y",
                        'created_by' => $last_insert_id_users,
                        'created_at' => date('Y-m-d H:i:s'),
                    );
                    
                    $insert_service_log = $this->retailer_service_mapping_model->saveServiceDetailsLog($logData);
                    //added by harshada kulkarni on 13-07-2018 for CR 335 -end
                }
                //die('sdjf');
                /* End: Update services - Gopal */

                $ud = $this->upload_docs($input, $last_insert_id_users);

                $this->agent_bank_detail_model->agent_id = $last_insert_id_users;
                $this->agent_bank_detail_model->bank_acc_name = $accname;
                $this->agent_bank_detail_model->bank_acc_no = $ac_no;
                $this->agent_bank_detail_model->bank_name = $bank_nm;
                $this->agent_bank_detail_model->bank_branch = $bran_nm;
                $this->agent_bank_detail_model->bank_ifsc_code = $ifsc_code;
                //$this->agent_bank_detail_model->inserted_by = $this->session->userdata('user_id');
                $this->agent_bank_detail_model->is_status = 1;
                $this->agent_bank_detail_model->inserted_date = date('Y-m-d H:i:s');
                $this->agent_bank_detail_model->is_deleted = 0;
                $this->agent_bank_detail_model->save();


                /* code by pallavi for email and sms date 23-01-2019 */

                $reg_sms_array = array(
                    "{{username}}" => ucwords($display_name),
                    "{{email}}" => $email
                );
                $temp_sms_msg = str_replace(array_keys($reg_sms_array), array_values($reg_sms_array), ROKAD_SIGNUP);
                $is_send = sendSMS($mobile_no, $temp_sms_msg);
                $response = save_n_send_activation_key($last_insert_id_users, $email, $display_name, 'email_signup');
                $this->send_verification_email($last_insert_id_users);

                /* end pallavi code */

                $this->session->set_flashdata('msg_type', 'success');
                $this->session->set_flashdata('msg', 'Registration completed successfully.');
                redirect(base_url() . 'admin/login/login_view');
            }
        } else {
            $error = $this->form_validation->error_array();
            // show($error, 1);
            $input = $this->input->post(NULL, TRUE);
            eval(LEVEL_CHAIN_CONSTANTS);
            $data ['states'] = $this->state_master_model->get_all_state();
            load_front_view(SIGNUP_VIEW, $data);
        }
    }

//end of register_authenticate


    /*
     * @function    : agent_register_authenticate
     * @param       : 
     * @detail      : to register agent.               
     */

    public function agent_register_authenticate() {
        //insert user in table
        $input = $this->input->post(NULL, TRUE);
        $agent_name = trim($input['agent_name']);
        $email = trim($input['email']);
        $organiazation = trim($input['organiazation']);
        $contact_no = trim($input['contact_no']);
        $agent_address = trim($input['agent_address']);
        $pincode = trim($input['pincode']);
        // for server side validation
        $config = array(
            array('field' => 'agent_name', 'label' => 'Agent name', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter agent name.')),
            array('field' => 'email', 'label' => 'Email', 'rules' => 'trim|required|valid_email|xss_clean', 'errors' => array('required' => 'Please enter valid email.')),
            array('field' => 'organiazation', 'label' => 'Organiazation', 'rules' => 'trim|required|xss_clean'),
            array('field' => 'contact_no', 'label' => 'Contact Number', 'rules' => 'trim|required|is_natural|max_length[11]|xss_clean'),
            array('field' => 'agent_address', 'label' => 'Agent Address', 'rules' => 'trim|required|xss_clean'),
            array('field' => 'pincode', 'label' => 'Pincode', 'rules' => 'trim|required|is_natural|max_length[6]|xss_clean')
        );

        if (form_validate_rules($config)) {
            $this->load->model('agent_model');
            $this->agent_model->email = $email;
            $agentr_exists = $this->agent_model->select();
            //$agentr_exists = $this->agent_model->check_agent_exists($email);
            if (empty($agentr_exists)) {
                $agent_id = $this->generateid_model->getRequiredId('booking_agents');
                if ($agent_id != "") {
                    //insert user in table

                    $this->agent_model->agent_id = $agent_id;
                    $this->agent_model->agent_name = $agent_name;
                    $this->agent_model->organization = $organization;
                    $this->agent_model->email = $email;
                    $this->agent_model->contact_no = $contact_no;
                    $this->agent_model->agent_address = $agent_address;
                    $this->agent_model->pincode = $pincode;
                    $last_insert_id = $this->agent_model->save();

                    //to add agent  and set session after user data entery in database
                    if ($last_insert_id > 0) {
                        $this->session->set_flashdata('msg_type', 'success');
                        $this->session->set_flashdata('msg', 'Registration completed successfully..');
                        redirect(base_url() . "home");
                    } else {
                        $this->session->set_flashdata('msg_type', 'error');
                        $this->session->set_flashdata('msg', 'Some error occured. Please try again.');
                        redirect(base_url() . "home");
                    }
                } else {
                    $this->session->set_flashdata('msg_type', 'error');
                    $this->session->set_flashdata('msg', 'Some error occured. Please try again.');
                    redirect(base_url() . "home");
                }
            } else {
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', 'Your id is already register with us. Please login.');
                redirect(base_url() . "home");
            }
        } else {
            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'Please enter proper data.');
            redirect(base_url() . "home");
        }
    }

    function thank() {
        load_front_view(THANK_YOU);
        // $this->load->view(THANK_YOU);
    }

//end of agent_register_authenticate


    /*
     * @function    : user_guest_booking
     * @param       : 
     * @detail      : to enter email and phone number of user in user table as guest user.               
     */

    public function user_guest_booking() {

        //insert user in table
        $input = $this->input->post();
        $email = trim($input['email']);
        $mobile_no = trim($input['mobile_no']);

        if ($email != "") {
            $this->load->model('users_model');
            $user_exists = $this->users_model->check_user_exists("", $email);
            if ($user_exists == 0) {
                $user_id = $this->generateid_model->getRequiredId('users');
                if ($user_id != "") {
                    // to get role id
                    $this->load->model('role_model');
                    $role_array = $this->role_model->get_roleid_by_name(USER_ROLE_NAME);

                    $userdata = array(
                        'user_id' => $user_id,
                        'role_id' => $role_array["role_id"],
                        "email" => $email,
                        "mobile_no" => $mobile_no
                    );

                    if ($this->users_model->add_user($userdata)) {
                        return $user_id;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {

                $guest_user = $this->users_model->get_user_by_email($email);
                return $guest_user["user_id"];
            }
        } else {
            return false;
        }
    }

//end of user_guest_booking


    /*
     * @Author      : Suraj Rathod
     * @function    : activate
     * @param       : $key -. key sent in mail
     * @detail      : to activate users.               
     */
    function activate($key, $validate_email = "") {

        if ($key != "") {
            $custom = array();
            $userdata = $this->users_model
                    ->where('activate_key', $key)
                    ->find_all();

            if ($userdata) {

                if ($userdata[0]->status != "Y") {
                    if ($userdata[0]->agent_type == "ors_agent" || $userdata[0]->agent_type == "distributor") {
                        if ($userdata[0]->email_verify_status != "Y") {
                            $to_update = array('email_verify_status' => "Y", "activate_key" => "");
                            $id = $this->users_model->update($userdata[0]->id, $to_update);
                            if ($id) {
                                $this->session->set_flashdata('msg_type', 'success');
                                $this->session->set_flashdata('msg', 'Email verified successfully.');
                                $data = array();
                                load_front_view(THANK, $data);
                            } else {
                                $this->session->set_flashdata('msg_type', 'error');
                                $this->session->set_flashdata('msg', 'Please try again.');
                                load_front_view(UNEXPECTED_ERROR);
                            }
                        }
                    } else {
                        $to_update = array('status' => "Y", 'verify_status' => "Y", "activate_key" => "");
                        $custom["verification_type"] = "account";
                        $custom["mail_title"] = "email_verification_success";
                        $this->activation_email($userdata, $to_update, $custom);
                    }
                } else if ($userdata[0]->status == "Y") {
                    if ($userdata[0]->agent_type == "ors_agent" || $userdata[0]->agent_type == "distributor") {
                        
                    } else {
                        if ($validate_email != "" && trim($validate_email) == "email") {
                            $to_update = array('verify_status' => "Y", "activate_key" => "");
                            $custom["verification_type"] = "email";
                            $custom["mail_title"] = "verify_email_id_success";
                            $this->activation_email($userdata, $to_update, $custom);
                        }
                    }
                    $this->session->set_flashdata('msg_type', 'success');
                    $this->session->set_flashdata('msg', 'User already activated.');

                    redirect(base_url() . "admin/login/login_view");
                } else {
                    $this->session->set_flashdata('msg_type', 'success');
                    $this->session->set_flashdata('msg', 'Some unexpected has happend. Please try again');

                    redirect(base_url() . "admin/login/login_view");
                }
            } else {
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', 'Invalid key. Please contact our customer support.');
                load_front_view(INVALID_KEY);
            }
        } else {
            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'Some unexpected has occured.');
            load_front_view(INVALID_KEY);
        }
    }

    /*
     * @Author      : Suraj Rathod
     * @function    : activate_user
     * @param       : $key -. key sent in mail
     * @detail      : to activate users.               
     */

    function activate_user_view($key) {
        $data = array();

        if ($key != "") {
            $userdata = $this->users_model
                    ->where('activate_key', $key)
                    ->find_all();

            if ($userdata && $this->input->cookie("akey") < 4) {
                if ($userdata[0]->verify_status != "Y") {
                    $input = $this->input->post(NULL, TRUE);

                    log_data("registration/" . date("M") . "/user_registration_log.log", $input, "Post data for registration");
                    $data["user_details"] = $userdata[0];
                    load_front_view(USER_REGISTRATION, $data);
                }
            } else {
                if ($this->input->cookie("akey") != "") {
                    $wrong_key = $this->input->cookie("akey");
                    $this->input->set_cookie("akey", ++$wrong_key, 7776000, '', '/');
                } else {
                    $this->input->set_cookie("akey", 0, 7776000, '', '/');
                }

                if ($this->input->cookie("akey") < 4) {
                    $this->session->set_flashdata('msg_type', 'error');
                    $this->session->set_flashdata('msg', 'Invalid key. Please contact our customer support.');
                    load_front_view(INVALID_KEY);
                } else {
                    $data["custom_title"] = "Security Warning";
                    $data["custom_heading"] = "Suspicious Activity";
                    $data["custom_type"] = "error";
                    $data["custom_message"] = "Security error detected Due to Suspicious Activity found on your computer.";
                    load_front_view(CUSTOM_ERROR, $data);
                }
            }
        } else {
            if ($this->input->cookie("akey") != "") {
                $wrong_key = $this->input->cookie("akey");
                $this->input->set_cookie("akey", ++$wrong_key, 7776000, '', '/');
            } else {
                $this->input->set_cookie("akey", 0, 7776000, '', '/');
            }
            /* $this->session->set_flashdata('msg_type', 'error');
              $this->session->set_flashdata('msg', 'Invalid key. Please contact our customer support.');
              load_front_view(INVALID_KEY); */
            if ($this->input->cookie("akey") < 4) {
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', 'Invalid key. Please contact our customer support.');
                load_front_view(INVALID_KEY);
            } else {
                $data["custom_title"] = "Security Warning";
                $data["custom_heading"] = "Suspicious Activity";
                $data["custom_type"] = "error";
                $data["custom_message"] = "Security error detected Due to Suspicious Activity found on your computer.";
                load_front_view(CUSTOM_ERROR, $data);
            }
        }
    }

    /*
     * @Author      : Suraj Rathod
     * @function    : activate_user
     * @param       : $key -. key sent in mail
     * @detail      : to activate users.               
     */

    function activate_user($key) {
        $input = $this->input->post(NULL, TRUE);

        log_data("registration/" . date("M") . "/user_registration_log.log", $input, "Post data for registration");
        log_data("registration/" . date("M") . "/user_registration_log.log", $key, "KEY for registration");

        if ($key != "") {
            $custom = array();
            $userdata = $this->users_model
                    ->where('activate_key', $key)
                    ->find_all();
            if ($userdata) {
                $first_name = trim($input['first_name']);
                $last_name = trim($input['last_name']);
                $display_name = $first_name . " " . $last_name;
                $username = trim($input['username']);
                $password = trim($input['password']);
                $confirm_password = trim($input['confirm_password']);
                // $g_recaptcha_response = trim($input['g-recaptcha-response']);

                $salt = getRandomId();
                // for server side validation
                $config = array(
                    array('field' => 'first_name', 'label' => 'First Name', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter first name.')),
                    array('field' => 'last_name', 'label' => 'Last Name', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter last name.')),
                    array('field' => 'username', 'label' => 'Username', 'rules' => 'trim|required|min_length[5]|xss_clean'),
                    array('field' => 'password', 'label' => 'password', 'rules' => 'trim|required|min_length[8]|xss_clean'),
                    array('field' => 'confirm_password', 'label' => 'Confirm Password', 'rules' => 'trim|required|matches[password]|xss_clean')
                );

                if (form_validate_rules($config)) {
                    // if (isset($g_recaptcha_response)) {
                    if ($password != "" and $password == $confirm_password) {
                        //Google Captcha
                        //$g_captcha_data = google_captcha($g_recaptcha_response);
                        // if ($g_captcha_data->success) {
                        if ($username != "" && $display_name != "") {
                            $user_exists = $this->users_model->check_user_registered($username);

                            if (empty($user_exists)) {
                                $pass = md5($salt . $password);
                                $this->role_model->role_name = USER_ROLE_NAME;
                                $rdata = $this->role_model->select();
                                $user_role_id = $rdata[0]->id;

                                $user_fields_update = array(
                                    "role_id" => $user_role_id,
                                    "username" => $username,
                                    "first_name" => $first_name,
                                    "last_name" => $last_name,
                                    "display_name" => $display_name,
                                    "salt" => $salt,
                                    "password" => $pass,
                                    "status" => "Y",
                                    "verify_status" => "Y",
                                    "activate_key" => "",
                                    "inserted_by" => $userdata[0]->id,
                                    /* "ip_address" => $this->input->ip_address(),
                                      "user_agent" => $this->input->user_agent(), */
                                    "ip_address" => $this->input->ip_address(),
                                    "user_agent" => $this->input->user_agent(),
                                );
                                $user_id = $this->users_model->update(array("activate_key" => $key), $user_fields_update);

                                if ($user_id) {
                                    //To generate wallet account
                                    // $this->wallet_model->w_id = $this->generateid_model->getRequiredId('wallet');
                                    $this->wallet_model->user_id = $userdata[0]->id;
                                    $this->wallet_model->amt = 0.00;
                                    $this->wallet_model->status = "Y";
                                    $this->wallet_model->comment = "Created Account";
                                    $this->wallet_model->added_by = $user_id;
                                    $this->wallet_model->save();



                                    $reg_username = $display_name;
                                    $reg_sms_array = array("{{username}}" => ucwords($reg_username));
                                    $temp_sms_msg = str_replace(array_keys($reg_sms_array), array_values($reg_sms_array), BOS_ACCOUNT_ACTIVATION);
                                    $is_send = sendSMS($userdata[0]->mobile_no, $temp_sms_msg);


                                    $mailbody_array = array("mail_title" => "email_verification_success",
                                        "mail_client" => $userdata[0]->email,
                                        "mail_replacement_array" => array("{{username}}" => ucwords($display_name))
                                    );

                                    $mail_result = setup_mail($mailbody_array);

                                    $this->session->set_flashdata('msg_type', 'success');
                                    $this->session->set_flashdata('msg', 'Registration completed successfully. Please login to verify your account.');

                                    redirect(base_url() . "admin/login/login_view");
                                } else {
                                    log_data("registration/" . date("M") . "/user_registration_log.log", "User not saved in database", "DB Query");

                                    $this->session->set_flashdata('msg_type', 'error');
                                    $this->session->set_flashdata('msg', 'Some error occured. Please try again.');
                                    redirect(base_url() . "admin/login/login_view");
                                }
                            }
                        } else {
                            log_data("registration/" . date("M") . "/user_registration_log.log", "Username,email,displayname empty", "empty fields");
                            $this->session->set_flashdata('msg_type', 'error');
                            $this->session->set_flashdata('msg', 'Please provide information to register.');
                            redirect(base_url() . "home");
                        }
                        // } else {
                        //     log_data("registration/" . date("M") . "/user_registration_log.log", "Captcha failed", "Captcha");
                        //     $this->session->set_flashdata('msg_type', 'error');
                        //     $this->session->set_flashdata('msg', 'Please verify captcha.');
                        //     redirect(base_url() . "home");
                        // }
                    } else {
                        $string_to_save = "Password missmatched. password entered is " . $password . " and confirm password entered is" . $confirm_password;
                        log_data("registration/" . date("M") . "/user_registration_log.log", $string_to_save, "Post data for registration");
                        $this->session->set_flashdata('msg_type', 'error');
                        $this->session->set_flashdata('msg', 'Password missmatched.');
                        redirect(base_url() . "home");
                    }
                    // } else {
                    //     log_data("registration/" . date("M") . "/user_registration_log.log", "Captcha not verified", "Captcha");
                    //     $this->session->set_flashdata('msg_type', 'error');
                    //     $this->session->set_flashdata('msg', 'Please verify captcha.');
                    //     redirect(base_url() . "home");
                    // }
                } else {
                    log_data("registration/" . date("M") . "/user_registration_log.log", "Form validation failed", "Form Validation");
                    $this->session->set_flashdata('msg_type', 'error');
                    $this->session->set_flashdata('msg', 'Please enter proper data.');
                    redirect(base_url() . "home");
                }
            } else {
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', 'Invalid key. Please contact our customer support.');
                load_front_view(INVALID_KEY);
            }
        } else {
            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'Some unexpected has occured.');
            load_front_view(INVALID_KEY);
        }
    }

    /*
     * @Author      : Suraj Rathod
     * @function    : activation_email
     * @param       : $key -. key sent in mail
     * @detail      : to email.               
     */

    function activation_email($userdata, $to_update, $custom_data = array()) {
        $id = $this->users_model->update($userdata[0]->id, $to_update);

        if ($id) {
            //Send sms
            $reg_username = ($userdata[0]->display_name != "") ? $userdata[0]->display_name : $userdata[0]->first_name . " " . $userdata[0]->last_name;
            $reg_sms_array = array("{{username}}" => ucwords($reg_username));
            $temp_sms_msg = str_replace(array_keys($reg_sms_array), array_values($reg_sms_array), BOS_ACCOUNT_ACTIVATION);
            $is_send = sendSMS($userdata[0]->mobile_no, $temp_sms_msg);

            if (isset($custom_data["mail_title"])) {
                $mailbody_array = array("mail_title" => $custom_data["mail_title"],
                    "mail_client" => $userdata[0]->email,
                    "mail_replacement_array" =>
                    array("{{username}}" => ucwords(strtolower($userdata[0]->first_name . " " . $userdata[0]->last_name))
                    )
                );

                $mail_result = setup_mail($mailbody_array);

                $this->session->set_flashdata('msg_type', 'success');

                if (!empty($custom_data) && isset($custom_data["verification_type"])) {
                    if (trim($custom_data["verification_type"]) == "account") {
                        $this->session->set_flashdata('msg', 'User activated successfully. Please login in to your account.');
                    } else if (trim($custom_data["verification_type"]) == "email") {
                        $this->session->set_flashdata('msg', 'Email verified successfully.');
                    }
                }

                redirect(base_url() . "admin/login/login_view");
            } else {
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', 'Verification done successfully.');
                redirect(base_url() . "admin/login/login_view");
            }
        } else {
            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'Please try again.');
            load_front_view(UNEXPECTED_ERROR);
        }
    }

    function resend_otp() {
        $response['msg_type'] = "";
        $response['msg'] = "";

        $input = $this->input->post();
        $resend_key = $input['resend_key'];
        $continue = true;

        $user_common_data = unserialize($this->session->userdata('user_common_data'));

        $user_id = $user_common_data['user_id'];
        $display_name = $user_common_data['display_name'];
        $mobile_no = $user_common_data['username'];

        if ($resend_key === "registration") {
            $sms_template = REGISTRATION_OTP;
            $reason = 'registration';
        } else if ($resend_key === "reset_rmn_old") {
            $sms_template = RESET_USERNAME_OTP;
            $reason = 'reset_username';
        } else if ($resend_key === "reset_rmn_new") {
            $mobile_no = $user_common_data['new_username'];
            $sms_template = RESET_USERNAME_OTP;
            $reason = 'reset_username';
        } else {
            $continue = false;
        }

        if ($continue) {
            $otp_response = check_otp_attempts('registration_otp');
            if ($otp_response['msg_type'] == 'success') {
                $response = save_n_send_otp($user_id, $mobile_no, $display_name, $sms_template, $reason);
            } else {
                $response['msg_type'] = "error";
                $response['msg'] = $otp_response['msg'];
            }
        }

        data2json($response);
    }

    function user_verify($verify_key = "", $hashid = "") {
        $this->session->unset_userdata('user_common_data');
        $data = array();
        $verify_email = false;
        $verify_mobile = false;
        $verify_adhar = false;

        $response = verify_email($verify_key, $hashid);
        if ($response['msg_type'] == 'success') {
            $user_id = $response['msg'];
            $email_verify_status = $response['email_verify'];
            $user_data = $this->users_model->where('id', $user_id)->find_all();
            if (count($user_data) > 0) {
                $user_data = $user_data[0];
                $display_name = $user_data->display_name;
                $mobile_no = rokad_decrypt($user_data->username, $this->config->item('pos_encryption_key'));
                $email = rokad_decrypt($user_data->email, $this->config->item('pos_encryption_key'));
                $adharcard = rokad_decrypt($user_data->adharcard, $this->config->item('pos_encryption_key'));

                $user_common_data = array('user_id' => $user_id,
                    'display_name' => $display_name,
                    'username' => $mobile_no,
                    'email' => $email,
                    'adharcard' => $adharcard
                );
                $this->session->set_userdata('user_common_data', serialize($user_common_data));

                // check if mobile is verified
                if ($user_data->otp_verify_status == "Y") {
                    /* if($user_data->adhar_verify_status == "Y")
                      {
                      // all details are verified
                      $data['msg_type'] = "success";
                      $data['msg'] = "All details have been verified successfully";
                      }
                      else
                      { // redirect to verify aadhar card
                      $random_key = sha1(getRandomId(8));
                      $this->session->set_userdata('random_key', $random_key);
                      $this->session->set_userdata('verify_mobile', 'Y');
                      redirect('admin/login/show_adhar_form/'.$random_key);
                      } */

                    // all details are verified
                    $data['msg_type'] = "success";
                    $data['msg'] = "All details have been verified successfully";
                } else {
                    $response = save_n_send_otp($user_id, $mobile_no, $display_name, REGISTRATION_OTP, 'registration');
                    if ($response['msg_type'] == 'success') {
                        // redirect to verify RMN
                        $random_key = sha1(getRandomId(8));
                        $this->session->set_userdata('random_key', $random_key);
                        $this->session->set_userdata('verify_email', 'Y');
                        redirect('admin/login/show_otp_form/' . $random_key . '/' . $email_verify_status);
                    } else {
                        $data['msg_type'] = "error";
                        $data['msg'] = $response['msg'];
                    }
                }
            } else {
                $data['msg_type'] = "error";
                $data['msg'] = "Invalid user";
            }
        } else {
            $data["msg_type"] = 'error';
            $data["msg"] = $response['msg'];
        }

        load_front_view(VERIFY_MSG, $data);
    }

    function show_otp_form($random_key = "", $email_status = "") {
        if ($random_key == $this->session->userdata('random_key')) {
            if ($this->session->userdata('verify_email') == "Y") {
                $data['msg_type'] = 'success';
                if ($email_status == 'already_verified') {
                    $data['msg'] = "Email already verified <br> OTP has been sent on your RMN";
                } elseif ($email_status == 'verified') {
                    $data['msg'] = "Email verified successfully <br> OTP has been sent on your RMN";
                }
                $data['show_OTP_form'] = true;
                load_front_view(EMAIL_VERIFY, $data);
            }
        }
    }

    function authenticate_otp() {
        clear_otp_attempts();
        $input = $this->input->post();

        $otp = trim($input['otp_code']);
        $data['msg_type'] = "";
        $data['msg'] = "";
        $data['show_OTP_form'] = true;
        if (!isset($input['otp_code'])) {
            $data['msg_type'] = "error";
            $data['msg'] = "Invalid Link";
            $data['show_OTP_form'] = false;
        }

        $this->form_validation->set_rules('otp_code', 'OTP', 'trim|required|numeric|exact_length[6]|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            load_front_view(EMAIL_VERIFY, $data);
        } else {
            $user_common_data = unserialize($this->session->userdata('user_common_data'));
            if ($user_common_data) {
                if (sizeof($user_common_data) > 0) {
                    $user_id = $user_common_data['user_id'];
                    $username = $user_common_data['username'];

                    $response = verify_otp($user_id, $username, $otp);
                    if ($response['msg_type'] == 'success') {
                        $this->users_model->verify_user_email($user_id);
                        $this->users_model->verify_user_mobile($user_id);

                        // redirect to verify aadhar card
                        /* $random_key = sha1(getRandomId(8));
                          $this->session->set_userdata('random_key', $random_key);
                          $this->session->set_userdata('verify_mobile', 'Y');
                          redirect('admin/login/show_adhar_form/'.$random_key); */

                        if ($this->save_login_send_welcome_mail($user_id)) {
                            $data['msg_type'] = "success";
                            $data['msg'] = "TPIN is sent on your registered Mobile No.";
                            $data['show_OTP_form'] = false;
                            $this->session->unset_userdata('user_common_data');
                        }
                    } else {
                        $data["msg_type"] = 'error';
                        $data["msg"] = $response['msg'];
                    }
                }
            }
            load_front_view(EMAIL_VERIFY, $data);
        }
    }

    function email_exists() {
        $input = $this->input->post(NULL, TRUE);

        $this->db->where('email', $input['email']);
        $this->db->from('users');

        $count = $this->db->count_all_results();

        $flag = ($count) ? 'true' : 'false';
        echo $flag;
    }

    /*
     * @Author      : Suraj Rathod
     * @function    : is_registered
     * @param       : 
     * @detail      :            
     */

    function is_registered() {
        $input = $this->input->post(NULL, TRUE);

        $where_user = array(
            "username !=" => "",
            "salt !=" => "",
            "password !=" => "",
            "email" => $input['email'],
                /* "role_id" => USER_ROLE_ID, */
        );

        $count = $this->users_model
                ->where($where_user)
                ->count_all();

        $flag = ($count) ? 'true' : 'false';
        echo $flag;
    }

    /*
     * @Author      : Suraj Rathod
     * @function    : is_user_exists
     * @param       : 
     * @detail      :            
     */

    function is_user_exists() {
        $input = $this->input->post();

        $where_user = array(
            "salt !=" => "",
            "password !=" => "",
            "role_id" => USER_ROLE_ID,
        );

        $where_or = array(
            "username" => $input['username'],
            "email" => $input['username'],
        );

        $count = $this->users_model
                ->or_where($where_or)
                ->where($where_user)
                ->count_all();

        $flag = ($count) ? 'true' : 'false';
        echo $flag;
    }

    function user_exists() {
        $input = $this->input->post(NULL, TRUE);
        $this->db->where('username', $input['username']);
        $this->db->or_where('email', $input['username']);
        $this->db->from('users');

        $count = $this->db->count_all_results();

        $flag = ($count) ? 'true' : 'false';
        echo $flag;
    }

    function is_mobile_no_registered() {
        $input = $this->input->post(NULL, TRUE);
        $is_exsit = $this->users_model->check_mobile_no_registered($input);
        echo $is_exsit;
    }

    function welcome_mail() {
        //show('welcome_mail', 1);
        $display_name = 'Keshav';
        $mailbody_array = array(
            "mail_title" => "rokad_registration",
            "mail_client" => 'keshavw@bookonspot.com',
            "mail_replacement_array" =>
            array(
                '{{fullname}}' => ucwords($display_name),
                '{{username}}' => '34245555151',
                '{{password}}' => 'abcd@123'
            )
        );
        //show($mailbody_array, 1);             
        $mail_result = setup_mail($mailbody_array);
    }

    function change_email_verify($verify_key = "", $hashid = "") {

        if (!empty($verify_key) && !empty($hashid)) {
            $data = array();
            $verify_email = false;
            $mail_verification_expiry = 24;
            $user_email_details = $this->user_email_model->get_userby_key_n_id($verify_key, $hashid);
            //show($user_email_details);            
            if (count($user_email_details) > 0) {

                $user_id = $user_email_details['user_id'];
                $user_email_encrypted = $user_email_details['user_mail'];

                $curr_date = date('Y-m-d H:i:s');
                $diff = get_difference_in_time($user_email_details['sent_date'], $curr_date, 3600);
                if ($diff <= $mail_verification_expiry) {
                    $this->user_email_model->update_verify_status($user_email_details['id']);
                    $user_data = $this->users_model->where('id', $user_id)->find_all();
                    if ($user_data) {
                        $display_name = $user_data[0]->display_name;
                        $data['user_id'] = $user_id;

                        //show($data, 1);                   
                        if ($user_data[0]->email_verify_status == "Y" && $user_data[0]->email == $user_email_encrypted) {
                            $data['msg_type'] = "success";
                            $data['msg'] = "This email id is already verified";
                            $verify_email = true;
                        } else {
                            $this->users_model->update_user_email($user_id, $user_email_encrypted);
                            $this->users_model->verify_user_email($user_id);
                            $data['msg_type'] = "success";
                            $data['msg'] = "Email verified successfully.";
                            $verify_email = true;
                        }
                        $this->session->sess_destroy();
                        unset($_SESSION);
                    } else {
                        $data['msg_type'] = "error";
                        $data['msg'] = "User not found";
                    }
                    load_front_view(CHANGE_EMAIL_VERIFY, $data);
                } else {
                    $data['msg_type'] = "error";
                    $data['msg'] = "Verification link expired";
                    load_front_view(CHANGE_EMAIL_VERIFY, $data);
                }
            } else {
                $data['msg_type'] = "error";
                $data['msg'] = "Invalid Link";
                load_front_view(CHANGE_EMAIL_VERIFY, $data);
            }
        } else {
            $data['msg_type'] = "error";
            $data['msg'] = "Invalid Parameters in url";
            load_front_view(CHANGE_EMAIL_VERIFY, $data);
        }
    }

    function reset_username_verify($verify_key = "", $hashid = "") {
        $this->session->unset_userdata('user_common_data');
        if (!empty($verify_key) && !empty($hashid)) {
            $data = array();
            $verify_email = false;
            $mail_verification_expiry = 24;
            $data['show_RMN_form'] = false;
            $user_email_details = $this->user_email_model->get_userby_key_n_id($verify_key, $hashid);

            if (count($user_email_details) > 0) {

                $user_id = $user_email_details['user_id'];
                $curr_date = date('Y-m-d H:i:s');
                $diff = get_difference_in_time($user_email_details['sent_date'], $curr_date, 3600);
                if ($diff <= $mail_verification_expiry) {
                    $where = array('id' => $user_id, 'is_deleted' => 'N', 'status' => 'Y');
                    $user_data = $this->users_model->where($where)->find_all();
                    if ($user_data) {
                        $user_data = $user_data[0];
                        $user_common_data = array('user_id' => $user_id,
                            'display_name' => $user_data->display_name,
                            'username' => rokad_decrypt($user_data->username, $this->config->item('pos_encryption_key')),
                            'email' => rokad_decrypt($user_data->email, $this->config->item('pos_encryption_key'))
                        );
                        $this->session->set_userdata('user_common_data', serialize($user_common_data));
                        $data['show_RMN_form'] = true;
                        load_front_view(RESET_USERNAME_VIEW, $data);
                    } else {
                        $data['msg_type'] = "error";
                        $data['msg'] = "User Disabled/Deleted or Invalid Link";
                        load_front_view(RESET_USERNAME_VIEW, $data);
                    }
                } else {
                    $data['msg_type'] = "error";
                    $data['msg'] = "Verification link expired";
                    load_front_view(RESET_USERNAME_VIEW, $data);
                }
            } else {
                $data['msg_type'] = "error";
                $data['msg'] = "Invalid Link";
                load_front_view(RESET_USERNAME_VIEW, $data);
            }
        } else {
            $data['msg_type'] = "error";
            $data['msg'] = "Invalid Parameters in url";
            load_front_view(RESET_USERNAME_VIEW, $data);
        }
    }

    function reset_username_authenticate() {
        clear_otp_attempts();
        $data['msg_type'] = "";
        $data['msg'] = "";

        $input = $this->input->post(NULL, TRUE);
        $inp_usernamme = trim(base64_decode($input['username_hidden']));
        $user_common_data = unserialize($this->session->userdata('user_common_data'));
        if ($user_common_data) {
            if (sizeof($user_common_data) > 0) {
                $user_id = $user_common_data['user_id'];
                $display_name = $user_common_data['display_name'];
                $username = $user_common_data['username'];
                $email = $user_common_data['email'];

                if ($inp_usernamme == $username) {
                    //show($user_data, 1);
                    $response = save_n_send_otp($user_id, $username, $display_name, RESET_USERNAME_OTP, 'reset_username');
                    if ($response['msg_type'] == 'success') {
                        $data['msg_type'] = $response['msg_type'];
                        $data['msg'] = $response['msg'];
                    } else {
                        $data['msg_type'] = 'error';
                        $data['msg'] = $response['msg'];
                    }
                } else { // invalid user or mobile no
                    $data['msg_type'] = "error";
                    $data['msg'] = "Enter valid registered mobile number";
                }
            } else {
                $data['msg_type'] = "error";
                $data['msg'] = "Error";
            }
        } else {
            $data['msg_type'] = "error";
            $data['msg'] = "Error";
        }
        data2json($data);
    }

    function validate_username_otp() {
        clear_otp_attempts();
        $data['msg_type'] = "";
        $data['msg'] = "";

        $input = $this->input->post(NULL, TRUE);
        $otp = trim(base64_decode($input['otp_hidden']));
        $new_username = trim(base64_decode($input['new_username_hidden']));
        $user_common_data = unserialize($this->session->userdata('user_common_data'));

        $user_id = $user_common_data['user_id'];
        $display_name = $user_common_data['display_name'];
        $username = $user_common_data['username'];
        $email = $user_common_data['email'];

        if (validate_otp($otp)) {
            if (validate_username($new_username)) {
                if ($new_username != $username) {
                    $response = verify_otp($user_id, $username, $otp);
                    if ($response['msg_type'] == 'success') {
                        $encrypted_new_username = rokad_encrypt($new_username, $this->config->item('pos_encryption_key'));
                        $exists = $this->users_model->check_username_exists($encrypted_new_username);
                        if ($exists) {
                            $data['msg_type'] = "error";
                            $data['msg'] = "Mobile no already registered";
                        } else {
                            $response = save_n_send_otp($user_id, $new_username, $display_name, RESET_USERNAME_OTP, 'reset_username');
                            if ($response['msg_type'] == 'success') {
                                $data['msg_type'] = $response['msg_type'];
                                $data['msg'] = $response['msg'];
                                $user_common_data['new_username'] = $new_username;
                                $this->session->set_userdata('user_common_data', serialize($user_common_data));
                            } else {
                                $data['msg_type'] = $response['msg_type'];
                                $data['msg'] = $response['msg'];
                            }
                        }
                    } else {
                        $data["msg_type"] = 'error';
                        $data["msg"] = $response['msg'];
                    }
                } else {   // invalid user or mobile no
                    $data['msg_type'] = "error";
                    $data['msg'] = "New mobile no can not be same as RMN.";
                }
            } else { // invalid user or mobile no
                $data['msg_type'] = "error";
                $data['msg'] = "Enter valid 10 digit new mobile number";
            }
        } else { // invalid user or mobile no
            $data['msg_type'] = "error";
            $data['msg'] = "Enter valid 6 digit otp number";
        }
        data2json($data);
    }

    function validate_newusername_otp() {
        clear_otp_attempts();
        $data['msg_type'] = "";
        $data['msg'] = "";

        $input = $this->input->post(NULL, TRUE);
        $otp = trim(base64_decode($input['new_otp_hidden']));
        $user_common_data = unserialize($this->session->userdata('user_common_data'));

        $user_id = $user_common_data['user_id'];
        $display_name = $user_common_data['display_name'];
        $username = $user_common_data['username'];
        $email = $user_common_data['email'];
        $new_username = $user_common_data['new_username'];
        $encrypted_new_username = rokad_encrypt($new_username, $this->config->item('pos_encryption_key'));

        if (validate_otp($otp)) {
            $response = verify_otp($user_id, $new_username, $otp);
            if ($response['msg_type'] == 'success') {
                $this->users_model->update_username($user_id, $encrypted_new_username);
                $data['msg_type'] = "success";
                $data['msg'] = "You have successfully changed Mobile No. Please login with new Mobile No.";

                $this->session->sess_destroy();
                unset($_SESSION);
            } else {
                $data["msg_type"] = 'error';
                $data["msg"] = $response['msg'];
            }
        } else { // invalid user or mobile no
            $data['msg_type'] = "error";
            $data['msg'] = "Enter valid 6 digit otp number";
        }
        data2json($data);
    }

    function cryptoJsAesDecrypt($passphrase, $jsonString) {
        $jsondata = json_decode($jsonString, true);
        try {
            $salt = hex2bin($jsondata["s"]);
            $iv = hex2bin($jsondata["iv"]);
        } catch (Exception $e) {
            return null;
        }
        $ct = base64_decode($jsondata["ct"]);
        $concatedPassphrase = $passphrase . $salt;
        $md5 = array();
        $md5[0] = md5($concatedPassphrase, true);
        $result = $md5[0];
        for ($i = 1; $i < 3; $i++) {
            $md5[$i] = md5($md5[$i - 1] . $concatedPassphrase, true);
            $result .= $md5[$i];
        }
        $key = substr($result, 0, 32);
        $data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
        return json_decode($data, true);
    }

    function show_adhar_form($random_key = "") {
        if ($random_key == $this->session->userdata('random_key')) {
            if ($this->session->userdata('verify_mobile') == "Y") {
                $user_common_data = unserialize($this->session->userdata('user_common_data'));
                if ($user_common_data) {
                    if (sizeof($user_common_data) > 0) {
                        $user_id = $user_common_data['user_id'];
                        $data['display_name'] = $user_common_data['display_name'];
                        $data['mobile_no'] = $user_common_data['username'];
                        $adharcard = $user_common_data['adharcard'];
                        if ($this->users_model->get_adhar_status($user_id) == 'Y') {
                            $data['msg_type'] = "error";
                            $data['msg'] = "Aadhar Details are already verified.";
                            load_front_view(VERIFY_MSG, $data);
                        } else {
                            $data['show_adhar_verify_link'] = false;
                            $data['show_adhar_form'] = false;
                            if ($adharcard == '') {
                                //show('i am here', 1);
                                $data['msg_type'] = 'success';
                                $data['msg'] = "Mobile number verified successfully";
                                $data['show_adhar_form'] = true;
                            } else {
                                $response = get_adhar_details($adharcard);
                                if ($response['msg_type'] == "success") {
                                    $data['adhar_details'] = $response['msg'];
                                    $data['show_adhar_verify_link'] = true;
                                } else {
                                    $data['msg_type'] = "error";
                                    $data['msg'] = $response['msg'];
                                    $data['show_adhar_form'] = true;
                                }
                            }
                            load_front_view(AADHAR_FORM, $data);
                        }
                    }
                }
            }
        }
    }

    function authenticate_adhar() {
        $input = $this->input->post();
        //show($input);
        $adharcard = trim($input['adharcard']);
        $data['msg_type'] = "";
        $data['msg'] = "";

        $data['show_adhar_verify_link'] = false;
        $data['show_adhar_form'] = false;

        $this->form_validation->set_rules('adharcard', 'Aadhar Card', 'trim|required|numeric|exact_length[12]|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $data['show_adhar_form'] = true;
            load_front_view(AADHAR_FORM, $data);
        } else {
            $response = get_adhar_details($adharcard);
            if ($response['msg_type'] == "success") {
                $user_common_data = unserialize($this->session->userdata('user_common_data'));
                if ($user_common_data) {
                    if (sizeof($user_common_data) > 0) {
                        $user_common_data['adharcard'] = $adharcard; // update user aadhar id
                        $this->session->set_userdata('user_common_data', serialize($user_common_data));

                        $data['display_name'] = $user_common_data['display_name'];
                        $data['mobile_no'] = $user_common_data['username'];

                        $data['adhar_details'] = $response['msg'];
                        $data['show_adhar_verify_link'] = true;
                    }
                }
            } else {
                $data['msg_type'] = "error";
                $data['msg'] = $response['msg'];
                $data['show_adhar_form'] = true;
            }
            load_front_view(AADHAR_FORM, $data);
        }
    }

    function adhar_init_success() {
        $inp = $this->input->get(NULL, TRUE);
        if (sizeof($inp) > 0) {
            if ($inp['hash'] != "" && $inp['uuid'] != "" && $inp['requestId'] != "" && $inp['status'] != "") {
                $concat_char = '|';

                $salt = $this->config->item("aadhar_salt");
                $saCode = $this->config->item("aadhar_saCode");

                $user_common_data = unserialize($this->session->userdata('user_common_data'));
                if ($user_common_data) {
                    if (sizeof($user_common_data) > 0) {
                        $adharId = $user_common_data['adharcard'];
                        $user_id = $user_common_data['user_id'];
                        if ($this->users_model->get_adhar_status($user_id) == 'Y') {
                            $data['msg_type'] = "error";
                            $data['msg'] = "Aadhar Details are already verified.";
                            load_front_view(VERIFY_MSG, $data);
                        } else {
                            $response_sequence = $salt . $concat_char . $inp['requestId'] . $concat_char . $adharId . $concat_char . $saCode;
                            $response_hash = hash('sha256', $response_sequence);

                            if ($inp['status'] == "success") {
                                if ($response_hash == $inp['hash']) { // hash matched at response so now we can fetch kyc data
                                    /* $ekyc_request_url    = $this->config->item("aadhar_ekyc_request_url");
                                      $headers= array('Accept: application/json','Content-Type: application/json');

                                      $ekyc_request_sequence  = $response_uuid . $concat_char . $saCode . $concat_char . $adharId . $concat_char . $response_requestId . $concat_char . $salt;
                                      $ekyc_request_hash      = hash('sha256', $ekyc_request_sequence);

                                      $ekyc_request_data = array(
                                      "saCode" => $saCode,
                                      "uuid" => $response_uuid,
                                      "requestId" => $response_requestId,
                                      "aadhaarId" => $adharId,
                                      "hash" => $ekyc_request_hash
                                      );

                                      $ch = curl_init($ekyc_request_url);
                                      curl_setopt($ch, CURLOPT_POST, TRUE);
                                      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($ekyc_request_data));
                                      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                                      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
                                      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
                                      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                      $result['response'] = curl_exec($ch);
                                      curl_close($ch);
                                      $response = json_decode($result['response']); */

                                    // if success then get the data from response and save to database
                                    // inform user that his kyc details are updated
                                    // if failure then show user error message

                                    $result = get_sample_response();
                                    $result = json_decode($result, true);

                                    //show($result);
                                    $ekyc_response_sequence = $salt . $concat_char . $result['requestId'] . $concat_char . $adharId . $concat_char . $saCode . $concat_char . $result['uuid'];
                                    $ekyc_response_hash = hash('sha256', $ekyc_response_sequence);

                                    if ($ekyc_response_hash == $result['hash']) {
                                        if ($result['success'] == true) {
                                            $user_photo = $this->save_adhar_photo($user_id, $result);
                                            //show($user_photo, 1);
                                            if ($user_photo) {
                                                if ($this->save_adhar_details($user_id, $result, $user_photo)) {
                                                    if ($this->save_login_send_welcome_mail($user_id, $adharId, $user_photo)) {
                                                        $data['msg_type'] = "success";
                                                        $data['msg'] = "Aadhar id is verified.<br> TPIN is sent on your registered Mobile No.";
                                                        $this->session->unset_userdata('user_common_data');
                                                    }
                                                }
                                            }
                                        } else {
                                            $response_errorcode = $result['aadhaar-status-code'];
                                            $error_msg = get_aadhar_error_msg($response_errorcode);
                                            if ($error_msg == 'error') {
                                                $error_msg = 'Error Occured, Please contact support';
                                            }
                                            $data['msg_type'] = "error";
                                            $data['msg'] = $error_msg;
                                        }
                                    } else {
                                        $data['msg_type'] = "error";
                                        $data['msg'] = "Hash does not match, try again";
                                    }
                                } else {
                                    $data['msg_type'] = "error";
                                    $data['msg'] = "Hash does not match, try again";
                                }
                            }
                            load_front_view(VERIFY_MSG, $data);
                        }
                    }
                }
            }
        }
    }

    function adhar_init_failure() {
        $inp = $this->input->get(NULL, TRUE);
        $data['msg_type'] = "";
        $data['msg'] = "";
        if (sizeof($inp) > 0) {
            if ($inp['hash'] != "" && $inp['uuid'] != "" && $inp['requestId'] != "" && $inp['status'] != "") {
                $concat_char = '|';

                $salt = $this->config->item("aadhar_salt");
                $saCode = $this->config->item("aadhar_saCode");

                $user_common_data = unserialize($this->session->userdata('user_common_data'));
                if ($user_common_data) {
                    if (sizeof($user_common_data) > 0) {
                        $adharId = $user_common_data['adharcard'];

                        $response_sequence = $salt . $concat_char . $inp['requestId'] . $concat_char . $adharId . $concat_char . $saCode;
                        $hash = hash('sha256', $response_sequence);

                        if ($inp['status'] == "failure") {
                            if ($hash == $inp['hash']) {
                                $error_msg = get_aadhar_error_msg($inp['err']);
                                if ($error_msg == 'error') {
                                    $error_msg = 'Error Occured, Please contact support';
                                }
                                $data['msg_type'] = "error";
                                $data['msg'] = $error_msg;
                            } else {
                                $data['msg_type'] = "error";
                                $data['msg'] = "Hash does not match, try again";
                            }
                        }
                    }
                }
            }
            load_front_view(VERIFY_MSG, $data);
        }
    }

    private function save_login_send_welcome_mail($user_id, $adharId = "", $user_photo = "") {
        $result = false;

        $user_data = $this->users_model->where('id', $user_id)->find_all();
        $user_data = $user_data[0];

        $display_name = $user_data->display_name;
        $email = rokad_decrypt($user_data->email, $this->config->item('pos_encryption_key'));
        $username = rokad_decrypt($user_data->username, $this->config->item('pos_encryption_key'));

        // generate password and save to database
        $password = getRandomId(8);
        $salt = getRandomId();
        $pass = md5($salt . $password);

        if ($this->users_model->save_password($user_id, $pass, $salt)) {
            $pass_data = array("user_id" => $user_id,
                "salt" => $salt,
                "password" => $pass,
                "created_date" => date('Y-m-d H:i:s')
            );

            if ($this->user_password_model->save_password_log($pass_data)) {
                // generate TPIN and save to database
                $tpin = generate_tpin();
                $encrypted_tpin = rokad_encrypt($tpin, $this->config->item('pos_encryption_key'));

                if ($this->users_model->save_tpin($user_id, $encrypted_tpin)) {
                    $tpin_data = array(
                        "user_id" => $user_id,
                        "tpin" => $encrypted_tpin,
                        "created_date" => date('Y-m-d H:i:s')
                    );

                    if ($this->user_tpin_model->save_tpin_log($tpin_data)) {
                        // log tpin saved
                        $tpin_log_data = array(
                            "user_id" => $user_id,
                            "tpin" => $tpin,
                            "encrypted_tpin" => $encrypted_tpin,
                            "sent_date" => date('Y-m-d H:i:s')
                        );

                        log_data("tpin/tpin_" . date("d-m-Y") . "_log.log", $tpin_log_data, 'otp_data array');
                        // log tpin saved
                        // send tpin sms to user
                        $reg_sms_array = array("{{username}}" => ucwords($display_name), "{{tpin}}" => $tpin);
                        $temp_sms_msg = str_replace(array_keys($reg_sms_array), array_values($reg_sms_array), REGISTRATION_TPIN);
                        $is_send = sendSMS($username, $temp_sms_msg);

                        // send welcome email to user
                        $mailbody_array = array(
                            "mail_title" => "rokad_registration",
                            "mail_client" => $email,
                            "mail_replacement_array" =>
                            array(
                                '{{fullname}}' => ucwords($display_name),
                                '{{username}}' => $username,
                                '{{password}}' => $password,
                                '{{rokad_support_mail}}' => ROKAD_SUPPORT_MAIL,
                                '{{home_page_url}}' => base_url(),
                                '{{logo}}' => '<img src="' . DOMAIN_NAME_VAL . '/assets/travelo/front/images/logo.png" style="width:104px;"  />'
                            )
                        );
                        //show($mailbody_array, 1);
                        $mail_result = setup_mail($mailbody_array);

                        // activate aadhar status
                        if ($adharId != "" && $user_photo != "") {
                            $enc_adharId = rokad_encrypt($adharId, $this->config->item('pos_encryption_key'));
                            $this->users_model->verify_user_adhar($user_id, $enc_adharId, $user_photo);
                        }

                        // auto activate user
                        $this->users_model->auto_activate_user($user_id);
                        $result = true;
                    }
                }
            }
        }

        return $result;
    }

    private function save_adhar_details($user_id, $result_data, $user_photo) {
        $this->load->model('aadhar_details_model');
        $kyc_data = $result_data['kyc'];

        $exists = $this->aadhar_details_model->is_user_exists($user_id);
        if ($exists != "" || $exists != NULL) {
            $this->aadhar_details_model->id = $exists['id'];
        }

        $this->aadhar_details_model->user_id = $user_id;
        $this->aadhar_details_model->aadhaar_id = $result_data['aadhaar-id'];
        $this->aadhar_details_model->photo_base64 = $kyc_data['photo'];
        $this->aadhar_details_model->photo_binary = $user_photo;
        $this->aadhar_details_model->name = $kyc_data['poi']['name'];
        $this->aadhar_details_model->dob = $kyc_data['poi']['dob'];
        $this->aadhar_details_model->gender = $kyc_data['poi']['gender'];
        $this->aadhar_details_model->phone = $kyc_data['poi']['phone'];
        $this->aadhar_details_model->email = $kyc_data['poi']['email'];

        $this->aadhar_details_model->father_name = $kyc_data['poa']['co'];
        $this->aadhar_details_model->house_num = $kyc_data['poa']['house'];
        $this->aadhar_details_model->landmark = $kyc_data['poa']['lm'];
        $this->aadhar_details_model->colony_name = $kyc_data['poa']['lc'];
        $this->aadhar_details_model->subdistrict = $kyc_data['poa']['subdist'];
        $this->aadhar_details_model->district = $kyc_data['poa']['dist'];
        $this->aadhar_details_model->state = $kyc_data['poa']['state'];
        $this->aadhar_details_model->country = $kyc_data['poa']['country'];
        $this->aadhar_details_model->pincode = $kyc_data['poa']['pc'];
        $this->aadhar_details_model->post_office_name = $kyc_data['poa']['po'];

        $this->aadhar_details_model->local_data = json_encode($kyc_data['local-data']);
        $this->aadhar_details_model->raw_CmpResp = $kyc_data['raw-CmpResp'];
        $this->aadhar_details_model->aadhaar_reference_code = $result_data['aadhaar-reference-code'];
        $this->aadhar_details_model->hash = $result_data['hash'];
        $this->aadhar_details_model->uuid = $result_data['uuid'];
        $this->aadhar_details_model->requestId = $result_data['requestId'];
        $this->aadhar_details_model->date_added = date('Y-m-d H:i:s');

        if ($this->aadhar_details_model->save()) {
            return true;
        }
        return false;
    }

    private function save_adhar_photo($user_id, $result_data) {
        $image_type = 'data:image/jpg;base64,';
        $image_data = str_replace($image_type, '', $result_data['kyc']['photo']);
        $image_data = str_replace(' ', '+', $image_data);
        $image_decoded = base64_decode($image_data); // Decode image using base64_decode
        $filename = rand(100, 999);
        $filename .= time() . '.jpg';
        if (file_put_contents(PROFILE_IMAGE . $filename, $image_decoded)) {
            return $filename;
        }
        return false;
    }

    function get_doc_category($id) {
        return $docs_nm = $this->agent_model->doc_name($id);
    }

    function upload_docs($input, $id) {
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/register/' . 'upload_docs_input.log', $input, 'upload docs input');
        //show($input,1);
        log_data('rest/smart_card_login_api_' . date('d-m-Y') . '.log', $input, 'api');
        $this->load->library('upload');
        $doccat = isset($input['doccat']) ? $input['doccat'] : "";
        $pi_doc = isset($input['docname1']) ? $input['docname1'] : " ";
        $pa_doc = isset($input['docname2']) ? $input['docname2'] : " ";
        $ph_doc = isset($input['docname3']) ? $input['docname3'] : " ";


        $i = 0;
        $doc_nm[$i] = $pi_doc;
        $doc_nm[++$i] = $pa_doc;
        $doc_nm[++$i] = $ph_doc;


        $docname = $this->agent_model->selected_doc_name($doccat, $doc_nm);
        //show($docname,1);
        $send_data = array();

        if (!file_exists(AGENT_DOCS_FOLDER . '/' . $id)) {
            $old = umask(0);
            mkdir(AGENT_DOCS_FOLDER . '/' . $id, 0777, true);
            umask($old);
        }
        $subfolder = array("id_proof", 'add_proof', 'photo');
        for ($c = 0; $c < count($subfolder); $c++) {
            if (!file_exists(AGENT_DOCS_FOLDER . '/' . $id . '/' . $subfolder[$c] . '/large')) {
                mkdir(AGENT_DOCS_FOLDER . '/' . $id . '/' . $subfolder[$c] . '/large', 0777, true);
            }
            if (!file_exists(AGENT_DOCS_FOLDER . '/' . $id . '/' . $subfolder[$c] . '/thumb')) {
                mkdir(AGENT_DOCS_FOLDER . '/' . $id . '/' . $subfolder[$c] . '/thumb', 0777, true);
            }
        }

        //$files = $_FILES;
        //show($files,1); die;
        $i = 0;
        $files['img_doc']['name'][$i] = $_FILES['img_doc1']['name'];
        $files['img_doc']['type'][$i] = $_FILES['img_doc1']['type'];
        $files['img_doc']['tmp_name'][$i] = $_FILES['img_doc1']['tmp_name'];
        $files['img_doc']['error'][$i] = $_FILES['img_doc1']['error'];
        $files['img_doc']['size'][$i] = $_FILES['img_doc1']['size'];

        $files['img_doc']['name'][++$i] = $_FILES['img_doc2']['name'];
        $files['img_doc']['type'][$i] = $_FILES['img_doc2']['type'];
        $files['img_doc']['tmp_name'][$i] = $_FILES['img_doc2']['tmp_name'];
        $files['img_doc']['error'][$i] = $_FILES['img_doc2']['error'];
        $files['img_doc']['size'][$i] = $_FILES['img_doc2']['size'];

        $files['img_doc']['name'][++$i] = $_FILES['img_doc3']['name'];
        $files['img_doc']['type'][$i] = $_FILES['img_doc3']['type'];
        $files['img_doc']['tmp_name'][$i] = $_FILES['img_doc3']['tmp_name'];
        $files['img_doc']['error'][$i] = $_FILES['img_doc3']['error'];
        $files['img_doc']['size'][$i] = $_FILES['img_doc3']['size'];

        //show($files,1); die;
        $img1_wid = AGENT_DOC_IMAGE_WIDTH;
        $img1_hei = AGENT_DOC_IMAGE_HEIGHT;
        $medium = 'thumb';
        $data = array();
        $i = 0;
        $cpt = count($files['img_doc']['name']);

        if ($_FILES && $_FILES['img_doc1']['name'] !== "") {

            for ($i = 0; $i < $cpt; $i++) {

                $_FILES['img_doc']['name'] = $files['img_doc']['name'][$i];
                $_FILES['img_doc']['type'] = $files['img_doc']['type'][$i];
                $_FILES['img_doc']['tmp_name'] = $files['img_doc']['tmp_name'][$i];
                $_FILES['img_doc']['error'] = $files['img_doc']['error'][$i];
                $_FILES['img_doc']['size'] = $files['img_doc']['size'][$i];
                $info = explode('.', $files['img_doc']['name'][$i]);

                $file_ext = $info[1];
                $date = date_create();
                $time_stamp = date_timestamp_get($date);

                $file_name = str_replace(" ", "_", strtolower($docname[$i])) . "." . strtolower($file_ext);
                $pathname[$i] = $file_name;
                //show($input[$i]);

                if ($doccat[$i] != "" && $doccat[$i] == 1) {
                    $fold[$i] = 'id_proof';
                }
                if ($doccat[$i] != "" && $doccat[$i] == 2) {
                    $fold[$i] = 'add_proof';
                }
                if ($doccat[$i] != "" && $doccat[$i] == 3) {
                    $fold[$i] = 'photo';
                }
                $this->upload->initialize($this->set_upload_options($pathname[$i], $fold[$i], $id));

                if ($this->upload->do_upload('img_doc')) {
                    $img_data = $this->upload->data();

                    $old_path = $img_data['full_path'];
                    $new_path = AGENT_DOCS_FOLDER . $id . '/' . $fold[$i] . '/thumb/' . $file_name;

                    $this->resize($old_path, $new_path, AGENT_DOC_IMAGE_WIDTH, AGENT_DOC_IMAGE_HEIGHT);


                    $send_data[] = array("agent_id" => $id,
                        "docs_cat" => $doccat[$i],
                        "docs_name" => strtolower($doc_nm[$i]),
                        "large_img_link" => $pathname[$i],
                        "thum_img_link" => $pathname[$i],
                        "is_status" => 1,
                        "is_deleted" => 0,
                        "created_by" => $this->session->userdata('user_id'),
                    );
                } else {
                    return false;
                    $data['upload_errors'][$i] = $this->upload->display_errors();
                }
            }
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/register/' . 'agent_files.log', $_FILES, 'agent_files');
            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/register/' . 'agent_docs.log', $send_data, 'agent_docs');
            //$this->resize($data, $id, $img1_wid, $img1_hei);
            $this->agent_model->save_agent_docs_detail_batch($send_data);
            return true;
        } else {
            return false;
        }
    }

    private function set_upload_options($pathname, $folder, $uid) {
        $config = array();


        $config['upload_path'] = AGENT_DOCS_FOLDER . $uid . '/' . $folder . '/large';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['overwrite'] = FALSE;
        $config['file_name'] = $pathname;
        return $config;
    }

    public function resize($org_img, $new_img, $width, $height) {
        $this->load->library('image_lib');

        $config['image_library'] = 'gd2';
        $config['source_image'] = $org_img;
        $config['maintain_ratio'] = TRUE;
        $config['quality'] = "100";
        $config['new_image'] = $new_img;
        $config['width'] = $width;
        $this->image_lib->initialize($config);

//        if (!$this->image_lib->resize()) {
//            echo $this->image_lib->display_errors();
//        } 
        if ($this->image_lib->resize()) {
            return array('success' => true);
        } else {
            return $this->image_lib->display_errors();
        }
    }


    //by pallavi  for depot master 24 jan 2019//
    public function get_depot_list() {
        
         $data = array();
         $data['depot'] = $this->depot_master_model->get_depot();
        //show($data['city'],1);
        data2json($data);
            
    }

     /*
     * @Author : Vaidehi
     * @function : email_check
     * @param : $email.
     * @detail : To chcek the server side duplicate entery.
     */

     function email_check($email) {
        $chk_mob = $this->users_model->where(array(
                    'email' => $email,
                    "is_deleted" => 'N'
                ))->find_all();
        if ($chk_mob) {
            $this->form_validation->set_message('email_check', 'This email id already exsits with us.');
            return FALSE;
        } else {
            return TRUE;
}
    }
    //added by sonali 6-july-19
    public function send_verification_email($last_insert_id_users) {	
//		$input = $this->input->post();	
//                show($last_insert_id_users,1);
//		$encrypted_user_id = trim($input['id']);
//		$user_id = base64_decode($encrypted_user_id);
        
                $user_id = $last_insert_id_users;
				
		$where_array = array("id" => $user_id);
		$user_data = $this->users_model->where($where_array)->find_all();
                
                //show($user_data,1);
		$allow_to_send_flag = true;
		$mail_verification_limit = 2; // in minutes
		if(count($user_data) > 0)
		{
			$user_data = $user_data[0];			
			$email_verify_status = $user_data->email_verify_status;	
                        
			if($email_verify_status == "N")
			{				
				$user_email_encrypted = $user_data->email;
				$user_email_plain = rokad_decrypt($user_email_encrypted, $this->config->item('pos_encryption_key'));
				$display_name = $user_data->display_name;							
				$last_sent = $this->user_email_model->get_last_sent($user_id, $user_email_encrypted);
                                
//				if(count($last_sent) > 0)
//				{	
//                                    
//					$curr_date = date('Y-m-d H:i:s');
//					$last_sent_date = $last_sent['sent_date'];			
//					$diff = get_difference_in_time($last_sent_date, $curr_date, 60);			
//					if($diff < $mail_verification_limit)
//					{					
//						// not allow to send
//						//show("not allow to send");					
//						$data["flag"] = '@#failed#@';
//						$data["msg_type"] = 'error';
//						$data["msg"] = 'Email verification link not allow to send again within '. $mail_verification_limit. ' minutes.';					
//						$allow_to_send_flag = false;						
//					}				
//				}					
			
				if($allow_to_send_flag)
				{
                                                                        
					// allow to send
					$response = save_n_send_activation_key($user_id, $user_email_plain, $display_name, 'email_verification');
					if($response['msg_type'] == 'success')
					{
						$data['flag'] = "@success#";	
					}
					else
					{
						$data["flag"] = '@#failed#@';
					}
					$data["msg_type"] = 'success';
					$data["msg"] = 'Email verification link sent successfully';				
				}
			}
			else
			{
				// User email is already verified
				$data["flag"] = '@#failed#@';
				$data["msg_type"] = 'error';
				$data["msg"] = 'User email is already verified';	
			}
		}
		else
		{
			// Invalid user
			$data["flag"] = '@#failed#@';
			$data["msg_type"] = 'error';
			$data["msg"] = 'Invalid user';	
		}
		data2json($data);
	}
        
        public function resend_email_verification_link()  {
            

        $user_data= $this->get_users_data();

            
//		$input = $this->input->post();		
//		$encrypted_user_id = trim($input['id']);
//		$user_id = base64_decode($encrypted_user_id);
//					
//		$where_array = array("id" => $user_id);
//		$user_data = $this->users_model->where($where_array)->find_all();
                $user_id='';
		$allow_to_send_flag = true;
		$mail_verification_limit = 2; // in minutes
		if(count($user_data) > 0){
                     foreach($user_data as $value){
            
            
           
        
        //show($data['email_verify_status'],1);
			//$user_data = $user_data[0];
                        //			
			//$email_verify_status = $user_data->email_verify_status;
                        $user_id = $value->id;
                        $email_verify_status = $value->email_verify_status;			
			
			if($email_verify_status == "N")
			{				
				//$user_email_encrypted = $user_data->email;
                                $user_email_encrypted = $value->email;

                            
				$user_email_plain = rokad_decrypt($user_email_encrypted, $this->config->item('pos_encryption_key'));
				//$display_name = $user_data->display_name;
                                $display_name = $value->display_name;							
							
				$last_sent = $this->user_email_model->get_last_sent($user_id, $user_email_encrypted);		
				if(count($last_sent) > 0)
				{				
					$curr_date = date('Y-m-d H:i:s');
					$last_sent_date = $last_sent['sent_date'];			
					$diff = get_difference_in_time($last_sent_date, $curr_date, 60);			
					if($diff < $mail_verification_limit)
					{					
						// not allow to send
						//show("not allow to send");					
						$data["flag"] = '@#failed#@';
						$data["msg_type"] = 'error';
						$data["msg"] = 'Email verification link not allow to send again within '. $mail_verification_limit. ' minutes.';					
						$allow_to_send_flag = false;						
					}				
				}					
			
				if($allow_to_send_flag)
				{
					// allow to send
					$response = save_n_send_activation_key($user_id, $user_email_plain, $display_name, 'email_verification');
					if($response['msg_type'] == 'success')
					{
						$data['flag'] = "@success#";	
					}
					else
					{
						$data["flag"] = '@#failed#@';
					}
					$data["msg_type"] = 'success';
					$data["msg"] = 'Email verification link sent successfully';				
				}
			}
			else
			{
				// User email is already verified
				$data["flag"] = '@#failed#@';
				$data["msg_type"] = 'error';
				$data["msg"] = 'User email is already verified';	
			}
		}
                }
		else
		{
			// Invalid user
			$data["flag"] = '@#failed#@';
			$data["msg_type"] = 'error';
			$data["msg"] = 'Invalid user';	
		}
		//data2json($data["msg"]);
                echo $data["msg"];
                exit();
	}
        
        public function get_users_data(){
             $sQuery = "SELECT `u`.`id` as `id`, `u`.`display_name`, `u`.`kyc_verify_status` as `kyc_status`, `u`.`email_verify_status` ,u.email,u.display_name
                            FROM `users` `u`
                            WHERE 
                            `is_deleted` = 'N'
                            AND `role_id` in ('0','7')
                            AND `kyc_verify_status` = 'N'
                            AND `email_verify_status` = 'N'
                            AND DATE_FORMAT(u.created_date, '%Y-%m-%d') >= '".START_DATE."'
                            AND DATE_FORMAT(u.created_date, '%Y-%m-%d') <= '".END_DATE."'
                            ";
        $query = $this->db->query($sQuery);
//        echo $this->db->last_query();
//        die;
        return $query->result();
        }
}

//end of login class

