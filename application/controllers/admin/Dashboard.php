<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {

        parent::__construct();
        is_logged_in();
        $this->load->model(array('users_model','api_balance_log_model', 'role_model', 'tickets_model', 'dashboard_model','cb_commission_distribution_model', 'providers_master_model', 'utilities_master_model','providers_utilities_model'));
    }

    public function index()
    {   
        $input = $this->input->post();    
        
        $data = array();
        $data['period'] = $this->input->post('period');
        $from_date  = date('Y-m-d');
        $to_date  = date('Y-m-d');
        $data['from_date'] = $from_date;
        $data['to_date'] = $to_date;
        if ($this->input->post('period') == 'current_month')
        {
            $data['period'] = $this->input->post('period');
            $from_date  = date('Y-m-01');
            $to_date  = date('Y-m-d');
            $data['from_date'] = $from_date;
            $data['to_date'] = $to_date;
        }
        if($this->input->post('period') == 'date_range'){
            $data['period'] = $this->input->post('period');
            $from_date = $this->input->post('from_date');
            $to_date = $this->input->post('till_date');
            $data['from_date'] = $from_date;
            $data['to_date'] = $to_date;
        }
        $data["analytics_data"] = array();
        $tickets_analytics      = array();
        $user_role_name         = $this->session->userdata("role_name");
        $user_role_id           = $this->session->userdata("role_id");
        $data["user_role_name"] = $user_role_name;
        $data["user_role_id"]   = $user_role_id;
       
        //$data['data'] = $this->get_list($data);
        
        if  (($this->session->userdata('role_id') == COMPANY_ROLE_ID)
        		|| ($this->session->userdata('role_id') == MASTER_DISTRIBUTOR_ROLE_ID)
                || ($this->session->userdata('role_id') == AREA_DISTRIBUTOR_ROLE_ID)
                || ($this->session->userdata('role_id') == DISTRIBUTOR
                || ($this->session->userdata('role_id') == RETAILER_ROLE_ID)        )
            )  
        {     
          	
            $temp_val = $this->all_entities_dashboard($data);
            $data['name'] = $temp_val;
            $data['total_commission_rokad'] = (isset($temp_val['total_commission_rokad']['0']['total_commission'])) ? ($temp_val['total_commission_rokad']['0']['total_commission']): "0";
            $data['active_agents_rokad'] = (isset($temp_val['active_agents_rokad']['0']['totalusersactive'])) ? ($temp_val['active_agents_rokad']['0']['totalusersactive']): "0";
            $data['total_gmv'] = (isset($temp_val['total_gmv']['0']['total_gmv'])) ? ($temp_val['total_gmv']['0']['total_gmv']): "0";
        } 
        elseif($this->session->userdata('role_id') == SUPERADMIN_ROLE_ID || $this->session->userdata('role_id') == TRIMAX_ROLE_ID || $this->session->userdata('role_id') ==  BUSINESS_ADMIN_ROLE_ID || $this->session->userdata('role_id') ==  OPERATION_ADMIN_ROLE_ID || $this->session->userdata('role_id') ==  OPERATION_SUPPORT_ROLE_ID)
        { 
//            show($data,1);
            $values                        = $this->admin_dashboard($data); 
            $data['active_users']                  = (isset($values['usersdata']['0']['totalusersactive']))  ? ($values['usersdata']['0']['totalusersactive']) : "0"; ;
            $data['pending_users_data']            = (isset($values['pending_users_data']['0']['totaluserspending'])) ?  ($values['pending_users_data']['0']['totaluserspending']):"0";
            $data['kycpending']                    = (isset($values['pending_kyc']['0']['kycpending'])) ? ($values['pending_kyc']['0']['kycpending']) : "0";
            $data['ticket_report_success']         = (isset($values['total_tickets']['0']['ticketsucess'])) ? ($values['total_tickets']['0']['ticketsucess']) : "0";
            $data['total_tickets_amt']             = (isset($values['total_tickets_amt']['0']['total_tickets_amt_master'])) ? ($values['total_tickets_amt']['0']['total_tickets_amt_master']) : "0";
            $data['final_commission']              = (isset($values['final_commission']['0']['final_commission'])) ? ($values['final_commission']['0']['final_commission']): "0";
            $data['final_commission_chained']      = (isset($values['final_commission_chained']['0']['final_commission_chained'])) ? ($values['final_commission_chained']['0']['final_commission_chained']): "0";
            /*code Added by Aparna- 2018-06-29 - CR 314*/
            $data['total_passenger']               = (isset($values['total_passenger']['0']['total_passenger'])) ? ($values['total_passenger']['0']['total_passenger']): "0";
            $data['total_tickets_number']      = (isset($values['total_tickets_number']['0']['total_tickets'])) ? ($values['total_tickets_number']['0']['total_tickets']): "0";
             /*code Added by Aparna- 2018-06-29 - CR 314*/
            $data['total_commission_rokad'] = (isset($values['total_commission_rokad']['0']['total_commission'])) ? ($values['total_commission_rokad']['0']['total_commission']): "0";
            $data['active_agents_rokad'] = (isset($values['active_agents_rokad']['0']['totalusersactive'])) ? ($values['active_agents_rokad']['0']['totalusersactive']): "0";
            $data['total_gmv'] = (isset($values['total_gmv']['0']['total_gmv'])) ? ($values['total_gmv']['0']['total_gmv']): "0";
        } 
      
//         print_r($this->db->last_query());  print_r('test_dashboard'); die;
        load_back_view(BACK_VIEW.'admin/dashboard/dashboard',$data);
    }
    
    public function admin_dashboard($userdata='')
    {
        
        $from_date = '';
        $to_date = '';
        if(!empty($userdata))
        {
            $from_date = $userdata['from_date']." 00:00:00";
            $to_date   = $userdata['to_date']." 23:59:59";
        }
        $data                        = array();
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id'); 
         /*code Modified by Aparna, passed from_date and to_date as parameter to function- 2018-06-29 - CR 314*/
        $users_data                  = $this->users_model->active_user_list($session_level,$session_id,$from_date,$to_date);      
        $pending_users_data          = $this->users_model->pending_users_list($session_level,$session_id,$from_date,$to_date);
        $kyc                         = $this->users_model->kyc_pending_list($session_level,$session_id,$from_date,$to_date);     
        $total_tickets               = $this->tickets_model->ticket_report_success($from_date,$to_date);        
        $total_tickets_amt           = $this->tickets_model->get_total_ticket_msrtc_amt($from_date,$to_date);
        $commission_earned           = $this->tickets_model->commission_earned_today($from_date,$to_date);        
        $commission_earned_chained   = $this->tickets_model->commission_earned_chained($from_date,$to_date);
         /*code Added by Aparna- 2018-06-29 - CR 314*/
        $total_passenger             = $this->tickets_model->get_total_passenger($from_date,$to_date);         
        $total_tickets_number        = $this->tickets_model->get_total_ticket_number($from_date,$to_date);
         /*code Added by Aparna- 2018-06-29 - CR 314*/
       
        $total_commission_rokad = $this->dashboard_model->total_commission($from_date,$to_date,'','');
        $active_agents_rokad = $this->dashboard_model->active_agents($from_date,$to_date,'','');
        $total_gmv = $this->dashboard_model->total_gmv($from_date,$to_date,'','');
        
        if(empty($users_data['0']['totalusersactive']))
        {
            $users_data          = '0';
        }
        if(empty($pending_users_data['0']['totaluserspending']))
        {
            $pending_users_data  = '0';
        }
        if(empty($kyc['0']['kycpending']))
        {
            $kyc                 = '0';
        }
        if(empty($total_tickets['0']['ticketsucess']))
        {
            $total_tickets       =  '0';
        }
        if(empty($total_tickets_amt['0']['total_tickets_amt_master']))
        {
            $total_tickets_amt       =  '0';
        }
        if(empty($commission_earned['0']['final_commission']))
        {
            $commission_earned   = '0';
        }
        if(empty($commission_earned_chained['0']['final_commission_chained']))
        {
            $commission_earned_chained   = '0';
        }
        if(empty($total_passenger['0']['total_passenger']))
        {
            $total_passenger = '0';
        }
        if(empty($total_tickets_number['0']['total_tickets']))
        {
            $total_tickets_number = '0';
        }
        if(empty($total_commission_rokad['0']['total_commission']))
        {
            $total_commission_rokad          = '0';
        }
        if(empty($active_agents_rokad['0']['totalusersactive'])){
        
            $active_agents_rokad          = '0';
        }
        if(empty($total_gmv['0']['total_gmv'])){
            
            $total_gmv          = '0';
        }
        
        $data['usersdata']                   = $users_data;
        $data['pending_users_data']          = $pending_users_data;
        $data['pending_kyc']                 = $kyc;
        $data['total_tickets']               = $total_tickets;
        $data['total_tickets_amt']           = $total_tickets_amt;
        $data['final_commission']            = $commission_earned;
        $data['final_commission_chained']    = $commission_earned_chained;
        $data['total_passenger']             = $total_passenger;
        $data['total_tickets_number']        = $total_tickets_number;       
        $data['total_commission_rokad']      = $total_commission_rokad;
        $data['active_agents_rokad']      = $active_agents_rokad;
        $data['total_gmv']      = $total_gmv;
//        show($data,1);
        return $data;
    }
    
    public function all_entities_dashboard($data)
    {

        $user_id  = $this->session->userdata("user_id");
        /* added by prabhat */
        $id = $commission_earned_masters = $vas_commission_details = array();
        $commission_earned_masters['0']['final_transaction_amt'] = 0;
        $vas_commission_details[0]['final_transaction_amt'] = 0;
        $commission_earned_masters['0']['final_earned_commission'] = 0;
        $vas_commission_details[0]['final_earned_commission'] = 0;
        $final_transaction_amt = $commission_earned_masters = $total_transactn_amt = $total_passenger = $total_tickets = 0;
         $data['total_transactn_amt']=0;
        /* end added by prabhat */
        $id = array();
        
        if(!empty($data))
        {
            $from_date = $data['from_date']." 00:00:00";
            $to_date   = $data['to_date']." 23:59:59";
        }
        
        if($data['user_role_id'] == COMPANY_ROLE_ID)
        {
           
        	$level    = COMPANY_LEVEL;
        	$column   = 'level_'.$level;
        	
        	//show($user_id);
        	
        	//show($column);
        	$result   = $this->users_model->get_users_under_master($column,$user_id);
        	if(is_array($result) && count($result) > 0)
        	{
        		$result_c = count($result);
        		$res      = $this->array_imp($result);
        	}
        	else
        	{
        		$result_c = '0';
        		$res      = '0';
        	}
        	
        	
        	$ticketdetails_total = $this->tickets_model->ticket_report_success_master($res,$from_date,$to_date);
        	//show($ticketdetails_total, 1);
        	if(is_array($ticketdetails_total) && count($ticketdetails_total) > 0)
        	{
        		$ticketdetails_total  = count($ticketdetails_total);
        	}
        	else
        	{
        		$ticketdetails_total = '0';
        	}
        	
        	$pending_kyc_total   = $this->users_model->pending_kyc_masters($res,$from_date,$to_date);
        	//show($pending_kyc_total);
        	if(is_array($pending_kyc_total) && count($pending_kyc_total) > 0)
        	{
        		$pending_kyc_total  = ($pending_kyc_total['0']['pendingkyc_masters']);
        	}
        	else
        	{
        		$pending_kyc_total = '0';
        	}
        	
        	$pending_activation = $this->users_model->pending_activation_masters($res,$from_date,$to_date);
        	//show($pending_activation);
        	if(is_array($pending_activation) && count($pending_activation) > 0)
        	{
        		$pending_activation  = ($pending_activation['0']['pending_activation_masters']);
        	}
        	else
        	{
        		$pending_activation = '0';
        	}
        	
        	// $commission_earned_masters = $this->tickets_model->commission_earned_masters($res,$column,$user_id);
        	// //show($pending_activation);
        	// if(is_array($commission_earned_masters) && count($commission_earned_masters) > 0)
        	// {
        	// 	$commission_earned_masters  = ($commission_earned_masters['0']['final_commission_masters']);
        	// }
        	// else
        	// {
        	// 	$commission_earned_masters = '0';
        	// }
        	
            $commission_earned_masters = $this->cb_commission_distribution_model->getDailyEarnedCommission($data['user_role_id'],$user_id,
                                                                                                          $from_date,$to_date);
           // show($commission_earned_masters);
            $vas_commission_details = $this->cb_commission_distribution_model->getDailyEarnedVasCommission($data['user_role_id'],$user_id,$from_date,$to_date);
            //show($vas_commission_details,1);
            
            if(count($commission_earned_masters) > 0 || count($vas_commission_details) > 0)
            {
                $final_transaction_amt  = $commission_earned_masters['0']['final_transaction_amt'] + $vas_commission_details[0]['final_transaction_amt'];
                $commission_earned_masters  = $commission_earned_masters['0']['final_earned_commission'] + $vas_commission_details[0]['final_earned_commission'];
            }

        	// $total_transactn_amt = $this->tickets_model->total_transactn_amt($res);
            $total_transactn_amt = $this->tickets_model->get_total_ticket_msrtc_amt($from_date,$to_date);
        	//show($pending_activation);
        	if(is_array($total_transactn_amt) && count($total_transactn_amt) > 0)
        	{
        		$total_transactn_amt  = ($total_transactn_amt['0']['total_tickets_amt_master']);
        	}
        	else
        	{
        		$total_transactn_amt = '0';
        	}
        	
        	// $network_transactn_commission = $this->tickets_model->network_transactn_commission($res,$column,$user_id);
        	// //show($pending_activation, 1);
        	// if(is_array($network_transactn_commission) && count($network_transactn_commission) > 0)
        	// {
        	// 	$network_transactn_commission  = ($network_transactn_commission['0']['network_commission_masters']);
        	// }
        	// else
        	// {
        	// 	$network_transactn_commission = '0';
        	// }

            $network_transactn_commission = $this->cb_commission_distribution_model->getChainCommissionData($data['user_role_id'],$user_id,
                                                                                                            $from_date,$to_date);
        
            if(is_array($network_transactn_commission) && count($network_transactn_commission) > 0)
            {
                $network_transactn_commission  = ($network_transactn_commission['0']['chain_commission_masters']);
            }
            else
            {
                $network_transactn_commission = '0';
            }

             /*code Added by Aparna- 2018-06-29 - CR 314*/
            $total_passenger = $this->tickets_model->get_total_passenger($from_date,$to_date);

            if(is_array($total_passenger) && count($total_passenger) > 0)
            {
                $total_passenger  = (!empty($total_passenger['0']['total_passenger']) ? $total_passenger['0']['total_passenger'] : '0');
            }
            else
            {
                $total_passenger = '0';
            }

            $total_tickets_number        = $this->tickets_model->get_total_ticket_number($from_date,$to_date);
            if(is_array($total_tickets_number) && count($total_tickets_number) > 0)
            {
                $total_tickets  = (!empty($total_tickets_number['0']['total_tickets']) ? $total_tickets_number['0']['total_tickets'] : '0');
            }
            else
            {
                $total_tickets = '0';
            }
             /*code Added by Aparna- 2018-06-29 - CR 314*/
              $total_commission_rokad = $this->dashboard_model->total_commission($from_date,$to_date,$column,$user_id);
             if(empty($total_commission_rokad['0']['total_commission']))
            {
                $total_commission_rokad          = '0';
        }
            $active_agents_rokad = $this->dashboard_model->active_agents($from_date,$to_date,$column,$user_id);
            if(empty($active_agents_rokad['0']['totalusersactive'])){

                $active_agents_rokad          = '0';
            }
            $total_gmv = $this->dashboard_model->total_gmv($from_date,$to_date,$column,$user_id);
            if(empty($total_gmv['0']['total_gmv'])){

                $total_gmv          = '0';
            }
        }
        elseif($data['user_role_id'] == MASTER_DISTRIBUTOR_ROLE_ID)
        {
           
            $level    = MASTER_DISTRIBUTOR_LEVEL;
            $column   = 'level_'.$level;
            $result   = $this->users_model->get_users_under_master($column,$user_id);
            if(is_array($result) && count($result) > 0)
            {
                $result_c = count($result);
                $res      = $this->array_imp($result);
            }
            else
            {
                $result_c = '0';
                $res      = '0';
            }
            $ticketdetails_total = $this->tickets_model->ticket_report_success_master($res,$from_date,$to_date);
            if(is_array($ticketdetails_total) && count($ticketdetails_total) > 0)
            {
                $ticketdetails_total  = count($ticketdetails_total);
            }
            else
            {
                $ticketdetails_total = '0';
            }
            
            $pending_kyc_total   = $this->users_model->pending_kyc_masters($res,$from_date,$to_date);
            //show($pending_kyc_total, 1);
            if(is_array($pending_kyc_total) && count($pending_kyc_total) > 0)
            {
                $pending_kyc_total  = ($pending_kyc_total['0']['pendingkyc_masters']);
            }
            else
            {
                $pending_kyc_total = '0';
            }
            
            $pending_activation = $this->users_model->pending_activation_masters($res,$from_date,$to_date);
            if(is_array($pending_activation) && count($pending_activation) > 0)
            {
                $pending_activation  = ($pending_activation['0']['pending_activation_masters']);
            }
            else
            {
                $pending_activation = '0';
            }
            
            // $commission_earned_masters = $this->tickets_model->commission_earned_masters($res,$column,$user_id);
            // if(is_array($commission_earned_masters) && count($commission_earned_masters) > 0)
            // {
            //     $commission_earned_masters  = ($commission_earned_masters['0']['final_commission_masters']);
            // }
            // else
            // {
            //     $commission_earned_masters = '0';
            // }

            $commission_earned_masters = $this->cb_commission_distribution_model->getDailyEarnedCommission($data['user_role_id'],$user_id,$from_date,$to_date);
            //show($commission_earned_masters);
            $vas_commission_details = $this->cb_commission_distribution_model->getDailyEarnedVasCommission($data['user_role_id'],$user_id,$from_date,$to_date);
            //show($vas_commission_details,1);
            
            if(count($commission_earned_masters) > 0 || count($vas_commission_details) > 0)
            {
                $final_transaction_amt  = $commission_earned_masters['0']['final_transaction_amt'] + $vas_commission_details[0]['final_transaction_amt'];
                $commission_earned_masters  = $commission_earned_masters['0']['final_earned_commission'] + $vas_commission_details[0]['final_earned_commission'];
            }

            // $total_transactn_amt = $this->tickets_model->total_transactn_amt($res);
            $total_transactn_amt = $this->tickets_model->get_total_ticket_msrtc_amt($from_date,$to_date);
            if(is_array($total_transactn_amt) && count($total_transactn_amt) > 0)
            {
                $total_transactn_amt  = ($total_transactn_amt['0']['total_tickets_amt_master']);
            }
            else
            {
                $total_transactn_amt = '0';
            }
            
            // $network_transactn_commission = $this->tickets_model->network_transactn_commission($res,$column,$user_id);
            // if(is_array($network_transactn_commission) && count($network_transactn_commission) > 0)
            // {
            //     $network_transactn_commission  = ($network_transactn_commission['0']['network_commission_masters']);
            // }
            // else
            // {
            //     $network_transactn_commission = '0';
            // }

            $network_transactn_commission = $this->cb_commission_distribution_model->getChainCommissionData($data['user_role_id'],$user_id,$from_date,$to_date);
            //show($network_transactn_commission,1);
            if(is_array($network_transactn_commission) && count($network_transactn_commission) > 0)
            {
                $network_transactn_commission  = ($network_transactn_commission['0']['chain_commission_masters']);
            }
            else
            {
                $network_transactn_commission = '0';
            }
            
             /*code Added by Aparna- 2018-06-29 - CR 314*/
            $total_passenger = $this->tickets_model->get_total_passenger($from_date,$to_date);

            if(is_array($total_passenger) && count($total_passenger) > 0)
            {
                $total_passenger  = (!empty($total_passenger['0']['total_passenger']) ? $total_passenger['0']['total_passenger'] : '0');
            }
            else
            {
                $total_passenger = '0';
            }

            $total_tickets_number        = $this->tickets_model->get_total_ticket_number($from_date,$to_date);
            if(is_array($total_tickets_number) && count($total_tickets_number) > 0)
            {
                $total_tickets  = (!empty($total_tickets_number['0']['total_tickets']) ? $total_tickets_number['0']['total_tickets'] : '0');
            }
            else
            {
                $total_tickets = '0';
            }
             /*code Added by Aparna- 2018-06-29 - CR 314*/
             $total_commission_rokad = $this->dashboard_model->total_commission($from_date,$to_date,$column,$user_id);
             
             if(empty($total_commission_rokad['0']['total_commission']))
            {
                $total_commission_rokad          = '0';
        }
             $active_agents_rokad = $this->dashboard_model->active_agents($from_date,$to_date,$column,$user_id);
            if(empty($active_agents_rokad['0']['totalusersactive'])){

                $active_agents_rokad          = '0';
            }
            $total_gmv = $this->dashboard_model->total_gmv($from_date,$to_date,$column,$user_id);
            if(empty($total_gmv['0']['total_gmv'])){

                $total_gmv          = '0';
            }
           
        }
        elseif($data['user_role_id'] == AREA_DISTRIBUTOR_ROLE_ID)
        {
             
            $level    = AREA_DISTRIBUTOR_LEVEL;
            $column   = 'level_'.$level;
            $result   = $this->users_model->get_users_under_master($column,$user_id);
            if(is_array($result) && count($result) > 0)
            {
                $result_c = count($result);
                $res = $this->array_imp($result);

            }
            else
            {
                $result_c = '0';
                $res      = '0';
            }
            $ticketdetails_total = $this->tickets_model->ticket_report_success_master($res,$from_date,$to_date);
            if(is_array($ticketdetails_total) && count($ticketdetails_total) > 0)
            {
                $ticketdetails_total  = count($ticketdetails_total);
            }
            else
            {
                $ticketdetails_total = '0';
            }
            
            $pending_kyc_total   = $this->users_model->pending_kyc_masters($res,$from_date,$to_date);
            if(is_array($pending_kyc_total) && count($pending_kyc_total) > 0)
            {
                $pending_kyc_total  = ($pending_kyc_total['0']['pendingkyc_masters']);
            }
            else
            {
                $pending_kyc_total = '0';
            }
            
            $pending_activation = $this->users_model->pending_activation_masters($res,$from_date,$to_date);
            if(is_array($pending_activation) && count($pending_activation) > 0)
            {
                $pending_activation  = ($pending_activation['0']['pending_activation_masters']);
            }
            else
            {
                $pending_activation = '0';
            }
            
            // $commission_earned_masters = $this->tickets_model->commission_earned_masters($res,$column,$user_id);
            // if(is_array($commission_earned_masters) && count($commission_earned_masters) > 0)
            // {
            //     $commission_earned_masters  = ($commission_earned_masters['0']['final_commission_masters']);
            // }
            // else
            // {
            //     $commission_earned_masters = '0';
            // }

            $commission_earned_masters = $this->cb_commission_distribution_model->getDailyEarnedCommission($data['user_role_id'],$user_id,$from_date,$to_date);
            //print_r(($commission_earned_masters);
            $vas_commission_details = $this->cb_commission_distribution_model->getDailyEarnedVasCommission($data['user_role_id'],$user_id,$from_date,$to_date);
            //print_r($vas_commission_details,1);
            
            if(count($commission_earned_masters) > 0 || count($vas_commission_details) > 0)
            {
                 $final_transaction_amt  = $commission_earned_masters['0']['final_transaction_amt'] + $vas_commission_details[0]['final_transaction_amt'];
                 $commission_earned_masters  = $commission_earned_masters['0']['final_earned_commission'] + $vas_commission_details[0]['final_earned_commission'];
            }
            
            
            // $total_transactn_amt = $this->tickets_model->total_transactn_amt($res,$column,$user_id);
            $total_transactn_amt = $this->tickets_model->get_total_ticket_msrtc_amt($from_date,$to_date);
            if(is_array($total_transactn_amt) && count($total_transactn_amt) > 0)
            {
                $total_transactn_amt  = ($total_transactn_amt['0']['total_tickets_amt_master']);
            }
            else
            {
                $total_transactn_amt = '0';
            }
            
            // $network_transactn_commission = $this->tickets_model->network_transactn_commission($res,$column,$user_id);
            // if(is_array($network_transactn_commission) && count($network_transactn_commission) > 0)
            // {
            //     $network_transactn_commission  = ($network_transactn_commission['0']['network_commission_masters']);
            // }
            // else
            // {
            //     $network_transactn_commission = '0';
            // }

             $network_transactn_commission = $this->cb_commission_distribution_model->getChainCommissionData($data['user_role_id'],$user_id,$from_date,$to_date);
            //show($network_transactn_commission,1);
            if(is_array($network_transactn_commission) && count($network_transactn_commission) > 0)
            {
                $network_transactn_commission  = ($network_transactn_commission['0']['chain_commission_masters']);
            }
            else
            {
                $network_transactn_commission = '0';
            }

             /*code Added by Aparna- 2018-06-29 - CR 314*/
            $total_passenger = $this->tickets_model->get_total_passenger($from_date,$to_date);

            if(is_array($total_passenger) && count($total_passenger) > 0)
            {
                $total_passenger  = (!empty($total_passenger['0']['total_passenger']) ? $total_passenger['0']['total_passenger'] : '0');
            }
            else
            {
                $total_passenger = '0';
            }

            $total_tickets_number        = $this->tickets_model->get_total_ticket_number($from_date,$to_date);
            if(is_array($total_tickets_number) && count($total_tickets_number) > 0)
            {
                $total_tickets  = (!empty($total_tickets_number['0']['total_tickets']) ? $total_tickets_number['0']['total_tickets'] : '0');
            }
            else
            {
                $total_tickets = '0';
            }
             /*code Added by Aparna- 2018-06-29 - CR 314*/
        }
        elseif($data['user_role_id'] == DISTRIBUTOR)
        {
             
            $level  = POS_DISTRIBUTOR_LEVEL;
            $column = 'level_'.$level;
            $result = $this->users_model->get_users_under_master($column,$user_id);

            if(is_array($result) && count($result) > 0)
            {
                $result_c = count($result);
                $res = $this->array_imp($result);
            }
            else
            {
                $result_c = '0';
                $res      = '0';
            }

            $ticketdetails_total = $this->tickets_model->ticket_report_success_master($res,$from_date,$to_date);
            
            if(is_array($ticketdetails_total) && count($ticketdetails_total) > 0)
            {
                $ticketdetails_total  = count($ticketdetails_total);
            }
            else
            {
                $ticketdetails_total = '0';
            }
            
            $pending_kyc_total   = $this->users_model->pending_kyc_masters($res,$from_date,$to_date);
            
            if(is_array($pending_kyc_total) && count($pending_kyc_total) > 0)
            {
                $pending_kyc_total  = ($pending_kyc_total['0']['pendingkyc_masters']);
            }
            else
            {
                $pending_kyc_total = '0';
            }
            
            $pending_activation = $this->users_model->pending_activation_masters($res,$from_date,$to_date);
            
            if(is_array($pending_activation) && count($pending_activation) > 0)
            {
                $pending_activation  = ($pending_activation['0']['pending_activation_masters']);
            }
            else
            {
                $pending_activation = '0';
            }
            
            // $commission_earned_masters = $this->tickets_model->commission_earned_masters($res,$column,$user_id);
            // if(is_array($commission_earned_masters) && count($commission_earned_masters) > 0)
            // {
            //     $commission_earned_masters  = ($commission_earned_masters['0']['final_commission_masters']);
            // }
            // else
            // {
            //     $commission_earned_masters = '0';
            // }
            
            $commission_earned_masters = $this->cb_commission_distribution_model->getDailyEarnedCommission($data['user_role_id'],$user_id,$from_date,$to_date);
           $vas_commission_details = $this->cb_commission_distribution_model->getDailyEarnedVasCommission($data['user_role_id'],$user_id,$from_date,$to_date);
            //show($vas_commission_details,1);
            
            if(count($commission_earned_masters) > 0 || count($vas_commission_details) > 0)
            {
                $final_transaction_amt  = $commission_earned_masters['0']['final_transaction_amt'] + $vas_commission_details[0]['final_transaction_amt'];
                $commission_earned_masters  = $commission_earned_masters['0']['final_earned_commission'] + $vas_commission_details[0]['final_earned_commission'];
            }

            // $total_transactn_amt = $this->tickets_model->total_transactn_amt($res,$column,$user_id);
            $total_transactn_amt = $this->tickets_model->get_total_ticket_msrtc_amt($from_date,$to_date);

            if(is_array($total_transactn_amt) && count($total_transactn_amt) > 0)
            {
                $total_transactn_amt  = ($total_transactn_amt['0']['total_tickets_amt_master']);
            }
            else
            {
                $total_transactn_amt = '0';
            }
            
            // $network_transactn_commission = $this->tickets_model->network_transactn_commission($res,$column,$user_id);
            // if(is_array($network_transactn_commission) && count($network_transactn_commission) > 0)
            // {
            //     $network_transactn_commission  = ($network_transactn_commission['0']['network_commission_masters']);
            // }
            // else
            // {
            //     $network_transactn_commission = '0';
            // }

             $network_transactn_commission = $this->cb_commission_distribution_model->getChainCommissionData($data['user_role_id'],$user_id,
            $from_date,$to_date);
            //show($network_transactn_commission,1);
            if(is_array($network_transactn_commission) && count($network_transactn_commission) > 0)
            {
                $network_transactn_commission  = ($network_transactn_commission['0']['chain_commission_masters']);
            }
            else
            {
                $network_transactn_commission = '0';
            }

             /*code Added by Aparna- 2018-06-29 - CR 314*/
            $total_passenger = $this->tickets_model->get_total_passenger($from_date,$to_date);

            if(is_array($total_passenger) && count($total_passenger) > 0)
            {
                $total_passenger  = (!empty($total_passenger['0']['total_passenger']) ? $total_passenger['0']['total_passenger'] : '0');
            }
            else
            {
                $total_passenger = '0';
            }

            $total_tickets_number        = $this->tickets_model->get_total_ticket_number($from_date,$to_date);
            if(is_array($total_tickets_number) && count($total_tickets_number) > 0)
            {
                $total_tickets  = (!empty($total_tickets_number['0']['total_tickets']) ? $total_tickets_number['0']['total_tickets'] : '0');
            }
            else
            {
                $total_tickets = '0';
            }
            /*code Added by Aparna- 2018-06-29 - CR 314*/
        }
        else if($data['user_role_id'] == RETAILER_ROLE_ID)
        {
            
            $level  = RETAILOR_LEVEL;
            $column = 'level_'.$level;
            $ticketdetails_total = $this->tickets_model->ticket_report_success_master($user_id);
            if(is_array($ticketdetails_total) && count($ticketdetails_total) > 0)
            {
                $ticketdetails_total  = count($ticketdetails_total);
            }
            else
            {
                $ticketdetails_total = '0';
            }
            
            // $commission_earned_masters = $this->tickets_model->commission_earned_masters($user_id,$column,$user_id);
            // if(is_array($commission_earned_masters) && count($commission_earned_masters) > 0)
            // {
            //     $commission_earned_masters  = ($commission_earned_masters['0']['final_commission_masters']);
            // }
            // else
            // {
            //     $commission_earned_masters = '0';
            // }

             $commission_earned_masters = $this->cb_commission_distribution_model->getDailyEarnedCommission($data['user_role_id'],$user_id,$from_date,$to_date);
            $vas_commission_details = $this->cb_commission_distribution_model->getDailyEarnedVasCommission($data['user_role_id'],$user_id,$from_date,$to_date);
           // print_r($vas_commission_details); die;
            //pallavi changes
            if(count($commission_earned_masters) > 0 || count($vas_commission_details) > 0)
            { 
                $final_transaction_amt  = $commission_earned_masters['0']['final_transaction_amt'] + $vas_commission_details[0]['final_transaction_amt'];
                $commission_earned_masters  = $commission_earned_masters['0']['final_earned_commission'] + $vas_commission_details[0]['final_earned_commission'];
             //    $commission_earned_masters  = $vas_commission_details[0]['final_earned_commission'];
            }


            // $total_transactn_amt = $this->tickets_model->total_transactn_amt($user_id,$column,$user_id);
            $total_transactn_amt = $this->tickets_model->get_total_ticket_msrtc_amt($from_date,$to_date);
            if(is_array($total_transactn_amt) && count($total_transactn_amt) > 0)
            {
                $total_transactn_amt  = ($total_transactn_amt['0']['total_tickets_amt_master']);
            }
            else
            {
                $total_transactn_amt = '0';
            }

             /*code Added by Aparna- 2018-06-29 - CR 314*/
           $total_passenger = $this->tickets_model->get_total_passenger($from_date,$to_date);

            if(is_array($total_passenger) && count($total_passenger) > 0)
            {
                $total_passenger  = (!empty($total_passenger['0']['total_passenger']) ? $total_passenger['0']['total_passenger'] : '0');
            }
            else
            {
                $total_passenger = '0';
            }

            $total_tickets_number        = $this->tickets_model->get_total_ticket_number($from_date,$to_date);
            if(is_array($total_tickets_number) && count($total_tickets_number) > 0)
            {
                $total_tickets  = (!empty($total_tickets_number['0']['total_tickets']) ? $total_tickets_number['0']['total_tickets'] : '0');
            }
            else
            {
                $total_tickets = '0';
            }
             /*code Added by Aparna- 2018-06-29 - CR 314*/
        }
        //print_r($commission_earned_masters); die;
        $data['result_c']                      = $result_c;
        $data['ticketdetails_total']           = $ticketdetails_total;
        $data['pending_kyc_total']             = $pending_kyc_total;
        $data['pending_activation_total']      = $pending_activation;
        $data['commission_earned_masters']     = round($commission_earned_masters,2);
        $data['total_transactn_amt']           = $total_transactn_amt + $final_transaction_amt;
        $data['network_transactn_commission']  = round($network_transactn_commission,2);
         /*code Added by Aparna- 2018-06-29 - CR 314*/
        $data['total_passenger']               = $total_passenger;
        $data['total_tickets']                 = $total_tickets;
         /*code Added by Aparna- 2018-06-29 - CR 314*/
        $data['total_commission_rokad']      = $total_commission_rokad;
        $data['active_agents_rokad']      = $active_agents_rokad;
        $data['total_gmv']      = $total_gmv;
        
//	echo"<pre>"; print_r($data); die;
        return $data;
    }
    
    public function array_imp($result)
    {
        
        foreach($result as $value)
        {
            $id[] = $value['id'];
       
        }
        //$id =  implode(',', $id);
        
        return $id;
    }
    
    public function export_success_cancel_ticket()
    {
        $output_array = array();
        $input = $this->input->post();

        $image = $input["image"];
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $image = str_replace('[removed]', '', $image);
        $output_array["image3"] = $image;
        $image = base64_decode(trim($image));
        $output_array["image4"] = $image;
        $file = TMP_PATH.'export_success_cancel_ticket.png';
        file_put_contents($file, $image);

        eval(ADMIN_EMAIL_ARRAY);
        $mail_content = array(
                                "subject" => "Last 30 Days Ticket Report",
                                "message" => "Please find the attached image",
                                "to" => $admin_array,
                                "attachment" => TMP_PATH.'export_success_cancel_ticket.png'
                             );

        send_mail($mail_content);
    }

    public function assign_providers(){
        
        $data = [];
        $data['provider_array'] = $this->providers_master_model->find_all();
        $data['utilities_array'] = $this->utilities_master_model->find_all();
        $mapped_data = $this->db->query("SELECT pm.id as provider_id, um.id as utility_id FROM                             providers_master pm 
                                        JOIN providers_utilities pu on pm.id = pu.provider_id
                                        JOIN utilities_master um on um.id = pu.utilities_id");
        
        $selected_array = [];
        foreach ($mapped_data->result_array() as $key => $map) {
            $selected_array[$map['provider_id']][] = $map['utility_id'];
        }

        $data['selected_array'] = $selected_array;

        load_back_view(ASSIGN_PROVIDERS_TO_SERVICES, $data);      
    }

    public function save_assign_providers(){

        $input = $this->input->post();
        $provider_data = [];

        foreach($input['providers'] as $key => $prov){
            $check_if_provider_data_exist = $this->providers_utilities_model->findproviders($key);

            if(count($check_if_provider_data_exist->result()) > 0){
                $this->providers_utilities_model->deleterecords($key);
            }

            foreach ($prov as $utility) {
                $data['provider_id'] = $key;
                $data['utilities_id'] = $utility;              
                $data['created_at'] = date_format(date_create(), 'Y-m-d h:i:s');
                $this->providers_utilities_model->insert($data);
            }

            $msg = 'Data inserted Successfully';
            echo $msg;             
        }
    }
    
    function get_list() {
        $session_level = $this->session->userdata('level');
        $session_id = $this->session->userdata('id');
        
        $from = $this->input->post('from');
        $to = $this->input->post('to');
        
        $from_date = '';
        $to_date = '';
        if(!empty($from) && !empty($to))
        {
            $from_date = $from.' 00:00:00';
            $to_date   = $to.' 23:59:59';
        }
       
        $this->datatables->select("1,s.service_name,count(v.id) as total_transaction,sum(transaction_amt) as total_gmv ,sum(sa_amt) as total_commission,s.id");
        $this->datatables->from("vas_commission_detail v");
        $this->datatables->join('service_master s', 'v.service_id = s.id', 'inner');
        $this->datatables->where('v.status', 'Y');
        if($from_date != "" && $to_date != ""){
            $this->datatables->where('v.created_date >=',$from_date);
            $this->datatables->where('v.created_date <=',$to_date);
        }
         
        if (!empty($session_level) && isset($session_level)) {
            if ($this->session->userdata('role_id') == MASTER_DISTRIBUTOR_ROLE_ID) {//RD
                    $this->datatables->where("v.user_id IN(SELECT id FROM users where level_3='" . $session_id . "')");
            }elseif ($this->session->userdata('role_id') == TRIMAX_ROLE_ID) {//rokad trimax value
                 $this->datatables->where("v.user_id IN(SELECT  id from users where level_1='" . $session_id . "')");
            }elseif ($this->session->userdata('role_id') == COMPANY_ROLE_ID) {//company
                  $this->datatables->where("v.user_id IN(SELECT  id FROM users WHERE level_2='" . $session_id . "')");
            }
        }
        
        $this->datatables->group_by("v.service_id");
        $this->db->order_by('s.id',"asc");
    
        $data = $this->datatables->generate('json');
        //show($this->db->last_query(), 1);
        echo $data;
}
    
}
