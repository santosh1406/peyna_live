<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Smart_card_registration extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		is_logged_in();
		$this->load->model(array('machine_unregister_model','common_model','users_model','wallet_topup_model','machine_allocation_model'));
        $this->load->library('form_validation');
        $this->load->helper('smartcard_helper');
        $this->sessionData['id'] = $this->session->userdata('user_id');
        $this->sessionData["username"] = $this->session->userdata('username');
        $this->sessionData["password"] = $this->session->userdata('password');
		
	}

    public function sc_concession_pass_registration() {
        $data['occupationList'] = $this->get_occupation_list();
        load_back_view(SC_CONCESSION_PASS_REGISTRATION_VIEW, $data);
    }

    public function get_occupation_list(){

        $apiCred = json_decode(SMART_CARD_CREDS,true);

        $header_value = $apiCred['X_API_KEY'];
        $username= $apiCred['username'];
        $password= $apiCred['password'];
        $URL= $apiCred['url'].'/Common_api/getOccupationmaster';
        $header = array(
            'X-API-KEY:'.$header_value
        );
        $occupationList = [];
        $request = array(
            'apploginuser'=>1,
            'pass_category_id'=>1,
            'session_id'=>5317,
            'version_id'=> '1.2.2'
            );

        

        $response = curlForPost($URL,$request,$sessionData, $header);
        $occupationResponseArr =json_decode($response,true);

        if ($occupationResponseArr['status'] == 'success') {
            foreach ($occupationResponseArr['data'] as $key => $occupationResponse) {
                $occupationList[] = $occupationResponse['ocuupation_name'];
            }
        }

        return $occupationList;
        

    }



}
	