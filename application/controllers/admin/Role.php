<?php

class Role extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model(array('role_model'));

        // uncomment later
        /*
         * $isadmin = strtolower($this->session->userdata('role_name'));
          if ($isadmin != "super admin" or $isadmin != "admin") {
          $this->session->set_flashdata('invalid', 'your are not authorized to modify URL.');
          redirect(base_url() . "admin/role/");
          } */
    }

    /*
     * @function    : role_view
     * @param       : 
     * @detail      : all role list.
     *                 
     */

    public function index() {

        //$role_data = $this->role_model->get_all_roles();
        // $data["role_data"] = $role_data;
        load_back_view(ROLE_VIEW);
    }

// end of role_view

    /*
     * @function    : add_role_view
     * @param       : 
     * @detail      : form for add role.
     *                 
     */

    public function add_role_view() {

        load_back_view(ADD_ROLE_VIEW);
    }

// end of add_role_view


    /*
     * @function    : save
     * @param       : 
     * @detail      : insert.
     *                 
     */

    public function save() {
        $append_msg = "";
        $input = $this->input->post();
        $istrue = true;
        $config = array(
            array('field' => 'role_name', 'label' => 'Role Name', 'rules' => 'trim|required', 'errors' => array('required' => 'Please enter role name.')),
            array('field' => 'home_page', 'label' => 'Home Page', 'rules' => 'trim|required', 'errors' => array('required' => 'Please enter home page.'))
        );

        if (form_validate_rules($config)) {
            //check if user is authorized to perform edit action
            if (isset($input["id"]) and $input["id"] > 0) {

                $where = "role_name = '" . $input['role_name'] . "' AND id != '" . $input['id']."'";
                $role_exists = $this->role_model->where($where, "", false)->select();
                if (count($role_exists) == 0) {
                    $this->role_model->id = $input["id"];
                    $this->role_model->updated_on = date("Y-m-d H:i:s");
                    $append_msg = "updated";
                } else {
                    $istrue = false;
                }
            } else {
                $where = "role_name = '" . $input['role_name'] . "'";
                $role_exists = $this->role_model->where($where, "", false)->select();
                if (count($role_exists) == 0) {
                    $this->role_model->added_on = date("Y-m-d H:i:s");
                    $append_msg = "added";
                } else {
                    $istrue = false;
                }
            }

            if ($istrue) {
                $this->role_model->role_name = trim($input["role_name"]);
                $this->role_model->home_page = trim($input["home_page"]);
                $this->role_model->added_by = trim($this->session->userdata("user_id"));


                $id = $this->role_model->save();

                if ($id > 0) {
                    $data["flag"] = '@#success#@';
                    $data["msg_type"] = 'success';
                    $data["msg"] = 'Role ' . $append_msg . ' successfully.';
                } else {
                    $data["flag"] = '@#failed#@';
                    $data["msg_type"] = 'error';
                    $data["msg"] = 'Role could not be ' . $append_msg . '.';
                }
            } else {
                $data["flag"] = '@#failed#@';
                $data["msg_type"] = 'error';
                $data["msg"] = "Role with name '" . $input['role_name'] . "' already exists.";
            }
        } else {
            $data["flag"] = '@#failed#@';
            $data["msg_type"] = 'error';
            $data["msg"] = 'Role could not be added/updated without role name and home page.';
        }

        data2json($data);
    }

// end of save


    /*
     * @function    : edit_role
     * @param       : $id
     * @detail      : view edit role.
     *                 
     */
    public function edit_role() {
        $input = $this->input->post();

        $this->role_model->id = $input["id"];
        $rdata = $this->role_model->select();

        $data["flag"] = '@#success#@';
        $data["role_data"] = $rdata;

        data2json($data);
    }

    // end of edit_role


    /*
     * @function    : delete_role
     * @param       : 
     * @detail      : delete role.
     *                 
     */
    public function delete_role() {
        $input = $this->input->post();
        $this->role_model->id = $input["id"];
        $this->role_model->record_status = $input["postyn"];
        $id = $this->role_model->save();

        if ($id > 0) {
            $data["flag"] = '@#success#@';
            $this->session->set_flashdata('msg_type', 'success');
            $this->session->set_flashdata('msg', 'Role deleted successfully.');
        } else {
            $data["flag"] = '@#failed#@';
            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'Role could not be deleted.');
        }
        data2json($data);
    }

// end of delete_role


    function get_list() {
        $this->datatables->select('id,role_name,home_page,record_status');
        $this->datatables->from('user_roles');

        $this->datatables->add_column('action', '<a href="javascript:void" class="edit_btn margin" ref="$1"> <i  class="glyphicon glyphicon-pencil"></i> </a> <a href="javascript:void" class="del_btn" ref="$1" data-ref="$2">  <i  class="glyphicon glyphicon-trash"></i> </a>', 'id,record_status');
        $data = $this->datatables->generate('json');

        echo $data;
    }

}

//end of users class