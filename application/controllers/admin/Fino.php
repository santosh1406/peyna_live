<?php

//Created by pooja kambali on 29-09-2018 
class Fino extends CI_Controller {

    public $sessionData;
    
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Fino_model');
        $this->sessionData['id'] = $this->session->userdata('user_id');
        $this->sessionData["username"] = $this->session->userdata('username');
        $this->sessionData["password"] = $this->session->userdata('password');
        
         $this->header = array(
            'Content-Type: multipart/form-data; charset=utf-8',
            'X-API-KEY: ' . X_API_KEY . '',
        );
         

    }

    public function index() {
        load_back_view(FINO_VIEW);
   }

    public function neft() {
        $data = array();
        $input = $this->input->post();
      
        if (!empty($input)) {
             $curl_url ="http://10.20.107.140/rokad/rest_server/Fino/fino";
             $currdate = date('Y-m-d H:m:s'); 
             $data['user_id'] = $this->session->userdata('user_id');
             $data['created_on']=$currdate;
             $input['rfu2']='test';
             $input['rfu3']='test';
             $config = array(
               array('field' => 'type', 'label' => 'Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter  type.')),
               array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter mobile number.')),
               array('field' => 'ifsc_code', 'label' => 'IFSC Code', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter ifsc code.')),
               array('field' => 'bene_acc_no', 'label' => 'Beneficiary Account Number.', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter Beneficiary Account number.')),
               array('field' => 'bene_name', 'label' => 'Beneficiary Name.', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Beneficiary Name.')),
               array('field' => 'amount', 'label' => 'Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter Amount.')),
               array('field' => 'customer_name', 'label' => 'Customer Name.', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter Customer Name.')),
               array('field' => 'rfu1', 'label' => 'Remarks', 'rules' => 'trim|xss_clean', 'errors' => array('required' => 'Please enter Remark'))
                
                 );
           
            
              if (form_validate_rules($config)==False) {
               $error = $this->form_validation->error_array();
               $this->response(array('status' => 'failed', 'error' => $error), 200);
               }else{
                  
                $data['type'] = $input['type'];
                $data['ClientUniqueID'] = '1234';
                $data['CustomerMobileNo'] = $input['mobile_number'];
                $data['BeneIFSCCode'] = $input['ifsc_code'];
                $data['BeneAccountNo'] =  $input['bene_acc_no'];
                $data['BeneName'] = $input['bene_name'];
                $data['Amount'] =   $input['amount'];
                $data['CustomerName'] = $input['customer_name'];
                $data['RFU1'] =$input['rfu1'];
                $data['RFU2'] = $input['rfu2']; 
                $data['RFU3'] = $input['rfu3'];
                
                $transfer= $this->Fino_model->neft($data);
                 log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'fino.log', $data, 'Input-Operator');
                $curlResult = curlForPostData_fino($curl_url, $data);
                $respArr = json_decode($curlResult,1);
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'fino_respone.log', $respArr, 'Input-Operator');
                $up= $this->Fino_model->neftupdate($respArr);
             
             echo  $curlResult;
              exit;
            }
        }
       // redirect(base_url() . 'admin/Fino');
   
    }
    
    
    public function transaction_list(){
      load_back_view(TRANSACTION_LIST);             
        }
        
        public function transaction(){
        $data = $this->input->post();
        $this->datatables->select("'1',Clientuniqueid,type,CustomerMobileNo,BeneIFSCCode,BeneAccountNo,BeneName,Amount,CustomerName,created_on,status,message");
        $this->datatables->from('dmt_transfer_detail');
        
         if (isset($data['from_date']) && $data['from_date'] != '') {
            $this->datatables->where("DATE_FORMAT(created_on, '%Y-%m-%d') >=", date('Y-m-d', strtotime($data['from_date'])));
        }

        if (isset($data['to_date']) && $data['to_date'] != '') {
            $this->datatables->where("DATE_FORMAT(created_on, '%Y-%m-%d')<=", date('Y-m-d', strtotime($data['to_date'])));
        }
         $data = $this->datatables->generate('json');
        echo $data;
       
        }
    
   

}
