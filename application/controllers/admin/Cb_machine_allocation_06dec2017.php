<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cb_machine_allocation extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		is_logged_in();
		$this->load->model(array('machine_unregister_model','common_model','users_model','wallet_topup_model','machine_allocation_model'));
        $this->load->library('form_validation');
		//echo "hi";exit;
		
	}
    
    public function index()
	{	   
       $data = array();
       $data['user_data']  = $this->session->userdata();
	   $data['user_type']  = $data['user_data']['role_name'];
	   $RollID = $this->session->userdata("role_id");
	   $UserID = $this->session->userdata("user_id");
	   if($RollID == DISTRIBUTOR){
		$UserData =  $this->machine_allocation_model->getRetailers($UserID);  
		$data['UserList'] = $UserData;
	   }

	   load_back_view(MACHINE_REGISTRATION_VIEW, $data);             
    }
	
    /** Get List Of User For Machine Register */
    public function DataTableMachineRegistration() {
		
        $aColumns = array('id','first_name','email','mobile_no', 'id');

        $DataTableArray = getDefinedDataTable($aColumns);
        $sWhere = $DataTableArray['sWhere'];
        $sOrder = $DataTableArray['sOrder'];
        $sLimit = $DataTableArray['sLimit'];
		
		$RollID = $this->session->userdata("role_id");
		$UserID = $this->session->userdata("user_id");
        
		if($RollID == DISTRIBUTOR){
			if($UserID !="")
			{
				if($sWhere<>''){
					$sWhere .= " WHERE level_5  = '".$UserID."' AND kyc_verify_status='Y' AND status ='Y'";
				}else{
					$sWhere .= " WHERE level_5  = '".$UserID."' AND kyc_verify_status='Y' AND status ='Y'";
				}
			}
		} else if($RollID == RETAILER_ROLE_ID ){
		if($UserID !="")
        {            
           if($sWhere<>''){
            $sWhere .= " WHERE id  = '".$UserID."' AND kyc_verify_status='Y' AND status ='Y'"; 
           }else{
               $sWhere .= " WHERE id  = '".$UserID."' AND kyc_verify_status='Y' AND status ='Y'"; 
           }                                    
        }	
		} 
		
		$SearchBy = (isset($_GET['SearchBy']) ? $_GET['SearchBy'] :'');
        if($SearchBy !="")
        {            
           if($sWhere<>''){
            $sWhere .= " AND id = '".$SearchBy."'"; 
           }else{
               $sWhere .= " WHERE id = '".$SearchBy."'"; 
           }                                    
        }
				        
        $DataTableArray = $this->machine_allocation_model->LoadMachineRegDataTable($sWhere, $sOrder, $sLimit);
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $DataTableArray['iTotal'],
            "iTotalDisplayRecords" => $DataTableArray['iTotal'],
            "aaData" => array()
        );
        $aHeader = array('id','Name','email','mobile_no','Actions');
        $site_url = base_url();
        
        foreach ($DataTableArray['ResultSet'] as $aRow) {
            $row = array();
            $TotalHeader = count($aHeader);
            $k=0;
            for ($i = 0; $i < $TotalHeader; $i++) {
				$k++;
				if ($aHeader[$i] == "id") {
				   $row[] = $k;
			    } else if ($aHeader[$i] == "email") {
					$Email = rokad_decrypt($aRow['email'],$this->config->item('pos_encryption_key'));
					$row[] = htmlspecialchars($Email, ENT_QUOTES);
                } else if ($aHeader[$i] == "mobile_no") {
					$Mobile = rokad_decrypt($aRow['mobile_no'],$this->config->item('pos_encryption_key'));
					$row[] = htmlspecialchars($Mobile, ENT_QUOTES);
                } else if ($aHeader[$i] == "Actions") {
                   /* $Action = '<a style="padding: 0 4px;" class="pull-left view_btn btn-primary btn-sm btn-small" href="'.$site_url.'admin/Cb_machine_allocation/machine_register/'.base64_encode($aRow['id']).'" ref="'.$aRow['id'].'" rel="tooltip" data-placement="top" data-original-title="View" title="Register Machine">Register</a>&nbsp;&nbsp;&nbsp;';*/
                
                    $Action = '<button type="button" style="padding: 0 4px;color:#fff;" class="pull-left view_btn btn-primary btn-sm btn-small"  ref="" rel="tooltip" data-placement="top" data-original-title="Collection" title="register" onclick="RegisterMachine('.$aRow['id'].')">Register Machine</button>';
                    $row[] = $Action;
                } else if ($aHeader[$i] != ' ') {
                    /* General output */
                    $row[] = $aRow[$aHeader[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode($output);
    }
	
	public function allocate_machine()
    {
		
        $input = $this->input->post(NULL,TRUE);
        
        $data_res= $this->machine_allocation_model->get_allocate_waybilll($input['user_id']);

		$AgentId = $data_res[0]->id;
		$AgentCode = $data_res[0]->agent_code;
		$OpId = 'MSRTC';
		//$BusStop = $data_res[0]->star_seller_code;
                $BusStop = 'VSHITT';
        $ApiKey = 'fdac7124c6a2b3f6';
        $post_data = 'agent_code=' . $AgentCode . '###op_code=' . $OpId . '###api_key= ' . $ApiKey . '###bus_stop=' . trim($BusStop) . "###";

        $encryptedData = encrypt($post_data, 'fdac7124c6a2b3f6');
       
        $data['flag'] = '@#success#@';
        $data['msg'] = $data_res;
        $data['encrypt_data'] = $encryptedData;
        data2json($data);
    }
	
	public function waybill_allocation()
	{
       $data = array();
       $data['user_data']  = $this->session->userdata();
	   $data['user_type']  = $data['user_data']['role_name'];
	   $RollID = $this->session->userdata("role_id");
	   $UserID = $this->session->userdata("user_id");
	   if($RollID == DISTRIBUTOR){
		$UserData =  $this->machine_allocation_model->getRetailers($UserID);  
		$data['UserList'] = $UserData;
	   }
       load_back_view(MACHINE_ALLOCATION_VIEW, $data);
    }
	
	/** Get List Of User For Waybill Allocation */
    public function DataTableWaybillAllocation() {
		
        $aColumns = array('u.id','u.first_name','u.agent_code','u.star_seller_name','u.depot_name','u.securitydeposite','w.amt','id');

        $DataTableArray = getDefinedDataTable($aColumns);
        $sWhere = $DataTableArray['sWhere'];
        $sOrder = $DataTableArray['sOrder'];
        $sLimit = $DataTableArray['sLimit'];
		
		$RollID = $this->session->userdata("role_id");
		$UserID = $this->session->userdata("user_id");
        
		if($RollID == DISTRIBUTOR){
			if($UserID !="")
			{
				if($sWhere<>''){
					$sWhere .= " WHERE u.level_5  = '".$UserID."' AND u.kyc_verify_status='Y' AND u.status ='Y'";
				}else{
					$sWhere .= " WHERE u.level_5  = '".$UserID."' AND u.kyc_verify_status='Y' AND u.status ='Y'";
				}
			}
		} else if($RollID == RETAILER_ROLE_ID ){
		if($UserID !="")
        {            
           if($sWhere<>''){
            $sWhere .= " WHERE u.id  = '".$UserID."' AND u.kyc_verify_status='Y' AND u.status ='Y'"; 
           }else{
               $sWhere .= " WHERE u.id  = '".$UserID."' AND u.kyc_verify_status='Y' AND u.status ='Y'"; 
           }                                    
        }	
		} 
		
		$SearchBy = (isset($_GET['SearchBy']) ? $_GET['SearchBy'] :'');
        if($SearchBy !="")
        {            
           if($sWhere<>''){
            $sWhere .= " AND u.id = '".$SearchBy."'"; 
           }else{
               $sWhere .= " WHERE u.id = '".$SearchBy."'"; 
           }                                    
        }
				        
        $DataTableArray = $this->machine_allocation_model->LoadWaybillAllocationDataTable($sWhere, $sOrder, $sLimit);
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $DataTableArray['iTotal'],
            "iTotalDisplayRecords" => $DataTableArray['iTotal'],
            "aaData" => array()
        );
        $aHeader = array('id','Name','AgentCode','star_seller_name','depot_name','amt','securitydeposite','Actions');
        $site_url = base_url();
        
        foreach ($DataTableArray['ResultSet'] as $aRow) {
            $row = array();
            $TotalHeader = count($aHeader);
            $j=0;

            $class = $attr = '';
            if(empty($aRow['star_seller_name'])) {
                $class = 'btn disabled';
                $attr = 'disabled';
            }

            for ($i = 0; $i < $TotalHeader; $i++) {
				$j++;
				if ($aHeader[$i] == "id") {
				   $row[] = $j;
			    } else if ($aHeader[$i] == "amt") {
					$Amnt = (string)$aRow['amt'];
					$Amt =strpos($Amnt,'.');
					$Amount =substr($Amnt,0,$Amt+3);
					$row[] = $Amount;
                } else if ($aHeader[$i] == "securitydeposite") {
					$amount="<input type='text' name='amount' id='amount".$aRow['id']."'>";
					$row[] = $amount;
			    } else if ($aHeader[$i] == "Actions") {
                    $Action = '<button '.$attr.' type="button" style="padding: 0 4px;color:#fff;" class="pull-left view_btn btn-primary btn-sm btn-small '.$class.' " ref="" rel="tooltip" data-placement="top" data-original-title="Allocation" title="Allocation" onclick="WaybilAllocation('.$aRow['id'].','.$aRow['amt'].')">Allocate Waybill</button>';
                    $row[] = $Action;
                } else if ($aHeader[$i] != ' ') {
                    /* General output */
                    $row[] = $aRow[$aHeader[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode($output);
    }
	
	public function assign_waybill()
    {
		
        $input = $this->input->post(NULL,TRUE);
        
        $data_res= $this->machine_allocation_model->get_allocate_waybilll($input['user_id']);

		$Amount = $input['amount'];
		$AgentId = $data_res[0]->id;
		$AgentCode = $data_res[0]->agent_code;
		$OpId = 'MSRTC';
		$BusStop = $data_res[0]->star_seller_code;
		//$BusStop = 'KLPCBS';
        $ApiKey = EncryptKey;
        $Flag = 2;
        $post_data = 'agent_code=' . $AgentCode . '###op_code=' . $OpId . '###balance=' . $Amount . '###api_key= ' . $ApiKey . '###bus_stop='.trim($BusStop).'###flag='.$Flag.'';

        $encryptedData = encrypt($post_data, $ApiKey);
       
        $data['flag'] = '@#success#@';
        $data['msg'] = $data_res;
        $data['encrypt_data'] = $encryptedData;
        data2json($data);
    }
	
	public function waybill_collection()
	{
       $data = array();
       $data['user_data']  = $this->session->userdata();
	   $data['user_type']  = $data['user_data']['role_name'];
	   $RollID = $this->session->userdata("role_id");
	   $UserID = $this->session->userdata("user_id");
	   if($RollID == DISTRIBUTOR){
		$UserData =  $this->machine_allocation_model->getRetailers($UserID);  
		$data['UserList'] = $UserData;
	   }
       load_back_view(MACHINE_COLLECTION_VIEW, $data);
    }

	/** Get List Of User For Waybill Collection */
    public function DataTableMachineCollection() {
		
        $aColumns = array('u.id','u.first_name','u.agent_code','u.star_seller_name','u.depot_name','w.amt','id');

        $DataTableArray = getDefinedDataTable($aColumns);
        $sWhere = $DataTableArray['sWhere'];
        $sOrder = $DataTableArray['sOrder'];
        $sLimit = $DataTableArray['sLimit'];
		
		$RollID = $this->session->userdata("role_id");
		$UserID = $this->session->userdata("user_id");
        
		if($RollID == DISTRIBUTOR){
			if($UserID !="")
			{
				if($sWhere<>''){
					$sWhere .= " WHERE u.level_5  = '".$UserID."' AND u.kyc_verify_status='Y' AND u.status ='Y'";
				}else{
					$sWhere .= " WHERE u.level_5  = '".$UserID."' AND u.kyc_verify_status='Y' AND u.status ='Y'";
				}
			}
		} else if($RollID == RETAILER_ROLE_ID ){
		if($UserID !="")
        {            
           if($sWhere<>''){
            $sWhere .= " WHERE u.id  = '".$UserID."' AND u.kyc_verify_status='Y' AND u.status ='Y'"; 
           }else{
               $sWhere .= " WHERE u.id  = '".$UserID."' AND u.kyc_verify_status='Y' AND u.status ='Y'"; 
           }                                    
        }	
		} 
		
		$SearchBy = (isset($_GET['SearchBy']) ? $_GET['SearchBy'] :'');
        if($SearchBy !="")
        {            
           if($sWhere<>''){
            $sWhere .= " AND u.id = '".$SearchBy."'"; 
           }else{
               $sWhere .= " WHERE u.id = '".$SearchBy."'"; 
           }                                    
        }
				        
        $DataTableArray = $this->machine_allocation_model->LoadMachineCollectionDataTable($sWhere, $sOrder, $sLimit);
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $DataTableArray['iTotal'],
            "iTotalDisplayRecords" => $DataTableArray['iTotal'],
            "aaData" => array()
        );
        $aHeader = array('id','Name','AgentCode','star_seller_name','depot_name','amt','Actions');
        $site_url = base_url();
        
        foreach ($DataTableArray['ResultSet'] as $aRow) {
            $row = array();
            $TotalHeader = count($aHeader);
            $j=0;
            for ($i = 0; $i < $TotalHeader; $i++) {
				$j++;
				if ($aHeader[$i] == "id") {
				   $row[] = $j;
			    } else if ($aHeader[$i] == "Actions") {
                    $Action = '<button type="button" style="padding: 0 4px;color:#fff;" class="pull-left view_btn btn-primary btn-sm btn-small"  ref="" rel="tooltip" data-placement="top" data-original-title="Collection" title="Collection" onclick="WaybilCollection('.$aRow['id'].')">Collection</button>';
                    $row[] = $Action;
                } else if ($aHeader[$i] != ' ') {
                    /* General output */
                    $row[] = $aRow[$aHeader[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode($output);
    }
	
	public function collection_machine()
    {
		
        $input = $this->input->post(NULL,TRUE);
        
        $data_res= $this->machine_allocation_model->get_allocate_waybilll($input['user_id']);

		$AgentId = $data_res[0]->id;
		$AgentCode = $data_res[0]->agent_code;
		$OpId = 'MSRTC';
		//$BusStop = $data_res[0]->star_seller_code;
        $Flag = 4;
	    $BusStop = 'VSHITT';
        $ApiKey = 'fdac7124c6a2b3f6';
        $post_data = 'agent_code=' . $AgentCode . '###op_code=' . $OpId . '###api_key= ' . $ApiKey . '###bus_stop=' . trim($BusStop) .'###flag='.$Flag. "###";

        $encryptedData = encrypt($post_data, 'fdac7124c6a2b3f6');
        
        $data['flag'] = '@#success#@';
        $data['msg'] = $data_res;
        $data['encrypt_data'] = $encryptedData;

        data2json($data);
    }
    public function machine_register($id)
    {
        $id= base64_decode($id); 
        $data['id'] = $id;
        $userdata = $this->users_model->column('id,first_name,last_name')->where('id',$id)->find_all();
        $machine_data = $this->machine_allocation_model->check_machine_registration($id);
        $data['machine_data'] = $machine_data[0];
        $data['userdata'] = $userdata;
        load_back_view(MACHINE_REG_ALLOCATION_VIEW, $data);       
    }
   

    public function waybill_collection_allocation()
    {
        if($this->session->role_id == DISTRIBUTOR || $this->session->role_id == RETAILER_ROLE_ID )
        {
          load_back_view(MACHINE_COLLECTION_ALLOCATION_VIEW);
        }else
        {
            redirect(base_url().'admin/dashboard');
        }
    }
    
    function collection_allocation_details()
    {
        $user_id = $this->session->userdata('user_id');

        $this->datatables->select('1,
                        t.`boss_ref_no` AS "Boss Reference No",
                        t.pnr_no As "PNR NO",t.api_ticket_no,
                        t.`booked_by` AS "user type",
                        td.psgr_name as first_name,
                        t.user_email_id as email,
                        u.id as agent_id,
                        t.mobile_no,
                        sm.stState,
                        cm.stCity,
                        t.`op_name` AS "Operator Name",
                        ap.name as "Povider Name",
                        t.from_stop_name as "From Stop",                      
                        t.till_stop_name as "To Stop",
                        DATE_FORMAT(t.inserted_date,"%d-%m-%Y %h:%i:%s") as "issued date",
                        DATE_FORMAT(t.dept_time,"%d-%m-%Y %h:%i:%s") as doj,
                        t.num_passgr as "No of passenger"'
        );

        $this->datatables->from('users u');
        $this->datatables->join('ticket_details td', 'td.ticket_id = t.ticket_id', 'inner');
        $this->datatables->join('api_provider ap', 't.provider_id = ap.id', 'left');

        $data = $this->datatables->generate('json');

        echo $data;
    }
     public function register_machine()
    {
        $input = $this->input->post(NULL,TRUE);
        
        $data_res= $this->machine_allocation_model->get_allocate_waybilll($input['user_id']);

        $AgentId = $data_res[0]->id;
        $AgentCode = $data_res[0]->agent_code;
        $agentfirst= $data_res[0]->first_name;
        $agentsecond= $data_res[0]->last_name;

        $OpId = 'MSRTC';
        /*$machine_data = $this->machine_allocation_model->check_machine_registration($id);*/

        $ApiKey = EncryptKey;
        $Flag = 3;
        $post_data = 'agent_code=' . $AgentCode .'###agent_first='. $agentfirst.'###agent_Second='.$agentsecond. '###api_key= ' . $ApiKey . '###flag='.$Flag.'';
        $encryptedData = encrypt($post_data, $ApiKey);
        $data['flag'] = '@#success#@';
        $data['msg'] = $data_res;
        $data['encrypt_data'] = $encryptedData;
        data2json($data);
    }
     public function machine_unregister()
	{	   
       $data = array();
       $data['user_data']  = $this->session->userdata();
	   $data['user_type']  = $data['user_data']['role_name'];
	   $RollID = $this->session->userdata("role_id");
	   $UserID = $this->session->userdata("user_id");
	   if($RollID == DISTRIBUTOR){
		$UserData =  $this->machine_allocation_model->getRetailers($UserID);  
		$data['UserList'] = $UserData;
	   }

	   load_back_view(MACHINE_UNREGISTRATION_VIEW, $data);             
    }
    
     public function machineUnegistration() {
		
        $aColumns = array('id','first_name','email','mobile_no', 'id');

        $DataTableArray = getDefinedDataTable($aColumns);
        $sWhere = $DataTableArray['sWhere'];
        $sOrder = $DataTableArray['sOrder'];
        $sLimit = $DataTableArray['sLimit'];
		
		$RollID = $this->session->userdata("role_id");
		$UserID = $this->session->userdata("user_id");
        
		if($RollID == DISTRIBUTOR){
			if($UserID !="")
			{
				if($sWhere<>''){
					$sWhere .= " WHERE level_5  = '".$UserID."' AND kyc_verify_status='Y' AND status ='Y'";
				}else{
					$sWhere .= " WHERE level_5  = '".$UserID."' AND kyc_verify_status='Y' AND status ='Y'";
				}
			}
		} else if($RollID == RETAILER_ROLE_ID ){
		if($UserID !="")
        {            
           if($sWhere<>''){
            $sWhere .= " WHERE id  = '".$UserID."' AND kyc_verify_status='Y' AND status ='Y'"; 
           }else{
               $sWhere .= " WHERE id  = '".$UserID."' AND kyc_verify_status='Y' AND status ='Y'"; 
           }                                    
        }	
		} 
		
		$SearchBy = (isset($_GET['SearchBy']) ? $_GET['SearchBy'] :'');
        if($SearchBy !="")
        {            
           if($sWhere<>''){
            $sWhere .= " AND id = '".$SearchBy."'"; 
           }else{
               $sWhere .= " WHERE id = '".$SearchBy."'"; 
           }                                    
        }
				        
        $DataTableArray = $this->machine_allocation_model->LoadMachineRegDataTable($sWhere, $sOrder, $sLimit);
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $DataTableArray['iTotal'],
            "iTotalDisplayRecords" => $DataTableArray['iTotal'],
            "aaData" => array()
        );
        $aHeader = array('id','Name','email','mobile_no','Actions');
        $site_url = base_url();
        
        foreach ($DataTableArray['ResultSet'] as $aRow) {
            $row = array();
            $TotalHeader = count($aHeader);
            $k=0;
            for ($i = 0; $i < $TotalHeader; $i++) {
				$k++;
				if ($aHeader[$i] == "id") {
				   $row[] = $k;
			    } else if ($aHeader[$i] == "email") {
					$Email = rokad_decrypt($aRow['email'],$this->config->item('pos_encryption_key'));
					$row[] = htmlspecialchars($Email, ENT_QUOTES);
                } else if ($aHeader[$i] == "mobile_no") {
					$Mobile = rokad_decrypt($aRow['mobile_no'],$this->config->item('pos_encryption_key'));
					$row[] = htmlspecialchars($Mobile, ENT_QUOTES);
                } else if ($aHeader[$i] == "Actions") {
                   /* $Action = '<a style="padding: 0 4px;" class="pull-left view_btn btn-primary btn-sm btn-small" href="'.$site_url.'admin/Cb_machine_allocation/machine_register/'.base64_encode($aRow['id']).'" ref="'.$aRow['id'].'" rel="tooltip" data-placement="top" data-original-title="View" title="Register Machine">Register</a>&nbsp;&nbsp;&nbsp;';*/
                
                    $Action = '<button type="button" style="padding: 0 4px;color:#fff;" class="pull-left view_btn btn-primary btn-sm btn-small"  ref="" rel="tooltip" data-placement="top" data-original-title="Collection" title="unregister" onclick="UnregisterMachine('.$aRow['id'].')">Unregister Machine</button>';
                    $row[] = $Action;
                } else if ($aHeader[$i] != ' ') {
                    /* General output */
                    $row[] = $aRow[$aHeader[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode($output);
    }
     public function unregister_machine()
    {
        $input = $this->input->post(NULL,TRUE);
        
        $data_res= $this->machine_allocation_model->get_allocate_waybilll($input['user_id']);

        $AgentId = $data_res[0]->id;
        $AgentCode = $data_res[0]->agent_code;
        $agentfirst= $data_res[0]->first_name;
        $agentsecond= $data_res[0]->last_name;

        $OpId = 'MSRTC';
        /*$machine_data = $this->machine_allocation_model->check_machine_registration($id);*/

        $ApiKey = EncryptKey;
      //   $ApiKey = 'fdac7124c6a2b3f6';
        $Flag = 5;
        $post_data = 'agent_code=' . $AgentCode .'###agent_first='. $agentfirst.'###agent_Second='.$agentsecond. '###api_key= ' . $ApiKey . '###flag='.$Flag.'';
        $encryptedData = encrypt($post_data, $ApiKey);
        $data['flag'] = '@#success#@';
        $data['msg'] = $data_res;
        $data['encrypt_data'] = $encryptedData;
        data2json($data);
    }
	
    //assign machine code start here
    public function machine_assign()
    {      
       $data = array();
       $data['user_data']  = $this->session->userdata();
       $data['user_type']  = $data['user_data']['role_name'];
       $RollID = $this->session->userdata("role_id");
       $UserID = $this->session->userdata("user_id");
       if($RollID == DISTRIBUTOR){
        $UserData =  $this->machine_allocation_model->getRetailers($UserID);  
        $data['UserList'] = $UserData;
       }

       load_back_view(MACHINE_ASSIGN_VIEW, $data);             
    }
    
     public function machineAssign() {
        
        $aColumns = array('id','first_name','email','mobile_no', 'id');

        $DataTableArray = getDefinedDataTable($aColumns);
        $sWhere = $DataTableArray['sWhere'];
        $sOrder = $DataTableArray['sOrder'];
        $sLimit = $DataTableArray['sLimit'];
        
        $RollID = $this->session->userdata("role_id");
        $UserID = $this->session->userdata("user_id");
        
        if($RollID == DISTRIBUTOR){
            if($UserID !="")
            {
                if($sWhere<>''){
                    $sWhere .= " WHERE level_5  = '".$UserID."' AND kyc_verify_status='Y' AND status ='Y'";
                }else{
                    $sWhere .= " WHERE level_5  = '".$UserID."' AND kyc_verify_status='Y' AND status ='Y'";
                }
            }
        } else if($RollID == RETAILER_ROLE_ID ){
        if($UserID !="")
        {            
           if($sWhere<>''){
            $sWhere .= " WHERE id  = '".$UserID."' AND kyc_verify_status='Y' AND status ='Y'"; 
           }else{
               $sWhere .= " WHERE id  = '".$UserID."' AND kyc_verify_status='Y' AND status ='Y'"; 
           }                                    
        }   
        } 
        
        $SearchBy = (isset($_GET['SearchBy']) ? $_GET['SearchBy'] :'');
        if($SearchBy !="")
        {            
           if($sWhere<>''){
            $sWhere .= " AND id = '".$SearchBy."'"; 
           }else{
               $sWhere .= " WHERE id = '".$SearchBy."'"; 
           }                                    
        }
                        
        $DataTableArray = $this->machine_allocation_model->LoadMachineRegDataTable($sWhere, $sOrder, $sLimit);
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $DataTableArray['iTotal'],
            "iTotalDisplayRecords" => $DataTableArray['iTotal'],
            "aaData" => array()
        );
        $aHeader = array('id','Name','email','mobile_no','Actions');
        $site_url = base_url();
        
        foreach ($DataTableArray['ResultSet'] as $aRow) {
            $row = array();
            $TotalHeader = count($aHeader);
            $k=0;
            for ($i = 0; $i < $TotalHeader; $i++) {
                $k++;
                if ($aHeader[$i] == "id") {
                   $row[] = $k;
                } else if ($aHeader[$i] == "email") {
                    $Email = rokad_decrypt($aRow['email'],$this->config->item('pos_encryption_key'));
                    $row[] = htmlspecialchars($Email, ENT_QUOTES);
                } else if ($aHeader[$i] == "mobile_no") {
                    $Mobile = rokad_decrypt($aRow['mobile_no'],$this->config->item('pos_encryption_key'));
                    $row[] = htmlspecialchars($Mobile, ENT_QUOTES);
                } else if ($aHeader[$i] == "Actions") {
                    $Action = '<button type="button" style="padding: 0 4px;color:#fff;" class="pull-left view_btn btn-primary btn-sm btn-small"  ref="" rel="tooltip" data-placement="top" data-original-title="Collection" title="assign" onclick="AssignMachine('.$aRow['id'].')">Assign Machine</button>';
                    $row[] = $Action;
                } else if ($aHeader[$i] != ' ') {
                    /* General output */
                    $row[] = $aRow[$aHeader[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode($output);
    }

    public function assign_machine()
    {
        $input = $this->input->post(NULL,TRUE);
        
        $data_res= $this->machine_allocation_model->get_allocate_waybilll($input['user_id']);

        $AgentId = $data_res[0]->id;
        $AgentCode = $data_res[0]->agent_code;
        $agentfirst= $data_res[0]->first_name;
        $agentsecond= $data_res[0]->last_name;

        $OpId = 'MSRTC';
        /*$machine_data = $this->machine_allocation_model->check_machine_registration($id);*/

        $ApiKey = EncryptKey;
      //   $ApiKey = 'fdac7124c6a2b3f6';
        $Flag = 6;
        $post_data = 'agent_code=' . $AgentCode .'###agent_first='. $agentfirst.'###agent_Second='.$agentsecond. '###api_key= ' . $ApiKey . '###flag='.$Flag.'';
        $encryptedData = encrypt($post_data, $ApiKey);
        $data['flag'] = '@#success#@';
        $data['msg'] = $data_res;
        $data['encrypt_data'] = $encryptedData;
        data2json($data);
    }
    
    //Lost machine collection code start here
    public function machine_lost_collection()
    {      
       $data = array();
       $data['user_data']  = $this->session->userdata();
       $data['user_type']  = $data['user_data']['role_name'];
       $RollID = $this->session->userdata("role_id");
       $UserID = $this->session->userdata("user_id");
       if($RollID == DISTRIBUTOR){
        $UserData =  $this->machine_allocation_model->getRetailers($UserID);  
        $data['UserList'] = $UserData;
       }

       load_back_view(MACHINE_LOST_COLLECTION_VIEW, $data);             
    }
    
     public function machineLostCollection() {
        
        $aColumns = array('id','first_name','email','mobile_no', 'id');

        $DataTableArray = getDefinedDataTable($aColumns);
        $sWhere = $DataTableArray['sWhere'];
        $sOrder = $DataTableArray['sOrder'];
        $sLimit = $DataTableArray['sLimit'];
        
        $RollID = $this->session->userdata("role_id");
        $UserID = $this->session->userdata("user_id");
        
        if($RollID == DISTRIBUTOR){
            if($UserID !="")
            {
                if($sWhere<>''){
                    $sWhere .= " WHERE level_5  = '".$UserID."' AND kyc_verify_status='Y' AND status ='Y'";
                }else{
                    $sWhere .= " WHERE level_5  = '".$UserID."' AND kyc_verify_status='Y' AND status ='Y'";
                }
            }
        } else if($RollID == RETAILER_ROLE_ID ){
        if($UserID !="")
        {            
           if($sWhere<>''){
            $sWhere .= " WHERE id  = '".$UserID."' AND kyc_verify_status='Y' AND status ='Y'"; 
           }else{
               $sWhere .= " WHERE id  = '".$UserID."' AND kyc_verify_status='Y' AND status ='Y'"; 
           }                                    
        }   
        } 
        
        $SearchBy = (isset($_GET['SearchBy']) ? $_GET['SearchBy'] :'');
        if($SearchBy !="")
        {            
           if($sWhere<>''){
            $sWhere .= " AND id = '".$SearchBy."'"; 
           }else{
               $sWhere .= " WHERE id = '".$SearchBy."'"; 
           }                                    
        }

        if($sWhere<>''){
            $sWhere .= "AND agent_code like '%AGT%'";
        }else {
            $sWhere .= "WHERE agent_code like '%AGT%'";
        }
                        
        $DataTableArray = $this->machine_allocation_model->LoadMachineRegDataTable($sWhere, $sOrder, $sLimit);
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $DataTableArray['iTotal'],
            "iTotalDisplayRecords" => $DataTableArray['iTotal'],
            "aaData" => array()
        );
        $aHeader = array('id','Name','email','mobile_no','Actions');
        $site_url = base_url();
        
        foreach ($DataTableArray['ResultSet'] as $aRow) {
            $row = array();
            $TotalHeader = count($aHeader);
            $k=0;
            for ($i = 0; $i < $TotalHeader; $i++) {
                $k++;
                if ($aHeader[$i] == "id") {
                   $row[] = $k;
                } else if ($aHeader[$i] == "email") {
                    $Email = rokad_decrypt($aRow['email'],$this->config->item('pos_encryption_key'));
                    $row[] = htmlspecialchars($Email, ENT_QUOTES);
                } else if ($aHeader[$i] == "mobile_no") {
                    $Mobile = rokad_decrypt($aRow['mobile_no'],$this->config->item('pos_encryption_key'));
                    $row[] = htmlspecialchars($Mobile, ENT_QUOTES);
                } else if ($aHeader[$i] == "Actions") {
                    $Action = '<button type="button" style="padding: 0 4px;color:#fff;" class="pull-left view_btn btn-primary btn-sm btn-small"  ref="" rel="tooltip" data-placement="top" data-original-title="Collection" title="lost" onclick="LostMachineCollection('.$aRow['id'].')">Lost Collection</button>';
                    $row[] = $Action;
                } else if ($aHeader[$i] != ' ') {
                    /* General output */
                    $row[] = $aRow[$aHeader[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode($output);
    }

    public function lost_machine_collection()
    {
        $input = $this->input->post(NULL,TRUE);
        
        $data_res= $this->machine_allocation_model->get_allocate_waybilll($input['user_id']);

        $AgentId = $data_res[0]->id;
        $AgentCode = $data_res[0]->agent_code;
        $agentfirst= $data_res[0]->first_name;
        $agentsecond= $data_res[0]->last_name;

        $OpId = 'MSRTC';
        /*$machine_data = $this->machine_allocation_model->check_machine_registration($id);*/

        $ApiKey = EncryptKey;
      //   $ApiKey = 'fdac7124c6a2b3f6';
        $Flag = 7;
        $post_data = 'agent_code=' . $AgentCode .'###agent_first='. $agentfirst.'###agent_Second='.$agentsecond. '###api_key= ' . $ApiKey . '###flag='.$Flag.'';
        $encryptedData = encrypt($post_data, $ApiKey);
        $data['flag'] = '@#success#@';
        $data['msg'] = $data_res;
        $data['encrypt_data'] = $encryptedData;
        data2json($data);
    }

}