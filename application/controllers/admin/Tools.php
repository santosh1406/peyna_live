<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of login
 * 
 * @author Suraj Rothod<surajra@trimax.in>
 * 
 * Revision History
 * 
 * 
 */
class Tools extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->helper("url");        
		 $this->load->helper("cookie");
    }

	public function encrypt_decrypt() {
		$this->load->view('encrypt_decrypt_view');
	}
	public function test_hex()
	{
		$transaction_no = substr(hexdec(uniqid()), 4, 12);
		show($transaction_no,1);
	}
	public function get_value() {
		$input = $this->input->post();
		$data['msg'] = "";
		$data['msg_type'] = "error";
		
		if($input['inp_key'] == "encrypt")
		{
			$data['msg'] = rokad_encrypt($input['inp_val'], $this->config->item('pos_encryption_key'));
			$data['msg_type'] = "success";
		}
		else if($input['inp_key'] == "decrypt")
		{
			$data['msg'] = rokad_decrypt($input['inp_val'], $this->config->item('pos_encryption_key'));
			$data['msg_type'] = "success";
		}
		data2json($data);
	}
	
	public function decrypt_mobile() {
		$encrypted_mobile_no = 'RLd5PvDBovfRjhxmhuO3fzWIoK1ZhHQEt6F5ZVPojyE5urGjzJzH50QK7p4i6duq+UindmN5NzhR1Bbl1EsfSA==';
		$mobile_no = rokad_decrypt($encrypted_mobile_no, $this->config->item('pos_encryption_key'));
		show($mobile_no, 1);
	}
	
	public function encrypt_mobile() {
		$mobile_no = '9930510306';
		$encrypted_mobile_no = rokad_encrypt($mobile_no, $this->config->item('pos_encryption_key'));
		show($encrypted_mobile_no, 1);
	}	
	
	public function generate_wallet_account() {
		$account_no = substr ( hexdec ( uniqid () ), 4, 12 );
		show($account_no, 1);
	}	
	
	public function get_parent_id() {
		$this->load->model('users_model');
		//$session_cmp_id = $this->users_model->get_parent_id ( 10, MASTER_DISTRIBUTOR_LEVEL);
		$session_cmp_id = $this->users_model->get_trimax_user_id();
		show($session_cmp_id, 1);
	}
	
	public function user_common_data() {
		$this->session->unset_userdata('user_common_data');	
		$user_common_data = unserialize($this->session->userdata('user_common_data'));
		var_dump($user_common_data);
		exit;
		
		$user_common_data = array('user_id' => $user_id,
				'display_name' => $display_name,
				'username' => $mobile_no,
				'email' => $email,
				'adharcard' => $adharcard
		);
		$this->session->set_userdata('user_common_data', serialize($user_common_data));
	}
	
	public function generate_hash() {
		/* $concat_char = '|';
		
		$saCode = '88ad4f';
		$aadhaarId = '123456789012';
		$requestId = '16550766';	
		$salt = 'c8a735a148';	
		
		// init request hash
		// <saCode>|<aadhaarId>|<requestId>|<salt>	
		$init_request_sequence = $saCode . $concat_char . $aadhaarId . $concat_char . $requestId . $concat_char . $salt;
		$init_request_hash = hash('sha256', $init_request_sequence);
		
		show($init_request_hash, null, 'init_request_hash');		
		
		// init response hash
		// <salt>|<requestId>|<aadhaarId>|<saCode>	
		$init_response_sequence = $salt . $concat_char . $requestId . $concat_char . $aadhaarId . $concat_char . $saCode;
		$init_response_hash= hash('sha256', $init_response_sequence);
		
		show($init_response_hash, 1, 'init_response_hash'); */
		
		//$result = get_sample_response();
		//$result = json_decode($result, true);
		
		$this->load->model('aadhar_details_model');		
		$exists = $this->aadhar_details_model->is_user_exists(10);
		if($exists != "" || $exists != NULL)
		{
			$this->aadhar_details_model->id = $exists['id'];
		}
		$this->aadhar_details_model->name = 'test';
		$this->aadhar_details_model->save();			
	}
}