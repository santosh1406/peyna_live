
  SELECT
    AA.reportdate,
    COUNT(AA.ticketcount) AS ticketcount,
    0 AS ticketamt,
    0 AS cwatickamt,
    0 AS winticketcount,
    0 AS winticketamt,
    0 AS wincwatickamt,
    0 AS PSGR 
    FROM
    (
        SELECT 
        DATE_FORMAT(way.cashdate,'%Y-%m-%d') AS reportdate,
        IF(
                tic.group_ticket_flag=1, 
                'GR-00', IF(
                        tic.ticket_code = 'LG' AND tic.pass_id IS NOT NULL AND tic.pass_id != '', 
                        'LGSPL-00',
                            IF(
                                tic.ticket_code = 'LG' AND (tic.pass_id IS NULL OR tic.pass_id = ''), 
                                'LG-00',
                                group_concat(DISTINCT CONCAT_WS('-',tic.ticket_code,tic.insp_conc_code) ORDER BY CONCAT_WS('-',tic.ticket_code,tic.insp_conc_code)) 
                                #GROUP_CONCAT(DISTINCT CONCAT_WS('-',IF(LOCATE('EX',tic.ticket_number) > 0, 'EX', tic.ticket_code), tic.insp_conc_code) ORDER BY CONCAT_WS('-',tic.ticket_code,tic.insp_conc_code))
                            )
                        )
        ) ticket_code1,
        #COUNT(DISTINCT(CONCAT_WS('',tic.waybill_no,tic.ticket_number))) AS ticketcount,
        COUNT(DISTINCT(CONCAT_WS('',tic.waybill_no,tic.ticket_number,tic.etim_no))) AS ticketcount,
        0 AS ticketamt,
        0 AS cwatickamt,
        0 AS winticketcount,
        0 AS winticketamt,
        0 AS wincwatickamt,
        FROM waybillprogramming way
        INNER JOIN ticket tic ON tic.waybill_no=way.waybill_id
        LEFT JOIN concessions as con ON con.CONCESSION_ID = tic.insp_conc_code
        LEFT JOIN concession_rates as conrate ON conrate.CONCESSION_CD = con.CONCESSION_CD 
        AND conrate.BUS_TYPE_CD = (SELECT BUS_TYPE_CD FROM bus_types WHERE BUS_TYPE_ID = tic.bus_service)
        AND concat(DATE_FORMAT(concat_ws('/',SUBSTRING(tic.ticket_date,7,2),SUBSTRING(tic.ticket_date,4,2),SUBSTRING(tic.ticket_date,1,2)),'%Y-%m-%d'), ' ',tic.ticket_time)  BETWEEN conrate.EFFECTIVE_FROM AND conrate.EFFECTIVE_TILL 
        WHERE way.cashdate BETWEEN '2018-05-16 00:00:00' AND '2018-05-16 23:59:59'
        AND way.cashdate NOT BETWEEN '2016-04-01 00:00:00' AND '2016-05-31 23:59:59'
        0 AS PSGR
        AND way.status='Closed'
        AND (CAST(tic.total_amt AS UNSIGNED) > 0)
        AND 
        (    
            tic.ticket_code NOT IN ('CA','RF','PS','TO','OL','NG','DS','DU','CR','RC','UN','UP','LP','TC','AD','DF','PL','SC','CN')
            OR ( (tic.ticket_code = 'CN') AND CAST(IFNULL(conrate.CONCESSION_RATE,0.00) AS UNSIGNED) < 100)
        )
        AND way.`is_new_machine` = 1 
        GROUP BY DATE_FORMAT(way.cashdate,'%Y-%m-%d'),tic.waybill_no,tic.ticket_number,tic.etim_no
        HAVING ticket_code1 != 'LG-00' 
    ) AA
    GROUP BY AA.reportdate

==========================================================================================================

  SELECT
    AA.reportdate,COUNT(AA.ticketcount) AS ticketcount,0 AS ticketamt,
    0 AS cwatickamt,0 AS winticketcount,0 AS winticketamt,0 AS wincwatickamt,0 AS PSGR 
    FROM
    (
        SELECT 
        DATE_FORMAT(way.COLLECTION_TM,'%Y-%m-%d') AS reportdate,
        IF(
                tic.group_ticket_flag=1, 
                'GR-00', IF(
                        tic.ticket_code = 'LG' AND tic.pass_id IS NOT NULL AND tic.pass_id != '', 
                        'LGSPL-00',
                            IF(
                                tic.ticket_code = 'LG' AND (tic.pass_id IS NULL OR tic.pass_id = ''), 
                                'LG-00',
                                group_concat(DISTINCT CONCAT_WS('-',tic.ticket_code,tic.insp_conc_code) ORDER BY CONCAT_WS('-',tic.ticket_code,tic.insp_conc_code)) 
                                #GROUP_CONCAT(DISTINCT CONCAT_WS('-',IF(LOCATE('EX',tic.ticket_number) > 0, 'EX', tic.ticket_code), tic.insp_conc_code) ORDER BY CONCAT_WS('-',tic.ticket_code,tic.insp_conc_code))
                            )
                        )
        ) ticket_code1,
        #COUNT(DISTINCT(CONCAT_WS('',tic.waybill_no,tic.ticket_number))) AS ticketcount,
        COUNT(DISTINCT(CONCAT_WS('',tic.waybill_no,tic.ticket_number,tic.etim_no))) AS ticketcount,
        0 AS ticketamt,
        0 AS cwatickamt,
        0 AS winticketcount,
        0 AS winticketamt,
        0 AS wincwatickamt
        FROM cb_waybillprogramming way
INNER JOIN cb_ticket tic ON tic.waybill_no=way.waybill_id
LEFT JOIN msrtc_replication.concessions as con ON con.CONCESSION_ID = tic.insp_conc_code
LEFT JOIN msrtc_replication.concession_rates as conrate ON conrate.CONCESSION_CD = con.CONCESSION_CD 
AND conrate.BUS_TYPE_CD = (SELECT BUS_TYPE_CD FROM msrtc_replication.bus_types WHERE BUS_TYPE_ID = tic.bus_service)
AND concat(DATE_FORMAT(concat_ws('/',SUBSTRING(tic.ticket_date,7,2),SUBSTRING(tic.ticket_date,4,2),SUBSTRING(tic.ticket_date,1,2)),'%Y-%m-%d'), ' ',tic.ticket_time)  BETWEEN conrate.EFFECTIVE_FROM AND conrate.EFFECTIVE_TILL 
WHERE way.COLLECTION_TM BETWEEN '2018-06-01 00:00:00' AND '2018-06-30 23:59:59'
AND way.COLLECTION_TM NOT BETWEEN '2018-05-01 00:00:00' AND '2018-05-31 23:59:59' /*and
'0' AS PSGR*/ AND way.WAYBILL_STATUS='CLOSED' AND (CAST(tic.total_amt AS UNSIGNED) > 0)
AND (  
tic.ticket_code NOT IN ('CA','RF','PS','TO','OL','NG','DS','DU','CR','RC','UN','UP','LP','TC','AD','DF','PL','SC','CN')
OR ( (tic.ticket_code = 'CN') AND CAST(IFNULL(conrate.CONCESSION_RATE,0.00) AS UNSIGNED) < 100)
)
/*AND way.`is_new_machine` = 1 */
GROUP BY DATE_FORMAT(way.COLLECTION_TM,'%Y-%m-%d'),tic.waybill_no,tic.ticket_number,tic.etim_no
HAVING ticket_code1 != 'LG-00' 
) AA   GROUP BY AA.reportdate


==============================================================


 SELECT 
        DATE_FORMAT(way.COLLECTION_TM,'%Y-%m-%d') AS reportdate,

 COUNT(DISTINCT(CONCAT_WS('',tic.waybill_no,tic.ticket_number,tic.etim_no))) AS ticketcount,
        0 AS ticketamt,
        0 AS cwatickamt,
        0 AS winticketcount,
        0 AS winticketamt,
        0 AS wincwatickamt
        FROM cb_waybillprogramming way
INNER JOIN cb_ticket tic ON tic.waybill_no=way.waybill_id
LEFT JOIN msrtc_replication.concessions as con ON con.CONCESSION_ID = tic.insp_conc_code
LEFT JOIN msrtc_replication.concession_rates as conrate ON conrate.CONCESSION_CD = con.CONCESSION_CD 


==================================================================================================



  SELECT
    AA.reportdate,COUNT(AA.ticketcount) AS ticketcount,0 AS ticketamt,
    0 AS cwatickamt,0 AS winticketcount,0 AS winticketamt,0 AS wincwatickamt,0 AS PSGR 
    FROM
    (
        SELECT 
        DATE_FORMAT(way.COLLECTION_TM,'%Y-%m-%d') AS reportdate,
        IF(
                tic.group_ticket_flag=1, 
                'GR-00', IF(
                        tic.ticket_code = 'LG' AND tic.pass_id IS NOT NULL AND tic.pass_id != '', 
                        'LGSPL-00',
                            IF(
                                tic.ticket_code = 'LG' AND (tic.pass_id IS NULL OR tic.pass_id = ''), 
                                'LG-00',
                                group_concat(DISTINCT CONCAT_WS('-',tic.ticket_code,tic.insp_conc_code) ORDER BY CONCAT_WS('-',tic.ticket_code,tic.insp_conc_code)) 
                                #GROUP_CONCAT(DISTINCT CONCAT_WS('-',IF(LOCATE('EX',tic.ticket_number) > 0, 'EX', tic.ticket_code), tic.insp_conc_code) ORDER BY CONCAT_WS('-',tic.ticket_code,tic.insp_conc_code))
                            )
                        )
        ) ticket_code1,
        #COUNT(DISTINCT(CONCAT_WS('',tic.waybill_no,tic.ticket_number))) AS ticketcount,
        COUNT(DISTINCT(CONCAT_WS('',tic.waybill_no,tic.ticket_number,tic.etim_no))) AS ticketcount,
        0 AS ticketamt,
        0 AS cwatickamt,
        0 AS winticketcount,
        0 AS winticketamt,
        0 AS wincwatickamt
        FROM cb_waybillprogramming way
INNER JOIN cb_ticket tic ON tic.waybill_no=way.waybill_no
LEFT JOIN msrtc_replication.concessions as con ON con.CONCESSION_ID = tic.insp_conc_code
LEFT JOIN msrtc_replication.concession_rates as conrate ON conrate.CONCESSION_CD = con.CONCESSION_CD 
AND conrate.BUS_TYPE_CD = (SELECT BUS_TYPE_CD FROM msrtc_replication.bus_types WHERE BUS_TYPE_ID = tic.bus_service)
/*AND concat(DATE_FORMAT(concat_ws('/',SUBSTRING(tic.ticket_date,7,2),SUBSTRING(tic.ticket_date,4,2),SUBSTRING(tic.ticket_date,1,2)),'%Y-%m-%d'), ' ',tic.ticket_time)  BETWEEN conrate.EFFECTIVE_FROM AND conrate.EFFECTIVE_TILL*/ 
WHERE way.COLLECTION_TM BETWEEN '2018-06-01 00:00:00' AND '2018-06-30 23:59:59'
AND way.COLLECTION_TM NOT BETWEEN '2018-05-01 00:00:00' AND '2018-05-31 23:59:59' /*and
'0' AS PSGR*/ AND way.WAYBILL_STATUS='CLOSED' AND (CAST(tic.total_amt AS UNSIGNED) > 0)
AND (  
tic.ticket_code NOT IN ('CA','RF','PS','TO','OL','NG','DS','DU','CR','RC','UN','UP','LP','TC','AD','DF','PL','SC','CN')
OR ( (tic.ticket_code = 'CN') AND CAST(IFNULL(conrate.CONCESSION_RATE,0.00) AS UNSIGNED) < 100)
)
/*AND way.`is_new_machine` = 1 */
GROUP BY /*DATE_FORMAT(way.COLLECTION_TM,'%Y-%m-%d'),*/tic.waybill_no,tic.ticket_number,tic.etim_no
/*HAVING ticket_code1 != 'LG-00' */
) AA   GROUP BY AA.reportdate