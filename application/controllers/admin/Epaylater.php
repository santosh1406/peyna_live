<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Epaylater extends CI_Controller
{

    public $logFileName;
    public $category = array();
    public $epay_status = array();

    public function __construct()
    {
        parent::__construct();
        $this->ci = & get_instance();
        is_logged_in();

        $this->load->library(array('form_validation', 'epayLaterEncryptDecryptUtil', 'epaylater_api'));
        $this->load->model(array('epaylater_transaction_model', 'epaylater_request_response'));
        $this->load->helper(array('epaylater_helper', 'wallet_balance_helper', 'various_helper', 'form_validation', 'tso_helper'));

        // epaylater categories
        $this->category = json_decode(EPAYLATER_CATEGORY, true);

        // status codes
        $this->status = json_decode(EPAYLATER_STATUS, true);
    }

    /*
     *  This function is used to redirect to ePayLater site for payment
     *  for mobile recharge
     */
    public function epaylater_recharge()
    {
        $errMsg = null;
        $input = $this->input->post();
        $valid_service = array('jri', 'tso');

        if ($input) {
            if (!empty($input['type']) && in_array(strtolower($input['type']), $valid_service) && $input['payment_type'] == 'epaylater') {

                $user_id = $this->session->userdata('user_id');
                $service_type = 'mobile_' . strtolower(trim($input['type']));  // mobile_tso or mobile_jri
                $this->session->set_userdata('service_type', $service_type);
                $this->setLogFileName();

                // validation form
                $form_config = $this->form_validation_config($service_type, $input);
                if (form_validate_rules($form_config)) {
                    $recharge_amount = $input['recharge_amount'];

                    // check user wallet balance
                    if ($this->check_user_balances($service_type, $recharge_amount, $user_id) === false) {
                        $this->ci->session->set_flashdata('msg', 'Insufficient balance');
                        redirect(base_url() . 'admin/Utilities/mobile_recharge');
                    }

                    $unique_id = $user_id . 'T' . time();

                    $data = array();
                    $data['agent_id'] = $user_id;
                    $data['category'] = $this->category['MOBILE'];
                    $data['trans_date'] = get_current_date();
                    $data['amount'] = $recharge_amount;
                    $data['trimax_epay_transaction_id'] = $unique_id;
                    $data['status'] = $this->status['TRIMAX_INITIATE'];
                    $last_insert_id = $this->epaylater_transaction_model->insert_data($data);

                    $data['epay_trans_date'] = get_current_date('ISO8601');
                    $data['customer_mobile'] = $input['mobile_number'];
                    $data['device_client'] = $this->input->server('HTTP_USER_AGENT');

                    $request_arr = epay_request_arr($data);
                    $request_json = json_encode($request_arr);
                    log_data($this->logFileName, $request_arr, 'Request array to ePayLater for redirection');

                    $insert_data = array(
                        'request' => $request_json,
                        'request_type' => $this->status['REQUEST_REDIRECTION'],
                        'transaction_id' => $last_insert_id,
                        'request_date' => get_current_date()
                    );
                    $last_request_response_id = $this->epaylater_request_response->insert_data($insert_data);

                    // create session for later use
                    $epay_info = array(
                        'trimax_table_id' => $last_insert_id,
                        'request_response_id' => $last_request_response_id,
                        'trimax_epay_transaction_id' => $data['trimax_epay_transaction_id']
                    );
                    $this->session->set_userdata('epay_info', $epay_info);
                    $this->session->set_userdata('form_post_date', $input);

                    # encryption
                    $data['hash'] = epayLaterEncryptDecryptUtil::createChecksum($request_json);
                    $data['encrypt'] = epayLaterEncryptDecryptUtil::encrypt(EPAY_ENCRYPTION_KEY, EPAY_IV, $request_json);
                    $data['merchant_code'] = EPAY_MERCHANT_CODE;

                    $view = EPAY_TRANSACTION_VIEW;
                    load_back_view($view, $data);
                } else {
                    redirect(base_url() . 'admin/Utilities/mobile_recharge');
                }
            } else {
                $this->ci->session->set_flashdata('message', 'Invalid service type');
                redirect(base_url() . 'admin/Utilities/mobile_recharge');
            }
        } else {
            $this->ci->session->set_flashdata('message', 'Invalid form details');
            redirect(base_url() . 'admin/dashboard');
        }
    }

    /*
     *  This function is used to redirect to ePayLater site for payment with request data
     *  for datacard recharge
     */
    public function epaylater_datacard_recharge()
    {
        $input = $this->input->post();
        $valid_service = array('jri');

        if ($input) {
            if (!empty($input['type']) && in_array(strtolower($input['type']), $valid_service) && $input['payment_type'] == 'epaylater') {

                $user_id = $this->session->userdata('user_id');
                $service_type = 'datacard_' . strtolower(trim($input['type']));
                $this->session->set_userdata('service_type', $service_type);
                $this->setLogFileName();

                // validation form
                $form_config = $this->form_validation_config($service_type, $input);
                if (form_validate_rules($form_config) && strtolower($input['type']) == 'jri') {
                    $recharge_amount = $input['recharge_amount'];

                    // check user wallet balance
                    if ($this->check_user_balances($service_type, $recharge_amount, $user_id) === false) {
                        $this->ci->session->set_flashdata('msg', 'Insufficient balance');
                        redirect(base_url() . 'admin/Utilities/datacard');
                    }

                    // unique id (merchantOrderId) for transaction
                    $unique_id = $user_id . 'T' . time();

                    $data = array();
                    $data['agent_id'] = $user_id;
                    $data['category'] = $this->category['DATACARD'];
                    $data['trans_date'] = get_current_date();
                    $data['amount'] = $recharge_amount;
                    $data['trimax_epay_transaction_id'] = $unique_id;
                    $data['status'] = $this->status['TRIMAX_INITIATE'];
                    $last_insert_id = $this->epaylater_transaction_model->insert_data($data);

                    $data['epay_trans_date'] = get_current_date('ISO8601');
                    $data['customer_mobile'] = $input['mobile_number'];
                    $data['device_client'] = $this->input->server('HTTP_USER_AGENT');

                    $request_arr = epay_request_arr($data);
                    $request_json = json_encode($request_arr);
                    log_data($this->logFileName, $request_arr, 'Request array to ePayLater for redirection');

                    $insert_data = array(
                        'request' => $request_json,
                        'request_type' => $this->status['REQUEST_REDIRECTION'],
                        'transaction_id' => $last_insert_id,
                        'request_date' => get_current_date()
                    );
                    $last_request_response_id = $this->epaylater_request_response->insert_data($insert_data);

                    // create session for later use
                    $epay_info = array(
                        'trimax_table_id' => $last_insert_id,
                        'request_response_id' => $last_request_response_id,
                        'trimax_epay_transaction_id' => $data['trimax_epay_transaction_id']
                    );
                    $this->session->set_userdata('epay_info', $epay_info);
                    $this->session->set_userdata('form_post_date', $input);

                    # encryption
                    $data['hash'] = epayLaterEncryptDecryptUtil::createChecksum($request_json);
                    $data['encrypt'] = epayLaterEncryptDecryptUtil::encrypt(EPAY_ENCRYPTION_KEY, EPAY_IV, $request_json);
                    $data['merchant_code'] = EPAY_MERCHANT_CODE;

                    $view = EPAY_TRANSACTION_VIEW;
                    load_back_view($view, $data);
                } else {
                    redirect(base_url() . 'admin/Utilities/datacard');
                }
            } else {
                $this->ci->session->set_flashdata('message', 'Invalid service type');
                redirect(base_url() . 'admin/Utilities/datacard');
            }
        } else {
            $this->ci->session->set_flashdata('message', 'Invalid form details');
            redirect(base_url() . 'admin/dashboard');
        }
    }

    /*
     *  This function is used to redirect to ePayLater site for payment with request data
     *  for dht recharge
     */
    public function epaylater_dth_recharge()
    {
        $errMsg = null;
        $input = $this->input->post();
        $valid_service = array('jri', 'tso');

        if ($input) {
            if (!empty($input['type']) && in_array(strtolower($input['type']), $valid_service) && $input['payment_type'] == 'epaylater') {
                $user_id = $this->session->userdata('user_id');
                $service_type = 'dth_' . strtolower(trim($input['type']));
                $this->session->set_userdata('service_type', $service_type);
                $this->setLogFileName();

                $form_config = $this->form_validation_config($service_type, $input);
                if (form_validate_rules($form_config)) {
                    $recharge_amount = $input['recharge_amount'];

                    // check user wallet balance
                    if ($this->check_user_balances($service_type, $recharge_amount, $user_id) === false) {
                        $this->ci->session->set_flashdata('msg', 'Insufficient balance');
                        redirect(base_url() . 'admin/Utilities/datacard');
                    }

                    // unique id (merchantOrderId) for transaction
                    $unique_id = $user_id . 'T' . time();

                    $data = array();
                    $data['agent_id'] = $user_id;
                    $data['category'] = $this->category['DTH'];
                    $data['trans_date'] = get_current_date();
                    $data['amount'] = $recharge_amount;
                    $data['trimax_epay_transaction_id'] = $unique_id;
                    $data['status'] = $this->status['TRIMAX_INITIATE'];
                    $last_insert_id = $this->epaylater_transaction_model->insert_data($data);

                    $data['epay_trans_date'] = get_current_date('ISO8601');
                    $data['customer_mobile'] = $input['mobile_number'];
                    $data['device_client'] = $this->input->server('HTTP_USER_AGENT');

                    $request_arr = epay_request_arr($data);
                    $request_json = json_encode($request_arr);
                    log_data($this->logFileName, $request_arr, 'Request array to ePayLater for redirection');

                    $insert_data = array(
                        'request' => $request_json,
                        'request_type' => $this->status['REQUEST_REDIRECTION'],
                        'transaction_id' => $last_insert_id,
                        'request_date' => get_current_date()
                    );
                    $last_request_response_id = $this->epaylater_request_response->insert_data($insert_data);

                    // create session for later use
                    $epay_info = array(
                        'trimax_table_id' => $last_insert_id,
                        'request_response_id' => $last_request_response_id,
                        'trimax_epay_transaction_id' => $data['trimax_epay_transaction_id']
                    );
                    $this->session->set_userdata('epay_info', $epay_info);
                    $this->session->set_userdata('form_post_date', $input);

                    # encryption
                    $data['hash'] = epayLaterEncryptDecryptUtil::createChecksum($request_json);
                    $data['encrypt'] = epayLaterEncryptDecryptUtil::encrypt(EPAY_ENCRYPTION_KEY, EPAY_IV, $request_json);
                    $data['merchant_code'] = EPAY_MERCHANT_CODE;

                    $view = EPAY_TRANSACTION_VIEW;
                    load_back_view($view, $data);
                } else {
                    redirect(base_url() . 'admin/Utilities/dth');
                }
            } else {
                $this->ci->session->set_flashdata('message', 'Invalid service type');
                redirect(base_url() . 'admin/Utilities/datacard');
            }
        } else {
            $this->ci->session->set_flashdata('message', 'Invalid form details');
            redirect(base_url() . 'admin/dashboard');
        }
    }

    /*
     *  This function is used to read response from ePayLater after paymnet completes
     *  parse response and send to respective controller
     */
    public function epay_redirect()
    {
        $input = $this->input->post(); 
	$this->setLogFileName();
        log_data($this->logFileName, $input, 'POST data return from ePayLater (callbackUrl)');

        if (!empty($input)) {
            $eay_info = array();
            $encdata = trim($input['encdata']);
            $responseChecksum = trim($input['checksum']);
            $decryptJson = epayLaterEncryptDecryptUtil::decrypt(EPAY_ENCRYPTION_KEY, EPAY_IV, $encdata);

            $checksum = epayLaterEncryptDecryptUtil::createChecksum($decryptJson);
            $decrypt = json_decode($decryptJson, 1);
            log_data($this->logFileName, $decrypt, 'Post `encdata` data in array');

            // append epay info in session array
            $epay_info = $this->session->userdata('epay_info');
            $epay_info['epay_order_id'] = $decrypt['eplOrderId'];
            $epay_info['status'] = $decrypt['status'];
            $epay_info['amount'] = $decrypt['amount'];
            $epay_info['epay_trans_date'] = $decrypt['date'];
            $this->session->set_userdata('epay_info', $epay_info);

            if (strtolower($decrypt['status']) == 'success')
                $epay_respose_status = $this->status['EPAY_APPROVE_SUCCESS'];
            else
                $epay_respose_status = $this->status['EPAY_APPROVE_FAILED'];

            // update database `epaylater_transaction` table
            $update_data = array();
            $update_data['epay_order_id'] = $decrypt['eplOrderId'];
            $update_data['amount_approved'] = convert_into_rupees($decrypt['amount']);
            $update_data['status'] = $epay_respose_status;
            $update_data['status_code'] = $decrypt['statusCode'];
            $update_data['status_description'] = $decrypt['statusDesc'];

            if ($checksum != $responseChecksum) {
                // something went wrong
                $update_data['status'] = $this->status['EPAY_APPROVE_FAILED'];
                $update_data['comments'] = 'Something went wrong (Invalid checksum)';

                // update db table
                $update_db = $this->epaylater_transaction_model->update_data($epay_info['trimax_table_id'], $update_data);
                $this->session->unset_userdata('form_post_date');
                $this->session->unset_userdata('epay_info');

                log_data($this->logFileName, $responseChecksum, 'Invalid checksum');
                $this->ci->session->set_flashdata('msg', 'ePayLater Transaction failed (invalid checksum)');
                redirect(base_url() . 'admin/dashboard');
            } else {
                // update db table
                $update_db = $this->epaylater_transaction_model->update_data($epay_info['trimax_table_id'], $update_data);
            }

            // update epay response in table
            $up_response = array('response' => $decryptJson, 'response_date' => get_current_date());
            $update_respose = $this->epaylater_request_response->update_data($epay_info['request_response_id'], $up_response);

            // EXCEPTION (if intentionally click inbetween payment process)
           /* if (!isset($epay_info['trimax_table_id']) || empty($epay_info['trimax_table_id'])) {
                $this->session->unset_userdata('form_post_date');
                $this->session->unset_userdata('epay_info');
                $this->ci->session->set_flashdata('msg', 'ePayLater transaction executed at background');
                redirect(base_url() . 'admin/dashboard');
            }*/

            if (strtolower($decrypt['status']) != 'success') {
                $this->session->unset_userdata('form_post_date');
                $this->session->unset_userdata('epay_info');
                $this->ci->session->set_flashdata('msg', 'ePayLater Transaction failed (' . $update_data['status_description'] . ')');
                redirect(base_url() . 'admin/dashboard');
            } else {

                $insert_data = array(
                    'request' => 'PUT request - EpayOrderId (' . $decrypt['eplOrderId'] . ') and TrimaxEpayTransactionId (' . $decrypt['marketplaceOrderId'] . ')',
                    'request_type' => $this->status['REQUEST_CONFIRMATION'],
                    'transaction_id' => $epay_info['trimax_table_id'],
                    'request_date' => get_current_date()
                );
                $last_request_response_id = $this->epaylater_request_response->insert_data($insert_data);

                // call API for confimed & delivered

                $response = $this->epaylater_api->confirmed_transaction($decrypt['eplOrderId'], $decrypt['marketplaceOrderId']);
                log_data($this->logFileName, $response, 'ePayLater response (Transaction confirmed & delivered)');

                $up_response = array('response' => json_encode($response), 'response_date' => get_current_date());
                $update_respose = $this->epaylater_request_response->update_data($last_request_response_id, $up_response);

                if (strtolower($response['status']) != 'delivered') {

                    $update_data = array('status' => $this->status['EPAY_CONFIRM_FAILED']);
                    $update_id = $this->epaylater_transaction_model->update_data($epay_info['trimax_table_id'], $update_data);

                    $this->session->unset_userdata('form_post_date');
                    $this->session->unset_userdata('epay_info');
                    $this->ci->session->set_flashdata('msg', 'ePayLater Transaction failed');
                    redirect(base_url() . 'admin/dashboard');
                } else {
                    //update in database & redirect to main function with form post data(from session)
                    $update_data = array('status' => $this->status['EPAY_CONFIRM_SUCCESS']);
                    $update_id = $this->epaylater_transaction_model->update_data($epay_info['trimax_table_id'], $update_data);

                    $service_type = $this->session->userdata('service_type');
                    if ($service_type == 'mobile_tso') {
                        redirect(base_url() . 'admin/Tso/recharge');
                    } elseif ($service_type == 'mobile_jri') {
                        redirect(base_url() . 'admin/Utilities/recharge');
                    } elseif ($service_type == 'datacard_jri') {
                        redirect(base_url() . 'admin/Utilities/datacard_recharge');
                    } elseif ($service_type == 'dth_jri' || $service_type == 'dth_tso') {
                        redirect(base_url() . 'admin/Utilities/dth_recharge');
                    }
                }
            }
        } else {
            log_data($this->logFileName, $input, 'Empty response from ePayLater (Transaction failed)');
            $this->ci->session->set_flashdata('msg', 'Empty response from ePayLater (Transaction failed)');
            redirect(base_url() . 'admin/dashboard');
        }
    }

    function setLogFileName($logFile = null)
    {
        $service_type = $this->session->userdata('service_type');
        if ($service_type == 'mobile_jri') {
            $log = 'JRI_recharge.log';
        } elseif ($service_type == 'mobile_tso') {
            $log = 'TSO_recharge.log';
        } elseif ($service_type == 'datacard_jri') {
            $log = 'JRI_datacard_recharge.log';
        } elseif ($service_type == 'dth_jri') {
            $log = 'jri_dth.log';
        } elseif ($service_type == 'dth_tso') {
            $log = 'tso_dth.log';
        } else {
            $log = 'Epaylater.log';
        }
        if ($logFile) {
            $log = $logFile . '.log';
        }

        $this->logFileName = date('Y') . '/' . date("M") . '/' . date("d") . '/' . $log;
    }

    function form_validation_config($service_type, $input)
    {
        $config = false;
        if ($service_type == 'mobile_jri') {
            $config = array(
                array('field' => 'recharge_type', 'label' => 'Recharge Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter recharge type.')),
                array('field' => 'mobile_operator', 'label' => 'Mobile Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter mobile operator.')),
                array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.')),
                array('field' => 'recharge_amount', 'label' => 'Recharge Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter recharge amount.'))
            );
        } elseif ($service_type == 'mobile_tso') {
            if (!empty($input['post_operator'])) {
                $input['rec_type'] = 'Postpaid Mobile';
                $input['mobile_operator'] = getTsoPostpaidOperators($input['post_operator']);
            } else {
                $input['rec_type'] = 'Prepaid Mobile';
                $input['mobile_operator'] = getTsoPrepaidOperators($input['pre_operator']);
            }

            $config = array(
                array('field' => 'plan_type', 'label' => 'Recharge Plan Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select recharge plan type.')),
                array('field' => ($input['plan_type'] == 'Prepaid Mobile') ? 'pre_operator' : 'post_operator', 'label' => 'Mobile Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select mobile operator.')),
                array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.')),
                array('field' => 'recharge_amount', 'label' => 'Recharge Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter recharge amount.')),
                array('field' => ($input['plan_type'] == 'Prepaid Mobile') ? 'recharge_type' : '', 'label' => 'Recharge Type', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please select recharge type.'))
            );
        } elseif ($service_type == 'datacard_jri') {
            $config = array(
                array('field' => 'mobile_operator', 'label' => 'Mobile Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter mobile operator.')),
                array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.')),
                array('field' => 'recharge_amount', 'label' => 'Recharge Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter recharge amount.'))
            );
        } elseif ($service_type == 'dth_jri' || $service_type == 'dth_tso') {
            $config = array(
                array('field' => 'dth_operator', 'label' => 'DTH Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter dth operator.')),
                array('field' => 'customer_number', 'label' => 'Customer Number', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please check Customer number.')),
                array('field' => 'recharge_amount', 'label' => 'Recharge Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter recharge amount.')),
                array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.')),
            );
        }
        return $config;
    }

    public function check_user_balances($service_type, $recharge_amount, $user_id)
    {
        $jri_balance_arr = array('mobile_jri', 'datacard_jri', 'dth_jri');

        if (in_array($service_type, $jri_balance_arr)) {
            $jri_baalance = jri_balance();
            if ($jri_baalance['Balance'] == 0 && $recharge_amount > $jri_baalance['Balance']) {
                return false;
            }
        } elseif ($service_type == 'mobile_tso' || $service_type == 'dth_tso') {
            $tso_baalance = authTSO();
            if ($tso_baalance['STCODE'] != 0) {
                return false;
            }
        }

        // get user wallet balanace
        $check_user_wallet = $this->epaylater_transaction_model->get_user_wallet_details($user_id);
        if ($recharge_amount > $check_user_wallet) {
            return false;
        }
        return true;
    }

    /*
     * dispplay status descritions
     * this status come from constant file
     */
    public function status_descriptions()
    {
        $data['status'] = $this->status;
        $view = EPAY_STATUS_DESCRIPTIONS;
        load_back_view($view, $data);
    }

    /*
     * Display all transaction made by agent
     */
    public function transaction_list()
    {
        $this->load->library('pagination');

        $input = $this->input->post();
        $data['trimax_id'] = null;
        $where = array();

        $roll_id = $this->session->userdata("role_id");
        $user_id = $this->session->userdata('user_id');
        if ($roll_id == 7) {
            $where['agent_id'] = $user_id;
        }

        if ($input) {
            if (!empty($input['trimax_id'])) {
                $data['trimax_id'] = $input['trimax_id'];
                $where['trimax_epay_transaction_id'] = $input['trimax_id'];
            }
            if (!empty($input['category'])) {
                $data['category'] = $input['category'];
                $where['category'] = $input['category'];
            }
            if (!empty($input['trans_start_date'])) {
                $data['trans_start_date'] = $input['trans_start_date'];
                $where['trans_date >='] = date('Y-m-d 00:00:00', strtotime($input['trans_start_date']));
            }
            if (!empty($input['trans_end_date'])) {
                $data['trans_end_date'] = $input['trans_end_date'];
                $where['trans_date <'] = date('Y-m-d 00:00:00', strtotime($input['trans_end_date'] . ' +1 day'));
            }
        }
        
        $config['base_url'] = base_url('admin/epaylater/transaction_list/');
        $config['per_page'] = 10;
        $config['total_rows'] = $this->epaylater_transaction_model->get_count_rows($where);
        $config['use_page_numbers'] = TRUE;

        $this->pagination->initialize($config);
        $page = $this->uri->segment(4) ? $this->uri->segment(4) : 0;
        $data['pagi_links'] = $this->pagination->create_links();
        $data['result'] = $this->epaylater_transaction_model->get_all_rows($config['per_page'], ($page * $config['per_page']), $where);

        $data['category_arr'] = $this->category;

        $view = EPAY_TRANSACTION_LIST;
        load_back_view($view, $data);
    }

    /*
     * Display agent wise amount settlement with transaction date
     */
    public function agent_settlement_report()
    {
        $this->load->library('pagination');
        $this->load->model(array('epaylater_agent_settlement_model'));

        $input = $this->input->post();
        $where = array();

        if ($input) {
            if (!empty($input['agent_id'])) {
                $where['user_id'] = $input['agent_id'];
                $data['agent_id'] = $input['agent_id'];
            }
            if (!empty($input['trans_date'])) {
                $where['transaction_date'] = date('Y-m-d', strtotime($input['trans_date']));
                $data['trans_date'] = $input['trans_date'];
            }
        } else {
            // defaul value
            $data['agent_id'] = NULL;
            $data['trans_date'] = date('d-m-Y', strtotime("-1 days"));
            $where['transaction_date'] = date('Y-m-d', strtotime("-1 days"));
        }

        $config['base_url'] = base_url('admin/epaylater/agent_settlement_report/');
        $config['per_page'] = 10;
        $config['total_rows'] = $this->epaylater_agent_settlement_model->get_count_rows($where);
        $config['use_page_numbers'] = TRUE;

        $this->pagination->initialize($config);
        $page = $this->uri->segment(4) ? $this->uri->segment(4) : 0;
        $data['pagi_links'] = $this->pagination->create_links();
        $data['result'] = $this->epaylater_agent_settlement_model->get_all_rows($config['per_page'], $page, $where);

        $view = EPAY_AGENT_SETTLEMENT_REPORT;
        load_back_view($view, $data);
    }

    /*
     * Display total amount of transaction made by agent
    */
    public function agent_daily_tranaction()
    {
        $input = $this->input->post();
        $user_id = $this->session->userdata('user_id');
        $data['from_date'] = null;
        $data['to_date'] = null;

        if (!empty($input)) {
            $where = array();
            $where['agent_id'] = $user_id;
            $where['trans_date >= '] = date('Y-m-d 00:00:00', strtotime($input['from_date']));
            $where['trans_date < '] = date('Y-m-d 00:00:00', strtotime($input['to_date'] . ' +1 day'));

            // get total amount for transaction which are sucess
            $where['status'] = $this->status['TRIMAX_SUCCESS'];
            $result = $this->epaylater_transaction_model->get_agent_transacton($where);
            $data['trimax_success_amt'] = $result->amount;

            // get total amount of transaction which are failed at trimax end but success at epaylater
            $where['status'] = $this->status['EPAY_CONFIRM_SUCCESS'];
            $result = $this->epaylater_transaction_model->get_agent_transacton($where);
            $data['trimax_falied_amt'] = $result->amount;

            $data['from_date'] = $input['from_date'];
            $data['to_date'] = $input['to_date'];
        }

        $view = EPAY_AGENT_REPORTS;
        load_back_view($view, $data);
    }

    /*
     * This funcitonlity use by account people to approve epayleter settlement amount in our Rokad system
     * Account person will see all individual agent settlement amount by date
     * and then he approve the settlemet ammount
     */
    public function check_and_approve_amt_settlement()
    {
        $this->load->model(array('epaylater_agent_settlement_model'));

        $get_input = $this->input->get();
        $input = $this->input->post();
        $where = array();

        if ($input) {
            if (!empty($input['trans_date'])) {
                $where['transaction_date'] = date('Y-m-d', strtotime($input['trans_date']));
                $data['trans_date'] = $input['trans_date'];
            }
        } else {
            // get value
            if ($get_input && isset($get_input['date'])) {
                $where['transaction_date'] = date('Y-m-d', strtotime($get_input['date']));
                $data['trans_date'] = $get_input['date'];
            } else {
                // defaul value
                $data['trans_date'] = date('d-m-Y', strtotime("-1 days"));
                $where['transaction_date'] = date('Y-m-d', strtotime("-1 days"));
            }
        }

        $data['result'] = $this->epaylater_agent_settlement_model->get_all_rows(null, null, $where);

        $data['total_amount'] = null;
        $data['amt_transfer_info'] = null;
        if (count($data['result']) > 0 && !empty($where['transaction_date'])) {
            // Get total amount for `transaction_date`
            $data['total_amount'] = $this->epaylater_agent_settlement_model->get_total_amount($where);
            // Get all info for `transaction_date`
            $data['amt_transfer_info'] = $this->epaylater_transaction_model->get_amt_transfer_info($where);
        }

        $view = EPAY_CHECK_AMOUNT_SETTLEMENT;
        load_back_view($view, $data);
    }

    /*
     *  form submition to submit settlement amount in out rokad system
     */
    public function check_and_approve_amt_settlement_submit()
    {
        $input = $this->input->post();
        if ($input) {
            $config = array(
                array('field' => 'total_epay_settlement_amount_in_day', 'label' => 'Total Settlement Amount', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter valid amount.')),
                array('field' => 'transaction_date_for_approval', 'label' => 'Transaction date', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter transaction date.')),
                array('field' => 'comments', 'label' => 'Comments', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter transaction date.'))
            );

            if (form_validate_rules($config)) {
                $insertData = array();
                $insertData['credit_amount'] = base64_decode($input['total_epay_settlement_amount_in_day']);
                $insertData['transaction_date'] = date('Y-m-d', strtotime(base64_decode($input['transaction_date_for_approval'])));
                $insertData['current_date'] = date('Y-m-d H:i:s');
                $insertData['comments'] = $input['comments'];
                $insertData['status'] = 'Y';
                $insertData['is_trimax_wallet_transfer'] = 'NO';
                $insertData['is_agent_wallet_transfer'] = 'NO';

                $last_id = $this->epaylater_transaction_model->insert_transac_amount($insertData);
                if ($last_id) {
                    $this->session->set_flashdata('msg', 'Settlement amount approved successfully');
                    redirect(base_url() . 'admin/epaylater/show_amount_approval_history');
                } else {
                    $this->session->set_flashdata('msg', 'Error : Settlement amount approval failed');
                    redirect(base_url() . 'admin/epaylater/check_and_approve_amt_settlement');
                }
            } else {
                redirect(base_url() . 'admin/epaylater/check_and_approve_amt_settlement');
            }
        } else {
            redirect(base_url() . 'admin/epaylater/check_and_approve_amt_settlement');
        }
    }

    /*
     * Dispaly settlement amount history with respect to date
     * this functionality use for account person
     */
    public function show_amount_approval_history()
    {
        $this->load->library('pagination');
        $this->load->model(array('wallet_trans_model'));

        $where = array();

        $input = $this->input->post();
        if ($input) {
            if (!empty($input['trans_date'])) {
                $where['transaction_date'] = date('Y-m-d', strtotime($input['trans_date']));
                $data['trans_date'] = $input['trans_date'];
            }
        }

        $config['base_url'] = base_url('admin/epaylater/show_amount_approval_history/');
        $config['per_page'] = 10;
        $config['total_rows'] = $this->epaylater_transaction_model->get_count($where);
        $config['use_page_numbers'] = TRUE;

        $this->pagination->initialize($config);
        $page = $this->uri->segment(4) ? $this->uri->segment(4) : 0;
        $data['pagi_links'] = $this->pagination->create_links();
        $data['result'] = $this->epaylater_transaction_model->get_epay_transfer_amount($config['per_page'], ($page), $where);

        $view = EPAY_CREDIT_AMOUNT;
        load_back_view($view, $data);
    }

    /*
     * This function read & parse the settlement sheet share by epaylater to our system
     * and put all data in `epaylater_settlement_sheet` table
     */
    public function save_settlement_sheet()
    {
        $this->load->library("Excel");
        $this->load->model('Epaylater_settlement_sheet_model');
        $this->setLogFileName('EpayLater');

        $fileName = 'sample-settlement-file-sample-records.xlsx';
        $filePath = '/var/www/html/rokad/';
        $inputFileName = $filePath . $fileName;
        log_data($this->logFileName, $inputFileName, 'Epaylater settlement sheet');

        try {
            //  Read your Excel workbook
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);

            //  Get worksheet dimensions
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            #$highestColumn = $sheet->getHighestColumn();
            $highestColumn = 'N';

            //  Loop through each row of the worksheet in turn
            $insertData = array();
            for ($row = 2; $row <= $highestRow; $row++) {
                $rowDataArr = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $rowData = $rowDataArr[0];

                // remove white space from all values
                $rowData = array_map('trim', $rowData);

                $tempData = array();
                $tempData['epl_transaction_id'] = $rowData[0];
                $tempData['marketplace_orderid'] = $rowData[1];
                $tempData['invoice_number'] = $rowData[2];
                $tempData['transaction_date'] = convert_date_to_mysql_format($rowData[3]);
                $tempData['delivered_date'] = convert_date_to_mysql_format($rowData[4]);
                $tempData['transaction_status'] = $rowData[5];
                $tempData['inv_amt'] = $rowData[6];
                $tempData['mdr'] = $rowData[7];
                $tempData['settlement_amount'] = $rowData[8];
                $tempData['settlement_status'] = $rowData[9];
                $tempData['settlement_date'] = convert_date_to_mysql_format($rowData[10]);
                $tempData['utr_neft_imps_ref_no'] = $rowData[11];
                $tempData['remark'] = $rowData[12];
                $tempData['file_upload_name'] = $fileName;
                $tempData['file_upload_date'] = date('Y-m-d H:i:s');

                $insertData[] = $tempData;
            }

            $dataCount = count($insertData);
            if ($dataCount > 0) {
                $chunkSize = 500;
                $chunk_data = array_chunk($insertData, $chunkSize);
                foreach ($chunk_data as $insertDataChunk) {
                    $udatedRowsCount = $this->Epaylater_settlement_sheet_model->insert_data($insertDataChunk);
                }

                // update `trimax_agent_id` in epaylater_settlement_sheet table with `agent_id` in `epaylater_transaction` table
                $udated_trimax_agent_ids = $this->Epaylater_settlement_sheet_model->update_trimax_agent_id();
            }
        } catch (Exception $e) {
            log_data($this->logFileName, 'Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage(), 'sheet uploading error');
        }
    }

    /*
     * This function read the date from `epaylater_settlement_sheet` table date wise
     * and put date in `epaylater_agent_settlement` table
     * `epaylater_agent_settlement` table contain total settlement amount by individual agent with date
     */
    public function agent_amount_settlement()
    {
        $this->load->model(array('epaylater_agent_settlement_model'));
        $this->setLogFileName('EpayLater');

        $start_date = date('Y-m-d  00:00:00', strtotime("-1 days"));
        $end_date = date('Y-m-d  00:00:00');

        $result = $this->epaylater_transaction_model->get_trans_amount($start_date, $end_date);
        log_data($this->logFileName, $result, 'agent_amount_settlement');

        if (!empty($result)) {
            $bulkInsert = array();
            $user_ids = array();
            foreach ($result as $eachRow) {
                $tempArr = array();
                $tempArr['user_id'] = $eachRow['trimax_agent_id'];
                $tempArr['amount'] = $eachRow['inv_amt'];
                $tempArr['transaction_date'] = date('Y-m-d', strtotime($start_date)); // $start_date;
                $tempArr['created_date'] = date('Y-m-d H:i:s');

                $user_ids[] = $eachRow['trimax_agent_id'];
                $bulkInsert[] = $tempArr;
            }
            $udatedRowsCount = $this->epaylater_agent_settlement_model->insert_data($bulkInsert);
            log_data($this->logFileName, $bulkInsert, 'insert bulk data in `epaylater_agent_settlement`table');

            // update company level3 in `epaylater_agent_settlement`
            $str_user_ids = implode(", ", $user_ids);
            $result = $this->epaylater_transaction_model->get_level3_id_by_user_ids($str_user_ids);

            $transaction_date = date('Y-m-d', strtotime($start_date));
            foreach ($result as $eachUserInfo) {
                if (!empty($eachUserInfo['level_3'])) {
                    $udated_company_id = $this->epaylater_agent_settlement_model->update_level3_id(
                        $eachUserInfo['id'], $transaction_date, $eachUserInfo['level_3']
                    );
                }
            }
        }
    }

    /*
     * CRON script to update trimax wallet after account team approve the settlemetn amount
     */
    public function update_trimax_wallet($yesterday = '')
    {
        $this->load->model(array('wallet_trans_model','epaylater_transaction_model','users_model'));
        $this->setLogFileName('EpayLater');

        $TRIMAX_USER_ID = $this->users_model->get_trimax_user_id();
        
        if (empty($yesterday)) {
            $yesterday = date('Y-m-d', strtotime("-1 days"));
        }
        $where = array('transaction_date' => $yesterday);
        $transfer_amount_info = $this->epaylater_transaction_model->get_epay_transfer_amount(null, null, $where);
        $epay_credit_amount = $transfer_amount_info[0]['credit_amount'];

        if (trim($transfer_amount_info[0]['is_trimax_wallet_transfer']) != 'NO') {
            log_data($this->logFileName, 'Already updated the TRIMAX user acoount');
            die('Already updated the TRIMAX user acoount');
        }

        // addind in trimax account in `wallet` table
        $trimax_wallet_info = $this->epaylater_transaction_model->get_user_details($TRIMAX_USER_ID);
        $new_trimax_amount = $trimax_wallet_info['amt'] + $epay_credit_amount;
        $update_trimax_wallet = $this->epaylater_transaction_model->update_user_wallet($new_trimax_amount, $TRIMAX_USER_ID);
        $logTxt = "Trimax Old amount -> " . $trimax_wallet_info ['amt'] . " New amount -> $new_trimax_amount,  value added by epaylater -> " . $epay_credit_amount;
        log_data($this->logFileName, $logTxt, 'updating trimax wallet amount (adding amount)');

        $tras_detail = array();
        $tras_detail['w_id'] = $trimax_wallet_info['id'];
        $tras_detail['amt'] = $epay_credit_amount;
        $tras_detail['user_id'] = '';
        $tras_detail['status'] = 'Amount Credited';
        $tras_detail['amt_before_trans'] = $trimax_wallet_info['amt'];
        $tras_detail['amt_after_trans'] = $new_trimax_amount;
        $tras_detail['transaction_type'] = 'Credited';

        $this->update_wallet_trans_table($tras_detail);

        // update status in `epaylater_transfer_amount` table to avoid repeated execution
        $this->epaylater_transaction_model->update_cron_status_for_transfer_amount('is_trimax_wallet_transfer', 'YES', $where);
    }

    /*
     * CRON SCRIPT update regional distributor wallet
     */
    public function update_regional_distributor_wallet()
    {
        /*
        $this->load->model(array('epaylater_agent_settlement_model', 'wallet_trans_model'));
        $this->setLogFileName('EpayLater');

        $TRIMAX_USER_ID = 195;
        $yesterday = date('Y-m-d', strtotime("-1 days"));
        $result = $this->epaylater_agent_settlement_model->get_group_amount_by_rd($yesterday);
        
        $where = array('transaction_date' => $yesterday);
        $transfer_amount_info = $this->epaylater_transaction_model->get_epay_transfer_amount(null, null, $where);
        if (trim($transfer_amount_info[0]['is_rd_wallet_transfer']) != 'NO') {
            log_data($this->logFileName, 'Already updated the RD users acoount');
            die('Already updated the RD users acoount');
        }

        if (!empty($result)) {
            foreach ($result as $eachRecord) {
                // deduct from company user
                $trimax_wallet_info = $this->epaylater_transaction_model->get_user_details($TRIMAX_USER_ID);
                $trimax_amount_deduct = $trimax_wallet_info['amt'] - $eachRecord['amount'];
                $update_trimax_wallet = $this->epaylater_transaction_model->update_user_wallet($trimax_amount_deduct, $TRIMAX_USER_ID);
                $logTxt = "Trimax Old amount -> " . $trimax_wallet_info['amt'] . " New amount -> $trimax_amount_deduct,  amount deduct -> " . $eachRecord['amount'];
                log_data($this->logFileName, $logTxt, 'updating trimax wallet (deduct amount)');

                //adding new entry in `wallet_trans` table for trimax account deduction
                $tras_detail = array();
                $tras_detail['w_id'] = $trimax_wallet_info['id'];
                $tras_detail['amt'] = $eachRecord['amount'];
                $tras_detail['status'] = 'Amount Debited';
                $tras_detail['user_id'] = $TRIMAX_USER_ID;
                $tras_detail['amt_before_trans'] = $trimax_wallet_info['amt'];
                $tras_detail['amt_after_trans'] = $trimax_amount_deduct;
                $tras_detail['transaction_type'] = 'Debited';

                $this->update_wallet_trans_table($tras_detail);

                // adding amount in `regional_distributor` wallet
                $rd_wallet_info = $this->epaylater_transaction_model->get_user_details($eachRecord['regional_distributor_id']);
                $new_amount = $rd_wallet_info['amt'] + $eachRecord['amount'];
                $update_wallet = $this->epaylater_transaction_model->update_user_wallet($new_amount, $eachRecord['regional_distributor_id']);
                $logTxt = "Regional Distributor Old amount -> " . $rd_wallet_info['amt'] . " New amount -> $new_amount,  value added by epaylater -> " . $eachRecord['amount'];
                log_data($this->logFileName, $logTxt, 'updating regional_distributor_wallet amount (adding amount)');

                //adding new entry in `wallet_trans` table for trimax account deduction
                $tras_detail = array();
                $tras_detail['w_id'] = $rd_wallet_info['id'];
                $tras_detail['amt'] = $eachRecord['amount'];
                $tras_detail['status'] = 'Amount Credited';
                $tras_detail['user_id'] = $eachRecord['regional_distributor_id'];
                $tras_detail['amt_before_trans'] = $rd_wallet_info['amt'];
                $tras_detail['amt_after_trans'] = $new_amount;
                $tras_detail['transaction_type'] = 'Credited';

                $this->update_wallet_trans_table($tras_detail);
            }

            // update status in `epaylater_transfer_amount` table to avoid repeated execution
            $this->epaylater_transaction_model->update_cron_status_for_transfer_amount('is_rd_wallet_transfer', 'YES', $where);
        }
        */
    }

    /*
     * CRON SCRIPT update Agent wallet
     */
    public function update_agent_wallet()
    {
        $this->load->model(array('epaylater_agent_settlement_model', 'wallet_trans_model', 'epaylater_transaction_model', 'users_model'));
        $this->setLogFileName('EpayLater');
        
        $TRIMAX_USER_ID = $this->users_model->get_trimax_user_id();

        $yesterday = date('Y-m-d', strtotime("-1 days"));
        $result = $this->epaylater_agent_settlement_model->get_user_amount_by_date($yesterday);

        $where = array('transaction_date' => $yesterday);
        $transfer_amount_info = $this->epaylater_transaction_model->get_epay_transfer_amount(null, null, $where);
        if (trim($transfer_amount_info[0]['is_agent_wallet_transfer']) != 'NO') {
            log_data($this->logFileName, 'Already updated the Agent acoount');
            die('Already updated the Agent acoount');
        }

        if (!empty($result)) {
            foreach ($result as $eachRecord) {               
                
                // deduct from company user
                $trimax_wallet_info = $this->epaylater_transaction_model->get_user_details($TRIMAX_USER_ID);
                $trimax_amount_deduct = $trimax_wallet_info['amt'] - $eachRecord['amount'];
                $update_trimax_wallet = $this->epaylater_transaction_model->update_user_wallet($trimax_amount_deduct, $TRIMAX_USER_ID);
                $logTxt = "Trimax Old amount -> " . $trimax_wallet_info['amt'] . " New amount -> $trimax_amount_deduct,  amount deduct -> " . $eachRecord['amount'];
                log_data($this->logFileName, $logTxt, 'updating trimax wallet (deduct amount)');
                
                //adding new entry in `wallet_trans` table for trimax account deduction
                $tras_detail = array();
                $tras_detail['w_id'] = $trimax_wallet_info['id'];
                $tras_detail['amt'] = $eachRecord['amount'];
                $tras_detail['status'] = 'Amount Debited';
                $tras_detail['user_id'] = $TRIMAX_USER_ID;
                $tras_detail['amt_before_trans'] = $trimax_wallet_info['amt'];
                $tras_detail['amt_after_trans'] = $trimax_amount_deduct;
                $tras_detail['transaction_type'] = 'Debited';

                $this->update_wallet_trans_table($tras_detail);

                // adding in user's account (agent wallet)
                $user_wallet_info = $this->epaylater_transaction_model->get_user_details($eachRecord['user_id']);
                $new_amount = $user_wallet_info['amt'] + $eachRecord['amount'];
                $update_agent_wallet = $this->epaylater_transaction_model->update_user_wallet($new_amount, $eachRecord['user_id']);
                $logTxt = "Agent Old amount -> " . $user_wallet_info['amt'] . " New amount -> $new_amount,  amount deduct -> " . $eachRecord['amount'];
                log_data($this->logFileName, $logTxt, 'updating user wallet (adding amount)');

                //adding new entry in `wallet_trans` table for trimax account deduction
                $tras_detail = array();
                $tras_detail['w_id'] = $user_wallet_info['id'];
                $tras_detail['amt'] = $eachRecord['amount'];
                $tras_detail['status'] = 'Amount Credited';
                $tras_detail['user_id'] = $eachRecord['user_id'];
                $tras_detail['amt_before_trans'] = $user_wallet_info['amt'];
                $tras_detail['amt_after_trans'] = $new_amount;
                $tras_detail['transaction_type'] = 'Credited';

                $this->update_wallet_trans_table($tras_detail);

                $update_status = $this->epaylater_agent_settlement_model->update_status(
                    $eachRecord['id'], 'Y', 'Amount debited in Agent account'
                );
            }

            // update status in `epaylater_transfer_amount` table to avoid repeated execution
            $this->epaylater_transaction_model->update_cron_status_for_transfer_amount('is_agent_wallet_transfer', 'YES', $where);
        }
    }

    /*
     * update details in `wallet_trans` whenever any change made in `wallet` table
     */
    private function update_wallet_trans_table($detail)
    {
        $wallet_trans_detail = array();
        $wallet_trans_detail['w_id'] = $detail['w_id'];
        $wallet_trans_detail['amt'] = $detail['amt'];
        $wallet_trans_detail['comment'] = 'ePayLater T+1 Transaction';
        $wallet_trans_detail['status'] = $detail['status'];
        $wallet_trans_detail['user_id'] = $detail['user_id'];
        $wallet_trans_detail['added_by'] = 'ePayLater Cron Script';
        $wallet_trans_detail['added_on'] = date("Y-m-d H:i:s");
        $wallet_trans_detail['amt_before_trans'] = $detail['amt_before_trans'];
        $wallet_trans_detail['amt_after_trans'] = $detail['amt_after_trans'];
        $wallet_trans_detail['wallet_type'] = 'actual_wallet';
        $wallet_trans_detail['transaction_type'] = $detail['transaction_type'];
        $wallet_trans_detail['is_status'] = 'Y';


        $wallet_trans_id = $this->wallet_trans_model->insert($wallet_trans_detail);
        log_data($this->logFileName, $wallet_trans_id, 'adding new entry in wallet_trans table for Trimax');
    }

    function ajax_view_agent_transactions_by_date()
    {
        $this->load->model(array('epaylater_settlement_sheet_model'));

        $input = $this->input->post();

        $epay_agent_settlmnt_tbl_id = $input['id'];
        $transaction_date = $input['transaction_date'];
        $user_id = $input['user_id'];

        $sheet_result = $this->epaylater_settlement_sheet_model->get_trimax_order_ids($user_id, $transaction_date);

        $final_data = array();
        if (!empty($sheet_result)) {
            $i = 1;
            foreach ($sheet_result as $eachRow) {
                $temp = array();
                $temp['No'] = $i;
                $temp['AgentName'] = $eachRow['display_name'];
                $temp['Type'] = $eachRow['type'];
                $temp['Operator'] = $eachRow['operator'];
                $temp['RechargeOn'] = $eachRow['recharge_on'];
                $temp['Amount'] = $eachRow['inv_amt'];
                $temp['TrimaxId'] = $eachRow['marketplace_orderid'];
                $temp['WalletTranId'] = $eachRow['wallet_tran_id'];
                $temp['CreatedOn'] = $eachRow['created_on'];

                $final_data[] = $temp;
                $i++;
            }
        }
        echo '{ "data": ' . json_encode($final_data) . ' }';
        die;
    }
    
}
