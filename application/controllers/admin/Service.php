<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('service_master_model', 'providers_master_model', 'providers_utilities_model'));
        $this->load->library(array('form_validation', 'bookonspot'));
        is_logged_in();
    }

    public function index() {
        $data = array();
        $data['provider_array'] = $this->providers_master_model->find_all();
        $data['service_list'] = $this->service_master_model->where(array('status' => '1'))->find_all();
        // show($data['service_list'], 1);
        load_back_view(SERVICE_VIEW, $data);
    }

    /*
     * @function    : get_list
     * @param       : 
     * @detail      : all service  list.
     *                 
     */

    function get_list() {

        $this->datatables->select("sm.id,sm.service_name");
        $this->datatables->from("service_master sm");
        $this->datatables->add_column('action', '<a href="javascript:void" class="edit_btn margin" ref="$1"> <i  class="glyphicon glyphicon-pencil"></i> </a>', 'id');
        $data = $this->datatables->generate('json');
        echo $data;
    }

    public function create_service() {
        $data['provider_array'] = $this->providers_master_model->find_all();

        // show($data, 1);
        load_back_view(CREATE_SERVICE_VIEW, $data);
    }

    public function is_service_exists() {
        $input = $this->input->post();
        $count = $this->service_master_model->where('service_name', $input['name'])->count_all();
        $flag = ($count) ? 'true' : 'false';
        echo $flag;
    }

    public function save_service() {
        $input = $this->input->post();

        $group = [
            'service_name' => $input['service_name'],
            'service_detail' => $input['service_detail'],
            'status' => '1',
            'created_date' => date('Y-m-d H:i:s'),
            'created_by' => $this->session->userdata('user_id'),
            'updated_date' => date('Y-m-d H:i:s'),
            'updated_by' => $this->session->userdata('user_id'),
        ];
        $serviceId = $this->service_master_model->insert($group);


        /* Save the mapping of providers with services - Gopal */
          $last_insert_id = $this->service_master_model->last_insert_id();
          $check_if_service_data_exist = $this->providers_utilities_model->findservices($last_insert_id);

          if(count($check_if_service_data_exist->result()) > 0){
              $this->providers_utilities_model->deleterecords($last_insert_id);
          }

          foreach ($input['providers'] as $key => $provider) {
            $provider_data['provider_id'] = $provider; 
            $provider_data['utilities_id'] = $last_insert_id; 
            $provider_data['created_at'] = date_format(date_create(), 'Y-m-d h:i:s');

             $this->providers_utilities_model->insert($provider_data);
          }

        $this->session->set_flashdata('msg_type', 'success');
        $this->session->set_flashdata('msg', 'Service Added Sucessfully.');

        redirect(site_url() . '/admin/service');
    }

    /*
     * @function    : edit_service
     * @param       : $id
     * @detail      : edit service.
     *                 
     */

    public function edit_service() {
        $input = $this->input->post();
        $provider_selected_array = [];
        $this->service_master_model->id = $input["id"];
        $servicedata = $this->service_master_model->select();
        
        $this->providers_utilities_model->utilities_id = $input['id'];
        $mapped_data = $this->providers_utilities_model->select();
        foreach ($mapped_data as $key => $value) {
          $provider_selected_array[] = $value->provider_id;
        }

        if (count($servicedata) > 0) {
            $data["flag"] = '@#success#@';
            $data["service_data"] = $servicedata;
            $data['provider_selected_array'] = $provider_selected_array;

        } else {
            $data["flag"] = '@#failed#@';
        }


        data2json($data);
    }

    /**
     * [update_service description] : update the service name or service detail
     * @return [type] [description]
     * @author mihir shah
     */
    public function update_service() {
        $input = $this->input->post();

        $config = array(
            array('field' => 'service_name', 'label' => 'Name', 'rules' => 'trim|required', 'errors' => array('required' => 'Please enter Service name.')),
        );

        if (form_validate_rules($config) == FALSE) {

            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', 'Service not updated,kindly do again.');

            redirect(base_url() . 'admin/service');
        } else {
            $update_array = array(
                "service_name" => $input['service_name'],
                "service_detail" => $input['service_detail'],
                'updated_date' => date('Y-m-d H:i:s'),
                'updated_by' => $this->session->userdata('user_id'),
            );

            $update = $this->service_master_model->update_where('id', $input['id'], $update_array);

            /* Save the mapping of providers with services - Gopal */
            $check_if_service_data_exist = $this->providers_utilities_model->findservices($input['id']);
            
            if(count($check_if_service_data_exist->result()) > 0){
                $this->providers_utilities_model->deleterecords($input['id']);
            }

            foreach ($input['providers'] as $key => $provider) {
              $provider_data['provider_id'] = $provider; 
              $provider_data['utilities_id'] = $input['id']; 
              $provider_data['created_at'] = date_format(date_create(), 'Y-m-d h:i:s');

               $this->providers_utilities_model->insert($provider_data);
            }

            $this->session->set_flashdata('msg_type', 'success');
            $this->session->set_flashdata('msg', 'Service updated successfully');

            redirect(base_url() . 'admin/service');
        }
    }

    public function redirect_commission() {
        $input = $this->input->post();
        $library = trim($input['library']);
        if (!empty($library)) {
            $this->{$library}->commission_redirect();
        }
    }

}
