<?php

class Tso extends CI_Controller {

    public $sessionData;

    public function __construct() {

        parent::__construct();
        is_logged_in();
        $this->load->model(array("Tso_model"));
        $this->load->library('form_validation');
        $this->load->helper('tso_helper');

        //$this->load->model(array('services/service_5/service_5_commission_detail_model', 'retailer_service_mapping_model', 'assign_package_log_model', 'package_detail_model', 'cb_msrtc_agent_transaction_model', 'tmp_agent_balance_track_model', 'cron/cron_model', 'payid_summary_details_model', 'payout_summary_details_model', 'payment_gateway_transaction_model', 'master_services_model', 'travel_destinations_model', 'tickets_model', 'api_balance_log_model', 'wallet_model', 'wallet_trans_model', 'wallet_topup_model', 'users_model', 'wallet_trans_log_model', 'common_model', 'commission_calculation_model', 'cb_commission_distribution_model'));
        
        $this->sessionData['id'] = $this->session->userdata('user_id');
        $this->sessionData["username"] = $this->session->userdata('username');
        $this->sessionData["password"] = $this->session->userdata('password');
        $this->header = array(
            'Content-Type: multipart/form-data; charset=utf-8',
            'X-API-KEY: ' . X_API_KEY . '',
        );

    }

    public function index() {
        
        $data['type'] = 'TSO';
        $data['pre_operators'] = json_decode(TSO_PRE_MOBILE_OPERATORS, true);
        $data['post_operators'] = json_decode(TSO_POST_MOBILE_OPERATORS, true);
        
        load_back_view(TSO_PREPAID_RECHARGE_VIEW, $data);
        
    }
    
    public function recharge() {

        $data = array();
        $input = $this->input->post();
        
        if (!empty($input)) {
            
            $input['user_id'] = $this->sessionData['id'];

            $config = array(
                array('field' => 'plan_type', 'label' => 'Recharge Plan Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select recharge plan type.')),
                //array('field' => 'mobile_operator', 'label' => 'Mobile Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select mobile operator.')),
                array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.')),
                array('field' => 'recharge_amount', 'label' => 'Recharge Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter recharge amount.')),
                array('field' => 'recharge_type', 'label' => 'Recharge Type', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please select recharge type.'))
            );

            if (form_validate_rules($config)) {
                $curl_url = base_url() . "rest_server/Tsoapi/recharge";
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'TSO_recharge.log', $input, 'Input-Operator');
                $curlResult = curlForPostDataTSO($curl_url, $input, $this->sessionData, $this->header);
                $output = json_decode($curlResult);
                // show($output->msg, 1);
                $this->session->set_flashdata('message', $output->msg);
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'TSO_recharge.log', $curlResult, 'TSO Recharge Response Final');                
            }
        }
        redirect(base_url() . 'admin/tso');
    }

}
