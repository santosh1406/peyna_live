<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fund_request extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		is_logged_in();
		$this->load->model(array('fund_request_model','common_model','users_model', 'payment_transaction_fss_model','wallet_topup_model'));
        $this->load->library('form_validation');
		$this->load->library('cca_payment_gateway');
    }
    
    public function index()
	{	   
       $data = array();
       $data['user_data']  = $this->session->userdata();
	   $data['user_type']  = $data['user_data']['role_name'];
	   $RollID = $this->session->userdata("role_id");
	   $UserID = $this->session->userdata("user_id");
	   load_back_view(BACK_FUND_REQUEST_LIST, $data);             
    }
	
	public function create_new()
	{	   
       $data = array();
       $data['user_data']  = $this->session->userdata();
	   $data['user_type']  = $data['user_data']['role_name'];
	   $RollID = $this->session->userdata("role_id");
	   $UserID = $this->session->userdata("user_id");
	   $LvlId  = 1;
	   $data['Pay_Trans_Mode'] = $this->fund_request_model->getPaymentTransferMode();
	   $data['CompanyOwner'] = $this->common_model->get_value('users','id,first_name,last_name','level='.$LvlId);
	   if($RollID == COMPANY_ROLE_ID){	   	
	   	$data['CompanyOwner'] = $this->common_model->get_value('users','id,first_name,last_name','id=1');		 
	   	$data['AboveLevel'] = $this->common_model->get_value('users','id,first_name,last_name','id=2');		 
	   }else if($RollID == MASTER_DISTRIBUTOR_ROLE_ID){
		 $Level =  $this->common_model->get_value('users','id,level_2','id='.$UserID);
         $data['AboveLevel'] = $this->common_model->get_value('users','id,first_name,last_name','id='.$Level->level_2);		 
	   } else if($RollID == AREA_DISTRIBUTOR_ROLE_ID){
		 $Level =  $this->common_model->get_value('users','id,level_3','id='.$UserID);
         $data['AboveLevel'] = $this->common_model->get_value('users','id,first_name,last_name','id='.$Level->level_3);  
	   } else if($RollID == DISTRIBUTOR){
		 $Level =  $this->common_model->get_value('users','id,level_4','id='.$UserID);
         $data['AboveLevel'] = $this->common_model->get_value('users','id,first_name,last_name','id='.$Level->level_4); 
	   } else if($RollID == RETAILER_ROLE_ID){
		 $Level =  $this->common_model->get_value('users','id,level_5','id='.$UserID);
		 $data['AboveLevel'] = $this->common_model->get_value('users','id,first_name,last_name','id='.$Level->level_5);  
	   }
	   
       load_back_view(BACK_FUND_REQUEST_CREATE, $data);             
    }
	
	/** Save Function For Funds Request */
	public function save_fund_request() 
	{
	   $input = $this->input->post(NULL,TRUE);
        
        $config = array(
            array('field' => 'request_to', 'label' => 'Request To', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please Select Request To.')),
            array('field' => 'amount', 'label' => 'Amount', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please Enter Amount.')),
			array('field' => 'transanction_mode', 'label' => 'Transfer Mode', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please Select Transfer Mode.'))
        );		
        
        if (form_validate_rules($config)) {
                $amount = intval($input['amount']);
                   if($amount <= 0){
					    $data["flag"] = '@#failed#@';
						$data["msg_type"] = 'error';
						$data["msg"] = 'Amount Should Be Greater Than 0';  
				}else{
                $UserID = $this->session->userdata("user_id");
				
				$now = date('Y-m-d H:i:s', time());
				$OwnAvaiableBalance = $this->common_model->get_value('wallet','id,amt','user_id='.$UserID);
				$NowTotalBalance = $OwnAvaiableBalance->amt + $input['amount'];
				
				if($NowTotalBalance > WALLET_LIMIT){
				 $data["flag"] = '@#failed#@';
                 $data["msg"] = 'Sorry, Balance Request can not be processed as you are exceeding wallet balance limit.';				 
				} else {
				
					$TransMode =  $this->common_model->get_value('payment_transfer_mode','mode','id='.$input['transanction_mode']);
					
					$data = array(
					'user_id'	=> $UserID,
					'request_touserid'	=> $input['request_to'], 
					'amount'	        => $input['amount'], 
					'topup_by'	        => $TransMode->mode,
					'transaction_no'    => $input['transanction_ref'],
					'bank_name'	        => $input['bank_name'], 
					'bank_acc_no'	    => $input['bank_account_no'],
					'transaction_date'	=> $now,				
					'transaction_remark'=> $input['remark'], 
					'is_approvel'	    => 'NA',
					'is_status'	        => 'Y',
					'created_by'	    => $UserID,
					'created_date'      => $now);
					
					$id = $this->common_model->insert('wallet_topup', $data);
					log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $input, "User Enters data");
                    log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $data, "Data Added In to Wallet Table");
					if ($id > 0) {
						
						$data["flag"] = '@#success#@';
						$data["msg_type"] = 'success';
						$data["msg"] = 'Balance Request Added successfully.';						
												
						// send email						
						$firstname = $this->session->userdata('first_name');
						$lastname = $this->session->userdata('last_name');
						$ToEmail =  $this->common_model->get_value('users','email','id='.$input['request_to']);
						$ToMobile =  $this->common_model->get_value('users','mobile_no','id='.$input['request_to']);
						$ToName =  $this->common_model->get_value('users','first_name,last_name','id='.$input['request_to']);
						$mail_replacement_array = array(
							"{{topup_by}}" => $TransMode->mode,
							"{{amount}}" => round($input['amount'],2),
							"{{bank_name}}" => $input['bank_name'],
							"{{bank_acc_no}}" => $input['bank_account_no'],
							"{{transaction_no}}" => $input['transanction_ref'],
							"{{transaction_remark}}" => $input['remark'],
							"{{username}}" => $firstname . ' ' . $lastname,
							"{{useremail}}" => $this->session->userdata('email'),
							"{{transaction_date}}" => date('d-m-Y H:i:s', strtotime($now)),
							"{{is_approvel}}" => 'Pending'
						);
						//for admin and not for Retailer
						$data['mail_content'] = str_replace(array_keys($mail_replacement_array), array_values($mail_replacement_array), WALLET_TOPUP_REQUEST);

						$mailbody_array = array("subject" => "Wallet Topup Request of Rs " .round($input['amount'],2). " by " . $firstname . ' ' . $lastname . " ",
							"message" => $data['mail_content'],
							"to" => rokad_decrypt($ToEmail->email,$this->config->item('pos_encryption_key'))
						);						
						
						$mail_result = send_mail($mailbody_array);
						log_data('log1.log',$mail_result,"mail_result");
						if ($mail_result) {
							$data1['flag'] = "@#success#@";
						}

						//ADMIN sms
						$flag = true;
						if ($flag == true) {
							$sms_array = array (
											"{{request_to_username}}" => $ToName->first_name.' '.$ToName->last_name,
											"{{request_by_role_name}}" => $this->session->userdata('role_name'),
											"{{request_by_username}}" => $firstname . ' ' . $lastname,
											"{{amount}}" => round($input['amount'],2)

											);
							$msg_to_sent = str_replace(array_keys($sms_array), array_values($sms_array), WALLET_TOPUP_REQUEST_TO);
							$is_send = sendSMS(rokad_decrypt($ToMobile->mobile_no,$this->config->item('pos_encryption_key')), $msg_to_sent);
							if ($is_send) {
								$data1['sms'] = "@#success#@";
							}
						}						
						
					} else {
						$data["flag"] = '@#failed#@';
						$data["msg_type"] = 'error';
						$data["msg"] = 'Balance Request could not be Added.';						
					}
                }
                }
        } else {
            $data["flag"] = '@#failed#@';
            $data["msg_type"] = 'error';
            $data["msg"] = 'Balance Request could not be added/updated without All * Neccessary Data Not Fill.';
		}		
		data2json($data);
	}
	
	/** View Function For Funds Request */
	public function view_fund_request($id) {
		$id= base64_decode($id);
        $FundRequestData = $this->fund_request_model->getFundRequestData($id);
		$Request = $this->common_model->get_value('users','first_name,last_name','id='.$FundRequestData->request_touserid);
		$RequestName = $Request->first_name.' '.$Request->last_name;
		$RequestBy = $this->common_model->get_value('users','first_name,last_name','id='.$FundRequestData->user_id);
		$RequestByName = $RequestBy->first_name.' '.$RequestBy->last_name;
        $data["RequestBy"] = $RequestByName;
        $data["RequestTo"] = $RequestName;
        $data["FundRequest"] = $FundRequestData;
        load_back_view(BACK_FUND_REQUEST_VIEW, $data);
        //data2json($data);
    }
	
    /** Get List For Funds Request */
    public function DataTableFundRequest() {
		
        $aColumns = array('id','user_id','request_touserid','created_date', 'amount','topup_by', 'transaction_remark','is_approvel', 'id');

        $DataTableArray = getDefinedDataTable($aColumns);
        $sWhere = $DataTableArray['sWhere'];
        $sOrder = $DataTableArray['sOrder'];
        $sLimit = $DataTableArray['sLimit'];
		
		$RollID = $this->session->userdata("role_id");
		$UserID = $this->session->userdata("user_id");
        
		if($RollID == COMPANY_ROLE_ID){
			if($UserID !="")
			{
				if($sWhere<>''){
					$sWhere .= " WHERE user_id  = '".$UserID."'";
				}else{
					$sWhere .= " WHERE user_id  = '".$UserID."'";
				}
			}
		} else if($RollID == MASTER_DISTRIBUTOR_ROLE_ID ){
		if($UserID !="")
        {            
           if($sWhere<>''){
            $sWhere .= " WHERE user_id  = '".$UserID."'"; 
           }else{
               $sWhere .= " WHERE user_id  = '".$UserID."'"; 
           }                                    
        }	
		} else if($RollID == AREA_DISTRIBUTOR_ROLE_ID ){
		if($UserID !="")
        {            
           if($sWhere<>''){
            $sWhere .= " WHERE user_id  = '".$UserID."'"; 
           }else{
               $sWhere .= " WHERE user_id  = '".$UserID."'"; 
           }                                    
        }
		} else if($RollID == DISTRIBUTOR ){
		if($UserID !="")
        {            
           if($sWhere<>''){
            $sWhere .= " WHERE user_id  = '".$UserID."'"; 
           }else{
               $sWhere .= " WHERE user_id  = '".$UserID."'"; 
           }                                    
        }
		} else if($RollID == RETAILER_ROLE_ID ){
		if($UserID !="")
        {            
           if($sWhere<>''){
            $sWhere .= " WHERE user_id  = '".$UserID."'"; 
           }else{
               $sWhere .= " WHERE user_id  = '".$UserID."'"; 
           }                                    
        }	
        }
				        
        $DataTableArray = $this->fund_request_model->LoadFundDataTable($sWhere, $sOrder, $sLimit);
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $DataTableArray['iTotal'],
            "iTotalDisplayRecords" => $DataTableArray['iTotal'],
            "aaData" => array()
        );
        $aHeader = array('id','user_id','request_touserid','Date','amount', 'topup_by', 'transaction_remark', 'is_approvel', 'Actions');
        $site_url = base_url();
        
        foreach ($DataTableArray['ResultSet'] as $aRow) {
            $row = array();
            $TotalHeader = count($aHeader);

            for ($i = 0; $i < $TotalHeader; $i++) {
                if ($aHeader[$i] == "user_id") {
                    $GetName = $this->common_model->get_value('users','first_name,last_name','id='.$aRow['user_id']);
					$FullNameReqBy = $GetName->first_name.' '.$GetName->last_name;
                    $row[] = htmlspecialchars($FullNameReqBy, ENT_QUOTES);
                } else if ($aHeader[$i] == "request_touserid") {
					if($aRow['topup_by'] =='PG'){
					$GetName = $this->common_model->get_value('users','first_name,last_name','id='.$aRow['user_id']);
					$FullNameReqTo = $GetName->first_name.' '.$GetName->last_name;	
					}else{
                    $GetName = $this->common_model->get_value('users','first_name,last_name','id='.$aRow['request_touserid']);
					$FullNameReqTo = $GetName->first_name.' '.$GetName->last_name;
					}
                    $row[] = htmlspecialchars($FullNameReqTo, ENT_QUOTES);
                } else if ($aHeader[$i] == "amount") {
					$Amnt = (string)$aRow['amount'];
					$Amt =strpos($Amnt,'.');
					$Amount =substr($Amnt,0,$Amt+3);
					$row[] = htmlspecialchars($Amount, ENT_QUOTES);
                }  else if ($aHeader[$i] == "topup_by") {
					$TopUpBy = $aRow['topup_by'];
					$row[] = htmlspecialchars($TopUpBy, ENT_QUOTES);
                } else if ($aHeader[$i] == "transaction_remark") {
					$TransRemark = $aRow['transaction_remark'];
					$row[] = htmlspecialchars($TransRemark, ENT_QUOTES);
                } else if ($aHeader[$i] == "is_approvel") {
					$TopupStatus = $aRow['is_approvel'];
					if($TopupStatus =='A'){
					$Tstatus =	'Accepted';
					}elseif($TopupStatus =='R'){
					$Tstatus =	'Rejected';	
					}elseif($aRow['topup_by'] =='PG' && ($TopupStatus == 'NA' or $TopupStatus == 'aborted' or $TopupStatus == 'pfailed')){
					$Tstatus =	'Failed';	
					} else {	
					$Tstatus =	'Pending';	
					}
					$row[] = htmlspecialchars($Tstatus, ENT_QUOTES);
                } else if ($aHeader[$i] == "Actions") {
                    $Action = '<a style="padding: 0 4px;" class="pull-left view_btn" href="'.$site_url.'admin/fund_request/view_fund_request/'.base64_encode($aRow['id']).'" ref="'.$aRow['id'].'" rel="tooltip" data-placement="top" data-original-title="View" title="Request View"><i class="fa fa-eye" style="font-size:18px;color:#337ab7;"></i></a>&nbsp;&nbsp;&nbsp;';
                    $row[] = $Action;
                } else if ($aHeader[$i] != ' ') {
                    /* General output */
                    $row[] = $aRow[$aHeader[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode($output);
    }
	
	/** Load Funds Request Approval Page*/
	public function fund_request_approval()
	{
       $data = array();
       $data['user_data']  = $this->session->userdata();
       $data['user_type']  = $data['user_data']['role_name'];
	   load_back_view(BACK_FUND_REQUEST_APPROVAL_LIST, $data);             
    }
	
    /** Get List For Funds Request Approval*/
    public function DataTableFundRequestApproval() {
		
        $aColumns = array('id', 'user_id','request_touserid','created_date', 'amount','topup_by', 'transaction_remark','is_approvel', 'a.id');

        $DataTableArray = getDefinedDataTable($aColumns);
        $sWhere = $DataTableArray['sWhere'];
        $sOrder = $DataTableArray['sOrder'];
        $sLimit = $DataTableArray['sLimit'];
		
		$RollID = $this->session->userdata("role_id");
		$UserID = $this->session->userdata("user_id");
		
		if($RollID == MASTER_DISTRIBUTOR_ROLE_ID ){
		if($UserID !="")
        {            
           if($sWhere<>''){
            $sWhere .= " WHERE request_touserid  = '".$UserID."'"; 
           }else{
               $sWhere .= " WHERE request_touserid  = '".$UserID."'"; 
           }                                    
        }
		} else if($RollID == AREA_DISTRIBUTOR_ROLE_ID ){
		if($UserID !="")
        {            
           if($sWhere<>''){
            $sWhere .= " WHERE request_touserid  = '".$UserID."'"; 
           }else{
               $sWhere .= " WHERE request_touserid  = '".$UserID."'"; 
           }                                    
        }
		} else if($RollID == DISTRIBUTOR ){
		if($UserID !="")
        {            
           if($sWhere<>''){
            $sWhere .= " WHERE request_touserid  = '".$UserID."'"; 
           }else{
               $sWhere .= " WHERE request_touserid  = '".$UserID."'"; 
           }                                    
        }	
        } else {
			
			if($sWhere<>''){
              $sWhere .= " WHERE payment_transaction_id IS NULL"; 
           }else{
              $sWhere .= " WHERE payment_transaction_id IS NULL"; 
           } 
		}
		
		$RequestBy = (isset($_GET['RequestBy']) ? $_GET['RequestBy'] :'');
        if($RequestBy !="")
        {            
           if($sWhere<>''){
            $sWhere .= " AND user_id = '".$RequestBy."'"; 
           }else{
               $sWhere .= " WHERE user_id = '".$RequestBy."'"; 
           }                                    
        }
       
        $status = (isset($_GET['status']) ? $_GET['status'] :'');
        if($status !="")
        {            
           if($sWhere<>''){
            $sWhere .= " AND is_approvel = '".$status."'"; 
           }else{
               $sWhere .= " WHERE is_approvel = '".$status."'"; 
           }                                    
        }
		
		$DataTableArray = $this->fund_request_model->LoadFundApprovalDataTable($sWhere, $sOrder, $sLimit);
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $DataTableArray['iTotal'],
            "iTotalDisplayRecords" => $DataTableArray['iTotal'],
            "aaData" => array()
        );
        $aHeader = array('id', 'user_id','request_touserid','Date','amount', 'topup_by', 'transaction_remark', 'is_approvel', 'Actions');
        $site_url = base_url();
        
        foreach ($DataTableArray['ResultSet'] as $aRow) {
            $row = array();
            $TotalHeader = count($aHeader);

            for ($i = 0; $i < $TotalHeader; $i++) {
                if ($aHeader[$i] == "user_id") {
                    $GetName = $this->common_model->get_value('users','first_name,last_name','id='.$aRow['user_id']);
					$FullName = $GetName->first_name.' '.$GetName->last_name;
                    $row[] = htmlspecialchars($FullName, ENT_QUOTES);
                } else if ($aHeader[$i] == "request_touserid") {
                    $GetName = $this->common_model->get_value('users','first_name,last_name','id='.$aRow['request_touserid']);
					$FullNameReqTo = $GetName->first_name.' '.$GetName->last_name;
                    $row[] = htmlspecialchars($FullNameReqTo, ENT_QUOTES);
                } else if ($aHeader[$i] == "amount") {
					$Amnt = (string)$aRow['amount'];
					$Amt =strpos($Amnt,'.');
					$Amount =substr($Amnt,0,$Amt+3);
					$row[] = htmlspecialchars($Amount, ENT_QUOTES);
                } else if ($aHeader[$i] == "topup_by") {
					$TopUpBy = $aRow['topup_by'];
					$row[] = htmlspecialchars($TopUpBy, ENT_QUOTES);
                } else if ($aHeader[$i] == "transaction_remark") {
					$TransRemark = $aRow['transaction_remark'];
					$row[] = htmlspecialchars($TransRemark, ENT_QUOTES);
                } else if ($aHeader[$i] == "is_approvel") {
					$TopupStatus = $aRow['is_approvel'];
					if($TopupStatus =='A'){
					$Tstatus =	'Accepted';
					}elseif($TopupStatus =='R'){
					$Tstatus =	'Rejected';	
					}else{
					$Tstatus =	'Pending';	
					}
					$row[] = htmlspecialchars($Tstatus, ENT_QUOTES);
                } else if ($aHeader[$i] == "Actions") {
					if($aRow['request_touserid'] == $UserID){
					if($aRow['is_approvel'] == 'A' or $aRow['is_approvel'] == 'R'){ 
					$Action = '<a style="padding: 0 4px;" class="pull-left" href="'.$site_url.'admin/fund_request/view_fund_request_approval/'.base64_encode($aRow['id']).'" ref="'.$aRow['id'].'" rel="tooltip" data-placement="top" data-original-title="View" title="Approval View"><i class="fa fa-eye" style="font-size:18px;color:#337ab7;"></i></a>&nbsp;&nbsp;&nbsp;';
					}else {
					$Action = '<a style="padding: 0 4px;" class="pull-left" href="'.$site_url.'admin/fund_request/edit_fund_request_approval/'.base64_encode($aRow['id']).'" ref="'.$aRow['id'].'" rel="tooltip" data-placement="top" data-original-title="Edit" title="Approval Edit"><i class="fa fa-edit" style="font-size:18px;color:#337ab7;"></i></a>&nbsp;&nbsp;&nbsp;';	
					}
                   	} else{
					if($aRow['is_approvel'] == 'A' or $aRow['is_approvel'] == 'R' or $aRow['is_approvel'] == 'NA'){ 
					$Action = '<a style="padding: 0 4px;" class="pull-left" href="'.$site_url.'admin/fund_request/view_fund_request_approval/'.base64_encode($aRow['id']).'" ref="'.$aRow['id'].'" rel="tooltip" data-placement="top" data-original-title="View" title="Approval View"><i class="fa fa-eye" style="font-size:18px;color:#337ab7;"></i></a>&nbsp;&nbsp;&nbsp;';
					}	
					}
					$row[] = $Action;
                } else if ($aHeader[$i] != ' ') {
                    /* General output */
                    $row[] = $aRow[$aHeader[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode($output);
    }

	/** Edit Function For Funds Request Approval*/
	public function edit_fund_request_approval($id) {
		$id= base64_decode($id);
		$data['user_data']  = $this->session->userdata();
		$FundRequestData = $this->fund_request_model->getFundRequestApprovalData($id);
        $Request = $this->common_model->get_value('users','first_name,last_name','id='.$FundRequestData->user_id);
		$RequestName = $Request->first_name.' '.$Request->last_name;
        $data["RequestBy"] = $RequestName;
        $data["FundRequest"] = $FundRequestData;
        load_back_view(BACK_FUND_REQUEST_APPROVAL, $data); 
    }
	
	/** View Function For Funds Request Approval*/
	public function view_fund_request_approval($id) {
		$id= base64_decode($id); 
		$FundRequestData = $this->fund_request_model->getFundRequestApprovalData($id);
        $Request = $this->common_model->get_value('users','first_name,last_name','id='.$FundRequestData->user_id);
		$RequestName = $Request->first_name.' '.$Request->last_name;
        $data["RequestBy"] = $RequestName;
        $data["FundRequest"] = $FundRequestData;
        load_back_view(BACK_FUND_REQUEST_APPROVAL_VIEW, $data); 
    }	

	/** Save Function For Funds Request */
	public function save_fund_request_approval() 
	{
	   $input = $this->input->post(NULL,TRUE);
	   $fundRequest_id = $input['fundRequest_id'];
	   $config = array(
            array('field' => 'request_status', 'label' => 'Status', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please Select Status.'))
        );
		
        if (form_validate_rules($config)) {
			    $AprovalStatus = $this->common_model->get_value('wallet_topup','is_approvel,amount,request_touserid,user_id','id='.$fundRequest_id);
				$requested_by = $AprovalStatus->user_id;
				$request_to = $AprovalStatus->request_touserid;
				$WalletAmountRequestTo = $this->common_model->get_value('wallet','amt','user_id='.$request_to);
				if($input['request_status'] == 'A'){
				if($AprovalStatus->amount > $WalletAmountRequestTo->amt){
					 $this->session->set_flashdata('msg_type', 'success');
                     $this->session->set_flashdata('msg', 'Your Balance is less than requested Amount.');
                     redirect(base_url().'admin/fund_request/edit_fund_request_approval/'.base64_encode($fundRequest_id));
					
				}else{
					
					
					if($AprovalStatus->is_approvel == 'NA'){
					$UserID = $this->session->userdata("user_id");
					
					$now = date('Y-m-d H:i:s', time());
					
					$data = array(
					'is_status'    => 'Y',
					'is_approvel'  => 'A',
					'app_rej_by'   => $UserID,
					'app_rej_date' => $now,
					'updated_date' => $now);
				    $id = $this->common_model->update('wallet_topup','id',$fundRequest_id, $data);
					
					log_data("topup/" . date("M") . "/" . date("d") . "/topup_accepted_log.log", $data, "Accepted By data");
                    
					// For Credit Insertion OF Users
					
					$RequestBy = $this->common_model->get_value('wallet','amt,id','user_id='.$requested_by);
					$AmntAfterTransfer = $RequestBy->amt + $AprovalStatus->amount;
					$transaction_no = substr(hexdec(uniqid()), 4, 12);
					$creditdata = array(
						'transaction_no'      => $transaction_no,
						'w_id'	              => $RequestBy->id,
						'amt'	              => $AprovalStatus->amount,
						'user_id'	          => $requested_by,
						'amt_before_trans'    => $RequestBy->amt,	 
						'amt_after_trans'	  => $AmntAfterTransfer ,
						'topup_id'	          => $fundRequest_id,
						'status'              => 'added',
						'transaction_type'	  => 'Credited',
						'comment'	          => 'Top Up Request Accept',
						'is_status'	          => 'Y',
						'transaction_type_id' => 14,
						'added_on'            => $now, 
						'added_on'            => $now, 
						'added_by'            => $UserID);
				    $CreditIdBy = $this->common_model->insert('wallet_trans',$creditdata);
					log_data("topup/" . date("M") . "/" . date("d") . "/topup_accepted_log.log", $creditdata, "Credit In to Wallet Trans Table");
					
					if($CreditIdBy){
						$data = array(
							'amt'	             => $AmntAfterTransfer,
							'updated_by'         => $UserID,
							'last_transction_id' => $CreditIdBy,
							'updated_date'       => $now);
				        $UpdateId = $this->common_model->update('wallet','user_id',$requested_by,$data);	
					}
					
					// For Debit Insertion OF Users
					
					$RequestTo = $this->common_model->get_value('wallet','id,amt','user_id='.$request_to);
					$AmntAfterTransferTo = $RequestTo->amt - $AprovalStatus->amount;
					$debit_transaction_no = substr(hexdec(uniqid()), 4, 12);
					$debitdata = array(
					    'transaction_no'      => $debit_transaction_no,
					    'w_id'	              => $RequestTo->id,
						'amt'	              => $AprovalStatus->amount,
						'user_id'	          => $request_to,
						'amt_before_trans'    => $RequestTo->amt,	 
						'amt_after_trans'	  => $AmntAfterTransferTo ,
						'topup_id'	          => $fundRequest_id,
						'status'              => 'deduct',
						'transaction_type'	  => 'Debited',
						'comment'	          => 'Top Up Request Accept',
						'is_status'	          => 'Y',
						'transaction_type_id' => 14,
						'added_on'            => $now, 
						'added_by'            => $UserID);
						
				    $DebitIdTo = $this->common_model->insert('wallet_trans',$debitdata);
					log_data("topup/" . date("M") . "/" . date("d") . "/topup_accepted_log.log", $debitdata, "Debit In to Wallet Trans Table");
					if($DebitIdTo){
						$data = array(
							'amt'	             => $AmntAfterTransferTo,
							'updated_by'         => $UserID,
							'last_transction_id' => $DebitIdTo,
							'updated_date'       => $now);
				        $UpdateId = $this->common_model->update('wallet','user_id',$request_to,$data);	
					}
					
					//send email and sms to agent and admin once accepted
                    $result_accepted        = $this->get_topup_emaildata($fundRequest_id);					
					
					$ToEmail =  $this->common_model->get_value('users','email','id='.$result_accepted['request_touserid']);
					$ToMobile =  $this->common_model->get_value('users','mobile_no,first_name,last_name','id='.$result_accepted['request_touserid']);
					$mail_replacement_array =   array (
                                                "{{topup_by}}"                         => $result_accepted['topup_by'],
                                                "{{amount}}"                           => round($result_accepted['amount'],2),
                                                "{{bank_name}}"                        => $result_accepted['bank_name'],
                                                "{{bank_acc_no}}"                      => $result_accepted['bank_acc_no'],
                                                "{{transaction_no}}"                   => $result_accepted['transaction_no'],
                                                "{{transaction_remark}}"               => $result_accepted['transaction_remark'],
                                                "{{username}}"                         => $result_accepted['agent_name'],
                                                "{{useremail}}"                        => rokad_decrypt($result_accepted['email'],$this->config->item('pos_encryption_key')),
                                                "{{transaction_date}}"                 => date('d-m-Y H:i:s',strtotime($result_accepted['transaction_date'])),
                                                "{{is_approvel}}"                      => $result_accepted['is_approvel']
                                            ); 
                    $data['mail_content']       =   str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),WALLET_TOPUP_REQUEST);
                    
					// for requested to 
					$mailbody_array_admin       =   array( "subject"  => "Approved TopUp Request of Rs ".round($result_accepted['amount'],2)." for  ".$result_accepted['agent_name']." ",
                                                                       "message"  => $data['mail_content'],
                                                                       "to"       => rokad_decrypt($ToEmail->email,$this->config->item('pos_encryption_key'))
                                                                );
                    $mail_result_admin          =   send_mail($mailbody_array_admin);					
					
					// for requested by
					$mailbody_array_agent       =   array(  "subject" => "Approved TopUp Request of Rs ".round($result_accepted['amount'],2)." ",
															"message" => $data['mail_content'],
															"to"      => rokad_decrypt($result_accepted['email'],$this->config->item('pos_encryption_key'))
														);
					$mail_result_agent          = send_mail($mailbody_array_agent);					
					
					$mail_replacement_arraysms =   array(
															"{{amount}}" => $AmntAfterTransfer
														); 
					//send sms to approval by
					$flag     = true;
					if($flag == true) {
						$ApprovalByWalletAmount = $this->common_model->get_value('wallet','amt','user_id='.$result_accepted['request_touserid']);
                    	$sms_approval_by_array = array (
                    							"{{username}}" => $ToMobile->first_name.' '.$ToMobile->last_name,
                    							"{{transaction_amount}}" => round($result_accepted['amount'],2),
                    							"{{transaction_no}}" => $debit_transaction_no,
                    							"{{wallet_balance}}" => round($ApprovalByWalletAmount->amt,2)
                    							);

                        $msg_to_sent_admin = str_replace(array_keys($sms_approval_by_array),array_values($sms_approval_by_array),WALLET_TOPUP_APPROVAL_BY);
                        $is_send     = sendSMS(rokad_decrypt($ToMobile->mobile_no,$this->config->item('pos_encryption_key')), $msg_to_sent_admin);
                        if($is_send) {
                           $data1['sms']       = "@#success#@";
                        }
					}
					//send sms to approval to        
					$flags     = true;
					if($flags == true) {
						$ApprovalToWalletAmount = $this->common_model->get_value('wallet','amt','user_id='.$result_accepted['user_id']);
                    	$sms_approval_to_array = array (
                    							"{{username}}" => $result_accepted['agent_name'],
                    							"{{accept_reject}}" => 'Accepted',
                    							"{{wallet_balance}}" => round($ApprovalToWalletAmount->amt,2)
                    							);
                        $msg_to_sent_admin = str_replace(array_keys($sms_approval_to_array),array_values($sms_approval_to_array),WALLET_TOPUP_APPROVAL_TO);
                        $is_send           = sendSMS(rokad_decrypt($result_accepted['mobile_no'],$this->config->item('pos_encryption_key')), $msg_to_sent_admin);
                        if($is_send) {
                           $data1['sms_admin']       = "@#success#@";
                        }
					}
										
					$this->session->set_flashdata('msg_type', 'success');
                    $this->session->set_flashdata('msg', 'Balance request has been approved.');
                    redirect(base_url().'admin/fund_request/fund_request_approval');
					}else{
					$this->session->set_flashdata('msg_type', 'success');
					$this->session->set_flashdata('msg', 'Balance Request Approval could not be added/updated because its already added/updated.');
					redirect(base_url().'admin/fund_request/edit_fund_request_approval/'.base64_encode($fundRequest_id));	
						
					}
					
				}
				} else if($input['request_status'] == 'R') {
					
					$UserID = $this->session->userdata("user_id");
					$now = date('Y-m-d H:i:s', time());
					
					$data = array(
					'is_approvel'	   => 'R',
					'reject_reason' => $input['reject_reason'],
					'app_rej_by'   => $UserID,
					'updated_by'	=> $UserID,
					'updated_date'	=> $now,
					'app_rej_date' => $now);
				    $id = $this->common_model->update('wallet_topup','id',$fundRequest_id, $data);
					
					if ($id > 0) {
						logging_data("topup/".date("M")."/".date("d")."/agent_topup_request_log.log", $data,"Request Reject Time Data");
						
						$result_accepted        = $this->get_topup_emaildata($fundRequest_id);
						$ToEmail =  $this->common_model->get_value('users','email','id='.$result_accepted['request_touserid']);
						$ToMobile =  $this->common_model->get_value('users','mobile_no,first_name,last_name','id='.$result_accepted['request_touserid']);
						$mail_replacement_array =   array (
                                        "{{topup_by}}"                         => $result_accepted['topup_by'],
                                        "{{amount}}"                           => round($result_accepted['amount'],2),
                                        "{{bank_name}}"                        => $result_accepted['bank_name'],
                                        "{{bank_acc_no}}"                      => $result_accepted['bank_acc_no'],
                                        "{{transaction_no}}"                   => $result_accepted['transaction_no'],
                                        "{{transaction_remark}}"               => $result_accepted['transaction_remark'],
                                        "{{username}}"                         => $result_accepted['agent_name'],
                                        "{{useremail}}"                        => rokad_decrypt($result_accepted['email'],$this->config->item('pos_encryption_key')),
                                        "{{transaction_date}}"                 => date('d-m-Y H:i:s',strtotime($result_accepted['transaction_date'])),
                                        "{{is_approvel}}"                      => $result_accepted['is_approvel'],
                                        "{{reject_reason}}"                    => $result_accepted['reject_reason'],
                                    ); 
						$data['mail_content']       =   str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),WALLET_TOPUP_REQUEST_REJECTED);
						
						// for requested to 
                        $mailbody_array_admin       =   array( "subject"  => "Rejected TopUp Request of Rs ".round($result_accepted['amount'],2)." for  ".$result_accepted['agent_name']." ",
                                                               "message"  => $data['mail_content'],
                                                               "to"       => rokad_decrypt($ToEmail->email,$this->config->item('pos_encryption_key'))
															);
                        $mail_result_admin          =   send_mail($mailbody_array_admin);

						// for requested by
						$mailbody_array_agent       =   array(  "subject" => "Rejected TopUp Request of Rs ".round($result_accepted['amount'],2)." ",
                                                                "message" => $data['mail_content'],
                                                                "to"      => rokad_decrypt($result_accepted['email'],$this->config->item('pos_encryption_key'))
                                                            );
                        $mail_result_agent          = send_mail($mailbody_array_agent);
						
						 //send sms to approval by
                        $flag     = true;
                        if($flag == true) {
                            $ApprovalByWalletAmount = $this->common_model->get_value('wallet','amt','user_id='.$result_accepted['request_touserid']);
                        	$sms_approval_by_array = array (
                        							"{{username}}" => $ToMobile->first_name.' '.$ToMobile->last_name,
                        							"{{accept_reject}}" => 'Rejected',
                        							"{{wallet_balance}}" => round($ApprovalByWalletAmount->amt,2)
                        							);
                            $msg_to_sent_admin = str_replace(array_keys($sms_approval_by_array),array_values($sms_approval_by_array),WALLET_TOPUP_APPROVAL_TO);
                            $is_send     = sendSMS(rokad_decrypt($ToMobile->mobile_no,$this->config->item('pos_encryption_key')), $msg_to_sent_admin);
                            if($is_send) {
                               $data1['sms']       = "@#success#@";
                            }
                        }
						
						//send sms to approval  to        
                        $flags     = true;
                        if($flags == true) {
                        	$ApprovalToWalletAmount = $this->common_model->get_value('wallet','amt','user_id='.$result_accepted['user_id']);
                        	$sms_approval_to_array = array (
                        							"{{username}}" => $result_accepted['agent_name'],
                        							"{{accept_reject}}" => 'Rejected',
                        							"{{wallet_balance}}" => round($ApprovalToWalletAmount->amt,2)
                        							);
                            $msg_to_sent_admin = str_replace(array_keys($sms_approval_to_array),array_values($sms_approval_to_array),WALLET_TOPUP_APPROVAL_TO);
                            $is_send           = sendSMS(rokad_decrypt($result_accepted['mobile_no'],$this->config->item('pos_encryption_key')), $msg_to_sent_admin);
                            if($is_send) {
                               $data1['sms_admin']       = "@#success#@";
                            }
                        }
						
						$this->session->set_flashdata('msg_type', 'success');
						$this->session->set_flashdata('msg', 'Balance request has been rejected.');
						redirect(base_url().'admin/fund_request/fund_request_approval');					
					} 
					
				}			
                
        } else {
			$this->session->set_flashdata('msg_type', 'success');
		    $this->session->set_flashdata('msg', 'Balance Request Approval could not be added/updated without All * Neccessary Data Not Fill.');
		    redirect(base_url().'admin/fund_request/edit_fund_request_approval/'.base64_encode($fundRequest_id));
			
        }
    }
	
	/** Add Direct Topup To Your Wallet Account By Ccavenue */
	public function add_wallet_money()
	{
        $input = $this->input->post(NULL,TRUE);

        $pg_amt = $input['pg_amt'];
        $config = array(
            array('field' => 'pg_amt', 'label' => 'Amount', 'rules' => 'trim|required|numeric|greater_than[0]|xss_clean', 'errors' => array('required' => 'Please enter valid Topup amount.')),
        );
        $users_data = $this->users_model
                        ->where("id", $this->session->userdata('user_id'))
                        ->as_array()->find_all();

        if (form_validate_rules($config) == FALSE) {

            $data['error'] = $this->form_validation->error_array();
            log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $input, "User Enters data");
            log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $data['error'], "form error");

            $this->session->set_flashdata('msg_type', 'error');
            $this->session->set_flashdata('msg', $data['error']['pg_amt']);
            redirect(base_url() . 'admin/fund_request/index');
            //redirect(base_url() . 'admin/fund_request/add_money_wallet');
        } else {
			$amount = intval($input['pg_amt']);
		    if($amount <= 0){
				$data["flag"] = '@#failed#@';
				$data["msg_type"] = 'error';
				$data["msg"] = 'Amount Should Be Greater Than 0';  
		    }else{
			$RollId = COMPANY_ROLE_ID;
			$RokadData = $this->common_model->get_value('users','id','role_id='.$RollId);
			$RokadWallet = $this->common_model->get_value('wallet','id,amt,threshold_limit','user_id='.$RokadData->id);
			$RokadWalletAmount = round($RokadWallet->amt,2);
			$RokadWalletThresholdLimit = $RokadWallet->threshold_limit;
			if($pg_amt > $RokadWalletAmount){ 
			    $data1["flag"] = '@#failed#@';
                $data1["msg"] = 'Sorry, Company Balance is less than requested Amount.';
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', $data1['msg']);
                redirect(base_url() . 'admin/fund_request/index');
			}else{
			
			$OwnAvaiableBalance = $this->common_model->get_value('wallet','id,amt','user_id='.$this->session->userdata('user_id'));
            $NowTotalBalance = $OwnAvaiableBalance->amt + $pg_amt;
            
            if($NowTotalBalance > WALLET_LIMIT){
                $data1["flag"] = '@#failed#@';
                $data1["msg"] = 'Sorry, Balance Request can not be processed as you are exceeding wallet balance limit.';
                $this->session->set_flashdata('msg_type', 'error');
                $this->session->set_flashdata('msg', $data1['msg']);
                redirect(base_url() . 'admin/fund_request/index');
				
            }else {
            $php_uniqid = "agw" . uniqid() . getRandomId(4);
            //$track_id = $php_uniqid . $this->session->userdata('user_id');
            $track_id = $php_uniqid;

            $top_up_pg_details = [
                'track_id' => $track_id,
                'payment_by' => 'ccavenue',
                'user_id' => $this->session->userdata('user_id'),
                'total_amount' => $pg_amt,
                'status' => "initialize",
                'ip_address' => $this->input->ip_address(),
                'user_agent' => $this->session->userdata("user_agent")
            ];

            $payment_tran_fss_id = $this->payment_transaction_fss_model->insert($top_up_pg_details);

            $wallet_topup = [
                'user_id' => $this->session->userdata('user_id'),
                'amount' => $pg_amt,
                'payment_transaction_id' => $payment_tran_fss_id,
                'transaction_status' => "pfailed",
                'payment_confirmation' => "not_done",
                /* 'pg_tracking_id' => $track_id, */
                'topup_by' => 'PG',
                'transaction_remark' => 'payment_by_online',
                'created_by' => $this->session->userdata('user_id'),
                'created_date' => date('Y-m-d h:i:s'),
                'transaction_date' => date('Y-m-d h:i:s'),
                'is_status' => 'N',
                'is_approvel' => 'NA',
            ];

            $wallet_topup_id = $this->wallet_topup_model->insert($wallet_topup);

            log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $wallet_topup, "Wallet Topup  Pg Request data");

            if ($wallet_topup_id) {
                $ReqTid = "tid=" . time();
                $ReqorderId = $track_id;
                $ReqTotalAmount = $pg_amt;
                $merchant_param1 = $payment_tran_fss_id;
                $merchant_param2 = $wallet_topup_id;
                $merchant_param3 = '';
                $merchant_param4 = "";

                $ReqMerchantOrderId = "order_id=" . $ReqorderId;
                $ReqMerchantId = "merchant_id=" . $this->config->item("cca_merchant_id_agent");
                $ReqAmount = "amount=" . $pg_amt;
                $ReqCurrency = "currency=INR";
                $ReqRedirectUrl = "redirect_url=" . base_url() . "admin/fund_request/get_cca_response";
                $ReqCancelUrl = "cancel_url=" . base_url() . "admin/fund_request/get_cca_response";
                $ReqLanguage = "language=EN";
                $ReqMerchantPara1 = "merchant_param1=" . $merchant_param1;
                $ReqMerchantPara2 = "merchant_param2=" . $merchant_param2;
                $ReqMerchantPara3 = "merchant_param3=" . $merchant_param3;
                $ReqMerchantPara4 = "merchant_param4=" . $merchant_param4;

                $Strhashs = trim($ReqMerchantOrderId) . "&" . trim($ReqAmount) . "&" . trim($ReqCurrency) . "&" . trim($ReqMerchantPara1) . "&" . trim($ReqMerchantPara2) . "&" . trim($ReqMerchantPara3) . "&" . trim($ReqMerchantPara4);

                if (isset($users_data) && count($users_data) > 0) {
                    if (isset($users_data[0]['first_name'])) {
                        $ReqBillingName = "billing_name=" . trim($users_data[0]['first_name']);
                        $Strhashs = $Strhashs . "&" . trim($ReqBillingName);
                    }

                    if (isset($users_data[0]['email'])) {
                        $ReqBillingEmail = "billing_email=" . rokad_decrypt(trim($users_data[0]['email']),$this->config->item('pos_encryption_key'));
                        $Strhashs = $Strhashs . "&" . trim($ReqBillingEmail);
                    }

                    if (isset($users_data[0]['phone']) && $users_data[0]['phone'] != '0') {
                        $ReqBillingTelNo = "billing_tel=" . trim($users_data[0]['phone']);
                        $Strhashs = $Strhashs . "&" . trim($ReqBillingTelNo);
                    }
                }

                //For geo location
                //UNCOMMENT BELOW LINE ON LIVE
                //$ip_address = $this->session->userdata("ip_address");
                //COMMENT BELOW LINE ON LIVE
                $locationdata = array();
                $ip_address = $this->input->ip_address();

                $ReqBillingCountry = "billing_country=India";
                $Strhashs = $Strhashs . "&" . trim($ReqBillingCountry);
                //AFTERWARDS DO THIS OR CHANGE LOGIC FOR merchant_param5
                //Use hash method which is defined below for Hashing ,It will return Hashed valued of above string
                $hashedstring = hash('sha256', $Strhashs);
                $ReqMerchantPara5 = "merchant_param5=" . $hashedstring;
                $Reqparam = trim($ReqTid) . "&" . trim($ReqMerchantId) . "&" . trim($ReqMerchantOrderId) . "&" . trim($ReqAmount) . "&" . trim($ReqCurrency) . "&" . trim($ReqRedirectUrl) . "&" . trim($ReqCancelUrl) . "&" . trim($ReqLanguage) . "&" . trim($ReqMerchantPara1) . "&" . trim($ReqMerchantPara2) . "&" . trim($ReqMerchantPara3) . "&" . trim($ReqMerchantPara4);
                if (isset($ReqBillingName)) {
                    $Reqparam .= "&" . $ReqBillingName;
                }

                if (isset($ReqBillingEmail)) {
                    $Reqparam .= "&" . $ReqBillingEmail;
                }

                if (isset($ReqBillingTelNo)) {
                    $Reqparam .= "&" . $ReqBillingTelNo;
                }

                $Reqparam .= "&" . $ReqBillingCountry;
                $Reqparam = $Reqparam . "&" . trim($ReqMerchantPara5) . "&";

                log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $Reqparam, "Data For Payment");

                $encrypted_data = encrypt($Reqparam, $this->config->item("cca_working_key_agent"));
                $ccprocess_data["encrypted_data"] = $encrypted_data;

                log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $encrypted_data, "Encrypted Data For Payment");

                load_front_view(AGENT_PGPROCESSING_VIEW, $ccprocess_data);
            } else {
                $custom_error_mail = array(
                    "custom_error_subject" => "Data Not Add into Wallet TopUp.",
                    "custom_error_message" => "Data Not added into wallet topup while topup from pg - " . json_encode($wallet_topup)
                );
                $this->common_model->developer_custom_error_mail($custom_error_mail);

                log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $wallet_topup, "Pg Request Time Data");

                load_front_view(UNEXPECTED_ERROR);
            }
        }                    // Wallet Limit Condition
	   }
	  }
	  }
    }

    public function get_cca_response() {
        $data = $this->cca_payment_gateway->GetTopupResponse();

        $this->session->set_flashdata('msg_type', $data["page_msg_type"]);
        $this->session->set_flashdata('msg', $data["page_msg"]);

        redirect($data["page_redirect"]);
    }
	
	public function balance_requestby_support() {
       $data = array();
       $data['user_data']  = $this->session->userdata();
	   $data['user_type']  = $data['user_data']['role_name'];
	   $RollID = $this->session->userdata("role_id");
	   $UserID = $this->session->userdata("user_id");
	   $LvlId  = 1;
	   $data['Pay_Trans_Mode'] = $this->fund_request_model->getPaymentTransferMode();
	   $data['CompanyOwner'] = $this->common_model->get_value('users','id,first_name,last_name','level='.$LvlId);
	   $data['MasterDistributor'] = $this->fund_request_model->getMasterDistributor($id=3);
	   $data['AreaDistributor'] = $this->fund_request_model->getAreaDistributor($id=4);
	   $data['Distributor'] = $this->fund_request_model->getDistributor($id=5);
	   $data['Retailer'] = $this->fund_request_model->getRetailer($id=6);
	   load_back_view(BACK_FUND_SUPPORT_REQUEST_LIST, $data);
    }
	
	public function create_requestby_support(){
	   $data = array();
       $data['user_data']  = $this->session->userdata();
	   $data['user_type']  = $data['user_data']['role_name'];
	   $RollID = $this->session->userdata("role_id");
	   $UserID = $this->session->userdata("user_id");
	   $LvlId  = 1;
	   $data['Pay_Trans_Mode'] = $this->fund_request_model->getPaymentTransferMode();
	   $data['CompanyOwner'] = $this->common_model->get_value('users','id,first_name,last_name','level='.$LvlId);
	   $data['MasterDistributor'] = $this->fund_request_model->getMasterDistributor($id=3);
	   $data['AreaDistributor'] = $this->fund_request_model->getAreaDistributor($id=4);
	   $data['Distributor'] = $this->fund_request_model->getDistributor($id=5);
	   $data['Retailer'] = $this->fund_request_model->getRetailer($id=6);
	   load_back_view(BACK_FUND_SUPPORT_REQUEST_CREATE, $data);
		
	}
	
	/** Get List For Funds Support Request */
    public function DataTableFundSupportRequest() {
		
        $aColumns = array('id','user_id','request_touserid','created_date', 'amount','topup_by', 'transaction_remark','is_approvel', 'id');

        $DataTableArray = getDefinedDataTable($aColumns);
        $sWhere = $DataTableArray['sWhere'];
        $sOrder = $DataTableArray['sOrder'];
        $sLimit = $DataTableArray['sLimit'];
		
		$RollID = $this->session->userdata("role_id");
		$UserID = $this->session->userdata("user_id");
        
		if($RollID == SUPPORT_ROLE_ID ){
		if($UserID !="")
        {            
           if($sWhere<>''){
            $sWhere .= " WHERE suprt_userid  = '".$UserID."' AND support_request=1"; 
           }else{
               $sWhere .= " WHERE suprt_userid  = '".$UserID."' AND support_request=1"; 
           }                                    
        }	
		}
        
		if($RollID == SUPERADMIN_ROLE_ID or $RollID == ADMIN_ROLE_ID or $RollID == COMPANY_ROLE_ID){
		$SupportID = 1;
        if($SupportID !="")
        {            
           if($sWhere<>''){
            $sWhere .= " WHERE support_request  = '".$SupportID."'"; 
           }else{
               $sWhere .= " WHERE support_request  = '".$SupportID."'";
           }                                    
        }		
        }		
		        
        $DataTableArray = $this->fund_request_model->LoadFundSupportDataTable($sWhere, $sOrder, $sLimit);
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $DataTableArray['iTotal'],
            "iTotalDisplayRecords" => $DataTableArray['iTotal'],
            "aaData" => array()
        );
        $aHeader = array('id','user_id','request_touserid','Date','amount', 'topup_by', 'transaction_remark', 'is_approvel', 'Actions');
        $site_url = base_url();
        
        foreach ($DataTableArray['ResultSet'] as $aRow) {
            $row = array();
            $TotalHeader = count($aHeader);

            for ($i = 0; $i < $TotalHeader; $i++) {
                if ($aHeader[$i] == "user_id") {
                    $GetName = $this->common_model->get_value('users','first_name,last_name','id='.$aRow['user_id']);
					$FullNameReqBy = $GetName->first_name.' '.$GetName->last_name;
                    $row[] = $FullNameReqBy;
                } else if ($aHeader[$i] == "request_touserid") {
					if($aRow['topup_by'] =='PG'){
					$GetName = $this->common_model->get_value('users','first_name,last_name','id='.$aRow['user_id']);
					$FullNameReqTo = $GetName->first_name.' '.$GetName->last_name;	
					}else{
                    $GetName = $this->common_model->get_value('users','first_name,last_name','id='.$aRow['request_touserid']);
					$FullNameReqTo = $GetName->first_name.' '.$GetName->last_name;
					}
                    $row[] = $FullNameReqTo;
                } else if ($aHeader[$i] == "amount") {
					$Amnt = (string)$aRow['amount'];
					$Amt =strpos($Amnt,'.');
					$Amount =substr($Amnt,0,$Amt+3);
					$row[] = $Amount;
                } else if ($aHeader[$i] == "topup_by") {
					$TopUpBy = $aRow['topup_by'];
					$row[] = htmlspecialchars($TopUpBy, ENT_QUOTES);
                } else if ($aHeader[$i] == "transaction_remark") {
					$TransRemark = $aRow['transaction_remark'];
					$row[] = htmlspecialchars($TransRemark, ENT_QUOTES);
                } else if ($aHeader[$i] == "is_approvel") {
					$TopupStatus = $aRow['is_approvel'];
					if($TopupStatus =='A'){
					$Tstatus =	'Accepted';
					}elseif($TopupStatus =='R'){
					$Tstatus =	'Rejected';	
					}else{
					$Tstatus =	'Pending';	
					}
					$row[] = $Tstatus;
                } else if ($aHeader[$i] == "Actions") {
                    $Action = '<a style="padding: 0 4px;" class="pull-left view_btn" href="" ref="'.$aRow['id'].'" rel="tooltip" data-placement="top" data-original-title="View" title="Request View"><i class="fa fa-eye" style="font-size:18px;color:#337ab7;"></i></a>&nbsp;&nbsp;&nbsp;';
                    $row[] = $Action;
                } else if ($aHeader[$i] != ' ') {
                    /* General output */
                    $row[] = $aRow[$aHeader[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode($output);
    }
	
	/** Save Function For Funds Support Request */
	public function save_fund_support_request() 
	{
	   $input = $this->input->post(NULL,TRUE);
        
        $config = array(
            array('field' => 'retailer', 'label' => 'Retailer', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please Select Retailer.')),
            array('field' => 'amount', 'label' => 'Amount', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please Enter Amount.')),
			array('field' => 'transanction_mode', 'label' => 'Transfer Mode', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please Select Transfer Mode.'))
        );

        if (form_validate_rules($config)) {

                $amount = intval($input['amount']);
			    if($amount <= 0){
					$data["flag"] = '@#failed#@';
					$data["msg_type"] = 'error';
					$data["msg"] = 'Amount Should Be Greater Than 0';  
			    }else{
                $UserID = $this->session->userdata("user_id");
				$now = date('Y-m-d H:i:s', time());
				$OwnAvaiableBalance = $this->common_model->get_value('wallet','id,amt','user_id='.$input['retailer']);
				$NowTotalBalance = $OwnAvaiableBalance->amt + $input['amount'];
				
				if($NowTotalBalance > WALLET_LIMIT){
				 $data["flag"] = '@#failed#@';
                 $data["msg"] = 'Sorry, Balance Request can not be processed as you are exceeding wallet balance limit.';
				} else {
				
					$TransMode =  $this->common_model->get_value('payment_transfer_mode','mode','id='.$input['transanction_mode']);
					
					$data = array(
					'user_id'	        => $input['retailer'],
					'request_touserid'	=> $input['request_to'],
					'amount'	        => $input['amount'], 
					'topup_by'	        => $TransMode->mode,
					'transaction_no'    => $input['transanction_ref'],
					'bank_name'	        => $input['bank_name'], 
					'bank_acc_no'	    => $input['bank_account_no'],
					'transaction_date'	=> $now,				
					'transaction_remark'=> $input['remark'], 
					'is_approvel'	    => 'NA',
					'is_status'	        => 'Y',
					'created_by'	    => $UserID,
					'support_request'	=> 1,
					'suprt_userid'	    => $UserID,
					'created_date'      => $now);
				
					$id = $this->common_model->insert('wallet_topup', $data);
					log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $input, "User Enters data");
                    log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $data, "Data Added In to Wallet Table");
					if ($id > 0) {
						
						$data["flag"] = '@#success#@';
						$data["msg_type"] = 'success';
						$data["msg"] = 'Balance Request Added Successfully.';						
												
						// send email
                        $RequestByData =  $this->common_model->get_value('users','first_name,last_name','id='.$input['retailer']);						
						$firstname = $RequestByData->first_name;
						$lastname = $RequestByData->last_name;
						$ToEmail =  $this->common_model->get_value('users','email','id='.$input['request_to']);
						$ToMobile =  $this->common_model->get_value('users','mobile_no,first_name,last_name','id='.$input['request_to']);
						$mail_replacement_array = array(
							"{{topup_by}}" => $TransMode->mode,
							"{{amount}}" => round($input['amount'],2),
							"{{bank_name}}" => $input['bank_name'],
							"{{bank_acc_no}}" => $input['bank_account_no'],
							"{{transaction_no}}" => $input['transanction_ref'],
							"{{transaction_remark}}" => $input['remark'],
							"{{username}}" => $firstname . ' ' . $lastname,
							"{{useremail}}" => $this->session->userdata('email'),
							"{{transaction_date}}" => date('d-m-Y H:i:s', strtotime($now)),
							"{{is_approvel}}" => 'Pending'
						);
						eval(ADMIN_EMAIL_ARRAY_FOR_AGENT);
						//for admin and not for Retailer
						$data['mail_content'] = str_replace(array_keys($mail_replacement_array), array_values($mail_replacement_array), WALLET_TOPUP_REQUEST);

						$mailbody_array = array("subject" => "Wallet Topup Request of Rs " .round($input['amount'],2). " by Retailer " . $firstname . ' ' . $lastname . " ",
							"message" => $data['mail_content'],
							"to" => rokad_decrypt($ToEmail->email,$this->config->item('pos_encryption_key'))
						);						
						
						$mail_result = send_mail($mailbody_array);
						if ($mail_result) {
							$data1['flag'] = "@#success#@";
						}

						//ROKAD sms
						$flag = true;
						if ($flag == true) {
							$sms_array = array (
											"{{request_to_username}}" => $ToMobile->first_name.' '.$ToMobile->last_name,
											"{{request_by_role_name}}" => $this->session->userdata('role_name'),
											"{{request_by_username}}" => $firstname . ' ' . $lastname,
											"{{amount}}" => round($input['amount'],2)

											);
							$msg_to_sent = str_replace(array_keys($sms_array), array_values($sms_array), WALLET_TOPUP_REQUEST_TO);
							$is_send = sendSMS(rokad_decrypt($ToMobile->mobile_no,$this->config->item('pos_encryption_key')), $msg_to_sent);
							if ($is_send) {
								$data1['sms'] = "@#success#@";
							}
						}
							
					} else {
						$data["flag"] = '@#failed#@';
						$data["msg_type"] = 'error';
						$data["msg"] = 'Balance Request could not be Added.';						
					}
                }
                }
        } else {
            $data["flag"] = '@#failed#@';
            $data["msg_type"] = 'error';
            $data["msg"] = 'Balance Request could not be added/updated without All * Neccessary Data Not Fill.';
        }

        data2json($data);
    }
	
	/** View Function For Funds Support Request */
	public function view_fund_support_request() {
        $input = $this->input->post();
        $FundRequestData = $this->fund_request_model->getFundRequestData($input["id"]);
		$Request = $this->common_model->get_value('users','first_name,last_name','id='.$FundRequestData->request_touserid);
		$RequestName = $Request->first_name.' '.$Request->last_name;
		$RequestBy = $this->common_model->get_value('users','first_name,last_name','id='.$FundRequestData->user_id);
		$RequestByName = $RequestBy->first_name.' '.$RequestBy->last_name;
        $data["flag"] = '@#success#@';
        $data["RequestTo"] = $RequestName;
        $data["RequestByName"] = $RequestByName;
        $data["FundRequest"] = $FundRequestData;

        data2json($data);
    }
	
	public function getUserByLevel() {
        $lvlid = $this->input->post('SearchBy', TRUE);
		
		if($lvlid == COMPANY_ROLE_ID){
		$data['result'] = $this->fund_request_model->getRokadByRoleId($lvlid);
		} elseif($lvlid == MASTER_DISTRIBUTOR_ROLE_ID){
        $data['result'] = $this->fund_request_model->getMasterDistributorByRoleId($lvlid);
		} elseif($lvlid == AREA_DISTRIBUTOR_ROLE_ID){
        $data['result'] = $this->fund_request_model->getAreaDistributorByRoleId($lvlid);
		} elseif($lvlid == DISTRIBUTOR){
        $data['result'] = $this->fund_request_model->getDistributorByRoleId($lvlid);
		} elseif($lvlid == RETAILER_ROLE_ID){
        $data['result'] = $this->fund_request_model->getRetailerByRoleId($lvlid);
		}
        echo json_encode($data);
    }
	
	/**
     * [get_topup_emaildata description] : use in get_reason function for get topup detail 
     * @param  [type] $id [description] : wallet topup id 
     * @return [type]     [description]
     */
	public function get_topup_emaildata($id)
	{
		if(isset($id) && $id != '') {
			$this->db->select('concat(u.first_name," ",u.last_name) as agent_name,u.mobile_no,wt.topup_by,u.email,wt.amount,,wt.bank_name,wt.bank_acc_no,wt.transaction_no,DATE_FORMAT(wt.transaction_date,"%d-%m-%Y %h:%i:%s") as transaction_date,wt.transaction_remark,(CASE wt.is_approvel when "NA" THEN "Pending" when "A" THEN "Accepted" when "R" then "Rejected" ELSE `is_approvel`  END) as is_approvel,wt.app_rej_date,wt.reject_reason,wt.request_touserid,wt.user_id');
			$this->db->from("wallet_topup wt ");
			$this->db->join('users u','wt.user_id=u.id','inner');
			$this->db->where("wt.id = " . $id);
			$query        = $this->db->get();
			$result_array = $query->row_array();

			return $result_array;
		}
	}

}
