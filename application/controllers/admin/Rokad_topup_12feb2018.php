<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rokad_topup extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		is_logged_in();
		$this->load->model(array('rokad_topup_model','fund_request_model','common_model','users_model', 'payment_transaction_fss_model','wallet_topup_model', 'user_email_model', 'user_otp_model', 'user_tpin_model'));
        $this->load->library('form_validation');
		$this->load->library('cca_payment_gateway');
    }
    
    public function index()
	{
	   
       $data = array();
       $data['user_data']  = $this->session->userdata();
	   $data['user_type']  = $data['user_data']['role_name'];
	   $RollID = $this->session->userdata("role_id");
	   $UserID = $this->session->userdata("user_id");
	   $data['Pay_Trans_Mode'] = $this->fund_request_model->getPaymentTransferMode();
	   
       load_back_view(BACK_ROKAD_TOPUP_REQUEST_LIST, $data);             
    }
	
	public function create_new()
	{
	   
       $data = array();
       $data['user_data']  = $this->session->userdata();
	   $data['user_type']  = $data['user_data']['role_name'];
	   $RollID = $this->session->userdata("role_id");
	   $UserID = $this->session->userdata("user_id");
	   $data['Pay_Trans_Mode'] = $this->fund_request_model->getPaymentTransferMode();
	   
       load_back_view(BACK_ROKAD_TOPUP_REQUEST_CREATE, $data);             
    }
	
	/** Save Function For Funds Request */
	public function save_rokad_topup() 
	{
		$this->session->unset_userdata('rokad_topup');
		$input = $this->input->post(NULL,TRUE);	   

        $config = array(
            array('field' => 'amount', 'label' => 'About', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please Enter Amount.')),
			array('field' => 'transanction_mode', 'label' => 'Transfer Mode', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please Select Transfer Mode.'))
        );

        if (form_validate_rules($config)) {
			$amount = intval($input['amount']);
			if($amount <= 0){
			    $data["flag"] = '@#failed#@';
				$data["msg_type"] = 'error';
				$data["msg"] = 'Amount Should Be Greater Than 0';  
			}else{
			    $UserID = $this->session->userdata("user_id");
				$username = $this->session->userdata("username");
				$display_name = $this->session->userdata("display_name");
				
			    $now = date('Y-m-d H:i:s', time());

				$TransMode =  $this->common_model->get_value('payment_transfer_mode','mode','id='.$input['transanction_mode']);
									
				$TrimaxData = $this->common_model->get_value('users','id','role_id='.TRIMAX_ROLE_ID);

				$trimaxWalletBal = $this->common_model->get_value('wallet','amt','user_id='.$TrimaxData->id);
			    if($input['amount'] > $trimaxWalletBal->amt) {
			    	$data["flag"] = '@#failed#@';
					$data["msg_type"] = 'error';
					$data["msg"] = 'Insufficient Balance In Trimax Wallet. Please Enter Lesser Amount.';
			    } else {
					$submitted_data = array(
					'user_id'	        => $UserID,
					'request_touserid'	=> $TrimaxData->id,
					'amount'	        => $input['amount'], 
					'topup_by'	        => $TransMode->mode,
					'transaction_no'    => $input['transanction_ref'],
					'bank_name'	        => $input['bank_name'], 
					'bank_acc_no'	    => $input['bank_account_no'],
					'transaction_date'	=> $now,				
					'transaction_remark'=> $input['remark'], 
					'is_approvel'	    => 'NA',
					'is_status'	        => 'Y',
					'created_by'	    => $UserID,
					'created_date'      => $now);
					
					//show($data);
					$response = save_n_send_otp($UserID, $username, $display_name, RESET_USERNAME_OTP, 'rokad_topup_request');
					if($response['msg_type'] == 'success')
					{						
						$rokad_topup_str = serialize($submitted_data);
						$this->session->set_userdata('rokad_topup', $rokad_topup_str);

						$data["flag"] = '@#success#@';
						$data["msg_type"] = 'success';
						$data["msg"] = 'success';						
					}
					else
					{
						$data["flag"] = '@#failed#@';
						$data["msg_type"] = 'error';
						$data["msg"] = $response['msg'];						
					}
				}
			}					
		} else {
            $data["flag"] = '@#failed#@';
            $data["msg_type"] = 'error';
            $data["msg"] = 'Topup Request could not be added/updated without All * Neccessary Data Not Fill.';
        }

        data2json($data);
    }
	
	public function otp_rokad_topup() {	
		
		$input = $this->input->post(NULL,TRUE);
        $config = array(
            array('field' => 'otp', 'label' => 'OTP', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please Enter OTP.'))
		);

        if (form_validate_rules($config)) {
			
			 $UserID = $this->session->userdata("user_id");
			 $username = $this->session->userdata("username");
			 
			 $response = verify_otp($UserID, $username, $input['otp']);			 
			 if($response['msg_type'] == 'success')
			 {
				$rokad_topup = unserialize($this->session->userdata('rokad_topup'));				
				$id = $this->common_model->insert('wallet_topup', $rokad_topup);				
				log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $input, "User Enters data");
				log_data("topup/" . date("M") . "/" . date("d") . "/topup_request_log.log", $rokad_topup, "Data Added In to Wallet Table");
				if ($id > 0) {
					$data["flag"] = '@#success#@';
					$data["msg_type"] = 'success';
					$data["msg"] = 'Topup Request Added successfully.';
					
					// send email						
					$firstname = $this->session->userdata('first_name');
					$lastname = $this->session->userdata('last_name');
										
					$TrimaxData     = $this->common_model->get_value('users','first_name,last_name,email,mobile_no','role_id='.TRIMAX_ROLE_ID);
										
					$mail_replacement_array = array(
						"{{topup_by}}" => $rokad_topup['topup_by'],
						"{{amount}}" => round($rokad_topup['amount'],2),
						"{{bank_name}}" => $rokad_topup['bank_name'],
						"{{bank_acc_no}}" => $rokad_topup['bank_acc_no'],
						"{{transaction_no}}" => $rokad_topup['transaction_no'],
						"{{transaction_remark}}" => $rokad_topup['transaction_remark'],
						"{{username}}" => $firstname . ' ' . $lastname,
						"{{useremail}}" => $this->session->userdata('email'),
						"{{transaction_date}}" => date('d-m-Y H:i:s', strtotime($rokad_topup['transaction_date'])),
						"{{is_approvel}}" => 'Pending'
					);
					
					//for admin and not for Retailer
					$data1['mail_content'] = str_replace(array_keys($mail_replacement_array), array_values($mail_replacement_array), WALLET_TOPUP_REQUEST);

					$mailbody_array = array("subject" => "Wallet Topup Request of Rs " .round($rokad_topup['amount'],2). " by " . $firstname . ' ' . $lastname . " ",
						"message" => $data1['mail_content'],
							"to" => rokad_decrypt($TrimaxData->email,$this->config->item('pos_encryption_key'))
					);	
					
					$mail_result = send_mail($mailbody_array);
										
					if ($mail_result) {
						$data1['flag'] = "@#success#@";
					}

					//ADMIN sms
					$flag = true;
					if ($flag == true) {
							$sms_array = array (
											"{{request_to_username}}" => $TrimaxData->first_name.' '.$TrimaxData->last_name,
											"{{request_by_role_name}}" => $this->session->userdata('role_name'),
											"{{request_by_username}}" => $firstname . ' ' . $lastname,
											"{{amount}}" => round($rokad_topup['amount'],2)

											);
							$msg_to_sent = str_replace(array_keys($sms_array), array_values($sms_array), WALLET_TOPUP_REQUEST_TO);
							$is_send = sendSMS(rokad_decrypt($TrimaxData->mobile_no,$this->config->item('pos_encryption_key')), $msg_to_sent);
							if ($is_send) {
								$data1['sms'] = "@#success#@";
							}
						}
				} else {
					$data["flag"] = '@#failed#@';
					$data["msg_type"] = 'error';
					$data["msg"] = 'Topup Request could not be Added.';
				}
			}
			 else
			 {
				$data["flag"] = '@#failed#@';
				$data["msg_type"] = 'error';
				$data["msg"] = $response['msg'];				 
			 }
		}
		else {
            $data["flag"] = '@#failed#@';
            $data["msg_type"] = 'error';
            $data["msg"] = 'All * Neccessary Data Not Fill.';
        }
		
		data2json($data);
	}
	
	/** View Function For Funds Request */
	public function view_rokad_topup() {
        $input = $this->input->post();
        $FundRequestData = $this->rokad_topup_model->getRokadRequestData($input["id"]);
		$data["flag"] = '@#success#@';
        $data["FundRequest"] = $FundRequestData;

        data2json($data);
    }
	
    /** Get List For Funds Request */
    public function DataTableTopupRequest() {
		
        $aColumns = array('id','created_date', 'amount','topup_by', 'transaction_remark','is_approvel', 'id');

        $DataTableArray = getDefinedDataTable($aColumns);
        $sWhere = $DataTableArray['sWhere'];
        $sOrder = $DataTableArray['sOrder'];
        $sLimit = $DataTableArray['sLimit'];
		
		        
		//$RollID = COMPANY_ROLE_ID;
		//$UserID = $this->common_model->get_value('users','id','role_id='.$RollID);

		$UserID = $this->session->userdata("user_id");
		
		
		if($UserID !="")
        {            
           if($sWhere<>''){
            $sWhere .= " WHERE user_id  = '".$UserID."'"; 
           }else{
               $sWhere .= " WHERE user_id  = '".$UserID."'"; 
           }                                    
        }	
			        
        $DataTableArray = $this->rokad_topup_model->LoadRokadDataTable($sWhere, $sOrder, $sLimit);
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $DataTableArray['iTotal'],
            "iTotalDisplayRecords" => $DataTableArray['iTotal'],
            "aaData" => array()
        );
        $aHeader = array('id','Date','amount', 'topup_by', 'transaction_remark', 'is_approvel', 'Actions');
        $site_url = base_url();
        
        foreach ($DataTableArray['ResultSet'] as $aRow) {
            $row = array();
            $TotalHeader = count($aHeader);

            for ($i = 0; $i < $TotalHeader; $i++) {
                if ($aHeader[$i] == "amount") {
					$Amnt = (string)$aRow['amount'];
					$Amt =strpos($Amnt,'.');
					$Amount =substr($Amnt,0,$Amt+3);
					$row[] = $Amount;
                } else if ($aHeader[$i] == "is_approvel") {
					$TopupStatus = $aRow['is_approvel'];
					if($TopupStatus =='A'){
					$Tstatus =	'Accepted';
					}elseif($TopupStatus =='R'){
					$Tstatus =	'Rejected';	
					}else{
					$Tstatus =	'Pending';	
					}
					$row[] = $Tstatus;
                } else if ($aHeader[$i] == "Actions") {
                    $Action = '<a style="padding: 0 4px;" class="pull-left view_btn" href="" ref="'.$aRow['id'].'" rel="tooltip" data-placement="top" data-original-title="View" title="Topup View"><i class="fa fa-eye" style="font-size:18px;color:#337ab7;"></i></a>&nbsp;&nbsp;&nbsp;';
                    $row[] = $Action;
                } else if ($aHeader[$i] != ' ') {
                    /* General output */
                    $row[] = $aRow[$aHeader[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode($output);
    }
	
	/** Load Funds Request Approval Page*/
	public function rokad_topup_approval()
	{
       $data = array();
       $data['user_data']  = $this->session->userdata();
       $data['user_type']  = $data['user_data']['role_name'];
	   load_back_view(BACK_ROKAD_TOPUP_REQUEST_APPROVAL_LIST, $data);             
    }
	
    /** Get List For Funds Request Approval*/
    public function DataTableTopupApproval() {
		
        $aColumns = array('id', 'created_date', 'amount','topup_by', 'transaction_remark','is_approvel', 'a.id');

        $DataTableArray = getDefinedDataTable($aColumns);
        $sWhere = $DataTableArray['sWhere'];
        $sOrder = $DataTableArray['sOrder'];
        $sLimit = $DataTableArray['sLimit'];

		//$RollID = COMPANY_ROLE_ID;		
		$RollID = $this->session->userdata("role_id");
		$UserID = $this->session->userdata("user_id");
		//$UserID = $this->common_model->get_value('users','id','role_id='.$RollID);
		
		$sWhere = '';

		$sWhere .= "WHERE topup_by != 'pg'";

		if($RollID == TRIMAX_ROLE_ID){
			if($UserID != "")
			{
				if($sWhere<>''){
					$sWhere .= " and request_touserid  = '".$UserID."'";
				}else{
					$sWhere .= " WHERE request_touserid  = '".$UserID."'";
				}
				$sWhere .= " and user_id  != '".$UserID."'";
			}
		}
		
		/*if($UserID !="")
		{            
			if($sWhere<>''){
			$sWhere .= " WHERE user_id  = '".$UserID."'"; 
			}else{
			$sWhere .= " WHERE user_id = '".$UserID."'"; 
			}                                    
		}*/
				
		
		$DataTableArray = $this->rokad_topup_model->LoadRokadApprovalDataTable($sWhere, $sOrder, $sLimit);
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $DataTableArray['iTotal'],
            "iTotalDisplayRecords" => $DataTableArray['iTotal'],
            "aaData" => array()
        );
        $aHeader = array('id', 'Date','amount', 'topup_by', 'transaction_remark', 'is_approvel', 'Actions');
        $site_url = base_url();
        
        foreach ($DataTableArray['ResultSet'] as $aRow) {
            $row = array();
            $TotalHeader = count($aHeader);

            for ($i = 0; $i < $TotalHeader; $i++) {
                if ($aHeader[$i] == "amount") {
					$Amnt = (string)$aRow['amount'];
					$Amt =strpos($Amnt,'.');
					$Amount =substr($Amnt,0,$Amt+3);
					$row[] = $Amount;
                } else if ($aHeader[$i] == "is_approvel") {
					$TopupStatus = $aRow['is_approvel'];
					if($TopupStatus =='A'){
					$Tstatus =	'Accepted';
					}elseif($TopupStatus =='R'){
					$Tstatus =	'Rejected';	
					}else{
					$Tstatus =	'Pending';	
					}
					$row[] = $Tstatus;
                } else if ($aHeader[$i] == "Actions") {
					
					if($aRow['is_approvel'] == 'A' or $aRow['is_approvel'] == 'R'){ 
					$Action = '<a style="padding: 0 4px;" class="pull-left" href="'.$site_url.'admin/rokad_topup/view_rokad_topup_approval/'.base64_encode($aRow['id']).'" ref="'.$aRow['id'].'" rel="tooltip" data-placement="top" data-original-title="View" title="Topup View"><i class="fa fa-eye" style="font-size:18px;color:#337ab7;"></i></a>&nbsp;&nbsp;&nbsp;';
					}else {
					$Action = '<a style="padding: 0 4px;" class="pull-left" href="'.$site_url.'admin/rokad_topup/edit_rokad_topup_approval/'.base64_encode($aRow['id']).'" ref="'.$aRow['id'].'" rel="tooltip" data-placement="top" data-original-title="Edit" title="Topup Edit"><i class="fa fa-edit" style="font-size:18px;color:#337ab7;"></i></a>&nbsp;&nbsp;&nbsp;';	
					}
                   
					$row[] = $Action;
                } else if ($aHeader[$i] != ' ') {
                    /* General output */
                    $row[] = $aRow[$aHeader[$i]];
                }
            }
            $output['aaData'][] = $row;
        }

        echo json_encode($output);
    }

	/** Edit Function For Funds Request Approval*/
	public function edit_rokad_topup_approval($id) {
		$id= base64_decode($id);
		$FundRequestData = $this->rokad_topup_model->getRokadRequestApprovalData($id);
        $data["FundRequest"] = $FundRequestData;
        load_back_view(BACK_ROKAD_TOPUP_REQUEST_APPROVAL, $data); 
    }
	
	/** View Function For Funds Request Approval*/
	public function view_rokad_topup_approval($id,$self_view = '') {
		$id= base64_decode($id);
		if(!empty($self_view)) {
			$data['self_view'] =  base64_decode($self_view);
		}
		$FundRequestData = $this->rokad_topup_model->getRokadRequestApprovalData($id);
        $data["FundRequest"] = $FundRequestData;
        load_back_view(BACK_ROKAD_TOPUP_REQUEST_APPROVAL_VIEW, $data); 
    }	

	/** Save Function For Funds Request */
	public function save_rokad_topup_approval() 
	{
	   $this->session->unset_userdata('rokad_topup_approval');
	   $input = $this->input->post(NULL,TRUE);
	   $id = $input['fundRequest_id'];
	   $config = array(
            array('field' => 'request_status', 'label' => 'Status', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please Select Status.'))
        );
        
        if (form_validate_rules($config)) {
						
					$UserID = $this->session->userdata("user_id");
					$username = $this->session->userdata("username");
					$display_name = $this->session->userdata("display_name");
					
				    $submitted_data = array(
					'fundRequest_id' => $id,
					'requested_by'	=> $input['requested_by'], 
					'amount'	    => $input['amount'],
					'request_status' => $input['request_status'],
					'reject_reason'	  => $input['reject_reason']
					);
					
					//show($data);
					$response = save_n_send_otp($UserID, $username, $display_name, RESET_USERNAME_OTP, 'rokad_topup_request_approval');
					if($response['msg_type'] == 'success')
					{						
						$rokad_topup_str = serialize($submitted_data);
						$this->session->set_userdata('rokad_topup_approval', $rokad_topup_str);

						$data["flag"] = '@#success#@';
						$data["msg_type"] = 'success';
						$data["msg"] = 'success';						
					}
					else
					{
						$data["flag"] = '@#failed#@';
						$data["msg_type"] = 'error';
						$data["msg"] = $response['msg'];						
					}
		} else {
			
			$data["flag"] = '@#failed#@';
            $data["msg_type"] = 'error';
            $data["msg"] = 'Topup Request Approval could not be added/updated without All * Neccessary Data Not Fill.';		
        }
		data2json($data);	
    }
	
	public function rokad_topup_approval_otp() 
	{		
		$input = $this->input->post(NULL,TRUE);
        $config = array(
            array('field' => 'otp', 'label' => 'OTP', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please Enter OTP.'))
		);
		
		if (form_validate_rules($config)) {
			
			 $UserID = $this->session->userdata("user_id");
			 $username = $this->session->userdata("username");
			 
				$response = verify_otp($UserID, $username, $input['otp']);	
				
			 if($response['msg_type'] == 'success')
			 {
				$rokad_topup = unserialize($this->session->userdata('rokad_topup_approval'));					 
				$fundRequest_id = $rokad_topup['fundRequest_id'];
								
				if($rokad_topup['request_status'] == 'A'){
							
					$UserID = $this->session->userdata("user_id");
					$now = date('Y-m-d H:i:s', time());
					
					$data = array(
					'is_status'    => 'Y',
					'is_approvel'  => 'A',
					'app_rej_by'   => $UserID,
					'app_rej_date' => $now,
					'updated_date' => $now);
				    $id = $this->common_model->update('wallet_topup','id',$fundRequest_id, $data);
					log_data("topup/" . date("M") . "/" . date("d") . "/topup_accepted_log.log", $data, "Accepted By data");
                    
					// For Credit Insertion OF Users
					
					$RequestBy = $this->common_model->get_value('wallet','amt,id','user_id='.$rokad_topup['requested_by']);
					$AmntAfterTransfer = $RequestBy->amt + $rokad_topup['amount'];
					
					$transaction_no = substr(hexdec(uniqid()), 4, 12);
					$creditdata = array(
						'transaction_no'      => $transaction_no,
						'w_id'	              => $RequestBy->id,
						'amt'	              => $rokad_topup['amount'],
						'user_id'	          => $rokad_topup['requested_by'],
						'amt_before_trans'    => $RequestBy->amt,	 
						'amt_after_trans'	  => $AmntAfterTransfer ,
						'topup_id'	          => $rokad_topup['fundRequest_id'],
						'status'              => 'added',
						'transaction_type'	  => 'Credited',
						'comment'	          => 'Top Up Request Accept',
						'is_status'	          => 'Y',
						'transaction_type_id' => 1,
						'added_on'            => $now, 
						'added_by'            => $UserID);
			
				    $CreditIdBy = $this->common_model->insert('wallet_trans',$creditdata);

					log_data("topup/" . date("M") . "/" . date("d") . "/topup_accepted_log.log", $creditdata, "Credit In to Wallet Trans Table");
					
					if($CreditIdBy){
						$data = array(
							'amt'	       => $AmntAfterTransfer,
							'updated_by'   => $UserID,
							'updated_date' => $now);
				        $UpdateId = $this->common_model->update('wallet','user_id',$rokad_topup['requested_by'],$data);	
					}
					
					// For Debit Insertion OF Users
					
					$FundRequestData = $this->rokad_topup_model->getRokadRequestApprovalData($rokad_topup['fundRequest_id']);				
					
                                        $RequestTo = $this->common_model->get_value('wallet','id,amt','user_id='.$FundRequestData->request_touserid);

					$AmntAfterTransferTo = $RequestTo->amt - $FundRequestData->amount;
					$debit_transaction_no = substr(hexdec(uniqid()), 4, 12);
					$debitdata = array(
							'transaction_no'      => $debit_transaction_no,
							'w_id'	              => $RequestTo->id,
							'amt'	              => $FundRequestData->amount,
							'user_id'	          => $FundRequestData->request_touserid,
							'amt_before_trans'    => $RequestTo->amt,
							'amt_after_trans'	  => $AmntAfterTransferTo ,
							'topup_id'	          => $rokad_topup['fundRequest_id'],
							'status'              => 'deduct',
							'transaction_type'	  => 'Debited',
							'comment'	          => 'Top Up Request Accept',
							'is_status'	          => 'Y',
							'transaction_type_id' => 14,
							'added_on'            => $now,
							'added_by'            => $UserID);
					
					$DebitIdTo = $this->common_model->insert('wallet_trans',$debitdata);
					log_data("topup/" . date("M") . "/" . date("d") . "/topup_accepted_log.log", $debitdata, "Debit In to Wallet Trans Table");
					if($DebitIdTo){
						$data = array(
								'amt'	             => $AmntAfterTransferTo,
								'updated_by'         => $UserID,
								'last_transction_id' => $DebitIdTo,
								'updated_date'       => $now);
						$UpdateId = $this->common_model->update('wallet','user_id',$FundRequestData->request_touserid,$data);
				
					}
					
					//send email and sms to agent and admin once accepted
                    $result_accepted        = $this->get_topup_emaildata($fundRequest_id);					
															
                    $RequestToUser = $this->common_model->get_value('users','email','id='.$FundRequestData->request_touserid);                   										
					
					$mail_replacement_array =   array (
                                                "{{topup_by}}"                         => $result_accepted['topup_by'],
                                                "{{amount}}"                           => round($result_accepted['amount'],2),
                                                "{{bank_name}}"                        => $result_accepted['bank_name'],
                                                "{{bank_acc_no}}"                      => $result_accepted['bank_acc_no'],
                                                "{{transaction_no}}"                   => $result_accepted['transaction_no'],
                                                "{{transaction_remark}}"               => $result_accepted['transaction_remark'],
                                                "{{username}}"                         => $result_accepted['agent_name'],
                                                "{{useremail}}"                        => rokad_decrypt($result_accepted['email'],$this->config->item('pos_encryption_key')),
                                                "{{transaction_date}}"                 => date('d-m-Y H:i:s',strtotime($result_accepted['transaction_date'])),
                                                "{{is_approvel}}"                      => $result_accepted['is_approvel']
                                            ); 
                    $data['mail_content']       =   str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),WALLET_TOPUP_REQUEST);
                    
					// for requested to 
					$mailbody_array_admin       =   array( "subject"  => "Approved TopUp Request of Rs ".round($result_accepted['amount'],2)." for  ".$result_accepted['agent_name']." ",
                                                                         "message"  => $data['mail_content'],
																		 "to"       => rokad_decrypt($RequestToUser->email, $this->config->item('pos_encryption_key'))
                                                                );
                    $mail_result_admin          =   send_mail($mailbody_array_admin);					
					
					// for requested by
					$mailbody_array_agent       =   array(  "subject" => "Approved TopUp Request of Rs ".round($result_accepted['amount'],2)." ",
															"message" => $data['mail_content'],
															"to"      => rokad_decrypt($result_accepted['email'],$this->config->item('pos_encryption_key'))
														);
					$mail_result_agent          = send_mail($mailbody_array_agent);					
					
					$mail_replacement_arraysms =   array(
															"{{amount}}" => $AmntAfterTransfer
														); 							
					
					//send sms to approval to        
					$flags     = true;
					if($flags == true) { 
						$ApprovalToWalletAmount = $this->common_model->get_value('wallet','amt','user_id='.$result_accepted['user_id']);
	$sms_approval_to_array = array (
                    							"{{username}}" => $result_accepted['agent_name'],
                    							"{{accept_reject}}" => 'Accepted',
                    							"{{wallet_balance}}" => round($ApprovalToWalletAmount->amt,2)
                    							);
                        $msg_to_sent_admin = str_replace(array_keys($sms_approval_to_array),array_values($sms_approval_to_array),WALLET_TOPUP_APPROVAL_TO);
                        $is_send           = sendSMS(rokad_decrypt($result_accepted['mobile_no'],$this->config->item('pos_encryption_key')), $msg_to_sent_admin);
                        if($is_send) {
                           $data1['sms_admin']       = "@#success#@";
                        }
					}
					
					$data["flag"] = '@#success#@';
					$data["msg_type"] = 'success';
					$data["msg"] = 'Topup Request Approval Added successfully.';
					
				} else if($rokad_topup['request_status'] == 'R') {
					
					$UserID = $this->session->userdata("user_id");
					$now = date('Y-m-d H:i:s', time());
					
					$data = array(
					'is_approvel'	  => 'R',
					'reject_reason'   => $rokad_topup['reject_reason'],
					'app_rej_by'      => $UserID,
					'updated_by'	  => $UserID,
					'updated_date'	  => $now,
					'app_rej_date'    => $now);
				    $id = $this->common_model->update('wallet_topup','id',$fundRequest_id, $data);
					$FundRequestData = $this->rokad_topup_model->getRokadRequestApprovalData($rokad_topup['fundRequest_id']);
					if ($id > 0) {

						$result_accepted        = $this->get_topup_emaildata($fundRequest_id);						
					   
					    $RequestToUser = $this->common_model->get_value('users','email','id='.$FundRequestData->request_touserid);             
					     
						$mail_replacement_array =   array (
                                        "{{topup_by}}"                         => $result_accepted['topup_by'],
                                        "{{amount}}"                           => round($result_accepted['amount'],2),
                                        "{{bank_name}}"                        => $result_accepted['bank_name'],
                                        "{{bank_acc_no}}"                      => $result_accepted['bank_acc_no'],
                                        "{{transaction_no}}"                   => $result_accepted['transaction_no'],
                                        "{{transaction_remark}}"               => $result_accepted['transaction_remark'],
                                        "{{username}}"                         => $result_accepted['agent_name'],
                                        "{{useremail}}"                        => rokad_decrypt($result_accepted['email'],$this->config->item('pos_encryption_key')),
                                        "{{transaction_date}}"                 => date('d-m-Y H:i:s',strtotime($result_accepted['transaction_date'])),
                                        "{{is_approvel}}"                      => $result_accepted['is_approvel'],
                                        "{{reject_reason}}"                    => $result_accepted['reject_reason'],
                                    ); 
						$data['mail_content']       =   str_replace(array_keys($mail_replacement_array),array_values($mail_replacement_array),WALLET_TOPUP_REQUEST_REJECTED);
						
						// for requested to 
                        $mailbody_array_admin       =   array( "subject"  => "Rejected TopUp Request of Rs ".round($result_accepted['amount'],2)." for  ".$result_accepted['agent_name']." ",
                                                               "message"  => $data['mail_content'],
                        									   "to"       => rokad_decrypt($RequestToUser->email, $this->config->item('pos_encryption_key'))
															);
                        $mail_result_admin          =   send_mail($mailbody_array_admin);

						// for requested by
						$mailbody_array_agent       =   array(  "subject" => "Rejected TopUp Request of Rs ".round($result_accepted['amount'],2)." ",
                                                                "message" => $data['mail_content'],
                                                                "to"      => rokad_decrypt($result_accepted['email'],$this->config->item('pos_encryption_key'))
                                                            );
                        $mail_result_agent          = send_mail($mailbody_array_agent);
						
						//send sms to approval  to        
                        $flags     = true;
                        if($flags == true) {
                        	$ApprovalToWalletAmount = $this->common_model->get_value('wallet','amt','user_id='.$result_accepted['user_id']);
                        	$sms_approval_to_array = array (
                        							"{{username}}" => $result_accepted['agent_name'],
                        							"{{accept_reject}}" => 'Rejected',
                        							"{{wallet_balance}}" => round($ApprovalToWalletAmount->amt,2)
                        							);
                            $msg_to_sent_admin = str_replace(array_keys($sms_approval_to_array),array_values($sms_approval_to_array),WALLET_TOPUP_APPROVAL_TO);
                            $is_send           = sendSMS(rokad_decrypt($result_accepted['mobile_no'],$this->config->item('pos_encryption_key')), $msg_to_sent_admin);
                            if($is_send) {
                               $data1['sms_admin']       = "@#success#@";
                            }
                        }
												
						$data["flag"] = '@#success#@';
						$data["msg_type"] = 'success';
						$data["msg"] = 'Topup Request Rejected.';
					} 
				}
			}
			 else
			 {
				$data["flag"] = '@#failed#@';
				$data["msg_type"] = 'error';
				$data["msg"] = $response['msg'];				 
			 }
		}
		else {
            $data["flag"] = '@#failed#@';
            $data["msg_type"] = 'error';
            $data["msg"] = 'All * Neccessary Data Not Fill.';
        }		
		data2json($data);
	}
	
	/**
     * [get_topup_emaildata description] : use in get_reason function for get topup detail 
     * @param  [type] $id [description] : wallet topup id 
     * @return [type]     [description]
     */
	public function get_topup_emaildata($id)
	{
		if(isset($id) && $id != '') {
			$this->db->select('concat(u.first_name," ",u.last_name) as agent_name,u.mobile_no,wt.topup_by,u.email,wt.user_id,wt.amount,,wt.bank_name,wt.bank_acc_no,wt.transaction_no,DATE_FORMAT(wt.transaction_date,"%d-%m-%Y %h:%i:%s") as transaction_date,wt.transaction_remark,(CASE wt.is_approvel when "NA" THEN "Pending" when "A" THEN "Accepted" when "R" then "Rejected" ELSE `is_approvel`  END) as is_approvel,wt.app_rej_date,wt.reject_reason');
			$this->db->from("wallet_topup wt ");
			$this->db->join('users u','wt.user_id=u.id','inner');
			$this->db->where("wt.id = " . $id);
			$query        = $this->db->get();
			$result_array = $query->row_array();

			return $result_array;
		}
	}
}