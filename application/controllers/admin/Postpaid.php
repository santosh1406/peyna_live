<?php

/*
****Author:Lilawati Karale
*** Mobile Postpaid for JRI & TSO Service 

*/
class Postpaid extends CI_Controller {
	
    public function __construct(){
        parent::__construct();
        $this->load->model(array());
        $this->ci = & get_instance();
        $this->load->model(array("Utilities_model"));
        $this->load->library('form_validation');
        $this->load->library('Pdf');
         $this->load->library('Commission_distribution_api');
        $this->load->helper(array('utilities_helper','tso_helper','common_helper'));

        $this->load->model(array('services/service_5/service_5_commission_detail_model', 'retailer_service_mapping_model', 'assign_package_log_model', 'package_detail_model', 'cb_msrtc_agent_transaction_model', 'tmp_agent_balance_track_model', 'cron/cron_model', 'payid_summary_details_model', 'payout_summary_details_model', 'payment_gateway_transaction_model', 'master_services_model', 'travel_destinations_model', 'tickets_model', 'api_balance_log_model', 'wallet_model', 'wallet_trans_model', 'wallet_topup_model', 'users_model', 'wallet_trans_log_model', 'common_model', 'commission_calculation_model', 'cb_commission_distribution_model', 'service_master_model','Tso_model'));
        is_logged_in();


        $this->sessionData['id'] = $this->session->userdata('user_id');
        $this->sessionData["username"] = $this->session->userdata('username');
        $this->sessionData["password"] = $this->session->userdata('password');

        $this->header = array(
            'Content-Type: multipart/form-data; charset=utf-8',
            'X-API-KEY: ' . X_API_KEY . '',
        );
	}

	public function index(){
	 // unset ePayLater sessions
        $this->session->unset_userdata('form_post_date');
        $this->session->unset_userdata('epay_info');
        $this->session->unset_userdata('file_data');
        
        $user_data = $this->session->userdata();
        $agent_id = $user_data['user_id'];
        $retailer_services = $this->retailer_service_mapping_model->getServiceByAgentId($agent_id);
        $service_array = array_column($retailer_services, 'service_id');        
        //check Postpaid service
        if(in_array(POSTPAID_SERVICE_ID, $service_array)){
            $type ="Mobile Postpaid";
            $service = $this->service_master_model->get_aggregator($type);
            if($service=='JRI') {
                $data['operators'] = json_decode(JRI_MOBILE_OPERATORS, true);
                $data['locations'] = json_decode(LOCATIONS, true);
                $view = JRI_POSTPAID_RECHARGE_VIEW;
            } elseif($service == 'TSO'){
                //$data['pre_operators'] = json_decode(TSO_PRE_MOBILE_OPERATORS, true);
                $data['post_operators'] = json_decode(TSO_POST_MOBILE_OPERATORS, true);
                $view = TSO_POSTPAID_RECHARGE_VIEW;
            }else{
                $this->session->set_flashdata('msg', 'No aggregator assigned. Please contact your administrator.');
                redirect(base_url().'admin/dashboard');
            } 
            $data['type'] = $service;
            load_back_view($view, $data);
        }
        else{
            /* Redirect to Dashboard if agent has not enabled the service. - Gopal */
            $this->session->set_flashdata('msg', 'No access to module');
            redirect(base_url().'admin/dashboard');
        }      

    } 


     //After jri reacharge submit function 
    public function recharge() {

        $form_post_date = $this->session->userdata('form_post_date');
        $epay_info = $this->session->userdata('epay_info');
        if ($form_post_date) {
            // request come from 'Epaylater/epay_redirect/' method
            // putting session data in POST
            foreach ($form_post_date as $key => $val) {
                $_POST[$key] = $val;
            }
            $_SERVER["REQUEST_METHOD"] = 'POST';
            $this->session->unset_userdata('form_post_date');
            $this->session->unset_userdata('epay_info');
            $input = null;
        }

        $input = $this->input->post();
        //show($input);
        $type = 'Mobile Postpaid';
        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $input, 'using mobile postpaid mobile Input');
        $service = $this->service_master_model->get_aggregator($type);
        if(strtolower($input['type']) != strtolower($service)){
            $this->session->set_tempdata('message', 'Recharge Unsuccessful. Service has been changed.. Please try again', 3);
            redirect(base_url() . 'admin/Postpaid/index');
        }
			
        $data = array();
        // $input = $this->input->post();
        $error_messages = json_decode(file_get_contents('messages.json'), true);

        if (!empty($input)) {

            $input['user_id'] = $this->sessionData['id'];
            $config = array(
                array('field' => 'recharge_type', 'label' => 'Recharge Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter recharge type.')),
                array('field' => 'mobile_operator', 'label' => 'Mobile Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please enter mobile operator.')),
                array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.')),
                array('field' => 'recharge_amount', 'label' => 'Recharge Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter recharge amount.'))
            );

            if (form_validate_rules($config)) {
                // show($input, 1);
                if($input['type'] == 'JRI'){
                    $curl_url = base_url() . "rest_server/utilities/jri_recharge";
                    // show($curl_url, 1);
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $input, 'Input-Operator');
                    $curlResult = curlForPostData($curl_url, $input, $this->sessionData, $this->header);
                  //  echo"res: "; print_r($curlResult); die;
                    $output = json_decode($curlResult);
                    // $this->ci->output->clear_all_cache();

                    //For Recharge in Process status change 
                    if($output->msg=="Recharge in Process"){
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log',$output->msg , 'Change status of Recharge in Process to Recharge Successful');
                        $output->msg="Recharge Successful";
                    }
                    
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $curlResult, 'JRI Recharge Response Final');
                    $this->session->set_tempdata('message', trim($output->msg), 3);
                    if($output->msg=="Recharge Successful") {
                     //$this->export_pdf($input);
                          $filePath = $_SERVER['DOCUMENT_ROOT'] . 'jri_prepaid/';
                        $file_data = $this->generatePdf($input,$filePath,$output->data->transaction_id);

                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $file_data, 'Mobile Postpaid JRI Recharge Receipt');
                        $this->session->set_userdata('file_data', $file_data);                     
                        log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log','Recharge Receipt' , 'JRI Recharge Receipt is generated');
                        redirect(base_url() . 'admin/Postpaid/receipt_page');
                    }

                }else{
                    $curl_url = base_url() . "rest_server/utilities/recharge";

                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'tata_docomo_special.log', $input, 'Input-Operator');
                    $curlResult = curlForPostData($curl_url, $input, $this->sessionData, $this->header);
                    $output = json_decode($curlResult);
                    $this->session->set_flashdata('message', $output->msg);
//                    if($output->msg=="Recharge Successful") {
//                        $this->export_pdf($input);
//                    }
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'tata_docomo_special.log', $curlResult, 'Web Recharge Response');
                }

                // for ePayLater payment mode
                if ($epay_info) {
                    $this->load->library('epaylater_api');
                    $this->load->helper('epaylater_helper');
                    $this->load->model(array('epaylater_transaction_model', 'epaylater_request_response'));
                    $status = json_decode(EPAYLATER_STATUS, true);

                    $logFileName = date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log';

                    if (strtolower($epay_info['status']) == 'success') {
                        // trimax mobile recharge sucess
                        if (strtolower($output->status) == 'success') {
                            // update transaction id in db
                            $update_data = array();
                            $update_data['wallet_tran_id'] = $output->data->wallet_tran_id;
                            $update_data['transaction_id'] = $output->data->transaction_id;
                            $update_data['status'] = $status['TRIMAX_SUCCESS'];
                            $update_data['comments'] = $curlResult;
                            $update_id = $this->epaylater_transaction_model->update_data($epay_info['trimax_table_id'], $update_data);
				            //Add epaylater commission
                            $commissionResult = $this->Utilities_model->serviceRetailerCommissionDistributionForePay($input, $output->data->wallet_tran_id);
                           
                           //create receipt                               
                            //$this->export_pdf($input,$epay_info);
                            $filePath = $_SERVER['DOCUMENT_ROOT'] . 'jri_postpaid/';
                            $file_data = $this->generatePdf_epay($input,$epay_info,$filePath,$output->data->transaction_id);

                            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'JRI_recharge.log', $file_data, 'epay Mobile Postpaid JRI Recharge Receipt');
                            $this->session->set_userdata('file_data', $file_data);                     
                            redirect(base_url() . 'admin/Postpaid/receipt_page');

                           log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ePayLater_vas_comm_log.log', $commissionResult, 'postpaid JRI_recharge - Commission result');
                        } else {
                            $refund_data = array();
                            $refund_data['marketplaceOrderId'] = $epay_info['trimax_epay_transaction_id'];
                            $refund_data['returnAmount'] = $epay_info['amount'];
                            $refund_data['returnAcceptedDate'] = get_current_formatted_date(null, 1);
                            $refund_data['returnShipmentReceivedDate'] = get_current_formatted_date(null, 1);
                            $refund_data['refundDate'] = get_current_formatted_date(null, 1);
                            $refund_data['returnType'] = 'full';
                            log_data($logFileName, $refund_data, 'request data for refund');
                            #show($refund_data);

                            $insert_data = array(
                                'request' => json_encode($refund_data),
                                'request_type' => $status['REQUEST_CANCEL_N_REFUND'],
                                'transaction_id' => $epay_info['trimax_table_id'],
                                'request_date' => get_current_date()
                            );
                            $last_request_response_id = $this->epaylater_request_response->insert_data($insert_data);

                            $response = $this->epaylater_api->request_to_refund($refund_data);
                            log_data($logFileName, $response, 'Refund API response from ePayLater');

                            $new_status = $status['EMPTY_REFUND_RESPONSE'];
                            if ($response) {
                                if (strtoupper($response['status']) == 'RETURNED')
                                    $new_status = $status['EPAY_CANCEL_N_REFUND_SUCCESS'];
                                else
                                    $new_status = $status['EPAY_CANCEL_N_REFUND_FAILED'];
                            }
                            $update_data = array();
                            $update_data['status'] = $new_status;
                            $update_data['status_description'] = '';
                            $update_data['comments'] = $curlResult;
                            if (isset($output->data->TransactionReference) && !empty($output->data->TransactionReference)) {
                                $update_data['transaction_id'] = $output->data->TransactionReference;
                            }

                            $update_id = $this->epaylater_transaction_model->update_data($epay_info['trimax_table_id'], $update_data);

                            $up_response = array('response' => json_encode($response), 'response_date' => get_current_date());
                            $update_respose = $this->epaylater_request_response->update_data($last_request_response_id, $up_response);
                        }
                    }
                }
            }
        }

        redirect(base_url() . 'admin/Postpaid/index');
    }  


    //after Tso recharge submit function 
        public function tso_recharge() {        
        $form_post_date = $this->session->userdata('form_post_date');
        $epay_info = $this->session->userdata('epay_info');

        if ($form_post_date) {
            // request come from 'Epaylater/epay_redirect/' method
            // putting session data in POST
            foreach ($form_post_date as $key => $val) {
                $_POST[$key] = $val;
            }
            $_SERVER["REQUEST_METHOD"] = 'POST';
            $this->session->unset_userdata('form_post_date');
            $this->session->unset_userdata('epay_info');
            $input = null;
        }
        
        $input = $this->input->post();
        //show($input);
		log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'TSO_recharge.log', $input, 'using mobile postpaid TSO Input');  
        $type = 'Mobile Postpaid';
        $service = $this->service_master_model->get_aggregator($type);
        if(strtolower($input['service']) != strtolower($service)){
            $this->session->set_tempdata('message', 'Recharge Unsuccessful. Service has been changed.. Please try again', 3);
            redirect(base_url() . 'admin/Postpaid/index');
        }
		
		
        $data = array();
        // $input = $this->input->post();

        if (!empty($input)) {

            $input['user_id'] = $this->sessionData['id'];
            if(!empty($input['post_operator'])){
                $input['rec_type'] = 'Postpaid Mobile';
                $input['mobile_operator'] = getTsoPostpaidOperators($input['post_operator']);
            }/*else{
                $input['rec_type'] = 'Prepaid Mobile';
                $input['mobile_operator'] = getTsoPrepaidOperators($input['pre_operator']);
            }*/
        
            $config = array(
                    array('field' => 'plan_type', 'label' => 'Recharge Plan Type', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select recharge plan type.')),
                    array('field' =>  ($input['plan_type'] == 'Prepaid Mobile') ? 'pre_operator' : 'post_operator', 'label' => 'Mobile Operator', 'rules' => 'trim|required|xss_clean', 'errors' => array('required' => 'Please select mobile operator.')),
                    array('field' => 'mobile_number', 'label' => 'Mobile Number', 'rules' => 'trim|required|numeric|exact_length[10]|xss_clean', 'errors' => array('required' => 'Please check mobile number.')),
                    array('field' => 'recharge_amount', 'label' => 'Recharge Amount', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please enter recharge amount.')),
                    array('field' =>  ($input['plan_type'] == 'Prepaid Mobile') ? 'recharge_type' : '', 'label' => 'Recharge Type', 'rules' => 'trim|required|numeric|xss_clean', 'errors' => array('required' => 'Please select recharge type.'))
                );

            if (form_validate_rules($config)) {
                $comm_date = date('Y-m-d');
                $curl_url = base_url() . "rest_server/Tsoapi/recharge";                
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'Postpaid_TSO_recharge.log', $input, 'Input-Operator');
                $curlResult = curlForPostDataTSO($curl_url, $input, $this->sessionData, $this->header);
                $output = json_decode($curlResult);
                // show($output->msg, 1);
                log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'Postpaid_TSO_recharge.log', $output, 'TSO Recharge Response Final');
                $this->session->set_tempdata('message', trim($output->msg), 3);
                if($output->msg=="Recharge Successful") {
                    //$this->export_pdf($input);
                    //distribute commission on success
                //    $comm_data = $this->commission_distribution_api->credit_vas_amount(POSTPAID_SERVICE_ID,$comm_date,$output->data->transaction_id);
                    log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'TSO_recharge.log', $comm_data.' Comm distributed for '.$output->data->transaction_id, 'Using Mobile Prepaid TSO Recharge Comm Dist');
                    
                    $filePath = $_SERVER['DOCUMENT_ROOT'] . 'tso_postpaid/';
                    $file_data = $this->generatePdf($input,$filePath,$output->data->transaction_id);
                  
                    $this->session->set_userdata('file_data', $file_data);                     
                    redirect(base_url() . 'admin/Postpaid/receipt_page');
                }
               
                // for ePayLater payment mode
               if ($epay_info) {
                    $this->load->library('epaylater_api');
                    $this->load->helper('epaylater_helper');
                    $this->load->model(array('epaylater_transaction_model', 'epaylater_request_response'));
                    $status = json_decode(EPAYLATER_STATUS, true);

                    $logFileName = date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'Postpaid_TSO_recharge.log';

                    if (strtolower($epay_info['status']) == 'success') {
                        // trimax mobile recharge sucess
                        if (strtolower($output->status) == 'success') {
                            // update transaction id in db
                            $update_data = array();
                            $update_data['wallet_tran_id'] = $output->data->wallet_tran_id;
                            $update_data['transaction_id'] = $output->data->transaction_id;
                            $update_data['status'] = $status['TRIMAX_SUCCESS'];
                            $update_data['comments'] = $curlResult;
                            $update_id = $this->epaylater_transaction_model->update_data($epay_info['trimax_table_id'], $update_data);
	                       //Add epaylater commission
                            $commissionResult = $this->Utilities_model->serviceRetailerCommissionDistributionForePay($input, $output->data->wallet_tran_id);

                            //create receipt                               
                            //$this->export_pdf($input,$epay_info);
                           log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'ePayLater_vas_comm_log.log', $commissionResult, 'Postpaid TSO_recharge - Commission result');
                            $filePath = $_SERVER['DOCUMENT_ROOT'] . 'tso_postpaid/';
                            $file_data = $this->generatePdf_epay($input,$epay_info,$filePath,$output->data->transaction_id);

                            log_data(date('Y') . '/' . date("M") . '/' . date("d") . '/' . 'Postpaid_TSO_recharge.log', $file_data, 'Mobile Postpaid TSO Recharge Receipt');
                            $this->session->set_userdata('file_data', $file_data);                     
                            redirect(base_url() . 'admin/Prepaid/receipt_page');
                            
                        } else {
                            $refund_data = array();
                            $refund_data['marketplaceOrderId'] = $epay_info['trimax_epay_transaction_id'];
                            $refund_data['returnAmount'] = $epay_info['amount'];
                            $refund_data['returnAcceptedDate'] = get_current_formatted_date(null, 1);
                            $refund_data['returnShipmentReceivedDate'] = get_current_formatted_date(null, 1);
                            $refund_data['refundDate'] = get_current_formatted_date(null, 1);
                            $refund_data['returnType'] = 'full';
                            log_data($logFileName, $refund_data, 'request data for refund');
                            #show($refund_data);

                            $insert_data = array(
                                'request' => json_encode($refund_data),
                                'request_type' => $status['REQUEST_CANCEL_N_REFUND'],
                                'transaction_id' => $epay_info['trimax_table_id'],
                                'request_date' => get_current_date()
                            );
                            $last_request_response_id = $this->epaylater_request_response->insert_data($insert_data);

                            $response = $this->epaylater_api->request_to_refund($refund_data);
                            log_data($logFileName, $response, 'Refund API response from ePayLater');

                            $new_status = $status['EMPTY_REFUND_RESPONSE'];
                            if ($response) {
                                if (strtoupper($response['status']) == 'RETURNED')
                                    $new_status = $status['EPAY_CANCEL_N_REFUND_SUCCESS'];
                                else
                                    $new_status = $status['EPAY_CANCEL_N_REFUND_FAILED'];
                            }
                            $update_data = array();
                            $update_data['status'] = $new_status;
                            $update_data['status_description'] = '';
                            $update_data['comments'] = $curlResult;
                            $update_id = $this->epaylater_transaction_model->update_data($epay_info['trimax_table_id'], $update_data);

                            $up_response = array('response' => json_encode($response), 'response_date' => get_current_formatted_date());
                            $update_respose = $this->epaylater_request_response->update_data($last_request_response_id, $up_response);
                        }
                    }
                }

                redirect(base_url() . 'admin/Postpaid/index');
            } else {
                $data['type'] = 'TSO';
                //$data['pre_operators'] = json_decode(TSO_PRE_MOBILE_OPERATORS, true);
                $data['post_operators'] = json_decode(TSO_POST_MOBILE_OPERATORS, true);
        
                load_back_view(TSO_POSTPAID_RECHARGE_VIEW, $data);
            }
        } else {
            redirect(base_url() . 'admin/Postpaid/index');
        }
    }


/*Postpaid Mobile Receipt */
    public function export_pdf($input,$epay_info='') {
      $user_id =$input['user_id'];
      if($input['type']=="JRI") {
        $type="jri";
        $trasaction_details = $this->Utilities_model->getTransactionReceiptForPostpaid($user_id,$type);
       //$pdf_gen  = $this->generatePdf($trasaction_details,$type);
      } else if($input['type']=="TSO"){
        $type="tso";
        $trasaction_details = $this->Utilities_model->getTransactionReceiptForPostpaid($user_id,$type);
       // $pdf_gen  = $this->generatePdf($trasaction_details,$type);
      } 
      if(empty($epay_info)){
        $pdf_gen  = $this->generatePdf($trasaction_details,$type);
      } else{
        $pdf_gen  = $this->generatePdf_epay($trasaction_details,$type,$epay_info);
      } 
    } 
    
    public function generatePdf($input,$path='',$trans_id=''){
        $user_id = $input['user_id'];
          if($input['type']=="JRI") {
            $type="jri";
            $trasaction_details = $this->Utilities_model->getTransactionReceiptForPostpaid($user_id,$type);
          } else if($input['type']=="TSO"){
            $type="tso";
            $trasaction_details = $this->Utilities_model->getTransactionReceiptForPostpaid($user_id,$type);
          }  
          if(empty($epay_info)){
            $result = $trasaction_details;
            $type = $type;
          } else{
            $result = $trasaction_details;
            $type = $type;
            $epay_info = $epay_info;
          }    
    $datetime = date('Y-m-d H:i:s');
    
    $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $obj_pdf->SetCreator(PDF_CREATOR);
    $title = "Mobile Postpaid Receipt";
    $obj_pdf->SetTitle($title);
    
    //$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
    $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $obj_pdf->SetDefaultMonospacedFont('helvetica');
    $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    $obj_pdf->SetFont('helvetica', '', 9);
    $obj_pdf->setFontSubsetting(false);
    $obj_pdf->AddPage();
    ob_start();

    if($type=="jri") {
        $a_mail_replacement_array = array(
                                '{{service_name}}' => 'Mobile Postpaid',
                                "{{customer_mobile_no}}" => $result->mobileno,
                                "{{location}}" => $result->location,
                                "{{provider}}" => $result->provider,
                                "{{paid_by}}" => 'Wallet',
                                "{{recharge_amount}}" => $result->amount,
                                "{{amount_in_words}}" => ucwords(getIndianCurrency($result->amount)),
                                "{{trans_no}}" => $result->wallet_trans_no,
                                "{{agent_name}}" => $result->agent_name,
                                "{{agent_code}}" => $result->agent_code,
                                );
       
    } elseif($type=="tso"){
        $tso_operator = array(
            'AP' => 'AIRTEL BILL',
            'BP' => 'BSNL BILL',
            'IP' => 'Idea BILL',
            'VP' => 'Vodafone BILL',
            'RP' => 'Reliance BILL',
            'TP' => 'Tata BILL',
            'AIP' => 'AIRCEL BILL'
        );      
    $operator ='';
    if(array_key_exists($result->operator, $tso_operator)) {
       $operator = $tso_operator[$result->operator];
    } 
        $a_mail_replacement_array = array(
                                "{{home_url}}" => base_url().'/admin/Postpaid/index',
                                "{{service_name}}" => 'Mobile Postpaid',
                                "{{customer_mobile_no}}" => $result->customer_number,
                                "{{location}}" => "",
                                "{{recharge_type}}" => $result->recharge_type,
                                "{{provider}}" => $operator,
                                "{{paid_by}}" => 'Wallet',
                                "{{recharge_amount}}" => number_format($result->amount,2),
                                "{{amount_in_words}}" => ucwords(getIndianCurrency($result->amount)),
                                "{{trans_no}}" => $result->wallet_trans_no,
                                "{{agent_name}}" => $result->agent_name,
                                "{{agent_code}}" => $result->agent_code,
                            );
    }
    $content = $data['agent_mail_content'] = str_replace(array_keys($a_mail_replacement_array),array_values($a_mail_replacement_array),MOBILE_POSTPAID_RECEIPT);
   
     if($type=="jri") {
        $mobileno = $result->mobileno;
        $table_name = 'jri_transaction_details';
    } elseif($type=="tso"){
        $mobileno = $result->customer_number;
        $table_name = 'tso_transaction_details';
    }
    $filename = $path.'MobilePostpaid_'.$mobileno.'_'.$datetime.'.pdf';
    
    $update_data = array(
        'receipt_name' => $filename,
        'receipt_data' => $content
    );
    $id = $this->common_model->update($table_name, 'transaction_id', $trans_id, $update_data);
    
       // show($content,1);     
    ob_end_clean();
//    $obj_pdf->writeHTML($content, true, false, true, false, '');
//    $obj_pdf->Output('Mobile Postpaid Receipt.pdf', 'I');
    $fileData = array();
    $fileData['file_name'] = $filename;
    $fileData['file_content'] = $content;
    return $fileData;
    }

    public function generatePdf_epay($input,$epay_info,$path='',$trans_id=''){
         $user_id = $input['user_id'];
          if($input['type']=="JRI") {
            $type="jri";
            $trasaction_details = $this->Utilities_model->getTransactionReceiptForPrepaid($user_id,$type);
          } else if($input['type']=="TSO"){
            $type="tso";
            $trasaction_details = $this->Utilities_model->getTransactionReceiptForPrepaid($user_id,$type);
          }  
          if(empty($epay_info)){
            $result = $trasaction_details;
            $type = $type;
          } else{
            $result = $trasaction_details;
            $type = $type;
            $epay_info = $epay_info;
          }    
         $datetime = date('Y-m-d H:i:s');
         
        $obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $obj_pdf->SetCreator(PDF_CREATOR);
        $title = "ePay Mobile Postpaid Receipt";
        $obj_pdf->SetTitle($title);
        
        //$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
        $obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $obj_pdf->SetDefaultMonospacedFont('helvetica');
        $obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $obj_pdf->SetFont('helvetica', '', 9);
        $obj_pdf->setFontSubsetting(false);
        $obj_pdf->AddPage();
        ob_start();

        if($type=="jri") {
            $a_mail_replacement_array = array(
                                    '{{service_name}}' => 'Mobile Postpaid',
                                    "{{customer_mobile_no}}" => $result->mobileno,
                                    "{{location}}" => $result->location,
                                    "{{provider}}" => $result->provider,
                                    "{{paid_by}}" => 'Wallet',
                                    "{{recharge_amount}}" => $result->amount,
                                    "{{amount_in_words}}" => ucwords(getIndianCurrency($result->amount)),
                                    "{{trans_no}}" => $result->systemReference,
                                    "{{agent_name}}" => $result->agent_name,
                                    "{{agent_code}}" => $result->agent_code,
                                    "{{epay_trans_no}}" => $epay_info['trimax_epay_transaction_id']
                                    );
           
        } elseif($type=="tso"){
            $tso_operator = array(
                'AB' => 'AIRTEL BILL',
                'BB' => 'BSNL BILL',
                'IB' => 'Idea BILL',
                'VB' => 'Vodafone BILL',
                'RB' => 'Reliance BILL',
                'TB' => 'Tata BILL'
            );      
        $operator ='';
        if(array_key_exists($result->operator, $tso_operator)) {
           $operator = $tso_operator[$result->operator];
        } 
            $a_mail_replacement_array = array(
                                    "{{service_name}}" => 'Mobile Postpaid',
                                    "{{customer_mobile_no}}" => $result->customer_number,
                                    "{{location}}" => "",
                                    "{{recharge_type}}" => $result->recharge_type,
                                    "{{provider}}" => $operator,
                                    "{{paid_by}}" => 'Wallet',
                                    "{{recharge_amount}}" => number_format($result->amount,2),
                                    "{{amount_in_words}}" => ucwords(getIndianCurrency($result->amount)),
                                    "{{trans_no}}" => $result->trans_reference,
                                    "{{agent_name}}" => $result->agent_name,
                                    "{{agent_code}}" => $result->agent_code,
                                    "{{epay_trans_no}}" => $epay_info['trimax_epay_transaction_id']
                                );
        }
        $content = $data['agent_mail_content'] = str_replace(array_keys($a_mail_replacement_array),array_values($a_mail_replacement_array),EPAYLATER_RECHARGE_RECEIPT);
        $filename = $path.'ePayMobilePostpaid_'.$mobileno.'_'.$datetime.'.pdf';
    
        $update_data = array(
            'receipt_name' => $filename,
            'receipt_data' => $content
        );
        $id = $this->common_model->update('epaylater_transaction', 'trimax_epay_transaction_id', $epay_info['trimax_epay_transaction_id'], $update_data);

           // show($content,1);     
        ob_end_clean();
//        $obj_pdf->writeHTML($content, true, false, true, false, '');
//        $obj_pdf->Output('Mobile Postpaid Receipt.pdf', 'I');
         $fileData = array();
        $fileData['file_name'] = $filename;
        $fileData['file_content'] = $content;
        return $fileData;
    }
    
    public function receipt_page(){
       load_back_view(POSTPAID_RECEIPT_PAGE);
    }
}
