<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cb_machine_master_sync extends CI_Controller {

    public function __construct() {
        parent::__construct();
        is_logged_in(); 
        $this->load->model(array('machine_allocation_model'));
        $this->load->library('form_validation');
    }

    public function index() {
        $data = array();
        $data['user_data'] = $this->session->userdata();
        $data['user_type'] = $data['user_data']['role_name'];
        $RollID = $this->session->userdata("role_id");
        $UserID = $this->session->userdata("user_id");
        if ($RollID == DISTRIBUTOR) {
            $UserData = $this->machine_allocation_model->getMachineAllotedData($UserID);
            $data['UserList'] = $UserData;
        }
        load_back_view(MACHINE_SYNC_VIEW, $data);
    }

    public function master_synch() {
        $id = $_POST['id'];
        $now = date('Y-m-d H:i:s');
        $Value = 'P';

        $check = $this->machine_allocation_model->checkSyncData($id);
        if ($check) {
            echo "exist";
        } else {
            if ($id) {
                $data = array(
                    'depot_master' => $Value,
                    'busservice_master' => $Value,
                    'concession_master' => $Value,
                    'cause_master' => $Value,
                    'service_master' => $Value,
                    'luggage_master' => $Value,
                    'footer' => $Value,
                    'concession_rates' => $Value,
                    'rto_circle' => $Value,
                    'city_fare' => $Value,
                    'stagekm_conversion' => $Value,
                    'city_kmstage' => $Value,
                    'ticket_type' => $Value,
                    'reservation_quotas' => $Value,
                    'substage_amount' => $Value,
                    'route_type' => $Value,
                    'ticket_para' => $Value,
                    'is_status' => 'P',
                    'is_deleted' => 'N',
                    'created_by' => $this->session->userdata('user_id'),
                    'created_date' => $now,
                    'SW_Machine_No' => $id
                );

                $id = $this->machine_allocation_model->masterSyncData($data);
                if ($id) {
                    echo "success";
                } else {
                    echo "fail";
                }
            }
        }
    }

    public function master_resynch() {
        $id = $_POST['id'];
        $now = date('Y-m-d H:i:s');
        $Value = 'P';

        $data = array(
            'depot_master' => $Value,
            'busservice_master' => $Value,
            'concession_master' => $Value,
            'cause_master' => $Value,
            'service_master' => $Value,
            'luggage_master' => $Value,
            'footer' => $Value,
            'concession_rates' => $Value,
            'rto_circle' => $Value,
            'city_fare' => $Value,
            'stagekm_conversion' => $Value,
            'city_kmstage' => $Value,
            'ticket_type' => $Value,
            'reservation_quotas' => $Value,
            'substage_amount' => $Value,
            'route_type' => $Value,
            'ticket_para' => $Value,
            'is_status' => 'P',
            'is_deleted' => 'N',
            'created_by' => $this->session->userdata('user_id'),
            'created_date' => $now,
            'SW_Machine_No' => $id
        );

        $check = $this->machine_allocation_model->reSyncData($id, $data);
        if ($check) {
            echo "success";
        } else {
            echo "fail";
        }
    }

}